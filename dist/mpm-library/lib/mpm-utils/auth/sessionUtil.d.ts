import { Subscription } from 'rxjs';
export declare class SessionInfo {
    static _instance: SessionInfo;
    checkName: string;
    sessionInfoCookieName: string;
    samlArtifactCookieName: string;
    samlArtifactCookiePath: string;
    otmmSessionSubscription: Subscription;
    constructor();
    static getInstance(): SessionInfo;
    setSessionInfo(preLoginInfoObj: any): boolean;
    getSessionInfoObj(): any;
    getSessionProp(propertyName: any): any;
}
