export declare const GatewayModule: {
    ajaxSetup: {
        converters: {
            'text soap'(result: any): any;
            'soap json'(result: any): any;
        };
    };
    extensions: {
        x2jsRaw: any;
        X2JS: any;
        soap(config: any): any;
    };
};
