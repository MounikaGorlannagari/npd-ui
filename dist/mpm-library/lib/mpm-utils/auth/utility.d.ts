export declare function findObjectsByProp(obj: any, name: any): Array<any>;
export declare function findObjects(obj: any, name: any, val: any): Array<any>;
export declare function findObject(obj: any, name: any, val: any): any;
export declare function findObjectByProp(obj: any, name: any): any;
export declare function isEqualObjects(x: any, y: any): boolean;
export declare function validateDate(dateString: any): boolean;
export declare function getDateWithLocaleTimeZone(date: any): Date | "";
export declare function showConsoleWarning(): void;
/**
 * @author SathishSrinivas
 * @param listItem
 * @param searchText
 * @param keyName
 */
export declare function findValueInList(listItem: Array<any>, searchText: string, keyName: string): any;
