export interface EnvConfig {
    'production': boolean;
    'environment': string;
    'gatewayProtocol': string;
    'gatewayPort': string;
    'gatewayHost': string;
    'psHome': string;
    'organizationName': string;
    'gatewayUrl': string;
    'otdsProtocol': string;
    'otdsHost': string;
    'otdsPort': string;
    'otdsRestUrl': string;
    'preLoginUrl': string;
    'psSSOLoginUrl': string;
    'psSSOLogoutUrl': string;
    'psOtdsResource': string;
    'psGatewayLogoutUrl': string;
    'psTicketConsumerUrl': string;
    'psDeployedPath': string;
    'logoutUrl': string;
    'otmmSessionCookieName': string;
    'otdsTicketCookieName': string;
    'instanceIdentifier': string;
    'maxFileSize'?: string;
    'maxFiles'?: number;
    'qdsProtocol': string;
    'qdsHost': string;
    'qdsPort': string;
    'qdsVersion': string;
    'qdsLibraryUrl': string;
    'brandConfigPath'?: string;
    'enableIndexerFieldRestriction'?: boolean;
    'maximumFieldsAllowed'?: number;
    'freezeColumnCount'?: number;
    'projectClassificationItemId':string
}
