export declare const otmmServicesConstants: {
    APP_ID: string;
    otmmapiBaseUrl: string;
    searchAssetUrl: string;
    textAssetSearchUrl: string;
    savedSearchUrl: string;
    userDetailsUrl: string;
    otmmSessionUrl: string;
    qdsSessionUrl: string;
    asssetUrl: string;
    importJobUrl: string;
    checkinUrl: string;
    assetStateUrl: string;
    renditionsUrl: string;
    assetFoldersUrl: string;
    exportContent: string;
    systemdetailsUrl: string;
    otmmCredential: string;
    sessionContentType: string;
    metadatamodels: string;
    searchoperators: string;
    foldersUrl: string;
    savedsearches: string;
    searchconfigurations: string;
    lookupdomains: string;
    facetconfigurations: string;
    CONTENT_TYPE: {
        URLENCODED: string;
        MULTIPARTFORMDATA: string;
        JSONDATA: string;
        UNDEFINED: any;
    };
    assetSearchSubTypes: {
        SEARCH_SCALAR_CONDITION: string;
        SEARCH_METADATA_CONDITION: string;
        SEARCH_CUSTOM_CONDITION: string;
        SEARCH_VOCABULARY_CONDITION: string;
        SEARCH_FULLTEXT_CONDITION: string;
        SEARCH_TABULAR_CONDITION: string;
        SEARCH_TABULAR_FIELD_CONDITION: string;
    };
    assetSelectionContextSubTypes: {
        SEARCH_SELECTION_CONTEXT: string;
        FOLDER_SELECTION_CONTEXT: string;
        CATEGORY_SELECTION_CONTEXT: string;
        ASSET_IDS_SELECTION_CONTEXT: string;
        LIGHT_BOX_SELECTION_CONTEXT: string;
        ASSET_COLLECTION_SELECTION_CONTEXT: string;
        RECENT_ACCESS_SELECTION_CONTEXT: string;
        REVIEW_JOB_SELECTION_CONTEXT: string;
    };
    assetSearchSelectionContextSubTypes: {
        FACET_SIMPLE_FIELD_RESTRICTION: string;
        FACET_NUMERIC_RANGE_FIELD_RESTRICTION: string;
        FACET_NUMERIC_INTERVAL_FIELD_RESTRICTION: string;
        FACET_DATE_RANGE_FIELD_RESTRICTION: string;
        FACET_DATE_INTERVAL_FIELD_RESTRICTION: string;
        FACET_CASCADING_FIELD_RESTRICTION: string;
    };
    loadType: {
        METADATA: string;
        CUSTOM: string;
        INHERITED_METADATA: string;
        SYSTEM: string;
        FULL: string;
    };
    levelOfDetail: {
        SLIM: string;
        FULL: string;
    };
    preferenceID: {
        GALLERY_VIEW: string;
    };
    defaultAssetMetadataFields: {
        ID: string;
    };
    relationalOperators: {
        IS: {
            ID: string;
            NAME: string;
        };
    };
    OTMM_SERVICE_VARIABLES: {
        maskingType: string;
        mediaManagerConfigUrl: string;
        httpOTMMTicket: string;
        webServerOtmmCookieName: string;
        isSessionMasking: boolean;
        userSessionId: string;
        isQDSUpload: boolean;
        findQDSClient: boolean;
        showQDSTransferTray: boolean;
    };
};
