import { SessionInfo } from '../auth/sessionUtil';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as ɵngcc0 from '@angular/core';
export declare class AuthenticationService {
    http: HttpClient;
    static sessionUtil: SessionInfo;
    constructor(http: HttpClient);
    setSessionInfo: (data: any) => void;
    getPreLoginDetails: () => Observable<unknown>;
    getUserInfo: () => Observable<unknown>;
    authenticateInOtds: (username: any, password: any) => Observable<unknown>;
    logout: () => Observable<unknown>;
    setPreLoginCookie: () => Observable<any>;
    fireRestUrl(url: any, urlparam: any): Observable<Object>;
    getauthenticationCookie(url: any, paramters: any): Observable<Object>;
    getCookieValue(cname: any): string;
    getUrlParams(): {};
    isValidCookie(currentCookie: any): Observable<unknown>;
    logIn(): Observable<unknown>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AuthenticationService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJhdXRoZW50aWNhdGlvbi5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU2Vzc2lvbkluZm8gfSBmcm9tICcuLi9hdXRoL3Nlc3Npb25VdGlsJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBdXRoZW50aWNhdGlvblNlcnZpY2Uge1xyXG4gICAgaHR0cDogSHR0cENsaWVudDtcclxuICAgIHN0YXRpYyBzZXNzaW9uVXRpbDogU2Vzc2lvbkluZm87XHJcbiAgICBjb25zdHJ1Y3RvcihodHRwOiBIdHRwQ2xpZW50KTtcclxuICAgIHNldFNlc3Npb25JbmZvOiAoZGF0YTogYW55KSA9PiB2b2lkO1xyXG4gICAgZ2V0UHJlTG9naW5EZXRhaWxzOiAoKSA9PiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgZ2V0VXNlckluZm86ICgpID0+IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBhdXRoZW50aWNhdGVJbk90ZHM6ICh1c2VybmFtZTogYW55LCBwYXNzd29yZDogYW55KSA9PiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgbG9nb3V0OiAoKSA9PiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgc2V0UHJlTG9naW5Db29raWU6ICgpID0+IE9ic2VydmFibGU8YW55PjtcclxuICAgIGZpcmVSZXN0VXJsKHVybDogYW55LCB1cmxwYXJhbTogYW55KTogT2JzZXJ2YWJsZTxPYmplY3Q+O1xyXG4gICAgZ2V0YXV0aGVudGljYXRpb25Db29raWUodXJsOiBhbnksIHBhcmFtdGVyczogYW55KTogT2JzZXJ2YWJsZTxPYmplY3Q+O1xyXG4gICAgZ2V0Q29va2llVmFsdWUoY25hbWU6IGFueSk6IHN0cmluZztcclxuICAgIGdldFVybFBhcmFtcygpOiB7fTtcclxuICAgIGlzVmFsaWRDb29raWUoY3VycmVudENvb2tpZTogYW55KTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIGxvZ0luKCk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbn1cclxuIl19