import { AppService } from './app.service';
import { Observable } from 'rxjs';
import { EntityAppDefService } from './entity.appdef.service';
import { UtilService } from './util.service';
import { SharingService } from './sharing.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificationService } from '../../notification/notification.service';
import { QdsService } from '../../upload/services/qds.service';
import * as ɵngcc0 from '@angular/core';
export declare class OTMMService {
    entityAppDefService: EntityAppDefService;
    appService: AppService;
    utilService: UtilService;
    sharingService: SharingService;
    notificationService: NotificationService;
    http: HttpClient;
    qdsService: QdsService;
    constructor(entityAppDefService: EntityAppDefService, appService: AppService, utilService: UtilService, sharingService: SharingService, notificationService: NotificationService, http: HttpClient, qdsService: QdsService);
    renditionCount: number;
    otmmSessionExists: boolean;
    executedCount: number;
    intervalID: any;
    isSessionMasking: boolean;
    INTERVAL_DELAY: number;
    maskingUserId: any;
    STANDARD_SESSION_TIMEOUT_INTERVAL: number;
    standardIntervalID: any;
    userDetailsObj: {
        email: string;
    };
    getSessionResponse: any;
    qdsSessionResponse: any;
    getAssetsbyAssetUrl(url: any, parameters: any): Observable<any>;
    setMasking(isMasking: any): void;
    getMasking(): boolean;
    getMaskingType(): string;
    getMediaManagerUrl(): string;
    getJessionIdFromWebServer(): string;
    fireGetRestRequest(url: any, parameters: any): Observable<any>;
    firePostRestRequest(url: any, parameters: any, contentType: any, headerObject: any): Observable<any>;
    fireRequestforJsessionCookie(url: any, parameters: any, contentType: any): Observable<any>;
    shareAssetsPostRequest(otmmapiurl: any, parameters: any, contentType: any): Observable<any>;
    fireDeleteRestRequest(url: any, httpOptions?: any): Observable<any>;
    firePatchRestRequest(url: any, parameters: any, contentType: any, headerObject: any): Observable<any>;
    firePutRestRequest(url: any, parameters: any, contentType: any, headerObject: HttpHeaders): Observable<any>;
    getCookie(cname: any): string;
    formRestParams(parameters: any): string;
    formRestUrl(otmmapiurl: any, urlparam: any, isRendition?: any): string;
    doPostRequest(url: any, parameters: any, headerObject: any, isOnlyResponse: any): Observable<unknown>;
    doPutRequest(url: any, parameters: any, headerObject: any, isOnlyResponse: any): Observable<unknown>;
    getExportJob(): void;
    checkOtmmSession(isUpload?: any, openQDSTray?: any): Observable<any>;
    getLoggedInUserDetails(userId: any): Observable<any>;
    matchSessionAttributes(compareUserId: any, loginname: any): boolean;
    startOTMMSession(initialLogin?: any): Observable<any>;
    destroyOtmmSession(): Observable<any>;
    getLoginEmailId(userId: any): Observable<unknown>;
    getUploadManifest(files: any): any;
    createUploadManifest(files: any): any;
    createUploadManifestForRevision(files: any): any;
    createTransferDetails(scheme: string, files: Array<any>): any;
    getAssetRepresentation(metadataFields: any, metaDataFieldValues: any, metadataModel: any): any;
    createAssetRepresentation(metadataFields: any, metaDataFieldValues: any, metaDataModel: any, securityPolicyID: any): any;
    uploadAssets(files: any, metadataFields: any, metadataFieldValues: any, parentForlderID: any, importTemplateID: any, metadataModel: any, securityPolicyID: any): any;
    formAssetRepresentation(metadata: any, metaDataModel: any, securityPolicyID: any): any;
    uploadSingleAsset(files: any, metaDataFieldValues: any, parentFolderID: any, importTemplateID: any, metadataModel: any, securityPolicy: any): Observable<unknown>;
    createOTMMJob(fileName: string): Observable<any>;
    importOTMMJob(files: any, metadataFields: any, metaDataFieldValues: any, parentForlderID: any, importTemplateID: any, metadataModel: any, securityPolicy: any, importJobId: any, isRevision?: any): Observable<unknown>;
    validateAssetLockState(assetList: any): boolean;
    assetCheckout(assetIds: Array<string>): Observable<unknown>;
    lockAssets(assetIds: Array<string>): Observable<unknown>;
    assetImport(fileName: any): Observable<unknown>;
    assetRendition(file: any, importJobID: any): Observable<unknown>;
    assetsRendition(files: any, importJobID: any): Observable<unknown>;
    assetCheckIn(assetDetail: any, importJobID: any): Observable<unknown>;
    getFilePaths(files: Array<any>): any[];
    formDownloadName(downloadType?: any): string;
    createExportJob(assetIds: Array<any>, downloadType: string): Observable<any>;
    getExportJobs(params: any): Observable<any>;
    getOTMMSystemDetails(): Observable<any>;
    getOTMMAssetById(assetId: any, loadType: any): Observable<any>;
    getOTMMFodlerById(folderId: any): Observable<any>;
    getSearchOperatorsList(): Observable<any>;
    getAllMetadatModelList(): Observable<any>;
    getMetadatModelById(modelId: any): Observable<any>;
    saveSearch(searchConfig: any, skip: any, top: any, userSessionId: any): Observable<any>;
    searchCustomText(searchConfig: any, skip: any, top: any, userSessionId: any): Observable<any>;
    updateMetadata(assetId: any, type: any, metadataParameters: any): Observable<any>;
    getAllSavedSearches(): Observable<any>;
    getSavedSearchById(id: any): Observable<any>;
    deleteSavedSearch(id: any): Observable<unknown>;
    addRelationalOperator(searchConditions: any[]): any[];
    getAssetVersionsByAssetId(assetId: any): Observable<any>;
    getAssetByAssetId(assetId: any): Observable<any>;
    getsearchconfigurations(): Observable<unknown>;
    getAllLookupDomains(): Observable<any>;
    getLookupDomain(lookupDomainId: string): Observable<any>;
    getFacetConfigurations(): Observable<any>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<OTMMService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RtbS5zZXJ2aWNlLmQudHMiLCJzb3VyY2VzIjpbIm90bW0uc2VydmljZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4vdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE9UTU1TZXJ2aWNlIHtcclxuICAgIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2U7XHJcbiAgICBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlO1xyXG4gICAgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlO1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIGh0dHA6IEh0dHBDbGllbnQ7XHJcbiAgICBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlO1xyXG4gICAgY29uc3RydWN0b3IoZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSwgYXBwU2VydmljZTogQXBwU2VydmljZSwgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsIGh0dHA6IEh0dHBDbGllbnQsIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2UpO1xyXG4gICAgcmVuZGl0aW9uQ291bnQ6IG51bWJlcjtcclxuICAgIG90bW1TZXNzaW9uRXhpc3RzOiBib29sZWFuO1xyXG4gICAgZXhlY3V0ZWRDb3VudDogbnVtYmVyO1xyXG4gICAgaW50ZXJ2YWxJRDogYW55O1xyXG4gICAgaXNTZXNzaW9uTWFza2luZzogYm9vbGVhbjtcclxuICAgIElOVEVSVkFMX0RFTEFZOiBudW1iZXI7XHJcbiAgICBtYXNraW5nVXNlcklkOiBhbnk7XHJcbiAgICBTVEFOREFSRF9TRVNTSU9OX1RJTUVPVVRfSU5URVJWQUw6IG51bWJlcjtcclxuICAgIHN0YW5kYXJkSW50ZXJ2YWxJRDogYW55O1xyXG4gICAgdXNlckRldGFpbHNPYmo6IHtcclxuICAgICAgICBlbWFpbDogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIGdldFNlc3Npb25SZXNwb25zZTogYW55O1xyXG4gICAgcWRzU2Vzc2lvblJlc3BvbnNlOiBhbnk7XHJcbiAgICBnZXRBc3NldHNieUFzc2V0VXJsKHVybDogYW55LCBwYXJhbWV0ZXJzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBzZXRNYXNraW5nKGlzTWFza2luZzogYW55KTogdm9pZDtcclxuICAgIGdldE1hc2tpbmcoKTogYm9vbGVhbjtcclxuICAgIGdldE1hc2tpbmdUeXBlKCk6IHN0cmluZztcclxuICAgIGdldE1lZGlhTWFuYWdlclVybCgpOiBzdHJpbmc7XHJcbiAgICBnZXRKZXNzaW9uSWRGcm9tV2ViU2VydmVyKCk6IHN0cmluZztcclxuICAgIGZpcmVHZXRSZXN0UmVxdWVzdCh1cmw6IGFueSwgcGFyYW1ldGVyczogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZmlyZVBvc3RSZXN0UmVxdWVzdCh1cmw6IGFueSwgcGFyYW1ldGVyczogYW55LCBjb250ZW50VHlwZTogYW55LCBoZWFkZXJPYmplY3Q6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGZpcmVSZXF1ZXN0Zm9ySnNlc3Npb25Db29raWUodXJsOiBhbnksIHBhcmFtZXRlcnM6IGFueSwgY29udGVudFR5cGU6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHNoYXJlQXNzZXRzUG9zdFJlcXVlc3Qob3RtbWFwaXVybDogYW55LCBwYXJhbWV0ZXJzOiBhbnksIGNvbnRlbnRUeXBlOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBmaXJlRGVsZXRlUmVzdFJlcXVlc3QodXJsOiBhbnksIGh0dHBPcHRpb25zPzogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZmlyZVBhdGNoUmVzdFJlcXVlc3QodXJsOiBhbnksIHBhcmFtZXRlcnM6IGFueSwgY29udGVudFR5cGU6IGFueSwgaGVhZGVyT2JqZWN0OiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBmaXJlUHV0UmVzdFJlcXVlc3QodXJsOiBhbnksIHBhcmFtZXRlcnM6IGFueSwgY29udGVudFR5cGU6IGFueSwgaGVhZGVyT2JqZWN0OiBIdHRwSGVhZGVycyk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldENvb2tpZShjbmFtZTogYW55KTogc3RyaW5nO1xyXG4gICAgZm9ybVJlc3RQYXJhbXMocGFyYW1ldGVyczogYW55KTogc3RyaW5nO1xyXG4gICAgZm9ybVJlc3RVcmwob3RtbWFwaXVybDogYW55LCB1cmxwYXJhbTogYW55LCBpc1JlbmRpdGlvbj86IGFueSk6IHN0cmluZztcclxuICAgIGRvUG9zdFJlcXVlc3QodXJsOiBhbnksIHBhcmFtZXRlcnM6IGFueSwgaGVhZGVyT2JqZWN0OiBhbnksIGlzT25seVJlc3BvbnNlOiBhbnkpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgZG9QdXRSZXF1ZXN0KHVybDogYW55LCBwYXJhbWV0ZXJzOiBhbnksIGhlYWRlck9iamVjdDogYW55LCBpc09ubHlSZXNwb25zZTogYW55KTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIGdldEV4cG9ydEpvYigpOiB2b2lkO1xyXG4gICAgY2hlY2tPdG1tU2Vzc2lvbihpc1VwbG9hZD86IGFueSwgb3BlblFEU1RyYXk/OiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRMb2dnZWRJblVzZXJEZXRhaWxzKHVzZXJJZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgbWF0Y2hTZXNzaW9uQXR0cmlidXRlcyhjb21wYXJlVXNlcklkOiBhbnksIGxvZ2lubmFtZTogYW55KTogYm9vbGVhbjtcclxuICAgIHN0YXJ0T1RNTVNlc3Npb24oaW5pdGlhbExvZ2luPzogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZGVzdHJveU90bW1TZXNzaW9uKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldExvZ2luRW1haWxJZCh1c2VySWQ6IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBnZXRVcGxvYWRNYW5pZmVzdChmaWxlczogYW55KTogYW55O1xyXG4gICAgY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXM6IGFueSk6IGFueTtcclxuICAgIGNyZWF0ZVVwbG9hZE1hbmlmZXN0Rm9yUmV2aXNpb24oZmlsZXM6IGFueSk6IGFueTtcclxuICAgIGNyZWF0ZVRyYW5zZmVyRGV0YWlscyhzY2hlbWU6IHN0cmluZywgZmlsZXM6IEFycmF5PGFueT4pOiBhbnk7XHJcbiAgICBnZXRBc3NldFJlcHJlc2VudGF0aW9uKG1ldGFkYXRhRmllbGRzOiBhbnksIG1ldGFEYXRhRmllbGRWYWx1ZXM6IGFueSwgbWV0YWRhdGFNb2RlbDogYW55KTogYW55O1xyXG4gICAgY3JlYXRlQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YUZpZWxkczogYW55LCBtZXRhRGF0YUZpZWxkVmFsdWVzOiBhbnksIG1ldGFEYXRhTW9kZWw6IGFueSwgc2VjdXJpdHlQb2xpY3lJRDogYW55KTogYW55O1xyXG4gICAgdXBsb2FkQXNzZXRzKGZpbGVzOiBhbnksIG1ldGFkYXRhRmllbGRzOiBhbnksIG1ldGFkYXRhRmllbGRWYWx1ZXM6IGFueSwgcGFyZW50Rm9ybGRlcklEOiBhbnksIGltcG9ydFRlbXBsYXRlSUQ6IGFueSwgbWV0YWRhdGFNb2RlbDogYW55LCBzZWN1cml0eVBvbGljeUlEOiBhbnkpOiBhbnk7XHJcbiAgICBmb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YTogYW55LCBtZXRhRGF0YU1vZGVsOiBhbnksIHNlY3VyaXR5UG9saWN5SUQ6IGFueSk6IGFueTtcclxuICAgIHVwbG9hZFNpbmdsZUFzc2V0KGZpbGVzOiBhbnksIG1ldGFEYXRhRmllbGRWYWx1ZXM6IGFueSwgcGFyZW50Rm9sZGVySUQ6IGFueSwgaW1wb3J0VGVtcGxhdGVJRDogYW55LCBtZXRhZGF0YU1vZGVsOiBhbnksIHNlY3VyaXR5UG9saWN5OiBhbnkpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgY3JlYXRlT1RNTUpvYihmaWxlTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgaW1wb3J0T1RNTUpvYihmaWxlczogYW55LCBtZXRhZGF0YUZpZWxkczogYW55LCBtZXRhRGF0YUZpZWxkVmFsdWVzOiBhbnksIHBhcmVudEZvcmxkZXJJRDogYW55LCBpbXBvcnRUZW1wbGF0ZUlEOiBhbnksIG1ldGFkYXRhTW9kZWw6IGFueSwgc2VjdXJpdHlQb2xpY3k6IGFueSwgaW1wb3J0Sm9iSWQ6IGFueSwgaXNSZXZpc2lvbj86IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICB2YWxpZGF0ZUFzc2V0TG9ja1N0YXRlKGFzc2V0TGlzdDogYW55KTogYm9vbGVhbjtcclxuICAgIGFzc2V0Q2hlY2tvdXQoYXNzZXRJZHM6IEFycmF5PHN0cmluZz4pOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgbG9ja0Fzc2V0cyhhc3NldElkczogQXJyYXk8c3RyaW5nPik6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBhc3NldEltcG9ydChmaWxlTmFtZTogYW55KTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIGFzc2V0UmVuZGl0aW9uKGZpbGU6IGFueSwgaW1wb3J0Sm9iSUQ6IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBhc3NldHNSZW5kaXRpb24oZmlsZXM6IGFueSwgaW1wb3J0Sm9iSUQ6IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBhc3NldENoZWNrSW4oYXNzZXREZXRhaWw6IGFueSwgaW1wb3J0Sm9iSUQ6IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBnZXRGaWxlUGF0aHMoZmlsZXM6IEFycmF5PGFueT4pOiBhbnlbXTtcclxuICAgIGZvcm1Eb3dubG9hZE5hbWUoZG93bmxvYWRUeXBlPzogYW55KTogc3RyaW5nO1xyXG4gICAgY3JlYXRlRXhwb3J0Sm9iKGFzc2V0SWRzOiBBcnJheTxhbnk+LCBkb3dubG9hZFR5cGU6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldEV4cG9ydEpvYnMocGFyYW1zOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRPVE1NU3lzdGVtRGV0YWlscygpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRPVE1NQXNzZXRCeUlkKGFzc2V0SWQ6IGFueSwgbG9hZFR5cGU6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldE9UTU1Gb2RsZXJCeUlkKGZvbGRlcklkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRTZWFyY2hPcGVyYXRvcnNMaXN0KCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldEFsbE1ldGFkYXRNb2RlbExpc3QoKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0TWV0YWRhdE1vZGVsQnlJZChtb2RlbElkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBzYXZlU2VhcmNoKHNlYXJjaENvbmZpZzogYW55LCBza2lwOiBhbnksIHRvcDogYW55LCB1c2VyU2Vzc2lvbklkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBzZWFyY2hDdXN0b21UZXh0KHNlYXJjaENvbmZpZzogYW55LCBza2lwOiBhbnksIHRvcDogYW55LCB1c2VyU2Vzc2lvbklkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICB1cGRhdGVNZXRhZGF0YShhc3NldElkOiBhbnksIHR5cGU6IGFueSwgbWV0YWRhdGFQYXJhbWV0ZXJzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRBbGxTYXZlZFNlYXJjaGVzKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldFNhdmVkU2VhcmNoQnlJZChpZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZGVsZXRlU2F2ZWRTZWFyY2goaWQ6IGFueSk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBhZGRSZWxhdGlvbmFsT3BlcmF0b3Ioc2VhcmNoQ29uZGl0aW9uczogYW55W10pOiBhbnlbXTtcclxuICAgIGdldEFzc2V0VmVyc2lvbnNCeUFzc2V0SWQoYXNzZXRJZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0QXNzZXRCeUFzc2V0SWQoYXNzZXRJZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0c2VhcmNoY29uZmlndXJhdGlvbnMoKTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIGdldEFsbExvb2t1cERvbWFpbnMoKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0TG9va3VwRG9tYWluKGxvb2t1cERvbWFpbklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRGYWNldENvbmZpZ3VyYXRpb25zKCk6IE9ic2VydmFibGU8YW55PjtcclxufVxyXG4iXX0=