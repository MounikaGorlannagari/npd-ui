export declare const SearchConfigConstants: {
    SEARCH_NAME: {
        PROJECT: string;
        TASK: string;
        DELIVERABLE: string;
        MY_TASK_GROUP_BY_PROJECT: string;
        MY_TASK_GROUP_BY_TASK: string;
        PROJECT_TEMPLATE: string;
        MY_TASK_GROUP_BY_DELIVERABLE: string;
        ASSET_DETAILS: string;
        CAMPAIGN: string;
        MY_TASK_GROUP_BY_CAMPAIGN: string;
        RM_GROUP_BY_CAMPAIGN: string;
        RM_GROUP_BY_PROJECT: string;
        RM_GROUP_BY_TASK: string;
    };
};
