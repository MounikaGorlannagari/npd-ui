import { MPMMenuType } from './MenuTypes';
export interface MPMMenu {
    DISPLAY_NAME?: string;
    ICON?: string;
    IS_EXTERNAL_APP_URL?: string;
    LOCATION?: string;
    SEQUENCE?: number;
    TYPE?: MPMMenuType;
}
