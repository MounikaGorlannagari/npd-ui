export interface Roles {
    'MPM_APP_Roles-id'?: {
        Id?: string;
        ItemId?: string;
    };
    ROLE_NAME?: string;
    ROLE_DN?: string;
}
