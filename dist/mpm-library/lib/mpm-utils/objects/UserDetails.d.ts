export interface UserDetails {
    UserDN: string;
    UserDisplayName: string;
    UserId: string;
    CanQueryOrgUnits: string;
    ManagerFor: {
        Target: [{
            Id: string;
            Name: string;
            Type: string;
        }];
    };
}
