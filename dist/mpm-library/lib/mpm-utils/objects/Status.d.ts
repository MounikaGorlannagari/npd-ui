import { StatusLevel } from './StatusType';
export interface Status {
    'MPM_Status-id': {
        Id: string;
        ItemId: string;
    };
    TYPE: string;
    DISPLAY_ORDER: string;
    DESCRIPTION: string;
    IS_DEFAULT: boolean;
    IS_ACTIVE: boolean;
    NAME: string;
    R_PO_CATAGORY_LEVEL: {
        'MPM_Category_Level-id': {
            Id: string;
            ItemId: string;
        };
    };
    R_PO_STATUS_TYPE: {
        'MPM_Status_Type-id': {
            Id: string;
            ItemId: string;
        };
    };
    STATUS_LEVEL?: StatusLevel;
    STATUS_TYPE?: string;
}
