export interface OTMMTabularFieldList {
    type?: string;
    metadata_field_id?: string;
    relational_operator_id?: string;
    relational_operator_name?: string;
    value?: any;
}
export interface OTMMSearchCondition {
    type?: string;
    metadata_table_id?: string;
    tabular_field_list?: Array<OTMMTabularFieldList>;
    metadata_field_id?: string;
    relational_operator?: string;
    relational_operator_id?: string;
    relational_operator_name?: string;
    value?: any;
}
