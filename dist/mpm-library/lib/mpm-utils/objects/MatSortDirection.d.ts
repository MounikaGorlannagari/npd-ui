export declare enum MatSortDirections {
    ASC = "asc",
    DESC = "desc"
}
export declare type MatSortDirection = MatSortDirections.ASC | MatSortDirections.DESC | '';
