export declare enum ProjectTypes {
    PROJECT = "Project",
    TEMPLATE = "Template",
    CAMPAIGN = "Campaign"
}
export declare type ProjectType = ProjectTypes.PROJECT | ProjectTypes.TEMPLATE | ProjectTypes.CAMPAIGN;
