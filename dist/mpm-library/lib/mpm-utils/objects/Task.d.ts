import { Tracking } from './Tracking';
export interface Task {
    'Task-id'?: {
        Id?: string;
        ItemId?: string;
    };
    NAME?: string;
    DESCRIPTION?: string;
    DUE_DATE?: string;
    END_DATE?: string;
    IS_ACTIVE?: boolean;
    IS_APPROVAL_TASK?: boolean;
    IS_DELETED?: boolean;
    OTMM_TASK_FOLDER_ID?: string;
    PROGRESS?: string;
    REVISION_REVIEW_REQUIRED?: boolean;
    START_DATE?: string;
    VIEW_OTHER_COMMENTS?: boolean;
    ASSIGNMENT_TYPE?: string;
    TASK_TYPE?: string;
    IS_ACTIVE_TASK?: boolean;
    PARENT_TASK_ID?: string;
    ROOT_PARENT_TASK_ID?: string;
    PROJECT_FLOW_TYPE?: string;
    R_PO_STATUS?: {
        'MPM_Status-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    R_PO_PRIORITY?: {
        'MPM_Priority-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    R_PO_PROJECT?: {
        'Project-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    R_PO_TEAM_ROLE?: {
        'MPM_Team_Role_Mapping-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    R_PO_OWNER_ID?: {
        'Identity-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    R_PO_ACTIVE_USER_ID?: {
        'Identity-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    Tracking?: Tracking;
}
