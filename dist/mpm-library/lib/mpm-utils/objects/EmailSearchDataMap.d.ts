export interface EmailSearchDataMap {
    'Email_Search_Data_Map-id'?: {
        Id?: string;
        ItemId?: string;
    };
    GUID?: string;
    SEARCH_CONDITION?: {
        conditions: any[];
    };
}
