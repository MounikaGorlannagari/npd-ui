export declare enum DeliverableTypes {
    UPLOAD = "UPLOAD",
    REVIEW = "REVIEW",
    ACTION = "COMPLETE"
}
export declare type DeliverableType = DeliverableTypes.UPLOAD | DeliverableTypes.REVIEW | DeliverableTypes.ACTION;
