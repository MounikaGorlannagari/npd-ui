import { ProjectType } from './ProjectTypes';
import { Tracking } from './Tracking';
export interface Project {
    'Project-id'?: {
        Id?: string;
        ItemId?: string;
    };
    OTMM_WORKFLOW_FOLDER_ID?: string;
    PROJECT_TEMPLATE_REF_ID?: string;
    IS_STANDARD_STATUS?: boolean;
    PROGRESS?: string;
    PROJECT_ID?: string;
    END_DATE?: string;
    PROJECT_NAME?: string;
    DUE_DATE?: string;
    START_DATE?: string;
    PROJECT_TYPE?: ProjectType;
    OTMM_FOLDER_ID?: string;
    PROJECT_GROUP?: string;
    IS_ACTIVE?: boolean;
    IS_ON_HOLD?: boolean;
    DESCRIPTION?: string;
    EXPECTED_DURATION?: number;
    IS_CUSTOM_WORKFLOW?: boolean;
    R_PO_TEAM?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_CATEGORY_METADATA?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_CAMPAIGN?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_STATUS?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_PROJECT_OWNER?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_PRIORITY?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_CATEGORY?: {
        Id?: string;
        ItemId?: string;
    };
    Tracking?: Tracking;
}
