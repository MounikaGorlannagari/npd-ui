export declare enum OTMMDataTypes {
    CHAR = "CHAR",
    DATE = "DATE",
    DATE_TIME = "dateTime",
    NUMBER = "NUMBER",
    STRING = "string"
}
export declare type OTMMDataType = OTMMDataTypes.DATE | OTMMDataTypes.CHAR | OTMMDataTypes.STRING | OTMMDataTypes.DATE_TIME | OTMMDataTypes.NUMBER;
