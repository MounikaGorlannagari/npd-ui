export interface Tracking {
    LastModifiedDate?: string;
    CreatedDate?: string;
    LastModifiedBy?: {
        'Identity-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
    CreatedBy?: {
        'Identity-id'?: {
            Id?: string;
            ItemId?: string;
        };
    };
}
