export declare enum OTMMFolderFilterTypes {
    ALL = "all",
    DIRECT = "direct"
}
export declare type OTMMFolderFilterType = OTMMFolderFilterTypes.ALL | OTMMFolderFilterTypes.DIRECT;
