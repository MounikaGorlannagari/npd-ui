import { ProjectType } from './ProjectTypes';
import { Tracking } from './Tracking';
export interface Campaign {
    'Campaign-id'?: {
        Id?: string;
        ItemId?: string;
    };
    OTMM_WORKFLOW_FOLDER_ID?: string;
    CAMPAIGN_ID?: string;
    CAMPAIGN_END_DATE?: string;
    CAMPAIGN_NAME?: string;
    DUE_DATE?: string;
    CAMPAIGN_START_DATE?: string;
    CAMPAIGN_TYPE?: ProjectType;
    OTMM_FOLDER_ID?: string;
    CAMPAIGN_GROUP?: string;
    IS_ACTIVE?: boolean;
    DESCRIPTION?: string;
    R_PO_STATUS?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_CAMPAIGN_OWNER?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_PRIORITY?: {
        Id?: string;
        ItemId?: string;
    };
    R_PO_CATEGORY?: {
        Id?: string;
        ItemId?: string;
    };
    Tracking?: Tracking;
}
