import { MPMField } from './MPMField';
export interface ViewConfig {
    'MPM_View_Config-id': {
        Id: string;
        ItemId: string;
    };
    VIEW: string;
    CARD_FIELD_ORDER: string;
    LIST_FIELD_ORDER: string;
    R_PO_DEFAULT_SORT_FIELD: {
        'MPM_Fields_Config-id': {
            Id: string;
            ItemId: string;
        };
    };
    R_PO_ADVANCED_SEARCH_CONFIG: string;
    R_PO_CATEGORY: {
        'MPM_Category_Level-id': {
            Id: string;
            ItemId: string;
        };
    };
    R_PM_CARD_VIEW_MPM_FIELDS?: Array<MPMField>;
    R_PM_LIST_VIEW_MPM_FIELDS?: Array<MPMField>;
    ROLE_ID?: string;
}
