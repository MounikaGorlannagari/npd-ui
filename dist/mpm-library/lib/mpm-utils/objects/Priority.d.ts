export interface Priority {
    'MPM_Priority-id': {
        Id: string;
        ItemId: string;
    };
    NAME: string;
    DISPLAY_ORDER: string;
    DESCRIPTION: string;
    IS_DEFAULT: boolean;
    IS_ACTIVE: boolean;
    ICON_DIRECTION: string;
    ICON_COLOR: string;
    R_PO_CATEGORY_LEVEL: {
        'MPM_Category_Level-id': {
            Id: string;
            ItemId: string;
        };
    };
}
