export declare enum AllocationTypes {
    ROLE = "Role",
    USER = "User"
}
export declare type AllocationType = AllocationTypes.ROLE | AllocationTypes.USER;
