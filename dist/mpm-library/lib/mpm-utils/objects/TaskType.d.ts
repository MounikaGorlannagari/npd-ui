export declare enum TaskTypes {
    NORMAL_TASK = "NORMAL_TASK",
    TASK_WITHOUT_DELIVERABLE = "TASK_WITHOUT_DELIVERABLE",
    APPROVAL_TASK = "APPROVAL_TASK"
}
export declare type TaskType = TaskTypes.NORMAL_TASK | TaskTypes.TASK_WITHOUT_DELIVERABLE | TaskTypes.APPROVAL_TASK;
