export declare const ViewTypes: {
    MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN: {
        MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN: string;
        VIEW_NAME: any;
    };
    MY_TASK_VIEW_DASHBOARD_BY_PROJECT: {
        MY_TASK_VIEW_DASHBOARD_BY_PROJECT: string;
        VIEW_NAME: any;
    };
    MY_TASK_VIEW_DASHBOARD_BY_TASK: {
        MY_TASK_VIEW_DASHBOARD_BY_TASK: string;
        VIEW_NAME: any;
    };
};
