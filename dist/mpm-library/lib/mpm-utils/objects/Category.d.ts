export interface Category {
    'MPM_Category-id': {
        Id: string;
        ItemId: string;
    };
    CATEGORY_NAME: string;
    CATEGORY_DESCRIPTION: string;
    IS_ACTIVE: boolean;
    ROOT_FOLDER_ID: string;
    PROJECT_FOLDER_ID: string;
    TEMPLATE_FOLDER_ID: string;
}
