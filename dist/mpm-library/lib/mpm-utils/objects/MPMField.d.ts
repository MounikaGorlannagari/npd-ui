import { MPM_LEVEL } from './Level';
export interface MPMField {
    'MPM_Fields_Config-id': {
        Id: string;
        ItemId: string;
    };
    EDIT_TYPE: string;
    INDEXER_FIELD_ID: string;
    SEARCHABLE: string;
    MPM_FIELD_CONFIG_ID: string;
    FIELD_LEVEL: MPM_LEVEL;
    SORTABLE: string;
    FACETABLE: string;
    OTMM_FIELD_ID: string;
    DATA_TYPE: string;
    MAPPER_NAME: string;
    DISPLAY_NAME: string;
    IS_CUSTOM_METADATA: string;
    IS_DOMAIN_VALUE?: string;
    DOMAIN_FIELD_ID?: string;
    R_PO_MPM_CATEGORY_LEVEL: {
        'MPM_Category_Level-id': {
            Id: string;
            ItemId: string;
        };
    };
    VALUE?: any;
    IS_DROP_DOWN?: boolean;
    width?: any;
}
export declare enum MPMFieldKeys {
    MAPPER_NAME = "MAPPER_NAME",
    ID = "Id",
    INDEXER_FIELD_ID = "INDEXER_FIELD_ID",
    DISPLAY_NAME = "DISPLAY_NAME",
    MPM_FIELD_CONFIG_ID = "MPM_FIELD_CONFIG_ID",
    OTMM_FIELD_ID = "OTMM_FIELD_ID"
}
