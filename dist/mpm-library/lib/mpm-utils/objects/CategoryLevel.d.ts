export interface CategoryLevel {
    'MPM_Category_Level-id': {
        Id: string;
        ItemId: string;
    };
    CATEGORY_LEVEL_DESCRIPTION: string;
    CATEGORY_LEVEL_NAME: string;
    IS_ACTIVE: string;
    METADATA_MODEL_ID: string;
    ASSET_TYPE: string;
    TEMPLATE_ID: string;
    SECURTIY_POLICY_IDS: string;
    CATEGORY_LEVEL_TYPE: string;
    R_PO_CATEGORY: {
        'MPM_Category-id': {
            Id: string;
            ItemId: string;
        };
    };
}
