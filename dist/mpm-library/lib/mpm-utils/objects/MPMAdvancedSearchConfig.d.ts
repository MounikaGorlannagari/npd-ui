import { MPMField } from './MPMField';
export interface MPMAdvancedSearchConfig {
    'MPM_Advanced_Search_Config-id': {
        Id: string;
        ItemId?: string;
    };
    SEARCH_SCOPE: string;
    SEARCH_NAME: string;
    facetConfigId: string;
    R_PM_Search_Fields?: MPMField[];
    NAME?: string;
}
