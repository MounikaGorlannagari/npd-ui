export interface Team {
    'MPM_Teams-id'?: {
        Id?: string;
        ItemId?: string;
    };
    NAME?: string;
    IS_ACTIVE?: boolean;
    OTMM_GROUP_ID?: string;
    METADATA_MODEL_ID?: string;
    ROLE_DN?: string;
}
