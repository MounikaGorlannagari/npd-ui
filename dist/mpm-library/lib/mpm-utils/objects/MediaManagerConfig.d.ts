export interface MediaManagerConfig {
    url: string;
    callbackType: string;
    sessionTimeOut: string;
    apiVersion: string;
    enableQDS: boolean;
    qdsServerURL: string;
    otdsResourceName: string;
}
