import { MatSnackBarRef } from '@angular/material/snack-bar';
import { NotificationData } from './object/NotificationData';
import * as ɵngcc0 from '@angular/core';
export declare class NotificationComponent {
    snackBarRef: MatSnackBarRef<NotificationComponent>;
    data: NotificationData;
    constructor(snackBarRef: MatSnackBarRef<NotificationComponent>, data: NotificationData);
    getIcon(): "error" | "check_circle" | "warning" | "info";
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NotificationComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NotificationComponent, "mpm-notification", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJub3RpZmljYXRpb24uY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1hdFNuYWNrQmFyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc25hY2stYmFyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uRGF0YSB9IGZyb20gJy4vb2JqZWN0L05vdGlmaWNhdGlvbkRhdGEnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBOb3RpZmljYXRpb25Db21wb25lbnQge1xyXG4gICAgc25hY2tCYXJSZWY6IE1hdFNuYWNrQmFyUmVmPE5vdGlmaWNhdGlvbkNvbXBvbmVudD47XHJcbiAgICBkYXRhOiBOb3RpZmljYXRpb25EYXRhO1xyXG4gICAgY29uc3RydWN0b3Ioc25hY2tCYXJSZWY6IE1hdFNuYWNrQmFyUmVmPE5vdGlmaWNhdGlvbkNvbXBvbmVudD4sIGRhdGE6IE5vdGlmaWNhdGlvbkRhdGEpO1xyXG4gICAgZ2V0SWNvbigpOiBcImVycm9yXCIgfCBcImNoZWNrX2NpcmNsZVwiIHwgXCJ3YXJuaW5nXCIgfCBcImluZm9cIjtcclxufVxyXG4iXX0=