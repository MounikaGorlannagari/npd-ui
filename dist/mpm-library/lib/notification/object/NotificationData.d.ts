import { NotificationType } from './NotificationTypes';
export interface NotificationData {
    message: string;
    type: NotificationType;
}
