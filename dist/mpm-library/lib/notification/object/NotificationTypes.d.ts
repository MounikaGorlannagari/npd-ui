export declare enum NotificationTypes {
    ERROR = "ERROR",
    SUCCESS = "SUCCESS",
    INFO = "INFO",
    WARN = "WARN"
}
export declare type NotificationType = NotificationTypes.ERROR | NotificationTypes.SUCCESS | NotificationTypes.INFO | NotificationTypes.WARN;
