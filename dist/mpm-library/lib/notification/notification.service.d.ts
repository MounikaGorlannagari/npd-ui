import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import * as ɵngcc0 from '@angular/core';
export declare class NotificationService {
    snackBar: MatSnackBar;
    constructor(snackBar: MatSnackBar);
    enableAutoHide: boolean;
    autoHideDuration: number;
    horizontalPosition: MatSnackBarHorizontalPosition;
    verticalPosition: MatSnackBarVerticalPosition;
    defaultClass: string;
    info: (message: string) => void;
    success: (message: string) => void;
    warn: (message: string) => void;
    error: (message: string) => void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NotificationService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9uLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0U25hY2tCYXJIb3Jpem9udGFsUG9zaXRpb24sIE1hdFNuYWNrQmFyVmVydGljYWxQb3NpdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NuYWNrLWJhcic7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE5vdGlmaWNhdGlvblNlcnZpY2Uge1xyXG4gICAgc25hY2tCYXI6IE1hdFNuYWNrQmFyO1xyXG4gICAgY29uc3RydWN0b3Ioc25hY2tCYXI6IE1hdFNuYWNrQmFyKTtcclxuICAgIGVuYWJsZUF1dG9IaWRlOiBib29sZWFuO1xyXG4gICAgYXV0b0hpZGVEdXJhdGlvbjogbnVtYmVyO1xyXG4gICAgaG9yaXpvbnRhbFBvc2l0aW9uOiBNYXRTbmFja0Jhckhvcml6b250YWxQb3NpdGlvbjtcclxuICAgIHZlcnRpY2FsUG9zaXRpb246IE1hdFNuYWNrQmFyVmVydGljYWxQb3NpdGlvbjtcclxuICAgIGRlZmF1bHRDbGFzczogc3RyaW5nO1xyXG4gICAgaW5mbzogKG1lc3NhZ2U6IHN0cmluZykgPT4gdm9pZDtcclxuICAgIHN1Y2Nlc3M6IChtZXNzYWdlOiBzdHJpbmcpID0+IHZvaWQ7XHJcbiAgICB3YXJuOiAobWVzc2FnZTogc3RyaW5nKSA9PiB2b2lkO1xyXG4gICAgZXJyb3I6IChtZXNzYWdlOiBzdHJpbmcpID0+IHZvaWQ7XHJcbn1cclxuIl19