import { OnInit, EventEmitter, ElementRef, AfterViewChecked } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { UserInfoModal } from '../objects/comment.modal';
import * as ɵngcc0 from '@angular/core';
export declare class CommentsMessageLayoutComponent implements OnInit, AfterViewChecked {
    commentUtilService: CommentsUtilService;
    commentListData: Array<any>;
    pageDetails: any;
    isScrollDown: boolean;
    userListData: Array<UserInfoModal>;
    replyToComment: EventEmitter<any>;
    loadingMoreComments: EventEmitter<any>;
    myScrollContainer: ElementRef;
    loadMore: boolean;
    navigateToReply: any;
    onScrollEvent(eventData: any): void;
    constructor(commentUtilService: CommentsUtilService);
    ngAfterViewChecked(): void;
    onloadMoreComments(eventData: any): void;
    onReplyToComment(parentComment: any): void;
    scrollToBottom(height: any): void;
    onClickingOnReplyMessage(parentCommentId: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CommentsMessageLayoutComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CommentsMessageLayoutComponent, "mpm-comments-message-layout", never, { "commentListData": "commentListData"; "pageDetails": "pageDetails"; "isScrollDown": "isScrollDown"; "userListData": "userListData"; }, { "replyToComment": "replyToComment"; "loadingMoreComments": "loadingMoreComments"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbWVzc2FnZS1sYXlvdXQuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNvbW1lbnRzLW1lc3NhZ2UtbGF5b3V0LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciwgRWxlbWVudFJlZiwgQWZ0ZXJWaWV3Q2hlY2tlZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlckluZm9Nb2RhbCB9IGZyb20gJy4uL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvbW1lbnRzTWVzc2FnZUxheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3Q2hlY2tlZCB7XHJcbiAgICBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2U7XHJcbiAgICBjb21tZW50TGlzdERhdGE6IEFycmF5PGFueT47XHJcbiAgICBwYWdlRGV0YWlsczogYW55O1xyXG4gICAgaXNTY3JvbGxEb3duOiBib29sZWFuO1xyXG4gICAgdXNlckxpc3REYXRhOiBBcnJheTxVc2VySW5mb01vZGFsPjtcclxuICAgIHJlcGx5VG9Db21tZW50OiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGxvYWRpbmdNb3JlQ29tbWVudHM6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgbXlTY3JvbGxDb250YWluZXI6IEVsZW1lbnRSZWY7XHJcbiAgICBsb2FkTW9yZTogYm9vbGVhbjtcclxuICAgIG5hdmlnYXRlVG9SZXBseTogYW55O1xyXG4gICAgb25TY3JvbGxFdmVudChldmVudERhdGE6IGFueSk6IHZvaWQ7XHJcbiAgICBjb25zdHJ1Y3Rvcihjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2UpO1xyXG4gICAgbmdBZnRlclZpZXdDaGVja2VkKCk6IHZvaWQ7XHJcbiAgICBvbmxvYWRNb3JlQ29tbWVudHMoZXZlbnREYXRhOiBhbnkpOiB2b2lkO1xyXG4gICAgb25SZXBseVRvQ29tbWVudChwYXJlbnRDb21tZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgc2Nyb2xsVG9Cb3R0b20oaGVpZ2h0OiBhbnkpOiB2b2lkO1xyXG4gICAgb25DbGlja2luZ09uUmVwbHlNZXNzYWdlKHBhcmVudENvbW1lbnRJZDogYW55KTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19