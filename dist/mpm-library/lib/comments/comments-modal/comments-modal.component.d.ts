import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import * as ɵngcc0 from '@angular/core';
export declare class CommentsModalComponent implements OnInit {
    dialogRef: MatDialogRef<CommentsModalComponent>;
    data: any;
    projectId: number;
    isManager: boolean;
    deliverableId: number;
    reviewDeliverableId: number;
    enableToComment: boolean;
    isExternalUser: any;
    constructor(dialogRef: MatDialogRef<CommentsModalComponent>, data: any);
    closeCommentsPopup(data: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CommentsModalComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CommentsModalComponent, "mpm-comments-modal", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbW9kYWwuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNvbW1lbnRzLW1vZGFsLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDb21tZW50c01vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPENvbW1lbnRzTW9kYWxDb21wb25lbnQ+O1xyXG4gICAgZGF0YTogYW55O1xyXG4gICAgcHJvamVjdElkOiBudW1iZXI7XHJcbiAgICBpc01hbmFnZXI6IGJvb2xlYW47XHJcbiAgICBkZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgICByZXZpZXdEZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgICBlbmFibGVUb0NvbW1lbnQ6IGJvb2xlYW47XHJcbiAgICBpc0V4dGVybmFsVXNlcjogYW55O1xyXG4gICAgY29uc3RydWN0b3IoZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8Q29tbWVudHNNb2RhbENvbXBvbmVudD4sIGRhdGE6IGFueSk7XHJcbiAgICBjbG9zZUNvbW1lbnRzUG9wdXAoZGF0YTogYW55KTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19