import { OnInit, EventEmitter } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { UserInfoModal, CommentsModal } from '../objects/comment.modal';
import { CommentsService } from '../services/comments.service';
import * as ɵngcc0 from '@angular/core';
export declare class CommentsMessageComponent implements OnInit {
    commentUtilService: CommentsUtilService;
    sharingService: SharingService;
    commentsService: CommentsService;
    commentData: CommentsModal;
    isReply: boolean;
    userListData: Array<UserInfoModal>;
    replyToComment: EventEmitter<any>;
    clickingOnReplyMessage: EventEmitter<any>;
    commentTextTemp: any;
    currUserId: any;
    hover: boolean;
    arrayInfo: any[];
    reasonInfo: any[];
    restartReasonInfo: any[];
    constructor(commentUtilService: CommentsUtilService, sharingService: SharingService, commentsService: CommentsService);
    ngOnInit(): void;
    onclickToReply(data: any): void;
    toggleEvent(event: any): void;
    handleDateManipulation(dateString: any): string;
    onParentCommentNavigation(parentCommentId: any): void;
    handleUserInfoDetailsTemp(commentText: string, displayName: Array<any>): any;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CommentsMessageComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CommentsMessageComponent, "mpm-comments-message", never, { "commentData": "commentData"; "isReply": "isReply"; "userListData": "userListData"; }, { "replyToComment": "replyToComment"; "clickingOnReplyMessage": "clickingOnReplyMessage"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbWVzc2FnZS5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiY29tbWVudHMtbWVzc2FnZS5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1lbnRzVXRpbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9jb21tZW50cy51dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VySW5mb01vZGFsLCBDb21tZW50c01vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9jb21tZW50Lm1vZGFsJztcclxuaW1wb3J0IHsgQ29tbWVudHNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvY29tbWVudHMuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvbW1lbnRzTWVzc2FnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2U7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBjb21tZW50c1NlcnZpY2U6IENvbW1lbnRzU2VydmljZTtcclxuICAgIGNvbW1lbnREYXRhOiBDb21tZW50c01vZGFsO1xyXG4gICAgaXNSZXBseTogYm9vbGVhbjtcclxuICAgIHVzZXJMaXN0RGF0YTogQXJyYXk8VXNlckluZm9Nb2RhbD47XHJcbiAgICByZXBseVRvQ29tbWVudDogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBjbGlja2luZ09uUmVwbHlNZXNzYWdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGNvbW1lbnRUZXh0VGVtcDogYW55O1xyXG4gICAgY3VyclVzZXJJZDogYW55O1xyXG4gICAgaG92ZXI6IGJvb2xlYW47XHJcbiAgICBhcnJheUluZm86IGFueVtdO1xyXG4gICAgcmVhc29uSW5mbzogYW55W107XHJcbiAgICByZXN0YXJ0UmVhc29uSW5mbzogYW55W107XHJcbiAgICBjb25zdHJ1Y3Rvcihjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2UsIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSwgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2UpO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxuICAgIG9uY2xpY2tUb1JlcGx5KGRhdGE6IGFueSk6IHZvaWQ7XHJcbiAgICB0b2dnbGVFdmVudChldmVudDogYW55KTogdm9pZDtcclxuICAgIGhhbmRsZURhdGVNYW5pcHVsYXRpb24oZGF0ZVN0cmluZzogYW55KTogc3RyaW5nO1xyXG4gICAgb25QYXJlbnRDb21tZW50TmF2aWdhdGlvbihwYXJlbnRDb21tZW50SWQ6IGFueSk6IHZvaWQ7XHJcbiAgICBoYW5kbGVVc2VySW5mb0RldGFpbHNUZW1wKGNvbW1lbnRUZXh0OiBzdHJpbmcsIGRpc3BsYXlOYW1lOiBBcnJheTxhbnk+KTogYW55O1xyXG59XHJcbiJdfQ==