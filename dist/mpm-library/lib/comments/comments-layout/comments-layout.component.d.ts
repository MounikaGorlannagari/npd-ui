import { OnInit, EventEmitter } from '@angular/core';
import { NewCommentModal, UserInfoModal } from '../objects/comment.modal';
import * as ɵngcc0 from '@angular/core';
export declare class CommentsLayoutComponent implements OnInit {
    commentDataList: Array<any>;
    pageDetails: any;
    isScrollDown: boolean;
    isReadonly: boolean;
    userListData: Array<UserInfoModal>;
    creatingNewComment: EventEmitter<any>;
    loadingMoreComments: EventEmitter<any>;
    enableToComment: boolean;
    isExternalUser: any;
    projectId: any;
    parentComment: any;
    constructor();
    ngOnInit(): void;
    onReplyToComment(parentComment: any): void;
    onCreatingNewComment(newComment: NewCommentModal): void;
    onLoadingMoreComments(pageDetail: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CommentsLayoutComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CommentsLayoutComponent, "mpm-comments-layout", never, { "commentDataList": "commentDataList"; "pageDetails": "pageDetails"; "isScrollDown": "isScrollDown"; "isReadonly": "isReadonly"; "userListData": "userListData"; "enableToComment": "enableToComment"; "isExternalUser": "isExternalUser"; "projectId": "projectId"; }, { "creatingNewComment": "creatingNewComment"; "loadingMoreComments": "loadingMoreComments"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbGF5b3V0LmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJjb21tZW50cy1sYXlvdXQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ld0NvbW1lbnRNb2RhbCwgVXNlckluZm9Nb2RhbCB9IGZyb20gJy4uL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvbW1lbnRzTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGNvbW1lbnREYXRhTGlzdDogQXJyYXk8YW55PjtcclxuICAgIHBhZ2VEZXRhaWxzOiBhbnk7XHJcbiAgICBpc1Njcm9sbERvd246IGJvb2xlYW47XHJcbiAgICBpc1JlYWRvbmx5OiBib29sZWFuO1xyXG4gICAgdXNlckxpc3REYXRhOiBBcnJheTxVc2VySW5mb01vZGFsPjtcclxuICAgIGNyZWF0aW5nTmV3Q29tbWVudDogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBsb2FkaW5nTW9yZUNvbW1lbnRzOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGVuYWJsZVRvQ29tbWVudDogYm9vbGVhbjtcclxuICAgIGlzRXh0ZXJuYWxVc2VyOiBhbnk7XHJcbiAgICBwcm9qZWN0SWQ6IGFueTtcclxuICAgIHBhcmVudENvbW1lbnQ6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG4gICAgb25SZXBseVRvQ29tbWVudChwYXJlbnRDb21tZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgb25DcmVhdGluZ05ld0NvbW1lbnQobmV3Q29tbWVudDogTmV3Q29tbWVudE1vZGFsKTogdm9pZDtcclxuICAgIG9uTG9hZGluZ01vcmVDb21tZW50cyhwYWdlRGV0YWlsOiBhbnkpOiB2b2lkO1xyXG59XHJcbiJdfQ==