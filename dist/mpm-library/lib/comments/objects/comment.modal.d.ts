export interface UserInfoModal {
    id: string;
    displayName: string;
    name?: string;
    commentText?: string;
}
export interface CommentsModal {
    commentId: number;
    commentText: string;
    userInfo: UserInfoModal;
    timeStamp: Date;
    isExternal: boolean;
    projectDetail: number;
    deliverableDetail: number;
    refComment: {
        refCommentId: number;
        refCommentText: string;
    };
    tagUser?: Array<UserInfoModal>;
    projectStatus?: string;
}
export interface ProjectDeliverableModal {
    titleName: string;
    icon: string;
    isActive: boolean;
    isProject: boolean;
    projectId: number;
    deliverableId: number;
    status?: string;
    owner?: string;
}
export interface NewCommentModal {
    ProjectID: number;
    DeliverableID: number;
    CommentText: string;
    RefCommentID: number;
    IsExternal: boolean;
    IsRead?: boolean;
    TagUser?: {
        R_PM_TAG_USER: Array<any>;
    };
}
