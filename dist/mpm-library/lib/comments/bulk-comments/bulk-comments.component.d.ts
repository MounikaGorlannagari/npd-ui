import { OnInit, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from '../../notification/notification.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as ɵngcc0 from '@angular/core';
export declare class BulkCommentsComponent implements OnInit {
    dialogRef: MatDialogRef<BulkCommentsComponent>;
    data: any;
    projectData: any;
    private notificationService;
    private formBuilder;
    commentFormGroup: FormGroup;
    system: any;
    crRole: any;
    mandatory: boolean;
    requireComments: boolean;
    requireReasons: boolean;
    enableReason: boolean;
    crStatusReasons: any;
    statusReasons: any;
    bulkCRAction: EventEmitter<any>;
    constructor(dialogRef: MatDialogRef<BulkCommentsComponent>, data: any, projectData: any, notificationService: NotificationService, formBuilder: FormBuilder);
    closeDialog(): void;
    bulkAction(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<BulkCommentsComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<BulkCommentsComponent, "mpm-bulk-comments", never, {}, { "bulkCRAction": "bulkCRAction"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVsay1jb21tZW50cy5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiYnVsay1jb21tZW50cy5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBCdWxrQ29tbWVudHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QnVsa0NvbW1lbnRzQ29tcG9uZW50PjtcclxuICAgIGRhdGE6IGFueTtcclxuICAgIHByb2plY3REYXRhOiBhbnk7XHJcbiAgICBwcml2YXRlIG5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyO1xyXG4gICAgY29tbWVudEZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gICAgc3lzdGVtOiBhbnk7XHJcbiAgICBjclJvbGU6IGFueTtcclxuICAgIG1hbmRhdG9yeTogYm9vbGVhbjtcclxuICAgIHJlcXVpcmVDb21tZW50czogYm9vbGVhbjtcclxuICAgIHJlcXVpcmVSZWFzb25zOiBib29sZWFuO1xyXG4gICAgZW5hYmxlUmVhc29uOiBib29sZWFuO1xyXG4gICAgY3JTdGF0dXNSZWFzb25zOiBhbnk7XHJcbiAgICBzdGF0dXNSZWFzb25zOiBhbnk7XHJcbiAgICBidWxrQ1JBY3Rpb246IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgY29uc3RydWN0b3IoZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QnVsa0NvbW1lbnRzQ29tcG9uZW50PiwgZGF0YTogYW55LCBwcm9qZWN0RGF0YTogYW55LCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIpO1xyXG4gICAgY2xvc2VEaWFsb2coKTogdm9pZDtcclxuICAgIGJ1bGtBY3Rpb24oKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19