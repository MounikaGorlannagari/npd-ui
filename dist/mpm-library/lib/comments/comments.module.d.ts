import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './comments-layout/comments-layout.component';
import * as ɵngcc2 from './comments-index/comments-index.component';
import * as ɵngcc3 from './comments-message/comments-message.component';
import * as ɵngcc4 from './comments-text/comments-text.component';
import * as ɵngcc5 from './comments.component';
import * as ɵngcc6 from './comments-message-layout/comments-message-layout.component';
import * as ɵngcc7 from './comments-modal/comments-modal.component';
import * as ɵngcc8 from './bulk-comments/bulk-comments.component';
import * as ɵngcc9 from '@angular/common';
import * as ɵngcc10 from '../material.module';
export declare class CommentsModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<CommentsModule, [typeof ɵngcc1.CommentsLayoutComponent, typeof ɵngcc2.CommentsIndexComponent, typeof ɵngcc3.CommentsMessageComponent, typeof ɵngcc4.CommentsTextComponent, typeof ɵngcc5.CommentsComponent, typeof ɵngcc6.CommentsMessageLayoutComponent, typeof ɵngcc7.CommentsModalComponent, typeof ɵngcc8.BulkCommentsComponent], [typeof ɵngcc9.CommonModule, typeof ɵngcc10.MaterialModule], [typeof ɵngcc1.CommentsLayoutComponent, typeof ɵngcc2.CommentsIndexComponent, typeof ɵngcc3.CommentsMessageComponent, typeof ɵngcc4.CommentsTextComponent, typeof ɵngcc5.CommentsComponent, typeof ɵngcc6.CommentsMessageLayoutComponent, typeof ɵngcc7.CommentsModalComponent, typeof ɵngcc8.BulkCommentsComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<CommentsModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbImNvbW1lbnRzLm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlY2xhcmUgY2xhc3MgQ29tbWVudHNNb2R1bGUge1xyXG59XHJcbiJdfQ==