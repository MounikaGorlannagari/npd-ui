import { OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectDeliverableModal } from '../objects/comment.modal';
import * as ɵngcc0 from '@angular/core';
export declare class CommentsIndexComponent implements OnInit, OnChanges {
    activatedroute: ActivatedRoute;
    commentDeliverableList: Array<ProjectDeliverableModal>;
    commentsIndexChange: EventEmitter<any>;
    notifiactionValue: any;
    deliverableIdFromUrl: any;
    dIdTopass: any;
    menuItemToPass: any;
    constructor(activatedroute: ActivatedRoute);
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    routeToSelectedMenu(menuItem: any): void;
    changeIsActiveOnNotification(event: any): any;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CommentsIndexComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CommentsIndexComponent, "mpm-comments-index", never, { "deliverableIdFromUrl": "deliverableIdFromUrl"; "commentDeliverableList": "commentDeliverableList"; "notifiactionValue": "notifiactionValue"; }, { "commentsIndexChange": "commentsIndexChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtaW5kZXguY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNvbW1lbnRzLWluZGV4LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwgfSBmcm9tICcuLi9vYmplY3RzL2NvbW1lbnQubW9kYWwnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDb21tZW50c0luZGV4Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgYWN0aXZhdGVkcm91dGU6IEFjdGl2YXRlZFJvdXRlO1xyXG4gICAgY29tbWVudERlbGl2ZXJhYmxlTGlzdDogQXJyYXk8UHJvamVjdERlbGl2ZXJhYmxlTW9kYWw+O1xyXG4gICAgY29tbWVudHNJbmRleENoYW5nZTogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBub3RpZmlhY3Rpb25WYWx1ZTogYW55O1xyXG4gICAgZGVsaXZlcmFibGVJZEZyb21Vcmw6IGFueTtcclxuICAgIGRJZFRvcGFzczogYW55O1xyXG4gICAgbWVudUl0ZW1Ub1Bhc3M6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKGFjdGl2YXRlZHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSk7XHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbiAgICByb3V0ZVRvU2VsZWN0ZWRNZW51KG1lbnVJdGVtOiBhbnkpOiB2b2lkO1xyXG4gICAgY2hhbmdlSXNBY3RpdmVPbk5vdGlmaWNhdGlvbihldmVudDogYW55KTogYW55O1xyXG59XHJcbiJdfQ==