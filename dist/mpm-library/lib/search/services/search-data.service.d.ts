import { BehaviorSubject } from 'rxjs';
import * as ɵngcc0 from '@angular/core';
export declare class SearchDataService {
    searchDataSubject: BehaviorSubject<any[]>;
    searchData: import("rxjs").Observable<any[]>;
    KEYWORD_MAPPER: string;
    constructor();
    setSearchData(data: any[]): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<SearchDataService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWRhdGEuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJzZWFyY2gtZGF0YS5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIFNlYXJjaERhdGFTZXJ2aWNlIHtcclxuICAgIHNlYXJjaERhdGFTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8YW55W10+O1xyXG4gICAgc2VhcmNoRGF0YTogaW1wb3J0KFwicnhqc1wiKS5PYnNlcnZhYmxlPGFueVtdPjtcclxuICAgIEtFWVdPUkRfTUFQUEVSOiBzdHJpbmc7XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgc2V0U2VhcmNoRGF0YShkYXRhOiBhbnlbXSk6IHZvaWQ7XHJcbn1cclxuIl19