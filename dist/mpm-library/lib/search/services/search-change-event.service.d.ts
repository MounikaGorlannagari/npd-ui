import { BehaviorSubject } from 'rxjs';
import { SearchEventModal } from '../objects/search-event-modal';
import { ViewConfig } from '../../mpm-utils/objects/ViewConfig';
import { MPMAdvancedSearchConfig } from '../../mpm-utils/objects/MPMAdvancedSearchConfig';
import * as ɵngcc0 from '@angular/core';
export declare class SearchChangeEventService {
    activeSearchId: any;
    currentSearchViewConfig: BehaviorSubject<ViewConfig>;
    onSearchViewChange: import("rxjs").Observable<ViewConfig>;
    viewToggleSearch: BehaviorSubject<any>;
    onViewToggleSearch: import("rxjs").Observable<any>;
    advancedSearchConfig: Array<any>;
    constructor();
    update(viewInfo: ViewConfig): void;
    setAdvancedSearchConfig(advancedSearchConfig: any): void;
    getAdvancedSearchConfigs(): Array<MPMAdvancedSearchConfig>;
    getAdvancedSearchConfigByName(searchIdentifier: any): number;
    getFacetConfigIdForActiveSearch(): string;
    updateOnViewChange(updateAction: SearchEventModal): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<SearchChangeEventService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNoYW5nZS1ldmVudC5zZXJ2aWNlLmQudHMiLCJzb3VyY2VzIjpbInNlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2VhcmNoRXZlbnRNb2RhbCB9IGZyb20gJy4uL29iamVjdHMvc2VhcmNoLWV2ZW50LW1vZGFsJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUFkdmFuY2VkU2VhcmNoQ29uZmlnJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlIHtcclxuICAgIGFjdGl2ZVNlYXJjaElkOiBhbnk7XHJcbiAgICBjdXJyZW50U2VhcmNoVmlld0NvbmZpZzogQmVoYXZpb3JTdWJqZWN0PFZpZXdDb25maWc+O1xyXG4gICAgb25TZWFyY2hWaWV3Q2hhbmdlOiBpbXBvcnQoXCJyeGpzXCIpLk9ic2VydmFibGU8Vmlld0NvbmZpZz47XHJcbiAgICB2aWV3VG9nZ2xlU2VhcmNoOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICAgIG9uVmlld1RvZ2dsZVNlYXJjaDogaW1wb3J0KFwicnhqc1wiKS5PYnNlcnZhYmxlPGFueT47XHJcbiAgICBhZHZhbmNlZFNlYXJjaENvbmZpZzogQXJyYXk8YW55PjtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICB1cGRhdGUodmlld0luZm86IFZpZXdDb25maWcpOiB2b2lkO1xyXG4gICAgc2V0QWR2YW5jZWRTZWFyY2hDb25maWcoYWR2YW5jZWRTZWFyY2hDb25maWc6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRBZHZhbmNlZFNlYXJjaENvbmZpZ3MoKTogQXJyYXk8TVBNQWR2YW5jZWRTZWFyY2hDb25maWc+O1xyXG4gICAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeU5hbWUoc2VhcmNoSWRlbnRpZmllcjogYW55KTogbnVtYmVyO1xyXG4gICAgZ2V0RmFjZXRDb25maWdJZEZvckFjdGl2ZVNlYXJjaCgpOiBzdHJpbmc7XHJcbiAgICB1cGRhdGVPblZpZXdDaGFuZ2UodXBkYXRlQWN0aW9uOiBTZWFyY2hFdmVudE1vZGFsKTogdm9pZDtcclxufVxyXG4iXX0=