import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './simple-search/simple-search.component';
import * as ɵngcc2 from './advanced-search/advanced-search.component';
import * as ɵngcc3 from './search-icon/search-icon.component';
import * as ɵngcc4 from './advanced-search-field/advanced-search-field.component';
import * as ɵngcc5 from '@angular/common';
import * as ɵngcc6 from '../material.module';
export declare class SearchModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<SearchModule, [typeof ɵngcc1.SimpleSearchComponent, typeof ɵngcc2.AdvancedSearchComponent, typeof ɵngcc3.SearchIconComponent, typeof ɵngcc4.AdvancedSearchFieldComponent], [typeof ɵngcc5.CommonModule, typeof ɵngcc6.MaterialModule], [typeof ɵngcc1.SimpleSearchComponent, typeof ɵngcc3.SearchIconComponent, typeof ɵngcc2.AdvancedSearchComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<SearchModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLm1vZHVsZS5kLnRzIiwic291cmNlcyI6WyJzZWFyY2gubW9kdWxlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWNsYXJlIGNsYXNzIFNlYXJjaE1vZHVsZSB7XHJcbn1cclxuIl19