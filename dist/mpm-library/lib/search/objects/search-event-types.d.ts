export declare enum SearchEventTypes {
    CLEAR = "CLEAR",
    CHANGE_KEY = "CHANGE_KEY"
}
export declare type SearchEventType = SearchEventTypes.CHANGE_KEY | SearchEventTypes.CLEAR;
