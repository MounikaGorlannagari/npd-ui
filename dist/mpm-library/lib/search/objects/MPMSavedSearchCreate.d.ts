import { ConditionList } from './ConditionList';
export interface MPMSavedSearchReq {
    NAME: string;
    DESCRIPTION: string;
    IS_PUBLIC: boolean;
    IS_ROLE_SPECIFIC: boolean;
    ROLE_ID: string;
    TYPE: string;
    IS_SYSTEM_DEFAULT_SEARCH: boolean;
    DEFAULT_OPERATOR: string;
    SEARCH_DATA: string;
    VIEW_NAME: string;
    R_PO_CREATED_USER: {
        'Identity-id': {
            Id: string;
            ItemId?: string;
        };
    };
}
export interface MPMSavedSearchCreate {
    'MPM_Saved_Searches-create': MPMSavedSearchReq;
}
export interface SearchConditionList {
    search_condition_list: {
        search_condition: ConditionList[];
    };
}
