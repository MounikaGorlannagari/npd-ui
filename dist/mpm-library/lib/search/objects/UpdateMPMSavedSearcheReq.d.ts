import { MPMSavedSearchReq } from './MPMSavedSearchCreate';
export interface UpdateMPMSavedSearcheReq {
    'MPM_Saved_Searches-id': {
        Id: string;
        ItemId?: string;
    };
    'MPM_Saved_Searches-update': MPMSavedSearchReq;
}
