import { SearchEventType } from './search-event-types';
export interface SearchEventModal {
    action: SearchEventType;
    value?: string;
}
