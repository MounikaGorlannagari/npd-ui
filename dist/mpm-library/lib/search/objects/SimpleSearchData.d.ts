import { SearchConfigData } from './SearchConfigData';
export interface SimpleSearchData {
    searchData: {
        searchValue: string;
        searchConfig: SearchConfigData;
        advancedSearchConfig: any;
    };
}
