export declare const StandardProjectValue: {
    PROJECT_TEAM: string;
    PROJECT_PRIORITY_ID: string;
    PROJECT_STATUS: string;
    PROJECT_OWNER_NAME: string;
    PROJECT_PRIORITY: string;
    PROJECT_CATEGORY: string;
};
export declare const StandardTaskValue: {
    TASK_TEAM: string;
    TASK_PRIORITY_ID: string;
    TASK_STATUS: string;
    TASK_OWNER_NAME: string;
    TASK_PRIORITY: string;
    TASK_ROLE_NAME: string;
};
export declare const StandardDeliverableValue: {
    DELIVERABLE_TEAM: string;
    DELIVERABLE_PRIORITY_ID: string;
    DELIVERABLE_STATUS: string;
    DELIVERABLE_OWNER_NAME: string;
    DELIVERABLE_PRIORITY: string;
    DELIVERABLE_APPROVER_NAME: string;
    DELIVERABLE_APPROVER_ROLE_NAME: string;
    DELIVERABLE_OWNER_ROLE_NAME: string;
};
export declare const StandardCampaignValue: {
    CAMPAIGN_PRIORITY_ID: string;
    CAMPAIGN_STATUS: string;
    CAMPAIGN_OWNER_NAME: string;
    CAMPAIGN_PRIORITY: string;
};
export declare const AdvancedSearchStandardValue: {
    ADVANCED_SEARCH_STANDARD_VALUE: string[];
};
