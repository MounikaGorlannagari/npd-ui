import { MPMSearchOperator } from '../../shared/services/objects/MPMSearchOperator';
import { MPMField } from '../../mpm-utils/objects/MPMField';
import { MPMAdvancedSearchConfig } from '../../mpm-utils/objects/MPMAdvancedSearchConfig';
import { ViewConfig } from '../../mpm-utils/objects/ViewConfig';
export interface SearchConfigData {
    advancedSearchConfiguration: MPMAdvancedSearchConfig[];
    metadataFields: MPMField[];
    searchOperatorList: MPMSearchOperator[];
    searchIdentifier: ViewConfig;
}
