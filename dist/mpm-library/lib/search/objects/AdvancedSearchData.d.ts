import { MPMSavedSearch } from '../../shared/services/objects/MPMSavedSearch';
import { SearchConfigData } from './SearchConfigData';
export interface AdvancedSearchData {
    advancedSearchConfigData: SearchConfigData;
    availableSavedSearch: MPMSavedSearch[];
    savedSearch: MPMSavedSearch;
    recentSearch: any;
}
