import { FilterRowData } from './FilterRowData';
export interface ConditionList {
    isAdvancedSearch: boolean;
    searchIdentifier: string;
    filterRowData?: FilterRowData;
    type: string;
    field_id: string;
    relational_operator_id: string;
    relational_operator_name?: string;
    value?: any;
    left_paren?: string;
    right_paren?: string;
    display_value?: string;
    relational_operator?: string;
    savedSearch?: boolean;
    savedSearchId?: string;
    mapper_field_id?: string;
    mapper_name: string;
    view: string;
    NAME?: string;
}
