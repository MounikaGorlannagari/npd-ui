import { MPMField } from '../../mpm-utils/objects/MPMField';
export interface FilterRowData {
    searchOperatorId: string;
    searchOperatorName: string;
    searchSecondValue: any;
    searchValue: any;
    issearchValueRequired: boolean;
    hasTwoValues: boolean;
    searchField: string;
    availableSearchFields: MPMField[];
    isFilterSelected: boolean;
    isComboType: boolean;
}
