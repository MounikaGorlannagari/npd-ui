import { MPMField } from '../../mpm-utils/objects/MPMField';
import { MPMSearchOperator } from '../../shared/services/objects/MPMSearchOperator';
export interface AdvancedSearchConfigData {
    availableSearchFields: MPMField[];
    searchOperatorList: MPMSearchOperator[];
}
