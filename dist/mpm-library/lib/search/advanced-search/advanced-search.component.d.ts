import { OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { SearchSaveService } from '../../project/shared/services/search.save.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { DatePipe } from '@angular/common';
import { AdvancedSearchData } from '../objects/AdvancedSearchData';
import { AdvancedSearchConfigData } from '../objects/AdvancedSearchConfigData';
import { MPMField } from '../../mpm-utils/objects/MPMField';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { ConditionList } from '../objects/ConditionList';
import { FieldConfigService } from '../../shared/services/field-config.service';
import * as ɵngcc0 from '@angular/core';
export interface Keyword {
    name: string;
}
export declare class AdvancedSearchComponent implements OnInit {
    dialogRef: MatDialogRef<AdvancedSearchComponent>;
    data: AdvancedSearchData;
    loaderService: LoaderService;
    notificationService: NotificationService;
    searchSaveService: SearchSaveService;
    formatToLocalePipe: DatePipe;
    sharingService: SharingService;
    searchConfigService: SearchConfigService;
    fieldConfigService: FieldConfigService;
    datePipe: DatePipe;
    visible: boolean;
    selectable: boolean;
    removable: boolean;
    addOnBlur: boolean;
    filterRowList: any[];
    searchTobeSavedName: string;
    advancedSearchConfigData: AdvancedSearchConfigData;
    keywords: Keyword[];
    allSearchFields: MPMField[];
    availableSearchFields: MPMField[];
    constructor(dialogRef: MatDialogRef<AdvancedSearchComponent>, data: AdvancedSearchData, loaderService: LoaderService, notificationService: NotificationService, searchSaveService: SearchSaveService, formatToLocalePipe: DatePipe, sharingService: SharingService, searchConfigService: SearchConfigService, fieldConfigService: FieldConfigService, datePipe: DatePipe);
    keyEvent(event: KeyboardEvent): void;
    add(event: MatChipInputEvent): void;
    remove(keyword: Keyword): void;
    close(data?: any): void;
    filterSelectionChange(event: any): void;
    getFieldDetails(fieldId: any): MPMField[];
    clearFilter(): void;
    removeFilter(event: any): void;
    addFilter(): void;
    validateForSearch(): boolean;
    converToLocalDate(date: any): string;
    saveSearch(searchConditions: ConditionList[], viewName: string): void;
    search(isSavedSearch: any): void;
    isFieldAlreadySelected(fieldId: string): any;
    setAvialbleSearchFiels(): void;
    mapSeachConfig(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AdvancedSearchComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<AdvancedSearchComponent, "mpm-advanced-search", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJhZHZhbmNlZC1zZWFyY2guY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNlYXJjaFNhdmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvc2VhcmNoLnNhdmUuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hEYXRhIH0gZnJvbSAnLi4vb2JqZWN0cy9BZHZhbmNlZFNlYXJjaERhdGEnO1xyXG5pbXBvcnQgeyBBZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEgfSBmcm9tICcuLi9vYmplY3RzL0FkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmRpdGlvbkxpc3QgfSBmcm9tICcuLi9vYmplY3RzL0NvbmRpdGlvbkxpc3QnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5leHBvcnQgaW50ZXJmYWNlIEtleXdvcmQge1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG59XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50PjtcclxuICAgIGRhdGE6IEFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgc2VhcmNoU2F2ZVNlcnZpY2U6IFNlYXJjaFNhdmVTZXJ2aWNlO1xyXG4gICAgZm9ybWF0VG9Mb2NhbGVQaXBlOiBEYXRlUGlwZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHNlYXJjaENvbmZpZ1NlcnZpY2U6IFNlYXJjaENvbmZpZ1NlcnZpY2U7XHJcbiAgICBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZTtcclxuICAgIGRhdGVQaXBlOiBEYXRlUGlwZTtcclxuICAgIHZpc2libGU6IGJvb2xlYW47XHJcbiAgICBzZWxlY3RhYmxlOiBib29sZWFuO1xyXG4gICAgcmVtb3ZhYmxlOiBib29sZWFuO1xyXG4gICAgYWRkT25CbHVyOiBib29sZWFuO1xyXG4gICAgZmlsdGVyUm93TGlzdDogYW55W107XHJcbiAgICBzZWFyY2hUb2JlU2F2ZWROYW1lOiBzdHJpbmc7XHJcbiAgICBhZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGE6IEFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YTtcclxuICAgIGtleXdvcmRzOiBLZXl3b3JkW107XHJcbiAgICBhbGxTZWFyY2hGaWVsZHM6IE1QTUZpZWxkW107XHJcbiAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IE1QTUZpZWxkW107XHJcbiAgICBjb25zdHJ1Y3RvcihkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxBZHZhbmNlZFNlYXJjaENvbXBvbmVudD4sIGRhdGE6IEFkdmFuY2VkU2VhcmNoRGF0YSwgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSwgc2VhcmNoU2F2ZVNlcnZpY2U6IFNlYXJjaFNhdmVTZXJ2aWNlLCBmb3JtYXRUb0xvY2FsZVBpcGU6IERhdGVQaXBlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHNlYXJjaENvbmZpZ1NlcnZpY2U6IFNlYXJjaENvbmZpZ1NlcnZpY2UsIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLCBkYXRlUGlwZTogRGF0ZVBpcGUpO1xyXG4gICAga2V5RXZlbnQoZXZlbnQ6IEtleWJvYXJkRXZlbnQpOiB2b2lkO1xyXG4gICAgYWRkKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQ7XHJcbiAgICByZW1vdmUoa2V5d29yZDogS2V5d29yZCk6IHZvaWQ7XHJcbiAgICBjbG9zZShkYXRhPzogYW55KTogdm9pZDtcclxuICAgIGZpbHRlclNlbGVjdGlvbkNoYW5nZShldmVudDogYW55KTogdm9pZDtcclxuICAgIGdldEZpZWxkRGV0YWlscyhmaWVsZElkOiBhbnkpOiBNUE1GaWVsZFtdO1xyXG4gICAgY2xlYXJGaWx0ZXIoKTogdm9pZDtcclxuICAgIHJlbW92ZUZpbHRlcihldmVudDogYW55KTogdm9pZDtcclxuICAgIGFkZEZpbHRlcigpOiB2b2lkO1xyXG4gICAgdmFsaWRhdGVGb3JTZWFyY2goKTogYm9vbGVhbjtcclxuICAgIGNvbnZlclRvTG9jYWxEYXRlKGRhdGU6IGFueSk6IHN0cmluZztcclxuICAgIHNhdmVTZWFyY2goc2VhcmNoQ29uZGl0aW9uczogQ29uZGl0aW9uTGlzdFtdLCB2aWV3TmFtZTogc3RyaW5nKTogdm9pZDtcclxuICAgIHNlYXJjaChpc1NhdmVkU2VhcmNoOiBhbnkpOiB2b2lkO1xyXG4gICAgaXNGaWVsZEFscmVhZHlTZWxlY3RlZChmaWVsZElkOiBzdHJpbmcpOiBhbnk7XHJcbiAgICBzZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk6IHZvaWQ7XHJcbiAgICBtYXBTZWFjaENvbmZpZygpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=