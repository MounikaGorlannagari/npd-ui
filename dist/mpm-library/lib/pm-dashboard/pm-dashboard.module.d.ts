import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '../shared/components/name-icon/name-icon.component';
import * as ɵngcc2 from './project-card/project-card.component';
import * as ɵngcc3 from './project-list/project-list.component';
import * as ɵngcc4 from './project-sort/project-sort.component';
import * as ɵngcc5 from '@angular/common';
import * as ɵngcc6 from '../material.module';
import * as ɵngcc7 from '../mpm-library.module';
export declare class PMDashboardModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<PMDashboardModule, [typeof ɵngcc1.NameIconComponent, typeof ɵngcc2.ProjectCardComponent, typeof ɵngcc3.ProjectListComponent, typeof ɵngcc4.ProjectSortComponent], [typeof ɵngcc5.CommonModule, typeof ɵngcc6.MaterialModule, typeof ɵngcc7.MpmLibraryModule], [typeof ɵngcc2.ProjectCardComponent, typeof ɵngcc3.ProjectListComponent, typeof ɵngcc4.ProjectSortComponent, typeof ɵngcc1.NameIconComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<PMDashboardModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG0tZGFzaGJvYXJkLm1vZHVsZS5kLnRzIiwic291cmNlcyI6WyJwbS1kYXNoYm9hcmQubW9kdWxlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVjbGFyZSBjbGFzcyBQTURhc2hib2FyZE1vZHVsZSB7XHJcbn1cclxuIl19