import { OnInit, EventEmitter } from '@angular/core';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class ProjectSortComponent implements OnInit {
    sharingService: SharingService;
    sortOptions: any;
    selectedOption: any;
    sortChange: EventEmitter<any>;
    constructor(sharingService: SharingService);
    selectSorting(): void;
    onSelectionChange(event: any, fields: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ProjectSortComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ProjectSortComponent, "mpm-project-sort", never, { "selectedOption": "selectedOption"; "sortOptions": "sortOptions"; }, { "sortChange": "sortChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1zb3J0LmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJwcm9qZWN0LXNvcnQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgUHJvamVjdFNvcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgc29ydE9wdGlvbnM6IGFueTtcclxuICAgIHNlbGVjdGVkT3B0aW9uOiBhbnk7XHJcbiAgICBzb3J0Q2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGNvbnN0cnVjdG9yKHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSk7XHJcbiAgICBzZWxlY3RTb3J0aW5nKCk6IHZvaWQ7XHJcbiAgICBvblNlbGVjdGlvbkNoYW5nZShldmVudDogYW55LCBmaWVsZHM6IGFueSk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==