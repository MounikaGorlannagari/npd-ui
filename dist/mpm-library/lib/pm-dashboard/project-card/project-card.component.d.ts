import { OnInit, EventEmitter } from '@angular/core';
import { ProjectService } from '../../project/shared/services/project.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { MPMField } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ProjectBulkCountService } from '../../shared/services/project-bulk-count.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../notification/notification.service';
import { MatDialog } from '@angular/material/dialog';
import * as ɵngcc0 from '@angular/core';
export declare class ProjectCardComponent implements OnInit {
    dialog: MatDialog;
    utilService: UtilService;
    otmmMetadataService: OtmmMetadataService;
    fieldConfigService: FieldConfigService;
    viewConfigService: ViewConfigService;
    sharingService: SharingService;
    projectBulkCountService: ProjectBulkCountService;
    router: Router;
    projectService: ProjectService;
    notificationService: NotificationService;
    project: any;
    dashboardMenuConfig: any;
    viewProjectDetails: EventEmitter<any>;
    copyProject: EventEmitter<any>;
    openCommentsModule: EventEmitter<any>;
    selectedProjectCardCount: EventEmitter<any>;
    refreshHandler: EventEmitter<any>;
    showCopyProject: boolean;
    color: '#c0ca33';
    isTemplate: boolean;
    customMetadataValues: any[];
    priorityObj: {
        color: string;
        icon: string;
        tooltip: string;
    };
    mpmFieldConstants: {
        CONTENT_TYPE: string;
        OTMM_METADATA: {
            DELIVERABLE: {
                ITEM_ID: string;
            };
        };
        METADATA_REFERENT_ID: {
            CREATE_DELIVERABLE_ASSET: string;
        };
        MPM_PROJECT_FEILDS: {
            PROJECT_OTMM_FOLDER_ID: string;
            PROJECT_IS_STANDARD: string;
            PROJECT_CATEGORY: string;
            PROJECT_DUE_DATE: string;
            PROJECT_NAME: string;
            PROJECT_STATUS: string;
            PROJECT_STATUS_ID: string;
            PROJECT_STATUS_TYPE: string;
            PROJECT_OWNER_NAME: string;
            PROJECT_OWNER_ID: string;
            PROJECT_OWNER_CN: string;
            PROJECT_PRIORITY: string;
            PROJECT_PRIORITY_ID: string;
            PROJECT_START_DATE: string;
            PROJECT_TEAM: string;
            PROJECT_TEAM_ID: string;
            PROJECT_PROGRESS: string;
            IS_PROJECT_ACTIVE: string;
            IS_PROJECT_DELETED: string;
            PROJECT_CATEGORY_ID: string;
            PROJECT_DESCRIPTION: string;
            PROJECT_END_DATE: string;
            PROJECT_ITEM_ID: string;
            PROJECT_TYPE: string;
            PROJECT_ID: string;
            PROJECT_TEMPLATE_REFERENCE_ID: string;
            PROJECT_THUMBNAIL_ASSET_ID: string;
            PROJECT_THUMBNAIL_URL: string;
            ORIGINAL_PROJECT_START_DATE: string;
            ORIGINAL_PROJECT_DUE_DATE: string;
            ACTUAL_PROJECT_START_DATE: string;
            ACTUAL_PROJECT_DUE_DATE: string;
            EXPECTED_PROJECT_DURATION: string;
            PROJECT_TIME_SPENT: string;
            PROJECT_CATEGORY_METADATA: string;
            PROJECT_CATEGORY_METADATA_ID: string;
        };
        PROJECT_METADATA_FEILDS_MAPPING: {
            PROJECT_METADATA_GROUP_NAME: string;
        };
        MPM_TASK_FIELDS: {
            TASK_ID: string;
            TASK_ITEM_ID: string;
            TASK_NAME: string;
            TASK_DESCRIPTION: string;
            TASK_STATUS: string;
            TASK_STATUS_ID: string;
            TASK_STATUS_TYPE: string;
            TASK_START_DATE: string;
            TASK_DUE_DATE: string;
            TASK_END_DATE: string;
            TASK_OWNER_NAME: string;
            TASK_OWNER_ID: string;
            TASK_OWNER_CN: string;
            TASK_PRIORITY_ID: string;
            TASK_PRIORITY: string;
            IS_TASK_ACTIVE: string;
            TASK_PROGRESS: string;
            TASK_ROLE_NAME: string;
            TASK_ROLE_ID: string;
            TASK_TYPE: string;
            TASK_ACTIVE_USER_NAME: string;
            TASK_ACTIVE_USER_ID: string;
            TASK_ACTIVE_USER_CN: string;
            TASK_ROOT_PARENT_ID: string;
            TASK_PROJECT_ID: string;
            TASK_IS_DELETED: string;
            TASK_IS_APPROVAL: string;
            PROJECT_FLOW_TYPE: string;
            TASK_ASSIGNMENT_TYPE: string;
        };
        TASK_METADATA_FIELDS_MAPPING: {
            TASK_METADATA_GROUP_NAME: string;
        };
        DELIVERABLE_MPM_FIELDS: {
            DELIVERABLE_NAME: string;
            DELIVERABLE_ITEM_ID: string;
            DELIVERABLE_ID: string;
            DELIVERABLE_TYPE: string;
            DELIVERABLE_IS_ACTIVE: string;
            DELIVERABLE_IS_DELETED: string;
            DELIVERABLE_TASK_ID: string;
            DELIVERABLE_STATUS: string;
            DELIVERABLE_STATUS_ID: string;
            DELIVERABLE_START_DATE: string;
            DELIVERABLE_ROOT_PARENT_ID: string;
            DELIVERABLE_PROJECT_ID: string;
            DELIVERABLE_PRIORITY: string;
            DELIVERABLE_PRIORITY_ID: string;
            DELIVERABLE_PARENT_ID: string;
            DELIVERABLE_END_DATE: string;
            DELIVERABLE_DUE_DATE: string;
            DELIVERABLE_DESCRIPTION: string;
            DELIVERABLE_ASSIGNMENT_TYPE: string;
            DELIVERABLE_OWNER_CN: string;
            DELIVERABLE_OWNER_ID: string;
            DELIVERABLE_OWNER_NAME: string;
            DELIVERABLE_OWNER_ROLE_ID: string;
            DELIVERABLE_OWNER_ROLE_NAME: string;
            DELIVERABLE_APPROVER_CN: string;
            DELIVERABLE_APPROVER_ID: string;
            DELIVERABLE_APPROVER_NAME: string;
            DELIVERABLE_APPROVER_ROLE_NAME: string;
            DELIVERABLE_APPROVER_ROLE_ID: string;
            DELIVERABLE_UPLOAD_ASSET_ID: string;
            DELIVERABLE_IS_UPLOADED: string;
            DELIVERABLE_ACTIVE_USER_CN: string;
            DELIVERABLE_ACTIVE_USER_NAME: string;
            DELIVERABLE_ACTIVE_USER_ID: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_CN: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_ID: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_NAME: string;
            DELIVERABLE_APPROVER_ASSIGNMENT_TYPE: string;
            DELIVERABLE_ITERATION_COUNT: string;
            DELIVERABLE_REFERENCE_LINK_ID: string;
            DELIVERABLE_IS_PM_ASSIGNED: string;
            DELIVERABLE_CAMPAIGN_ID: string;
        };
        DELIVERABLE_REVIEW_MPM_FIELDS: {
            DELIVERABLE_NAME: string;
            REVIEW_DELIVERABLE_ITEM_ID: string;
            DELIVERABLE_ID: string;
            DELIVERABLE_TYPE: string;
            DELIVERABLE_IS_ACTIVE: string;
            DELIVERABLE_IS_DELETED: string;
            DELIVERABLE_TASK_ID: string;
            DELIVERABLE_STATUS: string;
            DELIVERABLE_STATUS_ID: string;
            DELIVERABLE_START_DATE: string;
            DELIVERABLE_ROOT_PARENT_ID: string;
            DELIVERABLE_PROJECT_ID: string;
            DELIVERABLE_PRIORITY: string;
            DELIVERABLE_PRIORITY_ID: string;
            DELIVERABLE_PARENT_ID: string;
            DELIVERABLE_END_DATE: string;
            DELIVERABLE_DUE_DATE: string;
            DELIVERABLE_DESCRIPTION: string;
            DELIVERABLE_ASSIGNMENT_TYPE: string;
            DELIVERABLE_OWNER_CN: string;
            DELIVERABLE_OWNER_ID: string;
            DELIVERABLE_OWNER_NAME: string;
            DELIVERABLE_OWNER_ROLE_ID: string;
            DELIVERABLE_OWNER_ROLE_NAME: string;
            DELIVERABLE_APPROVER_CN: string;
            DELIVERABLE_APPROVER_ID: string;
            DELIVERABLE_APPROVER_NAME: string;
            DELIVERABLE_APPROVER_ROLE_NAME: string;
            DELIVERABLE_APPROVER_ROLE_ID: string;
            DELIVERABLE_UPLOAD_ASSET_ID: string;
            DELIVERABLE_IS_UPLOADED: string;
            DELIVERABLE_ACTIVE_USER_CN: string;
            DELIVERABLE_ACTIVE_USER_NAME: string;
            DELIVERABLE_ACTIVE_USER_ID: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_CN: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_ID: string;
            DELIVERABLE_APPROVER_ACTIVE_USER_NAME: string;
            DELIVERABLE_APPROVER_ASSIGNMENT_TYPE: string;
            DELIVERABLE_ITERATION_COUNT: string;
            DELIVERABLE_REFERENCE_LINK_ID: string;
            DELIVERABLE_IS_PM_ASSIGNED: string;
            IS_DELIVERABLE_REVIEW_ACTIVE: string;
            DELIVERABLE_REVIEWER_ID: string;
            DELIVERABLE_REVIEWER_CN: string;
            DELIVERABLE_REVIEWER_NAME: string;
            DELIVERABLE_REVIEW_STATUS_ID: string;
            DELIVERABLE_REVIEW_STATUS_VALUE: string;
            DELIVERABLE_REVIEW_STATUS_TYPE: string;
            REVIEW_TASK_ITEM_ID: string;
            DELIVERABLE_REVIEW_ITEM_ID: string;
            DELIVERABLE_STATUS_TYPE: string;
        };
        MPM_CAMPAIGN_FEILDS: {
            CAMPAIGN_OTMM_FOLDER_ID: string;
            CAMPAIGN_TYPE: string;
            CAMPAIGN_CATEGORY: string;
            CAMPAIGN_CATEGORY_ID: string;
            CAMPAIGN_DESCRIPTION: string;
            CAMPAIGN_END_DATE: string;
            CAMPAIGN_NAME: string;
            CAMPAIGN_STATUS: string;
            CAMPAIGN_ID: string;
            CAMPAIGN_IS_ACTIVE: string;
            CAMPAIGN_ITEM_ID: string;
            CAMPAIGN_METADATA_MODEL_ID: string;
            CAMPAIGN_OWNER_CN: string;
            CAMPAIGN_OWNER_NAME: string;
            CAMPAIGN_OWNER_ID: string;
            CAMPAIGN_PRIORITY: string;
            CAMPAIGN_PRIORITY_ID: string;
            CAMPAIGN_PROGRESS: string;
            CAMPAIGN_START_DATE: string;
            CAMPAIGN_STATUS_ID: string;
            CAMPAIGN_STATUS_TYPE: string;
        };
        CAMPAIGN_METADATA_FEILDS_MAPPING: {
            CAMPAIGN_METADATA_GROUP_NAME: string;
        };
        USER_PREFERENCE_FREEZE_COUNT: number;
    };
    displayFieldList: Array<MPMField>;
    isCampaignDashboard: boolean;
    categoryLevel: MPM_LEVELS;
    loggedInUserId: any;
    finalCompleted: any;
    affectedTask: any[];
    constructor(dialog: MatDialog, utilService: UtilService, otmmMetadataService: OtmmMetadataService, fieldConfigService: FieldConfigService, viewConfigService: ViewConfigService, sharingService: SharingService, projectBulkCountService: ProjectBulkCountService, router: Router, projectService: ProjectService, notificationService: NotificationService);
    isProjectOwner: any;
    copyToolTip: string;
    status: string[];
    enableBulkEdit: boolean;
    copiedText(element: any): void;
    openProjectDetails(project: any): void;
    openComments(): void;
    openCopyProject(project: any, isCopyTemplate: boolean): void;
    converToLocalDate(data: any, propertyId: any): any;
    getProperty(projectData: any, propertyId: any): any;
    getPriority(projectData: any, propertyId: any): {
        color: any;
        icon: string;
        tooltip: any;
    };
    getMetadataTypeValue(metadata: any): string;
    isSelected(projectDetails: any, event: any): void;
    deleteProject(project: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ProjectCardComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ProjectCardComponent, "mpm-project-card", never, { "project": "project"; "dashboardMenuConfig": "dashboardMenuConfig"; }, { "viewProjectDetails": "viewProjectDetails"; "copyProject": "copyProject"; "openCommentsModule": "openCommentsModule"; "selectedProjectCardCount": "selectedProjectCardCount"; "refreshHandler": "refreshHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1jYXJkLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJwcm9qZWN0LWNhcmQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0QnVsa0NvdW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LWJ1bGstY291bnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBQcm9qZWN0Q2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBkaWFsb2c6IE1hdERpYWxvZztcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIG90bW1NZXRhZGF0YVNlcnZpY2U6IE90bW1NZXRhZGF0YVNlcnZpY2U7XHJcbiAgICBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZTtcclxuICAgIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHByb2plY3RCdWxrQ291bnRTZXJ2aWNlOiBQcm9qZWN0QnVsa0NvdW50U2VydmljZTtcclxuICAgIHJvdXRlcjogUm91dGVyO1xyXG4gICAgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlO1xyXG4gICAgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIHByb2plY3Q6IGFueTtcclxuICAgIGRhc2hib2FyZE1lbnVDb25maWc6IGFueTtcclxuICAgIHZpZXdQcm9qZWN0RGV0YWlsczogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBjb3B5UHJvamVjdDogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBvcGVuQ29tbWVudHNNb2R1bGU6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgc2VsZWN0ZWRQcm9qZWN0Q2FyZENvdW50OiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHJlZnJlc2hIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHNob3dDb3B5UHJvamVjdDogYm9vbGVhbjtcclxuICAgIGNvbG9yOiAnI2MwY2EzMyc7XHJcbiAgICBpc1RlbXBsYXRlOiBib29sZWFuO1xyXG4gICAgY3VzdG9tTWV0YWRhdGFWYWx1ZXM6IGFueVtdO1xyXG4gICAgcHJpb3JpdHlPYmo6IHtcclxuICAgICAgICBjb2xvcjogc3RyaW5nO1xyXG4gICAgICAgIGljb246IHN0cmluZztcclxuICAgICAgICB0b29sdGlwOiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgbXBtRmllbGRDb25zdGFudHM6IHtcclxuICAgICAgICBDT05URU5UX1RZUEU6IHN0cmluZztcclxuICAgICAgICBPVE1NX01FVEFEQVRBOiB7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFOiB7XHJcbiAgICAgICAgICAgICAgICBJVEVNX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfTtcclxuICAgICAgICBNRVRBREFUQV9SRUZFUkVOVF9JRDoge1xyXG4gICAgICAgICAgICBDUkVBVEVfREVMSVZFUkFCTEVfQVNTRVQ6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIE1QTV9QUk9KRUNUX0ZFSUxEUzoge1xyXG4gICAgICAgICAgICBQUk9KRUNUX09UTU1fRk9MREVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfSVNfU1RBTkRBUkQ6IHN0cmluZztcclxuICAgICAgICAgICAgUFJPSkVDVF9DQVRFR09SWTogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX0RVRV9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1NUQVRVUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1NUQVRVU19JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1NUQVRVU19UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfT1dORVJfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX09XTkVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfT1dORVJfQ046IHN0cmluZztcclxuICAgICAgICAgICAgUFJPSkVDVF9QUklPUklUWTogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1BSSU9SSVRZX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfU1RBUlRfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1RFQU06IHN0cmluZztcclxuICAgICAgICAgICAgUFJPSkVDVF9URUFNX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfUFJPR1JFU1M6IHN0cmluZztcclxuICAgICAgICAgICAgSVNfUFJPSkVDVF9BQ1RJVkU6IHN0cmluZztcclxuICAgICAgICAgICAgSVNfUFJPSkVDVF9ERUxFVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfQ0FURUdPUllfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgUFJPSkVDVF9ERVNDUklQVElPTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX0VORF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfSVRFTV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1RZUEU6IHN0cmluZztcclxuICAgICAgICAgICAgUFJPSkVDVF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1RFTVBMQVRFX1JFRkVSRU5DRV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1RIVU1CTkFJTF9BU1NFVF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX1RIVU1CTkFJTF9VUkw6IHN0cmluZztcclxuICAgICAgICAgICAgT1JJR0lOQUxfUFJPSkVDVF9TVEFSVF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIE9SSUdJTkFMX1BST0pFQ1RfRFVFX0RBVEU6IHN0cmluZztcclxuICAgICAgICAgICAgQUNUVUFMX1BST0pFQ1RfU1RBUlRfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBBQ1RVQUxfUFJPSkVDVF9EVUVfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBFWFBFQ1RFRF9QUk9KRUNUX0RVUkFUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfVElNRV9TUEVOVDogc3RyaW5nO1xyXG4gICAgICAgICAgICBQUk9KRUNUX0NBVEVHT1JZX01FVEFEQVRBOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfQ0FURUdPUllfTUVUQURBVEFfSUQ6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIFBST0pFQ1RfTUVUQURBVEFfRkVJTERTX01BUFBJTkc6IHtcclxuICAgICAgICAgICAgUFJPSkVDVF9NRVRBREFUQV9HUk9VUF9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBNUE1fVEFTS19GSUVMRFM6IHtcclxuICAgICAgICAgICAgVEFTS19JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX0lURU1fSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfREVTQ1JJUFRJT046IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19TVEFUVVM6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19TVEFUVVNfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19TVEFUVVNfVFlQRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX1NUQVJUX0RBVEU6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19EVUVfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX0VORF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfT1dORVJfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX09XTkVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfT1dORVJfQ046IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19QUklPUklUWV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX1BSSU9SSVRZOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElTX1RBU0tfQUNUSVZFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfUFJPR1JFU1M6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19ST0xFX05BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19ST0xFX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfVFlQRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX0FDVElWRV9VU0VSX05BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19BQ1RJVkVfVVNFUl9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX0FDVElWRV9VU0VSX0NOOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfUk9PVF9QQVJFTlRfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19QUk9KRUNUX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfSVNfREVMRVRFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBUQVNLX0lTX0FQUFJPVkFMOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFBST0pFQ1RfRkxPV19UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfQVNTSUdOTUVOVF9UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBUQVNLX01FVEFEQVRBX0ZJRUxEU19NQVBQSU5HOiB7XHJcbiAgICAgICAgICAgIFRBU0tfTUVUQURBVEFfR1JPVVBfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgREVMSVZFUkFCTEVfTVBNX0ZJRUxEUzoge1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0lURU1fSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfVFlQRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9JU19BQ1RJVkU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfSVNfREVMRVRFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9UQVNLX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1NUQVRVUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9TVEFUVVNfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfU1RBUlRfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9ST09UX1BBUkVOVF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9QUk9KRUNUX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1BSSU9SSVRZOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1BSSU9SSVRZX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1BBUkVOVF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9FTkRfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9EVUVfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9ERVNDUklQVElPTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BU1NJR05NRU5UX1RZUEU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfT1dORVJfQ046IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfT1dORVJfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfT1dORVJfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9PV05FUl9ST0xFX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX09XTkVSX1JPTEVfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BUFBST1ZFUl9DTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BUFBST1ZFUl9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BUFBST1ZFUl9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX1JPTEVfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BUFBST1ZFUl9ST0xFX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1VQTE9BRF9BU1NFVF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9JU19VUExPQURFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BQ1RJVkVfVVNFUl9DTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BQ1RJVkVfVVNFUl9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FDVElWRV9VU0VSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX0FDVElWRV9VU0VSX0NOOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX0FDVElWRV9VU0VSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX0FDVElWRV9VU0VSX05BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQVBQUk9WRVJfQVNTSUdOTUVOVF9UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0lURVJBVElPTl9DT1VOVDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9SRUZFUkVOQ0VfTElOS19JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9JU19QTV9BU1NJR05FRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9DQU1QQUlHTl9JRDogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXX01QTV9GSUVMRFM6IHtcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBSRVZJRVdfREVMSVZFUkFCTEVfSVRFTV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0lTX0FDVElWRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9JU19ERUxFVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1RBU0tfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfU1RBVFVTOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1NUQVRVU19JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9TVEFSVF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1JPT1RfUEFSRU5UX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1BST0pFQ1RfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUFJJT1JJVFk6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUFJJT1JJVFlfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUEFSRU5UX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0VORF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0RVRV9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0RFU0NSSVBUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FTU0lHTk1FTlRfVFlQRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9PV05FUl9DTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9PV05FUl9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9PV05FUl9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX09XTkVSX1JPTEVfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfT1dORVJfUk9MRV9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX0NOOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX05BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQVBQUk9WRVJfUk9MRV9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FQUFJPVkVSX1JPTEVfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfVVBMT0FEX0FTU0VUX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0lTX1VQTE9BREVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FDVElWRV9VU0VSX0NOOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0FDVElWRV9VU0VSX05BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQUNUSVZFX1VTRVJfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQVBQUk9WRVJfQUNUSVZFX1VTRVJfQ046IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQVBQUk9WRVJfQUNUSVZFX1VTRVJfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfQVBQUk9WRVJfQUNUSVZFX1VTRVJfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9BUFBST1ZFUl9BU1NJR05NRU5UX1RZUEU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfSVRFUkFUSU9OX0NPVU5UOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1JFRkVSRU5DRV9MSU5LX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX0lTX1BNX0FTU0lHTkVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElTX0RFTElWRVJBQkxFX1JFVklFV19BQ1RJVkU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXRVJfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXRVJfQ046IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXRVJfTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERUxJVkVSQUJMRV9SRVZJRVdfU1RBVFVTX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1JFVklFV19TVEFUVVNfVkFMVUU6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXX1NUQVRVU19UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFJFVklFV19UQVNLX0lURU1fSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfUkVWSUVXX0lURU1fSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgREVMSVZFUkFCTEVfU1RBVFVTX1RZUEU6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIE1QTV9DQU1QQUlHTl9GRUlMRFM6IHtcclxuICAgICAgICAgICAgQ0FNUEFJR05fT1RNTV9GT0xERVJfSUQ6IHN0cmluZztcclxuICAgICAgICAgICAgQ0FNUEFJR05fVFlQRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9DQVRFR09SWTogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9DQVRFR09SWV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9ERVNDUklQVElPTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9FTkRfREFURTogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1NUQVRVUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9JU19BQ1RJVkU6IHN0cmluZztcclxuICAgICAgICAgICAgQ0FNUEFJR05fSVRFTV9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9NRVRBREFUQV9NT0RFTF9JRDogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9PV05FUl9DTjogc3RyaW5nO1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9PV05FUl9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX09XTkVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1BSSU9SSVRZOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1BSSU9SSVRZX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1BST0dSRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1NUQVJUX0RBVEU6IHN0cmluZztcclxuICAgICAgICAgICAgQ0FNUEFJR05fU1RBVFVTX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIENBTVBBSUdOX1NUQVRVU19UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBDQU1QQUlHTl9NRVRBREFUQV9GRUlMRFNfTUFQUElORzoge1xyXG4gICAgICAgICAgICBDQU1QQUlHTl9NRVRBREFUQV9HUk9VUF9OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBVU0VSX1BSRUZFUkVOQ0VfRlJFRVpFX0NPVU5UOiBudW1iZXI7XHJcbiAgICB9O1xyXG4gICAgZGlzcGxheUZpZWxkTGlzdDogQXJyYXk8TVBNRmllbGQ+O1xyXG4gICAgaXNDYW1wYWlnbkRhc2hib2FyZDogYm9vbGVhbjtcclxuICAgIGNhdGVnb3J5TGV2ZWw6IE1QTV9MRVZFTFM7XHJcbiAgICBsb2dnZWRJblVzZXJJZDogYW55O1xyXG4gICAgZmluYWxDb21wbGV0ZWQ6IGFueTtcclxuICAgIGFmZmVjdGVkVGFzazogYW55W107XHJcbiAgICBjb25zdHJ1Y3RvcihkaWFsb2c6IE1hdERpYWxvZywgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLCBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLCBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSwgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHByb2plY3RCdWxrQ291bnRTZXJ2aWNlOiBQcm9qZWN0QnVsa0NvdW50U2VydmljZSwgcm91dGVyOiBSb3V0ZXIsIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSwgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSk7XHJcbiAgICBpc1Byb2plY3RPd25lcjogYW55O1xyXG4gICAgY29weVRvb2xUaXA6IHN0cmluZztcclxuICAgIHN0YXR1czogc3RyaW5nW107XHJcbiAgICBlbmFibGVCdWxrRWRpdDogYm9vbGVhbjtcclxuICAgIGNvcGllZFRleHQoZWxlbWVudDogYW55KTogdm9pZDtcclxuICAgIG9wZW5Qcm9qZWN0RGV0YWlscyhwcm9qZWN0OiBhbnkpOiB2b2lkO1xyXG4gICAgb3BlbkNvbW1lbnRzKCk6IHZvaWQ7XHJcbiAgICBvcGVuQ29weVByb2plY3QocHJvamVjdDogYW55LCBpc0NvcHlUZW1wbGF0ZTogYm9vbGVhbik6IHZvaWQ7XHJcbiAgICBjb252ZXJUb0xvY2FsRGF0ZShkYXRhOiBhbnksIHByb3BlcnR5SWQ6IGFueSk6IGFueTtcclxuICAgIGdldFByb3BlcnR5KHByb2plY3REYXRhOiBhbnksIHByb3BlcnR5SWQ6IGFueSk6IGFueTtcclxuICAgIGdldFByaW9yaXR5KHByb2plY3REYXRhOiBhbnksIHByb3BlcnR5SWQ6IGFueSk6IHtcclxuICAgICAgICBjb2xvcjogYW55O1xyXG4gICAgICAgIGljb246IHN0cmluZztcclxuICAgICAgICB0b29sdGlwOiBhbnk7XHJcbiAgICB9O1xyXG4gICAgZ2V0TWV0YWRhdGFUeXBlVmFsdWUobWV0YWRhdGE6IGFueSk6IHN0cmluZztcclxuICAgIGlzU2VsZWN0ZWQocHJvamVjdERldGFpbHM6IGFueSwgZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICBkZWxldGVQcm9qZWN0KHByb2plY3Q6IGFueSk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==