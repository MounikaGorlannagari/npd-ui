import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './share-assets.component';
import * as ɵngcc2 from '@angular/common';
import * as ɵngcc3 from '../material.module';
import * as ɵngcc4 from '../shared/components/custom-email-field/custom-email-field.module';
export declare class ShareAssetsModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<ShareAssetsModule, [typeof ɵngcc1.ShareAssetsComponent], [typeof ɵngcc2.CommonModule, typeof ɵngcc3.MaterialModule, typeof ɵngcc4.CustomEmailFieldModule], [typeof ɵngcc1.ShareAssetsComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<ShareAssetsModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLm1vZHVsZS5kLnRzIiwic291cmNlcyI6WyJzaGFyZS1hc3NldHMubW9kdWxlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVjbGFyZSBjbGFzcyBTaGFyZUFzc2V0c01vZHVsZSB7XHJcbn1cclxuIl19