import { OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { ShareAssetService } from './share-asset.service';
import { NotificationService } from '../notification/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { LoaderService } from '../loader/loader.service';
import { ShareAssetEmailService } from './services/share-asset-email.service';
import { ProjectService } from '../project/shared/services/project.service';
import * as ɵngcc0 from '@angular/core';
export declare class ShareAssetsComponent implements OnInit {
    fb: FormBuilder;
    dialog: MatDialog;
    sharingService: SharingService;
    shareAssetService: ShareAssetService;
    notificationService: NotificationService;
    loaderService: LoaderService;
    shareAssetEmailService: ShareAssetEmailService;
    projectService: ProjectService;
    assets: Array<any>;
    closeModalHandler: EventEmitter<any>;
    sharingOptions: {
        name: string;
        value: string;
        isDefault: boolean;
        emailSubject: string;
        emailMessage: string;
    }[];
    shareAssetForm: FormGroup;
    required: boolean;
    constructor(fb: FormBuilder, dialog: MatDialog, sharingService: SharingService, shareAssetService: ShareAssetService, notificationService: NotificationService, loaderService: LoaderService, shareAssetEmailService: ShareAssetEmailService, projectService: ProjectService);
    onFieldChange(event: any): void;
    closeDialog(): void;
    getProperty(projectData: any, propertyId: any): string;
    shareAsset(): void;
    intializeForm(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ShareAssetsComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ShareAssetsComponent, "mpm-share-assets", never, { "assets": "assets"; }, { "closeModalHandler": "closeModalHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJzaGFyZS1hc3NldHMuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyZUFzc2V0U2VydmljZSB9IGZyb20gJy4vc2hhcmUtYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmVBc3NldEVtYWlsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvc2hhcmUtYXNzZXQtZW1haWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgU2hhcmVBc3NldHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgZmI6IEZvcm1CdWlsZGVyO1xyXG4gICAgZGlhbG9nOiBNYXREaWFsb2c7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBzaGFyZUFzc2V0U2VydmljZTogU2hhcmVBc3NldFNlcnZpY2U7XHJcbiAgICBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZTtcclxuICAgIHNoYXJlQXNzZXRFbWFpbFNlcnZpY2U6IFNoYXJlQXNzZXRFbWFpbFNlcnZpY2U7XHJcbiAgICBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2U7XHJcbiAgICBhc3NldHM6IEFycmF5PGFueT47XHJcbiAgICBjbG9zZU1vZGFsSGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBzaGFyaW5nT3B0aW9uczoge1xyXG4gICAgICAgIG5hbWU6IHN0cmluZztcclxuICAgICAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgICAgIGlzRGVmYXVsdDogYm9vbGVhbjtcclxuICAgICAgICBlbWFpbFN1YmplY3Q6IHN0cmluZztcclxuICAgICAgICBlbWFpbE1lc3NhZ2U6IHN0cmluZztcclxuICAgIH1bXTtcclxuICAgIHNoYXJlQXNzZXRGb3JtOiBGb3JtR3JvdXA7XHJcbiAgICByZXF1aXJlZDogYm9vbGVhbjtcclxuICAgIGNvbnN0cnVjdG9yKGZiOiBGb3JtQnVpbGRlciwgZGlhbG9nOiBNYXREaWFsb2csIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSwgc2hhcmVBc3NldFNlcnZpY2U6IFNoYXJlQXNzZXRTZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBzaGFyZUFzc2V0RW1haWxTZXJ2aWNlOiBTaGFyZUFzc2V0RW1haWxTZXJ2aWNlLCBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UpO1xyXG4gICAgb25GaWVsZENoYW5nZShldmVudDogYW55KTogdm9pZDtcclxuICAgIGNsb3NlRGlhbG9nKCk6IHZvaWQ7XHJcbiAgICBnZXRQcm9wZXJ0eShwcm9qZWN0RGF0YTogYW55LCBwcm9wZXJ0eUlkOiBhbnkpOiBzdHJpbmc7XHJcbiAgICBzaGFyZUFzc2V0KCk6IHZvaWQ7XHJcbiAgICBpbnRpYWxpemVGb3JtKCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==