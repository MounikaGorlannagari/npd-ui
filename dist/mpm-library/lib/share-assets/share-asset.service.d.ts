import { SharingService } from '../mpm-utils/services/sharing.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { OTMMService } from '../mpm-utils/services/otmm.service';
import * as ɵngcc0 from '@angular/core';
export declare class ShareAssetService {
    http: HttpClient;
    sharingService: SharingService;
    otmmService: OTMMService;
    constructor(http: HttpClient, sharingService: SharingService, otmmService: OTMMService);
    formDownloadName(): string;
    shareAsset(assets: Array<any>, sharingDetails: any): Observable<unknown>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ShareAssetService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXQuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJzaGFyZS1hc3NldC5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIFNoYXJlQXNzZXRTZXJ2aWNlIHtcclxuICAgIGh0dHA6IEh0dHBDbGllbnQ7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3RvcihodHRwOiBIdHRwQ2xpZW50LCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSk7XHJcbiAgICBmb3JtRG93bmxvYWROYW1lKCk6IHN0cmluZztcclxuICAgIHNoYXJlQXNzZXQoYXNzZXRzOiBBcnJheTxhbnk+LCBzaGFyaW5nRGV0YWlsczogYW55KTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxufVxyXG4iXX0=