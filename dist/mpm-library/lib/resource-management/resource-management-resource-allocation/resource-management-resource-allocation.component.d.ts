import { OnInit, ElementRef, QueryList, Renderer2, EventEmitter, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import * as Moment from 'moment';
import { MediaMatcher } from '@angular/cdk/layout';
import * as ɵngcc0 from '@angular/core';
export declare class ResourceManagementResourceAllocationComponent implements OnInit {
    renderer: Renderer2;
    media: MediaMatcher;
    changeDetectorRef: ChangeDetectorRef;
    selectedMonth: any;
    data: any;
    users: any;
    selectedItems: any;
    allocation: any;
    selectedYear: any;
    removed: EventEmitter<any[]>;
    scrolled: boolean;
    dates: any[];
    viewTypeCellSize: number;
    momentPtr: import("moment-range").MomentRange & typeof Moment;
    currentViewStartDate: moment.Moment;
    currentViewEndDate: moment.Moment;
    calendarView: ElementRef;
    calendarHeaders: QueryList<ElementRef>;
    panelContainers: QueryList<ElementRef>;
    calendarCells: QueryList<ElementRef>;
    currentDate: any;
    sidebarStyle: {};
    datas: any;
    Object: ObjectConstructor;
    Math: Math;
    screenWidth: any;
    height: any;
    constructor(renderer: Renderer2, media: MediaMatcher, changeDetectorRef: ChangeDetectorRef);
    onResize(event: any): void;
    getHeightForScreen(): void;
    removeFilter(filter: any): void;
    /** * Scroll for Fixing the dates and header */
    scrolling: (el: any) => void;
    /** CALENDAR METHODS **/
    buildCalendar(startDate: any, endDate: any): void;
    getNoOfDays(selectedMonth: any): any;
    getDateDifference(startDate: any, endDate: any): number;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ResourceManagementResourceAllocationComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ResourceManagementResourceAllocationComponent, "mpm-resource-management-resource-allocation", never, { "selectedMonth": "selectedMonth"; "data": "data"; "users": "users"; "selectedItems": "selectedItems"; "allocation": "allocation"; "selectedYear": "selectedYear"; }, { "removed": "removed"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJyZXNvdXJjZS1tYW5hZ2VtZW50LXJlc291cmNlLWFsbG9jYXRpb24uY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRWxlbWVudFJlZiwgUXVlcnlMaXN0LCBSZW5kZXJlcjIsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcywgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0ICogYXMgTW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IE1lZGlhTWF0Y2hlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9sYXlvdXQnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBSZXNvdXJjZU1hbmFnZW1lbnRSZXNvdXJjZUFsbG9jYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgcmVuZGVyZXI6IFJlbmRlcmVyMjtcclxuICAgIG1lZGlhOiBNZWRpYU1hdGNoZXI7XHJcbiAgICBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWY7XHJcbiAgICBzZWxlY3RlZE1vbnRoOiBhbnk7XHJcbiAgICBkYXRhOiBhbnk7XHJcbiAgICB1c2VyczogYW55O1xyXG4gICAgc2VsZWN0ZWRJdGVtczogYW55O1xyXG4gICAgYWxsb2NhdGlvbjogYW55O1xyXG4gICAgc2VsZWN0ZWRZZWFyOiBhbnk7XHJcbiAgICByZW1vdmVkOiBFdmVudEVtaXR0ZXI8YW55W10+O1xyXG4gICAgc2Nyb2xsZWQ6IGJvb2xlYW47XHJcbiAgICBkYXRlczogYW55W107XHJcbiAgICB2aWV3VHlwZUNlbGxTaXplOiBudW1iZXI7XHJcbiAgICBtb21lbnRQdHI6IGltcG9ydChcIm1vbWVudC1yYW5nZVwiKS5Nb21lbnRSYW5nZSAmIHR5cGVvZiBNb21lbnQ7XHJcbiAgICBjdXJyZW50Vmlld1N0YXJ0RGF0ZTogbW9tZW50Lk1vbWVudDtcclxuICAgIGN1cnJlbnRWaWV3RW5kRGF0ZTogbW9tZW50Lk1vbWVudDtcclxuICAgIGNhbGVuZGFyVmlldzogRWxlbWVudFJlZjtcclxuICAgIGNhbGVuZGFySGVhZGVyczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG4gICAgcGFuZWxDb250YWluZXJzOiBRdWVyeUxpc3Q8RWxlbWVudFJlZj47XHJcbiAgICBjYWxlbmRhckNlbGxzOiBRdWVyeUxpc3Q8RWxlbWVudFJlZj47XHJcbiAgICBjdXJyZW50RGF0ZTogYW55O1xyXG4gICAgc2lkZWJhclN0eWxlOiB7fTtcclxuICAgIGRhdGFzOiBhbnk7XHJcbiAgICBPYmplY3Q6IE9iamVjdENvbnN0cnVjdG9yO1xyXG4gICAgTWF0aDogTWF0aDtcclxuICAgIHNjcmVlbldpZHRoOiBhbnk7XHJcbiAgICBoZWlnaHQ6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKHJlbmRlcmVyOiBSZW5kZXJlcjIsIG1lZGlhOiBNZWRpYU1hdGNoZXIsIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZik7XHJcbiAgICBvblJlc2l6ZShldmVudDogYW55KTogdm9pZDtcclxuICAgIGdldEhlaWdodEZvclNjcmVlbigpOiB2b2lkO1xyXG4gICAgcmVtb3ZlRmlsdGVyKGZpbHRlcjogYW55KTogdm9pZDtcclxuICAgIC8qKiAqIFNjcm9sbCBmb3IgRml4aW5nIHRoZSBkYXRlcyBhbmQgaGVhZGVyICovXHJcbiAgICBzY3JvbGxpbmc6IChlbDogYW55KSA9PiB2b2lkO1xyXG4gICAgLyoqIENBTEVOREFSIE1FVEhPRFMgKiovXHJcbiAgICBidWlsZENhbGVuZGFyKHN0YXJ0RGF0ZTogYW55LCBlbmREYXRlOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0Tm9PZkRheXMoc2VsZWN0ZWRNb250aDogYW55KTogYW55O1xyXG4gICAgZ2V0RGF0ZURpZmZlcmVuY2Uoc3RhcnREYXRlOiBhbnksIGVuZERhdGU6IGFueSk6IG51bWJlcjtcclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=