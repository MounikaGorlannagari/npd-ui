import { SortDirection } from '@angular/material/sort';
export interface ResourceManagementComponentParams {
    groupByFilter?: string;
    deliverableTaskFilter?: string;
    listDependentFilter?: string;
    emailLinkId?: string;
    sortBy?: string;
    sortOrder?: SortDirection;
    pageNumber?: number;
    pageSize?: number;
    searchName?: string;
    savedSearchName?: string;
    advSearchData?: string;
    facetData?: string;
}
export interface TaskSearchConditions {
    DELIVERABLE_CONDTION: Array<any>;
    TASK_CONDITION: Array<any>;
}
