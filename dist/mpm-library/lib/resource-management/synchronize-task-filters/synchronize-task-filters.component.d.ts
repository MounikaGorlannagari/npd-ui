import { OnInit, EventEmitter, SimpleChanges } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import * as Moment from 'moment';
import { Observable } from 'rxjs';
import { TaskSearchConditions } from '../object/ResourceManagementComponentParams';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { LoaderService } from '../../loader/loader.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { SearchDataService } from '../../search/services/search-data.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { DeliverableService } from '../../project/tasks/deliverable/deliverable.service';
import * as ɵngcc0 from '@angular/core';
export declare class SynchronizeTaskFiltersComponent implements OnInit {
    sharingService: SharingService;
    appService: AppService;
    entityAppDefService: EntityAppDefService;
    utilService: UtilService;
    fieldConfigService: FieldConfigService;
    filterHelperService: FilterHelperService;
    loaderService: LoaderService;
    viewConfigService: ViewConfigService;
    indexerService: IndexerService;
    deliverableService: DeliverableService;
    otmmService: OTMMService;
    searchDataService: SearchDataService;
    groupBy: any;
    selectedMonth: any;
    currentViewStartDates: any;
    currentViewEndDates: any;
    levels: any;
    viewConfigs: any;
    removedFilter: any;
    datess: any;
    datee: any;
    deliverableData: EventEmitter<any>;
    selectedUser: EventEmitter<any>;
    selectedFilters: EventEmitter<any>;
    allTeams: any;
    allRoles: any;
    teams: any[];
    roles: any[];
    users: any[];
    momentPtr: import("moment-range").MomentRange & typeof Moment;
    resource: any;
    selectedListFilter: any;
    selectedListDependentFilter: any;
    viewTypeCellSize: number;
    allUser: any;
    userId: any[];
    selectedTeam: any;
    selectedRole: any;
    selectedItems: any;
    allUsers: any[];
    userIds: any[];
    OwnerArray: any;
    searchConditions: any[];
    emailSearchCondition: any[];
    convertedDate: any[];
    storeSearchconditionObject: TaskSearchConditions;
    private subscriptions;
    inputName: any;
    teamInitial: string;
    roleInitial: string;
    userInitial: string;
    previousUserId: any[];
    constructor(sharingService: SharingService, appService: AppService, entityAppDefService: EntityAppDefService, utilService: UtilService, fieldConfigService: FieldConfigService, filterHelperService: FilterHelperService, loaderService: LoaderService, viewConfigService: ViewConfigService, indexerService: IndexerService, deliverableService: DeliverableService, otmmService: OTMMService, searchDataService: SearchDataService);
    GetRoleDNByRoleId(roleId: any): import("../../../public-api").Roles;
    getRolesAndUser(team: any): void;
    getUsersForTeam(team: any): Observable<any>;
    getTeamsAndUser(role: any): void;
    getUsersByRole(role: any, selectedTeam: any): Observable<any>;
    allocationTask(user: any): string;
    resourceTaskAllocation(user: any, currentViewStartDates: any, currentViewEndDates: any): any[];
    getProperty(deliverableData: any, mapperName: any): any;
    /**
       * @since MPMV3-946
       * @param deliverableType
       */
    getDeliverableTypeIcon(deliverableType: any): {
        NAME: string;
        DESCRIPTION: string;
        ICON: string;
    };
    reload(user: any): void;
    getDeliverableList(UserId: any): void;
    getDeliverable(user: any, removingFilterValue: any, isParent?: any): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<SynchronizeTaskFiltersComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<SynchronizeTaskFiltersComponent, "mpm-synchronize-task-filters", never, { "removedFilter": "removedFilter"; "groupBy": "groupBy"; "selectedMonth": "selectedMonth"; "currentViewStartDates": "currentViewStartDates"; "currentViewEndDates": "currentViewEndDates"; "levels": "levels"; "viewConfigs": "viewConfigs"; "datess": "datess"; "datee": "datee"; }, { "deliverableData": "deliverableData"; "selectedUser": "selectedUser"; "selectedFilters": "selectedFilters"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJzeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBNb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUYXNrU2VhcmNoQ29uZGl0aW9ucyB9IGZyb20gJy4uL29iamVjdC9SZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRQYXJhbXMnO1xyXG5pbXBvcnQgeyBGaWx0ZXJIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpbHRlci1oZWxwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kZXhlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9pbmRleGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hEYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgU3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlO1xyXG4gICAgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlO1xyXG4gICAgZmlsdGVySGVscGVyU2VydmljZTogRmlsdGVySGVscGVyU2VydmljZTtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2U7XHJcbiAgICBpbmRleGVyU2VydmljZTogSW5kZXhlclNlcnZpY2U7XHJcbiAgICBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZTtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIHNlYXJjaERhdGFTZXJ2aWNlOiBTZWFyY2hEYXRhU2VydmljZTtcclxuICAgIGdyb3VwQnk6IGFueTtcclxuICAgIHNlbGVjdGVkTW9udGg6IGFueTtcclxuICAgIGN1cnJlbnRWaWV3U3RhcnREYXRlczogYW55O1xyXG4gICAgY3VycmVudFZpZXdFbmREYXRlczogYW55O1xyXG4gICAgbGV2ZWxzOiBhbnk7XHJcbiAgICB2aWV3Q29uZmlnczogYW55O1xyXG4gICAgcmVtb3ZlZEZpbHRlcjogYW55O1xyXG4gICAgZGF0ZXNzOiBhbnk7XHJcbiAgICBkYXRlZTogYW55O1xyXG4gICAgZGVsaXZlcmFibGVEYXRhOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHNlbGVjdGVkVXNlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBzZWxlY3RlZEZpbHRlcnM6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgYWxsVGVhbXM6IGFueTtcclxuICAgIGFsbFJvbGVzOiBhbnk7XHJcbiAgICB0ZWFtczogYW55W107XHJcbiAgICByb2xlczogYW55W107XHJcbiAgICB1c2VyczogYW55W107XHJcbiAgICBtb21lbnRQdHI6IGltcG9ydChcIm1vbWVudC1yYW5nZVwiKS5Nb21lbnRSYW5nZSAmIHR5cGVvZiBNb21lbnQ7XHJcbiAgICByZXNvdXJjZTogYW55O1xyXG4gICAgc2VsZWN0ZWRMaXN0RmlsdGVyOiBhbnk7XHJcbiAgICBzZWxlY3RlZExpc3REZXBlbmRlbnRGaWx0ZXI6IGFueTtcclxuICAgIHZpZXdUeXBlQ2VsbFNpemU6IG51bWJlcjtcclxuICAgIGFsbFVzZXI6IGFueTtcclxuICAgIHVzZXJJZDogYW55W107XHJcbiAgICBzZWxlY3RlZFRlYW06IGFueTtcclxuICAgIHNlbGVjdGVkUm9sZTogYW55O1xyXG4gICAgc2VsZWN0ZWRJdGVtczogYW55O1xyXG4gICAgYWxsVXNlcnM6IGFueVtdO1xyXG4gICAgdXNlcklkczogYW55W107XHJcbiAgICBPd25lckFycmF5OiBhbnk7XHJcbiAgICBzZWFyY2hDb25kaXRpb25zOiBhbnlbXTtcclxuICAgIGVtYWlsU2VhcmNoQ29uZGl0aW9uOiBhbnlbXTtcclxuICAgIGNvbnZlcnRlZERhdGU6IGFueVtdO1xyXG4gICAgc3RvcmVTZWFyY2hjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zO1xyXG4gICAgcHJpdmF0ZSBzdWJzY3JpcHRpb25zO1xyXG4gICAgaW5wdXROYW1lOiBhbnk7XHJcbiAgICB0ZWFtSW5pdGlhbDogc3RyaW5nO1xyXG4gICAgcm9sZUluaXRpYWw6IHN0cmluZztcclxuICAgIHVzZXJJbml0aWFsOiBzdHJpbmc7XHJcbiAgICBwcmV2aW91c1VzZXJJZDogYW55W107XHJcbiAgICBjb25zdHJ1Y3RvcihzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSwgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsIGZpbHRlckhlbHBlclNlcnZpY2U6IEZpbHRlckhlbHBlclNlcnZpY2UsIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZSwgaW5kZXhlclNlcnZpY2U6IEluZGV4ZXJTZXJ2aWNlLCBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZSwgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLCBzZWFyY2hEYXRhU2VydmljZTogU2VhcmNoRGF0YVNlcnZpY2UpO1xyXG4gICAgR2V0Um9sZUROQnlSb2xlSWQocm9sZUlkOiBhbnkpOiBpbXBvcnQoXCIuLi8uLi8uLi9wdWJsaWMtYXBpXCIpLlJvbGVzO1xyXG4gICAgZ2V0Um9sZXNBbmRVc2VyKHRlYW06IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRVc2Vyc0ZvclRlYW0odGVhbTogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0VGVhbXNBbmRVc2VyKHJvbGU6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRVc2Vyc0J5Um9sZShyb2xlOiBhbnksIHNlbGVjdGVkVGVhbTogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgYWxsb2NhdGlvblRhc2sodXNlcjogYW55KTogc3RyaW5nO1xyXG4gICAgcmVzb3VyY2VUYXNrQWxsb2NhdGlvbih1c2VyOiBhbnksIGN1cnJlbnRWaWV3U3RhcnREYXRlczogYW55LCBjdXJyZW50Vmlld0VuZERhdGVzOiBhbnkpOiBhbnlbXTtcclxuICAgIGdldFByb3BlcnR5KGRlbGl2ZXJhYmxlRGF0YTogYW55LCBtYXBwZXJOYW1lOiBhbnkpOiBhbnk7XHJcbiAgICAvKipcclxuICAgICAgICogQHNpbmNlIE1QTVYzLTk0NlxyXG4gICAgICAgKiBAcGFyYW0gZGVsaXZlcmFibGVUeXBlXHJcbiAgICAgICAqL1xyXG4gICAgZ2V0RGVsaXZlcmFibGVUeXBlSWNvbihkZWxpdmVyYWJsZVR5cGU6IGFueSk6IHtcclxuICAgICAgICBOQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgREVTQ1JJUFRJT046IHN0cmluZztcclxuICAgICAgICBJQ09OOiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgcmVsb2FkKHVzZXI6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXREZWxpdmVyYWJsZUxpc3QoVXNlcklkOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0RGVsaXZlcmFibGUodXNlcjogYW55LCByZW1vdmluZ0ZpbHRlclZhbHVlOiBhbnksIGlzUGFyZW50PzogYW55KTogdm9pZDtcclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQ7XHJcbn1cclxuIl19