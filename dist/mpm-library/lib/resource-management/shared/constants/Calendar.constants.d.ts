export declare const CalendarConstants: {
    months: string[];
    evenMonth: string[];
    oddMonth: string[];
    leapMonth: string[];
};
