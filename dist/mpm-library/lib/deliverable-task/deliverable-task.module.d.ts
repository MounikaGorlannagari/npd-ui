import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './deliverable-task-list/deliverable-task-list.component';
import * as ɵngcc2 from '@angular/common';
import * as ɵngcc3 from '../material.module';
import * as ɵngcc4 from '../project/tasks/deliverable/deliverable.module';
import * as ɵngcc5 from '../mpm-library.module';
export declare class DeliverableTaskModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<DeliverableTaskModule, [typeof ɵngcc1.DeliverableTaskListComponent], [typeof ɵngcc2.CommonModule, typeof ɵngcc3.MaterialModule, typeof ɵngcc4.DeliverableModule, typeof ɵngcc5.MpmLibraryModule], [typeof ɵngcc1.DeliverableTaskListComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<DeliverableTaskModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdGFzay5tb2R1bGUuZC50cyIsInNvdXJjZXMiOlsiZGVsaXZlcmFibGUtdGFzay5tb2R1bGUuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVjbGFyZSBjbGFzcyBEZWxpdmVyYWJsZVRhc2tNb2R1bGUge1xyXG59XHJcbiJdfQ==