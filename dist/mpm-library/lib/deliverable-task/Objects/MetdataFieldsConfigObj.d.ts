export interface MetdataFieldsConfigObj {
    sortingFields?: Array<any>;
    metaDataFields?: Array<any>;
    searchingFields?: Array<any>;
    customMetadataFields?: Array<any>;
    defaultMetadataFields?: Array<any>;
}
