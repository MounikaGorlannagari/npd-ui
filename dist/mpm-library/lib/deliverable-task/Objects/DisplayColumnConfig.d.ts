export interface DisplayColumnConfig {
    displayableMetadataFields: Array<any>;
    displayColumns: Array<string>;
}
