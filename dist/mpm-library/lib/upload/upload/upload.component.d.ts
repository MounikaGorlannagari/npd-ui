import { OnInit } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../notification/notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { AssetUploadService } from '../services/asset.upload.service';
import { LoaderService } from '../../loader/loader.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import * as ɵngcc0 from '@angular/core';
export declare class UploadComponent implements OnInit {
    dialogRef: MatDialogRef<UploadComponent>;
    data: any;
    assetUploadService: AssetUploadService;
    loaderService: LoaderService;
    notificationService: NotificationService;
    otmmService: OTMMService;
    sharingService: SharingService;
    utilService: UtilService;
    constructor(dialogRef: MatDialogRef<UploadComponent>, data: any, assetUploadService: AssetUploadService, loaderService: LoaderService, notificationService: NotificationService, otmmService: OTMMService, sharingService: SharingService, utilService: UtilService);
    uploadedFiles: any[];
    uploadProcessing: boolean;
    assetConfig: any;
    selected: string;
    referenceAsset: string[];
    referenceTypeObj: any;
    closeDialog(): void;
    udploadFileEvent(files: any): void;
    validateFile(): boolean;
    uploadFiles(): void;
    changeReferenceType(option: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<UploadComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<UploadComponent, "mpm-upload", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJ1cGxvYWQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IEFzc2V0VXBsb2FkU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Fzc2V0LnVwbG9hZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIFVwbG9hZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxVcGxvYWRDb21wb25lbnQ+O1xyXG4gICAgZGF0YTogYW55O1xyXG4gICAgYXNzZXRVcGxvYWRTZXJ2aWNlOiBBc3NldFVwbG9hZFNlcnZpY2U7XHJcbiAgICBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlO1xyXG4gICAgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIGNvbnN0cnVjdG9yKGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPFVwbG9hZENvbXBvbmVudD4sIGRhdGE6IGFueSwgYXNzZXRVcGxvYWRTZXJ2aWNlOiBBc3NldFVwbG9hZFNlcnZpY2UsIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLCB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UpO1xyXG4gICAgdXBsb2FkZWRGaWxlczogYW55W107XHJcbiAgICB1cGxvYWRQcm9jZXNzaW5nOiBib29sZWFuO1xyXG4gICAgYXNzZXRDb25maWc6IGFueTtcclxuICAgIHNlbGVjdGVkOiBzdHJpbmc7XHJcbiAgICByZWZlcmVuY2VBc3NldDogc3RyaW5nW107XHJcbiAgICByZWZlcmVuY2VUeXBlT2JqOiBhbnk7XHJcbiAgICBjbG9zZURpYWxvZygpOiB2b2lkO1xyXG4gICAgdWRwbG9hZEZpbGVFdmVudChmaWxlczogYW55KTogdm9pZDtcclxuICAgIHZhbGlkYXRlRmlsZSgpOiBib29sZWFuO1xyXG4gICAgdXBsb2FkRmlsZXMoKTogdm9pZDtcclxuICAgIGNoYW5nZVJlZmVyZW5jZVR5cGUob3B0aW9uOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=