import * as ɵngcc0 from '@angular/core';
export declare class AssetFileConfigService {
    constructor();
    findIconByName(fileName: any): "image" | "insert_drive_file" | "description" | "movie_creation" | "picture_as_pdf" | "gif" | "archive";
    getfileFormatObject(filename: any): any;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetFileConfigService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQuZmlsZS5jb25maWcuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJhc3NldC5maWxlLmNvbmZpZy5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlY2xhcmUgY2xhc3MgQXNzZXRGaWxlQ29uZmlnU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgZmluZEljb25CeU5hbWUoZmlsZU5hbWU6IGFueSk6IFwiaW1hZ2VcIiB8IFwiaW5zZXJ0X2RyaXZlX2ZpbGVcIiB8IFwiZGVzY3JpcHRpb25cIiB8IFwibW92aWVfY3JlYXRpb25cIiB8IFwicGljdHVyZV9hc19wZGZcIiB8IFwiZ2lmXCIgfCBcImFyY2hpdmVcIjtcclxuICAgIGdldGZpbGVGb3JtYXRPYmplY3QoZmlsZW5hbWU6IGFueSk6IGFueTtcclxufVxyXG4iXX0=