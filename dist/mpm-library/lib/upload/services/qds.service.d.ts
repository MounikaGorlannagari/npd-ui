import { HttpClient } from '@angular/common/http';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import * as ɵngcc0 from '@angular/core';
export declare class QdsService {
    http: HttpClient;
    entityAppDefService: EntityAppDefService;
    notificationService: NotificationService;
    _qdsConnect: any;
    constructor(http: HttpClient, entityAppDefService: EntityAppDefService, notificationService: NotificationService);
    getQDSConnect(): any;
    setQDSConnect(qdsConnect: any): void;
    getQDSSession(userSessionId: any): Observable<any>;
    connectToQDS(qdsResource: any, isUpload?: any): Observable<any>;
    getJobs(includeCompleted: any): Observable<any>;
    createQDSImportJob(jobId: any, filePaths: any): void;
    toggleQDSImportJob(jobId: any, toggleToState: any): Observable<any>;
    toggleQDSExportJob(jobId: any, toggleToState: any): Observable<any>;
    retryFailedJob(job: any): Observable<any>;
    download(jobId: any, files: any): Observable<any>;
    getDownloadFolder(): Observable<any>;
    chooseDownloadFolder(): Observable<any>;
    getFileTransferProgress(size: number, transferred: number): number;
    inlineDownload: (downloadParams: any) => Promise<unknown>;
    postInlineDownload(resolve: any, reject: any, downloadJob: any): void;
    downloadError(errorHandler: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<QdsService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsicWRzLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgUWRzU2VydmljZSB7XHJcbiAgICBodHRwOiBIdHRwQ2xpZW50O1xyXG4gICAgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBfcWRzQ29ubmVjdDogYW55O1xyXG4gICAgY29uc3RydWN0b3IoaHR0cDogSHR0cENsaWVudCwgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSwgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSk7XHJcbiAgICBnZXRRRFNDb25uZWN0KCk6IGFueTtcclxuICAgIHNldFFEU0Nvbm5lY3QocWRzQ29ubmVjdDogYW55KTogdm9pZDtcclxuICAgIGdldFFEU1Nlc3Npb24odXNlclNlc3Npb25JZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgY29ubmVjdFRvUURTKHFkc1Jlc291cmNlOiBhbnksIGlzVXBsb2FkPzogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0Sm9icyhpbmNsdWRlQ29tcGxldGVkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBjcmVhdGVRRFNJbXBvcnRKb2Ioam9iSWQ6IGFueSwgZmlsZVBhdGhzOiBhbnkpOiB2b2lkO1xyXG4gICAgdG9nZ2xlUURTSW1wb3J0Sm9iKGpvYklkOiBhbnksIHRvZ2dsZVRvU3RhdGU6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHRvZ2dsZVFEU0V4cG9ydEpvYihqb2JJZDogYW55LCB0b2dnbGVUb1N0YXRlOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICByZXRyeUZhaWxlZEpvYihqb2I6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGRvd25sb2FkKGpvYklkOiBhbnksIGZpbGVzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXREb3dubG9hZEZvbGRlcigpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBjaG9vc2VEb3dubG9hZEZvbGRlcigpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRGaWxlVHJhbnNmZXJQcm9ncmVzcyhzaXplOiBudW1iZXIsIHRyYW5zZmVycmVkOiBudW1iZXIpOiBudW1iZXI7XHJcbiAgICBpbmxpbmVEb3dubG9hZDogKGRvd25sb2FkUGFyYW1zOiBhbnkpID0+IFByb21pc2U8dW5rbm93bj47XHJcbiAgICBwb3N0SW5saW5lRG93bmxvYWQocmVzb2x2ZTogYW55LCByZWplY3Q6IGFueSwgZG93bmxvYWRKb2I6IGFueSk6IHZvaWQ7XHJcbiAgICBkb3dubG9hZEVycm9yKGVycm9ySGFuZGxlcjogYW55KTogdm9pZDtcclxufVxyXG4iXX0=