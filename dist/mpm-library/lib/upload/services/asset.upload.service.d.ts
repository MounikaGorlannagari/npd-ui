import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { QdsService } from './qds.service';
import * as ɵngcc0 from '@angular/core';
export declare class AssetUploadService {
    appService: AppService;
    otmmService: OTMMService;
    entityAppDefService: EntityAppDefService;
    utilService: UtilService;
    categoryService: CategoryService;
    fieldConfigService: FieldConfigService;
    sharingService: SharingService;
    qdsService: QdsService;
    constructor(appService: AppService, otmmService: OTMMService, entityAppDefService: EntityAppDefService, utilService: UtilService, categoryService: CategoryService, fieldConfigService: FieldConfigService, sharingService: SharingService, qdsService: QdsService);
    IMPORT_DELIVERABLE_ASSET_NS: string;
    IMPORT_DELIVERABLE_ASSET_WS: string;
    assetConstants: any;
    constructMetadataFieldValuesFromEventHandler(metadataValues: any): any[];
    excludeFileFormat(name: any): any;
    uploadCallBack(deliverable: any, job: any, uploadType: any): Observable<any>;
    checkInAsset(filesToCheckIn: any, JobId: any, folderId: any): Observable<any>;
    checkOutAsset(filesToCheckOut: any): Observable<any>;
    uploadFilesViaHTTP(filesToUpload: any, eventData: any, isRevision: any): Observable<any>;
    uploadVersionViaHTTP(filesToUpload: any, eventData: any, isRevision: any): Observable<any>;
    uploadFilesViaQDS(filesToUpload: any, eventData: any, isRevision: any): Observable<any>;
    uploadVersionsViaQDS(files: any, eventData: any, isRevision: any): Observable<any>;
    startUpload(files: any, deliverable: any, isRevision: any, folderId?: any, project?: any): Observable<any>;
    formAssetMetadataModel(deliverableData: any, projectData?: any): {
        metadata_element_list: {
            metadata_element_list: any;
        }[];
        metadata_model_id: string;
        security_policy_list: {
            id: string;
        }[];
    };
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetUploadService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQudXBsb2FkLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsiYXNzZXQudXBsb2FkLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuL3Fkcy5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQXNzZXRVcGxvYWRTZXJ2aWNlIHtcclxuICAgIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlO1xyXG4gICAgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlO1xyXG4gICAgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2U7XHJcbiAgICBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3RvcihhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSwgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2UpO1xyXG4gICAgSU1QT1JUX0RFTElWRVJBQkxFX0FTU0VUX05TOiBzdHJpbmc7XHJcbiAgICBJTVBPUlRfREVMSVZFUkFCTEVfQVNTRVRfV1M6IHN0cmluZztcclxuICAgIGFzc2V0Q29uc3RhbnRzOiBhbnk7XHJcbiAgICBjb25zdHJ1Y3RNZXRhZGF0YUZpZWxkVmFsdWVzRnJvbUV2ZW50SGFuZGxlcihtZXRhZGF0YVZhbHVlczogYW55KTogYW55W107XHJcbiAgICBleGNsdWRlRmlsZUZvcm1hdChuYW1lOiBhbnkpOiBhbnk7XHJcbiAgICB1cGxvYWRDYWxsQmFjayhkZWxpdmVyYWJsZTogYW55LCBqb2I6IGFueSwgdXBsb2FkVHlwZTogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgY2hlY2tJbkFzc2V0KGZpbGVzVG9DaGVja0luOiBhbnksIEpvYklkOiBhbnksIGZvbGRlcklkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBjaGVja091dEFzc2V0KGZpbGVzVG9DaGVja091dDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgdXBsb2FkRmlsZXNWaWFIVFRQKGZpbGVzVG9VcGxvYWQ6IGFueSwgZXZlbnREYXRhOiBhbnksIGlzUmV2aXNpb246IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHVwbG9hZFZlcnNpb25WaWFIVFRQKGZpbGVzVG9VcGxvYWQ6IGFueSwgZXZlbnREYXRhOiBhbnksIGlzUmV2aXNpb246IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHVwbG9hZEZpbGVzVmlhUURTKGZpbGVzVG9VcGxvYWQ6IGFueSwgZXZlbnREYXRhOiBhbnksIGlzUmV2aXNpb246IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHVwbG9hZFZlcnNpb25zVmlhUURTKGZpbGVzOiBhbnksIGV2ZW50RGF0YTogYW55LCBpc1JldmlzaW9uOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBzdGFydFVwbG9hZChmaWxlczogYW55LCBkZWxpdmVyYWJsZTogYW55LCBpc1JldmlzaW9uOiBhbnksIGZvbGRlcklkPzogYW55LCBwcm9qZWN0PzogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZm9ybUFzc2V0TWV0YWRhdGFNb2RlbChkZWxpdmVyYWJsZURhdGE6IGFueSwgcHJvamVjdERhdGE/OiBhbnkpOiB7XHJcbiAgICAgICAgbWV0YWRhdGFfZWxlbWVudF9saXN0OiB7XHJcbiAgICAgICAgICAgIG1ldGFkYXRhX2VsZW1lbnRfbGlzdDogYW55O1xyXG4gICAgICAgIH1bXTtcclxuICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogc3RyaW5nO1xyXG4gICAgICAgIHNlY3VyaXR5X3BvbGljeV9saXN0OiB7XHJcbiAgICAgICAgICAgIGlkOiBzdHJpbmc7XHJcbiAgICAgICAgfVtdO1xyXG4gICAgfTtcclxufVxyXG4iXX0=