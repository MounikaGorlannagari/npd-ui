import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from '@angular/material/button';
import * as ɵngcc2 from '@angular/material/toolbar';
import * as ɵngcc3 from '@angular/material/icon';
import * as ɵngcc4 from '@angular/material/menu';
import * as ɵngcc5 from '@angular/material/radio';
import * as ɵngcc6 from '@angular/material/form-field';
import * as ɵngcc7 from '@angular/material/input';
import * as ɵngcc8 from '@angular/material/card';
import * as ɵngcc9 from '@angular/material/dialog';
import * as ɵngcc10 from '@angular/material/grid-list';
import * as ɵngcc11 from '@angular/material/snack-bar';
import * as ɵngcc12 from '@angular/material/progress-bar';
import * as ɵngcc13 from '@angular/material/expansion';
import * as ɵngcc14 from '@angular/material/table';
import * as ɵngcc15 from '@angular/forms';
import * as ɵngcc16 from '@angular/material/paginator';
import * as ɵngcc17 from '@angular/material/divider';
import * as ɵngcc18 from '@angular/material/list';
import * as ɵngcc19 from '@angular/material/progress-spinner';
import * as ɵngcc20 from '@angular/material/checkbox';
import * as ɵngcc21 from '@angular/material/chips';
import * as ɵngcc22 from '@angular/material/tooltip';
import * as ɵngcc23 from '@angular/material/sort';
import * as ɵngcc24 from '@angular/material/sidenav';
import * as ɵngcc25 from '@angular/material/slider';
import * as ɵngcc26 from '@angular/material/slide-toggle';
import * as ɵngcc27 from '@angular/material/button-toggle';
import * as ɵngcc28 from '@angular/material/core';
import * as ɵngcc29 from '@angular/material/datepicker';
import * as ɵngcc30 from '@angular/material/autocomplete';
import * as ɵngcc31 from '@angular/material/select';
import * as ɵngcc32 from '@angular/material/badge';
import * as ɵngcc33 from '@angular/cdk/drag-drop';
export declare class MaterialModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<MaterialModule, never, [typeof ɵngcc1.MatButtonModule, typeof ɵngcc2.MatToolbarModule, typeof ɵngcc3.MatIconModule, typeof ɵngcc4.MatMenuModule, typeof ɵngcc5.MatRadioModule, typeof ɵngcc6.MatFormFieldModule, typeof ɵngcc7.MatInputModule, typeof ɵngcc8.MatCardModule, typeof ɵngcc9.MatDialogModule, typeof ɵngcc10.MatGridListModule, typeof ɵngcc11.MatSnackBarModule, typeof ɵngcc12.MatProgressBarModule, typeof ɵngcc13.MatExpansionModule, typeof ɵngcc14.MatTableModule, typeof ɵngcc15.FormsModule, typeof ɵngcc15.ReactiveFormsModule, typeof ɵngcc16.MatPaginatorModule, typeof ɵngcc17.MatDividerModule, typeof ɵngcc18.MatListModule, typeof ɵngcc19.MatProgressSpinnerModule, typeof ɵngcc20.MatCheckboxModule, typeof ɵngcc21.MatChipsModule, typeof ɵngcc22.MatTooltipModule, typeof ɵngcc23.MatSortModule, typeof ɵngcc24.MatSidenavModule, typeof ɵngcc25.MatSliderModule, typeof ɵngcc26.MatSlideToggleModule, typeof ɵngcc27.MatButtonToggleModule, typeof ɵngcc28.MatOptionModule, typeof ɵngcc29.MatDatepickerModule, typeof ɵngcc30.MatAutocompleteModule, typeof ɵngcc31.MatSelectModule, typeof ɵngcc32.MatBadgeModule, typeof ɵngcc33.DragDropModule], [typeof ɵngcc1.MatButtonModule, typeof ɵngcc2.MatToolbarModule, typeof ɵngcc3.MatIconModule, typeof ɵngcc4.MatMenuModule, typeof ɵngcc5.MatRadioModule, typeof ɵngcc6.MatFormFieldModule, typeof ɵngcc7.MatInputModule, typeof ɵngcc8.MatCardModule, typeof ɵngcc9.MatDialogModule, typeof ɵngcc10.MatGridListModule, typeof ɵngcc11.MatSnackBarModule, typeof ɵngcc12.MatProgressBarModule, typeof ɵngcc13.MatExpansionModule, typeof ɵngcc14.MatTableModule, typeof ɵngcc15.FormsModule, typeof ɵngcc15.ReactiveFormsModule, typeof ɵngcc16.MatPaginatorModule, typeof ɵngcc17.MatDividerModule, typeof ɵngcc18.MatListModule, typeof ɵngcc19.MatProgressSpinnerModule, typeof ɵngcc20.MatCheckboxModule, typeof ɵngcc21.MatChipsModule, typeof ɵngcc22.MatTooltipModule, typeof ɵngcc23.MatSortModule, typeof ɵngcc24.MatSidenavModule, typeof ɵngcc25.MatSliderModule, typeof ɵngcc26.MatSlideToggleModule, typeof ɵngcc27.MatButtonToggleModule, typeof ɵngcc28.MatOptionModule, typeof ɵngcc29.MatDatepickerModule, typeof ɵngcc30.MatAutocompleteModule, typeof ɵngcc31.MatSelectModule, typeof ɵngcc28.MatNativeDateModule, typeof ɵngcc32.MatBadgeModule, typeof ɵngcc33.DragDropModule]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<MaterialModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbIm1hdGVyaWFsLm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVjbGFyZSBjbGFzcyBNYXRlcmlhbE1vZHVsZSB7XHJcbn1cclxuIl19