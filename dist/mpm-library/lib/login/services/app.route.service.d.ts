import { Router } from '@angular/router';
import { AuthenticationService } from '../../../lib/mpm-utils/services/authentication.service';
import { OTMMService } from '../../../lib/mpm-utils/services/otmm.service';
import * as ɵngcc0 from '@angular/core';
export declare class AppRouteService {
    router: Router;
    authService: AuthenticationService;
    otmmService: OTMMService;
    constructor(router: Router, authService: AuthenticationService, otmmService: OTMMService);
    goToLogout(): void;
    storeLastVisitLink(): void;
    getLastVisitedLinik(): string;
    goToLogin(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AppRouteService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRlLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsiYXBwLnJvdXRlLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbGliL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEFwcFJvdXRlU2VydmljZSB7XHJcbiAgICByb3V0ZXI6IFJvdXRlcjtcclxuICAgIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3Rvcihyb3V0ZXI6IFJvdXRlciwgYXV0aFNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSwgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlKTtcclxuICAgIGdvVG9Mb2dvdXQoKTogdm9pZDtcclxuICAgIHN0b3JlTGFzdFZpc2l0TGluaygpOiB2b2lkO1xyXG4gICAgZ2V0TGFzdFZpc2l0ZWRMaW5paygpOiBzdHJpbmc7XHJcbiAgICBnb1RvTG9naW4oKTogdm9pZDtcclxufVxyXG4iXX0=