import { EventEmitter } from '@angular/core';
import { AuthenticationService } from '../mpm-utils/services/authentication.service';
import { NotificationService } from '../notification/notification.service';
import { UtilService } from '../mpm-utils/services/util.service';
import * as ɵngcc0 from '@angular/core';
export declare class LoginComponent {
    authService: AuthenticationService;
    notification: NotificationService;
    utilService: UtilService;
    authenticated: EventEmitter<object>;
    isProcessing: boolean;
    isInvalid: boolean;
    usermodel: {
        username: string;
        password: string;
    };
    constructor(authService: AuthenticationService, notification: NotificationService, utilService: UtilService);
    resetFormState: () => void;
    toggleLoadingBtn: (isShow: any) => void;
    authenticate(userData: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<LoginComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<LoginComponent, "mpm-login", never, {}, { "authenticated": "authenticated"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgTG9naW5Db21wb25lbnQge1xyXG4gICAgYXV0aFNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIGF1dGhlbnRpY2F0ZWQ6IEV2ZW50RW1pdHRlcjxvYmplY3Q+O1xyXG4gICAgaXNQcm9jZXNzaW5nOiBib29sZWFuO1xyXG4gICAgaXNJbnZhbGlkOiBib29sZWFuO1xyXG4gICAgdXNlcm1vZGVsOiB7XHJcbiAgICAgICAgdXNlcm5hbWU6IHN0cmluZztcclxuICAgICAgICBwYXNzd29yZDogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIGNvbnN0cnVjdG9yKGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSwgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlKTtcclxuICAgIHJlc2V0Rm9ybVN0YXRlOiAoKSA9PiB2b2lkO1xyXG4gICAgdG9nZ2xlTG9hZGluZ0J0bjogKGlzU2hvdzogYW55KSA9PiB2b2lkO1xyXG4gICAgYXV0aGVudGljYXRlKHVzZXJEYXRhOiBhbnkpOiB2b2lkO1xyXG59XHJcbiJdfQ==