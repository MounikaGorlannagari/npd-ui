import { EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class CopyClipboardDirective {
    copyToClipboard: string;
    copiedToClipboard: EventEmitter<string>;
    onClick(event: MouseEvent): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CopyClipboardDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<CopyClipboardDirective, "[appCopyToClipboard]", never, { "copyToClipboard": "copyToClipboard"; }, { "copiedToClipboard": "copiedToClipboard"; }, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29weUNsaXBib2FyZERpcmVjdGl2ZS5kLnRzIiwic291cmNlcyI6WyJDb3B5Q2xpcGJvYXJkRGlyZWN0aXZlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvcHlDbGlwYm9hcmREaXJlY3RpdmUge1xyXG4gICAgY29weVRvQ2xpcGJvYXJkOiBzdHJpbmc7XHJcbiAgICBjb3BpZWRUb0NsaXBib2FyZDogRXZlbnRFbWl0dGVyPHN0cmluZz47XHJcbiAgICBvbkNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZDtcclxufVxyXG4iXX0=