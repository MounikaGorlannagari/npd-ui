import { ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import * as ɵngcc0 from '@angular/core';
export declare class AutofocusDirective {
    elementRef: ElementRef;
    ngControl: NgControl;
    constructor(elementRef: ElementRef, ngControl: NgControl);
    set appAutoFocus(condition: boolean);
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AutofocusDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<AutofocusDirective, "[appAutoFocus]", never, { "appAutoFocus": "appAutoFocus"; }, {}, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0b2ZvY3VzRGlyZWN0aXZlLmQudHMiLCJzb3VyY2VzIjpbIkF1dG9mb2N1c0RpcmVjdGl2ZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQXV0b2ZvY3VzRGlyZWN0aXZlIHtcclxuICAgIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWY7XHJcbiAgICBuZ0NvbnRyb2w6IE5nQ29udHJvbDtcclxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsIG5nQ29udHJvbDogTmdDb250cm9sKTtcclxuICAgIHNldCBhcHBBdXRvRm9jdXMoY29uZGl0aW9uOiBib29sZWFuKTtcclxufVxyXG4iXX0=