import { NgControl } from '@angular/forms';
import * as ɵngcc0 from '@angular/core';
export declare class DisableControlDirective {
    ngControl: NgControl;
    constructor(ngControl: NgControl);
    set disableControl(condition: boolean);
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DisableControlDirective, never>;
    static ɵdir: ɵngcc0.ɵɵDirectiveDefWithMeta<DisableControlDirective, "[disableControl]", never, { "disableControl": "disableControl"; }, {}, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGlzYWJsZUNvbnRyb2xEaXJlY3RpdmUuZC50cyIsInNvdXJjZXMiOlsiRGlzYWJsZUNvbnRyb2xEaXJlY3RpdmUuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ0NvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIERpc2FibGVDb250cm9sRGlyZWN0aXZlIHtcclxuICAgIG5nQ29udHJvbDogTmdDb250cm9sO1xyXG4gICAgY29uc3RydWN0b3IobmdDb250cm9sOiBOZ0NvbnRyb2wpO1xyXG4gICAgc2V0IGRpc2FibGVDb250cm9sKGNvbmRpdGlvbjogYm9vbGVhbik7XHJcbn1cclxuIl19