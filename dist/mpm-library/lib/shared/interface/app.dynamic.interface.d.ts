export interface AppDynamicConfigInterface {
    FAV_ICON_PATH: string;
    APP_LOGO_PATH: string;
    APP_THEME_NAME?: string;
    DEFAULT_BRANDING_STYLE_PATH: string;
    DEFAULT_PROJECT_THUMBNAIL?: string;
}
