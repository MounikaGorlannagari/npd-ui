import { Subscription } from 'rxjs';
export declare class OTMMSessionHandler {
    static instance: OTMMSessionHandler;
    static otmmSessionSubscription: Subscription;
    constructor();
    static getInstance(): OTMMSessionHandler;
    static subscribe(): Subscription;
    static unsubscribe(): void;
}
