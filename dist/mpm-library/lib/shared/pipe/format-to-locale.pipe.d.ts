import { PipeTransform } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class FormatToLocalePipe implements PipeTransform {
    constructor();
    transform(value: any, args?: any): any;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<FormatToLocalePipe, never>;
    static ɵpipe: ɵngcc0.ɵɵPipeDefWithMeta<FormatToLocalePipe, "formatToLocale">;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRvLWxvY2FsZS5waXBlLmQudHMiLCJzb3VyY2VzIjpbImZvcm1hdC10by1sb2NhbGUucGlwZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEZvcm1hdFRvTG9jYWxlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBhcmdzPzogYW55KTogYW55O1xyXG59XHJcbiJdfQ==