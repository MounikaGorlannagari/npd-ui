import { PipeTransform } from '@angular/core';
import { MPMField } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../services/field-config.service';
import * as ɵngcc0 from '@angular/core';
export declare class GetPropertyPipe implements PipeTransform {
    private fieldConfigService;
    constructor(fieldConfigService: FieldConfigService);
    transform(projectData: any, displayColumn: MPMField, mapperName?: string): unknown;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<GetPropertyPipe, never>;
    static ɵpipe: ɵngcc0.ɵɵPipeDefWithMeta<GetPropertyPipe, "getProperties">;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0LXByb3BlcnR5LnBpcGUuZC50cyIsInNvdXJjZXMiOlsiZ2V0LXByb3BlcnR5LnBpcGUuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBHZXRQcm9wZXJ0eVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAgIHByaXZhdGUgZmllbGRDb25maWdTZXJ2aWNlO1xyXG4gICAgY29uc3RydWN0b3IoZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UpO1xyXG4gICAgdHJhbnNmb3JtKHByb2plY3REYXRhOiBhbnksIGRpc3BsYXlDb2x1bW46IE1QTUZpZWxkLCBtYXBwZXJOYW1lPzogc3RyaW5nKTogdW5rbm93bjtcclxufVxyXG4iXX0=