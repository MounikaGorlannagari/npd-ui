import { PipeTransform } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class FormatToLocaleDateTimePipe implements PipeTransform {
    constructor();
    transform(value: any, args?: any): any;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<FormatToLocaleDateTimePipe, never>;
    static ɵpipe: ɵngcc0.ɵɵPipeDefWithMeta<FormatToLocaleDateTimePipe, "formatToLocaleDateTime">;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRvLWxvY2FsZS1kYXRlLXRpbWUucGlwZS5kLnRzIiwic291cmNlcyI6WyJmb3JtYXQtdG8tbG9jYWxlLWRhdGUtdGltZS5waXBlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgRm9ybWF0VG9Mb2NhbGVEYXRlVGltZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgYXJncz86IGFueSk6IGFueTtcclxufVxyXG4iXX0=