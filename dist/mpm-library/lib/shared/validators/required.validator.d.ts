import { AbstractControl } from '@angular/forms';
export declare class RequiredValidator {
    static validateRequired(c: AbstractControl): {
        required: boolean;
    };
}
