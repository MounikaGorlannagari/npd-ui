import { AbstractControl } from '@angular/forms';
export declare class DateValidators {
    static dateFormat(ac: AbstractControl): {
        dateFormat: boolean;
    };
}
