import { AbstractControl } from '@angular/forms';
export declare class EmailValidator {
    static validateEmails(c: AbstractControl): any;
}
