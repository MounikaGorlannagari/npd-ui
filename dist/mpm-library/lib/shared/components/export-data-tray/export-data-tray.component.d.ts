import { AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import * as ɵngcc0 from '@angular/core';
export declare class ExportDataTrayComponent implements AfterViewInit {
    loaderService: LoaderService;
    otmmService: OTMMService;
    notificationService: NotificationService;
    qdsService: QdsService;
    sharingService: SharingService;
    utilService: UtilService;
    appService: AppService;
    sanitizer: DomSanitizer;
    dialog: MatDialog;
    displayedColumns: string[];
    dataSource: MatTableDataSource<any>;
    isLoadingData: boolean;
    noDataMsg: string;
    noData: boolean;
    showActions: boolean;
    loggedInUser: any;
    loggedInUserName: any;
    fileUrl: any;
    exportTraySort: MatSort;
    constructor(loaderService: LoaderService, otmmService: OTMMService, notificationService: NotificationService, qdsService: QdsService, sharingService: SharingService, utilService: UtilService, appService: AppService, sanitizer: DomSanitizer, dialog: MatDialog);
    getExportData(): void;
    ngAfterViewInit(): void;
    refreshData(): void;
    downloadFile(encodedString: any, fileNameWithExtension: any): void;
    download(data: any): void;
    createFileExtension(exportData: any): string;
    deleteExportTray(exportData: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ExportDataTrayComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ExportDataTrayComponent, "mpm-export-data-tray", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEtdHJheS5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiZXhwb3J0LWRhdGEtdHJheS5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHsgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NvcnQnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBFeHBvcnREYXRhVHJheUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xyXG4gICAgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZTtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlO1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlO1xyXG4gICAgYXBwU2VydmljZTogQXBwU2VydmljZTtcclxuICAgIHNhbml0aXplcjogRG9tU2FuaXRpemVyO1xyXG4gICAgZGlhbG9nOiBNYXREaWFsb2c7XHJcbiAgICBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXTtcclxuICAgIGRhdGFTb3VyY2U6IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+O1xyXG4gICAgaXNMb2FkaW5nRGF0YTogYm9vbGVhbjtcclxuICAgIG5vRGF0YU1zZzogc3RyaW5nO1xyXG4gICAgbm9EYXRhOiBib29sZWFuO1xyXG4gICAgc2hvd0FjdGlvbnM6IGJvb2xlYW47XHJcbiAgICBsb2dnZWRJblVzZXI6IGFueTtcclxuICAgIGxvZ2dlZEluVXNlck5hbWU6IGFueTtcclxuICAgIGZpbGVVcmw6IGFueTtcclxuICAgIGV4cG9ydFRyYXlTb3J0OiBNYXRTb3J0O1xyXG4gICAgY29uc3RydWN0b3IobG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSwgYXBwU2VydmljZTogQXBwU2VydmljZSwgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIsIGRpYWxvZzogTWF0RGlhbG9nKTtcclxuICAgIGdldEV4cG9ydERhdGEoKTogdm9pZDtcclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkO1xyXG4gICAgcmVmcmVzaERhdGEoKTogdm9pZDtcclxuICAgIGRvd25sb2FkRmlsZShlbmNvZGVkU3RyaW5nOiBhbnksIGZpbGVOYW1lV2l0aEV4dGVuc2lvbjogYW55KTogdm9pZDtcclxuICAgIGRvd25sb2FkKGRhdGE6IGFueSk6IHZvaWQ7XHJcbiAgICBjcmVhdGVGaWxlRXh0ZW5zaW9uKGV4cG9ydERhdGE6IGFueSk6IHN0cmluZztcclxuICAgIGRlbGV0ZUV4cG9ydFRyYXkoZXhwb3J0RGF0YTogYW55KTogdm9pZDtcclxufVxyXG4iXX0=