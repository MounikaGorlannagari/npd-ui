import { AfterViewInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { AssetFileConfigService } from '../../../upload/services/asset.file.config.service';
import * as ɵngcc0 from '@angular/core';
export interface QDSJobFile {
    preview: any;
    filename: string;
    status: string;
    started: Date;
    type: string;
    fileType: string;
    icon: string;
    progress: number;
}
export declare class QdsTransferTrayComponent implements AfterViewInit, OnDestroy {
    loaderService: LoaderService;
    otmmService: OTMMService;
    notificationService: NotificationService;
    qdsService: QdsService;
    assetFileConfigService: AssetFileConfigService;
    displayedColumns: string[];
    dataSource: MatTableDataSource<QDSJobFile>;
    isLoadingData: boolean;
    thumbnailFormats: string[];
    transferStatusMapper: {
        complete: string;
        transferred: string;
        failed: string;
        reassembling: string;
        saving: string;
        starting: string;
        transferring: string;
        waiting: string;
    };
    activeJobIdList: {
        EXPORT: {};
        IMPORT: {};
    };
    failedJobIdList: {};
    showFailed: boolean;
    noDataMsg: string;
    noData: boolean;
    showActions: boolean;
    downloadLocation: string;
    restartingJobCount: number;
    sort: MatSort;
    liveDataSubscription: Subscription;
    constructor(loaderService: LoaderService, otmmService: OTMMService, notificationService: NotificationService, qdsService: QdsService, assetFileConfigService: AssetFileConfigService);
    getFileTranferStatus(job: any, file: any): any;
    refresh(): void;
    copyDownloadLocation(): void;
    getDownloadLocation(): void;
    changeDownloadLocation(): void;
    getBase64(filePath: any): Observable<any>;
    createFileObj(file: any, data: any, fileObjType?: any): any;
    getExportJob(): void;
    calculateInprogress(): void;
    getQDSTransferJobs(): Observable<unknown>;
    checkData(): Observable<any>;
    resumeJobs(state: any): void;
    toggleImportJobs(state: any): void;
    retryFailedJobs(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<QdsTransferTrayComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<QdsTransferTrayComponent, "mpm-qds-transfer-tray", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbInFkcy10cmFuc2Zlci10cmF5LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0JztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBRZHNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vdXBsb2FkL3NlcnZpY2VzL3Fkcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRGaWxlQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9hc3NldC5maWxlLmNvbmZpZy5zZXJ2aWNlJztcclxuZXhwb3J0IGludGVyZmFjZSBRRFNKb2JGaWxlIHtcclxuICAgIHByZXZpZXc6IGFueTtcclxuICAgIGZpbGVuYW1lOiBzdHJpbmc7XHJcbiAgICBzdGF0dXM6IHN0cmluZztcclxuICAgIHN0YXJ0ZWQ6IERhdGU7XHJcbiAgICB0eXBlOiBzdHJpbmc7XHJcbiAgICBmaWxlVHlwZTogc3RyaW5nO1xyXG4gICAgaWNvbjogc3RyaW5nO1xyXG4gICAgcHJvZ3Jlc3M6IG51bWJlcjtcclxufVxyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBRZHNUcmFuc2ZlclRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZTtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlO1xyXG4gICAgYXNzZXRGaWxlQ29uZmlnU2VydmljZTogQXNzZXRGaWxlQ29uZmlnU2VydmljZTtcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdO1xyXG4gICAgZGF0YVNvdXJjZTogTWF0VGFibGVEYXRhU291cmNlPFFEU0pvYkZpbGU+O1xyXG4gICAgaXNMb2FkaW5nRGF0YTogYm9vbGVhbjtcclxuICAgIHRodW1ibmFpbEZvcm1hdHM6IHN0cmluZ1tdO1xyXG4gICAgdHJhbnNmZXJTdGF0dXNNYXBwZXI6IHtcclxuICAgICAgICBjb21wbGV0ZTogc3RyaW5nO1xyXG4gICAgICAgIHRyYW5zZmVycmVkOiBzdHJpbmc7XHJcbiAgICAgICAgZmFpbGVkOiBzdHJpbmc7XHJcbiAgICAgICAgcmVhc3NlbWJsaW5nOiBzdHJpbmc7XHJcbiAgICAgICAgc2F2aW5nOiBzdHJpbmc7XHJcbiAgICAgICAgc3RhcnRpbmc6IHN0cmluZztcclxuICAgICAgICB0cmFuc2ZlcnJpbmc6IHN0cmluZztcclxuICAgICAgICB3YWl0aW5nOiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgYWN0aXZlSm9iSWRMaXN0OiB7XHJcbiAgICAgICAgRVhQT1JUOiB7fTtcclxuICAgICAgICBJTVBPUlQ6IHt9O1xyXG4gICAgfTtcclxuICAgIGZhaWxlZEpvYklkTGlzdDoge307XHJcbiAgICBzaG93RmFpbGVkOiBib29sZWFuO1xyXG4gICAgbm9EYXRhTXNnOiBzdHJpbmc7XHJcbiAgICBub0RhdGE6IGJvb2xlYW47XHJcbiAgICBzaG93QWN0aW9uczogYm9vbGVhbjtcclxuICAgIGRvd25sb2FkTG9jYXRpb246IHN0cmluZztcclxuICAgIHJlc3RhcnRpbmdKb2JDb3VudDogbnVtYmVyO1xyXG4gICAgc29ydDogTWF0U29ydDtcclxuICAgIGxpdmVEYXRhU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgICBjb25zdHJ1Y3Rvcihsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2UsIGFzc2V0RmlsZUNvbmZpZ1NlcnZpY2U6IEFzc2V0RmlsZUNvbmZpZ1NlcnZpY2UpO1xyXG4gICAgZ2V0RmlsZVRyYW5mZXJTdGF0dXMoam9iOiBhbnksIGZpbGU6IGFueSk6IGFueTtcclxuICAgIHJlZnJlc2goKTogdm9pZDtcclxuICAgIGNvcHlEb3dubG9hZExvY2F0aW9uKCk6IHZvaWQ7XHJcbiAgICBnZXREb3dubG9hZExvY2F0aW9uKCk6IHZvaWQ7XHJcbiAgICBjaGFuZ2VEb3dubG9hZExvY2F0aW9uKCk6IHZvaWQ7XHJcbiAgICBnZXRCYXNlNjQoZmlsZVBhdGg6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGNyZWF0ZUZpbGVPYmooZmlsZTogYW55LCBkYXRhOiBhbnksIGZpbGVPYmpUeXBlPzogYW55KTogYW55O1xyXG4gICAgZ2V0RXhwb3J0Sm9iKCk6IHZvaWQ7XHJcbiAgICBjYWxjdWxhdGVJbnByb2dyZXNzKCk6IHZvaWQ7XHJcbiAgICBnZXRRRFNUcmFuc2ZlckpvYnMoKTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIGNoZWNrRGF0YSgpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICByZXN1bWVKb2JzKHN0YXRlOiBhbnkpOiB2b2lkO1xyXG4gICAgdG9nZ2xlSW1wb3J0Sm9icyhzdGF0ZTogYW55KTogdm9pZDtcclxuICAgIHJldHJ5RmFpbGVkSm9icygpOiB2b2lkO1xyXG4gICAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQ7XHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkO1xyXG59XHJcbiJdfQ==