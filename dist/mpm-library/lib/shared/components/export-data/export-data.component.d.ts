import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AppService } from '../../../mpm-utils/services/app.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class ExportDataComponent implements OnInit {
    dialogRef: MatDialogRef<ExportDataComponent>;
    data: any;
    private appService;
    private formBuilder;
    private loaderService;
    private sharingService;
    notificationService: NotificationService;
    exportDataForm: FormGroup;
    appConfig: any;
    filePath: any;
    pageLimit: any;
    exportDataMinCount: any;
    exportType: {
        value: string;
        viewValue: string;
    }[];
    constructor(dialogRef: MatDialogRef<ExportDataComponent>, data: any, appService: AppService, formBuilder: FormBuilder, loaderService: LoaderService, sharingService: SharingService, notificationService: NotificationService);
    ngOnInit(): void;
    onExportData(): void;
    initialiseForm(): void;
    closeDialog(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ExportDataComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ExportDataComponent, "mpm-export-data", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImV4cG9ydC1kYXRhLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBFeHBvcnREYXRhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEV4cG9ydERhdGFDb21wb25lbnQ+O1xyXG4gICAgZGF0YTogYW55O1xyXG4gICAgcHJpdmF0ZSBhcHBTZXJ2aWNlO1xyXG4gICAgcHJpdmF0ZSBmb3JtQnVpbGRlcjtcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTtcclxuICAgIHByaXZhdGUgc2hhcmluZ1NlcnZpY2U7XHJcbiAgICBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgZXhwb3J0RGF0YUZvcm06IEZvcm1Hcm91cDtcclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgZmlsZVBhdGg6IGFueTtcclxuICAgIHBhZ2VMaW1pdDogYW55O1xyXG4gICAgZXhwb3J0RGF0YU1pbkNvdW50OiBhbnk7XHJcbiAgICBleHBvcnRUeXBlOiB7XHJcbiAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgICAgICB2aWV3VmFsdWU6IHN0cmluZztcclxuICAgIH1bXTtcclxuICAgIGNvbnN0cnVjdG9yKGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEV4cG9ydERhdGFDb21wb25lbnQ+LCBkYXRhOiBhbnksIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlciwgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlKTtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbiAgICBvbkV4cG9ydERhdGEoKTogdm9pZDtcclxuICAgIGluaXRpYWxpc2VGb3JtKCk6IHZvaWQ7XHJcbiAgICBjbG9zZURpYWxvZygpOiB2b2lkO1xyXG59XHJcbiJdfQ==