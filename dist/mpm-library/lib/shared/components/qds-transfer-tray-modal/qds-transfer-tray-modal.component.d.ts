import { MatDialogRef } from '@angular/material/dialog';
import * as ɵngcc0 from '@angular/core';
export declare class QdsTransferTrayModalComponent {
    dialogRef: MatDialogRef<QdsTransferTrayModalComponent>;
    data: any;
    modalTitle: string;
    constructor(dialogRef: MatDialogRef<QdsTransferTrayModalComponent>, data: any);
    cancelDialog(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<QdsTransferTrayModalComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<QdsTransferTrayModalComponent, "mpm-qds-transfer-tray-modal", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbInFkcy10cmFuc2Zlci10cmF5LW1vZGFsLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCB7XHJcbiAgICBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudD47XHJcbiAgICBkYXRhOiBhbnk7XHJcbiAgICBtb2RhbFRpdGxlOiBzdHJpbmc7XHJcbiAgICBjb25zdHJ1Y3RvcihkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudD4sIGRhdGE6IGFueSk7XHJcbiAgICBjYW5jZWxEaWFsb2coKTogdm9pZDtcclxufVxyXG4iXX0=