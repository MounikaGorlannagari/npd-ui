import { OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import * as ɵngcc0 from '@angular/core';
export declare class IframeComponent implements OnInit {
    sanitizer: DomSanitizer;
    location: Location;
    iframeObj: any;
    url: any;
    constructor(sanitizer: DomSanitizer, location: Location);
    close(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<IframeComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<IframeComponent, "mpm-iframe", never, { "iframeObj": "iframeObj"; }, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWZyYW1lLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJpZnJhbWUuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERvbVNhbml0aXplciB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIElmcmFtZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBzYW5pdGl6ZXI6IERvbVNhbml0aXplcjtcclxuICAgIGxvY2F0aW9uOiBMb2NhdGlvbjtcclxuICAgIGlmcmFtZU9iajogYW55O1xyXG4gICAgdXJsOiBhbnk7XHJcbiAgICBjb25zdHJ1Y3RvcihzYW5pdGl6ZXI6IERvbVNhbml0aXplciwgbG9jYXRpb246IExvY2F0aW9uKTtcclxuICAgIGNsb3NlKCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==