import { OnInit, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import * as ɵngcc0 from '@angular/core';
export declare class CustomSearchFieldComponent implements OnInit {
    searchFieldConfig: any;
    required: any;
    valueChange: EventEmitter<any>;
    selectedOption: EventEmitter<any>;
    details: EventEmitter<any>;
    userFormControl: FormControl;
    filterOptions: Observable<any[]>;
    requiredField: boolean;
    selectedValue: {
        id: string;
        value: string;
    };
    constructor();
    filterUser(value: string): string[];
    displayFn(user?: any): string | undefined;
    onFocusOut(event: any): void;
    changeAutoComplete(event: any): void;
    refreshSearchFields(): void;
    getDetails(name: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CustomSearchFieldComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CustomSearchFieldComponent, "mpm-custom-search-field", never, { "searchFieldConfig": "searchFieldConfig"; "required": "required"; }, { "valueChange": "valueChange"; "selectedOption": "selectedOption"; "details": "details"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHNlYXJjaEZpZWxkQ29uZmlnOiBhbnk7XHJcbiAgICByZXF1aXJlZDogYW55O1xyXG4gICAgdmFsdWVDaGFuZ2U6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgc2VsZWN0ZWRPcHRpb246IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgZGV0YWlsczogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICB1c2VyRm9ybUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG4gICAgZmlsdGVyT3B0aW9uczogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgICByZXF1aXJlZEZpZWxkOiBib29sZWFuO1xyXG4gICAgc2VsZWN0ZWRWYWx1ZToge1xyXG4gICAgICAgIGlkOiBzdHJpbmc7XHJcbiAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgIH07XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgZmlsdGVyVXNlcih2YWx1ZTogc3RyaW5nKTogc3RyaW5nW107XHJcbiAgICBkaXNwbGF5Rm4odXNlcj86IGFueSk6IHN0cmluZyB8IHVuZGVmaW5lZDtcclxuICAgIG9uRm9jdXNPdXQoZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICBjaGFuZ2VBdXRvQ29tcGxldGUoZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICByZWZyZXNoU2VhcmNoRmllbGRzKCk6IHZvaWQ7XHJcbiAgICBnZXREZXRhaWxzKG5hbWU6IGFueSk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==