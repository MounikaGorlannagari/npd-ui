import { OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { CustomSelect } from './objects/CustomSelectObject';
import * as ɵngcc0 from '@angular/core';
export declare class CustomSelectComponent implements OnInit, OnChanges {
    selected: CustomSelect;
    options: Array<CustomSelect>;
    controlLabel: string;
    selectionChange: EventEmitter<CustomSelect>;
    bulkEdit: any;
    deliverableBulkEdit: any;
    constructor();
    selectedOption: CustomSelect;
    onSelectionChange(currentOption: any): void;
    handleDefaultSelection(): void;
    initalize(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CustomSelectComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CustomSelectComponent, "mpm-custom-select", never, { "options": "options"; "selected": "selected"; "controlLabel": "controlLabel"; "bulkEdit": "bulkEdit"; "deliverableBulkEdit": "deliverableBulkEdit"; }, { "selectionChange": "selectionChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDdXN0b21TZWxlY3QgfSBmcm9tICcuL29iamVjdHMvQ3VzdG9tU2VsZWN0T2JqZWN0JztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQ3VzdG9tU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgc2VsZWN0ZWQ6IEN1c3RvbVNlbGVjdDtcclxuICAgIG9wdGlvbnM6IEFycmF5PEN1c3RvbVNlbGVjdD47XHJcbiAgICBjb250cm9sTGFiZWw6IHN0cmluZztcclxuICAgIHNlbGVjdGlvbkNoYW5nZTogRXZlbnRFbWl0dGVyPEN1c3RvbVNlbGVjdD47XHJcbiAgICBidWxrRWRpdDogYW55O1xyXG4gICAgZGVsaXZlcmFibGVCdWxrRWRpdDogYW55O1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIHNlbGVjdGVkT3B0aW9uOiBDdXN0b21TZWxlY3Q7XHJcbiAgICBvblNlbGVjdGlvbkNoYW5nZShjdXJyZW50T3B0aW9uOiBhbnkpOiB2b2lkO1xyXG4gICAgaGFuZGxlRGVmYXVsdFNlbGVjdGlvbigpOiB2b2lkO1xyXG4gICAgaW5pdGFsaXplKCk6IHZvaWQ7XHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19