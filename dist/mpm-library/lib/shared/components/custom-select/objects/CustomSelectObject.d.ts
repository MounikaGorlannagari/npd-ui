import { StatusTypes } from '../../../../mpm-utils/objects/StatusType';
export interface CustomSelect {
    name?: string;
    value?: Readonly<string>;
    default?: boolean;
    searchCondition?: any;
    mapperName?: string;
    statusFilterList?: Array<StatusTypes>;
}
