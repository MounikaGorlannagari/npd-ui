import { AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class DownloadsTransferTrayComponent implements AfterViewInit {
    loaderService: LoaderService;
    otmmService: OTMMService;
    notificationService: NotificationService;
    qdsService: QdsService;
    sharingService: SharingService;
    utilService: UtilService;
    displayedColumns: string[];
    dataSource: MatTableDataSource<any>;
    isLoadingData: boolean;
    noDataMsg: string;
    noData: boolean;
    showActions: boolean;
    constructor(loaderService: LoaderService, otmmService: OTMMService, notificationService: NotificationService, qdsService: QdsService, sharingService: SharingService, utilService: UtilService);
    convertToMB(byte: number): number;
    getExportJobs(): void;
    download(job: any): void;
    ngAfterViewInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DownloadsTransferTrayComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<DownloadsTransferTrayComponent, "mpm-downloads-transfer-tray", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImRvd25sb2Fkcy10cmFuc2Zlci10cmF5LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBEb3dubG9hZHNUcmFuc2ZlclRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgcWRzU2VydmljZTogUWRzU2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdO1xyXG4gICAgZGF0YVNvdXJjZTogTWF0VGFibGVEYXRhU291cmNlPGFueT47XHJcbiAgICBpc0xvYWRpbmdEYXRhOiBib29sZWFuO1xyXG4gICAgbm9EYXRhTXNnOiBzdHJpbmc7XHJcbiAgICBub0RhdGE6IGJvb2xlYW47XHJcbiAgICBzaG93QWN0aW9uczogYm9vbGVhbjtcclxuICAgIGNvbnN0cnVjdG9yKGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSwgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSwgcWRzU2VydmljZTogUWRzU2VydmljZSwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLCB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UpO1xyXG4gICAgY29udmVydFRvTUIoYnl0ZTogbnVtYmVyKTogbnVtYmVyO1xyXG4gICAgZ2V0RXhwb3J0Sm9icygpOiB2b2lkO1xyXG4gICAgZG93bmxvYWQoam9iOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19