import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NotificationService } from '../../../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class ConfirmationModalComponent implements OnInit {
    dialogRef: MatDialogRef<ConfirmationModalComponent>;
    data: any;
    notificationService: NotificationService;
    message: any;
    confirmationComment: string;
    confirmationHeading: string;
    taskName: any;
    constructor(dialogRef: MatDialogRef<ConfirmationModalComponent>, data: any, notificationService: NotificationService);
    closeDialog(): void;
    ngOnInit(): void;
    onYesClick(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ConfirmationModalComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ConfirmationModalComponent, "mpm-confirmation-modal", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJjb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50PjtcclxuICAgIGRhdGE6IGFueTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBtZXNzYWdlOiBhbnk7XHJcbiAgICBjb25maXJtYXRpb25Db21tZW50OiBzdHJpbmc7XHJcbiAgICBjb25maXJtYXRpb25IZWFkaW5nOiBzdHJpbmc7XHJcbiAgICB0YXNrTmFtZTogYW55O1xyXG4gICAgY29uc3RydWN0b3IoZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8Q29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQ+LCBkYXRhOiBhbnksIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UpO1xyXG4gICAgY2xvc2VEaWFsb2coKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbiAgICBvblllc0NsaWNrKCk6IHZvaWQ7XHJcbn1cclxuIl19