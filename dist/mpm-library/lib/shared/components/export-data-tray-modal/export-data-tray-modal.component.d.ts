import { MatDialogRef } from '@angular/material/dialog';
import { ExportDataTrayComponent } from '../export-data-tray/export-data-tray.component';
import * as ɵngcc0 from '@angular/core';
export declare class ExportDataTrayModalComponent {
    dialogRef: MatDialogRef<ExportDataTrayComponent>;
    data: any;
    modalTitle: string;
    constructor(dialogRef: MatDialogRef<ExportDataTrayComponent>, data: any);
    cancelDialog(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ExportDataTrayModalComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ExportDataTrayModalComponent, "mpm-export-data-tray-modal", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBFeHBvcnREYXRhVHJheUNvbXBvbmVudCB9IGZyb20gJy4uL2V4cG9ydC1kYXRhLXRyYXkvZXhwb3J0LWRhdGEtdHJheS5jb21wb25lbnQnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBFeHBvcnREYXRhVHJheU1vZGFsQ29tcG9uZW50IHtcclxuICAgIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEV4cG9ydERhdGFUcmF5Q29tcG9uZW50PjtcclxuICAgIGRhdGE6IGFueTtcclxuICAgIG1vZGFsVGl0bGU6IHN0cmluZztcclxuICAgIGNvbnN0cnVjdG9yKGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEV4cG9ydERhdGFUcmF5Q29tcG9uZW50PiwgZGF0YTogYW55KTtcclxuICAgIGNhbmNlbERpYWxvZygpOiB2b2lkO1xyXG59XHJcbiJdfQ==