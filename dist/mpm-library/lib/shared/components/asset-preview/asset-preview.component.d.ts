import { OnInit, OnChanges, SimpleChanges } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class AssetPreviewComponent implements OnInit, OnChanges {
    assetURL: string;
    rectangleWidth: string;
    rectangleHeight: string;
    isCollapsed: boolean;
    tempAppDynamicConfig: string;
    constructor();
    handleDefaultAssetURL(): void;
    ngOnInit(): void;
    ngOnChanges(simpleChanges: SimpleChanges): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetPreviewComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<AssetPreviewComponent, "mpm-asset-preview", never, { "assetURL": "assetURL"; "rectangleWidth": "rectangleWidth"; "rectangleHeight": "rectangleHeight"; "isCollapsed": "isCollapsed"; }, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtcHJldmlldy5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiYXNzZXQtcHJldmlldy5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBc3NldFByZXZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICBhc3NldFVSTDogc3RyaW5nO1xyXG4gICAgcmVjdGFuZ2xlV2lkdGg6IHN0cmluZztcclxuICAgIHJlY3RhbmdsZUhlaWdodDogc3RyaW5nO1xyXG4gICAgaXNDb2xsYXBzZWQ6IGJvb2xlYW47XHJcbiAgICB0ZW1wQXBwRHluYW1pY0NvbmZpZzogc3RyaW5nO1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIGhhbmRsZURlZmF1bHRBc3NldFVSTCgpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxuICAgIG5nT25DaGFuZ2VzKHNpbXBsZUNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkO1xyXG59XHJcbiJdfQ==