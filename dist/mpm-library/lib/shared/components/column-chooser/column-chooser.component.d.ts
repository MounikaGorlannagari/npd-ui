import { EventEmitter, OnChanges, OnInit } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class ColumnChooserComponent implements OnInit, OnChanges {
    columnChooserFields: any;
    isListView: boolean;
    isPMView: any;
    isColumnsFreezable: any;
    isAssetsTab: any;
    columnChooserHandler: EventEmitter<{
        'column': any[];
        'frezeCount': any;
    }>;
    saveColumnChooserHandler: EventEmitter<any[]>;
    resetColumnChooserHandler: EventEmitter<any>;
    freezeColumnCount: EventEmitter<any>;
    userPreferenceFreezeCount: any;
    constructor();
    ngOnChanges(): void;
    handleColumnChooser(columnChooserFields: any): void;
    saveColumnChooser(columnChooserFields: any): void;
    resetColumnChooser(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ColumnChooserComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ColumnChooserComponent, "mpm-column-chooser", never, { "columnChooserFields": "columnChooserFields"; "isListView": "isListView"; "isPMView": "isPMView"; "isColumnsFreezable": "isColumnsFreezable"; "isAssetsTab": "isAssetsTab"; "userPreferenceFreezeCount": "userPreferenceFreezeCount"; }, { "columnChooserHandler": "columnChooserHandler"; "saveColumnChooserHandler": "saveColumnChooserHandler"; "resetColumnChooserHandler": "resetColumnChooserHandler"; "freezeColumnCount": "freezeColumnCount"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXZlbnRFbWl0dGVyLCBPbkNoYW5nZXMsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDb2x1bW5DaG9vc2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgY29sdW1uQ2hvb3NlckZpZWxkczogYW55O1xyXG4gICAgaXNMaXN0VmlldzogYm9vbGVhbjtcclxuICAgIGlzUE1WaWV3OiBhbnk7XHJcbiAgICBpc0NvbHVtbnNGcmVlemFibGU6IGFueTtcclxuICAgIGlzQXNzZXRzVGFiOiBhbnk7XHJcbiAgICBjb2x1bW5DaG9vc2VySGFuZGxlcjogRXZlbnRFbWl0dGVyPHtcclxuICAgICAgICAnY29sdW1uJzogYW55W107XHJcbiAgICAgICAgJ2ZyZXplQ291bnQnOiBhbnk7XHJcbiAgICB9PjtcclxuICAgIHNhdmVDb2x1bW5DaG9vc2VySGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueVtdPjtcclxuICAgIHJlc2V0Q29sdW1uQ2hvb3NlckhhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgZnJlZXplQ29sdW1uQ291bnQ6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgdXNlclByZWZlcmVuY2VGcmVlemVDb3VudDogYW55O1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQ7XHJcbiAgICBoYW5kbGVDb2x1bW5DaG9vc2VyKGNvbHVtbkNob29zZXJGaWVsZHM6IGFueSk6IHZvaWQ7XHJcbiAgICBzYXZlQ29sdW1uQ2hvb3Nlcihjb2x1bW5DaG9vc2VyRmllbGRzOiBhbnkpOiB2b2lkO1xyXG4gICAgcmVzZXRDb2x1bW5DaG9vc2VyKCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==