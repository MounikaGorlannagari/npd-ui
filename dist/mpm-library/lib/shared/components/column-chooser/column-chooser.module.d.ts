import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './column-chooser.component';
import * as ɵngcc2 from './column-chooser-field/column-chooser-field.component';
import * as ɵngcc3 from '@angular/common';
import * as ɵngcc4 from '../../../material.module';
export declare class ColumnChooserModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<ColumnChooserModule, [typeof ɵngcc1.ColumnChooserComponent, typeof ɵngcc2.ColumnChooserFieldComponent], [typeof ɵngcc3.CommonModule, typeof ɵngcc4.MaterialModule], [typeof ɵngcc1.ColumnChooserComponent, typeof ɵngcc2.ColumnChooserFieldComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<ColumnChooserModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbImNvbHVtbi1jaG9vc2VyLm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGRlY2xhcmUgY2xhc3MgQ29sdW1uQ2hvb3Nlck1vZHVsZSB7XHJcbn1cclxuIl19