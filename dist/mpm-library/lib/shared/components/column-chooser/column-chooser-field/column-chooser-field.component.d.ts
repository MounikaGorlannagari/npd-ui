import { EventEmitter, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class ColumnChooserFieldComponent implements OnInit, OnChanges {
    columnChooserFields: any;
    isPMView: any;
    isColumnsFreezable: any;
    isAssetsTab: boolean;
    columnChooserHandler: EventEmitter<{
        'column': any[];
        'frezeCount': any;
    }>;
    saveColumnChooserHandler: EventEmitter<{
        'column': any[];
        'frezeCount': any;
    }>;
    resetColumnChooserHandler: EventEmitter<any>;
    userPreferenceFreezeCount: any;
    freezeColumnCountNumber: any;
    maxFreezeCount: any;
    columnsCount: any;
    enableIndexerFieldRestriction: any;
    maximumFieldsAllowed: any;
    constructor();
    onChecked(field: any): void;
    updateColumnChooser(isPreference: any): void;
    resetData(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    compareObjects(o1: any, o2: any): boolean;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ColumnChooserFieldComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ColumnChooserFieldComponent, "mpm-column-chooser-field", never, { "columnChooserFields": "columnChooserFields"; "isPMView": "isPMView"; "isColumnsFreezable": "isColumnsFreezable"; "isAssetsTab": "isAssetsTab"; "userPreferenceFreezeCount": "userPreferenceFreezeCount"; }, { "columnChooserHandler": "columnChooserHandler"; "saveColumnChooserHandler": "saveColumnChooserHandler"; "resetColumnChooserHandler": "resetColumnChooserHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXItZmllbGQuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNvbHVtbi1jaG9vc2VyLWZpZWxkLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgT25Jbml0LCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIENvbHVtbkNob29zZXJGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuICAgIGNvbHVtbkNob29zZXJGaWVsZHM6IGFueTtcclxuICAgIGlzUE1WaWV3OiBhbnk7XHJcbiAgICBpc0NvbHVtbnNGcmVlemFibGU6IGFueTtcclxuICAgIGlzQXNzZXRzVGFiOiBib29sZWFuO1xyXG4gICAgY29sdW1uQ2hvb3NlckhhbmRsZXI6IEV2ZW50RW1pdHRlcjx7XHJcbiAgICAgICAgJ2NvbHVtbic6IGFueVtdO1xyXG4gICAgICAgICdmcmV6ZUNvdW50JzogYW55O1xyXG4gICAgfT47XHJcbiAgICBzYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXI6IEV2ZW50RW1pdHRlcjx7XHJcbiAgICAgICAgJ2NvbHVtbic6IGFueVtdO1xyXG4gICAgICAgICdmcmV6ZUNvdW50JzogYW55O1xyXG4gICAgfT47XHJcbiAgICByZXNldENvbHVtbkNob29zZXJIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHVzZXJQcmVmZXJlbmNlRnJlZXplQ291bnQ6IGFueTtcclxuICAgIGZyZWV6ZUNvbHVtbkNvdW50TnVtYmVyOiBhbnk7XHJcbiAgICBtYXhGcmVlemVDb3VudDogYW55O1xyXG4gICAgY29sdW1uc0NvdW50OiBhbnk7XHJcbiAgICBlbmFibGVJbmRleGVyRmllbGRSZXN0cmljdGlvbjogYW55O1xyXG4gICAgbWF4aW11bUZpZWxkc0FsbG93ZWQ6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICBvbkNoZWNrZWQoZmllbGQ6IGFueSk6IHZvaWQ7XHJcbiAgICB1cGRhdGVDb2x1bW5DaG9vc2VyKGlzUHJlZmVyZW5jZTogYW55KTogdm9pZDtcclxuICAgIHJlc2V0RGF0YSgpOiB2b2lkO1xyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG4gICAgY29tcGFyZU9iamVjdHMobzE6IGFueSwgbzI6IGFueSk6IGJvb2xlYW47XHJcbn1cclxuIl19