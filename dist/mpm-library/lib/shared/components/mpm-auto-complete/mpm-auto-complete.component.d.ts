import { OnInit, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import * as ɵngcc0 from '@angular/core';
export declare class MpmAutoCompleteComponent implements OnInit, AfterViewInit {
    autoComplete: ElementRef;
    inputField: ElementRef;
    searchFieldConfig: any;
    required: any;
    autoFocus: any;
    valueChangeEvent: EventEmitter<any>;
    formControl: FormControl;
    filterOptions: Observable<any[]>;
    requiredField: boolean;
    selectedValue: {
        id: string;
        value: string;
    };
    constructor();
    filterValue(value: string): string[];
    displayFn(value?: any): string | undefined;
    onFocusOut(event: any): void;
    changeAutoComplete(event: any): void;
    mapInitialSelectedValue(): void;
    initializeSearchFields(): void;
    ngAfterViewInit(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<MpmAutoCompleteComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<MpmAutoCompleteComponent, "mpm-mpm-auto-complete", never, { "searchFieldConfig": "searchFieldConfig"; "required": "required"; "autoFocus": "autoFocus"; }, { "valueChangeEvent": "valueChangeEvent"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbIm1wbS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyLCBFbGVtZW50UmVmLCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE1wbUF1dG9Db21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgICBhdXRvQ29tcGxldGU6IEVsZW1lbnRSZWY7XHJcbiAgICBpbnB1dEZpZWxkOiBFbGVtZW50UmVmO1xyXG4gICAgc2VhcmNoRmllbGRDb25maWc6IGFueTtcclxuICAgIHJlcXVpcmVkOiBhbnk7XHJcbiAgICBhdXRvRm9jdXM6IGFueTtcclxuICAgIHZhbHVlQ2hhbmdlRXZlbnQ6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgZm9ybUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG4gICAgZmlsdGVyT3B0aW9uczogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgICByZXF1aXJlZEZpZWxkOiBib29sZWFuO1xyXG4gICAgc2VsZWN0ZWRWYWx1ZToge1xyXG4gICAgICAgIGlkOiBzdHJpbmc7XHJcbiAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgIH07XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgZmlsdGVyVmFsdWUodmFsdWU6IHN0cmluZyk6IHN0cmluZ1tdO1xyXG4gICAgZGlzcGxheUZuKHZhbHVlPzogYW55KTogc3RyaW5nIHwgdW5kZWZpbmVkO1xyXG4gICAgb25Gb2N1c091dChldmVudDogYW55KTogdm9pZDtcclxuICAgIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudDogYW55KTogdm9pZDtcclxuICAgIG1hcEluaXRpYWxTZWxlY3RlZFZhbHVlKCk6IHZvaWQ7XHJcbiAgICBpbml0aWFsaXplU2VhcmNoRmllbGRzKCk6IHZvaWQ7XHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19