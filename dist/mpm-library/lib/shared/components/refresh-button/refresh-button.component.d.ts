import { OnInit, EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class RefreshButtonComponent implements OnInit {
    disabled: any;
    clicked: EventEmitter<any>;
    constructor();
    refresh(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<RefreshButtonComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<RefreshButtonComponent, "mpm-refresh-button", never, { "disabled": "disabled"; }, { "clicked": "clicked"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmcmVzaC1idXR0b24uY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbInJlZnJlc2gtYnV0dG9uLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBSZWZyZXNoQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGRpc2FibGVkOiBhbnk7XHJcbiAgICBjbGlja2VkOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICByZWZyZXNoKCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==