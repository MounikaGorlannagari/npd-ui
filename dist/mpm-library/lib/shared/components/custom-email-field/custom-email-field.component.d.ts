import { OnInit, EventEmitter } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { AbstractControl } from '@angular/forms';
import { NotificationService } from '../../../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class CustomEmailFieldComponent implements OnInit {
    notification: NotificationService;
    emailFieldFormControl: AbstractControl;
    label: string;
    valueChanges: EventEmitter<any>;
    required: boolean;
    separatorKeysCodes: number[];
    emails: any[];
    disabled: boolean;
    mandatory: boolean;
    isIdExists: boolean;
    constructor(notification: NotificationService);
    checkIfEmailInString(text: any): boolean;
    addEmail(event: MatChipInputEvent): void;
    onRemoveEmail(email: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CustomEmailFieldComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CustomEmailFieldComponent, "mpm-custom-email-field", never, { "emailFieldFormControl": "emailFieldFormControl"; "label": "label"; "required": "required"; }, { "valueChanges": "valueChanges"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWVtYWlsLWZpZWxkLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJjdXN0b20tZW1haWwtZmllbGQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0Q2hpcElucHV0RXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jaGlwcyc7XHJcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEN1c3RvbUVtYWlsRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgZW1haWxGaWVsZEZvcm1Db250cm9sOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBsYWJlbDogc3RyaW5nO1xyXG4gICAgdmFsdWVDaGFuZ2VzOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHJlcXVpcmVkOiBib29sZWFuO1xyXG4gICAgc2VwYXJhdG9yS2V5c0NvZGVzOiBudW1iZXJbXTtcclxuICAgIGVtYWlsczogYW55W107XHJcbiAgICBkaXNhYmxlZDogYm9vbGVhbjtcclxuICAgIG1hbmRhdG9yeTogYm9vbGVhbjtcclxuICAgIGlzSWRFeGlzdHM6IGJvb2xlYW47XHJcbiAgICBjb25zdHJ1Y3Rvcihub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UpO1xyXG4gICAgY2hlY2tJZkVtYWlsSW5TdHJpbmcodGV4dDogYW55KTogYm9vbGVhbjtcclxuICAgIGFkZEVtYWlsKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQ7XHJcbiAgICBvblJlbW92ZUVtYWlsKGVtYWlsOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=