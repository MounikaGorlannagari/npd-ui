export interface ListFilter {
    value?: string;
    name?: string;
    count?: number;
    default?: boolean;
    searchCondition?: any;
    mapperName?: string;
    [x: string]: any;
}
