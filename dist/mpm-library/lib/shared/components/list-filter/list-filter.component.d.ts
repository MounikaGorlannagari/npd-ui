import { OnInit, EventEmitter, OnDestroy, SimpleChanges, OnChanges } from '@angular/core';
import { ListFilter } from './objects/ListFilter';
import * as ɵngcc0 from '@angular/core';
export declare class ListFilterComponent implements OnInit, OnDestroy, OnChanges {
    filters: Array<ListFilter>;
    selected: ListFilter;
    clicked: EventEmitter<void>;
    fitlerChanged: EventEmitter<ListFilter>;
    filterLengthRestricted: boolean;
    constructor();
    selectedFilter: ListFilter;
    onClick($event: Event): void;
    onFilterChange(currentFilter: ListFilter): void;
    handleDefaultSelection(): void;
    initialize(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ListFilterComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ListFilterComponent, "mpm-list-filter", never, { "filters": "filters"; "selected": "selected"; }, { "clicked": "clicked"; "fitlerChanged": "fitlerChanged"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1maWx0ZXIuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImxpc3QtZmlsdGVyLmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyLCBPbkRlc3Ryb3ksIFNpbXBsZUNoYW5nZXMsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBMaXN0RmlsdGVyIH0gZnJvbSAnLi9vYmplY3RzL0xpc3RGaWx0ZXInO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBMaXN0RmlsdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XHJcbiAgICBmaWx0ZXJzOiBBcnJheTxMaXN0RmlsdGVyPjtcclxuICAgIHNlbGVjdGVkOiBMaXN0RmlsdGVyO1xyXG4gICAgY2xpY2tlZDogRXZlbnRFbWl0dGVyPHZvaWQ+O1xyXG4gICAgZml0bGVyQ2hhbmdlZDogRXZlbnRFbWl0dGVyPExpc3RGaWx0ZXI+O1xyXG4gICAgZmlsdGVyTGVuZ3RoUmVzdHJpY3RlZDogYm9vbGVhbjtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICBzZWxlY3RlZEZpbHRlcjogTGlzdEZpbHRlcjtcclxuICAgIG9uQ2xpY2soJGV2ZW50OiBFdmVudCk6IHZvaWQ7XHJcbiAgICBvbkZpbHRlckNoYW5nZShjdXJyZW50RmlsdGVyOiBMaXN0RmlsdGVyKTogdm9pZDtcclxuICAgIGhhbmRsZURlZmF1bHRTZWxlY3Rpb24oKTogdm9pZDtcclxuICAgIGluaXRpYWxpemUoKTogdm9pZDtcclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQ7XHJcbn1cclxuIl19