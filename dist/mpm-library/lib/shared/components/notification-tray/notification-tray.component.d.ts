import { OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UtilService } from '../../../mpm-utils/services/util.service';
import * as moment_ from 'moment';
import { LoaderService } from '../../../loader/loader.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { DatePipe } from '@angular/common';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class NotificationTrayComponent implements OnInit, OnChanges {
    utilService: UtilService;
    loaderService: LoaderService;
    appService: AppService;
    datePipe: DatePipe;
    sharingService: SharingService;
    router: Router;
    notificationService: NotificationService;
    notifiedCount: number;
    notificationData: any[];
    todayNotification: any[];
    yesterdayNotification: any[];
    pastNotification: any[];
    hasUnreadNotification: any;
    skip: number;
    top: number;
    canLoadMore: boolean;
    constructor(utilService: UtilService, loaderService: LoaderService, appService: AppService, datePipe: DatePipe, sharingService: SharingService, router: Router, notificationService: NotificationService);
    ngOnChanges(changes: SimpleChanges): void;
    setNotificationData(): void;
    getDaysBetweenTwoDates(startDate: Date, endDate: Date, diffType: moment_.unitOfTime.Diff): number;
    getDateByGiveFormat(dateObject: Date, formatValue: string): string;
    handleDateManipulation(dateString: any): string;
    formNavigationUrl(notificationContent: any): void;
    clearNotification(notificationContent: any): void;
    markReadUnreadNotification(notificationContent: any): void;
    markAsReadNotification(notificationContent: any): boolean;
    clearAllNotification(): void;
    markAllAsReadNotification(): void;
    updateNotifiedCount(): void;
    loadData(): void;
    refresh(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NotificationTrayComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NotificationTrayComponent, "mpm-notification-tray", never, {}, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbIm5vdGlmaWNhdGlvbi10cmF5LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25DaGFuZ2VzLCBPbkluaXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE5vdGlmaWNhdGlvblRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICB1dGlsU2VydmljZTogVXRpbFNlcnZpY2U7XHJcbiAgICBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlO1xyXG4gICAgYXBwU2VydmljZTogQXBwU2VydmljZTtcclxuICAgIGRhdGVQaXBlOiBEYXRlUGlwZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHJvdXRlcjogUm91dGVyO1xyXG4gICAgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIG5vdGlmaWVkQ291bnQ6IG51bWJlcjtcclxuICAgIG5vdGlmaWNhdGlvbkRhdGE6IGFueVtdO1xyXG4gICAgdG9kYXlOb3RpZmljYXRpb246IGFueVtdO1xyXG4gICAgeWVzdGVyZGF5Tm90aWZpY2F0aW9uOiBhbnlbXTtcclxuICAgIHBhc3ROb3RpZmljYXRpb246IGFueVtdO1xyXG4gICAgaGFzVW5yZWFkTm90aWZpY2F0aW9uOiBhbnk7XHJcbiAgICBza2lwOiBudW1iZXI7XHJcbiAgICB0b3A6IG51bWJlcjtcclxuICAgIGNhbkxvYWRNb3JlOiBib29sZWFuO1xyXG4gICAgY29uc3RydWN0b3IodXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBkYXRlUGlwZTogRGF0ZVBpcGUsIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSwgcm91dGVyOiBSb3V0ZXIsIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UpO1xyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQ7XHJcbiAgICBzZXROb3RpZmljYXRpb25EYXRhKCk6IHZvaWQ7XHJcbiAgICBnZXREYXlzQmV0d2VlblR3b0RhdGVzKHN0YXJ0RGF0ZTogRGF0ZSwgZW5kRGF0ZTogRGF0ZSwgZGlmZlR5cGU6IG1vbWVudF8udW5pdE9mVGltZS5EaWZmKTogbnVtYmVyO1xyXG4gICAgZ2V0RGF0ZUJ5R2l2ZUZvcm1hdChkYXRlT2JqZWN0OiBEYXRlLCBmb3JtYXRWYWx1ZTogc3RyaW5nKTogc3RyaW5nO1xyXG4gICAgaGFuZGxlRGF0ZU1hbmlwdWxhdGlvbihkYXRlU3RyaW5nOiBhbnkpOiBzdHJpbmc7XHJcbiAgICBmb3JtTmF2aWdhdGlvblVybChub3RpZmljYXRpb25Db250ZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgY2xlYXJOb3RpZmljYXRpb24obm90aWZpY2F0aW9uQ29udGVudDogYW55KTogdm9pZDtcclxuICAgIG1hcmtSZWFkVW5yZWFkTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbkNvbnRlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICBtYXJrQXNSZWFkTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbkNvbnRlbnQ6IGFueSk6IGJvb2xlYW47XHJcbiAgICBjbGVhckFsbE5vdGlmaWNhdGlvbigpOiB2b2lkO1xyXG4gICAgbWFya0FsbEFzUmVhZE5vdGlmaWNhdGlvbigpOiB2b2lkO1xyXG4gICAgdXBkYXRlTm90aWZpZWRDb3VudCgpOiB2b2lkO1xyXG4gICAgbG9hZERhdGEoKTogdm9pZDtcclxuICAgIHJlZnJlc2goKTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19