import { OnInit, EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class CustomMetadataFieldComponent implements OnInit {
    metadataFieldFormControl: any;
    valueChanges: EventEmitter<any>;
    label: string;
    datatype: string;
    requiredField: boolean;
    keyRestriction: {
        NUMBER: number[];
    };
    constructor();
    valueChange(event: any): void;
    refreshField(): void;
    restrictKeysOnType(event: any, datatype: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<CustomMetadataFieldComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<CustomMetadataFieldComponent, "mpm-custom-metadata-field", never, { "metadataFieldFormControl": "metadataFieldFormControl"; }, { "valueChanges": "valueChanges"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLW1ldGFkYXRhLWZpZWxkLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJjdXN0b20tbWV0YWRhdGEtZmllbGQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDdXN0b21NZXRhZGF0YUZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIG1ldGFkYXRhRmllbGRGb3JtQ29udHJvbDogYW55O1xyXG4gICAgdmFsdWVDaGFuZ2VzOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGxhYmVsOiBzdHJpbmc7XHJcbiAgICBkYXRhdHlwZTogc3RyaW5nO1xyXG4gICAgcmVxdWlyZWRGaWVsZDogYm9vbGVhbjtcclxuICAgIGtleVJlc3RyaWN0aW9uOiB7XHJcbiAgICAgICAgTlVNQkVSOiBudW1iZXJbXTtcclxuICAgIH07XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgdmFsdWVDaGFuZ2UoZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICByZWZyZXNoRmllbGQoKTogdm9pZDtcclxuICAgIHJlc3RyaWN0S2V5c09uVHlwZShldmVudDogYW55LCBkYXRhdHlwZTogYW55KTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19