import { OnInit } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class NameIconComponent implements OnInit {
    userName: any;
    color: any;
    charCount: any;
    userProfileCharacter: any;
    constructor();
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<NameIconComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<NameIconComponent, "mpm-name-icon", never, { "color": "color"; "userName": "userName"; "charCount": "charCount"; }, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmFtZS1pY29uLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJuYW1lLWljb24uY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIE5hbWVJY29uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHVzZXJOYW1lOiBhbnk7XHJcbiAgICBjb2xvcjogYW55O1xyXG4gICAgY2hhckNvdW50OiBhbnk7XHJcbiAgICB1c2VyUHJvZmlsZUNoYXJhY3RlcjogYW55O1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19