import { Options } from './options';
export interface NewProjectType {
    appDynamicConfigId: string;
    defaultValue: string;
    fieldName: string;
    inputType: string;
    isShown: boolean;
    label: string;
    options: Options;
    placeholder: string;
}
