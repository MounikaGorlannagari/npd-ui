export declare const ApplicationConfigConstants: {
    MPM_PROJECT_CONFIG: {
        ENABLE_PROJECT_TEMPLATE_CREATION: string;
        ENABLE_CREATE_PROJECT: string;
        ENABLE_CREATE_TEMPLATE: string;
        ENABLE_COPY_PROJECT: string;
        SHOW_TEMPLATE: string;
        SHOW_STANDARD_STATUS: string;
        SHOW_REVISION_REVIEW_REQUIRED: string;
        ENABLE_NEED_REVIEW: string;
        DEFAULT_REVISION_REQUIRED_STATUS: string;
        PROJECT_THUMBNAIL_TYPE: string;
        ENABLE_CREATE_CAMPAIGN: string;
        ENABLE_REVIEW_APPROVAL: string;
        ENABLE_DURATION: string;
        ENABLE_WORK_WEEK: string;
        ENABLE_DUPLICATE_NAMES: string;
    };
    MPM_APP_CONFIG: {
        DEFAULT_PROJECT_VIEW: string;
        ENABLE_DUPLICATE_DELIVERABLES: string;
        DEFAULT_TASK_VIEW: string;
        SHOW_TASK_SLA: string;
        SHOW_REMINDER_OPTION: string;
        EXTERNAL_APP_ACCESS: string;
        OPEN_APP_IN_NEW_TAB: string;
        SHOW_MY_APPLICATION: string;
        DEFAULT_MYTASK_VIEW_COLLAPSED: string;
        NAME_STRING_PATTERN: string;
        HIGHLIGHT_HIGH_PRIORITY_PROJECT: string;
        SHOW_PROJECT_LOGO: string;
        UPDATE_PROJECT_LOGO: string;
        ONLINE_PROOFING_TOOL: string;
        ENABLE_ASSET_DOWNLOAD: string;
        DISABLE_GRID_VIEW: string;
        ENABLE_DELEGATION: string;
        ENABLE_REVOKE: string;
        ENABLE_PROJECT_TRANSFER: string;
        ENABLE_SKIP_TASK: string;
        ENABLE_SYSTEM_NOTIFICATION: string;
        OVERRIDE_APPROVAL_DECISION: string;
        OVERRIDE_UPLOAD_ASSET: string;
        DEFAULT_PAGINATION: string;
        DEFAULT_SORT_ORDER: string;
        ALLOW_ACTION_TASK: string;
        ENABLE_CAMPAIGN: string;
        ENABLE_REFERENCE_ASSET: string;
        ENABLE_CUSTOM_WORKFLOW: string;
        ENABLE_PROJECT_RESTART: string;
        EXPORT_DATA_FILE_PATH: string;
        EXPORT_DATA_PAGE_LIMIT: string;
        EXPORT_DATA_MIN_COUNT: string;
    };
    MPM_ASSET_CONFIG: {
        MAX_NO_OF_FILES: string;
        MAX_FILE_SIZE: string;
        ALLOWED_FILE_TYPES: string;
    };
    MPM_BRAND_CONFIG: {
        FAV_ICON_PATH: string;
        APP_LOGO_PATH: string;
        APP_THEME_NAME: string;
        DEFAULT_BRANDING_STYLE_PATH: string;
        SHOW_COPYRIGHT: string;
    };
    MPM_COMMENTS_CONFIG: {
        SHOW_EXTERNAL_INTERNAL_COMMENT_SWITCH: string;
        DEFAULT_COMMENT: string;
        EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL: string;
        COMMENTS_ITEM_PER_PAGE: string;
        EXTERNAL_COMMENT_LABEL: string;
        INTERNAL_COMMENT_LABEL: string;
        ENABLE_COMMENT_NOTIFICATION: string;
    };
    OTMM_SYSTEM_DETAILS_CONSTANTS: {
        'BUILD_VERSION': string;
    };
};
