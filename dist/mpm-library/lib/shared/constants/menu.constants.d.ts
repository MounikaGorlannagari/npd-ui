export declare const MenuConstants: {
    PROJECT_MANAGEMENT: string;
    OVERVIEW: string;
    PROJECTS: string;
    TASKS: string;
    DELIVERABLES: string;
    ASSETS: string;
    COMMENTS: string;
    DELIVERABLE_TASK_VIEW: string;
    RESOURCE_MANAGEMENT: string;
};
