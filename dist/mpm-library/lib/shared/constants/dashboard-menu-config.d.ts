import { NewProjectType } from './project-type';
import { ViewConfig } from '../../mpm-utils/objects/ViewConfig';
export interface DashboardMenuConfig {
    'FILTER_LIST': any;
    'IS_PROJECT_TYPE': boolean;
    'NEW_PROJECT_TYPES': NewProjectType;
    'SELECTED_SORT': any;
    'IS_LIST_VIEW': boolean;
    'IS_LIST_VIEW_CHANGE': boolean;
    'DASHBOARD_CONTROL_FILTER_LIST': any;
    'isBulk': boolean;
    'selectedViewByName': string;
    'projectViewConfig': ViewConfig;
    'IS_CAMPAIGN_TYPE': boolean;
    'campaignViewConfig': ViewConfig;
}
