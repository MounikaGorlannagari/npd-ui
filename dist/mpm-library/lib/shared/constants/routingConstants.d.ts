export declare const RoutingConstants: {
    mpmBaseRoute: string;
    prjectView: string;
    projectViewBaseRoute: string;
    campaignView: string;
    campaignViewBaseRoute: string;
    projectManagement: string;
    deliverableWorkView: string;
    noAccess: string;
    resourceManagement: string;
};
