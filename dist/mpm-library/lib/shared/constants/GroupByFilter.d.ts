import { OTMMMPMDataTypes } from "../../mpm-utils/objects/OTMMMPMDataTypes";
import { CustomSelect } from "../components/custom-select/objects/CustomSelectObject";
import { GroupByFilterTypes } from "./GroupByFilterTypes";
export declare let GROUP_BY_DELIVERABLE_FILTER: {
    value: GroupByFilterTypes;
    name: string;
    default: boolean;
    searchCondition: {
        type: string;
        metadata_field_id: string;
        relational_operator_id: string;
        relational_operator_name: string;
        value: OTMMMPMDataTypes;
        relational_operator: string;
    }[];
};
export declare let GROUP_BY_FILTERS: Array<CustomSelect>;
export declare let GROUP_BY_ALL_FILTERS: Array<CustomSelect>;
