export declare const SaveOptionConstant: {
    deliverableSaveOptions: {
        name: string;
        value: string;
    }[];
    taskSaveOptions: {
        name: string;
        value: string;
    }[];
    templateSaveOptions: {
        name: string;
        value: string;
    }[];
};
