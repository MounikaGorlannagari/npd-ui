import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { OTMMDataTypes } from '../../mpm-utils/objects/OTMMDataTypes';
import * as ɵngcc0 from '@angular/core';
export declare class OtmmMetadataService {
    appService: AppService;
    otmmService: OTMMService;
    METADATA_MAPPING_METHOD_NS: string;
    GET_METADATA_MAPPING_WS_METHOD_NAME: string;
    metaDataFieldDataTypesMapping: {
        CHAR: string;
        DATE: OTMMDataTypes;
        NUMBER: string;
    };
    defaultProjectMetadataFields: any[];
    customProjectMetadataFields: any[];
    defaultTaskMetadataFields: any[];
    customTaskMetadataFields: any[];
    defaultDeliverableMetadataFields: any[];
    customDeliverableMetadataFields: any[];
    constructor(appService: AppService, otmmService: OTMMService);
    setDefaultProjectMetadataFields(metadataFields: any): void;
    getDefaultProjectMetadataFields(): any[];
    setCustomProjectMetadataFields(metadataFields: any): void;
    getCustomProjectMetadataFields(): any[];
    setDefaultTaskMetadataFields(metadataFields: any): void;
    getDefaultTaskMetadataFields(): any[];
    setCustomTaskMetadataFields(metadataFields: any): void;
    getCustomTaskMetadataFields(): any[];
    setDefaultDeliverableMetadataFields(metadataFields: any): void;
    getDefaultDeliverableMetadataFields(): any[];
    setCustomDeliverableMetadataFields(metadataFields: any): void;
    getCustomDeliverableMetadataFields(): any[];
    getMetadataFieldType(metadatModelFieldDataType: any): any;
    getMetaData(categoryId: any, categoryLevelName: any): Observable<any>;
    getFieldValueById(metaDataValues: any, fieldId: any, isDisplayValue?: any): string;
    getFieldValue(selectedMetadataField: any, isDisplayValue?: any): string;
    /**
     * @author SathishSrinivas
     * @param assetList
     * @param metadataList
     */
    setReferenceValueForMetadataFeilds(assetList: Array<any>, metadataList: Array<any>): Array<any>;
    /**
     * @author SathishSrinivas
     * @param metadataFieldList
     * @param metadataFilterList
     * @param isDefaultMetadataField
     */
    getDisplayMetadataFields(metadataFieldList: Array<any>, metadataFilterList: Array<any>, isDefaultMetadataField: boolean): any[];
    static ɵfac: ɵngcc0.ɵɵFactoryDef<OtmmMetadataService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RtbS1tZXRhZGF0YS5zZXJ2aWNlLmQudHMiLCJzb3VyY2VzIjpbIm90bW0tbWV0YWRhdGEuc2VydmljZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1EYXRhVHlwZXMnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBPdG1tTWV0YWRhdGFTZXJ2aWNlIHtcclxuICAgIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBNRVRBREFUQV9NQVBQSU5HX01FVEhPRF9OUzogc3RyaW5nO1xyXG4gICAgR0VUX01FVEFEQVRBX01BUFBJTkdfV1NfTUVUSE9EX05BTUU6IHN0cmluZztcclxuICAgIG1ldGFEYXRhRmllbGREYXRhVHlwZXNNYXBwaW5nOiB7XHJcbiAgICAgICAgQ0hBUjogc3RyaW5nO1xyXG4gICAgICAgIERBVEU6IE9UTU1EYXRhVHlwZXM7XHJcbiAgICAgICAgTlVNQkVSOiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgZGVmYXVsdFByb2plY3RNZXRhZGF0YUZpZWxkczogYW55W107XHJcbiAgICBjdXN0b21Qcm9qZWN0TWV0YWRhdGFGaWVsZHM6IGFueVtdO1xyXG4gICAgZGVmYXVsdFRhc2tNZXRhZGF0YUZpZWxkczogYW55W107XHJcbiAgICBjdXN0b21UYXNrTWV0YWRhdGFGaWVsZHM6IGFueVtdO1xyXG4gICAgZGVmYXVsdERlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHM6IGFueVtdO1xyXG4gICAgY3VzdG9tRGVsaXZlcmFibGVNZXRhZGF0YUZpZWxkczogYW55W107XHJcbiAgICBjb25zdHJ1Y3RvcihhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UpO1xyXG4gICAgc2V0RGVmYXVsdFByb2plY3RNZXRhZGF0YUZpZWxkcyhtZXRhZGF0YUZpZWxkczogYW55KTogdm9pZDtcclxuICAgIGdldERlZmF1bHRQcm9qZWN0TWV0YWRhdGFGaWVsZHMoKTogYW55W107XHJcbiAgICBzZXRDdXN0b21Qcm9qZWN0TWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZHM6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRDdXN0b21Qcm9qZWN0TWV0YWRhdGFGaWVsZHMoKTogYW55W107XHJcbiAgICBzZXREZWZhdWx0VGFza01ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0RGVmYXVsdFRhc2tNZXRhZGF0YUZpZWxkcygpOiBhbnlbXTtcclxuICAgIHNldEN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcyhtZXRhZGF0YUZpZWxkczogYW55KTogdm9pZDtcclxuICAgIGdldEN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcygpOiBhbnlbXTtcclxuICAgIHNldERlZmF1bHREZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0RGVmYXVsdERlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHMoKTogYW55W107XHJcbiAgICBzZXRDdXN0b21EZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0Q3VzdG9tRGVsaXZlcmFibGVNZXRhZGF0YUZpZWxkcygpOiBhbnlbXTtcclxuICAgIGdldE1ldGFkYXRhRmllbGRUeXBlKG1ldGFkYXRNb2RlbEZpZWxkRGF0YVR5cGU6IGFueSk6IGFueTtcclxuICAgIGdldE1ldGFEYXRhKGNhdGVnb3J5SWQ6IGFueSwgY2F0ZWdvcnlMZXZlbE5hbWU6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldEZpZWxkVmFsdWVCeUlkKG1ldGFEYXRhVmFsdWVzOiBhbnksIGZpZWxkSWQ6IGFueSwgaXNEaXNwbGF5VmFsdWU/OiBhbnkpOiBzdHJpbmc7XHJcbiAgICBnZXRGaWVsZFZhbHVlKHNlbGVjdGVkTWV0YWRhdGFGaWVsZDogYW55LCBpc0Rpc3BsYXlWYWx1ZT86IGFueSk6IHN0cmluZztcclxuICAgIC8qKlxyXG4gICAgICogQGF1dGhvciBTYXRoaXNoU3Jpbml2YXNcclxuICAgICAqIEBwYXJhbSBhc3NldExpc3RcclxuICAgICAqIEBwYXJhbSBtZXRhZGF0YUxpc3RcclxuICAgICAqL1xyXG4gICAgc2V0UmVmZXJlbmNlVmFsdWVGb3JNZXRhZGF0YUZlaWxkcyhhc3NldExpc3Q6IEFycmF5PGFueT4sIG1ldGFkYXRhTGlzdDogQXJyYXk8YW55Pik6IEFycmF5PGFueT47XHJcbiAgICAvKipcclxuICAgICAqIEBhdXRob3IgU2F0aGlzaFNyaW5pdmFzXHJcbiAgICAgKiBAcGFyYW0gbWV0YWRhdGFGaWVsZExpc3RcclxuICAgICAqIEBwYXJhbSBtZXRhZGF0YUZpbHRlckxpc3RcclxuICAgICAqIEBwYXJhbSBpc0RlZmF1bHRNZXRhZGF0YUZpZWxkXHJcbiAgICAgKi9cclxuICAgIGdldERpc3BsYXlNZXRhZGF0YUZpZWxkcyhtZXRhZGF0YUZpZWxkTGlzdDogQXJyYXk8YW55PiwgbWV0YWRhdGFGaWx0ZXJMaXN0OiBBcnJheTxhbnk+LCBpc0RlZmF1bHRNZXRhZGF0YUZpZWxkOiBib29sZWFuKTogYW55W107XHJcbn1cclxuIl19