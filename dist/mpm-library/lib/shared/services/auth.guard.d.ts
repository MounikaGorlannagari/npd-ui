import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AuthenticationService } from '../../mpm-utils/services/authentication.service';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoaderService } from '../../loader/loader.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MPMMenu } from '../../mpm-utils/objects/Menu';
import { OTDSTicketService } from '../../mpm-utils/services/OTDSTicket.service';
import * as ɵngcc0 from '@angular/core';
export declare class AuthGuard {
    entityAppDefService: EntityAppDefService;
    utilServices: UtilService;
    authenticationService: AuthenticationService;
    loaderService: LoaderService;
    otmmService: OTMMService;
    appRouteService: AppRouteService;
    appService: AppService;
    sharingService: SharingService;
    otdsTicketService: OTDSTicketService;
    router: Router;
    otmmSessionSubscription: import("rxjs").Subscription;
    constructor(entityAppDefService: EntityAppDefService, utilServices: UtilService, authenticationService: AuthenticationService, loaderService: LoaderService, otmmService: OTMMService, appRouteService: AppRouteService, appService: AppService, sharingService: SharingService, otdsTicketService: OTDSTicketService, router: Router);
    isDataAlradyLoaded: boolean;
    appItems: any[];
    MENU_HEADER_TYPE: string;
    MENU_TYPE: string;
    menus: Array<MPMMenu>;
    iconToAccessMPM: boolean;
    allApplicationItems: any;
    rolesForUser: any[];
    APP_DEF_PATH: string;
    userName: any;
    clearLocalStorage(): void;
    goToLogin(): void;
    loadApp(): Observable<unknown>;
    initLogging(): Observable<unknown>;
    storeLastVisitLink(): void;
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AuthGuard, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5kLnRzIiwic291cmNlcyI6WyJhdXRoLmd1YXJkLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90LCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNTWVudSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01lbnUnO1xyXG5pbXBvcnQgeyBPVERTVGlja2V0U2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9PVERTVGlja2V0LnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBdXRoR3VhcmQge1xyXG4gICAgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlczogVXRpbFNlcnZpY2U7XHJcbiAgICBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZTtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBhcHBSb3V0ZVNlcnZpY2U6IEFwcFJvdXRlU2VydmljZTtcclxuICAgIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2U7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBvdGRzVGlja2V0U2VydmljZTogT1REU1RpY2tldFNlcnZpY2U7XHJcbiAgICByb3V0ZXI6IFJvdXRlcjtcclxuICAgIG90bW1TZXNzaW9uU3Vic2NyaXB0aW9uOiBpbXBvcnQoXCJyeGpzXCIpLlN1YnNjcmlwdGlvbjtcclxuICAgIGNvbnN0cnVjdG9yKGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsIHV0aWxTZXJ2aWNlczogVXRpbFNlcnZpY2UsIGF1dGhlbnRpY2F0aW9uU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIGFwcFJvdXRlU2VydmljZTogQXBwUm91dGVTZXJ2aWNlLCBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIG90ZHNUaWNrZXRTZXJ2aWNlOiBPVERTVGlja2V0U2VydmljZSwgcm91dGVyOiBSb3V0ZXIpO1xyXG4gICAgaXNEYXRhQWxyYWR5TG9hZGVkOiBib29sZWFuO1xyXG4gICAgYXBwSXRlbXM6IGFueVtdO1xyXG4gICAgTUVOVV9IRUFERVJfVFlQRTogc3RyaW5nO1xyXG4gICAgTUVOVV9UWVBFOiBzdHJpbmc7XHJcbiAgICBtZW51czogQXJyYXk8TVBNTWVudT47XHJcbiAgICBpY29uVG9BY2Nlc3NNUE06IGJvb2xlYW47XHJcbiAgICBhbGxBcHBsaWNhdGlvbkl0ZW1zOiBhbnk7XHJcbiAgICByb2xlc0ZvclVzZXI6IGFueVtdO1xyXG4gICAgQVBQX0RFRl9QQVRIOiBzdHJpbmc7XHJcbiAgICB1c2VyTmFtZTogYW55O1xyXG4gICAgY2xlYXJMb2NhbFN0b3JhZ2UoKTogdm9pZDtcclxuICAgIGdvVG9Mb2dpbigpOiB2b2lkO1xyXG4gICAgbG9hZEFwcCgpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgaW5pdExvZ2dpbmcoKTogT2JzZXJ2YWJsZTx1bmtub3duPjtcclxuICAgIHN0b3JlTGFzdFZpc2l0TGluaygpOiB2b2lkO1xyXG4gICAgY2FuQWN0aXZhdGUobmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgYm9vbGVhbjtcclxufVxyXG4iXX0=