import { SelectionModel } from '@angular/cdk/collections';
import * as ɵngcc0 from '@angular/core';
export declare class ProjectBulkCountService {
    projectId: any[];
    projectDetails: any[];
    selectedProjects: SelectionModel<any>;
    constructor();
    getProjectBulkCount(projectData: any): any[];
    getProjectBulkData(projectData: any): SelectionModel<any>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ProjectBulkCountService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsicHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgUHJvamVjdEJ1bGtDb3VudFNlcnZpY2Uge1xyXG4gICAgcHJvamVjdElkOiBhbnlbXTtcclxuICAgIHByb2plY3REZXRhaWxzOiBhbnlbXTtcclxuICAgIHNlbGVjdGVkUHJvamVjdHM6IFNlbGVjdGlvbk1vZGVsPGFueT47XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgZ2V0UHJvamVjdEJ1bGtDb3VudChwcm9qZWN0RGF0YTogYW55KTogYW55W107XHJcbiAgICBnZXRQcm9qZWN0QnVsa0RhdGEocHJvamVjdERhdGE6IGFueSk6IFNlbGVjdGlvbk1vZGVsPGFueT47XHJcbn1cclxuIl19