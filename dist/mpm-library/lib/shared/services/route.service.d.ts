import { Router } from '@angular/router';
import * as ɵngcc0 from '@angular/core';
export declare class RouteService {
    router: Router;
    routingConstants: {
        mpmBaseRoute: string;
        prjectView: string;
        projectViewBaseRoute: string;
        campaignView: string;
        campaignViewBaseRoute: string;
        projectManagement: string;
        deliverableWorkView: string;
        noAccess: string;
        resourceManagement: string;
    };
    constructor(router: Router);
    goToResourceManagement(queryParams: any, searchName: any, savedSearchName: any, advSearchData: any, facetData: any): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<RouteService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJyb3V0ZS5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgUm91dGVTZXJ2aWNlIHtcclxuICAgIHJvdXRlcjogUm91dGVyO1xyXG4gICAgcm91dGluZ0NvbnN0YW50czoge1xyXG4gICAgICAgIG1wbUJhc2VSb3V0ZTogc3RyaW5nO1xyXG4gICAgICAgIHByamVjdFZpZXc6IHN0cmluZztcclxuICAgICAgICBwcm9qZWN0Vmlld0Jhc2VSb3V0ZTogc3RyaW5nO1xyXG4gICAgICAgIGNhbXBhaWduVmlldzogc3RyaW5nO1xyXG4gICAgICAgIGNhbXBhaWduVmlld0Jhc2VSb3V0ZTogc3RyaW5nO1xyXG4gICAgICAgIHByb2plY3RNYW5hZ2VtZW50OiBzdHJpbmc7XHJcbiAgICAgICAgZGVsaXZlcmFibGVXb3JrVmlldzogc3RyaW5nO1xyXG4gICAgICAgIG5vQWNjZXNzOiBzdHJpbmc7XHJcbiAgICAgICAgcmVzb3VyY2VNYW5hZ2VtZW50OiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgY29uc3RydWN0b3Iocm91dGVyOiBSb3V0ZXIpO1xyXG4gICAgZ29Ub1Jlc291cmNlTWFuYWdlbWVudChxdWVyeVBhcmFtczogYW55LCBzZWFyY2hOYW1lOiBhbnksIHNhdmVkU2VhcmNoTmFtZTogYW55LCBhZHZTZWFyY2hEYXRhOiBhbnksIGZhY2V0RGF0YTogYW55KTogdm9pZDtcclxufVxyXG4iXX0=