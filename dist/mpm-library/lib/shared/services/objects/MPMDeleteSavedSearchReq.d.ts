export interface MPMDeleteSavedSearchReq {
    MPM_Saved_Searches: {
        'MPM_Saved_Searches-id': {
            Id: string;
        };
    };
}
