export interface MPMSearchOperator {
    'MPM_Advanced_Search_Operators-id': {
        Id: string;
        ItemId: string;
    };
    DISPLAY_SEQUENCE: number;
    NAME: string;
    DATA_TYPE: string;
    OPERATOR_ID: string;
    VALUE_COUNT: number;
}
