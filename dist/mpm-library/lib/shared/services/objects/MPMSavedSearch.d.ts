import { SearchConditionList } from '../../../search/objects/MPMSavedSearchCreate';
export interface MPMSavedSearch {
    'MPM_Saved_Searches-id': {
        Id: string;
        ItemId: string;
    };
    DEFAULT_OPERATOR: string;
    IS_ROLE_SPECIFIC: boolean;
    VIEW_NAME: string;
    TYPE: string;
    ROLE_ID: string;
    DESCRIPTION: string;
    IS_SYSTEM_DEFAULT_SEARCH: boolean;
    IS_PUBLIC: boolean;
    SEARCH_DATA: SearchConditionList;
    NAME: string;
    Tracking: {
        CreatedBy: {
            Id: string;
            ItemId: string;
        };
    };
}
