import * as ɵngcc0 from '@angular/core';
export declare class ScriptService {
    scripts: any;
    constructor();
    load(...scripts: string[]): Promise<any[]>;
    loadScript(name: string): Promise<unknown>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ScriptService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NyaXB0LnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsic2NyaXB0LnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWNsYXJlIGNsYXNzIFNjcmlwdFNlcnZpY2Uge1xyXG4gICAgc2NyaXB0czogYW55O1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIGxvYWQoLi4uc2NyaXB0czogc3RyaW5nW10pOiBQcm9taXNlPGFueVtdPjtcclxuICAgIGxvYWRTY3JpcHQobmFtZTogc3RyaW5nKTogUHJvbWlzZTx1bmtub3duPjtcclxufVxyXG4iXX0=