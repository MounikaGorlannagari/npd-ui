export interface Cursor {
    page_index: number;
    page_size: number;
}
