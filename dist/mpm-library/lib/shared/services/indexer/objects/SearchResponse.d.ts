export interface SearchResponse {
    data: Array<any>;
    facet?: {
        facet_queries: any;
        facet_fields: Array<any>;
        facet_ranges: Array<any>;
        facet_intervals: Array<any>;
    };
    facet_field_response_list?: Array<any>;
    cursor: {
        page_index: number;
        page_size: number;
        total_records: number;
    };
}
