import { SearchCondition } from './SearchCondition';
import { Sort } from './Sort';
import { Cursor } from './Cursor';
import { FacetConditionList } from '../../../../facet/objects/MPMFacetConditionList';
export interface SearchRequest {
    search_config_id: number;
    keyword: string;
    grouping?: {
        group_by: string;
        group_from_type: string;
        search_condition_list?: {
            search_condition: SearchCondition[];
        };
    };
    search_condition_list?: {
        search_condition: SearchCondition[];
    };
    displayable_field_list?: {
        displayable_fields: any;
    };
    facet_condition_list?: FacetConditionList;
    sorting_list?: {
        sort: Sort[];
    };
    cursor: Cursor;
}
