export declare enum IndexerEndPoints {
    base = "/mpm-indexer",
    search = "/search"
}
