export interface Sort {
    field_id: string;
    order: 'ASC' | 'DESC';
}
