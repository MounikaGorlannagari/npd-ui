export declare enum IndexerDataTypes {
    DATETIME = "dateTime",
    BOOLEAN = "boolean",
    STRING = "string",
    NUMBER = "number",
    DECIMAL = "decimal"
}
export declare type IndexerDataType = IndexerDataTypes.DATETIME | IndexerDataTypes.BOOLEAN | IndexerDataTypes.DECIMAL | IndexerDataTypes.NUMBER | IndexerDataTypes.STRING | '';
