export interface MPMIndexerConfig {
    'MPM_Indexer_Config-id': {
        Id: string;
        ItemId: string;
    };
    R_PO_CATEGORY: {
        'MPM_Category-id': {
            Id: string;
            ItemId: string;
        };
    };
    URL: string;
    SEARCH_URL: string;
    CORE_NAME: string;
}
