export interface SearchCondition {
    type: string;
    field_id: string;
    relational_operator_id: string;
    relational_operator_name: string;
    value?: any;
    relational_operator: string;
}
