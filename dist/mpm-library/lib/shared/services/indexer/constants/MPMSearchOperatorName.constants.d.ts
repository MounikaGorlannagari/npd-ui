export declare enum MPMSearchOperatorNames {
    OR = "or",
    AND = "and",
    IS_NOT = "is not",
    IS_NOT_EMPTY = "is not empty",
    IS = "is",
    IS_BETWEEN = "is between",
    IS_ON_OR_BEFORE = "is on or before",
    IS_ON_OR_AFTER = "is on or after"
}
