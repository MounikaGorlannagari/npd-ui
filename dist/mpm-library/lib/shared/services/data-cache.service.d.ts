import { Observable } from 'rxjs';
import { MPM_LEVEL } from '../../mpm-utils/objects/Level';
import { Status } from '../../mpm-utils/objects/Status';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusCache } from '../object/StatusCache';
import { StatusService } from './status.service';
import * as ɵngcc0 from '@angular/core';
export declare class DataCacheService {
    private statusService;
    private sharingservice;
    constructor(statusService: StatusService, sharingservice: SharingService);
    statusCache: StatusCache;
    cachedListDependentFilter: any;
    status: any;
    getStatusbyLevel(level: MPM_LEVEL): Observable<Array<Status> | null>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DataCacheService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jYWNoZS5zZXJ2aWNlLmQudHMiLCJzb3VyY2VzIjpbImRhdGEtY2FjaGUuc2VydmljZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c0NhY2hlIH0gZnJvbSAnLi4vb2JqZWN0L1N0YXR1c0NhY2hlJztcclxuaW1wb3J0IHsgU3RhdHVzU2VydmljZSB9IGZyb20gJy4vc3RhdHVzLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBEYXRhQ2FjaGVTZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgc3RhdHVzU2VydmljZTtcclxuICAgIHByaXZhdGUgc2hhcmluZ3NlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3RvcihzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLCBzaGFyaW5nc2VydmljZTogU2hhcmluZ1NlcnZpY2UpO1xyXG4gICAgc3RhdHVzQ2FjaGU6IFN0YXR1c0NhY2hlO1xyXG4gICAgY2FjaGVkTGlzdERlcGVuZGVudEZpbHRlcjogYW55O1xyXG4gICAgc3RhdHVzOiBhbnk7XHJcbiAgICBnZXRTdGF0dXNieUxldmVsKGxldmVsOiBNUE1fTEVWRUwpOiBPYnNlcnZhYmxlPEFycmF5PFN0YXR1cz4gfCBudWxsPjtcclxufVxyXG4iXX0=