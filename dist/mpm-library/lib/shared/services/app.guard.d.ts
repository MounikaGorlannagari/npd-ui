import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import * as ɵngcc0 from '@angular/core';
export declare class AppGuard implements CanActivate {
    appService: AppService;
    otmmService: OTMMService;
    sharingService: SharingService;
    appRouteService: AppRouteService;
    constructor(appService: AppService, otmmService: OTMMService, sharingService: SharingService, appRouteService: AppRouteService);
    getCRActionsForUser(): Observable<any>;
    getLookupDomains(): Observable<any>;
    canActivate(): Observable<boolean>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AppGuard, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmd1YXJkLmQudHMiLCJzb3VyY2VzIjpbImFwcC5ndWFyZC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDYW5BY3RpdmF0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQXBwR3VhcmQgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcbiAgICBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlO1xyXG4gICAgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlO1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgYXBwUm91dGVTZXJ2aWNlOiBBcHBSb3V0ZVNlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3RvcihhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSwgYXBwUm91dGVTZXJ2aWNlOiBBcHBSb3V0ZVNlcnZpY2UpO1xyXG4gICAgZ2V0Q1JBY3Rpb25zRm9yVXNlcigpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRMb29rdXBEb21haW5zKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGNhbkFjdGl2YXRlKCk6IE9ic2VydmFibGU8Ym9vbGVhbj47XHJcbn1cclxuIl19