import { AppDynamicConfigInterface } from '../interface/app.dynamic.interface';
export declare class AppDynamicConfig {
    constructor();
    static brandingConfig: AppDynamicConfigInterface;
    static setBrandingConfig(brandingConfig: any): void;
    static getDefaultStylePath(): string;
    static getAppThemeName(): string;
    static getFavIcon(): string;
    static getAppLogo(): string;
    static getProjectThumbnail(): string;
}
