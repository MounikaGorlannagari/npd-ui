import { HttpClient } from '@angular/common/http';
import * as ɵngcc0 from '@angular/core';
export declare class AppDynamicConfigService {
    http: HttpClient;
    constructor(http: HttpClient);
    getBrandingAndConfig(url: any): import("rxjs").Observable<Object>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AppDynamicConfigService, never>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AppDynamicConfigService>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmR5bmFtaWMuY29uZmlnLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsiYXBwLmR5bmFtaWMuY29uZmlnLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBcHBEeW5hbWljQ29uZmlnU2VydmljZSB7XHJcbiAgICBodHRwOiBIdHRwQ2xpZW50O1xyXG4gICAgY29uc3RydWN0b3IoaHR0cDogSHR0cENsaWVudCk7XHJcbiAgICBnZXRCcmFuZGluZ0FuZENvbmZpZyh1cmw6IGFueSk6IGltcG9ydChcInJ4anNcIikuT2JzZXJ2YWJsZTxPYmplY3Q+O1xyXG59XHJcbiJdfQ==