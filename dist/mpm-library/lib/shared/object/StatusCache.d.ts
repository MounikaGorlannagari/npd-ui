import { MPM_LEVEL } from "../../mpm-utils/objects/Level";
import { Status } from "../../mpm-utils/objects/Status";
export declare type StatusCache = {
    [key in MPM_LEVEL]?: Array<Status>;
};
