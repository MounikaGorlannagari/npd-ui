import { OnInit } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AssetMetadataHelper } from '../shared/services/asset.metadata.helper';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class AssetDetailsComponent implements OnInit {
    otmmService: OTMMService;
    assetMetdataHelper: AssetMetadataHelper;
    sharingService: SharingService;
    asset: any;
    data: {
        displayableMetadataFields: any[];
        displayableInheritedMetadataFields: any[];
    };
    assetInheritedData: any;
    constructor(otmmService: OTMMService, assetMetdataHelper: AssetMetadataHelper, sharingService: SharingService);
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetDetailsComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<AssetDetailsComponent, "mpm-asset-details", never, { "asset": "asset"; }, {}, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtZGV0YWlscy5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiYXNzZXQtZGV0YWlscy5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IEFzc2V0TWV0YWRhdGFIZWxwZXIgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvYXNzZXQubWV0YWRhdGEuaGVscGVyJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQXNzZXREZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIGFzc2V0TWV0ZGF0YUhlbHBlcjogQXNzZXRNZXRhZGF0YUhlbHBlcjtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIGFzc2V0OiBhbnk7XHJcbiAgICBkYXRhOiB7XHJcbiAgICAgICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkczogYW55W107XHJcbiAgICAgICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkczogYW55W107XHJcbiAgICB9O1xyXG4gICAgYXNzZXRJbmhlcml0ZWREYXRhOiBhbnk7XHJcbiAgICBjb25zdHJ1Y3RvcihvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIGFzc2V0TWV0ZGF0YUhlbHBlcjogQXNzZXRNZXRhZGF0YUhlbHBlciwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlKTtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19