export interface ProjectFromTemplateObj {
    'SourceID': number;
    'ProjectName': string;
    'ProjectOwner': string;
    'ProjectDescription': string;
    'ProjectStartDate': string;
    'ProjectDueDate': string;
    'TaskStartDate': string;
    'TaskDueDate': string;
    'IsCopyTask': boolean;
    'IsCopyDeliverable': boolean;
    'CustomMetadata': [];
}
export interface CustomMetadataObj {
    'customMetadataFields': any;
    'customFieldsGroup': object;
    'fieldsetGroup': any;
    'fieldGroups': any;
}
export interface ProjectTemplateConfigObject {
    'allTemplateCount': number;
    'alltemplateList': Array<object>;
    'projectSearchCondition': Array<object>;
    'projectType': String;
    'selectedProject': object;
    'isDataLoaded': boolean;
}
