export declare const AssetStatusConstants: {
    CANCELLED: string;
    REQUESTED_CHANGES: string;
    APPROVED: string;
    IN_PROGRESS: string;
    DEFINED: string;
    DRAFT: string;
    COMPLETED: string;
    REJECTED: string;
};
