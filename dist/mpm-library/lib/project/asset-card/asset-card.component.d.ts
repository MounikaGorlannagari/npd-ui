import { OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AssetService } from '../shared/services/asset.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { TaskService } from '../tasks/task.service';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { UtilService } from '../../mpm-utils/services/util.service';
import { QdsService } from '../../upload/services/qds.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { Router } from '@angular/router';
import { DeliverableService } from '../tasks/deliverable/deliverable.service';
import * as ɵngcc0 from '@angular/core';
export declare class AssetCardComponent implements OnInit, OnChanges {
    otmmService: OTMMService;
    assetService: AssetService;
    sharingService: SharingService;
    taskService: TaskService;
    utilService: UtilService;
    qdsService: QdsService;
    entityAppdefService: EntityAppDefService;
    loaderService: LoaderService;
    notificationService: NotificationService;
    appService: AppService;
    router: Router;
    deliverableService: DeliverableService;
    asset: any;
    selectedAssetData: any;
    isVersion: any;
    selectAllDeliverables: any;
    filterType: any;
    IsNotPMView: any;
    assetCardFields: any;
    assetVersionsCallBackHandler: EventEmitter<any>;
    assetDetailsCallBackHandler: EventEmitter<any>;
    selectedAssetsCallBackHandler: EventEmitter<any>;
    unSelectedAssetsCallBackHandler: EventEmitter<any>;
    shareAssetsCallbackHandler: EventEmitter<any>;
    assetPreviewCallBackHandler: EventEmitter<any>;
    downloadCallBackHandler: EventEmitter<any>;
    openCreativeReview: EventEmitter<any>;
    downloadHandler: EventEmitter<any>;
    otmmBaseUrl: any;
    checked: boolean;
    copyToolTip: string;
    assetStatus: any;
    assetDescription: string;
    assetStatusConstants: {
        CANCELLED: string;
        REQUESTED_CHANGES: string;
        APPROVED: string;
        IN_PROGRESS: string;
        DEFINED: string;
        DRAFT: string;
        COMPLETED: string;
        REJECTED: string;
    };
    deliverableConstants: {
        CATEGORY_LEVEL_DELIVERABLE: string;
        STATUS_TYPE_INITIAL: string;
        ALLOCATION_TYPE_ROLE: string;
        ALLOCATION_TYPE_USER: string;
        dataTypes: {
            STRING: string;
            DATE: string;
        };
        DELIVERABLE_ITEM_ID_METADATAFIELD_ID: string;
        DELIVERABLE_NAME_METADATAFIELD_ID: string;
        StatusConstant: {
            DELIVERABLE_STATUS: {
                FINAL: string;
                ACCEPTED: string;
                REWORK: string;
                CANCELLED: string;
            };
        };
        DELIVERABLE_SEARCH_CONDITION: {
            search_condition_list: {
                search_condition: ({
                    type: import("../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../public-api").MPMSearchOperatorNames;
                    value: string;
                    relational_operator?: undefined;
                } | {
                    type: import("../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../public-api").MPMSearchOperatorNames;
                    value: OTMMMPMDataTypes;
                    relational_operator: string;
                })[];
            };
        };
        DELIVERABLE_RESOURCE_SEARCH_CONDITION: {
            type: import("../../../public-api").IndexerDataTypes;
            field_id: string;
            relational_operator_id: import("../../../public-api").MPMSearchOperators;
            relational_operator_name: import("../../../public-api").MPMSearchOperatorNames;
            value: string;
            relational_operator: string;
        }[];
        ALLOCATION_TYPE: {
            NAME: string;
            DESCRIPTION: import("../../../public-api").AllocationTypes;
        }[];
        DATE_ERROR_MESSAGE: string;
        DELIVERABLE_TYPE_LIST: {
            NAME: string;
            DESCRIPTION: string;
            ICON: string;
        }[];
        DEFAULT_CARD_FIELDS: string[];
    };
    skip: number;
    top: number;
    appConfig: any;
    canDownload: boolean;
    isReferenceAsset: any;
    crActionData: any;
    projectData: any;
    currentUserCRData: any;
    currentUserTeamRoleMapping: any;
    crRole: any;
    currProjectId: any;
    taskId: any;
    taskName: any;
    deliverableId: any;
    taskData: any;
    projectCompletedStatus: any;
    deliverableStatus: any;
    isOnHoldProject: any;
    constructor(otmmService: OTMMService, assetService: AssetService, sharingService: SharingService, taskService: TaskService, utilService: UtilService, qdsService: QdsService, entityAppdefService: EntityAppDefService, loaderService: LoaderService, notificationService: NotificationService, appService: AppService, router: Router, deliverableService: DeliverableService);
    findDocType(fileName: any): "image" | "insert_drive_file" | "description" | "movie_creation" | "picture_as_pdf" | "gif" | "archive" | "graphic_eq";
    fileFormat(fileName: any): any;
    checkDeliverable(asset: any): void;
    download(asset: any): void;
    getAssetVersions(asset: any): void;
    getAssetDetails(asset: any): void;
    getAssetPreview(asset: any): void;
    updateDescription(): void;
    notificationHandler(event: any): void;
    loadingHandler(event: any): void;
    getColumnValue(column: any): any;
    getProperty(asset: any, property: any): string;
    shareAsset(): void;
    getDeliverableDetails(deliverableId: any): Observable<any>;
    onClickEdit(): void;
    openCR(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetCardComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<AssetCardComponent, "mpm-asset-card", never, { "assetCardFields": "assetCardFields"; "asset": "asset"; "selectedAssetData": "selectedAssetData"; "isVersion": "isVersion"; "selectAllDeliverables": "selectAllDeliverables"; "filterType": "filterType"; "IsNotPMView": "IsNotPMView"; }, { "assetVersionsCallBackHandler": "assetVersionsCallBackHandler"; "assetDetailsCallBackHandler": "assetDetailsCallBackHandler"; "selectedAssetsCallBackHandler": "selectedAssetsCallBackHandler"; "unSelectedAssetsCallBackHandler": "unSelectedAssetsCallBackHandler"; "shareAssetsCallbackHandler": "shareAssetsCallbackHandler"; "assetPreviewCallBackHandler": "assetPreviewCallBackHandler"; "downloadCallBackHandler": "downloadCallBackHandler"; "openCreativeReview": "openCreativeReview"; "downloadHandler": "downloadHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtY2FyZC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiYXNzZXQtY2FyZC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vdGFza3MvdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTU1QTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1NUE1EYXRhVHlwZXMnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBRZHNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vdXBsb2FkL3NlcnZpY2VzL3Fkcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU2VydmljZSB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBc3NldENhcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICBvdG1tU2VydmljZTogT1RNTVNlcnZpY2U7XHJcbiAgICBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZTtcclxuICAgIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZTtcclxuICAgIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2U7XHJcbiAgICBlbnRpdHlBcHBkZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlO1xyXG4gICAgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlO1xyXG4gICAgcm91dGVyOiBSb3V0ZXI7XHJcbiAgICBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZTtcclxuICAgIGFzc2V0OiBhbnk7XHJcbiAgICBzZWxlY3RlZEFzc2V0RGF0YTogYW55O1xyXG4gICAgaXNWZXJzaW9uOiBhbnk7XHJcbiAgICBzZWxlY3RBbGxEZWxpdmVyYWJsZXM6IGFueTtcclxuICAgIGZpbHRlclR5cGU6IGFueTtcclxuICAgIElzTm90UE1WaWV3OiBhbnk7XHJcbiAgICBhc3NldENhcmRGaWVsZHM6IGFueTtcclxuICAgIGFzc2V0VmVyc2lvbnNDYWxsQmFja0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHVuU2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgYXNzZXRQcmV2aWV3Q2FsbEJhY2tIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGRvd25sb2FkQ2FsbEJhY2tIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIG9wZW5DcmVhdGl2ZVJldmlldzogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBkb3dubG9hZEhhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgb3RtbUJhc2VVcmw6IGFueTtcclxuICAgIGNoZWNrZWQ6IGJvb2xlYW47XHJcbiAgICBjb3B5VG9vbFRpcDogc3RyaW5nO1xyXG4gICAgYXNzZXRTdGF0dXM6IGFueTtcclxuICAgIGFzc2V0RGVzY3JpcHRpb246IHN0cmluZztcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FOQ0VMTEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVRVUVTVEVEX0NIQU5HRVM6IHN0cmluZztcclxuICAgICAgICBBUFBST1ZFRDogc3RyaW5nO1xyXG4gICAgICAgIElOX1BST0dSRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgREVGSU5FRDogc3RyaW5nO1xyXG4gICAgICAgIERSQUZUOiBzdHJpbmc7XHJcbiAgICAgICAgQ09NUExFVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVKRUNURUQ6IHN0cmluZztcclxuICAgIH07XHJcbiAgICBkZWxpdmVyYWJsZUNvbnN0YW50czoge1xyXG4gICAgICAgIENBVEVHT1JZX0xFVkVMX0RFTElWRVJBQkxFOiBzdHJpbmc7XHJcbiAgICAgICAgU1RBVFVTX1RZUEVfSU5JVElBTDogc3RyaW5nO1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRV9ST0xFOiBzdHJpbmc7XHJcbiAgICAgICAgQUxMT0NBVElPTl9UWVBFX1VTRVI6IHN0cmluZztcclxuICAgICAgICBkYXRhVHlwZXM6IHtcclxuICAgICAgICAgICAgU1RSSU5HOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERBVEU6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIERFTElWRVJBQkxFX0lURU1fSURfTUVUQURBVEFGSUVMRF9JRDogc3RyaW5nO1xyXG4gICAgICAgIERFTElWRVJBQkxFX05BTUVfTUVUQURBVEFGSUVMRF9JRDogc3RyaW5nO1xyXG4gICAgICAgIFN0YXR1c0NvbnN0YW50OiB7XHJcbiAgICAgICAgICAgIERFTElWRVJBQkxFX1NUQVRVUzoge1xyXG4gICAgICAgICAgICAgICAgRklOQUw6IHN0cmluZztcclxuICAgICAgICAgICAgICAgIEFDQ0VQVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBSRVdPUks6IHN0cmluZztcclxuICAgICAgICAgICAgICAgIENBTkNFTExFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgREVMSVZFUkFCTEVfU0VBUkNIX0NPTkRJVElPTjoge1xyXG4gICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246ICh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogaW1wb3J0KFwiLi4vLi4vLi4vcHVibGljLWFwaVwiKS5JbmRleGVyRGF0YVR5cGVzO1xyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkX2lkOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogaW1wb3J0KFwiLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBpbXBvcnQoXCIuLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9yTmFtZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yPzogdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgfSB8IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogT1RNTU1QTURhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICB9KVtdO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgREVMSVZFUkFCTEVfUkVTT1VSQ0VfU0VBUkNIX0NPTkRJVElPTjoge1xyXG4gICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgIGZpZWxkX2lkOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgIH1bXTtcclxuICAgICAgICBBTExPQ0FUSU9OX1RZUEU6IHtcclxuICAgICAgICAgICAgTkFNRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBERVNDUklQVElPTjogaW1wb3J0KFwiLi4vLi4vLi4vcHVibGljLWFwaVwiKS5BbGxvY2F0aW9uVHlwZXM7XHJcbiAgICAgICAgfVtdO1xyXG4gICAgICAgIERBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERFTElWRVJBQkxFX1RZUEVfTElTVDoge1xyXG4gICAgICAgICAgICBOQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElDT046IHN0cmluZztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgREVGQVVMVF9DQVJEX0ZJRUxEUzogc3RyaW5nW107XHJcbiAgICB9O1xyXG4gICAgc2tpcDogbnVtYmVyO1xyXG4gICAgdG9wOiBudW1iZXI7XHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIGNhbkRvd25sb2FkOiBib29sZWFuO1xyXG4gICAgaXNSZWZlcmVuY2VBc3NldDogYW55O1xyXG4gICAgY3JBY3Rpb25EYXRhOiBhbnk7XHJcbiAgICBwcm9qZWN0RGF0YTogYW55O1xyXG4gICAgY3VycmVudFVzZXJDUkRhdGE6IGFueTtcclxuICAgIGN1cnJlbnRVc2VyVGVhbVJvbGVNYXBwaW5nOiBhbnk7XHJcbiAgICBjclJvbGU6IGFueTtcclxuICAgIGN1cnJQcm9qZWN0SWQ6IGFueTtcclxuICAgIHRhc2tJZDogYW55O1xyXG4gICAgdGFza05hbWU6IGFueTtcclxuICAgIGRlbGl2ZXJhYmxlSWQ6IGFueTtcclxuICAgIHRhc2tEYXRhOiBhbnk7XHJcbiAgICBwcm9qZWN0Q29tcGxldGVkU3RhdHVzOiBhbnk7XHJcbiAgICBkZWxpdmVyYWJsZVN0YXR1czogYW55O1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBhbnk7XHJcbiAgICBjb25zdHJ1Y3RvcihvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSwgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLCBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlLCBlbnRpdHlBcHBkZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCByb3V0ZXI6IFJvdXRlciwgZGVsaXZlcmFibGVTZXJ2aWNlOiBEZWxpdmVyYWJsZVNlcnZpY2UpO1xyXG4gICAgZmluZERvY1R5cGUoZmlsZU5hbWU6IGFueSk6IFwiaW1hZ2VcIiB8IFwiaW5zZXJ0X2RyaXZlX2ZpbGVcIiB8IFwiZGVzY3JpcHRpb25cIiB8IFwibW92aWVfY3JlYXRpb25cIiB8IFwicGljdHVyZV9hc19wZGZcIiB8IFwiZ2lmXCIgfCBcImFyY2hpdmVcIiB8IFwiZ3JhcGhpY19lcVwiO1xyXG4gICAgZmlsZUZvcm1hdChmaWxlTmFtZTogYW55KTogYW55O1xyXG4gICAgY2hlY2tEZWxpdmVyYWJsZShhc3NldDogYW55KTogdm9pZDtcclxuICAgIGRvd25sb2FkKGFzc2V0OiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0QXNzZXRWZXJzaW9ucyhhc3NldDogYW55KTogdm9pZDtcclxuICAgIGdldEFzc2V0RGV0YWlscyhhc3NldDogYW55KTogdm9pZDtcclxuICAgIGdldEFzc2V0UHJldmlldyhhc3NldDogYW55KTogdm9pZDtcclxuICAgIHVwZGF0ZURlc2NyaXB0aW9uKCk6IHZvaWQ7XHJcbiAgICBub3RpZmljYXRpb25IYW5kbGVyKGV2ZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgbG9hZGluZ0hhbmRsZXIoZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRDb2x1bW5WYWx1ZShjb2x1bW46IGFueSk6IGFueTtcclxuICAgIGdldFByb3BlcnR5KGFzc2V0OiBhbnksIHByb3BlcnR5OiBhbnkpOiBzdHJpbmc7XHJcbiAgICBzaGFyZUFzc2V0KCk6IHZvaWQ7XHJcbiAgICBnZXREZWxpdmVyYWJsZURldGFpbHMoZGVsaXZlcmFibGVJZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgb25DbGlja0VkaXQoKTogdm9pZDtcclxuICAgIG9wZW5DUigpOiB2b2lkO1xyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==