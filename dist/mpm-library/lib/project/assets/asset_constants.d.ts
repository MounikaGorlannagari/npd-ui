import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export declare const AssetConstants: {
    ASSET_KEYWORD: OTMMMPMDataTypes;
    ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: ({
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator?: undefined;
            } | {
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator: string;
            })[];
        };
    };
    REFERENCE_ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: ({
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator?: undefined;
                value?: undefined;
            } | {
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator: string;
                value: string;
            })[];
        };
    };
    ADD_REFERENCE_ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: ({
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator?: undefined;
                value?: undefined;
            } | {
                type: string;
                metadata_field_id: string;
                relational_operator_id: string;
                relational_operator_name: string;
                relational_operator: string;
                value: string;
            })[];
        };
    };
    ASSET_DETAILS_CONDITION: {
        search_condition_list: {
            search_condition: ({
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: string;
                relational_operator?: undefined;
            } | {
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: string;
                relational_operator: string;
            })[];
        };
    };
    REFERENCE_ASSET_DETAILS_CONDITION: {
        search_condition_list: {
            search_condition: {
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: string;
            }[];
        };
    };
    ASSET_DETAILS_VIEW_CONFIG: {
        type: string;
        content_type: string;
        group_display_name: string;
    }[];
    REFERENCE_ASSET_DETAILS_VIEW_CONFIG: {
        type: string;
        content_type: string;
        group_display_name: string;
    }[];
    ASSET_UPLOAD_FIELDS: {
        id: string;
        mapperName: string;
    }[];
    REFERENCE_ASSET_UPLOAD_FIELDS: {
        id: string;
        mapperName: string;
    }[];
    REFERENCE_TYPES_OBJECTS: {
        selected: {
            name: string;
            value: string;
        };
        options: {
            name: string;
            value: string;
        }[];
    };
    DOWNLOAD_TYPES_OBJECTS: {
        selected: {
            name: string;
            value: string;
        };
        options: {
            name: string;
            value: string;
        }[];
    };
    DOWNLOAD_TYPES: {
        ZIP: string;
        INDIVIDUAL_FILES: string;
    };
};
