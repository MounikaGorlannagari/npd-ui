import { OnInit, EventEmitter } from '@angular/core';
import { SwimLaneComponent } from '../swim-lane/swim-lane.component';
import { TaskListViewComponent } from '../task-list-view/task-list-view.component';
import * as ɵngcc0 from '@angular/core';
export declare class TaskGridComponent implements OnInit {
    taskConfig: any;
    refreshTaskGrid: EventEmitter<any>;
    editTaskHandler: EventEmitter<any>;
    taskDetailsHandler: EventEmitter<any>;
    sortChange: EventEmitter<any>;
    swimLaneComponent: SwimLaneComponent;
    taskListViewComponent: TaskListViewComponent;
    constructor();
    refreshChild(taskConfig: any): void;
    refreshSwimLine(event: any): void;
    refreshTaskList(event: any): void;
    editTask(eventData: any): void;
    openTaskDetails(eventData: any): void;
    onSortChange(eventData: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<TaskGridComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<TaskGridComponent, "mpm-task-grid", never, { "taskConfig": "taskConfig"; }, { "refreshTaskGrid": "refreshTaskGrid"; "editTaskHandler": "editTaskHandler"; "taskDetailsHandler": "taskDetailsHandler"; "sortChange": "sortChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1ncmlkLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJ0YXNrLWdyaWQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN3aW1MYW5lQ29tcG9uZW50IH0gZnJvbSAnLi4vc3dpbS1sYW5lL3N3aW0tbGFuZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYXNrTGlzdFZpZXdDb21wb25lbnQgfSBmcm9tICcuLi90YXNrLWxpc3Qtdmlldy90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBUYXNrR3JpZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICB0YXNrQ29uZmlnOiBhbnk7XHJcbiAgICByZWZyZXNoVGFza0dyaWQ6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgZWRpdFRhc2tIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHRhc2tEZXRhaWxzSGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBzb3J0Q2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHN3aW1MYW5lQ29tcG9uZW50OiBTd2ltTGFuZUNvbXBvbmVudDtcclxuICAgIHRhc2tMaXN0Vmlld0NvbXBvbmVudDogVGFza0xpc3RWaWV3Q29tcG9uZW50O1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIHJlZnJlc2hDaGlsZCh0YXNrQ29uZmlnOiBhbnkpOiB2b2lkO1xyXG4gICAgcmVmcmVzaFN3aW1MaW5lKGV2ZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgcmVmcmVzaFRhc2tMaXN0KGV2ZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgZWRpdFRhc2soZXZlbnREYXRhOiBhbnkpOiB2b2lkO1xyXG4gICAgb3BlblRhc2tEZXRhaWxzKGV2ZW50RGF0YTogYW55KTogdm9pZDtcclxuICAgIG9uU29ydENoYW5nZShldmVudERhdGE6IGFueSk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==