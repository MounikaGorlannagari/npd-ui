import { AllocationTypes } from '../../mpm-utils/objects/AllocationType';
import { TaskTypes } from '../../mpm-utils/objects/TaskType';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export declare const TaskConstants: {
    CATEGORY_LEVEL_TASK: string;
    STATUS_TYPE_INITIAL: string;
    ALLOCATION_TYPE_ROLE: string;
    ALLOCATION_TYPE_USER: string;
    TASK_TYPE: {
        NORMAL_TASK: string;
        TASK_WITHOUT_DELIVERABLE: string;
        APPROVAL_TASK: string;
    };
    TASK_ITEM_ID_METADATAFIELD_ID: string;
    dataTypes: {
        STRING: string;
        DATE: string;
    };
    StatusConstant: {
        TASK_STATUS: {
            FINAL: string;
            ACCEPTED: string;
            REWORK: string;
            CANCELLED: string;
        };
    };
    GET_ALL_TASKS_SEARCH_CONDITION_LIST: {
        search_condition_list: {
            search_condition: ({
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: string;
                relational_operator?: undefined;
            } | {
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: OTMMMPMDataTypes;
                relational_operator: string;
            })[];
        };
    };
    ACTIVE_TASK_SEARCH_CONDITION: {
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: string;
        relational_operator: string;
    };
    NOTIFICATION_LABELS: {
        ERROR: string;
        SUCCESS: string;
        INFO: string;
        WARN: string;
    };
    ALLOCATION_TYPE: {
        NAME: string;
        DESCRIPTION: AllocationTypes;
    }[];
    TASK_TYPE_LIST: {
        NAME: TaskTypes;
        DESCRIPTION: string;
        ICON: string;
    }[];
    NAME_STRING_PATTERN: string;
    PROJECT_DATE_ERROR_MESSAGE: string;
    DELIVERABLE_DATE_ERROR_MESSAGE: string;
    DELIVERABLE_MAX_DATE_ERROR_MESSAGE: string;
    START_DATE_ERROR_MESSAGE: string;
    DURATION_ERROR_MESSAGE: string;
    DURATION_MIN_ERROR_MESSAGE: string;
    DEFAULT_CARD_FIELDS: string[];
};
