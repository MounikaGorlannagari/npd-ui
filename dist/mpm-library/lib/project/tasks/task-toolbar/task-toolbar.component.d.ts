import { OnInit, EventEmitter } from '@angular/core';
import { FormControl, FormArray } from '@angular/forms';
import { ItemSortComponent } from '../item-sort/item-sort.component';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { AssetService } from '../../shared/services/asset.service';
import * as ɵngcc0 from '@angular/core';
export declare class TaskToolbarComponent implements OnInit {
    sharingService: SharingService;
    commentUtilService: CommentsUtilService;
    utilService: UtilService;
    assetService: AssetService;
    taskConfig: any;
    columnChooserFields: any;
    taskConfigChange: EventEmitter<any>;
    createNewTaskHandler: EventEmitter<any>;
    createNewTaskByTypeHandler: EventEmitter<any>;
    viewChange: EventEmitter<any>;
    customWorkflowHandler: EventEmitter<any>;
    columnChooserHandler: EventEmitter<any>;
    saveColumnChooserHandler: EventEmitter<any>;
    resetColumnChooserHandler: EventEmitter<any>;
    isColumnsFreezable: boolean;
    isProjectCompleted: boolean;
    showTaskViewButton: boolean;
    selectedOption: any;
    resources: FormArray;
    viewByResources: FormControl;
    taskConstants: {
        CATEGORY_LEVEL_TASK: string;
        STATUS_TYPE_INITIAL: string;
        ALLOCATION_TYPE_ROLE: string;
        ALLOCATION_TYPE_USER: string;
        TASK_TYPE: {
            NORMAL_TASK: string;
            TASK_WITHOUT_DELIVERABLE: string;
            APPROVAL_TASK: string;
        };
        TASK_ITEM_ID_METADATAFIELD_ID: string;
        dataTypes: {
            STRING: string;
            DATE: string;
        };
        StatusConstant: {
            TASK_STATUS: {
                FINAL: string;
                ACCEPTED: string;
                REWORK: string;
                CANCELLED: string;
            };
        };
        GET_ALL_TASKS_SEARCH_CONDITION_LIST: {
            search_condition_list: {
                search_condition: ({
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: string;
                    relational_operator?: undefined;
                } | {
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: import("../../../../public-api").OTMMMPMDataTypes;
                    relational_operator: string;
                })[];
            };
        };
        ACTIVE_TASK_SEARCH_CONDITION: {
            type: import("../../../../public-api").IndexerDataTypes;
            field_id: string;
            relational_operator_id: import("../../../../public-api").MPMSearchOperators;
            relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
            value: string;
            relational_operator: string;
        };
        NOTIFICATION_LABELS: {
            ERROR: string;
            SUCCESS: string;
            INFO: string;
            WARN: string;
        };
        ALLOCATION_TYPE: {
            NAME: string;
            DESCRIPTION: import("../../../../public-api").AllocationTypes;
        }[];
        TASK_TYPE_LIST: {
            NAME: import("../../../../public-api").TaskTypes;
            DESCRIPTION: string;
            ICON: string;
        }[];
        NAME_STRING_PATTERN: string;
        PROJECT_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_MAX_DATE_ERROR_MESSAGE: string;
        START_DATE_ERROR_MESSAGE: string;
        DURATION_ERROR_MESSAGE: string;
        DURATION_MIN_ERROR_MESSAGE: string;
        DEFAULT_CARD_FIELDS: string[];
    };
    disableNewTask: boolean;
    taskTypeIcon: {
        NORMAL_TASK: import("../../shared/constants/task-type-icon.constants").TaskTypeIcon;
        TASK_WITHOUT_DELIVERABLE: import("../../shared/constants/task-type-icon.constants").TaskTypeIcon;
        APPROVAL_TASK: import("../../shared/constants/task-type-icon.constants").TaskTypeIcon;
    };
    disableGridView: boolean;
    appConfig: any;
    isOnHoldProject: boolean;
    enableApprovalTask: boolean;
    itemSortComponent: ItemSortComponent;
    constructor(sharingService: SharingService, commentUtilService: CommentsUtilService, utilService: UtilService, assetService: AssetService);
    createNewTask(): void;
    createNewTaskByType(type: any): void;
    refresh(): void;
    openCustomWorkflow(): void;
    updateDisplayColumns(displayColumns: any): void;
    saveDisplayColumns(displayColumns: any): void;
    resetDisplayColumns(): void;
    changeView(): void;
    onSortChange(eventData: any): void;
    refreshChild(taskConfig: any): void;
    onResourceSelect(): void;
    onResourceRemoved(resource: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<TaskToolbarComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<TaskToolbarComponent, "mpm-task-toolbar", never, { "taskConfig": "taskConfig"; "columnChooserFields": "columnChooserFields"; }, { "taskConfigChange": "taskConfigChange"; "createNewTaskHandler": "createNewTaskHandler"; "createNewTaskByTypeHandler": "createNewTaskByTypeHandler"; "viewChange": "viewChange"; "customWorkflowHandler": "customWorkflowHandler"; "columnChooserHandler": "columnChooserHandler"; "saveColumnChooserHandler": "saveColumnChooserHandler"; "resetColumnChooserHandler": "resetColumnChooserHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay10b29sYmFyLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJ0YXNrLXRvb2xiYXIuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sLCBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEl0ZW1Tb3J0Q29tcG9uZW50IH0gZnJvbSAnLi4vaXRlbS1zb3J0L2l0ZW0tc29ydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29tbWVudHMvc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2Fzc2V0LnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBUYXNrVG9vbGJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2U7XHJcbiAgICB1dGlsU2VydmljZTogVXRpbFNlcnZpY2U7XHJcbiAgICBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZTtcclxuICAgIHRhc2tDb25maWc6IGFueTtcclxuICAgIGNvbHVtbkNob29zZXJGaWVsZHM6IGFueTtcclxuICAgIHRhc2tDb25maWdDaGFuZ2U6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgY3JlYXRlTmV3VGFza0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgY3JlYXRlTmV3VGFza0J5VHlwZUhhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgdmlld0NoYW5nZTogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBjdXN0b21Xb3JrZmxvd0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgY29sdW1uQ2hvb3NlckhhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgc2F2ZUNvbHVtbkNob29zZXJIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHJlc2V0Q29sdW1uQ2hvb3NlckhhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgaXNDb2x1bW5zRnJlZXphYmxlOiBib29sZWFuO1xyXG4gICAgaXNQcm9qZWN0Q29tcGxldGVkOiBib29sZWFuO1xyXG4gICAgc2hvd1Rhc2tWaWV3QnV0dG9uOiBib29sZWFuO1xyXG4gICAgc2VsZWN0ZWRPcHRpb246IGFueTtcclxuICAgIHJlc291cmNlczogRm9ybUFycmF5O1xyXG4gICAgdmlld0J5UmVzb3VyY2VzOiBGb3JtQ29udHJvbDtcclxuICAgIHRhc2tDb25zdGFudHM6IHtcclxuICAgICAgICBDQVRFR09SWV9MRVZFTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgU1RBVFVTX1RZUEVfSU5JVElBTDogc3RyaW5nO1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRV9ST0xFOiBzdHJpbmc7XHJcbiAgICAgICAgQUxMT0NBVElPTl9UWVBFX1VTRVI6IHN0cmluZztcclxuICAgICAgICBUQVNLX1RZUEU6IHtcclxuICAgICAgICAgICAgTk9STUFMX1RBU0s6IHN0cmluZztcclxuICAgICAgICAgICAgVEFTS19XSVRIT1VUX0RFTElWRVJBQkxFOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIEFQUFJPVkFMX1RBU0s6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIFRBU0tfSVRFTV9JRF9NRVRBREFUQUZJRUxEX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgZGF0YVR5cGVzOiB7XHJcbiAgICAgICAgICAgIFNUUklORzogc3RyaW5nO1xyXG4gICAgICAgICAgICBEQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBTdGF0dXNDb25zdGFudDoge1xyXG4gICAgICAgICAgICBUQVNLX1NUQVRVUzoge1xyXG4gICAgICAgICAgICAgICAgRklOQUw6IHN0cmluZztcclxuICAgICAgICAgICAgICAgIEFDQ0VQVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBSRVdPUks6IHN0cmluZztcclxuICAgICAgICAgICAgICAgIENBTkNFTExFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgR0VUX0FMTF9UQVNLU19TRUFSQ0hfQ09ORElUSU9OX0xJU1Q6IHtcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuSW5kZXhlckRhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvck5hbWVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcj86IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgICAgIH0gfCB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5JbmRleGVyRGF0YVR5cGVzO1xyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkX2lkOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9yTmFtZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuT1RNTU1QTURhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICB9KVtdO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgQUNUSVZFX1RBU0tfU0VBUkNIX0NPTkRJVElPTjoge1xyXG4gICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgIGZpZWxkX2lkOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgTk9USUZJQ0FUSU9OX0xBQkVMUzoge1xyXG4gICAgICAgICAgICBFUlJPUjogc3RyaW5nO1xyXG4gICAgICAgICAgICBTVUNDRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElORk86IHN0cmluZztcclxuICAgICAgICAgICAgV0FSTjogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgQUxMT0NBVElPTl9UWVBFOiB7XHJcbiAgICAgICAgICAgIE5BTUU6IHN0cmluZztcclxuICAgICAgICAgICAgREVTQ1JJUFRJT046IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuQWxsb2NhdGlvblR5cGVzO1xyXG4gICAgICAgIH1bXTtcclxuICAgICAgICBUQVNLX1RZUEVfTElTVDoge1xyXG4gICAgICAgICAgICBOQU1FOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLlRhc2tUeXBlcztcclxuICAgICAgICAgICAgREVTQ1JJUFRJT046IHN0cmluZztcclxuICAgICAgICAgICAgSUNPTjogc3RyaW5nO1xyXG4gICAgICAgIH1bXTtcclxuICAgICAgICBOQU1FX1NUUklOR19QQVRURVJOOiBzdHJpbmc7XHJcbiAgICAgICAgUFJPSkVDVF9EQVRFX0VSUk9SX01FU1NBR0U6IHN0cmluZztcclxuICAgICAgICBERUxJVkVSQUJMRV9EQVRFX0VSUk9SX01FU1NBR0U6IHN0cmluZztcclxuICAgICAgICBERUxJVkVSQUJMRV9NQVhfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgRFVSQVRJT05fRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERVUkFUSU9OX01JTl9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVGQVVMVF9DQVJEX0ZJRUxEUzogc3RyaW5nW107XHJcbiAgICB9O1xyXG4gICAgZGlzYWJsZU5ld1Rhc2s6IGJvb2xlYW47XHJcbiAgICB0YXNrVHlwZUljb246IHtcclxuICAgICAgICBOT1JNQUxfVEFTSzogaW1wb3J0KFwiLi4vLi4vc2hhcmVkL2NvbnN0YW50cy90YXNrLXR5cGUtaWNvbi5jb25zdGFudHNcIikuVGFza1R5cGVJY29uO1xyXG4gICAgICAgIFRBU0tfV0lUSE9VVF9ERUxJVkVSQUJMRTogaW1wb3J0KFwiLi4vLi4vc2hhcmVkL2NvbnN0YW50cy90YXNrLXR5cGUtaWNvbi5jb25zdGFudHNcIikuVGFza1R5cGVJY29uO1xyXG4gICAgICAgIEFQUFJPVkFMX1RBU0s6IGltcG9ydChcIi4uLy4uL3NoYXJlZC9jb25zdGFudHMvdGFzay10eXBlLWljb24uY29uc3RhbnRzXCIpLlRhc2tUeXBlSWNvbjtcclxuICAgIH07XHJcbiAgICBkaXNhYmxlR3JpZFZpZXc6IGJvb2xlYW47XHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIGlzT25Ib2xkUHJvamVjdDogYm9vbGVhbjtcclxuICAgIGVuYWJsZUFwcHJvdmFsVGFzazogYm9vbGVhbjtcclxuICAgIGl0ZW1Tb3J0Q29tcG9uZW50OiBJdGVtU29ydENvbXBvbmVudDtcclxuICAgIGNvbnN0cnVjdG9yKHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSwgY29tbWVudFV0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlLCB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlKTtcclxuICAgIGNyZWF0ZU5ld1Rhc2soKTogdm9pZDtcclxuICAgIGNyZWF0ZU5ld1Rhc2tCeVR5cGUodHlwZTogYW55KTogdm9pZDtcclxuICAgIHJlZnJlc2goKTogdm9pZDtcclxuICAgIG9wZW5DdXN0b21Xb3JrZmxvdygpOiB2b2lkO1xyXG4gICAgdXBkYXRlRGlzcGxheUNvbHVtbnMoZGlzcGxheUNvbHVtbnM6IGFueSk6IHZvaWQ7XHJcbiAgICBzYXZlRGlzcGxheUNvbHVtbnMoZGlzcGxheUNvbHVtbnM6IGFueSk6IHZvaWQ7XHJcbiAgICByZXNldERpc3BsYXlDb2x1bW5zKCk6IHZvaWQ7XHJcbiAgICBjaGFuZ2VWaWV3KCk6IHZvaWQ7XHJcbiAgICBvblNvcnRDaGFuZ2UoZXZlbnREYXRhOiBhbnkpOiB2b2lkO1xyXG4gICAgcmVmcmVzaENoaWxkKHRhc2tDb25maWc6IGFueSk6IHZvaWQ7XHJcbiAgICBvblJlc291cmNlU2VsZWN0KCk6IHZvaWQ7XHJcbiAgICBvblJlc291cmNlUmVtb3ZlZChyZXNvdXJjZTogYW55KTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19