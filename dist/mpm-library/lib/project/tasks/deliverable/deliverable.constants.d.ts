import { AllocationTypes } from '../../../mpm-utils/objects/AllocationType';
import { OTMMMPMDataTypes } from '../../../mpm-utils/objects/OTMMMPMDataTypes';
import { IndexerDataTypes } from '../../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export declare const DeliverableConstants: {
    CATEGORY_LEVEL_DELIVERABLE: string;
    STATUS_TYPE_INITIAL: string;
    ALLOCATION_TYPE_ROLE: string;
    ALLOCATION_TYPE_USER: string;
    dataTypes: {
        STRING: string;
        DATE: string;
    };
    DELIVERABLE_ITEM_ID_METADATAFIELD_ID: string;
    DELIVERABLE_NAME_METADATAFIELD_ID: string;
    StatusConstant: {
        DELIVERABLE_STATUS: {
            FINAL: string;
            ACCEPTED: string;
            REWORK: string;
            CANCELLED: string;
        };
    };
    DELIVERABLE_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: ({
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: string;
                relational_operator?: undefined;
            } | {
                type: IndexerDataTypes;
                field_id: string;
                relational_operator_id: MPMSearchOperators;
                relational_operator_name: MPMSearchOperatorNames;
                value: OTMMMPMDataTypes;
                relational_operator: string;
            })[];
        };
    };
    DELIVERABLE_RESOURCE_SEARCH_CONDITION: {
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: string;
        relational_operator: string;
    }[];
    ALLOCATION_TYPE: {
        NAME: string;
        DESCRIPTION: AllocationTypes;
    }[];
    DATE_ERROR_MESSAGE: string;
    DELIVERABLE_TYPE_LIST: {
        NAME: string;
        DESCRIPTION: string;
        ICON: string;
    }[];
    DEFAULT_CARD_FIELDS: string[];
};
