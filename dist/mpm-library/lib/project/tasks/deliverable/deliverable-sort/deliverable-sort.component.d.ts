import { OnInit, EventEmitter } from '@angular/core';
import { SharingService } from '../../../../../lib/mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class DeliverableSortComponent implements OnInit {
    sharingService: SharingService;
    sortOptions: any;
    selectedOption: any;
    onSortChange: EventEmitter<any>;
    appConfig: any;
    constructor(sharingService: SharingService);
    selectSorting(): void;
    onSelectionChange(event: any, fields: any): void;
    refreshData(taskConfig: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DeliverableSortComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<DeliverableSortComponent, "mpm-deliverable-sort", never, { "selectedOption": "selectedOption"; "sortOptions": "sortOptions"; }, { "onSortChange": "onSortChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIERlbGl2ZXJhYmxlU29ydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBzb3J0T3B0aW9uczogYW55O1xyXG4gICAgc2VsZWN0ZWRPcHRpb246IGFueTtcclxuICAgIG9uU29ydENoYW5nZTogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIGNvbnN0cnVjdG9yKHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSk7XHJcbiAgICBzZWxlY3RTb3J0aW5nKCk6IHZvaWQ7XHJcbiAgICBvblNlbGVjdGlvbkNoYW5nZShldmVudDogYW55LCBmaWVsZHM6IGFueSk6IHZvaWQ7XHJcbiAgICByZWZyZXNoRGF0YSh0YXNrQ29uZmlnOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=