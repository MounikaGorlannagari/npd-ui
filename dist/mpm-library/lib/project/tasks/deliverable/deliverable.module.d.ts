import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './deliverable-toolbar/deliverable-toolbar.component';
import * as ɵngcc2 from './deliverable-swin-lane/deliverable-swin-lane.component';
import * as ɵngcc3 from './deliverable-card/deliverable-card.component';
import * as ɵngcc4 from './deliverable-sort/deliverable-sort.component';
import * as ɵngcc5 from './bulk-actions/bulk-actions.component';
import * as ɵngcc6 from '@angular/common';
import * as ɵngcc7 from '../../../material.module';
import * as ɵngcc8 from '../../../mpm-library.module';
export declare class DeliverableModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<DeliverableModule, [typeof ɵngcc1.DeliverableToolbarComponent, typeof ɵngcc2.DeliverableSwinLaneComponent, typeof ɵngcc3.DeliverableCardComponent, typeof ɵngcc4.DeliverableSortComponent, typeof ɵngcc5.BulkActionsComponent], [typeof ɵngcc6.CommonModule, typeof ɵngcc7.MaterialModule, typeof ɵngcc8.MpmLibraryModule], [typeof ɵngcc1.DeliverableToolbarComponent, typeof ɵngcc2.DeliverableSwinLaneComponent, typeof ɵngcc3.DeliverableCardComponent, typeof ɵngcc4.DeliverableSortComponent, typeof ɵngcc5.BulkActionsComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<DeliverableModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbImRlbGl2ZXJhYmxlLm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWNsYXJlIGNsYXNzIERlbGl2ZXJhYmxlTW9kdWxlIHtcclxufVxyXG4iXX0=