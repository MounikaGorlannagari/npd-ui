import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import * as ɵngcc0 from '@angular/core';
export declare class DeliverableService {
    appService: AppService;
    DELIVERABLE_NS: string;
    WORKFLOW_NS: string;
    FORCE_DELETE_UPLOAD_DELIVERABLE_WS: string;
    SOFT_DELETE_UPLOAD_DELIVERABLE_WS: string;
    GET_DELIVERABLE_DETAILS_WS: string;
    CLAIM_DELIVERABLE_WS: string;
    PERFORM_DELIVERABLE_ACTION_WS: string;
    REVOKE_DELIVERABLE_WS: string;
    EDIT_DELIVERABLE_WS: string;
    constructor(appService: AppService);
    performDeliverableAction(deliverableId: string, actionId: string): Observable<any>;
    moveToInprogress(deliverableId: string, statusId: string): Observable<any>;
    claimDeliverable(parameter: any): Observable<any>;
    revokeDeliverable(parameter: any): Observable<any>;
    readDeliverable(deliverableId: string): Observable<any>;
    getDeliverableTypeIcon(taskType: string): {
        NAME: string;
        DESCRIPTION: string;
        ICON: string;
    };
    updateDeliverable(parameters: any): Observable<any>;
    forceDeleteDeliverable(parameters: any): Observable<any>;
    softDeleteDeliverable(parameters: any): Observable<any>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DeliverableService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJkZWxpdmVyYWJsZS5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIERlbGl2ZXJhYmxlU2VydmljZSB7XHJcbiAgICBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlO1xyXG4gICAgREVMSVZFUkFCTEVfTlM6IHN0cmluZztcclxuICAgIFdPUktGTE9XX05TOiBzdHJpbmc7XHJcbiAgICBGT1JDRV9ERUxFVEVfVVBMT0FEX0RFTElWRVJBQkxFX1dTOiBzdHJpbmc7XHJcbiAgICBTT0ZUX0RFTEVURV9VUExPQURfREVMSVZFUkFCTEVfV1M6IHN0cmluZztcclxuICAgIEdFVF9ERUxJVkVSQUJMRV9ERVRBSUxTX1dTOiBzdHJpbmc7XHJcbiAgICBDTEFJTV9ERUxJVkVSQUJMRV9XUzogc3RyaW5nO1xyXG4gICAgUEVSRk9STV9ERUxJVkVSQUJMRV9BQ1RJT05fV1M6IHN0cmluZztcclxuICAgIFJFVk9LRV9ERUxJVkVSQUJMRV9XUzogc3RyaW5nO1xyXG4gICAgRURJVF9ERUxJVkVSQUJMRV9XUzogc3RyaW5nO1xyXG4gICAgY29uc3RydWN0b3IoYXBwU2VydmljZTogQXBwU2VydmljZSk7XHJcbiAgICBwZXJmb3JtRGVsaXZlcmFibGVBY3Rpb24oZGVsaXZlcmFibGVJZDogc3RyaW5nLCBhY3Rpb25JZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgbW92ZVRvSW5wcm9ncmVzcyhkZWxpdmVyYWJsZUlkOiBzdHJpbmcsIHN0YXR1c0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBjbGFpbURlbGl2ZXJhYmxlKHBhcmFtZXRlcjogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgcmV2b2tlRGVsaXZlcmFibGUocGFyYW1ldGVyOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICByZWFkRGVsaXZlcmFibGUoZGVsaXZlcmFibGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0RGVsaXZlcmFibGVUeXBlSWNvbih0YXNrVHlwZTogc3RyaW5nKToge1xyXG4gICAgICAgIE5BTUU6IHN0cmluZztcclxuICAgICAgICBERVNDUklQVElPTjogc3RyaW5nO1xyXG4gICAgICAgIElDT046IHN0cmluZztcclxuICAgIH07XHJcbiAgICB1cGRhdGVEZWxpdmVyYWJsZShwYXJhbWV0ZXJzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBmb3JjZURlbGV0ZURlbGl2ZXJhYmxlKHBhcmFtZXRlcnM6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHNvZnREZWxldGVEZWxpdmVyYWJsZShwYXJhbWV0ZXJzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbn1cclxuIl19