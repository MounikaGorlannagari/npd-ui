import { OnInit, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../../../../notification/notification.service';
import { LoaderService } from '../../../../loader/loader.service';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../../../mpm-utils/services/entity.appdef.service';
import { Observable } from 'rxjs';
import { AppService } from '../../../../mpm-utils/services/app.service';
import { StatusService } from '../../../../shared/services/status.service';
import * as ɵngcc0 from '@angular/core';
export declare class BulkActionsComponent implements OnInit {
    dialog: MatDialog;
    notificationService: NotificationService;
    loaderService: LoaderService;
    sharingService: SharingService;
    entityAppdefService: EntityAppDefService;
    appService: AppService;
    statusService: StatusService;
    enableAction: any;
    delCount: any;
    deliverableData: any;
    projectData: any;
    unselectDeliverable: EventEmitter<any>;
    bulkResponse: EventEmitter<any>;
    bulkResponseData: EventEmitter<any>;
    selectedProjectData: any;
    categoryLevel: any;
    priorityList: any;
    isApprovalTask: any;
    teamId: any;
    teamRolesOptions: any;
    teamRoleUserOptions: any;
    selectedRole: any;
    selectedAllocation: any;
    bulkActions: boolean;
    crRole: any;
    bulkStatus: any;
    statusId: any;
    statusOptions: any;
    bulkStatusAction: any;
    projectStatus: {
        REQUIRE_REASON: string;
        REQUIRE_COMMENTS: string;
        MPM_Status_Reason: string;
    };
    constructor(dialog: MatDialog, notificationService: NotificationService, loaderService: LoaderService, sharingService: SharingService, entityAppdefService: EntityAppDefService, appService: AppService, statusService: StatusService);
    bulkAction(data: any, bulkAction: any): void;
    getBulkActionData(appId: any, deliverableId: any, assetId: any, crRoleId: any, versioning: any, taskName: any, bulkComment: any, isBulkApprove: any, statusId: any, selectedReasonsData: any): Observable<any>;
    performBulkAction(action: 'Approve' | 'Reject' | 'Complete', appId: any, deliverableData: any, assetId: any, crRole: any, versioning: any, taskName: any, bulkComment: any, isBulkApprove: any, selectedReasonsData: any): void;
    masterToogle(event: any): void;
    getPriorities(): Observable<any>;
    getAllRoles(): Observable<unknown>;
    getUsersByRole(): Observable<any>;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<BulkActionsComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<BulkActionsComponent, "mpm-bulk-actions", never, { "enableAction": "enableAction"; "delCount": "delCount"; "deliverableData": "deliverableData"; "projectData": "projectData"; "selectedProjectData": "selectedProjectData"; "categoryLevel": "categoryLevel"; }, { "unselectDeliverable": "unselectDeliverable"; "bulkResponse": "bulkResponse"; "bulkResponseData": "bulkResponseData"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVsay1hY3Rpb25zLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJidWxrLWFjdGlvbnMuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvc3RhdHVzLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBCdWxrQWN0aW9uc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBkaWFsb2c6IE1hdERpYWxvZztcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlO1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZTtcclxuICAgIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2U7XHJcbiAgICBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlO1xyXG4gICAgZW5hYmxlQWN0aW9uOiBhbnk7XHJcbiAgICBkZWxDb3VudDogYW55O1xyXG4gICAgZGVsaXZlcmFibGVEYXRhOiBhbnk7XHJcbiAgICBwcm9qZWN0RGF0YTogYW55O1xyXG4gICAgdW5zZWxlY3REZWxpdmVyYWJsZTogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBidWxrUmVzcG9uc2U6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgYnVsa1Jlc3BvbnNlRGF0YTogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBzZWxlY3RlZFByb2plY3REYXRhOiBhbnk7XHJcbiAgICBjYXRlZ29yeUxldmVsOiBhbnk7XHJcbiAgICBwcmlvcml0eUxpc3Q6IGFueTtcclxuICAgIGlzQXBwcm92YWxUYXNrOiBhbnk7XHJcbiAgICB0ZWFtSWQ6IGFueTtcclxuICAgIHRlYW1Sb2xlc09wdGlvbnM6IGFueTtcclxuICAgIHRlYW1Sb2xlVXNlck9wdGlvbnM6IGFueTtcclxuICAgIHNlbGVjdGVkUm9sZTogYW55O1xyXG4gICAgc2VsZWN0ZWRBbGxvY2F0aW9uOiBhbnk7XHJcbiAgICBidWxrQWN0aW9uczogYm9vbGVhbjtcclxuICAgIGNyUm9sZTogYW55O1xyXG4gICAgYnVsa1N0YXR1czogYW55O1xyXG4gICAgc3RhdHVzSWQ6IGFueTtcclxuICAgIHN0YXR1c09wdGlvbnM6IGFueTtcclxuICAgIGJ1bGtTdGF0dXNBY3Rpb246IGFueTtcclxuICAgIHByb2plY3RTdGF0dXM6IHtcclxuICAgICAgICBSRVFVSVJFX1JFQVNPTjogc3RyaW5nO1xyXG4gICAgICAgIFJFUVVJUkVfQ09NTUVOVFM6IHN0cmluZztcclxuICAgICAgICBNUE1fU3RhdHVzX1JlYXNvbjogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIGNvbnN0cnVjdG9yKGRpYWxvZzogTWF0RGlhbG9nLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIGVudGl0eUFwcGRlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsIHN0YXR1c1NlcnZpY2U6IFN0YXR1c1NlcnZpY2UpO1xyXG4gICAgYnVsa0FjdGlvbihkYXRhOiBhbnksIGJ1bGtBY3Rpb246IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRCdWxrQWN0aW9uRGF0YShhcHBJZDogYW55LCBkZWxpdmVyYWJsZUlkOiBhbnksIGFzc2V0SWQ6IGFueSwgY3JSb2xlSWQ6IGFueSwgdmVyc2lvbmluZzogYW55LCB0YXNrTmFtZTogYW55LCBidWxrQ29tbWVudDogYW55LCBpc0J1bGtBcHByb3ZlOiBhbnksIHN0YXR1c0lkOiBhbnksIHNlbGVjdGVkUmVhc29uc0RhdGE6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIHBlcmZvcm1CdWxrQWN0aW9uKGFjdGlvbjogJ0FwcHJvdmUnIHwgJ1JlamVjdCcgfCAnQ29tcGxldGUnLCBhcHBJZDogYW55LCBkZWxpdmVyYWJsZURhdGE6IGFueSwgYXNzZXRJZDogYW55LCBjclJvbGU6IGFueSwgdmVyc2lvbmluZzogYW55LCB0YXNrTmFtZTogYW55LCBidWxrQ29tbWVudDogYW55LCBpc0J1bGtBcHByb3ZlOiBhbnksIHNlbGVjdGVkUmVhc29uc0RhdGE6IGFueSk6IHZvaWQ7XHJcbiAgICBtYXN0ZXJUb29nbGUoZXZlbnQ6IGFueSk6IHZvaWQ7XHJcbiAgICBnZXRQcmlvcml0aWVzKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldEFsbFJvbGVzKCk6IE9ic2VydmFibGU8dW5rbm93bj47XHJcbiAgICBnZXRVc2Vyc0J5Um9sZSgpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==