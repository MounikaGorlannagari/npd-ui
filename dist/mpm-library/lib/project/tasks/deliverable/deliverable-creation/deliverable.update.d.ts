export interface DeliverableUpdateObject {
    'DeliverableId'?: {
        'Id'?: string;
        'ItemId'?: string;
    };
    'DeliverableName'?: string;
    'Description'?: string;
    'StartDate'?: string;
    'EndDate'?: string;
    'DueDate'?: string;
    'IsActive'?: boolean;
    'IsDeleted'?: boolean;
    'IterationCount'?: string;
    'DeliverableType'?: string;
    'OwnerAssignmentType'?: string;
    'ApproverAssignmentType'?: string;
    'DeliverableParentID'?: string;
    'DeliverableSequence'?: string;
    'IsUploaded'?: string;
    'OTMMAssetID'?: string;
    'ReferenceLinkID'?: string;
    'ExpectedDuration'?: number;
    'RPOStatus'?: {
        'MPMStatusID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPOPriority'?: {
        'MPMPriorityID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPOTask'?: {
        'TaskID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPOProject'?: {
        'ProjectID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPOOwnerRoleID'?: {
        'MPMTeamRoleMappingID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPOApproverRoleID'?: {
        'MPMTeamRoleMappingID'?: {
            'Id'?: string;
            'ItemId'?: string;
        };
    };
    'RPODeliverableOwnerID'?: {
        'IdentityID'?: {
            'Id'?: string;
            'ItemId'?: string;
            'userID'?: string;
        };
    };
    'RPODeliverableApproverID'?: {
        'IdentityID'?: {
            'Id'?: string;
            'ItemId'?: string;
            'userID'?: string;
        };
    };
    'RPOOwnerActiveUserID'?: {
        'IdentityID'?: {
            'Id'?: string;
            'ItemId'?: string;
            'userID'?: string;
        };
    };
    'RPOApproverActiveUserID'?: {
        'IdentityID'?: {
            'Id'?: string;
            'ItemId'?: string;
            'userID'?: string;
        };
    };
    'CustomMetadata'?: {};
}
