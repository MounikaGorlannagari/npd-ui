import { OnInit, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { DeliverableSortComponent } from '../deliverable-sort/deliverable-sort.component';
import { TaskService } from '../../task.service';
import * as ɵngcc0 from '@angular/core';
export declare class DeliverableToolbarComponent implements OnInit {
    sharingService: SharingService;
    taskService: TaskService;
    deliverableConfig: any;
    isViewByResourceFilterEnabled: any;
    resouceFilterChange: EventEmitter<any>;
    deliverableConfigChange: EventEmitter<any>;
    createNewDeliverableHandler: EventEmitter<any>;
    enableAction: any;
    delCount: any;
    crBulkResponse: any;
    isProjectCompleted: boolean;
    showTaskViewButton: boolean;
    selectedOption: any;
    viewByResources: FormControl;
    taskConstants: {
        CATEGORY_LEVEL_TASK: string;
        STATUS_TYPE_INITIAL: string;
        ALLOCATION_TYPE_ROLE: string;
        ALLOCATION_TYPE_USER: string;
        TASK_TYPE: {
            NORMAL_TASK: string;
            TASK_WITHOUT_DELIVERABLE: string;
            APPROVAL_TASK: string;
        };
        TASK_ITEM_ID_METADATAFIELD_ID: string;
        dataTypes: {
            STRING: string;
            DATE: string;
        };
        StatusConstant: {
            TASK_STATUS: {
                FINAL: string;
                ACCEPTED: string;
                REWORK: string;
                CANCELLED: string;
            };
        };
        GET_ALL_TASKS_SEARCH_CONDITION_LIST: {
            search_condition_list: {
                search_condition: ({
                    type: import("../../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../../public-api").MPMSearchOperatorNames;
                    value: string;
                    relational_operator?: undefined;
                } | {
                    type: import("../../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../../public-api").MPMSearchOperatorNames;
                    value: import("../../../../../public-api").OTMMMPMDataTypes;
                    relational_operator: string;
                })[];
            };
        };
        ACTIVE_TASK_SEARCH_CONDITION: {
            type: import("../../../../../public-api").IndexerDataTypes;
            field_id: string;
            relational_operator_id: import("../../../../../public-api").MPMSearchOperators;
            relational_operator_name: import("../../../../../public-api").MPMSearchOperatorNames;
            value: string;
            relational_operator: string;
        };
        NOTIFICATION_LABELS: {
            ERROR: string;
            SUCCESS: string;
            INFO: string;
            WARN: string;
        };
        ALLOCATION_TYPE: {
            NAME: string;
            DESCRIPTION: import("../../../../../public-api").AllocationTypes;
        }[];
        TASK_TYPE_LIST: {
            NAME: import("../../../../../public-api").TaskTypes;
            DESCRIPTION: string;
            ICON: string;
        }[];
        NAME_STRING_PATTERN: string;
        PROJECT_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_MAX_DATE_ERROR_MESSAGE: string;
        START_DATE_ERROR_MESSAGE: string;
        DURATION_ERROR_MESSAGE: string;
        DURATION_MIN_ERROR_MESSAGE: string;
        DEFAULT_CARD_FIELDS: string[];
    };
    assetStatusConstants: {
        CANCELLED: string;
        REQUESTED_CHANGES: string;
        APPROVED: string;
        IN_PROGRESS: string;
        DEFINED: string;
        DRAFT: string;
        COMPLETED: string;
        REJECTED: string;
    };
    disableAction: boolean;
    isOnHoldProject: boolean;
    deliverableSortComponent: DeliverableSortComponent;
    constructor(sharingService: SharingService, taskService: TaskService);
    createNewDeliverable(): void;
    selectResource(resourceName: any): void;
    refresh(): void;
    changeView(): void;
    onSortChange(eventData: any): void;
    refreshChild(deliverableConfig: any): void;
    onResourceSelect(): void;
    onResourceRemoved(resource: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<DeliverableToolbarComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<DeliverableToolbarComponent, "mpm-deliverable-toolbar", never, { "deliverableConfig": "deliverableConfig"; "isViewByResourceFilterEnabled": "isViewByResourceFilterEnabled"; "enableAction": "enableAction"; "delCount": "delCount"; "crBulkResponse": "crBulkResponse"; }, { "resouceFilterChange": "resouceFilterChange"; "deliverableConfigChange": "deliverableConfigChange"; "createNewDeliverableHandler": "createNewDeliverableHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsiZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQgfSBmcm9tICcuLi9kZWxpdmVyYWJsZS1zb3J0L2RlbGl2ZXJhYmxlLXNvcnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi8uLi90YXNrLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBEZWxpdmVyYWJsZVRvb2xiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlO1xyXG4gICAgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlO1xyXG4gICAgZGVsaXZlcmFibGVDb25maWc6IGFueTtcclxuICAgIGlzVmlld0J5UmVzb3VyY2VGaWx0ZXJFbmFibGVkOiBhbnk7XHJcbiAgICByZXNvdWNlRmlsdGVyQ2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGRlbGl2ZXJhYmxlQ29uZmlnQ2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGNyZWF0ZU5ld0RlbGl2ZXJhYmxlSGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBlbmFibGVBY3Rpb246IGFueTtcclxuICAgIGRlbENvdW50OiBhbnk7XHJcbiAgICBjckJ1bGtSZXNwb25zZTogYW55O1xyXG4gICAgaXNQcm9qZWN0Q29tcGxldGVkOiBib29sZWFuO1xyXG4gICAgc2hvd1Rhc2tWaWV3QnV0dG9uOiBib29sZWFuO1xyXG4gICAgc2VsZWN0ZWRPcHRpb246IGFueTtcclxuICAgIHZpZXdCeVJlc291cmNlczogRm9ybUNvbnRyb2w7XHJcbiAgICB0YXNrQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FURUdPUllfTEVWRUxfVEFTSzogc3RyaW5nO1xyXG4gICAgICAgIFNUQVRVU19UWVBFX0lOSVRJQUw6IHN0cmluZztcclxuICAgICAgICBBTExPQ0FUSU9OX1RZUEVfUk9MRTogc3RyaW5nO1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRV9VU0VSOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19UWVBFOiB7XHJcbiAgICAgICAgICAgIE5PUk1BTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfV0lUSE9VVF9ERUxJVkVSQUJMRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBBUFBST1ZBTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBUQVNLX0lURU1fSURfTUVUQURBVEFGSUVMRF9JRDogc3RyaW5nO1xyXG4gICAgICAgIGRhdGFUeXBlczoge1xyXG4gICAgICAgICAgICBTVFJJTkc6IHN0cmluZztcclxuICAgICAgICAgICAgREFURTogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgU3RhdHVzQ29uc3RhbnQ6IHtcclxuICAgICAgICAgICAgVEFTS19TVEFUVVM6IHtcclxuICAgICAgICAgICAgICAgIEZJTkFMOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBBQ0NFUFRFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgUkVXT1JLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBDQU5DRUxMRUQ6IHN0cmluZztcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEdFVF9BTExfVEFTS1NfU0VBUkNIX0NPTkRJVElPTl9MSVNUOiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogKHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I/OiB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICB9IHwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGltcG9ydChcIi4uLy4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuSW5kZXhlckRhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvck5hbWVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk9UTU1NUE1EYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgfSlbXTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFDVElWRV9UQVNLX1NFQVJDSF9DT05ESVRJT046IHtcclxuICAgICAgICAgICAgdHlwZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5JbmRleGVyRGF0YVR5cGVzO1xyXG4gICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9yTmFtZXM7XHJcbiAgICAgICAgICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIE5PVElGSUNBVElPTl9MQUJFTFM6IHtcclxuICAgICAgICAgICAgRVJST1I6IHN0cmluZztcclxuICAgICAgICAgICAgU1VDQ0VTUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBJTkZPOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFdBUk46IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRToge1xyXG4gICAgICAgICAgICBOQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkFsbG9jYXRpb25UeXBlcztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgVEFTS19UWVBFX0xJU1Q6IHtcclxuICAgICAgICAgICAgTkFNRTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5UYXNrVHlwZXM7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElDT046IHN0cmluZztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgTkFNRV9TVFJJTkdfUEFUVEVSTjogc3RyaW5nO1xyXG4gICAgICAgIFBST0pFQ1RfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfTUFYX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIFNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERVUkFUSU9OX0VSUk9SX01FU1NBR0U6IHN0cmluZztcclxuICAgICAgICBEVVJBVElPTl9NSU5fRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERFRkFVTFRfQ0FSRF9GSUVMRFM6IHN0cmluZ1tdO1xyXG4gICAgfTtcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FOQ0VMTEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVRVUVTVEVEX0NIQU5HRVM6IHN0cmluZztcclxuICAgICAgICBBUFBST1ZFRDogc3RyaW5nO1xyXG4gICAgICAgIElOX1BST0dSRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgREVGSU5FRDogc3RyaW5nO1xyXG4gICAgICAgIERSQUZUOiBzdHJpbmc7XHJcbiAgICAgICAgQ09NUExFVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVKRUNURUQ6IHN0cmluZztcclxuICAgIH07XHJcbiAgICBkaXNhYmxlQWN0aW9uOiBib29sZWFuO1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBib29sZWFuO1xyXG4gICAgZGVsaXZlcmFibGVTb3J0Q29tcG9uZW50OiBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQ7XHJcbiAgICBjb25zdHJ1Y3RvcihzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSk7XHJcbiAgICBjcmVhdGVOZXdEZWxpdmVyYWJsZSgpOiB2b2lkO1xyXG4gICAgc2VsZWN0UmVzb3VyY2UocmVzb3VyY2VOYW1lOiBhbnkpOiB2b2lkO1xyXG4gICAgcmVmcmVzaCgpOiB2b2lkO1xyXG4gICAgY2hhbmdlVmlldygpOiB2b2lkO1xyXG4gICAgb25Tb3J0Q2hhbmdlKGV2ZW50RGF0YTogYW55KTogdm9pZDtcclxuICAgIHJlZnJlc2hDaGlsZChkZWxpdmVyYWJsZUNvbmZpZzogYW55KTogdm9pZDtcclxuICAgIG9uUmVzb3VyY2VTZWxlY3QoKTogdm9pZDtcclxuICAgIG9uUmVzb3VyY2VSZW1vdmVkKHJlc291cmNlOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=