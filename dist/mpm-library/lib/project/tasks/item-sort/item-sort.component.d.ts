import { OnInit, EventEmitter } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class ItemSortComponent implements OnInit {
    sharingService: SharingService;
    sortOptions: any;
    selectedOption: any;
    onSortChange: EventEmitter<any>;
    appConfig: any;
    constructor(sharingService: SharingService);
    selectSorting(): void;
    onSelectionChange(event: any, fields: any): void;
    refreshData(taskConfig: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ItemSortComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ItemSortComponent, "mpm-item-sort", never, { "selectedOption": "selectedOption"; "sortOptions": "sortOptions"; }, { "onSortChange": "onSortChange"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1zb3J0LmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJpdGVtLXNvcnQuY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEl0ZW1Tb3J0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIHNvcnRPcHRpb25zOiBhbnk7XHJcbiAgICBzZWxlY3RlZE9wdGlvbjogYW55O1xyXG4gICAgb25Tb3J0Q2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgY29uc3RydWN0b3Ioc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlKTtcclxuICAgIHNlbGVjdFNvcnRpbmcoKTogdm9pZDtcclxuICAgIG9uU2VsZWN0aW9uQ2hhbmdlKGV2ZW50OiBhbnksIGZpZWxkczogYW55KTogdm9pZDtcclxuICAgIHJlZnJlc2hEYXRhKHRhc2tDb25maWc6IGFueSk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==