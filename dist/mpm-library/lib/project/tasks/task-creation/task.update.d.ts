export interface TaskUpdateObj {
    'TaskId': {
        'Id': string;
    };
    'TaskName': string;
    'Description': string;
    'StartDate': string;
    'DueDate': string;
    'TaskDuration': string;
    'TaskDurationType': string;
    'ParentTaskID': string;
    'RevisionReviewRequired': boolean;
    'IsMilestone': boolean;
    'IsTaskRequiredonRejection': boolean;
    'IsApprovalTask': boolean;
    'AssignmentType': string;
    'TaskType': string;
    'RPOStatus': {
        'StatusID': {
            'Id': string;
        };
    };
    'RPOTeamRole': {
        'TeamRole': {
            'Id': string;
        };
    };
    'RPOOwnerUser': {
        'User': {
            'userID': string;
        };
    };
    'RPOPriority': {
        'PriorityID': {
            'Id': string;
        };
    };
    'RPOProject': {
        'ProjectID': {
            'Id': string;
        };
    };
    'Reviewers': any;
}
