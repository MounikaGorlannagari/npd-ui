import { OnInit, EventEmitter } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { TaskUpdateObj } from './task.update';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { TaskService } from '../task.service';
import { EntityAppDefService } from '../../../mpm-utils/services/entity.appdef.service';
import { LoaderService } from '../../../loader/loader.service';
import { MatDialog } from '@angular/material/dialog';
import { ProjectUtilService } from '../../shared/services/project-util.service';
import { TaskConfigInterface } from '../task.config.interface';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ElementRef } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { NotificationService } from '../../../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class TaskCreationComponent implements OnInit {
    appService: AppService;
    sharingService: SharingService;
    adapter: DateAdapter<any>;
    utilService: UtilService;
    taskService: TaskService;
    entityAppdefService: EntityAppDefService;
    loaderService: LoaderService;
    dialog: MatDialog;
    projectUtilService: ProjectUtilService;
    commentsUtilService: CommentsUtilService;
    notificationService: NotificationService;
    taskConfig: TaskConfigInterface;
    closeCallbackHandler: EventEmitter<any>;
    saveCallBackHandler: EventEmitter<any>;
    notificationHandler: EventEmitter<any>;
    taskForm: FormGroup;
    taskModalConfig: any;
    teamRolesOptions: any;
    teamRoleUserOptions: any;
    taskStatus: any;
    selectedOwner: any;
    isInvalidTaskOperation: boolean;
    ownerProjectCount: boolean;
    ownerProjectCountMessage: string;
    isZeroOwnerProjectCount: boolean;
    showRevisionReviewRequired: boolean;
    disableTaskSave: boolean;
    existingTaskTypeDetails: any;
    taskConstants: {
        CATEGORY_LEVEL_TASK: string;
        STATUS_TYPE_INITIAL: string;
        ALLOCATION_TYPE_ROLE: string;
        ALLOCATION_TYPE_USER: string;
        TASK_TYPE: {
            NORMAL_TASK: string;
            TASK_WITHOUT_DELIVERABLE: string;
            APPROVAL_TASK: string;
        };
        TASK_ITEM_ID_METADATAFIELD_ID: string;
        dataTypes: {
            STRING: string;
            DATE: string;
        };
        StatusConstant: {
            TASK_STATUS: {
                FINAL: string;
                ACCEPTED: string;
                REWORK: string;
                CANCELLED: string;
            };
        };
        GET_ALL_TASKS_SEARCH_CONDITION_LIST: {
            search_condition_list: {
                search_condition: ({
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: string;
                    relational_operator?: undefined;
                } | {
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: import("../../../../public-api").OTMMMPMDataTypes;
                    relational_operator: string;
                })[];
            };
        };
        ACTIVE_TASK_SEARCH_CONDITION: {
            type: import("../../../../public-api").IndexerDataTypes;
            field_id: string;
            relational_operator_id: import("../../../../public-api").MPMSearchOperators;
            relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
            value: string;
            relational_operator: string;
        };
        NOTIFICATION_LABELS: {
            ERROR: string;
            SUCCESS: string;
            INFO: string;
            WARN: string;
        };
        ALLOCATION_TYPE: {
            NAME: string;
            DESCRIPTION: import("../../../../public-api").AllocationTypes;
        }[];
        TASK_TYPE_LIST: {
            NAME: import("../../../../public-api").TaskTypes;
            DESCRIPTION: string;
            ICON: string;
        }[];
        NAME_STRING_PATTERN: string;
        PROJECT_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_MAX_DATE_ERROR_MESSAGE: string;
        START_DATE_ERROR_MESSAGE: string;
        DURATION_ERROR_MESSAGE: string;
        DURATION_MIN_ERROR_MESSAGE: string;
        DEFAULT_CARD_FIELDS: string[];
    };
    selectedStatusInfo: any;
    oldTaskObject: TaskUpdateObj;
    dateValidation: {
        startMaxDate: Date;
        endMinDate: Date;
    };
    maxDuration: any;
    durationErrorMessage: any;
    defaultRevisionRequiredStatus: boolean;
    projectConfig: any;
    appConfig: any;
    nameStringPattern: any;
    allowActionTask: any;
    reviewerInput: ElementRef;
    savedReviewers: any[];
    allReviewers: any[];
    deliverableReviewers: any[];
    reviewerStatusData: any[];
    filteredReviewers: Observable<any[]>;
    newreviewersObj: any;
    removedreviewersObj: any;
    reviewersObj: any;
    selectedReviewersObj: any;
    visible: boolean;
    selectable: boolean;
    removable: boolean;
    addOnBlur: boolean;
    separatorKeysCodes: number[];
    newReviewers: any[];
    removedReviewers: any[];
    loggedInUser: any;
    dateErrorMessage: any;
    isPM: boolean;
    chipChanged: any;
    saveOptions: {
        name: string;
        value: string;
    }[];
    SELECTED_SAVE_OPTION: {
        name: string;
        value: string;
    };
    DurationType: {
        value: string;
        viewValue: string;
    }[];
    enableDuration: boolean;
    enableWorkWeek: boolean;
    reviewerCompleted: any;
    keyRestriction: {
        NUMBER: number[];
    };
    matAutocomplete: MatAutocomplete;
    constructor(appService: AppService, sharingService: SharingService, adapter: DateAdapter<any>, utilService: UtilService, taskService: TaskService, entityAppdefService: EntityAppDefService, loaderService: LoaderService, dialog: MatDialog, projectUtilService: ProjectUtilService, commentsUtilService: CommentsUtilService, notificationService: NotificationService);
    filterReviewers(value: any): any[];
    restrictKeysOnType(event: any, datatype: any): void;
    selected(event: MatAutocompleteSelectedEvent): void;
    addReviewer(event: MatChipInputEvent): void;
    onRemoveReviewer(reviewerValue: any): void;
    getProjectDetails(projectObj: any): Observable<any>;
    getPriorities(): Observable<any>;
    getStatus(): Observable<any>;
    getInitialStatus(): Observable<any>;
    getAllRoles(): Observable<unknown>;
    getReviewerById(taskUserID: any): Observable<any>;
    getDeliverableReviewByTaskId(TaskId: any): Observable<unknown>;
    getReviewers(): Observable<any>;
    getUsersByRole(): Observable<any>;
    getTaskTypeDetails(taskTypeId: string): Observable<unknown>;
    getDependentTasks(isApprovalTask: boolean): Observable<any>;
    getPreDetails(): void;
    getTaskFields(task: any): {
        isApprovalTaskSelected: boolean;
        showOtherComments: boolean;
        isOtherCommentsSelected: boolean;
        showPredecessor: boolean;
        enableDeliverableApprover: boolean;
        showDeliveryPackage: boolean;
    };
    getPredecessortask(predecessorId: any): any;
    initialiseTaskForm(): void;
    validateDuration(): void;
    onChangeofAllocationType(allocationTypeValue: any, ownerValue: any, roleValue: any, changeDeliverableApprover: any): void;
    onChangeofRole(role: any): void;
    onApprovalStateChange(): void;
    formPredecessorValue(task: any): string;
    formPredecessor(taskId: any): any;
    formOwnerValue(taskUserID: any): Observable<any>;
    formRoleValue(taskRoleID: any): any;
    getTaskOwnerAssignmentCount(): void;
    cancelTaskCreation(): void;
    updateTask(saveOption: any): void;
    formReviewerObj(): void;
    updateTaskDetails(saveOption: any): void;
    saveTask(TaskObject: any, saveOption: any): void;
    initialiseTaskConfig(): void;
    ngOnInit(): void;
    dateFilter: (date: Date | null) => boolean;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<TaskCreationComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<TaskCreationComponent, "mpm-task-creation", never, { "taskConfig": "taskConfig"; }, { "closeCallbackHandler": "closeCallbackHandler"; "saveCallBackHandler": "saveCallBackHandler"; "notificationHandler": "notificationHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jcmVhdGlvbi5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsidGFzay1jcmVhdGlvbi5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY29yZSc7XHJcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IFRhc2tVcGRhdGVPYmogfSBmcm9tICcuL3Rhc2sudXBkYXRlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBQcm9qZWN0VXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC11dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uZmlnSW50ZXJmYWNlIH0gZnJvbSAnLi4vdGFzay5jb25maWcuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgQ29tbWVudHNVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2NvbW1lbnRzL3NlcnZpY2VzL2NvbW1lbnRzLnV0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9hdXRvY29tcGxldGUnO1xyXG5pbXBvcnQgeyBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgVGFza0NyZWF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2U7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+O1xyXG4gICAgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlO1xyXG4gICAgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlO1xyXG4gICAgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZTtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICBkaWFsb2c6IE1hdERpYWxvZztcclxuICAgIHByb2plY3RVdGlsU2VydmljZTogUHJvamVjdFV0aWxTZXJ2aWNlO1xyXG4gICAgY29tbWVudHNVdGlsU2VydmljZTogQ29tbWVudHNVdGlsU2VydmljZTtcclxuICAgIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2U7XHJcbiAgICB0YXNrQ29uZmlnOiBUYXNrQ29uZmlnSW50ZXJmYWNlO1xyXG4gICAgY2xvc2VDYWxsYmFja0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgc2F2ZUNhbGxCYWNrSGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBub3RpZmljYXRpb25IYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIHRhc2tGb3JtOiBGb3JtR3JvdXA7XHJcbiAgICB0YXNrTW9kYWxDb25maWc6IGFueTtcclxuICAgIHRlYW1Sb2xlc09wdGlvbnM6IGFueTtcclxuICAgIHRlYW1Sb2xlVXNlck9wdGlvbnM6IGFueTtcclxuICAgIHRhc2tTdGF0dXM6IGFueTtcclxuICAgIHNlbGVjdGVkT3duZXI6IGFueTtcclxuICAgIGlzSW52YWxpZFRhc2tPcGVyYXRpb246IGJvb2xlYW47XHJcbiAgICBvd25lclByb2plY3RDb3VudDogYm9vbGVhbjtcclxuICAgIG93bmVyUHJvamVjdENvdW50TWVzc2FnZTogc3RyaW5nO1xyXG4gICAgaXNaZXJvT3duZXJQcm9qZWN0Q291bnQ6IGJvb2xlYW47XHJcbiAgICBzaG93UmV2aXNpb25SZXZpZXdSZXF1aXJlZDogYm9vbGVhbjtcclxuICAgIGRpc2FibGVUYXNrU2F2ZTogYm9vbGVhbjtcclxuICAgIGV4aXN0aW5nVGFza1R5cGVEZXRhaWxzOiBhbnk7XHJcbiAgICB0YXNrQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FURUdPUllfTEVWRUxfVEFTSzogc3RyaW5nO1xyXG4gICAgICAgIFNUQVRVU19UWVBFX0lOSVRJQUw6IHN0cmluZztcclxuICAgICAgICBBTExPQ0FUSU9OX1RZUEVfUk9MRTogc3RyaW5nO1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRV9VU0VSOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19UWVBFOiB7XHJcbiAgICAgICAgICAgIE5PUk1BTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfV0lUSE9VVF9ERUxJVkVSQUJMRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBBUFBST1ZBTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBUQVNLX0lURU1fSURfTUVUQURBVEFGSUVMRF9JRDogc3RyaW5nO1xyXG4gICAgICAgIGRhdGFUeXBlczoge1xyXG4gICAgICAgICAgICBTVFJJTkc6IHN0cmluZztcclxuICAgICAgICAgICAgREFURTogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgU3RhdHVzQ29uc3RhbnQ6IHtcclxuICAgICAgICAgICAgVEFTS19TVEFUVVM6IHtcclxuICAgICAgICAgICAgICAgIEZJTkFMOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBBQ0NFUFRFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgUkVXT1JLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBDQU5DRUxMRUQ6IHN0cmluZztcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEdFVF9BTExfVEFTS1NfU0VBUkNIX0NPTkRJVElPTl9MSVNUOiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogKHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I/OiB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICB9IHwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuSW5kZXhlckRhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvck5hbWVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk9UTU1NUE1EYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgfSlbXTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFDVElWRV9UQVNLX1NFQVJDSF9DT05ESVRJT046IHtcclxuICAgICAgICAgICAgdHlwZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5JbmRleGVyRGF0YVR5cGVzO1xyXG4gICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9yTmFtZXM7XHJcbiAgICAgICAgICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIE5PVElGSUNBVElPTl9MQUJFTFM6IHtcclxuICAgICAgICAgICAgRVJST1I6IHN0cmluZztcclxuICAgICAgICAgICAgU1VDQ0VTUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBJTkZPOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFdBUk46IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRToge1xyXG4gICAgICAgICAgICBOQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkFsbG9jYXRpb25UeXBlcztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgVEFTS19UWVBFX0xJU1Q6IHtcclxuICAgICAgICAgICAgTkFNRTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5UYXNrVHlwZXM7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElDT046IHN0cmluZztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgTkFNRV9TVFJJTkdfUEFUVEVSTjogc3RyaW5nO1xyXG4gICAgICAgIFBST0pFQ1RfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfTUFYX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIFNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERVUkFUSU9OX0VSUk9SX01FU1NBR0U6IHN0cmluZztcclxuICAgICAgICBEVVJBVElPTl9NSU5fRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERFRkFVTFRfQ0FSRF9GSUVMRFM6IHN0cmluZ1tdO1xyXG4gICAgfTtcclxuICAgIHNlbGVjdGVkU3RhdHVzSW5mbzogYW55O1xyXG4gICAgb2xkVGFza09iamVjdDogVGFza1VwZGF0ZU9iajtcclxuICAgIGRhdGVWYWxpZGF0aW9uOiB7XHJcbiAgICAgICAgc3RhcnRNYXhEYXRlOiBEYXRlO1xyXG4gICAgICAgIGVuZE1pbkRhdGU6IERhdGU7XHJcbiAgICB9O1xyXG4gICAgbWF4RHVyYXRpb246IGFueTtcclxuICAgIGR1cmF0aW9uRXJyb3JNZXNzYWdlOiBhbnk7XHJcbiAgICBkZWZhdWx0UmV2aXNpb25SZXF1aXJlZFN0YXR1czogYm9vbGVhbjtcclxuICAgIHByb2plY3RDb25maWc6IGFueTtcclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgbmFtZVN0cmluZ1BhdHRlcm46IGFueTtcclxuICAgIGFsbG93QWN0aW9uVGFzazogYW55O1xyXG4gICAgcmV2aWV3ZXJJbnB1dDogRWxlbWVudFJlZjtcclxuICAgIHNhdmVkUmV2aWV3ZXJzOiBhbnlbXTtcclxuICAgIGFsbFJldmlld2VyczogYW55W107XHJcbiAgICBkZWxpdmVyYWJsZVJldmlld2VyczogYW55W107XHJcbiAgICByZXZpZXdlclN0YXR1c0RhdGE6IGFueVtdO1xyXG4gICAgZmlsdGVyZWRSZXZpZXdlcnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gICAgbmV3cmV2aWV3ZXJzT2JqOiBhbnk7XHJcbiAgICByZW1vdmVkcmV2aWV3ZXJzT2JqOiBhbnk7XHJcbiAgICByZXZpZXdlcnNPYmo6IGFueTtcclxuICAgIHNlbGVjdGVkUmV2aWV3ZXJzT2JqOiBhbnk7XHJcbiAgICB2aXNpYmxlOiBib29sZWFuO1xyXG4gICAgc2VsZWN0YWJsZTogYm9vbGVhbjtcclxuICAgIHJlbW92YWJsZTogYm9vbGVhbjtcclxuICAgIGFkZE9uQmx1cjogYm9vbGVhbjtcclxuICAgIHNlcGFyYXRvcktleXNDb2RlczogbnVtYmVyW107XHJcbiAgICBuZXdSZXZpZXdlcnM6IGFueVtdO1xyXG4gICAgcmVtb3ZlZFJldmlld2VyczogYW55W107XHJcbiAgICBsb2dnZWRJblVzZXI6IGFueTtcclxuICAgIGRhdGVFcnJvck1lc3NhZ2U6IGFueTtcclxuICAgIGlzUE06IGJvb2xlYW47XHJcbiAgICBjaGlwQ2hhbmdlZDogYW55O1xyXG4gICAgc2F2ZU9wdGlvbnM6IHtcclxuICAgICAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgIH1bXTtcclxuICAgIFNFTEVDVEVEX1NBVkVfT1BUSU9OOiB7XHJcbiAgICAgICAgbmFtZTogc3RyaW5nO1xyXG4gICAgICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICB9O1xyXG4gICAgRHVyYXRpb25UeXBlOiB7XHJcbiAgICAgICAgdmFsdWU6IHN0cmluZztcclxuICAgICAgICB2aWV3VmFsdWU6IHN0cmluZztcclxuICAgIH1bXTtcclxuICAgIGVuYWJsZUR1cmF0aW9uOiBib29sZWFuO1xyXG4gICAgZW5hYmxlV29ya1dlZWs6IGJvb2xlYW47XHJcbiAgICByZXZpZXdlckNvbXBsZXRlZDogYW55O1xyXG4gICAga2V5UmVzdHJpY3Rpb246IHtcclxuICAgICAgICBOVU1CRVI6IG51bWJlcltdO1xyXG4gICAgfTtcclxuICAgIG1hdEF1dG9jb21wbGV0ZTogTWF0QXV0b2NvbXBsZXRlO1xyXG4gICAgY29uc3RydWN0b3IoYXBwU2VydmljZTogQXBwU2VydmljZSwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLCBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+LCB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSwgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSwgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSwgZGlhbG9nOiBNYXREaWFsb2csIHByb2plY3RVdGlsU2VydmljZTogUHJvamVjdFV0aWxTZXJ2aWNlLCBjb21tZW50c1V0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlKTtcclxuICAgIGZpbHRlclJldmlld2Vycyh2YWx1ZTogYW55KTogYW55W107XHJcbiAgICByZXN0cmljdEtleXNPblR5cGUoZXZlbnQ6IGFueSwgZGF0YXR5cGU6IGFueSk6IHZvaWQ7XHJcbiAgICBzZWxlY3RlZChldmVudDogTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCk6IHZvaWQ7XHJcbiAgICBhZGRSZXZpZXdlcihldmVudDogTWF0Q2hpcElucHV0RXZlbnQpOiB2b2lkO1xyXG4gICAgb25SZW1vdmVSZXZpZXdlcihyZXZpZXdlclZhbHVlOiBhbnkpOiB2b2lkO1xyXG4gICAgZ2V0UHJvamVjdERldGFpbHMocHJvamVjdE9iajogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0UHJpb3JpdGllcygpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRTdGF0dXMoKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0SW5pdGlhbFN0YXR1cygpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRBbGxSb2xlcygpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgZ2V0UmV2aWV3ZXJCeUlkKHRhc2tVc2VySUQ6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldERlbGl2ZXJhYmxlUmV2aWV3QnlUYXNrSWQoVGFza0lkOiBhbnkpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgZ2V0UmV2aWV3ZXJzKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldFVzZXJzQnlSb2xlKCk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldFRhc2tUeXBlRGV0YWlscyh0YXNrVHlwZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHVua25vd24+O1xyXG4gICAgZ2V0RGVwZW5kZW50VGFza3MoaXNBcHByb3ZhbFRhc2s6IGJvb2xlYW4pOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRQcmVEZXRhaWxzKCk6IHZvaWQ7XHJcbiAgICBnZXRUYXNrRmllbGRzKHRhc2s6IGFueSk6IHtcclxuICAgICAgICBpc0FwcHJvdmFsVGFza1NlbGVjdGVkOiBib29sZWFuO1xyXG4gICAgICAgIHNob3dPdGhlckNvbW1lbnRzOiBib29sZWFuO1xyXG4gICAgICAgIGlzT3RoZXJDb21tZW50c1NlbGVjdGVkOiBib29sZWFuO1xyXG4gICAgICAgIHNob3dQcmVkZWNlc3NvcjogYm9vbGVhbjtcclxuICAgICAgICBlbmFibGVEZWxpdmVyYWJsZUFwcHJvdmVyOiBib29sZWFuO1xyXG4gICAgICAgIHNob3dEZWxpdmVyeVBhY2thZ2U6IGJvb2xlYW47XHJcbiAgICB9O1xyXG4gICAgZ2V0UHJlZGVjZXNzb3J0YXNrKHByZWRlY2Vzc29ySWQ6IGFueSk6IGFueTtcclxuICAgIGluaXRpYWxpc2VUYXNrRm9ybSgpOiB2b2lkO1xyXG4gICAgdmFsaWRhdGVEdXJhdGlvbigpOiB2b2lkO1xyXG4gICAgb25DaGFuZ2VvZkFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWU6IGFueSwgb3duZXJWYWx1ZTogYW55LCByb2xlVmFsdWU6IGFueSwgY2hhbmdlRGVsaXZlcmFibGVBcHByb3ZlcjogYW55KTogdm9pZDtcclxuICAgIG9uQ2hhbmdlb2ZSb2xlKHJvbGU6IGFueSk6IHZvaWQ7XHJcbiAgICBvbkFwcHJvdmFsU3RhdGVDaGFuZ2UoKTogdm9pZDtcclxuICAgIGZvcm1QcmVkZWNlc3NvclZhbHVlKHRhc2s6IGFueSk6IHN0cmluZztcclxuICAgIGZvcm1QcmVkZWNlc3Nvcih0YXNrSWQ6IGFueSk6IGFueTtcclxuICAgIGZvcm1Pd25lclZhbHVlKHRhc2tVc2VySUQ6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGZvcm1Sb2xlVmFsdWUodGFza1JvbGVJRDogYW55KTogYW55O1xyXG4gICAgZ2V0VGFza093bmVyQXNzaWdubWVudENvdW50KCk6IHZvaWQ7XHJcbiAgICBjYW5jZWxUYXNrQ3JlYXRpb24oKTogdm9pZDtcclxuICAgIHVwZGF0ZVRhc2soc2F2ZU9wdGlvbjogYW55KTogdm9pZDtcclxuICAgIGZvcm1SZXZpZXdlck9iaigpOiB2b2lkO1xyXG4gICAgdXBkYXRlVGFza0RldGFpbHMoc2F2ZU9wdGlvbjogYW55KTogdm9pZDtcclxuICAgIHNhdmVUYXNrKFRhc2tPYmplY3Q6IGFueSwgc2F2ZU9wdGlvbjogYW55KTogdm9pZDtcclxuICAgIGluaXRpYWxpc2VUYXNrQ29uZmlnKCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG4gICAgZGF0ZUZpbHRlcjogKGRhdGU6IERhdGUgfCBudWxsKSA9PiBib29sZWFuO1xyXG59XHJcbiJdfQ==