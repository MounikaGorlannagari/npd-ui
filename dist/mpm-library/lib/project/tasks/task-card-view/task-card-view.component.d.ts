import { OnInit, EventEmitter } from '@angular/core';
import { TaskService } from '../task.service';
import { SharingService } from '../../../../lib/mpm-utils/services/sharing.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { MatDialog } from '@angular/material/dialog';
import { MPMField } from '../../../mpm-utils/objects/MPMField';
import { ViewConfigService } from '../../../shared/services/view-config.service';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class TaskCardViewComponent implements OnInit {
    taskService: TaskService;
    sharingService: SharingService;
    otmmMetadataService: OtmmMetadataService;
    formatToLocalePipe: FormatToLocalePipe;
    utilService: UtilService;
    viewConfigService: ViewConfigService;
    fieldConfigService: FieldConfigService;
    loaderService: LoaderService;
    notificationService: NotificationService;
    dialog: MatDialog;
    taskConfig: any;
    taskData: any;
    isTemplate: any;
    projectOwner: string;
    projectStatusType: any;
    isOnHoldProject: any;
    refreshTask: EventEmitter<any>;
    editTaskHandler: EventEmitter<any>;
    taskDetailsHandler: EventEmitter<any>;
    customWorkflowHandler: EventEmitter<any>;
    taskConstants: {
        CATEGORY_LEVEL_TASK: string;
        STATUS_TYPE_INITIAL: string;
        ALLOCATION_TYPE_ROLE: string;
        ALLOCATION_TYPE_USER: string;
        TASK_TYPE: {
            NORMAL_TASK: string;
            TASK_WITHOUT_DELIVERABLE: string;
            APPROVAL_TASK: string;
        };
        TASK_ITEM_ID_METADATAFIELD_ID: string;
        dataTypes: {
            STRING: string;
            DATE: string;
        };
        StatusConstant: {
            TASK_STATUS: {
                FINAL: string;
                ACCEPTED: string;
                REWORK: string;
                CANCELLED: string;
            };
        };
        GET_ALL_TASKS_SEARCH_CONDITION_LIST: {
            search_condition_list: {
                search_condition: ({
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: string;
                    relational_operator?: undefined;
                } | {
                    type: import("../../../../public-api").IndexerDataTypes;
                    field_id: string;
                    relational_operator_id: import("../../../../public-api").MPMSearchOperators;
                    relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
                    value: import("../../../../public-api").OTMMMPMDataTypes;
                    relational_operator: string;
                })[];
            };
        };
        ACTIVE_TASK_SEARCH_CONDITION: {
            type: import("../../../../public-api").IndexerDataTypes;
            field_id: string;
            relational_operator_id: import("../../../../public-api").MPMSearchOperators;
            relational_operator_name: import("../../../../public-api").MPMSearchOperatorNames;
            value: string;
            relational_operator: string;
        };
        NOTIFICATION_LABELS: {
            ERROR: string;
            SUCCESS: string;
            INFO: string;
            WARN: string;
        };
        ALLOCATION_TYPE: {
            NAME: string;
            DESCRIPTION: import("../../../../public-api").AllocationTypes;
        }[];
        TASK_TYPE_LIST: {
            NAME: import("../../../../public-api").TaskTypes;
            DESCRIPTION: string;
            ICON: string;
        }[];
        NAME_STRING_PATTERN: string;
        PROJECT_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_DATE_ERROR_MESSAGE: string;
        DELIVERABLE_MAX_DATE_ERROR_MESSAGE: string;
        START_DATE_ERROR_MESSAGE: string;
        DURATION_ERROR_MESSAGE: string;
        DURATION_MIN_ERROR_MESSAGE: string;
        DEFAULT_CARD_FIELDS: string[];
    };
    assetStatusConstants: {
        CANCELLED: string;
        REQUESTED_CHANGES: string;
        APPROVED: string;
        IN_PROGRESS: string;
        DEFINED: string;
        DRAFT: string;
        COMPLETED: string;
        REJECTED: string;
    };
    isProjectOwner: boolean;
    isFinalStatus: boolean;
    isCancelledTask: boolean;
    isFinalTask: boolean;
    copyToolTip: string;
    view: string;
    displayableMPMFields: Array<MPMField>;
    MPMFeildConstants: {
        TASK_ID: string;
        TASK_ITEM_ID: string;
        TASK_NAME: string;
        TASK_DESCRIPTION: string;
        TASK_STATUS: string;
        TASK_STATUS_ID: string;
        TASK_STATUS_TYPE: string;
        TASK_START_DATE: string;
        TASK_DUE_DATE: string;
        TASK_END_DATE: string;
        TASK_OWNER_NAME: string;
        TASK_OWNER_ID: string;
        TASK_OWNER_CN: string;
        TASK_PRIORITY_ID: string;
        TASK_PRIORITY: string;
        IS_TASK_ACTIVE: string;
        TASK_PROGRESS: string;
        TASK_ROLE_NAME: string;
        TASK_ROLE_ID: string;
        TASK_TYPE: string;
        TASK_ACTIVE_USER_NAME: string;
        TASK_ACTIVE_USER_ID: string;
        TASK_ACTIVE_USER_CN: string;
        TASK_ROOT_PARENT_ID: string;
        TASK_PROJECT_ID: string;
        TASK_IS_DELETED: string;
        TASK_IS_APPROVAL: string;
        PROJECT_FLOW_TYPE: string;
        TASK_ASSIGNMENT_TYPE: string;
    };
    priorityObj: {
        color: string;
        icon: string;
        tooltip: string;
    };
    isNormalTask: boolean;
    projectCancelledStatus: any;
    currentWorkflowActions: any;
    taskStatusType: any;
    forceDeletion: boolean;
    affectedTask: any[];
    constructor(taskService: TaskService, sharingService: SharingService, otmmMetadataService: OtmmMetadataService, formatToLocalePipe: FormatToLocalePipe, utilService: UtilService, viewConfigService: ViewConfigService, fieldConfigService: FieldConfigService, loaderService: LoaderService, notificationService: NotificationService, dialog: MatDialog);
    openTaskDetails(isNewDeliverable: any): void;
    openCustomWorkflow(): void;
    editTask(): void;
    deleteTask(isForceDelete: boolean): void;
    openComment(): void;
    openRequestDetails(): void;
    showReminder(): void;
    refresh(): void;
    triggerActionRule(actionId: any): void;
    refreshCurrentTask(): void;
    getProperty(taskData: any, mapperName: string): string;
    converToLocalDate(deliverableData: any, propertyId: any): any;
    getPriority(taskData: any, mapperName: string): {
        color: any;
        icon: string;
        tooltip: any;
    };
    /**
     * @since MPMV3-946
     * @param taskType
     */
    getTaskTypeIcon(taskType: any): string;
    /**
     * @since MPMV3-1141
     * @param taskType
     */
    handleActionsOnTaskView(): void;
    getMetadataTypeValue(metadata: any): string;
    hasCurrentWorkflowActions(): boolean;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<TaskCardViewComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<TaskCardViewComponent, "mpm-task-card-view", never, { "taskConfig": "taskConfig"; "taskData": "taskData"; "isTemplate": "isTemplate"; "projectOwner": "projectOwner"; "projectStatusType": "projectStatusType"; "isOnHoldProject": "isOnHoldProject"; }, { "refreshTask": "refreshTask"; "editTaskHandler": "editTaskHandler"; "taskDetailsHandler": "taskDetailsHandler"; "customWorkflowHandler": "customWorkflowHandler"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jYXJkLXZpZXcuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbInRhc2stY2FyZC12aWV3LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9saWIvbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybWF0VG9Mb2NhbGVQaXBlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS5waXBlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIFRhc2tDYXJkVmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICB0YXNrU2VydmljZTogVGFza1NlcnZpY2U7XHJcbiAgICBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2U7XHJcbiAgICBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlO1xyXG4gICAgZm9ybWF0VG9Mb2NhbGVQaXBlOiBGb3JtYXRUb0xvY2FsZVBpcGU7XHJcbiAgICB1dGlsU2VydmljZTogVXRpbFNlcnZpY2U7XHJcbiAgICB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2U7XHJcbiAgICBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZTtcclxuICAgIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2U7XHJcbiAgICBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlO1xyXG4gICAgZGlhbG9nOiBNYXREaWFsb2c7XHJcbiAgICB0YXNrQ29uZmlnOiBhbnk7XHJcbiAgICB0YXNrRGF0YTogYW55O1xyXG4gICAgaXNUZW1wbGF0ZTogYW55O1xyXG4gICAgcHJvamVjdE93bmVyOiBzdHJpbmc7XHJcbiAgICBwcm9qZWN0U3RhdHVzVHlwZTogYW55O1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBhbnk7XHJcbiAgICByZWZyZXNoVGFzazogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICBlZGl0VGFza0hhbmRsZXI6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgdGFza0RldGFpbHNIYW5kbGVyOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIGN1c3RvbVdvcmtmbG93SGFuZGxlcjogRXZlbnRFbWl0dGVyPGFueT47XHJcbiAgICB0YXNrQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FURUdPUllfTEVWRUxfVEFTSzogc3RyaW5nO1xyXG4gICAgICAgIFNUQVRVU19UWVBFX0lOSVRJQUw6IHN0cmluZztcclxuICAgICAgICBBTExPQ0FUSU9OX1RZUEVfUk9MRTogc3RyaW5nO1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRV9VU0VSOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19UWVBFOiB7XHJcbiAgICAgICAgICAgIE5PUk1BTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFRBU0tfV0lUSE9VVF9ERUxJVkVSQUJMRTogc3RyaW5nO1xyXG4gICAgICAgICAgICBBUFBST1ZBTF9UQVNLOiBzdHJpbmc7XHJcbiAgICAgICAgfTtcclxuICAgICAgICBUQVNLX0lURU1fSURfTUVUQURBVEFGSUVMRF9JRDogc3RyaW5nO1xyXG4gICAgICAgIGRhdGFUeXBlczoge1xyXG4gICAgICAgICAgICBTVFJJTkc6IHN0cmluZztcclxuICAgICAgICAgICAgREFURTogc3RyaW5nO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgU3RhdHVzQ29uc3RhbnQ6IHtcclxuICAgICAgICAgICAgVEFTS19TVEFUVVM6IHtcclxuICAgICAgICAgICAgICAgIEZJTkFMOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBBQ0NFUFRFRDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgUkVXT1JLOiBzdHJpbmc7XHJcbiAgICAgICAgICAgICAgICBDQU5DRUxMRUQ6IHN0cmluZztcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEdFVF9BTExfVEFTS1NfU0VBUkNIX0NPTkRJVElPTl9MSVNUOiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogKHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkluZGV4ZXJEYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JOYW1lcztcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I/OiB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICB9IHwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuSW5kZXhlckRhdGFUeXBlcztcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IGltcG9ydChcIi4uLy4uLy4uLy4uL3B1YmxpYy1hcGlcIikuTVBNU2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5NUE1TZWFyY2hPcGVyYXRvck5hbWVzO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk9UTU1NUE1EYXRhVHlwZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgfSlbXTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFDVElWRV9UQVNLX1NFQVJDSF9DT05ESVRJT046IHtcclxuICAgICAgICAgICAgdHlwZTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5JbmRleGVyRGF0YVR5cGVzO1xyXG4gICAgICAgICAgICBmaWVsZF9pZDogc3RyaW5nO1xyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9ycztcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLk1QTVNlYXJjaE9wZXJhdG9yTmFtZXM7XHJcbiAgICAgICAgICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIE5PVElGSUNBVElPTl9MQUJFTFM6IHtcclxuICAgICAgICAgICAgRVJST1I6IHN0cmluZztcclxuICAgICAgICAgICAgU1VDQ0VTUzogc3RyaW5nO1xyXG4gICAgICAgICAgICBJTkZPOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIFdBUk46IHN0cmluZztcclxuICAgICAgICB9O1xyXG4gICAgICAgIEFMTE9DQVRJT05fVFlQRToge1xyXG4gICAgICAgICAgICBOQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBpbXBvcnQoXCIuLi8uLi8uLi8uLi9wdWJsaWMtYXBpXCIpLkFsbG9jYXRpb25UeXBlcztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgVEFTS19UWVBFX0xJU1Q6IHtcclxuICAgICAgICAgICAgTkFNRTogaW1wb3J0KFwiLi4vLi4vLi4vLi4vcHVibGljLWFwaVwiKS5UYXNrVHlwZXM7XHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBzdHJpbmc7XHJcbiAgICAgICAgICAgIElDT046IHN0cmluZztcclxuICAgICAgICB9W107XHJcbiAgICAgICAgTkFNRV9TVFJJTkdfUEFUVEVSTjogc3RyaW5nO1xyXG4gICAgICAgIFBST0pFQ1RfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFOiBzdHJpbmc7XHJcbiAgICAgICAgREVMSVZFUkFCTEVfTUFYX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIFNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERVUkFUSU9OX0VSUk9SX01FU1NBR0U6IHN0cmluZztcclxuICAgICAgICBEVVJBVElPTl9NSU5fRVJST1JfTUVTU0FHRTogc3RyaW5nO1xyXG4gICAgICAgIERFRkFVTFRfQ0FSRF9GSUVMRFM6IHN0cmluZ1tdO1xyXG4gICAgfTtcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzOiB7XHJcbiAgICAgICAgQ0FOQ0VMTEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVRVUVTVEVEX0NIQU5HRVM6IHN0cmluZztcclxuICAgICAgICBBUFBST1ZFRDogc3RyaW5nO1xyXG4gICAgICAgIElOX1BST0dSRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgREVGSU5FRDogc3RyaW5nO1xyXG4gICAgICAgIERSQUZUOiBzdHJpbmc7XHJcbiAgICAgICAgQ09NUExFVEVEOiBzdHJpbmc7XHJcbiAgICAgICAgUkVKRUNURUQ6IHN0cmluZztcclxuICAgIH07XHJcbiAgICBpc1Byb2plY3RPd25lcjogYm9vbGVhbjtcclxuICAgIGlzRmluYWxTdGF0dXM6IGJvb2xlYW47XHJcbiAgICBpc0NhbmNlbGxlZFRhc2s6IGJvb2xlYW47XHJcbiAgICBpc0ZpbmFsVGFzazogYm9vbGVhbjtcclxuICAgIGNvcHlUb29sVGlwOiBzdHJpbmc7XHJcbiAgICB2aWV3OiBzdHJpbmc7XHJcbiAgICBkaXNwbGF5YWJsZU1QTUZpZWxkczogQXJyYXk8TVBNRmllbGQ+O1xyXG4gICAgTVBNRmVpbGRDb25zdGFudHM6IHtcclxuICAgICAgICBUQVNLX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19JVEVNX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19OQU1FOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19ERVNDUklQVElPTjogc3RyaW5nO1xyXG4gICAgICAgIFRBU0tfU1RBVFVTOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19TVEFUVVNfSUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX1NUQVRVU19UWVBFOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19TVEFSVF9EQVRFOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19EVUVfREFURTogc3RyaW5nO1xyXG4gICAgICAgIFRBU0tfRU5EX0RBVEU6IHN0cmluZztcclxuICAgICAgICBUQVNLX09XTkVSX05BTUU6IHN0cmluZztcclxuICAgICAgICBUQVNLX09XTkVSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19PV05FUl9DTjogc3RyaW5nO1xyXG4gICAgICAgIFRBU0tfUFJJT1JJVFlfSUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX1BSSU9SSVRZOiBzdHJpbmc7XHJcbiAgICAgICAgSVNfVEFTS19BQ1RJVkU6IHN0cmluZztcclxuICAgICAgICBUQVNLX1BST0dSRVNTOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19ST0xFX05BTUU6IHN0cmluZztcclxuICAgICAgICBUQVNLX1JPTEVfSUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX1RZUEU6IHN0cmluZztcclxuICAgICAgICBUQVNLX0FDVElWRV9VU0VSX05BTUU6IHN0cmluZztcclxuICAgICAgICBUQVNLX0FDVElWRV9VU0VSX0lEOiBzdHJpbmc7XHJcbiAgICAgICAgVEFTS19BQ1RJVkVfVVNFUl9DTjogc3RyaW5nO1xyXG4gICAgICAgIFRBU0tfUk9PVF9QQVJFTlRfSUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX1BST0pFQ1RfSUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX0lTX0RFTEVURUQ6IHN0cmluZztcclxuICAgICAgICBUQVNLX0lTX0FQUFJPVkFMOiBzdHJpbmc7XHJcbiAgICAgICAgUFJPSkVDVF9GTE9XX1RZUEU6IHN0cmluZztcclxuICAgICAgICBUQVNLX0FTU0lHTk1FTlRfVFlQRTogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIHByaW9yaXR5T2JqOiB7XHJcbiAgICAgICAgY29sb3I6IHN0cmluZztcclxuICAgICAgICBpY29uOiBzdHJpbmc7XHJcbiAgICAgICAgdG9vbHRpcDogc3RyaW5nO1xyXG4gICAgfTtcclxuICAgIGlzTm9ybWFsVGFzazogYm9vbGVhbjtcclxuICAgIHByb2plY3RDYW5jZWxsZWRTdGF0dXM6IGFueTtcclxuICAgIGN1cnJlbnRXb3JrZmxvd0FjdGlvbnM6IGFueTtcclxuICAgIHRhc2tTdGF0dXNUeXBlOiBhbnk7XHJcbiAgICBmb3JjZURlbGV0aW9uOiBib29sZWFuO1xyXG4gICAgYWZmZWN0ZWRUYXNrOiBhbnlbXTtcclxuICAgIGNvbnN0cnVjdG9yKHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSwgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLCBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLCBmb3JtYXRUb0xvY2FsZVBpcGU6IEZvcm1hdFRvTG9jYWxlUGlwZSwgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLCB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLCBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLCBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBkaWFsb2c6IE1hdERpYWxvZyk7XHJcbiAgICBvcGVuVGFza0RldGFpbHMoaXNOZXdEZWxpdmVyYWJsZTogYW55KTogdm9pZDtcclxuICAgIG9wZW5DdXN0b21Xb3JrZmxvdygpOiB2b2lkO1xyXG4gICAgZWRpdFRhc2soKTogdm9pZDtcclxuICAgIGRlbGV0ZVRhc2soaXNGb3JjZURlbGV0ZTogYm9vbGVhbik6IHZvaWQ7XHJcbiAgICBvcGVuQ29tbWVudCgpOiB2b2lkO1xyXG4gICAgb3BlblJlcXVlc3REZXRhaWxzKCk6IHZvaWQ7XHJcbiAgICBzaG93UmVtaW5kZXIoKTogdm9pZDtcclxuICAgIHJlZnJlc2goKTogdm9pZDtcclxuICAgIHRyaWdnZXJBY3Rpb25SdWxlKGFjdGlvbklkOiBhbnkpOiB2b2lkO1xyXG4gICAgcmVmcmVzaEN1cnJlbnRUYXNrKCk6IHZvaWQ7XHJcbiAgICBnZXRQcm9wZXJ0eSh0YXNrRGF0YTogYW55LCBtYXBwZXJOYW1lOiBzdHJpbmcpOiBzdHJpbmc7XHJcbiAgICBjb252ZXJUb0xvY2FsRGF0ZShkZWxpdmVyYWJsZURhdGE6IGFueSwgcHJvcGVydHlJZDogYW55KTogYW55O1xyXG4gICAgZ2V0UHJpb3JpdHkodGFza0RhdGE6IGFueSwgbWFwcGVyTmFtZTogc3RyaW5nKToge1xyXG4gICAgICAgIGNvbG9yOiBhbnk7XHJcbiAgICAgICAgaWNvbjogc3RyaW5nO1xyXG4gICAgICAgIHRvb2x0aXA6IGFueTtcclxuICAgIH07XHJcbiAgICAvKipcclxuICAgICAqIEBzaW5jZSBNUE1WMy05NDZcclxuICAgICAqIEBwYXJhbSB0YXNrVHlwZVxyXG4gICAgICovXHJcbiAgICBnZXRUYXNrVHlwZUljb24odGFza1R5cGU6IGFueSk6IHN0cmluZztcclxuICAgIC8qKlxyXG4gICAgICogQHNpbmNlIE1QTVYzLTExNDFcclxuICAgICAqIEBwYXJhbSB0YXNrVHlwZVxyXG4gICAgICovXHJcbiAgICBoYW5kbGVBY3Rpb25zT25UYXNrVmlldygpOiB2b2lkO1xyXG4gICAgZ2V0TWV0YWRhdGFUeXBlVmFsdWUobWV0YWRhdGE6IGFueSk6IHN0cmluZztcclxuICAgIGhhc0N1cnJlbnRXb3JrZmxvd0FjdGlvbnMoKTogYm9vbGVhbjtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19