import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './item-count/item-count.component';
import * as ɵngcc2 from './item-sort/item-sort.component';
import * as ɵngcc3 from './resource-view/resource-view.component';
import * as ɵngcc4 from './swim-lane/swim-lane.component';
import * as ɵngcc5 from './task-grid/task-grid.component';
import * as ɵngcc6 from './task-list-view/task-list-view.component';
import * as ɵngcc7 from './task-toolbar/task-toolbar.component';
import * as ɵngcc8 from './custom-workflow-field/custom-workflow-field.component';
import * as ɵngcc9 from '@angular/common';
import * as ɵngcc10 from '../../material.module';
import * as ɵngcc11 from '../../mpm-library.module';
export declare class TasksModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<TasksModule, [typeof ɵngcc1.ItemCountComponent, typeof ɵngcc2.ItemSortComponent, typeof ɵngcc3.ResourceViewComponent, typeof ɵngcc4.SwimLaneComponent, typeof ɵngcc5.TaskGridComponent, typeof ɵngcc6.TaskListViewComponent, typeof ɵngcc7.TaskToolbarComponent, typeof ɵngcc8.CustomWorkflowFieldComponent], [typeof ɵngcc9.CommonModule, typeof ɵngcc10.MaterialModule, typeof ɵngcc11.MpmLibraryModule], [typeof ɵngcc1.ItemCountComponent, typeof ɵngcc2.ItemSortComponent, typeof ɵngcc3.ResourceViewComponent, typeof ɵngcc4.SwimLaneComponent, typeof ɵngcc5.TaskGridComponent, typeof ɵngcc6.TaskListViewComponent, typeof ɵngcc7.TaskToolbarComponent, typeof ɵngcc8.CustomWorkflowFieldComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<TasksModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza3MubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbInRhc2tzLm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWNsYXJlIGNsYXNzIFRhc2tzTW9kdWxlIHtcclxufVxyXG4iXX0=