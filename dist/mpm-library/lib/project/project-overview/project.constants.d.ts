import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export declare const ProjectConstant: {
    PROJECT_TYPE_PROJECT: string;
    PROJECT_TYPE_TEMPLATE: string;
    CATEGORY_LEVEL_PROJECT: string;
    CATEGORY_LEVEL_CAMPAIGN: string;
    STATUS_TYPE_INITIAL: string;
    STATUS_TYPE_FINAL_CANCELLED: string;
    STATUS_TYPE_FINAL_COMPLETED: string;
    STATUS_TYPE_ONHOLD: string;
    STATUS_TYPE_IN_PROGRESS: string;
    NOTIFICATION_LABELS: {
        ERROR: string;
        SUCCESS: string;
        INFO: string;
        WARN: string;
    };
    NAME_STRING_PATTERN: string;
    MPM_PROJECT_METADATA_GROUP: string;
    MPM_CAMPAIGN_METADATA_GROUP: string;
    MPM_PROJECT_CUSTOM_METADATA_GROUP: string;
    MPM_CAMPAIGN_CUSTOM_METADATA_GROUP: string;
    METADATA_EDIT_TYPES: {
        SIMPLE: string;
        DATE: string;
        TEXTAREA: string;
        COMBO: string;
        NUMBER: string;
    };
    DEFAULT_CARD_FIELDS: string[];
    GET_ALL_PROJECT_SEARCH_CONDITION_LIST: ({
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: string;
        relational_operator?: undefined;
    } | {
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: OTMMMPMDataTypes;
        relational_operator: MPMSearchOperators;
    })[];
    GET_ALL_CAMPAIGN_SEARCH_CONDITION_LIST: ({
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: string;
        relational_operator?: undefined;
    } | {
        type: IndexerDataTypes;
        field_id: string;
        relational_operator_id: MPMSearchOperators;
        relational_operator_name: MPMSearchOperatorNames;
        value: OTMMMPMDataTypes;
        relational_operator: MPMSearchOperators;
    })[];
    PROJECT_START_DATE_ERROR_MESSAGE: string;
    CAMPAIGN_START_DATE_ERROR_MESSAGE: string;
    PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE: string;
    START_DATE_ERROR_MESSAGE: string;
    TASK_DATE_ERROR_MESSAGE: string;
    TASK_START_DATE_ERROR_MESSAGE: string;
};
