export interface ProjectMetadataObj {
    'PROJECT_IS_ACTIVE'?: boolean;
    'PROJECT_IS_DELETED'?: boolean;
    'PROJECT_CATEGORY'?: string;
    'PROJECT_DESCRIPTION'?: string;
    'PROJECT_DUE_DATE'?: string;
    'PROJECT_END_DATE'?: string;
    'PROJECT_NAME'?: string;
    'PROJECT_OWNER_NAME'?: string;
    'PROJECT_PRIORITY'?: string;
    'PROJECT_START_DATE'?: string;
    'PROJECT_STATUS'?: string;
    'PROJECT_TEAM'?: string;
    'DATA_TYPE'?: string;
    'PROJECT_ITEM_ID'?: string;
    'PROJECT_PROGRESS'?: string;
    'PROJECT_TYPE'?: string;
    'IS_STANDARD_STATUS'?: boolean;
    'STATUS_ID'?: string;
    'PROJECT_STATUS_TYPE'?: string;
    'PROJECT_ID'?: string;
    'ORIGINAL_PROJECT_START_DATE'?: string;
    'ORIGINAL_PROJECT_DUE_DATE'?: string;
    'EXPECTED_PROJECT_DURATION'?: number;
    'PROJECT_TIME_SPENT'?: number;
    'PROJECT_CAMPAIGN_ID'?: string;
}
