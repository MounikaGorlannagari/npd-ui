import { Team } from '../../mpm-utils/objects/Team';
import { AbstractControl } from '@angular/forms';
import { Status } from '../../mpm-utils/objects/Status';
import { Priority } from '../../mpm-utils/objects/Priority';
import { ProjectType } from '../../mpm-utils/objects/ProjectTypes';
import { Project } from '../../mpm-utils/objects/Project';
export interface OverViewConfig {
    isTemplate?: boolean;
    isProject?: boolean;
    isCampaign?: boolean;
    metaDataValues?: Array<any>;
    formType?: ProjectType;
    statusOptions?: Array<Status>;
    priorityOptions?: Array<Priority>;
    teamsOptions?: Array<Team>;
    currentUserInfo?: any;
    hasProjectAdminRole?: boolean;
    currentUserId?: string;
    currentUserItemId?: string;
    projectOwners?: string;
    selectedProject?: Project;
    selectedCampaign?: any;
    isReadOnly?: boolean;
    showRequesterField?: boolean;
    ownerFieldConfig?: {
        label?: string;
        filterOptions?: Array<any>;
        formControl?: AbstractControl;
    };
    campaignDataConfig?: {
        label?: string;
        filterOptions?: Array<any>;
        formControl?: AbstractControl;
    };
    campaignOwnerFieldConfig?: {
        label?: string;
        filterOptions?: Array<any>;
        formControl?: AbstractControl;
    };
    categoryMetadataConfig?: {
        label?: string;
        filterOptions?: Array<any>;
        formControl?: AbstractControl;
    };
    dateValidation?: {
        startMaxDate: Date;
        endMinDate: Date;
    };
}
