export interface ProjectUpdateObj {
    'ProjectId': {
        'Id': string;
    };
    'ProjectName'?: string;
    'StartDate'?: string;
    'DueDate'?: string;
    'Description'?: string;
    'ExpectedDuration'?: number;
    'IsStandardStatus'?: boolean;
    'IsCustomWorkflow'?: boolean;
    'ProjectType'?: string;
    'RPOTeam'?: {
        'TeamID': {
            'Id': string;
        };
    };
    'RPOCategory'?: {
        'CategoryID': {
            'Id': string;
        };
    };
    'RPOCampaign'?: {
        'CampaignID': {
            'Id': string;
        };
    };
    'RPOPriority'?: {
        'PriorityID': {
            'Id': string;
        };
    };
    'RPOStatus'?: {
        'StatusID': {
            'Id': string;
        };
    };
    'RPOCategoryMetadata'?: {
        'CategoryMetadataID': {
            'Id': string;
        };
    };
    'RPOProjectOwner'?: {
        'IdentityID': {
            'Id': string;
        };
    };
    'CustomMetadata'?: {};
}
