export interface CampaignUpdateObj {
    'CampaignId': {
        'Id': string;
    };
    'CampaignName'?: string;
    'StartDate'?: string;
    'EndDate'?: string;
    'Description'?: string;
    'RPOCategory'?: {
        'CategoryID': {
            'Id': string;
        };
    };
    'RPOPriority'?: {
        'PriorityID': {
            'Id': string;
        };
    };
    'RPOStatus'?: {
        'StatusID': {
            'Id': string;
        };
    };
    'RPOCampaignOwner'?: {
        'IdentityID': {
            'Id': string;
        };
    };
    'CustomMetadata'?: {};
}
