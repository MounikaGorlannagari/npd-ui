import { Observable } from 'rxjs';
import { MPMSavedSearchCreate, SearchConditionList } from '../../../search/objects/MPMSavedSearchCreate';
import { AppService } from '../../../mpm-utils/services/app.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as ɵngcc0 from '@angular/core';
export declare class SearchSaveService {
    appService: AppService;
    sharingService: SharingService;
    constructor(appService: AppService, sharingService: SharingService);
    CreateMPMSavedSearchesOperationNS: string;
    CreateMPMSavedSearchesWS: string;
    DeleteMPMSavedSearchesWS: string;
    DeleteMPMSavedSearchesNS: string;
    UpdateMPMSavedSearchesWS: string;
    UpdateMPMSavedSearchesNS: string;
    saveSearch(name: string, desc: string, searchConditionList: SearchConditionList, isPublic: boolean, viewName: any): Observable<MPMSavedSearchCreate>;
    deleteSavedSearch(savedSearchId: string): Observable<boolean>;
    updateSavedSearch(seachId: string, name: string, desc: string, searchConditionList: SearchConditionList, isPublic: boolean, viewName: any): Observable<boolean>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<SearchSaveService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNhdmUuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJzZWFyY2guc2F2ZS5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE1QTVNhdmVkU2VhcmNoQ3JlYXRlLCBTZWFyY2hDb25kaXRpb25MaXN0IH0gZnJvbSAnLi4vLi4vLi4vc2VhcmNoL29iamVjdHMvTVBNU2F2ZWRTZWFyY2hDcmVhdGUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuZXhwb3J0IGRlY2xhcmUgY2xhc3MgU2VhcmNoU2F2ZVNlcnZpY2Uge1xyXG4gICAgYXBwU2VydmljZTogQXBwU2VydmljZTtcclxuICAgIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZTtcclxuICAgIGNvbnN0cnVjdG9yKGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSk7XHJcbiAgICBDcmVhdGVNUE1TYXZlZFNlYXJjaGVzT3BlcmF0aW9uTlM6IHN0cmluZztcclxuICAgIENyZWF0ZU1QTVNhdmVkU2VhcmNoZXNXUzogc3RyaW5nO1xyXG4gICAgRGVsZXRlTVBNU2F2ZWRTZWFyY2hlc1dTOiBzdHJpbmc7XHJcbiAgICBEZWxldGVNUE1TYXZlZFNlYXJjaGVzTlM6IHN0cmluZztcclxuICAgIFVwZGF0ZU1QTVNhdmVkU2VhcmNoZXNXUzogc3RyaW5nO1xyXG4gICAgVXBkYXRlTVBNU2F2ZWRTZWFyY2hlc05TOiBzdHJpbmc7XHJcbiAgICBzYXZlU2VhcmNoKG5hbWU6IHN0cmluZywgZGVzYzogc3RyaW5nLCBzZWFyY2hDb25kaXRpb25MaXN0OiBTZWFyY2hDb25kaXRpb25MaXN0LCBpc1B1YmxpYzogYm9vbGVhbiwgdmlld05hbWU6IGFueSk6IE9ic2VydmFibGU8TVBNU2F2ZWRTZWFyY2hDcmVhdGU+O1xyXG4gICAgZGVsZXRlU2F2ZWRTZWFyY2goc2F2ZWRTZWFyY2hJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPjtcclxuICAgIHVwZGF0ZVNhdmVkU2VhcmNoKHNlYWNoSWQ6IHN0cmluZywgbmFtZTogc3RyaW5nLCBkZXNjOiBzdHJpbmcsIHNlYXJjaENvbmRpdGlvbkxpc3Q6IFNlYXJjaENvbmRpdGlvbkxpc3QsIGlzUHVibGljOiBib29sZWFuLCB2aWV3TmFtZTogYW55KTogT2JzZXJ2YWJsZTxib29sZWFuPjtcclxufVxyXG4iXX0=