import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { IndexerService } from '../../../shared/services/indexer/indexer.service';
import { QdsService } from '../../../upload/services/qds.service';
import * as ɵngcc0 from '@angular/core';
export declare class AssetService {
    appService: AppService;
    otmmService: OTMMService;
    indexerService: IndexerService;
    qdsService: QdsService;
    constructor(appService: AppService, otmmService: OTMMService, indexerService: IndexerService, qdsService: QdsService);
    PROJECT_METHOD_NS: string;
    GET_PROJECT_DETAILS_WS_METHOD_NAME: string;
    CAMPAIGN_METHOD_NS: string;
    GET_CAMPAIGN_DETAILS_WS_METHOD_NAME: string;
    OTMM_METHOD_NS: string;
    FORM_METADATA_WS_METHOD_NAME: string;
    GET_CR_IS_VIEWER_NS: string;
    GET_CR_IS_VIEWER_WS_METHOD_NAME: string;
    getProjectDetails(projectId: any): Observable<any>;
    getCampaignDetails(campaignId: any): Observable<any>;
    getMetadataFields(referenceId: any): Observable<any>;
    getAssetDetails(searchCondition: any): Observable<any>;
    getAssets(searchCondition: any, folderId: any, skip: any, top: any, userSessionId: any): Observable<any>;
    initiateDownload(fileURL: any, fileName: any): void;
    initQDSDownload(downloadParams: any): void;
    findIconByName(fileName: any): "image" | "insert_drive_file" | "description" | "movie_creation" | "picture_as_pdf" | "gif" | "archive" | "graphic_eq";
    getfileFormatObject(filename: any): any;
    GetCRIsViewer(isViewer: any): Observable<any>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AssetService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQuc2VydmljZS5kLnRzIiwic291cmNlcyI6WyJhc3NldC5zZXJ2aWNlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJbmRleGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2luZGV4ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBc3NldFNlcnZpY2Uge1xyXG4gICAgYXBwU2VydmljZTogQXBwU2VydmljZTtcclxuICAgIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZTtcclxuICAgIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZTtcclxuICAgIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2U7XHJcbiAgICBjb25zdHJ1Y3RvcihhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLCBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZSwgcWRzU2VydmljZTogUWRzU2VydmljZSk7XHJcbiAgICBQUk9KRUNUX01FVEhPRF9OUzogc3RyaW5nO1xyXG4gICAgR0VUX1BST0pFQ1RfREVUQUlMU19XU19NRVRIT0RfTkFNRTogc3RyaW5nO1xyXG4gICAgQ0FNUEFJR05fTUVUSE9EX05TOiBzdHJpbmc7XHJcbiAgICBHRVRfQ0FNUEFJR05fREVUQUlMU19XU19NRVRIT0RfTkFNRTogc3RyaW5nO1xyXG4gICAgT1RNTV9NRVRIT0RfTlM6IHN0cmluZztcclxuICAgIEZPUk1fTUVUQURBVEFfV1NfTUVUSE9EX05BTUU6IHN0cmluZztcclxuICAgIEdFVF9DUl9JU19WSUVXRVJfTlM6IHN0cmluZztcclxuICAgIEdFVF9DUl9JU19WSUVXRVJfV1NfTUVUSE9EX05BTUU6IHN0cmluZztcclxuICAgIGdldFByb2plY3REZXRhaWxzKHByb2plY3RJZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+O1xyXG4gICAgZ2V0Q2FtcGFpZ25EZXRhaWxzKGNhbXBhaWduSWQ6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGdldE1ldGFkYXRhRmllbGRzKHJlZmVyZW5jZUlkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRBc3NldERldGFpbHMoc2VhcmNoQ29uZGl0aW9uOiBhbnkpOiBPYnNlcnZhYmxlPGFueT47XHJcbiAgICBnZXRBc3NldHMoc2VhcmNoQ29uZGl0aW9uOiBhbnksIGZvbGRlcklkOiBhbnksIHNraXA6IGFueSwgdG9wOiBhbnksIHVzZXJTZXNzaW9uSWQ6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxuICAgIGluaXRpYXRlRG93bmxvYWQoZmlsZVVSTDogYW55LCBmaWxlTmFtZTogYW55KTogdm9pZDtcclxuICAgIGluaXRRRFNEb3dubG9hZChkb3dubG9hZFBhcmFtczogYW55KTogdm9pZDtcclxuICAgIGZpbmRJY29uQnlOYW1lKGZpbGVOYW1lOiBhbnkpOiBcImltYWdlXCIgfCBcImluc2VydF9kcml2ZV9maWxlXCIgfCBcImRlc2NyaXB0aW9uXCIgfCBcIm1vdmllX2NyZWF0aW9uXCIgfCBcInBpY3R1cmVfYXNfcGRmXCIgfCBcImdpZlwiIHwgXCJhcmNoaXZlXCIgfCBcImdyYXBoaWNfZXFcIjtcclxuICAgIGdldGZpbGVGb3JtYXRPYmplY3QoZmlsZW5hbWU6IGFueSk6IGFueTtcclxuICAgIEdldENSSXNWaWV3ZXIoaXNWaWV3ZXI6IGFueSk6IE9ic2VydmFibGU8YW55PjtcclxufVxyXG4iXX0=