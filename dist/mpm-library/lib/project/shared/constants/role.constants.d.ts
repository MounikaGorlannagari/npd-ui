export declare const RoleConstants: {
    PROJECT_MANAGER: string;
    PROJECT_MEMBER: string;
    PROJECT_APPROVER: string;
    PROJECT_REVIEWER: string;
};
