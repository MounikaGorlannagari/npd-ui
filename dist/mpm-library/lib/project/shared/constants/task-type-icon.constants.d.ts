export declare enum TaskTypeIcon {
    ACTION = "done_all",
    UPLOAD = "publish",
    APPROVAL = "thumbs_up_down"
}
export declare const TaskTypeIconForTask: {
    NORMAL_TASK: TaskTypeIcon;
    TASK_WITHOUT_DELIVERABLE: TaskTypeIcon;
    APPROVAL_TASK: TaskTypeIcon;
};
export declare const TaskTypeIconForDeliverable: {
    UPLOAD: TaskTypeIcon;
    COMPLETE: TaskTypeIcon;
    REVIEW: TaskTypeIcon;
    'UPLOAD REVIEW': TaskTypeIcon;
};
