import { OnInit, EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class AppMenuComponent implements OnInit {
    menus: any;
    menuClicked: EventEmitter<object>;
    constructor();
    onMenuClick(menu: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AppMenuComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<AppMenuComponent, "mpm-app-menu", never, { "menus": "menus"; }, { "menuClicked": "menuClicked"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLW1lbnUuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImFwcC1tZW51LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBcHBNZW51Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIG1lbnVzOiBhbnk7XHJcbiAgICBtZW51Q2xpY2tlZDogRXZlbnRFbWl0dGVyPG9iamVjdD47XHJcbiAgICBjb25zdHJ1Y3RvcigpO1xyXG4gICAgb25NZW51Q2xpY2sobWVudTogYW55KTogdm9pZDtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQ7XHJcbn1cclxuIl19