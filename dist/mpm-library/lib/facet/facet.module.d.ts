import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './facet.component';
import * as ɵngcc2 from './facet-button/facet-button.component';
import * as ɵngcc3 from './chicklet/chicklet.component';
import * as ɵngcc4 from '@angular/common';
import * as ɵngcc5 from '../material.module';
export declare class FacetModule {
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<FacetModule, [typeof ɵngcc1.FacetComponent, typeof ɵngcc2.FacetButtonComponent, typeof ɵngcc3.ChickletComponent], [typeof ɵngcc4.CommonModule, typeof ɵngcc5.MaterialModule], [typeof ɵngcc1.FacetComponent, typeof ɵngcc2.FacetButtonComponent, typeof ɵngcc3.ChickletComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<FacetModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQubW9kdWxlLmQudHMiLCJzb3VyY2VzIjpbImZhY2V0Lm1vZHVsZS5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWNsYXJlIGNsYXNzIEZhY2V0TW9kdWxlIHtcclxufVxyXG4iXX0=