import { OnInit, EventEmitter } from '@angular/core';
import * as ɵngcc0 from '@angular/core';
export declare class FacetButtonComponent implements OnInit {
    disabled: any;
    initalState: any;
    isRMView: any;
    facetToggled: EventEmitter<any>;
    expansionState: boolean;
    leftState: string;
    isLeftNavExpanded: boolean;
    constructor();
    toggleFacet(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<FacetButtonComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<FacetButtonComponent, "mpm-facet-button", never, { "disabled": "disabled"; "initalState": "initalState"; "isRMView": "isRMView"; }, { "facetToggled": "facetToggled"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQtYnV0dG9uLmNvbXBvbmVudC5kLnRzIiwic291cmNlcyI6WyJmYWNldC1idXR0b24uY29tcG9uZW50LmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPbkluaXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBGYWNldEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBkaXNhYmxlZDogYW55O1xyXG4gICAgaW5pdGFsU3RhdGU6IGFueTtcclxuICAgIGlzUk1WaWV3OiBhbnk7XHJcbiAgICBmYWNldFRvZ2dsZWQ6IEV2ZW50RW1pdHRlcjxhbnk+O1xyXG4gICAgZXhwYW5zaW9uU3RhdGU6IGJvb2xlYW47XHJcbiAgICBsZWZ0U3RhdGU6IHN0cmluZztcclxuICAgIGlzTGVmdE5hdkV4cGFuZGVkOiBib29sZWFuO1xyXG4gICAgY29uc3RydWN0b3IoKTtcclxuICAgIHRvZ2dsZUZhY2V0KCk6IHZvaWQ7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkO1xyXG59XHJcbiJdfQ==