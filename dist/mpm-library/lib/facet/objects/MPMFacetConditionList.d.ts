export interface FacetFieldCondition {
    type?: string;
    behavior?: string;
    field_id?: string;
    values?: any[];
}
export interface FacetConditionList {
    facet_condition?: FacetFieldCondition[];
}
