import { MPMField } from '../../mpm-utils/objects/MPMField';
export interface MPMFacetConfig {
    id: string;
    name: string;
    description: string;
    defaultFacetDisplayed: number;
    defaultFacetValueDisplayed: number;
    multiSelect: boolean;
    valueOrder: 'COUNT' | '';
    valueLimit: number;
    fields: {
        facetField: MPMField[];
    };
}
