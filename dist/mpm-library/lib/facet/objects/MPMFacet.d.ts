import { FormGroup } from '@angular/forms';
export interface FaceValueList {
    asset_count?: number;
    value?: string;
    isSelected?: boolean;
    date_interval?: any;
}
export interface Facet {
    type?: string;
    height?: string;
    isShowAll?: boolean;
    isDate?: boolean;
    isNumeric?: boolean;
    isRange?: boolean;
    isInterval?: boolean;
    selectedItem?: string;
    formGroup?: FormGroup;
    facet_field_request?: {
        field_id?: string;
        name?: string;
        multi_select?: boolean;
    };
    facet_value_list?: Array<FaceValueList>;
}
