import { OnInit, EventEmitter } from '@angular/core';
import { FacetChickletEventService } from '../services/facet-chicklet-event.service';
import * as ɵngcc0 from '@angular/core';
export declare class ChickletComponent implements OnInit {
    facetChickletEventService: FacetChickletEventService;
    filters: any[];
    removed: EventEmitter<any[]>;
    constructor(facetChickletEventService: FacetChickletEventService);
    removeFilter(filter: any): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<ChickletComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<ChickletComponent, "mpm-chicklet", never, {}, { "removed": "removed"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpY2tsZXQuY29tcG9uZW50LmQudHMiLCJzb3VyY2VzIjpbImNoaWNrbGV0LmNvbXBvbmVudC5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT25Jbml0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRmFjZXRDaGlja2xldEV2ZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2ZhY2V0LWNoaWNrbGV0LWV2ZW50LnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBDaGlja2xldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBmYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlOiBGYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlO1xyXG4gICAgZmlsdGVyczogYW55W107XHJcbiAgICByZW1vdmVkOiBFdmVudEVtaXR0ZXI8YW55W10+O1xyXG4gICAgY29uc3RydWN0b3IoZmFjZXRDaGlja2xldEV2ZW50U2VydmljZTogRmFjZXRDaGlja2xldEV2ZW50U2VydmljZSk7XHJcbiAgICByZW1vdmVGaWx0ZXIoZmlsdGVyOiBhbnkpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=