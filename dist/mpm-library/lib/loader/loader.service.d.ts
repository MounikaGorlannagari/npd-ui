import { Subject } from 'rxjs';
import { LoaderState } from './loader';
import * as ɵngcc0 from '@angular/core';
export declare class LoaderService {
    loaderSubject: Subject<LoaderState>;
    loaderState: import("rxjs").Observable<LoaderState>;
    constructor();
    show(): void;
    hide(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<LoaderService, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnNlcnZpY2UuZC50cyIsInNvdXJjZXMiOlsibG9hZGVyLnNlcnZpY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTG9hZGVyU3RhdGUgfSBmcm9tICcuL2xvYWRlcic7XHJcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIExvYWRlclNlcnZpY2Uge1xyXG4gICAgbG9hZGVyU3ViamVjdDogU3ViamVjdDxMb2FkZXJTdGF0ZT47XHJcbiAgICBsb2FkZXJTdGF0ZTogaW1wb3J0KFwicnhqc1wiKS5PYnNlcnZhYmxlPExvYWRlclN0YXRlPjtcclxuICAgIGNvbnN0cnVjdG9yKCk7XHJcbiAgICBzaG93KCk6IHZvaWQ7XHJcbiAgICBoaWRlKCk6IHZvaWQ7XHJcbn1cclxuIl19