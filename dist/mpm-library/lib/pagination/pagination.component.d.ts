import { OnInit, OnChanges, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { NotificationService } from '../notification/notification.service';
import * as ɵngcc0 from '@angular/core';
export declare class PaginationComponent implements OnInit, OnChanges {
    notificationService: NotificationService;
    length: any;
    pageSize: any;
    pageIndex: any;
    paginator: EventEmitter<any>;
    matPaginator: MatPaginator;
    pageSizeOptions: number[];
    totalPageNumber: number;
    manualPageChangeControl: FormControl;
    constructor(notificationService: NotificationService);
    calculateTotalPageSize(): void;
    changePaginator(): void;
    onpageValueChange(event: any): void;
    initializeForm(): void;
    ngOnChanges(): void;
    numberOnly(event: any): boolean;
    setFirstPage(): void;
    setLastPage(): void;
    ngOnInit(): void;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<PaginationComponent, never>;
    static ɵcmp: ɵngcc0.ɵɵComponentDefWithMeta<PaginationComponent, "mpm-pagination", never, { "pageIndex": "pageIndex"; "pageSize": "pageSize"; "length": "length"; }, { "paginator": "paginator"; }, never, never>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5jb21wb25lbnQuZC50cyIsInNvdXJjZXMiOlsicGFnaW5hdGlvbi5jb21wb25lbnQuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9uSW5pdCwgT25DaGFuZ2VzLCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5leHBvcnQgZGVjbGFyZSBjbGFzcyBQYWdpbmF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZTtcclxuICAgIGxlbmd0aDogYW55O1xyXG4gICAgcGFnZVNpemU6IGFueTtcclxuICAgIHBhZ2VJbmRleDogYW55O1xyXG4gICAgcGFnaW5hdG9yOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuICAgIG1hdFBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gICAgcGFnZVNpemVPcHRpb25zOiBudW1iZXJbXTtcclxuICAgIHRvdGFsUGFnZU51bWJlcjogbnVtYmVyO1xyXG4gICAgbWFudWFsUGFnZUNoYW5nZUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG4gICAgY29uc3RydWN0b3Iobm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSk7XHJcbiAgICBjYWxjdWxhdGVUb3RhbFBhZ2VTaXplKCk6IHZvaWQ7XHJcbiAgICBjaGFuZ2VQYWdpbmF0b3IoKTogdm9pZDtcclxuICAgIG9ucGFnZVZhbHVlQ2hhbmdlKGV2ZW50OiBhbnkpOiB2b2lkO1xyXG4gICAgaW5pdGlhbGl6ZUZvcm0oKTogdm9pZDtcclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQ7XHJcbiAgICBudW1iZXJPbmx5KGV2ZW50OiBhbnkpOiBib29sZWFuO1xyXG4gICAgc2V0Rmlyc3RQYWdlKCk6IHZvaWQ7XHJcbiAgICBzZXRMYXN0UGFnZSgpOiB2b2lkO1xyXG4gICAgbmdPbkluaXQoKTogdm9pZDtcclxufVxyXG4iXX0=