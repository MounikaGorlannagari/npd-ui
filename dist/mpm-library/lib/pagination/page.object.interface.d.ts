export interface PageObjectInterface {
    length: any;
    pageIndex: any;
    pageSize: any;
    totalPages: any;
}
