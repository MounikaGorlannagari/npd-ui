import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import { NotificationTypes } from './object/NotificationTypes';
let NotificationComponent = class NotificationComponent {
    constructor(snackBarRef, data) {
        this.snackBarRef = snackBarRef;
        this.data = data;
    }
    getIcon() {
        switch (this.data.type) {
            case NotificationTypes.SUCCESS:
                return 'check_circle';
            case NotificationTypes.ERROR:
                return 'error';
            case NotificationTypes.WARN:
                return 'warning';
            case NotificationTypes.INFO:
                return 'info';
        }
    }
};
NotificationComponent.ctorParameters = () => [
    { type: MatSnackBarRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_SNACK_BAR_DATA,] }] }
];
NotificationComponent = __decorate([
    Component({
        selector: 'mpm-notification',
        template: "<div class=\"notification-container\">\r\n    <div class=\"icon\">\r\n        <mat-icon>{{getIcon()}}</mat-icon>\r\n    </div>\r\n    <div class=\"data\">{{data.message}}</div>\r\n    <div class=\"dismiss\">\r\n        <button mat-icon-button (click)=\"snackBarRef.dismiss()\">\r\n            <mat-icon>highlight_off</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>",
        styles: [".notification-container{display:flex;align-items:center;justify-content:space-between}.notification-container .icon{line-height:40px;height:40px;width:40px}.notification-container .icon mat-icon{font-size:36px;height:36px;width:36px;cursor:default}"]
    }),
    __param(1, Inject(MAT_SNACK_BAR_DATA))
], NotificationComponent);
export { NotificationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFakYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFRL0QsSUFBYSxxQkFBcUIsR0FBbEMsTUFBYSxxQkFBcUI7SUFFOUIsWUFDVyxXQUFrRCxFQUN0QixJQUFzQjtRQURsRCxnQkFBVyxHQUFYLFdBQVcsQ0FBdUM7UUFDdEIsU0FBSSxHQUFKLElBQUksQ0FBa0I7SUFDekQsQ0FBQztJQUVMLE9BQU87UUFDSCxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3BCLEtBQUssaUJBQWlCLENBQUMsT0FBTztnQkFDMUIsT0FBTyxjQUFjLENBQUM7WUFDMUIsS0FBSyxpQkFBaUIsQ0FBQyxLQUFLO2dCQUN4QixPQUFPLE9BQU8sQ0FBQztZQUNuQixLQUFLLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3ZCLE9BQU8sU0FBUyxDQUFDO1lBQ3JCLEtBQUssaUJBQWlCLENBQUMsSUFBSTtnQkFDdkIsT0FBTyxNQUFNLENBQUM7U0FDckI7SUFDTCxDQUFDO0NBRUosQ0FBQTs7WUFqQjJCLGNBQWM7NENBQ2pDLE1BQU0sU0FBQyxrQkFBa0I7O0FBSnJCLHFCQUFxQjtJQU5qQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLGdZQUE0Qzs7S0FFL0MsQ0FBQztJQU1PLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7R0FKdEIscUJBQXFCLENBb0JqQztTQXBCWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNQVRfU05BQ0tfQkFSX0RBVEEsIE1hdFNuYWNrQmFyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc25hY2stYmFyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uRGF0YSB9IGZyb20gJy4vb2JqZWN0L05vdGlmaWNhdGlvbkRhdGEnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25UeXBlcyB9IGZyb20gJy4vb2JqZWN0L05vdGlmaWNhdGlvblR5cGVzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tbm90aWZpY2F0aW9uJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9ub3RpZmljYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25Db21wb25lbnQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzbmFja0JhclJlZjogTWF0U25hY2tCYXJSZWY8Tm90aWZpY2F0aW9uQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9TTkFDS19CQVJfREFUQSkgcHVibGljIGRhdGE6IE5vdGlmaWNhdGlvbkRhdGFcclxuICAgICkgeyB9XHJcblxyXG4gICAgZ2V0SWNvbigpIHtcclxuICAgICAgICBzd2l0Y2ggKHRoaXMuZGF0YS50eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9uVHlwZXMuU1VDQ0VTUzpcclxuICAgICAgICAgICAgICAgIHJldHVybiAnY2hlY2tfY2lyY2xlJztcclxuICAgICAgICAgICAgY2FzZSBOb3RpZmljYXRpb25UeXBlcy5FUlJPUjpcclxuICAgICAgICAgICAgICAgIHJldHVybiAnZXJyb3InO1xyXG4gICAgICAgICAgICBjYXNlIE5vdGlmaWNhdGlvblR5cGVzLldBUk46XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ3dhcm5pbmcnO1xyXG4gICAgICAgICAgICBjYXNlIE5vdGlmaWNhdGlvblR5cGVzLklORk86XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJ2luZm8nO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19