import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NotificationComponent } from './notification.component';
import { NotificationTypes } from './object/NotificationTypes';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
let NotificationService = class NotificationService {
    constructor(snackBar) {
        this.snackBar = snackBar;
        this.enableAutoHide = true;
        this.autoHideDuration = 5000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.defaultClass = 'mpm-notification';
        this.info = (message) => {
            if (!message) {
                return;
            }
            const config = new MatSnackBarConfig();
            config.verticalPosition = this.verticalPosition;
            config.horizontalPosition = this.horizontalPosition;
            config.panelClass = ['info-notification', this.defaultClass];
            config.data = {
                message,
                type: NotificationTypes.INFO
            };
            if (this.enableAutoHide) {
                config.duration = this.autoHideDuration;
            }
            this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.success = (message) => {
            if (!message) {
                return;
            }
            const config = new MatSnackBarConfig();
            config.verticalPosition = this.verticalPosition;
            config.horizontalPosition = this.horizontalPosition;
            config.panelClass = ['success-notification', this.defaultClass];
            config.data = {
                message,
                type: NotificationTypes.SUCCESS
            };
            if (this.enableAutoHide) {
                config.duration = this.autoHideDuration;
            }
            this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.warn = (message) => {
            if (!message) {
                return;
            }
            const config = new MatSnackBarConfig();
            config.verticalPosition = this.verticalPosition;
            config.horizontalPosition = this.horizontalPosition;
            config.panelClass = ['warn-notification', this.defaultClass];
            config.data = {
                message,
                type: NotificationTypes.WARN
            };
            if (this.enableAutoHide) {
                config.duration = this.autoHideDuration;
            }
            this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.error = (message) => {
            if (!message) {
                return;
            }
            const config = new MatSnackBarConfig();
            config.verticalPosition = this.verticalPosition;
            config.horizontalPosition = this.horizontalPosition;
            config.panelClass = ['error-notification', this.defaultClass];
            config.data = {
                message,
                type: NotificationTypes.ERROR
            };
            if (this.enableAutoHide) {
                config.duration = this.autoHideDuration;
            }
            this.snackBar.openFromComponent(NotificationComponent, config);
        };
    }
};
NotificationService.ctorParameters = () => [
    { type: MatSnackBar }
];
NotificationService.ɵprov = i0.ɵɵdefineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.ɵɵinject(i1.MatSnackBar)); }, token: NotificationService, providedIn: "root" });
NotificationService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], NotificationService);
export { NotificationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSw2QkFBNkIsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3pJLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7QUFNL0QsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFFNUIsWUFDVyxRQUFxQjtRQUFyQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBR2hDLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4Qix1QkFBa0IsR0FBa0MsUUFBUSxDQUFDO1FBQzdELHFCQUFnQixHQUFnQyxRQUFRLENBQUM7UUFDekQsaUJBQVksR0FBRyxrQkFBa0IsQ0FBQztRQUVsQyxTQUFJLEdBQUcsQ0FBQyxPQUFlLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNWLE9BQU87YUFDVjtZQUNELE1BQU0sTUFBTSxHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQztZQUN2QyxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1lBQ2hELE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDcEQsTUFBTSxDQUFDLFVBQVUsR0FBRyxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM3RCxNQUFNLENBQUMsSUFBSSxHQUFHO2dCQUNWLE9BQU87Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDL0IsQ0FBQztZQUNGLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtRQUVELFlBQU8sR0FBRyxDQUFDLE9BQWUsRUFBRSxFQUFFO1lBQzFCLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ1YsT0FBTzthQUNWO1lBQ0QsTUFBTSxNQUFNLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ3ZDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDaEQsTUFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUNwRCxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2hFLE1BQU0sQ0FBQyxJQUFJLEdBQUc7Z0JBQ1YsT0FBTztnQkFDUCxJQUFJLEVBQUUsaUJBQWlCLENBQUMsT0FBTzthQUNsQyxDQUFDO1lBQ0YsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNyQixNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzthQUMzQztZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbkUsQ0FBQyxDQUFBO1FBRUQsU0FBSSxHQUFHLENBQUMsT0FBZSxFQUFFLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPO2FBQ1Y7WUFDRCxNQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3BELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0QsTUFBTSxDQUFDLElBQUksR0FBRztnQkFDVixPQUFPO2dCQUNQLElBQUksRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2FBQy9CLENBQUM7WUFDRixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO2FBQzNDO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNuRSxDQUFDLENBQUE7UUFFRCxVQUFLLEdBQUcsQ0FBQyxPQUFlLEVBQUUsRUFBRTtZQUN4QixJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNWLE9BQU87YUFDVjtZQUNELE1BQU0sTUFBTSxHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQztZQUN2QyxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1lBQ2hELE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDcEQsTUFBTSxDQUFDLFVBQVUsR0FBRyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5RCxNQUFNLENBQUMsSUFBSSxHQUFHO2dCQUNWLE9BQU87Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLEtBQUs7YUFDaEMsQ0FBQztZQUNGLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtJQWxGRyxDQUFDO0NBbUZSLENBQUE7O1lBcEZ3QixXQUFXOzs7QUFIdkIsbUJBQW1CO0lBSi9CLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxtQkFBbUIsQ0F1Ri9CO1NBdkZZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0U25hY2tCYXIsIE1hdFNuYWNrQmFyQ29uZmlnLCBNYXRTbmFja0Jhckhvcml6b250YWxQb3NpdGlvbiwgTWF0U25hY2tCYXJWZXJ0aWNhbFBvc2l0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc25hY2stYmFyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uVHlwZXMgfSBmcm9tICcuL29iamVjdC9Ob3RpZmljYXRpb25UeXBlcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgc25hY2tCYXI6IE1hdFNuYWNrQmFyXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGVuYWJsZUF1dG9IaWRlID0gdHJ1ZTtcclxuICAgIGF1dG9IaWRlRHVyYXRpb24gPSA1MDAwO1xyXG4gICAgaG9yaXpvbnRhbFBvc2l0aW9uOiBNYXRTbmFja0Jhckhvcml6b250YWxQb3NpdGlvbiA9ICdjZW50ZXInO1xyXG4gICAgdmVydGljYWxQb3NpdGlvbjogTWF0U25hY2tCYXJWZXJ0aWNhbFBvc2l0aW9uID0gJ2JvdHRvbSc7XHJcbiAgICBkZWZhdWx0Q2xhc3MgPSAnbXBtLW5vdGlmaWNhdGlvbic7XHJcblxyXG4gICAgaW5mbyA9IChtZXNzYWdlOiBzdHJpbmcpID0+IHtcclxuICAgICAgICBpZiAoIW1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBjb25maWcgPSBuZXcgTWF0U25hY2tCYXJDb25maWcoKTtcclxuICAgICAgICBjb25maWcudmVydGljYWxQb3NpdGlvbiA9IHRoaXMudmVydGljYWxQb3NpdGlvbjtcclxuICAgICAgICBjb25maWcuaG9yaXpvbnRhbFBvc2l0aW9uID0gdGhpcy5ob3Jpem9udGFsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLnBhbmVsQ2xhc3MgPSBbJ2luZm8tbm90aWZpY2F0aW9uJywgdGhpcy5kZWZhdWx0Q2xhc3NdO1xyXG4gICAgICAgIGNvbmZpZy5kYXRhID0ge1xyXG4gICAgICAgICAgICBtZXNzYWdlLFxyXG4gICAgICAgICAgICB0eXBlOiBOb3RpZmljYXRpb25UeXBlcy5JTkZPXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVBdXRvSGlkZSkge1xyXG4gICAgICAgICAgICBjb25maWcuZHVyYXRpb24gPSB0aGlzLmF1dG9IaWRlRHVyYXRpb247XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNuYWNrQmFyLm9wZW5Gcm9tQ29tcG9uZW50KE5vdGlmaWNhdGlvbkNvbXBvbmVudCwgY29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdWNjZXNzID0gKG1lc3NhZ2U6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGlmICghbWVzc2FnZSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNvbmZpZyA9IG5ldyBNYXRTbmFja0JhckNvbmZpZygpO1xyXG4gICAgICAgIGNvbmZpZy52ZXJ0aWNhbFBvc2l0aW9uID0gdGhpcy52ZXJ0aWNhbFBvc2l0aW9uO1xyXG4gICAgICAgIGNvbmZpZy5ob3Jpem9udGFsUG9zaXRpb24gPSB0aGlzLmhvcml6b250YWxQb3NpdGlvbjtcclxuICAgICAgICBjb25maWcucGFuZWxDbGFzcyA9IFsnc3VjY2Vzcy1ub3RpZmljYXRpb24nLCB0aGlzLmRlZmF1bHRDbGFzc107XHJcbiAgICAgICAgY29uZmlnLmRhdGEgPSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE5vdGlmaWNhdGlvblR5cGVzLlNVQ0NFU1NcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmICh0aGlzLmVuYWJsZUF1dG9IaWRlKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZy5kdXJhdGlvbiA9IHRoaXMuYXV0b0hpZGVEdXJhdGlvbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc25hY2tCYXIub3BlbkZyb21Db21wb25lbnQoTm90aWZpY2F0aW9uQ29tcG9uZW50LCBjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHdhcm4gPSAobWVzc2FnZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgaWYgKCFtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29uZmlnID0gbmV3IE1hdFNuYWNrQmFyQ29uZmlnKCk7XHJcbiAgICAgICAgY29uZmlnLnZlcnRpY2FsUG9zaXRpb24gPSB0aGlzLnZlcnRpY2FsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLmhvcml6b250YWxQb3NpdGlvbiA9IHRoaXMuaG9yaXpvbnRhbFBvc2l0aW9uO1xyXG4gICAgICAgIGNvbmZpZy5wYW5lbENsYXNzID0gWyd3YXJuLW5vdGlmaWNhdGlvbicsIHRoaXMuZGVmYXVsdENsYXNzXTtcclxuICAgICAgICBjb25maWcuZGF0YSA9IHtcclxuICAgICAgICAgICAgbWVzc2FnZSxcclxuICAgICAgICAgICAgdHlwZTogTm90aWZpY2F0aW9uVHlwZXMuV0FSTlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlQXV0b0hpZGUpIHtcclxuICAgICAgICAgICAgY29uZmlnLmR1cmF0aW9uID0gdGhpcy5hdXRvSGlkZUR1cmF0aW9uO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zbmFja0Jhci5vcGVuRnJvbUNvbXBvbmVudChOb3RpZmljYXRpb25Db21wb25lbnQsIGNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgZXJyb3IgPSAobWVzc2FnZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgaWYgKCFtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29uZmlnID0gbmV3IE1hdFNuYWNrQmFyQ29uZmlnKCk7XHJcbiAgICAgICAgY29uZmlnLnZlcnRpY2FsUG9zaXRpb24gPSB0aGlzLnZlcnRpY2FsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLmhvcml6b250YWxQb3NpdGlvbiA9IHRoaXMuaG9yaXpvbnRhbFBvc2l0aW9uO1xyXG4gICAgICAgIGNvbmZpZy5wYW5lbENsYXNzID0gWydlcnJvci1ub3RpZmljYXRpb24nLCB0aGlzLmRlZmF1bHRDbGFzc107XHJcbiAgICAgICAgY29uZmlnLmRhdGEgPSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE5vdGlmaWNhdGlvblR5cGVzLkVSUk9SXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVBdXRvSGlkZSkge1xyXG4gICAgICAgICAgICBjb25maWcuZHVyYXRpb24gPSB0aGlzLmF1dG9IaWRlRHVyYXRpb247XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNuYWNrQmFyLm9wZW5Gcm9tQ29tcG9uZW50KE5vdGlmaWNhdGlvbkNvbXBvbmVudCwgY29uZmlnKTtcclxuICAgIH1cclxufVxyXG4iXX0=