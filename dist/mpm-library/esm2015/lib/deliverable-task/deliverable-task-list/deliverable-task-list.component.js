import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer2, OnChanges, SimpleChanges } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { OTMMDataTypes } from '../../mpm-utils/objects/OTMMDataTypes';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { ProjectService } from '../../project/shared/services/project.service';
import { FormatToLocalePipe } from '../../shared/pipe/format-to-locale.pipe';
import { MatSort } from '@angular/material/sort';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { DeliverableBulkCountService } from '../service/deliverable-bulk-count.service';
import { MatDialog } from '@angular/material/dialog';
import { AssetService } from '../../project/shared/services/asset.service';
import { Observable } from 'rxjs';
import { AssetConstants } from '../../project/assets/asset_constants';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { moveItemInArray } from '@angular/cdk/drag-drop';
//import { DeliverableCreationModalComponent } from 'src/app/core/mpm/project-view/task/task-details/deliverable-creation-modal/deliverable-creation-modal.component';
let DeliverableTaskListComponent = class DeliverableTaskListComponent {
    constructor(utilService, projectService, formatToLocalePipe, element, fieldConfigService, sharingService, deliverableBulkCountService, dialog, assetService, otmmService, router, renderer) {
        this.utilService = utilService;
        this.projectService = projectService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.element = element;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.deliverableBulkCountService = deliverableBulkCountService;
        this.dialog = dialog;
        this.assetService = assetService;
        this.otmmService = otmmService;
        this.router = router;
        this.renderer = renderer;
        this.sortChange = new EventEmitter();
        this.refresh = new EventEmitter();
        this.rowExpanded = new EventEmitter();
        this.rowCollapsed = new EventEmitter();
        this.deliverableDetailsHandler = new EventEmitter();
        this.versionsHandler = new EventEmitter();
        this.shareAssetHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.bulkCheck = new EventEmitter();
        this.deliverableBulkCheckCount = new EventEmitter();
        this.selectedDeliverableCounts = new EventEmitter();
        this.selectedDeliverableData = new EventEmitter();
        this.selectedDeliverableDatas = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.editDeliverableHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.colWidth = 270;
        this.metaDataDataTypes = OTMMDataTypes;
        this.MPMFieldConstants = MPMFieldConstants;
        this.selection = new SelectionModel(true, []);
        this.deliverableTaskDataSource = new MatTableDataSource([]);
        this.deliverableId = [];
        this.selected = false;
        this.folderId = [];
        this.project = [];
        this.assetData = [];
        this.assetDetailDatas = [];
    }
    ngOnChanges(changes) {
        this.reload();
    }
    openCR(event) {
        this.crHandler.emit(event);
    }
    editDeliverable(deliverableConfig) {
        this.editDeliverableHandler.next(deliverableConfig);
        /*  const dialogRef = this.dialog.open(DeliverableCreationModalComponent, {
             width: '38%',
             disableClose: true,
             data: {
                 projectId: deliverableConfig.projectId,
                 isTemplate: deliverableConfig.isTemplate,
                 taskId: deliverableConfig.taskId,
                 deliverableId: deliverableConfig.deliverableId,
                 selectedProject: deliverableConfig.selectedProject,
                 selectedTask: deliverableConfig.selectedTask,
                 modalLabel: 'Edit Deliverable'
             }
         });
         dialogRef.afterClosed().subscribe(result => {
             if (result) {
                 console.log(result)
                 //this.taskService.setDeliverableRefreshData(200);
             }
         }); */
    }
    deliverableBulkEditCheckCount(deliverableData) {
        let id;
        let deliverableid;
        if (this.bulkActions === false && !this.selected) {
            id = [];
            deliverableid = true;
            this.selected = true;
        }
        id = this.deliverableBulkCountService.getBulkDeliverableId(deliverableData, deliverableid);
        if (id.length > 1) {
            this.deliverableBulkCheckCount.next(true);
        }
        else {
            this.deliverableBulkCheckCount.next(false);
        }
        this.selectedDeliverableCounts.next(id.length);
        this.selectedDeliverableDatas.next(id);
    }
    selectedBulkDeliverableData(deliverableData) {
        this.selectedDeliverableData.next(deliverableData);
    }
    refreshDeliverable(event) {
        if (event) {
            this.refresh.next(event);
        }
    }
    deliverableDetails(event) {
        this.deliverableDetailsHandler.emit(event);
    }
    viewVersions(event) {
        this.versionsHandler.emit(event);
    }
    shareAsset(event) {
        this.shareAssetHandler.emit(event);
    }
    computeGridCols() {
        this.cols = Math.floor(this.element.nativeElement.offsetParent.offsetWidth / this.colWidth);
    }
    expandRow(row) {
        if (row.isExpanded) {
            row.isExpanded = false;
            this.rowCollapsed.next();
        }
        else {
            row.isExpanded = true;
            this.rowExpanded.next(row);
        }
    }
    getPriority(projectData, columnData) {
        const priorityObj = this.utilService.getPriorityIcon(this.getProperty(projectData, columnData), this.level);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    }
    getProperty(projectData, columnData) {
        return this.fieldConfigService.getFieldValueByDisplayColumn(columnData, projectData);
    }
    converToLocalDate(data, propertyId) {
        return this.formatToLocalePipe.transform(this.projectService.converToLocalDate(data, propertyId));
    }
    openComment(row) {
        alert('To Be done');
    }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected(data) {
        const numSelected = this.selection.selected.length;
        const numRows = this.deliverableTaskDataSource.data.length;
        //if (this.selection.hasValue() && numSelected === numRows && (data || data == undefined)) {
        if (data) {
            this.bulkCheck.next(true);
        }
        else {
            this.bulkCheck.next(false);
        }
        return numSelected === numRows;
    }
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle(event) {
        this.isAllSelected(event.checked) ?
            this.selection.clear() :
            this.deliverableTaskDataSource.data.forEach(row => this.selection.select(row));
    }
    refreshEvent() {
        this.refresh.emit();
    }
    tableDrop(event) {
        let count = 0;
        if (this.displayColumns.find(column => column === 'Select')) {
            count = count + 1;
        }
        if (this.displayColumns.find(column => column === 'Expand')) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        const orderedFields = [];
        this.displayColumns.forEach(column => {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(this.displayableMetadataFields.find(displayableField => displayableField.INDEXER_FIELD_ID === column));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    }
    onResizeMouseDown(event, column) {
        event.stopPropagation();
        event.preventDefault();
        const start = event.target;
        const pressed = true;
        const startX = event.x;
        const startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    }
    initResizableColumns(start, pressed, startX, startWidth, column) {
        this.renderer.listen('body', 'mousemove', (event) => {
            if (pressed) {
                const width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', (event) => {
            if (pressed) {
                pressed = false;
                this.resizeDisplayableFields.next();
            }
        });
    }
    reload() {
        this.computeGridCols();
        if (!this.displayColumns) {
            this.displayColumns = [];
        }
        if (!this.displayableMetadataFields) {
            this.displayableMetadataFields = [];
        }
        if (!this.dataSource) {
            this.dataSource = [];
        }
        this.deliverableTaskDataSource.data = this.dataSource;
        if (!this.level) {
            console.warn('No Level info was provided to list view to render');
            return;
        }
        if (this.sort) {
            this.sort.disableClear = true;
            this.sortControl.active = this.sort.active;
            this.sortControl.direction = this.sort.direction;
            this.sortControl.disableClear = true;
            this.deliverableTaskDataSource.sort = this.sortControl;
        }
        this.sortControl.sortChange.subscribe(() => {
            this.sortChange.emit(this.sortControl);
        });
    }
    retrieveAssetData(event) {
        this.assetDetailsCallBackHandler.next(event);
    }
    getAssets() {
        return new Observable(observer => {
            this.otmmService.checkOtmmSession()
                .subscribe(sessionResponse => {
                let userSessionId;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                } /* this.skip */
                this.folderId.forEach(folderId => {
                    this.assetService.getAssets(AssetConstants.ASSET_SEARCH_CONDITION, folderId, 0, 50, userSessionId).subscribe(res => {
                        if (res && res.search_result_resource && res.search_result_resource.asset_list) {
                            //  this.loadingHandler(false);
                            this.totalAssetsCount = res.search_result_resource.search_result.total_hit_count;
                            this.assets = res.search_result_resource.asset_list;
                            this.assetData = [];
                            this.assets.forEach(asset => {
                                asset.metadata.metadata_element_list.forEach(metadataElement => {
                                    if (metadataElement.id === 'MPM.ASSET.GROUP') {
                                        // [0].metadata_element_list
                                        metadataElement.metadata_element_list.forEach(metadataElementList => {
                                            if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                                                if (metadataElementList.value.value.value === 'true') {
                                                    this.assetData.push(asset);
                                                }
                                            }
                                        });
                                    }
                                });
                            });
                        }
                        observer.next(this.assetData);
                        observer.complete();
                    });
                });
            });
        });
    }
    ngOnInit() {
        console.log(this.displayableMetadataFields);
        this.filterType = (this.selectedFilter === 'MY_TEAM_TASKS') ? true : false;
        //deliverableWorkView
        this.IsNotPMView = this.router.url.includes('deliverableWorkView') ? true : false;
        this.reload();
        this.appConfig = this.sharingService.getAppConfig();
        this.defaultMyTaskViewCollapsed = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_MYTASK_VIEW_COLLAPSED]);
        if (!this.defaultMyTaskViewCollapsed) {
            this.deliverableTaskDataSource.data[0].isExpanded = true;
            this.rowExpanded.next(this.deliverableTaskDataSource.data[0]);
        }
    }
};
DeliverableTaskListComponent.ctorParameters = () => [
    { type: UtilService },
    { type: ProjectService },
    { type: FormatToLocalePipe },
    { type: ElementRef },
    { type: FieldConfigService },
    { type: SharingService },
    { type: DeliverableBulkCountService },
    { type: MatDialog },
    { type: AssetService },
    { type: OTMMService },
    { type: Router },
    { type: Renderer2 }
];
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "level", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "sort", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "displayColumns", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "dataSource", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "displayableMetadataFields", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "bulkActions", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "unselectDeliverable", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "assetDetailData", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "selectedFilter", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "selectedListDependentFilter", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "bulkResponseData", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "sortChange", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "refresh", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "rowExpanded", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "rowCollapsed", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "deliverableDetailsHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "versionsHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "shareAssetHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "crHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "bulkCheck", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "deliverableBulkCheckCount", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "selectedDeliverableCounts", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "selectedDeliverableData", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "selectedDeliverableDatas", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "assetDetailsCallBackHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "editDeliverableHandler", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "orderedDisplayableFields", void 0);
__decorate([
    Output()
], DeliverableTaskListComponent.prototype, "resizeDisplayableFields", void 0);
__decorate([
    Input()
], DeliverableTaskListComponent.prototype, "userPreferenceFreezeCount", void 0);
__decorate([
    ViewChild(MatSort, { static: true })
], DeliverableTaskListComponent.prototype, "sortControl", void 0);
DeliverableTaskListComponent = __decorate([
    Component({
        selector: 'mpm-deliverable-task-list',
        template: "<table class=\"deliverable-task-list\" mat-table [dataSource]=\"deliverableTaskDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Expand\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-icon *ngIf=\"!row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_right</mat-icon>\r\n            <mat-icon *ngIf=\"row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_down</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <!--  <mat-checkbox color=\"red\" (change)=\"$event ? masterToggle($event) : null\"\r\n                [checked]=\"selection.hasValue() && isAllSelected(true)\"\r\n                [indeterminate]=\"selection.hasValue() && !isAllSelected(true)\">\r\n            </mat-checkbox> -->\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <!--  <mat-checkbox color=\"primary\" (click)=\"$event.stopPropagation()\"\r\n                (change)=\"$event ? selection.toggle(row) : null\" [checked]=\"selection.isSelected(row)\">\r\n            </mat-checkbox> -->\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields; index as i\" [sticky]=\"i<userPreferenceFreezeCount\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':metaDataDataTypes.STRING === column.DATA_TYPE}\">\r\n            <span>\r\n                <div *ngIf=\"(column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) && \r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_PRIORITY) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROGRESS) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS)\">\r\n                    <a matTooltip=\"{{getProperty(row, column)}}\">{{getProperty(row, column) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY || \r\n                column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">{{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n                <div\r\n                    *ngIf=\"(column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || (column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROGRESS)\">\r\n                    <span matTooltip=\"{{getProperty(row, column) || '0'}}%\">{{getProperty(row, column) || 'NA'}}%\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\" value=\"{{getProperty(row, column) || '0'}}\"\r\n                            class=\"\">\r\n                        </mat-progress-bar>\r\n                    </span>\r\n            </div>\r\n            <div *ngIf=\"(column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || (column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS)\">\r\n                <mat-chip-list>\r\n                    <mat-chip class=\"status-chip\">{{getProperty(row, column) || 'NA'}}</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <!-- <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\" class=\"min-width-action\">\r\n            <div>\r\n                <button mat-icon-button matTooltip=\"Comments\" (click)=\"openComment(row)\">\r\n                    <mat-icon>comment</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container> -->\r\n\r\n    <ng-container matColumnDef=\"expandedDetail\">\r\n        <td class=\"task-details\" mat-cell *matCellDef=\"let row\" [attr.colspan]=\"displayColumns?.length\">\r\n            <div class=\"task-element-detail\" [@detailExpand]=\"row.isExpanded ? 'expanded' : 'collapsed'\">\r\n                <div class=\"deliverables-info\" *ngIf=\"(!row.deliverableDetails || row.deliverableDetails.length === 0)\">\r\n                    <div class=\"task-heading\">No Deliverables are there to work</div>\r\n                </div>\r\n                <div class=\"deliverables-card-area\">\r\n                    <mat-grid-list cols=\"{{cols}}\" rowHeight=\"370\" (window:resize)=\"computeGridCols()\" gutterSize=\"10\">\r\n                        <mat-grid-tile *ngFor=\"let deliverableDetail of row.deliverableDetails\">\r\n                            <mpm-deliverable-card [isPMView]=\"false\" [bulkEdit]=\"row\" [deliverableBulkResponse]=\"bulkResponseData\" (deliverableBulkCheckCount)=\"deliverableBulkEditCheckCount($event)\" (selectedDeliverableData)=\"selectedBulkDeliverableData($event)\" [deliverableData]=\"deliverableDetail.deliverableData\"\r\n                                [projectRowData]=\"row\" (refreshDeliverable)=\"refreshDeliverable($event)\" (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\" (editDeliverableHandler)=\"editDeliverable($event)\" (crHandler)=\"openCR($event)\" (deliverableDetailsHandler)=\"deliverableDetails($event)\"\r\n                                (versionsHandler)=\"viewVersions($event)\" (shareAssetHandler)=\"shareAsset($event)\" [level]=\"level\" [bulkActions]=\"bulkActions\" [unselectDeliverable]=\"unselectDeliverable\">\r\n                            </mpm-deliverable-card>\r\n                        </mat-grid-tile>\r\n                        <!-- for reference asset card -->\r\n                        <!--  <mat-grid-tile *ngFor=\"let assetData of assetDetailDatas\">\r\n                            <div *ngFor=\"let asset of assetData\">\r\n                            <mpm-asset-card  [asset]=\"asset\">\r\n\r\n                            </mpm-asset-card>\r\n                            </div>\r\n                        </mat-grid-tile> -->\r\n                        <!-- [isVersion]=\"false\"\r\n                            [selectAllDeliverables]=\"selectAllDeliverables\"(assetVersionsCallBackHandler)=\"retrieveAssetVersionsData($event)\"\r\n                (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\"\r\n                (selectedAssetsCallBackHandler)=\"getSelectedDeliverable(asset)\"\r\n                (unSelectedAssetsCallBackHandler)=\"getUnSelectedDeliverable(asset)\"\r\n                (shareAssetsCallbackHandler)=\"shareAsset($event)\"\r\n                (assetPreviewCallBackHandler)=\"retrieveAssetPreviewData($event)\"\r\n                (downloadCallBackHandler)=\"downloadAsset($event)\" -->\r\n                        <!--  </mat-grid-tile> -->\r\n                    </mat-grid-list>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"task-element-row\" (click)=\"expandRow(row)\">\r\n    </tr>\r\n\r\n    <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\" class=\"task-details-row\"></tr>\r\n</table>\r\n\r\n<div *ngIf=\"(deliverableTaskDataSource && deliverableTaskDataSource.data && deliverableTaskDataSource.data.length == 0)\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No tasks available.</button>\r\n</div>",
        animations: [trigger('detailExpand', [
                state('collapsed', style({ height: '0' })),
                state('expanded', style({ height: '*' })),
                transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.42, 0.0, 0.58, 1.0)')),
            ])],
        styles: ["table.deliverable-task-list{width:100%}table.deliverable-task-list tr{cursor:pointer}table.deliverable-task-list tr .mat-checkbox{padding:0 8px}table.deliverable-task-list tr th:last-child{padding-left:12px}table.deliverable-task-list tr td{min-width:8rem}table.deliverable-task-list tr td a.mat-button{padding:0;text-align:left}table.deliverable-task-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.deliverable-task-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}.deliverables-info{width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex;align-items:center;justify-content:center;text-align:center}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}td.task-details{padding:0!important;cursor:default}tr.task-details-row{height:0}.task-element-row td{border-bottom-width:0}.task-element-detail{overflow:hidden;display:flex;flex-wrap:wrap}.deliverables-card-area{width:100%;padding:8px;font-weight:lighter;margin:0 25px 25px}td.wrap-text-custom span>div{max-width:14em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:3em}.mat-checkbox{color:red}"]
    })
], DeliverableTaskListComponent);
export { DeliverableTaskListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdGFzay1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2RlbGl2ZXJhYmxlLXRhc2svZGVsaXZlcmFibGUtdGFzay1saXN0L2RlbGl2ZXJhYmxlLXRhc2stbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0ksT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTdELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDN0UsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRWhGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUN4RixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFlLGVBQWUsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RFLHNLQUFzSztBQWF0SyxJQUFhLDRCQUE0QixHQUF6QyxNQUFhLDRCQUE0QjtJQWdDckMsWUFDVyxXQUF3QixFQUN4QixjQUE4QixFQUM5QixrQkFBc0MsRUFDdEMsT0FBbUIsRUFDbkIsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLDJCQUF3RCxFQUN4RCxNQUFpQixFQUNqQixZQUEwQixFQUMxQixXQUF3QixFQUN4QixNQUFjLEVBQ2QsUUFBbUI7UUFYbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsWUFBTyxHQUFQLE9BQU8sQ0FBWTtRQUNuQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQ0FBMkIsR0FBM0IsMkJBQTJCLENBQTZCO1FBQ3hELFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUE5QnBCLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3JDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDO1FBQ25DLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0QyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdkMsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRCxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM1QyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyw4QkFBeUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BELDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDcEQsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNsRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsMkJBQXNCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNqRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUF1QjVELGFBQVEsR0FBRyxHQUFHLENBQUM7UUFHZixzQkFBaUIsR0FBRyxhQUFhLENBQUM7UUFDbEMsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFFdEMsY0FBUyxHQUFHLElBQUksY0FBYyxDQUFNLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUM5Qyw4QkFBeUIsR0FBRyxJQUFJLGtCQUFrQixDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBSzVELGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFHakIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixjQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ2YscUJBQWdCLEdBQUcsRUFBRSxDQUFDO0lBM0JsQixDQUFDO0lBQ0wsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtJQUVqQixDQUFDO0lBOEJELE1BQU0sQ0FBQyxLQUFLO1FBQ1IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELGVBQWUsQ0FBQyxpQkFBaUI7UUFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBRXBEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFrQk87SUFDWCxDQUFDO0lBRUQsNkJBQTZCLENBQUMsZUFBZTtRQUN6QyxJQUFJLEVBQUUsQ0FBQztRQUNQLElBQUksYUFBYSxDQUFDO1FBQ2xCLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzlDLEVBQUUsR0FBRyxFQUFFLENBQUM7WUFDUixhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBRXhCO1FBQ0QsRUFBRSxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFFM0YsSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNmLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0M7YUFBTTtZQUNILElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCwyQkFBMkIsQ0FBQyxlQUFlO1FBQ3ZDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM1QjtJQUNMLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxLQUFLO1FBQ3BCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsZUFBZTtRQUNYLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQsU0FBUyxDQUFDLEdBQUc7UUFDVCxJQUFJLEdBQUcsQ0FBQyxVQUFVLEVBQUU7WUFDaEIsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUM1QjthQUFNO1lBQ0gsR0FBRyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFvQjtRQUN6QyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsRUFDMUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWhCLE9BQU87WUFDSCxLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUs7WUFDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxXQUFXLENBQUMsT0FBTztTQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVELFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBb0I7UUFDekMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsVUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN0RyxDQUFDO0lBRUQsV0FBVyxDQUFDLEdBQUc7UUFDWCxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGdGQUFnRjtJQUNoRixhQUFhLENBQUMsSUFBVTtRQUNwQixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDbkQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDM0QsNEZBQTRGO1FBQzVGLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlCO1FBQ0QsT0FBTyxXQUFXLEtBQUssT0FBTyxDQUFDO0lBQ25DLENBQUM7SUFFRCxnRkFBZ0Y7SUFDaEYsWUFBWSxDQUFDLEtBQUs7UUFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxTQUFTLENBQUMsS0FBNEI7UUFDbEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsRUFBRTtZQUN6RCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUNyQjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLEVBQUU7WUFDekQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUM7U0FDckI7UUFDRCxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzlGLE1BQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxLQUFLLFNBQVMsQ0FBQyxFQUFFO2dCQUN2RSxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDN0g7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQUssRUFBRSxNQUFNO1FBQzNCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMzQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN2QixNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFdBQVcsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2hELElBQUksT0FBTyxFQUFFO2dCQUNULE1BQU0sS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDOUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztTQUM1QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDakMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLEVBQUUsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2IsT0FBTyxDQUFDLElBQUksQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO1lBQ2xFLE9BQU87U0FDVjtRQUNELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDckMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQzFEO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUN2QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSztRQUNuQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxTQUFTO1FBQ0wsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFO2lCQUM5QixTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksYUFBYSxDQUFDO2dCQUNsQixJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsZ0JBQWdCLElBQUksZUFBZSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sSUFBSSxlQUFlLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRTtvQkFDaEosYUFBYSxHQUFHLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO2lCQUMvRCxDQUFBLGVBQWU7Z0JBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUMvRyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsc0JBQXNCLElBQUksR0FBRyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRTs0QkFDNUUsK0JBQStCOzRCQUMvQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUM7NEJBQ2pGLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQzs0QkFDcEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7NEJBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUN4QixLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsRUFBRTtvQ0FDM0QsSUFBSSxlQUFlLENBQUMsRUFBRSxLQUFLLGlCQUFpQixFQUFFO3dDQUMxQyw0QkFBNEI7d0NBQzVCLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsRUFBRTs0Q0FDaEUsSUFBSSxtQkFBbUIsQ0FBQyxFQUFFLEtBQUssOEJBQThCLEVBQUU7Z0RBQzNELElBQUksbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssTUFBTSxFQUFFO29EQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpREFDOUI7NkNBQ0o7d0NBQ0wsQ0FBQyxDQUFDLENBQUM7cUNBQ047Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBRTlCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFFBQVE7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxLQUFLLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMzRSxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDbEYsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7UUFDNUosSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDekQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pFO0lBQ0wsQ0FBQztDQUNKLENBQUE7O1lBaFQyQixXQUFXO1lBQ1IsY0FBYztZQUNWLGtCQUFrQjtZQUM3QixVQUFVO1lBQ0Msa0JBQWtCO1lBQ3RCLGNBQWM7WUFDRCwyQkFBMkI7WUFDaEQsU0FBUztZQUNILFlBQVk7WUFDYixXQUFXO1lBQ2hCLE1BQU07WUFDSixTQUFTOztBQTFDckI7SUFBUixLQUFLLEVBQUU7MkRBQXlCO0FBQ3hCO0lBQVIsS0FBSyxFQUFFOzBEQUFzQjtBQUNyQjtJQUFSLEtBQUssRUFBRTtvRUFBc0M7QUFDckM7SUFBUixLQUFLLEVBQUU7Z0VBQStCO0FBQzlCO0lBQVIsS0FBSyxFQUFFOytFQUE4QztBQUM3QztJQUFSLEtBQUssRUFBRTtpRUFBb0I7QUFDbkI7SUFBUixLQUFLLEVBQUU7eUVBQTRCO0FBQzNCO0lBQVIsS0FBSyxFQUFFO3FFQUF3QjtBQUN2QjtJQUFSLEtBQUssRUFBRTtvRUFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7aUZBQW9DO0FBQ25DO0lBQVIsS0FBSyxFQUFFO3NFQUF5QjtBQUV2QjtJQUFULE1BQU0sRUFBRTtnRUFBc0M7QUFDckM7SUFBVCxNQUFNLEVBQUU7NkRBQW9DO0FBQ25DO0lBQVQsTUFBTSxFQUFFO2lFQUF1QztBQUN0QztJQUFULE1BQU0sRUFBRTtrRUFBd0M7QUFDdkM7SUFBVCxNQUFNLEVBQUU7K0VBQXFEO0FBQ3BEO0lBQVQsTUFBTSxFQUFFO3FFQUEyQztBQUMxQztJQUFULE1BQU0sRUFBRTt1RUFBNkM7QUFDNUM7SUFBVCxNQUFNLEVBQUU7K0RBQXFDO0FBQ3BDO0lBQVQsTUFBTSxFQUFFOytEQUFxQztBQUNwQztJQUFULE1BQU0sRUFBRTsrRUFBcUQ7QUFDcEQ7SUFBVCxNQUFNLEVBQUU7K0VBQXFEO0FBQ3BEO0lBQVQsTUFBTSxFQUFFOzZFQUFtRDtBQUNsRDtJQUFULE1BQU0sRUFBRTs4RUFBb0Q7QUFDbkQ7SUFBVCxNQUFNLEVBQUU7aUZBQXVEO0FBQ3REO0lBQVQsTUFBTSxFQUFFOzRFQUFrRDtBQUNqRDtJQUFULE1BQU0sRUFBRTs4RUFBb0Q7QUFDbkQ7SUFBVCxNQUFNLEVBQUU7NkVBQW1EO0FBQ25EO0lBQVIsS0FBSyxFQUFFOytFQUEyQjtBQW9CRztJQUFyQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO2lFQUFzQjtBQW5EbEQsNEJBQTRCO0lBWHhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSwyQkFBMkI7UUFDckMsd2pRQUFxRDtRQUVyRCxVQUFVLEVBQUUsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFO2dCQUNqQyxLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQyxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUN6QyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7YUFDNUYsQ0FBQyxDQUFDOztLQUNOLENBQUM7R0FFVyw0QkFBNEIsQ0FpVnhDO1NBalZZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgUmVuZGVyZXIyLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTCB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgT1RNTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1EYXRhVHlwZXMnO1xyXG5pbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIGFuaW1hdGUsIHRyYW5zaXRpb24gfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0JztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9kZWxpdmVyYWJsZS1idWxrLWNvdW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBc3NldENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Byb2plY3QvYXNzZXRzL2Fzc2V0X2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuaW1wb3J0IHsgQ2RrRHJhZ0Ryb3AsIG1vdmVJdGVtSW5BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG4vL2ltcG9ydCB7IERlbGl2ZXJhYmxlQ3JlYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJ3NyYy9hcHAvY29yZS9tcG0vcHJvamVjdC12aWV3L3Rhc2svdGFzay1kZXRhaWxzL2RlbGl2ZXJhYmxlLWNyZWF0aW9uLW1vZGFsL2RlbGl2ZXJhYmxlLWNyZWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWRlbGl2ZXJhYmxlLXRhc2stbGlzdCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGVsaXZlcmFibGUtdGFzay1saXN0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RlbGl2ZXJhYmxlLXRhc2stbGlzdC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgYW5pbWF0aW9uczogW3RyaWdnZXIoJ2RldGFpbEV4cGFuZCcsIFtcclxuICAgICAgICBzdGF0ZSgnY29sbGFwc2VkJywgc3R5bGUoeyBoZWlnaHQ6ICcwJyB9KSksXHJcbiAgICAgICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyBoZWlnaHQ6ICcqJyB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignZXhwYW5kZWQgPD0+IGNvbGxhcHNlZCcsIGFuaW1hdGUoJzIyNW1zIGN1YmljLWJlemllcigwLjQyLCAwLjAsIDAuNTgsIDEuMCknKSksXHJcbiAgICBdKV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEZWxpdmVyYWJsZVRhc2tMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KCkgcHVibGljIGxldmVsOiBNUE1fTEVWRUw7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgc29ydDogTWF0U29ydDtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNwbGF5Q29sdW1uczogQXJyYXk8c3RyaW5nPjtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyBkYXRhU291cmNlOiBBcnJheTxhbnk+O1xyXG4gICAgQElucHV0KCkgcHVibGljIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM6IEFycmF5PGFueT47XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgYnVsa0FjdGlvbnM7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgdW5zZWxlY3REZWxpdmVyYWJsZTtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyBhc3NldERldGFpbERhdGE7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgc2VsZWN0ZWRGaWx0ZXI7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyO1xyXG4gICAgQElucHV0KCkgcHVibGljIGJ1bGtSZXNwb25zZURhdGE7XHJcblxyXG4gICAgQE91dHB1dCgpIHNvcnRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoID0gbmV3IEV2ZW50RW1pdHRlcjx2b2lkPigpO1xyXG4gICAgQE91dHB1dCgpIHJvd0V4cGFuZGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcm93Q29sbGFwc2VkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZGVsaXZlcmFibGVEZXRhaWxzSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHZlcnNpb25zSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNoYXJlQXNzZXRIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3JIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgYnVsa0NoZWNrID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZGVsaXZlcmFibGVCdWxrQ2hlY2tDb3VudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdGVkRGVsaXZlcmFibGVDb3VudHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlRGF0YSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdGVkRGVsaXZlcmFibGVEYXRhcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGFzc2V0RGV0YWlsc0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGVkaXREZWxpdmVyYWJsZUhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBvcmRlcmVkRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSByZXNpemVEaXNwbGF5YWJsZUZpZWxkcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQElucHV0KCkgdXNlclByZWZlcmVuY2VGcmVlemVDb3VudDtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZm9ybWF0VG9Mb2NhbGVQaXBlOiBGb3JtYXRUb0xvY2FsZVBpcGUsXHJcbiAgICAgICAgcHVibGljIGVsZW1lbnQ6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRlbGl2ZXJhYmxlQnVsa0NvdW50U2VydmljZTogRGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgYXNzZXRTZXJ2aWNlOiBBc3NldFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHVibGljIHJlbmRlcmVyOiBSZW5kZXJlcjJcclxuICAgICkgeyB9XHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQoKVxyXG4gICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBzb3J0Q29udHJvbDogTWF0U29ydDtcclxuXHJcbiAgICBjb2xXaWR0aCA9IDI3MDtcclxuICAgIGNvbHM6IG51bWJlcjtcclxuXHJcbiAgICBtZXRhRGF0YURhdGFUeXBlcyA9IE9UTU1EYXRhVHlwZXM7XHJcbiAgICBNUE1GaWVsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzO1xyXG5cclxuICAgIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxhbnk+KHRydWUsIFtdKTtcclxuICAgIGRlbGl2ZXJhYmxlVGFza0RhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlPGFueT4oW10pO1xyXG5cclxuICAgIGFwcENvbmZpZztcclxuICAgIGRlZmF1bHRNeVRhc2tWaWV3Q29sbGFwc2VkOiBib29sZWFuO1xyXG5cclxuICAgIGRlbGl2ZXJhYmxlSWQgPSBbXTtcclxuICAgIHNlbGVjdGVkID0gZmFsc2U7XHJcblxyXG4gICAgYXNzZXRzO1xyXG4gICAgZm9sZGVySWQgPSBbXTtcclxuICAgIHByb2plY3QgPSBbXTtcclxuICAgIGFzc2V0RGF0YSA9IFtdO1xyXG4gICAgYXNzZXREZXRhaWxEYXRhcyA9IFtdO1xyXG4gICAgYXNzZXREZXRhaWw7XHJcbiAgICB0b3RhbEFzc2V0c0NvdW50O1xyXG5cclxuICAgIGZpbHRlclR5cGU7XHJcbiAgICBJc05vdFBNVmlldztcclxuXHJcbiAgICBvcGVuQ1IoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmNySGFuZGxlci5lbWl0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0RGVsaXZlcmFibGUoZGVsaXZlcmFibGVDb25maWcpIHtcclxuICAgICAgICB0aGlzLmVkaXREZWxpdmVyYWJsZUhhbmRsZXIubmV4dChkZWxpdmVyYWJsZUNvbmZpZyk7XHJcblxyXG4gICAgICAgIC8qICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKERlbGl2ZXJhYmxlQ3JlYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgd2lkdGg6ICczOCUnLFxyXG4gICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgIHByb2plY3RJZDogZGVsaXZlcmFibGVDb25maWcucHJvamVjdElkLFxyXG4gICAgICAgICAgICAgICAgIGlzVGVtcGxhdGU6IGRlbGl2ZXJhYmxlQ29uZmlnLmlzVGVtcGxhdGUsXHJcbiAgICAgICAgICAgICAgICAgdGFza0lkOiBkZWxpdmVyYWJsZUNvbmZpZy50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVJZDogZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgICAgICBzZWxlY3RlZFByb2plY3Q6IGRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUHJvamVjdCxcclxuICAgICAgICAgICAgICAgICBzZWxlY3RlZFRhc2s6IGRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkVGFzayxcclxuICAgICAgICAgICAgICAgICBtb2RhbExhYmVsOiAnRWRpdCBEZWxpdmVyYWJsZSdcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7XHJcbiAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdClcclxuICAgICAgICAgICAgICAgICAvL3RoaXMudGFza1NlcnZpY2Uuc2V0RGVsaXZlcmFibGVSZWZyZXNoRGF0YSgyMDApO1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgICB9KTsgKi9cclxuICAgIH1cclxuXHJcbiAgICBkZWxpdmVyYWJsZUJ1bGtFZGl0Q2hlY2tDb3VudChkZWxpdmVyYWJsZURhdGEpIHtcclxuICAgICAgICBsZXQgaWQ7XHJcbiAgICAgICAgbGV0IGRlbGl2ZXJhYmxlaWQ7XHJcbiAgICAgICAgaWYgKHRoaXMuYnVsa0FjdGlvbnMgPT09IGZhbHNlICYmICF0aGlzLnNlbGVjdGVkKSB7XHJcbiAgICAgICAgICAgIGlkID0gW107XHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlkID0gdGhpcy5kZWxpdmVyYWJsZUJ1bGtDb3VudFNlcnZpY2UuZ2V0QnVsa0RlbGl2ZXJhYmxlSWQoZGVsaXZlcmFibGVEYXRhLCBkZWxpdmVyYWJsZWlkKTtcclxuXHJcbiAgICAgICAgaWYgKGlkLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUJ1bGtDaGVja0NvdW50Lm5leHQodHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUJ1bGtDaGVja0NvdW50Lm5leHQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVDb3VudHMubmV4dChpZC5sZW5ndGgpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZURhdGFzLm5leHQoaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdGVkQnVsa0RlbGl2ZXJhYmxlRGF0YShkZWxpdmVyYWJsZURhdGEpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVEYXRhLm5leHQoZGVsaXZlcmFibGVEYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoRGVsaXZlcmFibGUoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWxpdmVyYWJsZURldGFpbHMoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRGV0YWlsc0hhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgdmlld1ZlcnNpb25zKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy52ZXJzaW9uc0hhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hhcmVBc3NldChldmVudCkge1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEhhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcHV0ZUdyaWRDb2xzKCkge1xyXG4gICAgICAgIHRoaXMuY29scyA9IE1hdGguZmxvb3IodGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQub2Zmc2V0UGFyZW50Lm9mZnNldFdpZHRoIC8gdGhpcy5jb2xXaWR0aCk7XHJcbiAgICB9XHJcblxyXG4gICAgZXhwYW5kUm93KHJvdykge1xyXG4gICAgICAgIGlmIChyb3cuaXNFeHBhbmRlZCkge1xyXG4gICAgICAgICAgICByb3cuaXNFeHBhbmRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLnJvd0NvbGxhcHNlZC5uZXh0KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcm93LmlzRXhwYW5kZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnJvd0V4cGFuZGVkLm5leHQocm93KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJpb3JpdHkocHJvamVjdERhdGEsIGNvbHVtbkRhdGE6IE1QTUZpZWxkKSB7XHJcbiAgICAgICAgY29uc3QgcHJpb3JpdHlPYmogPSB0aGlzLnV0aWxTZXJ2aWNlLmdldFByaW9yaXR5SWNvbih0aGlzLmdldFByb3BlcnR5KHByb2plY3REYXRhLCBjb2x1bW5EYXRhKSxcclxuICAgICAgICAgICAgdGhpcy5sZXZlbCk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBwcmlvcml0eU9iai5jb2xvcixcclxuICAgICAgICAgICAgaWNvbjogcHJpb3JpdHlPYmouaWNvbixcclxuICAgICAgICAgICAgdG9vbHRpcDogcHJpb3JpdHlPYmoudG9vbHRpcFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHkocHJvamVjdERhdGEsIGNvbHVtbkRhdGE6IE1QTUZpZWxkKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihjb2x1bW5EYXRhLCBwcm9qZWN0RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUoZGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdFRvTG9jYWxlUGlwZS50cmFuc2Zvcm0odGhpcy5wcm9qZWN0U2VydmljZS5jb252ZXJUb0xvY2FsRGF0ZShkYXRhLCBwcm9wZXJ0eUlkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNvbW1lbnQocm93KSB7XHJcbiAgICAgICAgYWxlcnQoJ1RvIEJlIGRvbmUnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogV2hldGhlciB0aGUgbnVtYmVyIG9mIHNlbGVjdGVkIGVsZW1lbnRzIG1hdGNoZXMgdGhlIHRvdGFsIG51bWJlciBvZiByb3dzLiAqL1xyXG4gICAgaXNBbGxTZWxlY3RlZChkYXRhPzogYW55KSB7XHJcbiAgICAgICAgY29uc3QgbnVtU2VsZWN0ZWQgPSB0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgbnVtUm93cyA9IHRoaXMuZGVsaXZlcmFibGVUYXNrRGF0YVNvdXJjZS5kYXRhLmxlbmd0aDtcclxuICAgICAgICAvL2lmICh0aGlzLnNlbGVjdGlvbi5oYXNWYWx1ZSgpICYmIG51bVNlbGVjdGVkID09PSBudW1Sb3dzICYmIChkYXRhIHx8IGRhdGEgPT0gdW5kZWZpbmVkKSkge1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYnVsa0NoZWNrLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5idWxrQ2hlY2submV4dChmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudW1TZWxlY3RlZCA9PT0gbnVtUm93cztcclxuICAgIH1cclxuXHJcbiAgICAvKiogU2VsZWN0cyBhbGwgcm93cyBpZiB0aGV5IGFyZSBub3QgYWxsIHNlbGVjdGVkOyBvdGhlcndpc2UgY2xlYXIgc2VsZWN0aW9uLiAqL1xyXG4gICAgbWFzdGVyVG9nZ2xlKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5pc0FsbFNlbGVjdGVkKGV2ZW50LmNoZWNrZWQpID9cclxuICAgICAgICAgICAgdGhpcy5zZWxlY3Rpb24uY2xlYXIoKSA6XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrRGF0YVNvdXJjZS5kYXRhLmZvckVhY2gocm93ID0+IHRoaXMuc2VsZWN0aW9uLnNlbGVjdChyb3cpKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoRXZlbnQoKSB7XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoLmVtaXQoKTtcclxuICAgIH1cclxuXHJcbiAgICB0YWJsZURyb3AoZXZlbnQ6IENka0RyYWdEcm9wPHN0cmluZ1tdPikge1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgaWYgKHRoaXMuZGlzcGxheUNvbHVtbnMuZmluZChjb2x1bW4gPT4gY29sdW1uID09PSAnU2VsZWN0JykpIHtcclxuICAgICAgICAgICAgY291bnQgPSBjb3VudCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRpc3BsYXlDb2x1bW5zLmZpbmQoY29sdW1uID0+IGNvbHVtbiA9PT0gJ0V4cGFuZCcpKSB7XHJcbiAgICAgICAgICAgIGNvdW50ID0gY291bnQgKyAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBtb3ZlSXRlbUluQXJyYXkodGhpcy5kaXNwbGF5Q29sdW1ucywgZXZlbnQucHJldmlvdXNJbmRleCArIGNvdW50LCBldmVudC5jdXJyZW50SW5kZXggKyBjb3VudCk7XHJcbiAgICAgICAgY29uc3Qgb3JkZXJlZEZpZWxkcyA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICAgICAgICBpZiAoIShjb2x1bW4gPT09ICdTZWxlY3QnIHx8IGNvbHVtbiA9PT0gJ0V4cGFuZCcgfHwgY29sdW1uID09PSAnQWN0aW9ucycpKSB7XHJcbiAgICAgICAgICAgICAgICBvcmRlcmVkRmllbGRzLnB1c2godGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLmZpbmQoZGlzcGxheWFibGVGaWVsZCA9PiBkaXNwbGF5YWJsZUZpZWxkLklOREVYRVJfRklFTERfSUQgPT09IGNvbHVtbikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcmRlcmVkRGlzcGxheWFibGVGaWVsZHMubmV4dChvcmRlcmVkRmllbGRzKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJlc2l6ZU1vdXNlRG93bihldmVudCwgY29sdW1uKSB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBzdGFydCA9IGV2ZW50LnRhcmdldDtcclxuICAgICAgICBjb25zdCBwcmVzc2VkID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCBzdGFydFggPSBldmVudC54O1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0V2lkdGggPSAkKHN0YXJ0KS5wYXJlbnQoKS53aWR0aCgpO1xyXG4gICAgICAgIHRoaXMuaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0UmVzaXphYmxlQ29sdW1ucyhzdGFydCwgcHJlc3NlZCwgc3RhcnRYLCBzdGFydFdpZHRoLCBjb2x1bW4pIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLmxpc3RlbignYm9keScsICdtb3VzZW1vdmUnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gc3RhcnRXaWR0aCArIChldmVudC54IC0gc3RhcnRYKTtcclxuICAgICAgICAgICAgICAgIGNvbHVtbi53aWR0aCA9IHdpZHRoO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2V1cCcsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocHJlc3NlZCkge1xyXG4gICAgICAgICAgICAgICAgcHJlc3NlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNpemVEaXNwbGF5YWJsZUZpZWxkcy5uZXh0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZWxvYWQoKSB7ICAgICAgIFxyXG4gICAgICAgIHRoaXMuY29tcHV0ZUdyaWRDb2xzKCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLmRpc3BsYXlDb2x1bW5zKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF0aGlzLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGhpcy5kYXRhU291cmNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlVGFza0RhdGFTb3VyY2UuZGF0YSA9IHRoaXMuZGF0YVNvdXJjZTtcclxuICAgICAgICBpZiAoIXRoaXMubGV2ZWwpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdObyBMZXZlbCBpbmZvIHdhcyBwcm92aWRlZCB0byBsaXN0IHZpZXcgdG8gcmVuZGVyJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuc29ydCkge1xyXG4gICAgICAgICAgICB0aGlzLnNvcnQuZGlzYWJsZUNsZWFyID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zb3J0Q29udHJvbC5hY3RpdmUgPSB0aGlzLnNvcnQuYWN0aXZlO1xyXG4gICAgICAgICAgICB0aGlzLnNvcnRDb250cm9sLmRpcmVjdGlvbiA9IHRoaXMuc29ydC5kaXJlY3Rpb247XHJcbiAgICAgICAgICAgIHRoaXMuc29ydENvbnRyb2wuZGlzYWJsZUNsZWFyID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVRhc2tEYXRhU291cmNlLnNvcnQgPSB0aGlzLnNvcnRDb250cm9sO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNvcnRDb250cm9sLnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLmVtaXQodGhpcy5zb3J0Q29udHJvbCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0cmlldmVBc3NldERhdGEoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0RGV0YWlsc0NhbGxCYWNrSGFuZGxlci5uZXh0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jaGVja090bW1TZXNzaW9uKClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdXNlclNlc3Npb25JZDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2Vzc2lvblJlc3BvbnNlICYmIHNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlICYmIHNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24gJiYgc2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyU2Vzc2lvbklkID0gc2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5pZDtcclxuICAgICAgICAgICAgICAgICAgICB9LyogdGhpcy5za2lwICovXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb2xkZXJJZC5mb3JFYWNoKGZvbGRlcklkID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldFNlcnZpY2UuZ2V0QXNzZXRzKEFzc2V0Q29uc3RhbnRzLkFTU0VUX1NFQVJDSF9DT05ESVRJT04sIGZvbGRlcklkLCAwLCA1MCwgdXNlclNlc3Npb25JZCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzICYmIHJlcy5zZWFyY2hfcmVzdWx0X3Jlc291cmNlICYmIHJlcy5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLmFzc2V0X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgdGhpcy5sb2FkaW5nSGFuZGxlcihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b3RhbEFzc2V0c0NvdW50ID0gcmVzLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2Uuc2VhcmNoX3Jlc3VsdC50b3RhbF9oaXRfY291bnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldHMgPSByZXMuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZS5hc3NldF9saXN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXNzZXREYXRhID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldHMuZm9yRWFjaChhc3NldCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5mb3JFYWNoKG1ldGFkYXRhRWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50LmlkID09PSAnTVBNLkFTU0VULkdST1VQJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUVsZW1lbnQubWV0YWRhdGFfZWxlbWVudF9saXN0LmZvckVhY2gobWV0YWRhdGFFbGVtZW50TGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUVsZW1lbnRMaXN0LmlkID09PSAnTVBNLkFTU0VULklTX1JFRkVSRU5DRV9BU1NFVCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUVsZW1lbnRMaXN0LnZhbHVlLnZhbHVlLnZhbHVlID09PSAndHJ1ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFzc2V0RGF0YS5wdXNoKGFzc2V0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hc3NldERhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzKTtcclxuICAgICAgICB0aGlzLmZpbHRlclR5cGUgPSAodGhpcy5zZWxlY3RlZEZpbHRlciA9PT0gJ01ZX1RFQU1fVEFTS1MnKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAvL2RlbGl2ZXJhYmxlV29ya1ZpZXdcclxuICAgICAgICB0aGlzLklzTm90UE1WaWV3ID0gdGhpcy5yb3V0ZXIudXJsLmluY2x1ZGVzKCdkZWxpdmVyYWJsZVdvcmtWaWV3JykgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5yZWxvYWQoKTtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0TXlUYXNrVmlld0NvbGxhcHNlZCA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfTVlUQVNLX1ZJRVdfQ09MTEFQU0VEXSk7XHJcbiAgICAgICAgaWYgKCF0aGlzLmRlZmF1bHRNeVRhc2tWaWV3Q29sbGFwc2VkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrRGF0YVNvdXJjZS5kYXRhWzBdLmlzRXhwYW5kZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnJvd0V4cGFuZGVkLm5leHQodGhpcy5kZWxpdmVyYWJsZVRhc2tEYXRhU291cmNlLmRhdGFbMF0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=