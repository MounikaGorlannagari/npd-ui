import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
let MetdataListViewFieldObjectHelperService = class MetdataListViewFieldObjectHelperService {
    constructor() {
    }
    getDisplayMetadataFields(metadataFieldList, metadataFilterList, isDefaultMetadataField) {
        const displayableMetadataFields = [];
        if (!Array.isArray(metadataFieldList) || !Array.isArray(metadataFilterList)) {
            return displayableMetadataFields;
        }
        metadataFieldList.map(field => {
            const fieldObj = metadataFilterList.find(e => (e.id === field.FIELD_ID) && (field.IS_GRID_VIEW === 'true'));
            if (fieldObj) {
                fieldObj.sequence = Number(field.DISPLAY_SEQUENCE);
                fieldObj.sortSequence = Number(field.SORT_SEQUENCE);
                fieldObj.referenceValue = (field && field.REFERENCE_VALUE && typeof field.REFERENCE_VALUE === 'string')
                    ? field.REFERENCE_VALUE : null;
                fieldObj.isDefaultMetadataField = isDefaultMetadataField;
                displayableMetadataFields.push(fieldObj);
            }
        });
        return displayableMetadataFields;
    }
    getListDisplayColumnConfig(metaDataFields, defaultMetadataFields, customMetadataFields) {
        const data = {
            displayableMetadataFields: [],
            displayColumns: []
        };
        let displayColumns = [];
        let displayableMetadataFields = [];
        const displayableCustomMetadataFields = [];
        if (metaDataFields && metaDataFields.metadata_element_list) {
            for (const metadataElement of metaDataFields.metadata_element_list) {
                const metadataFields = metadataElement.metadata_element_list;
                if (metadataFields && Array.isArray(metadataFields)) {
                    displayableMetadataFields.push(...this.getDisplayMetadataFields(defaultMetadataFields, metadataFields, true));
                    // displayableCustomMetadataFields.push(...this.getDisplayMetadataFields(customMetadataFields, metadataFields, false));
                }
            }
        }
        displayableMetadataFields = displayableMetadataFields.sort((a, b) => {
            if (a.isDefaultMetadataField === b.isDefaultMetadataField) {
                return a.sequence - b.sequence;
            }
            else {
                return a.isDefaultMetadataField ? -1 : 1;
            }
        });
        displayableMetadataFields = displayableMetadataFields.concat(displayableCustomMetadataFields);
        displayColumns = displayableMetadataFields.map((column) => column.id);
        displayColumns.unshift('Select');
        displayColumns.unshift('Expand');
        // displayColumns.push('Actions');
        data.displayColumns = displayColumns;
        data.displayableMetadataFields = displayableMetadataFields;
        return data;
    }
};
MetdataListViewFieldObjectHelperService.ɵprov = i0.ɵɵdefineInjectable({ factory: function MetdataListViewFieldObjectHelperService_Factory() { return new MetdataListViewFieldObjectHelperService(); }, token: MetdataListViewFieldObjectHelperService, providedIn: "root" });
MetdataListViewFieldObjectHelperService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], MetdataListViewFieldObjectHelperService);
export { MetdataListViewFieldObjectHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0ZGF0YS1saXN0LXZpZXctZmllbGQtb2JqZWN0LWhlbHBlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL21ldGRhdGEtbGlzdC12aWV3LWZpZWxkLW9iamVjdC1oZWxwZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFPM0MsSUFBYSx1Q0FBdUMsR0FBcEQsTUFBYSx1Q0FBdUM7SUFFbEQ7SUFBZ0IsQ0FBQztJQUVqQix3QkFBd0IsQ0FBQyxpQkFBNkIsRUFBRSxrQkFBOEIsRUFBRSxzQkFBK0I7UUFDckgsTUFBTSx5QkFBeUIsR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsRUFBRTtZQUMzRSxPQUFPLHlCQUF5QixDQUFDO1NBQ2xDO1FBQ0QsaUJBQWlCLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzVCLE1BQU0sUUFBUSxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDNUcsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ25ELFFBQVEsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDcEQsUUFBUSxDQUFDLGNBQWMsR0FBRyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsZUFBZSxJQUFJLE9BQU8sS0FBSyxDQUFDLGVBQWUsS0FBSyxRQUFRLENBQUM7b0JBQ3JHLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2pDLFFBQVEsQ0FBQyxzQkFBc0IsR0FBRyxzQkFBc0IsQ0FBQztnQkFDekQseUJBQXlCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLHlCQUF5QixDQUFDO0lBQ25DLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxjQUFjLEVBQUUscUJBQXFCLEVBQUUsb0JBQW9CO1FBQ3BGLE1BQU0sSUFBSSxHQUF3QjtZQUNoQyx5QkFBeUIsRUFBRSxFQUFFO1lBQzdCLGNBQWMsRUFBRSxFQUFFO1NBQ25CLENBQUM7UUFDRixJQUFJLGNBQWMsR0FBa0IsRUFBRSxDQUFDO1FBQ3ZDLElBQUkseUJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQ25DLE1BQU0sK0JBQStCLEdBQUcsRUFBRSxDQUFDO1FBQzNDLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRTtZQUMxRCxLQUFLLE1BQU0sZUFBZSxJQUFJLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDbEUsTUFBTSxjQUFjLEdBQUcsZUFBZSxDQUFDLHFCQUFxQixDQUFDO2dCQUM3RCxJQUFJLGNBQWMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO29CQUNuRCx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzlHLHVIQUF1SDtpQkFDeEg7YUFDRjtTQUNGO1FBRUQseUJBQXlCLEdBQUcseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ2xFLElBQUksQ0FBQyxDQUFDLHNCQUFzQixLQUFLLENBQUMsQ0FBQyxzQkFBc0IsRUFBRTtnQkFDekQsT0FBTyxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDMUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILHlCQUF5QixHQUFHLHlCQUF5QixDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBRTlGLGNBQWMsR0FBRyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFXLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMzRSxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakMsa0NBQWtDO1FBRWxDLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1FBQ3JDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyx5QkFBeUIsQ0FBQztRQUMzRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7Q0FFRixDQUFBOztBQTlEWSx1Q0FBdUM7SUFKbkQsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUVXLHVDQUF1QyxDQThEbkQ7U0E5RFksdUNBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEaXNwbGF5Q29sdW1uQ29uZmlnIH0gZnJvbSAnLi4vT2JqZWN0cy9EaXNwbGF5Q29sdW1uQ29uZmlnJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBNZXRkYXRhTGlzdFZpZXdGaWVsZE9iamVjdEhlbHBlclNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBnZXREaXNwbGF5TWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZExpc3Q6IEFycmF5PGFueT4sIG1ldGFkYXRhRmlsdGVyTGlzdDogQXJyYXk8YW55PiwgaXNEZWZhdWx0TWV0YWRhdGFGaWVsZDogYm9vbGVhbik6IEFycmF5PGFueT4ge1xyXG4gICAgY29uc3QgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KG1ldGFkYXRhRmllbGRMaXN0KSB8fCAhQXJyYXkuaXNBcnJheShtZXRhZGF0YUZpbHRlckxpc3QpKSB7XHJcbiAgICAgIHJldHVybiBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG4gICAgbWV0YWRhdGFGaWVsZExpc3QubWFwKGZpZWxkID0+IHtcclxuICAgICAgY29uc3QgZmllbGRPYmogPSBtZXRhZGF0YUZpbHRlckxpc3QuZmluZChlID0+IChlLmlkID09PSBmaWVsZC5GSUVMRF9JRCkgJiYgKGZpZWxkLklTX0dSSURfVklFVyA9PT0gJ3RydWUnKSk7XHJcbiAgICAgIGlmIChmaWVsZE9iaikge1xyXG4gICAgICAgIGZpZWxkT2JqLnNlcXVlbmNlID0gTnVtYmVyKGZpZWxkLkRJU1BMQVlfU0VRVUVOQ0UpO1xyXG4gICAgICAgIGZpZWxkT2JqLnNvcnRTZXF1ZW5jZSA9IE51bWJlcihmaWVsZC5TT1JUX1NFUVVFTkNFKTtcclxuICAgICAgICBmaWVsZE9iai5yZWZlcmVuY2VWYWx1ZSA9IChmaWVsZCAmJiBmaWVsZC5SRUZFUkVOQ0VfVkFMVUUgJiYgdHlwZW9mIGZpZWxkLlJFRkVSRU5DRV9WQUxVRSA9PT0gJ3N0cmluZycpXHJcbiAgICAgICAgICA/IGZpZWxkLlJFRkVSRU5DRV9WQUxVRSA6IG51bGw7XHJcbiAgICAgICAgZmllbGRPYmouaXNEZWZhdWx0TWV0YWRhdGFGaWVsZCA9IGlzRGVmYXVsdE1ldGFkYXRhRmllbGQ7XHJcbiAgICAgICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5wdXNoKGZpZWxkT2JqKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcztcclxuICB9XHJcblxyXG4gIGdldExpc3REaXNwbGF5Q29sdW1uQ29uZmlnKG1ldGFEYXRhRmllbGRzLCBkZWZhdWx0TWV0YWRhdGFGaWVsZHMsIGN1c3RvbU1ldGFkYXRhRmllbGRzKTogRGlzcGxheUNvbHVtbkNvbmZpZyB7XHJcbiAgICBjb25zdCBkYXRhOiBEaXNwbGF5Q29sdW1uQ29uZmlnID0ge1xyXG4gICAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzOiBbXSxcclxuICAgICAgZGlzcGxheUNvbHVtbnM6IFtdXHJcbiAgICB9O1xyXG4gICAgbGV0IGRpc3BsYXlDb2x1bW5zOiBBcnJheTxzdHJpbmc+ID0gW107XHJcbiAgICBsZXQgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgY29uc3QgZGlzcGxheWFibGVDdXN0b21NZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgaWYgKG1ldGFEYXRhRmllbGRzICYmIG1ldGFEYXRhRmllbGRzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICBmb3IgKGNvbnN0IG1ldGFkYXRhRWxlbWVudCBvZiBtZXRhRGF0YUZpZWxkcy5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICBjb25zdCBtZXRhZGF0YUZpZWxkcyA9IG1ldGFkYXRhRWxlbWVudC5tZXRhZGF0YV9lbGVtZW50X2xpc3Q7XHJcbiAgICAgICAgaWYgKG1ldGFkYXRhRmllbGRzICYmIEFycmF5LmlzQXJyYXkobWV0YWRhdGFGaWVsZHMpKSB7XHJcbiAgICAgICAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLnB1c2goLi4udGhpcy5nZXREaXNwbGF5TWV0YWRhdGFGaWVsZHMoZGVmYXVsdE1ldGFkYXRhRmllbGRzLCBtZXRhZGF0YUZpZWxkcywgdHJ1ZSkpO1xyXG4gICAgICAgICAgLy8gZGlzcGxheWFibGVDdXN0b21NZXRhZGF0YUZpZWxkcy5wdXNoKC4uLnRoaXMuZ2V0RGlzcGxheU1ldGFkYXRhRmllbGRzKGN1c3RvbU1ldGFkYXRhRmllbGRzLCBtZXRhZGF0YUZpZWxkcywgZmFsc2UpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5zb3J0KChhLCBiKSA9PiB7XHJcbiAgICAgIGlmIChhLmlzRGVmYXVsdE1ldGFkYXRhRmllbGQgPT09IGIuaXNEZWZhdWx0TWV0YWRhdGFGaWVsZCkge1xyXG4gICAgICAgIHJldHVybiBhLnNlcXVlbmNlIC0gYi5zZXF1ZW5jZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gYS5pc0RlZmF1bHRNZXRhZGF0YUZpZWxkID8gLTEgOiAxO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMuY29uY2F0KGRpc3BsYXlhYmxlQ3VzdG9tTWV0YWRhdGFGaWVsZHMpO1xyXG5cclxuICAgIGRpc3BsYXlDb2x1bW5zID0gZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5tYXAoKGNvbHVtbjogYW55KSA9PiBjb2x1bW4uaWQpO1xyXG4gICAgZGlzcGxheUNvbHVtbnMudW5zaGlmdCgnU2VsZWN0Jyk7XHJcbiAgICBkaXNwbGF5Q29sdW1ucy51bnNoaWZ0KCdFeHBhbmQnKTtcclxuICAgIC8vIGRpc3BsYXlDb2x1bW5zLnB1c2goJ0FjdGlvbnMnKTtcclxuXHJcbiAgICBkYXRhLmRpc3BsYXlDb2x1bW5zID0gZGlzcGxheUNvbHVtbnM7XHJcbiAgICBkYXRhLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gICAgcmV0dXJuIGRhdGE7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=