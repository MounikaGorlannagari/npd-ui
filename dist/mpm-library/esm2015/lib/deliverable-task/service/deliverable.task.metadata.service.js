import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { ViewTypes } from '../../mpm-utils/objects/ViewTypes';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import * as i0 from "@angular/core";
import * as i1 from "../../project/shared/services/category.service";
import * as i2 from "../../mpm-utils/services/entity.appdef.service";
import * as i3 from "../../mpm-utils/services/otmm.service";
import * as i4 from "../../mpm-utils/services/sharing.service";
import * as i5 from "../../shared/services/view-config.service";
let DeliverableTaskMetadataService = class DeliverableTaskMetadataService {
    constructor(categoryService, entityAppDefService, otmmService, sharingService, viewConfigService) {
        this.categoryService = categoryService;
        this.entityAppDefService = entityAppDefService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.viewConfigService = viewConfigService;
    }
    getMPMFieldsByLevel(level) {
        /* if (level) {
          let viewName: string;
          if (level === MPM_LEVELS.PROJECT) {
            viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT;
          } else if (level === MPM_LEVELS.TASK) {
            viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK;
          }
          return this.viewConfigService.getAllDisplayFeildForViewConfig(viewName);
        } */
        if (level) {
            let viewName;
            if (level === MPM_LEVELS.PROJECT) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.MY_TASK_VIEW_DASHBOARD_BY_PROJECT;
            }
            else if (level === MPM_LEVELS.TASK) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.MY_TASK_VIEW_DASHBOARD_BY_TASK;
            }
            else if (level === MPM_LEVELS.CAMPAIGN) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN;
            }
            return this.viewConfigService.getAllDisplayFeildForViewConfig(viewName);
        }
    }
};
DeliverableTaskMetadataService.ctorParameters = () => [
    { type: CategoryService },
    { type: EntityAppDefService },
    { type: OTMMService },
    { type: SharingService },
    { type: ViewConfigService }
];
DeliverableTaskMetadataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function DeliverableTaskMetadataService_Factory() { return new DeliverableTaskMetadataService(i0.ɵɵinject(i1.CategoryService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.OTMMService), i0.ɵɵinject(i4.SharingService), i0.ɵɵinject(i5.ViewConfigService)); }, token: DeliverableTaskMetadataService, providedIn: "root" });
DeliverableTaskMetadataService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], DeliverableTaskMetadataService);
export { DeliverableTaskMetadataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUudGFzay5tZXRhZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL2RlbGl2ZXJhYmxlLnRhc2subWV0YWRhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBRWpGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUUzRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBRTFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7Ozs7O0FBSzlFLElBQWEsOEJBQThCLEdBQTNDLE1BQWEsOEJBQThCO0lBRXpDLFlBQ1MsZUFBZ0MsRUFDaEMsbUJBQXdDLEVBQ3hDLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLGlCQUFvQztRQUpwQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUN6QyxDQUFDO0lBRUwsbUJBQW1CLENBQUMsS0FBSztRQUV2Qjs7Ozs7Ozs7WUFRSTtRQUNKLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxRQUFnQixDQUFDO1lBQ3JCLElBQUksS0FBSyxLQUFLLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBRWhDLFFBQVEsR0FBRyxTQUFTLENBQUMsaUNBQWlDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsaUNBQWlDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsaUNBQWlDLENBQUMsaUNBQWlDLENBQUM7YUFDMU07aUJBQU0sSUFBSSxLQUFLLEtBQUssVUFBVSxDQUFDLElBQUksRUFBRTtnQkFDcEMsUUFBUSxHQUFHLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyw4QkFBOEIsQ0FBQzthQUM5TDtpQkFBTSxJQUFJLEtBQUssS0FBSyxVQUFVLENBQUMsUUFBUSxFQUFFO2dCQUN4QyxRQUFRLEdBQUcsU0FBUyxDQUFDLGtDQUFrQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGtDQUFrQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGtDQUFrQyxDQUFDLGtDQUFrQyxDQUFDO2FBQzlNO1lBQ0QsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsK0JBQStCLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDekU7SUFDSCxDQUFDO0NBQ0YsQ0FBQTs7WUEvQjJCLGVBQWU7WUFDWCxtQkFBbUI7WUFDM0IsV0FBVztZQUNSLGNBQWM7WUFDWCxpQkFBaUI7OztBQVBsQyw4QkFBOEI7SUFIMUMsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLDhCQUE4QixDQWtDMUM7U0FsQ1ksOEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IENhdGVnb3J5U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE1ldGRhdGFGaWVsZHNDb25maWdPYmogfSBmcm9tICcuLi9PYmplY3RzL01ldGRhdGFGaWVsZHNDb25maWdPYmonO1xyXG5pbXBvcnQgeyBWaWV3VHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9WaWV3VHlwZXMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVUYXNrTWV0YWRhdGFTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIGdldE1QTUZpZWxkc0J5TGV2ZWwobGV2ZWwpOiBPYnNlcnZhYmxlPFZpZXdDb25maWc+IHtcclxuXHJcbiAgICAvKiBpZiAobGV2ZWwpIHtcclxuICAgICAgbGV0IHZpZXdOYW1lOiBzdHJpbmc7XHJcbiAgICAgIGlmIChsZXZlbCA9PT0gTVBNX0xFVkVMUy5QUk9KRUNUKSB7XHJcbiAgICAgICAgdmlld05hbWUgPSBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9QUk9KRUNUO1xyXG4gICAgICB9IGVsc2UgaWYgKGxldmVsID09PSBNUE1fTEVWRUxTLlRBU0spIHtcclxuICAgICAgICB2aWV3TmFtZSA9IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1RBU0s7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3TmFtZSk7XHJcbiAgICB9ICovXHJcbiAgICBpZiAobGV2ZWwpIHtcclxuICAgICAgbGV0IHZpZXdOYW1lOiBzdHJpbmc7XHJcbiAgICAgIGlmIChsZXZlbCA9PT0gTVBNX0xFVkVMUy5QUk9KRUNUKSB7XHJcblxyXG4gICAgICAgIHZpZXdOYW1lID0gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfUFJPSkVDVC5WSUVXX05BTUUgPyBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9QUk9KRUNULlZJRVdfTkFNRSA6IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1BST0pFQ1QuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9QUk9KRUNUO1xyXG4gICAgICB9IGVsc2UgaWYgKGxldmVsID09PSBNUE1fTEVWRUxTLlRBU0spIHtcclxuICAgICAgICB2aWV3TmFtZSA9IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1RBU0suVklFV19OQU1FID8gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfVEFTSy5WSUVXX05BTUUgOiBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9UQVNLLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfVEFTSztcclxuICAgICAgfSBlbHNlIGlmIChsZXZlbCA9PT0gTVBNX0xFVkVMUy5DQU1QQUlHTikge1xyXG4gICAgICAgIHZpZXdOYW1lID0gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfQ0FNUEFJR04uVklFV19OQU1FID8gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfQ0FNUEFJR04uVklFV19OQU1FIDogVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfQ0FNUEFJR04uTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9DQU1QQUlHTjtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdOYW1lKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19