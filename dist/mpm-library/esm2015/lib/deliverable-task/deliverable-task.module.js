import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliverableTaskListComponent } from './deliverable-task-list/deliverable-task-list.component';
import { MaterialModule } from '../material.module';
import { DeliverableModule } from '../project/tasks/deliverable/deliverable.module';
import { MpmLibraryModule } from '../mpm-library.module';
let DeliverableTaskModule = class DeliverableTaskModule {
};
DeliverableTaskModule = __decorate([
    NgModule({
        declarations: [
            DeliverableTaskListComponent
        ],
        imports: [
            CommonModule,
            MaterialModule,
            DeliverableModule,
            MpmLibraryModule
        ],
        exports: [
            DeliverableTaskListComponent
        ]
    })
], DeliverableTaskModule);
export { DeliverableTaskModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdGFzay5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9kZWxpdmVyYWJsZS10YXNrL2RlbGl2ZXJhYmxlLXRhc2subW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUN2RyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDcEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFpQnpELElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0NBQUksQ0FBQTtBQUF6QixxQkFBcUI7SUFkakMsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osNEJBQTRCO1NBQzdCO1FBQ0QsT0FBTyxFQUFFO1lBQ1AsWUFBWTtZQUNaLGNBQWM7WUFDZCxpQkFBaUI7WUFDakIsZ0JBQWdCO1NBQ2pCO1FBQ0QsT0FBTyxFQUFFO1lBQ1AsNEJBQTRCO1NBQzdCO0tBQ0YsQ0FBQztHQUNXLHFCQUFxQixDQUFJO1NBQXpCLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlVGFza0xpc3RDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLXRhc2stbGlzdC9kZWxpdmVyYWJsZS10YXNrLWxpc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZU1vZHVsZSB9IGZyb20gJy4uL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUubW9kdWxlJztcclxuaW1wb3J0IHsgTXBtTGlicmFyeU1vZHVsZSB9IGZyb20gJy4uL21wbS1saWJyYXJ5Lm1vZHVsZSc7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIERlbGl2ZXJhYmxlVGFza0xpc3RDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgRGVsaXZlcmFibGVNb2R1bGUsXHJcbiAgICBNcG1MaWJyYXJ5TW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBEZWxpdmVyYWJsZVRhc2tMaXN0Q29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVUYXNrTW9kdWxlIHsgfVxyXG4iXX0=