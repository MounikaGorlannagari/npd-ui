import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl, Validators } from '@angular/forms';
import { NotificationService } from '../notification/notification.service';
let PaginationComponent = class PaginationComponent {
    constructor(notificationService) {
        this.notificationService = notificationService;
        this.paginator = new EventEmitter();
        this.pageSizeOptions = [5, 10, 25, 50, 100];
        this.totalPageNumber = 1;
    }
    calculateTotalPageSize() {
        this.pageIndex = this.pageIndex ? Number(this.pageIndex) : (this.matPaginator.pageIndex + 1);
        const pageSize = this.pageSize ? Number(this.pageSize) : this.matPaginator.pageSize;
        this.matPaginator.pageIndex = this.pageIndex - 1;
        this.matPaginator.pageSize = pageSize;
        this.totalPageNumber = Math.ceil(this.length / pageSize);
    }
    changePaginator() {
        if (this.pageSize !== this.matPaginator.pageSize) {
            this.pageIndex = 1;
            this.matPaginator.pageIndex = 0;
        }
        else {
            this.pageIndex = this.matPaginator.pageIndex + 1;
        }
        this.pageSize = this.matPaginator.pageSize;
        this.initializeForm();
        const pageObject = {
            length: this.matPaginator.length,
            pageIndex: this.matPaginator.pageIndex,
            pageSize: this.matPaginator.pageSize,
            totalPages: this.totalPageNumber
        };
        this.paginator.next(pageObject);
    }
    onpageValueChange(event) {
        if (event.target && event.target.value && (event.relatedTarget == null || event.relatedTarget == undefined)) {
            if (Number(event.target.value) <= this.totalPageNumber) {
                this.matPaginator.pageIndex = Number(event.target.value) - 1;
                this.changePaginator();
            }
            else {
                this.notificationService.error('Current page exceeds total no. of pages');
            }
        }
    }
    initializeForm() {
        this.calculateTotalPageSize();
        this.manualPageChangeControl = new FormControl(this.pageIndex, [Validators.max(this.totalPageNumber), Validators.min(1), Validators.required]);
    }
    ngOnChanges() {
        this.initializeForm();
    }
    numberOnly(event) {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    setFirstPage() {
        this.matPaginator.firstPage();
    }
    setLastPage() {
        this.matPaginator.lastPage();
    }
    ngOnInit() {
        this.initializeForm();
        this.matPaginator.page.subscribe(() => {
            this.changePaginator();
        });
    }
};
PaginationComponent.ctorParameters = () => [
    { type: NotificationService }
];
__decorate([
    Input()
], PaginationComponent.prototype, "length", void 0);
__decorate([
    Input()
], PaginationComponent.prototype, "pageSize", void 0);
__decorate([
    Input()
], PaginationComponent.prototype, "pageIndex", void 0);
__decorate([
    Output()
], PaginationComponent.prototype, "paginator", void 0);
__decorate([
    ViewChild(MatPaginator, { static: true })
], PaginationComponent.prototype, "matPaginator", void 0);
PaginationComponent = __decorate([
    Component({
        selector: 'mpm-pagination',
        template: "<div class=\"pagination\">\r\n    <mat-form-field (keydown.enter)=\"onpageValueChange($event)\" (focusout)=\"onpageValueChange($event)\">\r\n        <input min=\"1\" (keypress)=\"numberOnly($event)\" matInput type=\"number\" [(value)]=\"pageIndex\" [formControl]=\"manualPageChangeControl\">\r\n    </mat-form-field>\r\n    <span class=\"total-page-section-info\"> of {{totalPageNumber}} page{{(totalPageNumber !== 1) ? 's': ''}}</span>\r\n    <mat-paginator class=\"paginator\" [length]=\"length\" [pageSize]=\"pageSize\" [pageSizeOptions]=\"pageSizeOptions\"\r\n        [showFirstLastButtons]=\"true\">\r\n    </mat-paginator>\r\n</div>",
        styles: [".pagination{display:flex;margin-bottom:0}.pagination mat-form-field{width:42px;font-size:12px;margin:4px 4px 0 0}.pagination .total-page-section-info{font-size:12px;margin-top:20px}.mat-paginator{background-color:transparent!important}.mat-paginator-page-size-select{width:45px!important}.mat-paginator-page-size{font-size:10px!important;margin:0!important}.mat-paginator-range-actions .mat-icon-button{width:30px!important}.mat-paginator-range-actions .mat-icon-button .mat-button-wrapper{line-height:30px!important}.mat-paginator-range-label{margin:0!important}"]
    })
], PaginationComponent);
export { PaginationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wYWdpbmF0aW9uL3BhZ2luYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckcsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzNELE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFTM0UsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFhNUIsWUFDVyxtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVR6QyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUk5QyxvQkFBZSxHQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELG9CQUFlLEdBQUcsQ0FBQyxDQUFDO0lBS2hCLENBQUM7SUFFTCxzQkFBc0I7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzdGLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO1FBQ3BGLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUV0QyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQsZUFBZTtRQUNYLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUM5QyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDbkM7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUUzQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsTUFBTSxVQUFVLEdBQXdCO1lBQ3BDLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU07WUFDaEMsU0FBUyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUztZQUN0QyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRO1lBQ3BDLFVBQVUsRUFBRSxJQUFJLENBQUMsZUFBZTtTQUNuQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQUs7UUFDbkIsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxTQUFTLENBQUMsRUFBRTtZQUN6RyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQzFCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQzthQUM3RTtTQUNKO0lBQ0wsQ0FBQztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFDekQsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBSztRQUNaLE1BQU0sUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzdELElBQUksUUFBUSxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFO1lBQ25ELE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFFaEIsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFBO0lBQ2pDLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtJQUNoQyxDQUFDO0lBQ0QsUUFBUTtRQUNKLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FHSixDQUFBOztZQTVFbUMsbUJBQW1COztBQVoxQztJQUFSLEtBQUssRUFBRTttREFBUTtBQUNQO0lBQVIsS0FBSyxFQUFFO3FEQUFVO0FBQ1Q7SUFBUixLQUFLLEVBQUU7c0RBQVc7QUFDVDtJQUFULE1BQU0sRUFBRTtzREFBcUM7QUFFSDtJQUExQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO3lEQUE0QjtBQVA3RCxtQkFBbUI7SUFOL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGdCQUFnQjtRQUMxQiwwb0JBQTBDOztLQUU3QyxDQUFDO0dBRVcsbUJBQW1CLENBMEYvQjtTQTFGWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25DaGFuZ2VzLCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFBhZ2VPYmplY3RJbnRlcmZhY2UgfSBmcm9tICcuL3BhZ2Uub2JqZWN0LmludGVyZmFjZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXBhZ2luYXRpb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BhZ2luYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcGFnaW5hdGlvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUGFnaW5hdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKSBsZW5ndGg7XHJcbiAgICBASW5wdXQoKSBwYWdlU2l6ZTtcclxuICAgIEBJbnB1dCgpIHBhZ2VJbmRleDtcclxuICAgIEBPdXRwdXQoKSBwYWdpbmF0b3IgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvciwgeyBzdGF0aWM6IHRydWUgfSkgbWF0UGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcblxyXG4gICAgcGFnZVNpemVPcHRpb25zOiBudW1iZXJbXSA9IFs1LCAxMCwgMjUsIDUwLCAxMDBdO1xyXG4gICAgdG90YWxQYWdlTnVtYmVyID0gMTtcclxuICAgIG1hbnVhbFBhZ2VDaGFuZ2VDb250cm9sOiBGb3JtQ29udHJvbDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjYWxjdWxhdGVUb3RhbFBhZ2VTaXplKCkge1xyXG4gICAgICAgIHRoaXMucGFnZUluZGV4ID0gdGhpcy5wYWdlSW5kZXggPyBOdW1iZXIodGhpcy5wYWdlSW5kZXgpIDogKHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VJbmRleCArIDEpO1xyXG4gICAgICAgIGNvbnN0IHBhZ2VTaXplID0gdGhpcy5wYWdlU2l6ZSA/IE51bWJlcih0aGlzLnBhZ2VTaXplKSA6IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VTaXplO1xyXG4gICAgICAgIHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VJbmRleCA9IHRoaXMucGFnZUluZGV4IC0gMTtcclxuICAgICAgICB0aGlzLm1hdFBhZ2luYXRvci5wYWdlU2l6ZSA9IHBhZ2VTaXplO1xyXG5cclxuICAgICAgICB0aGlzLnRvdGFsUGFnZU51bWJlciA9IE1hdGguY2VpbCh0aGlzLmxlbmd0aCAvIHBhZ2VTaXplKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VQYWdpbmF0b3IoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFnZVNpemUgIT09IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VTaXplKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZUluZGV4ID0gMTtcclxuICAgICAgICAgICAgdGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4ID0gMDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2VJbmRleCA9IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VJbmRleCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucGFnZVNpemUgPSB0aGlzLm1hdFBhZ2luYXRvci5wYWdlU2l6ZTtcclxuXHJcbiAgICAgICAgdGhpcy5pbml0aWFsaXplRm9ybSgpO1xyXG4gICAgICAgIGNvbnN0IHBhZ2VPYmplY3Q6IFBhZ2VPYmplY3RJbnRlcmZhY2UgPSB7XHJcbiAgICAgICAgICAgIGxlbmd0aDogdGhpcy5tYXRQYWdpbmF0b3IubGVuZ3RoLFxyXG4gICAgICAgICAgICBwYWdlSW5kZXg6IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VJbmRleCxcclxuICAgICAgICAgICAgcGFnZVNpemU6IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VTaXplLFxyXG4gICAgICAgICAgICB0b3RhbFBhZ2VzOiB0aGlzLnRvdGFsUGFnZU51bWJlclxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5wYWdpbmF0b3IubmV4dChwYWdlT2JqZWN0KTtcclxuICAgIH1cclxuXHJcbiAgICBvbnBhZ2VWYWx1ZUNoYW5nZShldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC50YXJnZXQgJiYgZXZlbnQudGFyZ2V0LnZhbHVlICYmIChldmVudC5yZWxhdGVkVGFyZ2V0ID09IG51bGwgfHwgZXZlbnQucmVsYXRlZFRhcmdldCA9PSB1bmRlZmluZWQpKSB7XHJcbiAgICAgICAgICAgIGlmIChOdW1iZXIoZXZlbnQudGFyZ2V0LnZhbHVlKSA8PSB0aGlzLnRvdGFsUGFnZU51bWJlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4ID0gTnVtYmVyKGV2ZW50LnRhcmdldC52YWx1ZSkgLSAxO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGFuZ2VQYWdpbmF0b3IoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignQ3VycmVudCBwYWdlIGV4Y2VlZHMgdG90YWwgbm8uIG9mIHBhZ2VzJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZUZvcm0oKSB7XHJcbiAgICAgICAgdGhpcy5jYWxjdWxhdGVUb3RhbFBhZ2VTaXplKCk7XHJcbiAgICAgICAgdGhpcy5tYW51YWxQYWdlQ2hhbmdlQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCh0aGlzLnBhZ2VJbmRleCxcclxuICAgICAgICAgICAgW1ZhbGlkYXRvcnMubWF4KHRoaXMudG90YWxQYWdlTnVtYmVyKSwgVmFsaWRhdG9ycy5taW4oMSksIFZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcygpIHtcclxuICAgICAgICB0aGlzLmluaXRpYWxpemVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbnVtYmVyT25seShldmVudCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGNvbnN0IGNoYXJDb2RlID0gKGV2ZW50LndoaWNoKSA/IGV2ZW50LndoaWNoIDogZXZlbnQua2V5Q29kZTtcclxuICAgICAgICBpZiAoY2hhckNvZGUgPiAzMSAmJiAoY2hhckNvZGUgPCA0OCB8fCBjaGFyQ29kZSA+IDU3KSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZXRGaXJzdFBhZ2UoKXtcclxuICAgICAgICB0aGlzLm1hdFBhZ2luYXRvci5maXJzdFBhZ2UoKVxyXG4gICAgfVxyXG5cclxuICAgIHNldExhc3RQYWdlKCl7XHJcbiAgICAgICAgdGhpcy5tYXRQYWdpbmF0b3IubGFzdFBhZ2UoKVxyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5pbml0aWFsaXplRm9ybSgpO1xyXG4gICAgICAgIHRoaXMubWF0UGFnaW5hdG9yLnBhZ2Uuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jaGFuZ2VQYWdpbmF0b3IoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgXHJcbn1cclxuIl19