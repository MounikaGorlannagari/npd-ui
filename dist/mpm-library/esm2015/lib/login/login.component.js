import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AuthenticationService } from '../mpm-utils/services/authentication.service';
import { NotificationService } from '../notification/notification.service';
import { UtilService } from '../mpm-utils/services/util.service';
let LoginComponent = class LoginComponent {
    constructor(authService, notification, utilService) {
        this.authService = authService;
        this.notification = notification;
        this.utilService = utilService;
        this.authenticated = new EventEmitter();
        this.isProcessing = false;
        this.isInvalid = false;
        this.usermodel = {
            username: '',
            password: ''
        };
        this.resetFormState = () => {
            setTimeout(() => { this.isInvalid = false; }, 1000);
        };
        this.toggleLoadingBtn = (isShow) => {
            this.isProcessing = isShow ? true : false;
        };
    }
    authenticate(userData) {
        if (!userData || !userData.username || !userData.password) {
            this.isInvalid = true;
            this.resetFormState();
            return;
        }
        this.toggleLoadingBtn(true);
        this.authService.getPreLoginDetails().subscribe(data => {
            this.authService.setSessionInfo(data);
            this.authService.authenticateInOtds(userData.username, userData.password)
                .subscribe(authData => {
                //  MPMV3-2237
                /* this.authService.getUserInfo()
                    .subscribe(userInfo => {
                        const defer = $.Deferred();
                        this.utilService.indexer().then(
                            (ticketObj) => {
                                console.log(ticketObj);
                                this.toggleLoadingBtn(false);
                                this.authenticated.emit({
                                    isAuthenticated: true,
                                    reason: 'Successful.'
                                });
                                console.log('Logged in: ' + userInfo);
                            }
                        ).fail((errCode) => {
                            console.log(errCode);
                            defer.reject(errCode);
                        });

                    }, userErr => {
                        this.toggleLoadingBtn(false);
                        this.authenticated.emit({
                            isAuthenticated: false,
                            reason: 'Invalid Credentials'
                        });
                        this.notification.error('Invalid Credentials');
                        this.isInvalid = true;
                        this.resetFormState();
                    }); */
                this.authService.getUserInfo()
                    .subscribe(userInfo => {
                    this.toggleLoadingBtn(false);
                    this.authenticated.emit({
                        isAuthenticated: true,
                        reason: 'Successful.'
                    });
                    console.log('Logged in: ' + userInfo);
                }, userErr => {
                    this.toggleLoadingBtn(false);
                    this.authenticated.emit({
                        isAuthenticated: false,
                        reason: 'Invalid Credentials'
                    });
                    this.notification.error('Invalid Credentials');
                    this.isInvalid = true;
                    this.resetFormState();
                });
            }, authErr => {
                this.toggleLoadingBtn(false);
                this.authenticated.emit({
                    isAuthenticated: false,
                    reason: 'Invalid Credentials'
                });
                this.notification.error('Invalid Credentials');
                this.isInvalid = true;
                this.resetFormState();
            });
        }, error => {
            this.toggleLoadingBtn(false);
            this.authenticated.emit({
                isAuthenticated: false,
                reason: 'Something went wrong, Contant Admin'
            });
            this.notification.error('Something went wrong, Contant Admin');
            this.isInvalid = true;
            this.resetFormState();
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: AuthenticationService },
    { type: NotificationService },
    { type: UtilService }
];
__decorate([
    Output()
], LoginComponent.prototype, "authenticated", void 0);
LoginComponent = __decorate([
    Component({
        selector: 'mpm-login',
        template: "<div class=\"login-container\">\r\n    <mat-card class=\"login-card\" [@flyInOut]=\"'in'\" [ngClass]=\"{'invalid': isInvalid}\">\r\n        <div class=\"mat-card-custom-header\">\r\n        </div>\r\n        <mat-card-content color=\"primary\">\r\n            <mat-grid-list cols=\"12\">\r\n                <mat-form-field class=\"login-form-field\" appearance=\"outline\">\r\n                    <mat-label>Username</mat-label>\r\n                    <input matInput type=\"text\" placeholder=\"Username\" [(ngModel)]=\"usermodel.username\" />\r\n                    <button mat-button *ngIf=\"usermodel.username\" matSuffix mat-icon-button aria-label=\"Clear\"\r\n                        (click)=\"usermodel.username=''\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </mat-form-field>\r\n            </mat-grid-list>\r\n            <mat-grid-list cols=\"12\">\r\n                <mat-form-field class=\"login-form-field\" appearance=\"outline\">\r\n                    <input matInput type=\"password\" placeholder=\"Password\" [(ngModel)]=\"usermodel.password\" />\r\n                    <mat-label>Password</mat-label>\r\n                    <button mat-button *ngIf=\"usermodel.password\" matSuffix mat-icon-button aria-label=\"Clear\"\r\n                        (click)=\"usermodel.password=''\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </mat-form-field>\r\n            </mat-grid-list>\r\n            <mat-grid-list cols=\"12\" class=\"login-action\">\r\n                <button class=\"login-button\" mat-button color=\"primary\" (click)=\"authenticate(usermodel)\"\r\n                    *ngIf=\"!isProcessing\">SIGN\r\n                    IN</button>\r\n                <mat-progress-bar mode=\"query\" *ngIf=\"isProcessing\"></mat-progress-bar>\r\n            </mat-grid-list>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>",
        animations: [
            trigger('flyInOut', [
                state('in', style({ opacity: 1, transform: 'translateX(0)' })),
                transition('void => *', [
                    style({
                        opacity: 0,
                        transform: 'translateY(-25%)'
                    }),
                    animate('0.5s ease-in')
                ]),
                transition('* => void', [
                    animate('0.5s 0.5s ease-out', style({
                        opacity: 0,
                        transform: 'translateY(25%)'
                    }))
                ])
            ])
        ],
        styles: [".login-container{height:100%}.login-container mat-card{text-align:center;width:300px;top:10%}.login-container .login-form-field,.login-container .mat-button{width:100%}.login-container .login-action{margin:15px 0 25px}.login-container .mat-card-custom-header .two-k-avatar-container img{width:50px;height:auto}.login-container .mat-card-custom-header .two-k-avatar-container{padding:10px 0 20px}.login-container mat-card.invalid{-webkit-animation:.82s cubic-bezier(.36,.07,.19,.97) both shake;animation:.82s cubic-bezier(.36,.07,.19,.97) both shake;transform:translate3d(0,0,0);-webkit-backface-visibility:hidden;backface-visibility:hidden;perspective:1000px}@-webkit-keyframes shake{10%,90%{transform:translate3d(-1px,0,0)}20%,80%{transform:translate3d(2px,0,0)}30%,50%,70%{transform:translate3d(-4px,0,0)}40%,60%{transform:translate3d(4px,0,0)}}@keyframes shake{10%,90%{transform:translate3d(-1px,0,0)}20%,80%{transform:translate3d(2px,0,0)}30%,50%,70%{transform:translate3d(-4px,0,0)}40%,60%{transform:translate3d(4px,0,0)}}"]
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbG9naW4vbG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVqRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUNyRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUEyQmpFLElBQWEsY0FBYyxHQUEzQixNQUFhLGNBQWM7SUFXdkIsWUFDVyxXQUFrQyxFQUNsQyxZQUFpQyxFQUNqQyxXQUF3QjtRQUZ4QixnQkFBVyxHQUFYLFdBQVcsQ0FBdUI7UUFDbEMsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQ2pDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBWnpCLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUVyRCxpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGNBQVMsR0FBRztZQUNSLFFBQVEsRUFBRSxFQUFFO1lBQ1osUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO1FBUUYsbUJBQWMsR0FBRyxHQUFHLEVBQUU7WUFDbEIsVUFBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hELENBQUMsQ0FBQTtRQUVELHFCQUFnQixHQUFHLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzlDLENBQUMsQ0FBQTtJQVJHLENBQUM7SUFVTCxZQUFZLENBQUMsUUFBUTtRQUNqQixJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7WUFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFLENBQUMsU0FBUyxDQUMzQyxJQUFJLENBQUMsRUFBRTtZQUNILElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDO2lCQUNwRSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2pCLGNBQWM7Z0JBQ2Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzswQkEyQlU7Z0JBR1QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7cUJBQ3pCLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQzt3QkFDcEIsZUFBZSxFQUFFLElBQUk7d0JBQ3JCLE1BQU0sRUFBRSxhQUFhO3FCQUN4QixDQUFDLENBQUM7b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUM7Z0JBQzFDLENBQUMsRUFBRSxPQUFPLENBQUMsRUFBRTtvQkFDVCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO3dCQUNwQixlQUFlLEVBQUUsS0FBSzt3QkFDdEIsTUFBTSxFQUFFLHFCQUFxQjtxQkFDaEMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUN0QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLE9BQU8sQ0FBQyxFQUFFO2dCQUNULElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7b0JBQ3BCLGVBQWUsRUFBRSxLQUFLO29CQUN0QixNQUFNLEVBQUUscUJBQXFCO2lCQUNoQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNQLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztnQkFDcEIsZUFBZSxFQUFFLEtBQUs7Z0JBQ3RCLE1BQU0sRUFBRSxxQ0FBcUM7YUFDaEQsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0NBQ0osQ0FBQTs7WUFoRzJCLHFCQUFxQjtZQUNwQixtQkFBbUI7WUFDcEIsV0FBVzs7QUFaekI7SUFBVCxNQUFNLEVBQUU7cURBQTRDO0FBRjVDLGNBQWM7SUF4QjFCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxXQUFXO1FBQ3JCLG04REFBcUM7UUFFckMsVUFBVSxFQUFFO1lBQ1IsT0FBTyxDQUFDLFVBQVUsRUFBRTtnQkFDaEIsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxlQUFlLEVBQUUsQ0FBQyxDQUFDO2dCQUM5RCxVQUFVLENBQUMsV0FBVyxFQUFFO29CQUNwQixLQUFLLENBQUM7d0JBQ0YsT0FBTyxFQUFFLENBQUM7d0JBQ1YsU0FBUyxFQUFFLGtCQUFrQjtxQkFDaEMsQ0FBQztvQkFDRixPQUFPLENBQUMsY0FBYyxDQUFDO2lCQUMxQixDQUFDO2dCQUNGLFVBQVUsQ0FBQyxXQUFXLEVBQUU7b0JBQ3BCLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUM7d0JBQ2hDLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFNBQVMsRUFBRSxpQkFBaUI7cUJBQy9CLENBQUMsQ0FBQztpQkFDTixDQUFDO2FBQ0wsQ0FBQztTQUNMOztLQUNKLENBQUM7R0FFVyxjQUFjLENBNEcxQjtTQTVHWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIGFuaW1hdGUgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuXHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tbG9naW4nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2xvZ2luLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignZmx5SW5PdXQnLCBbXHJcbiAgICAgICAgICAgIHN0YXRlKCdpbicsIHN0eWxlKHsgb3BhY2l0eTogMSwgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgwKScgfSkpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKC0yNSUpJ1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBhbmltYXRlKCcwLjVzIGVhc2UtaW4nKVxyXG4gICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMC41cyAwLjVzIGVhc2Utb3V0Jywgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgyNSUpJ1xyXG4gICAgICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIF0pXHJcbiAgICAgICAgXSlcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCB7XHJcblxyXG4gICAgQE91dHB1dCgpIGF1dGhlbnRpY2F0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcclxuXHJcbiAgICBpc1Byb2Nlc3NpbmcgPSBmYWxzZTtcclxuICAgIGlzSW52YWxpZCA9IGZhbHNlO1xyXG4gICAgdXNlcm1vZGVsID0ge1xyXG4gICAgICAgIHVzZXJuYW1lOiAnJyxcclxuICAgICAgICBwYXNzd29yZDogJydcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHJlc2V0Rm9ybVN0YXRlID0gKCkgPT4ge1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4geyB0aGlzLmlzSW52YWxpZCA9IGZhbHNlOyB9LCAxMDAwKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVMb2FkaW5nQnRuID0gKGlzU2hvdykgPT4ge1xyXG4gICAgICAgIHRoaXMuaXNQcm9jZXNzaW5nID0gaXNTaG93ID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGF1dGhlbnRpY2F0ZSh1c2VyRGF0YSkge1xyXG4gICAgICAgIGlmICghdXNlckRhdGEgfHwgIXVzZXJEYXRhLnVzZXJuYW1lIHx8ICF1c2VyRGF0YS5wYXNzd29yZCkge1xyXG4gICAgICAgICAgICB0aGlzLmlzSW52YWxpZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMucmVzZXRGb3JtU3RhdGUoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy50b2dnbGVMb2FkaW5nQnRuKHRydWUpO1xyXG4gICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZ2V0UHJlTG9naW5EZXRhaWxzKCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICBkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2Uuc2V0U2Vzc2lvbkluZm8oZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLmF1dGhlbnRpY2F0ZUluT3Rkcyh1c2VyRGF0YS51c2VybmFtZSwgdXNlckRhdGEucGFzc3dvcmQpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShhdXRoRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgTVBNVjMtMjIzN1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvKiB0aGlzLmF1dGhTZXJ2aWNlLmdldFVzZXJJbmZvKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXNlckluZm8gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlZmVyID0gJC5EZWZlcnJlZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaW5kZXhlcigpLnRoZW4oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aWNrZXRPYmopID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRpY2tldE9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZUxvYWRpbmdCdG4oZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGVkLmVtaXQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQXV0aGVudGljYXRlZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFzb246ICdTdWNjZXNzZnVsLidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ0xvZ2dlZCBpbjogJyArIHVzZXJJbmZvKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkuZmFpbCgoZXJyQ29kZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJDb2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmZXIucmVqZWN0KGVyckNvZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVzZXJFcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlTG9hZGluZ0J0bihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGVkLmVtaXQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0F1dGhlbnRpY2F0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFzb246ICdJbnZhbGlkIENyZWRlbnRpYWxzJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uLmVycm9yKCdJbnZhbGlkIENyZWRlbnRpYWxzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0ludmFsaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVzZXRGb3JtU3RhdGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZ2V0VXNlckluZm8oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXNlckluZm8gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZUxvYWRpbmdCdG4oZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmF1dGhlbnRpY2F0ZWQuZW1pdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0F1dGhlbnRpY2F0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFzb246ICdTdWNjZXNzZnVsLidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdMb2dnZWQgaW46ICcgKyB1c2VySW5mbyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXNlckVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlTG9hZGluZ0J0bihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aGVudGljYXRlZC5lbWl0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQXV0aGVudGljYXRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFzb246ICdJbnZhbGlkIENyZWRlbnRpYWxzJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ0ludmFsaWQgQ3JlZGVudGlhbHMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0ludmFsaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc2V0Rm9ybVN0YXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgYXV0aEVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlTG9hZGluZ0J0bihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aGVudGljYXRlZC5lbWl0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQXV0aGVudGljYXRlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFzb246ICdJbnZhbGlkIENyZWRlbnRpYWxzJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ0ludmFsaWQgQ3JlZGVudGlhbHMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0ludmFsaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc2V0Rm9ybVN0YXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlTG9hZGluZ0J0bihmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGhlbnRpY2F0ZWQuZW1pdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNBdXRoZW50aWNhdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICByZWFzb246ICdTb21ldGhpbmcgd2VudCB3cm9uZywgQ29udGFudCBBZG1pbidcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBDb250YW50IEFkbWluJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzSW52YWxpZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0Rm9ybVN0YXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcblxyXG4iXX0=