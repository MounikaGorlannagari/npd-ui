import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../lib/mpm-utils/services/authentication.service';
import { GloabalConfig as config } from '../../../lib/mpm-utils/config/config';
import { setAuthContextCookie } from '../../../lib/mpm-utils/auth/setAuthContextCookie';
import * as cookie from 'js-cookie';
import { hex_sha1 } from '../../../lib/mpm-utils/auth/sha1';
import { OTMMService } from '../../../lib/mpm-utils/services/otmm.service';
import { OTMMSessionHandler } from '../../../lib/shared/otmmSessionHandler';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "../../mpm-utils/services/authentication.service";
import * as i3 from "../../mpm-utils/services/otmm.service";
let AppRouteService = class AppRouteService {
    constructor(router, authService, otmmService) {
        this.router = router;
        this.authService = authService;
        this.otmmService = otmmService;
    }
    goToLogout() {
        sessionStorage.clear();
        OTMMSessionHandler.unsubscribe();
        this.otmmService.destroyOtmmSession()
            .subscribe(responseDestroyOtmmSession => {
            if (config.config.environment === 'local') {
                this.authService.logout()
                    .subscribe(response => {
                    this.router.navigate(['/login']);
                }, error => {
                    console.error(error);
                });
            }
            else {
                window.location.href = config.config.logoutUrl;
            }
        }, errorDestroyOtmmSession => {
            if (config.config.environment === 'local') {
                this.authService.logout()
                    .subscribe(response => {
                    this.router.navigate(['/login']);
                }, error => {
                    console.error(error);
                });
            }
            else {
                window.location.href = config.config.logoutUrl;
            }
        });
    }
    storeLastVisitLink() {
        const currentUrl = window.location.toString();
        if (!currentUrl) {
            return localStorage.setItem('MPM_V3_LAST_LINK', '');
        }
        else if (currentUrl !== config.getOTDSLoginPageUrl() && currentUrl !== config.getPsOrgEndpoint()) {
            const splittedURL = currentUrl.split('/#/');
            if (Array.isArray(splittedURL) && splittedURL.length === 2 && splittedURL[1] !== '') {
                return localStorage.setItem('MPM_V3_LAST_LINK', currentUrl);
            }
        }
    }
    getLastVisitedLinik() {
        return localStorage.getItem('MPM_V3_LAST_LINK');
    }
    goToLogin() {
        if (config.config.environment === 'local') {
            this.router.navigate(['/login']);
        }
        else {
            setAuthContextCookie().subscribe(done => {
                this.authService.getPreLoginDetails().subscribe(data => {
                    this.authService.setSessionInfo(data);
                    const samlCookieName = data['Authenticator']['SamlArtifactCookieName'];
                    const ctCookieName = data['Authenticator']['CheckName'];
                    const authenticationContext = {
                        samlCookieName: AuthenticationService.sessionUtil.getSessionProp(samlCookieName),
                        checkCookieName: AuthenticationService.sessionUtil.getSessionProp(ctCookieName),
                        cookiesPath: AuthenticationService.sessionUtil.getSessionProp('/') || '/',
                    };
                    cookie.remove(ctCookieName);
                    const artifact = config.getSAMLart();
                    if (artifact) {
                        const ct = hex_sha1(artifact);
                        cookie.set(ctCookieName, ct, { path: authenticationContext.cookiesPath });
                    }
                    this.authService.getUserInfo().subscribe(res => {
                        if (this.getLastVisitedLinik() &&
                            this.getLastVisitedLinik() !== '') {
                            window.location.replace(this.getLastVisitedLinik());
                        }
                        else {
                            this.router.navigate(['/apps/mpm']);
                        }
                    }, error => {
                        this.storeLastVisitLink();
                        if (isDevMode()) {
                            window.location.href = config.getOTDSLoginPageUrl();
                        }
                        else {
                            window.location.href = config.getPsOrgEndpoint();
                        }
                    });
                });
            }, error => {
                this.storeLastVisitLink();
                if (isDevMode()) {
                    window.location.href = config.getOTDSLoginPageUrl();
                }
                else {
                    window.location.href = config.getPsOrgEndpoint();
                }
            });
        }
    }
};
AppRouteService.ctorParameters = () => [
    { type: Router },
    { type: AuthenticationService },
    { type: OTMMService }
];
AppRouteService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppRouteService_Factory() { return new AppRouteService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AuthenticationService), i0.ɵɵinject(i3.OTMMService)); }, token: AppRouteService, providedIn: "root" });
AppRouteService = __decorate([
    Injectable({
        providedIn: 'root',
    })
], AppRouteService);
export { AppRouteService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnJvdXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9sb2dpbi9zZXJ2aWNlcy9hcHAucm91dGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQy9GLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDL0UsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxLQUFLLE1BQU0sTUFBTSxXQUFXLENBQUM7QUFDcEMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzVELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUMzRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQzs7Ozs7QUFNNUUsSUFBYSxlQUFlLEdBQTVCLE1BQWEsZUFBZTtJQUV4QixZQUNXLE1BQWMsRUFDZCxXQUFrQyxFQUNsQyxXQUF3QjtRQUZ4QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsZ0JBQVcsR0FBWCxXQUFXLENBQXVCO1FBQ2xDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBQy9CLENBQUM7SUFFTCxVQUFVO1FBQ04sY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZCLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7YUFDaEMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLEVBQUU7WUFDcEMsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsS0FBSyxPQUFPLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO3FCQUNwQixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3pCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7YUFDbEQ7UUFDTCxDQUFDLEVBQUUsdUJBQXVCLENBQUMsRUFBRTtZQUN6QixJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtnQkFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7cUJBQ3BCLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7b0JBQ1AsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQzthQUNsRDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELGtCQUFrQjtRQUNkLE1BQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNiLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUN2RDthQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLFVBQVUsS0FBSyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtZQUNoRyxNQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNqRixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUM7SUFFRCxtQkFBbUI7UUFDZixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsU0FBUztRQUNMLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssT0FBTyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNwQzthQUFNO1lBQ0gsb0JBQW9CLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QyxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsd0JBQXdCLENBQUMsQ0FBQztvQkFDdkUsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN4RCxNQUFNLHFCQUFxQixHQUFHO3dCQUMxQixjQUFjLEVBQUUscUJBQXFCLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7d0JBQ2hGLGVBQWUsRUFBRSxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQzt3QkFDL0UsV0FBVyxFQUFFLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRztxQkFDNUUsQ0FBQztvQkFDRixNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUM1QixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ3JDLElBQUksUUFBUSxFQUFFO3dCQUNWLE1BQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDOUIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7cUJBQzdFO29CQUNELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUNwQyxHQUFHLENBQUMsRUFBRTt3QkFDRixJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTs0QkFDMUIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLEtBQUssRUFBRSxFQUFFOzRCQUNuQyxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDO3lCQUN2RDs2QkFBTTs0QkFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7eUJBQ3ZDO29CQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTt3QkFDUCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzt3QkFDMUIsSUFBSSxTQUFTLEVBQUUsRUFBRTs0QkFDYixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzt5QkFDdkQ7NkJBQU07NEJBQ0gsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7eUJBQ3BEO29CQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUMxQixJQUFJLFNBQVMsRUFBRSxFQUFFO29CQUNiLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2lCQUN2RDtxQkFBTTtvQkFDSCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDcEQ7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ04sQ0FBQztDQUVILENBQUE7O1lBbEdzQixNQUFNO1lBQ0QscUJBQXFCO1lBQ3JCLFdBQVc7OztBQUwxQixlQUFlO0lBSjNCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxlQUFlLENBcUczQjtTQXJHWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgaXNEZXZNb2RlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvc2VydmljZXMvYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vbGliL21wbS11dGlscy9jb25maWcvY29uZmlnJztcclxuaW1wb3J0IHsgc2V0QXV0aENvbnRleHRDb29raWUgfSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL2F1dGgvc2V0QXV0aENvbnRleHRDb29raWUnO1xyXG5pbXBvcnQgKiBhcyBjb29raWUgZnJvbSAnanMtY29va2llJztcclxuaW1wb3J0IHsgaGV4X3NoYTEgfSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL2F1dGgvc2hhMSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbGliL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2Vzc2lvbkhhbmRsZXIgfSBmcm9tICcuLi8uLi8uLi9saWIvc2hhcmVkL290bW1TZXNzaW9uSGFuZGxlcic7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwUm91dGVTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHVibGljIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBnb1RvTG9nb3V0KCkge1xyXG4gICAgICAgIHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XHJcbiAgICAgICAgT1RNTVNlc3Npb25IYW5kbGVyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgdGhpcy5vdG1tU2VydmljZS5kZXN0cm95T3RtbVNlc3Npb24oKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlRGVzdHJveU90bW1TZXNzaW9uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChjb25maWcuY29uZmlnLmVudmlyb25tZW50ID09PSAnbG9jYWwnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5sb2dvdXQoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY29uZmlnLmNvbmZpZy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGVycm9yRGVzdHJveU90bW1TZXNzaW9uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChjb25maWcuY29uZmlnLmVudmlyb25tZW50ID09PSAnbG9jYWwnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5sb2dvdXQoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY29uZmlnLmNvbmZpZy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlTGFzdFZpc2l0TGluaygpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgaWYgKCFjdXJyZW50VXJsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnTVBNX1YzX0xBU1RfTElOSycsICcnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGN1cnJlbnRVcmwgIT09IGNvbmZpZy5nZXRPVERTTG9naW5QYWdlVXJsKCkgJiYgY3VycmVudFVybCAhPT0gY29uZmlnLmdldFBzT3JnRW5kcG9pbnQoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBzcGxpdHRlZFVSTCA9IGN1cnJlbnRVcmwuc3BsaXQoJy8jLycpO1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzcGxpdHRlZFVSTCkgJiYgc3BsaXR0ZWRVUkwubGVuZ3RoID09PSAyICYmIHNwbGl0dGVkVVJMWzFdICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdNUE1fVjNfTEFTVF9MSU5LJywgY3VycmVudFVybCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TGFzdFZpc2l0ZWRMaW5paygpIHtcclxuICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ01QTV9WM19MQVNUX0xJTksnKTtcclxuICAgIH1cclxuXHJcbiAgICBnb1RvTG9naW4oKSB7XHJcbiAgICAgICAgaWYgKGNvbmZpZy5jb25maWcuZW52aXJvbm1lbnQgPT09ICdsb2NhbCcpIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2V0QXV0aENvbnRleHRDb29raWUoKS5zdWJzY3JpYmUoZG9uZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLmdldFByZUxvZ2luRGV0YWlscygpLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnNldFNlc3Npb25JbmZvKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNhbWxDb29raWVOYW1lID0gZGF0YVsnQXV0aGVudGljYXRvciddWydTYW1sQXJ0aWZhY3RDb29raWVOYW1lJ107XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY3RDb29raWVOYW1lID0gZGF0YVsnQXV0aGVudGljYXRvciddWydDaGVja05hbWUnXTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhdXRoZW50aWNhdGlvbkNvbnRleHQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNhbWxDb29raWVOYW1lOiBBdXRoZW50aWNhdGlvblNlcnZpY2Uuc2Vzc2lvblV0aWwuZ2V0U2Vzc2lvblByb3Aoc2FtbENvb2tpZU5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGVja0Nvb2tpZU5hbWU6IEF1dGhlbnRpY2F0aW9uU2VydmljZS5zZXNzaW9uVXRpbC5nZXRTZXNzaW9uUHJvcChjdENvb2tpZU5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb29raWVzUGF0aDogQXV0aGVudGljYXRpb25TZXJ2aWNlLnNlc3Npb25VdGlsLmdldFNlc3Npb25Qcm9wKCcvJykgfHwgJy8nLFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgY29va2llLnJlbW92ZShjdENvb2tpZU5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFydGlmYWN0ID0gY29uZmlnLmdldFNBTUxhcnQoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoYXJ0aWZhY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY3QgPSBoZXhfc2hhMShhcnRpZmFjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvb2tpZS5zZXQoY3RDb29raWVOYW1lLCBjdCwgeyBwYXRoOiBhdXRoZW50aWNhdGlvbkNvbnRleHQuY29va2llc1BhdGggfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZ2V0VXNlckluZm8oKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5nZXRMYXN0VmlzaXRlZExpbmlrKCkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldExhc3RWaXNpdGVkTGluaWsoKSAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZSh0aGlzLmdldExhc3RWaXNpdGVkTGluaWsoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2FwcHMvbXBtJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JlTGFzdFZpc2l0TGluaygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzRGV2TW9kZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBjb25maWcuZ2V0T1REU0xvZ2luUGFnZVVybCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGNvbmZpZy5nZXRQc09yZ0VuZHBvaW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RvcmVMYXN0VmlzaXRMaW5rKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXNEZXZNb2RlKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGNvbmZpZy5nZXRPVERTTG9naW5QYWdlVXJsKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY29uZmlnLmdldFBzT3JnRW5kcG9pbnQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICB9XHJcblxyXG59XHJcbiJdfQ==