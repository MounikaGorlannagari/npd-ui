import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../project/shared/services/project.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { ProjectBulkCountService } from '../../shared/services/project-bulk-count.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../notification/notification.service';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
let ProjectCardComponent = class ProjectCardComponent {
    constructor(dialog, utilService, otmmMetadataService, fieldConfigService, viewConfigService, sharingService, projectBulkCountService, router, projectService, notificationService) {
        this.dialog = dialog;
        this.utilService = utilService;
        this.otmmMetadataService = otmmMetadataService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.sharingService = sharingService;
        this.projectBulkCountService = projectBulkCountService;
        this.router = router;
        this.projectService = projectService;
        this.notificationService = notificationService;
        this.viewProjectDetails = new EventEmitter();
        this.copyProject = new EventEmitter();
        this.openCommentsModule = new EventEmitter();
        this.selectedProjectCardCount = new EventEmitter();
        this.refreshHandler = new EventEmitter();
        this.customMetadataValues = [];
        this.priorityObj = {
            color: '',
            icon: '',
            tooltip: ''
        };
        this.mpmFieldConstants = MPMFieldConstants;
        this.displayFieldList = [];
        this.isCampaignDashboard = false;
        this.categoryLevel = MPM_LEVELS.PROJECT;
        this.affectedTask = [];
        this.copyToolTip = 'Click to copy project name';
        this.status = ['Defined', 'In Progress'];
        this.enableBulkEdit = false;
    }
    copiedText(element) {
        this.copyToolTip = 'Copied!';
        setTimeout(() => {
            if (this.project.PROJECT_TYPE === 'PROJECT') {
                this.copyToolTip = 'Click to copy project name';
            }
            else {
                this.copyToolTip = 'Click to copy template name';
            }
        }, 2000);
    }
    openProjectDetails(project) {
        if (this.isCampaignDashboard) {
            let campaignId = this.getProperty(project, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID);
            if (campaignId) {
                campaignId = campaignId.split('.')[1];
            }
            this.viewProjectDetails.emit(campaignId);
        }
        else {
            let projectId = this.getProperty(project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
            if (projectId) {
                projectId = projectId.split('.')[1];
            }
            this.viewProjectDetails.emit(projectId);
        }
    }
    openComments() {
        let projectId = this.getProperty(this.project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
        if (projectId) {
            projectId = projectId.split('.')[1];
        }
        this.openCommentsModule.emit(projectId);
    }
    openCopyProject(project, isCopyTemplate) {
        if (!this.isTemplate) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        project.isCopyTemplate = isCopyTemplate ? true : false;
        this.copyProject.next(project);
    }
    converToLocalDate(data, propertyId) {
        return this.getProperty(data, propertyId);
    }
    getProperty(projectData, propertyId) {
        const currMPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, propertyId, this.categoryLevel);
        this.status.forEach(statusFilter => {
            if (this.fieldConfigService.getFieldValueByDisplayColumn(currMPMField, projectData) === statusFilter) {
                this.enableBulkEdit = true;
            }
        });
        return this.fieldConfigService.getFieldValueByDisplayColumn(currMPMField, projectData);
    }
    getPriority(projectData, propertyId) {
        const priorityObj = this.utilService.getPriorityIcon(this.getProperty(projectData, propertyId), MPM_LEVELS.PROJECT);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    }
    getMetadataTypeValue(metadata) {
        if (metadata.value && metadata.value.value) {
            return metadata.value.value.type ? metadata.value.value.type : null;
        }
        return null;
    }
    isSelected(projectDetails, event) {
        //console.log(this.projectBulkCountService.getProjectBulkCount(projectDetails));
        this.selectedProjectCardCount.next(this.projectBulkCountService.getProjectBulkCount(projectDetails));
    }
    deleteProject(project) {
        console.log("Project card deleteing Project");
        console.log(project);
        this.affectedTask = [];
        let tasks;
        let msg;
        let isCustomWorkflow = this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW);
        const name = this.isTemplate ? 'Template' : 'Project';
        if (isCustomWorkflow) {
            msg = "Are you sure you want to delete the " + name + " and reconfigure the workflow rule engine?";
        }
        else {
            msg = "Are you sure you want to delete the " + name + " and Exit?";
        }
        this.projectService.getTaskByProjectID(project.ID).subscribe(projectTaskResponse => {
            console.log(projectTaskResponse.GetTaskByProjectIDResponse.Task);
            if (projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task && !Array.isArray(projectTaskResponse.GetTaskByProjectIDResponse.Task)) {
                tasks = [projectTaskResponse.GetTaskByProjectIDResponse.Task];
            }
            else {
                tasks = projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task ? projectTaskResponse.GetTaskByProjectIDResponse.Task : '';
            }
            if (tasks && tasks.length > 0) {
                tasks.forEach(task => {
                    this.affectedTask.push(task.NAME);
                });
            }
            // });
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: !this.isTemplate ? this.affectedTask : [],
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(response => {
                if (response) {
                    this.projectService.deleteProject(project.ID, project.ITEM_ID, project.PROJECT_STATUS_ID).subscribe(deleteProjectResponse => {
                        this.notificationService.success(name + ' has been deleted successfully');
                        //   this.refresh();
                        this.refreshHandler.emit();
                    }, error => {
                        this.notificationService.error('Delete ' + name + ' operation failed, try again later');
                    });
                }
            });
        });
    }
    ngOnInit() {
        let viewConfig;
        const userId = this.sharingService.getCurrentUserItemID();
        this.loggedInUserId = +userId;
        this.finalCompleted = StatusTypes.FINAL_COMPLETED;
        const projectConfig = this.sharingService.getProjectConfig();
        this.showCopyProject = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_COPY_PROJECT]);
        this.isTemplate = this.getProperty(this.project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE) === 'TEMPLATE' ? true : false;
        if (this.router.url.includes('Campaigns') || this.dashboardMenuConfig.IS_CAMPAIGN_TYPE) {
            this.isCampaignDashboard = true;
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
        }
        if (this.dashboardMenuConfig.projectViewName) {
            viewConfig = this.dashboardMenuConfig.projectViewName; //this.sharingService.getViewConfigById(this.dashboardMenuConfig.projectViewId).VIEW;
        }
        else {
            //MPM_V3 -1799
            /*  viewConfig = this.isTemplate ?
                 SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE : SearchConfigConstants.SEARCH_NAME.PROJECT;
          */
            viewConfig = this.isCampaignDashboard ? SearchConfigConstants.SEARCH_NAME.CAMPAIGN : this.isTemplate ?
                SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE : SearchConfigConstants.SEARCH_NAME.PROJECT;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetailsParent) => {
            const viewDetails = viewDetailsParent ? JSON.parse(JSON.stringify(viewDetailsParent)) : null;
            if (viewDetails && viewDetails.R_PM_CARD_VIEW_MPM_FIELDS) {
                viewDetails.R_PM_CARD_VIEW_MPM_FIELDS.forEach((mpmField) => {
                    if (ProjectConstant.DEFAULT_CARD_FIELDS.indexOf(mpmField.MAPPER_NAME) < 0) {
                        mpmField.VALUE = this.fieldConfigService.getFieldValueByDisplayColumn(mpmField, this.project);
                        this.displayFieldList.push(mpmField);
                    }
                });
            }
        });
    }
};
ProjectCardComponent.ctorParameters = () => [
    { type: MatDialog },
    { type: UtilService },
    { type: OtmmMetadataService },
    { type: FieldConfigService },
    { type: ViewConfigService },
    { type: SharingService },
    { type: ProjectBulkCountService },
    { type: Router },
    { type: ProjectService },
    { type: NotificationService }
];
__decorate([
    Input()
], ProjectCardComponent.prototype, "project", void 0);
__decorate([
    Input()
], ProjectCardComponent.prototype, "dashboardMenuConfig", void 0);
__decorate([
    Output()
], ProjectCardComponent.prototype, "viewProjectDetails", void 0);
__decorate([
    Output()
], ProjectCardComponent.prototype, "copyProject", void 0);
__decorate([
    Output()
], ProjectCardComponent.prototype, "openCommentsModule", void 0);
__decorate([
    Output()
], ProjectCardComponent.prototype, "selectedProjectCardCount", void 0);
__decorate([
    Output()
], ProjectCardComponent.prototype, "refreshHandler", void 0);
ProjectCardComponent = __decorate([
    Component({
        selector: 'mpm-project-card',
        template: "<mat-card class=\"project-card\">\r\n    <div class=\"project-header\">\r\n        <div class=\"project-card-image\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-title-wrapper\">\r\n                    <!-- <mat-checkbox [disabled]=\"!enableBulkEdit\" color=\"primary\" class=\"select-project\" (click)=\"$event.stopPropagation()\"\r\n                    (change)=\"isSelected(project,$event)\"\r\n                    >\r\n                    </mat-checkbox> -->\r\n                    <mpm-name-icon\r\n                        [userName]=\"getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'\"\r\n                        [charCount]=\"2\" [color]=\"'#c0ca33'\"></mpm-name-icon>\r\n                    <span class=\"card-title\" (click)=\"openProjectDetails(project)\"\r\n                        matTooltip=\"{{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'}}\">\r\n                        {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'}}\r\n                    </span>\r\n                </div>\r\n                <div class=\"flex-row-item action-button\">\r\n                    <mat-icon *ngIf=\"showCopyProject || !isTemplate\" color=\"primary\" matTooltip=\"Actions\" matSuffix\r\n                        [matMenuTriggerFor]=\"projectMenu\">\r\n                        more_vert\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- <div>\r\n            <mat-icon matTooltip=\"Priority: {{priorityObj.tooltip}}\" [style.color]=\"priorityObj.color\">\r\n                {{priorityObj.icon}}</mat-icon>\r\n\r\n            <mat-icon color=\"primary\" matTooltip=\"Actions\" matSuffix [matMenuTriggerFor]=\"projectMenu\">\r\n                more_vert\r\n            </mat-icon>\r\n        </div> -->\r\n    </div>\r\n    <div class=\"flex-col-item\">\r\n        <!-- <span> -->\r\n        <div class=\"flex-col-item\" *ngIf=\"!isTemplate\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-item\">\r\n                    <div class=\"project-percentage\"\r\n                        matTooltip=\"Project Completion - {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || 0}}%\">\r\n                        <span class=\"item-title\">\r\n                            {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || 0}}%</span>\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                            value=\"{{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS)}}\">\r\n                        </mat-progress-bar>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-item\" *ngIf=\"!isTemplate\"\r\n                    matTooltip=\"Completion Date: {{converToLocalDate(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DUE_DATE) || 'NA'}}\">\r\n                    <mat-icon>event</mat-icon>\r\n                    <span class=\"item-value\">{{getProperty(project,\r\n                        mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DUE_DATE) || 'NA'}}</span>\r\n                </div>\r\n                <div class=\"flex-row-item card-item\">\r\n                    <mat-chip\r\n                        matTooltip=\"Status: {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || 'NA'}}\"\r\n                        class=\"status-chip\">\r\n                        {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || 'NA'}}\r\n                    </mat-chip>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                        matTooltip=\"Priority: {{getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).tooltip}}\"\r\n                        [style.color]=\"getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).color\">\r\n                        {{getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <span *ngIf=\"displayFieldList && displayFieldList.length > 0\">\r\n            <div class=\"flex-col-item\" *ngFor=\"let metadata of displayFieldList\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item card-item\"\r\n                        matTooltip=\"{{metadata.DISPLAY_NAME}} : {{metadata.VALUE || 'NA'}}\">\r\n                        <span class=\"item-title\">{{metadata.DISPLAY_NAME}}:</span>\r\n                        <span class=\"item-value\"\r\n                            [ngClass]=\"{'text-warp': metadata.DATA_TYPE === 'string'}\">{{metadata.VALUE || 'NA'}}</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </span>\r\n        <!-- </span> -->\r\n    </div>\r\n</mat-card>\r\n\r\n<mat-menu #projectMenu=\"matMenu\">\r\n    <button *ngIf=\"showCopyProject\" mat-menu-item\r\n        matTooltip=\"{{!isTemplate ? 'Copy Project' : 'Project from template'}}\" (click)=\"openCopyProject(project,false)\">\r\n        <mat-icon>insert_drive_file</mat-icon>\r\n        <span>{{!isTemplate ? 'Copy Project' : 'Project from template'}}</span>\r\n    </button>\r\n    <!--showCopyProject && !isCampaignDashboard && -->\r\n    <button *ngIf=\"isTemplate\" mat-menu-item\r\n        matTooltip=\"Copy Template\" (click)=\"openCopyProject(project,true)\">\r\n        <mat-icon>insert_drive_file</mat-icon>\r\n        <span>Copy Template</span>\r\n    </button>\r\n    <button mat-menu-item matTooltip=\"Comment\" (click)=\"openComments()\" *ngIf=\"!isTemplate\">\r\n        <mat-icon>\r\n            comment\r\n        </mat-icon>\r\n        <span>Comments</span>\r\n    </button>\r\n    <!--*ngIf=\"!isTemplate\"-->\r\n    <button mat-menu-item matTooltip=\"{{isTemplate ? 'Delete Template' : 'Delete Project'}}\" (click)=\"deleteProject(project)\" >\r\n        <mat-icon>delete</mat-icon>\r\n        <span>{{isTemplate ? 'Delete Template' : 'Delete Project'}}</span>\r\n    </button>\r\n</mat-menu>",
        styles: [".project-header{display:flex;flex-direction:row}.project-card-image{flex-grow:1;margin-bottom:5px;margin-left:5px}.project-card-image .action-button{justify-content:flex-end;align-items:center}.project-percentage{margin-bottom:10px;text-align:right;width:100%}mat-card{min-width:270px;min-height:200px;cursor:pointer}.card-title-wrapper{align-items:center}.card-title{display:inline-block;text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap;margin:5px;font-size:15px;min-width:20px;max-width:190px}.flex-row-item p{font-size:13px;margin:2px}.project-card{padding:12px}.project-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.card-heading{font-weight:590}span.item-title{margin-right:4px;font-size:14px;font-weight:600;line-height:16px}.card-item{margin:4px;font-size:14px;align-items:center;max-width:260px}.status-chip{padding:4px 8px;border-radius:4px;font-weight:400}.text-warp{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;max-width:56%}.select-project{padding:10px}"]
    })
], ProjectCardComponent);
export { ProjectCardComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1jYXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LWNhcmQvcHJvamVjdC1jYXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFFL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUUzRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUUvRSxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ25GLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3JILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVFyRCxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQTZCN0IsWUFDVyxNQUFpQixFQUNqQixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGlCQUFvQyxFQUNwQyxjQUE4QixFQUM5Qix1QkFBZ0QsRUFDaEQsTUFBYyxFQUNkLGNBQThCLEVBQzlCLG1CQUF3QztRQVR4QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQW5DekMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3QyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3Qyw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUtuRCx5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBRztZQUNWLEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixPQUFPLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRixzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0QyxxQkFBZ0IsR0FBb0IsRUFBRSxDQUFDO1FBQ3ZDLHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUU1QixrQkFBYSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFJbkMsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFpQmxCLGdCQUFXLEdBQUcsNEJBQTRCLENBQUM7UUFDM0MsV0FBTSxHQUFHLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3BDLG1CQUFjLEdBQUcsS0FBSyxDQUFDO0lBTG5CLENBQUM7SUFNTCxVQUFVLENBQUMsT0FBTztRQUNkLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDWixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxLQUFLLFNBQVMsRUFBRTtnQkFDekMsSUFBSSxDQUFDLFdBQVcsR0FBRyw0QkFBNEIsQ0FBQzthQUNuRDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsV0FBVyxHQUFHLDZCQUE2QixDQUFDO2FBQ3BEO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELGtCQUFrQixDQUFDLE9BQU87UUFDdEIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDMUIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDeEcsSUFBSSxVQUFVLEVBQUU7Z0JBQ1osVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekM7WUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzVDO2FBQU07WUFDSCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDckcsSUFBSSxTQUFTLEVBQUU7Z0JBQ1gsU0FBUyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzNDO0lBQ0wsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzFHLElBQUksU0FBUyxFQUFFO1lBQ1gsU0FBUyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBRTVDLENBQUM7SUFFRCxlQUFlLENBQUMsT0FBTyxFQUFDLGNBQXNCO1FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO2FBQU07WUFDSCxPQUFPLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELE9BQU8sQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFVBQVU7UUFDOUIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFVO1FBQy9CLE1BQU0sWUFBWSxHQUFhLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFcEksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxLQUFLLFlBQVksRUFBRTtnQkFDbEcsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7YUFDOUI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsQ0FBQztJQUMzRixDQUFDO0lBRUQsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFVO1FBQy9CLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUMxRixVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFeEIsT0FBTztZQUNILEtBQUssRUFBRSxXQUFXLENBQUMsS0FBSztZQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUk7WUFDdEIsT0FBTyxFQUFFLFdBQVcsQ0FBQyxPQUFPO1NBQy9CLENBQUM7SUFDTixDQUFDO0lBQ0Qsb0JBQW9CLENBQUMsUUFBUTtRQUN6QixJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDeEMsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ3ZFO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELFVBQVUsQ0FBQyxjQUFjLEVBQUUsS0FBSztRQUM1QixnRkFBZ0Y7UUFDaEYsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUN6RyxDQUFDO0lBRUQsYUFBYSxDQUFDLE9BQU87UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxLQUFLLENBQUM7UUFDVixJQUFJLEdBQUcsQ0FBQztRQUNSLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDcEYsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxnQkFBZ0IsRUFBRTtZQUNsQixHQUFHLEdBQUcsc0NBQXNDLEdBQUMsSUFBSSxHQUFDLDRDQUE0QyxDQUFDO1NBQ2xHO2FBQU07WUFDSCxHQUFHLEdBQUcsc0NBQXNDLEdBQUMsSUFBSSxHQUFDLFlBQVksQ0FBQztTQUNsRTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO1lBQy9FLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakUsSUFBSSxtQkFBbUIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNyTSxLQUFLLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqRTtpQkFBTTtnQkFDSCxLQUFLLEdBQUcsbUJBQW1CLElBQUksbUJBQW1CLENBQUMsMEJBQTBCLElBQUksbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUNuTTtZQUNELElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMzQixLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RDLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFDRCxNQUFNO1lBQ04sTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzNELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxFQUFFLEdBQUc7b0JBQ1osSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDL0MsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNyQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksUUFBUSxFQUFFO29CQUNWLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRTt3QkFDeEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUMsZ0NBQWdDLENBQUMsQ0FBQzt3QkFDeEUsb0JBQW9CO3dCQUNwQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMvQixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7d0JBQ1AsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUMsSUFBSSxHQUFDLG9DQUFvQyxDQUFDLENBQUM7b0JBQ3hGLENBQUMsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxRQUFRO1FBQ0osSUFBSSxVQUFVLENBQUM7UUFDZixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztRQUMxSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2SSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUU7WUFDcEYsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDNUM7UUFDRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLEVBQUU7WUFDMUMsVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsQ0FBQSxxRkFBcUY7U0FFOUk7YUFBTTtZQUNILGNBQWM7WUFDZDs7WUFFQTtZQUNBLFVBQVUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbEcscUJBQXFCLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1NBRXRHO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGlCQUE2QixFQUFFLEVBQUU7WUFDM0csTUFBTSxXQUFXLEdBQWUsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN6RyxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMseUJBQXlCLEVBQUU7Z0JBQ3RELFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFrQixFQUFFLEVBQUU7b0JBQ2pFLElBQUksZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUN2RSxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUM5RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUN4QztnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUEzTHNCLFNBQVM7WUFDSixXQUFXO1lBQ0gsbUJBQW1CO1lBQ3BCLGtCQUFrQjtZQUNuQixpQkFBaUI7WUFDcEIsY0FBYztZQUNMLHVCQUF1QjtZQUN4QyxNQUFNO1lBQ0UsY0FBYztZQUNULG1CQUFtQjs7QUFyQzFDO0lBQVIsS0FBSyxFQUFFO3FEQUFnQjtBQUNmO0lBQVIsS0FBSyxFQUFFO2lFQUFxQjtBQUNuQjtJQUFULE1BQU0sRUFBRTtnRUFBOEM7QUFDN0M7SUFBVCxNQUFNLEVBQUU7eURBQXVDO0FBQ3RDO0lBQVQsTUFBTSxFQUFFO2dFQUE4QztBQUM3QztJQUFULE1BQU0sRUFBRTtzRUFBb0Q7QUFDbkQ7SUFBVCxNQUFNLEVBQUU7NERBQTBDO0FBUjFDLG9CQUFvQjtJQU5oQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLGd6TUFBNEM7O0tBRS9DLENBQUM7R0FFVyxvQkFBb0IsQ0F5TmhDO1NBek5ZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZVBpcGUgfSBmcm9tICcuLi8uLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcblxyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9UTU1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgTVBNRmllbGQsIE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0QnVsa0NvdW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LWJ1bGstY291bnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXByb2plY3QtY2FyZCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcHJvamVjdC1jYXJkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Byb2plY3QtY2FyZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvamVjdENhcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHB1YmxpYyBwcm9qZWN0O1xyXG4gICAgQElucHV0KCkgZGFzaGJvYXJkTWVudUNvbmZpZztcclxuICAgIEBPdXRwdXQoKSB2aWV3UHJvamVjdERldGFpbHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjb3B5UHJvamVjdCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG9wZW5Db21tZW50c01vZHVsZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdGVkUHJvamVjdENhcmRDb3VudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHJlZnJlc2hIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgc2hvd0NvcHlQcm9qZWN0OiBib29sZWFuO1xyXG4gICAgY29sb3I6ICcjYzBjYTMzJztcclxuICAgIGlzVGVtcGxhdGU6IGJvb2xlYW47XHJcbiAgICBjdXN0b21NZXRhZGF0YVZhbHVlcyA9IFtdO1xyXG4gICAgcHJpb3JpdHlPYmogPSB7XHJcbiAgICAgICAgY29sb3I6ICcnLFxyXG4gICAgICAgIGljb246ICcnLFxyXG4gICAgICAgIHRvb2x0aXA6ICcnXHJcbiAgICB9O1xyXG4gICAgbXBtRmllbGRDb25zdGFudHMgPSBNUE1GaWVsZENvbnN0YW50cztcclxuICAgIGRpc3BsYXlGaWVsZExpc3Q6IEFycmF5PE1QTUZpZWxkPiA9IFtdO1xyXG4gICAgaXNDYW1wYWlnbkRhc2hib2FyZCA9IGZhbHNlO1xyXG5cclxuICAgIGNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLlBST0pFQ1Q7XHJcblxyXG4gICAgbG9nZ2VkSW5Vc2VySWQ7XHJcbiAgICBmaW5hbENvbXBsZXRlZDtcclxuICAgIGFmZmVjdGVkVGFzayA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHByb2plY3RCdWxrQ291bnRTZXJ2aWNlOiBQcm9qZWN0QnVsa0NvdW50U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG5cclxuICAgICkgeyB9XHJcblxyXG4gICAgaXNQcm9qZWN0T3duZXI7XHJcbiAgICBjb3B5VG9vbFRpcCA9ICdDbGljayB0byBjb3B5IHByb2plY3QgbmFtZSc7XHJcbiAgICBzdGF0dXMgPSBbJ0RlZmluZWQnLCAnSW4gUHJvZ3Jlc3MnXTtcclxuICAgIGVuYWJsZUJ1bGtFZGl0ID0gZmFsc2U7XHJcbiAgICBjb3BpZWRUZXh0KGVsZW1lbnQpIHtcclxuICAgICAgICB0aGlzLmNvcHlUb29sVGlwID0gJ0NvcGllZCEnO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0LlBST0pFQ1RfVFlQRSA9PT0gJ1BST0pFQ1QnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvcHlUb29sVGlwID0gJ0NsaWNrIHRvIGNvcHkgcHJvamVjdCBuYW1lJztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29weVRvb2xUaXAgPSAnQ2xpY2sgdG8gY29weSB0ZW1wbGF0ZSBuYW1lJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDIwMDApO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5Qcm9qZWN0RGV0YWlscyhwcm9qZWN0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCkge1xyXG4gICAgICAgICAgICBsZXQgY2FtcGFpZ25JZCA9IHRoaXMuZ2V0UHJvcGVydHkocHJvamVjdCwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fQ0FNUEFJR05fRkVJTERTLkNBTVBBSUdOX0lURU1fSUQpO1xyXG4gICAgICAgICAgICBpZiAoY2FtcGFpZ25JZCkge1xyXG4gICAgICAgICAgICAgICAgY2FtcGFpZ25JZCA9IGNhbXBhaWduSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnZpZXdQcm9qZWN0RGV0YWlscy5lbWl0KGNhbXBhaWduSWQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCBwcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KHByb2plY3QsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCk7XHJcbiAgICAgICAgICAgIGlmIChwcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgICAgIHByb2plY3RJZCA9IHByb2plY3RJZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQocHJvamVjdElkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNvbW1lbnRzKCkge1xyXG4gICAgICAgIGxldCBwcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMucHJvamVjdCwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lEKTtcclxuICAgICAgICBpZiAocHJvamVjdElkKSB7XHJcbiAgICAgICAgICAgIHByb2plY3RJZCA9IHByb2plY3RJZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wZW5Db21tZW50c01vZHVsZS5lbWl0KHByb2plY3RJZCk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5Db3B5UHJvamVjdChwcm9qZWN0LGlzQ29weVRlbXBsYXRlOmJvb2xlYW4pIHtcclxuICAgICAgICBpZiAoIXRoaXMuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICBwcm9qZWN0LmlzUHJvamVjdFR5cGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHByb2plY3QuaXNQcm9qZWN0VHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwcm9qZWN0LmlzQ29weVRlbXBsYXRlID0gaXNDb3B5VGVtcGxhdGUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5jb3B5UHJvamVjdC5uZXh0KHByb2plY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlclRvTG9jYWxEYXRlKGRhdGEsIHByb3BlcnR5SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRQcm9wZXJ0eShkYXRhLCBwcm9wZXJ0eUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9wZXJ0eShwcm9qZWN0RGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIGNvbnN0IGN1cnJNUE1GaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBwcm9wZXJ0eUlkLCB0aGlzLmNhdGVnb3J5TGV2ZWwpO1xyXG5cclxuICAgICAgICB0aGlzLnN0YXR1cy5mb3JFYWNoKHN0YXR1c0ZpbHRlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGN1cnJNUE1GaWVsZCwgcHJvamVjdERhdGEpID09PSBzdGF0dXNGaWx0ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW5hYmxlQnVsa0VkaXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oY3Vyck1QTUZpZWxkLCBwcm9qZWN0RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJpb3JpdHkocHJvamVjdERhdGEsIHByb3BlcnR5SWQpIHtcclxuICAgICAgICBjb25zdCBwcmlvcml0eU9iaiA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0UHJpb3JpdHlJY29uKHRoaXMuZ2V0UHJvcGVydHkocHJvamVjdERhdGEsIHByb3BlcnR5SWQpLFxyXG4gICAgICAgICAgICBNUE1fTEVWRUxTLlBST0pFQ1QpO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBjb2xvcjogcHJpb3JpdHlPYmouY29sb3IsXHJcbiAgICAgICAgICAgIGljb246IHByaW9yaXR5T2JqLmljb24sXHJcbiAgICAgICAgICAgIHRvb2x0aXA6IHByaW9yaXR5T2JqLnRvb2x0aXBcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgZ2V0TWV0YWRhdGFUeXBlVmFsdWUobWV0YWRhdGEpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChtZXRhZGF0YS52YWx1ZSAmJiBtZXRhZGF0YS52YWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbWV0YWRhdGEudmFsdWUudmFsdWUudHlwZSA/IG1ldGFkYXRhLnZhbHVlLnZhbHVlLnR5cGUgOiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBpc1NlbGVjdGVkKHByb2plY3REZXRhaWxzLCBldmVudCkge1xyXG4gICAgICAgIC8vY29uc29sZS5sb2codGhpcy5wcm9qZWN0QnVsa0NvdW50U2VydmljZS5nZXRQcm9qZWN0QnVsa0NvdW50KHByb2plY3REZXRhaWxzKSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RDYXJkQ291bnQubmV4dCh0aGlzLnByb2plY3RCdWxrQ291bnRTZXJ2aWNlLmdldFByb2plY3RCdWxrQ291bnQocHJvamVjdERldGFpbHMpKTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVQcm9qZWN0KHByb2plY3QpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlByb2plY3QgY2FyZCBkZWxldGVpbmcgUHJvamVjdFwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhwcm9qZWN0KTtcclxuICAgICAgICB0aGlzLmFmZmVjdGVkVGFzayA9IFtdO1xyXG4gICAgICAgIGxldCB0YXNrcztcclxuICAgICAgICBsZXQgbXNnO1xyXG4gICAgICAgIGxldCBpc0N1c3RvbVdvcmtmbG93ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpO1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSB0aGlzLmlzVGVtcGxhdGU/ICdUZW1wbGF0ZScgOiAnUHJvamVjdCc7XHJcbiAgICAgICAgaWYgKGlzQ3VzdG9tV29ya2Zsb3cpIHtcclxuICAgICAgICAgICAgbXNnID0gXCJBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoZSBcIituYW1lK1wiIGFuZCByZWNvbmZpZ3VyZSB0aGUgd29ya2Zsb3cgcnVsZSBlbmdpbmU/XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbXNnID0gXCJBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoZSBcIituYW1lK1wiIGFuZCBFeGl0P1wiO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFRhc2tCeVByb2plY3RJRChwcm9qZWN0LklEKS5zdWJzY3JpYmUocHJvamVjdFRhc2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzayk7XHJcbiAgICAgICAgICAgIGlmIChwcm9qZWN0VGFza1Jlc3BvbnNlICYmIHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrICYmICFBcnJheS5pc0FycmF5KHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzaykpIHtcclxuICAgICAgICAgICAgICAgIHRhc2tzID0gW3Byb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFza107XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrcyA9IHByb2plY3RUYXNrUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZSAmJiBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2sgPyBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2sgOiAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGFza3MgJiYgdGFza3MubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGFza3MuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFmZmVjdGVkVGFzay5wdXNoKHRhc2suTkFNRSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyB9KTtcclxuICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IG1zZyxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAhdGhpcy5pc1RlbXBsYXRlID8gdGhpcy5hZmZlY3RlZFRhc2sgOiBbXSxcclxuICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZGVsZXRlUHJvamVjdChwcm9qZWN0LklELCBwcm9qZWN0LklURU1fSUQsIHByb2plY3QuUFJPSkVDVF9TVEFUVVNfSUQpLnN1YnNjcmliZShkZWxldGVQcm9qZWN0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcyhuYW1lKycgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoSGFuZGxlci5lbWl0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0RlbGV0ZSAnK25hbWUrJyBvcGVyYXRpb24gZmFpbGVkLCB0cnkgYWdhaW4gbGF0ZXInKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICAgICAgY29uc3QgdXNlcklkID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VySWQgPSArdXNlcklkO1xyXG4gICAgICAgIHRoaXMuZmluYWxDb21wbGV0ZWQgPSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQ7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UHJvamVjdENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuc2hvd0NvcHlQcm9qZWN0ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX0NPUFlfUFJPSkVDVF0pO1xyXG4gICAgICAgIHRoaXMuaXNUZW1wbGF0ZSA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5wcm9qZWN0LCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX1RZUEUpID09PSAnVEVNUExBVEUnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnJvdXRlci51cmwuaW5jbHVkZXMoJ0NhbXBhaWducycpIHx8IHRoaXMuZGFzaGJvYXJkTWVudUNvbmZpZy5JU19DQU1QQUlHTl9UWVBFKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnlMZXZlbCA9IE1QTV9MRVZFTFMuQ0FNUEFJR047XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRhc2hib2FyZE1lbnVDb25maWcucHJvamVjdFZpZXdOYW1lKSB7XHJcbiAgICAgICAgICAgIHZpZXdDb25maWcgPSB0aGlzLmRhc2hib2FyZE1lbnVDb25maWcucHJvamVjdFZpZXdOYW1lOy8vdGhpcy5zaGFyaW5nU2VydmljZS5nZXRWaWV3Q29uZmlnQnlJZCh0aGlzLmRhc2hib2FyZE1lbnVDb25maWcucHJvamVjdFZpZXdJZCkuVklFVztcclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy9NUE1fVjMgLTE3OTlcclxuICAgICAgICAgICAgLyogIHZpZXdDb25maWcgPSB0aGlzLmlzVGVtcGxhdGUgP1xyXG4gICAgICAgICAgICAgICAgIFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5QUk9KRUNUX1RFTVBMQVRFIDogU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlBST0pFQ1Q7XHJcbiAgICAgICAgICAqL1xyXG4gICAgICAgICAgICB2aWV3Q29uZmlnID0gdGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkID8gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLkNBTVBBSUdOIDogdGhpcy5pc1RlbXBsYXRlID9cclxuICAgICAgICAgICAgICAgIFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5QUk9KRUNUX1RFTVBMQVRFIDogU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlBST0pFQ1Q7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld0NvbmZpZykuc3Vic2NyaWJlKCh2aWV3RGV0YWlsc1BhcmVudDogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB2aWV3RGV0YWlsczogVmlld0NvbmZpZyA9IHZpZXdEZXRhaWxzUGFyZW50ID8gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh2aWV3RGV0YWlsc1BhcmVudCkpIDogbnVsbDtcclxuICAgICAgICAgICAgaWYgKHZpZXdEZXRhaWxzICYmIHZpZXdEZXRhaWxzLlJfUE1fQ0FSRF9WSUVXX01QTV9GSUVMRFMpIHtcclxuICAgICAgICAgICAgICAgIHZpZXdEZXRhaWxzLlJfUE1fQ0FSRF9WSUVXX01QTV9GSUVMRFMuZm9yRWFjaCgobXBtRmllbGQ6IE1QTUZpZWxkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKFByb2plY3RDb25zdGFudC5ERUZBVUxUX0NBUkRfRklFTERTLmluZGV4T2YobXBtRmllbGQuTUFQUEVSX05BTUUpIDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtcG1GaWVsZC5WQUxVRSA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4obXBtRmllbGQsIHRoaXMucHJvamVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheUZpZWxkTGlzdC5wdXNoKG1wbUZpZWxkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=