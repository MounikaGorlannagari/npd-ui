import { __decorate } from "tslib";
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges, Renderer2 } from '@angular/core';
import { ProjectService } from '../../project/shared/services/project.service';
//import { RouteService } from '../../login/services/app.route.service
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataTypeConstants } from '../../project/shared/constants/data-type.constants';
import { FormatToLocalePipe } from '../../shared/pipe/format-to-locale.pipe';
import { SelectionModel } from '@angular/cdk/collections';
import { NotificationService } from '../../notification/notification.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { TaskService } from '../../project/tasks/task.service';
import { LoaderService } from '../../loader/loader.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ProjectBulkCountService } from '../../shared/services/project-bulk-count.service';
import { Router } from '@angular/router';
import { ProjectTypes } from '../../mpm-utils/objects/ProjectTypes';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { GetPropertyPipe } from '../../shared/pipe/get-property.pipe';
let ProjectListComponent = class ProjectListComponent {
    constructor(dialog, projectService, taskService, utilService, notificationService, loaderService, formatToLocalePipe, otmmMetadataService, entityAppDefService, fieldConfigService, sharingService, projectBulkCountService, router, renderer, getPropertyPipe) {
        this.dialog = dialog;
        this.projectService = projectService;
        this.taskService = taskService;
        this.utilService = utilService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.otmmMetadataService = otmmMetadataService;
        this.entityAppDefService = entityAppDefService;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.projectBulkCountService = projectBulkCountService;
        this.router = router;
        this.renderer = renderer;
        this.getPropertyPipe = getPropertyPipe;
        this.enableGhostResize = false;
        this.viewProjectDetails = new EventEmitter();
        this.viewTaskDetails = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.copyProject = new EventEmitter();
        this.openMilestoneDialog = new EventEmitter();
        this.openCommentsModule = new EventEmitter();
        /*  @Output() selectedProjectCount = new EventEmitter<any>();
         @Output() selectedProjectData = new EventEmitter<any>();
         @Output() selectedProjectDatas = new EventEmitter<any>(); */
        this.selectedProjectsCallBackHandler = new EventEmitter();
        // @Output() selectAllProjectsCount = new EventEmitter<boolean>();
        this.openCampaignInProjectList = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.refreshEmitter = new EventEmitter();
        this.selection = new SelectionModel(true, []);
        this.projectDataDataSource = new MatTableDataSource([]);
        this.metaDataDataTypes = DataTypeConstants;
        this.displayableMetadataFields = [];
        this.displayableCustomMetadataFields = [];
        this.displayColumns = [];
        this.mpmFieldConstants = MPMFieldConstants;
        this.position = {
            'value': ''
        };
        this.status = ['Defined', 'In Progress'];
        this.enableBulkEdit = false;
        this.selectedProjects = new SelectionModel(true, []);
        this.isCampaignDashboard = false;
        this.milestoneTasks = '';
        this.categoryLevel = MPM_LEVELS.PROJECT;
        this.isRefresh = true;
        this.affectedTask = [];
    }
    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.projectDataDataSource.data.length;
        return numSelected === numRows;
    }
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    /*  masterToggle(data, value?: any) {
         
         let projectId;
         if (this.isAllSelected()) {
             this.projectDataDataSource.data.forEach(row => {
 
                 projectId = this.projectBulkCountService.getProjectBulkCount(row);
             });
             this.selection.clear();
         } else {
             this.projectDataDataSource.data.forEach(row => {
                 this.selection.select(row);
                 // projectId.push(row.ID);
                 projectId = this.projectBulkCountService.getProjectBulkCount(row);
             });
             this.selectedProjectData.next(this.projectDataDataSource.data);
         }
         console.log(projectId.length);
         this.selectedProjectCount.next(projectId.length)
         
     } */
    getCompletedMilestonesByProjectId(projectId) {
        this.projectService.getCompletedMilestonesByProjectId(projectId).subscribe(response => {
            this.loaderService.show();
            if (response && response.tuple && response.tuple) {
                if (!Array.isArray(response.tuple)) {
                    response.tuple = [response.tuple];
                }
                let milestoneList;
                milestoneList = response.tuple;
                if (milestoneList && milestoneList.length && milestoneList.length > 0) {
                    milestoneList.map(milestone => {
                        if (milestone && milestone.old && milestone.old.Task) {
                            this.milestoneTasks = this.milestoneTasks.concat(milestone.old.Task.TaskName);
                            this.milestoneTasks = this.milestoneTasks.concat(",");
                        }
                    });
                }
                /*if (this.milestoneTasks.length <= 0) {
                    this.milestoneTasks = '';
                }
                else {
                    this.milestoneTasks = '';
                }*/
                this.milestoneTasks = '';
            }
            else {
                this.milestoneTasks = '';
            }
        }, () => {
        });
    }
    masterToggle() {
        this.selectAllProjectChecked ? this.checkAllProjects() : this.uncheckAllProjects();
    }
    checkAllProjects() {
        this.projectDataDataSource.data.forEach(row => {
            row.selected = true;
            this.selectedProjects.select(row);
        });
        this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    }
    uncheckAllProjects() {
        this.projectDataDataSource.data.forEach(row => {
            row.selected = false;
            this.selectedProjects.deselect(row);
        });
        this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    }
    checkProject(projectData, event) {
        if (event.checked) {
            projectData.selected = true;
            this.selectedProjects.select(projectData);
            this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
        }
        else {
            this.selectedProjects.deselect(projectData);
            this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
        }
    }
    getProperty(displayColumn, projectData) {
        this.status.forEach(statusFilter => {
            if (this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData) === statusFilter) {
                this.enableBulkEdit = true;
            }
        });
        // if(displayColumn.MAPPER_NAME === this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS){
        //     this.getCompletedMilestonesByProjectId(projectData.ID);
        //     return (this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData).concat(this.milestoneTasks));
        // }
        // else{
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData);
        // }
    }
    getPropertyByMapper(projectData, propertyId) {
        const currMPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, propertyId, this.categoryLevel);
        return this.getProperty(currMPMField, projectData);
    }
    converToLocalDate(data, propertyId) {
        return this.formatToLocalePipe.transform(this.projectService.converToLocalDate(data, propertyId));
    }
    getPriority(projectData, displayColum) {
        const priorityObj = this.utilService.getPriorityIcon(this.getProperty(displayColum, projectData), MPM_LEVELS.PROJECT);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    }
    editProject(event, project, isCopyTemplate) {
        event.stopPropagation();
        if (this.projectConfig.IS_PROJECT_TYPE) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        project.isCopyTemplate = isCopyTemplate ? true : false;
        this.copyProject.next(project);
    }
    openMilestone(event, project) {
        event.stopPropagation();
        if (this.projectConfig.IS_PROJECT_TYPE) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        this.openMilestoneDialog.next(project);
    }
    editTask(eventData) {
        this.editTaskHandler.emit(eventData);
    }
    openComment(event, projectDetails) {
        event.stopPropagation();
        const projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ID, this.categoryLevel), projectDetails);
        this.openCommentsModule.emit(projectId);
    }
    getAllActiveTasksForProject(project, isUpdate) {
        if (!project.taskList || isUpdate) {
            project.taskList = [];
            project.taskTotalCount = 0;
            this.projectStatusType = project.PROJECT_STATUS_TYPE;
            this.projectCancelledStatus = this.projectStatusType === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
            this.isOnHoldProject = project.IS_ON_HOLD === 'true' ? true : false;
            this.projectConfig.currentProjectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ID, this.categoryLevel), project);
            this.projectConfig.currentProjectItemId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel), project);
            const parameters = {
                projectId: this.projectConfig.currentProjectItemId,
                skip: 0,
                top: 100
            };
            this.loaderService.show();
            this.taskService.getAllActiveTasks(parameters, this.projectConfig.projectViewName)
                .subscribe((response) => {
                this.loaderService.hide();
                project.taskList = response.data;
                project.taskTotalCount = response.cursor.total_records;
            }, error => {
                this.loaderService.hide();
                this.notificationService.error('Something went wrong while getting active tasks for project');
            });
        }
    }
    refreshTask(project) {
        this.getAllActiveTasksForProject(project, true);
    }
    openTaskDetails(eventData) {
        this.viewTaskDetails.next(eventData);
    }
    expandRow(event, row) {
        event.stopPropagation();
        if (row.isExpanded) {
            row.isExpanded = false;
        }
        else {
            row.isExpanded = true;
            this.getAllActiveTasksForProject(row);
        }
    }
    openProjectDetails(project, columnName, enableCampaign, isCampaignManager, isPmView) {
        if (columnName === MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID && enableCampaign && isCampaignManager && isPmView) { /* isCampaignManager */
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
            const campaignId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID, this.categoryLevel), project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                if (!this.isCampaignDashboard) {
                    this.openCampaignInProjectList.emit(true);
                }
                this.viewProjectDetails.emit(campaignId.split('.').length > 1 ? campaignId.split('.')[1] : campaignId.split('.')[0]);
            } /* else {
                this.notificationService.error('Invalid campaign Id');
            } */
        }
        else if (this.isCampaignDashboard) {
            // this.categoryLevel = MPM_LEVELS.CAMPAIGN;
            const campaignId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID, this.categoryLevel), project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                /* if (!this.isCampaignDashboard) {
                    this.openCampaignInProjectList.emit(true);
                } */
                this.viewProjectDetails.emit(campaignId.split('.')[1]);
            }
            else {
                this.notificationService.error('Invalid campaign Id');
            }
        }
        else {
            this.categoryLevel = MPM_LEVELS.PROJECT;
            const projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel), project);
            if (projectId && projectId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(projectId.split('.')[1]);
            }
            else {
                this.notificationService.error('Invalid project Id');
            }
        }
        /* if (this.isCampaignDashboard ) {
            const campaignId =  this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID, this.categoryLevel)
                , project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(campaignId.split('.')[1]);
            } else {
                this.notificationService.error('Invalid campaign Id');
            }
        } else {
            this.categoryLevel = MPM_LEVELS.PROJECT;
            const projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel)
                , project);
            if (projectId && projectId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(projectId.split('.')[1]);
            } else {
                this.notificationService.error('Invalid project Id');
            }
        } */
    }
    refreshData() {
        this.displayableMetadataFields = this.projectConfig.projectViewConfig ?
            this.utilService.getDisplayOrderData(this.displayableFields, this.projectConfig.projectViewConfig.listFieldOrder) : [];
        // this.displayableMetadataFields = this.projectConfig.projectViewConfig ?
        //     this.utilService.getDisplayOrderData(this.projectConfig.projectViewConfig.R_PM_LIST_VIEW_MPM_FIELDS, this.projectConfig.projectViewConfig.LIST_FIELD_ORDER) : [];
        this.displayColumns = this.displayableMetadataFields.map((column) => column.INDEXER_FIELD_ID);
        this.isSelect = this.isCampaign ? !this.isCampaign : (!this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE);
        /* if ((this.isCampaign && !this.isCampaign) || (this.isCampaignDashboard && !this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE)) {
         */
        if (this.isSelect) {
            this.displayColumns.unshift('Select');
        }
        if (this.projectConfig.IS_PROJECT_TYPE) {
            this.displayColumns.unshift('Expand');
        }
        //Copy template ticket-shree tharani
        /*if (this.showCopyProject || this.projectConfig.IS_PROJECT_TYPE) {
            this.displayColumns.push('Actions');
        }*/
        this.displayColumns.push('Actions');
        this.projectDataDataSource.data = [];
        this.projectDataDataSource.data = this.projectConfig.projectData;
        this.sort.active = this.projectConfig.selectedSortOption.option.name;
        this.sort.direction = this.projectConfig.selectedSortOption.sortType.toLowerCase();
        this.projectDataDataSource.sort = this.sort;
    }
    tableDrop(event) {
        let count = 0;
        if (this.displayColumns.find(column => column === 'Select')) {
            count = count + 1;
        }
        if (this.displayColumns.find(column => column === 'Expand')) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        const orderedFields = [];
        this.displayColumns.forEach(column => {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(this.displayableFields.find(displayableField => displayableField.INDEXER_FIELD_ID === column));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    }
    onResizeMouseDown(event, column) {
        event.stopPropagation();
        event.preventDefault();
        const start = event.target;
        const pressed = true;
        const startX = event.x;
        const startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    }
    initResizableColumns(start, pressed, startX, startWidth, column) {
        this.renderer.listen('body', 'mousemove', (event) => {
            if (pressed) {
                const width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', (event) => {
            if (pressed) {
                pressed = false;
                this.resizeDisplayableFields.next();
            }
        });
    }
    deleteProject(event, row) {
        console.log(event);
        console.log(row);
        let msg;
        this.affectedTask = [];
        let tasks;
        let isCustomWorkflow = this.utilService.getBooleanValue(row.IS_CUSTOM_WORKFLOW);
        const name = this.isTemplate ? 'Template' : 'Project';
        if (isCustomWorkflow) {
            msg = "Are you sure you want to delete the " + name + " and reconfigure the workflow rule engine?";
        }
        else {
            msg = "Are you sure you want to delete the " + name + " and Exit?";
        }
        this.projectService.getTaskByProjectID(row.ID).subscribe(projectTaskResponse => {
            console.log(projectTaskResponse.GetTaskByProjectIDResponse.Task);
            if (projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task && !Array.isArray(projectTaskResponse.GetTaskByProjectIDResponse.Task)) {
                tasks = [projectTaskResponse.GetTaskByProjectIDResponse.Task];
            }
            else {
                tasks = projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task ? projectTaskResponse.GetTaskByProjectIDResponse.Task : '';
            }
            if (tasks && tasks.length > 0) {
                tasks.forEach(task => {
                    this.affectedTask.push(task.NAME);
                });
            }
            // });
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: this.projectConfig.IS_PROJECT_TYPE ? this.affectedTask : [],
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(response => {
                if (response) {
                    this.projectService.deleteProject(row.ID, row.ITEM_ID, row.PROJECT_STATUS_ID).subscribe(deleteProjectResponse => {
                        this.notificationService.success(name + ' has been deleted successfully');
                        //this.refreshData();
                        this.refreshEmitter.next();
                    }, error => {
                        this.notificationService.error('Delete ' + name + ' operation failed, try again later');
                    });
                }
            });
        });
    }
    ngOnChanges(changes) {
        if (changes.displayableFields && !changes.displayableFields.firstChange &&
            (JSON.stringify(changes.displayableFields.currentValue) !== JSON.stringify(changes.displayableFields.previousValue))) {
            this.refreshData();
        }
        this.refreshData();
    }
    ngOnInit() {
        this.position.value = 'below';
        const userId = this.sharingService.getCurrentUserItemID();
        this.loggedInUserId = +userId;
        this.finalCompleted = StatusTypes.FINAL_COMPLETED;
        const projectConfig = this.sharingService.getProjectConfig();
        const appConfig = this.sharingService.getAppConfig();
        this.showCopyProject = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_COPY_PROJECT]);
        this.enableCampaign = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.isCampaignManager = (this.sharingService.getCampaignRole() && this.sharingService.getCampaignRole().length > 0) ? true : false;
        this.IsPMView = (this.projectType === ProjectTypes.CAMPAIGN || this.router.url.includes('Campaigns') || this.router.url.includes('campaign')) ? true : false;
        this.isTemplate = this.projectType === ProjectTypes.TEMPLATE || this.router.url.includes('Templates') ? true : false;
        if (this.router.url.includes('Campaigns') || this.projectConfig.IS_CAMPAIGN_TYPE) {
            this.isCampaignDashboard = true;
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
        }
        if (this.projectConfig.metaDataFields && this.projectConfig.projectData) {
            this.refreshData();
        }
        if (!this.projectConfig.projectData) {
            this.projectConfig.projectData = [];
        }
        if (!this.projectConfig.metaDataFields) {
            this.projectConfig.metaDataFields = [];
        }
        if (this.sort) {
            this.sort.disableClear = true;
            this.sort.sortChange.subscribe(() => {
                if (this.sort.active && this.sort.direction) {
                    const selectedSortList = {
                        sortType: this.sort.direction.toUpperCase(),
                        option: {
                            name: this.sort.active,
                            displayName: ''
                        }
                    };
                    this.sortChange.emit(selectedSortList);
                }
            });
        }
        /*  if (this.selectAllDeliverables) {
             this.selectAllDeliverables.subscribe((data: boolean) => {
                 if (data) {
                     this.checked = true;
                 } else {
                     this.checked = false;
                 }
                 this.checkDeliverable(this.asset);
             });
         } */
    }
};
ProjectListComponent.ctorParameters = () => [
    { type: MatDialog },
    { type: ProjectService },
    { type: TaskService },
    { type: UtilService },
    { type: NotificationService },
    { type: LoaderService },
    { type: FormatToLocalePipe },
    { type: OtmmMetadataService },
    { type: EntityAppDefService },
    { type: FieldConfigService },
    { type: SharingService },
    { type: ProjectBulkCountService },
    { type: Router },
    { type: Renderer2 },
    { type: GetPropertyPipe }
];
__decorate([
    Input()
], ProjectListComponent.prototype, "projectConfig", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "isCampaign", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "projectType", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "displayableFields", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "facetExpansionState", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "enableGhostResize", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "viewProjectDetails", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "viewTaskDetails", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "editTaskHandler", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "sortChange", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "copyProject", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "openMilestoneDialog", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "openCommentsModule", void 0);
__decorate([
    Input()
], ProjectListComponent.prototype, "userPreferenceFreezeCount", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "selectedProjectsCallBackHandler", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "openCampaignInProjectList", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "orderedDisplayableFields", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "resizeDisplayableFields", void 0);
__decorate([
    Output()
], ProjectListComponent.prototype, "refreshEmitter", void 0);
__decorate([
    ViewChild(MatSort, { static: true })
], ProjectListComponent.prototype, "sort", void 0);
ProjectListComponent = __decorate([
    Component({
        selector: 'mpm-project-list',
        template: "<table class=\"project-list\" mat-table [dataSource]=\"projectDataDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Expand\" *ngIf=\"projectConfig.IS_PROJECT_TYPE\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\" (click)=\"expandRow($event, row)\">\r\n            <mat-icon *ngIf=\"!row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_right</mat-icon>\r\n            <mat-icon *ngIf=\"row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_down</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllProjectChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"selection.hasValue() && isAllSelected()\" [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n                <!-- [(ngModel)]=\"selectAllProjectChecked\" -->\r\n            </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"row.selected\" [disabled]=\"!enableBulkEdit\" (click)=\"$event.stopPropagation()\" (change)=\"$event ? checkProject(row,$event) : null\" [checked]=\"selection.isSelected(row)\">\r\n            </mat-checkbox>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields; index as i\" [sticky]=\"i < userPreferenceFreezeCount\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':column.DATA_TYPE === 'string'}\">\r\n            <span>\r\n                <div *ngIf=\"column.MAPPER_NAME!== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS && column.MAPPER_NAME !== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY && column.MAPPER_NAME !== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS\"\r\n                    class=\"flex-card\" (click)=\"(openProjectDetails(row,column.MAPPER_NAME,enableCampaign , isCampaignManager , !IsPMView))\">\r\n                    <a matTooltip=\"{{getProperty(column, row)}}\" [matTooltipPosition]=\"position.value\"\r\n                        matTooltipClass=\"mattooltipclass\" [ngClass]=\"{'campaignLink': getProperty(column, row) != 'NA' && (column.MAPPER_NAME === mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID) && enableCampaign && isCampaignManager && !IsPMView}\">{{getProperty(column, row) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">\r\n                        {{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS\">\r\n                    <span matTooltip=\"{{getProperty(column, row) || 'NA'}}% {{milestoneTasks}}\">{{getProperty(column, row) || 'NA'}}%\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                            value=\"{{getProperty(column, row) || 'NA'}}\" class=\"\">\r\n                        </mat-progress-bar>\r\n                    </span>\r\n            </div>\r\n            <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS\">\r\n                <mat-chip-list>\r\n                    <mat-chip class=\"status-chip\">{{getProperty(column, row) || 'NA'}}</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div class=\"min-width-action flex-card\">\r\n                <button *ngIf=\"showCopyProject\" mat-icon-button matTooltip=\"{{projectConfig.IS_PROJECT_TYPE ? 'Copy Project': 'Project from template'}}\" (click)=\"editProject($event, row, false)\">\r\n                    <mat-icon>insert_drive_file</mat-icon>\r\n                </button>\r\n                <!--Copy Template\r\n                (!isCampaignDashboard || !projectConfig.IS_CAMPAIGN_TYPE) && !this.projectConfig.IS_PROJECT_TYPE\r\n                isTemplate-change-->\r\n                <button *ngIf=\"isTemplate\" mat-icon-button matTooltip=\"Copy Template\" (click)=\"editProject($event, row, true)\">\r\n                    <mat-icon>insert_drive_file</mat-icon>\r\n                </button>\r\n\r\n                <button mat-icon-button matTooltip=\"Comments\" *ngIf=\"projectConfig.IS_PROJECT_TYPE\" (click)=\"openComment($event, row)\">\r\n                    <mat-icon>comment</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"expandedDetail\">\r\n        <td class=\"project-detail\" mat-cell *matCellDef=\"let row\" [attr.colspan]=\"displayColumns.length\">\r\n            <div class=\"project-element-detail\" [@detailExpand]=\"row.isExpanded ? 'expanded' : 'collapsed'\">\r\n                <div class=\"task-info data\" *ngIf=\"row.taskTotalCount !== 0\">\r\n                    <div class=\"task-heading\">Current Active Tasks:</div>\r\n                </div>\r\n                <div class=\"project-element-diagram\" *ngFor=\"let task of row.taskList\">\r\n                    <mpm-task-card-view [isTemplate]=\"!projectConfig.IS_PROJECT_TYPE\" [taskData]=\"task\" [projectStatusType]=\"projectStatusType\" [taskConfig]=\"projectConfig\" (refreshTask)=\"refreshTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\" (editTaskHandler)=\"editTask($event)\"\r\n                        [projectOwner]=\"getPropertyByMapper(row, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_ID)\">\r\n                    </mpm-task-card-view>\r\n                </div>\r\n                <div class=\"task-info no-data\" *ngIf=\"row.taskTotalCount === 0\">\r\n                    <div class=\"task-heading\">No Active Task</div>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"project-element-row\">\r\n        <!-- (click)=\"openProjectDetails(row,isCampaignManager)\" -->\r\n    </tr>\r\n\r\n    <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\" class=\"project-detail-row\"></tr>\r\n</table>\r\n\r\n<div *ngIf=\"projectConfig.totalProjectCount == 0\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No project is available.</button>\r\n</div>",
        animations: [trigger('detailExpand', [
                state('collapsed', style({ height: '0px' })),
                state('expanded', style({ height: '*' })),
                transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.42, 0.0, 0.58, 1.0)')),
            ])] /* ,
        encapsulation: ViewEncapsulation.None */,
        styles: ["table.project-list{width:100%}table.project-list tr{cursor:pointer}table.project-list tr .mat-checkbox{padding:0 8px}table.project-list tr th:last-child{padding-left:12px}table.project-list tr td{min-width:8em}table.project-list tr td a.mat-button{padding:0;text-align:left}table.project-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.project-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}table.project-list tr td mat-progress-bar{max-width:7em}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.project-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.project-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.project-detail{padding:0!important}tr.project-detail-row{height:0}.project-element-row td{border-bottom-width:0}.project-element-detail{overflow:hidden;display:flex;flex-wrap:wrap;cursor:auto!important}.project-element-diagram{min-width:80px;padding:8px;font-weight:lighter;margin:8px 0}.task-info{min-width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex}.task-info.data{justify-content:left}.task-info.no-data{justify-content:center}td.wrap-text-custom span>div{max-width:12em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:6em}.mattooltipclass{background-color:red;margin-left:70px}"]
    })
], ProjectListComponent);
export { ProjectListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LWxpc3QvcHJvamVjdC1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9ILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMvRSxzRUFBc0U7QUFDdEUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRXBFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFaEYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQzNGLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ25GLE9BQU8sRUFBZSxlQUFlLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN0RSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDakUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFFckgsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQWF0RSxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQTREN0IsWUFDVyxNQUFpQixFQUNqQixjQUE4QixFQUM5QixXQUF3QixFQUN4QixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsYUFBNEIsRUFDNUIsa0JBQXNDLEVBQ3RDLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLHVCQUFnRCxFQUNoRCxNQUFjLEVBQ2QsUUFBbUIsRUFDbkIsZUFBZ0M7UUFkaEMsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQXJFbEMsc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ2xDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0Msb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQzVDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUM1QyxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNyQyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5Qyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRXZEOztxRUFFNkQ7UUFDbkQsb0NBQStCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRSxrRUFBa0U7UUFDeEQsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQztRQUN4RCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3JELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7UUFDcEQsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBSXJELGNBQVMsR0FBRyxJQUFJLGNBQWMsQ0FBTSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDOUMsMEJBQXFCLEdBQUcsSUFBSSxrQkFBa0IsQ0FBTSxFQUFFLENBQUMsQ0FBQztRQUN4RCxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0Qyw4QkFBeUIsR0FBb0IsRUFBRSxDQUFDO1FBQ2hELG9DQUErQixHQUFHLEVBQUUsQ0FBQztRQUVyQyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUV0QyxhQUFRLEdBQUc7WUFDUCxPQUFPLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRixXQUFNLEdBQUcsQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDcEMsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIscUJBQWdCLEdBQUcsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBR2hELHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUc1QixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQixrQkFBYSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFPbkMsY0FBUyxHQUFHLElBQUksQ0FBQztRQUdqQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQWlCZCxDQUFDO0lBR0wsZ0ZBQWdGO0lBQ2hGLGFBQWE7UUFDVCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDbkQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkQsT0FBTyxXQUFXLEtBQUssT0FBTyxDQUFDO0lBQ25DLENBQUM7SUFFRCxnRkFBZ0Y7SUFDaEY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1NBb0JLO0lBRUwsaUNBQWlDLENBQUMsU0FBUztRQUV2QyxJQUFJLENBQUMsY0FBYyxDQUFDLGlDQUFpQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNsRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNoQyxRQUFRLENBQUMsS0FBSyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyQztnQkFDRCxJQUFJLGFBQWEsQ0FBQztnQkFDbEIsYUFBYSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7Z0JBQy9CLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ25FLGFBQWEsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQzFCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxHQUFHLElBQUksU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUU7NEJBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQzlFLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7eUJBQ3pEO29CQUVMLENBQUMsQ0FBQyxDQUFDO2lCQUVOO2dCQUNEOzs7OzttQkFLRztnQkFDSCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzthQUM1QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzthQUM1QjtRQUNMLENBQUMsRUFBRSxHQUFHLEVBQUU7UUFDUixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZO1FBQ1IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDdkYsQ0FBQztJQUVELGdCQUFnQjtRQUNaLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDMUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCxZQUFZLENBQUMsV0FBVyxFQUFFLEtBQUs7UUFDM0IsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2YsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM3RTthQUFNO1lBQ0gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM3RTtJQUVMLENBQUM7SUFDRCxXQUFXLENBQUMsYUFBdUIsRUFBRSxXQUFnQjtRQUNqRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUMvQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLEtBQUssWUFBWSxFQUFFO2dCQUNuRyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQzthQUM5QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsZ0dBQWdHO1FBQ2hHLDhEQUE4RDtRQUM5RCw2SEFBNkg7UUFDN0gsSUFBSTtRQUNKLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDeEYsSUFBSTtJQUVSLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsVUFBVTtRQUN2QyxNQUFNLFlBQVksR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUNELGlCQUFpQixDQUFDLElBQUksRUFBRSxVQUFVO1FBQzlCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQ3RHLENBQUM7SUFFRCxXQUFXLENBQUMsV0FBZ0IsRUFBRSxZQUFzQjtRQUNoRCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsRUFDNUYsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXhCLE9BQU87WUFDSCxLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUs7WUFDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxXQUFXLENBQUMsT0FBTztTQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFDLGNBQXNCO1FBQzdDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFO1lBQ3BDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO2FBQU07WUFDSCxPQUFPLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELE9BQU8sQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsYUFBYSxDQUFDLEtBQUssRUFBRSxPQUFPO1FBQ3hCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFO1lBQ3BDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO2FBQU07WUFDSCxPQUFPLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELFFBQVEsQ0FBQyxTQUFTO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLLEVBQUUsY0FBYztRQUM3QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsRUFDM0ssY0FBYyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsMkJBQTJCLENBQUMsT0FBWSxFQUFFLFFBQWtCO1FBQ3hELElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLFFBQVEsRUFBRTtZQUMvQixPQUFPLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1lBQ3JELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEtBQUssZUFBZSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwSCxJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwRSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQ2xELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUM1SSxPQUFPLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FDdEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQ2pKLE9BQU8sQ0FBQyxDQUFDO1lBQ2YsTUFBTSxVQUFVLEdBQUc7Z0JBQ2YsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CO2dCQUNsRCxJQUFJLEVBQUUsQ0FBQztnQkFDUCxHQUFHLEVBQUUsR0FBRzthQUNYLENBQUM7WUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRTFCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDO2lCQUM3RSxTQUFTLENBQUMsQ0FBQyxRQUF3QixFQUFFLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDakMsT0FBTyxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztZQUMzRCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO1lBQ2xHLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQVk7UUFDcEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsZUFBZSxDQUFDLFNBQVM7UUFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUFLLEVBQUUsR0FBRztRQUNoQixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2hCLEdBQUcsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQzFCO2FBQU07WUFDSCxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsMkJBQTJCLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDekM7SUFDTCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsUUFBUTtRQUMvRSxJQUFJLFVBQVUsS0FBSyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLElBQUksY0FBYyxJQUFJLGlCQUFpQixJQUFJLFFBQVEsRUFBRSxFQUFDLHVCQUF1QjtZQUM3SSxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDekMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsRUFDOUssT0FBTyxDQUFDLENBQUM7WUFDZixJQUFJLFVBQVUsSUFBSSxVQUFVLEtBQUssSUFBSSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO29CQUMzQixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM3QztnQkFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3hILENBQUM7O2dCQUVFO1NBQ1A7YUFDSSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUMvQiw0Q0FBNEM7WUFDNUMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUNuTCxPQUFPLENBQUMsQ0FBQztZQUNmLElBQUksVUFBVSxJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCOztvQkFFSTtnQkFDSixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUMxRDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDLENBQUM7YUFDekQ7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDO1lBQ3hDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQ2hMLE9BQU8sQ0FBQyxDQUFDO1lBQ2YsSUFBSSxTQUFTLElBQUksU0FBUyxLQUFLLElBQUksRUFBRTtnQkFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2FBQ3hEO1NBQ0o7UUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQW1CSTtJQUNSLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0gsMEVBQTBFO1FBQzFFLHdLQUF3SztRQUN4SyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFnQixFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN4RyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6SDtXQUNHO1FBQ0gsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3pDO1FBQ0Qsb0NBQW9DO1FBQ3BDOztXQUVHO1FBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUNqRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDckUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkYsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2hELENBQUM7SUFFRCxTQUFTLENBQUMsS0FBNEI7UUFDbEMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsRUFBRTtZQUN6RCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUNyQjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLEVBQUU7WUFDekQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUM7U0FDckI7UUFDRCxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzlGLE1BQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxLQUFLLFNBQVMsQ0FBQyxFQUFFO2dCQUN2RSxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDckg7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGlCQUFpQixDQUFDLEtBQUssRUFBRSxNQUFNO1FBQzNCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMzQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN2QixNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFdBQVcsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2hELElBQUksT0FBTyxFQUFFO2dCQUNULE1BQU0sS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDOUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYSxDQUFDLEtBQUssRUFBRSxHQUFHO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUFJLEdBQUcsQ0FBQztRQUNSLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksS0FBSyxDQUFDO1FBQ1YsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoRixNQUFNLElBQUksR0FBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUN2RCxJQUFJLGdCQUFnQixFQUFFO1lBQ2xCLEdBQUcsR0FBRyxzQ0FBc0MsR0FBQyxJQUFJLEdBQUMsNENBQTRDLENBQUM7U0FDbEc7YUFBTTtZQUNILEdBQUcsR0FBRyxzQ0FBc0MsR0FBQyxJQUFJLEdBQUMsWUFBWSxDQUFDO1NBQ2xFO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7WUFDM0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqRSxJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3JNLEtBQUssR0FBRyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNO2dCQUNILEtBQUssR0FBRyxtQkFBbUIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ25NO1lBQ0QsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzNCLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUNELE1BQU07WUFDTixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsR0FBRztvQkFDWixJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ2pFLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDckI7YUFDSixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN6QyxJQUFJLFFBQVEsRUFBRTtvQkFDVixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQzVHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFDLGdDQUFnQyxDQUFDLENBQUM7d0JBQ3hFLHFCQUFxQjt3QkFDckIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDL0IsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFDLElBQUksR0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO29CQUN4RixDQUFDLENBQUMsQ0FBQztpQkFFTjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksT0FBTyxDQUFDLGlCQUFpQixJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFdBQVc7WUFDbkUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO1lBQ3RILElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtJQUN0QixDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUM5QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFBO1FBQ3pJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3BJLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLFlBQVksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM3SixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLEtBQUssWUFBWSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3RILElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUU7WUFDOUUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDNUM7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQ3JFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRTtZQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUU7WUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1gsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ3pDLE1BQU0sZ0JBQWdCLEdBQUc7d0JBQ3JCLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUU7d0JBQzNDLE1BQU0sRUFBRTs0QkFDSixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNOzRCQUN0QixXQUFXLEVBQUUsRUFBRTt5QkFDbEI7cUJBQ0osQ0FBQztvQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2lCQUMxQztZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRDs7Ozs7Ozs7O2FBU0s7SUFDVCxDQUFDO0NBQ0osQ0FBQTs7WUF6ZHNCLFNBQVM7WUFDRCxjQUFjO1lBQ2pCLFdBQVc7WUFDWCxXQUFXO1lBQ0gsbUJBQW1CO1lBQ3pCLGFBQWE7WUFDUixrQkFBa0I7WUFDakIsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNwQixrQkFBa0I7WUFDdEIsY0FBYztZQUNMLHVCQUF1QjtZQUN4QyxNQUFNO1lBQ0osU0FBUztZQUNGLGVBQWU7O0FBMUVsQztJQUFSLEtBQUssRUFBRTsyREFBZTtBQUNkO0lBQVIsS0FBSyxFQUFFO3dEQUFZO0FBQ1g7SUFBUixLQUFLLEVBQUU7eURBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTsrREFBbUI7QUFDbEI7SUFBUixLQUFLLEVBQUU7aUVBQXFCO0FBQ3BCO0lBQVIsS0FBSyxFQUFFOytEQUFvQztBQUNsQztJQUFULE1BQU0sRUFBRTtnRUFBOEM7QUFDN0M7SUFBVCxNQUFNLEVBQUU7NkRBQTZDO0FBQzVDO0lBQVQsTUFBTSxFQUFFOzZEQUE2QztBQUM1QztJQUFULE1BQU0sRUFBRTt3REFBc0M7QUFDckM7SUFBVCxNQUFNLEVBQUU7eURBQXVDO0FBQ3RDO0lBQVQsTUFBTSxFQUFFO2lFQUErQztBQUM5QztJQUFULE1BQU0sRUFBRTtnRUFBOEM7QUFDOUM7SUFBUixLQUFLLEVBQUU7dUVBQTJCO0FBSXpCO0lBQVQsTUFBTSxFQUFFOzZFQUEyRDtBQUUxRDtJQUFULE1BQU0sRUFBRTt1RUFBeUQ7QUFDeEQ7SUFBVCxNQUFNLEVBQUU7c0VBQXNEO0FBQ3JEO0lBQVQsTUFBTSxFQUFFO3FFQUFxRDtBQUNwRDtJQUFULE1BQU0sRUFBRTs0REFBNEM7QUFFZjtJQUFyQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO2tEQUFlO0FBekIzQyxvQkFBb0I7SUFaaEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGtCQUFrQjtRQUM1QiwyaE9BQTRDO1FBRTVDLFVBQVUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUU7Z0JBQ2pDLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQzVDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsMENBQTBDLENBQUMsQ0FBQzthQUM1RixDQUFDLENBQUMsQ0FBQTtnREFDcUM7O0tBQzNDLENBQUM7R0FFVyxvQkFBb0IsQ0FzaEJoQztTQXRoQlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBWaWV3Q2hpbGQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbi8vaW1wb3J0IHsgUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2VcclxuaW1wb3J0IHsgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NvcnQnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IERhdGFUeXBlQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvY29uc3RhbnRzL2RhdGEtdHlwZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZVBpcGUgfSBmcm9tICcuLi8uLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIGFuaW1hdGUgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3Rhc2tzL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGQsIE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoUmVzcG9uc2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlc3BvbnNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0QnVsa0NvdW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LWJ1bGstY291bnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFByb2plY3RUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1Byb2plY3RUeXBlcyc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uLy4uL3Byb2plY3QvcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wLCBtb3ZlSXRlbUluQXJyYXkgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IEdldFByb3BlcnR5UGlwZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9waXBlL2dldC1wcm9wZXJ0eS5waXBlJztcclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1wcm9qZWN0LWxpc3QnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Byb2plY3QtbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9wcm9qZWN0LWxpc3QuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGFuaW1hdGlvbnM6IFt0cmlnZ2VyKCdkZXRhaWxFeHBhbmQnLCBbXHJcbiAgICAgICAgc3RhdGUoJ2NvbGxhcHNlZCcsIHN0eWxlKHsgaGVpZ2h0OiAnMHB4JyB9KSksXHJcbiAgICAgICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyBoZWlnaHQ6ICcqJyB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignZXhwYW5kZWQgPD0+IGNvbGxhcHNlZCcsIGFuaW1hdGUoJzIyNW1zIGN1YmljLWJlemllcigwLjQyLCAwLjAsIDAuNTgsIDEuMCknKSksXHJcbiAgICBdKV0vKiAsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lICovXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvamVjdExpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICBASW5wdXQoKSBwcm9qZWN0Q29uZmlnO1xyXG4gICAgQElucHV0KCkgaXNDYW1wYWlnbjtcclxuICAgIEBJbnB1dCgpIHByb2plY3RUeXBlO1xyXG4gICAgQElucHV0KCkgZGlzcGxheWFibGVGaWVsZHM7XHJcbiAgICBASW5wdXQoKSBmYWNldEV4cGFuc2lvblN0YXRlO1xyXG4gICAgQElucHV0KCkgZW5hYmxlR2hvc3RSZXNpemU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIEBPdXRwdXQoKSB2aWV3UHJvamVjdERldGFpbHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB2aWV3VGFza0RldGFpbHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gICAgQE91dHB1dCgpIGVkaXRUYXNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgICBAT3V0cHV0KCkgc29ydENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNvcHlQcm9qZWN0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3Blbk1pbGVzdG9uZURpYWxvZyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG9wZW5Db21tZW50c01vZHVsZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQElucHV0KCkgdXNlclByZWZlcmVuY2VGcmVlemVDb3VudDtcclxuICAgIC8qICBAT3V0cHV0KCkgc2VsZWN0ZWRQcm9qZWN0Q291bnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgICBAT3V0cHV0KCkgc2VsZWN0ZWRQcm9qZWN0RGF0YSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgIEBPdXRwdXQoKSBzZWxlY3RlZFByb2plY3REYXRhcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpOyAqL1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdGVkUHJvamVjdHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIC8vIEBPdXRwdXQoKSBzZWxlY3RBbGxQcm9qZWN0c0NvdW50ID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG4gICAgQE91dHB1dCgpIG9wZW5DYW1wYWlnbkluUHJvamVjdExpc3QgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3JkZXJlZERpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICAgIEBPdXRwdXQoKSByZXNpemVEaXNwbGF5YWJsZUZpZWxkcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVmcmVzaEVtaXR0ZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoTWF0U29ydCwgeyBzdGF0aWM6IHRydWUgfSkgc29ydDogTWF0U29ydDtcclxuXHJcbiAgICBzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8YW55Pih0cnVlLCBbXSk7XHJcbiAgICBwcm9qZWN0RGF0YURhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlPGFueT4oW10pO1xyXG4gICAgbWV0YURhdGFEYXRhVHlwZXMgPSBEYXRhVHlwZUNvbnN0YW50cztcclxuICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM6IEFycmF5PE1QTUZpZWxkPiA9IFtdO1xyXG4gICAgZGlzcGxheWFibGVDdXN0b21NZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgaXNQcm9qZWN0T3duZXI7XHJcbiAgICBkaXNwbGF5Q29sdW1ucyA9IFtdO1xyXG4gICAgbXBtRmllbGRDb25zdGFudHMgPSBNUE1GaWVsZENvbnN0YW50cztcclxuICAgIHNob3dDb3B5UHJvamVjdDogYm9vbGVhbjtcclxuICAgIHBvc2l0aW9uID0ge1xyXG4gICAgICAgICd2YWx1ZSc6ICcnXHJcbiAgICB9O1xyXG4gICAgc3RhdHVzID0gWydEZWZpbmVkJywgJ0luIFByb2dyZXNzJ107XHJcbiAgICBlbmFibGVCdWxrRWRpdCA9IGZhbHNlO1xyXG4gICAgc2VsZWN0ZWRQcm9qZWN0cyA9IG5ldyBTZWxlY3Rpb25Nb2RlbCh0cnVlLCBbXSk7XHJcbiAgICBzZWxlY3RBbGxQcm9qZWN0Q2hlY2tlZDogYm9vbGVhbjtcclxuICAgIGNoZWNrZWQ6IGJvb2xlYW47XHJcbiAgICBpc0NhbXBhaWduRGFzaGJvYXJkID0gZmFsc2U7XHJcbiAgICBpc1NlbGVjdDtcclxuICAgIGlzQ2FtcGFpZ25NYW5hZ2VyO1xyXG4gICAgbWlsZXN0b25lVGFza3MgPSAnJztcclxuXHJcbiAgICBjYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5QUk9KRUNUO1xyXG4gICAgZW5hYmxlQ2FtcGFpZ247XHJcbiAgICBJc1BNVmlldztcclxuICAgIGlzVGVtcGxhdGU7XHJcbiAgICBwcm9qZWN0U3RhdHVzVHlwZTtcclxuICAgIHByb2plY3RDYW5jZWxsZWRTdGF0dXM7XHJcbiAgICBpc09uSG9sZFByb2plY3Q7XHJcbiAgICBpc1JlZnJlc2ggPSB0cnVlO1xyXG4gICAgbG9nZ2VkSW5Vc2VySWQ7XHJcbiAgICBmaW5hbENvbXBsZXRlZDtcclxuICAgIGFmZmVjdGVkVGFzayA9IFtdO1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmb3JtYXRUb0xvY2FsZVBpcGU6IEZvcm1hdFRvTG9jYWxlUGlwZSxcclxuICAgICAgICBwdWJsaWMgb3RtbU1ldGFkYXRhU2VydmljZTogT3RtbU1ldGFkYXRhU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdEJ1bGtDb3VudFNlcnZpY2U6IFByb2plY3RCdWxrQ291bnRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwdWJsaWMgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICBwdWJsaWMgZ2V0UHJvcGVydHlQaXBlOiBHZXRQcm9wZXJ0eVBpcGVcclxuICAgICkgeyB9XHJcblxyXG5cclxuICAgIC8qKiBXaGV0aGVyIHRoZSBudW1iZXIgb2Ygc2VsZWN0ZWQgZWxlbWVudHMgbWF0Y2hlcyB0aGUgdG90YWwgbnVtYmVyIG9mIHJvd3MuICovXHJcbiAgICBpc0FsbFNlbGVjdGVkKCkge1xyXG4gICAgICAgIGNvbnN0IG51bVNlbGVjdGVkID0gdGhpcy5zZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IG51bVJvd3MgPSB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5kYXRhLmxlbmd0aDtcclxuICAgICAgICByZXR1cm4gbnVtU2VsZWN0ZWQgPT09IG51bVJvd3M7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIFNlbGVjdHMgYWxsIHJvd3MgaWYgdGhleSBhcmUgbm90IGFsbCBzZWxlY3RlZDsgb3RoZXJ3aXNlIGNsZWFyIHNlbGVjdGlvbi4gKi9cclxuICAgIC8qICBtYXN0ZXJUb2dnbGUoZGF0YSwgdmFsdWU/OiBhbnkpIHtcclxuICAgICAgICAgXHJcbiAgICAgICAgIGxldCBwcm9qZWN0SWQ7XHJcbiAgICAgICAgIGlmICh0aGlzLmlzQWxsU2VsZWN0ZWQoKSkge1xyXG4gICAgICAgICAgICAgdGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiBcclxuICAgICAgICAgICAgICAgICBwcm9qZWN0SWQgPSB0aGlzLnByb2plY3RCdWxrQ291bnRTZXJ2aWNlLmdldFByb2plY3RCdWxrQ291bnQocm93KTtcclxuICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgdGhpcy5zZWxlY3Rpb24uY2xlYXIoKTtcclxuICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgIHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEuZm9yRWFjaChyb3cgPT4ge1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uLnNlbGVjdChyb3cpO1xyXG4gICAgICAgICAgICAgICAgIC8vIHByb2plY3RJZC5wdXNoKHJvdy5JRCk7XHJcbiAgICAgICAgICAgICAgICAgcHJvamVjdElkID0gdGhpcy5wcm9qZWN0QnVsa0NvdW50U2VydmljZS5nZXRQcm9qZWN0QnVsa0NvdW50KHJvdyk7XHJcbiAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0RGF0YS5uZXh0KHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEpO1xyXG4gICAgICAgICB9IFxyXG4gICAgICAgICBjb25zb2xlLmxvZyhwcm9qZWN0SWQubGVuZ3RoKTtcclxuICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RDb3VudC5uZXh0KHByb2plY3RJZC5sZW5ndGgpXHJcbiAgICAgICAgIFxyXG4gICAgIH0gKi9cclxuXHJcbiAgICBnZXRDb21wbGV0ZWRNaWxlc3RvbmVzQnlQcm9qZWN0SWQocHJvamVjdElkKSB7XHJcblxyXG4gICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0Q29tcGxldGVkTWlsZXN0b25lc0J5UHJvamVjdElkKHByb2plY3RJZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnR1cGxlICYmIHJlc3BvbnNlLnR1cGxlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UudHVwbGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UudHVwbGUgPSBbcmVzcG9uc2UudHVwbGVdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbGV0IG1pbGVzdG9uZUxpc3Q7XHJcbiAgICAgICAgICAgICAgICBtaWxlc3RvbmVMaXN0ID0gcmVzcG9uc2UudHVwbGU7XHJcbiAgICAgICAgICAgICAgICBpZiAobWlsZXN0b25lTGlzdCAmJiBtaWxlc3RvbmVMaXN0Lmxlbmd0aCAmJiBtaWxlc3RvbmVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBtaWxlc3RvbmVMaXN0Lm1hcChtaWxlc3RvbmUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWlsZXN0b25lICYmIG1pbGVzdG9uZS5vbGQgJiYgbWlsZXN0b25lLm9sZC5UYXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1pbGVzdG9uZVRhc2tzID0gdGhpcy5taWxlc3RvbmVUYXNrcy5jb25jYXQobWlsZXN0b25lLm9sZC5UYXNrLlRhc2tOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWlsZXN0b25lVGFza3MgPSB0aGlzLm1pbGVzdG9uZVRhc2tzLmNvbmNhdChcIixcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLyppZiAodGhpcy5taWxlc3RvbmVUYXNrcy5sZW5ndGggPD0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWlsZXN0b25lVGFza3MgPSAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWlsZXN0b25lVGFza3MgPSAnJztcclxuICAgICAgICAgICAgICAgIH0qL1xyXG4gICAgICAgICAgICAgICAgdGhpcy5taWxlc3RvbmVUYXNrcyA9ICcnO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5taWxlc3RvbmVUYXNrcyA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG1hc3RlclRvZ2dsZSgpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdEFsbFByb2plY3RDaGVja2VkID8gdGhpcy5jaGVja0FsbFByb2plY3RzKCkgOiB0aGlzLnVuY2hlY2tBbGxQcm9qZWN0cygpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrQWxsUHJvamVjdHMoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiAgICAgICAgICAgIHJvdy5zZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5zZWxlY3Qocm93KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdHNDYWxsQmFja0hhbmRsZXIuZW1pdCh0aGlzLnNlbGVjdGVkUHJvamVjdHMuc2VsZWN0ZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHVuY2hlY2tBbGxQcm9qZWN0cygpIHtcclxuICAgICAgICB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5kYXRhLmZvckVhY2gocm93ID0+IHtcclxuICAgICAgICAgICAgcm93LnNlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5kZXNlbGVjdChyb3cpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5zZWxlY3RlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tQcm9qZWN0KHByb2plY3REYXRhLCBldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5jaGVja2VkKSB7XHJcbiAgICAgICAgICAgIHByb2plY3REYXRhLnNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdChwcm9qZWN0RGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5zZWxlY3RlZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzLmRlc2VsZWN0KHByb2plY3REYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzQ2FsbEJhY2tIYW5kbGVyLmVtaXQodGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdGVkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgZ2V0UHJvcGVydHkoZGlzcGxheUNvbHVtbjogTVBNRmllbGQsIHByb2plY3REYXRhOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIHRoaXMuc3RhdHVzLmZvckVhY2goc3RhdHVzRmlsdGVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtbiwgcHJvamVjdERhdGEpID09PSBzdGF0dXNGaWx0ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW5hYmxlQnVsa0VkaXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gaWYoZGlzcGxheUNvbHVtbi5NQVBQRVJfTkFNRSA9PT0gdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9QUk9HUkVTUyl7XHJcbiAgICAgICAgLy8gICAgIHRoaXMuZ2V0Q29tcGxldGVkTWlsZXN0b25lc0J5UHJvamVjdElkKHByb2plY3REYXRhLklEKTtcclxuICAgICAgICAvLyAgICAgcmV0dXJuICh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bW4sIHByb2plY3REYXRhKS5jb25jYXQodGhpcy5taWxlc3RvbmVUYXNrcykpO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAvLyBlbHNle1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bW4sIHByb2plY3REYXRhKTtcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5QnlNYXBwZXIocHJvamVjdERhdGEsIHByb3BlcnR5SWQpIHtcclxuICAgICAgICBjb25zdCBjdXJyTVBNRmllbGQ6IE1QTUZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgcHJvcGVydHlJZCwgdGhpcy5jYXRlZ29yeUxldmVsKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRQcm9wZXJ0eShjdXJyTVBNRmllbGQsIHByb2plY3REYXRhKTtcclxuICAgIH1cclxuICAgIGNvbnZlclRvTG9jYWxEYXRlKGRhdGEsIHByb3BlcnR5SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKHRoaXMucHJvamVjdFNlcnZpY2UuY29udmVyVG9Mb2NhbERhdGUoZGF0YSwgcHJvcGVydHlJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByaW9yaXR5KHByb2plY3REYXRhOiBhbnksIGRpc3BsYXlDb2x1bTogTVBNRmllbGQpIHtcclxuICAgICAgICBjb25zdCBwcmlvcml0eU9iaiA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0UHJpb3JpdHlJY29uKHRoaXMuZ2V0UHJvcGVydHkoZGlzcGxheUNvbHVtLCBwcm9qZWN0RGF0YSksXHJcbiAgICAgICAgICAgIE1QTV9MRVZFTFMuUFJPSkVDVCk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBwcmlvcml0eU9iai5jb2xvcixcclxuICAgICAgICAgICAgaWNvbjogcHJpb3JpdHlPYmouaWNvbixcclxuICAgICAgICAgICAgdG9vbHRpcDogcHJpb3JpdHlPYmoudG9vbHRpcFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdFByb2plY3QoZXZlbnQsIHByb2plY3QsaXNDb3B5VGVtcGxhdGU6Ym9vbGVhbikge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RDb25maWcuSVNfUFJPSkVDVF9UWVBFKSB7XHJcbiAgICAgICAgICAgIHByb2plY3QuaXNQcm9qZWN0VHlwZSA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJvamVjdC5pc1Byb2plY3RUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgfSBcclxuICAgICAgICBwcm9qZWN0LmlzQ29weVRlbXBsYXRlID0gaXNDb3B5VGVtcGxhdGUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5jb3B5UHJvamVjdC5uZXh0KHByb2plY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5NaWxlc3RvbmUoZXZlbnQsIHByb2plY3QpIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBpZiAodGhpcy5wcm9qZWN0Q29uZmlnLklTX1BST0pFQ1RfVFlQRSkge1xyXG4gICAgICAgICAgICBwcm9qZWN0LmlzUHJvamVjdFR5cGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHByb2plY3QuaXNQcm9qZWN0VHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9wZW5NaWxlc3RvbmVEaWFsb2cubmV4dChwcm9qZWN0KTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0VGFzayhldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLmVkaXRUYXNrSGFuZGxlci5lbWl0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNvbW1lbnQoZXZlbnQsIHByb2plY3REZXRhaWxzKSB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdElkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICwgcHJvamVjdERldGFpbHMpO1xyXG4gICAgICAgIHRoaXMub3BlbkNvbW1lbnRzTW9kdWxlLmVtaXQocHJvamVjdElkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxBY3RpdmVUYXNrc0ZvclByb2plY3QocHJvamVjdDogYW55LCBpc1VwZGF0ZT86IGJvb2xlYW4pIHtcclxuICAgICAgICBpZiAoIXByb2plY3QudGFza0xpc3QgfHwgaXNVcGRhdGUpIHtcclxuICAgICAgICAgICAgcHJvamVjdC50YXNrTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICBwcm9qZWN0LnRhc2tUb3RhbENvdW50ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0U3RhdHVzVHlwZSA9IHByb2plY3QuUFJPSkVDVF9TVEFUVVNfVFlQRTtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0Q2FuY2VsbGVkU3RhdHVzID0gdGhpcy5wcm9qZWN0U3RhdHVzVHlwZSA9PT0gUHJvamVjdENvbnN0YW50LlNUQVRVU19UWVBFX0ZJTkFMX0NBTkNFTExFRCA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5pc09uSG9sZFByb2plY3QgPSBwcm9qZWN0LklTX09OX0hPTEQgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0Q29uZmlnLmN1cnJlbnRQcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JRCwgdGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAgICAgICAgLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0Q29uZmlnLmN1cnJlbnRQcm9qZWN0SXRlbUlkID0gdGhpcy5nZXRQcm9wZXJ0eShcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCwgdGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAgICAgICAgLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHByb2plY3RJZDogdGhpcy5wcm9qZWN0Q29uZmlnLmN1cnJlbnRQcm9qZWN0SXRlbUlkLFxyXG4gICAgICAgICAgICAgICAgc2tpcDogMCxcclxuICAgICAgICAgICAgICAgIHRvcDogMTAwXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmdldEFsbEFjdGl2ZVRhc2tzKHBhcmFtZXRlcnMsIHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0Vmlld05hbWUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKChyZXNwb25zZTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHByb2plY3QudGFza0xpc3QgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIHByb2plY3QudGFza1RvdGFsQ291bnQgPSByZXNwb25zZS5jdXJzb3IudG90YWxfcmVjb3JkcztcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBhY3RpdmUgdGFza3MgZm9yIHByb2plY3QnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoVGFzayhwcm9qZWN0OiBhbnkpIHtcclxuICAgICAgICB0aGlzLmdldEFsbEFjdGl2ZVRhc2tzRm9yUHJvamVjdChwcm9qZWN0LCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuVGFza0RldGFpbHMoZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy52aWV3VGFza0RldGFpbHMubmV4dChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4cGFuZFJvdyhldmVudCwgcm93KSB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgaWYgKHJvdy5pc0V4cGFuZGVkKSB7XHJcbiAgICAgICAgICAgIHJvdy5pc0V4cGFuZGVkID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcm93LmlzRXhwYW5kZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmdldEFsbEFjdGl2ZVRhc2tzRm9yUHJvamVjdChyb3cpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcGVuUHJvamVjdERldGFpbHMocHJvamVjdCwgY29sdW1uTmFtZSwgZW5hYmxlQ2FtcGFpZ24sIGlzQ2FtcGFpZ25NYW5hZ2VyLCBpc1BtVmlldykge1xyXG4gICAgICAgIGlmIChjb2x1bW5OYW1lID09PSBNUE1GaWVsZENvbnN0YW50cy5NUE1fQ0FNUEFJR05fRkVJTERTLkNBTVBBSUdOX0lEICYmIGVuYWJsZUNhbXBhaWduICYmIGlzQ2FtcGFpZ25NYW5hZ2VyICYmIGlzUG1WaWV3KSB7LyogaXNDYW1wYWlnbk1hbmFnZXIgKi9cclxuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5DQU1QQUlHTjtcclxuICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25JZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fQ0FNUEFJR05fRkVJTERTLkNBTVBBSUdOX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBpZiAoY2FtcGFpZ25JZCAmJiBjYW1wYWlnbklkICE9PSAnTkEnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5DYW1wYWlnbkluUHJvamVjdExpc3QuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQoY2FtcGFpZ25JZC5zcGxpdCgnLicpLmxlbmd0aCA+IDEgPyBjYW1wYWlnbklkLnNwbGl0KCcuJylbMV0gOiBjYW1wYWlnbklkLnNwbGl0KCcuJylbMF0pO1xyXG4gICAgICAgICAgICB9IC8qIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdJbnZhbGlkIGNhbXBhaWduIElkJyk7XHJcbiAgICAgICAgICAgIH0gKi9cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSBpZiAodGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkKSB7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuY2F0ZWdvcnlMZXZlbCA9IE1QTV9MRVZFTFMuQ0FNUEFJR047XHJcbiAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX0NBTVBBSUdOX0ZFSUxEUy5DQU1QQUlHTl9JVEVNX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBpZiAoY2FtcGFpZ25JZCAmJiBjYW1wYWlnbklkICE9PSAnTkEnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgLyogaWYgKCF0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5DYW1wYWlnbkluUHJvamVjdExpc3QuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH0gKi9cclxuICAgICAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQoY2FtcGFpZ25JZC5zcGxpdCgnLicpWzFdKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignSW52YWxpZCBjYW1wYWlnbiBJZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5QUk9KRUNUO1xyXG4gICAgICAgICAgICBjb25zdCBwcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCwgdGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAgICAgICAgLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgaWYgKHByb2plY3RJZCAmJiBwcm9qZWN0SWQgIT09ICdOQScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdQcm9qZWN0RGV0YWlscy5lbWl0KHByb2plY3RJZC5zcGxpdCgnLicpWzFdKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignSW52YWxpZCBwcm9qZWN0IElkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qIGlmICh0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduSWQgPSAgdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9DQU1QQUlHTl9GRUlMRFMuQ0FNUEFJR05fSVRFTV9JRCwgdGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAgICAgICAgLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgaWYgKGNhbXBhaWduSWQgJiYgY2FtcGFpZ25JZCAhPT0gJ05BJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQoY2FtcGFpZ25JZC5zcGxpdCgnLicpWzFdKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignSW52YWxpZCBjYW1wYWlnbiBJZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5QUk9KRUNUO1xyXG4gICAgICAgICAgICBjb25zdCBwcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCwgdGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAgICAgICAgLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgaWYgKHByb2plY3RJZCAmJiBwcm9qZWN0SWQgIT09ICdOQScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdQcm9qZWN0RGV0YWlscy5lbWl0KHByb2plY3RJZC5zcGxpdCgnLicpWzFdKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignSW52YWxpZCBwcm9qZWN0IElkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9ICovXHJcbiAgICB9XHJcbiAgICBcclxuICAgIHJlZnJlc2hEYXRhKCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0Vmlld0NvbmZpZyA/XHJcbiAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuZ2V0RGlzcGxheU9yZGVyRGF0YSh0aGlzLmRpc3BsYXlhYmxlRmllbGRzLCB0aGlzLnByb2plY3RDb25maWcucHJvamVjdFZpZXdDb25maWcubGlzdEZpZWxkT3JkZXIpIDogW107XHJcbiAgICAgICAgLy8gdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gdGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3RWaWV3Q29uZmlnID9cclxuICAgICAgICAvLyAgICAgdGhpcy51dGlsU2VydmljZS5nZXREaXNwbGF5T3JkZXJEYXRhKHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0Vmlld0NvbmZpZy5SX1BNX0xJU1RfVklFV19NUE1fRklFTERTLCB0aGlzLnByb2plY3RDb25maWcucHJvamVjdFZpZXdDb25maWcuTElTVF9GSUVMRF9PUkRFUikgOiBbXTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLm1hcCgoY29sdW1uOiBNUE1GaWVsZCkgPT4gY29sdW1uLklOREVYRVJfRklFTERfSUQpO1xyXG4gICAgICAgIHRoaXMuaXNTZWxlY3QgPSB0aGlzLmlzQ2FtcGFpZ24gPyAhdGhpcy5pc0NhbXBhaWduIDogKCF0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgfHwgIXRoaXMucHJvamVjdENvbmZpZy5JU19DQU1QQUlHTl9UWVBFKTtcclxuICAgICAgICAvKiBpZiAoKHRoaXMuaXNDYW1wYWlnbiAmJiAhdGhpcy5pc0NhbXBhaWduKSB8fCAodGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkICYmICF0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgfHwgIXRoaXMucHJvamVjdENvbmZpZy5JU19DQU1QQUlHTl9UWVBFKSkge1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGlmICh0aGlzLmlzU2VsZWN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMudW5zaGlmdCgnU2VsZWN0Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RDb25maWcuSVNfUFJPSkVDVF9UWVBFKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMudW5zaGlmdCgnRXhwYW5kJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vQ29weSB0ZW1wbGF0ZSB0aWNrZXQtc2hyZWUgdGhhcmFuaVxyXG4gICAgICAgIC8qaWYgKHRoaXMuc2hvd0NvcHlQcm9qZWN0IHx8IHRoaXMucHJvamVjdENvbmZpZy5JU19QUk9KRUNUX1RZUEUpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy5wdXNoKCdBY3Rpb25zJyk7XHJcbiAgICAgICAgfSovIFxyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMucHVzaCgnQWN0aW9ucycpO1xyXG4gICAgICAgIHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEgPSBbXTtcclxuICAgICAgICB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5kYXRhID0gdGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3REYXRhO1xyXG4gICAgICAgIHRoaXMuc29ydC5hY3RpdmUgPSB0aGlzLnByb2plY3RDb25maWcuc2VsZWN0ZWRTb3J0T3B0aW9uLm9wdGlvbi5uYW1lO1xyXG4gICAgICAgIHRoaXMuc29ydC5kaXJlY3Rpb24gPSB0aGlzLnByb2plY3RDb25maWcuc2VsZWN0ZWRTb3J0T3B0aW9uLnNvcnRUeXBlLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2Uuc29ydCA9IHRoaXMuc29ydDtcclxuICAgIH1cclxuXHJcbiAgICB0YWJsZURyb3AoZXZlbnQ6IENka0RyYWdEcm9wPHN0cmluZ1tdPikge1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgaWYgKHRoaXMuZGlzcGxheUNvbHVtbnMuZmluZChjb2x1bW4gPT4gY29sdW1uID09PSAnU2VsZWN0JykpIHtcclxuICAgICAgICAgICAgY291bnQgPSBjb3VudCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRpc3BsYXlDb2x1bW5zLmZpbmQoY29sdW1uID0+IGNvbHVtbiA9PT0gJ0V4cGFuZCcpKSB7XHJcbiAgICAgICAgICAgIGNvdW50ID0gY291bnQgKyAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBtb3ZlSXRlbUluQXJyYXkodGhpcy5kaXNwbGF5Q29sdW1ucywgZXZlbnQucHJldmlvdXNJbmRleCArIGNvdW50LCBldmVudC5jdXJyZW50SW5kZXggKyBjb3VudCk7XHJcbiAgICAgICAgY29uc3Qgb3JkZXJlZEZpZWxkcyA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICAgICAgICBpZiAoIShjb2x1bW4gPT09ICdTZWxlY3QnIHx8IGNvbHVtbiA9PT0gJ0V4cGFuZCcgfHwgY29sdW1uID09PSAnQWN0aW9ucycpKSB7XHJcbiAgICAgICAgICAgICAgICBvcmRlcmVkRmllbGRzLnB1c2godGhpcy5kaXNwbGF5YWJsZUZpZWxkcy5maW5kKGRpc3BsYXlhYmxlRmllbGQgPT4gZGlzcGxheWFibGVGaWVsZC5JTkRFWEVSX0ZJRUxEX0lEID09PSBjb2x1bW4pKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3JkZXJlZERpc3BsYXlhYmxlRmllbGRzLm5leHQob3JkZXJlZEZpZWxkcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNpemVNb3VzZURvd24oZXZlbnQsIGNvbHVtbikge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3Qgc3RhcnQgPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgY29uc3QgcHJlc3NlZCA9IHRydWU7XHJcbiAgICAgICAgY29uc3Qgc3RhcnRYID0gZXZlbnQueDtcclxuICAgICAgICBjb25zdCBzdGFydFdpZHRoID0gJChzdGFydCkucGFyZW50KCkud2lkdGgoKTtcclxuICAgICAgICB0aGlzLmluaXRSZXNpemFibGVDb2x1bW5zKHN0YXJ0LCBwcmVzc2VkLCBzdGFydFgsIHN0YXJ0V2lkdGgsIGNvbHVtbik7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKSB7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2Vtb3ZlJywgKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB3aWR0aCA9IHN0YXJ0V2lkdGggKyAoZXZlbnQueCAtIHN0YXJ0WCk7XHJcbiAgICAgICAgICAgICAgICBjb2x1bW4ud2lkdGggPSB3aWR0aDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIubGlzdGVuKCdib2R5JywgJ21vdXNldXAnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgIHByZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzaXplRGlzcGxheWFibGVGaWVsZHMubmV4dCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlUHJvamVjdChldmVudCwgcm93KSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHJvdyk7XHJcbiAgICAgICAgbGV0IG1zZztcclxuICAgICAgICB0aGlzLmFmZmVjdGVkVGFzayA9IFtdO1xyXG4gICAgICAgIGxldCB0YXNrcztcclxuICAgICAgICBsZXQgaXNDdXN0b21Xb3JrZmxvdyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHJvdy5JU19DVVNUT01fV09SS0ZMT1cpO1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSAgdGhpcy5pc1RlbXBsYXRlID8gJ1RlbXBsYXRlJyA6ICdQcm9qZWN0JztcclxuICAgICAgICBpZiAoaXNDdXN0b21Xb3JrZmxvdykge1xyXG4gICAgICAgICAgICBtc2cgPSBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIFwiK25hbWUrXCIgYW5kIHJlY29uZmlndXJlIHRoZSB3b3JrZmxvdyBydWxlIGVuZ2luZT9cIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtc2cgPSBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIFwiK25hbWUrXCIgYW5kIEV4aXQ/XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0VGFza0J5UHJvamVjdElEKHJvdy5JRCkuc3Vic2NyaWJlKHByb2plY3RUYXNrUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2spO1xyXG4gICAgICAgICAgICBpZiAocHJvamVjdFRhc2tSZXNwb25zZSAmJiBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlICYmIHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzayAmJiAhQXJyYXkuaXNBcnJheShwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2spKSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrcyA9IFtwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2tdO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGFza3MgPSBwcm9qZWN0VGFza1Jlc3BvbnNlICYmIHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrID8gcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrIDogJyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRhc2tzICYmIHRhc2tzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRhc2tzLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZmZlY3RlZFRhc2sucHVzaCh0YXNrLk5BTUUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBtc2csXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdGhpcy5wcm9qZWN0Q29uZmlnLklTX1BST0pFQ1RfVFlQRSA/IHRoaXMuYWZmZWN0ZWRUYXNrIDogW10sXHJcbiAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmRlbGV0ZVByb2plY3Qocm93LklELCByb3cuSVRFTV9JRCwgcm93LlBST0pFQ1RfU1RBVFVTX0lEKS5zdWJzY3JpYmUoZGVsZXRlUHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MobmFtZSsnIGhhcyBiZWVuIGRlbGV0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2hFbWl0dGVyLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRGVsZXRlICcrbmFtZSsnIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGlmIChjaGFuZ2VzLmRpc3BsYXlhYmxlRmllbGRzICYmICFjaGFuZ2VzLmRpc3BsYXlhYmxlRmllbGRzLmZpcnN0Q2hhbmdlICYmXHJcbiAgICAgICAgICAgIChKU09OLnN0cmluZ2lmeShjaGFuZ2VzLmRpc3BsYXlhYmxlRmllbGRzLmN1cnJlbnRWYWx1ZSkgIT09IEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMucHJldmlvdXNWYWx1ZSkpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaERhdGEoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5wb3NpdGlvbi52YWx1ZSA9ICdiZWxvdyc7XHJcbiAgICAgICAgY29uc3QgdXNlcklkID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VySWQgPSArdXNlcklkO1xyXG4gICAgICAgIHRoaXMuZmluYWxDb21wbGV0ZWQgPSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQ7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UHJvamVjdENvbmZpZygpO1xyXG4gICAgICAgIGNvbnN0IGFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5zaG93Q29weVByb2plY3QgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5FTkFCTEVfQ09QWV9QUk9KRUNUXSlcclxuICAgICAgICB0aGlzLmVuYWJsZUNhbXBhaWduID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUoYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVOQUJMRV9DQU1QQUlHTl0pO1xyXG4gICAgICAgIHRoaXMuaXNDYW1wYWlnbk1hbmFnZXIgPSAodGhpcy5zaGFyaW5nU2VydmljZS5nZXRDYW1wYWlnblJvbGUoKSAmJiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhbXBhaWduUm9sZSgpLmxlbmd0aCA+IDApID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuSXNQTVZpZXcgPSAodGhpcy5wcm9qZWN0VHlwZSA9PT0gUHJvamVjdFR5cGVzLkNBTVBBSUdOIHx8IHRoaXMucm91dGVyLnVybC5pbmNsdWRlcygnQ2FtcGFpZ25zJykgfHwgdGhpcy5yb3V0ZXIudXJsLmluY2x1ZGVzKCdjYW1wYWlnbicpKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB0aGlzLmlzVGVtcGxhdGUgPSB0aGlzLnByb2plY3RUeXBlID09PSBQcm9qZWN0VHlwZXMuVEVNUExBVEUgfHwgdGhpcy5yb3V0ZXIudXJsLmluY2x1ZGVzKCdUZW1wbGF0ZXMnKSAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMucm91dGVyLnVybC5pbmNsdWRlcygnQ2FtcGFpZ25zJykgfHwgdGhpcy5wcm9qZWN0Q29uZmlnLklTX0NBTVBBSUdOX1RZUEUpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5DQU1QQUlHTjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdENvbmZpZy5tZXRhRGF0YUZpZWxkcyAmJiB0aGlzLnByb2plY3RDb25maWcucHJvamVjdERhdGEpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0RGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RDb25maWcucHJvamVjdERhdGEgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF0aGlzLnByb2plY3RDb25maWcubWV0YURhdGFGaWVsZHMpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0Q29uZmlnLm1ldGFEYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnNvcnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LmRpc2FibGVDbGVhciA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zb3J0LmFjdGl2ZSAmJiB0aGlzLnNvcnQuZGlyZWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRTb3J0TGlzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydFR5cGU6IHRoaXMuc29ydC5kaXJlY3Rpb24udG9VcHBlckNhc2UoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLnNvcnQuYWN0aXZlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc29ydENoYW5nZS5lbWl0KHNlbGVjdGVkU29ydExpc3QpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qICBpZiAodGhpcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMpIHtcclxuICAgICAgICAgICAgIHRoaXMuc2VsZWN0QWxsRGVsaXZlcmFibGVzLnN1YnNjcmliZSgoZGF0YTogYm9vbGVhbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tEZWxpdmVyYWJsZSh0aGlzLmFzc2V0KTtcclxuICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICB9ICovXHJcbiAgICB9XHJcbn0iXX0=