import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { SortConstants } from '../../shared/constants/sort.constants';
let ProjectSortComponent = class ProjectSortComponent {
    constructor(sharingService) {
        this.sharingService = sharingService;
        this.sortChange = new EventEmitter();
    }
    selectSorting() {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.sortChange.next(this.selectedOption);
    }
    onSelectionChange(event, fields) {
        if (event && event.currentTarget.value) {
            fields.map(option => {
                if (option.name === event.currentTarget.value) {
                    this.selectedOption.option = option;
                }
            });
        }
        this.sortChange.next(this.selectedOption);
    }
    ngOnInit() {
        const appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] };
            }
            else {
                // this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] };
            }
        }
    }
};
ProjectSortComponent.ctorParameters = () => [
    { type: SharingService }
];
__decorate([
    Input()
], ProjectSortComponent.prototype, "sortOptions", void 0);
__decorate([
    Input()
], ProjectSortComponent.prototype, "selectedOption", void 0);
__decorate([
    Output()
], ProjectSortComponent.prototype, "sortChange", void 0);
ProjectSortComponent = __decorate([
    Component({
        selector: 'mpm-project-sort',
        template: "<button class=\"sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption && selectedOption.option && selectedOption.option.displayName ? selectedOption.option.displayName :''}}\"\r\n    [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n    <span>Sort by: {{selectedOption && selectedOption.option && selectedOption.option.displayName ? selectedOption.option.displayName :''}}</span>\r\n    <mat-icon>arrow_drop_down</mat-icon>\r\n</button>\r\n<button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n    mat-icon-button>\r\n    <mat-icon *ngIf=\"selectedOption.sortType === 'desc'\">arrow_downward</mat-icon>\r\n    <mat-icon *ngIf=\"selectedOption.sortType === 'asc'\">arrow_upward</mat-icon>\r\n</button>\r\n<mat-menu #sortOrderListMenu=\"matMenu\">\r\n    <span *ngFor=\"let fieldOption of sortOptions\">\r\n        <button *ngIf=\"selectedOption.option.name !== fieldOption.name\" mat-menu-item\r\n            matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n            [value]=\"fieldOption.name\">\r\n            <span>{{fieldOption.displayName}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
        styles: [""]
    })
], ProjectSortComponent);
export { ProjectSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1zb3J0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LXNvcnQvcHJvamVjdC1zb3J0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBUXRFLElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBTTdCLFlBQ1csY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBSC9CLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBSTNDLENBQUM7SUFFTCxhQUFhO1FBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsS0FBSyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3JHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1FBQzVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLE1BQU07UUFDM0IsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7WUFDcEMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDaEIsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFO29CQUMzQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7aUJBQ3ZDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsUUFBUTtRQUNKLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQix5RUFBeUU7Z0JBQ3pFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7YUFDNUk7aUJBQU07Z0JBQ0gseURBQXlEO2dCQUN6RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7YUFDM0g7U0FDSjtJQUNMLENBQUM7Q0FDSixDQUFBOztZQWhDOEIsY0FBYzs7QUFMaEM7SUFBUixLQUFLLEVBQUU7eURBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTs0REFBZ0I7QUFDZDtJQUFULE1BQU0sRUFBRTt3REFBc0M7QUFKdEMsb0JBQW9CO0lBTmhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsMnRDQUE0Qzs7S0FFL0MsQ0FBQztHQUVXLG9CQUFvQixDQXVDaEM7U0F2Q1ksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNvcnRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3NvcnQuY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tcHJvamVjdC1zb3J0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wcm9qZWN0LXNvcnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcHJvamVjdC1zb3J0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9qZWN0U29ydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgc29ydE9wdGlvbnM7XHJcbiAgICBASW5wdXQoKSBzZWxlY3RlZE9wdGlvbjtcclxuICAgIEBPdXRwdXQoKSBzb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBzZWxlY3RTb3J0aW5nKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPSAodGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9PT0gU29ydENvbnN0YW50cy5BU0MpID8gJ2Rlc2MnIDogJ2FzYyc7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9IHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGU7XHJcbiAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLm5leHQodGhpcy5zZWxlY3RlZE9wdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgb25TZWxlY3Rpb25DaGFuZ2UoZXZlbnQsIGZpZWxkcykge1xyXG4gICAgICAgIGlmIChldmVudCAmJiBldmVudC5jdXJyZW50VGFyZ2V0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGZpZWxkcy5tYXAob3B0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChvcHRpb24ubmFtZSA9PT0gZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24ub3B0aW9uID0gb3B0aW9uO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLm5leHQodGhpcy5zZWxlY3RlZE9wdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc29ydE9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6IGFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiBhcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==