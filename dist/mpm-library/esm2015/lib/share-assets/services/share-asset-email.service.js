import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
let ShareAssetEmailService = class ShareAssetEmailService {
    constructor(appService) {
        this.appService = appService;
    }
    sendShareAssetEmail(performedUserName, deliverableId, assetName, sharedToMailAddress) {
        const param = {
            performedUserName,
            deliverables: {
                deliverable: {
                    id: deliverableId
                }
            },
            assetName,
            sharedToMailAddress,
            assetVersion: ''
        };
        this.appService.invokeRequest('http://schemas.acheron.com/mpm/email/bpm/1.0', 'ShareAssetEmailHandler', param).subscribe(data => '');
    }
};
ShareAssetEmailService.ctorParameters = () => [
    { type: AppService }
];
ShareAssetEmailService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ShareAssetEmailService_Factory() { return new ShareAssetEmailService(i0.ɵɵinject(i1.AppService)); }, token: ShareAssetEmailService, providedIn: "root" });
ShareAssetEmailService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ShareAssetEmailService);
export { ShareAssetEmailService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXQtZW1haWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zZXJ2aWNlcy9zaGFyZS1hc3NldC1lbWFpbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQzs7O0FBS2xFLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBRWpDLFlBQ1MsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMzQixDQUFDO0lBRUwsbUJBQW1CLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxtQkFBbUI7UUFDbEYsTUFBTSxLQUFLLEdBQUc7WUFDWixpQkFBaUI7WUFDakIsWUFBWSxFQUFFO2dCQUNaLFdBQVcsRUFBRTtvQkFDWCxFQUFFLEVBQUUsYUFBYTtpQkFDbEI7YUFDRjtZQUNELFNBQVM7WUFDVCxtQkFBbUI7WUFDbkIsWUFBWSxFQUFFLEVBQUU7U0FDakIsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLDhDQUE4QyxFQUFFLHdCQUF3QixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZJLENBQUM7Q0FDRixDQUFBOztZQWpCc0IsVUFBVTs7O0FBSHBCLHNCQUFzQjtJQUhsQyxVQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csc0JBQXNCLENBb0JsQztTQXBCWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2hhcmVBc3NldEVtYWlsU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBzZW5kU2hhcmVBc3NldEVtYWlsKHBlcmZvcm1lZFVzZXJOYW1lLCBkZWxpdmVyYWJsZUlkLCBhc3NldE5hbWUsIHNoYXJlZFRvTWFpbEFkZHJlc3MpIHtcclxuICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICBwZXJmb3JtZWRVc2VyTmFtZSxcclxuICAgICAgZGVsaXZlcmFibGVzOiB7XHJcbiAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgIGlkOiBkZWxpdmVyYWJsZUlkXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBhc3NldE5hbWUsXHJcbiAgICAgIHNoYXJlZFRvTWFpbEFkZHJlc3MsXHJcbiAgICAgIGFzc2V0VmVyc2lvbjogJydcclxuICAgIH07XHJcbiAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCgnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2VtYWlsL2JwbS8xLjAnLCAnU2hhcmVBc3NldEVtYWlsSGFuZGxlcicsIHBhcmFtKS5zdWJzY3JpYmUoZGF0YSA9PiAnJyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==