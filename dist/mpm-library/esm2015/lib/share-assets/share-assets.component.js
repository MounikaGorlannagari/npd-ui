import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { ShareAssetService } from './share-asset.service';
import { NotificationService } from '../notification/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../shared/components/confirmation-modal/confirmation-modal.component';
import { RequiredValidator } from '../shared/validators/required.validator';
import { EmailValidator } from '../shared/validators/email.validator';
import { LoaderService } from '../loader/loader.service';
import { ShareAssetEmailService } from './services/share-asset-email.service';
import { ProjectService } from '../project/shared/services/project.service';
import { DeliverableConstants } from '../project/tasks/deliverable/deliverable.constants';
let ShareAssetsComponent = class ShareAssetsComponent {
    constructor(fb, dialog, sharingService, shareAssetService, notificationService, loaderService, shareAssetEmailService, projectService) {
        this.fb = fb;
        this.dialog = dialog;
        this.sharingService = sharingService;
        this.shareAssetService = shareAssetService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.shareAssetEmailService = shareAssetEmailService;
        this.projectService = projectService;
        this.closeModalHandler = new EventEmitter();
        this.sharingOptions = [{
                name: 'Email with attachment',
                value: 'SEND_FILES',
                isDefault: true,
                emailSubject: 'Content sent from Media Manager via MPM.',
                emailMessage: 'Please find the attached content.'
            }, {
                name: 'Email with link to asset',
                value: 'SEND_LINK',
                isDefault: false,
                emailSubject: 'Content sent from Media Manager via MPM.',
                emailMessage: 'I am sending you assets from OpenText Media Manager via MPM. Please click the above link to access these in the Media Manager application.'
            }];
        this.required = true;
    }
    onFieldChange(event) {
        this.shareAssetForm.updateValueAndValidity();
    }
    closeDialog() {
        if (this.shareAssetForm.pristine) {
            this.closeModalHandler.emit(true);
        }
        else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '25%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    this.closeModalHandler.emit(true);
                }
            });
        }
    }
    getProperty(projectData, propertyId) {
        return this.projectService.getProperty(projectData, propertyId);
    }
    shareAsset() {
        this.shareAssetForm.get('to').markAsTouched();
        this.shareAssetForm.get('from').markAsTouched();
        this.shareAssetForm.get('cc').markAsTouched();
        this.shareAssetForm.get('replyTo').markAsTouched();
        if (this.shareAssetForm.valid) {
            this.loaderService.show();
            this.shareAssetService.shareAsset(this.assets, this.shareAssetForm.getRawValue())
                .subscribe(response => {
                if (response) {
                    if (this.assets.length === 1) {
                        const toAddress = this.shareAssetForm.get('to').value.join(', ');
                        const deliverableId = this.getProperty(this.assets[0], DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
                        const assetName = this.assets[0].name;
                        this.shareAssetEmailService.sendShareAssetEmail(this.sharingService.getCurrentUserDisplayName(), deliverableId, assetName, toAddress);
                    }
                    this.loaderService.hide();
                    this.notificationService.success('Asset(s) shared successfully');
                    this.closeModalHandler.emit(true);
                }
                else {
                    this.loaderService.hide();
                    this.notificationService.error('Something went wrong while sharing assets');
                    this.closeModalHandler.emit(true);
                }
            }, error => {
                this.loaderService.hide();
                this.notificationService.error('Something went wrong while sharing assets');
                this.closeModalHandler.emit(true);
            });
        }
    }
    intializeForm() {
        this.shareAssetForm = this.fb.group({
            sharingMethod: new FormControl(this.sharingOptions[0], [Validators.required]),
            to: new FormControl([], [Validators.required, RequiredValidator.validateRequired, EmailValidator.validateEmails]),
            from: new FormControl([this.sharingService.getCurrentUserEmailId()], [Validators.required, RequiredValidator.validateRequired, EmailValidator.validateEmails]),
            cc: new FormControl([], [EmailValidator.validateEmails]),
            replyTo: new FormControl([this.sharingService.getCurrentUserEmailId()], [EmailValidator.validateEmails]),
            subject: new FormControl(this.sharingOptions[0].emailSubject, [Validators.required]),
            message: new FormControl(this.sharingOptions[0].emailMessage),
            compressFiles: new FormControl(true)
        });
        this.shareAssetForm.get('from').disable();
        this.shareAssetForm.get('sharingMethod').valueChanges.subscribe(value => {
            this.shareAssetForm.get('subject').patchValue(this.shareAssetForm.get('sharingMethod').value.emailSubject);
            this.shareAssetForm.get('message').patchValue(this.shareAssetForm.get('sharingMethod').value.emailMessage);
            this.shareAssetForm.updateValueAndValidity();
        });
    }
    ngOnInit() {
        this.intializeForm();
    }
};
ShareAssetsComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: MatDialog },
    { type: SharingService },
    { type: ShareAssetService },
    { type: NotificationService },
    { type: LoaderService },
    { type: ShareAssetEmailService },
    { type: ProjectService }
];
__decorate([
    Input()
], ShareAssetsComponent.prototype, "assets", void 0);
__decorate([
    Output()
], ShareAssetsComponent.prototype, "closeModalHandler", void 0);
ShareAssetsComponent = __decorate([
    Component({
        selector: 'mpm-share-assets',
        template: "<div class=\"share-asset-container\">\r\n    <form class=\"share-asset-form\" *ngIf=\"shareAssetForm\" [formGroup]=\"shareAssetForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item sharing-method-radio-group full-width\">\r\n                <label id=\"sharing-option-label\">Sharing method: </label>\r\n                <mat-radio-group aria-labelledby=\"sharing-option-label\" formControlName=\"sharingMethod\">\r\n                    <mat-radio-button class=\"sharing-method-radio-btn\" color=\"primary\"\r\n                        *ngFor=\"let sharingOption of sharingOptions\" [value]=\"sharingOption\"\r\n                        [checked]=\"sharingOption.isDefault\">\r\n                        {{sharingOption.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'To'\" [emailFieldFormControl]=\"shareAssetForm.get('to')\"\r\n            (valueChanges)=\"onFieldChange($event)\" [required]=\"required\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'From'\" [emailFieldFormControl]=\"shareAssetForm.get('from')\"\r\n            (valueChanges)=\"onFieldChange($event)\" [required]=\"required\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'CC'\" [emailFieldFormControl]=\"shareAssetForm.get('cc')\"\r\n            (valueChanges)=\"onFieldChange($event)\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'Reply To'\"\r\n            [emailFieldFormControl]=\"shareAssetForm.get('replyTo')\" (valueChanges)=\"onFieldChange($event)\">\r\n        </mpm-custom-email-field>\r\n\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" class=\"full-width\">\r\n                    <mat-label>Subject</mat-label>\r\n                    <input matInput formControlName=\"subject\" required>\r\n                    <mat-error\r\n                        *ngIf=\"shareAssetForm.get('subject').errors && shareAssetForm.get('subject').errors.required\">\r\n                        Email subject is required.\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" class=\"full-width\">\r\n                    <mat-label>Message</mat-label>\r\n                    <textarea matInput formControlName=\"message\" rows=\"3\"></textarea>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-checkbox color=\"primary\" formControlName=\"compressFiles\" class=\"full-width\"\r\n                    [ngClass]=\"shareAssetForm.get('sharingMethod').value.value === 'SEND_FILES' ? 'visible' : 'hidden'\">\r\n                    <span>Compress the assets into one file?</span>\r\n                </mat-checkbox>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"form-actions\">\r\n                    <button mat-stroked-button type=\"button\" (click)=\"closeDialog()\">Cancel</button>\r\n                    <button color='primary' mat-flat-button type=\"button\" (click)=\"shareAsset()\"\r\n                        [disabled]=\"(shareAssetForm && shareAssetForm.invalid) || (shareAssetForm && shareAssetForm.pristine) \">Share</button>\r\n                </span>\r\n            </div>\r\n        </div>\r\n\r\n    </form>\r\n</div>",
        styles: [".full-width{width:100%;padding:0 8px}.share-asset-container{padding:8px;font-size:14px}.share-asset-container .share-asset-form{display:flex;flex-direction:column;flex-wrap:wrap}.share-asset-container .share-asset-form .sharing-method-radio-group{align-items:center;padding-bottom:16px}.share-asset-container .share-asset-form .sharing-method-radio-group .sharing-method-radio-btn{padding:0 8px}.share-asset-container .share-asset-form .form-actions{padding:0 16px;margin-left:auto}.share-asset-container .share-asset-form .form-actions button{margin-left:16px}.share-asset-container .share-asset-form :ng-deep mpm-custom-email-field .mat-form-field-wrapper{padding-bottom:0!important}.visible{visibility:visible}.hidden{visibility:hidden}"]
    })
], ShareAssetsComponent);
export { ShareAssetsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDM0UsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHNFQUFzRSxDQUFDO0FBQ2xILE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBUTFGLElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBc0I3QixZQUNXLEVBQWUsRUFDZixNQUFpQixFQUNqQixjQUE4QixFQUM5QixpQkFBb0MsRUFDcEMsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLHNCQUE4QyxFQUM5QyxjQUE4QjtRQVA5QixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBM0IvQixzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRXRELG1CQUFjLEdBQUcsQ0FBQztnQkFDZCxJQUFJLEVBQUUsdUJBQXVCO2dCQUM3QixLQUFLLEVBQUUsWUFBWTtnQkFDbkIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsWUFBWSxFQUFFLDBDQUEwQztnQkFDeEQsWUFBWSxFQUFFLG1DQUFtQzthQUNwRCxFQUFFO2dCQUNDLElBQUksRUFBRSwwQkFBMEI7Z0JBQ2hDLEtBQUssRUFBRSxXQUFXO2dCQUNsQixTQUFTLEVBQUUsS0FBSztnQkFDaEIsWUFBWSxFQUFFLDBDQUEwQztnQkFDeEQsWUFBWSxFQUFFLDRJQUE0STthQUM3SixDQUFDLENBQUM7UUFHSCxhQUFRLEdBQUcsSUFBSSxDQUFDO0lBV1osQ0FBQztJQUVMLGFBQWEsQ0FBQyxLQUFLO1FBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2pELENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRTtZQUM5QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3JDO2FBQU07WUFDSCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsaUNBQWlDO29CQUMxQyxZQUFZLEVBQUUsS0FBSztvQkFDbkIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtvQkFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVTtRQUMvQixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQsVUFBVTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRW5ELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDNUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsRUFBRTtvQkFDVixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDMUIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDakUsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLG9CQUFvQixDQUFDLG9DQUFvQyxDQUFDLENBQUM7d0JBQ2xILE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUN0QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMseUJBQXlCLEVBQUUsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3FCQUM3RjtvQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7b0JBQ2pFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3JDO3FCQUFNO29CQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztvQkFDNUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckM7WUFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO2dCQUM1RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7WUFDaEMsYUFBYSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0UsRUFBRSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ2pILElBQUksRUFBRSxJQUFJLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDOUosRUFBRSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RCxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN4RyxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEYsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO1lBQzdELGFBQWEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUM7U0FDdkMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNwRSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFM0csSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztDQUVKLENBQUE7O1lBbkdrQixXQUFXO1lBQ1AsU0FBUztZQUNELGNBQWM7WUFDWCxpQkFBaUI7WUFDZixtQkFBbUI7WUFDekIsYUFBYTtZQUNKLHNCQUFzQjtZQUM5QixjQUFjOztBQTVCaEM7SUFBUixLQUFLLEVBQUU7b0RBQW9CO0FBQ2xCO0lBQVQsTUFBTSxFQUFFOytEQUE2QztBQUg3QyxvQkFBb0I7SUFOaEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGtCQUFrQjtRQUM1Qiw2MEhBQTRDOztLQUUvQyxDQUFDO0dBRVcsb0JBQW9CLENBMEhoQztTQTFIWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIsIEZvcm1Db250cm9sLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyZUFzc2V0U2VydmljZSB9IGZyb20gJy4vc2hhcmUtYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVxdWlyZWRWYWxpZGF0b3IgfSBmcm9tICcuLi9zaGFyZWQvdmFsaWRhdG9ycy9yZXF1aXJlZC52YWxpZGF0b3InO1xyXG5pbXBvcnQgeyBFbWFpbFZhbGlkYXRvciB9IGZyb20gJy4uL3NoYXJlZC92YWxpZGF0b3JzL2VtYWlsLnZhbGlkYXRvcic7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyZUFzc2V0RW1haWxTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9zaGFyZS1hc3NldC1lbWFpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlQ29uc3RhbnRzIH0gZnJvbSAnLi4vcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5jb25zdGFudHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1zaGFyZS1hc3NldHMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NoYXJlLWFzc2V0cy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9zaGFyZS1hc3NldHMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNoYXJlQXNzZXRzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBhc3NldHM6IEFycmF5PGFueT47XHJcbiAgICBAT3V0cHV0KCkgY2xvc2VNb2RhbEhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBzaGFyaW5nT3B0aW9ucyA9IFt7XHJcbiAgICAgICAgbmFtZTogJ0VtYWlsIHdpdGggYXR0YWNobWVudCcsXHJcbiAgICAgICAgdmFsdWU6ICdTRU5EX0ZJTEVTJyxcclxuICAgICAgICBpc0RlZmF1bHQ6IHRydWUsXHJcbiAgICAgICAgZW1haWxTdWJqZWN0OiAnQ29udGVudCBzZW50IGZyb20gTWVkaWEgTWFuYWdlciB2aWEgTVBNLicsXHJcbiAgICAgICAgZW1haWxNZXNzYWdlOiAnUGxlYXNlIGZpbmQgdGhlIGF0dGFjaGVkIGNvbnRlbnQuJ1xyXG4gICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICdFbWFpbCB3aXRoIGxpbmsgdG8gYXNzZXQnLFxyXG4gICAgICAgIHZhbHVlOiAnU0VORF9MSU5LJyxcclxuICAgICAgICBpc0RlZmF1bHQ6IGZhbHNlLFxyXG4gICAgICAgIGVtYWlsU3ViamVjdDogJ0NvbnRlbnQgc2VudCBmcm9tIE1lZGlhIE1hbmFnZXIgdmlhIE1QTS4nLFxyXG4gICAgICAgIGVtYWlsTWVzc2FnZTogJ0kgYW0gc2VuZGluZyB5b3UgYXNzZXRzIGZyb20gT3BlblRleHQgTWVkaWEgTWFuYWdlciB2aWEgTVBNLiBQbGVhc2UgY2xpY2sgdGhlIGFib3ZlIGxpbmsgdG8gYWNjZXNzIHRoZXNlIGluIHRoZSBNZWRpYSBNYW5hZ2VyIGFwcGxpY2F0aW9uLidcclxuICAgIH1dO1xyXG5cclxuICAgIHNoYXJlQXNzZXRGb3JtOiBGb3JtR3JvdXA7XHJcbiAgICByZXF1aXJlZCA9IHRydWU7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmVBc3NldFNlcnZpY2U6IFNoYXJlQXNzZXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyZUFzc2V0RW1haWxTZXJ2aWNlOiBTaGFyZUFzc2V0RW1haWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgb25GaWVsZENoYW5nZShldmVudCkge1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlRGlhbG9nKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnNoYXJlQXNzZXRGb3JtLnByaXN0aW5lKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VNb2RhbEhhbmRsZXIuZW1pdCh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzI1JScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjbG9zZT8nLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VNb2RhbEhhbmRsZXIuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KHByb2plY3REYXRhLCBwcm9wZXJ0eUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvcGVydHkocHJvamVjdERhdGEsIHByb3BlcnR5SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNoYXJlQXNzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ3RvJykubWFya0FzVG91Y2hlZCgpO1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdmcm9tJykubWFya0FzVG91Y2hlZCgpO1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdjYycpLm1hcmtBc1RvdWNoZWQoKTtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgncmVwbHlUbycpLm1hcmtBc1RvdWNoZWQoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc2hhcmVBc3NldEZvcm0udmFsaWQpIHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgdGhpcy5zaGFyZUFzc2V0U2VydmljZS5zaGFyZUFzc2V0KHRoaXMuYXNzZXRzLCB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldFJhd1ZhbHVlKCkpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYXNzZXRzLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9BZGRyZXNzID0gdGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ3RvJykudmFsdWUuam9pbignLCAnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMuYXNzZXRzWzBdLCBEZWxpdmVyYWJsZUNvbnN0YW50cy5ERUxJVkVSQUJMRV9JVEVNX0lEX01FVEFEQVRBRklFTERfSUQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXNzZXROYW1lID0gdGhpcy5hc3NldHNbMF0ubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmVBc3NldEVtYWlsU2VydmljZS5zZW5kU2hhcmVBc3NldEVtYWlsKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJEaXNwbGF5TmFtZSgpLCBkZWxpdmVyYWJsZUlkLCBhc3NldE5hbWUsIHRvQWRkcmVzcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ0Fzc2V0KHMpIHNoYXJlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZU1vZGFsSGFuZGxlci5lbWl0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgc2hhcmluZyBhc3NldHMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZU1vZGFsSGFuZGxlci5lbWl0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgc2hhcmluZyBhc3NldHMnKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlTW9kYWxIYW5kbGVyLmVtaXQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW50aWFsaXplRm9ybSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgICAgICAgIHNoYXJpbmdNZXRob2Q6IG5ldyBGb3JtQ29udHJvbCh0aGlzLnNoYXJpbmdPcHRpb25zWzBdLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICB0bzogbmV3IEZvcm1Db250cm9sKFtdLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgUmVxdWlyZWRWYWxpZGF0b3IudmFsaWRhdGVSZXF1aXJlZCwgRW1haWxWYWxpZGF0b3IudmFsaWRhdGVFbWFpbHNdKSxcclxuICAgICAgICAgICAgZnJvbTogbmV3IEZvcm1Db250cm9sKFt0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyRW1haWxJZCgpXSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFJlcXVpcmVkVmFsaWRhdG9yLnZhbGlkYXRlUmVxdWlyZWQsIEVtYWlsVmFsaWRhdG9yLnZhbGlkYXRlRW1haWxzXSksXHJcbiAgICAgICAgICAgIGNjOiBuZXcgRm9ybUNvbnRyb2woW10sIFtFbWFpbFZhbGlkYXRvci52YWxpZGF0ZUVtYWlsc10pLFxyXG4gICAgICAgICAgICByZXBseVRvOiBuZXcgRm9ybUNvbnRyb2woW3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJFbWFpbElkKCldLCBbRW1haWxWYWxpZGF0b3IudmFsaWRhdGVFbWFpbHNdKSxcclxuICAgICAgICAgICAgc3ViamVjdDogbmV3IEZvcm1Db250cm9sKHRoaXMuc2hhcmluZ09wdGlvbnNbMF0uZW1haWxTdWJqZWN0LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICBtZXNzYWdlOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5zaGFyaW5nT3B0aW9uc1swXS5lbWFpbE1lc3NhZ2UpLFxyXG4gICAgICAgICAgICBjb21wcmVzc0ZpbGVzOiBuZXcgRm9ybUNvbnRyb2wodHJ1ZSlcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ2Zyb20nKS5kaXNhYmxlKCk7XHJcblxyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdzaGFyaW5nTWV0aG9kJykudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh2YWx1ZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdzdWJqZWN0JykucGF0Y2hWYWx1ZSh0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnc2hhcmluZ01ldGhvZCcpLnZhbHVlLmVtYWlsU3ViamVjdCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdtZXNzYWdlJykucGF0Y2hWYWx1ZSh0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnc2hhcmluZ01ldGhvZCcpLnZhbHVlLmVtYWlsTWVzc2FnZSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmludGlhbGl6ZUZvcm0oKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19