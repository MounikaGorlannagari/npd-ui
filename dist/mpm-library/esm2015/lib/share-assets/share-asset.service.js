import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { otmmServicesConstants } from '../mpm-utils/config/otmmService.constant';
import { OTMMService } from '../mpm-utils/services/otmm.service';
import { SessionStorageConstants } from '../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../mpm-utils/services/sharing.service";
import * as i3 from "../mpm-utils/services/otmm.service";
let ShareAssetService = class ShareAssetService {
    constructor(http, sharingService, otmmService) {
        this.http = http;
        this.sharingService = sharingService;
        this.otmmService = otmmService;
    }
    formDownloadName() {
        return 'Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + '.zip';
    }
    shareAsset(assets, sharingDetails) {
        const assetIds = [];
        const contentTypes = [];
        const contentTypeTransformerMap = [];
        const exportJobTransformers = [{
                transformer_id: 'ARTESIA.TRANSFORMER.ZIP COMPRESSION.DEFAULT',
                arguments: [
                    null,
                    this.formDownloadName()
                ]
            }];
        var otmmVersion = sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION) != undefined ? parseFloat(JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION))) : 0;
        const to = sharingDetails.to.join(';');
        const from = sharingDetails.from.join(';');
        const cc = sharingDetails.cc.join(';');
        const replyTo = sharingDetails.replyTo.join(';');
        if (otmmVersion == 0 || otmmVersion == null) {
            this.otmmService.getOTMMSystemDetails();
            otmmVersion = sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION) != undefined ? parseFloat(sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION)) : 0;
        }
        return new Observable(observer => {
            const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
            const version = this.sharingService.getMediaManagerConfig().apiVersion;
            const url = baseUrl + 'otmmapi/' + version + '/jobs/exports';
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED,
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            assets.forEach(asset => {
                assetIds.push(asset.asset_id);
                contentTypes.push(asset.content_type);
                contentTypeTransformerMap.push({
                    content_type: asset.content_type,
                    include_original: true,
                    include_preview: false,
                    include_sub_files: false,
                    trans_arguments: [],
                    exclusion_asset_id_list: []
                });
            });
            const selectionContext = {
                selection_context_param: {
                    selection_context: {
                        asset_ids: assetIds,
                        asssetContentType: contentTypes,
                        type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                        include_descendants: 'NONE'
                    }
                }
            };
            const exportRequest = {
                export_request_param: {
                    export_request: {
                        content_request: {
                            export_master_content: true,
                            export_preview_content: true
                        },
                        error_handling_action: 'DEFAULT_HANDLING',
                        export_job_transformers: (sharingDetails.sharingMethod.value === 'SEND_FILES' && sharingDetails.compressFiles) ? exportJobTransformers : [],
                        preserve_folder_hierarchy: false,
                        delivery_template: {
                            attribute_values: [{
                                    argument_number: 3,
                                    value: to
                                }, {
                                    argument_number: 4,
                                    value: from
                                }, {
                                    argument_number: 5,
                                    value: cc
                                }, {
                                    argument_number: 6,
                                    value: replyTo
                                }, {
                                    argument_number: otmmVersion > 22 ? 8 : 7,
                                    value: sharingDetails.subject
                                }, {
                                    argument_number: otmmVersion > 22 ? 9 : 8,
                                    value: sharingDetails.message
                                }],
                            id: sharingDetails.sharingMethod.value === 'SEND_FILES' ? 'ARTESIA.DELIVERY.EMAIL.DEFAULT' : 'ARTESIA.DELIVERY.EMAILURL.DEFAULT',
                            transformer_id: sharingDetails.sharingMethod.value === 'SEND_FILES' ? 'ARTESIA.TRANSFORMER.PROFILE.E-MAIL' : 'ARTESIA.TRANSFORMER.PROFILE.E-MAILURL'
                        },
                        write_xml: true,
                        write_csv: false
                    }
                }
            };
            const contentTypeToTransformerMap = {
                content_type_transformer_map_list: {
                    content_type_transformer_map: contentTypeTransformerMap
                }
            };
            const assetIdToTransformerMap = {
                asset_id_transformer_map_list: {
                    asset_id_transformer_map: []
                }
            };
            const params = 'selection_context=' + encodeURIComponent(JSON.stringify(selectionContext)) +
                '&export_contents=assets_and_metadata' +
                '&job_name=Assets sent via Email' +
                '&export_request=' + encodeURIComponent(JSON.stringify(exportRequest)) +
                '&content_type_to_transformers_map=' + encodeURIComponent(JSON.stringify(contentTypeToTransformerMap)) +
                '&asset_id_to_transformers_map=' + encodeURIComponent(JSON.stringify(assetIdToTransformerMap));
            this.http.post(url, params, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
};
ShareAssetService.ctorParameters = () => [
    { type: HttpClient },
    { type: SharingService },
    { type: OTMMService }
];
ShareAssetService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ShareAssetService_Factory() { return new ShareAssetService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.OTMMService)); }, token: ShareAssetService, providedIn: "root" });
ShareAssetService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ShareAssetService);
export { ShareAssetService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNqRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQzs7Ozs7QUFNeEYsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFFMUIsWUFDVyxJQUFnQixFQUNoQixjQUE4QixFQUM5QixXQUF3QjtRQUZ4QixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUUvQixDQUFDO0lBRUwsZ0JBQWdCO1FBQ1osT0FBTyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7SUFDekYsQ0FBQztJQUVELFVBQVUsQ0FBQyxNQUFrQixFQUFFLGNBQW1CO1FBQzlDLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixNQUFNLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDeEIsTUFBTSx5QkFBeUIsR0FBRyxFQUFFLENBQUM7UUFDckMsTUFBTSxxQkFBcUIsR0FBRyxDQUFDO2dCQUMzQixjQUFjLEVBQUUsNkNBQTZDO2dCQUM3RCxTQUFTLEVBQUU7b0JBQ1AsSUFBSTtvQkFDSixJQUFJLENBQUMsZ0JBQWdCLEVBQUU7aUJBQzFCO2FBQ0osQ0FBQyxDQUFDO1FBRUgsSUFBSSxXQUFXLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBRSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFFdkwsTUFBTSxFQUFFLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkMsTUFBTSxJQUFJLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0MsTUFBTSxFQUFFLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkMsTUFBTSxPQUFPLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFakQsSUFBRyxXQUFXLElBQUUsQ0FBQyxJQUFJLFdBQVcsSUFBRyxJQUFJLEVBQUM7WUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxDQUFBO1lBQ3RDLFdBQVcsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQzFLO1FBR0QsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3JGLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDdkUsTUFBTSxHQUFHLEdBQUcsT0FBTyxHQUFHLFVBQVUsR0FBQyxPQUFPLEdBQUMsZUFBZSxDQUFDO1lBRXpELE1BQU0sV0FBVyxHQUFHO2dCQUNoQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUNyQixjQUFjLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFVBQVU7b0JBQzdELGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7aUJBQzFGLENBQUM7YUFDTCxDQUFDO1lBRUYsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbkIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzlCLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN0Qyx5QkFBeUIsQ0FBQyxJQUFJLENBQUM7b0JBQzNCLFlBQVksRUFBRSxLQUFLLENBQUMsWUFBWTtvQkFDaEMsZ0JBQWdCLEVBQUUsSUFBSTtvQkFDdEIsZUFBZSxFQUFFLEtBQUs7b0JBQ3RCLGlCQUFpQixFQUFFLEtBQUs7b0JBQ3hCLGVBQWUsRUFBRSxFQUFFO29CQUNuQix1QkFBdUIsRUFBRSxFQUFFO2lCQUM5QixDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztZQUVILE1BQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLHVCQUF1QixFQUFFO29CQUNyQixpQkFBaUIsRUFBRTt3QkFDZixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsaUJBQWlCLEVBQUUsWUFBWTt3QkFDL0IsSUFBSSxFQUFFLHNEQUFzRDt3QkFDNUQsbUJBQW1CLEVBQUUsTUFBTTtxQkFDOUI7aUJBQ0o7YUFDSixDQUFDO1lBRUYsTUFBTSxhQUFhLEdBQUc7Z0JBQ2xCLG9CQUFvQixFQUFFO29CQUNsQixjQUFjLEVBQUU7d0JBQ1osZUFBZSxFQUFFOzRCQUNiLHFCQUFxQixFQUFFLElBQUk7NEJBQzNCLHNCQUFzQixFQUFFLElBQUk7eUJBQy9CO3dCQUNELHFCQUFxQixFQUFFLGtCQUFrQjt3QkFDekMsdUJBQXVCLEVBQUUsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxZQUFZLElBQUksY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDM0kseUJBQXlCLEVBQUUsS0FBSzt3QkFDaEMsaUJBQWlCLEVBQUU7NEJBQ2YsZ0JBQWdCLEVBQUUsQ0FBQztvQ0FDZixlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLEVBQUU7aUNBQ1osRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLElBQUk7aUNBQ2QsRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLEVBQUU7aUNBQ1osRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLE9BQU87aUNBQ2pCLEVBQUU7b0NBQ0MsZUFBZSxFQUFFLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQ0FDeEMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxPQUFPO2lDQUNoQyxFQUFFO29DQUNDLGVBQWUsRUFBRSxXQUFXLEdBQUUsRUFBRSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7b0NBQ3RDLEtBQUssRUFBRSxjQUFjLENBQUMsT0FBTztpQ0FDaEMsQ0FBQzs0QkFDRixFQUFFLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsbUNBQW1DOzRCQUNoSSxjQUFjLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUMsdUNBQXVDO3lCQUN2Sjt3QkFDRCxTQUFTLEVBQUUsSUFBSTt3QkFDZixTQUFTLEVBQUUsS0FBSztxQkFDbkI7aUJBQ0o7YUFDSixDQUFDO1lBRUYsTUFBTSwyQkFBMkIsR0FBRztnQkFDaEMsaUNBQWlDLEVBQUU7b0JBQy9CLDRCQUE0QixFQUFFLHlCQUF5QjtpQkFDMUQ7YUFDSixDQUFDO1lBRUYsTUFBTSx1QkFBdUIsR0FBRztnQkFDNUIsNkJBQTZCLEVBQUU7b0JBQzNCLHdCQUF3QixFQUFFLEVBQUU7aUJBQy9CO2FBQ0osQ0FBQztZQUVGLE1BQU0sTUFBTSxHQUFHLG9CQUFvQixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdEYsc0NBQXNDO2dCQUN0QyxpQ0FBaUM7Z0JBQ2pDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3RFLG9DQUFvQyxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMkJBQTJCLENBQUMsQ0FBQztnQkFDdEcsZ0NBQWdDLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7WUFFbkcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUM7aUJBQ25DLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0osQ0FBQTs7WUEzSW9CLFVBQVU7WUFDQSxjQUFjO1lBQ2pCLFdBQVc7OztBQUwxQixpQkFBaUI7SUFKN0IsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLGlCQUFpQixDQThJN0I7U0E5SVksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgaXNEZXZNb2RlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSHR0cEhlYWRlcnMsIEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyZUFzc2V0U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlXHJcblxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBmb3JtRG93bmxvYWROYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuICdEb3dubG9hZF8nICsgbmV3IERhdGUoKS50b0pTT04oKS5yZXBsYWNlKC9bLTpdL2csICcnKS5zcGxpdCgnLicpWzBdICsgJy56aXAnO1xyXG4gICAgfVxyXG5cclxuICAgIHNoYXJlQXNzZXQoYXNzZXRzOiBBcnJheTxhbnk+LCBzaGFyaW5nRGV0YWlsczogYW55KSB7XHJcbiAgICAgICAgY29uc3QgYXNzZXRJZHMgPSBbXTtcclxuICAgICAgICBjb25zdCBjb250ZW50VHlwZXMgPSBbXTtcclxuICAgICAgICBjb25zdCBjb250ZW50VHlwZVRyYW5zZm9ybWVyTWFwID0gW107XHJcbiAgICAgICAgY29uc3QgZXhwb3J0Sm9iVHJhbnNmb3JtZXJzID0gW3tcclxuICAgICAgICAgICAgdHJhbnNmb3JtZXJfaWQ6ICdBUlRFU0lBLlRSQU5TRk9STUVSLlpJUCBDT01QUkVTU0lPTi5ERUZBVUxUJyxcclxuICAgICAgICAgICAgYXJndW1lbnRzOiBbXHJcbiAgICAgICAgICAgICAgICBudWxsLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtRG93bmxvYWROYW1lKClcclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH1dO1xyXG5cclxuICAgICAgICB2YXIgb3RtbVZlcnNpb24gPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTikgIT0gdW5kZWZpbmVkID8gcGFyc2VGbG9hdCggSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTikpICk6IDBcclxuXHJcbiAgICAgICAgY29uc3QgdG8gPSBzaGFyaW5nRGV0YWlscy50by5qb2luKCc7Jyk7XHJcbiAgICAgICAgY29uc3QgZnJvbSA9IHNoYXJpbmdEZXRhaWxzLmZyb20uam9pbignOycpO1xyXG4gICAgICAgIGNvbnN0IGNjID0gc2hhcmluZ0RldGFpbHMuY2Muam9pbignOycpO1xyXG4gICAgICAgIGNvbnN0IHJlcGx5VG8gPSBzaGFyaW5nRGV0YWlscy5yZXBseVRvLmpvaW4oJzsnKTtcclxuXHJcbiAgICAgICAgaWYob3RtbVZlcnNpb249PTAgfHwgb3RtbVZlcnNpb249PSBudWxsKXtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5nZXRPVE1NU3lzdGVtRGV0YWlscygpXHJcbiAgICAgICAgICAgICBvdG1tVmVyc2lvbiA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1RNTV9WRVJTSU9OKSAhPSB1bmRlZmluZWQgPyBwYXJzZUZsb2F0KHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1RNTV9WRVJTSU9OKSkgOiAwXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgYmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJy4vJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG4gICAgICAgICAgICBjb25zdCB2ZXJzaW9uID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5hcGlWZXJzaW9uO1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBiYXNlVXJsICsgJ290bW1hcGkvJyt2ZXJzaW9uKycvam9icy9leHBvcnRzJztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICAgICAnQ29udGVudC1UeXBlJzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLkNPTlRFTlRfVFlQRS5VUkxFTkNPREVELFxyXG4gICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBhc3NldHMuZm9yRWFjaChhc3NldCA9PiB7XHJcbiAgICAgICAgICAgICAgICBhc3NldElkcy5wdXNoKGFzc2V0LmFzc2V0X2lkKTtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlcy5wdXNoKGFzc2V0LmNvbnRlbnRfdHlwZSk7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50VHlwZVRyYW5zZm9ybWVyTWFwLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRfdHlwZTogYXNzZXQuY29udGVudF90eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIGluY2x1ZGVfb3JpZ2luYWw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaW5jbHVkZV9wcmV2aWV3OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBpbmNsdWRlX3N1Yl9maWxlczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNfYXJndW1lbnRzOiBbXSxcclxuICAgICAgICAgICAgICAgICAgICBleGNsdXNpb25fYXNzZXRfaWRfbGlzdDogW11cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGlvbkNvbnRleHQgPSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dF9wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbl9jb250ZXh0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzc3NldENvbnRlbnRUeXBlOiBjb250ZW50VHlwZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5hc3NldC5zZWxlY3Rpb24uQXNzZXRJZHNTZWxlY3Rpb25Db250ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5jbHVkZV9kZXNjZW5kYW50czogJ05PTkUnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZXhwb3J0UmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3JlcXVlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfbWFzdGVyX2NvbnRlbnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfcHJldmlld19jb250ZW50OiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yX2hhbmRsaW5nX2FjdGlvbjogJ0RFRkFVTFRfSEFORExJTkcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfam9iX3RyYW5zZm9ybWVyczogKHNoYXJpbmdEZXRhaWxzLnNoYXJpbmdNZXRob2QudmFsdWUgPT09ICdTRU5EX0ZJTEVTJyAmJiBzaGFyaW5nRGV0YWlscy5jb21wcmVzc0ZpbGVzKSA/IGV4cG9ydEpvYlRyYW5zZm9ybWVycyA6IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmVzZXJ2ZV9mb2xkZXJfaGllcmFyY2h5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcnlfdGVtcGxhdGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0ZV92YWx1ZXM6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiAzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0b1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogNCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogZnJvbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogY2NcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IDYsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJlcGx5VG9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IG90bW1WZXJzaW9uID4gMjIgPyA4IDo3LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzaGFyaW5nRGV0YWlscy5zdWJqZWN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiBvdG1tVmVyc2lvbj4gMjI/IDkgOjgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHNoYXJpbmdEZXRhaWxzLm1lc3NhZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1dLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHNoYXJpbmdEZXRhaWxzLnNoYXJpbmdNZXRob2QudmFsdWUgPT09ICdTRU5EX0ZJTEVTJyA/ICdBUlRFU0lBLkRFTElWRVJZLkVNQUlMLkRFRkFVTFQnIDogJ0FSVEVTSUEuREVMSVZFUlkuRU1BSUxVUkwuREVGQVVMVCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1lcl9pZDogc2hhcmluZ0RldGFpbHMuc2hhcmluZ01ldGhvZC52YWx1ZSA9PT0gJ1NFTkRfRklMRVMnID8gJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5FLU1BSUwnIDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5FLU1BSUxVUkwnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdyaXRlX3htbDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd3JpdGVfY3N2OiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGNvbnRlbnRUeXBlVG9UcmFuc2Zvcm1lck1hcCA9IHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnRfdHlwZV90cmFuc2Zvcm1lcl9tYXBfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRfdHlwZV90cmFuc2Zvcm1lcl9tYXA6IGNvbnRlbnRUeXBlVHJhbnNmb3JtZXJNYXBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFzc2V0SWRUb1RyYW5zZm9ybWVyTWFwID0ge1xyXG4gICAgICAgICAgICAgICAgYXNzZXRfaWRfdHJhbnNmb3JtZXJfbWFwX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICBhc3NldF9pZF90cmFuc2Zvcm1lcl9tYXA6IFtdXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSAnc2VsZWN0aW9uX2NvbnRleHQ9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzZWxlY3Rpb25Db250ZXh0KSkgK1xyXG4gICAgICAgICAgICAgICAgJyZleHBvcnRfY29udGVudHM9YXNzZXRzX2FuZF9tZXRhZGF0YScgK1xyXG4gICAgICAgICAgICAgICAgJyZqb2JfbmFtZT1Bc3NldHMgc2VudCB2aWEgRW1haWwnICtcclxuICAgICAgICAgICAgICAgICcmZXhwb3J0X3JlcXVlc3Q9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShleHBvcnRSZXF1ZXN0KSkgK1xyXG4gICAgICAgICAgICAgICAgJyZjb250ZW50X3R5cGVfdG9fdHJhbnNmb3JtZXJzX21hcD0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGNvbnRlbnRUeXBlVG9UcmFuc2Zvcm1lck1hcCkpICtcclxuICAgICAgICAgICAgICAgICcmYXNzZXRfaWRfdG9fdHJhbnNmb3JtZXJzX21hcD0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGFzc2V0SWRUb1RyYW5zZm9ybWVyTWFwKSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmh0dHAucG9zdCh1cmwsIHBhcmFtcywgaHR0cE9wdGlvbnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==