import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareAssetsComponent } from './share-assets.component';
import { MaterialModule } from '../material.module';
import { CustomEmailFieldModule } from '../shared/components/custom-email-field/custom-email-field.module';
let ShareAssetsModule = class ShareAssetsModule {
};
ShareAssetsModule = __decorate([
    NgModule({
        declarations: [
            ShareAssetsComponent
        ],
        imports: [
            CommonModule,
            MaterialModule,
            CustomEmailFieldModule
        ],
        exports: [
            ShareAssetsComponent
        ]
    })
], ShareAssetsModule);
export { ShareAssetsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sbUVBQW1FLENBQUM7QUFnQjNHLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0NBQUksQ0FBQTtBQUFyQixpQkFBaUI7SUFkN0IsUUFBUSxDQUFDO1FBQ04sWUFBWSxFQUFFO1lBQ1Ysb0JBQW9CO1NBQ3ZCO1FBQ0QsT0FBTyxFQUFFO1lBQ0wsWUFBWTtZQUNaLGNBQWM7WUFDZCxzQkFBc0I7U0FDekI7UUFDRCxPQUFPLEVBQUU7WUFDTCxvQkFBb0I7U0FDdkI7S0FDSixDQUFDO0dBRVcsaUJBQWlCLENBQUk7U0FBckIsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU2hhcmVBc3NldHNDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlLWFzc2V0cy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IEN1c3RvbUVtYWlsRmllbGRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tZW1haWwtZmllbGQvY3VzdG9tLWVtYWlsLWZpZWxkLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgU2hhcmVBc3NldHNDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIEN1c3RvbUVtYWlsRmllbGRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVBc3NldHNDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyZUFzc2V0c01vZHVsZSB7IH1cclxuIl19