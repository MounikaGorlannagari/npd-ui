import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let AppMenuComponent = class AppMenuComponent {
    constructor() {
        this.menuClicked = new EventEmitter();
    }
    onMenuClick(menu) {
        this.menuClicked.emit(menu.LOCATION);
    }
    ngOnInit() {
    }
};
__decorate([
    Input()
], AppMenuComponent.prototype, "menus", void 0);
__decorate([
    Output()
], AppMenuComponent.prototype, "menuClicked", void 0);
AppMenuComponent = __decorate([
    Component({
        selector: 'mpm-app-menu',
        template: "<div class=\"menu-list\" *ngIf=\"menus && menus.length\">\r\n    <a class=\"menu-link\" mat-button *ngFor=\"let menu of menus\"  [class.active]=\"menu.ACTIVE\"\r\n        (click)=\"onMenuClick(menu)\">{{menu.DISPLAY_NAME}}</a>\r\n</div>",
        styles: [".menu-list{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center}.menu-list a{font-weight:400;color:#000}"]
    })
], AppMenuComponent);
export { AppMenuComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLW1lbnUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvYXBwLW1lbnUvYXBwLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUS9FLElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBS3pCO1FBRlUsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO0lBRW5DLENBQUM7SUFFakIsV0FBVyxDQUFDLElBQUk7UUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0NBRUosQ0FBQTtBQVpZO0lBQVIsS0FBSyxFQUFFOytDQUFZO0FBQ1Y7SUFBVCxNQUFNLEVBQUU7cURBQTBDO0FBSDFDLGdCQUFnQjtJQU41QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsY0FBYztRQUN4Qix3UEFBd0M7O0tBRTNDLENBQUM7R0FFVyxnQkFBZ0IsQ0FjNUI7U0FkWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWFwcC1tZW51JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hcHAtbWVudS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hcHAtbWVudS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgbWVudXM6IGFueTtcclxuICAgIEBPdXRwdXQoKSBtZW51Q2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXI8b2JqZWN0PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgb25NZW51Q2xpY2sobWVudSkge1xyXG4gICAgICAgIHRoaXMubWVudUNsaWNrZWQuZW1pdChtZW51LkxPQ0FUSU9OKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgIH1cclxuXHJcbn1cclxuIl19