import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
let ExportDataTrayModalComponent = class ExportDataTrayModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'Export Data Tray';
    }
    cancelDialog() {
        this.dialogRef.close();
    }
};
ExportDataTrayModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
ExportDataTrayModalComponent = __decorate([
    Component({
        selector: 'mpm-export-data-tray-modal',
        template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-export-data-tray></mpm-export-data-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
        styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], ExportDataTrayModalComponent);
export { ExportDataTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9leHBvcnQtZGF0YS10cmF5LW1vZGFsL2V4cG9ydC1kYXRhLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBUXpFLElBQWEsNEJBQTRCLEdBQXpDLE1BQWEsNEJBQTRCO0lBSXZDLFlBQ1MsU0FBZ0QsRUFDdkIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUF1QztRQUN2QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxrQkFBa0IsQ0FBQztJQUs1QixDQUFDO0lBRUwsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekIsQ0FBQztDQUVGLENBQUE7O1lBUnFCLFlBQVk7NENBQzdCLE1BQU0sU0FBQyxlQUFlOztBQU5kLDRCQUE0QjtJQUx4QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsNEJBQTRCO1FBQ3RDLHNmQUFzRDs7S0FFdkQsQ0FBQztJQU9HLFdBQUEsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO0dBTmYsNEJBQTRCLENBYXhDO1NBYlksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IEV4cG9ydERhdGFUcmF5Q29tcG9uZW50IH0gZnJvbSAnLi4vZXhwb3J0LWRhdGEtdHJheS9leHBvcnQtZGF0YS10cmF5LmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1leHBvcnQtZGF0YS10cmF5LW1vZGFsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFeHBvcnREYXRhVHJheU1vZGFsQ29tcG9uZW50ICAge1xyXG5cclxuICBtb2RhbFRpdGxlID0gJ0V4cG9ydCBEYXRhIFRyYXknO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxFeHBvcnREYXRhVHJheUNvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGFcclxuICApIHsgfVxyXG5cclxuICBjYW5jZWxEaWFsb2coKSB7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19