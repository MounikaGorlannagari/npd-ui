import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { UtilService } from '../../../mpm-utils/services/util.service';
import * as moment from 'moment';
import { LoaderService } from '../../../loader/loader.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { DatePipe } from '@angular/common';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../notification/notification.service';
import { TaskFilterTypes } from '../../constants/taskListfilters/TaskFilterTypes';
import { TaskListDependentFilterTypes } from '../../constants/taskListfilters/TaskListDependentFilterTypes';
import { GroupByFilterTypes } from '../../constants/GroupByFilterTypes';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { MenuConstants } from '../../constants/menu.constants';
let NotificationTrayComponent = class NotificationTrayComponent {
    constructor(utilService, loaderService, appService, datePipe, sharingService, router, notificationService) {
        this.utilService = utilService;
        this.loaderService = loaderService;
        this.appService = appService;
        this.datePipe = datePipe;
        this.sharingService = sharingService;
        this.router = router;
        this.notificationService = notificationService;
        this.notifiedCount = 0;
        this.notificationData = [];
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.skip = 0;
        this.top = 10;
        this.canLoadMore = true;
    }
    ngOnChanges(changes) {
        // throw new Error('Method not implemented.');
    }
    /* @HostListener('scroll', [])
    onScroll(): void {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        this.skip = this.skip + this.top;
        this.loadData();
      }
    } */
    /* @HostListener('scroll', ['$event']) onScrollEvent(eventData) {
      // this.myScrollContainer.nativeElement.scrollTop <= 1000
      if (false) {
        this.skip = this.skip + this.top;
        this.loadData();
      }
    }
  
    scrolling = (el): void => {
      const scrollTop = el.srcElement.scrollTop;
      if (scrollTop >= 100) {
        // this.scrolled = true;
      } else {
        // this.scrolled = false;
      }
    } */
    setNotificationData() {
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.hasUnreadNotification = false;
        this.notificationData.forEach(content => {
            if (this.utilService.isNullOrEmpty(content.NAVIGATION_URL)) {
                this.formNavigationUrl(content);
            }
            if (content.IS_READ === 'false') {
                this.hasUnreadNotification = true;
            }
            const yesterdayDate = new Date();
            yesterdayDate.setDate(yesterdayDate.getDate() - 1);
            if (!(this.utilService.isNullOrEmpty(content.TITLE) || this.utilService.isNullOrEmpty(content.DESCRIPTION))) {
                if (this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                    this.todayNotification.push(content);
                }
                else if (this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === this.datePipe.transform(yesterdayDate, 'yyyy-MM-dd')) {
                    this.yesterdayNotification.push(content);
                }
                else {
                    this.pastNotification.push(content);
                }
            }
        });
    }
    getDaysBetweenTwoDates(startDate, endDate, diffType) {
        const start = moment(startDate);
        const end = moment(endDate);
        return start.diff(end, diffType, true);
    }
    getDateByGiveFormat(dateObject, formatValue) {
        const dateMoment = moment(dateObject);
        return dateMoment.format(formatValue);
    }
    handleDateManipulation(dateString) {
        if (!dateString) {
            return '';
        }
        if (dateString && !(dateString instanceof Date)) {
            dateString = new Date(dateString);
        }
        let numericValueDiff = 0;
        numericValueDiff = this.getDaysBetweenTwoDates(new Date(), dateString, 'day');
        if (numericValueDiff < 1) {
            numericValueDiff = this.getDaysBetweenTwoDates(new Date(), dateString, 'hours');
            if (numericValueDiff < 1) {
                return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'minutes')) + ' mins ago';
            }
            else {
                return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'hours')) + ' hours ago';
            }
        }
        else {
            return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'day')) + ' days ago';
        }
        // return this.getDateByGiveFormat(dateString, 'MMM D Y, h:mm a');
    }
    formNavigationUrl(notificationContent) {
        let baseUrl;
        let navigationUrl;
        const emailConfig = this.sharingService.getDefaultEmailConfig();
        if (emailConfig) {
            baseUrl = emailConfig.APPLICATION_URL;
        }
        else {
            this.notificationService.error('Application Url is not configured');
            return;
        }
        if (this.sharingService.isManager) {
            if (notificationContent.R_PO_PROJECT && notificationContent.R_PO_PROJECT['Project-id'] && notificationContent.R_PO_PROJECT['Project-id'].Id) {
                if (notificationContent.EVENT_TYPE === MPM_LEVELS.PROJECT) {
                    navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.OVERVIEW + '?menuName=' + MenuConstants.OVERVIEW;
                }
                else if (notificationContent.EVENT_TYPE === MPM_LEVELS.TASK) {
                    if (notificationContent.R_PO_TASK && notificationContent.R_PO_TASK['Task-id'] && notificationContent.R_PO_TASK['Task-id'].Id) {
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '/' +
                            notificationContent.R_PO_TASK['Task-id'].Id + '?menuName=' + MenuConstants.DELIVERABLES;
                    }
                    else {
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '?menuName=' + MenuConstants.TASKS;
                    }
                }
                else if (notificationContent.EVENT_TYPE === MPM_LEVELS.DELIVERABLE && notificationContent.R_PO_TASK && notificationContent.R_PO_TASK['Task-id']) {
                    navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '/' +
                        notificationContent.R_PO_TASK['Task-id'].Id + '?menuName=' + MenuConstants.DELIVERABLES;
                }
                else if (notificationContent.EVENT_TYPE === 'COMMENT') {
                    if (notificationContent.R_PO_DELIVERABLE != undefined)
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.COMMENTS + '?menuName=' + MenuConstants.COMMENTS + '&deliverableId=' + notificationContent.R_PO_DELIVERABLE['Deliverable-id'].Id;
                    else
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.COMMENTS + '?menuName=' + MenuConstants.COMMENTS;
                }
                else {
                    navigationUrl = baseUrl;
                }
            }
        }
        else {
            navigationUrl = baseUrl + '/deliverableWorkView?menuName=deliverableWorkView';
            if (notificationContent.ASSIGNMENT_TYPE === 'ROLE') {
                navigationUrl = navigationUrl + '&' + 'groupByFilter' + '=' + GroupByFilterTypes.GROUP_BY_PROJECT + '&' +
                    'deliverableTaskFilter' + '=' + TaskFilterTypes.MY_TEAM_TASKS + '&' + 'listDependentFilter' + '=' + TaskListDependentFilterTypes.ALL_DELIVERABLE_TYPES + '&' +
                    'emailLinkId' + '=' + notificationContent.GUID;
            }
            else {
                navigationUrl = navigationUrl + '&' + 'groupByFilter' + '=' + GroupByFilterTypes.GROUP_BY_PROJECT + '&' +
                    'deliverableTaskFilter' + '=' + TaskFilterTypes.MY_TASKS + '&' + 'listDependentFilter' + '=' + TaskListDependentFilterTypes.TO_DO_FILTER + '&' +
                    'emailLinkId' + '=' + notificationContent.GUID;
            }
        }
        if (navigationUrl) {
            const parameters = {
                'Notification-id': {
                    Id: notificationContent['Notification-id'].Id
                },
                'Notification-update': {
                    NAVIGATION_URL: navigationUrl
                }
            };
            this.appService.updateNotification(parameters).subscribe();
            notificationContent.NAVIGATION_URL = navigationUrl;
        }
    }
    clearNotification(notificationContent) {
        this.hasUnreadNotification = false;
        const parameters = {
            'Notification-id': {
                Id: notificationContent['Notification-id'].Id
            },
            'Notification-update': {
                IS_DELETED: 'true'
            }
        };
        this.appService.updateNotification(parameters).subscribe();
        const selectedNotificationIndex = this.notificationData.findIndex(data => data['Notification-id'].Id === notificationContent['Notification-id'].Id);
        if (selectedNotificationIndex >= 0) {
            this.notificationData.splice(selectedNotificationIndex, 1);
        }
        const selectedPastNotificationIndex = this.pastNotification.findIndex(data => data['Notification-id'].Id === notificationContent['Notification-id'].Id);
        if (selectedPastNotificationIndex >= 0) {
            this.pastNotification.splice(selectedPastNotificationIndex, 1);
        }
        const selectedTodayNotificationIndex = this.todayNotification.findIndex(data => data['Notification-id'].Id === notificationContent['Notification-id'].Id);
        if (selectedTodayNotificationIndex >= 0) {
            this.todayNotification.splice(selectedTodayNotificationIndex, 1);
        }
        const selectedYesterdayNotificationIndex = this.yesterdayNotification.findIndex(data => data['Notification-id'].Id === notificationContent['Notification-id'].Id);
        if (selectedYesterdayNotificationIndex >= 0) {
            this.yesterdayNotification.splice(selectedYesterdayNotificationIndex, 1);
        }
        this.notificationData.forEach(content => {
            if (content.IS_READ === 'false') {
                this.hasUnreadNotification = true;
            }
        });
    }
    markReadUnreadNotification(notificationContent) {
        this.hasUnreadNotification = false;
        const parameters = {
            'Notification-id': {
                Id: notificationContent['Notification-id'].Id
            },
            'Notification-update': {
                IS_READ: notificationContent.IS_READ === 'true' ? 'false' : 'true'
            }
        };
        this.appService.updateNotification(parameters).subscribe();
        notificationContent.IS_READ = notificationContent.IS_READ === 'true' ? 'false' : 'true';
        this.notificationData.forEach(content => {
            if (content.IS_READ === 'false') {
                this.hasUnreadNotification = true;
            }
        });
    }
    markAsReadNotification(notificationContent) {
        this.sharingService.getNotificationDetails(notificationContent);
        if (notificationContent.IS_READ === 'false') {
            this.hasUnreadNotification = false;
            const parameters = {
                'Notification-id': {
                    Id: notificationContent['Notification-id'].Id
                },
                'Notification-update': {
                    IS_READ: 'true'
                }
            };
            this.appService.updateNotification(parameters).subscribe();
            notificationContent.IS_READ = 'true';
            this.notificationData.forEach(content => {
                if (content.IS_READ === 'false') {
                    this.hasUnreadNotification = true;
                }
            });
        }
        return true;
    }
    clearAllNotification() {
        this.notificationData.forEach(data => {
            if (data.IS_DELETED === 'false') {
                const parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_DELETED: 'true'
                    }
                };
                this.appService.updateNotification(parameters).subscribe();
            }
        });
        this.hasUnreadNotification = false;
        this.notificationData = [];
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.canLoadMore = false;
    }
    markAllAsReadNotification() {
        this.hasUnreadNotification = false;
        this.notificationData.forEach(data => {
            if (data.IS_READ === 'false') {
                const parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_READ: 'true'
                    }
                };
                this.appService.updateNotification(parameters).subscribe();
                data.IS_READ = 'true';
            }
        });
        this.setNotificationData();
    }
    updateNotifiedCount() {
        this.notifiedCount = 0;
        this.notificationData.forEach(data => {
            if (data.IS_NOTIFIED === 'false') {
                const parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_NOTIFIED: 'true'
                    }
                };
                this.appService.updateNotification(parameters).subscribe();
                data.IS_NOTIFIED = 'true';
            }
        });
    }
    loadData() {
        this.skip = this.skip + this.top;
        this.loaderService.show();
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(response => {
            this.loaderService.hide();
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    response.Notification.forEach(notification => {
                        const existingNotification = this.notificationData.find(data => data['Notification-id'].Id === notification['Notification-id'].Id);
                        if (!existingNotification) {
                            this.notificationData.push(notification);
                        }
                    });
                    if (response.Notification.length < this.top) {
                        this.canLoadMore = false;
                    }
                }
                else {
                    const existingNotification = this.notificationData.find(data => data['Notification-id'].Id === response.Notification['Notification-id'].Id);
                    if (!existingNotification) {
                        this.notificationData.push(response.Notification);
                    }
                    this.canLoadMore = false;
                }
            }
            else {
                this.canLoadMore = false;
            }
            this.updateNotifiedCount();
            this.setNotificationData();
        });
    }
    refresh() {
        this.loaderService.show();
        this.skip = 0;
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(response => {
            this.loaderService.hide();
            this.canLoadMore = true;
            this.notificationData = [];
            this.todayNotification = [];
            this.yesterdayNotification = [];
            this.pastNotification = [];
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    this.notificationData = response.Notification;
                    if (response.Notification.length < this.top) {
                        this.canLoadMore = false;
                    }
                }
                else {
                    this.notificationData = [response.Notification];
                    this.canLoadMore = false;
                }
            }
            else {
                this.notificationData = [];
                this.canLoadMore = false;
            }
            this.updateNotifiedCount();
            this.setNotificationData();
        });
    }
    ngOnInit() {
        this.loaderService.show();
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(response => {
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    this.notificationData = response.Notification;
                    if (response.Notification.length < this.top) {
                        this.canLoadMore = false;
                    }
                }
                else {
                    this.notificationData = [response.Notification];
                    this.canLoadMore = false;
                }
            }
            else {
                this.notificationData = [];
                this.canLoadMore = false;
            }
            this.notificationData.forEach(content => {
                if (this.utilService.isNullOrEmpty(content.NAVIGATION_URL)) {
                    this.formNavigationUrl(content);
                }
                if (content.IS_READ === 'false') {
                    this.hasUnreadNotification = true;
                }
                if (content.IS_NOTIFIED === 'false') {
                    this.notifiedCount = this.notifiedCount + 1;
                }
                const yesterdayDate = new Date();
                yesterdayDate.setDate(yesterdayDate.getDate() - 1);
                if (!(this.utilService.isNullOrEmpty(content.TITLE) || this.utilService.isNullOrEmpty(content.DESCRIPTION))) {
                    if (this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                        this.todayNotification.push(content);
                    }
                    else if (this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === this.datePipe.transform(yesterdayDate, 'yyyy-MM-dd')) {
                        this.yesterdayNotification.push(content);
                    }
                    else {
                        this.pastNotification.push(content);
                    }
                }
            });
        });
    }
};
NotificationTrayComponent.ctorParameters = () => [
    { type: UtilService },
    { type: LoaderService },
    { type: AppService },
    { type: DatePipe },
    { type: SharingService },
    { type: Router },
    { type: NotificationService }
];
NotificationTrayComponent = __decorate([
    Component({
        selector: 'mpm-notification-tray',
        template: "<button class=\"notification-icon\" mat-icon-button (click)=\"updateNotifiedCount()\" [matMenuTriggerFor]=\"notification\"\r\n    matTooltip=\"Notifications\">\r\n    <mat-icon [matBadge]=\"notifiedCount\" [ngClass]=\"{'hide-icon': notifiedCount === 0}\" color=\"primary\">\r\n        notifications\r\n    </mat-icon>\r\n</button>\r\n<mat-menu #notification=\"matMenu\" xPosition=\"before\">\r\n    <div style=\"padding: 8px;\" (click)=\"$event.stopPropagation()\">\r\n        <p class=\"notification-heading\">Notifications&nbsp;<mat-icon class=\"refresh-icon\" matTooltip=\"Refresh\"\r\n                (click)=\"refresh()\">refresh\r\n            </mat-icon>\r\n        </p>\r\n        <hr />\r\n        <div class=\"notification-data\">\r\n            <span *ngIf=\"notificationData.length > 0\" class=\"notification-action\" (click)=\"clearAllNotification()\">Clear\r\n                all</span>\r\n            <span *ngIf=\"hasUnreadNotification\" class=\"notification-action\" (click)=\"markAllAsReadNotification()\">Mark\r\n                all\r\n                as read</span>\r\n            <span *ngIf=\"todayNotification.length > 0\">\r\n                <span class=\"notification-group\">TODAY</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of todayNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <span *ngIf=\"yesterdayNotification.length > 0\">\r\n                <span class=\"notification-group\">YESTERDAY</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of yesterdayNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <br />\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <span *ngIf=\"pastNotification.length > 0\">\r\n                <span class=\"notification-group\">PAST</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of pastNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <br />\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <button mat-button class=\"more-btn\" *ngIf=\"notificationData.length > 0 && canLoadMore\"\r\n                (click)=\"loadData()\">Show more...</button>\r\n        </div>\r\n        <div *ngIf=\"notificationData.length === 0\" color=\"accent\" class=\"no-data\">No\r\n            Notifications</div>\r\n    </div>\r\n</mat-menu>",
        styles: [".notification-icon ::ng-deep .mat-badge-content{width:22px!important;border-radius:15px!important}.unread-notification{font-weight:600}::ng-deep .mat-badge.hide-icon .mat-badge-content{display:none}.notification-action-icon{float:right}.notification-action-icon mat-icon{font-size:20px}.notification-group-data .notification-clear-icon{color:transparent;cursor:pointer}.notification-group-data .notification-read-icon{color:transparent}.notification-group-data .notification-description,.notification-group-data .notification-title{width:316px;text-overflow:ellipsis;display:inline-block;overflow:hidden;white-space:nowrap}.notification-group-data .notification-date{font-size:13px;font-weight:600}::ng-deep div.mat-menu-panel{min-width:112px;max-width:500px}.notification-heading{font-size:24px;font-weight:600;margin-bottom:16px}.notification-group{padding:8px;font-size:14px;font-weight:600}.refresh-icon{cursor:pointer;vertical-align:middle}.notification-action{cursor:pointer;float:right;font-size:12px;margin-right:4px}.notification-action:hover{text-decoration:underline}.notification-data{max-height:570px;overflow:auto}.notification-data .more-btn{float:right;font-weight:600;margin-right:8px}.no-data{font-weight:600;width:400px;text-align:center;margin:8px 8px 0}.notification-group-data{padding:12px}"]
    })
], NotificationTrayComponent);
export { NotificationTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbm90aWZpY2F0aW9uLXRyYXkvbm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFvQyxNQUFNLGVBQWUsQ0FBQztBQUM1RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFFdkUsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDbEYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDNUcsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU8vRCxJQUFhLHlCQUF5QixHQUF0QyxNQUFhLHlCQUF5QjtJQVdwQyxZQUNTLFdBQXdCLEVBQ3hCLGFBQTRCLEVBQzVCLFVBQXNCLEVBQ3RCLFFBQWtCLEVBQ2xCLGNBQThCLEVBQzlCLE1BQWMsRUFDZCxtQkFBd0M7UUFOeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2Qsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQWhCakQsa0JBQWEsR0FBRyxDQUFDLENBQUM7UUFDbEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUN2QiwwQkFBcUIsR0FBRyxFQUFFLENBQUM7UUFDM0IscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRXRCLFNBQUksR0FBRyxDQUFDLENBQUM7UUFDVCxRQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ1QsZ0JBQVcsR0FBRyxJQUFJLENBQUM7SUFTZixDQUFDO0lBQ0wsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLDhDQUE4QztJQUNoRCxDQUFDO0lBRUQ7Ozs7OztRQU1JO0lBQ0o7Ozs7Ozs7Ozs7Ozs7OztRQWVJO0lBRUosbUJBQW1CO1FBQ2pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN0QyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDMUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2pDO1lBQ0QsSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtnQkFDL0IsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzthQUNuQztZQUNELE1BQU0sYUFBYSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7WUFDakMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDbkQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFO2dCQUMzRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7b0JBQzdILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3RDO3FCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFO29CQUN2SSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUMxQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNyQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsU0FBZSxFQUFFLE9BQWEsRUFBRSxRQUFpQztRQUN0RixNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzVCLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxVQUFnQixFQUFFLFdBQW1CO1FBQ3ZELE1BQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0QyxPQUFPLFVBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELHNCQUFzQixDQUFDLFVBQVU7UUFDL0IsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNmLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFDRCxJQUFJLFVBQVUsSUFBSSxDQUFDLENBQUMsVUFBVSxZQUFZLElBQUksQ0FBQyxFQUFFO1lBQy9DLFVBQVUsR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNuQztRQUNELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLGdCQUFnQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM5RSxJQUFJLGdCQUFnQixHQUFHLENBQUMsRUFBRTtZQUN4QixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDaEYsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUM7YUFDakc7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxHQUFHLFlBQVksQ0FBQzthQUNoRztTQUNGO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDLEdBQUcsV0FBVyxDQUFDO1NBQzdGO1FBQ0Qsa0VBQWtFO0lBQ3BFLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxtQkFBbUI7UUFDbkMsSUFBSSxPQUFPLENBQUM7UUFDWixJQUFJLGFBQWEsQ0FBQztRQUNsQixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDaEUsSUFBSSxXQUFXLEVBQUU7WUFDZixPQUFPLEdBQUcsV0FBVyxDQUFDLGVBQWUsQ0FBQztTQUV2QzthQUFNO1lBQ0wsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1lBQ3BFLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUU7WUFDakMsSUFBSSxtQkFBbUIsQ0FBQyxZQUFZLElBQUksbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxJQUFJLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBRTNJLElBQUksbUJBQW1CLENBQUMsVUFBVSxLQUFLLFVBQVUsQ0FBQyxPQUFPLEVBQUU7b0JBRXpELGFBQWEsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7aUJBQ2xLO3FCQUFNLElBQUksbUJBQW1CLENBQUMsVUFBVSxLQUFLLFVBQVUsQ0FBQyxJQUFJLEVBQUU7b0JBQzdELElBQUksbUJBQW1CLENBQUMsU0FBUyxJQUFJLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUM1SCxhQUFhLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsS0FBSyxHQUFHLEdBQUc7NEJBQ3pILG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsWUFBWSxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUM7cUJBQzNGO3lCQUFNO3dCQUNMLGFBQWEsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQyxLQUFLLEdBQUcsWUFBWSxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUM7cUJBQzVKO2lCQUNGO3FCQUFNLElBQUksbUJBQW1CLENBQUMsVUFBVSxLQUFLLFVBQVUsQ0FBQyxXQUFXLElBQUksbUJBQW1CLENBQUMsU0FBUyxJQUFJLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtvQkFDakosYUFBYSxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLEtBQUssR0FBRyxHQUFHO3dCQUN6SCxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxHQUFHLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDO2lCQUMzRjtxQkFBTSxJQUFJLG1CQUFtQixDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7b0JBQ3ZELElBQUksbUJBQW1CLENBQUMsZ0JBQWdCLElBQUksU0FBUzt3QkFDbkQsYUFBYSxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLFFBQVEsR0FBRyxZQUFZLEdBQUcsYUFBYSxDQUFDLFFBQVEsR0FBRyxpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs7d0JBRWpQLGFBQWEsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQyxRQUFRLEdBQUcsWUFBWSxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7aUJBQ3BLO3FCQUFNO29CQUNMLGFBQWEsR0FBRyxPQUFPLENBQUM7aUJBQ3pCO2FBQ0Y7U0FDRjthQUFNO1lBRUwsYUFBYSxHQUFHLE9BQU8sR0FBRyxtREFBbUQsQ0FBQztZQUM5RSxJQUFJLG1CQUFtQixDQUFDLGVBQWUsS0FBSyxNQUFNLEVBQUU7Z0JBRWxELGFBQWEsR0FBRyxhQUFhLEdBQUcsR0FBRyxHQUFHLGVBQWUsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsZ0JBQWdCLEdBQUcsR0FBRztvQkFDckcsdUJBQXVCLEdBQUcsR0FBRyxHQUFHLGVBQWUsQ0FBQyxhQUFhLEdBQUcsR0FBRyxHQUFHLHFCQUFxQixHQUFHLEdBQUcsR0FBRyw0QkFBNEIsQ0FBQyxxQkFBcUIsR0FBRyxHQUFHO29CQUM1SixhQUFhLEdBQUcsR0FBRyxHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQzthQUNsRDtpQkFBTTtnQkFFTCxhQUFhLEdBQUcsYUFBYSxHQUFHLEdBQUcsR0FBRyxlQUFlLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLGdCQUFnQixHQUFHLEdBQUc7b0JBQ3JHLHVCQUF1QixHQUFHLEdBQUcsR0FBRyxlQUFlLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxxQkFBcUIsR0FBRyxHQUFHLEdBQUcsNEJBQTRCLENBQUMsWUFBWSxHQUFHLEdBQUc7b0JBQzlJLGFBQWEsR0FBRyxHQUFHLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDO2FBQ2xEO1NBQ0Y7UUFDRCxJQUFJLGFBQWEsRUFBRTtZQUVqQixNQUFNLFVBQVUsR0FBRztnQkFDakIsaUJBQWlCLEVBQUU7b0JBQ2pCLEVBQUUsRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7aUJBQzlDO2dCQUNELHFCQUFxQixFQUFFO29CQUNyQixjQUFjLEVBQUUsYUFBYTtpQkFDOUI7YUFDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUMzRCxtQkFBbUIsQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDO1NBQ3BEO0lBQ0gsQ0FBQztJQUVELGlCQUFpQixDQUFDLG1CQUFtQjtRQUNuQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLE1BQU0sVUFBVSxHQUFHO1lBQ2pCLGlCQUFpQixFQUFFO2dCQUNqQixFQUFFLEVBQUUsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO2FBQzlDO1lBQ0QscUJBQXFCLEVBQUU7Z0JBQ3JCLFVBQVUsRUFBRSxNQUFNO2FBQ25CO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDM0QsTUFBTSx5QkFBeUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQ3ZFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzVFLElBQUkseUJBQXlCLElBQUksQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDNUQ7UUFDRCxNQUFNLDZCQUE2QixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FDM0UsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxLQUFLLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDNUUsSUFBSSw2QkFBNkIsSUFBSSxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNoRTtRQUNELE1BQU0sOEJBQThCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUM3RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM1RSxJQUFJLDhCQUE4QixJQUFJLENBQUMsRUFBRTtZQUN2QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLDhCQUE4QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2xFO1FBQ0QsTUFBTSxrQ0FBa0MsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQ3JGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzVFLElBQUksa0NBQWtDLElBQUksQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsa0NBQWtDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3RDLElBQUksT0FBTyxDQUFDLE9BQU8sS0FBSyxPQUFPLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDbkM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxtQkFBbUI7UUFDNUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxNQUFNLFVBQVUsR0FBRztZQUNqQixpQkFBaUIsRUFBRTtnQkFDakIsRUFBRSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRTthQUM5QztZQUNELHFCQUFxQixFQUFFO2dCQUNyQixPQUFPLEVBQUUsbUJBQW1CLENBQUMsT0FBTyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNO2FBQ25FO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDM0QsbUJBQW1CLENBQUMsT0FBTyxHQUFHLG1CQUFtQixDQUFDLE9BQU8sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3hGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDdEMsSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtnQkFDL0IsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzthQUNuQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNCQUFzQixDQUFDLG1CQUFtQjtRQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDaEUsSUFBSSxtQkFBbUIsQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO1lBQzNDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7WUFDbkMsTUFBTSxVQUFVLEdBQUc7Z0JBQ2pCLGlCQUFpQixFQUFFO29CQUNqQixFQUFFLEVBQUUsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO2lCQUM5QztnQkFDRCxxQkFBcUIsRUFBRTtvQkFDckIsT0FBTyxFQUFFLE1BQU07aUJBQ2hCO2FBQ0YsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDM0QsbUJBQW1CLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNyQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN0QyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO29CQUMvQixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2lCQUNuQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNuQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssT0FBTyxFQUFFO2dCQUMvQixNQUFNLFVBQVUsR0FBRztvQkFDakIsaUJBQWlCLEVBQUU7d0JBQ2pCLEVBQUUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO3FCQUMvQjtvQkFDRCxxQkFBcUIsRUFBRTt3QkFDckIsVUFBVSxFQUFFLE1BQU07cUJBQ25CO2lCQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUM1RDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCx5QkFBeUI7UUFDdkIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxPQUFPLEVBQUU7Z0JBQzVCLE1BQU0sVUFBVSxHQUFHO29CQUNqQixpQkFBaUIsRUFBRTt3QkFDakIsRUFBRSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7cUJBQy9CO29CQUNELHFCQUFxQixFQUFFO3dCQUNyQixPQUFPLEVBQUUsTUFBTTtxQkFDaEI7aUJBQ0YsQ0FBQztnQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzRCxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELG1CQUFtQjtRQUNqQixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxPQUFPLEVBQUU7Z0JBQ2hDLE1BQU0sVUFBVSxHQUFHO29CQUNqQixpQkFBaUIsRUFBRTt3QkFDakIsRUFBRSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7cUJBQy9CO29CQUNELHFCQUFxQixFQUFFO3dCQUNyQixXQUFXLEVBQUUsTUFBTTtxQkFDcEI7aUJBQ0YsQ0FBQztnQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzRCxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQzthQUMzQjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3RGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtnQkFDckMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRTtvQkFDeEMsUUFBUSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7d0JBQzNDLE1BQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxZQUFZLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbkksSUFBSSxDQUFDLG9CQUFvQixFQUFFOzRCQUN6QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3lCQUMxQztvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO3FCQUMxQjtpQkFDRjtxQkFBTTtvQkFDTCxNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUM1SSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUNuRDtvQkFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDMUI7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQzthQUMxQjtZQUNELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDdEYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLEVBQUU7Z0JBQ3JDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDO29CQUM5QyxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO3FCQUMxQjtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ2hELElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUMxQjthQUNGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2FBQzFCO1lBQ0QsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDdEYsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtnQkFDckMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRTtvQkFDeEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUM7b0JBQzlDLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7cUJBQzFCO2lCQUNGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDaEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzFCO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDMUI7WUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN0QyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDMUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO29CQUMvQixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2lCQUNuQztnQkFDRCxJQUFJLE9BQU8sQ0FBQyxXQUFXLEtBQUssT0FBTyxFQUFFO29CQUNuQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO2lCQUM3QztnQkFDRCxNQUFNLGFBQWEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO2dCQUNqQyxhQUFhLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFO29CQUMzRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7d0JBQzdILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3RDO3lCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFO3dCQUN2SSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUMxQzt5QkFBTTt3QkFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNyQztpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YsQ0FBQTs7WUFqWnVCLFdBQVc7WUFDVCxhQUFhO1lBQ2hCLFVBQVU7WUFDWixRQUFRO1lBQ0YsY0FBYztZQUN0QixNQUFNO1lBQ08sbUJBQW1COztBQWxCdEMseUJBQXlCO0lBTHJDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSx1QkFBdUI7UUFDakMsbzZPQUFpRDs7S0FFbEQsQ0FBQztHQUNXLHlCQUF5QixDQTZackM7U0E3WlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkNoYW5nZXMsIE9uSW5pdCwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tGaWx0ZXJUeXBlcyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy90YXNrTGlzdGZpbHRlcnMvVGFza0ZpbHRlclR5cGVzJztcclxuaW1wb3J0IHsgVGFza0xpc3REZXBlbmRlbnRGaWx0ZXJUeXBlcyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy90YXNrTGlzdGZpbHRlcnMvVGFza0xpc3REZXBlbmRlbnRGaWx0ZXJUeXBlcyc7XHJcbmltcG9ydCB7IEdyb3VwQnlGaWx0ZXJUeXBlcyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9Hcm91cEJ5RmlsdGVyVHlwZXMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBNZW51Q29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL21lbnUuY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLW5vdGlmaWNhdGlvbi10cmF5JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL25vdGlmaWNhdGlvbi10cmF5LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvblRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcblxyXG4gIG5vdGlmaWVkQ291bnQgPSAwO1xyXG4gIG5vdGlmaWNhdGlvbkRhdGEgPSBbXTtcclxuICB0b2RheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gIHllc3RlcmRheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gIHBhc3ROb3RpZmljYXRpb24gPSBbXTtcclxuICBoYXNVbnJlYWROb3RpZmljYXRpb247XHJcbiAgc2tpcCA9IDA7XHJcbiAgdG9wID0gMTA7XHJcbiAgY2FuTG9hZE1vcmUgPSB0cnVlO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZGF0ZVBpcGU6IERhdGVQaXBlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgKSB7IH1cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgICAvLyB0aHJvdyBuZXcgRXJyb3IoJ01ldGhvZCBub3QgaW1wbGVtZW50ZWQuJyk7XHJcbiAgfVxyXG5cclxuICAvKiBASG9zdExpc3RlbmVyKCdzY3JvbGwnLCBbXSlcclxuICBvblNjcm9sbCgpOiB2b2lkIHtcclxuICAgIGlmICgod2luZG93LmlubmVySGVpZ2h0ICsgd2luZG93LnNjcm9sbFkpID49IGRvY3VtZW50LmJvZHkub2Zmc2V0SGVpZ2h0KSB7XHJcbiAgICAgIHRoaXMuc2tpcCA9IHRoaXMuc2tpcCArIHRoaXMudG9wO1xyXG4gICAgICB0aGlzLmxvYWREYXRhKCk7XHJcbiAgICB9XHJcbiAgfSAqL1xyXG4gIC8qIEBIb3N0TGlzdGVuZXIoJ3Njcm9sbCcsIFsnJGV2ZW50J10pIG9uU2Nyb2xsRXZlbnQoZXZlbnREYXRhKSB7XHJcbiAgICAvLyB0aGlzLm15U2Nyb2xsQ29udGFpbmVyLm5hdGl2ZUVsZW1lbnQuc2Nyb2xsVG9wIDw9IDEwMDBcclxuICAgIGlmIChmYWxzZSkge1xyXG4gICAgICB0aGlzLnNraXAgPSB0aGlzLnNraXAgKyB0aGlzLnRvcDtcclxuICAgICAgdGhpcy5sb2FkRGF0YSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2Nyb2xsaW5nID0gKGVsKTogdm9pZCA9PiB7XHJcbiAgICBjb25zdCBzY3JvbGxUb3AgPSBlbC5zcmNFbGVtZW50LnNjcm9sbFRvcDtcclxuICAgIGlmIChzY3JvbGxUb3AgPj0gMTAwKSB7XHJcbiAgICAgIC8vIHRoaXMuc2Nyb2xsZWQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gdGhpcy5zY3JvbGxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH0gKi9cclxuXHJcbiAgc2V0Tm90aWZpY2F0aW9uRGF0YSgpIHtcclxuICAgIHRoaXMudG9kYXlOb3RpZmljYXRpb24gPSBbXTtcclxuICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24gPSBbXTtcclxuICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChjb250ZW50ID0+IHtcclxuICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShjb250ZW50Lk5BVklHQVRJT05fVVJMKSkge1xyXG4gICAgICAgIHRoaXMuZm9ybU5hdmlnYXRpb25VcmwoY29udGVudCk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGNvbnRlbnQuSVNfUkVBRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCB5ZXN0ZXJkYXlEYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgeWVzdGVyZGF5RGF0ZS5zZXREYXRlKHllc3RlcmRheURhdGUuZ2V0RGF0ZSgpIC0gMSk7XHJcbiAgICAgIGlmICghKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShjb250ZW50LlRJVExFKSB8fCB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5ERVNDUklQVElPTikpKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNvbnRlbnQuVHJhY2tpbmcuQ3JlYXRlZERhdGUsICd5eXl5LU1NLWRkJykgPT09IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgIHRoaXMudG9kYXlOb3RpZmljYXRpb24ucHVzaChjb250ZW50KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNvbnRlbnQuVHJhY2tpbmcuQ3JlYXRlZERhdGUsICd5eXl5LU1NLWRkJykgPT09IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKHllc3RlcmRheURhdGUsICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uLnB1c2goY29udGVudCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMucGFzdE5vdGlmaWNhdGlvbi5wdXNoKGNvbnRlbnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXREYXlzQmV0d2VlblR3b0RhdGVzKHN0YXJ0RGF0ZTogRGF0ZSwgZW5kRGF0ZTogRGF0ZSwgZGlmZlR5cGU6IG1vbWVudF8udW5pdE9mVGltZS5EaWZmKTogbnVtYmVyIHtcclxuICAgIGNvbnN0IHN0YXJ0ID0gbW9tZW50KHN0YXJ0RGF0ZSk7XHJcbiAgICBjb25zdCBlbmQgPSBtb21lbnQoZW5kRGF0ZSk7XHJcbiAgICByZXR1cm4gc3RhcnQuZGlmZihlbmQsIGRpZmZUeXBlLCB0cnVlKTtcclxuICB9XHJcblxyXG4gIGdldERhdGVCeUdpdmVGb3JtYXQoZGF0ZU9iamVjdDogRGF0ZSwgZm9ybWF0VmFsdWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBkYXRlTW9tZW50ID0gbW9tZW50KGRhdGVPYmplY3QpO1xyXG4gICAgcmV0dXJuIGRhdGVNb21lbnQuZm9ybWF0KGZvcm1hdFZhbHVlKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZURhdGVNYW5pcHVsYXRpb24oZGF0ZVN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBpZiAoIWRhdGVTdHJpbmcpIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gICAgaWYgKGRhdGVTdHJpbmcgJiYgIShkYXRlU3RyaW5nIGluc3RhbmNlb2YgRGF0ZSkpIHtcclxuICAgICAgZGF0ZVN0cmluZyA9IG5ldyBEYXRlKGRhdGVTdHJpbmcpO1xyXG4gICAgfVxyXG4gICAgbGV0IG51bWVyaWNWYWx1ZURpZmYgPSAwO1xyXG4gICAgbnVtZXJpY1ZhbHVlRGlmZiA9IHRoaXMuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnZGF5Jyk7XHJcbiAgICBpZiAobnVtZXJpY1ZhbHVlRGlmZiA8IDEpIHtcclxuICAgICAgbnVtZXJpY1ZhbHVlRGlmZiA9IHRoaXMuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnaG91cnMnKTtcclxuICAgICAgaWYgKG51bWVyaWNWYWx1ZURpZmYgPCAxKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy5nZXREYXlzQmV0d2VlblR3b0RhdGVzKG5ldyBEYXRlKCksIGRhdGVTdHJpbmcsICdtaW51dGVzJykpICsgJyBtaW5zIGFnbyc7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy5nZXREYXlzQmV0d2VlblR3b0RhdGVzKG5ldyBEYXRlKCksIGRhdGVTdHJpbmcsICdob3VycycpKSArICcgaG91cnMgYWdvJztcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy5nZXREYXlzQmV0d2VlblR3b0RhdGVzKG5ldyBEYXRlKCksIGRhdGVTdHJpbmcsICdkYXknKSkgKyAnIGRheXMgYWdvJztcclxuICAgIH1cclxuICAgIC8vIHJldHVybiB0aGlzLmdldERhdGVCeUdpdmVGb3JtYXQoZGF0ZVN0cmluZywgJ01NTSBEIFksIGg6bW0gYScpO1xyXG4gIH1cclxuXHJcbiAgZm9ybU5hdmlnYXRpb25Vcmwobm90aWZpY2F0aW9uQ29udGVudCkge1xyXG4gICAgbGV0IGJhc2VVcmw7XHJcbiAgICBsZXQgbmF2aWdhdGlvblVybDtcclxuICAgIGNvbnN0IGVtYWlsQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXREZWZhdWx0RW1haWxDb25maWcoKTtcclxuICAgIGlmIChlbWFpbENvbmZpZykge1xyXG4gICAgICBiYXNlVXJsID0gZW1haWxDb25maWcuQVBQTElDQVRJT05fVVJMO1xyXG5cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignQXBwbGljYXRpb24gVXJsIGlzIG5vdCBjb25maWd1cmVkJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNoYXJpbmdTZXJ2aWNlLmlzTWFuYWdlcikge1xyXG4gICAgICBpZiAobm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1QgJiYgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXSAmJiBub3RpZmljYXRpb25Db250ZW50LlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkKSB7XHJcblxyXG4gICAgICAgIGlmIChub3RpZmljYXRpb25Db250ZW50LkVWRU5UX1RZUEUgPT09IE1QTV9MRVZFTFMuUFJPSkVDVCkge1xyXG5cclxuICAgICAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsICsgJy9wcm9qZWN0LycgKyBub3RpZmljYXRpb25Db250ZW50LlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkICsgJy8nICsgTWVudUNvbnN0YW50cy5PVkVSVklFVyArICc/bWVudU5hbWU9JyArIE1lbnVDb25zdGFudHMuT1ZFUlZJRVc7XHJcbiAgICAgICAgfSBlbHNlIGlmIChub3RpZmljYXRpb25Db250ZW50LkVWRU5UX1RZUEUgPT09IE1QTV9MRVZFTFMuVEFTSykge1xyXG4gICAgICAgICAgaWYgKG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLICYmIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLWydUYXNrLWlkJ10gJiYgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1RBU0tbJ1Rhc2staWQnXS5JZCkge1xyXG4gICAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvcHJvamVjdC8nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCArICcvJyArIE1lbnVDb25zdGFudHMuVEFTS1MgKyAnLycgK1xyXG4gICAgICAgICAgICAgIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLWydUYXNrLWlkJ10uSWQgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLkRFTElWRVJBQkxFUztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsICsgJy9wcm9qZWN0LycgKyBub3RpZmljYXRpb25Db250ZW50LlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkICsgJy8nICsgTWVudUNvbnN0YW50cy5UQVNLUyArICc/bWVudU5hbWU9JyArIE1lbnVDb25zdGFudHMuVEFTS1M7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChub3RpZmljYXRpb25Db250ZW50LkVWRU5UX1RZUEUgPT09IE1QTV9MRVZFTFMuREVMSVZFUkFCTEUgJiYgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1RBU0sgJiYgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1RBU0tbJ1Rhc2staWQnXSkge1xyXG4gICAgICAgICAgbmF2aWdhdGlvblVybCA9IGJhc2VVcmwgKyAnL3Byb2plY3QvJyArIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19QUk9KRUNUWydQcm9qZWN0LWlkJ10uSWQgKyAnLycgKyBNZW51Q29uc3RhbnRzLlRBU0tTICsgJy8nICtcclxuICAgICAgICAgICAgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1RBU0tbJ1Rhc2staWQnXS5JZCArICc/bWVudU5hbWU9JyArIE1lbnVDb25zdGFudHMuREVMSVZFUkFCTEVTO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobm90aWZpY2F0aW9uQ29udGVudC5FVkVOVF9UWVBFID09PSAnQ09NTUVOVCcpIHtcclxuICAgICAgICAgIGlmIChub3RpZmljYXRpb25Db250ZW50LlJfUE9fREVMSVZFUkFCTEUgIT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvcHJvamVjdC8nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCArICcvJyArIE1lbnVDb25zdGFudHMuQ09NTUVOVFMgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLkNPTU1FTlRTICsgJyZkZWxpdmVyYWJsZUlkPScgKyBub3RpZmljYXRpb25Db250ZW50LlJfUE9fREVMSVZFUkFCTEVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQ7XHJcbiAgICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsICsgJy9wcm9qZWN0LycgKyBub3RpZmljYXRpb25Db250ZW50LlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkICsgJy8nICsgTWVudUNvbnN0YW50cy5DT01NRU5UUyArICc/bWVudU5hbWU9JyArIE1lbnVDb25zdGFudHMuQ09NTUVOVFM7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsICsgJy9kZWxpdmVyYWJsZVdvcmtWaWV3P21lbnVOYW1lPWRlbGl2ZXJhYmxlV29ya1ZpZXcnO1xyXG4gICAgICBpZiAobm90aWZpY2F0aW9uQ29udGVudC5BU1NJR05NRU5UX1RZUEUgPT09ICdST0xFJykge1xyXG5cclxuICAgICAgICBuYXZpZ2F0aW9uVXJsID0gbmF2aWdhdGlvblVybCArICcmJyArICdncm91cEJ5RmlsdGVyJyArICc9JyArIEdyb3VwQnlGaWx0ZXJUeXBlcy5HUk9VUF9CWV9QUk9KRUNUICsgJyYnICtcclxuICAgICAgICAgICdkZWxpdmVyYWJsZVRhc2tGaWx0ZXInICsgJz0nICsgVGFza0ZpbHRlclR5cGVzLk1ZX1RFQU1fVEFTS1MgKyAnJicgKyAnbGlzdERlcGVuZGVudEZpbHRlcicgKyAnPScgKyBUYXNrTGlzdERlcGVuZGVudEZpbHRlclR5cGVzLkFMTF9ERUxJVkVSQUJMRV9UWVBFUyArICcmJyArXHJcbiAgICAgICAgICAnZW1haWxMaW5rSWQnICsgJz0nICsgbm90aWZpY2F0aW9uQ29udGVudC5HVUlEO1xyXG4gICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICBuYXZpZ2F0aW9uVXJsID0gbmF2aWdhdGlvblVybCArICcmJyArICdncm91cEJ5RmlsdGVyJyArICc9JyArIEdyb3VwQnlGaWx0ZXJUeXBlcy5HUk9VUF9CWV9QUk9KRUNUICsgJyYnICtcclxuICAgICAgICAgICdkZWxpdmVyYWJsZVRhc2tGaWx0ZXInICsgJz0nICsgVGFza0ZpbHRlclR5cGVzLk1ZX1RBU0tTICsgJyYnICsgJ2xpc3REZXBlbmRlbnRGaWx0ZXInICsgJz0nICsgVGFza0xpc3REZXBlbmRlbnRGaWx0ZXJUeXBlcy5UT19ET19GSUxURVIgKyAnJicgK1xyXG4gICAgICAgICAgJ2VtYWlsTGlua0lkJyArICc9JyArIG5vdGlmaWNhdGlvbkNvbnRlbnQuR1VJRDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKG5hdmlnYXRpb25VcmwpIHtcclxuXHJcbiAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgJ05vdGlmaWNhdGlvbi1pZCc6IHtcclxuICAgICAgICAgIElkOiBub3RpZmljYXRpb25Db250ZW50WydOb3RpZmljYXRpb24taWQnXS5JZFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgJ05vdGlmaWNhdGlvbi11cGRhdGUnOiB7XHJcbiAgICAgICAgICBOQVZJR0FUSU9OX1VSTDogbmF2aWdhdGlvblVybFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgICAgbm90aWZpY2F0aW9uQ29udGVudC5OQVZJR0FUSU9OX1VSTCA9IG5hdmlnYXRpb25Vcmw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjbGVhck5vdGlmaWNhdGlvbihub3RpZmljYXRpb25Db250ZW50KSB7XHJcbiAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgJ05vdGlmaWNhdGlvbi1pZCc6IHtcclxuICAgICAgICBJZDogbm90aWZpY2F0aW9uQ29udGVudFsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgfSxcclxuICAgICAgJ05vdGlmaWNhdGlvbi11cGRhdGUnOiB7XHJcbiAgICAgICAgSVNfREVMRVRFRDogJ3RydWUnXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICB0aGlzLmFwcFNlcnZpY2UudXBkYXRlTm90aWZpY2F0aW9uKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgpO1xyXG4gICAgY29uc3Qgc2VsZWN0ZWROb3RpZmljYXRpb25JbmRleCA9IHRoaXMubm90aWZpY2F0aW9uRGF0YS5maW5kSW5kZXgoZGF0YSA9PlxyXG4gICAgICBkYXRhWydOb3RpZmljYXRpb24taWQnXS5JZCA9PT0gbm90aWZpY2F0aW9uQ29udGVudFsnTm90aWZpY2F0aW9uLWlkJ10uSWQpO1xyXG4gICAgaWYgKHNlbGVjdGVkTm90aWZpY2F0aW9uSW5kZXggPj0gMCkge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuc3BsaWNlKHNlbGVjdGVkTm90aWZpY2F0aW9uSW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VsZWN0ZWRQYXN0Tm90aWZpY2F0aW9uSW5kZXggPSB0aGlzLnBhc3ROb3RpZmljYXRpb24uZmluZEluZGV4KGRhdGEgPT5cclxuICAgICAgZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkKTtcclxuICAgIGlmIChzZWxlY3RlZFBhc3ROb3RpZmljYXRpb25JbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMucGFzdE5vdGlmaWNhdGlvbi5zcGxpY2Uoc2VsZWN0ZWRQYXN0Tm90aWZpY2F0aW9uSW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VsZWN0ZWRUb2RheU5vdGlmaWNhdGlvbkluZGV4ID0gdGhpcy50b2RheU5vdGlmaWNhdGlvbi5maW5kSW5kZXgoZGF0YSA9PlxyXG4gICAgICBkYXRhWydOb3RpZmljYXRpb24taWQnXS5JZCA9PT0gbm90aWZpY2F0aW9uQ29udGVudFsnTm90aWZpY2F0aW9uLWlkJ10uSWQpO1xyXG4gICAgaWYgKHNlbGVjdGVkVG9kYXlOb3RpZmljYXRpb25JbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMudG9kYXlOb3RpZmljYXRpb24uc3BsaWNlKHNlbGVjdGVkVG9kYXlOb3RpZmljYXRpb25JbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgICBjb25zdCBzZWxlY3RlZFllc3RlcmRheU5vdGlmaWNhdGlvbkluZGV4ID0gdGhpcy55ZXN0ZXJkYXlOb3RpZmljYXRpb24uZmluZEluZGV4KGRhdGEgPT5cclxuICAgICAgZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkKTtcclxuICAgIGlmIChzZWxlY3RlZFllc3RlcmRheU5vdGlmaWNhdGlvbkluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy55ZXN0ZXJkYXlOb3RpZmljYXRpb24uc3BsaWNlKHNlbGVjdGVkWWVzdGVyZGF5Tm90aWZpY2F0aW9uSW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLmZvckVhY2goY29udGVudCA9PiB7XHJcbiAgICAgIGlmIChjb250ZW50LklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbWFya1JlYWRVbnJlYWROb3RpZmljYXRpb24obm90aWZpY2F0aW9uQ29udGVudCkge1xyXG4gICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgSWQ6IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkXHJcbiAgICAgIH0sXHJcbiAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgIElTX1JFQUQ6IG5vdGlmaWNhdGlvbkNvbnRlbnQuSVNfUkVBRCA9PT0gJ3RydWUnID8gJ2ZhbHNlJyA6ICd0cnVlJ1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgIG5vdGlmaWNhdGlvbkNvbnRlbnQuSVNfUkVBRCA9IG5vdGlmaWNhdGlvbkNvbnRlbnQuSVNfUkVBRCA9PT0gJ3RydWUnID8gJ2ZhbHNlJyA6ICd0cnVlJztcclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGNvbnRlbnQgPT4ge1xyXG4gICAgICBpZiAoY29udGVudC5JU19SRUFEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG1hcmtBc1JlYWROb3RpZmljYXRpb24obm90aWZpY2F0aW9uQ29udGVudCkge1xyXG4gICAgdGhpcy5zaGFyaW5nU2VydmljZS5nZXROb3RpZmljYXRpb25EZXRhaWxzKG5vdGlmaWNhdGlvbkNvbnRlbnQpO1xyXG4gICAgaWYgKG5vdGlmaWNhdGlvbkNvbnRlbnQuSVNfUkVBRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgICBJZDogbm90aWZpY2F0aW9uQ29udGVudFsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgICB9LFxyXG4gICAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgICAgSVNfUkVBRDogJ3RydWUnXHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UudXBkYXRlTm90aWZpY2F0aW9uKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgpO1xyXG4gICAgICBub3RpZmljYXRpb25Db250ZW50LklTX1JFQUQgPSAndHJ1ZSc7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGNvbnRlbnQgPT4ge1xyXG4gICAgICAgIGlmIChjb250ZW50LklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBjbGVhckFsbE5vdGlmaWNhdGlvbigpIHtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICBpZiAoZGF0YS5JU19ERUxFVEVEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgICAgIElkOiBkYXRhWydOb3RpZmljYXRpb24taWQnXS5JZFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgICAgICBJU19ERUxFVEVEOiAndHJ1ZSdcclxuICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS51cGRhdGVOb3RpZmljYXRpb24ocGFyYW1ldGVycykuc3Vic2NyaWJlKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IFtdO1xyXG4gICAgdGhpcy50b2RheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgdGhpcy55ZXN0ZXJkYXlOb3RpZmljYXRpb24gPSBbXTtcclxuICAgIHRoaXMucGFzdE5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgbWFya0FsbEFzUmVhZE5vdGlmaWNhdGlvbigpIHtcclxuICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgaWYgKGRhdGEuSVNfUkVBRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLWlkJzoge1xyXG4gICAgICAgICAgICBJZDogZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLXVwZGF0ZSc6IHtcclxuICAgICAgICAgICAgSVNfUkVBRDogJ3RydWUnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UudXBkYXRlTm90aWZpY2F0aW9uKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgpO1xyXG4gICAgICAgIGRhdGEuSVNfUkVBRCA9ICd0cnVlJztcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLnNldE5vdGlmaWNhdGlvbkRhdGEoKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZU5vdGlmaWVkQ291bnQoKSB7XHJcbiAgICB0aGlzLm5vdGlmaWVkQ291bnQgPSAwO1xyXG4gICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgIGlmIChkYXRhLklTX05PVElGSUVEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgICAgIElkOiBkYXRhWydOb3RpZmljYXRpb24taWQnXS5JZFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgICAgICBJU19OT1RJRklFRDogJ3RydWUnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UudXBkYXRlTm90aWZpY2F0aW9uKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgpO1xyXG4gICAgICAgIGRhdGEuSVNfTk9USUZJRUQgPSAndHJ1ZSc7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbG9hZERhdGEoKSB7XHJcbiAgICB0aGlzLnNraXAgPSB0aGlzLnNraXAgKyB0aGlzLnRvcDtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uRm9yQ3VycmVudFVzZXIodGhpcy5za2lwLCB0aGlzLnRvcCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLk5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHJlc3BvbnNlLk5vdGlmaWNhdGlvbikpIHtcclxuICAgICAgICAgIHJlc3BvbnNlLk5vdGlmaWNhdGlvbi5mb3JFYWNoKG5vdGlmaWNhdGlvbiA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV4aXN0aW5nTm90aWZpY2F0aW9uID0gdGhpcy5ub3RpZmljYXRpb25EYXRhLmZpbmQoZGF0YSA9PiBkYXRhWydOb3RpZmljYXRpb24taWQnXS5JZCA9PT0gbm90aWZpY2F0aW9uWydOb3RpZmljYXRpb24taWQnXS5JZCk7XHJcbiAgICAgICAgICAgIGlmICghZXhpc3RpbmdOb3RpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEucHVzaChub3RpZmljYXRpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIGlmIChyZXNwb25zZS5Ob3RpZmljYXRpb24ubGVuZ3RoIDwgdGhpcy50b3ApIHtcclxuICAgICAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zdCBleGlzdGluZ05vdGlmaWNhdGlvbiA9IHRoaXMubm90aWZpY2F0aW9uRGF0YS5maW5kKGRhdGEgPT4gZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IHJlc3BvbnNlLk5vdGlmaWNhdGlvblsnTm90aWZpY2F0aW9uLWlkJ10uSWQpO1xyXG4gICAgICAgICAgaWYgKCFleGlzdGluZ05vdGlmaWNhdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEucHVzaChyZXNwb25zZS5Ob3RpZmljYXRpb24pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmNhbkxvYWRNb3JlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy51cGRhdGVOb3RpZmllZENvdW50KCk7XHJcbiAgICAgIHRoaXMuc2V0Tm90aWZpY2F0aW9uRGF0YSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICByZWZyZXNoKCkge1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIHRoaXMuc2tpcCA9IDA7XHJcbiAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uRm9yQ3VycmVudFVzZXIodGhpcy5za2lwLCB0aGlzLnRvcCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IHRydWU7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IFtdO1xyXG4gICAgICB0aGlzLnRvZGF5Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICAgIHRoaXMucGFzdE5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuTm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkocmVzcG9uc2UuTm90aWZpY2F0aW9uKSkge1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhID0gcmVzcG9uc2UuTm90aWZpY2F0aW9uO1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlLk5vdGlmaWNhdGlvbi5sZW5ndGggPCB0aGlzLnRvcCkge1xyXG4gICAgICAgICAgICB0aGlzLmNhbkxvYWRNb3JlID0gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IFtyZXNwb25zZS5Ob3RpZmljYXRpb25dO1xyXG4gICAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSBbXTtcclxuICAgICAgICB0aGlzLmNhbkxvYWRNb3JlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy51cGRhdGVOb3RpZmllZENvdW50KCk7XHJcbiAgICAgIHRoaXMuc2V0Tm90aWZpY2F0aW9uRGF0YSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uRm9yQ3VycmVudFVzZXIodGhpcy5za2lwLCB0aGlzLnRvcCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLk5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHJlc3BvbnNlLk5vdGlmaWNhdGlvbikpIHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IHJlc3BvbnNlLk5vdGlmaWNhdGlvbjtcclxuICAgICAgICAgIGlmIChyZXNwb25zZS5Ob3RpZmljYXRpb24ubGVuZ3RoIDwgdGhpcy50b3ApIHtcclxuICAgICAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSBbcmVzcG9uc2UuTm90aWZpY2F0aW9uXTtcclxuICAgICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhID0gW107XHJcbiAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGNvbnRlbnQgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5OQVZJR0FUSU9OX1VSTCkpIHtcclxuICAgICAgICAgIHRoaXMuZm9ybU5hdmlnYXRpb25VcmwoY29udGVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjb250ZW50LklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGNvbnRlbnQuSVNfTk9USUZJRUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICAgIHRoaXMubm90aWZpZWRDb3VudCA9IHRoaXMubm90aWZpZWRDb3VudCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHllc3RlcmRheURhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgICAgIHllc3RlcmRheURhdGUuc2V0RGF0ZSh5ZXN0ZXJkYXlEYXRlLmdldERhdGUoKSAtIDEpO1xyXG4gICAgICAgIGlmICghKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShjb250ZW50LlRJVExFKSB8fCB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5ERVNDUklQVElPTikpKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0oY29udGVudC5UcmFja2luZy5DcmVhdGVkRGF0ZSwgJ3l5eXktTU0tZGQnKSA9PT0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRvZGF5Tm90aWZpY2F0aW9uLnB1c2goY29udGVudCk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNvbnRlbnQuVHJhY2tpbmcuQ3JlYXRlZERhdGUsICd5eXl5LU1NLWRkJykgPT09IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKHllc3RlcmRheURhdGUsICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgICAgdGhpcy55ZXN0ZXJkYXlOb3RpZmljYXRpb24ucHVzaChjb250ZW50KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFzdE5vdGlmaWNhdGlvbi5wdXNoKGNvbnRlbnQpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19