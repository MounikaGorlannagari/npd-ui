import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let ListFilterComponent = class ListFilterComponent {
    constructor() {
        this.clicked = new EventEmitter();
        this.fitlerChanged = new EventEmitter();
        this.filterLengthRestricted = false;
    }
    onClick($event) {
        if (this.filters && this.filters.length > 1) {
            this.clicked.next();
        }
    }
    onFilterChange(currentFilter) {
        this.filters.map(filter => {
            if (filter.value === currentFilter.value) {
                this.selectedFilter = filter;
            }
        });
        this.fitlerChanged.next(this.selectedFilter);
    }
    handleDefaultSelection() {
        if (!this.filters || this.filters.length <= 0) {
            this.filters = [];
            this.selectedFilter = null;
        }
        else {
            if (this.selected) {
                this.selectedFilter = this.filters.find(filter => filter.value === this.selected.value);
            }
            else {
                this.selectedFilter = this.filters.find(filter => filter.default === true);
            }
            // this.onFilterChange(this.selectedFilter);
        }
    }
    initialize() {
        this.handleDefaultSelection();
    }
    ngOnChanges(changes) {
        if (changes && changes.selected && !changes.selected.firstChange && JSON.stringify(changes.selected.previousValue) !== JSON.stringify(changes.selected.currentValue)
            && changes.selected.currentValue.value !== this.selectedFilter.value) {
            this.handleDefaultSelection();
        }
    }
    ngOnInit() {
        this.initialize();
        this.filterLengthRestricted = this.filters.length <= 1 ? true : false;
    }
    ngOnDestroy() {
    }
};
__decorate([
    Input()
], ListFilterComponent.prototype, "filters", void 0);
__decorate([
    Input()
], ListFilterComponent.prototype, "selected", void 0);
__decorate([
    Output()
], ListFilterComponent.prototype, "clicked", void 0);
__decorate([
    Output()
], ListFilterComponent.prototype, "fitlerChanged", void 0);
ListFilterComponent = __decorate([
    Component({
        selector: 'mpm-list-filter',
        template: "<button class=\"sort-btn\" *ngIf=\"selectedFilter && filters?.length>0\" mat-button\r\n    matTooltip=\"Click to see more view options\" [matMenuTriggerFor]=\"MPMListFilterMenu\" (click)=\"onClick($event)\">\r\n    <span>{{selectedFilter.name}}</span>\r\n    <span>{{(selectedFilter?.count>=0) ? (' (' + selectedFilter.count + ')') : ''}}</span>\r\n    <mat-icon color=\"primary\" *ngIf=\"filters?.length>1\">arrow_drop_down</mat-icon>\r\n</button>\r\n<mat-menu #MPMListFilterMenu=\"matMenu\">\r\n    <span *ngFor=\"let filter of filters\">\r\n        <button mat-menu-item *ngIf=\"filter.value != selectedFilter?.value\" (click)=\"onFilterChange(filter)\"\r\n            [value]=\"filter.value\">\r\n            <span>{{filter.name}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
        styles: [""]
    })
], ListFilterComponent);
export { ListFilterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlzdC1maWx0ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbGlzdC1maWx0ZXIvbGlzdC1maWx0ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUF1QyxNQUFNLGVBQWUsQ0FBQztBQVFwSCxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQVU5QjtRQUxVLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDO1FBQ25DLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQztRQUV6RCwyQkFBc0IsR0FBWSxLQUFLLENBQUM7SUFFeEIsQ0FBQztJQUlqQixPQUFPLENBQUMsTUFBYTtRQUNuQixJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBRUQsY0FBYyxDQUFDLGFBQXlCO1FBQ3RDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3hCLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsS0FBSyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQzthQUM5QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxzQkFBc0I7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekY7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLENBQUM7YUFDNUU7WUFDRCw0Q0FBNEM7U0FDN0M7SUFDSCxDQUFDO0lBRUQsVUFBVTtRQUNSLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO2VBQy9KLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRTtZQUN0RSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3hFLENBQUM7SUFFRCxXQUFXO0lBRVgsQ0FBQztDQUVGLENBQUE7QUE3RFU7SUFBUixLQUFLLEVBQUU7b0RBQTRCO0FBQzNCO0lBQVIsS0FBSyxFQUFFO3FEQUFzQjtBQUVwQjtJQUFULE1BQU0sRUFBRTtvREFBb0M7QUFDbkM7SUFBVCxNQUFNLEVBQUU7MERBQWdEO0FBTjlDLG1CQUFtQjtJQUwvQixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLG95QkFBMkM7O0tBRTVDLENBQUM7R0FDVyxtQkFBbUIsQ0ErRC9CO1NBL0RZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uRGVzdHJveSwgU2ltcGxlQ2hhbmdlcywgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IExpc3RGaWx0ZXIgfSBmcm9tICcuL29iamVjdHMvTGlzdEZpbHRlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1saXN0LWZpbHRlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2xpc3QtZmlsdGVyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9saXN0LWZpbHRlci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMaXN0RmlsdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XHJcblxyXG4gIEBJbnB1dCgpIGZpbHRlcnM6IEFycmF5PExpc3RGaWx0ZXI+O1xyXG4gIEBJbnB1dCgpIHNlbGVjdGVkOiBMaXN0RmlsdGVyO1xyXG5cclxuICBAT3V0cHV0KCkgY2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuICBAT3V0cHV0KCkgZml0bGVyQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXI8TGlzdEZpbHRlcj4oKTtcclxuXHJcbiAgZmlsdGVyTGVuZ3RoUmVzdHJpY3RlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBzZWxlY3RlZEZpbHRlcjogTGlzdEZpbHRlcjtcclxuXHJcbiAgb25DbGljaygkZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICBpZiAodGhpcy5maWx0ZXJzICYmIHRoaXMuZmlsdGVycy5sZW5ndGggPiAxKSB7XHJcbiAgICAgIHRoaXMuY2xpY2tlZC5uZXh0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvbkZpbHRlckNoYW5nZShjdXJyZW50RmlsdGVyOiBMaXN0RmlsdGVyKSB7XHJcbiAgICB0aGlzLmZpbHRlcnMubWFwKGZpbHRlciA9PiB7XHJcbiAgICAgIGlmIChmaWx0ZXIudmFsdWUgPT09IGN1cnJlbnRGaWx0ZXIudmFsdWUpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRmlsdGVyID0gZmlsdGVyO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuZml0bGVyQ2hhbmdlZC5uZXh0KHRoaXMuc2VsZWN0ZWRGaWx0ZXIpO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlRGVmYXVsdFNlbGVjdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5maWx0ZXJzIHx8IHRoaXMuZmlsdGVycy5sZW5ndGggPD0gMCkge1xyXG4gICAgICB0aGlzLmZpbHRlcnMgPSBbXTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEZpbHRlciA9IG51bGw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRGaWx0ZXIgPSB0aGlzLmZpbHRlcnMuZmluZChmaWx0ZXIgPT4gZmlsdGVyLnZhbHVlID09PSB0aGlzLnNlbGVjdGVkLnZhbHVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRmlsdGVyID0gdGhpcy5maWx0ZXJzLmZpbmQoZmlsdGVyID0+IGZpbHRlci5kZWZhdWx0ID09PSB0cnVlKTtcclxuICAgICAgfVxyXG4gICAgICAvLyB0aGlzLm9uRmlsdGVyQ2hhbmdlKHRoaXMuc2VsZWN0ZWRGaWx0ZXIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW5pdGlhbGl6ZSgpIHtcclxuICAgIHRoaXMuaGFuZGxlRGVmYXVsdFNlbGVjdGlvbigpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5zZWxlY3RlZCAmJiAhY2hhbmdlcy5zZWxlY3RlZC5maXJzdENoYW5nZSAmJiBKU09OLnN0cmluZ2lmeShjaGFuZ2VzLnNlbGVjdGVkLnByZXZpb3VzVmFsdWUpICE9PSBKU09OLnN0cmluZ2lmeShjaGFuZ2VzLnNlbGVjdGVkLmN1cnJlbnRWYWx1ZSlcclxuICAgICAgJiYgY2hhbmdlcy5zZWxlY3RlZC5jdXJyZW50VmFsdWUudmFsdWUgIT09IHRoaXMuc2VsZWN0ZWRGaWx0ZXIudmFsdWUpIHtcclxuICAgICAgdGhpcy5oYW5kbGVEZWZhdWx0U2VsZWN0aW9uKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuaW5pdGlhbGl6ZSgpO1xyXG4gICAgdGhpcy5maWx0ZXJMZW5ndGhSZXN0cmljdGVkID0gdGhpcy5maWx0ZXJzLmxlbmd0aCA8PSAxID8gdHJ1ZSA6IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcblxyXG4gIH1cclxuXHJcbn1cclxuIl19