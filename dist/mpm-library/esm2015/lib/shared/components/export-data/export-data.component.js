import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../../mpm-utils/services/app.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../constants/application.config.constants';
let ExportDataComponent = class ExportDataComponent {
    constructor(dialogRef, data, appService, formBuilder, loaderService, sharingService, notificationService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.loaderService = loaderService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.exportType = [
            { value: 'EXCEL', viewValue: 'Excel' },
            { value: 'CSV', viewValue: 'CSV' }
        ];
    }
    ngOnInit() {
        this.initialiseForm();
        this.appConfig = this.sharingService.getAppConfig();
        this.filePath = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_FILE_PATH];
        this.pageLimit = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT];
        this.exportDataMinCount = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_MIN_COUNT];
        console.log(this.data);
        console.log(this.appConfig, 'appconfig');
        console.log(this.filePath);
    }
    onExportData() {
        this.loaderService.show();
        const itemIds = [];
        // this.data.exportData.forEach(data => {
        //   itemIds.push(
        //      data.ITEM_ID
        // );
        // });
        this.appService.exportData(this.data.columns, this.exportDataForm.value.exportFileName, this.exportDataForm.value.exportType, itemIds, this.filePath, this.data.searchRequest, this.pageLimit, this.data.totalCount, this.exportDataMinCount).subscribe(response => {
            this.notificationService.success("Export Data Initiated Successfully! Check in Export Data Tray for the details");
            this.loaderService.hide();
            this.dialogRef.close(false);
        }, () => {
            this.loaderService.hide();
            this.notificationService.error('Something went wrong while exporting data');
        });
    }
    initialiseForm() {
        const disableField = false;
        this.exportDataForm = null;
        this.exportDataForm = this.formBuilder.group({
            exportFileName: new FormControl('', [Validators.required, Validators.maxLength(120)]),
            exportType: new FormControl('', [Validators.required])
        });
    }
    closeDialog() {
        this.dialogRef.close(false);
    }
};
ExportDataComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: AppService },
    { type: FormBuilder },
    { type: LoaderService },
    { type: SharingService },
    { type: NotificationService }
];
ExportDataComponent = __decorate([
    Component({
        selector: 'mpm-export-data',
        template: "<h1 mat-dialog-title class=\"export-data-modal-title\">Export Data</h1>\r\n<button matTooltip=\"Close\" mat-icon-button color=\"primary\" (click)=\"closeDialog()\" [mat-dialog-close]=\"true\"\r\n    class=\"mat-icon-close-btn\">\r\n    <mat-icon>close</mat-icon>\r\n</button>\r\n<mat-divider></mat-divider>\r\n<form class=\"export-data-form\" [formGroup]=\"exportDataForm\">\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Export File Name</mat-label>\r\n                <input appAutoFocus matInput placeholder=\"Export File Name\" formControlName=\"exportFileName\" required>\r\n\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item max-width\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Export Type</mat-label>\r\n                <mat-select formControlName=\"exportType\" placeholder=\"Export Type\" name=\"exportType\" required>\r\n                    <mat-option *ngFor=\"let type of exportType\" value=\"{{type.value}}\">\r\n                        {{type.viewValue}}\r\n                    </mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <span class=\"form-actions\">\r\n                <button mat-stroked-button type=\"button\" (click)=\"closeDialog()\">\r\n                    Close\r\n                </button>\r\n                <button id=\"bulk-comments-button\" mat-stroked-button color=\"primary\" type=\"button\"\r\n                    (click)=\"onExportData()\" [disabled]=\"(exportDataForm.invalid)\">\r\n                    Done\r\n                </button>\r\n            </span>\r\n        </div>\r\n    </div>\r\n</form>",
        styles: [".export-data-modal-title{font-weight:700;flex-grow:1}mat-form-field{width:100%}.export-data-form{display:flex;flex-direction:column;width:100%}span.form-actions{display:flex;margin-left:auto;margin-bottom:10px}#bulk-comments-button{margin-right:0;border-radius:4px;margin-left:15px}.mat-icon-close-btn{float:right;top:-57px}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], ExportDataComponent);
export { ExportDataComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZXhwb3J0LWRhdGEvZXhwb3J0LWRhdGEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQU8xRixJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQWE5QixZQUNTLFNBQTRDLEVBQ25CLElBQVMsRUFDakMsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsYUFBNEIsRUFDNUIsY0FBOEIsRUFDL0IsbUJBQXdDO1FBTnhDLGNBQVMsR0FBVCxTQUFTLENBQW1DO1FBQ25CLFNBQUksR0FBSixJQUFJLENBQUs7UUFDakMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDL0Isd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVpqRCxlQUFVLEdBQUc7WUFDWCxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRTtZQUN0QyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRTtTQUNyQyxDQUFDO0lBVUksQ0FBQztJQUVMLFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNoRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDbEcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDMUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQzVCLENBQUM7SUFFRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbkIseUNBQXlDO1FBQ3pDLGtCQUFrQjtRQUNsQixvQkFBb0I7UUFDcEIsS0FBSztRQUNMLE1BQU07UUFDTixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2pRLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsK0VBQStFLENBQUMsQ0FBQztZQUNsSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUMsRUFBRSxHQUFHLEVBQUU7WUFDTixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztRQUVoRixDQUFDLENBQUMsQ0FBQztJQUNILENBQUM7SUFFRCxjQUFjO1FBQ1osTUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDM0MsY0FBYyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdEQsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDO0NBSUYsQ0FBQTs7WUF0RHFCLFlBQVk7NENBQzdCLE1BQU0sU0FBQyxlQUFlO1lBQ0gsVUFBVTtZQUNULFdBQVc7WUFDVCxhQUFhO1lBQ1osY0FBYztZQUNWLG1CQUFtQjs7QUFwQnRDLG1CQUFtQjtJQUwvQixTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsaUJBQWlCO1FBQzNCLHU1REFBMkM7O0tBRTVDLENBQUM7SUFnQkcsV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FmZixtQkFBbUIsQ0FvRS9CO1NBcEVZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1leHBvcnQtZGF0YScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2V4cG9ydC1kYXRhLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9leHBvcnQtZGF0YS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFeHBvcnREYXRhQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgZXhwb3J0RGF0YUZvcm06IEZvcm1Hcm91cDtcclxuICBhcHBDb25maWc7XHJcbiAgZmlsZVBhdGg7XHJcbiAgcGFnZUxpbWl0O1xyXG4gIGV4cG9ydERhdGFNaW5Db3VudDtcclxuXHJcbiAgZXhwb3J0VHlwZSA9IFtcclxuICAgIHsgdmFsdWU6ICdFWENFTCcsIHZpZXdWYWx1ZTogJ0V4Y2VsJyB9LFxyXG4gICAgeyB2YWx1ZTogJ0NTVicsIHZpZXdWYWx1ZTogJ0NTVicgfVxyXG5dO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxFeHBvcnREYXRhQ29tcG9uZW50PixcclxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55LFxyXG4gICAgcHJpdmF0ZSBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmluaXRpYWxpc2VGb3JtKCk7XHJcbiAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICB0aGlzLmZpbGVQYXRoID0gdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRVhQT1JUX0RBVEFfRklMRV9QQVRIXTtcclxuICAgIHRoaXMucGFnZUxpbWl0ID0gdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRVhQT1JUX0RBVEFfUEFHRV9MSU1JVF07XHJcbiAgICB0aGlzLmV4cG9ydERhdGFNaW5Db3VudCA9IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVYUE9SVF9EQVRBX01JTl9DT1VOVF07XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmRhdGEpXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmFwcENvbmZpZywnYXBwY29uZmlnJylcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZmlsZVBhdGgpXHJcbiAgfVxyXG5cclxuICBvbkV4cG9ydERhdGEoKXtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICBjb25zdCBpdGVtSWRzID0gW107XHJcbiAgICAvLyB0aGlzLmRhdGEuZXhwb3J0RGF0YS5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgLy8gICBpdGVtSWRzLnB1c2goXHJcbiAgICAvLyAgICAgIGRhdGEuSVRFTV9JRFxyXG4gICAgLy8gKTtcclxuICAgIC8vIH0pO1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLmV4cG9ydERhdGEodGhpcy5kYXRhLmNvbHVtbnMsIHRoaXMuZXhwb3J0RGF0YUZvcm0udmFsdWUuZXhwb3J0RmlsZU5hbWUsIHRoaXMuZXhwb3J0RGF0YUZvcm0udmFsdWUuZXhwb3J0VHlwZSwgaXRlbUlkcywgdGhpcy5maWxlUGF0aCwgdGhpcy5kYXRhLnNlYXJjaFJlcXVlc3QsIHRoaXMucGFnZUxpbWl0LCB0aGlzLmRhdGEudG90YWxDb3VudCwgdGhpcy5leHBvcnREYXRhTWluQ291bnQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKFwiRXhwb3J0IERhdGEgSW5pdGlhdGVkIFN1Y2Nlc3NmdWxseSEgQ2hlY2sgaW4gRXhwb3J0IERhdGEgVHJheSBmb3IgdGhlIGRldGFpbHNcIik7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGZhbHNlKTtcclxuICAgIH0sICgpID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBleHBvcnRpbmcgZGF0YScpO1xyXG4gIFxyXG4gIH0pO1xyXG4gIH1cclxuXHJcbiAgaW5pdGlhbGlzZUZvcm0oKSB7XHJcbiAgICBjb25zdCBkaXNhYmxlRmllbGQgPSBmYWxzZTtcclxuICAgIHRoaXMuZXhwb3J0RGF0YUZvcm0gPSBudWxsO1xyXG4gICAgdGhpcy5leHBvcnREYXRhRm9ybSA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICBleHBvcnRGaWxlTmFtZTogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCxWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApXSksXHJcbiAgICAgIGV4cG9ydFR5cGU6IG5ldyBGb3JtQ29udHJvbCgnJyxbVmFsaWRhdG9ycy5yZXF1aXJlZF0pXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNsb3NlRGlhbG9nKCkge1xyXG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoZmFsc2UpO1xyXG4gIH1cclxuXHJcblxyXG5cclxufVxyXG4iXX0=