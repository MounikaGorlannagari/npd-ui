import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let NameIconComponent = class NameIconComponent {
    constructor() { }
    ngOnInit() {
        if (this.userName && this.userName.length && this.userName.length > 0) {
            if (this.charCount) {
                this.userProfileCharacter = this.userName.substring(0, this.charCount).toUpperCase();
            }
            else if (this.userName) {
                this.userProfileCharacter = this.userName.charAt(0).toUpperCase();
            }
        }
        if (!this.color) {
            this.color = '';
        }
    }
};
__decorate([
    Input()
], NameIconComponent.prototype, "userName", void 0);
__decorate([
    Input()
], NameIconComponent.prototype, "color", void 0);
__decorate([
    Input()
], NameIconComponent.prototype, "charCount", void 0);
NameIconComponent = __decorate([
    Component({
        selector: 'mpm-name-icon',
        template: "<div class=\"user-profile-section\">\r\n    <span class=\"circled-name-letters\" style.background=\"{{color}}\">{{userProfileCharacter}}</span>\r\n</div>",
        styles: [".circled-name-letters{content:attr(data-letters);display:inline-block;font-size:16px;width:2.5em;line-height:2.5em;text-align:center;border-radius:50%;vertical-align:middle}"]
    })
], NameIconComponent);
export { NameIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmFtZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL25hbWUtaWNvbi9uYW1lLWljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVF6RCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQVExQixnQkFBZ0IsQ0FBQztJQUVqQixRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3hGO2lCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3JFO1NBQ0o7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1NBQ25CO0lBQ0wsQ0FBQztDQUVKLENBQUE7QUFyQlk7SUFBUixLQUFLLEVBQUU7bURBQVU7QUFDVDtJQUFSLEtBQUssRUFBRTtnREFBTztBQUNOO0lBQVIsS0FBSyxFQUFFO29EQUFXO0FBSlYsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLHFLQUF5Qzs7S0FFNUMsQ0FBQztHQUVXLGlCQUFpQixDQXVCN0I7U0F2QlksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLW5hbWUtaWNvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbmFtZS1pY29uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL25hbWUtaWNvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTmFtZUljb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHVzZXJOYW1lO1xyXG4gICAgQElucHV0KCkgY29sb3I7XHJcbiAgICBASW5wdXQoKSBjaGFyQ291bnQ7XHJcblxyXG4gICAgdXNlclByb2ZpbGVDaGFyYWN0ZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy51c2VyTmFtZSAmJiB0aGlzLnVzZXJOYW1lLmxlbmd0aCAmJiB0aGlzLnVzZXJOYW1lLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY2hhckNvdW50KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcm9maWxlQ2hhcmFjdGVyID0gdGhpcy51c2VyTmFtZS5zdWJzdHJpbmcoMCwgdGhpcy5jaGFyQ291bnQpLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy51c2VyTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VyUHJvZmlsZUNoYXJhY3RlciA9IHRoaXMudXNlck5hbWUuY2hhckF0KDApLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF0aGlzLmNvbG9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29sb3IgPSAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==