import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
let DownloadsTransferTrayModalComponent = class DownloadsTransferTrayModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'My Downloads';
    }
    cancelDialog() {
        this.dialogRef.close();
    }
};
DownloadsTransferTrayModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
DownloadsTransferTrayModalComponent = __decorate([
    Component({
        selector: 'mpm-downloads-transfer-tray-modal',
        template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-downloads-transfer-tray></mpm-downloads-transfer-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
        styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], DownloadsTransferTrayModalComponent);
export { DownloadsTransferTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFLElBQWEsbUNBQW1DLEdBQWhELE1BQWEsbUNBQW1DO0lBSTlDLFlBQ1MsU0FBdUQsRUFDOUIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUE4QztRQUM5QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxjQUFjLENBQUM7SUFLeEIsQ0FBQztJQUVMLFlBQVk7UUFDVixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3pCLENBQUM7Q0FFRixDQUFBOztZQVJxQixZQUFZOzRDQUM3QixNQUFNLFNBQUMsZUFBZTs7QUFOZCxtQ0FBbUM7SUFML0MsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLG1DQUFtQztRQUM3QyxvZ0JBQTZEOztLQUU5RCxDQUFDO0lBT0csV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FOZixtQ0FBbUMsQ0FhL0M7U0FiWSxtQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERvd25sb2Fkc1RyYW5zZmVyVHJheUNvbXBvbmVudCB9IGZyb20gJy4uL2Rvd25sb2Fkcy10cmFuc2Zlci10cmF5L2Rvd25sb2Fkcy10cmFuc2Zlci10cmF5LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWRvd25sb2Fkcy10cmFuc2Zlci10cmF5LW1vZGFsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2Rvd25sb2Fkcy10cmFuc2Zlci10cmF5LW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIERvd25sb2Fkc1RyYW5zZmVyVHJheU1vZGFsQ29tcG9uZW50IHtcclxuXHJcbiAgbW9kYWxUaXRsZSA9ICdNeSBEb3dubG9hZHMnO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxEb3dubG9hZHNUcmFuc2ZlclRyYXlDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhXHJcbiAgKSB7IH1cclxuXHJcbiAgY2FuY2VsRGlhbG9nKCkge1xyXG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==