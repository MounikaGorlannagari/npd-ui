import { __decorate, __param } from "tslib";
import { Component, Inject } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { RoleConstants } from ".././../../../lib/project/shared/constants/role.constants";
import { Observable } from "rxjs";
import { LoaderService } from "../../../loader/loader.service";
import { AppService } from "../../../mpm-utils/services/app.service";
import { SharingService } from "../../../mpm-utils/services/sharing.service";
import { NotificationService } from "../../../notification/notification.service";
let EmailNotificationPreferenceComponent = class EmailNotificationPreferenceComponent {
    constructor(dialogRef, data, sharingService, appService, formBuilder, loaderService, notification) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.sharingService = sharingService;
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.loaderService = loaderService;
        this.notification = notification;
        this.isPM = false;
        this.isUser = false;
        this.allEmailEvents = [];
        this.addedEvents = [];
        this.modifiedEvents = [];
        this.modifiedEventsFlag = 0;
        this.enabledEvents = [];
        this.disabledEvents = [];
        this.displayEmailEvents = [];
        this.userPreferenceFlag = 0;
        this.userPreferenceEmailEvent = [];
        this.hasUserPreference = false;
        this.commonEvents = [];
        this.notificationPreferenceFlag = 0;
        this.showNotification = false;
        this.hasUserEmailPreference = false;
        this.selectAll = "Select All";
        this.restore = false;
    }
    setSelectAll(event) {
        this.displayEmailEvents.forEach((emailLists) => {
            if (!(event) == Boolean(JSON.parse(emailLists.isChecked))) {
                this.masterToggle(event, emailLists.Name, emailLists.Id, emailLists);
            }
            emailLists.isChecked = event;
        });
    }
    setAllCheck(emailEventLists) {
        this.allCheck = emailEventLists.every((emailevent) => emailevent.isChecked != false);
    }
    closeDialog() {
        this.dialogRef.close(false);
    }
    restoreDefault() {
        this.loaderService.show();
        const userPreferenceID = {
            Id: this.userEmailPreferenceId
        };
        this.appService.DeleteMPM_Email_Preference(userPreferenceID).subscribe(response => {
            this.hasUserEmailPreference = false;
            this.emailPreferences(this.isPM, this.isUser);
        });
    }
    masterToggle(event, eventName, id, emailEvent) {
        emailEvent.isChecked = event;
        this.setAllCheck(this.displayEmailEvents);
        this.intermediateCheck = {
            Name: eventName.replace(/_/g, ' '),
            Id: id,
            isChecked: event
        };
        if (Array.isArray(this.modifiedEvents) && this.modifiedEvents.length > 0) {
            this.modifiedEventsFlag = 0;
            this.modifiedEvents.forEach((element, index) => {
                if (element.Name === eventName.replace(/_/g, ' ')) {
                    this.modifiedEvents.splice(index, 1);
                    this.modifiedEventsFlag = 1;
                }
            });
            if (this.modifiedEventsFlag !== 1) {
                this.modifiedEvents.push(this.intermediateCheck);
            }
        }
        else {
            this.modifiedEvents = [];
            this.modifiedEvents.push(this.intermediateCheck);
        }
    }
    notificationPreference() {
        this.addedEvents = [];
        this.enabledEvents = [];
        this.disabledEvents = [];
        this.showNotification = false;
        if (this.hasUserPreference) {
            this.showNotification = false;
            if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                this.modifiedEvents.forEach((element, index) => {
                    this.notificationPreferenceFlag = 0;
                    if (this.userPreferenceEmailEvent.length > 0) {
                        this.userPreferenceEmailEvent.forEach(userEmailEvent => {
                            if (element.Id === userEmailEvent.Id) {
                                this.commonEvents.push(element);
                                this.notificationPreferenceFlag = 1;
                            }
                        });
                    }
                    if (this.notificationPreferenceFlag === 0) {
                        this.addedEvents.push(element);
                    }
                });
            }
            else if (this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0) {
                if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                    this.showNotification = true;
                }
            }
        }
        else {
            if (this.modifiedEvents && this.modifiedEvents.length > 0 && this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0) {
            }
            else {
                if (this.displayEmailEvents && this.displayEmailEvents.length > 0) {
                    this.displayEmailEvents.forEach((element, index) => {
                        this.notificationPreferenceFlag = 0;
                        if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                            this.modifiedEvents.forEach(userEmailEvent => {
                                if (element.Id === userEmailEvent.Id) {
                                    this.notificationPreferenceFlag = 1;
                                }
                            });
                        }
                        if (this.notificationPreferenceFlag === 0) {
                            this.addedEvents.push(element);
                        }
                    });
                }
            }
        }
        this.addedEvents.forEach(emailEvent => {
            this.enabledEvents.push({
                Id: emailEvent.Id
            });
        });
        if (this.commonEvents && this.commonEvents.length > 0) {
            this.commonEvents.forEach(emailEvent => {
                this.disabledEvents.push({
                    Id: emailEvent.Id
                });
            });
        }
        if ((this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0)) {
            if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                const parameters = {
                    userCN: this.userID
                };
                this.appService.getPersonByUserId(parameters).subscribe(response => {
                    this.userId = response.Id;
                    this.loaderService.show();
                    this.appService.MailPreference(this.userId, this.enabledEvents, this.disabledEvents).subscribe(responses => {
                        this.loaderService.hide();
                        this.notification.info('Subscription saved successfully');
                        this.closeDialog();
                    });
                });
            }
            else {
                this.notification.error('No custom preference to subscribe');
            }
        }
        else {
            const parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(response => {
                this.userId = response.Id;
                this.loaderService.show();
                this.appService.MailPreference(this.userId, this.enabledEvents, this.disabledEvents).subscribe(responses => {
                    this.loaderService.hide();
                    this.notification.info('Subscription saved successfully');
                    this.closeDialog();
                });
            });
        }
    }
    emailEventsForManager() {
        return new Observable(observer => {
            this.appService.GetEmailEventsForManager().subscribe(managerEventResponse => {
                this.allEmailEvents = [];
                if (managerEventResponse && managerEventResponse.Email_Event_Template) {
                    managerEventResponse.Email_Event_Template.forEach(emailEvent => {
                        this.emailEvents = {
                            Name: emailEvent.EVENT_ID.replace(/_/g, ' '),
                            Id: emailEvent['Email_Event_Template-id'].Id,
                            isChecked: emailEvent.MANAGER_EVENT
                        };
                        this.allEmailEvents.push(this.emailEvents);
                    });
                }
                observer.next(this.allEmailEvents);
                observer.complete();
            });
        });
    }
    emailEventsForUser() {
        return new Observable(observer => {
            this.appService.GetEmailEventsForUser().subscribe(userEventResponse => {
                this.allEmailEvents = [];
                if (userEventResponse && userEventResponse.Email_Event_Template) {
                    userEventResponse.Email_Event_Template.forEach(emailEvent => {
                        this.emailEvents = {
                            Name: emailEvent.EVENT_ID.replace(/_/g, ' '),
                            Id: emailEvent['Email_Event_Template-id'].Id,
                            isChecked: emailEvent.USER_EVENT
                        };
                        this.allEmailEvents.push(this.emailEvents);
                    });
                }
                observer.next(this.allEmailEvents);
                observer.complete();
            });
        });
    }
    removeDuplicateEvents(allEvents) {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < allEvents.length; i++) {
            for (let j = i + 1; j < allEvents.length; j++) {
                if (allEvents[i].Id === allEvents[j].Id) {
                    for (let k = j; k < allEvents.length; k++) {
                        allEvents[k] = allEvents[k + 1];
                    }
                    allEvents.length--;
                    j--;
                }
            }
        }
        return allEvents;
    }
    emailPreferences(isPM, isUser) {
        if (isPM && isUser) {
            const parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(personResponse => {
                this.userId = personResponse.Id;
                this.appService.GetMailPreferenceByUserId(this.userId).subscribe(preferenceResponse => {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        this.hasUserEmailPreference = true;
                        this.appService.GetUserPreferenceMailTemplates(this.userID).subscribe(userPrefernceMailTemplateResponse => {
                            this.emailEventsForManager().subscribe(allManagerEvents => {
                                this.emailEventsForUser().subscribe(allUserEvents => {
                                    const allEvents = allManagerEvents.concat(allUserEvents);
                                    const allEvent = this.removeDuplicateEvents(allEvents);
                                    this.hasUserPreference = true;
                                    if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                        this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                    }
                                    else {
                                        this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                    }
                                    this.loaderService.show();
                                    allEvent.forEach(events => {
                                        this.userPreferenceFlag = 0;
                                        // userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => {
                                        if (this.userPreferenceEmailEvent.length > 0) {
                                            this.userPreferenceEmailEvent.forEach(userPreference => {
                                                if (events.Id === userPreference.Id) {
                                                    this.userPreferenceFlag = 1;
                                                    this.displayEmailEvents.push(events);
                                                }
                                            });
                                        }
                                        if (this.userPreferenceFlag === 0) {
                                            events.isChecked = false;
                                            this.displayEmailEvents.push(events);
                                        }
                                    });
                                    this.setAllCheck(this.displayEmailEvents);
                                    this.loaderService.hide();
                                });
                            });
                        });
                    }
                    else {
                        this.loaderService.show();
                        this.modifiedEvents = [];
                        this.userPreferenceEmailEvent = [];
                        this.hasUserPreference = false;
                        this.emailEventsForManager().subscribe(allManagerEvents => {
                            this.emailEventsForUser().subscribe(allUserEvents => {
                                const allEvents = allManagerEvents.concat(allUserEvents);
                                const allEvent = this.removeDuplicateEvents(allEvents);
                                this.loaderService.hide();
                                this.displayEmailEvents = allEvent;
                                this.setAllCheck(this.displayEmailEvents);
                            });
                        });
                    }
                });
            });
        }
        else if (isPM) {
            const parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(personResponse => {
                this.userId = personResponse.Id;
                this.appService.GetMailPreferenceByUserId(this.userId).subscribe(preferenceResponse => {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        this.hasUserEmailPreference = true;
                        this.appService.GetUserPreferenceMailTemplates(this.userID).subscribe(userPrefernceMailTemplateResponse => {
                            this.emailEventsForManager().subscribe(allEvents => {
                                this.hasUserPreference = true;
                                if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                    this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                }
                                else {
                                    this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                }
                                this.loaderService.show();
                                allEvents.forEach(events => {
                                    this.userPreferenceFlag = 0;
                                    /* userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => { */
                                    if (this.userPreferenceEmailEvent.length > 0) {
                                        this.userPreferenceEmailEvent.forEach(userPreference => {
                                            if (events.Id === userPreference.Id) {
                                                this.userPreferenceFlag = 1;
                                                this.displayEmailEvents.push(events);
                                            }
                                        });
                                    }
                                    if (this.userPreferenceFlag === 0) {
                                        events.isChecked = false;
                                        this.displayEmailEvents.push(events);
                                    }
                                });
                                this.setAllCheck(this.displayEmailEvents);
                                this.loaderService.hide();
                            });
                        });
                    }
                    else {
                        this.loaderService.show();
                        this.modifiedEvents = [];
                        this.userPreferenceEmailEvent = [];
                        this.hasUserPreference = false;
                        this.emailEventsForManager().subscribe(allEvents => {
                            this.loaderService.hide();
                            this.displayEmailEvents = allEvents;
                            this.setAllCheck(this.displayEmailEvents);
                        });
                    }
                });
            });
        }
        else {
            const parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(personResponse => {
                this.userId = personResponse.Id;
                this.appService.GetMailPreferenceByUserId(this.userId).subscribe(preferenceResponse => {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        this.hasUserEmailPreference = true;
                        this.appService.GetUserPreferenceMailTemplates(this.userID).subscribe(userPrefernceMailTemplateResponse => {
                            this.emailEventsForUser().subscribe(allEvents => {
                                this.hasUserPreference = true;
                                if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                    this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                }
                                else {
                                    this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                }
                                //this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                this.loaderService.show();
                                allEvents.forEach(events => {
                                    this.userPreferenceFlag = 0;
                                    //  userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => {
                                    if (this.userPreferenceEmailEvent.length > 0) {
                                        this.userPreferenceEmailEvent.forEach(userPreference => {
                                            if (events.Id === userPreference.Id) {
                                                this.userPreferenceFlag = 1;
                                                this.displayEmailEvents.push(events);
                                            }
                                        });
                                    }
                                    if (this.userPreferenceFlag === 0) {
                                        events.isChecked = false;
                                        this.displayEmailEvents.push(events);
                                    }
                                });
                                this.setAllCheck(this.displayEmailEvents);
                                this.loaderService.hide();
                            });
                        });
                    }
                    else {
                        this.loaderService.show();
                        this.modifiedEvents = [];
                        this.userPreferenceEmailEvent = [];
                        this.hasUserPreference = false;
                        this.emailEventsForUser().subscribe(allEvents => {
                            this.loaderService.hide();
                            this.displayEmailEvents = allEvents;
                            this.setAllCheck(this.displayEmailEvents);
                        });
                    }
                });
            });
        }
    }
    // ngOnChanges(changes: SimpleChanges) {
    //     console.log(changes);
    // }
    ngOnInit() {
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach((user) => {
            if (user.Name === RoleConstants.PROJECT_MANAGER) {
                this.isPM = true;
            }
            else if (user.Name.includes(RoleConstants.PROJECT_MEMBER) ||
                user.Name.includes(RoleConstants.PROJECT_APPROVER) ||
                user.Name.includes(RoleConstants.PROJECT_REVIEWER)) {
                this.isUser = true;
            }
        });
        this.EmailEventFormGroup = this.formBuilder.group({
            emailPreference: new FormControl(),
        });
        this.userID = this.sharingService.getCurrentUserID();
        this.loaderService.show();
        this.emailPreferences(this.isPM, this.isUser);
    }
};
EmailNotificationPreferenceComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: SharingService },
    { type: AppService },
    { type: FormBuilder },
    { type: LoaderService },
    { type: NotificationService }
];
EmailNotificationPreferenceComponent = __decorate([
    Component({
        selector: "mpm-email-notification-preference",
        template: "<div class=\"flex-container\">\r\n    <div class=\"headerAlign\">\r\n        <h1 mat-dialog-title class=\"bulk-comments-modal-title\">\r\n            {{ data.message }}\r\n        </h1>\r\n    </div>\r\n    <div>\r\n        <button matTooltip=\"Close\" mat-icon-button color=\"primary\" (click)=\"closeDialog()\" [mat-dialog-close]=\"true\"\r\n            class=\"mat-icon-close-btn\">\r\n            <mat-icon>close</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<form [formGroup]=\"EmailEventFormGroup\">\r\n    <mat-checkbox [checked]=\"allCheck\" (change)=\"setSelectAll($event.checked)\">\r\n        <strong>{{ selectAll}}</strong>\r\n    </mat-checkbox>\r\n\r\n    <div class=\"email-lists\">\r\n        <div *ngFor=\"let emailEvent of displayEmailEvents\">\r\n            <mat-checkbox [checked]=\"emailEvent.isChecked\" (change)=\"\r\n                    $event\r\n                        ? masterToggle($event.checked, emailEvent.Name, emailEvent.Id,emailEvent)\r\n                        : null\r\n                \">\r\n                <span>{{ emailEvent.Name | titlecase }}</span>\r\n            </mat-checkbox>\r\n        </div>\r\n    </div>\r\n    <div class=\"button\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"form-actions\">\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Cancel\" (click)=\"closeDialog()\">\r\n                        Cancel\r\n                    </button>\r\n                    &nbsp;\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Save\" (click)=\"notificationPreference()\"\r\n                        [disabled]=\"EmailEventFormGroup.invalid\">\r\n                        Save\r\n                    </button>\r\n                    &nbsp;\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Restore Default\" (click)=\"restoreDefault()\"\r\n                        [disabled]=\"!hasUserEmailPreference\">\r\n                        Restore Default\r\n                    </button>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n<!-- <div class=\"example-container mat-elevation-z8\">\r\n        <table mat-table [dataSource]=\"displayEmailEvents\">\r\n    \r\n            <ng-container >\r\n    \r\n                <td mat-cell *matCellDef=\"let emailEvent\">\r\n                    <mat-checkbox [checked]=\"emailEvent.isChecked\"\r\n                        (change)=\"$event ? masterToggle($event,emailEvent.Name,emailEvent.Id) : null\">\r\n                    </mat-checkbox>\r\n                </td>\r\n    \r\n            </ng-container>\r\n    \r\n    \r\n            <ng-container >\r\n    \r\n                <td mat-cell *matCellDef=\"let emailEvent\"> <span>{{emailEvent.Name | titlecase}}</span> </td>\r\n    \r\n            </ng-container>\r\n    \r\n    \r\n            <tr mat-row *matRowDef=\"let row;\"></tr>\r\n           \r\n        </table>\r\n    </div> -->",
        styles: [".form-actions{margin-left:auto}.bulk-comments-modal-title{font-weight:700;flex-grow:1}.flex-container{display:flex}.headerAlign{flex-grow:5}.email-lists{height:62vh;margin-top:2%;overflow-y:scroll;overflow-x:hidden}.button{margin-top:30px}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], EmailNotificationPreferenceComponent);
export { EmailNotificationPreferenceComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UvZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDekUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkRBQTJELENBQUM7QUFDMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQU9qRixJQUFhLG9DQUFvQyxHQUFqRCxNQUFhLG9DQUFvQztJQTRCN0MsWUFDVyxTQUE2RCxFQUNwQyxJQUFTLEVBQ2xDLGNBQThCLEVBQzlCLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3ZCLGFBQTRCLEVBQzVCLFlBQWlDO1FBTmxDLGNBQVMsR0FBVCxTQUFTLENBQW9EO1FBQ3BDLFNBQUksR0FBSixJQUFJLENBQUs7UUFDbEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBakM3QyxTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2IsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUVmLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBR3BCLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHVCQUFrQixHQUFHLENBQUMsQ0FBQztRQUV2QixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDeEIsdUJBQWtCLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLDZCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUM5QixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsK0JBQTBCLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUV6QiwyQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDL0IsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUV6QixZQUFPLEdBQUcsS0FBSyxDQUFDO0lBVVosQ0FBQztJQUVMLFlBQVksQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQzNDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFO2dCQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUE7YUFDdkU7WUFDRCxVQUFVLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxXQUFXLENBQUMsZUFBZTtRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsY0FBYztRQUNWLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsTUFBTSxnQkFBZ0IsR0FBRztZQUNyQixFQUFFLEVBQUUsSUFBSSxDQUFDLHFCQUFxQjtTQUNqQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQywwQkFBMEIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM5RSxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxZQUFZLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsVUFBVTtRQUN6QyxVQUFVLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBRTFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRztZQUNyQixJQUFJLEVBQUUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO1lBQ2xDLEVBQUUsRUFBRSxFQUFFO1lBQ04sU0FBUyxFQUFFLEtBQUs7U0FDbkIsQ0FBQztRQUNGLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQzNDLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRTtvQkFDL0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO2lCQUMvQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssQ0FBQyxFQUFFO2dCQUMvQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUNwRDtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7SUFFRCxzQkFBc0I7UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3ZELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUMzQyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMxQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFOzRCQUNuRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtnQ0FFbEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0NBQ2hDLElBQUksQ0FBQywwQkFBMEIsR0FBRyxDQUFDLENBQUM7NkJBRXZDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELElBQUksSUFBSSxDQUFDLDBCQUEwQixLQUFLLENBQUMsRUFBRTt3QkFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2xDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU0sSUFBSSxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xGLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQ2hDO2FBQ0o7U0FDSjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7YUFDdkk7aUJBQU07Z0JBRUgsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQy9ELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7d0JBQy9DLElBQUksQ0FBQywwQkFBMEIsR0FBRyxDQUFDLENBQUM7d0JBQ3BDLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3ZELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dDQUN6QyxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtvQ0FDbEMsSUFBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsQ0FBQztpQ0FDdkM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBQ0QsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEtBQUssQ0FBQyxFQUFFOzRCQUN2QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDbEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtTQUNKO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLEVBQUUsRUFBRSxVQUFVLENBQUMsRUFBRTthQUNwQixDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO29CQUNyQixFQUFFLEVBQUUsVUFBVSxDQUFDLEVBQUU7aUJBQ3BCLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDN0UsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkQsTUFBTSxVQUFVLEdBQUc7b0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2lCQUN0QixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMvRCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUN2RyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO3dCQUMxRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3ZCLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQzthQUNoRTtTQUNKO2FBQU07WUFDSCxNQUFNLFVBQVUsR0FBRztnQkFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07YUFDdEIsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUMvRCxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO29CQUN2RyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO29CQUMxRCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCxxQkFBcUI7UUFDakIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLHdCQUF3QixFQUFFLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLEVBQUU7Z0JBQ3hFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLG9CQUFvQixFQUFFO29CQUNuRSxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7d0JBQzNELElBQUksQ0FBQyxXQUFXLEdBQUc7NEJBQ2YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7NEJBQzVDLEVBQUUsRUFBRSxVQUFVLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFOzRCQUM1QyxTQUFTLEVBQUUsVUFBVSxDQUFDLGFBQWE7eUJBQ3RDLENBQUM7d0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMvQyxDQUFDLENBQUMsQ0FBQztpQkFDTjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0JBQ2xFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLG9CQUFvQixFQUFFO29CQUM3RCxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7d0JBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUc7NEJBQ2YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7NEJBQzVDLEVBQUUsRUFBRSxVQUFVLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFOzRCQUM1QyxTQUFTLEVBQUUsVUFBVSxDQUFDLFVBQVU7eUJBQ25DLENBQUM7d0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMvQyxDQUFDLENBQUMsQ0FBQztpQkFDTjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUJBQXFCLENBQUMsU0FBUztRQUMzQiwwQ0FBMEM7UUFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdkMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUMzQyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ3ZDLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3FCQUNuQztvQkFDRCxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ25CLENBQUMsRUFBRSxDQUFDO2lCQUNQO2FBQ0o7U0FDSjtRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsTUFBTTtRQUN6QixJQUFJLElBQUksSUFBSSxNQUFNLEVBQUU7WUFDaEIsTUFBTSxVQUFVLEdBQUc7Z0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2FBQ3RCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDckUsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQUMsRUFBRSxDQUFDO2dCQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTtvQkFDbEYsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxvQkFBb0I7d0JBQzdELGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLElBQUksa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFLEVBQUU7d0JBQzdJLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3QkFDbkcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQzt3QkFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLGlDQUFpQyxDQUFDLEVBQUU7NEJBQ3RHLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2dDQUN0RCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7b0NBQ2hELE1BQU0sU0FBUyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztvQ0FDekQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUN2RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO29DQUM5QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDcE0sSUFBSSxDQUFDLHdCQUF3QixHQUFHLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUM7cUNBQ3RIO3lDQUFNO3dDQUNILElBQUksQ0FBQyx3QkFBd0IsR0FBRyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQ0FDaE87b0NBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQ0FDMUIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTt3Q0FDdEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQzt3Q0FDNUIsaUhBQWlIO3dDQUNqSCxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRDQUMxQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dEQUNuRCxJQUFJLE1BQU0sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtvREFDakMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztvREFDNUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpREFDeEM7NENBQ0wsQ0FBQyxDQUFDLENBQUM7eUNBQ047d0NBQ0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssQ0FBQyxFQUFFOzRDQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzs0Q0FDekIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt5Q0FDeEM7b0NBQ0wsQ0FBQyxDQUFDLENBQUM7b0NBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztvQ0FDMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDOUIsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7d0JBQ3pCLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUM7d0JBQ25DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7d0JBQy9CLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFOzRCQUN0RCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0NBQ2hELE1BQU0sU0FBUyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztnQ0FDekQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2dDQUN2RCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dDQUMxQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsUUFBUSxDQUFDO2dDQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDOzRCQUM5QyxDQUFDLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFFTjtnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBRU47YUFBTSxJQUFJLElBQUksRUFBRTtZQUNiLE1BQU0sVUFBVSxHQUFHO2dCQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTthQUN0QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3JFLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7b0JBQ2xGLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsb0JBQW9CO3dCQUM3RCxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUM3SSxJQUFJLENBQUMscUJBQXFCLEdBQUcsa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ25HLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7d0JBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxFQUFFOzRCQUN0RyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0NBQy9DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0NBQzlCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29DQUNwTSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztpQ0FDdEg7cUNBQU07b0NBQ0gsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNoTztnQ0FDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dDQUMxQixTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO29DQUN2QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO29DQUM1QixvSEFBb0g7b0NBRXBILElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0NBQzFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7NENBQ25ELElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxjQUFjLENBQUMsRUFBRSxFQUFFO2dEQUNqQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO2dEQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzZDQUN4Qzt3Q0FDTCxDQUFDLENBQUMsQ0FBQztxQ0FDTjtvQ0FDRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxDQUFDLEVBQUU7d0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO3dDQUN6QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FDQUN4QztnQ0FFTCxDQUFDLENBQUMsQ0FBQztnQ0FDSCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dDQUMxQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDOzRCQUM5QixDQUFDLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjt5QkFBTTt3QkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzt3QkFDekIsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQzt3QkFDbkMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQzt3QkFDL0IsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFOzRCQUMvQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDOzRCQUMxQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFDOzRCQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO3dCQUM5QyxDQUFDLENBQUMsQ0FBQztxQkFDTjtnQkFDTCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILE1BQU0sVUFBVSxHQUFHO2dCQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTthQUN0QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3JFLElBQUksQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7b0JBQ2xGLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsb0JBQW9CO3dCQUM3RCxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUM3SSxJQUFJLENBQUMscUJBQXFCLEdBQUcsa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ25HLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7d0JBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsOEJBQThCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxFQUFFOzRCQUN0RyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7Z0NBQzVDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0NBQzlCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29DQUNwTSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztpQ0FDdEg7cUNBQU07b0NBQ0gsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNoTztnQ0FDRCxxSEFBcUg7Z0NBQ3JILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLFNBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7b0NBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7b0NBQzVCLGtIQUFrSDtvQ0FDbEgsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDMUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTs0Q0FDbkQsSUFBSSxNQUFNLENBQUMsRUFBRSxLQUFLLGNBQWMsQ0FBQyxFQUFFLEVBQUU7Z0RBQ2pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7Z0RBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NkNBQ3hDO3dDQUNMLENBQUMsQ0FBQyxDQUFDO3FDQUNOO29DQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixLQUFLLENBQUMsRUFBRTt3Q0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7d0NBQ3pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7cUNBQ3hDO2dDQUVMLENBQUMsQ0FBQyxDQUFDO2dDQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0NBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7NEJBQzlCLENBQUMsQ0FBQyxDQUFDO3dCQUNQLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO3dCQUN6QixJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO3dCQUMvQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEVBQUU7NEJBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7NEJBQzFCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUM7NEJBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQzlDLENBQUMsQ0FBQyxDQUFDO3FCQUVOO2dCQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCx3Q0FBd0M7SUFDeEMsNEJBQTRCO0lBQzVCLElBQUk7SUFFSixRQUFRO1FBQ0osSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDL0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsZUFBZSxFQUFFO2dCQUM3QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUNwQjtpQkFBTSxJQUNILElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEVBQ3BEO2dCQUNFLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDOUMsZUFBZSxFQUFFLElBQUksV0FBVyxFQUFFO1NBQ3JDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xELENBQUM7Q0FDSixDQUFBOztZQWxheUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7WUFDQSxjQUFjO1lBQ2xCLFVBQVU7WUFDVCxXQUFXO1lBQ1IsYUFBYTtZQUNkLG1CQUFtQjs7QUFuQ3BDLG9DQUFvQztJQUxoRCxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsbUNBQW1DO1FBQzdDLG0vRkFBNkQ7O0tBRWhFLENBQUM7SUErQk8sV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0E5Qm5CLG9DQUFvQyxDQStiaEQ7U0EvYlksb0NBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2RpYWxvZ1wiO1xyXG5pbXBvcnQgeyBSb2xlQ29uc3RhbnRzIH0gZnJvbSBcIi4uLy4vLi4vLi4vLi4vbGliL3Byb2plY3Qvc2hhcmVkL2NvbnN0YW50cy9yb2xlLmNvbnN0YW50c1wiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJtcG0tZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2VcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wiLi9lbWFpbC1ub3RpZmljYXRpb24tcHJlZmVyZW5jZS5jb21wb25lbnQuY3NzXCJdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGxvZ2dlZEluVXNlcjtcclxuICAgIGlzUE0gPSBmYWxzZTtcclxuICAgIGlzVXNlciA9IGZhbHNlO1xyXG4gICAgZW1haWxFdmVudHM7XHJcbiAgICBhbGxFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgRW1haWxFdmVudEZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gICAgaW50ZXJtZWRpYXRlQ2hlY2s7XHJcbiAgICBhZGRlZEV2ZW50czogYW55W10gPSBbXTtcclxuICAgIG1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICBtb2RpZmllZEV2ZW50c0ZsYWcgPSAwO1xyXG4gICAgdXNlcklkO1xyXG4gICAgZW5hYmxlZEV2ZW50cyA9IFtdO1xyXG4gICAgZGlzYWJsZWRFdmVudHMgPSBbXTtcclxuICAgIHVzZXJJRDtcclxuICAgIGRpc3BsYXlFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgdXNlclByZWZlcmVuY2VGbGFnID0gMDtcclxuICAgIHVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IFtdO1xyXG4gICAgaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgIGNvbW1vbkV2ZW50cyA9IFtdO1xyXG4gICAgbm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgdXNlckVtYWlsUHJlZmVyZW5jZUlkO1xyXG4gICAgaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IGZhbHNlO1xyXG4gICAgc2VsZWN0QWxsID0gXCJTZWxlY3QgQWxsXCI7XHJcbiAgICBhbGxDaGVjazogQm9vbGVhbjtcclxuICAgIHJlc3RvcmUgPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8RW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHNldFNlbGVjdEFsbChldmVudCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzLmZvckVhY2goKGVtYWlsTGlzdHMpID0+IHtcclxuICAgICAgICAgICAgaWYgKCEoZXZlbnQpID09IEJvb2xlYW4oSlNPTi5wYXJzZShlbWFpbExpc3RzLmlzQ2hlY2tlZCkpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hc3RlclRvZ2dsZShldmVudCwgZW1haWxMaXN0cy5OYW1lLCBlbWFpbExpc3RzLklkLCBlbWFpbExpc3RzKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVtYWlsTGlzdHMuaXNDaGVja2VkID0gZXZlbnQ7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbENoZWNrKGVtYWlsRXZlbnRMaXN0cykge1xyXG4gICAgICAgIHRoaXMuYWxsQ2hlY2sgPSBlbWFpbEV2ZW50TGlzdHMuZXZlcnkoKGVtYWlsZXZlbnQpID0+IGVtYWlsZXZlbnQuaXNDaGVja2VkICE9IGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZURpYWxvZygpIHtcclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzdG9yZURlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBjb25zdCB1c2VyUHJlZmVyZW5jZUlEID0ge1xyXG4gICAgICAgICAgICBJZDogdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5EZWxldGVNUE1fRW1haWxfUHJlZmVyZW5jZSh1c2VyUHJlZmVyZW5jZUlEKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmhhc1VzZXJFbWFpbFByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lbWFpbFByZWZlcmVuY2VzKHRoaXMuaXNQTSwgdGhpcy5pc1VzZXIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBtYXN0ZXJUb2dnbGUoZXZlbnQsIGV2ZW50TmFtZSwgaWQsIGVtYWlsRXZlbnQpIHtcclxuICAgICAgICBlbWFpbEV2ZW50LmlzQ2hlY2tlZCA9IGV2ZW50O1xyXG4gICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG5cclxuICAgICAgICB0aGlzLmludGVybWVkaWF0ZUNoZWNrID0ge1xyXG4gICAgICAgICAgICBOYW1lOiBldmVudE5hbWUucmVwbGFjZSgvXy9nLCAnICcpLFxyXG4gICAgICAgICAgICBJZDogaWQsXHJcbiAgICAgICAgICAgIGlzQ2hlY2tlZDogZXZlbnRcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMubW9kaWZpZWRFdmVudHMpICYmIHRoaXMubW9kaWZpZWRFdmVudHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzRmxhZyA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50Lk5hbWUgPT09IGV2ZW50TmFtZS5yZXBsYWNlKC9fL2csICcgJykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RpZmllZEV2ZW50c0ZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHNGbGFnICE9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzLnB1c2godGhpcy5pbnRlcm1lZGlhdGVDaGVjayk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMucHVzaCh0aGlzLmludGVybWVkaWF0ZUNoZWNrKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbm90aWZpY2F0aW9uUHJlZmVyZW5jZSgpIHtcclxuICAgICAgICB0aGlzLmFkZGVkRXZlbnRzID0gW107XHJcbiAgICAgICAgdGhpcy5lbmFibGVkRXZlbnRzID0gW107XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlZEV2ZW50cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmhhc1VzZXJQcmVmZXJlbmNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5mb3JFYWNoKHVzZXJFbWFpbEV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LklkID09PSB1c2VyRW1haWxFdmVudC5JZCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbW1vbkV2ZW50cy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkZWRFdmVudHMucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCAmJiB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNob3dOb3RpZmljYXRpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHMgJiYgdGhpcy5tb2RpZmllZEV2ZW50cy5sZW5ndGggPiAwICYmIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ICYmIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kaXNwbGF5RW1haWxFdmVudHMgJiYgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzLmZvckVhY2goKGVsZW1lbnQsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCh1c2VyRW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuSWQgPT09IHVzZXJFbWFpbEV2ZW50LklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZGVkRXZlbnRzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmFkZGVkRXZlbnRzLmZvckVhY2goZW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlZEV2ZW50cy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIElkOiBlbWFpbEV2ZW50LklkXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLmNvbW1vbkV2ZW50cyAmJiB0aGlzLmNvbW1vbkV2ZW50cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbW9uRXZlbnRzLmZvckVhY2goZW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVkRXZlbnRzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBlbWFpbEV2ZW50LklkXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgJiYgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHMgJiYgdGhpcy5tb2RpZmllZEV2ZW50cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLk1haWxQcmVmZXJlbmNlKHRoaXMudXNlcklkLCB0aGlzLmVuYWJsZWRFdmVudHMsIHRoaXMuZGlzYWJsZWRFdmVudHMpLnN1YnNjcmliZShyZXNwb25zZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbi5pbmZvKCdTdWJzY3JpcHRpb24gc2F2ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VEaWFsb2coKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ05vIGN1c3RvbSBwcmVmZXJlbmNlIHRvIHN1YnNjcmliZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5NYWlsUHJlZmVyZW5jZSh0aGlzLnVzZXJJZCwgdGhpcy5lbmFibGVkRXZlbnRzLCB0aGlzLmRpc2FibGVkRXZlbnRzKS5zdWJzY3JpYmUocmVzcG9uc2VzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uLmluZm8oJ1N1YnNjcmlwdGlvbiBzYXZlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlRGlhbG9nKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGVtYWlsRXZlbnRzRm9yTWFuYWdlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRFbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUobWFuYWdlckV2ZW50UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbGxFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1hbmFnZXJFdmVudFJlc3BvbnNlICYmIG1hbmFnZXJFdmVudFJlc3BvbnNlLkVtYWlsX0V2ZW50X1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFuYWdlckV2ZW50UmVzcG9uc2UuRW1haWxfRXZlbnRfVGVtcGxhdGUuZm9yRWFjaChlbWFpbEV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50cyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5hbWU6IGVtYWlsRXZlbnQuRVZFTlRfSUQucmVwbGFjZSgvXy9nLCAnICcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IGVtYWlsRXZlbnRbJ0VtYWlsX0V2ZW50X1RlbXBsYXRlLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0NoZWNrZWQ6IGVtYWlsRXZlbnQuTUFOQUdFUl9FVkVOVFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbEVtYWlsRXZlbnRzLnB1c2godGhpcy5lbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsRW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZW1haWxFdmVudHNGb3JVc2VyKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldEVtYWlsRXZlbnRzRm9yVXNlcigpLnN1YnNjcmliZSh1c2VyRXZlbnRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFsbEVtYWlsRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlckV2ZW50UmVzcG9uc2UgJiYgdXNlckV2ZW50UmVzcG9uc2UuRW1haWxfRXZlbnRfVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VyRXZlbnRSZXNwb25zZS5FbWFpbF9FdmVudF9UZW1wbGF0ZS5mb3JFYWNoKGVtYWlsRXZlbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTmFtZTogZW1haWxFdmVudC5FVkVOVF9JRC5yZXBsYWNlKC9fL2csICcgJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogZW1haWxFdmVudFsnRW1haWxfRXZlbnRfVGVtcGxhdGUtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQ2hlY2tlZDogZW1haWxFdmVudC5VU0VSX0VWRU5UXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsRW1haWxFdmVudHMucHVzaCh0aGlzLmVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hbGxFbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVEdXBsaWNhdGVFdmVudHMoYWxsRXZlbnRzKSB7XHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBwcmVmZXItZm9yLW9mXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxFdmVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgYWxsRXZlbnRzLmxlbmd0aDsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoYWxsRXZlbnRzW2ldLklkID09PSBhbGxFdmVudHNbal0uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBrID0gajsgayA8IGFsbEV2ZW50cy5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHNba10gPSBhbGxFdmVudHNbayArIDFdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHMubGVuZ3RoLS07XHJcbiAgICAgICAgICAgICAgICAgICAgai0tO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhbGxFdmVudHM7XHJcbiAgICB9XHJcblxyXG4gICAgZW1haWxQcmVmZXJlbmNlcyhpc1BNLCBpc1VzZXIpIHtcclxuICAgICAgICBpZiAoaXNQTSAmJiBpc1VzZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHBlcnNvblJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldE1haWxQcmVmZXJlbmNlQnlVc2VySWQodGhpcy51c2VySWQpLnN1YnNjcmliZShwcmVmZXJlbmNlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmVmZXJlbmNlUmVzcG9uc2UgJiYgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWQgPSBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRVc2VyUHJlZmVyZW5jZU1haWxUZW1wbGF0ZXModGhpcy51c2VySUQpLnN1YnNjcmliZSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsTWFuYWdlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0ZvclVzZXIoKS5zdWJzY3JpYmUoYWxsVXNlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFsbEV2ZW50cyA9IGFsbE1hbmFnZXJFdmVudHMuY29uY2F0KGFsbFVzZXJFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudCA9IHRoaXMucmVtb3ZlRHVwbGljYXRlRXZlbnRzKGFsbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50KSAmJiB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudCAhPT0gdW5kZWZpbmVkID8gW3VzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnRdIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsRXZlbnQuZm9yRWFjaChldmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudC5mb3JFYWNoKHVzZXJQcmVmZXJlbmNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudHMuSWQgPT09IHVzZXJQcmVmZXJlbmNlLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50cy5pc0NoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEFsbENoZWNrKHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsTWFuYWdlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzRm9yVXNlcigpLnN1YnNjcmliZShhbGxVc2VyRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudHMgPSBhbGxNYW5hZ2VyRXZlbnRzLmNvbmNhdChhbGxVc2VyRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudCA9IHRoaXMucmVtb3ZlRHVwbGljYXRlRXZlbnRzKGFsbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyA9IGFsbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNQTSkge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgdXNlckNOOiB0aGlzLnVzZXJJRFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHBlcnNvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcklkID0gcGVyc29uUmVzcG9uc2UuSWQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuR2V0TWFpbFByZWZlcmVuY2VCeVVzZXJJZCh0aGlzLnVzZXJJZCkuc3Vic2NyaWJlKHByZWZlcmVuY2VSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByZWZlcmVuY2VSZXNwb25zZSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2UgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlWydNUE1fRW1haWxfUHJlZmVyZW5jZS1pZCddICYmIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJFbWFpbFByZWZlcmVuY2VJZCA9IHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYXNVc2VyRW1haWxQcmVmZXJlbmNlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldFVzZXJQcmVmZXJlbmNlTWFpbFRlbXBsYXRlcyh0aGlzLnVzZXJJRCkuc3Vic2NyaWJlKHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzRm9yTWFuYWdlcigpLnN1YnNjcmliZShhbGxFdmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQpICYmIHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50ICE9PSB1bmRlZmluZWQgPyBbdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudF0gOiBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHMuZm9yRWFjaChldmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qIHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7ICovXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50cy5JZCA9PT0gdXNlclByZWZlcmVuY2UuSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudHMuaXNDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRBbGxDaGVjayh0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyA9IGFsbEV2ZW50cztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHBlcnNvblJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldE1haWxQcmVmZXJlbmNlQnlVc2VySWQodGhpcy51c2VySWQpLnN1YnNjcmliZShwcmVmZXJlbmNlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmVmZXJlbmNlUmVzcG9uc2UgJiYgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWQgPSBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRVc2VyUHJlZmVyZW5jZU1haWxUZW1wbGF0ZXModGhpcy51c2VySUQpLnN1YnNjcmliZSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0ZvclVzZXIoKS5zdWJzY3JpYmUoYWxsRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc1VzZXJQcmVmZXJlbmNlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50KSAmJiB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudCAhPT0gdW5kZWZpbmVkID8gW3VzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnRdIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsRXZlbnRzLmZvckVhY2goZXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudC5mb3JFYWNoKHVzZXJQcmVmZXJlbmNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50LmZvckVhY2godXNlclByZWZlcmVuY2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudHMuSWQgPT09IHVzZXJQcmVmZXJlbmNlLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VGbGFnID0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMucHVzaChldmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRzLmlzQ2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMucHVzaChldmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RpZmllZEV2ZW50cyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc1VzZXJQcmVmZXJlbmNlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW1haWxFdmVudHNGb3JVc2VyKCkuc3Vic2NyaWJlKGFsbEV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMgPSBhbGxFdmVudHM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEFsbENoZWNrKHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGNoYW5nZXMpO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlck9iamVjdCgpO1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyLk1hbmFnZXJGb3IuVGFyZ2V0LmZvckVhY2goKHVzZXIpID0+IHtcclxuICAgICAgICAgICAgaWYgKHVzZXIuTmFtZSA9PT0gUm9sZUNvbnN0YW50cy5QUk9KRUNUX01BTkFHRVIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNQTSA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICAgICAgICB1c2VyLk5hbWUuaW5jbHVkZXMoUm9sZUNvbnN0YW50cy5QUk9KRUNUX01FTUJFUikgfHxcclxuICAgICAgICAgICAgICAgIHVzZXIuTmFtZS5pbmNsdWRlcyhSb2xlQ29uc3RhbnRzLlBST0pFQ1RfQVBQUk9WRVIpIHx8XHJcbiAgICAgICAgICAgICAgICB1c2VyLk5hbWUuaW5jbHVkZXMoUm9sZUNvbnN0YW50cy5QUk9KRUNUX1JFVklFV0VSKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNVc2VyID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuRW1haWxFdmVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgICAgICBlbWFpbFByZWZlcmVuY2U6IG5ldyBGb3JtQ29udHJvbCgpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudXNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKCk7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICB0aGlzLmVtYWlsUHJlZmVyZW5jZXModGhpcy5pc1BNLCB0aGlzLmlzVXNlcik7XHJcbiAgICB9XHJcbn1cclxuIl19