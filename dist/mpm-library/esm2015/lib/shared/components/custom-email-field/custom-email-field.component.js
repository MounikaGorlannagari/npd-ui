import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ENTER, COMMA, SEMICOLON, SPACE } from '@angular/cdk/keycodes';
import { NotificationService } from '../../../notification/notification.service';
let CustomEmailFieldComponent = class CustomEmailFieldComponent {
    constructor(notification) {
        this.notification = notification;
        this.valueChanges = new EventEmitter();
        this.separatorKeysCodes = [ENTER, COMMA, SEMICOLON, SPACE];
        this.emails = [];
        this.disabled = false;
        this.mandatory = false;
        this.isIdExists = false;
    }
    checkIfEmailInString(text) {
        var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        return re.test(text);
    }
    addEmail(event) {
        const input = event.input;
        const value = event.value;
        const index = this.emails.findIndex(val => val === value.trim());
        if (this.checkIfEmailInString(value)) {
            this.isIdExists = false;
            if ((value.trim() !== '')) {
                this.emailFieldFormControl.setErrors(null);
                const tempEmails = this.emailFieldFormControl.value;
                if (index < 0) {
                    tempEmails.push(value.trim());
                }
                this.emailFieldFormControl.setValue(tempEmails);
                if (this.emailFieldFormControl.valid) {
                    this.emailFieldFormControl.markAsDirty();
                    if (index !== -1) {
                        this.isIdExists = true;
                        this.notification.error('Email id alreday exists');
                    }
                    input.value = '';
                }
                else {
                    if (index !== -1) {
                        this.emails.splice(index, 1);
                    }
                }
                this.valueChanges.next(this.emailFieldFormControl);
            }
            else {
                this.emailFieldFormControl.updateValueAndValidity();
                this.valueChanges.next(this.emailFieldFormControl);
            }
        }
        else {
            if (value != '') {
                this.isIdExists = true;
                this.notification.error('Provided email id is not valid,enter valid email id');
            }
        }
    }
    onRemoveEmail(email) {
        const index = this.emails.indexOf(email, 0);
        if (index > -1) {
            this.emails.splice(index, 1);
        }
        this.emailFieldFormControl.updateValueAndValidity();
        this.emailFieldFormControl.markAsDirty();
        this.valueChanges.next(this.emailFieldFormControl);
    }
    ngOnInit() {
        this.emails = this.emailFieldFormControl.value;
        this.emailFieldFormControl.patchValue(this.emails);
        /* if (this.label == 'To' || this.label == 'From') {
            this.required = true;
        } */ /*  else {
            this.asterik = false;
        } */
        if (this.emailFieldFormControl && this.emailFieldFormControl.status === 'DISABLED') {
            this.disabled = true;
        }
        if (this.emailFieldFormControl && ((this.emailFieldFormControl.errors && this.emailFieldFormControl.errors.required) || (this.emailFieldFormControl.pristine && this.emailFieldFormControl.status === 'DISABLED'))) {
            this.mandatory = true;
        }
    }
};
CustomEmailFieldComponent.ctorParameters = () => [
    { type: NotificationService }
];
__decorate([
    Input()
], CustomEmailFieldComponent.prototype, "emailFieldFormControl", void 0);
__decorate([
    Input()
], CustomEmailFieldComponent.prototype, "label", void 0);
__decorate([
    Output()
], CustomEmailFieldComponent.prototype, "valueChanges", void 0);
__decorate([
    Input()
], CustomEmailFieldComponent.prototype, "required", void 0);
CustomEmailFieldComponent = __decorate([
    Component({
        selector: 'mpm-custom-email-field',
        template: "<mat-form-field appearance=\"outline\" class=\"full-width\" [ngClass]=\"{'comment': isIdExists}\">\r\n    <mat-label *ngIf=\"mandatory\">{{label}} *</mat-label>\r\n    <mat-label *ngIf=\"!mandatory\">{{label}}</mat-label>\r\n    <mat-chip-list #emailList>\r\n        <mat-chip class=\"email-chip\" *ngFor=\"let email of emails\" [disabled]=\"disabled\" removable\r\n            (removed)=\"onRemoveEmail(email)\">\r\n            <span class=\"email-value\" matTooltip=\"{{email}}\">\r\n                {{email}}\r\n            </span>\r\n            <mat-icon matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n        <input [disabled]=\"disabled\" class=\"email-chip-input\" [matChipInputAddOnBlur]=\"true\"\r\n            [matChipInputFor]=\"emailList\" [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n            (matChipInputTokenEnd)=\"addEmail($event)\" [required]=\"mandatory\">\r\n    </mat-chip-list>\r\n</mat-form-field>",
        styles: [".full-width{width:100%}.email-chip{padding:8px 4px 8px 8px!important;min-height:24px;font-size:12px;overflow:hidden}.email-chip .email-value{text-overflow:ellipsis;overflow:hidden;max-width:290px;white-space:nowrap}.email-chip-input{min-height:24px}.comment ::ng-deep .mat-form-field-outline{color:red!important}"]
    })
], CustomEmailFieldComponent);
export { CustomEmailFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWVtYWlsLWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1lbWFpbC1maWVsZC9jdXN0b20tZW1haWwtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUd2RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQVNqRixJQUFhLHlCQUF5QixHQUF0QyxNQUFhLHlCQUF5QjtJQWFsQyxZQUNXLFlBQWlDO1FBQWpDLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQVZsQyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFHakQsdUJBQWtCLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0RCxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ1osYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGVBQVUsR0FBWSxLQUFLLENBQUM7SUFJeEIsQ0FBQztJQUVMLG9CQUFvQixDQUFDLElBQUk7UUFDckIsSUFBSSxFQUFFLEdBQUcsd0pBQXdKLENBQUM7UUFDbEssT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxRQUFRLENBQUMsS0FBd0I7UUFDN0IsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUMxQixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzFCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRWpFLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BELElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtvQkFDWCxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUU7b0JBQ2xDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDekMsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7cUJBQ3REO29CQUNELEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNwQjtxQkFBTTtvQkFDSCxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDZCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBQ2hDO2lCQUNKO2dCQUVELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQ3REO2lCQUFNO2dCQUNILElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUNwRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQzthQUN0RDtTQUNKO2FBQU07WUFDSCxJQUFJLEtBQUssSUFBSSxFQUFFLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7YUFDbEY7U0FDSjtJQUNMLENBQUM7SUFFRCxhQUFhLENBQUMsS0FBVTtRQUNwQixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEM7UUFFRCxJQUFJLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUM7UUFDL0MsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFbkQ7O1lBRUksQ0FBQTs7WUFFQTtRQUNKLElBQUksSUFBSSxDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQ2hGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxJQUFJLENBQUMscUJBQXFCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxFQUFFO1lBQ2hOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQztDQUVKLENBQUE7O1lBN0U0QixtQkFBbUI7O0FBWm5DO0lBQVIsS0FBSyxFQUFFO3dFQUF3QztBQUN2QztJQUFSLEtBQUssRUFBRTt3REFBZTtBQUNiO0lBQVQsTUFBTSxFQUFFOytEQUF3QztBQUN4QztJQUFSLEtBQUssRUFBRTsyREFBbUI7QUFMbEIseUJBQXlCO0lBTnJDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx3QkFBd0I7UUFDbEMsZzhCQUFrRDs7S0FFckQsQ0FBQztHQUVXLHlCQUF5QixDQTJGckM7U0EzRlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBFTlRFUiwgQ09NTUEsIFNFTUlDT0xPTiwgU1BBQ0UgfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xyXG5pbXBvcnQgeyBNYXRDaGlwSW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NoaXBzJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSWZTdG10IH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXInO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1jdXN0b20tZW1haWwtZmllbGQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2N1c3RvbS1lbWFpbC1maWVsZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jdXN0b20tZW1haWwtZmllbGQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbUVtYWlsRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGVtYWlsRmllbGRGb3JtQ29udHJvbDogQWJzdHJhY3RDb250cm9sO1xyXG4gICAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuICAgIEBPdXRwdXQoKSB2YWx1ZUNoYW5nZXMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBJbnB1dCgpIHJlcXVpcmVkOiBib29sZWFuO1xyXG5cclxuICAgIHNlcGFyYXRvcktleXNDb2RlcyA9IFtFTlRFUiwgQ09NTUEsIFNFTUlDT0xPTiwgU1BBQ0VdO1xyXG4gICAgZW1haWxzID0gW107XHJcbiAgICBkaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgbWFuZGF0b3J5ID0gZmFsc2U7XHJcbiAgICBpc0lkRXhpc3RzOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjaGVja0lmRW1haWxJblN0cmluZyh0ZXh0KSB7XHJcbiAgICAgICAgdmFyIHJlID0gLygoW148PigpW1xcXVxcXFwuLDs6XFxzQFxcXCJdKyhcXC5bXjw+KClbXFxdXFxcXC4sOzpcXHNAXFxcIl0rKSopfChcIi4rXFxcIikpQCgoXFxbWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcXSl8KChbYS16QS1aXFwtMC05XStcXC4pK1thLXpBLVpdezIsfSkpLztcclxuICAgICAgICByZXR1cm4gcmUudGVzdCh0ZXh0KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRFbWFpbChldmVudDogTWF0Q2hpcElucHV0RXZlbnQpIHtcclxuICAgICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gZXZlbnQudmFsdWU7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmVtYWlscy5maW5kSW5kZXgodmFsID0+IHZhbCA9PT0gdmFsdWUudHJpbSgpKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2hlY2tJZkVtYWlsSW5TdHJpbmcodmFsdWUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNJZEV4aXN0cyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAoKHZhbHVlLnRyaW0oKSAhPT0gJycpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5zZXRFcnJvcnMobnVsbCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0ZW1wRW1haWxzID0gdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wudmFsdWU7XHJcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcEVtYWlscy5wdXNoKHZhbHVlLnRyaW0oKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5zZXRWYWx1ZSh0ZW1wRW1haWxzKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC52YWxpZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLm1hcmtBc0RpcnR5KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzSWRFeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbi5lcnJvcignRW1haWwgaWQgYWxyZWRheSBleGlzdHMnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlscy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnZhbHVlQ2hhbmdlcy5uZXh0KHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmFsdWVDaGFuZ2VzLm5leHQodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlICE9ICcnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzSWRFeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ1Byb3ZpZGVkIGVtYWlsIGlkIGlzIG5vdCB2YWxpZCxlbnRlciB2YWxpZCBlbWFpbCBpZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmVtb3ZlRW1haWwoZW1haWw6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5lbWFpbHMuaW5kZXhPZihlbWFpbCwgMCk7XHJcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgdGhpcy5lbWFpbHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5tYXJrQXNEaXJ0eSgpO1xyXG5cclxuICAgICAgICB0aGlzLnZhbHVlQ2hhbmdlcy5uZXh0KHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVtYWlscyA9IHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnZhbHVlO1xyXG4gICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnBhdGNoVmFsdWUodGhpcy5lbWFpbHMpO1xyXG5cclxuICAgICAgICAvKiBpZiAodGhpcy5sYWJlbCA9PSAnVG8nIHx8IHRoaXMubGFiZWwgPT0gJ0Zyb20nKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVxdWlyZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gKi8vKiAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXN0ZXJpayA9IGZhbHNlO1xyXG4gICAgICAgIH0gKi9cclxuICAgICAgICBpZiAodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wgJiYgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wuc3RhdHVzID09PSAnRElTQUJMRUQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wgJiYgKCh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5lcnJvcnMgJiYgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wuZXJyb3JzLnJlcXVpcmVkKSB8fCAodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wucHJpc3RpbmUgJiYgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wuc3RhdHVzID09PSAnRElTQUJMRUQnKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5tYW5kYXRvcnkgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19