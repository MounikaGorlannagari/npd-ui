import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomEmailFieldComponent } from './custom-email-field.component';
import { MaterialModule } from '../../../material.module';
let CustomEmailFieldModule = class CustomEmailFieldModule {
};
CustomEmailFieldModule = __decorate([
    NgModule({
        declarations: [
            CustomEmailFieldComponent
        ],
        imports: [
            CommonModule,
            MaterialModule
        ],
        exports: [
            CustomEmailFieldComponent
        ]
    })
], CustomEmailFieldModule);
export { CustomEmailFieldModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWVtYWlsLWZpZWxkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1lbWFpbC1maWVsZC9jdXN0b20tZW1haWwtZmllbGQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFlMUQsSUFBYSxzQkFBc0IsR0FBbkMsTUFBYSxzQkFBc0I7Q0FBSSxDQUFBO0FBQTFCLHNCQUFzQjtJQWJsQyxRQUFRLENBQUM7UUFDTixZQUFZLEVBQUU7WUFDVix5QkFBeUI7U0FDNUI7UUFDRCxPQUFPLEVBQUU7WUFDTCxZQUFZO1lBQ1osY0FBYztTQUNqQjtRQUNELE9BQU8sRUFBRTtZQUNMLHlCQUF5QjtTQUM1QjtLQUNKLENBQUM7R0FFVyxzQkFBc0IsQ0FBSTtTQUExQixzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDdXN0b21FbWFpbEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jdXN0b20tZW1haWwtZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi8uLi8uLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEN1c3RvbUVtYWlsRmllbGRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIEN1c3RvbUVtYWlsRmllbGRDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDdXN0b21FbWFpbEZpZWxkTW9kdWxlIHsgfVxyXG4iXX0=