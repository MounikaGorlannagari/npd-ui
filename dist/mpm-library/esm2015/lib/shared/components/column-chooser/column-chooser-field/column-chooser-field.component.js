import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GloabalConfig as config } from '../../../../mpm-utils/config/config';
let ColumnChooserFieldComponent = class ColumnChooserFieldComponent {
    constructor() {
        // @Output() columnChooserHandler = new EventEmitter<any[]>();
        this.columnChooserHandler = new EventEmitter();
        this.saveColumnChooserHandler = new EventEmitter();
        this.resetColumnChooserHandler = new EventEmitter();
        this.maxFreezeCount = Array.from({ length: config.getFreezeColumnCount() }, (v, k) => k + 1);
    }
    onChecked(field) {
        this.columnsCount = 0;
        this.columnChooserFields.forEach((eachField) => {
            if (eachField.selected) {
                this.columnsCount += 1;
            }
        });
        if (!field.selected && this.columnsCount >= this.maximumFieldsAllowed) {
            field.disabled = true;
        }
        else {
            field.disabled = false;
        }
    }
    updateColumnChooser(isPreference) {
        if (isPreference) {
            this.saveColumnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.freezeColumnCountNumber });
        }
        else {
            this.columnChooserHandler.next({ 'column': this.columnChooserFields, 'frezeCount': this.freezeColumnCountNumber });
        }
    }
    resetData() {
        this.resetColumnChooserHandler.next();
    }
    ngOnChanges(changes) {
        this.freezeColumnCountNumber = this.userPreferenceFreezeCount;
    }
    ngOnInit() {
        this.enableIndexerFieldRestriction = config.getFieldRestriction();
        this.maximumFieldsAllowed = config.getMaximumFieldsAllowed();
        // this.maxFreezeCount= config.getFreezeColumnCount();
        // console.log('freezeColumnCountNumber', config.getFreezeColumnCount())
    }
    compareObjects(o1, o2) {
        if (o1 === parseInt(o2)) {
            return true;
        }
    }
};
__decorate([
    Input()
], ColumnChooserFieldComponent.prototype, "columnChooserFields", void 0);
__decorate([
    Input()
], ColumnChooserFieldComponent.prototype, "isPMView", void 0);
__decorate([
    Input()
], ColumnChooserFieldComponent.prototype, "isColumnsFreezable", void 0);
__decorate([
    Input()
], ColumnChooserFieldComponent.prototype, "isAssetsTab", void 0);
__decorate([
    Output()
], ColumnChooserFieldComponent.prototype, "columnChooserHandler", void 0);
__decorate([
    Output()
], ColumnChooserFieldComponent.prototype, "saveColumnChooserHandler", void 0);
__decorate([
    Output()
], ColumnChooserFieldComponent.prototype, "resetColumnChooserHandler", void 0);
__decorate([
    Input()
], ColumnChooserFieldComponent.prototype, "userPreferenceFreezeCount", void 0);
ColumnChooserFieldComponent = __decorate([
    Component({
        selector: 'mpm-column-chooser-field',
        template: "<!-- <div class=\"columnChooserHeader\">Configure Table Header</div> -->\r\n<button mat-flat-button color=\"primary\" id=\"reset-btn\" (click)=\"resetData()\">\r\n    <span>Reset to Default</span>\r\n</button>\r\n<div class=\"column-chooser-container\">\r\n    <div class=\"columnChooserField\" *ngFor=\"let field of columnChooserFields\" (click)=\"$event.stopPropagation()\">\r\n        <mat-checkbox color=\"primary\" [(ngModel)]=\"field.selected\" [checked]=\"enableIndexerFieldRestriction && onChecked(field)\" [disabled]=\"field.disabled\">\r\n            <span matTooltip=\"{{field.DISPLAY_NAME}}\">{{field.DISPLAY_NAME}}</span>\r\n        </mat-checkbox>\r\n    </div>\r\n</div>\r\n<div class=\"column-chooser-actions\">\r\n\r\n\r\n\r\n    <button mat-stroked-button>\r\n        <span>Cancel</span>\r\n    </button>\r\n    <button mat-flat-button color=\"primary\" id=\"save-btn\" (click)=\"updateColumnChooser(false)\">\r\n        <span>Save</span>\r\n    </button>\r\n    <button *ngIf=\"!isPMView\" mat-flat-button color=\"primary\" id=\"save-option\" (click)=\"$event.stopPropagation()\" [matMenuTriggerFor]=\"saveOptionsMenu\">\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n    </button>\r\n    <mat-menu #saveOptionsMenu=\"matMenu\">\r\n        <button mat-menu-item (click)=\"updateColumnChooser(true)\">\r\n            <span>Save as Preference</span>\r\n        </button>\r\n    </mat-menu>\r\n\r\n    <!-- Drop down For the Freeze Column-->\r\n\r\n    <mat-form-field id=\"freeze\" *ngIf=\"isColumnsFreezable\" (click)=\"$event.stopPropagation()\">\r\n        <mat-label>Freeze No. Column</mat-label>\r\n        <mat-select [(value)]=\"freezeColumnCountNumber\" [compareWith]=\"compareObjects\">\r\n            <mat-option [value]=\"0\">None</mat-option>\r\n            <mat-option *ngFor=\"let i of maxFreezeCount;\" [value]=\"i\">{{i}}</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>",
        styles: ["#reset-btn{width:100%}.column-chooser-container{max-height:450px;overflow:auto}.column-chooser-container .columnChooserField{padding:8px}.column-chooser-container .columnChooserField ::ng-deep .mat-checkbox-label{max-width:14em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.column-chooser-actions{float:right;padding:4px 4px 8px}.column-chooser-actions #save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}.column-chooser-actions #save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:8px}.column-chooser-actions #freeze{margin:0 0 0 15px;width:30%;position:relative}"]
    })
], ColumnChooserFieldComponent);
export { ColumnChooserFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXItZmllbGQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvY29sdW1uLWNob29zZXIvY29sdW1uLWNob29zZXItZmllbGQvY29sdW1uLWNob29zZXItZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDekcsT0FBTyxFQUFFLGFBQWEsSUFBSSxNQUFNLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQU85RSxJQUFhLDJCQUEyQixHQUF4QyxNQUFhLDJCQUEyQjtJQWdCdEM7UUFWQSw4REFBOEQ7UUFDcEQseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQXNDLENBQUM7UUFDOUUsNkJBQXdCLEdBQUcsSUFBSSxZQUFZLEVBQXNDLENBQUM7UUFDbEYsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQVM1RCxJQUFJLENBQUMsY0FBYyxHQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLG9CQUFvQixFQUFFLEVBQUMsRUFBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsRUFBRSxDQUFBLENBQUMsR0FBQyxDQUFDLENBQUMsQ0FBQztJQUN0RixDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQUs7UUFDWCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDN0MsSUFBRyxTQUFTLENBQUMsUUFBUSxFQUFDO2dCQUNwQixJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQzthQUN4QjtRQUNILENBQUMsQ0FBQyxDQUFBO1FBQ0YsSUFBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUM7WUFDbkUsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDdkI7YUFBSTtZQUNILEtBQUssQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1NBQ3hCO0lBQ0gsQ0FBQztJQUVILG1CQUFtQixDQUFDLFlBQVk7UUFDMUIsSUFBSSxZQUFZLEVBQUU7WUFDbEIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxFQUFDLFFBQVEsRUFBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsWUFBWSxFQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBQyxDQUFDLENBQUM7U0FFcEg7YUFBTTtZQUNMLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBQyxRQUFRLEVBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLFlBQVksRUFBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUMsQ0FBQyxDQUFDO1NBQ2hIO0lBQ0gsQ0FBQztJQUdILFNBQVM7UUFDUCxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEMsQ0FBQztJQUNELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLENBQUMsdUJBQXVCLEdBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFBO0lBQzVELENBQUM7SUFDRixRQUFRO1FBQ04sSUFBSSxDQUFDLDZCQUE2QixHQUFHLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQ2xFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUM5RCxzREFBc0Q7UUFDckQsd0VBQXdFO0lBQzFFLENBQUM7SUFFRCxjQUFjLENBQUMsRUFBTyxFQUFFLEVBQU87UUFDN0IsSUFBRyxFQUFFLEtBQUssUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQ3JCLE9BQU8sSUFBSSxDQUFBO1NBQ1o7SUFFSCxDQUFDO0NBQ0YsQ0FBQTtBQTlEVTtJQUFSLEtBQUssRUFBRTt3RUFBcUI7QUFDcEI7SUFBUixLQUFLLEVBQUU7NkRBQVU7QUFDVDtJQUFSLEtBQUssRUFBRTt1RUFBb0I7QUFDbkI7SUFBUixLQUFLLEVBQUU7Z0VBQXFCO0FBRW5CO0lBQVQsTUFBTSxFQUFFO3lFQUErRTtBQUM5RTtJQUFULE1BQU0sRUFBRTs2RUFBbUY7QUFDbEY7SUFBVCxNQUFNLEVBQUU7OEVBQXFEO0FBQ3JEO0lBQVIsS0FBSyxFQUFFOzhFQUEyQjtBQVZ4QiwyQkFBMkI7SUFMdkMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLDBCQUEwQjtRQUNwQyw2NERBQW9EOztLQUVyRCxDQUFDO0dBQ1csMkJBQTJCLENBZ0V2QztTQWhFWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgR2xvYWJhbENvbmZpZyBhcyBjb25maWcgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvY29uZmlnL2NvbmZpZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jb2x1bW4tY2hvb3Nlci1maWVsZCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbHVtbi1jaG9vc2VyLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb2x1bW4tY2hvb3Nlci1maWVsZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb2x1bW5DaG9vc2VyRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgY29sdW1uQ2hvb3NlckZpZWxkcztcclxuICBASW5wdXQoKSBpc1BNVmlldztcclxuICBASW5wdXQoKSBpc0NvbHVtbnNGcmVlemFibGU7XHJcbiAgQElucHV0KCkgaXNBc3NldHNUYWI6Ym9vbGVhbjtcclxuICAvLyBAT3V0cHV0KCkgY29sdW1uQ2hvb3NlckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gIEBPdXRwdXQoKSBjb2x1bW5DaG9vc2VySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8eydjb2x1bW4nOmFueVtdLCAnZnJlemVDb3VudCc6YW55fT4oKTtcclxuICBAT3V0cHV0KCkgc2F2ZUNvbHVtbkNob29zZXJIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjx7J2NvbHVtbic6YW55W10sICdmcmV6ZUNvdW50Jzphbnl9PigpO1xyXG4gIEBPdXRwdXQoKSByZXNldENvbHVtbkNob29zZXJIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgdXNlclByZWZlcmVuY2VGcmVlemVDb3VudDtcclxuICBmcmVlemVDb2x1bW5Db3VudE51bWJlclxyXG4gIG1heEZyZWV6ZUNvdW50O1xyXG4gIGNvbHVtbnNDb3VudDtcclxuICBlbmFibGVJbmRleGVyRmllbGRSZXN0cmljdGlvbjtcclxuICBtYXhpbXVtRmllbGRzQWxsb3dlZDtcclxuICBjb25zdHJ1Y3RvcigpIHsgXHJcblxyXG4gICAgdGhpcy5tYXhGcmVlemVDb3VudD0gQXJyYXkuZnJvbSh7bGVuZ3RoOiBjb25maWcuZ2V0RnJlZXplQ29sdW1uQ291bnQoKX0sKHYsayk9PmsrMSk7XHJcbiAgfVxyXG4gXHJcbiAgb25DaGVja2VkKGZpZWxkKXtcclxuICAgICAgdGhpcy5jb2x1bW5zQ291bnQgPSAwOyBcclxuICAgICAgdGhpcy5jb2x1bW5DaG9vc2VyRmllbGRzLmZvckVhY2goKGVhY2hGaWVsZCkgPT4ge1xyXG4gICAgICAgIGlmKGVhY2hGaWVsZC5zZWxlY3RlZCl7IFxyXG4gICAgICAgICAgdGhpcy5jb2x1bW5zQ291bnQgKz0gMTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIGlmKCFmaWVsZC5zZWxlY3RlZCAmJiB0aGlzLmNvbHVtbnNDb3VudCA+PSB0aGlzLm1heGltdW1GaWVsZHNBbGxvd2VkKXtcclxuICAgICAgICBmaWVsZC5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGZpZWxkLmRpc2FibGVkID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgdXBkYXRlQ29sdW1uQ2hvb3Nlcihpc1ByZWZlcmVuY2UpIHtcclxuICAgICAgICBpZiAoaXNQcmVmZXJlbmNlKSB7XHJcbiAgICAgICAgdGhpcy5zYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXIubmV4dCh7J2NvbHVtbic6dGhpcy5jb2x1bW5DaG9vc2VyRmllbGRzLCAnZnJlemVDb3VudCc6dGhpcy5mcmVlemVDb2x1bW5Db3VudE51bWJlcn0pO1xyXG4gICAgXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jb2x1bW5DaG9vc2VySGFuZGxlci5uZXh0KHsnY29sdW1uJzp0aGlzLmNvbHVtbkNob29zZXJGaWVsZHMsICdmcmV6ZUNvdW50Jzp0aGlzLmZyZWV6ZUNvbHVtbkNvdW50TnVtYmVyfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gIHJlc2V0RGF0YSgpIHtcclxuICAgIHRoaXMucmVzZXRDb2x1bW5DaG9vc2VySGFuZGxlci5uZXh0KCk7XHJcbiAgfVxyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIHRoaXMuZnJlZXplQ29sdW1uQ291bnROdW1iZXI9dGhpcy51c2VyUHJlZmVyZW5jZUZyZWV6ZUNvdW50XHJcbiAgIH1cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZW5hYmxlSW5kZXhlckZpZWxkUmVzdHJpY3Rpb24gPSBjb25maWcuZ2V0RmllbGRSZXN0cmljdGlvbigpO1xyXG4gICAgdGhpcy5tYXhpbXVtRmllbGRzQWxsb3dlZCA9IGNvbmZpZy5nZXRNYXhpbXVtRmllbGRzQWxsb3dlZCgpO1xyXG4gICAvLyB0aGlzLm1heEZyZWV6ZUNvdW50PSBjb25maWcuZ2V0RnJlZXplQ29sdW1uQ291bnQoKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKCdmcmVlemVDb2x1bW5Db3VudE51bWJlcicsIGNvbmZpZy5nZXRGcmVlemVDb2x1bW5Db3VudCgpKVxyXG4gIH1cclxuXHJcbiAgY29tcGFyZU9iamVjdHMobzE6IGFueSwgbzI6IGFueSkge1xyXG4gICAgaWYobzEgPT09IHBhcnNlSW50KG8yKSl7XHJcbiAgICAgIHJldHVybiB0cnVlXHJcbiAgICB9XHJcblxyXG4gIH1cclxufVxyXG4iXX0=