import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnChooserComponent } from './column-chooser.component';
import { MaterialModule } from '../../../material.module';
import { ColumnChooserFieldComponent } from './column-chooser-field/column-chooser-field.component';
let ColumnChooserModule = class ColumnChooserModule {
};
ColumnChooserModule = __decorate([
    NgModule({
        declarations: [
            ColumnChooserComponent,
            ColumnChooserFieldComponent
        ],
        imports: [
            CommonModule,
            MaterialModule
        ],
        exports: [
            ColumnChooserComponent,
            ColumnChooserFieldComponent
        ]
    })
], ColumnChooserModule);
export { ColumnChooserModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvY29sdW1uLWNob29zZXIvY29sdW1uLWNob29zZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sdURBQXVELENBQUM7QUFrQnBHLElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0NBQUksQ0FBQTtBQUF2QixtQkFBbUI7SUFkL0IsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osc0JBQXNCO1lBQ3RCLDJCQUEyQjtTQUM1QjtRQUNELE9BQU8sRUFBRTtZQUNQLFlBQVk7WUFDWixjQUFjO1NBQ2Y7UUFDRCxPQUFPLEVBQUU7WUFDUCxzQkFBc0I7WUFDdEIsMkJBQTJCO1NBQzVCO0tBQ0YsQ0FBQztHQUNXLG1CQUFtQixDQUFJO1NBQXZCLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IENvbHVtbkNob29zZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgQ29sdW1uQ2hvb3NlckZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jb2x1bW4tY2hvb3Nlci1maWVsZC9jb2x1bW4tY2hvb3Nlci1maWVsZC5jb21wb25lbnQnO1xyXG5cclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgQ29sdW1uQ2hvb3NlckNvbXBvbmVudCxcclxuICAgIENvbHVtbkNob29zZXJGaWVsZENvbXBvbmVudFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIENvbHVtbkNob29zZXJDb21wb25lbnQsXHJcbiAgICBDb2x1bW5DaG9vc2VyRmllbGRDb21wb25lbnRcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb2x1bW5DaG9vc2VyTW9kdWxlIHsgfVxyXG4iXX0=