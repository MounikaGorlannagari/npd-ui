import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../../notification/notification.service';
let ConfirmationModalComponent = class ConfirmationModalComponent {
    constructor(dialogRef, data, notificationService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.notificationService = notificationService;
        this.confirmationComment = '';
        this.confirmationHeading = 'Confirmation';
    }
    closeDialog() {
        const result = {
            isTrue: false
        };
        this.dialogRef.close();
    }
    ngOnInit() {
        this.message = this.data.message;
        this.taskName = this.data.name;
        if (this.data.confirmationHeading) {
            this.confirmationHeading = this.data.confirmationHeading;
        }
    }
    onYesClick() {
        const result = {
            isTrue: true,
            confirmationComment: ''
        };
        if (this.data.hasConfirmationComment) {
            if (this.data.isCommentRequired && this.confirmationComment.trim().length === 0) {
                this.notificationService.error(this.data.errorMessage);
                return;
            }
            result.confirmationComment = this.confirmationComment;
        }
        this.dialogRef.close(result);
    }
};
ConfirmationModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: NotificationService }
];
ConfirmationModalComponent = __decorate([
    Component({
        selector: 'mpm-confirmation-modal',
        template: "<h1 mat-dialog-title class=\"confirmation-modal-title\">{{confirmationHeading}}</h1>\r\n<mat-dialog-content>\r\n    <div *ngIf=\"taskName && taskName.length > 0\" class=\"task-info\">\r\n        <!-- class=\"task-info\" -->\r\n        <fieldset>\r\n            <legend>The following task will also be deleted,along with deliverable:</legend>\r\n            <mat-list *ngFor=\"let task of taskName\" style=\"padding-bottom:4px;\">\r\n                <mat-list-item>\r\n                    <mat-icon class=\"brand\">keyboard_arrow_right</mat-icon> <span matTooltip=\"{{task}}\">{{task}}</span>\r\n                </mat-list-item>\r\n            </mat-list>\r\n        </fieldset>\r\n    </div>\r\n    <br />\r\n    <span *ngIf=\"message\">\r\n        <p [innerHTML]=\"message\"></p>\r\n    </span>\r\n    <mat-form-field *ngIf=\"data.hasConfirmationComment\" class=\"comment-field\" appearance=\"outline\"\r\n        style=\"width: 100%!important;\">\r\n        <mat-label>{{data.commentText}}</mat-label>\r\n        <textarea required=\"{{data.isCommentRequired}}\" matInput id=\"confirmationComment\" name=\"\" cols=\"\" rows=\"\"\r\n            [(ngModel)]=\"confirmationComment\"></textarea>\r\n    </mat-form-field>\r\n</mat-dialog-content>\r\n<div align=\"end\">\r\n    <button mat-stroked-button (click)=\"closeDialog()\">{{data.cancelButton}}</button>\r\n    <button color=\"primary\" mat-flat-button (click)=\"onYesClick()\" cdkFocusInitial>{{data.submitButton}}</button>\r\n</div>",
        styles: ["confirmation-modal-title{font-weight:700}.comment-field{margin-top:20px}button{margin:10px}.task-info{max-height:300px;overflow:auto;padding:3px}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], ConfirmationModalComponent);
export { ConfirmationModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUWpGLElBQWEsMEJBQTBCLEdBQXZDLE1BQWEsMEJBQTBCO0lBTW5DLFlBQ1csU0FBbUQsRUFDMUIsSUFBUyxFQUNsQyxtQkFBd0M7UUFGeEMsY0FBUyxHQUFULFNBQVMsQ0FBMEM7UUFDMUIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNsQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBTm5ELHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQUN6Qix3QkFBbUIsR0FBRyxjQUFjLENBQUM7SUFNakMsQ0FBQztJQUVMLFdBQVc7UUFDUCxNQUFNLE1BQU0sR0FBRztZQUNYLE1BQU0sRUFBRSxLQUFLO1NBQ2hCLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQy9CLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUMvQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUM1RDtJQUNMLENBQUM7SUFFRCxVQUFVO1FBQ04sTUFBTSxNQUFNLEdBQUc7WUFDWCxNQUFNLEVBQUUsSUFBSTtZQUNaLG1CQUFtQixFQUFFLEVBQUU7U0FDMUIsQ0FBQztRQUNGLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzdFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDdkQsT0FBTzthQUNWO1lBQ0QsTUFBTSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUN6RDtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Q0FFSixDQUFBOztZQW5DeUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7WUFDSyxtQkFBbUI7O0FBVDFDLDBCQUEwQjtJQU50QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsd0JBQXdCO1FBQ2xDLDQ5Q0FBa0Q7O0tBRXJELENBQUM7SUFVTyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtHQVJuQiwwQkFBMEIsQ0EwQ3RDO1NBMUNZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tY29uZmlybWF0aW9uLW1vZGFsJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgbWVzc2FnZTtcclxuICAgIGNvbmZpcm1hdGlvbkNvbW1lbnQgPSAnJztcclxuICAgIGNvbmZpcm1hdGlvbkhlYWRpbmcgPSAnQ29uZmlybWF0aW9uJztcclxuICAgIHRhc2tOYW1lO1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICkgeyB9XHJcblxyXG4gICAgY2xvc2VEaWFsb2coKSB7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0ge1xyXG4gICAgICAgICAgICBpc1RydWU6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMubWVzc2FnZSA9IHRoaXMuZGF0YS5tZXNzYWdlO1xyXG4gICAgICAgIHRoaXMudGFza05hbWUgPSB0aGlzLmRhdGEubmFtZTtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLmNvbmZpcm1hdGlvbkhlYWRpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5jb25maXJtYXRpb25IZWFkaW5nID0gdGhpcy5kYXRhLmNvbmZpcm1hdGlvbkhlYWRpbmc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uWWVzQ2xpY2soKSB7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0ge1xyXG4gICAgICAgICAgICBpc1RydWU6IHRydWUsXHJcbiAgICAgICAgICAgIGNvbmZpcm1hdGlvbkNvbW1lbnQ6ICcnXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLmhhc0NvbmZpcm1hdGlvbkNvbW1lbnQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGF0YS5pc0NvbW1lbnRSZXF1aXJlZCAmJiB0aGlzLmNvbmZpcm1hdGlvbkNvbW1lbnQudHJpbSgpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHRoaXMuZGF0YS5lcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJlc3VsdC5jb25maXJtYXRpb25Db21tZW50ID0gdGhpcy5jb25maXJtYXRpb25Db21tZW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShyZXN1bHQpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=