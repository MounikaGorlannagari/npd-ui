import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
let CustomSearchFieldComponent = class CustomSearchFieldComponent {
    constructor() {
        this.valueChange = new EventEmitter();
        this.selectedOption = new EventEmitter();
        this.details = new EventEmitter();
        this.selectedValue = {
            id: '',
            value: ''
        };
    }
    filterUser(value) {
        let filterValue = value;
        try {
            filterValue = value.toLowerCase();
        }
        catch (exception) {
            filterValue = value;
        }
        if (this.searchFieldConfig.filterOptions) {
            return this.searchFieldConfig.filterOptions.filter(option => {
                return option.displayName && option.displayName.toLowerCase().indexOf(filterValue) >= 0;
            });
        }
        else {
            return [];
        }
    }
    displayFn(user) {
        return user ? user.displayName : undefined;
    }
    onFocusOut(event) {
        if (event.target.value.trim() === '' &&
            (!this.userFormControl || !this.userFormControl.errors || (this.userFormControl.errors && !this.userFormControl.errors.required))) {
            return;
        }
        else {
            if (this.searchFieldConfig.filterOptions) {
                if (!this.searchFieldConfig.filterOptions.some(e => e.displayName === event.target.value)) {
                    this.userFormControl.setErrors({ incorrect: true });
                }
                else {
                    this.userFormControl.setErrors(null);
                }
            }
        }
    }
    changeAutoComplete(event) {
        var _a;
        (_a = this.userFormControl) === null || _a === void 0 ? void 0 : _a.setErrors(null);
        this.selectedValue.id = event.option.value.value;
        this.selectedValue.value = event.option.value.displayName;
        this.valueChange.next(this.selectedValue);
        this.selectedOption.next(event.option._selected);
    }
    refreshSearchFields() {
        if (this.searchFieldConfig) {
            if (this.searchFieldConfig.formControl) {
                this.userFormControl = this.searchFieldConfig.formControl;
            }
            else {
                this.userFormControl = new FormControl();
            }
            if (this.userFormControl && this.userFormControl.errors && this.userFormControl.errors.required) {
                this.requiredField = true;
            }
            else {
                this.requiredField = false;
            }
            if (this.searchFieldConfig.filterOptions && this.searchFieldConfig.filterOptions.length &&
                this.searchFieldConfig.filterOptions.length > 0) {
                this.filterOptions = this.userFormControl.valueChanges
                    .pipe(startWith(''), map(value => {
                    this.searchFieldConfig.filterOptions.map(option => {
                        if (option.value === value) {
                            this.selectedValue.value = option.displayName;
                            this.selectedValue.id = option.value;
                        }
                    });
                    return this.filterUser(value);
                }));
            }
        }
        if (this.searchFieldConfig.filterOptions) {
            this.searchFieldConfig.filterOptions.map(option => {
                if (option.value === this.userFormControl.value) {
                    this.selectedValue.value = option.displayName;
                    this.selectedValue.id = option.value;
                }
            });
        }
        if (this.userFormControl && this.userFormControl.status === 'DISABLED') {
            this.userFormControl.disable();
        }
    }
    getDetails(name) {
        this.details.next(name);
    }
    ngOnInit() {
        this.refreshSearchFields();
    }
};
__decorate([
    Input()
], CustomSearchFieldComponent.prototype, "searchFieldConfig", void 0);
__decorate([
    Input()
], CustomSearchFieldComponent.prototype, "required", void 0);
__decorate([
    Output()
], CustomSearchFieldComponent.prototype, "valueChange", void 0);
__decorate([
    Output()
], CustomSearchFieldComponent.prototype, "selectedOption", void 0);
__decorate([
    Output()
], CustomSearchFieldComponent.prototype, "details", void 0);
CustomSearchFieldComponent = __decorate([
    Component({
        selector: 'mpm-custom-search-field',
        template: "<mat-form-field appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\" (keyup)=\"getDetails($event)\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"userFormControl\" (focusout)=\"onFocusOut($event)\"\r\n        [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <div #optionposition\r\n                [matTooltip]=\"spanposition.getBoundingClientRect().width > optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                <span #spanposition\r\n                    [matTooltip]=\"spanposition.getBoundingClientRect().width <= optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                    {{option.displayName}}\r\n                </span>\r\n            </div>\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n    <mat-error *ngIf=\"userFormControl.hasError('incorrect')\">No Matches!</mat-error>\r\n</mat-form-field>",
        styles: ["mat-form-field{width:100%}"]
    })
], CustomSearchFieldComponent);
export { CustomSearchFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tc2VhcmNoLWZpZWxkL2N1c3RvbS1zZWFyY2gtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBU2hELElBQWEsMEJBQTBCLEdBQXZDLE1BQWEsMEJBQTBCO0lBaUJuQztRQWJVLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFFekMsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFLNUMsa0JBQWEsR0FBRztZQUNaLEVBQUUsRUFBRSxFQUFFO1lBQ04sS0FBSyxFQUFFLEVBQUU7U0FDWixDQUFDO0lBRWMsQ0FBQztJQUVWLFVBQVUsQ0FBQyxLQUFhO1FBQzNCLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJO1lBQ0EsV0FBVyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNyQztRQUFDLE9BQU8sU0FBUyxFQUFFO1lBQ2hCLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDdkI7UUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7WUFDdEMsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDeEQsT0FBTyxNQUFNLENBQUMsV0FBVyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1RixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBRUwsQ0FBQztJQUVELFNBQVMsQ0FBQyxJQUFVO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFDL0MsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ1osSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFO1lBQ2hDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUU7WUFDbkksT0FBTztTQUNWO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDdkYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDdkQ7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO2FBQ0o7U0FDSjtJQUNMLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxLQUFLOztRQUNwQixNQUFBLElBQUksQ0FBQyxlQUFlLDBDQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUU7UUFDdEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2pELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztRQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsbUJBQW1CO1FBQ2YsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFO2dCQUNwQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7YUFDN0Q7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO2FBQzVDO1lBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDN0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7YUFDOUI7WUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxNQUFNO2dCQUNuRixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZO3FCQUNqRCxJQUFJLENBQ0QsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDUixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDOUMsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUssRUFBRTs0QkFDeEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQzs0QkFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzt5QkFDeEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQyxDQUFDLENBQUMsQ0FDTCxDQUFDO2FBQ1Q7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRTtZQUN0QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDOUMsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFO29CQUM3QyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO29CQUM5QyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO2lCQUN4QztZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQ3BFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEM7SUFDTCxDQUFDO0lBRUQsVUFBVSxDQUFDLElBQUk7UUFDWCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBQ0QsUUFBUTtRQUNKLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Q0FFSixDQUFBO0FBaEhZO0lBQVIsS0FBSyxFQUFFO3FFQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTs0REFBVTtBQUNSO0lBQVQsTUFBTSxFQUFFOytEQUF1QztBQUN0QztJQUFULE1BQU0sRUFBRTtrRUFBMEM7QUFFekM7SUFBVCxNQUFNLEVBQUU7MkRBQW1DO0FBUG5DLDBCQUEwQjtJQU50QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUseUJBQXlCO1FBQ25DLHl2Q0FBbUQ7O0tBRXRELENBQUM7R0FFVywwQkFBMEIsQ0FrSHRDO1NBbEhZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IG1hcCwgc3RhcnRXaXRoIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWN1c3RvbS1zZWFyY2gtZmllbGQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2N1c3RvbS1zZWFyY2gtZmllbGQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tU2VhcmNoRmllbGRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHNlYXJjaEZpZWxkQ29uZmlnO1xyXG4gICAgQElucHV0KCkgcmVxdWlyZWQ7XHJcbiAgICBAT3V0cHV0KCkgdmFsdWVDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZE9wdGlvbiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBPdXRwdXQoKSBkZXRhaWxzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgdXNlckZvcm1Db250cm9sOiBGb3JtQ29udHJvbDtcclxuICAgIGZpbHRlck9wdGlvbnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gICAgcmVxdWlyZWRGaWVsZDogYm9vbGVhbjtcclxuICAgIHNlbGVjdGVkVmFsdWUgPSB7XHJcbiAgICAgICAgaWQ6ICcnLFxyXG4gICAgICAgIHZhbHVlOiAnJ1xyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAgIHB1YmxpYyBmaWx0ZXJVc2VyKHZhbHVlOiBzdHJpbmcpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgbGV0IGZpbHRlclZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgZmlsdGVyVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGV4Y2VwdGlvbikge1xyXG4gICAgICAgICAgICBmaWx0ZXJWYWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMuZmlsdGVyKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb3B0aW9uLmRpc3BsYXlOYW1lICYmIG9wdGlvbi5kaXNwbGF5TmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID49IDA7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGRpc3BsYXlGbih1c2VyPzogYW55KTogc3RyaW5nIHwgdW5kZWZpbmVkIHtcclxuICAgICAgICByZXR1cm4gdXNlciA/IHVzZXIuZGlzcGxheU5hbWUgOiB1bmRlZmluZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgb25Gb2N1c091dChldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC50YXJnZXQudmFsdWUudHJpbSgpID09PSAnJyAmJlxyXG4gICAgICAgICAgICAoIXRoaXMudXNlckZvcm1Db250cm9sIHx8ICF0aGlzLnVzZXJGb3JtQ29udHJvbC5lcnJvcnMgfHwgKHRoaXMudXNlckZvcm1Db250cm9sLmVycm9ycyAmJiAhdGhpcy51c2VyRm9ybUNvbnRyb2wuZXJyb3JzLnJlcXVpcmVkKSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLnNvbWUoZSA9PiBlLmRpc3BsYXlOYW1lID09PSBldmVudC50YXJnZXQudmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRm9ybUNvbnRyb2wuc2V0RXJyb3JzKHsgaW5jb3JyZWN0OiB0cnVlIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJGb3JtQ29udHJvbC5zZXRFcnJvcnMobnVsbCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlQXV0b0NvbXBsZXRlKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy51c2VyRm9ybUNvbnRyb2w/LnNldEVycm9ycyhudWxsKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVmFsdWUuaWQgPSBldmVudC5vcHRpb24udmFsdWUudmFsdWU7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gZXZlbnQub3B0aW9uLnZhbHVlLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgIHRoaXMudmFsdWVDaGFuZ2UubmV4dCh0aGlzLnNlbGVjdGVkVmFsdWUpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24ubmV4dChldmVudC5vcHRpb24uX3NlbGVjdGVkKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoU2VhcmNoRmllbGRzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZvcm1Db250cm9sKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJGb3JtQ29udHJvbCA9IHRoaXMuc2VhcmNoRmllbGRDb25maWcuZm9ybUNvbnRyb2w7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJGb3JtQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXJGb3JtQ29udHJvbCAmJiB0aGlzLnVzZXJGb3JtQ29udHJvbC5lcnJvcnMgJiYgdGhpcy51c2VyRm9ybUNvbnRyb2wuZXJyb3JzLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlcXVpcmVkRmllbGQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXF1aXJlZEZpZWxkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucyAmJiB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubGVuZ3RoICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maWx0ZXJPcHRpb25zID0gdGhpcy51c2VyRm9ybUNvbnRyb2wudmFsdWVDaGFuZ2VzXHJcbiAgICAgICAgICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcCh2YWx1ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9wdGlvbi52YWx1ZSA9PT0gdmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gb3B0aW9uLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmFsdWUuaWQgPSBvcHRpb24udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maWx0ZXJVc2VyKHZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucy5tYXAob3B0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChvcHRpb24udmFsdWUgPT09IHRoaXMudXNlckZvcm1Db250cm9sLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gb3B0aW9uLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS5pZCA9IG9wdGlvbi52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy51c2VyRm9ybUNvbnRyb2wgJiYgdGhpcy51c2VyRm9ybUNvbnRyb2wuc3RhdHVzID09PSAnRElTQUJMRUQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlckZvcm1Db250cm9sLmRpc2FibGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGV0YWlscyhuYW1lKSB7XHJcbiAgICAgICAgdGhpcy5kZXRhaWxzLm5leHQobmFtZSk7XHJcbiAgICB9XHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hTZWFyY2hGaWVsZHMoKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19