import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { AssetFileConfigService } from '../../../upload/services/asset.file.config.service';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
let QdsTransferTrayComponent = class QdsTransferTrayComponent {
    constructor(loaderService, otmmService, notificationService, qdsService, assetFileConfigService) {
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.qdsService = qdsService;
        this.assetFileConfigService = assetFileConfigService;
        this.displayedColumns = ['preview', 'filename', 'status', 'started', 'type'];
        this.isLoadingData = true;
        this.thumbnailFormats = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];
        this.transferStatusMapper = {
            complete: 'COMPLETE',
            transferred: 'COMPLETE',
            failed: 'FAILED',
            reassembling: 'INPROGRESS',
            saving: 'INPROGRESS',
            starting: 'INPROGRESS',
            transferring: 'INPROGRESS',
            waiting: 'INPROGRESS',
        };
        this.activeJobIdList = {
            EXPORT: {},
            IMPORT: {}
        };
        this.failedJobIdList = {};
        this.showFailed = false;
        this.noData = false;
        this.showActions = true;
        this.restartingJobCount = 0;
        this.sort = new MatSort();
        const QDSJobFiles = [];
        this.dataSource = new MatTableDataSource(QDSJobFiles);
        this.refresh();
    }
    getFileTranferStatus(job, file) {
        if (job.started && !job.running && job.filesFailed === 0 && file.transferred < file.size) {
            return 'PAUSED';
        }
        else {
            return this.transferStatusMapper[file.status];
        }
    }
    refresh() {
        this.showFailed = false;
        this.getDownloadLocation();
    }
    copyDownloadLocation() {
        const selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = this.downloadLocation;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
        this.notificationService.info('Download location copied to clipboard');
    }
    getDownloadLocation() {
        this.restartingJobCount = 0;
        this.isLoadingData = true;
        //this.toggleImportJobs('RESUME');// MSV-239
        this.qdsService.getDownloadFolder()
            .subscribe(response => {
            this.downloadLocation = response.downloadFolder;
            //this.getQDSTransferJobs();
            this.getQDSTransferJobs().subscribe(response => {
                this.resumeJobs('RESUME');
                this.calculateInprogress();
            });
            //this.toggleImportJobs('RESUME');// MSV-239
        }, error => {
            this.isLoadingData = false;
            this.notificationService.error('Something went wrong, Try again');
        });
    }
    changeDownloadLocation() {
        this.qdsService.chooseDownloadFolder()
            .subscribe(response => {
            this.downloadLocation = response;
        }, error => {
            this.notificationService.error('Something went wrong, Try again');
        });
    }
    getBase64(filePath) {
        return new Observable(observer => {
            // tslint:disable-next-line: prefer-const
            var self = this;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', filePath, true);
            xhr.responseType = 'blob';
            xhr.onload = function (e) {
                console.log(this.response);
                // tslint:disable-next-line: prefer-const
                var reader = new FileReader();
                reader.onload = (event) => {
                    var base64 = reader.result; // (event.target.result);
                    console.log(reader.result);
                    console.log(base64);
                    // tslint:disable-next-line: prefer-const
                    var fileObj = {
                        base64: base64,
                        file: file
                    };
                    console.log(fileObj);
                    observer.next(fileObj); // base64
                    observer.complete();
                };
                var file = this.response;
                reader.readAsDataURL(file);
            };
            xhr.send();
        });
    }
    createFileObj(file, data, fileObjType) {
        console.log(data);
        if (file && file !== null) {
            if (data && data !== null) {
                file.data = (data && data.changingThisBreaksApplicationSecurity) ? data.changingThisBreaksApplicationSecurity.split('base64,')[1] :
                    data.base64 ? data.base64.split('base64,')[1] : data.split('base64,')[1];
                if (data && data.base64 && data.base64.split('base64')[0].split('data:')[1].split(';')[0] === 'application/postscript') {
                    file.url = data.base64; // 'data:' + fileObjType + ';base64,' + file.data;
                }
                else {
                    file.url = (data && data.changingThisBreaksApplicationSecurity) ? data.changingThisBreaksApplicationSecurity : data.base64 ? data.base64 : data;
                }
                if (file.size === 0) {
                    file.sizes = data.file.size;
                }
            }
            if (data && data.changingThisBreaksApplicationSecurity && data.changingThisBreaksApplicationSecurity.split('data:')[0] === '') {
                file.url = 'data:' + fileObjType + data.changingThisBreaksApplicationSecurity.split('data:')[1];
            }
            return file;
        }
        else {
            return null;
        }
    }
    getExportJob() {
        const params = [{
                key: 'load_asset_details',
                value: 'true'
            }, {
                key: 'sort',
                value: 'desc_last_updated_date_time'
            }, {
                key: 'load_job_details',
                value: 'true'
            }, {
                key: 'last_updated_date_time',
                value: 'TODAY'
            }, {
                key: 'limit',
                value: '5000'
            }];
        this.otmmService.getExportJobs(params)
            .subscribe(response => {
            if (response && response.jobs_resource && response.jobs_resource.collection_summary &&
                response.jobs_resource.collection_summary.actual_count_of_items &&
                response.jobs_resource.collection_summary.actual_count_of_items > 0) {
                console.log("get export response");
            }
            else {
                console.log("No items");
            }
        }, error => {
            console.log("Unable to get data from QDS.");
        });
    }
    // MSV-239
    calculateInprogress() {
        const jobFile = [];
        const inprogressJobIdList = [];
        let hasPauseJobs = false;
        let hasStopJobs = false;
        this.qdsService.getJobs(true)
            .subscribe(getJobsResponse => {
            if (getJobsResponse && getJobsResponse.jobs && getJobsResponse.jobs.length === 0) {
                this.noData = true;
                this.noDataMsg = 'No items.';
                this.isLoadingData = false;
                return;
            }
            this.noData = false;
            getJobsResponse.jobs.forEach((job) => {
                if (job.filesTotal > 0) {
                    if (this.showFailed) {
                        job.files.forEach((file) => {
                            if (job.filesFailed > 0 && file.status === 'failed') {
                                const transferFile = {
                                    preview: file.thumbnail,
                                    filename: file.name,
                                    status: this.getFileTranferStatus(job, file),
                                    started: new Date(file.transferStart),
                                    type: job.isImport ? 'Upload' : 'Download',
                                    fileType: file.type,
                                    icon: this.assetFileConfigService.findIconByName(file.name),
                                    progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                                };
                                jobFile.push(transferFile);
                                if (transferFile.status === 'INPROGRESS') {
                                    inprogressJobIdList.push(job.qdsJobId);
                                }
                            }
                        });
                    }
                    else {
                        job.files.forEach((file) => {
                            const transferFile = {
                                preview: file.thumbnail,
                                filename: file.name,
                                status: this.getFileTranferStatus(job, file),
                                started: new Date(file.transferStart),
                                type: job.isImport ? 'Upload' : 'Download',
                                fileType: file.type,
                                icon: this.assetFileConfigService.findIconByName(file.name),
                                progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                            };
                            jobFile.push(transferFile);
                            if (transferFile.status === 'INPROGRESS') {
                                inprogressJobIdList.push(job.qdsJobId);
                                console.log('In progress jobs: ' + inprogressJobIdList);
                            }
                            if ((file.status === 'paused') && file.transferDone === -1) {
                                hasPauseJobs = true;
                            }
                            if ((file.status === 'starting') && file.transferDone === -1) {
                                hasStopJobs = true;
                            }
                        });
                    }
                    if (!job.completed && job.filesFailed === 0) {
                        if (job.isImport) {
                            this.activeJobIdList['IMPORT'][job.qdsJobId] = {
                                otmmJobId: job.otmmJobId,
                                qdsJobId: job.qdsJobId
                            };
                        }
                        else {
                            this.activeJobIdList['EXPORT'][job.qdsJobId] = {
                                otmmJobId: job.otmmJobId,
                                qdsJobId: job.qdsJobId
                            };
                        }
                    }
                    if (job.filesFailed > 0) {
                        this.failedJobIdList[job.qdsJobId] = {
                            isImport: job.isImport,
                            otmmJobId: job.otmmJobId,
                            qdsJobId: job.qdsJobId
                        };
                    }
                }
            });
            this.dataSource = new MatTableDataSource(jobFile);
            this.dataSource.sort = this.sort;
            this.isLoadingData = false;
            if (hasPauseJobs) {
                hasPauseJobs = false;
                this.resumeJobs('RESUME');
            }
            if (hasStopJobs && this.restartingJobCount < 2) {
                hasPauseJobs = false;
                this.resumeJobs('RESUME');
                this.restartingJobCount++;
            }
            if (inprogressJobIdList.length > 0 && this.liveDataSubscription === undefined) {
                this.liveDataSubscription = timer(0, 3000)
                    .pipe(switchMap(() => this.checkData()))
                    .subscribe(result => {
                    this.calculateInprogress();
                });
            }
            if (inprogressJobIdList.length === 0) {
                if (this.liveDataSubscription) {
                    this.liveDataSubscription.unsubscribe();
                }
            }
        }, getJobsError => {
            this.noData = true;
            this.noDataMsg = 'Unable to get data from QDS.';
            this.isLoadingData = false;
        });
    }
    getQDSTransferJobs() {
        return new Observable(observer => {
            const jobFile = [];
            const inprogressJobIdList = [];
            //this.getExportJob();//MSV-239
            this.qdsService.getJobs(true)
                .subscribe(getJobsResponse => {
                if (getJobsResponse && getJobsResponse.jobs && getJobsResponse.jobs.length === 0) {
                    this.noData = true;
                    this.noDataMsg = 'No items.';
                    this.isLoadingData = false;
                    return;
                }
                this.noData = false;
                getJobsResponse.jobs.forEach((job) => {
                    if (job.filesTotal > 0) {
                        if (this.showFailed) {
                            job.files.forEach((file) => {
                                if (job.filesFailed > 0 && file.status === 'failed') {
                                    const transferFile = {
                                        preview: file.thumbnail,
                                        filename: file.name,
                                        status: this.getFileTranferStatus(job, file),
                                        started: new Date(file.transferStart),
                                        type: job.isImport ? 'Upload' : 'Download',
                                        fileType: file.type,
                                        icon: this.assetFileConfigService.findIconByName(file.name),
                                        progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                                    };
                                    jobFile.push(transferFile);
                                    if (transferFile.status === 'INPROGRESS') {
                                        inprogressJobIdList.push(job.qdsJobId);
                                    }
                                }
                            });
                        }
                        else {
                            job.files.forEach((file) => {
                                const transferFile = {
                                    preview: file.thumbnail,
                                    filename: file.name,
                                    status: this.getFileTranferStatus(job, file),
                                    started: new Date(file.transferStart),
                                    type: job.isImport ? 'Upload' : 'Download',
                                    fileType: file.type,
                                    icon: this.assetFileConfigService.findIconByName(file.name),
                                    progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                                };
                                jobFile.push(transferFile);
                                if (transferFile.status === 'INPROGRESS') {
                                    inprogressJobIdList.push(job.qdsJobId);
                                    console.log('In progress jobs: ' + inprogressJobIdList);
                                }
                                /*   if (!file.thumbnail) {
                                    this.getBase64(file.path).subscribe(base64Response => {
                                      const obj = this.createFileObj(file, base64Response);
                                      console.log(obj);
                                      const transferFile: QDSJobFile = {
                                        preview: file.thumbnail,
                                        filename: file.name,
                                        status: this.getFileTranferStatus(job, file),
                                        started: new Date(file.transferStart),
                                        type: job.isImport ? 'Upload' : 'Download',
                                        fileType: file.type,
                                        icon: this.assetFileConfigService.findIconByName(file.name),
                                        progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                                      };
                                      jobFile.push(transferFile);
                  
                                      if (transferFile.status === 'INPROGRESS') {
                                        inprogressJobIdList.push(job.qdsJobId);
                                        console.log('In progress jobs: ' + inprogressJobIdList);
                                      }
                                    });
                                  } else {
                                    const transferFile: QDSJobFile = {
                                      preview: file.thumbnail,
                                      filename: file.name,
                                      status: this.getFileTranferStatus(job, file),
                                      started: new Date(file.transferStart),
                                      type: job.isImport ? 'Upload' : 'Download',
                                      fileType: file.type,
                                      icon: this.assetFileConfigService.findIconByName(file.name),
                                      progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                                    };
                                    jobFile.push(transferFile);
                  
                                    if (transferFile.status === 'INPROGRESS') {
                                      inprogressJobIdList.push(job.qdsJobId);
                                      console.log('In progress jobs: ' + inprogressJobIdList);
                                    }
                                  } */
                            });
                            /* const transferFile: QDSJobFile = {
                              preview: file.thumbnail,
                              filename: file.name,
                              status: this.getFileTranferStatus(job, file),
                              started: new Date(file.transferStart),
                              type: job.isImport ? 'Upload' : 'Download',
                              fileType: file.type,
                              icon: this.assetFileConfigService.findIconByName(file.name),
                              progress: this.qdsService.getFileTransferProgress(file.size, file.transferred)
                            };
                            jobFile.push(transferFile);
              
                            if (transferFile.status === 'INPROGRESS') {
                              inprogressJobIdList.push(job.qdsJobId);
                              console.log('In progress jobs: ' + inprogressJobIdList);
                            }
                          }); */
                        }
                        if (!job.completed && job.filesFailed === 0) {
                            if (job.isImport) {
                                this.activeJobIdList['IMPORT'][job.qdsJobId] = {
                                    otmmJobId: job.otmmJobId,
                                    qdsJobId: job.qdsJobId
                                };
                            }
                            else {
                                this.activeJobIdList['EXPORT'][job.qdsJobId] = {
                                    otmmJobId: job.otmmJobId,
                                    qdsJobId: job.qdsJobId
                                };
                            }
                        }
                        if (job.filesFailed > 0) {
                            this.failedJobIdList[job.qdsJobId] = {
                                isImport: job.isImport,
                                otmmJobId: job.otmmJobId,
                                qdsJobId: job.qdsJobId
                            };
                        }
                    }
                });
                this.dataSource = new MatTableDataSource(jobFile);
                this.dataSource.sort = this.sort;
                this.isLoadingData = false;
                /* if (inprogressJobIdList.length > 0 && this.liveDataSubscription === undefined) {
                  this.liveDataSubscription = timer(0, 2000)
                    .pipe(switchMap(() => this.checkData()))
                    .subscribe(result => {
                      this.getQDSTransferJobs();
                    });
                }
        
                if (inprogressJobIdList.length === 0) {
                  if (this.liveDataSubscription) {
                    this.liveDataSubscription.unsubscribe();
                  }
                } */
                observer.next(true);
                observer.complete();
            }, getJobsError => {
                this.noData = true;
                this.noDataMsg = 'Unable to get data from QDS.';
                this.isLoadingData = false;
                observer.next(false);
            });
        });
    }
    checkData() {
        return new Observable(observer => {
            observer.next(true);
            observer.complete();
        });
    }
    // MSV-239
    resumeJobs(state) {
        const that = this;
        const activeImportJobList = this.activeJobIdList['IMPORT'];
        const activeExportJobList = this.activeJobIdList['EXPORT'];
        Object.keys(activeImportJobList).forEach(function (jobId) {
            const importJobId = activeImportJobList[jobId];
            that.qdsService.toggleQDSImportJob(importJobId, state)
                .subscribe(response => {
                if (response === 'COMPLETE') {
                    delete that.activeJobIdList['IMPORT'][jobId];
                }
            }, error => {
                this.notificationService.showError('Something went wrong, Try again');
            });
        });
        Object.keys(activeExportJobList).forEach(function (jobId) {
            const exportJobId = activeExportJobList[jobId];
            that.qdsService.toggleQDSExportJob(exportJobId, state)
                .subscribe(response => {
                if (response === 'COMPLETE') {
                    delete that.activeJobIdList['EXPORT'][jobId];
                }
            }, error => {
                this.notificationService.showError('Something went wrong, Try again');
            });
        });
    }
    toggleImportJobs(state) {
        const that = this;
        const activeImportJobList = this.activeJobIdList['IMPORT'];
        const activeExportJobList = this.activeJobIdList['EXPORT'];
        Object.keys(activeImportJobList).forEach(function (jobId) {
            const importJobId = activeImportJobList[jobId];
            that.qdsService.toggleQDSImportJob(importJobId, state)
                .subscribe(response => {
                if (response === 'COMPLETE') {
                    delete that.activeJobIdList['IMPORT'][jobId];
                }
                that.getDownloadLocation();
            }, error => {
                that.getDownloadLocation();
                this.notificationService.showError('Something went wrong, Try again');
            });
        });
        Object.keys(activeExportJobList).forEach(function (jobId) {
            const exportJobId = activeExportJobList[jobId];
            that.qdsService.toggleQDSExportJob(exportJobId, state)
                .subscribe(response => {
                if (response === 'COMPLETE') {
                    delete that.activeJobIdList['EXPORT'][jobId];
                }
                that.getDownloadLocation();
            }, error => {
                that.getDownloadLocation();
                this.notificationService.showError('Something went wrong, Try again');
            });
        });
    }
    retryFailedJobs() {
        const that = this;
        const failedImportJobList = this.failedJobIdList;
        Object.keys(failedImportJobList).forEach(function (job) {
            that.qdsService.retryFailedJob(job)
                .subscribe(response => {
                that.getDownloadLocation();
            }, error => {
                that.getDownloadLocation();
                this.notificationService.showError('Something went wrong, Try again');
            });
        });
    }
    ngAfterViewInit() {
        this.otmmService.checkOtmmSession(undefined, true) // MVS-239
            .subscribe(response => {
            this.loaderService.hide();
            this.isLoadingData = false;
            if (!otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload &&
                !otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
                this.showActions = false;
                this.isLoadingData = false;
                this.noData = true;
                this.noDataMsg = 'QDS is not installed or connected.';
            }
            else {
                this.getDownloadLocation();
            }
        }, error => {
            this.loaderService.hide();
            this.isLoadingData = false;
            this.noData = true;
            this.noDataMsg = 'Unable to connect to Media Manager';
            this.notificationService.error('Unable to initialize Media Manager session');
        });
    }
    ngOnDestroy() {
        if (this.liveDataSubscription) {
            this.liveDataSubscription.unsubscribe();
        }
    }
};
QdsTransferTrayComponent.ctorParameters = () => [
    { type: LoaderService },
    { type: OTMMService },
    { type: NotificationService },
    { type: QdsService },
    { type: AssetFileConfigService }
];
__decorate([
    ViewChild(MatSort, { static: true })
], QdsTransferTrayComponent.prototype, "sort", void 0);
QdsTransferTrayComponent = __decorate([
    Component({
        selector: 'mpm-qds-transfer-tray',
        template: "<mat-spinner diameter=\"40\" *ngIf=\"isLoadingData\"></mat-spinner>\r\n\r\n<mat-card class=\"qds-transfer-actions\" *ngIf=\"!isLoadingData && showActions\">\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <mat-checkbox class=\"qds-failed-checkbox\" [disabled]=\"noData\" [(ngModel)]=\"showFailed\"\r\n                (change)=\"getQDSTransferJobs()\">Show Failed Only\r\n            </mat-checkbox>\r\n        </div>\r\n        <div class=\"flex-row-item\">\r\n            <button mat-button class=\"action-button\" (click)=\"refresh()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n\r\n        <div class=\"flex-spacer\"></div>\r\n\r\n        <div class=\"flex-row-item\" style=\"align-items: center;\">\r\n            <span class=\"download-location\" matTooltip=\"{{downloadLocation}}\">{{downloadLocation}}</span>\r\n            <button mat-icon-button matTooltip=\"Change download location\" (click)=\"changeDownloadLocation()\"\r\n                [disabled]=\"noData\">\r\n                <mat-icon style=\"font-size: 14px;\">edit</mat-icon>\r\n            </button>\r\n            <button mat-icon-button matTooltip=\"Copy download location\" (click)=\"copyDownloadLocation()\"\r\n                [disabled]=\"noData\">\r\n                <mat-icon style=\"font-size: 14px;\">file_copy</mat-icon>\r\n            </button>\r\n        </div>\r\n\r\n        <div class=\"flex-spacer\"></div>\r\n\r\n        <div class=\"flex-row-item justify-flex-end\">\r\n            <button mat-button class=\"action-button\" (click)=\"toggleImportJobs('RESUME')\" matTooltip=\"Resume all\"\r\n                [disabled]=\"noData\">\r\n                <mat-icon>play_circle_outline</mat-icon>\r\n            </button>\r\n        </div>\r\n        <div class=\"flex-row-item justify-flex-end \">\r\n            <button mat-button class=\"action-button\" (click)=\"toggleImportJobs('PAUSE')\" matTooltip=\"Pause all\"\r\n                [disabled]=\"noData\">\r\n                <mat-icon>pause_circle_outline</mat-icon>\r\n            </button>\r\n        </div>\r\n        <div class=\"flex-row-item justify-flex-end\">\r\n            <button mat-button class=\"action-button\" (click)=\"retryFailedJobs()\" matTooltip=\"Retry all failed\"\r\n                [disabled]=\"noData\">\r\n                <mat-icon>replay</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</mat-card>\r\n<hr />\r\n<div class=\"qds-files-container\" [hidden]=\"isLoadingData\">\r\n    <table mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n        <ng-container matColumnDef=\"preview\">\r\n            <th mat-header-cell *matHeaderCellDef> Preview </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <mat-icon\r\n                    *ngIf=\"element.icon && (thumbnailFormats.indexOf(element.fileType) === -1 || !element.preview)\"\r\n                    class=\"preview-icon\">\r\n                    {{element.icon}}\r\n                </mat-icon>\r\n                <img *ngIf=\"element.preview && (thumbnailFormats.indexOf(element.fileType) !== -1)\" class=\"preview\"\r\n                    src=\"{{element.preview}}\">\r\n            </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"filename\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <div class=\"file-name\">\r\n                    <span matTooltip=\"{{element.filename}}\">{{element.filename}}</span>\r\n                </div>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"status\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <mat-icon *ngIf=\"element.status == 'COMPLETE'\" class=\"success\" matTooltip=\"Transfer Complete\">done\r\n                </mat-icon>\r\n                <mat-icon *ngIf=\"element.status == 'FAILED'\" class=\"danger\" matTooltip=\"Transfer Failed\">error\r\n                </mat-icon>\r\n                <span *ngIf=\"element.status == 'INPROGRESS'\" class=\"info\">Inprogress: {{element.progress}}%</span>\r\n                <span *ngIf=\"element.status == 'PAUSED'\" class=\"warning\">Paused: {{element.progress}}%</span>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"started\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Started </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.started | formatToLocaleDateTime}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"type\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Type </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.type}} </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n</div>\r\n\r\n<div *ngIf=\"noData && !isLoadingData\" class=\"no-data\">\r\n    <span>{{noDataMsg}}</span>\r\n</div>",
        styles: [".float-right{float:right}.justify-flex-end{justify-content:flex-end}.flex-spacer{display:flex;flex-grow:5}.qds-transfer-actions{padding:0;border-radius:0}.qds-transfer-actions .flex-row{align-items:center}.qds-transfer-actions .flex-row .flex-row-item{flex-grow:0!important}.qds-transfer-actions mat-checkbox{padding:0 10px}.qds-transfer-actions .action-button{border-radius:0}.qds-transfer-actions .download-location{font-size:15px;max-width:calc(100vh - 300px);white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.qds-files-container{overflow:scroll}.qds-files-container table{width:100%}.qds-files-container table .preview{padding:5px 5px 5px 0;width:64px;height:54px}.qds-files-container table .preview-icon{font-size:64px;height:64px;width:64px}mat-spinner.mat-progress-spinner{position:absolute;z-index:999999;top:48vh;left:48vw}.file-name{width:25em;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:inline-block}.success{color:green}.info{color:#1e90ff}.danger{color:red}.warning{color:orange}.no-data{padding:24px;text-align:center}.qds-failed-checkbox{font-size:14px}"]
    })
], QdsTransferTrayComponent);
export { QdsTransferTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvcWRzLXRyYW5zZmVyLXRyYXkvcWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQWdCLE1BQU0sTUFBTSxDQUFDO0FBQ3ZELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDNUYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFrQnZGLElBQWEsd0JBQXdCLEdBQXJDLE1BQWEsd0JBQXdCO0lBZ0NuQyxZQUNTLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxVQUFzQixFQUN0QixzQkFBOEM7UUFKOUMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFuQ3ZELHFCQUFnQixHQUFhLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBRWxGLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLHFCQUFnQixHQUFHLENBQUMsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDekUseUJBQW9CLEdBQUc7WUFDckIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsV0FBVyxFQUFFLFVBQVU7WUFDdkIsTUFBTSxFQUFFLFFBQVE7WUFDaEIsWUFBWSxFQUFFLFlBQVk7WUFDMUIsTUFBTSxFQUFFLFlBQVk7WUFDcEIsUUFBUSxFQUFFLFlBQVk7WUFDdEIsWUFBWSxFQUFFLFlBQVk7WUFDMUIsT0FBTyxFQUFFLFlBQVk7U0FDdEIsQ0FBQztRQUNGLG9CQUFlLEdBQUc7WUFDaEIsTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsRUFBRTtTQUNYLENBQUM7UUFDRixvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBRW5CLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUVuQix1QkFBa0IsR0FBRyxDQUFDLENBQUE7UUFFZ0IsU0FBSSxHQUFZLElBQUksT0FBTyxFQUFFLENBQUM7UUFXbEUsTUFBTSxXQUFXLEdBQWlCLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxHQUFHLEVBQUUsSUFBSTtRQUM1QixJQUFJLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxJQUFJLEdBQUcsQ0FBQyxXQUFXLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRTtZQUN4RixPQUFPLFFBQVEsQ0FBQztTQUNqQjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9DO0lBQ0gsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsb0JBQW9CO1FBQ2xCLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbEQsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQ2hDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUN4QixNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDdkIsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNmLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoQixRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRWxDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUU7YUFDaEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDO1lBQ2hELDRCQUE0QjtZQUM1QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzdCLENBQUMsQ0FBQyxDQUFBO1lBQ0YsNENBQTRDO1FBQzlDLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQztRQUNwRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBc0I7UUFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRTthQUNuQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztRQUNuQyxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDVCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7UUFDcEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsU0FBUyxDQUFDLFFBQVE7UUFDaEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQix5Q0FBeUM7WUFDekMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksR0FBRyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7WUFDL0IsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO1lBQzFCLEdBQUcsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDO2dCQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0IseUNBQXlDO2dCQUN6QyxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO2dCQUM5QixNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBQ3hCLElBQUksTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyx5QkFBeUI7b0JBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUUzQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNwQix5Q0FBeUM7b0JBQ3pDLElBQUksT0FBTyxHQUFHO3dCQUNaLE1BQU0sRUFBRSxNQUFNO3dCQUNkLElBQUksRUFBRSxJQUFJO3FCQUNYLENBQUM7b0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVM7b0JBQ2pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDO2dCQUNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDO1lBQ0YsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2IsQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDO0lBRUQsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsV0FBaUI7UUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixJQUFJLElBQUksSUFBSSxJQUFJLEtBQUssSUFBSSxFQUFFO1lBQ3pCLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQ0FBcUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNFLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyx3QkFBd0IsRUFBRTtvQkFDdEgsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsa0RBQWtEO2lCQUMzRTtxQkFBTTtvQkFDTCxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDako7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDN0I7YUFDRjtZQUNELElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxxQ0FBcUMsSUFBSSxJQUFJLENBQUMscUNBQXFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDN0gsSUFBSSxDQUFDLEdBQUcsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLElBQUksQ0FBQyxxQ0FBcUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakc7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNiO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELFlBQVk7UUFFVixNQUFNLE1BQU0sR0FBRyxDQUFDO2dCQUVkLEdBQUcsRUFBRSxvQkFBb0I7Z0JBQ3pCLEtBQUssRUFBRSxNQUFNO2FBQ2QsRUFBRTtnQkFDRCxHQUFHLEVBQUUsTUFBTTtnQkFDWCxLQUFLLEVBQUUsNkJBQTZCO2FBQ3JDLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLGtCQUFrQjtnQkFDdkIsS0FBSyxFQUFFLE1BQU07YUFDZCxFQUFFO2dCQUNELEdBQUcsRUFBRSx3QkFBd0I7Z0JBQzdCLEtBQUssRUFBRSxPQUFPO2FBQ2YsRUFBRTtnQkFDRCxHQUFHLEVBQUUsT0FBTztnQkFDWixLQUFLLEVBQUUsTUFBTTthQUNkLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQzthQUNuQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLGtCQUFrQjtnQkFDakYsUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUI7Z0JBQy9ELFFBQVEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxFQUFFO2dCQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7YUFDcEM7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN6QjtRQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUM5QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxVQUFVO0lBQ1YsbUJBQW1CO1FBQ2pCLE1BQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNuQixNQUFNLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQzthQUMxQixTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDM0IsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLElBQUksSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ2hGLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLE9BQU87YUFDUjtZQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBUSxFQUFFLEVBQUU7Z0JBQ3hDLElBQUksR0FBRyxDQUFDLFVBQVUsR0FBRyxDQUFDLEVBQUU7b0JBQ3RCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDbkIsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTs0QkFDOUIsSUFBSSxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLFFBQVEsRUFBRTtnQ0FDbkQsTUFBTSxZQUFZLEdBQWU7b0NBQy9CLE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUztvQ0FDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxJQUFJO29DQUNuQixNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7b0NBQzVDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO29DQUNyQyxJQUFJLEVBQUUsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVO29DQUMxQyxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUk7b0NBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0NBQzNELFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQztpQ0FDL0UsQ0FBQztnQ0FDRixPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2dDQUUzQixJQUFJLFlBQVksQ0FBQyxNQUFNLEtBQUssWUFBWSxFQUFFO29DQUN4QyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lDQUN4Qzs2QkFDRjt3QkFDSCxDQUFDLENBQUMsQ0FBQztxQkFDSjt5QkFBTTt3QkFDTCxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFOzRCQUM5QixNQUFNLFlBQVksR0FBZTtnQ0FDL0IsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTO2dDQUN2QixRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0NBQ25CLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQztnQ0FDNUMsT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7Z0NBQ3JDLElBQUksRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVU7Z0NBQzFDLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSTtnQ0FDbkIsSUFBSSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQ0FDM0QsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDOzZCQUMvRSxDQUFDOzRCQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBRTNCLElBQUksWUFBWSxDQUFDLE1BQU0sS0FBSyxZQUFZLEVBQUU7Z0NBQ3hDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7Z0NBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEdBQUcsbUJBQW1CLENBQUMsQ0FBQzs2QkFDekQ7NEJBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxDQUFDLENBQUMsRUFBRTtnQ0FDMUQsWUFBWSxHQUFHLElBQUksQ0FBQzs2QkFDckI7NEJBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxDQUFDLENBQUMsRUFBRTtnQ0FDNUQsV0FBVyxHQUFHLElBQUksQ0FBQzs2QkFDcEI7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBRUo7b0JBRUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLElBQUksR0FBRyxDQUFDLFdBQVcsS0FBSyxDQUFDLEVBQUU7d0JBQzNDLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRTs0QkFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUc7Z0NBQzdDLFNBQVMsRUFBRSxHQUFHLENBQUMsU0FBUztnQ0FDeEIsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFROzZCQUN2QixDQUFDO3lCQUNIOzZCQUFNOzRCQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHO2dDQUM3QyxTQUFTLEVBQUUsR0FBRyxDQUFDLFNBQVM7Z0NBQ3hCLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUTs2QkFDdkIsQ0FBQzt5QkFDSDtxQkFDRjtvQkFFRCxJQUFJLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFFO3dCQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRzs0QkFDbkMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFROzRCQUN0QixTQUFTLEVBQUUsR0FBRyxDQUFDLFNBQVM7NEJBQ3hCLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUTt5QkFDdkIsQ0FBQztxQkFDSDtpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFFM0IsSUFBSSxZQUFZLEVBQUU7Z0JBQ2hCLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDM0I7WUFDRCxJQUFJLFdBQVcsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxFQUFFO2dCQUM5QyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUMzQjtZQUNELElBQUksbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEtBQUssU0FBUyxFQUFFO2dCQUM3RSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUM7cUJBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7cUJBQ3ZDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFFRCxJQUFJLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO29CQUM3QixJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3pDO2FBQ0Y7UUFDSCxDQUFDLEVBQUUsWUFBWSxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyw4QkFBOEIsQ0FBQztZQUNoRCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbkIsTUFBTSxtQkFBbUIsR0FBRyxFQUFFLENBQUM7WUFDL0IsK0JBQStCO1lBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztpQkFDMUIsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUMzQixJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsSUFBSSxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDaEYsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO29CQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztvQkFDM0IsT0FBTztpQkFDUjtnQkFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDcEIsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFRLEVBQUUsRUFBRTtvQkFDeEMsSUFBSSxHQUFHLENBQUMsVUFBVSxHQUFHLENBQUMsRUFBRTt3QkFDdEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOzRCQUNuQixHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO2dDQUM5QixJQUFJLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO29DQUNuRCxNQUFNLFlBQVksR0FBZTt3Q0FDL0IsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTO3dDQUN2QixRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUk7d0NBQ25CLE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQzt3Q0FDNUMsT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7d0NBQ3JDLElBQUksRUFBRSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVU7d0NBQzFDLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSTt3Q0FDbkIsSUFBSSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt3Q0FDM0QsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDO3FDQUMvRSxDQUFDO29DQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7b0NBRTNCLElBQUksWUFBWSxDQUFDLE1BQU0sS0FBSyxZQUFZLEVBQUU7d0NBQ3hDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7cUNBQ3hDO2lDQUNGOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKOzZCQUFNOzRCQUNMLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7Z0NBQzlCLE1BQU0sWUFBWSxHQUFlO29DQUMvQixPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0NBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSTtvQ0FDbkIsTUFBTSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDO29DQUM1QyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztvQ0FDckMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsVUFBVTtvQ0FDMUMsUUFBUSxFQUFFLElBQUksQ0FBQyxJQUFJO29DQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29DQUMzRCxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUM7aUNBQy9FLENBQUM7Z0NBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQ0FFM0IsSUFBSSxZQUFZLENBQUMsTUFBTSxLQUFLLFlBQVksRUFBRTtvQ0FDeEMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQ0FDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxtQkFBbUIsQ0FBQyxDQUFDO2lDQUN6RDtnQ0FDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7c0NBc0NNOzRCQUVSLENBQUMsQ0FBQyxDQUFDOzRCQUNIOzs7Ozs7Ozs7Ozs7Ozs7O2dDQWdCSTt5QkFDTDt3QkFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsSUFBSSxHQUFHLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTs0QkFDM0MsSUFBSSxHQUFHLENBQUMsUUFBUSxFQUFFO2dDQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRztvQ0FDN0MsU0FBUyxFQUFFLEdBQUcsQ0FBQyxTQUFTO29DQUN4QixRQUFRLEVBQUUsR0FBRyxDQUFDLFFBQVE7aUNBQ3ZCLENBQUM7NkJBQ0g7aUNBQU07Z0NBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUc7b0NBQzdDLFNBQVMsRUFBRSxHQUFHLENBQUMsU0FBUztvQ0FDeEIsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFRO2lDQUN2QixDQUFDOzZCQUNIO3lCQUNGO3dCQUVELElBQUksR0FBRyxDQUFDLFdBQVcsR0FBRyxDQUFDLEVBQUU7NEJBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHO2dDQUNuQyxRQUFRLEVBQUUsR0FBRyxDQUFDLFFBQVE7Z0NBQ3RCLFNBQVMsRUFBRSxHQUFHLENBQUMsU0FBUztnQ0FDeEIsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFROzZCQUN2QixDQUFDO3lCQUNIO3FCQUNGO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBRTNCOzs7Ozs7Ozs7Ozs7b0JBWUk7Z0JBQ0osUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxZQUFZLENBQUMsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsOEJBQThCLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsU0FBUztRQUNQLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsVUFBVTtJQUNWLFVBQVUsQ0FBQyxLQUFLO1FBQ2QsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLE1BQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzRCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFM0QsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUs7WUFDdEQsTUFBTSxXQUFXLEdBQUcsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDO2lCQUNuRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksUUFBUSxLQUFLLFVBQVUsRUFBRTtvQkFDM0IsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QztZQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDVCxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7WUFDeEUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLO1lBQ3RELE1BQU0sV0FBVyxHQUFHLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQztpQkFDbkQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNwQixJQUFJLFFBQVEsS0FBSyxVQUFVLEVBQUU7b0JBQzNCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDOUM7WUFDSCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsS0FBSztRQUNwQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbEIsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELE1BQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUUzRCxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSztZQUN0RCxNQUFNLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUM7aUJBQ25ELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLEtBQUssVUFBVSxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzlDO2dCQUNELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzdCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDVCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSztZQUN0RCxNQUFNLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUM7aUJBQ25ELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLEtBQUssVUFBVSxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzlDO2dCQUNELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzdCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDVCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNiLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQztRQUNsQixNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7UUFFakQsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUc7WUFDcEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDO2lCQUNoQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzdCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDVCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLFVBQVU7YUFDMUQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVc7Z0JBQzNELENBQUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxFQUFFO2dCQUM3RCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLG9DQUFvQyxDQUFDO2FBQ3ZEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQzVCO1FBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLG9DQUFvQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQztRQUMvRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3pDO0lBQ0gsQ0FBQztDQUVGLENBQUE7O1lBcmpCeUIsYUFBYTtZQUNmLFdBQVc7WUFDSCxtQkFBbUI7WUFDNUIsVUFBVTtZQUNFLHNCQUFzQjs7QUFUakI7SUFBckMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztzREFBK0I7QUE1QnpELHdCQUF3QjtJQUxwQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsdUJBQXVCO1FBQ2pDLDB0S0FBaUQ7O0tBRWxELENBQUM7R0FDVyx3QkFBd0IsQ0FzbEJwQztTQXRsQlksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCB0aW1lciwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEFzc2V0RmlsZUNvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi91cGxvYWQvc2VydmljZXMvYXNzZXQuZmlsZS5jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBRRFNKb2JGaWxlIHtcclxuICBwcmV2aWV3OiBhbnk7XHJcbiAgZmlsZW5hbWU6IHN0cmluZztcclxuICBzdGF0dXM6IHN0cmluZztcclxuICBzdGFydGVkOiBEYXRlO1xyXG4gIHR5cGU6IHN0cmluZztcclxuICBmaWxlVHlwZTogc3RyaW5nO1xyXG4gIGljb246IHN0cmluZztcclxuICBwcm9ncmVzczogbnVtYmVyO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1xZHMtdHJhbnNmZXItdHJheScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3Fkcy10cmFuc2Zlci10cmF5LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9xZHMtdHJhbnNmZXItdHJheS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBRZHNUcmFuc2ZlclRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsncHJldmlldycsICdmaWxlbmFtZScsICdzdGF0dXMnLCAnc3RhcnRlZCcsICd0eXBlJ107XHJcbiAgZGF0YVNvdXJjZTogTWF0VGFibGVEYXRhU291cmNlPFFEU0pvYkZpbGU+O1xyXG4gIGlzTG9hZGluZ0RhdGEgPSB0cnVlO1xyXG4gIHRodW1ibmFpbEZvcm1hdHMgPSBbJ2ltYWdlL2pwZWcnLCAnaW1hZ2UvcG5nJywgJ2ltYWdlL2pwZycsICdpbWFnZS9naWYnXTtcclxuICB0cmFuc2ZlclN0YXR1c01hcHBlciA9IHtcclxuICAgIGNvbXBsZXRlOiAnQ09NUExFVEUnLFxyXG4gICAgdHJhbnNmZXJyZWQ6ICdDT01QTEVURScsXHJcbiAgICBmYWlsZWQ6ICdGQUlMRUQnLFxyXG4gICAgcmVhc3NlbWJsaW5nOiAnSU5QUk9HUkVTUycsXHJcbiAgICBzYXZpbmc6ICdJTlBST0dSRVNTJyxcclxuICAgIHN0YXJ0aW5nOiAnSU5QUk9HUkVTUycsXHJcbiAgICB0cmFuc2ZlcnJpbmc6ICdJTlBST0dSRVNTJyxcclxuICAgIHdhaXRpbmc6ICdJTlBST0dSRVNTJyxcclxuICB9O1xyXG4gIGFjdGl2ZUpvYklkTGlzdCA9IHtcclxuICAgIEVYUE9SVDoge30sXHJcbiAgICBJTVBPUlQ6IHt9XHJcbiAgfTtcclxuICBmYWlsZWRKb2JJZExpc3QgPSB7fTtcclxuICBzaG93RmFpbGVkID0gZmFsc2U7XHJcbiAgbm9EYXRhTXNnOiBzdHJpbmc7XHJcbiAgbm9EYXRhID0gZmFsc2U7XHJcbiAgc2hvd0FjdGlvbnMgPSB0cnVlO1xyXG4gIGRvd25sb2FkTG9jYXRpb246IHN0cmluZztcclxuICByZXN0YXJ0aW5nSm9iQ291bnQgPSAwXHJcblxyXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCwgeyBzdGF0aWM6IHRydWUgfSkgc29ydDogTWF0U29ydCA9IG5ldyBNYXRTb3J0KCk7XHJcblxyXG4gIGxpdmVEYXRhU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwdWJsaWMgcWRzU2VydmljZTogUWRzU2VydmljZSxcclxuICAgIHB1YmxpYyBhc3NldEZpbGVDb25maWdTZXJ2aWNlOiBBc3NldEZpbGVDb25maWdTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICBjb25zdCBRRFNKb2JGaWxlczogUURTSm9iRmlsZVtdID0gW107XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKFFEU0pvYkZpbGVzKTtcclxuICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmlsZVRyYW5mZXJTdGF0dXMoam9iLCBmaWxlKSB7XHJcbiAgICBpZiAoam9iLnN0YXJ0ZWQgJiYgIWpvYi5ydW5uaW5nICYmIGpvYi5maWxlc0ZhaWxlZCA9PT0gMCAmJiBmaWxlLnRyYW5zZmVycmVkIDwgZmlsZS5zaXplKSB7XHJcbiAgICAgIHJldHVybiAnUEFVU0VEJztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnRyYW5zZmVyU3RhdHVzTWFwcGVyW2ZpbGUuc3RhdHVzXTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlZnJlc2goKSB7XHJcbiAgICB0aGlzLnNob3dGYWlsZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuZ2V0RG93bmxvYWRMb2NhdGlvbigpO1xyXG4gIH1cclxuXHJcbiAgY29weURvd25sb2FkTG9jYXRpb24oKSB7XHJcbiAgICBjb25zdCBzZWxCb3ggPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZXh0YXJlYScpO1xyXG4gICAgc2VsQm94LnN0eWxlLnBvc2l0aW9uID0gJ2ZpeGVkJztcclxuICAgIHNlbEJveC5zdHlsZS5sZWZ0ID0gJzAnO1xyXG4gICAgc2VsQm94LnN0eWxlLnRvcCA9ICcwJztcclxuICAgIHNlbEJveC5zdHlsZS5vcGFjaXR5ID0gJzAnO1xyXG4gICAgc2VsQm94LnZhbHVlID0gdGhpcy5kb3dubG9hZExvY2F0aW9uO1xyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzZWxCb3gpO1xyXG4gICAgc2VsQm94LmZvY3VzKCk7XHJcbiAgICBzZWxCb3guc2VsZWN0KCk7XHJcbiAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnY29weScpO1xyXG4gICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChzZWxCb3gpO1xyXG5cclxuICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdEb3dubG9hZCBsb2NhdGlvbiBjb3BpZWQgdG8gY2xpcGJvYXJkJyk7XHJcbiAgfVxyXG5cclxuICBnZXREb3dubG9hZExvY2F0aW9uKCkge1xyXG4gICAgdGhpcy5yZXN0YXJ0aW5nSm9iQ291bnQgPSAwO1xyXG4gICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gdHJ1ZTtcclxuICAgIC8vdGhpcy50b2dnbGVJbXBvcnRKb2JzKCdSRVNVTUUnKTsvLyBNU1YtMjM5XHJcbiAgICB0aGlzLnFkc1NlcnZpY2UuZ2V0RG93bmxvYWRGb2xkZXIoKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkTG9jYXRpb24gPSByZXNwb25zZS5kb3dubG9hZEZvbGRlcjtcclxuICAgICAgICAvL3RoaXMuZ2V0UURTVHJhbnNmZXJKb2JzKCk7XHJcbiAgICAgICAgdGhpcy5nZXRRRFNUcmFuc2ZlckpvYnMoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgdGhpcy5yZXN1bWVKb2JzKCdSRVNVTUUnKTtcclxuICAgICAgICAgIHRoaXMuY2FsY3VsYXRlSW5wcm9ncmVzcygpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLy90aGlzLnRvZ2dsZUltcG9ydEpvYnMoJ1JFU1VNRScpOy8vIE1TVi0yMzlcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3JvbmcsIFRyeSBhZ2FpbicpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGNoYW5nZURvd25sb2FkTG9jYXRpb24oKSB7XHJcbiAgICB0aGlzLnFkc1NlcnZpY2UuY2hvb3NlRG93bmxvYWRGb2xkZXIoKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkTG9jYXRpb24gPSByZXNwb25zZTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3JvbmcsIFRyeSBhZ2FpbicpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEJhc2U2NChmaWxlUGF0aCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHByZWZlci1jb25zdFxyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuICAgICAgeGhyLm9wZW4oJ0dFVCcsIGZpbGVQYXRoLCB0cnVlKTtcclxuICAgICAgeGhyLnJlc3BvbnNlVHlwZSA9ICdibG9iJztcclxuICAgICAgeGhyLm9ubG9hZCA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5yZXNwb25zZSk7XHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBwcmVmZXItY29uc3RcclxuICAgICAgICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgICByZWFkZXIub25sb2FkID0gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICB2YXIgYmFzZTY0ID0gcmVhZGVyLnJlc3VsdDsgLy8gKGV2ZW50LnRhcmdldC5yZXN1bHQpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2cocmVhZGVyLnJlc3VsdCk7XHJcblxyXG4gICAgICAgICAgY29uc29sZS5sb2coYmFzZTY0KTtcclxuICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogcHJlZmVyLWNvbnN0XHJcbiAgICAgICAgICB2YXIgZmlsZU9iaiA9IHtcclxuICAgICAgICAgICAgYmFzZTY0OiBiYXNlNjQsXHJcbiAgICAgICAgICAgIGZpbGU6IGZpbGVcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhmaWxlT2JqKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmlsZU9iaik7IC8vIGJhc2U2NFxyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIHZhciBmaWxlID0gdGhpcy5yZXNwb25zZTtcclxuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcclxuICAgICAgfTtcclxuICAgICAgeGhyLnNlbmQoKTtcclxuICAgIH0pO1xyXG5cclxuICB9XHJcblxyXG4gIGNyZWF0ZUZpbGVPYmooZmlsZSwgZGF0YSwgZmlsZU9ialR5cGU/OiBhbnkpIHtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgaWYgKGZpbGUgJiYgZmlsZSAhPT0gbnVsbCkge1xyXG4gICAgICBpZiAoZGF0YSAmJiBkYXRhICE9PSBudWxsKSB7XHJcbiAgICAgICAgZmlsZS5kYXRhID0gKGRhdGEgJiYgZGF0YS5jaGFuZ2luZ1RoaXNCcmVha3NBcHBsaWNhdGlvblNlY3VyaXR5KSA/IGRhdGEuY2hhbmdpbmdUaGlzQnJlYWtzQXBwbGljYXRpb25TZWN1cml0eS5zcGxpdCgnYmFzZTY0LCcpWzFdIDpcclxuICAgICAgICAgIGRhdGEuYmFzZTY0ID8gZGF0YS5iYXNlNjQuc3BsaXQoJ2Jhc2U2NCwnKVsxXSA6IGRhdGEuc3BsaXQoJ2Jhc2U2NCwnKVsxXTtcclxuICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmJhc2U2NCAmJiBkYXRhLmJhc2U2NC5zcGxpdCgnYmFzZTY0JylbMF0uc3BsaXQoJ2RhdGE6JylbMV0uc3BsaXQoJzsnKVswXSA9PT0gJ2FwcGxpY2F0aW9uL3Bvc3RzY3JpcHQnKSB7XHJcbiAgICAgICAgICBmaWxlLnVybCA9IGRhdGEuYmFzZTY0OyAvLyAnZGF0YTonICsgZmlsZU9ialR5cGUgKyAnO2Jhc2U2NCwnICsgZmlsZS5kYXRhO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBmaWxlLnVybCA9IChkYXRhICYmIGRhdGEuY2hhbmdpbmdUaGlzQnJlYWtzQXBwbGljYXRpb25TZWN1cml0eSkgPyBkYXRhLmNoYW5naW5nVGhpc0JyZWFrc0FwcGxpY2F0aW9uU2VjdXJpdHkgOiBkYXRhLmJhc2U2NCA/IGRhdGEuYmFzZTY0IDogZGF0YTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpbGUuc2l6ZSA9PT0gMCkge1xyXG4gICAgICAgICAgZmlsZS5zaXplcyA9IGRhdGEuZmlsZS5zaXplO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoZGF0YSAmJiBkYXRhLmNoYW5naW5nVGhpc0JyZWFrc0FwcGxpY2F0aW9uU2VjdXJpdHkgJiYgZGF0YS5jaGFuZ2luZ1RoaXNCcmVha3NBcHBsaWNhdGlvblNlY3VyaXR5LnNwbGl0KCdkYXRhOicpWzBdID09PSAnJykge1xyXG4gICAgICAgIGZpbGUudXJsID0gJ2RhdGE6JyArIGZpbGVPYmpUeXBlICsgZGF0YS5jaGFuZ2luZ1RoaXNCcmVha3NBcHBsaWNhdGlvblNlY3VyaXR5LnNwbGl0KCdkYXRhOicpWzFdO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBmaWxlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRFeHBvcnRKb2IoKSB7XHJcblxyXG4gICAgY29uc3QgcGFyYW1zID0gW3tcclxuXHJcbiAgICAgIGtleTogJ2xvYWRfYXNzZXRfZGV0YWlscycsXHJcbiAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnc29ydCcsXHJcbiAgICAgIHZhbHVlOiAnZGVzY19sYXN0X3VwZGF0ZWRfZGF0ZV90aW1lJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdsb2FkX2pvYl9kZXRhaWxzJyxcclxuICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdsYXN0X3VwZGF0ZWRfZGF0ZV90aW1lJyxcclxuICAgICAgdmFsdWU6ICdUT0RBWSdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnbGltaXQnLFxyXG4gICAgICB2YWx1ZTogJzUwMDAnXHJcbiAgICB9XTtcclxuXHJcbiAgICB0aGlzLm90bW1TZXJ2aWNlLmdldEV4cG9ydEpvYnMocGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuam9ic19yZXNvdXJjZSAmJiByZXNwb25zZS5qb2JzX3Jlc291cmNlLmNvbGxlY3Rpb25fc3VtbWFyeSAmJlxyXG4gICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zICYmXHJcbiAgICAgICAgICByZXNwb25zZS5qb2JzX3Jlc291cmNlLmNvbGxlY3Rpb25fc3VtbWFyeS5hY3R1YWxfY291bnRfb2ZfaXRlbXMgPiAwKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcImdldCBleHBvcnQgcmVzcG9uc2VcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiTm8gaXRlbXNcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZ2V0IGRhdGEgZnJvbSBRRFMuXCIpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIE1TVi0yMzlcclxuICBjYWxjdWxhdGVJbnByb2dyZXNzKCkge1xyXG4gICAgY29uc3Qgam9iRmlsZSA9IFtdO1xyXG4gICAgY29uc3QgaW5wcm9ncmVzc0pvYklkTGlzdCA9IFtdO1xyXG4gICAgbGV0IGhhc1BhdXNlSm9icyA9IGZhbHNlO1xyXG4gICAgbGV0IGhhc1N0b3BKb2JzID0gZmFsc2U7XHJcbiAgICB0aGlzLnFkc1NlcnZpY2UuZ2V0Sm9icyh0cnVlKVxyXG4gICAgICAuc3Vic2NyaWJlKGdldEpvYnNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgaWYgKGdldEpvYnNSZXNwb25zZSAmJiBnZXRKb2JzUmVzcG9uc2Uuam9icyAmJiBnZXRKb2JzUmVzcG9uc2Uuam9icy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgIHRoaXMubm9EYXRhID0gdHJ1ZTtcclxuICAgICAgICAgIHRoaXMubm9EYXRhTXNnID0gJ05vIGl0ZW1zLic7XHJcbiAgICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubm9EYXRhID0gZmFsc2U7XHJcbiAgICAgICAgZ2V0Sm9ic1Jlc3BvbnNlLmpvYnMuZm9yRWFjaCgoam9iOiBhbnkpID0+IHtcclxuICAgICAgICAgIGlmIChqb2IuZmlsZXNUb3RhbCA+IDApIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc2hvd0ZhaWxlZCkge1xyXG4gICAgICAgICAgICAgIGpvYi5maWxlcy5mb3JFYWNoKChmaWxlOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChqb2IuZmlsZXNGYWlsZWQgPiAwICYmIGZpbGUuc3RhdHVzID09PSAnZmFpbGVkJykge1xyXG4gICAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2ZlckZpbGU6IFFEU0pvYkZpbGUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJldmlldzogZmlsZS50aHVtYm5haWwsXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZW5hbWU6IGZpbGUubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHRoaXMuZ2V0RmlsZVRyYW5mZXJTdGF0dXMoam9iLCBmaWxlKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGFydGVkOiBuZXcgRGF0ZShmaWxlLnRyYW5zZmVyU3RhcnQpLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IGpvYi5pc0ltcG9ydCA/ICdVcGxvYWQnIDogJ0Rvd25sb2FkJyxcclxuICAgICAgICAgICAgICAgICAgICBmaWxlVHlwZTogZmlsZS50eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIGljb246IHRoaXMuYXNzZXRGaWxlQ29uZmlnU2VydmljZS5maW5kSWNvbkJ5TmFtZShmaWxlLm5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICAgIHByb2dyZXNzOiB0aGlzLnFkc1NlcnZpY2UuZ2V0RmlsZVRyYW5zZmVyUHJvZ3Jlc3MoZmlsZS5zaXplLCBmaWxlLnRyYW5zZmVycmVkKVxyXG4gICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICBqb2JGaWxlLnB1c2godHJhbnNmZXJGaWxlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgIGlmICh0cmFuc2ZlckZpbGUuc3RhdHVzID09PSAnSU5QUk9HUkVTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICBpbnByb2dyZXNzSm9iSWRMaXN0LnB1c2goam9iLnFkc0pvYklkKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGpvYi5maWxlcy5mb3JFYWNoKChmaWxlOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRyYW5zZmVyRmlsZTogUURTSm9iRmlsZSA9IHtcclxuICAgICAgICAgICAgICAgICAgcHJldmlldzogZmlsZS50aHVtYm5haWwsXHJcbiAgICAgICAgICAgICAgICAgIGZpbGVuYW1lOiBmaWxlLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy5nZXRGaWxlVHJhbmZlclN0YXR1cyhqb2IsIGZpbGUpLFxyXG4gICAgICAgICAgICAgICAgICBzdGFydGVkOiBuZXcgRGF0ZShmaWxlLnRyYW5zZmVyU3RhcnQpLFxyXG4gICAgICAgICAgICAgICAgICB0eXBlOiBqb2IuaXNJbXBvcnQgPyAnVXBsb2FkJyA6ICdEb3dubG9hZCcsXHJcbiAgICAgICAgICAgICAgICAgIGZpbGVUeXBlOiBmaWxlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgIGljb246IHRoaXMuYXNzZXRGaWxlQ29uZmlnU2VydmljZS5maW5kSWNvbkJ5TmFtZShmaWxlLm5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICBwcm9ncmVzczogdGhpcy5xZHNTZXJ2aWNlLmdldEZpbGVUcmFuc2ZlclByb2dyZXNzKGZpbGUuc2l6ZSwgZmlsZS50cmFuc2ZlcnJlZClcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBqb2JGaWxlLnB1c2godHJhbnNmZXJGaWxlKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodHJhbnNmZXJGaWxlLnN0YXR1cyA9PT0gJ0lOUFJPR1JFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlucHJvZ3Jlc3NKb2JJZExpc3QucHVzaChqb2IucWRzSm9iSWQpO1xyXG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnSW4gcHJvZ3Jlc3Mgam9iczogJyArIGlucHJvZ3Jlc3NKb2JJZExpc3QpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKChmaWxlLnN0YXR1cyA9PT0gJ3BhdXNlZCcpICYmIGZpbGUudHJhbnNmZXJEb25lID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICBoYXNQYXVzZUpvYnMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKChmaWxlLnN0YXR1cyA9PT0gJ3N0YXJ0aW5nJykgJiYgZmlsZS50cmFuc2ZlckRvbmUgPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgIGhhc1N0b3BKb2JzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICgham9iLmNvbXBsZXRlZCAmJiBqb2IuZmlsZXNGYWlsZWQgPT09IDApIHtcclxuICAgICAgICAgICAgICBpZiAoam9iLmlzSW1wb3J0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUpvYklkTGlzdFsnSU1QT1JUJ11bam9iLnFkc0pvYklkXSA9IHtcclxuICAgICAgICAgICAgICAgICAgb3RtbUpvYklkOiBqb2Iub3RtbUpvYklkLFxyXG4gICAgICAgICAgICAgICAgICBxZHNKb2JJZDogam9iLnFkc0pvYklkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUpvYklkTGlzdFsnRVhQT1JUJ11bam9iLnFkc0pvYklkXSA9IHtcclxuICAgICAgICAgICAgICAgICAgb3RtbUpvYklkOiBqb2Iub3RtbUpvYklkLFxyXG4gICAgICAgICAgICAgICAgICBxZHNKb2JJZDogam9iLnFkc0pvYklkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGpvYi5maWxlc0ZhaWxlZCA+IDApIHtcclxuICAgICAgICAgICAgICB0aGlzLmZhaWxlZEpvYklkTGlzdFtqb2IucWRzSm9iSWRdID0ge1xyXG4gICAgICAgICAgICAgICAgaXNJbXBvcnQ6IGpvYi5pc0ltcG9ydCxcclxuICAgICAgICAgICAgICAgIG90bW1Kb2JJZDogam9iLm90bW1Kb2JJZCxcclxuICAgICAgICAgICAgICAgIHFkc0pvYklkOiBqb2IucWRzSm9iSWRcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2Uoam9iRmlsZSk7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlLnNvcnQgPSB0aGlzLnNvcnQ7XHJcbiAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmIChoYXNQYXVzZUpvYnMpIHtcclxuICAgICAgICAgIGhhc1BhdXNlSm9icyA9IGZhbHNlO1xyXG4gICAgICAgICAgdGhpcy5yZXN1bWVKb2JzKCdSRVNVTUUnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGhhc1N0b3BKb2JzICYmIHRoaXMucmVzdGFydGluZ0pvYkNvdW50IDwgMikge1xyXG4gICAgICAgICAgaGFzUGF1c2VKb2JzID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLnJlc3VtZUpvYnMoJ1JFU1VNRScpO1xyXG4gICAgICAgICAgdGhpcy5yZXN0YXJ0aW5nSm9iQ291bnQrKztcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlucHJvZ3Jlc3NKb2JJZExpc3QubGVuZ3RoID4gMCAmJiB0aGlzLmxpdmVEYXRhU3Vic2NyaXB0aW9uID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgIHRoaXMubGl2ZURhdGFTdWJzY3JpcHRpb24gPSB0aW1lcigwLCAzMDAwKVxyXG4gICAgICAgICAgICAucGlwZShzd2l0Y2hNYXAoKCkgPT4gdGhpcy5jaGVja0RhdGEoKSkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmNhbGN1bGF0ZUlucHJvZ3Jlc3MoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoaW5wcm9ncmVzc0pvYklkTGlzdC5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgIGlmICh0aGlzLmxpdmVEYXRhU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZURhdGFTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sIGdldEpvYnNFcnJvciA9PiB7XHJcbiAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubm9EYXRhTXNnID0gJ1VuYWJsZSB0byBnZXQgZGF0YSBmcm9tIFFEUy4nO1xyXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldFFEU1RyYW5zZmVySm9icygpIHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGNvbnN0IGpvYkZpbGUgPSBbXTtcclxuICAgICAgY29uc3QgaW5wcm9ncmVzc0pvYklkTGlzdCA9IFtdO1xyXG4gICAgICAvL3RoaXMuZ2V0RXhwb3J0Sm9iKCk7Ly9NU1YtMjM5XHJcbiAgICAgIHRoaXMucWRzU2VydmljZS5nZXRKb2JzKHRydWUpXHJcbiAgICAgICAgLnN1YnNjcmliZShnZXRKb2JzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKGdldEpvYnNSZXNwb25zZSAmJiBnZXRKb2JzUmVzcG9uc2Uuam9icyAmJiBnZXRKb2JzUmVzcG9uc2Uuam9icy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLm5vRGF0YU1zZyA9ICdObyBpdGVtcy4nO1xyXG4gICAgICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMubm9EYXRhID0gZmFsc2U7XHJcbiAgICAgICAgICBnZXRKb2JzUmVzcG9uc2Uuam9icy5mb3JFYWNoKChqb2I6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoam9iLmZpbGVzVG90YWwgPiAwKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMuc2hvd0ZhaWxlZCkge1xyXG4gICAgICAgICAgICAgICAgam9iLmZpbGVzLmZvckVhY2goKGZpbGU6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICBpZiAoam9iLmZpbGVzRmFpbGVkID4gMCAmJiBmaWxlLnN0YXR1cyA9PT0gJ2ZhaWxlZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2ZlckZpbGU6IFFEU0pvYkZpbGUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBwcmV2aWV3OiBmaWxlLnRodW1ibmFpbCxcclxuICAgICAgICAgICAgICAgICAgICAgIGZpbGVuYW1lOiBmaWxlLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHRoaXMuZ2V0RmlsZVRyYW5mZXJTdGF0dXMoam9iLCBmaWxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0ZWQ6IG5ldyBEYXRlKGZpbGUudHJhbnNmZXJTdGFydCksXHJcbiAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBqb2IuaXNJbXBvcnQgPyAnVXBsb2FkJyA6ICdEb3dubG9hZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICBmaWxlVHlwZTogZmlsZS50eXBlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgaWNvbjogdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGUubmFtZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICBwcm9ncmVzczogdGhpcy5xZHNTZXJ2aWNlLmdldEZpbGVUcmFuc2ZlclByb2dyZXNzKGZpbGUuc2l6ZSwgZmlsZS50cmFuc2ZlcnJlZClcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIGpvYkZpbGUucHVzaCh0cmFuc2ZlckZpbGUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNmZXJGaWxlLnN0YXR1cyA9PT0gJ0lOUFJPR1JFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBpbnByb2dyZXNzSm9iSWRMaXN0LnB1c2goam9iLnFkc0pvYklkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBqb2IuZmlsZXMuZm9yRWFjaCgoZmlsZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHRyYW5zZmVyRmlsZTogUURTSm9iRmlsZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBwcmV2aWV3OiBmaWxlLnRodW1ibmFpbCxcclxuICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogZmlsZS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy5nZXRGaWxlVHJhbmZlclN0YXR1cyhqb2IsIGZpbGUpLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0ZWQ6IG5ldyBEYXRlKGZpbGUudHJhbnNmZXJTdGFydCksXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogam9iLmlzSW1wb3J0ID8gJ1VwbG9hZCcgOiAnRG93bmxvYWQnLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbGVUeXBlOiBmaWxlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGUubmFtZSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvZ3Jlc3M6IHRoaXMucWRzU2VydmljZS5nZXRGaWxlVHJhbnNmZXJQcm9ncmVzcyhmaWxlLnNpemUsIGZpbGUudHJhbnNmZXJyZWQpXHJcbiAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgIGpvYkZpbGUucHVzaCh0cmFuc2ZlckZpbGUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgaWYgKHRyYW5zZmVyRmlsZS5zdGF0dXMgPT09ICdJTlBST0dSRVNTJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlucHJvZ3Jlc3NKb2JJZExpc3QucHVzaChqb2IucWRzSm9iSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdJbiBwcm9ncmVzcyBqb2JzOiAnICsgaW5wcm9ncmVzc0pvYklkTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgLyogICBpZiAoIWZpbGUudGh1bWJuYWlsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEJhc2U2NChmaWxlLnBhdGgpLnN1YnNjcmliZShiYXNlNjRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG9iaiA9IHRoaXMuY3JlYXRlRmlsZU9iaihmaWxlLCBiYXNlNjRSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKG9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRyYW5zZmVyRmlsZTogUURTSm9iRmlsZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBwcmV2aWV3OiBmaWxlLnRodW1ibmFpbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogZmlsZS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy5nZXRGaWxlVHJhbmZlclN0YXR1cyhqb2IsIGZpbGUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0ZWQ6IG5ldyBEYXRlKGZpbGUudHJhbnNmZXJTdGFydCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogam9iLmlzSW1wb3J0ID8gJ1VwbG9hZCcgOiAnRG93bmxvYWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVUeXBlOiBmaWxlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGUubmFtZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZ3Jlc3M6IHRoaXMucWRzU2VydmljZS5nZXRGaWxlVHJhbnNmZXJQcm9ncmVzcyhmaWxlLnNpemUsIGZpbGUudHJhbnNmZXJyZWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGpvYkZpbGUucHVzaCh0cmFuc2ZlckZpbGUpO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0cmFuc2ZlckZpbGUuc3RhdHVzID09PSAnSU5QUk9HUkVTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBpbnByb2dyZXNzSm9iSWRMaXN0LnB1c2goam9iLnFkc0pvYklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnSW4gcHJvZ3Jlc3Mgam9iczogJyArIGlucHJvZ3Jlc3NKb2JJZExpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgY29uc3QgdHJhbnNmZXJGaWxlOiBRRFNKb2JGaWxlID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmV2aWV3OiBmaWxlLnRodW1ibmFpbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZW5hbWU6IGZpbGUubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiB0aGlzLmdldEZpbGVUcmFuZmVyU3RhdHVzKGpvYiwgZmlsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0ZWQ6IG5ldyBEYXRlKGZpbGUudHJhbnNmZXJTdGFydCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IGpvYi5pc0ltcG9ydCA/ICdVcGxvYWQnIDogJ0Rvd25sb2FkJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZVR5cGU6IGZpbGUudHlwZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGUubmFtZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2dyZXNzOiB0aGlzLnFkc1NlcnZpY2UuZ2V0RmlsZVRyYW5zZmVyUHJvZ3Jlc3MoZmlsZS5zaXplLCBmaWxlLnRyYW5zZmVycmVkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgIGpvYkZpbGUucHVzaCh0cmFuc2ZlckZpbGUpO1xyXG4gICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNmZXJGaWxlLnN0YXR1cyA9PT0gJ0lOUFJPR1JFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlucHJvZ3Jlc3NKb2JJZExpc3QucHVzaChqb2IucWRzSm9iSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnSW4gcHJvZ3Jlc3Mgam9iczogJyArIGlucHJvZ3Jlc3NKb2JJZExpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gKi9cclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIC8qIGNvbnN0IHRyYW5zZmVyRmlsZTogUURTSm9iRmlsZSA9IHtcclxuICAgICAgICAgICAgICAgICAgcHJldmlldzogZmlsZS50aHVtYm5haWwsXHJcbiAgICAgICAgICAgICAgICAgIGZpbGVuYW1lOiBmaWxlLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgIHN0YXR1czogdGhpcy5nZXRGaWxlVHJhbmZlclN0YXR1cyhqb2IsIGZpbGUpLFxyXG4gICAgICAgICAgICAgICAgICBzdGFydGVkOiBuZXcgRGF0ZShmaWxlLnRyYW5zZmVyU3RhcnQpLFxyXG4gICAgICAgICAgICAgICAgICB0eXBlOiBqb2IuaXNJbXBvcnQgPyAnVXBsb2FkJyA6ICdEb3dubG9hZCcsXHJcbiAgICAgICAgICAgICAgICAgIGZpbGVUeXBlOiBmaWxlLnR5cGUsXHJcbiAgICAgICAgICAgICAgICAgIGljb246IHRoaXMuYXNzZXRGaWxlQ29uZmlnU2VydmljZS5maW5kSWNvbkJ5TmFtZShmaWxlLm5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICBwcm9ncmVzczogdGhpcy5xZHNTZXJ2aWNlLmdldEZpbGVUcmFuc2ZlclByb2dyZXNzKGZpbGUuc2l6ZSwgZmlsZS50cmFuc2ZlcnJlZClcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBqb2JGaWxlLnB1c2godHJhbnNmZXJGaWxlKTtcclxuICBcclxuICAgICAgICAgICAgICAgIGlmICh0cmFuc2ZlckZpbGUuc3RhdHVzID09PSAnSU5QUk9HUkVTUycpIHtcclxuICAgICAgICAgICAgICAgICAgaW5wcm9ncmVzc0pvYklkTGlzdC5wdXNoKGpvYi5xZHNKb2JJZCk7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdJbiBwcm9ncmVzcyBqb2JzOiAnICsgaW5wcm9ncmVzc0pvYklkTGlzdCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBpZiAoIWpvYi5jb21wbGV0ZWQgJiYgam9iLmZpbGVzRmFpbGVkID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoam9iLmlzSW1wb3J0KSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlSm9iSWRMaXN0WydJTVBPUlQnXVtqb2IucWRzSm9iSWRdID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIG90bW1Kb2JJZDogam9iLm90bW1Kb2JJZCxcclxuICAgICAgICAgICAgICAgICAgICBxZHNKb2JJZDogam9iLnFkc0pvYklkXHJcbiAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUpvYklkTGlzdFsnRVhQT1JUJ11bam9iLnFkc0pvYklkXSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBvdG1tSm9iSWQ6IGpvYi5vdG1tSm9iSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgcWRzSm9iSWQ6IGpvYi5xZHNKb2JJZFxyXG4gICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgaWYgKGpvYi5maWxlc0ZhaWxlZCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmFpbGVkSm9iSWRMaXN0W2pvYi5xZHNKb2JJZF0gPSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzSW1wb3J0OiBqb2IuaXNJbXBvcnQsXHJcbiAgICAgICAgICAgICAgICAgIG90bW1Kb2JJZDogam9iLm90bW1Kb2JJZCxcclxuICAgICAgICAgICAgICAgICAgcWRzSm9iSWQ6IGpvYi5xZHNKb2JJZFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2Uoam9iRmlsZSk7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2Uuc29ydCA9IHRoaXMuc29ydDtcclxuICAgICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgIC8qIGlmIChpbnByb2dyZXNzSm9iSWRMaXN0Lmxlbmd0aCA+IDAgJiYgdGhpcy5saXZlRGF0YVN1YnNjcmlwdGlvbiA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZURhdGFTdWJzY3JpcHRpb24gPSB0aW1lcigwLCAyMDAwKVxyXG4gICAgICAgICAgICAgIC5waXBlKHN3aXRjaE1hcCgoKSA9PiB0aGlzLmNoZWNrRGF0YSgpKSlcclxuICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFFEU1RyYW5zZmVySm9icygpO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgICAgaWYgKGlucHJvZ3Jlc3NKb2JJZExpc3QubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmxpdmVEYXRhU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5saXZlRGF0YVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9ICovXHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBnZXRKb2JzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5ub0RhdGFNc2cgPSAnVW5hYmxlIHRvIGdldCBkYXRhIGZyb20gUURTLic7XHJcbiAgICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjaGVja0RhdGEoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIE1TVi0yMzlcclxuICByZXN1bWVKb2JzKHN0YXRlKSB7XHJcbiAgICBjb25zdCB0aGF0ID0gdGhpcztcclxuICAgIGNvbnN0IGFjdGl2ZUltcG9ydEpvYkxpc3QgPSB0aGlzLmFjdGl2ZUpvYklkTGlzdFsnSU1QT1JUJ107XHJcbiAgICBjb25zdCBhY3RpdmVFeHBvcnRKb2JMaXN0ID0gdGhpcy5hY3RpdmVKb2JJZExpc3RbJ0VYUE9SVCddO1xyXG5cclxuICAgIE9iamVjdC5rZXlzKGFjdGl2ZUltcG9ydEpvYkxpc3QpLmZvckVhY2goZnVuY3Rpb24gKGpvYklkKSB7XHJcbiAgICAgIGNvbnN0IGltcG9ydEpvYklkID0gYWN0aXZlSW1wb3J0Sm9iTGlzdFtqb2JJZF07XHJcbiAgICAgIHRoYXQucWRzU2VydmljZS50b2dnbGVRRFNJbXBvcnRKb2IoaW1wb3J0Sm9iSWQsIHN0YXRlKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlID09PSAnQ09NUExFVEUnKSB7XHJcbiAgICAgICAgICAgIGRlbGV0ZSB0aGF0LmFjdGl2ZUpvYklkTGlzdFsnSU1QT1JUJ11bam9iSWRdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zaG93RXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBUcnkgYWdhaW4nKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIE9iamVjdC5rZXlzKGFjdGl2ZUV4cG9ydEpvYkxpc3QpLmZvckVhY2goZnVuY3Rpb24gKGpvYklkKSB7XHJcbiAgICAgIGNvbnN0IGV4cG9ydEpvYklkID0gYWN0aXZlRXhwb3J0Sm9iTGlzdFtqb2JJZF07XHJcbiAgICAgIHRoYXQucWRzU2VydmljZS50b2dnbGVRRFNFeHBvcnRKb2IoZXhwb3J0Sm9iSWQsIHN0YXRlKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlID09PSAnQ09NUExFVEUnKSB7XHJcbiAgICAgICAgICAgIGRlbGV0ZSB0aGF0LmFjdGl2ZUpvYklkTGlzdFsnRVhQT1JUJ11bam9iSWRdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zaG93RXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBUcnkgYWdhaW4nKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlSW1wb3J0Sm9icyhzdGF0ZSkge1xyXG4gICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcbiAgICBjb25zdCBhY3RpdmVJbXBvcnRKb2JMaXN0ID0gdGhpcy5hY3RpdmVKb2JJZExpc3RbJ0lNUE9SVCddO1xyXG4gICAgY29uc3QgYWN0aXZlRXhwb3J0Sm9iTGlzdCA9IHRoaXMuYWN0aXZlSm9iSWRMaXN0WydFWFBPUlQnXTtcclxuXHJcbiAgICBPYmplY3Qua2V5cyhhY3RpdmVJbXBvcnRKb2JMaXN0KS5mb3JFYWNoKGZ1bmN0aW9uIChqb2JJZCkge1xyXG4gICAgICBjb25zdCBpbXBvcnRKb2JJZCA9IGFjdGl2ZUltcG9ydEpvYkxpc3Rbam9iSWRdO1xyXG4gICAgICB0aGF0LnFkc1NlcnZpY2UudG9nZ2xlUURTSW1wb3J0Sm9iKGltcG9ydEpvYklkLCBzdGF0ZSlcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIGlmIChyZXNwb25zZSA9PT0gJ0NPTVBMRVRFJykge1xyXG4gICAgICAgICAgICBkZWxldGUgdGhhdC5hY3RpdmVKb2JJZExpc3RbJ0lNUE9SVCddW2pvYklkXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoYXQuZ2V0RG93bmxvYWRMb2NhdGlvbigpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoYXQuZ2V0RG93bmxvYWRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnNob3dFcnJvcignU29tZXRoaW5nIHdlbnQgd3JvbmcsIFRyeSBhZ2FpbicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgT2JqZWN0LmtleXMoYWN0aXZlRXhwb3J0Sm9iTGlzdCkuZm9yRWFjaChmdW5jdGlvbiAoam9iSWQpIHtcclxuICAgICAgY29uc3QgZXhwb3J0Sm9iSWQgPSBhY3RpdmVFeHBvcnRKb2JMaXN0W2pvYklkXTtcclxuICAgICAgdGhhdC5xZHNTZXJ2aWNlLnRvZ2dsZVFEU0V4cG9ydEpvYihleHBvcnRKb2JJZCwgc3RhdGUpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UgPT09ICdDT01QTEVURScpIHtcclxuICAgICAgICAgICAgZGVsZXRlIHRoYXQuYWN0aXZlSm9iSWRMaXN0WydFWFBPUlQnXVtqb2JJZF07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGF0LmdldERvd25sb2FkTG9jYXRpb24oKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGF0LmdldERvd25sb2FkTG9jYXRpb24oKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zaG93RXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBUcnkgYWdhaW4nKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0cnlGYWlsZWRKb2JzKCkge1xyXG4gICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcbiAgICBjb25zdCBmYWlsZWRJbXBvcnRKb2JMaXN0ID0gdGhpcy5mYWlsZWRKb2JJZExpc3Q7XHJcblxyXG4gICAgT2JqZWN0LmtleXMoZmFpbGVkSW1wb3J0Sm9iTGlzdCkuZm9yRWFjaChmdW5jdGlvbiAoam9iKSB7XHJcbiAgICAgIHRoYXQucWRzU2VydmljZS5yZXRyeUZhaWxlZEpvYihqb2IpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICB0aGF0LmdldERvd25sb2FkTG9jYXRpb24oKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGF0LmdldERvd25sb2FkTG9jYXRpb24oKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zaG93RXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBUcnkgYWdhaW4nKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgdGhpcy5vdG1tU2VydmljZS5jaGVja090bW1TZXNzaW9uKHVuZGVmaW5lZCwgdHJ1ZSkgLy8gTVZTLTIzOVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICghb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgJiZcclxuICAgICAgICAgICFvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5maW5kUURTQ2xpZW50KSB7XHJcbiAgICAgICAgICB0aGlzLnNob3dBY3Rpb25zID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICAgIHRoaXMubm9EYXRhID0gdHJ1ZTtcclxuICAgICAgICAgIHRoaXMubm9EYXRhTXNnID0gJ1FEUyBpcyBub3QgaW5zdGFsbGVkIG9yIGNvbm5lY3RlZC4nO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmdldERvd25sb2FkTG9jYXRpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubm9EYXRhID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm5vRGF0YU1zZyA9ICdVbmFibGUgdG8gY29ubmVjdCB0byBNZWRpYSBNYW5hZ2VyJztcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1VuYWJsZSB0byBpbml0aWFsaXplIE1lZGlhIE1hbmFnZXIgc2Vzc2lvbicpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMubGl2ZURhdGFTdWJzY3JpcHRpb24pIHtcclxuICAgICAgdGhpcy5saXZlRGF0YVN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19