import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { AppDynamicConfig } from '../../services/app.dynamic.config';
let AssetPreviewComponent = class AssetPreviewComponent {
    constructor() {
        this.tempAppDynamicConfig = AppDynamicConfig.getProjectThumbnail();
    }
    handleDefaultAssetURL() {
        if (!this.assetURL) {
            this.assetURL = AppDynamicConfig.getProjectThumbnail();
        }
        if (!this.rectangleWidth) {
            this.rectangleWidth = '100%';
        }
        if (!this.rectangleHeight) {
            this.rectangleHeight = '100%';
        }
    }
    ngOnInit() {
        this.handleDefaultAssetURL();
    }
    ngOnChanges(simpleChanges) {
        console.log(simpleChanges);
        // this.handleDefaultAssetURL();
    }
};
__decorate([
    Input()
], AssetPreviewComponent.prototype, "assetURL", void 0);
__decorate([
    Input()
], AssetPreviewComponent.prototype, "rectangleWidth", void 0);
__decorate([
    Input()
], AssetPreviewComponent.prototype, "rectangleHeight", void 0);
__decorate([
    Input()
], AssetPreviewComponent.prototype, "isCollapsed", void 0);
AssetPreviewComponent = __decorate([
    Component({
        selector: 'mpm-asset-preview',
        template: "<div class=\"flex-row\">\r\n    <div class=\"flex-row-item custom-triangle\">\r\n        <mat-card *ngIf=\"assetURL || tempAppDynamicConfig\" class=\"thumbnail-card\">\r\n            <div class=\"flex-col\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row image-holder\">\r\n                        <div class=\"asset-content-area\">\r\n                            <div class=\"asset-holder\" [ngStyle]=\"{'width': rectangleWidth, 'height': rectangleHeight}\">\r\n                                <img src=\"{{assetURL? assetURL : tempAppDynamicConfig}}\" alt=\"asset\" [ngClass]=\"{'collapsed': isCollapsed}\">\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n        <mat-card *ngIf=\"!(assetURL || tempAppDynamicConfig)\" class=\"thumbnail-card\">\r\n            <div class=\"flex-col\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row image-holder\">\r\n                        <div class=\"asset-content-area\">\r\n                            <div class=\"asset-holder\" [ngStyle]=\"{'width': rectangleWidth, 'height': rectangleHeight}\">\r\n                                \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n    </div>\r\n</div>",
        styles: ["mat-card.thumbnail-card{padding:0;margin:auto;box-shadow:none!important}div.asset-content-area{align-items:center;justify-content:center;outline:0;background-color:#ddd}div.asset-content-area .asset-holder img{display:block;left:0;right:0;top:0;bottom:0;margin:auto;width:100%;height:100%;max-width:100%;max-height:100%}.custom-triangle{position:relative}.custom-left-triangle:after{content:\"\";position:absolute;top:40%;left:0;width:0;height:0;border-top:10px solid transparent;border-right:15px solid transparent;border-bottom:10px solid transparent}.custom-right-triangle:after{content:\"\";position:absolute;top:40%;right:0;width:0;height:0;border-top:10px solid transparent;border-left:15px solid transparent;border-bottom:10px solid transparent}.custom-top-triangle:after{content:\"\";position:absolute;top:0;left:45%;width:0;height:0;border-left:10px solid transparent;border-bottom:15px solid transparent;border-right:10px solid transparent}.custom-bottom-triangle:after{content:\"\";position:absolute;bottom:0;left:45%;width:0;height:0;border-left:10px solid transparent;border-top:15px solid transparent;border-right:10px solid transparent}.width-empty{width:0;height:0}"]
    })
], AssetPreviewComponent);
export { AssetPreviewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtcHJldmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9hc3NldC1wcmV2aWV3L2Fzc2V0LXByZXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFRckUsSUFBYSxxQkFBcUIsR0FBbEMsTUFBYSxxQkFBcUI7SUFTaEM7UUFGQSx5QkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBRTlDLENBQUM7SUFFakIscUJBQXFCO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUN4RDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7U0FDL0I7SUFDSCxDQUFDO0lBQ0QsUUFBUTtRQUNOLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxXQUFXLENBQUMsYUFBNEI7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzQixnQ0FBZ0M7SUFDbEMsQ0FBQztDQUNGLENBQUE7QUE1QlU7SUFBUixLQUFLLEVBQUU7dURBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFOzZEQUF3QjtBQUN2QjtJQUFSLEtBQUssRUFBRTs4REFBeUI7QUFDeEI7SUFBUixLQUFLLEVBQUU7MERBQXNCO0FBTG5CLHFCQUFxQjtJQUxqQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLGsrQ0FBNkM7O0tBRTlDLENBQUM7R0FDVyxxQkFBcUIsQ0E4QmpDO1NBOUJZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcER5bmFtaWNDb25maWcgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hcHAuZHluYW1pYy5jb25maWcnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWFzc2V0LXByZXZpZXcnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9hc3NldC1wcmV2aWV3LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hc3NldC1wcmV2aWV3LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0UHJldmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgYXNzZXRVUkw6IHN0cmluZztcclxuICBASW5wdXQoKSByZWN0YW5nbGVXaWR0aDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIHJlY3RhbmdsZUhlaWdodDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGlzQ29sbGFwc2VkOiBib29sZWFuO1xyXG5cclxuICB0ZW1wQXBwRHluYW1pY0NvbmZpZyA9IEFwcER5bmFtaWNDb25maWcuZ2V0UHJvamVjdFRodW1ibmFpbCgpO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBoYW5kbGVEZWZhdWx0QXNzZXRVUkwoKSB7XHJcbiAgICBpZiAoIXRoaXMuYXNzZXRVUkwpIHtcclxuICAgICAgdGhpcy5hc3NldFVSTCA9IEFwcER5bmFtaWNDb25maWcuZ2V0UHJvamVjdFRodW1ibmFpbCgpO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLnJlY3RhbmdsZVdpZHRoKSB7XHJcbiAgICAgIHRoaXMucmVjdGFuZ2xlV2lkdGggPSAnMTAwJSc7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMucmVjdGFuZ2xlSGVpZ2h0KSB7XHJcbiAgICAgIHRoaXMucmVjdGFuZ2xlSGVpZ2h0ID0gJzEwMCUnO1xyXG4gICAgfVxyXG4gIH1cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuaGFuZGxlRGVmYXVsdEFzc2V0VVJMKCk7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhzaW1wbGVDaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhzaW1wbGVDaGFuZ2VzKTtcclxuICAgIC8vIHRoaXMuaGFuZGxlRGVmYXVsdEFzc2V0VVJMKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==