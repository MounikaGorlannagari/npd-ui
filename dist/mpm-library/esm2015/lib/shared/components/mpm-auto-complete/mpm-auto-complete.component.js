import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
let MpmAutoCompleteComponent = class MpmAutoCompleteComponent {
    constructor() {
        this.valueChangeEvent = new EventEmitter();
        this.selectedValue = {
            id: '',
            value: ''
        };
    }
    filterValue(value) {
        let filterValue = value;
        try {
            filterValue = value.toLowerCase();
        }
        catch (exception) {
            filterValue = value;
        }
        return this.searchFieldConfig.filterOptions.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
    }
    displayFn(value) {
        return value ? value.displayName : undefined;
    }
    onFocusOut(event) {
        //
    }
    changeAutoComplete(event) {
        // this.userFormControl.setErrors(null);
        this.selectedValue.id = event.option.value.value;
        this.selectedValue.value = event.option.value.displayName;
        this.valueChangeEvent.next(event.option.value);
    }
    mapInitialSelectedValue() {
        this.filterOptions = this.formControl.valueChanges
            .pipe(startWith(this.selectedValue.value), map(value => {
            this.searchFieldConfig.filterOptions.map(option => {
                if (option.value === value) {
                    this.selectedValue.value = option.displayName;
                    this.selectedValue.id = option.value;
                }
            });
            return this.filterValue(value);
        }));
    }
    initializeSearchFields() {
        if (this.searchFieldConfig) {
            if (this.searchFieldConfig.formControl) {
                this.formControl = this.searchFieldConfig.formControl;
            }
            else {
                this.formControl = new FormControl();
            }
            if (this.formControl.errors && this.formControl.errors.required) {
                this.requiredField = true;
            }
            else {
                this.requiredField = false;
            }
            if (this.searchFieldConfig.filterOptions && this.searchFieldConfig.filterOptions.length &&
                this.searchFieldConfig.filterOptions.length > 0) {
                this.mapInitialSelectedValue();
            }
        }
        if (this.searchFieldConfig && this.searchFieldConfig.filterOptions) {
            this.searchFieldConfig.filterOptions.map(option => {
                if (option.value === this.formControl.value) {
                    this.selectedValue.value = option.displayName;
                    this.selectedValue.id = option.value;
                }
            });
        }
        if (this.formControl.status === 'DISABLED') {
            this.formControl.disable();
        }
    }
    ngAfterViewInit() {
        setTimeout(() => {
            if (this.autoComplete && this.autoFocus) {
                const inputElement = this.inputField.nativeElement;
                inputElement.focus();
            }
        }, 500);
    }
    ngOnInit() {
        this.initializeSearchFields();
    }
};
__decorate([
    ViewChild('userAuto', { static: false })
], MpmAutoCompleteComponent.prototype, "autoComplete", void 0);
__decorate([
    ViewChild('inputField', { static: false })
], MpmAutoCompleteComponent.prototype, "inputField", void 0);
__decorate([
    Input()
], MpmAutoCompleteComponent.prototype, "searchFieldConfig", void 0);
__decorate([
    Input()
], MpmAutoCompleteComponent.prototype, "required", void 0);
__decorate([
    Input()
], MpmAutoCompleteComponent.prototype, "autoFocus", void 0);
__decorate([
    Output()
], MpmAutoCompleteComponent.prototype, "valueChangeEvent", void 0);
MpmAutoCompleteComponent = __decorate([
    Component({
        selector: 'mpm-mpm-auto-complete',
        template: "<!-- <mat-form-field class=\"flex-row-item mat-form-align\" appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"formControl\" (focusout)=\"onFocusOut($event)\" [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <span matTooltip=\"{{option.displayName}}\" matTooltipClass=\"custom-tooltip\">{{option.displayName}}</span>\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n</mat-form-field> -->\r\n<mat-form-field class=\"flex-row-item mat-form-align\" appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input #inputField matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"formControl\" (focusout)=\"onFocusOut($event)\" [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <!-- <div #optionposition\r\n                [matTooltip]=\"spanposition.getBoundingClientRect().width > optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                {{option.displayName}}   \r\n                <span #spanposition  \r\n                    [matTooltip]=\"spanposition.getBoundingClientRect().width <= optionposition.getBoundingClientRect().width ? option.displayName : null\" >\r\n                    {{option.displayName}}\r\n                </span>\r\n            </div> -->\r\n            <span matTooltip=\"{{option.displayName}}\">{{option.displayName}}</span>\r\n            <!-- matTooltipClass=\"custom-tooltip\" -->\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n</mat-form-field>",
        encapsulation: ViewEncapsulation.None,
        styles: [".mat-form-align{margin:0 5px}"]
    })
], MpmAutoCompleteComponent);
export { MpmAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbXBtLWF1dG8tY29tcGxldGUvbXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFjLFNBQVMsRUFBaUIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEksT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTdDLE9BQU8sRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFTaEQsSUFBYSx3QkFBd0IsR0FBckMsTUFBYSx3QkFBd0I7SUFpQm5DO1FBVFUscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUtyRCxrQkFBYSxHQUFHO1lBQ2QsRUFBRSxFQUFFLEVBQUU7WUFDTixLQUFLLEVBQUUsRUFBRTtTQUNWLENBQUM7SUFDYyxDQUFDO0lBRVYsV0FBVyxDQUFDLEtBQWE7UUFDOUIsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUk7WUFDRixXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ25DO1FBQUMsT0FBTyxTQUFTLEVBQUU7WUFDbEIsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUNyQjtRQUNELE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNySCxDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQVc7UUFDbkIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztJQUMvQyxDQUFDO0lBQ0QsVUFBVSxDQUFDLEtBQUs7UUFDZCxFQUFFO0lBQ0osQ0FBQztJQUNELGtCQUFrQixDQUFDLEtBQUs7UUFDdEIsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7UUFDMUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCx1QkFBdUI7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVk7YUFDL0MsSUFBSSxDQUNILFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUNuQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDVixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDaEQsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztpQkFDdEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FDSCxDQUFDO0lBQ04sQ0FBQztJQUVELHNCQUFzQjtRQUNwQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQzthQUN2RDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7YUFDdEM7WUFDRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDL0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7YUFDNUI7WUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxNQUFNO2dCQUNyRixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO2FBQ2hDO1NBQ0Y7UUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFO1lBQ2xFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoRCxJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7b0JBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQ3RDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsZUFBZTtRQUNiLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDdkMsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7Z0JBQ25ELFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN0QjtRQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztDQUVGLENBQUE7QUFwRzJDO0lBQXpDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7OERBQTBCO0FBQ3ZCO0lBQTNDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7NERBQXdCO0FBRTFEO0lBQVIsS0FBSyxFQUFFO21FQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTswREFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFOzJEQUFXO0FBQ1Q7SUFBVCxNQUFNLEVBQUU7a0VBQTRDO0FBUjFDLHdCQUF3QjtJQVBwQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsdUJBQXVCO1FBQ2pDLGlxRUFBaUQ7UUFFakQsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O0tBRXRDLENBQUM7R0FDVyx3QkFBd0IsQ0FzR3BDO1NBdEdZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIEVsZW1lbnRSZWYsIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgc3RhcnRXaXRoLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1tcG0tYXV0by1jb21wbGV0ZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL21wbS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9tcG0tYXV0by1jb21wbGV0ZS5jb21wb25lbnQuY3NzJ10sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG5cclxufSlcclxuZXhwb3J0IGNsYXNzIE1wbUF1dG9Db21wbGV0ZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3VzZXJBdXRvJywgeyBzdGF0aWM6IGZhbHNlIH0pIGF1dG9Db21wbGV0ZTogRWxlbWVudFJlZjtcclxuICBAVmlld0NoaWxkKCdpbnB1dEZpZWxkJywgeyBzdGF0aWM6IGZhbHNlIH0pIGlucHV0RmllbGQ6IEVsZW1lbnRSZWY7XHJcblxyXG4gIEBJbnB1dCgpIHNlYXJjaEZpZWxkQ29uZmlnO1xyXG4gIEBJbnB1dCgpIHJlcXVpcmVkO1xyXG4gIEBJbnB1dCgpIGF1dG9Gb2N1cztcclxuICBAT3V0cHV0KCkgdmFsdWVDaGFuZ2VFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBmb3JtQ29udHJvbDogRm9ybUNvbnRyb2w7XHJcbiAgZmlsdGVyT3B0aW9uczogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgcmVxdWlyZWRGaWVsZDogYm9vbGVhbjtcclxuICBzZWxlY3RlZFZhbHVlID0ge1xyXG4gICAgaWQ6ICcnLFxyXG4gICAgdmFsdWU6ICcnXHJcbiAgfTtcclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBwdWJsaWMgZmlsdGVyVmFsdWUodmFsdWU6IHN0cmluZyk6IHN0cmluZ1tdIHtcclxuICAgIGxldCBmaWx0ZXJWYWx1ZSA9IHZhbHVlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgZmlsdGVyVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7XHJcbiAgICAgIGZpbHRlclZhbHVlID0gdmFsdWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmZpbHRlcihvcHRpb24gPT4gb3B0aW9uLm5hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKGZpbHRlclZhbHVlKSA9PT0gMCk7XHJcbiAgfVxyXG5cclxuICBkaXNwbGF5Rm4odmFsdWU/OiBhbnkpOiBzdHJpbmcgfCB1bmRlZmluZWQge1xyXG4gICAgcmV0dXJuIHZhbHVlID8gdmFsdWUuZGlzcGxheU5hbWUgOiB1bmRlZmluZWQ7XHJcbiAgfVxyXG4gIG9uRm9jdXNPdXQoZXZlbnQpIHtcclxuICAgIC8vXHJcbiAgfVxyXG4gIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudCkge1xyXG4gICAgLy8gdGhpcy51c2VyRm9ybUNvbnRyb2wuc2V0RXJyb3JzKG51bGwpO1xyXG4gICAgdGhpcy5zZWxlY3RlZFZhbHVlLmlkID0gZXZlbnQub3B0aW9uLnZhbHVlLnZhbHVlO1xyXG4gICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gZXZlbnQub3B0aW9uLnZhbHVlLmRpc3BsYXlOYW1lO1xyXG4gICAgdGhpcy52YWx1ZUNoYW5nZUV2ZW50Lm5leHQoZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICB9XHJcblxyXG4gIG1hcEluaXRpYWxTZWxlY3RlZFZhbHVlKCkge1xyXG4gICAgdGhpcy5maWx0ZXJPcHRpb25zID0gdGhpcy5mb3JtQ29udHJvbC52YWx1ZUNoYW5nZXNcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3RhcnRXaXRoKHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSksXHJcbiAgICAgICAgbWFwKHZhbHVlID0+IHtcclxuICAgICAgICAgIHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucy5tYXAob3B0aW9uID0+IHtcclxuICAgICAgICAgICAgaWYgKG9wdGlvbi52YWx1ZSA9PT0gdmFsdWUpIHtcclxuICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmFsdWUudmFsdWUgPSBvcHRpb24uZGlzcGxheU5hbWU7XHJcbiAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLmlkID0gb3B0aW9uLnZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHJldHVybiB0aGlzLmZpbHRlclZhbHVlKHZhbHVlKTtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgaW5pdGlhbGl6ZVNlYXJjaEZpZWxkcygpIHtcclxuICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnKSB7XHJcbiAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZvcm1Db250cm9sKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtQ29udHJvbCA9IHRoaXMuc2VhcmNoRmllbGRDb25maWcuZm9ybUNvbnRyb2w7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5mb3JtQ29udHJvbCA9IG5ldyBGb3JtQ29udHJvbCgpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmZvcm1Db250cm9sLmVycm9ycyAmJiB0aGlzLmZvcm1Db250cm9sLmVycm9ycy5yZXF1aXJlZCkge1xyXG4gICAgICAgIHRoaXMucmVxdWlyZWRGaWVsZCA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5yZXF1aXJlZEZpZWxkID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucyAmJiB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubGVuZ3RoICYmXHJcbiAgICAgICAgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICB0aGlzLm1hcEluaXRpYWxTZWxlY3RlZFZhbHVlKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnICYmIHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucykge1xyXG4gICAgICB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgaWYgKG9wdGlvbi52YWx1ZSA9PT0gdGhpcy5mb3JtQ29udHJvbC52YWx1ZSkge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gb3B0aW9uLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLmlkID0gb3B0aW9uLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZm9ybUNvbnRyb2wuc3RhdHVzID09PSAnRElTQUJMRUQnKSB7XHJcbiAgICAgIHRoaXMuZm9ybUNvbnRyb2wuZGlzYWJsZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLmF1dG9Db21wbGV0ZSAmJiB0aGlzLmF1dG9Gb2N1cykge1xyXG4gICAgICAgIGNvbnN0IGlucHV0RWxlbWVudCA9IHRoaXMuaW5wdXRGaWVsZC5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgIGlucHV0RWxlbWVudC5mb2N1cygpO1xyXG4gICAgICB9XHJcbiAgICB9LCA1MDApO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmluaXRpYWxpemVTZWFyY2hGaWVsZHMoKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==