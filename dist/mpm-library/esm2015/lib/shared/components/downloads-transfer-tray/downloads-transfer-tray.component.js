import { __decorate } from "tslib";
import { Component, isDevMode } from '@angular/core';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { MatTableDataSource } from '@angular/material/table';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
let DownloadsTransferTrayComponent = class DownloadsTransferTrayComponent {
    constructor(loaderService, otmmService, notificationService, qdsService, sharingService, utilService) {
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.qdsService = qdsService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.displayedColumns = ['filename', 'no_of_files', 'time', 'size', 'action'];
        this.isLoadingData = true;
        this.noData = false;
        this.showActions = true;
        this.dataSource = new MatTableDataSource([]);
    }
    convertToMB(byte) {
        return Math.round((byte / 1024) / 1024 * 100) / 100;
    }
    getExportJobs() {
        this.isLoadingData = true;
        const params = [{
                key: 'after',
                value: '0'
            }, {
                key: 'limit',
                value: '100'
            }, {
                key: 'job_class',
                value: 'EXPORT'
            }, {
                key: 'load_downloadable_jobs_only',
                value: 'true'
            }, {
                key: 'load_asset_details',
                value: 'true'
            }, {
                key: 'asset_ids_limit',
                value: '3'
            }, {
                key: 'sort',
                value: 'desc_start_date_time'
            }, {
                key: 'load_job_details',
                value: 'true'
            }, {
                key: 'status',
                value: 'COMPLETED'
            }];
        this.otmmService.getExportJobs(params)
            .subscribe(response => {
            if (response && response.jobs_resource && response.jobs_resource.collection_summary &&
                response.jobs_resource.collection_summary.actual_count_of_items &&
                response.jobs_resource.collection_summary.actual_count_of_items > 0) {
                this.dataSource = new MatTableDataSource(response.jobs_resource.jobs);
                this.isLoadingData = false;
            }
            else {
                this.isLoadingData = false;
                this.noData = true;
                this.noDataMsg = 'No items.';
            }
        }, error => {
            this.noData = true;
            this.noDataMsg = 'Unable to get data from QDS.';
            this.isLoadingData = false;
        });
    }
    download(job) {
        if (otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload &&
            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
            const fileList = [];
            fileList.push({ name: job.job_details.export_zip_name, size: job.job_details.export_size });
            this.qdsService.download(job.job_id, fileList)
                .subscribe(response => {
                this.notificationService.info(response);
            }, error => {
                this.notificationService.error('Something went wrong');
            });
        }
        else {
            this.otmmService.checkOtmmSession().subscribe();
            if (job && job.job_details && job.job_details.relative_download_url) {
                const otmmBaseUrl = isDevMode() ? '' : this.sharingService.getMediaManagerConfig().url;
                const assetURL = otmmBaseUrl + job.job_details.relative_download_url.slice(1);
                this.utilService.initiateDownload(assetURL, job.job_details.export_zip_name);
            }
            else {
                this.notificationService.error('Something went wrong while downloading file');
                return;
            }
        }
    }
    ngAfterViewInit() {
        this.otmmService.checkOtmmSession()
            .subscribe(response => {
            this.loaderService.hide();
            this.isLoadingData = false;
            this.getExportJobs();
        }, error => {
            this.loaderService.hide();
            this.isLoadingData = false;
            this.noData = true;
            this.noDataMsg = 'Unable to connect to Media Manager.';
            this.notificationService.error('Unable to initialize OTMM session');
        });
    }
};
DownloadsTransferTrayComponent.ctorParameters = () => [
    { type: LoaderService },
    { type: OTMMService },
    { type: NotificationService },
    { type: QdsService },
    { type: SharingService },
    { type: UtilService }
];
DownloadsTransferTrayComponent = __decorate([
    Component({
        selector: 'mpm-downloads-transfer-tray',
        template: "<mat-spinner diameter=\"40\" *ngIf=\"isLoadingData\"></mat-spinner>\r\n\r\n<div class=\"qds-files-container\" [hidden]=\"isLoadingData\">\r\n    <table mat-table [dataSource]=\"dataSource\" matSort>\r\n        <ng-container matColumnDef=\"filename\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.job_details.export_zip_name}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"no_of_files\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> No. of Files </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.asset_count}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"time\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Exported Time </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.end_time | formatToLocaleDateTime}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"size\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Size </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{convertToMB(element.job_details.export_size)}} MB</td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <button mat-icon-button aria-label=\"Download\" matTooltip=\"Download\" (click)=\"download(element)\">\r\n                    <mat-icon class=\"success\">vertical_align_bottom</mat-icon>\r\n                </button>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n</div>\r\n\r\n<div *ngIf=\"noData && !isLoadingData\" class=\"no-data\">\r\n    <span>{{noDataMsg}}</span>\r\n</div>",
        styles: [".float-right{float:right}.justify-flex-end{justify-content:flex-end}.flex-spacer{display:flex;flex-grow:5}.qds-files-container{overflow:hidden}.qds-files-container table{width:100%}mat-spinner.mat-progress-spinner{position:absolute;z-index:999999;top:48vh;left:48vw}.success{color:green}.info{color:#00f}.danger{color:red}.warning{color:orange}.no-data{padding:24px;text-align:center}"]
    })
], DownloadsTransferTrayComponent);
export { DownloadsTransferTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUF5QixTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQU83RSxJQUFhLDhCQUE4QixHQUEzQyxNQUFhLDhCQUE4QjtJQVN6QyxZQUNTLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxVQUFzQixFQUN0QixjQUE4QixFQUM5QixXQUF3QjtRQUx4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBYmpDLHFCQUFnQixHQUFhLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRW5GLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBRXJCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixnQkFBVyxHQUFHLElBQUksQ0FBQztRQVdqQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ3RELENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFFMUIsTUFBTSxNQUFNLEdBQUcsQ0FBQztnQkFDZCxHQUFHLEVBQUUsT0FBTztnQkFDWixLQUFLLEVBQUUsR0FBRzthQUNYLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLE9BQU87Z0JBQ1osS0FBSyxFQUFFLEtBQUs7YUFDYixFQUFFO2dCQUNELEdBQUcsRUFBRSxXQUFXO2dCQUNoQixLQUFLLEVBQUUsUUFBUTthQUNoQixFQUFFO2dCQUNELEdBQUcsRUFBRSw2QkFBNkI7Z0JBQ2xDLEtBQUssRUFBRSxNQUFNO2FBQ2QsRUFBRTtnQkFDRCxHQUFHLEVBQUUsb0JBQW9CO2dCQUN6QixLQUFLLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLGlCQUFpQjtnQkFDdEIsS0FBSyxFQUFFLEdBQUc7YUFDWCxFQUFFO2dCQUNELEdBQUcsRUFBRSxNQUFNO2dCQUNYLEtBQUssRUFBRSxzQkFBc0I7YUFDOUIsRUFBRTtnQkFDRCxHQUFHLEVBQUUsa0JBQWtCO2dCQUN2QixLQUFLLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsS0FBSyxFQUFFLFdBQVc7YUFDbkIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO2FBQ25DLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCO2dCQUNqRixRQUFRLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQjtnQkFDL0QsUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsR0FBRyxDQUFDLEVBQUU7Z0JBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzthQUM1QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyw4QkFBOEIsQ0FBQztZQUNoRCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxRQUFRLENBQUMsR0FBRztRQUNWLElBQUkscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVztZQUMxRCxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEVBQUU7WUFDNUQsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUM1RixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQztpQkFDM0MsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDVCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDekQsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2hELElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLElBQUksR0FBRyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDbkUsTUFBTSxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztnQkFDdkYsTUFBTSxRQUFRLEdBQUcsV0FBVyxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQzlFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsNkNBQTZDLENBQUMsQ0FBQztnQkFDOUUsT0FBTzthQUNSO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUU7YUFDaEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQ0FBcUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7UUFDdEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUYsQ0FBQTs7WUF6R3lCLGFBQWE7WUFDZixXQUFXO1lBQ0gsbUJBQW1CO1lBQzVCLFVBQVU7WUFDTixjQUFjO1lBQ2pCLFdBQVc7O0FBZnRCLDhCQUE4QjtJQUwxQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsNkJBQTZCO1FBQ3ZDLHdoRUFBdUQ7O0tBRXhELENBQUM7R0FDVyw4QkFBOEIsQ0FtSDFDO1NBbkhZLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdJbml0LCBpc0Rldk1vZGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYmxlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBRZHNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vdXBsb2FkL3NlcnZpY2VzL3Fkcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tZG93bmxvYWRzLXRyYW5zZmVyLXRyYXknLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9kb3dubG9hZHMtdHJhbnNmZXItdHJheS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRG93bmxvYWRzVHJhbnNmZXJUcmF5Q29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XHJcblxyXG4gIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gWydmaWxlbmFtZScsICdub19vZl9maWxlcycsICd0aW1lJywgJ3NpemUnLCAnYWN0aW9uJ107XHJcbiAgZGF0YVNvdXJjZTogTWF0VGFibGVEYXRhU291cmNlPGFueT47XHJcbiAgaXNMb2FkaW5nRGF0YSA9IHRydWU7XHJcbiAgbm9EYXRhTXNnOiBzdHJpbmc7XHJcbiAgbm9EYXRhID0gZmFsc2U7XHJcbiAgc2hvd0FjdGlvbnMgPSB0cnVlO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuXHJcbiAgKSB7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKFtdKTtcclxuICB9XHJcblxyXG4gIGNvbnZlcnRUb01CKGJ5dGU6IG51bWJlcikge1xyXG4gICAgcmV0dXJuIE1hdGgucm91bmQoKGJ5dGUgLyAxMDI0KSAvIDEwMjQgKiAxMDApIC8gMTAwO1xyXG4gIH1cclxuXHJcbiAgZ2V0RXhwb3J0Sm9icygpIHtcclxuICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IHRydWU7XHJcblxyXG4gICAgY29uc3QgcGFyYW1zID0gW3tcclxuICAgICAga2V5OiAnYWZ0ZXInLFxyXG4gICAgICB2YWx1ZTogJzAnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ2xpbWl0JyxcclxuICAgICAgdmFsdWU6ICcxMDAnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ2pvYl9jbGFzcycsXHJcbiAgICAgIHZhbHVlOiAnRVhQT1JUJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdsb2FkX2Rvd25sb2FkYWJsZV9qb2JzX29ubHknLFxyXG4gICAgICB2YWx1ZTogJ3RydWUnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ2xvYWRfYXNzZXRfZGV0YWlscycsXHJcbiAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnYXNzZXRfaWRzX2xpbWl0JyxcclxuICAgICAgdmFsdWU6ICczJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdzb3J0JyxcclxuICAgICAgdmFsdWU6ICdkZXNjX3N0YXJ0X2RhdGVfdGltZSdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnbG9hZF9qb2JfZGV0YWlscycsXHJcbiAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnc3RhdHVzJyxcclxuICAgICAgdmFsdWU6ICdDT01QTEVURUQnXHJcbiAgICB9XTtcclxuXHJcbiAgICB0aGlzLm90bW1TZXJ2aWNlLmdldEV4cG9ydEpvYnMocGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuam9ic19yZXNvdXJjZSAmJiByZXNwb25zZS5qb2JzX3Jlc291cmNlLmNvbGxlY3Rpb25fc3VtbWFyeSAmJlxyXG4gICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zICYmXHJcbiAgICAgICAgICByZXNwb25zZS5qb2JzX3Jlc291cmNlLmNvbGxlY3Rpb25fc3VtbWFyeS5hY3R1YWxfY291bnRfb2ZfaXRlbXMgPiAwKSB7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlLmpvYnNfcmVzb3VyY2Uuam9icyk7XHJcbiAgICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgICAgICB0aGlzLm5vRGF0YSA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLm5vRGF0YU1zZyA9ICdObyBpdGVtcy4nO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMubm9EYXRhID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm5vRGF0YU1zZyA9ICdVbmFibGUgdG8gZ2V0IGRhdGEgZnJvbSBRRFMuJztcclxuICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBkb3dubG9hZChqb2IpIHtcclxuICAgIGlmIChvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZCAmJlxyXG4gICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5maW5kUURTQ2xpZW50KSB7XHJcbiAgICAgIGNvbnN0IGZpbGVMaXN0ID0gW107XHJcbiAgICAgIGZpbGVMaXN0LnB1c2goeyBuYW1lOiBqb2Iuam9iX2RldGFpbHMuZXhwb3J0X3ppcF9uYW1lLCBzaXplOiBqb2Iuam9iX2RldGFpbHMuZXhwb3J0X3NpemUgfSk7XHJcbiAgICAgIHRoaXMucWRzU2VydmljZS5kb3dubG9hZChqb2Iuam9iX2lkLCBmaWxlTGlzdClcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKHJlc3BvbnNlKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm90bW1TZXJ2aWNlLmNoZWNrT3RtbVNlc3Npb24oKS5zdWJzY3JpYmUoKTtcclxuICAgICAgaWYgKGpvYiAmJiBqb2Iuam9iX2RldGFpbHMgJiYgam9iLmpvYl9kZXRhaWxzLnJlbGF0aXZlX2Rvd25sb2FkX3VybCkge1xyXG4gICAgICAgIGNvbnN0IG90bW1CYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG4gICAgICAgIGNvbnN0IGFzc2V0VVJMID0gb3RtbUJhc2VVcmwgKyBqb2Iuam9iX2RldGFpbHMucmVsYXRpdmVfZG93bmxvYWRfdXJsLnNsaWNlKDEpO1xyXG4gICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaW5pdGlhdGVEb3dubG9hZChhc3NldFVSTCwgam9iLmpvYl9kZXRhaWxzLmV4cG9ydF96aXBfbmFtZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBkb3dubG9hZGluZyBmaWxlJyk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICB0aGlzLm90bW1TZXJ2aWNlLmNoZWNrT3RtbVNlc3Npb24oKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZ2V0RXhwb3J0Sm9icygpO1xyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm5vRGF0YSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub0RhdGFNc2cgPSAnVW5hYmxlIHRvIGNvbm5lY3QgdG8gTWVkaWEgTWFuYWdlci4nO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVW5hYmxlIHRvIGluaXRpYWxpemUgT1RNTSBzZXNzaW9uJyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbn1cclxuIl19