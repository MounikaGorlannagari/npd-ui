import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let CustomSelectComponent = class CustomSelectComponent {
    constructor() {
        this.selectionChange = new EventEmitter();
    }
    onSelectionChange(currentOption) {
        this.options.map(option => {
            if (option.value === currentOption.value) {
                this.selectedOption = option;
            }
        });
        this.selectionChange.next(this.selectedOption);
    }
    handleDefaultSelection() {
        if (!this.options || this.options.length <= 0) {
            this.options = [];
            this.selectedOption = null;
        }
        else {
            if (this.selected) {
                this.selectedOption = this.options.find(option => option.value === this.selected.value);
            }
            else {
                this.selectedOption = this.options.find(option => option.default === true);
            }
        }
    }
    initalize() {
        this.handleDefaultSelection();
    }
    ngOnChanges(changes) {
        if (changes && changes.selected && !changes.selected.firstChange && JSON.stringify(changes.selected.previousValue) !== JSON.stringify(changes.selected.currentValue)
            && changes.selected.currentValue.value !== this.selectedOption.value) {
            this.handleDefaultSelection();
        }
    }
    ngOnInit() {
        this.initalize();
    }
};
__decorate([
    Input()
], CustomSelectComponent.prototype, "selected", void 0);
__decorate([
    Input()
], CustomSelectComponent.prototype, "options", void 0);
__decorate([
    Input()
], CustomSelectComponent.prototype, "controlLabel", void 0);
__decorate([
    Output()
], CustomSelectComponent.prototype, "selectionChange", void 0);
__decorate([
    Input()
], CustomSelectComponent.prototype, "bulkEdit", void 0);
__decorate([
    Input()
], CustomSelectComponent.prototype, "deliverableBulkEdit", void 0);
CustomSelectComponent = __decorate([
    Component({
        selector: 'mpm-custom-select',
        template: "<button class=\"mpm-custom-dropdown-btn\" mat-stroked-button [disabled]=\"!bulkEdit\"*ngIf=\"selectedOption && options?.length>0\"\r\n    [matTooltip]=\"controlLabel ? (controlLabel + ' ' +selectedOption.name) : selectedOption.name\"\r\n    [matMenuTriggerFor]=\"customSelectMenu\">\r\n    <span>{{controlLabel ? (controlLabel + ' ' +selectedOption.name) : selectedOption.name}}</span>\r\n    <mat-icon>arrow_drop_down</mat-icon>\r\n</button>\r\n<mat-menu #customSelectMenu=\"matMenu\">\r\n    <span *ngFor=\"let option of options\">\r\n        <button *ngIf=\"option.value != selectedOption?.value\" mat-menu-item [matTooltip]=\"option.name\"\r\n            (click)=\"onSelectionChange(option)\" [value]=\"option.value\">\r\n            <span>{{option.name}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
        styles: [""]
    })
], CustomSelectComponent);
export { CustomSelectComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tc2VsZWN0L2N1c3RvbS1zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQVF6RyxJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQVVoQztRQUpVLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQWdCLENBQUM7SUFJN0MsQ0FBQztJQUlqQixpQkFBaUIsQ0FBQyxhQUFhO1FBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3hCLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsS0FBSyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQzthQUM5QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxzQkFBc0I7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekY7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLENBQUM7YUFDNUU7U0FDRjtJQUNILENBQUM7SUFFRCxTQUFTO1FBQ1AsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsUUFBUSxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7ZUFDL0osT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ3RFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1NBQy9CO0lBQ0gsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztDQUVGLENBQUE7QUFqRFU7SUFBUixLQUFLLEVBQUU7dURBQXdCO0FBRXZCO0lBQVIsS0FBSyxFQUFFO3NEQUE4QjtBQUM3QjtJQUFSLEtBQUssRUFBRTsyREFBc0I7QUFDcEI7SUFBVCxNQUFNLEVBQUU7OERBQW9EO0FBRXBEO0lBQVIsS0FBSyxFQUFFO3VEQUFVO0FBQ1Q7SUFBUixLQUFLLEVBQUU7a0VBQXFCO0FBVGxCLHFCQUFxQjtJQUxqQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLDZ6QkFBNkM7O0tBRTlDLENBQUM7R0FDVyxxQkFBcUIsQ0FtRGpDO1NBbkRZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDdXN0b21TZWxlY3QgfSBmcm9tICcuL29iamVjdHMvQ3VzdG9tU2VsZWN0T2JqZWN0JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWN1c3RvbS1zZWxlY3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jdXN0b20tc2VsZWN0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jdXN0b20tc2VsZWN0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEN1c3RvbVNlbGVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgc2VsZWN0ZWQ6IEN1c3RvbVNlbGVjdDtcclxuXHJcbiAgQElucHV0KCkgb3B0aW9uczogQXJyYXk8Q3VzdG9tU2VsZWN0PjtcclxuICBASW5wdXQoKSBjb250cm9sTGFiZWw6IHN0cmluZztcclxuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxDdXN0b21TZWxlY3Q+KCk7XHJcblxyXG4gIEBJbnB1dCgpIGJ1bGtFZGl0O1xyXG4gIEBJbnB1dCgpIGRlbGl2ZXJhYmxlQnVsa0VkaXQ7XHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgc2VsZWN0ZWRPcHRpb246IEN1c3RvbVNlbGVjdDtcclxuXHJcbiAgb25TZWxlY3Rpb25DaGFuZ2UoY3VycmVudE9wdGlvbikge1xyXG4gICAgdGhpcy5vcHRpb25zLm1hcChvcHRpb24gPT4ge1xyXG4gICAgICBpZiAob3B0aW9uLnZhbHVlID09PSBjdXJyZW50T3B0aW9uLnZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IG9wdGlvbjtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlRGVmYXVsdFNlbGVjdGlvbigpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5vcHRpb25zIHx8IHRoaXMub3B0aW9ucy5sZW5ndGggPD0gMCkge1xyXG4gICAgICB0aGlzLm9wdGlvbnMgPSBbXTtcclxuICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IG51bGw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB0aGlzLm9wdGlvbnMuZmluZChvcHRpb24gPT4gb3B0aW9uLnZhbHVlID09PSB0aGlzLnNlbGVjdGVkLnZhbHVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0gdGhpcy5vcHRpb25zLmZpbmQob3B0aW9uID0+IG9wdGlvbi5kZWZhdWx0ID09PSB0cnVlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW5pdGFsaXplKCk6IHZvaWQge1xyXG4gICAgdGhpcy5oYW5kbGVEZWZhdWx0U2VsZWN0aW9uKCk7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBpZiAoY2hhbmdlcyAmJiBjaGFuZ2VzLnNlbGVjdGVkICYmICFjaGFuZ2VzLnNlbGVjdGVkLmZpcnN0Q2hhbmdlICYmIEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuc2VsZWN0ZWQucHJldmlvdXNWYWx1ZSkgIT09IEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuc2VsZWN0ZWQuY3VycmVudFZhbHVlKVxyXG4gICAgICAmJiBjaGFuZ2VzLnNlbGVjdGVkLmN1cnJlbnRWYWx1ZS52YWx1ZSAhPT0gdGhpcy5zZWxlY3RlZE9wdGlvbi52YWx1ZSkge1xyXG4gICAgICB0aGlzLmhhbmRsZURlZmF1bHRTZWxlY3Rpb24oKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5pbml0YWxpemUoKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==