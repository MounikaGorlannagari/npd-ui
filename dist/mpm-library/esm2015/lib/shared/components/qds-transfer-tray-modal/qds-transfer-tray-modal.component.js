import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
let QdsTransferTrayModalComponent = class QdsTransferTrayModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'QDS File Transfers';
    }
    cancelDialog() {
        this.dialogRef.close();
    }
};
QdsTransferTrayModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
QdsTransferTrayModalComponent = __decorate([
    Component({
        selector: 'mpm-qds-transfer-tray-modal',
        template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-qds-transfer-tray></mpm-qds-transfer-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button class=\"action-button\" mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
        styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}.action-button{border-radius:0}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], QdsTransferTrayModalComponent);
export { QdsTransferTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFLElBQWEsNkJBQTZCLEdBQTFDLE1BQWEsNkJBQTZCO0lBSXhDLFlBQ1MsU0FBc0QsRUFDN0IsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUE2QztRQUM3QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxvQkFBb0IsQ0FBQztJQUs5QixDQUFDO0lBRUwsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekIsQ0FBQztDQUVGLENBQUE7O1lBUnFCLFlBQVk7NENBQzdCLE1BQU0sU0FBQyxlQUFlOztBQU5kLDZCQUE2QjtJQUx6QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsNkJBQTZCO1FBQ3ZDLGdoQkFBdUQ7O0tBRXhELENBQUM7SUFPRyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtHQU5mLDZCQUE2QixDQWF6QztTQWJZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTUFUX0RJQUxPR19EQVRBLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9xZHMtdHJhbnNmZXItdHJheS1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUWRzVHJhbnNmZXJUcmF5TW9kYWxDb21wb25lbnQge1xyXG5cclxuICBtb2RhbFRpdGxlID0gJ1FEUyBGaWxlIFRyYW5zZmVycyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPFFkc1RyYW5zZmVyVHJheU1vZGFsQ29tcG9uZW50PixcclxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YVxyXG4gICkgeyB9XHJcblxyXG4gIGNhbmNlbERpYWxvZygpIHtcclxuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=