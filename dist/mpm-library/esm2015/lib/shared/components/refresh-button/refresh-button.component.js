import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let RefreshButtonComponent = class RefreshButtonComponent {
    constructor() {
        this.clicked = new EventEmitter();
    }
    refresh() {
        this.clicked.next();
    }
    ngOnInit() {
        this.disabled = this.disabled ? true : false;
    }
};
__decorate([
    Input()
], RefreshButtonComponent.prototype, "disabled", void 0);
__decorate([
    Output()
], RefreshButtonComponent.prototype, "clicked", void 0);
RefreshButtonComponent = __decorate([
    Component({
        selector: 'mpm-refresh-button',
        template: "<button mat-icon-button matTooltip=\"Refresh\" (click)=\"refresh()\" [disabled]=\"disabled\">\r\n    <mat-icon color=\"primary\" class=\"mpm-refresh-button\">refresh</mat-icon>\r\n</button>",
        styles: [""]
    })
], RefreshButtonComponent);
export { RefreshButtonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmcmVzaC1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvcmVmcmVzaC1idXR0b24vcmVmcmVzaC1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTy9FLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBS2pDO1FBRlUsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFFdkIsQ0FBQztJQUVqQixPQUFPO1FBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDL0MsQ0FBQztDQUVGLENBQUE7QUFiVTtJQUFSLEtBQUssRUFBRTt3REFBVTtBQUNSO0lBQVQsTUFBTSxFQUFFO3VEQUE4QjtBQUg1QixzQkFBc0I7SUFMbEMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLG9CQUFvQjtRQUM5Qix5TUFBOEM7O0tBRS9DLENBQUM7R0FDVyxzQkFBc0IsQ0FlbEM7U0FmWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1yZWZyZXNoLWJ1dHRvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3JlZnJlc2gtYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9yZWZyZXNoLWJ1dHRvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZWZyZXNoQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgZGlzYWJsZWQ7XHJcbiAgQE91dHB1dCgpIGNsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHJlZnJlc2goKSB7XHJcbiAgICB0aGlzLmNsaWNrZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5kaXNhYmxlZCA/IHRydWUgOiBmYWxzZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==