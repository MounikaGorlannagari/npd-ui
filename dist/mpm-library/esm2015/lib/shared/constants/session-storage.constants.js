export const SessionStorageConstants = {
    ALL_ROLES: 'ALL_ROLES',
    ALL_APPLICATION_DETAILS: 'ALL_APPLICATION_DETAILS',
    ALL_VIEW_CONFIG: 'ALL_VIEW_CONFIG',
    ALL_TEAM_ROLES: 'ALL_TEAM_ROLES',
    ALL_TEAMS: 'ALL_TEAMS',
    ALL_PERSONS: 'ALL_PERSONS',
    CURRENT_MPM_USER_PREFERENCE: 'CURRENT_MPM_USER_PREFERENCE',
    MEDIA_MANAGER_CONFIG: 'MEDIA_MANAGER_CONFIG',
    MENU_FOR_USER: 'MENU_FOR_USER',
    ALL_CATEGORY: 'ALL_CATEGORY',
    ALL_PRIORITIES: 'ALL_PRIORITIES',
    ALL_STATUS: 'ALL_STATUS',
    ALL_MPM_FIELD_CONFIG: 'ALL_MPM_FIELD_CONFIG',
    INDEXER_CONFIG_BY_CATEGORY_ID: 'INDEXER_CONFIG_BY_CATEGORY_ID',
    ALL_MPM_VIEW_CONFIG: 'ALL_MPM_VIEW_CONFIG',
    ALL_LEVELS_BY_CATEGORY_ID: 'ALL_LEVELS_BY_CATEGORY_ID',
    USER_DETAILS_LDAP: 'USER_DETAILS_LDAP',
    USER_DETAILS_WORKFLOW: 'USER_DETAILS_WORKFLOW',
    STATUS_BY_CATEGORY_LEVEL_NAME: 'STATUS_BY_CATEGORY_LEVEL_NAME',
    ALL_USERS: 'ALL_USERS',
    FACET_CONFIGURATIONS: 'FACET_CONFIGURATIONS',
    EMAIL_CONFIG: 'EMAIL_CONFIG',
    // MPMV3-2237
    OTDS_TICKET: 'OTDS_TICKET',
    OTDS_USER_ID: 'OTDS_USER_ID',
    USER_NAME: 'USER_NAME',
    PASSWORD: 'PASSWORD',
    OTDS_TICKETS: 'OTDS_TICKETS',
    SAMLART: 'SAMLART',
    OTDS_USER_DN: 'OTDS_USER_DN',
    OTMM_VERSION: 'OTMM_VERSION',
    SYSTEM_DETAILS: 'SYSTEM_DETAILS'
    /*
    PROJECT_CONFIG_BY_ID: 'PROJECT_CONFIG_BY_ID',
    APP_CONFIG_BY_ID: 'APP_CONFIG_BY_ID',
    ASSET_CONFIG_BY_ID: 'ASSET_CONFIG_BY_ID',
    COMMENT_CONFIG_BY_ID: 'COMMENT_CONFIG_BY_ID',
    BRAND_CONFIG_BY_ID: 'BRAND_CONFIG_BY_ID',
     PERSON_BY_IDENTITY_USER_ID: 'PERSON_BY_IDENTITY_USER_ID',
    PERSON_BY_USER_ID: 'PERSON_BY_USER_ID',
     ALL_CAMPAIGN: 'ALL_CAMPAIGN',*/
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsTUFBTSx1QkFBdUIsR0FBRztJQUNuQyxTQUFTLEVBQUUsV0FBVztJQUN0Qix1QkFBdUIsRUFBRSx5QkFBeUI7SUFDbEQsZUFBZSxFQUFFLGlCQUFpQjtJQUNsQyxjQUFjLEVBQUUsZ0JBQWdCO0lBQ2hDLFNBQVMsRUFBRSxXQUFXO0lBQ3RCLFdBQVcsRUFBRSxhQUFhO0lBQzFCLDJCQUEyQixFQUFFLDZCQUE2QjtJQUMxRCxvQkFBb0IsRUFBRSxzQkFBc0I7SUFDNUMsYUFBYSxFQUFFLGVBQWU7SUFDOUIsWUFBWSxFQUFFLGNBQWM7SUFDNUIsY0FBYyxFQUFFLGdCQUFnQjtJQUNoQyxVQUFVLEVBQUUsWUFBWTtJQUN4QixvQkFBb0IsRUFBRSxzQkFBc0I7SUFDNUMsNkJBQTZCLEVBQUUsK0JBQStCO0lBQzlELG1CQUFtQixFQUFFLHFCQUFxQjtJQUMxQyx5QkFBeUIsRUFBRSwyQkFBMkI7SUFDdEQsaUJBQWlCLEVBQUUsbUJBQW1CO0lBQ3RDLHFCQUFxQixFQUFFLHVCQUF1QjtJQUM5Qyw2QkFBNkIsRUFBRSwrQkFBK0I7SUFDOUQsU0FBUyxFQUFFLFdBQVc7SUFDdEIsb0JBQW9CLEVBQUUsc0JBQXNCO0lBQzVDLFlBQVksRUFBRSxjQUFjO0lBQzVCLGFBQWE7SUFDYixXQUFXLEVBQUUsYUFBYTtJQUMxQixZQUFZLEVBQUUsY0FBYztJQUM1QixTQUFTLEVBQUUsV0FBVztJQUN0QixRQUFRLEVBQUUsVUFBVTtJQUVwQixZQUFZLEVBQUUsY0FBYztJQUM1QixPQUFPLEVBQUUsU0FBUztJQUNsQixZQUFZLEVBQUUsY0FBYztJQUM1QixZQUFZLEVBQUUsY0FBYztJQUM1QixjQUFjLEVBQUUsZ0JBQWdCO0lBRWhDOzs7Ozs7OztvQ0FRZ0M7Q0FDbkMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyA9IHtcclxuICAgIEFMTF9ST0xFUzogJ0FMTF9ST0xFUycsXHJcbiAgICBBTExfQVBQTElDQVRJT05fREVUQUlMUzogJ0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTJyxcclxuICAgIEFMTF9WSUVXX0NPTkZJRzogJ0FMTF9WSUVXX0NPTkZJRycsXHJcbiAgICBBTExfVEVBTV9ST0xFUzogJ0FMTF9URUFNX1JPTEVTJyxcclxuICAgIEFMTF9URUFNUzogJ0FMTF9URUFNUycsXHJcbiAgICBBTExfUEVSU09OUzogJ0FMTF9QRVJTT05TJyxcclxuICAgIENVUlJFTlRfTVBNX1VTRVJfUFJFRkVSRU5DRTogJ0NVUlJFTlRfTVBNX1VTRVJfUFJFRkVSRU5DRScsXHJcbiAgICBNRURJQV9NQU5BR0VSX0NPTkZJRzogJ01FRElBX01BTkFHRVJfQ09ORklHJyxcclxuICAgIE1FTlVfRk9SX1VTRVI6ICdNRU5VX0ZPUl9VU0VSJyxcclxuICAgIEFMTF9DQVRFR09SWTogJ0FMTF9DQVRFR09SWScsXHJcbiAgICBBTExfUFJJT1JJVElFUzogJ0FMTF9QUklPUklUSUVTJyxcclxuICAgIEFMTF9TVEFUVVM6ICdBTExfU1RBVFVTJyxcclxuICAgIEFMTF9NUE1fRklFTERfQ09ORklHOiAnQUxMX01QTV9GSUVMRF9DT05GSUcnLFxyXG4gICAgSU5ERVhFUl9DT05GSUdfQllfQ0FURUdPUllfSUQ6ICdJTkRFWEVSX0NPTkZJR19CWV9DQVRFR09SWV9JRCcsXHJcbiAgICBBTExfTVBNX1ZJRVdfQ09ORklHOiAnQUxMX01QTV9WSUVXX0NPTkZJRycsXHJcbiAgICBBTExfTEVWRUxTX0JZX0NBVEVHT1JZX0lEOiAnQUxMX0xFVkVMU19CWV9DQVRFR09SWV9JRCcsXHJcbiAgICBVU0VSX0RFVEFJTFNfTERBUDogJ1VTRVJfREVUQUlMU19MREFQJyxcclxuICAgIFVTRVJfREVUQUlMU19XT1JLRkxPVzogJ1VTRVJfREVUQUlMU19XT1JLRkxPVycsXHJcbiAgICBTVEFUVVNfQllfQ0FURUdPUllfTEVWRUxfTkFNRTogJ1NUQVRVU19CWV9DQVRFR09SWV9MRVZFTF9OQU1FJyxcclxuICAgIEFMTF9VU0VSUzogJ0FMTF9VU0VSUycsXHJcbiAgICBGQUNFVF9DT05GSUdVUkFUSU9OUzogJ0ZBQ0VUX0NPTkZJR1VSQVRJT05TJyxcclxuICAgIEVNQUlMX0NPTkZJRzogJ0VNQUlMX0NPTkZJRycsXHJcbiAgICAvLyBNUE1WMy0yMjM3XHJcbiAgICBPVERTX1RJQ0tFVDogJ09URFNfVElDS0VUJyxcclxuICAgIE9URFNfVVNFUl9JRDogJ09URFNfVVNFUl9JRCcsXHJcbiAgICBVU0VSX05BTUU6ICdVU0VSX05BTUUnLFxyXG4gICAgUEFTU1dPUkQ6ICdQQVNTV09SRCcsXHJcbiAgICBcclxuICAgIE9URFNfVElDS0VUUzogJ09URFNfVElDS0VUUycsXHJcbiAgICBTQU1MQVJUOiAnU0FNTEFSVCcsXHJcbiAgICBPVERTX1VTRVJfRE46ICdPVERTX1VTRVJfRE4nLFxyXG4gICAgT1RNTV9WRVJTSU9OOiAnT1RNTV9WRVJTSU9OJyxcclxuICAgIFNZU1RFTV9ERVRBSUxTOiAnU1lTVEVNX0RFVEFJTFMnXHJcblxyXG4gICAgLypcclxuICAgIFBST0pFQ1RfQ09ORklHX0JZX0lEOiAnUFJPSkVDVF9DT05GSUdfQllfSUQnLFxyXG4gICAgQVBQX0NPTkZJR19CWV9JRDogJ0FQUF9DT05GSUdfQllfSUQnLFxyXG4gICAgQVNTRVRfQ09ORklHX0JZX0lEOiAnQVNTRVRfQ09ORklHX0JZX0lEJyxcclxuICAgIENPTU1FTlRfQ09ORklHX0JZX0lEOiAnQ09NTUVOVF9DT05GSUdfQllfSUQnLFxyXG4gICAgQlJBTkRfQ09ORklHX0JZX0lEOiAnQlJBTkRfQ09ORklHX0JZX0lEJyxcclxuICAgICBQRVJTT05fQllfSURFTlRJVFlfVVNFUl9JRDogJ1BFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEJyxcclxuICAgIFBFUlNPTl9CWV9VU0VSX0lEOiAnUEVSU09OX0JZX1VTRVJfSUQnLCBcclxuICAgICBBTExfQ0FNUEFJR046ICdBTExfQ0FNUEFJR04nLCovXHJcbn07XHJcbiJdfQ==