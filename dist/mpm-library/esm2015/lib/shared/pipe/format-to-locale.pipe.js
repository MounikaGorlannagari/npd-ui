import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
import * as acronui from '../../mpm-utils/auth/utility';
let FormatToLocalePipe = class FormatToLocalePipe {
    constructor() { }
    transform(value, args) {
        let isoStringToLocale = '';
        if (value !== undefined && value !== '') {
            isoStringToLocale = acronui.getDateWithLocaleTimeZone(value);
            isoStringToLocale = isoStringToLocale.toLocaleDateString(navigator.language);
            return isoStringToLocale;
        }
        return '';
    }
};
FormatToLocalePipe = __decorate([
    Pipe({
        name: 'formatToLocale'
    })
], FormatToLocalePipe);
export { FormatToLocalePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRvLWxvY2FsZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBTXhELElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBRTNCLGdCQUFnQixDQUFDO0lBRWpCLFNBQVMsQ0FBQyxLQUFVLEVBQUUsSUFBVTtRQUM1QixJQUFJLGlCQUFpQixHQUFRLEVBQUUsQ0FBQztRQUNoQyxJQUFJLEtBQUssS0FBSyxTQUFTLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUNyQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0QsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdFLE9BQU8saUJBQWlCLENBQUM7U0FDNUI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Q0FFSixDQUFBO0FBZFksa0JBQWtCO0lBSjlCLElBQUksQ0FBQztRQUNGLElBQUksRUFBRSxnQkFBZ0I7S0FDekIsQ0FBQztHQUVXLGtCQUFrQixDQWM5QjtTQWRZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuXHJcbkBQaXBlKHtcclxuICAgIG5hbWU6ICdmb3JtYXRUb0xvY2FsZSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtYXRUb0xvY2FsZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBhcmdzPzogYW55KTogYW55IHtcclxuICAgICAgICBsZXQgaXNvU3RyaW5nVG9Mb2NhbGU6IGFueSA9ICcnO1xyXG4gICAgICAgIGlmICh2YWx1ZSAhPT0gdW5kZWZpbmVkICYmIHZhbHVlICE9PSAnJykge1xyXG4gICAgICAgICAgICBpc29TdHJpbmdUb0xvY2FsZSA9IGFjcm9udWkuZ2V0RGF0ZVdpdGhMb2NhbGVUaW1lWm9uZSh2YWx1ZSk7XHJcbiAgICAgICAgICAgIGlzb1N0cmluZ1RvTG9jYWxlID0gaXNvU3RyaW5nVG9Mb2NhbGUudG9Mb2NhbGVEYXRlU3RyaW5nKG5hdmlnYXRvci5sYW5ndWFnZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBpc29TdHJpbmdUb0xvY2FsZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=