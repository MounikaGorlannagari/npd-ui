import { __decorate } from "tslib";
import { NgControl } from '@angular/forms';
import { Directive, Input } from '@angular/core';
let DisableControlDirective = class DisableControlDirective {
    constructor(ngControl) {
        this.ngControl = ngControl;
    }
    set disableControl(condition) {
        const action = condition ? 'disable' : 'enable';
        this.ngControl.control[action]();
    }
};
DisableControlDirective.ctorParameters = () => [
    { type: NgControl }
];
__decorate([
    Input()
], DisableControlDirective.prototype, "disableControl", null);
DisableControlDirective = __decorate([
    Directive({
        // tslint:disable-next-line: directive-selector
        selector: '[disableControl]'
    })
], DisableControlDirective);
export { DisableControlDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGlzYWJsZUNvbnRyb2xEaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL0Rpc2FibGVDb250cm9sRGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPakQsSUFBYSx1QkFBdUIsR0FBcEMsTUFBYSx1QkFBdUI7SUFDaEMsWUFDVyxTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO0lBQzNCLENBQUM7SUFFSSxJQUFJLGNBQWMsQ0FBQyxTQUFrQjtRQUMxQyxNQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDckMsQ0FBQztDQUNKLENBQUE7O1lBUHlCLFNBQVM7O0FBR3RCO0lBQVIsS0FBSyxFQUFFOzZEQUdQO0FBUlEsdUJBQXVCO0lBTG5DLFNBQVMsQ0FBQztRQUNQLCtDQUErQztRQUMvQyxRQUFRLEVBQUUsa0JBQWtCO0tBQy9CLENBQUM7R0FFVyx1QkFBdUIsQ0FTbkM7U0FUWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ0NvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkaXJlY3RpdmUtc2VsZWN0b3JcclxuICAgIHNlbGVjdG9yOiAnW2Rpc2FibGVDb250cm9sXSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEaXNhYmxlQ29udHJvbERpcmVjdGl2ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICAgICkgeyB9XHJcblxyXG4gICAgQElucHV0KCkgc2V0IGRpc2FibGVDb250cm9sKGNvbmRpdGlvbjogYm9vbGVhbikge1xyXG4gICAgICAgIGNvbnN0IGFjdGlvbiA9IGNvbmRpdGlvbiA/ICdkaXNhYmxlJyA6ICdlbmFibGUnO1xyXG4gICAgICAgIHRoaXMubmdDb250cm9sLmNvbnRyb2xbYWN0aW9uXSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==