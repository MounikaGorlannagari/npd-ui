import { __decorate } from "tslib";
import { Directive, ElementRef, Input } from '@angular/core';
import { NgControl } from '@angular/forms';
let AutofocusDirective = class AutofocusDirective {
    constructor(elementRef, ngControl) {
        this.elementRef = elementRef;
        this.ngControl = ngControl;
    }
    set appAutoFocus(condition) {
        if (condition) {
            this.elementRef.nativeElement.focus();
        }
    }
};
AutofocusDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: NgControl }
];
__decorate([
    Input()
], AutofocusDirective.prototype, "appAutoFocus", null);
AutofocusDirective = __decorate([
    Directive({
        selector: '[appAutoFocus]'
    })
], AutofocusDirective);
export { AutofocusDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0b2ZvY3VzRGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2RpcmVjdGl2ZS9BdXRvZm9jdXNEaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFNM0MsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFFM0IsWUFDVyxVQUFzQixFQUN0QixTQUFvQjtRQURwQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQVc7SUFDM0IsQ0FBQztJQUVJLElBQUksWUFBWSxDQUFDLFNBQWtCO1FBQ3hDLElBQUksU0FBUyxFQUFFO1lBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekM7SUFDTCxDQUFDO0NBRUosQ0FBQTs7WUFWMEIsVUFBVTtZQUNYLFNBQVM7O0FBR3RCO0lBQVIsS0FBSyxFQUFFO3NEQUlQO0FBWFEsa0JBQWtCO0lBSjlCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxnQkFBZ0I7S0FDN0IsQ0FBQztHQUVXLGtCQUFrQixDQWE5QjtTQWJZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmdDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thcHBBdXRvRm9jdXNdJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dG9mb2N1c0RpcmVjdGl2ZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHVibGljIG5nQ29udHJvbDogTmdDb250cm9sXHJcbiAgICApIHsgfVxyXG5cclxuICAgIEBJbnB1dCgpIHNldCBhcHBBdXRvRm9jdXMoY29uZGl0aW9uOiBib29sZWFuKSB7XHJcbiAgICAgICAgaWYgKGNvbmRpdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19