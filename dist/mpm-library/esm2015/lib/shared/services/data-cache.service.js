import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusService } from './status.service';
import * as i0 from "@angular/core";
import * as i1 from "./status.service";
import * as i2 from "../../mpm-utils/services/sharing.service";
let DataCacheService = class DataCacheService {
    constructor(statusService, sharingservice) {
        this.statusService = statusService;
        this.sharingservice = sharingservice;
        this.statusCache = {};
    }
    getStatusbyLevel(level) {
        return new Observable(observer => {
            if (level && Array.isArray(this.statusCache[level]) && this.statusCache[level].length > 0) {
                observer.next(this.statusCache[level]);
                observer.complete();
            }
            else {
                /* this.statusService.getStatusByCategoryLevelName(level).subscribe(statuesResponse => {
                    this.statusCache[level] = statuesResponse;
                    observer.next(this.statusCache[level]);
                    observer.complete();
                }); */
                const status = this.statusService.getAllStatusBycategoryName(level);
                this.status = status && status.length > 0 && Array.isArray(status) ? status[0] : status;
                this.statusCache[level] = this.status;
                observer.next(this.statusCache[level]);
                observer.complete();
            }
        });
    }
};
DataCacheService.ctorParameters = () => [
    { type: StatusService },
    { type: SharingService }
];
DataCacheService.ɵprov = i0.ɵɵdefineInjectable({ factory: function DataCacheService_Factory() { return new DataCacheService(i0.ɵɵinject(i1.StatusService), i0.ɵɵinject(i2.SharingService)); }, token: DataCacheService, providedIn: "root" });
DataCacheService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], DataCacheService);
export { DataCacheService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jYWNoZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2RhdGEtY2FjaGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBR2xDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUxRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFNakQsSUFBYSxnQkFBZ0IsR0FBN0IsTUFBYSxnQkFBZ0I7SUFFekIsWUFDWSxhQUE0QixFQUM1QixjQUE4QjtRQUQ5QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFHMUMsZ0JBQVcsR0FBZ0IsRUFBRSxDQUFDO0lBRjFCLENBQUM7SUFNTCxnQkFBZ0IsQ0FBQyxLQUFnQjtRQUM3QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSDs7OztzQkFJTTtnQkFDTixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwRSxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDeEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUN0QyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUE1QjhCLGFBQWE7WUFDWixjQUFjOzs7QUFKakMsZ0JBQWdCO0lBSjVCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxnQkFBZ0IsQ0ErQjVCO1NBL0JZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c0NhY2hlIH0gZnJvbSAnLi4vb2JqZWN0L1N0YXR1c0NhY2hlJztcclxuaW1wb3J0IHsgU3RhdHVzU2VydmljZSB9IGZyb20gJy4vc3RhdHVzLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGF0YUNhY2hlU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgc2hhcmluZ3NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHN0YXR1c0NhY2hlOiBTdGF0dXNDYWNoZSA9IHt9O1xyXG4gICAgY2FjaGVkTGlzdERlcGVuZGVudEZpbHRlcjogYW55O1xyXG4gICAgc3RhdHVzO1xyXG5cclxuICAgIGdldFN0YXR1c2J5TGV2ZWwobGV2ZWw6IE1QTV9MRVZFTCk6IE9ic2VydmFibGU8QXJyYXk8U3RhdHVzPiB8IG51bGw+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAobGV2ZWwgJiYgQXJyYXkuaXNBcnJheSh0aGlzLnN0YXR1c0NhY2hlW2xldmVsXSkgJiYgdGhpcy5zdGF0dXNDYWNoZVtsZXZlbF0ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnN0YXR1c0NhY2hlW2xldmVsXSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLyogdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbE5hbWUobGV2ZWwpLnN1YnNjcmliZShzdGF0dWVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdHVzQ2FjaGVbbGV2ZWxdID0gc3RhdHVlc1Jlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5zdGF0dXNDYWNoZVtsZXZlbF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxTdGF0dXNCeWNhdGVnb3J5TmFtZShsZXZlbCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXR1cyA9IHN0YXR1cyAmJiBzdGF0dXMubGVuZ3RoID4gMCAmJiBBcnJheS5pc0FycmF5KHN0YXR1cykgPyBzdGF0dXNbMF0gOiBzdGF0dXM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXR1c0NhY2hlW2xldmVsXSA9IHRoaXMuc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnN0YXR1c0NhY2hlW2xldmVsXSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==