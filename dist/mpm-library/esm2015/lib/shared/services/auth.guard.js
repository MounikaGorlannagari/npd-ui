import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { GloabalConfig as config } from '../../mpm-utils/config/config';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AuthenticationService } from '../../mpm-utils/services/authentication.service';
import { Observable, timer } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoaderService } from '../../loader/loader.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { switchMap } from 'rxjs/operators';
import { OTMMSessionHandler } from '../otmmSessionHandler';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MPM_MENU_TYPES } from '../../mpm-utils/objects/MenuTypes';
import { OTDSTicketService } from '../../mpm-utils/services/OTDSTicket.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/entity.appdef.service";
import * as i2 from "../../mpm-utils/services/util.service";
import * as i3 from "../../mpm-utils/services/authentication.service";
import * as i4 from "../../loader/loader.service";
import * as i5 from "../../mpm-utils/services/otmm.service";
import * as i6 from "../../login/services/app.route.service";
import * as i7 from "../../mpm-utils/services/app.service";
import * as i8 from "../../mpm-utils/services/sharing.service";
import * as i9 from "../../mpm-utils/services/OTDSTicket.service";
import * as i10 from "@angular/router";
let AuthGuard = class AuthGuard {
    constructor(entityAppDefService, utilServices, authenticationService, loaderService, otmmService, appRouteService, appService, sharingService, otdsTicketService, router) {
        this.entityAppDefService = entityAppDefService;
        this.utilServices = utilServices;
        this.authenticationService = authenticationService;
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.appRouteService = appRouteService;
        this.appService = appService;
        this.sharingService = sharingService;
        this.otdsTicketService = otdsTicketService;
        this.router = router;
        this.otmmSessionSubscription = OTMMSessionHandler.subscribe();
        this.isDataAlradyLoaded = false;
        this.appItems = [];
        this.MENU_HEADER_TYPE = 'HEADER';
        this.MENU_TYPE = 'GENERAL';
        this.iconToAccessMPM = false;
        this.rolesForUser = [];
        this.APP_DEF_PATH = '';
    }
    clearLocalStorage() {
        window.localStorage.removeItem('MPM_V3_LAST_LINK');
    }
    goToLogin() {
        this.appRouteService.goToLogin();
    }
    loadApp() {
        return new Observable(observer => {
            this.rolesForUser = this.utilServices.getUserRoles();
            this.appService.getAllApplicationDetails()
                .subscribe(response => {
                console.log(response);
                /* sessionStorage.setItem('ALL_APPLICATION_DETAILS', response); */
                this.allApplicationItems = acronui.findObjectsByProp(response, 'MPM_Application_Definition');
                console.log(this.allApplicationItems);
                this.sharingService.setCampaignRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Campaign Manager'));
                this.sharingService.setReviewerRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Reviewer'));
                const reviewerRole = this.sharingService.getReviewerRole();
                const reviewer = (reviewerRole && reviewerRole.length && reviewerRole.length > 0) ? true : false;
                this.sharingService.setIsReviewer(reviewer);
                this.sharingService.setApproverRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Approver'));
                const approverRole = this.sharingService.getApproverRole();
                const approver = (approverRole && approverRole.length && approverRole.length > 0) ? true : false;
                this.sharingService.setIsApprover(approver);
                this.sharingService.setMemberRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Member'));
                const memberRole = this.sharingService.getMemberRole();
                const member = (memberRole && memberRole.length && memberRole.length > 0) ? true : false;
                this.sharingService.setIsMember(member);
                this.sharingService.setManagerRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Project Manager'));
                const managerRole = this.sharingService.getManagerRole();
                const manager = (managerRole && managerRole.length && managerRole.length > 0) ? true : false;
                this.sharingService.setIsManager(manager);
                /*                     this.sharingService.setCampaignManagerRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Campaign Manager'));
                                    const campaignManagerRole = this.sharingService.getCampaignManagerRole();
                                    const campaignManager = (campaignManagerRole && campaignManagerRole.length && campaignManagerRole.length > 0) ? true : false;
                                    this.sharingService.setIsCampaignManager(campaignManager); */ // MVSS-128
                if (this.allApplicationItems.length === 0) {
                    console.error('No application configuration available.');
                    observer.next('NOACCESS');
                    observer.complete();
                    return;
                }
                this.appService.getDefaultEmailConfig().subscribe(emailResponse => {
                    this.sharingService.setDefaultEmailConfig(emailResponse ? emailResponse : null);
                });
                this.appService.getProjectConfigById(this.allApplicationItems[0].R_PO_PROJECT_CONFIG['MPM_Project_Config-id'].Id).subscribe(projectConfigResponse => {
                    this.sharingService.setProjectConfig(projectConfigResponse.MPM_Project_Config);
                });
                this.appService.getAppConfigById(this.allApplicationItems[0].R_PO_APP_CONFIG['MPM_App_Config-id'].Id).subscribe(appConfigResponse => {
                    this.sharingService.setAppConfig(appConfigResponse.MPM_App_Config);
                });
                this.appService.getAssetConfigById(this.allApplicationItems[0].R_PO_ASSET_CONFIG['MPM_Asset_Config-id'].Id).subscribe(assetConfigResponse => {
                    this.sharingService.setAssetConfig(assetConfigResponse.MPM_Asset_Config);
                });
                this.appService.getCommentConfigById(this.allApplicationItems[0].R_PO_COMMENTS_CONFIG['MPM_Comments_Config-id'].Id).subscribe(commentConfigResponse => {
                    this.sharingService.setCommentConfig(commentConfigResponse.MPM_Comments_Config);
                });
                this.appService.getBrandConfigById(this.allApplicationItems[0].R_PO_BRAND_CONFIG['MPM_Brand_Config-id'].Id).subscribe(brandConfigResponse => {
                    this.sharingService.setBrandConfig(brandConfigResponse.MPM_Brand_Config);
                });
                this.appService.getAllViewConfig().subscribe(viewConfigResponse => {
                    this.sharingService.setAllViewConfig(viewConfigResponse.MPM_View_Config);
                });
                this.appService.getAllTeamRoles().subscribe(allTeamRolesResponse => {
                    this.sharingService.setAllTeamRoles(allTeamRolesResponse.MPM_Team_Role_Mapping);
                });
                this.appService.getAllTeams().subscribe(allTeamsResponse => {
                    this.sharingService.setAllTeams(allTeamsResponse.GetAllTeamsResponse.MPM_Teams);
                });
                // this.appService.getAllPersons().subscribe(allPersonsResponse => {
                //     this.sharingService.setAllPersons(allPersonsResponse.Person);
                // });
                this.appService.getPersonByIdentityUserId({
                    userId: this.sharingService.getcurrentUserItemID()
                }).subscribe(personResponse => {
                    this.sharingService.setCurrentPerson(personResponse);
                });
                this.appService.getCurrentUserPreference().subscribe(userPreferenceResponse => {
                    this.sharingService.setCurrentUserPreference(userPreferenceResponse);
                });
                if (this.allApplicationItems.length === 0) {
                    console.error('No application configuration available.');
                    observer.next(false);
                    observer.complete();
                    return;
                }
                for (const role of this.rolesForUser) {
                    for (const appItem of this.allApplicationItems) {
                        if (appItem.R_PO_ACCESS_ROLE &&
                            appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'] &&
                            appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'].Id &&
                            role.Id === this.utilServices.GetRoleDNByRoleId(appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'].Id).ROLE_DN) {
                            this.appItems.push(appItem);
                        }
                    }
                }
                if (this.appItems.length > 0) {
                    let selectedAppItem;
                    const appId = this.utilServices.getQueryParameterFromURL('applicationId', document.URL);
                    if (appId && appId !== '') {
                        for (const appItem of this.appItems) {
                            if (appItem.APPLICATION_ID === appId) {
                                selectedAppItem = appItem;
                                break;
                            }
                        }
                    }
                    else {
                        selectedAppItem = this.appItems[0];
                    }
                    if (!selectedAppItem) {
                        observer.next(false);
                        observer.complete();
                        console.warn('No access to the application.');
                        return;
                    }
                    this.utilServices.setSelectedApp(selectedAppItem)
                        .subscribe(data => {
                        this.utilServices.setConfigsForApp()
                            .subscribe(configsResponse => {
                            const userRoleWithCN = acronui.findObjects(this.rolesForUser, 'Name', 'MPM Application Access');
                            this.menus = this.sharingService.getMenuByType(MPM_MENU_TYPES.HEADER);
                            if (userRoleWithCN.length === 1 && this.entityAppDefService.hasRole(userRoleWithCN[0].Id) && this.menus.length > 0) {
                                observer.next(true);
                                observer.complete();
                            }
                            else {
                                observer.error('NOACCESS');
                            }
                        }, configsError => {
                            observer.error('Something went wrong while getting application config details.');
                        });
                    }, error => {
                        observer.error('Something went wrong while setting application details.');
                    });
                }
                else {
                    observer.next(false);
                    observer.complete();
                    console.warn('No Access to the Application.');
                    return;
                }
            }, error => {
                observer.error('Something went wrong while getting application details.');
            });
        });
    }
    initLogging() {
        return new Observable(observer => {
            this.authenticationService.logIn().subscribe(success => {
                if (success === true) {
                    this.utilServices.setUserInfo()
                        .subscribe(response => {
                        if (response) {
                            this.loadApp()
                                .subscribe(loadAppResponse => {
                                if (loadAppResponse == true) {
                                    observer.next(true);
                                    observer.complete();
                                }
                                else if (loadAppResponse == 'NOACCESS') {
                                    observer.next(loadAppResponse);
                                    observer.complete();
                                }
                                else {
                                    observer.next(false);
                                    observer.complete();
                                }
                            }, error => {
                                this.loaderService.hide();
                                if (error == 'NOACCESS') {
                                    observer.next(error);
                                    observer.complete();
                                }
                                observer.error(new Error(error));
                                observer.complete();
                            });
                        }
                    }, error => {
                        console.log(error);
                        if (error == 'NOACCESS') {
                            observer.next(error);
                            observer.complete();
                        }
                        observer.next(false);
                        observer.complete();
                    });
                }
                else {
                    observer.error(new Error('Something went wrong while authenticating.'));
                }
            }, error => {
                this.loaderService.hide();
                observer.error(new Error(error));
            });
        });
    }
    storeLastVisitLink() {
        const currentUrl = window.location.toString();
        if (!currentUrl) {
            return localStorage.setItem('MPM_V3_LAST_LINK', '');
        }
        else if (currentUrl !== config.getOTDSLoginPageUrl() && currentUrl !== config.getPsOrgEndpoint()) {
            const splittedURL = currentUrl.split('/#/');
            if (Array.isArray(splittedURL) && splittedURL.length === 2 && splittedURL[1] !== '') {
                return localStorage.setItem('MPM_V3_LAST_LINK', currentUrl);
            }
        }
    }
    canActivate(next, state) {
        return new Observable(observer => {
            this.loaderService.show();
            this.initLogging().subscribe(validatedResponse => {
                if (validatedResponse == true) {
                    this.clearLocalStorage();
                    this.otdsTicketService.setOTDSTicketForUser().subscribe(ticket => {
                        this.otmmService.getOTMMSystemDetails()
                            .subscribe(sysDetailsResponse => {
                            // tslint:disable-next-line: max-line-length
                            let sessionTimeOut = sysDetailsResponse.system_details_resource.system_details_map.entry.find(entry => entry.key === 'HTTP_SESSION_TIME_OUT');
                            if (sessionTimeOut && sessionTimeOut.value) {
                                // 75% of the HTTP Session timeout
                                sessionTimeOut = Math.floor((Number(sessionTimeOut.value) * 0.75) * 1000);
                            }
                            else {
                                sessionTimeOut = (typeof this.sharingService.getMediaManagerConfig().sessionTimeOut === 'string') ?
                                    Number(this.sharingService.getMediaManagerConfig().sessionTimeOut) * 60000 :
                                    this.otmmService.STANDARD_SESSION_TIMEOUT_INTERVAL;
                            }
                            this.otmmSessionSubscription = timer(0, sessionTimeOut)
                                .pipe(switchMap(() => this.otmmService.startOTMMSession(false)) // MVS-239false
                            )
                                .subscribe(otmmSessionResponse => {
                                this.loaderService.hide();
                                observer.next(true);
                                observer.complete();
                                console.log('OTMM Session is initialized.');
                            }, otmmSessionError => {
                                this.loaderService.hide();
                                observer.next(true);
                                observer.complete();
                                console.error('Something went wrong while getting OTMM Session.');
                                this.appRouteService.goToLogout();
                            });
                        }, sysDetailsError => {
                            this.loaderService.hide();
                            observer.next(true);
                            observer.complete();
                            console.error('Something went wrong while getting OTMM system details.');
                        });
                    }, error => {
                        this.loaderService.hide();
                        observer.next(true);
                        observer.complete();
                        console.error('Something went wrong while getting OTDS ticket.');
                    });
                }
                else if (validatedResponse == 'NOACCESS') {
                    this.loaderService.hide();
                    this.router.navigate(['/apps/mpm/noaccess']);
                    observer.next(true);
                    observer.complete();
                }
            }, error => {
                this.storeLastVisitLink();
                this.loaderService.hide();
                observer.next(false);
                observer.complete();
                this.goToLogin();
            });
        });
    }
};
AuthGuard.ctorParameters = () => [
    { type: EntityAppDefService },
    { type: UtilService },
    { type: AuthenticationService },
    { type: LoaderService },
    { type: OTMMService },
    { type: AppRouteService },
    { type: AppService },
    { type: SharingService },
    { type: OTDSTicketService },
    { type: Router }
];
AuthGuard.ɵprov = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.EntityAppDefService), i0.ɵɵinject(i2.UtilService), i0.ɵɵinject(i3.AuthenticationService), i0.ɵɵinject(i4.LoaderService), i0.ɵɵinject(i5.OTMMService), i0.ɵɵinject(i6.AppRouteService), i0.ɵɵinject(i7.AppService), i0.ɵɵinject(i8.SharingService), i0.ɵɵinject(i9.OTDSTicketService), i0.ɵɵinject(i10.Router)); }, token: AuthGuard, providedIn: "root" });
AuthGuard = __decorate([
    Injectable({
        providedIn: 'root',
    })
], AuthGuard);
export { AuthGuard };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9hdXRoLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDeEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sS0FBSyxPQUFPLE1BQU0sOEJBQThCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUVuRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQzs7Ozs7Ozs7Ozs7O0FBT2hGLElBQWEsU0FBUyxHQUF0QixNQUFhLFNBQVM7SUFJbEIsWUFDVyxtQkFBd0MsRUFDeEMsWUFBeUIsRUFDekIscUJBQTRDLEVBQzVDLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLGVBQWdDLEVBQ2hDLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLGlCQUFvQyxFQUNwQyxNQUFjO1FBVGQsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxpQkFBWSxHQUFaLFlBQVksQ0FBYTtRQUN6QiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFabEIsNEJBQXVCLEdBQUcsa0JBQWtCLENBQUMsU0FBUyxFQUFFLENBQUM7UUFlaEUsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzNCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxxQkFBZ0IsR0FBRyxRQUFRLENBQUM7UUFDNUIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUV0QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUV4QixpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQVZkLENBQUM7SUFhTCxpQkFBaUI7UUFDYixNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxTQUFTO1FBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsT0FBTztRQUNILE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3JELElBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUU7aUJBQ3JDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDdEIsa0VBQWtFO2dCQUNsRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO2dCQUM3RixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUV0QyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsTUFBTSxFQUFFLHNCQUFzQixDQUFDLENBQUMsQ0FBQztnQkFFNUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNwRyxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUMzRCxNQUFNLFFBQVEsR0FBRyxDQUFDLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNqRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNwRyxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUMzRCxNQUFNLFFBQVEsR0FBRyxDQUFDLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNqRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO2dCQUN2RCxNQUFNLE1BQU0sR0FBRyxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN6RixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzFHLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3pELE1BQU0sT0FBTyxHQUFHLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzdGLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMxQzs7O2lHQUdpRixDQUFDLFdBQVc7Z0JBQzdGLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ3ZDLE9BQU8sQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztvQkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNwQixPQUFPO2lCQUNWO2dCQUVELElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7b0JBQzlELElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwRixDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO29CQUNoSixJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ25GLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO29CQUNoSSxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDdkUsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsRUFBRTtvQkFDeEksSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDN0UsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRTtvQkFDbEosSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNwRixDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO29CQUN4SSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM3RSxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUFFLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7b0JBQzlELElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzdFLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLEVBQUU7b0JBQy9ELElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLENBQUM7Z0JBQ3BGLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7b0JBQ3ZELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNwRixDQUFDLENBQUMsQ0FBQztnQkFFSCxvRUFBb0U7Z0JBQ3BFLG9FQUFvRTtnQkFDcEUsTUFBTTtnQkFFTixJQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDO29CQUN0QyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRTtpQkFDckQsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDekQsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO29CQUMxRSxJQUFJLENBQUMsY0FBYyxDQUFDLHdCQUF3QixDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3pFLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ3ZDLE9BQU8sQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztvQkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNwQixPQUFPO2lCQUNWO2dCQUNELEtBQUssTUFBTSxJQUFJLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtvQkFDbEMsS0FBSyxNQUFNLE9BQU8sSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7d0JBQzVDLElBQUksT0FBTyxDQUFDLGdCQUFnQjs0QkFDeEIsT0FBTyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDOzRCQUM1QyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxFQUFFOzRCQUMvQyxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFOzRCQUMxRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDL0I7cUJBQ0o7aUJBQ0o7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzFCLElBQUksZUFBZSxDQUFDO29CQUNwQixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLHdCQUF3QixDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3hGLElBQUksS0FBSyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7d0JBQ3ZCLEtBQUssTUFBTSxPQUFPLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs0QkFDakMsSUFBSSxPQUFPLENBQUMsY0FBYyxLQUFLLEtBQUssRUFBRTtnQ0FDbEMsZUFBZSxHQUFHLE9BQU8sQ0FBQztnQ0FDMUIsTUFBTTs2QkFDVDt5QkFDSjtxQkFDSjt5QkFBTTt3QkFDSCxlQUFlLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDdEM7b0JBQ0QsSUFBSSxDQUFDLGVBQWUsRUFBRTt3QkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7d0JBQzlDLE9BQU87cUJBQ1Y7b0JBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDO3lCQUM1QyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTs2QkFDL0IsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFOzRCQUN6QixNQUFNLGNBQWMsR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsTUFBTSxFQUFFLHdCQUF3QixDQUFDLENBQUM7NEJBQ2hHLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUN0RSxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQ0FDaEgsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDOzZCQUN2QjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDOzZCQUM5Qjt3QkFDTCxDQUFDLEVBQUUsWUFBWSxDQUFDLEVBQUU7NEJBQ2QsUUFBUSxDQUFDLEtBQUssQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO3dCQUNyRixDQUFDLENBQUMsQ0FBQztvQkFDWCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7d0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO29CQUM5RSxDQUFDLENBQUMsQ0FBQztpQkFDVjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsQ0FBQztvQkFDOUMsT0FBTztpQkFDVjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7WUFDOUUsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNuRCxJQUFJLE9BQU8sS0FBSyxJQUFJLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFO3lCQUMxQixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ2xCLElBQUksUUFBUSxFQUFFOzRCQUNWLElBQUksQ0FBQyxPQUFPLEVBQUU7aUNBQ1QsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dDQUN6QixJQUFJLGVBQWUsSUFBSSxJQUFJLEVBQUU7b0NBQ3pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0NBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDdkI7cUNBQU0sSUFBSSxlQUFlLElBQUksVUFBVSxFQUFFO29DQUN0QyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29DQUMvQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUNBQ3ZCO3FDQUFNO29DQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0NBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDdkI7NEJBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dDQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLElBQUksS0FBSyxJQUFJLFVBQVUsRUFBRTtvQ0FDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQ0FDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUN2QjtnQ0FDRCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0NBQ2pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs0QkFDeEIsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ25CLElBQUksS0FBSyxJQUFJLFVBQVUsRUFBRTs0QkFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUN2Qjt3QkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2lCQUNWO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQyxDQUFDO2lCQUMzRTtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0I7UUFDZCxNQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDYixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDdkQ7YUFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxVQUFVLEtBQUssTUFBTSxDQUFDLGdCQUFnQixFQUFFLEVBQUU7WUFDaEcsTUFBTSxXQUFXLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDakYsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQy9EO1NBQ0o7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUNQLElBQTRCLEVBQzVCLEtBQTBCO1FBRTFCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0JBQzdDLElBQUksaUJBQWlCLElBQUksSUFBSSxFQUFFO29CQUMzQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztvQkFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUM3RCxJQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFOzZCQUNsQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTs0QkFDNUIsNENBQTRDOzRCQUM1QyxJQUFJLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQyx1QkFBdUIsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsS0FBSyx1QkFBdUIsQ0FBQyxDQUFDOzRCQUM5SSxJQUFJLGNBQWMsSUFBSSxjQUFjLENBQUMsS0FBSyxFQUFFO2dDQUN4QyxrQ0FBa0M7Z0NBQ2xDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQzs2QkFDN0U7aUNBQU07Z0NBQ0gsY0FBYyxHQUFHLENBQUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsY0FBYyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0NBQy9GLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsY0FBYyxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUM7b0NBQzVFLElBQUksQ0FBQyxXQUFXLENBQUMsaUNBQWlDLENBQUM7NkJBQzFEOzRCQUNELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxFQUFFLGNBQWMsQ0FBQztpQ0FDbEQsSUFBSSxDQUNELFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsZUFBZTs2QkFDNUU7aUNBQ0EsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7Z0NBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQ0FDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDOzRCQUNoRCxDQUFDLEVBQUUsZ0JBQWdCLENBQUMsRUFBRTtnQ0FDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dDQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7Z0NBQ2xFLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLENBQUM7NEJBQ3RDLENBQUMsQ0FBQyxDQUFDO3dCQUNYLENBQUMsRUFBRSxlQUFlLENBQUMsRUFBRTs0QkFDakIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7d0JBQzdFLENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTt3QkFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ3BCLE9BQU8sQ0FBQyxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztvQkFDckUsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU0sSUFBSSxpQkFBaUIsSUFBSSxVQUFVLEVBQUU7b0JBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO29CQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKLENBQUE7O1lBdlRtQyxtQkFBbUI7WUFDMUIsV0FBVztZQUNGLHFCQUFxQjtZQUM3QixhQUFhO1lBQ2YsV0FBVztZQUNQLGVBQWU7WUFDcEIsVUFBVTtZQUNOLGNBQWM7WUFDWCxpQkFBaUI7WUFDNUIsTUFBTTs7O0FBZGhCLFNBQVM7SUFKckIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLFNBQVMsQ0E0VHJCO1NBNVRZLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGltZXIgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgT1RNTVNlc3Npb25IYW5kbGVyIH0gZnJvbSAnLi4vb3RtbVNlc3Npb25IYW5kbGVyJztcclxuaW1wb3J0IHsgQXBwUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX01FTlVfVFlQRVMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NZW51VHlwZXMnO1xyXG5pbXBvcnQgeyBNUE1NZW51IH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTWVudSc7XHJcbmltcG9ydCB7IE9URFNUaWNrZXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL09URFNUaWNrZXQuc2VydmljZSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCB7XHJcblxyXG4gICAgcHVibGljIG90bW1TZXNzaW9uU3Vic2NyaXB0aW9uID0gT1RNTVNlc3Npb25IYW5kbGVyLnN1YnNjcmliZSgpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZXM6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBSb3V0ZVNlcnZpY2U6IEFwcFJvdXRlU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdGRzVGlja2V0U2VydmljZTogT1REU1RpY2tldFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGlzRGF0YUFscmFkeUxvYWRlZCA9IGZhbHNlO1xyXG4gICAgYXBwSXRlbXMgPSBbXTtcclxuICAgIE1FTlVfSEVBREVSX1RZUEUgPSAnSEVBREVSJztcclxuICAgIE1FTlVfVFlQRSA9ICdHRU5FUkFMJztcclxuICAgIG1lbnVzOiBBcnJheTxNUE1NZW51PjtcclxuICAgIGljb25Ub0FjY2Vzc01QTSA9IGZhbHNlO1xyXG4gICAgYWxsQXBwbGljYXRpb25JdGVtcztcclxuICAgIHJvbGVzRm9yVXNlciA9IFtdO1xyXG4gICAgQVBQX0RFRl9QQVRIID0gJyc7XHJcbiAgICB1c2VyTmFtZTtcclxuXHJcbiAgICBjbGVhckxvY2FsU3RvcmFnZSgpIHtcclxuICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ01QTV9WM19MQVNUX0xJTksnKTtcclxuICAgIH1cclxuXHJcbiAgICBnb1RvTG9naW4oKSB7XHJcbiAgICAgICAgdGhpcy5hcHBSb3V0ZVNlcnZpY2UuZ29Ub0xvZ2luKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZEFwcCgpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnJvbGVzRm9yVXNlciA9IHRoaXMudXRpbFNlcnZpY2VzLmdldFVzZXJSb2xlcygpO1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25EZXRhaWxzKClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAvKiBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdBTExfQVBQTElDQVRJT05fREVUQUlMUycsIHJlc3BvbnNlKTsgKi9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX0FwcGxpY2F0aW9uX0RlZmluaXRpb24nKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldENhbXBhaWduUm9sZShhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQ2FtcGFpZ24gTWFuYWdlcicpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZXZpZXdlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIFJldmlld2VyJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UmV2aWV3ZXJSb2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmV2aWV3ZXIgPSAocmV2aWV3ZXJSb2xlICYmIHJldmlld2VyUm9sZS5sZW5ndGggJiYgcmV2aWV3ZXJSb2xlLmxlbmd0aCA+IDApID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0SXNSZXZpZXdlcihyZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBcHByb3ZlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIEFwcHJvdmVyJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFwcHJvdmVyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwcm92ZXJSb2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXIgPSAoYXBwcm92ZXJSb2xlICYmIGFwcHJvdmVyUm9sZS5sZW5ndGggJiYgYXBwcm92ZXJSb2xlLmxlbmd0aCA+IDApID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0SXNBcHByb3ZlcihhcHByb3Zlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRNZW1iZXJSb2xlKGFjcm9udWkuZmluZE9iamVjdHModGhpcy5yb2xlc0ZvclVzZXIsICdOYW1lJywgJ01QTSBNZW1iZXInKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVtYmVyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVtYmVyUm9sZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IChtZW1iZXJSb2xlICYmIG1lbWJlclJvbGUubGVuZ3RoICYmIG1lbWJlclJvbGUubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRJc01lbWJlcihtZW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0TWFuYWdlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIFByb2plY3QgTWFuYWdlcicpKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtYW5hZ2VyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWFuYWdlclJvbGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtYW5hZ2VyID0gKG1hbmFnZXJSb2xlICYmIG1hbmFnZXJSb2xlLmxlbmd0aCAmJiBtYW5hZ2VyUm9sZS5sZW5ndGggPiAwKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldElzTWFuYWdlcihtYW5hZ2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAvKiAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q2FtcGFpZ25NYW5hZ2VyUm9sZShhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQ2FtcGFpZ24gTWFuYWdlcicpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduTWFuYWdlclJvbGUgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhbXBhaWduTWFuYWdlclJvbGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduTWFuYWdlciA9IChjYW1wYWlnbk1hbmFnZXJSb2xlICYmIGNhbXBhaWduTWFuYWdlclJvbGUubGVuZ3RoICYmIGNhbXBhaWduTWFuYWdlclJvbGUubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldElzQ2FtcGFpZ25NYW5hZ2VyKGNhbXBhaWduTWFuYWdlcik7ICovIC8vIE1WU1MtMTI4XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYWxsQXBwbGljYXRpb25JdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignTm8gYXBwbGljYXRpb24gY29uZmlndXJhdGlvbiBhdmFpbGFibGUuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJ05PQUNDRVNTJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXREZWZhdWx0RW1haWxDb25maWcoKS5zdWJzY3JpYmUoZW1haWxSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0RGVmYXVsdEVtYWlsQ29uZmlnKGVtYWlsUmVzcG9uc2UgPyBlbWFpbFJlc3BvbnNlIDogbnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQcm9qZWN0Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19QUk9KRUNUX0NPTkZJR1snTVBNX1Byb2plY3RfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShwcm9qZWN0Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFByb2plY3RDb25maWcocHJvamVjdENvbmZpZ1Jlc3BvbnNlLk1QTV9Qcm9qZWN0X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRBcHBDb25maWdCeUlkKHRoaXMuYWxsQXBwbGljYXRpb25JdGVtc1swXS5SX1BPX0FQUF9DT05GSUdbJ01QTV9BcHBfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShhcHBDb25maWdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QXBwQ29uZmlnKGFwcENvbmZpZ1Jlc3BvbnNlLk1QTV9BcHBfQ29uZmlnKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEFzc2V0Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19BU1NFVF9DT05GSUdbJ01QTV9Bc3NldF9Db25maWctaWQnXS5JZCkuc3Vic2NyaWJlKGFzc2V0Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFzc2V0Q29uZmlnKGFzc2V0Q29uZmlnUmVzcG9uc2UuTVBNX0Fzc2V0X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRDb21tZW50Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19DT01NRU5UU19DT05GSUdbJ01QTV9Db21tZW50c19Db25maWctaWQnXS5JZCkuc3Vic2NyaWJlKGNvbW1lbnRDb25maWdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q29tbWVudENvbmZpZyhjb21tZW50Q29uZmlnUmVzcG9uc2UuTVBNX0NvbW1lbnRzX0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRCcmFuZENvbmZpZ0J5SWQodGhpcy5hbGxBcHBsaWNhdGlvbkl0ZW1zWzBdLlJfUE9fQlJBTkRfQ09ORklHWydNUE1fQnJhbmRfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShicmFuZENvbmZpZ1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRCcmFuZENvbmZpZyhicmFuZENvbmZpZ1Jlc3BvbnNlLk1QTV9CcmFuZF9Db25maWcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVmlld0NvbmZpZygpLnN1YnNjcmliZSh2aWV3Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFZpZXdDb25maWcodmlld0NvbmZpZ1Jlc3BvbnNlLk1QTV9WaWV3X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKS5zdWJzY3JpYmUoYWxsVGVhbVJvbGVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFRlYW1Sb2xlcyhhbGxUZWFtUm9sZXNSZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVGVhbXMoKS5zdWJzY3JpYmUoYWxsVGVhbXNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsVGVhbXMoYWxsVGVhbXNSZXNwb25zZS5HZXRBbGxUZWFtc1Jlc3BvbnNlLk1QTV9UZWFtcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuYXBwU2VydmljZS5nZXRBbGxQZXJzb25zKCkuc3Vic2NyaWJlKGFsbFBlcnNvbnNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsUGVyc29ucyhhbGxQZXJzb25zUmVzcG9uc2UuUGVyc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFBlcnNvbihwZXJzb25SZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRDdXJyZW50VXNlclByZWZlcmVuY2UoKS5zdWJzY3JpYmUodXNlclByZWZlcmVuY2VSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlKHVzZXJQcmVmZXJlbmNlUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5hbGxBcHBsaWNhdGlvbkl0ZW1zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdObyBhcHBsaWNhdGlvbiBjb25maWd1cmF0aW9uIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCByb2xlIG9mIHRoaXMucm9sZXNGb3JVc2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgYXBwSXRlbSBvZiB0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHBJdGVtLlJfUE9fQUNDRVNTX1JPTEUgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBJdGVtLlJfUE9fQUNDRVNTX1JPTEVbJ01QTV9BUFBfUm9sZXMtaWQnXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcEl0ZW0uUl9QT19BQ0NFU1NfUk9MRVsnTVBNX0FQUF9Sb2xlcy1pZCddLklkICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZS5JZCA9PT0gdGhpcy51dGlsU2VydmljZXMuR2V0Um9sZUROQnlSb2xlSWQoYXBwSXRlbS5SX1BPX0FDQ0VTU19ST0xFWydNUE1fQVBQX1JvbGVzLWlkJ10uSWQpLlJPTEVfRE4pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcEl0ZW1zLnB1c2goYXBwSXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYXBwSXRlbXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRBcHBJdGVtO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBJZCA9IHRoaXMudXRpbFNlcnZpY2VzLmdldFF1ZXJ5UGFyYW1ldGVyRnJvbVVSTCgnYXBwbGljYXRpb25JZCcsIGRvY3VtZW50LlVSTCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHBJZCAmJiBhcHBJZCAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgYXBwSXRlbSBvZiB0aGlzLmFwcEl0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcEl0ZW0uQVBQTElDQVRJT05fSUQgPT09IGFwcElkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQXBwSXRlbSA9IGFwcEl0ZW07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQXBwSXRlbSA9IHRoaXMuYXBwSXRlbXNbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzZWxlY3RlZEFwcEl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gYWNjZXNzIHRvIHRoZSBhcHBsaWNhdGlvbi4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZXMuc2V0U2VsZWN0ZWRBcHAoc2VsZWN0ZWRBcHBJdGVtKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlcy5zZXRDb25maWdzRm9yQXBwKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjb25maWdzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlclJvbGVXaXRoQ04gPSBhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQXBwbGljYXRpb24gQWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZW51QnlUeXBlKE1QTV9NRU5VX1RZUEVTLkhFQURFUik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlclJvbGVXaXRoQ04ubGVuZ3RoID09PSAxICYmIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5oYXNSb2xlKHVzZXJSb2xlV2l0aENOWzBdLklkKSAmJiB0aGlzLm1lbnVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdOT0FDQ0VTUycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBjb25maWdzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgYXBwbGljYXRpb24gY29uZmlnIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBzZXR0aW5nIGFwcGxpY2F0aW9uIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdObyBBY2Nlc3MgdG8gdGhlIEFwcGxpY2F0aW9uLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIGFwcGxpY2F0aW9uIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0TG9nZ2luZygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5sb2dJbigpLnN1YnNjcmliZShzdWNjZXNzID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChzdWNjZXNzID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZXMuc2V0VXNlckluZm8oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZEFwcCgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobG9hZEFwcFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChsb2FkQXBwUmVzcG9uc2UgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobG9hZEFwcFJlc3BvbnNlID09ICdOT0FDQ0VTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGxvYWRBcHBSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnJvciA9PSAnTk9BQ0NFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnJvciA9PSAnTk9BQ0NFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgYXV0aGVudGljYXRpbmcuJykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlTGFzdFZpc2l0TGluaygpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgaWYgKCFjdXJyZW50VXJsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnTVBNX1YzX0xBU1RfTElOSycsICcnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGN1cnJlbnRVcmwgIT09IGNvbmZpZy5nZXRPVERTTG9naW5QYWdlVXJsKCkgJiYgY3VycmVudFVybCAhPT0gY29uZmlnLmdldFBzT3JnRW5kcG9pbnQoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBzcGxpdHRlZFVSTCA9IGN1cnJlbnRVcmwuc3BsaXQoJy8jLycpO1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzcGxpdHRlZFVSTCkgJiYgc3BsaXR0ZWRVUkwubGVuZ3RoID09PSAyICYmIHNwbGl0dGVkVVJMWzFdICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdNUE1fVjNfTEFTVF9MSU5LJywgY3VycmVudFVybCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGUoXHJcbiAgICAgICAgbmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdExvZ2dpbmcoKS5zdWJzY3JpYmUodmFsaWRhdGVkUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbGlkYXRlZFJlc3BvbnNlID09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyTG9jYWxTdG9yYWdlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdGRzVGlja2V0U2VydmljZS5zZXRPVERTVGlja2V0Rm9yVXNlcigpLnN1YnNjcmliZSh0aWNrZXQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldE9UTU1TeXN0ZW1EZXRhaWxzKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3lzRGV0YWlsc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZXNzaW9uVGltZU91dCA9IHN5c0RldGFpbHNSZXNwb25zZS5zeXN0ZW1fZGV0YWlsc19yZXNvdXJjZS5zeXN0ZW1fZGV0YWlsc19tYXAuZW50cnkuZmluZChlbnRyeSA9PiBlbnRyeS5rZXkgPT09ICdIVFRQX1NFU1NJT05fVElNRV9PVVQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2Vzc2lvblRpbWVPdXQgJiYgc2Vzc2lvblRpbWVPdXQudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gNzUlIG9mIHRoZSBIVFRQIFNlc3Npb24gdGltZW91dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uVGltZU91dCA9IE1hdGguZmxvb3IoKE51bWJlcihzZXNzaW9uVGltZU91dC52YWx1ZSkgKiAwLjc1KSAqIDEwMDApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25UaW1lT3V0ID0gKHR5cGVvZiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnNlc3Npb25UaW1lT3V0ID09PSAnc3RyaW5nJykgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTnVtYmVyKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuc2Vzc2lvblRpbWVPdXQpICogNjAwMDAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5TVEFOREFSRF9TRVNTSU9OX1RJTUVPVVRfSU5URVJWQUw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlc3Npb25TdWJzY3JpcHRpb24gPSB0aW1lcigwLCBzZXNzaW9uVGltZU91dClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2hNYXAoKCkgPT4gdGhpcy5vdG1tU2VydmljZS5zdGFydE9UTU1TZXNzaW9uKGZhbHNlKSkgLy8gTVZTLTIzOWZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShvdG1tU2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdPVE1NIFNlc3Npb24gaXMgaW5pdGlhbGl6ZWQuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIG90bW1TZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBPVE1NIFNlc3Npb24uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFJvdXRlU2VydmljZS5nb1RvTG9nb3V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgc3lzRGV0YWlsc0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIE9UTU0gc3lzdGVtIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgT1REUyB0aWNrZXQuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZhbGlkYXRlZFJlc3BvbnNlID09ICdOT0FDQ0VTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2FwcHMvbXBtL25vYWNjZXNzJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdG9yZUxhc3RWaXNpdExpbmsoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdvVG9Mb2dpbigpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=