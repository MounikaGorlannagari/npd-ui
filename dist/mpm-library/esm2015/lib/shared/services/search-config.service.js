import { __decorate } from "tslib";
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import { FieldConfigService } from './field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/sharing.service";
import * as i3 from "./field-config.service";
import * as i4 from "../../notification/notification.service";
let SearchConfigService = class SearchConfigService {
    constructor(appService, sharingService, fieldConfigService, notificationService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.fieldConfigService = fieldConfigService;
        this.notificationService = notificationService;
        this.SEARCH_CONFIG_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.SEARCH_CONFIG_WS = 'GetSearchFieldConfig';
        this.GET_PM_SEARCH_FIELDS_WS = 'GetR_PM_SEARCH_FIELDS';
        this.GET_PM_SEARCH_FIELDS_NS = 'http://schemas/AcheronMPMCore/MPM_Advanced_Search_Config/operations';
        this.GetAllAdvancedSearchOperatorsWS = 'GetAllAdvancedSearchOperators';
        this.GetAllAdvancedSearchOperatorsNS = 'http://schemas/AcheronMPMCore/MPM_Advanced_Search_Operators/operations';
        this.GetSavedSearchesWS = 'GetSavedSearches';
        this.GetSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.GetSavedSearchByIdWS = 'GetSavedSearchById';
        this.GetSavedSearchByIdNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
    }
    getCastedAdvancedSearchConfigResponse(searchConfig) {
        const advConfigs = [];
        if (Array.isArray(searchConfig) && searchConfig.length) {
            searchConfig.forEach(config => {
                const advConfig = {
                    'MPM_Advanced_Search_Config-id': {
                        Id: config.id,
                    },
                    facetConfigId: config.facetConfigId,
                    SEARCH_NAME: config.searchName,
                    SEARCH_SCOPE: config.searchScope,
                    R_PM_Search_Fields: []
                };
                const serachFields = acronui.findObjectsByProp(config, 'field');
                if (Array.isArray(serachFields) && serachFields.length) {
                    serachFields.forEach(field => {
                        if (field && field.id) {
                            const matchField = this.fieldConfigService.getFieldByKeyValue('Id', field.id);
                            if (matchField) {
                                advConfig.R_PM_Search_Fields.push(matchField);
                            }
                        }
                    });
                }
                advConfigs.push(advConfig);
            });
        }
        return advConfigs;
    }
    setAllAdvancedSearchConfig() {
        return new Observable(observer => {
            if (Array.isArray(this.allAdvancedSearchConfig) && this.allAdvancedSearchConfig.length) {
                observer.next(this.allAdvancedSearchConfig);
                observer.complete();
                return;
            }
            this.appService.invokeRequest(this.SEARCH_CONFIG_NS, this.SEARCH_CONFIG_WS, null).subscribe(response => {
                const searchConfig = acronui.findObjectsByProp(response, 'AdvancedSearchConfig');
                this.allAdvancedSearchConfig = this.getCastedAdvancedSearchConfigResponse(searchConfig);
                observer.next(this.allAdvancedSearchConfig);
                observer.complete();
            }, err => {
                this.notificationService.error('Something went wrong while getting search config');
                observer.error();
                observer.complete();
            });
        });
    }
    getAdvancedSearchConfigById(advancedSearchId) {
        return this.allAdvancedSearchConfig.find(searchConfig => {
            return searchConfig['MPM_Advanced_Search_Config-id'].Id === advancedSearchId;
        });
    }
    getAllAdvancedSearchConfigFields(advancedSearchId) {
        const advSearch = this.getAdvancedSearchConfigById(advancedSearchId);
        if (advSearch && Array.isArray(advSearch.R_PM_Search_Fields)) {
            return advSearch.R_PM_Search_Fields;
        }
        return [];
    }
    setAllSearchOperators() {
        return new Observable(observer => {
            if (Array.isArray(this.allSearchOperators) && this.allSearchOperators.length) {
                observer.next(this.allSearchOperators);
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.GetAllAdvancedSearchOperatorsNS, this.GetAllAdvancedSearchOperatorsWS, null).subscribe(response => {
                    const searchOperators = acronui.findObjectsByProp(response, 'MPM_Advanced_Search_Operators');
                    searchOperators.forEach(operator => {
                        let fieldValueCount = 0;
                        try {
                            fieldValueCount = parseInt(operator.VALUE_COUNT, 0);
                        }
                        catch (e) {
                            console.warn('no value for parsing value count for the operator config');
                        }
                        operator.VALUE_COUNT = fieldValueCount;
                    });
                    this.allSearchOperators = searchOperators;
                    observer.next(searchOperators);
                    observer.complete();
                }, error => {
                    this.notificationService.error('Something went wrong while getting search operators');
                    observer.error();
                    observer.complete();
                });
            }
        });
    }
    getSavedSearchesForViewAndCurrentUser(viewName) {
        return new Observable(observer => {
            const param = {
                userId: this.sharingService.getcurrentUserItemID(),
                view: viewName
            };
            this.appService.invokeRequest(this.GetSavedSearchesNS, this.GetSavedSearchesWS, param).subscribe(savedSearchResposne => {
                const savedSearches = acronui.findObjectsByProp(savedSearchResposne, 'MPM_Saved_Searches');
                savedSearches.forEach(search => {
                    if (search.SEARCH_DATA && typeof search.SEARCH_DATA === 'string') {
                        search.SEARCH_DATA = JSON.parse(search.SEARCH_DATA);
                    }
                    else {
                        search.SEARCH_DATA = '';
                    }
                });
                observer.next(savedSearches);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
    getSavedSearcheById(savedSearchId) {
        return new Observable(observer => {
            const param = {
                savedSearchId
            };
            this.appService.invokeRequest(this.GetSavedSearchByIdNS, this.GetSavedSearchByIdWS, param).subscribe(searchRes => {
                const savedSearches = acronui.findObjectsByProp(searchRes, 'MPM_Saved_Searches');
                let savedSearch = null;
                if (savedSearches && savedSearches[0]) {
                    savedSearch = savedSearches[0];
                    if (savedSearch.SEARCH_DATA && typeof savedSearch.SEARCH_DATA === 'string') {
                        savedSearch.SEARCH_DATA = JSON.parse(savedSearch.SEARCH_DATA);
                    }
                    else {
                        savedSearch.SEARCH_DATA = '';
                    }
                }
                observer.next(savedSearch);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
    getAllSearchOperators() {
        return this.allSearchOperators;
    }
};
SearchConfigService.ctorParameters = () => [
    { type: AppService },
    { type: SharingService },
    { type: FieldConfigService },
    { type: NotificationService }
];
SearchConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchConfigService_Factory() { return new SearchConfigService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.FieldConfigService), i0.ɵɵinject(i4.NotificationService)); }, token: SearchConfigService, providedIn: "root" });
SearchConfigService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SearchConfigService);
export { SearchConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC1jb25maWcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7Ozs7O0FBTzFFLElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBRTlCLFlBQ1MsVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsa0JBQXNDLEVBQ3RDLG1CQUF3QztRQUh4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFHakQscUJBQWdCLEdBQUcsK0NBQStDLENBQUM7UUFDbkUscUJBQWdCLEdBQUcsc0JBQXNCLENBQUM7UUFFMUMsNEJBQXVCLEdBQUcsdUJBQXVCLENBQUM7UUFDbEQsNEJBQXVCLEdBQUcscUVBQXFFLENBQUM7UUFFaEcsb0NBQStCLEdBQUcsK0JBQStCLENBQUM7UUFDbEUsb0NBQStCLEdBQUcsd0VBQXdFLENBQUM7UUFFM0csdUJBQWtCLEdBQUcsa0JBQWtCLENBQUM7UUFDeEMsdUJBQWtCLEdBQUcsNkRBQTZELENBQUM7UUFFbkYseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMseUJBQW9CLEdBQUcsNkRBQTZELENBQUM7SUFmakYsQ0FBQztJQW9CTCxxQ0FBcUMsQ0FBQyxZQUFtQjtRQUN2RCxNQUFNLFVBQVUsR0FBOEIsRUFBRSxDQUFDO1FBQ2pELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQ3RELFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzVCLE1BQU0sU0FBUyxHQUE0QjtvQkFDekMsK0JBQStCLEVBQUU7d0JBQy9CLEVBQUUsRUFBRSxNQUFNLENBQUMsRUFBRTtxQkFDZDtvQkFDRCxhQUFhLEVBQUUsTUFBTSxDQUFDLGFBQWE7b0JBQ25DLFdBQVcsRUFBRSxNQUFNLENBQUMsVUFBVTtvQkFDOUIsWUFBWSxFQUFFLE1BQU0sQ0FBQyxXQUFXO29CQUNoQyxrQkFBa0IsRUFBRSxFQUFFO2lCQUN2QixDQUFDO2dCQUNGLE1BQU0sWUFBWSxHQUFVLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO29CQUN0RCxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUMzQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsRUFBRSxFQUFFOzRCQUNyQixNQUFNLFVBQVUsR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDeEYsSUFBSSxVQUFVLEVBQUU7Z0NBQ2QsU0FBUyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs2QkFDL0M7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7Z0JBQ0QsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3QixDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVELDBCQUEwQjtRQUN4QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFO2dCQUN0RixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2dCQUM1QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNyRyxNQUFNLFlBQVksR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3hGLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMscUNBQXFDLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3hGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO2dCQUNuRixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJCQUEyQixDQUFDLGdCQUF3QjtRQUNsRCxPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDdEQsT0FBTyxZQUFZLENBQUMsK0JBQStCLENBQUMsQ0FBQyxFQUFFLEtBQUssZ0JBQWdCLENBQUM7UUFDL0UsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0NBQWdDLENBQUMsZ0JBQXdCO1FBQ3ZELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3JFLElBQUksU0FBUyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDNUQsT0FBTyxTQUFTLENBQUMsa0JBQWtCLENBQUM7U0FDckM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCxxQkFBcUI7UUFDbkIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRTtnQkFDNUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDdkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3JCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNuSSxNQUFNLGVBQWUsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLCtCQUErQixDQUFDLENBQUM7b0JBQzdGLGVBQWUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ2pDLElBQUksZUFBZSxHQUFHLENBQUMsQ0FBQzt3QkFDeEIsSUFBSTs0QkFDRixlQUFlLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7eUJBQ3JEO3dCQUFDLE9BQU8sQ0FBQyxFQUFFOzRCQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsMERBQTBELENBQUMsQ0FBQzt5QkFBRTt3QkFDekYsUUFBUSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUM7b0JBQ3pDLENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxrQkFBa0IsR0FBRyxlQUFlLENBQUM7b0JBQzFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNULElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMscURBQXFELENBQUMsQ0FBQztvQkFDdEYsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQ0FBcUMsQ0FBQyxRQUFnQjtRQUNwRCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLE1BQU0sS0FBSyxHQUFHO2dCQUNaLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFO2dCQUNsRCxJQUFJLEVBQUUsUUFBUTthQUNmLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO2dCQUNySCxNQUFNLGFBQWEsR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDbEcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDN0IsSUFBSSxNQUFNLENBQUMsV0FBVyxJQUFJLE9BQU8sTUFBTSxDQUFDLFdBQVcsS0FBSyxRQUFRLEVBQUU7d0JBQ2hFLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQ3JEO3lCQUFNO3dCQUNMLE1BQU0sQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO3FCQUN6QjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNULFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsbUJBQW1CLENBQUMsYUFBcUI7UUFDdkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLEtBQUssR0FBRztnQkFDWixhQUFhO2FBQ2QsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUMvRyxNQUFNLGFBQWEsR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLG9CQUFvQixDQUFDLENBQUM7Z0JBQ3hGLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDdkIsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNyQyxXQUFXLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvQixJQUFJLFdBQVcsQ0FBQyxXQUFXLElBQUksT0FBTyxXQUFXLENBQUMsV0FBVyxLQUFLLFFBQVEsRUFBRTt3QkFDMUUsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDL0Q7eUJBQU07d0JBQ0wsV0FBVyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7cUJBQzlCO2lCQUNGO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQkFBcUI7UUFDbkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDakMsQ0FBQztDQUVGLENBQUE7O1lBeEtzQixVQUFVO1lBQ04sY0FBYztZQUNWLGtCQUFrQjtZQUNqQixtQkFBbUI7OztBQU50QyxtQkFBbUI7SUFIL0IsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLG1CQUFtQixDQTJLL0I7U0EzS1ksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUFkdmFuY2VkU2VhcmNoQ29uZmlnJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvciB9IGZyb20gJy4vb2JqZWN0cy9NUE1TZWFyY2hPcGVyYXRvcic7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4vZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1TYXZlZFNlYXJjaCB9IGZyb20gJy4vb2JqZWN0cy9NUE1TYXZlZFNlYXJjaCc7XHJcbmltcG9ydCB7IE1QTURlbGV0ZVNhdmVkU2VhcmNoUmVxIH0gZnJvbSAnLi9vYmplY3RzL01QTURlbGV0ZVNhdmVkU2VhcmNoUmVxJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlYXJjaENvbmZpZ1NlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICkgeyB9XHJcblxyXG4gIFNFQVJDSF9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICBTRUFSQ0hfQ09ORklHX1dTID0gJ0dldFNlYXJjaEZpZWxkQ29uZmlnJztcclxuXHJcbiAgR0VUX1BNX1NFQVJDSF9GSUVMRFNfV1MgPSAnR2V0Ul9QTV9TRUFSQ0hfRklFTERTJztcclxuICBHRVRfUE1fU0VBUkNIX0ZJRUxEU19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy9vcGVyYXRpb25zJztcclxuXHJcbiAgR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNXUyA9ICdHZXRBbGxBZHZhbmNlZFNlYXJjaE9wZXJhdG9ycyc7XHJcbiAgR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQWR2YW5jZWRfU2VhcmNoX09wZXJhdG9ycy9vcGVyYXRpb25zJztcclxuXHJcbiAgR2V0U2F2ZWRTZWFyY2hlc1dTID0gJ0dldFNhdmVkU2VhcmNoZXMnO1xyXG4gIEdldFNhdmVkU2VhcmNoZXNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU2F2ZWRfU2VhcmNoZXMvb3BlcmF0aW9ucyc7XHJcblxyXG4gIEdldFNhdmVkU2VhcmNoQnlJZFdTID0gJ0dldFNhdmVkU2VhcmNoQnlJZCc7XHJcbiAgR2V0U2F2ZWRTZWFyY2hCeUlkTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1NhdmVkX1NlYXJjaGVzL29wZXJhdGlvbnMnO1xyXG5cclxuICBhbGxBZHZhbmNlZFNlYXJjaENvbmZpZzogTVBNQWR2YW5jZWRTZWFyY2hDb25maWdbXTtcclxuICBhbGxTZWFyY2hPcGVyYXRvcnM6IE1QTVNlYXJjaE9wZXJhdG9yW107XHJcblxyXG4gIGdldENhc3RlZEFkdmFuY2VkU2VhcmNoQ29uZmlnUmVzcG9uc2Uoc2VhcmNoQ29uZmlnOiBhbnlbXSk6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnW10ge1xyXG4gICAgY29uc3QgYWR2Q29uZmlnczogTVBNQWR2YW5jZWRTZWFyY2hDb25maWdbXSA9IFtdO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoc2VhcmNoQ29uZmlnKSAmJiBzZWFyY2hDb25maWcubGVuZ3RoKSB7XHJcbiAgICAgIHNlYXJjaENvbmZpZy5mb3JFYWNoKGNvbmZpZyA9PiB7XHJcbiAgICAgICAgY29uc3QgYWR2Q29uZmlnOiBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyA9IHtcclxuICAgICAgICAgICdNUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy1pZCc6IHtcclxuICAgICAgICAgICAgSWQ6IGNvbmZpZy5pZCxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBmYWNldENvbmZpZ0lkOiBjb25maWcuZmFjZXRDb25maWdJZCxcclxuICAgICAgICAgIFNFQVJDSF9OQU1FOiBjb25maWcuc2VhcmNoTmFtZSxcclxuICAgICAgICAgIFNFQVJDSF9TQ09QRTogY29uZmlnLnNlYXJjaFNjb3BlLFxyXG4gICAgICAgICAgUl9QTV9TZWFyY2hfRmllbGRzOiBbXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3Qgc2VyYWNoRmllbGRzOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoY29uZmlnLCAnZmllbGQnKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzZXJhY2hGaWVsZHMpICYmIHNlcmFjaEZpZWxkcy5sZW5ndGgpIHtcclxuICAgICAgICAgIHNlcmFjaEZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLmlkKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgbWF0Y2hGaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoJ0lkJywgZmllbGQuaWQpO1xyXG4gICAgICAgICAgICAgIGlmIChtYXRjaEZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBhZHZDb25maWcuUl9QTV9TZWFyY2hfRmllbGRzLnB1c2gobWF0Y2hGaWVsZCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYWR2Q29uZmlncy5wdXNoKGFkdkNvbmZpZyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFkdkNvbmZpZ3M7XHJcbiAgfVxyXG5cclxuICBzZXRBbGxBZHZhbmNlZFNlYXJjaENvbmZpZygpOiBPYnNlcnZhYmxlPEFycmF5PE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnPj4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5hbGxBZHZhbmNlZFNlYXJjaENvbmZpZykgJiYgdGhpcy5hbGxBZHZhbmNlZFNlYXJjaENvbmZpZy5sZW5ndGgpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsQWR2YW5jZWRTZWFyY2hDb25maWcpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU0VBUkNIX0NPTkZJR19OUywgdGhpcy5TRUFSQ0hfQ09ORklHX1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZzogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnQWR2YW5jZWRTZWFyY2hDb25maWcnKTtcclxuICAgICAgICB0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnID0gdGhpcy5nZXRDYXN0ZWRBZHZhbmNlZFNlYXJjaENvbmZpZ1Jlc3BvbnNlKHNlYXJjaENvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBzZWFyY2ggY29uZmlnJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeUlkKGFkdmFuY2VkU2VhcmNoSWQ6IHN0cmluZyk6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnIHtcclxuICAgIHJldHVybiB0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnLmZpbmQoc2VhcmNoQ29uZmlnID0+IHtcclxuICAgICAgcmV0dXJuIHNlYXJjaENvbmZpZ1snTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZCA9PT0gYWR2YW5jZWRTZWFyY2hJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWxsQWR2YW5jZWRTZWFyY2hDb25maWdGaWVsZHMoYWR2YW5jZWRTZWFyY2hJZDogc3RyaW5nKTogTVBNRmllbGRbXSB7XHJcbiAgICBjb25zdCBhZHZTZWFyY2ggPSB0aGlzLmdldEFkdmFuY2VkU2VhcmNoQ29uZmlnQnlJZChhZHZhbmNlZFNlYXJjaElkKTtcclxuICAgIGlmIChhZHZTZWFyY2ggJiYgQXJyYXkuaXNBcnJheShhZHZTZWFyY2guUl9QTV9TZWFyY2hfRmllbGRzKSkge1xyXG4gICAgICByZXR1cm4gYWR2U2VhcmNoLlJfUE1fU2VhcmNoX0ZpZWxkcztcclxuICAgIH1cclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIHNldEFsbFNlYXJjaE9wZXJhdG9ycygpOiBPYnNlcnZhYmxlPE1QTVNlYXJjaE9wZXJhdG9yW10+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzKSAmJiB0aGlzLmFsbFNlYXJjaE9wZXJhdG9ycy5sZW5ndGgpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNOUywgdGhpcy5HZXRBbGxBZHZhbmNlZFNlYXJjaE9wZXJhdG9yc1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgY29uc3Qgc2VhcmNoT3BlcmF0b3JzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9BZHZhbmNlZF9TZWFyY2hfT3BlcmF0b3JzJyk7XHJcbiAgICAgICAgICBzZWFyY2hPcGVyYXRvcnMuZm9yRWFjaChvcGVyYXRvciA9PiB7XHJcbiAgICAgICAgICAgIGxldCBmaWVsZFZhbHVlQ291bnQgPSAwO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgIGZpZWxkVmFsdWVDb3VudCA9IHBhcnNlSW50KG9wZXJhdG9yLlZBTFVFX0NPVU5ULCAwKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkgeyBjb25zb2xlLndhcm4oJ25vIHZhbHVlIGZvciBwYXJzaW5nIHZhbHVlIGNvdW50IGZvciB0aGUgb3BlcmF0b3IgY29uZmlnJyk7IH1cclxuICAgICAgICAgICAgb3BlcmF0b3IuVkFMVUVfQ09VTlQgPSBmaWVsZFZhbHVlQ291bnQ7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzID0gc2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChzZWFyY2hPcGVyYXRvcnMpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgc2VhcmNoIG9wZXJhdG9ycycpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2F2ZWRTZWFyY2hlc0ZvclZpZXdBbmRDdXJyZW50VXNlcih2aWV3TmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxNUE1TYXZlZFNlYXJjaFtdPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICB1c2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKSxcclxuICAgICAgICB2aWV3OiB2aWV3TmFtZVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdldFNhdmVkU2VhcmNoZXNOUywgdGhpcy5HZXRTYXZlZFNlYXJjaGVzV1MsIHBhcmFtKS5zdWJzY3JpYmUoc2F2ZWRTZWFyY2hSZXNwb3NuZSA9PiB7XHJcbiAgICAgICAgY29uc3Qgc2F2ZWRTZWFyY2hlczogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHNhdmVkU2VhcmNoUmVzcG9zbmUsICdNUE1fU2F2ZWRfU2VhcmNoZXMnKTtcclxuICAgICAgICBzYXZlZFNlYXJjaGVzLmZvckVhY2goc2VhcmNoID0+IHtcclxuICAgICAgICAgIGlmIChzZWFyY2guU0VBUkNIX0RBVEEgJiYgdHlwZW9mIHNlYXJjaC5TRUFSQ0hfREFUQSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgc2VhcmNoLlNFQVJDSF9EQVRBID0gSlNPTi5wYXJzZShzZWFyY2guU0VBUkNIX0RBVEEpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2VhcmNoLlNFQVJDSF9EQVRBID0gJyc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChzYXZlZFNlYXJjaGVzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2F2ZWRTZWFyY2hlQnlJZChzYXZlZFNlYXJjaElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE1QTVNhdmVkU2VhcmNoPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICBzYXZlZFNlYXJjaElkXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR2V0U2F2ZWRTZWFyY2hCeUlkTlMsIHRoaXMuR2V0U2F2ZWRTZWFyY2hCeUlkV1MsIHBhcmFtKS5zdWJzY3JpYmUoc2VhcmNoUmVzID0+IHtcclxuICAgICAgICBjb25zdCBzYXZlZFNlYXJjaGVzOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aoc2VhcmNoUmVzLCAnTVBNX1NhdmVkX1NlYXJjaGVzJyk7XHJcbiAgICAgICAgbGV0IHNhdmVkU2VhcmNoID0gbnVsbDtcclxuICAgICAgICBpZiAoc2F2ZWRTZWFyY2hlcyAmJiBzYXZlZFNlYXJjaGVzWzBdKSB7XHJcbiAgICAgICAgICBzYXZlZFNlYXJjaCA9IHNhdmVkU2VhcmNoZXNbMF07XHJcbiAgICAgICAgICBpZiAoc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEgJiYgdHlwZW9mIHNhdmVkU2VhcmNoLlNFQVJDSF9EQVRBID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzYXZlZFNlYXJjaC5TRUFSQ0hfREFUQSA9IEpTT04ucGFyc2Uoc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEgPSAnJztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChzYXZlZFNlYXJjaCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEFsbFNlYXJjaE9wZXJhdG9ycygpOiBNUE1TZWFyY2hPcGVyYXRvcltdIHtcclxuICAgIHJldHVybiB0aGlzLmFsbFNlYXJjaE9wZXJhdG9ycztcclxuICB9XHJcblxyXG59XHJcbiJdfQ==