import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { Observable } from 'rxjs';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../notification/notification.service";
let EmailDataService = class EmailDataService {
    constructor(appService, notificationService) {
        this.appService = appService;
        this.notificationService = notificationService;
        this.EmailSearchDataMapNS = 'http://schemas/AcheronMPMCore/Email_Search_Data_Map/operations';
        this.EmailSearchDataMapWS = 'GetEmailSearchDataMapById';
    }
    getEmailSearchDataByGuid(guid) {
        return new Observable(observer => {
            if (!guid) {
                observer.error(new Error('Manadatory parameter GUID not passed'));
                observer.complete();
            }
            const param = {
                guid
            };
            this.appService.invokeRequest(this.EmailSearchDataMapNS, this.EmailSearchDataMapWS, param).subscribe(data => {
                const emailSearchData = acronui.findObjectByProp(data, 'Email_Search_Data_Map');
                if (emailSearchData && emailSearchData.SEARCH_CONDITION) {
                    emailSearchData.SEARCH_CONDITION = JSON.parse(emailSearchData.SEARCH_CONDITION);
                }
                observer.next(emailSearchData);
                observer.complete();
            }, err => {
                this.notificationService.error('Error while getting email details');
                observer.error();
                observer.complete();
            });
        });
    }
};
EmailDataService.ctorParameters = () => [
    { type: AppService },
    { type: NotificationService }
];
EmailDataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function EmailDataService_Factory() { return new EmailDataService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.NotificationService)); }, token: EmailDataService, providedIn: "root" });
EmailDataService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], EmailDataService);
export { EmailDataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1haWwtZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2VtYWlsLWRhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDOzs7O0FBSzlFLElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBSzNCLFlBQ1MsVUFBc0IsRUFDdEIsbUJBQXdDO1FBRHhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUwxQyx5QkFBb0IsR0FBRyxnRUFBZ0UsQ0FBQztRQUN4Rix5QkFBb0IsR0FBRywyQkFBMkIsQ0FBQztJQUt0RCxDQUFDO0lBRUwsd0JBQXdCLENBQUMsSUFBWTtRQUNuQyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1QsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtZQUNELE1BQU0sS0FBSyxHQUFHO2dCQUNaLElBQUk7YUFDTCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFHLE1BQU0sZUFBZSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFDaEYsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLGdCQUFnQixFQUFFO29CQUN2RCxlQUFlLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztpQkFDakY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtnQkFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7Z0JBQ3BFLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBRUYsQ0FBQTs7WUE1QnNCLFVBQVU7WUFDRCxtQkFBbUI7OztBQVB0QyxnQkFBZ0I7SUFINUIsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLGdCQUFnQixDQWtDNUI7U0FsQ1ksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBFbWFpbFNlYXJjaERhdGFNYXAgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9FbWFpbFNlYXJjaERhdGFNYXAnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVtYWlsRGF0YVNlcnZpY2Uge1xyXG5cclxuICBwdWJsaWMgRW1haWxTZWFyY2hEYXRhTWFwTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRW1haWxfU2VhcmNoX0RhdGFfTWFwL29wZXJhdGlvbnMnO1xyXG4gIHB1YmxpYyBFbWFpbFNlYXJjaERhdGFNYXBXUyA9ICdHZXRFbWFpbFNlYXJjaERhdGFNYXBCeUlkJztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgZ2V0RW1haWxTZWFyY2hEYXRhQnlHdWlkKGd1aWQ6IHN0cmluZyk6IE9ic2VydmFibGU8RW1haWxTZWFyY2hEYXRhTWFwPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAoIWd1aWQpIHtcclxuICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ01hbmFkYXRvcnkgcGFyYW1ldGVyIEdVSUQgbm90IHBhc3NlZCcpKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgIGd1aWRcclxuICAgICAgfTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5FbWFpbFNlYXJjaERhdGFNYXBOUywgdGhpcy5FbWFpbFNlYXJjaERhdGFNYXBXUywgcGFyYW0pLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICBjb25zdCBlbWFpbFNlYXJjaERhdGEgPSBhY3JvbnVpLmZpbmRPYmplY3RCeVByb3AoZGF0YSwgJ0VtYWlsX1NlYXJjaF9EYXRhX01hcCcpO1xyXG4gICAgICAgIGlmIChlbWFpbFNlYXJjaERhdGEgJiYgZW1haWxTZWFyY2hEYXRhLlNFQVJDSF9DT05ESVRJT04pIHtcclxuICAgICAgICAgIGVtYWlsU2VhcmNoRGF0YS5TRUFSQ0hfQ09ORElUSU9OID0gSlNPTi5wYXJzZShlbWFpbFNlYXJjaERhdGEuU0VBUkNIX0NPTkRJVElPTik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG9ic2VydmVyLm5leHQoZW1haWxTZWFyY2hEYXRhKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgZ2V0dGluZyBlbWFpbCBkZXRhaWxzJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbn1cclxuIl19