export var MPMSearchOperatorNames;
(function (MPMSearchOperatorNames) {
    MPMSearchOperatorNames["OR"] = "or";
    MPMSearchOperatorNames["AND"] = "and";
    MPMSearchOperatorNames["IS_NOT"] = "is not";
    MPMSearchOperatorNames["IS_NOT_EMPTY"] = "is not empty";
    MPMSearchOperatorNames["IS"] = "is";
    MPMSearchOperatorNames["IS_BETWEEN"] = "is between";
    MPMSearchOperatorNames["IS_ON_OR_BEFORE"] = "is on or before";
    MPMSearchOperatorNames["IS_ON_OR_AFTER"] = "is on or after";
})(MPMSearchOperatorNames || (MPMSearchOperatorNames = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE1BQU0sQ0FBTixJQUFZLHNCQVNYO0FBVEQsV0FBWSxzQkFBc0I7SUFDOUIsbUNBQVMsQ0FBQTtJQUNULHFDQUFXLENBQUE7SUFDWCwyQ0FBaUIsQ0FBQTtJQUNqQix1REFBNkIsQ0FBQTtJQUM3QixtQ0FBUyxDQUFBO0lBQ1QsbURBQXlCLENBQUE7SUFDekIsNkRBQW1DLENBQUE7SUFDbkMsMkRBQWlDLENBQUE7QUFDckMsQ0FBQyxFQVRXLHNCQUFzQixLQUF0QixzQkFBc0IsUUFTakMiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuZXhwb3J0IGVudW0gTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB7XHJcbiAgICBPUiA9ICdvcicsXHJcbiAgICBBTkQgPSAnYW5kJyxcclxuICAgIElTX05PVCA9ICdpcyBub3QnLFxyXG4gICAgSVNfTk9UX0VNUFRZID0gJ2lzIG5vdCBlbXB0eScsXHJcbiAgICBJUyA9ICdpcycsXHJcbiAgICBJU19CRVRXRUVOID0gJ2lzIGJldHdlZW4nLFxyXG4gICAgSVNfT05fT1JfQkVGT1JFID0gJ2lzIG9uIG9yIGJlZm9yZScsXHJcbiAgICBJU19PTl9PUl9BRlRFUiA9ICdpcyBvbiBvciBhZnRlcidcclxufVxyXG4iXX0=