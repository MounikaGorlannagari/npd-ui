import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IndexerConfigService } from './mpm-indexer.config.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { IndexerEndPoints } from './objects/IndexerEndPoints';
import { SessionStorageConstants } from '../../constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../../mpm-utils/services/sharing.service";
import * as i3 from "./mpm-indexer.config.service";
let IndexerService = class IndexerService {
    constructor(httpClient, sharingService, indexerConfigService) {
        this.httpClient = httpClient;
        this.sharingService = sharingService;
        this.indexerConfigService = indexerConfigService;
    }
    indexerEndPoint(path) {
        const indexerBaseDomain = isDevMode() ? { SEARCH_URL: '.' } : this.indexerConfigService.getIndexerConfigByCategoryId(this.sharingService.getSelectedCategory()['MPM_Category-id'].Id);
        if (indexerBaseDomain && indexerBaseDomain.SEARCH_URL) {
            return indexerBaseDomain.SEARCH_URL + IndexerEndPoints.base + path;
        }
        return IndexerEndPoints.base + path;
    }
    search(req) {
        return new Observable(observer => {
            if (req.search_config_id || true) {
                // MPMV3-2237
                console.log(!isDevMode());
                const httpOptions = {
                    headers: new HttpHeaders({
                        'SAMLart': !isDevMode() ? '' : localStorage.getItem('SAMLART'),
                        'OTDSSSO': JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET)) !== null ? JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET)) : '',
                        'currentUserId': !isDevMode() ? JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID)) : localStorage.getItem('OTDS_USER_DN') //JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_USER_DN))
                    })
                };
                //   this.httpClient.post(this.indexerEndPoint(IndexerEndPoints.search), req).subscribe((response: SearchResponse) => {
                // this.httpClient.post(this.indexerEndPoint(IndexerEndPoints.search), req, !isDevMode() ? httpOptions : undefined).subscribe((response: SearchResponse) => {
                // MPMV3-2237
                this.httpClient.post(this.indexerEndPoint(IndexerEndPoints.search), req, httpOptions).subscribe((response) => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(error);
                    observer.complete();
                });
            }
            else {
                observer.next({
                    data: [],
                    cursor: { total_records: 0 },
                    facet: null
                });
                observer.complete();
            }
        });
    }
    searchTemp(req) {
        return new Observable(observer => {
            this.httpClient.get('./assets/mock.data.json').subscribe((response) => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
};
IndexerService.ctorParameters = () => [
    { type: HttpClient },
    { type: SharingService },
    { type: IndexerConfigService }
];
IndexerService.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerService_Factory() { return new IndexerService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.IndexerConfigService)); }, token: IndexerService, providedIn: "root" });
IndexerService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], IndexerService);
export { IndexerService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXhlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvaW5kZXhlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHL0QsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzlELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7OztBQU1wRixJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBRXpCLFlBQ1MsVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsb0JBQTBDO1FBRjFDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7SUFDL0MsQ0FBQztJQUdMLGVBQWUsQ0FBQyxJQUFJO1FBQ2xCLE1BQU0saUJBQWlCLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsNEJBQTRCLENBQ2xILElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ25FLElBQUksaUJBQWlCLElBQUksaUJBQWlCLENBQUMsVUFBVSxFQUFFO1lBQ3JELE9BQU8saUJBQWlCLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDcEU7UUFDRCxPQUFPLGdCQUFnQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7SUFDdEMsQ0FBQztJQUVELE1BQU0sQ0FBQyxHQUFrQjtRQUN2QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksR0FBRyxDQUFDLGdCQUFnQixJQUFJLElBQUksRUFBRTtnQkFDaEMsYUFBYTtnQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQTtnQkFDekIsTUFBTSxXQUFXLEdBQUc7b0JBQ2xCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdkIsU0FBUyxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7d0JBQzlELFNBQVMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUMxSyxlQUFlLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUEsMEVBQTBFO3FCQUMxTixDQUFDO2lCQUNILENBQUM7Z0JBQ0YsdUhBQXVIO2dCQUV2SCw2SkFBNko7Z0JBQzdKLGFBQWE7Z0JBQ2IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLEVBQUUsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBd0IsRUFBRSxFQUFFO29CQUMzSCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDVCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDWixJQUFJLEVBQUUsRUFBRTtvQkFDUixNQUFNLEVBQUUsRUFBRSxhQUFhLEVBQUUsQ0FBQyxFQUFFO29CQUM1QixLQUFLLEVBQUUsSUFBSTtpQkFDWixDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3JCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsVUFBVSxDQUFDLEdBQWtCO1FBQzNCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUF3QixFQUFFLEVBQUU7Z0JBQ3BGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ0YsQ0FBQTs7WUEzRHNCLFVBQVU7WUFDTixjQUFjO1lBQ1Isb0JBQW9COzs7QUFMeEMsY0FBYztJQUgxQixVQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csY0FBYyxDQThEMUI7U0E5RFksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4vb2JqZWN0cy9TZWFyY2hSZXNwb25zZSc7XHJcbmltcG9ydCB7IFNlYXJjaFJlcXVlc3QgfSBmcm9tICcuL29iamVjdHMvU2VhcmNoUmVxdWVzdCc7XHJcbmltcG9ydCB7IEluZGV4ZXJDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi9tcG0taW5kZXhlci5jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEluZGV4ZXJFbmRQb2ludHMgfSBmcm9tICcuL29iamVjdHMvSW5kZXhlckVuZFBvaW50cyc7XHJcbmltcG9ydCB7IFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEluZGV4ZXJTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgaW5kZXhlckNvbmZpZ1NlcnZpY2U6IEluZGV4ZXJDb25maWdTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcblxyXG4gIGluZGV4ZXJFbmRQb2ludChwYXRoKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IGluZGV4ZXJCYXNlRG9tYWluID0gaXNEZXZNb2RlKCkgPyB7IFNFQVJDSF9VUkw6ICcuJyB9IDogdGhpcy5pbmRleGVyQ29uZmlnU2VydmljZS5nZXRJbmRleGVyQ29uZmlnQnlDYXRlZ29yeUlkKFxyXG4gICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFNlbGVjdGVkQ2F0ZWdvcnkoKVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQpO1xyXG4gICAgaWYgKGluZGV4ZXJCYXNlRG9tYWluICYmIGluZGV4ZXJCYXNlRG9tYWluLlNFQVJDSF9VUkwpIHtcclxuICAgICAgcmV0dXJuIGluZGV4ZXJCYXNlRG9tYWluLlNFQVJDSF9VUkwgKyBJbmRleGVyRW5kUG9pbnRzLmJhc2UgKyBwYXRoO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIEluZGV4ZXJFbmRQb2ludHMuYmFzZSArIHBhdGg7XHJcbiAgfVxyXG5cclxuICBzZWFyY2gocmVxOiBTZWFyY2hSZXF1ZXN0KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChyZXEuc2VhcmNoX2NvbmZpZ19pZCB8fCB0cnVlKSB7XHJcbiAgICAgICAgLy8gTVBNVjMtMjIzN1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCFpc0Rldk1vZGUoKSlcclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICdTQU1MYXJ0JzogIWlzRGV2TW9kZSgpID8gJycgOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnU0FNTEFSVCcpLFxyXG4gICAgICAgICAgICAnT1REU1NTTyc6IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1RJQ0tFVCkpICE9PSBudWxsID8gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVElDS0VUKSkgOiAnJyxcclxuICAgICAgICAgICAgJ2N1cnJlbnRVc2VySWQnOiAhaXNEZXZNb2RlKCkgPyBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19VU0VSX0lEKSkgOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnT1REU19VU0VSX0ROJykvL0pTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1VTRVJfRE4pKVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vICAgdGhpcy5odHRwQ2xpZW50LnBvc3QodGhpcy5pbmRleGVyRW5kUG9pbnQoSW5kZXhlckVuZFBvaW50cy5zZWFyY2gpLCByZXEpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcblxyXG4gICAgICAgIC8vIHRoaXMuaHR0cENsaWVudC5wb3N0KHRoaXMuaW5kZXhlckVuZFBvaW50KEluZGV4ZXJFbmRQb2ludHMuc2VhcmNoKSwgcmVxLCAhaXNEZXZNb2RlKCkgPyBodHRwT3B0aW9ucyA6IHVuZGVmaW5lZCkuc3Vic2NyaWJlKChyZXNwb25zZTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAvLyBNUE1WMy0yMjM3XHJcbiAgICAgICAgdGhpcy5odHRwQ2xpZW50LnBvc3QodGhpcy5pbmRleGVyRW5kUG9pbnQoSW5kZXhlckVuZFBvaW50cy5zZWFyY2gpLCByZXEsIGh0dHBPcHRpb25zKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dCh7XHJcbiAgICAgICAgICBkYXRhOiBbXSxcclxuICAgICAgICAgIGN1cnNvcjogeyB0b3RhbF9yZWNvcmRzOiAwIH0sXHJcbiAgICAgICAgICBmYWNldDogbnVsbFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBzZWFyY2hUZW1wKHJlcTogU2VhcmNoUmVxdWVzdCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmh0dHBDbGllbnQuZ2V0KCcuL2Fzc2V0cy9tb2NrLmRhdGEuanNvbicpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=