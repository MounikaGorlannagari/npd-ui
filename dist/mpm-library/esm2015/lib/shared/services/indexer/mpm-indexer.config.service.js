import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../../mpm-utils/services/app.service';
import { Observable } from 'rxjs';
import * as acronui from '../../../mpm-utils/auth/utility';
import { SessionStorageConstants } from '../../constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
let IndexerConfigService = class IndexerConfigService {
    constructor(appService) {
        this.appService = appService;
        this.MPM_INDEXER_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Indexer_Config/operations';
        this.MPM_INDEXER_CONFIG_WS = 'GetIndexerConfigByCategoryId';
    }
    getIndexerConfigByCategoryId(categoryId) {
        if (categoryId && this.indexerConfig && this.indexerConfig.R_PO_CATEGORY &&
            this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
            return this.indexerConfig;
        }
        return;
    }
    /* setIndexerConfigByCategoryId(categoryId: string): Observable<MPMIndexerConfig> {
      return new Observable(observer => {
        if (categoryId && this.indexerConfig && this.indexerConfig.R_PO_CATEGORY
          && this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
          observer.next(this.indexerConfig);
          observer.complete();
          return;
        }
        const param = {
          CategoryId: categoryId
        };
        this.indexerConfig = null;
        this.appService.invokeRequest(this.MPM_INDEXER_CONFIG_NS, this.MPM_INDEXER_CONFIG_WS, param).subscribe(indexerConfig => {
          const configs = acronui.findObjectsByProp(indexerConfig, 'MPM_Indexer_Config');
          if (configs[0]) {
            this.indexerConfig = configs[0];
          }
          observer.next(this.indexerConfig);
          observer.complete();
        }, error => {
          observer.error();
          observer.complete();
        });
      });
    } */
    setIndexerConfigByCategoryId(categoryId) {
        return new Observable(observer => {
            if (categoryId && this.indexerConfig && this.indexerConfig.R_PO_CATEGORY
                && this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
                observer.next(this.indexerConfig);
                observer.complete();
                return;
            }
            const param = {
                CategoryId: categoryId
            };
            this.indexerConfig = null;
            if (sessionStorage.getItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID) !== null) {
                this.indexerConfig = JSON.parse(sessionStorage.getItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID));
                observer.next(this.indexerConfig);
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.MPM_INDEXER_CONFIG_NS, this.MPM_INDEXER_CONFIG_WS, param).subscribe(indexerConfig => {
                    const configs = acronui.findObjectsByProp(indexerConfig, 'MPM_Indexer_Config');
                    if (configs[0]) {
                        this.indexerConfig = configs[0];
                    }
                    sessionStorage.setItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID, JSON.stringify(this.indexerConfig));
                    observer.next(this.indexerConfig);
                    observer.complete();
                }, error => {
                    observer.error();
                    observer.complete();
                });
            }
        });
    }
};
IndexerConfigService.ctorParameters = () => [
    { type: AppService }
];
IndexerConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerConfigService_Factory() { return new IndexerConfigService(i0.ɵɵinject(i1.AppService)); }, token: IndexerConfigService, providedIn: "root" });
IndexerConfigService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], IndexerConfigService);
export { IndexerConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWluZGV4ZXIuY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9tcG0taW5kZXhlci5jb25maWcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLGlDQUFpQyxDQUFDO0FBRTNELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7QUFLcEYsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFFL0IsWUFDUyxVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBRy9CLDBCQUFxQixHQUFHLDZEQUE2RCxDQUFDO1FBQ3RGLDBCQUFxQixHQUFHLDhCQUE4QixDQUFDO0lBSG5ELENBQUM7SUFPTCw0QkFBNEIsQ0FBQyxVQUFrQjtRQUM3QyxJQUFJLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYTtZQUN0RSxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxVQUFVLEVBQUU7WUFDdkUsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO1NBQzNCO1FBQ0QsT0FBTztJQUNULENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBd0JJO0lBRUosNEJBQTRCLENBQUMsVUFBa0I7UUFDN0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixJQUFJLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYTttQkFDbkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVSxFQUFFO2dCQUMxRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDbEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNwQixPQUFPO2FBQ1I7WUFDRCxNQUFNLEtBQUssR0FBRztnQkFDWixVQUFVLEVBQUUsVUFBVTthQUN2QixDQUFDO1lBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDZCQUE2QixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUMxRixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7Z0JBQy9HLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNsQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7b0JBQ3JILE1BQU0sT0FBTyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztvQkFDL0UsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQ2pDO29CQUNELGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDbEgsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ2xDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNULFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUNKO1FBR0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBR0YsQ0FBQTs7WUE5RXNCLFVBQVU7OztBQUhwQixvQkFBb0I7SUFIaEMsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLG9CQUFvQixDQWlGaEM7U0FqRlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1JbmRleGVyQ29uZmlnIH0gZnJvbSAnLi9vYmplY3RzL01QTUluZGV4ZXJDb25maWcnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEluZGV4ZXJDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIE1QTV9JTkRFWEVSX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fSW5kZXhlcl9Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgTVBNX0lOREVYRVJfQ09ORklHX1dTID0gJ0dldEluZGV4ZXJDb25maWdCeUNhdGVnb3J5SWQnO1xyXG5cclxuICBpbmRleGVyQ29uZmlnOiBNUE1JbmRleGVyQ29uZmlnO1xyXG5cclxuICBnZXRJbmRleGVyQ29uZmlnQnlDYXRlZ29yeUlkKGNhdGVnb3J5SWQ6IHN0cmluZyk6IE1QTUluZGV4ZXJDb25maWcge1xyXG4gICAgaWYgKGNhdGVnb3J5SWQgJiYgdGhpcy5pbmRleGVyQ29uZmlnICYmIHRoaXMuaW5kZXhlckNvbmZpZy5SX1BPX0NBVEVHT1JZICYmXHJcbiAgICAgIHRoaXMuaW5kZXhlckNvbmZpZy5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCA9PT0gY2F0ZWdvcnlJZCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5pbmRleGVyQ29uZmlnO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgLyogc2V0SW5kZXhlckNvbmZpZ0J5Q2F0ZWdvcnlJZChjYXRlZ29yeUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE1QTUluZGV4ZXJDb25maWc+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChjYXRlZ29yeUlkICYmIHRoaXMuaW5kZXhlckNvbmZpZyAmJiB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWVxyXG4gICAgICAgICYmIHRoaXMuaW5kZXhlckNvbmZpZy5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCA9PT0gY2F0ZWdvcnlJZCkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5pbmRleGVyQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICBDYXRlZ29yeUlkOiBjYXRlZ29yeUlkXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuaW5kZXhlckNvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0lOREVYRVJfQ09ORklHX05TLCB0aGlzLk1QTV9JTkRFWEVSX0NPTkZJR19XUywgcGFyYW0pLnN1YnNjcmliZShpbmRleGVyQ29uZmlnID0+IHtcclxuICAgICAgICBjb25zdCBjb25maWdzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChpbmRleGVyQ29uZmlnLCAnTVBNX0luZGV4ZXJfQ29uZmlnJyk7XHJcbiAgICAgICAgaWYgKGNvbmZpZ3NbMF0pIHtcclxuICAgICAgICAgIHRoaXMuaW5kZXhlckNvbmZpZyA9IGNvbmZpZ3NbMF07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5pbmRleGVyQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH0gKi9cclxuXHJcbiAgc2V0SW5kZXhlckNvbmZpZ0J5Q2F0ZWdvcnlJZChjYXRlZ29yeUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE1QTUluZGV4ZXJDb25maWc+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChjYXRlZ29yeUlkICYmIHRoaXMuaW5kZXhlckNvbmZpZyAmJiB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWVxyXG4gICAgICAgICYmIHRoaXMuaW5kZXhlckNvbmZpZy5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCA9PT0gY2F0ZWdvcnlJZCkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5pbmRleGVyQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICBDYXRlZ29yeUlkOiBjYXRlZ29yeUlkXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuaW5kZXhlckNvbmZpZyA9IG51bGw7XHJcbiAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLklOREVYRVJfQ09ORklHX0JZX0NBVEVHT1JZX0lEKSAhPT0gbnVsbCkge1xyXG4gICAgICAgIHRoaXMuaW5kZXhlckNvbmZpZyA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5JTkRFWEVSX0NPTkZJR19CWV9DQVRFR09SWV9JRCkpO1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5pbmRleGVyQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0lOREVYRVJfQ09ORklHX05TLCB0aGlzLk1QTV9JTkRFWEVSX0NPTkZJR19XUywgcGFyYW0pLnN1YnNjcmliZShpbmRleGVyQ29uZmlnID0+IHtcclxuICAgICAgICAgIGNvbnN0IGNvbmZpZ3MgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKGluZGV4ZXJDb25maWcsICdNUE1fSW5kZXhlcl9Db25maWcnKTtcclxuICAgICAgICAgIGlmIChjb25maWdzWzBdKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5kZXhlckNvbmZpZyA9IGNvbmZpZ3NbMF07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLklOREVYRVJfQ09ORklHX0JZX0NBVEVHT1JZX0lELCBKU09OLnN0cmluZ2lmeSh0aGlzLmluZGV4ZXJDb25maWcpKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5pbmRleGVyQ29uZmlnKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuXHJcbn1cclxuIl19