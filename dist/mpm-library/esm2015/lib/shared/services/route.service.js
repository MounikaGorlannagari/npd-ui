import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingConstants } from '../constants/routingConstants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
let RouteService = class RouteService {
    constructor(router) {
        this.router = router;
        this.routingConstants = RoutingConstants;
    }
    // Resource Management MPMV3-2056
    goToResourceManagement(queryParams, searchName, savedSearchName, advSearchData, facetData) {
        this.router.navigate([this.routingConstants.mpmBaseRoute + '/' + this.routingConstants.resourceManagement], {
            queryParams: {
                menuName: 'ResourceManagement',
                groupByFilter: queryParams.groupByFilter,
                //  deliverableTaskFilter: queryParams.deliverableTaskFilter,
                //  listDependentFilter: queryParams.listDependentFilter,
                //  emailLinkId: queryParams.emailLinkId,
                sortBy: queryParams.sortBy,
                sortOrder: queryParams.sortOrder,
                pageNumber: queryParams.pageNumber,
                pageSize: queryParams.pageSize,
                searchName: searchName,
                savedSearchName: savedSearchName,
                advSearchData: advSearchData,
                facetData: facetData
            }
        });
    }
};
RouteService.ctorParameters = () => [
    { type: Router }
];
RouteService.ɵprov = i0.ɵɵdefineInjectable({ factory: function RouteService_Factory() { return new RouteService(i0.ɵɵinject(i1.Router)); }, token: RouteService, providedIn: "root" });
RouteService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], RouteService);
export { RouteService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9yb3V0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7O0FBTWpFLElBQWEsWUFBWSxHQUF6QixNQUFhLFlBQVk7SUFJckIsWUFDVyxNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUh6QixxQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztJQUloQyxDQUFDO0lBRVIsaUNBQWlDO0lBRWpDLHNCQUFzQixDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxTQUFTO1FBQ3RGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDeEcsV0FBVyxFQUFFO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLGFBQWEsRUFBRSxXQUFXLENBQUMsYUFBYTtnQkFDeEMsNkRBQTZEO2dCQUM3RCx5REFBeUQ7Z0JBQ3pELHlDQUF5QztnQkFDekMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dCQUMxQixTQUFTLEVBQUUsV0FBVyxDQUFDLFNBQVM7Z0JBQ2hDLFVBQVUsRUFBRSxXQUFXLENBQUMsVUFBVTtnQkFDbEMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dCQUM5QixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsZUFBZSxFQUFFLGVBQWU7Z0JBQ2hDLGFBQWEsRUFBRSxhQUFhO2dCQUM1QixTQUFTLEVBQUUsU0FBUzthQUN2QjtTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FFQSxDQUFBOztZQXpCc0IsTUFBTTs7O0FBTGhCLFlBQVk7SUFKeEIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLFlBQVksQ0E4QnhCO1NBOUJZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFJvdXRpbmdDb25zdGFudHMgfSBmcm9tICcuLi9jb25zdGFudHMvcm91dGluZ0NvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBSb3V0ZVNlcnZpY2Uge1xyXG5cclxuICAgIHJvdXRpbmdDb25zdGFudHMgPSBSb3V0aW5nQ29uc3RhbnRzO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICkgeyB9XHJcblxyXG4gLy8gUmVzb3VyY2UgTWFuYWdlbWVudCBNUE1WMy0yMDU2XHJcblxyXG4gZ29Ub1Jlc291cmNlTWFuYWdlbWVudChxdWVyeVBhcmFtcywgc2VhcmNoTmFtZSwgc2F2ZWRTZWFyY2hOYW1lLCBhZHZTZWFyY2hEYXRhLCBmYWNldERhdGEpIHtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnJvdXRpbmdDb25zdGFudHMubXBtQmFzZVJvdXRlICsgJy8nICsgdGhpcy5yb3V0aW5nQ29uc3RhbnRzLnJlc291cmNlTWFuYWdlbWVudF0sIHtcclxuICAgICAgICBxdWVyeVBhcmFtczoge1xyXG4gICAgICAgICAgICBtZW51TmFtZTogJ1Jlc291cmNlTWFuYWdlbWVudCcsXHJcbiAgICAgICAgICAgIGdyb3VwQnlGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmdyb3VwQnlGaWx0ZXIsXHJcbiAgICAgICAgICAgIC8vICBkZWxpdmVyYWJsZVRhc2tGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlcixcclxuICAgICAgICAgICAgLy8gIGxpc3REZXBlbmRlbnRGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmxpc3REZXBlbmRlbnRGaWx0ZXIsXHJcbiAgICAgICAgICAgIC8vICBlbWFpbExpbmtJZDogcXVlcnlQYXJhbXMuZW1haWxMaW5rSWQsXHJcbiAgICAgICAgICAgIHNvcnRCeTogcXVlcnlQYXJhbXMuc29ydEJ5LFxyXG4gICAgICAgICAgICBzb3J0T3JkZXI6IHF1ZXJ5UGFyYW1zLnNvcnRPcmRlcixcclxuICAgICAgICAgICAgcGFnZU51bWJlcjogcXVlcnlQYXJhbXMucGFnZU51bWJlcixcclxuICAgICAgICAgICAgcGFnZVNpemU6IHF1ZXJ5UGFyYW1zLnBhZ2VTaXplLFxyXG4gICAgICAgICAgICBzZWFyY2hOYW1lOiBzZWFyY2hOYW1lLFxyXG4gICAgICAgICAgICBzYXZlZFNlYXJjaE5hbWU6IHNhdmVkU2VhcmNoTmFtZSxcclxuICAgICAgICAgICAgYWR2U2VhcmNoRGF0YTogYWR2U2VhcmNoRGF0YSxcclxuICAgICAgICAgICAgZmFjZXREYXRhOiBmYWNldERhdGFcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufVxyXG5cclxufSJdfQ==