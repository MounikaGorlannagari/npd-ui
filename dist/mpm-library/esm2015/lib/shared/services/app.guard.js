import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../mpm-utils/services/sharing.service";
import * as i4 from "../../login/services/app.route.service";
let AppGuard = class AppGuard {
    constructor(appService, otmmService, sharingService, appRouteService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.appRouteService = appRouteService;
    }
    getCRActionsForUser() {
        return new Observable(observer => {
            const crActions = this.sharingService.getCRActions();
            if (crActions && crActions.length > 0) {
                observer.next(true);
                observer.complete();
            }
            else {
                this.appService.getPossibleCRActionsForUser().subscribe(response => {
                    this.sharingService.setCRActions(acronui.findObjectsByProp(response, 'MPM_Teams'));
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
    getLookupDomains() {
        return new Observable(observer => {
            this.otmmService.checkOtmmSession()
                .subscribe(sessionResponse => {
                const lookupDomains = this.sharingService.getAllLookupDomains();
                if (lookupDomains && lookupDomains.length > 0) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    this.otmmService.getAllLookupDomains()
                        .subscribe(lookupDomainsResponse => {
                        this.sharingService.setAllLookupDomains(lookupDomainsResponse);
                        observer.next(true);
                        observer.complete();
                    }, lookupDomainsError => {
                        observer.error(lookupDomainsError);
                    });
                }
            }, sessionError => {
                this.appRouteService.goToLogout();
                observer.error(sessionError);
            });
        });
    }
    canActivate() {
        return new Observable(observer => {
            this.getLookupDomains()
                .subscribe(lookupDomainsResponse => {
                this.getCRActionsForUser()
                    .subscribe(crActionsResponse => {
                    observer.next(true);
                    observer.complete();
                }, crActionsError => {
                    observer.next(false);
                    observer.complete();
                });
            }, lookupDomainsError => {
                observer.next(false);
                observer.complete();
            });
        });
    }
};
AppGuard.ctorParameters = () => [
    { type: AppService },
    { type: OTMMService },
    { type: SharingService },
    { type: AppRouteService }
];
AppGuard.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppGuard_Factory() { return new AppGuard(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.SharingService), i0.ɵɵinject(i4.AppRouteService)); }, token: AppGuard, providedIn: "root" });
AppGuard = __decorate([
    Injectable({
        providedIn: 'root',
    })
], AppGuard);
export { AppGuard };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmd1YXJkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2FwcC5ndWFyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDOzs7Ozs7QUFNeEQsSUFBYSxRQUFRLEdBQXJCLE1BQWEsUUFBUTtJQUVqQixZQUNXLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLGVBQWdDO1FBSGhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtJQUN2QyxDQUFDO0lBRUwsbUJBQW1CO1FBQ2YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3JELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQywyQkFBMkIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDL0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNuRixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFO2lCQUM5QixTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQ3pCLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDaEUsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzNDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsRUFBRTt5QkFDakMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsQ0FBQzt3QkFFL0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTt3QkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2QyxDQUFDLENBQUMsQ0FBQztpQkFDVjtZQUNMLENBQUMsRUFBRSxZQUFZLENBQUMsRUFBRTtnQkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNsQyxRQUFRLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2pDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsV0FBVztRQUNQLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFO2lCQUNsQixTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLG1CQUFtQixFQUFFO3FCQUNyQixTQUFTLENBQUMsaUJBQWlCLENBQUMsRUFBRTtvQkFDM0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsY0FBYyxDQUFDLEVBQUU7b0JBQ2hCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtnQkFDcEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0osQ0FBQTs7WUFwRTBCLFVBQVU7WUFDVCxXQUFXO1lBQ1IsY0FBYztZQUNiLGVBQWU7OztBQU5sQyxRQUFRO0lBSnBCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxRQUFRLENBdUVwQjtTQXZFWSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290JyxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFwcFJvdXRlU2VydmljZTogQXBwUm91dGVTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGdldENSQWN0aW9uc0ZvclVzZXIoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBjckFjdGlvbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENSQWN0aW9ucygpO1xyXG4gICAgICAgICAgICBpZiAoY3JBY3Rpb25zICYmIGNyQWN0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQb3NzaWJsZUNSQWN0aW9uc0ZvclVzZXIoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q1JBY3Rpb25zKGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fVGVhbXMnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TG9va3VwRG9tYWlucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY2hlY2tPdG1tU2Vzc2lvbigpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbG9va3VwRG9tYWlucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsTG9va3VwRG9tYWlucygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChsb29rdXBEb21haW5zICYmIGxvb2t1cERvbWFpbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuZ2V0QWxsTG9va3VwRG9tYWlucygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGxvb2t1cERvbWFpbnNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxMb29rdXBEb21haW5zKGxvb2t1cERvbWFpbnNSZXNwb25zZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGxvb2t1cERvbWFpbnNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobG9va3VwRG9tYWluc0Vycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIHNlc3Npb25FcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBSb3V0ZVNlcnZpY2UuZ29Ub0xvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHNlc3Npb25FcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjYW5BY3RpdmF0ZSgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmdldExvb2t1cERvbWFpbnMoKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb29rdXBEb21haW5zUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0Q1JBY3Rpb25zRm9yVXNlcigpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JBY3Rpb25zUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGNyQWN0aW9uc0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCBsb29rdXBEb21haW5zRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=