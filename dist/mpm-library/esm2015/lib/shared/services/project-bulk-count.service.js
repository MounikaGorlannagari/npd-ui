import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import * as i0 from "@angular/core";
let ProjectBulkCountService = class ProjectBulkCountService {
    constructor() {
        this.projectId = [];
        this.projectDetails = [];
        this.selectedProjects = new SelectionModel(true, []);
    }
    getProjectBulkCount(projectData) {
        if (projectData === true) {
            this.projectId = [];
            this.projectDetails = [];
        }
        else {
            if (this.projectId && !this.projectId.includes(projectData.ID)) {
                this.projectId.push(projectData.ID);
                this.projectDetails.push(projectData);
            }
            else {
                const index = this.projectId.indexOf(projectData.ID);
                if (index > -1) {
                    this.projectId.splice(index, 1);
                    this.projectDetails.splice(index, 1);
                }
            }
        }
        //  this.projectId.push(projectData.ID);
        //  return this.projectId;
        return this.projectDetails;
    }
    getProjectBulkData(projectData) {
        projectData.forEach(row => {
            row.selected = true;
            this.selectedProjects.select(row);
        });
        return this.selectedProjects;
    }
};
ProjectBulkCountService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectBulkCountService_Factory() { return new ProjectBulkCountService(); }, token: ProjectBulkCountService, providedIn: "root" });
ProjectBulkCountService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ProjectBulkCountService);
export { ProjectBulkCountService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDOztBQUsxRCxJQUFhLHVCQUF1QixHQUFwQyxNQUFhLHVCQUF1QjtJQUlsQztRQUhBLGNBQVMsR0FBRyxFQUFFLENBQUM7UUFDZixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixxQkFBZ0IsR0FBRyxJQUFJLGNBQWMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVqQixtQkFBbUIsQ0FBQyxXQUFXO1FBQzdCLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtZQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztTQUMxQjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUM5RCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUFNO2dCQUNMLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3RDO2FBQ0Y7U0FDRjtRQUNELHdDQUF3QztRQUN4QywwQkFBMEI7UUFDMUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzdCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxXQUFXO1FBQzVCLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDeEIsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDcEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQy9CLENBQUM7Q0FDRixDQUFBOztBQWxDWSx1QkFBdUI7SUFIbkMsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLHVCQUF1QixDQWtDbkM7U0FsQ1ksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQcm9qZWN0QnVsa0NvdW50U2VydmljZSB7XHJcbiAgcHJvamVjdElkID0gW107XHJcbiAgcHJvamVjdERldGFpbHMgPSBbXTtcclxuICBzZWxlY3RlZFByb2plY3RzID0gbmV3IFNlbGVjdGlvbk1vZGVsKHRydWUsIFtdKTtcclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBnZXRQcm9qZWN0QnVsa0NvdW50KHByb2plY3REYXRhKSB7XHJcbiAgICBpZiAocHJvamVjdERhdGEgPT09IHRydWUpIHtcclxuICAgICAgdGhpcy5wcm9qZWN0SWQgPSBbXTtcclxuICAgICAgdGhpcy5wcm9qZWN0RGV0YWlscyA9IFtdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMucHJvamVjdElkICYmICF0aGlzLnByb2plY3RJZC5pbmNsdWRlcyhwcm9qZWN0RGF0YS5JRCkpIHtcclxuICAgICAgICB0aGlzLnByb2plY3RJZC5wdXNoKHByb2plY3REYXRhLklEKTtcclxuICAgICAgICB0aGlzLnByb2plY3REZXRhaWxzLnB1c2gocHJvamVjdERhdGEpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5wcm9qZWN0SWQuaW5kZXhPZihwcm9qZWN0RGF0YS5JRCk7XHJcbiAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgIHRoaXMucHJvamVjdElkLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICB0aGlzLnByb2plY3REZXRhaWxzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyAgdGhpcy5wcm9qZWN0SWQucHVzaChwcm9qZWN0RGF0YS5JRCk7XHJcbiAgICAvLyAgcmV0dXJuIHRoaXMucHJvamVjdElkO1xyXG4gICAgcmV0dXJuIHRoaXMucHJvamVjdERldGFpbHM7XHJcbiAgfVxyXG5cclxuICBnZXRQcm9qZWN0QnVsa0RhdGEocHJvamVjdERhdGEpIHtcclxuICAgIHByb2plY3REYXRhLmZvckVhY2gocm93ID0+IHtcclxuICAgICAgcm93LnNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdChyb3cpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RlZFByb2plY3RzO1xyXG4gIH1cclxufVxyXG4iXX0=