import { __decorate } from "tslib";
import { Component, ViewChildren, Output, EventEmitter } from '@angular/core';
import { ChangeDetectorRef, ElementRef, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FacetChangeEventService } from '../../facet/services/facet-change-event.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { SearchChangeEventService } from '../../search/services/search-change-event.service';
import { DeliverableTaskMetadataService } from '../../deliverable-task/service/deliverable.task.metadata.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { SearchDataService } from '../../search/services/search-data.service';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { TitleCasePipe } from '@angular/common';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { TaskService } from '../../project/tasks/task.service';
import { CalendarConstants } from '../shared/constants/Calendar.constants';
import { MatSortDirections } from '../../mpm-utils/objects/MatSortDirection';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { DeliverableTypes } from '../../mpm-utils/objects/DeliverableTypes';
import { TaskConstants } from '../../project/tasks/task.constants';
import { trigger, state, style, transition, animate } from '@angular/animations';
import * as acronui from './../../mpm-utils/auth/utility';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { GROUP_BY_ALL_FILTERS, GROUP_BY_FILTERS } from '../../shared/constants/GroupByFilter';
import { GroupByFilterTypes } from '../../shared/constants/GroupByFilterTypes';
import { RouteService } from '../../shared/services/route.service';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { DeliverableService } from '../../project/tasks/deliverable/deliverable.service';
const moment = extendMoment(Moment);
let ResourceManagementToolbarComponent = class ResourceManagementToolbarComponent {
    constructor(utilService, facetChangeEventService, routeService, activatedRoute, sharingService, renderer, media, changeDetectorRef, searchChangeEventService, deliverableTaskMetadataService, viewConfigService, loaderService, notificationService, filterHelperService, searchDataService, indexerService, titlecasePipe, otmmService, appService, fieldConfigService, taskService, deliverableService) {
        this.utilService = utilService;
        this.facetChangeEventService = facetChangeEventService;
        this.routeService = routeService;
        this.activatedRoute = activatedRoute;
        this.sharingService = sharingService;
        this.renderer = renderer;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.searchChangeEventService = searchChangeEventService;
        this.deliverableTaskMetadataService = deliverableTaskMetadataService;
        this.viewConfigService = viewConfigService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.filterHelperService = filterHelperService;
        this.searchDataService = searchDataService;
        this.indexerService = indexerService;
        this.titlecasePipe = titlecasePipe;
        this.otmmService = otmmService;
        this.appService = appService;
        this.fieldConfigService = fieldConfigService;
        this.taskService = taskService;
        this.deliverableService = deliverableService;
        this.componentParams = this.activatedRoute.queryParamMap.pipe(map(params => this.resourceManagementComponentRoute(params)));
        this.subscriptions = [];
        this.addedResourceHandler = new EventEmitter();
        this.groupByFilters = [];
        this.isReset = false;
        this.isRoute = false;
        this.totalListDataCount = 0;
        this.displayType = 'month';
        this.viewTypeCellSize = 150; // 35;
        this.momentPtr = moment;
        this.dates = [];
        this.showWeekends = true;
        this.taskAllocationOverloadStyle = {};
        this.taskAllocationStyle = {};
        this.cellStyle = {};
        /*  months = ['January', 'february', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
         */ this.facetExpansionState = true;
        this.myTaskViewConfigDetails = null;
        this.searchConfigId = -1;
        this.componentRendering = false;
        this.isPageRefresh = true;
        this.isDataRefresh = true;
        this.storeSearchconditionObject = null;
        this.sortableFields = [];
        this.searchConditions = [];
        this.emailSearchCondition = [];
        this.validateFacet = false;
        this.sidebarStyle = {};
        this.scrolled = false;
        this.isWeekView = false;
        this.previousFlag = false;
        this.nextFlag = false;
        this.allUsers = [];
        this.UNASSIGNED_TASK_SEARCH_LIMIT = 100;
        this.eventDisplay = {
            displayView: 'dayView',
            duration: { days: 30 },
            resources: []
        };
        this.allFilteredResources = [];
        this.usersUnavailabity = [];
        this.allUsersCapacity = [];
        this.projectListData = [];
        this.deliverableData = [];
        this.Math = Math;
        /** * Scroll for Fixing the dates and header */
        this.scrolling = (el) => {
            const scrollTop = el.srcElement.scrollTop;
            if (scrollTop >= 100) {
                this.scrolled = true;
            }
            else {
                this.scrolled = false;
            }
        };
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    // PREVIOUS navigation for Calendar
    calendarPrevious() {
        this.previousFlag = false;
        console.log(this.selectedMonth);
        for (let i = 0; i < CalendarConstants.months.length; i++) {
            if (CalendarConstants.months[i + 1] === this.selectedMonth) {
                console.log(CalendarConstants.months[i]);
                this.selectedMonth = CalendarConstants.months[i];
                this.previousFlag = true;
                break;
            }
            else if (CalendarConstants.months[0] === this.selectedMonth) {
                this.selectedMonth = CalendarConstants.months[CalendarConstants.months.length - 1];
                this.year = this.year - 1;
                break;
            }
        }
        // to have exact all date
        let noOfDays = this.getNoOfDays(this.selectedMonth);
        let days;
        if (this.displayType === 'week') {
            const startDate = this.momentPtr(this.currentViewStartDate).startOf('week').subtract(1, 'week');
            const endDate = this.momentPtr(this.currentViewStartDate).subtract(1, 'day');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            const range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.displayType === 'month') {
            const startDate = this.momentPtr(this.currentViewStartDate).subtract(noOfDays, 'days'); // 30
            const endDate = this.momentPtr(this.currentViewStartDate).subtract(1, 'day');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            const range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.showWeekends) {
            this.dates = days;
        }
        else {
            // remove sat and sun from date range
            this.dates = days.filter(d => d.isoWeekday() !== 6 && d.isoWeekday() !== 7);
        }
        //  this.resizeCalendar();
        this.reload(false);
    }
    // NEXT navigation for Calendar
    calendarNext() {
        this.nextFlag = false;
        console.log(this.selectedMonth);
        if (this.selectedMonth === CalendarConstants.months[CalendarConstants.months.length - 1]) {
            this.selectedMonth = CalendarConstants.months[0];
            this.year = this.year + 1;
        }
        else {
            for (let i = 0; i < CalendarConstants.months.length && this.selectedMonth !== CalendarConstants.months[CalendarConstants.months.length - 1]; i++) {
                if (CalendarConstants.months[i] === this.selectedMonth) {
                    console.log(CalendarConstants.months[i + 1]);
                    this.selectedMonth = CalendarConstants.months[i + 1];
                    this.nextFlag = true;
                    break;
                }
            }
        }
        /* if (this.selectedMonth === CalendarConstants.months[CalendarConstants.months.length - 1]) {
          this.selectedMonth = CalendarConstants.months[0];
        } */
        let noOfDays = this.getNoOfDays(this.selectedMonth);
        let days;
        if (this.displayType === 'week') {
            const startDate = this.momentPtr(this.currentViewStartDate).endOf('week').add(1, 'day');
            const endDate = this.momentPtr(startDate).add(1, 'week');
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            const range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.displayType === 'month') {
            const startDate = this.momentPtr(this.currentViewEndDate).add(1, 'day');
            const endDate = this.momentPtr(this.currentViewEndDate).add(noOfDays, 'days'); // 30
            this.currentViewStartDate = startDate;
            this.currentViewEndDate = endDate;
            const range = this.momentPtr.range(startDate, endDate);
            days = Array.from(range.by('days'));
        }
        if (this.showWeekends) {
            this.dates = days;
        }
        else {
            // remove sat and sun from the date range
            this.dates = days.filter(d => d.isoWeekday() !== 6 && d.isoWeekday() !== 7);
        }
        // this.resizeCalendar();
        this.reload(false);
    }
    facetToggled(event) {
        this.facetExpansionState = event;
    }
    selectedUserr(user) {
        this.selectedUsers = user;
    }
    selectedFilter(filter) {
        this.selectedItems = filter;
    }
    resourceManagementComponentRoute(params) {
        const groupByFilter = params.get('groupByFilter') || '';
        const deliverableTaskFilter = params.get('deliverableTaskFilter') || '';
        const listDependentFilter = params.get('listDependentFilter') || '';
        const emailLinkId = params.get('emailLinkId') || '';
        const sortBy = params.get('sortBy') || '';
        const sortOrder = params.get('sortOrder') === 'asc' ? MatSortDirections.ASC : (params.get('sortOrder') === 'desc' ? MatSortDirections.DESC : null);
        const pageNumber = params.get('pageNumber') ? Number(params.get('pageNumber')) : null;
        const pageSize = params.get('pageSize') ? Number(params.get('pageSize')) : null;
        const searchName = params.get('searchName') ? params.get('searchName') : null;
        const savedSearchName = params.get('savedSearchName') ? params.get('savedSearchName') : null;
        const advSearchData = params.get('advSearchData') ? params.get('advSearchData') : null;
        const facetData = params.get('facetData') ? params.get('facetData') : null;
        return {
            groupByFilter,
            deliverableTaskFilter,
            listDependentFilter,
            emailLinkId,
            sortBy,
            sortOrder,
            pageNumber,
            pageSize,
            searchName,
            savedSearchName,
            advSearchData,
            facetData
        };
    }
    setLevel() {
        if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_CAMPAIGN) {
            this.level = MPM_LEVELS.CAMPAIGN;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_CAMPAIGN;
        }
        else if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_PROJECT) {
            this.level = MPM_LEVELS.PROJECT;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_PROJECT;
        }
        else if (this.selectedGroupByFilter.value === GroupByFilterTypes.GROUP_BY_TASK) {
            this.level = MPM_LEVELS.TASK;
            this.viewConfig = SearchConfigConstants.SEARCH_NAME.RM_GROUP_BY_TASK;
        }
    }
    groupByFilterChange(option) {
        this.isReset = true;
        this.selectedGroupByFilter = option;
        this.facetRestrictionList = {};
        this.loaderService.show();
        this.facetChangeEventService.resetFacet();
        this.setLevel();
        this.getLevelDetails();
        if (!(this.searchName || this.savedSearchName || this.advSearchData || this.facetData)) {
            this.isRoute = true;
        }
        this.routeComponentView();
        this.loaderService.hide();
    }
    resetPagination() {
        this.page = 1;
        this.skip = 0;
    }
    routeComponentView() {
        const queryParams = {};
        if (this.selectedGroupByFilter.value) {
            queryParams['groupByFilter'] = this.selectedGroupByFilter.value;
        }
        if (this.selectedSort) {
            queryParams['sortBy'] = this.selectedSortBy ? this.selectedSortBy : null;
            queryParams['sortOrder'] = this.selectedSortOrder ? this.selectedSortOrder : null;
        }
        if (this.searchName) {
            queryParams['searchName'] = this.searchName;
        }
        if (this.savedSearchName) {
            queryParams['savedSearchName'] = this.savedSearchName;
        }
        if (this.advSearchData) {
            queryParams['advSearchData'] = this.advSearchData;
        }
        if (this.facetData) {
            queryParams['facetData'] = this.facetData;
        }
        if (this.pageSize) {
            queryParams['pageSize'] = this.pageSize;
        }
        if (this.page) {
            if (this.isReset) {
                this.resetPagination();
                this.selectedPageNumber = this.page;
                this.isReset = false;
            }
            queryParams['pageNumber'] = this.page;
        }
        this.routeService.goToResourceManagement(queryParams, this.searchName, this.savedSearchName, this.advSearchData, this.facetData);
    }
    // toggle month , week and day view
    toggleView(viewType) {
        console.log('viewType', viewType);
        let days;
        this.displayType = viewType;
        const isReinitialize = true;
        if (this.displayType === 'week') {
            this.viewTypeCellSize = 150;
            const today = this.momentPtr({});
            const fromDate = this.momentPtr({}).startOf('week');
            const toDate = this.momentPtr({}).endOf('week');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            const range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        else if (this.displayType === 'day') {
            const fromDate = this.momentPtr({});
            const toDate = this.momentPtr().add(0, 'days');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            const range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        else {
            this.viewTypeCellSize = 35;
            const fromDate = this.momentPtr({});
            const toDate = this.momentPtr().add(29, 'days');
            this.currentViewStartDate = fromDate;
            this.currentViewEndDate = toDate;
            const range = this.momentPtr.range(fromDate, toDate);
            days = Array.from(range.by('days'));
        }
        this.dates = days;
        if (!this.showWeekends) { // do not show weekends
            // remove sat and sun from the date range
            this.dates = days.filter(d => d.isoWeekday() !== 6 && d.isoWeekday() !== 7);
        }
        // this.resizeCalendar();
    }
    // Resizing the calendar size based on the users display and for month and week view
    resizeCalendar() {
        this.calculateViewSize();
        this.calendarHeaders.forEach(((elem, index) => {
            this.renderer.setStyle(elem.nativeElement, 'width', this.viewTypeCellSize + 'px');
        }));
        this.calendarCells.forEach(((elem, index) => {
            this.renderer.setStyle(elem.nativeElement, 'width', this.viewTypeCellSize + 'px');
        }));
    }
    // calculate the view size for calendar container
    calculateViewSize() {
        const windowWidth = window.innerWidth;
        // its always constant value than the window width to fit in correctly in px
        const rightMargin = 255;
        // this is going the be the calendar container width
        const calendarViewWidth = Math.floor(windowWidth - rightMargin);
        this.viewTypeCellSize = Math.floor(calendarViewWidth / this.dates.length);
        this.renderer.setStyle(this.calendarView.nativeElement, 'width', calendarViewWidth + 'px');
        this.taskAllocationOverloadStyle = {
            'background-color': 'red',
            width: (this.viewTypeCellSize) + 'px'
        };
        this.taskAllocationStyle = {
            'background-color': '#0895CE',
            width: (this.viewTypeCellSize) + 'px'
        };
        this.cellStyle = {
            width: (this.viewTypeCellSize + 1) + 'px'
        };
    }
    getProperty(deliverableData, mapperName) {
        // const level = this.isReviewer ? MPM_LEVELS.DELIVERABLE_REVIEW : MPM_LEVELS.DELIVERABLE;
        const displayColumn = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.DELIVERABLE);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, deliverableData);
    }
    /**
       * @since MPMV3-946
       * @param deliverableType
       */
    getDeliverableTypeIcon(deliverableType) {
        return this.deliverableService.getDeliverableTypeIcon(deliverableType);
    }
    expandRow(event, resource) {
        console.log(resource);
        event.stopPropagation();
        this.loaderService.show();
        let viewConfig;
        if (resource.deliverableData && Array.isArray(resource.deliverableData) && resource.deliverableData.length > 0) {
            this.loaderService.hide();
            return;
        }
        const displayField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, 
        // tslint:disable-next-line: max-line-length
        this.level === MPM_LEVELS.CAMPAIGN ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_CAMPAIGN_ID : this.level === MPM_LEVELS.PROJECT ? MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID : MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID, MPM_LEVELS.DELIVERABLE);
        if (!displayField || !resource['ITEM_ID']) {
            this.loaderService.hide();
            return;
        }
        // MPM-V3-2224 - comment below one
        // const dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        // const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, dates, undefined, true);
        // MPM-V3-2224
        const conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', undefined, false, true);
        // tslint:disable-next-line: max-line-length
        const otmmMPMDataTypes = ((this.isApprover || this.isMember) && this.selectedListFilter.value !== 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : OTMMMPMDataTypes.DELIVERABLE;
        const condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
        condition ? conditionObject.DELIVERABLE_CONDTION.push(condition) : console.log('');
        conditionObject.DELIVERABLE_CONDTION.push({
            type: displayField ? displayField.DATA_TYPE : 'string',
            field_id: displayField.INDEXER_FIELD_ID,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            relational_operator: 'and',
            value: resource['ITEM_ID']
        });
        if (this.viewConfig) {
            viewConfig = this.viewConfig;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
            const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
            const parameters = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                search_condition_list: {
                    search_condition: conditionObject.DELIVERABLE_CONDTION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 100
                }
            };
            this.loaderService.show();
            this.subscriptions.push(this.indexerService.search(parameters).subscribe((data) => {
                console.log(data);
                resource.deliverableData = [];
                if (data.data.length > 0) {
                    const dFormat = 'YYYY-MM-DD';
                    data.data.forEach((item, index) => {
                        let styles = {};
                        let top = 44 * index; // 30
                        const eventStartDate = this.momentPtr(item.DELIVERABLE_START_DATE, dFormat);
                        const eventEndDate = this.momentPtr(item.DELIVERABLE_DUE_DATE, dFormat);
                        const diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                        const extraPixel = ((diff + 1) % 2) === 0 ? 2 : 0; // 0 : 2;
                        const width = ((diff + 1) * (this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
                        //  styles['width'] = (((diff + 1) % 2) === 0) ? (width + 'px') : (width + 4) + 'px';
                        styles['top'] = top + 'px';
                        const deliverableDetail = {
                            data: item,
                            icon: this.getDeliverableTypeIcon(this.getProperty(item, 'DELIVERABLE_TYPE')).ICON,
                            style: styles
                        };
                        resource.deliverableData.push(deliverableDetail);
                        console.log(resource.deliverableData);
                    });
                    /*  resource.height = 35 * data.data.length + 'px'; */
                    resource.height = 50 * data.data.length + 'px';
                }
            }));
            this.loaderService.hide();
        });
    }
    updateSearchConfigId() {
        if (this.myTaskViewConfigDetails) {
            const lastValue = this.searchConfigId;
            this.searchConfigId = parseInt(this.myTaskViewConfigDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10);
            if (this.searchConfigId > 0 && lastValue !== this.searchConfigId) {
                this.searchChangeEventService.update(this.myTaskViewConfigDetails);
            }
        }
    }
    reload(resetPagination, value) {
        /* var width = document.getElementById('calendarCell').getBoundingClientRect().width;
        alert(width); */
        this.getlistDataIndex(resetPagination);
    }
    getlistDataIndex(value) {
        this.loaderService.show();
        this.projectListData = [];
        this.totalListDataCount = 0;
        if (value) {
            this.previousRequest = null;
        }
        let keyWord = '';
        const dates = this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        /*  const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, dates, undefined, true); */ // this.level
        // MPM-V3-2224
        const conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.level, this.momentPtr.utc(this.currentViewStartDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDate).format('YYYY-MM-DDT00:00:00.000') + 'Z', undefined, false, true);
        this.storeSearchconditionObject = Object.assign({}, conditionObject);
        const otmmMPMDataTypes = ((this.isApprover || this.isMember) && this.selectedListFilter.value != 'MY_NA_REVIEW_TASKS') ? OTMMMPMDataTypes.DELIVERABLE : this.isReviewer ? OTMMMPMDataTypes.DELIVERABLE_REVIEW : OTMMMPMDataTypes.DELIVERABLE;
        let sortTemp = [];
        if (this.selectedSort && this.selectedSort.active) {
            const sortOption = this.sortableFields.find(option => option.name === this.selectedSort.active);
            sortTemp = sortOption ? [{
                    field_id: sortOption.indexerId,
                    order: this.selectedSort.direction
                }] : [];
        }
        if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
            keyWord = this.searchConditions[0].keyword;
        }
        else if (this.searchConditions && this.searchConditions.length > 0) {
            conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
        }
        this.emailSearchCondition = this.emailSearchCondition && Array.isArray(this.emailSearchCondition) ? this.emailSearchCondition : [];
        const parameters = {
            search_config_id: this.searchConfigId > 0 ? this.searchConfigId : null,
            keyword: keyWord,
            grouping: {
                group_by: this.level,
                group_from_type: otmmMPMDataTypes,
                search_condition_list: {
                    search_condition: [...conditionObject.DELIVERABLE_CONDTION, ...this.emailSearchCondition]
                }
            },
            search_condition_list: {
                search_condition: conditionObject.TASK_CONDITION
            },
            facet_condition_list: {
                facet_condition: (this.facetRestrictionList && this.facetRestrictionList.facet_condition) ? this.facetRestrictionList.facet_condition : []
            },
            sorting_list: {
                sort: sortTemp
            },
            cursor: {
                page_index: this.skip,
                page_size: 1000
            }
        };
        this.loaderService.show();
        if (!(JSON.stringify(this.previousRequest) === JSON.stringify(parameters))) {
            this.previousRequest = parameters;
            this.indexerService.search(parameters).subscribe((response) => {
                this.isDataRefresh = false;
                this.facetChangeEventService.updateFacetFilter(response.facet_field_response_list ? response.facet_field_response_list : []);
                this.projectListData = response.data;
                this.totalListDataCount = response.cursor.total_records;
                this.loaderService.hide();
            }, error => {
                this.notificationService.error('Failed while getting ' + this.titlecasePipe.transform(this.viewConfig) + ' lists');
                this.loaderService.hide();
            });
        }
        else {
            this.loaderService.hide();
        }
    }
    getLevelDetails(trigger) {
        if (this.viewConfig) {
            this.viewConfigService.getAllDisplayFeildForViewConfig(this.viewConfig).subscribe((viewDetails) => {
                this.myTaskViewConfigDetails = viewDetails;
                this.updateSearchConfigId();
            });
            if (!(trigger === true)) {
                this.reload(false);
            }
        }
    }
    refresh() {
        this.reload(true);
    }
    initalizeComponent(routeData) {
        let routedGroupFilter;
        if (routeData.groupByFilter) {
            routedGroupFilter = this.enableCampaign ? GROUP_BY_ALL_FILTERS.find(filter => {
                return filter.value === routeData.groupByFilter;
            }) : GROUP_BY_FILTERS.find(filter => {
                return filter.value === routeData.groupByFilter;
            });
        }
        // tslint:disable-next-line: max-line-length
        this.selectedGroupByFilter = (routedGroupFilter === undefined) ? this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : routedGroupFilter || GROUP_BY_FILTERS[0] : routedGroupFilter || GROUP_BY_FILTERS[0]; /* this.enableCampaign ? GROUP_BY_ALL_FILTERS[0] : */
        this.setLevel();
        if (routeData.sortBy) {
            this.selectedSortBy = routeData.sortBy;
        }
        if (routeData.sortOrder) {
            this.selectedSortOrder = routeData.sortOrder;
        }
        if (routeData.pageNumber) {
            this.selectedPageNumber = routeData.pageNumber;
        }
        if (routeData.pageSize) {
            this.selectedPageSize = routeData.pageSize;
        }
        if ((routeData.searchName === null && this.searchName) || (this.searchName === null && routeData.searchName)
            || (routeData.savedSearchName === null && this.savedSearchName) || (this.savedSearchName === null && routeData.savedSearchName)
            || (routeData.advSearchData === null && this.advSearchData) || (this.advSearchData === null && routeData.advSearchData)) {
            this.selectedPageNumber = 1;
            this.searchName = routeData.searchName;
            this.savedSearchName = routeData.savedSearchName;
            this.advSearchData = routeData.advSearchData;
            this.isReset = true;
            this.routeComponentView();
        }
        else if ((routeData.facetData === null && this.facetData) || (this.facetData === null && routeData.facetData)) {
            this.selectedPageNumber = 1;
            this.facetData = routeData.facetData;
            this.isReset = true;
            this.routeComponentView();
        }
        else {
            if (routeData.searchName) {
                this.searchName = routeData.searchName;
            }
            else {
                this.searchName = null;
            }
            if (routeData.savedSearchName) {
                this.savedSearchName = routeData.savedSearchName;
            }
            else {
                this.savedSearchName = null;
            }
            if (routeData.advSearchData) {
                this.advSearchData = routeData.advSearchData;
            }
            else {
                this.advSearchData = null;
            }
            if (routeData.facetData) {
                this.facetData = routeData.facetData;
            }
            else {
                this.facetData = null;
            }
            this.componentRendering = false;
            if (routeData.emailLinkId) {
                this.getLevelDetails();
            }
            else {
                this.emailLinkId = '';
                if (this.isPageRefresh && (this.searchName || this.advSearchData || this.savedSearchName)) {
                    this.getLevelDetails(true);
                }
                else if (!(this.isPageRefresh && this.facetData)) {
                    this.getLevelDetails();
                }
            }
        }
    }
    /** CALENDAR METHODS **/
    // build calendar based on given start  and end date
    buildCalendar(startDate, endDate) {
        this.currentViewStartDate = startDate;
        this.currentViewEndDate = endDate;
        const start = startDate; // this.momentPtr({}); //get currrent date
        const end = endDate; // this.momentPtr({}).add(30, 'days');
        const range = this.momentPtr.range(start, end);
        const days = Array.from(range.by('days'));
        this.dates = days;
    }
    getDynamicWordSpacing() {
        let spacing = '8px';
        if (this.displayType === 'month') {
            if (!this.showWeekends) {
                spacing = '16px';
            }
            spacing = '0px';
        }
        return spacing;
    }
    getNoOfDays(selectedMonth) {
        let noOfDays;
        if (CalendarConstants.evenMonth.includes(selectedMonth)) {
            noOfDays = 31;
        }
        else if (CalendarConstants.oddMonth.includes(selectedMonth)) {
            noOfDays = 30;
        }
        else if (CalendarConstants.leapMonth.includes(selectedMonth)) {
            /* const date = new Date(this.year, 1, 29);
            return date.getMonth() === 1; */
            noOfDays = 28;
        }
        return noOfDays;
    }
    getUsersByRole(resource, deliverableData) {
        return new Observable(observer => {
            let getUserByRoleRequest = {};
            const roleId = resource.TASK_ROLE_ID ? resource.TASK_ROLE_ID : DeliverableTypes.REVIEW === deliverableData.DELIVERABLE_TYPE ? deliverableData.DELIVERABLE_APPROVER_ROLE_ID : deliverableData.DELIVERABLE_OWNER_ROLE_ID;
            const roleDn = this.utilService.getTeamRoleDNByRoleId(roleId);
            this.teamRoleUserOptions = [];
            getUserByRoleRequest = {
                allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
                roleDN: roleDn.ROLE_DN,
                teamID: '',
                isApproveTask: false,
                isUploadTask: true
            };
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(response => {
                if (response && response.users && response.users.user) {
                    const userList = acronui.findObjectsByProp(response, 'user');
                    const users = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(user => {
                            const fieldObj = users.find(e => e.value === user.dn);
                            if (!fieldObj) {
                                users.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    this.teamRoleUserOptions = users;
                }
                else {
                    this.teamRoleUserOptions = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                observer.next(false);
                observer.complete();
            });
        });
    }
    getUsers(resource, deliverableData) {
        console.log(deliverableData);
        this.getUsersByRole(resource, deliverableData).subscribe(response => {
        });
    }
    /* updateTaskDeliverable(data, userId) {
      let TaskObject;
      TaskObject = {
        'BulkOperation': false,
        'RPOOwnerUser': {
          'User': {
            'userID': userId
          }
        },
        'TaskId': {
          'Id': data.TASK_ITEM_ID.split('.')[1]
        }
      }
      const requestObj = {
        TaskObject: TaskObject,
        DeliverableReviewers: {
          DeliverableReviewer: ''
        }
      }
      this.taskService.updateTask(requestObj).subscribe(response => {
        this.loaderService.hide();
        if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
          this.reload(true);
          this.notificationService.success('Task updated successfully');
        } else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
          && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
          this.reload(true);
          this.notificationService.error(response.APIResponse.error.errorMessage);
        } else {
          this.reload(true);
          this.notificationService.error('Something went wrong on while updating task');
        }
      }, error => {
        this.reload(true);
        this.notificationService.error('Something went wrong on while updating task');
      });
    } */
    // MPM-V3-2217
    updateTaskDeliverable(data, userId) {
        this.taskService.assignDeliverable(data.ID, userId).subscribe(response => {
            this.loaderService.hide();
            if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                this.reload(true);
                this.notificationService.success('Deliverable updated successfully');
                //  this.addedResourceHandler.next(userId);
                //   this.synchronizeTaskFiltersComponent.getDeliverable(userId,false);
            }
            else if (response && response.APIResponse && response.APIResponse.statusCode === '500') {
                this.reload(true);
                this.notificationService.error('Something went wrong on while updating deliverable');
            }
            else {
                this.loaderService.hide();
                this.reload(true);
                this.notificationService.error('Something went wrong on while updating deliverable');
            }
        }, error => {
            this.loaderService.hide();
            this.reload(true);
            this.notificationService.error('Something went wrong on while updating deliverable');
        });
    }
    updateDeliverable(data, userId) {
        let deliverableObject;
        deliverableObject = {
            'BulkOperation': false,
            'RPODeliverableOwnerID': {
                'IdentityID': {
                    'userID': userId
                }
            },
            'DeliverableId': {
                'Id': data.ID
            }
        };
        const requestObj = {
            DeliverableObject: deliverableObject,
            DeliverableReviewers: {
                DeliverableReviewer: ''
            }
        };
        this.deliverableService.updateDeliverable(requestObj)
            .subscribe(response => {
            this.loaderService.hide();
            if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                this.reload(true);
                this.notificationService.success('Deliverable updated successfully');
                //  this.addedResourceHandler.next(userId);
                //  this.synchronizeTaskFiltersComponent.getDeliverable(userId, false);
            }
            else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                this.reload(true);
                this.notificationService.error(response.APIResponse.error.errorMessage);
            }
            else {
                this.loaderService.hide();
                this.reload(true);
                this.notificationService.error('Something went wrong on while updating deliverable');
            }
        }, error => {
            this.loaderService.hide();
            this.reload(true);
            this.notificationService.error('Something went wrong on while updating deliverable');
        });
    }
    update(data, userId) {
        console.log(userId);
        this.loaderService.show();
        if (data.DELIVERABLE_TYPE === DeliverableTypes.UPLOAD) {
            this.updateDeliverable(data, userId);
        }
        else {
            this.updateTaskDeliverable(data, userId);
        }
    }
    deliverableDataHandler(event) {
        console.log(event);
        this.deliverableDatas = event;
    }
    onChickletRemoveFilter(filter) {
        console.log(filter);
        this.removedFilter = filter;
    }
    /* resourceAllocationHandler(allocation) {
      console.log(allocation);
      this.allocation = allocation;
    } */
    getDateDifference(startDate, endDate) {
        let sd = (new Date(startDate));
        let ed = new Date(endDate);
        return (Math.floor(ed - sd) / (1000 * 60 * 60 * 24)) + 1;
    }
    // ngOnChanges(simpleChanges: SimpleChanges) {
    //   var angular: any;
    //   console.log("Content-width: "
    //     + (document.getElementById("calendarCell").scrollWidth)
    //     + "px<br>");
    //   console.log("Content-width1: "
    //     + angular.element(document.getElementById("calendarCell").clientHeight)
    //     + "px<br>");
    // }
    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit() {
        this.componentRendering = true;
        this.appConfig = this.sharingService.getAppConfig();
        this.enableCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.groupByFilters = this.enableCampaign ? GROUP_BY_ALL_FILTERS : GROUP_BY_FILTERS;
        this.top = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        this.pageSize = this.utilService.convertStringToNumber(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_PAGINATION]);
        const currentFullDate = new Date();
        const currentYear = currentFullDate.getFullYear();
        const currentMonth = currentFullDate.getMonth() + 1;
        const currentDate = currentFullDate.getDate();
        this.currentDate = currentMonth + '/' + currentDate + '/' + currentYear;
        this.selectedMonth = CalendarConstants.months[currentFullDate.getMonth()];
        this.year = currentYear;
        let noOfDays = this.getNoOfDays(this.selectedMonth);
        // this.buildCalendar(this.momentPtr({}), this.momentPtr({}).add(29, 'days'));
        /* this.buildCalendar(currentMonth + '/01/' + currentYear, this.momentPtr({}).add(29, 'days')); */
        this.buildCalendar(currentMonth + '/01/' + currentYear, currentMonth + '/' + noOfDays + '/' + currentYear);
        window.addEventListener('scroll', this.scrolling, true);
        let initialLoad = true;
        this.subscriptions.push(this.searchDataService.searchData.subscribe(data => {
            this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
            if ((this.searchName || this.savedSearchName || this.advSearchData || this.facetData) && this.isDataRefresh) {
                if (data && data.length > 0) {
                    if (!this.facetData || ((this.facetData && this.facetRestrictionList) && (this.advSearchData || this.savedSearchName))) {
                        this.getLevelDetails();
                    }
                    else {
                        this.validateFacet = true;
                    }
                }
            }
            else if (this.searchName === null && this.savedSearchName === null && this.advSearchData === null && this.facetData === null && this.isDataRefresh) {
                this.getLevelDetails();
            }
            else if (!this.isDataRefresh) {
                if (!initialLoad) {
                    this.isReset = true;
                    this.getLevelDetails();
                }
            }
            initialLoad = false;
        }));
        initialLoad = true;
        this.subscriptions.push(this.facetChangeEventService.onFacetCondidtionChange.subscribe(data => {
            this.facetRestrictionList = data;
            if (this.facetData && this.isDataRefresh) {
                if (data && data.facet_condition && data.facet_condition.length > 0) {
                    if (!(this.advSearchData || this.savedSearchName) || this.validateFacet) {
                        this.getLevelDetails();
                        this.validateFacet = false;
                    }
                }
            }
            else if (this.facetData === null && this.isDataRefresh) {
                this.getLevelDetails();
            }
            else if (!this.isDataRefresh) {
                if (!initialLoad) {
                    this.previousRequest = null;
                    this.isReset = true;
                    this.getLevelDetails();
                }
            }
            initialLoad = false;
        }));
        this.subscriptions.push(this.componentParams.subscribe(routeData => {
            if (routeData.emailLinkId) {
                this.emailLinkId = routeData.emailLinkId;
                this.initalizeComponent(routeData);
            }
            else {
                this.initalizeComponent(routeData);
            }
        }));
    }
    ngOnDestroy() {
        this.searchChangeEventService.update(null);
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
        window.removeEventListener('scroll', this.scrolling, true);
    }
};
ResourceManagementToolbarComponent.ctorParameters = () => [
    { type: UtilService },
    { type: FacetChangeEventService },
    { type: RouteService },
    { type: ActivatedRoute },
    { type: SharingService },
    { type: Renderer2 },
    { type: MediaMatcher },
    { type: ChangeDetectorRef },
    { type: SearchChangeEventService },
    { type: DeliverableTaskMetadataService },
    { type: ViewConfigService },
    { type: LoaderService },
    { type: NotificationService },
    { type: FilterHelperService },
    { type: SearchDataService },
    { type: IndexerService },
    { type: TitleCasePipe },
    { type: OTMMService },
    { type: AppService },
    { type: FieldConfigService },
    { type: TaskService },
    { type: DeliverableService }
];
__decorate([
    Output()
], ResourceManagementToolbarComponent.prototype, "addedResourceHandler", void 0);
__decorate([
    ViewChildren('dateHeader')
], ResourceManagementToolbarComponent.prototype, "calendarHeaders", void 0);
__decorate([
    ViewChildren('calendarCell')
], ResourceManagementToolbarComponent.prototype, "calendarCells", void 0);
__decorate([
    ViewChildren('panelContainer')
], ResourceManagementToolbarComponent.prototype, "panelContainers", void 0);
__decorate([
    ViewChild('calendarView')
], ResourceManagementToolbarComponent.prototype, "calendarView", void 0);
ResourceManagementToolbarComponent = __decorate([
    Component({
        selector: 'mpm-resource-management-toolbar',
        template: "<mpm-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n    [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\" [levels]=\"level\"\r\n    [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\" (selectedUser)=\"selectedUserr($event)\"\r\n    (selectedFilters)=\"selectedFilter($event)\" [removedFilter]=\"removedFilter\" [datess]=\"dates\" [datee]=\"dates\">\r\n</mpm-synchronize-task-filters>\r\n<mat-toolbar>\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-container\">\r\n            <div><span class=\"action-item\">\r\n                    <!-- <mpm-custom-select controlLabel=\"{{'Group By: '}}\" [options]=\"groupByFilters\"\r\n                        (selectionChange)=\"groupByFilterChange($event)\" [selected]=\"selectedGroupByFilter\"\r\n                        [bulkEdit]='true'>\r\n                    </mpm-custom-select> -->\r\n                </span>\r\n                <span>\r\n                    <button mat-icon-button (click)=refresh() matTooltip=\"Refresh\">\r\n                        <mat-icon color=\"primary\" class=\"dashboard-action-btn\">refresh\r\n                        </mat-icon>\r\n                    </button>\r\n                </span>\r\n            </div>\r\n\r\n            &nbsp;&nbsp;\r\n            <div>\r\n                <span>\r\n                    <mat-button-toggle-group name=\"viewType\" aria-label=\"View Type\" [value]=\"displayType\">\r\n                        <mat-button-toggle value=\"month\" matTooltip=\"Display Month View\">\r\n\r\n                            <span class=\"toogleView\">Month</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"week\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display Week View\">\r\n\r\n                            <span class=\"toogleView\">Week</span>\r\n                        </mat-button-toggle>\r\n                        <!-- <mat-button-toggle value=\"day\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display day View\">\r\n\r\n                            <span class=\"toogleView\">Day</span>\r\n                        </mat-button-toggle> -->\r\n                    </mat-button-toggle-group>\r\n                </span>\r\n            </div>\r\n            <div style=\"font-size: 20px;\">\r\n                <span>\r\n                    {{ currentDate | date}}\r\n                </span>\r\n            </div>\r\n\r\n            <div class=\"calendar-container\">\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarPrevious()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Previous {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"previous\" class=\"calendar-nav-button-icon\">\r\n                            chevron_left\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n                <div class=\"month-alignment\">\r\n                    <span>{{selectedMonth}} - {{year}}</span>\r\n                </div>\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarNext()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Next {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"next\" class=\"calendar-nav-button-icon\" \u00CD\u00CD>\r\n                            chevron_right\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>\r\n\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" style=\"height:50%;\" autosize>\r\n\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <div class=\"facet-sidenav\">\r\n            <!-- <mpm-facet [dataCount]=\"totalListDataCount\"></mpm-facet> -->\r\n        </div>\r\n        <!--  <hr />\r\n            <div class=\"sync\">\r\n                <app-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                    [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                    [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                    (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                    [removedFilter]=\"removedFilter\" \r\n                    [datess]=\"dates\" [datee]=\"dates\">\r\n                </app-synchronize-task-filters>\r\n            </div> -->\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\">\r\n\r\n        <div>\r\n            <div class=\"sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n                <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n\r\n                    <div class=\"sidenav-header text-center\">\r\n                        <span style=\"float:left;\">\r\n                            <!-- <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                                [isRMView]=\"true\">\r\n                            </mpm-facet-button> -->\r\n                        </span>\r\n\r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n                    <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                        (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                        <div *ngFor=\"let date of dates\" class=\"calendar-header\"\r\n                            [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                            [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                            #dateHeader>\r\n\r\n                            {{date.format('dd DD')}}\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div fxLayout=\"row\" style=\"    height: 300px;\r\n                    overflow: auto;\">\r\n                        <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                            <mat-expansion-panel hideToggle [expanded]=\"false\"\r\n                                *ngFor=\"let resource of projectListData;\">\r\n\r\n                                <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                                    (click)=\"expandRow($event,resource)\">\r\n\r\n                                    <mat-panel-title fxFlex=\"100\">\r\n\r\n                                        <span class=\"resource-text-color\">\r\n                                            {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                            }}</span>\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div>\r\n                                    <div class=\"panel-container task-content\" #panelContainer\r\n                                        [ngStyle]=\"{height: resource.height}\">\r\n                                        <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                            class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n\r\n                                            <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                                <ng-container\r\n                                                    *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                                    <!-- <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                        mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\"> -->\r\n                                                    <!-- MPM-V3-2224 -->\r\n                                                    <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day') ? true :(date.isSame(currentViewStartDate, 'day') && date.isAfter(event.data.DELIVERABLE_START_DATE, 'day'))\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"\r\n                                                        { width: \r\n                                                        date.isSame(event.data.DELIVERABLE_START_DATE, 'day') ? \r\n                                                        ((momentPtr(event.data.DELIVERABLE_DUE_DATE).isSame(momentPtr(currentViewEndDate), 'day')\r\n                                                        || momentPtr(event.data.DELIVERABLE_DUE_DATE).isBefore( momentPtr(currentViewEndDate) ,'day') )\r\n                                                        ? (getDateDifference(event.data.DELIVERABLE_START_DATE,event.data.DELIVERABLE_DUE_DATE)*37)+ 'px' \r\n                                                        : (getDateDifference(event.data.DELIVERABLE_START_DATE,currentViewEndDate) *37) + 'px')\r\n                                                        : (date.isAfter(event.data.DELIVERABLE_START_DATE, 'day') && date.isSame(currentViewStartDate, 'day') \r\n                                                        ? ((momentPtr(event.data.DELIVERABLE_DUE_DATE).isSame(momentPtr(currentViewEndDate), 'day')\r\n                                                        || momentPtr(event.data.DELIVERABLE_DUE_DATE).isBefore(momentPtr(currentViewEndDate), 'day'))\r\n                                                        ? (getDateDifference(currentViewStartDate,event.data.DELIVERABLE_DUE_DATE)*37)+ 'px' \r\n                                                        : (getDateDifference(currentViewStartDate,currentViewEndDate)*37)+ 'px' )\t\r\n                                                        : '3px' ),top:event.style.top}\" class=\"rectangle\" mwlResizable\r\n                                                        matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n                                                        <span (click)=\"getUsers(resource,event.data)\"\r\n                                                            [matMenuTriggerFor]=\"projectMenu\" class=\"event-title\">\r\n                                                            <mat-icon style=\"margin-top:12px;\">{{event.icon}}</mat-icon>\r\n                                                            {{event.data.DELIVERABLE_NAME}}\r\n                                                        </span>\r\n                                                        <mat-menu #projectMenu=\"matMenu\">\r\n                                                            <mat-option *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                                <div class=\"check\"\r\n                                                                    (click)=\"update(event.data,roleName.value)\">\r\n                                                                    {{roleName.displayName}}</div>\r\n                                                            </mat-option>\r\n                                                        </mat-menu>\r\n\r\n                                                    </div>\r\n\r\n\r\n                                                </ng-container>\r\n                                            </div>\r\n\r\n\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                            <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                                <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                            </div>\r\n                        </mat-accordion>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <!--    <hr /> -->\r\n            <!-- <hr />\r\n                <div>\r\n                    <app-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                        [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\"\r\n                        (removed)=\"onChickletRemoveFilter($event)\" [allocation]=\"allocation\">\r\n                    </app-resource-management-resource-allocation>\r\n                </div>\r\n     -->\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n\r\n</mat-sidenav-container>\r\n\r\n\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" autosize>\r\n\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <!-- <div class=\"facet-sidenav\">\r\n                <app-facet [dataCount]=\"totalListDataCount\"></app-facet>\r\n            </div> -->\r\n        <hr />\r\n        <div class=\"sync\">\r\n            <mpm-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                [removedFilter]=\"removedFilter\" [datess]=\"dates\" [datee]=\"dates\">\r\n            </mpm-synchronize-task-filters>\r\n        </div>\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\" style=\"height: 35%;\">\r\n\r\n        <div>\r\n            <!-- <div class=\" sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n        <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n\r\n            <div class=\"sidenav-header text-center\">\r\n                <span style=\"float:left;\">\r\n                    <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                        [isRMView]=\"true\">\r\n                    </mpm-facet-button>\r\n                </span>\r\n\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n            <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                <div *ngFor=\"let date of dates\" class=\"calendar-header\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                    [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                    #dateHeader>\r\n\r\n                    {{date.format('dd DD')}}\r\n                </div>\r\n            </div>\r\n\r\n            <div fxLayout=\"row\">\r\n                <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                    <mat-expansion-panel hideToggle [expanded]=\"false\" *ngFor=\"let resource of projectListData;\">\r\n\r\n                        <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                            (click)=\"expandRow($event,resource)\">\r\n\r\n                            <mat-panel-title fxFlex=\"100\">\r\n\r\n                                <span class=\"resource-text-color\">\r\n                                    {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                    }}</span>\r\n                            </mat-panel-title>\r\n                        </mat-expansion-panel-header>\r\n                        <div>\r\n                            <div class=\"panel-container task-content\" #panelContainer\r\n                                [ngStyle]=\"{height: resource.height}\">\r\n                                <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                    class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n\r\n                                    <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                        <ng-container *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                            <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n\r\n                                                <div>\r\n                                                    <span (click)=\"getUsers(resource,event.data)\"\r\n                                                        [matMenuTriggerFor]=\"projectMenu\"\r\n                                                        class=\"event-title\">{{event.data.DELIVERABLE_NAME}}\r\n                                                    </span>\r\n                                                    <mat-menu #projectMenu=\"matMenu\">\r\n                                                        <mat-option *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                            <div class=\"check\"\r\n                                                                (click)=\"update(event.data,roleName.value)\">\r\n                                                                {{roleName.displayName}}</div>\r\n                                                        </mat-option>\r\n                                                    </mat-menu>\r\n                                                </div>\r\n\r\n                                            </div>\r\n\r\n\r\n                                        </ng-container>\r\n                                    </div>\r\n\r\n\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </mat-expansion-panel>\r\n                    <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                        <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                    </div>\r\n                </mat-accordion>\r\n            </div>\r\n\r\n        </div>\r\n        </div> -->\r\n            <!-- <hr /> -->\r\n            <div style=\"height: 14rem;\">\r\n                <mpm-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                    [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\" (removed)=\"onChickletRemoveFilter($event)\"\r\n                    [allocation]=\"allocation\" [selectedYear]=\"year\">\r\n                </mpm-resource-management-resource-allocation>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n\r\n</mat-sidenav-container>\r\n\r\n\r\n\r\n\r\n\r\n<!-- <mat-toolbar>\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-container\">\r\n            <div><span class=\"action-item\">\r\n                    <mpm-custom-select controlLabel=\"{{'Group By: '}}\" [options]=\"groupByFilters\"\r\n                        (selectionChange)=\"groupByFilterChange($event)\" [selected]=\"selectedGroupByFilter\"\r\n                        [bulkEdit]='true'>\r\n                    </mpm-custom-select>\r\n                </span>\r\n            </div>\r\n            &nbsp;&nbsp;\r\n            <div>\r\n                <span>\r\n                    <mat-button-toggle-group name=\"viewType\" aria-label=\"View Type\" [value]=\"displayType\">\r\n                        <mat-button-toggle value=\"month\" matTooltip=\"Display Month View\">\r\n                            \r\n                            <span class=\"toogleView\">Month</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"week\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display Week View\">\r\n                            \r\n                            <span class=\"toogleView\">Week</span>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle value=\"day\" [disabled]=\"true\" aria-disabled=\"true\"\r\n                            matTooltip=\"Display day View\">\r\n                            \r\n                            <span class=\"toogleView\">Day</span>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </span>\r\n            </div>\r\n            <div style=\"font-size: 20px;\">\r\n                <span>\r\n                    {{ currentDate | date}}\r\n                </span>\r\n            </div>\r\n\r\n            <div class=\"calendar-container\">\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarPrevious()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Previous {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"previous\" class=\"calendar-nav-button-icon\">\r\n                            chevron_left\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n                <div class=\"month-alignment\">\r\n                    <span>{{selectedMonth}}</span>\r\n                </div>\r\n                <div>\r\n                    <button mat-stroked-button color=\"primary\" (click)=\"calendarNext()\" class=\"calendar-nav-button\"\r\n                        matTooltip=\"Next {{displayType == 'month' ? 'Month' : 'Week'}}\">\r\n                        <mat-icon aria-hidden=\"false\" aria-label=\"next\" class=\"calendar-nav-button-icon\" \u00CD\u00CD>\r\n                            chevron_right\r\n                        </mat-icon>\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>\r\n<mat-sidenav-container [style.marginTop.px]=\"mobileQuery.matches ? 56 : 0\" autosize>\r\n    <mat-sidenav class=\"custom-sidenav\" #pmFaceNav [disableClose]=\"true\" [mode]=\"mobileQuery.matches ? 'over' : 'side'\"\r\n        [opened]=\"facetExpansionState\">\r\n        <div class=\"facet-sidenav\">\r\n            <app-facet [dataCount]=\"totalListDataCount\"></app-facet>\r\n        </div>\r\n        <hr />\r\n        <div class=\"sync\">\r\n            <app-synchronize-task-filters [groupBy]=\"selectedGroupByFilter\" [selectedMonth]=\"selectedMonth\"\r\n                [currentViewStartDates]=\"currentViewStartDate\" [currentViewEndDates]=\"currentViewEndDate\"\r\n                [levels]=\"level\" [viewConfigs]=\"viewConfig\" (deliverableData)=\"deliverableDataHandler($event)\"\r\n                (selectedUser)=\"selectedUserr($event)\" (selectedFilters)=\"selectedFilter($event)\"\r\n                [removedFilter]=\"removedFilter\" \r\n                [datess]=\"dates\" [datee]=\"dates\">\r\n            </app-synchronize-task-filters>\r\n        </div>\r\n    </mat-sidenav>\r\n\r\n    <mat-sidenav-content class=\"mat-content\">\r\n\r\n        <div>\r\n            <div class=\"sidenav-container chart-section dateFlex-container\" fxFlex fxLayout=\"row\">\r\n                <div class=\"border-design\" [ngStyle]=\"sidebarStyle\" fxLayout=\"column\">\r\n                 \r\n                    <div class=\"sidenav-header text-center\">\r\n                        <span style=\"float:left;\">\r\n                            <mpm-facet-button [initalState]=\"facetExpansionState\" (facetToggled)=\"facetToggled($event)\"\r\n                                [isRMView]=\"true\">\r\n                            </mpm-facet-button>\r\n                        </span>\r\n                       \r\n                    </div>\r\n\r\n                </div>\r\n\r\n                <div class=\"sidenav-content calendar border-design\" fxLayout=\"column\" #calendarView>\r\n                    <div fxLayout=\"row\" id=\"datesHolder\" class=\"sticky-dates scroll-margin dates-container\"\r\n                        (scroll)=\"scrolling($event)\" [ngClass]=\"{'scrolled-div': scrolled}\">\r\n                        <div *ngFor=\"let date of dates\" class=\"calendar-header\"\r\n                            [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                            [ngClass]=\"{'highlightWeekEnd':date.format('dd') === 'Sa' ||date.format('dd') === 'Su'}\"\r\n                            #dateHeader>\r\n                           \r\n                            {{date.format('dd DD')}}\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div fxLayout=\"row\">\r\n                        <mat-accordion class=\"fill-layout\" multi=\"true\">\r\n                            <mat-expansion-panel hideToggle [expanded]=\"false\"\r\n                                *ngFor=\"let resource of projectListData;\">\r\n                               \r\n                                <mat-expansion-panel-header class=\"scroll-margin\" style=\"padding: 0px;\"\r\n                                    (click)=\"expandRow($event,resource)\">\r\n                                   \r\n                                    <mat-panel-title fxFlex=\"100\">\r\n                                       \r\n                                        <span class=\"resource-text-color\">\r\n                                            {{resource.TASK_NAME || resource.PROJECT_NAME|| resource.CAMPAIGN_NAME\r\n                                            }}</span>\r\n                                    </mat-panel-title>\r\n                                </mat-expansion-panel-header>\r\n                                <div>\r\n                                    <div class=\"panel-container task-content\" #panelContainer\r\n                                        [ngStyle]=\"{height: resource.height}\">\r\n                                        <div *ngFor=\"let date of dates;\" [ngStyle]=\"{width: viewTypeCellSize + 'px'}\"\r\n                                            class=\"calendar-cell\" [attr.date-filled]=\"\" #calendarCell>\r\n\r\n                                           \r\n                                            <div *ngIf=\"resource.deliverableData && resource.deliverableData.length>0\">\r\n                                                <ng-container\r\n                                                    *ngFor=\"let event of resource.deliverableData; let idx=index\">\r\n                                                    <div *ngIf=\"date.isSame(event.data.DELIVERABLE_START_DATE, 'day')\"\r\n                                                        [id]=\"event.data.ID\" [ngStyle]=\"event.style\" class=\"rectangle\"\r\n                                                        mwlResizable matTooltip=\"{{event.data.DELIVERABLE_NAME}}\">\r\n                                                      \r\n                                                        <div>\r\n                                                            <span (click)=\"getUsers(resource,event.data)\"\r\n                                                                [matMenuTriggerFor]=\"projectMenu\"\r\n                                                                class=\"event-title\">{{event.data.DELIVERABLE_NAME}}\r\n                                                            </span>\r\n                                                            <mat-menu #projectMenu=\"matMenu\">\r\n                                                                <mat-option\r\n                                                                    *ngFor=\"let roleName of teamRoleUserOptions\">\r\n                                                                    <div class=\"check\"\r\n                                                                        (click)=\"update(event.data,roleName.value)\">\r\n                                                                        {{roleName.displayName}}</div>\r\n                                                                </mat-option>\r\n                                                            </mat-menu>\r\n                                                        </div>\r\n                                                       \r\n                                                    </div>\r\n\r\n\r\n                                                </ng-container>\r\n                                            </div>\r\n\r\n\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </mat-expansion-panel>\r\n                            <div *ngIf=\"totalListDataCount === 0\" class=\"central-info\">\r\n                                <button color=\"accent\" disabled mat-flat-button>No data is available.</button>\r\n                            </div>\r\n                        </mat-accordion>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n            <hr />\r\n            <div>\r\n                <app-resource-management-resource-allocation [selectedMonth]=\"selectedMonth\" [data]=\"deliverableDatas\"\r\n                    [users]=\"selectedUsers\" [selectedItems]=\"selectedItems\"\r\n                    (removed)=\"onChickletRemoveFilter($event)\" [allocation]=\"allocation\">\r\n                </app-resource-management-resource-allocation>\r\n            </div>\r\n\r\n        </div>\r\n\r\n    </mat-sidenav-content>\r\n</mat-sidenav-container> -->",
        animations: [
            trigger('rotatedState', [
                state('default', style({ transform: 'rotate(0deg)' })),
                state('rotated', style({ transform: 'rotate(180deg)' })),
                transition('rotated => default', animate('250ms ease-out')),
                transition('default => rotated', animate('250ms ease-in'))
            ])
        ],
        styles: [""]
    })
], ResourceManagementToolbarComponent);
export { ResourceManagementToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC10b29sYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvcmVzb3VyY2UtbWFuYWdlbWVudC10b29sYmFyL3Jlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsWUFBWSxFQUFhLE1BQU0sRUFBRSxZQUFZLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ2hILE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0YsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUM1QyxPQUFPLEtBQUssTUFBTSxNQUFNLFFBQVEsQ0FBQztBQUNqQyxPQUFPLEVBQUUsVUFBVSxFQUEwQixNQUFNLE1BQU0sQ0FBQztBQUMxRCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFckMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUM3RixPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQUNsSCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNoRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFFdEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBR2pILE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNuRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sS0FBSyxPQUFPLE1BQU0sZ0NBQWdDLENBQUM7QUFDMUQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFakcsT0FBTyxFQUFFLG9CQUFvQixFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDL0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBR3pGLE1BQU0sTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztBQWVwQyxJQUFhLGtDQUFrQyxHQUEvQyxNQUFhLGtDQUFrQztJQXNJN0MsWUFDUyxXQUF3QixFQUN4Qix1QkFBZ0QsRUFDaEQsWUFBMEIsRUFDMUIsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsUUFBbUIsRUFDbkIsS0FBbUIsRUFDbkIsaUJBQW9DLEVBQ3BDLHdCQUFrRCxFQUNsRCw4QkFBOEQsRUFDOUQsaUJBQW9DLEVBQ3BDLGFBQTRCLEVBQzVCLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGNBQThCLEVBQzlCLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3RCLGtCQUFzQyxFQUN0QyxXQUF3QixFQUN4QixrQkFBc0M7UUFyQnRDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLDRCQUF1QixHQUF2Qix1QkFBdUIsQ0FBeUI7UUFDaEQsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBQ2xELG1DQUE4QixHQUE5Qiw4QkFBOEIsQ0FBZ0M7UUFDOUQsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUExSnRDLG9CQUFlLEdBQWtELElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDOUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQzdELENBQUM7UUFDTSxrQkFBYSxHQUF3QixFQUFFLENBQUM7UUFFdEMseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQU96RCxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUF5QmhCLHVCQUFrQixHQUFHLENBQUMsQ0FBQztRQUV2QixnQkFBVyxHQUFHLE9BQU8sQ0FBQztRQUN0QixxQkFBZ0IsR0FBRyxHQUFHLENBQUMsQ0FBQSxNQUFNO1FBQzdCLGNBQVMsR0FBRyxNQUFNLENBQUM7UUFHbkIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBY3BCLGdDQUEyQixHQUFHLEVBQUUsQ0FBQztRQUNqQyx3QkFBbUIsR0FBRyxFQUFFLENBQUM7UUFDekIsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUtoQjtXQUNHLENBQUMsd0JBQW1CLEdBQUcsSUFBSSxDQUFDO1FBRTlCLDRCQUF1QixHQUFlLElBQUksQ0FBQztRQUMzQyxtQkFBYyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBR3BCLHVCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMzQixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUdyQiwrQkFBMEIsR0FBeUIsSUFBSSxDQUFDO1FBRXhELG1CQUFjLEdBQWUsRUFBRSxDQUFDO1FBRWhDLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUN0Qix5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFNMUIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFFbEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFJakIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLGlDQUE0QixHQUFHLEdBQUcsQ0FBQztRQUVuQyxpQkFBWSxHQUFHO1lBQ2IsV0FBVyxFQUFFLFNBQVM7WUFDdEIsUUFBUSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRTtZQUN0QixTQUFTLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRix5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUN0QixvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQixvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQWlCckIsU0FBSSxHQUFHLElBQUksQ0FBQztRQXVwQlosK0NBQStDO1FBRS9DLGNBQVMsR0FBRyxDQUFDLEVBQUUsRUFBUSxFQUFFO1lBQ3ZCLE1BQU0sU0FBUyxHQUFHLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1lBQzFDLElBQUksU0FBUyxJQUFJLEdBQUcsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDdEI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUE7UUF0b0JDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDNUIsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0gsQ0FBQztJQUVELG1DQUFtQztJQUNuQyxnQkFBZ0I7UUFFZCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUVoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4RCxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixNQUFNO2FBQ1A7aUJBQU0sSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbkYsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztnQkFDMUIsTUFBTTthQUNQO1NBQ0Y7UUFFRCx5QkFBeUI7UUFDekIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUM7UUFFVCxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO1lBQy9CLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDaEcsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRTdFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUVsQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtZQUNoQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLO1lBQzdGLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUU3RSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7WUFFbEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUVyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjthQUFNO1lBRUwscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQzdFO1FBQ0QsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELCtCQUErQjtJQUMvQixZQUFZO1FBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDaEMsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ3hGLElBQUksQ0FBQyxhQUFhLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7U0FDM0I7YUFBTTtZQUNMLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssaUJBQWlCLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hKLElBQUksaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBRXRELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3QyxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3JELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO29CQUNyQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUVEOztZQUVJO1FBRUosSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFcEQsSUFBSSxJQUFJLENBQUM7UUFFVCxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO1lBQy9CLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEYsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXpELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUVsQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLE9BQU8sRUFBRTtZQUNoQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDeEUsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSztZQUVwRixJQUFJLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7WUFFbEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZELElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUNyQztRQUVELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNuQjthQUFNO1lBRUwseUNBQXlDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQzdFO1FBQ0QseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLO1FBQ2hCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7SUFFbkMsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFJO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCxjQUFjLENBQUMsTUFBTTtRQUNuQixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztJQUM5QixDQUFDO0lBRUQsZ0NBQWdDLENBQUMsTUFBZ0I7UUFDL0MsTUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEQsTUFBTSxxQkFBcUIsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hFLE1BQU0sbUJBQW1CLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNwRSxNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNwRCxNQUFNLE1BQU0sR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQyxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25KLE1BQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN0RixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDaEYsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzlFLE1BQU0sZUFBZSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDN0YsTUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3ZGLE1BQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzRSxPQUFPO1lBQ0wsYUFBYTtZQUNiLHFCQUFxQjtZQUNyQixtQkFBbUI7WUFDbkIsV0FBVztZQUNYLE1BQU07WUFDTixTQUFTO1lBQ1QsVUFBVTtZQUNWLFFBQVE7WUFDUixVQUFVO1lBQ1YsZUFBZTtZQUNmLGFBQWE7WUFDYixTQUFTO1NBQ1YsQ0FBQztJQUNKLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxLQUFLLGtCQUFrQixDQUFDLGlCQUFpQixFQUFFO1lBQzdFLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztTQUMxRTthQUFNLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssS0FBSyxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNuRixJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUM7U0FDekU7YUFBTSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEtBQUssa0JBQWtCLENBQUMsYUFBYSxFQUFFO1lBQ2hGLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztTQUN0RTtJQUNILENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxNQUFvQjtRQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMscUJBQXFCLEdBQUcsTUFBTSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsdUJBQXVCLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDMUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDdEYsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDckI7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztJQUNoQixDQUFDO0lBRUQsa0JBQWtCO1FBQ2hCLE1BQU0sV0FBVyxHQUFzQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFO1lBQ3BDLFdBQVcsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDO1NBQ2pFO1FBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDekUsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDbkY7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsV0FBVyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDN0M7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztTQUN2RDtRQUNELElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixXQUFXLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztTQUNuRDtRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUMzQztRQUNELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixXQUFXLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztTQUN6QztRQUNELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNiLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDcEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDdEI7WUFDRCxXQUFXLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNuSSxDQUFDO0lBRUQsbUNBQW1DO0lBQ25DLFVBQVUsQ0FBQyxRQUFRO1FBRWpCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFBO1FBQ2pDLElBQUksSUFBSSxDQUFDO1FBRVQsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUM7UUFDNUIsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBRTVCLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxNQUFNLEVBQUU7WUFFL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQztZQUU1QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBRWpDLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRWhELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxRQUFRLENBQUM7WUFDckMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztZQUVqQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUssRUFBRTtZQUNyQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRS9DLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxRQUFRLENBQUM7WUFDckMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQztZQUVqQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBRXJDO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBRTNCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDcEMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFaEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQztZQUNyQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsTUFBTSxDQUFDO1lBRWpDLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVyRCxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FFckM7UUFFRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUVsQixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLHVCQUF1QjtZQUMvQyx5Q0FBeUM7WUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDN0U7UUFFRCx5QkFBeUI7SUFDM0IsQ0FBQztJQUVELG9GQUFvRjtJQUNwRixjQUFjO1FBRVosSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUM1QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDcEYsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVKLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3BGLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFTixDQUFDO0lBRUQsaURBQWlEO0lBQ2pELGlCQUFpQjtRQUVmLE1BQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFFdEMsNEVBQTRFO1FBQzVFLE1BQU0sV0FBVyxHQUFHLEdBQUcsQ0FBQztRQUV4QixvREFBb0Q7UUFDcEQsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsQ0FBQztRQUVoRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTFFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUUzRixJQUFJLENBQUMsMkJBQTJCLEdBQUc7WUFDakMsa0JBQWtCLEVBQUUsS0FBSztZQUN6QixLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxJQUFJO1NBQ3RDLENBQUM7UUFFRixJQUFJLENBQUMsbUJBQW1CLEdBQUc7WUFDekIsa0JBQWtCLEVBQUUsU0FBUztZQUM3QixLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxJQUFJO1NBQ3RDLENBQUM7UUFFRixJQUFJLENBQUMsU0FBUyxHQUFHO1lBQ2YsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUk7U0FDMUMsQ0FBQztJQUNKLENBQUM7SUFFRCxXQUFXLENBQUMsZUFBZSxFQUFFLFVBQVU7UUFDckMsMEZBQTBGO1FBQzFGLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDL0gsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsYUFBYSxFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFRDs7O1NBR0s7SUFDTCxzQkFBc0IsQ0FBQyxlQUFlO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFHRCxTQUFTLENBQUMsS0FBSyxFQUFFLFFBQVE7UUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLFVBQVUsQ0FBQztRQUNmLElBQUksUUFBUSxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDOUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixPQUFPO1NBQ1I7UUFDRCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVc7UUFDdEYsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxLQUFLLEtBQUssVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0UyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsT0FBTztTQUNSO1FBQ0Qsa0NBQWtDO1FBQ2xDLHVNQUF1TTtRQUV2TSwrTEFBK0w7UUFDL0wsY0FBYztRQUNkLE1BQU0sZUFBZSxHQUF5QixJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUMsU0FBUyxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsQ0FBQztRQUVyVyw0Q0FBNEM7UUFDNUMsTUFBTSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssS0FBSyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7UUFFOU8sTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFLEVBQUUsc0JBQXNCLENBQUMsRUFBRSxFQUFFLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVLLFNBQVMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuRixlQUFlLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO1lBQ3hDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDdEQsUUFBUSxFQUFFLFlBQVksQ0FBQyxnQkFBZ0I7WUFDdkMsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtZQUM3Qyx3QkFBd0IsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFO1lBQ25ELG1CQUFtQixFQUFFLEtBQUs7WUFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxTQUFTLENBQUM7U0FDM0IsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQzlCO2FBQU07WUFDTCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztTQUNyRDtRQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7WUFDdkcsTUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixJQUFJLE9BQU8sV0FBVyxDQUFDLDJCQUEyQixLQUFLLFFBQVE7Z0JBQ3pILENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDakUsTUFBTSxVQUFVLEdBQWtCO2dCQUNoQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDcEQsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gscUJBQXFCLEVBQUU7b0JBQ3JCLGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxvQkFBb0I7aUJBQ3ZEO2dCQUNELG9CQUFvQixFQUFFO29CQUNwQixlQUFlLEVBQUUsRUFBRTtpQkFDcEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLElBQUksRUFBRSxFQUFFO2lCQUNUO2dCQUNELE1BQU0sRUFBRTtvQkFDTixVQUFVLEVBQUUsQ0FBQztvQkFDYixTQUFTLEVBQUUsR0FBRztpQkFDZjthQUNGLENBQUM7WUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRTFCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNyQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFvQixFQUFFLEVBQUU7Z0JBQ3hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO2dCQUM5QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDeEIsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDO29CQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTt3QkFDaEMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO3dCQUNoQixJQUFJLEdBQUcsR0FBRyxFQUFFLEdBQUcsS0FBSyxDQUFDLENBQUMsS0FBSzt3QkFDM0IsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQzVFLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxDQUFDO3dCQUN4RSxNQUFNLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUN6RSxNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxTQUFTO3dCQUMzRCxNQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyw4Q0FBOEM7d0JBQ2pILHFGQUFxRjt3QkFDckYsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7d0JBQzNCLE1BQU0saUJBQWlCLEdBQUc7NEJBQ3hCLElBQUksRUFBRSxJQUFJOzRCQUNWLElBQUksRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUk7NEJBQ2xGLEtBQUssRUFBRSxNQUFNO3lCQUNkLENBQUM7d0JBQ0YsUUFBUSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3hDLENBQUMsQ0FBQyxDQUFDO29CQUNILHNEQUFzRDtvQkFDdEQsUUFBUSxDQUFDLE1BQU0sR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2lCQUNoRDtZQUNILENBQUMsQ0FBQyxDQUNILENBQUM7WUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUVELG9CQUFvQjtRQUNsQixJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUNoQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQywyQkFBMkIsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM3RixJQUFJLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLFNBQVMsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNoRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2FBQ3BFO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQXdCLEVBQUUsS0FBVztRQUUxQzt3QkFDZ0I7UUFDaEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFHRCxnQkFBZ0IsQ0FBQyxLQUFlO1FBQzlCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztRQUM1QixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxDQUFDO1FBRXBNLG1NQUFtTSxDQUFDLGFBQWE7UUFDak4sY0FBYztRQUNkLE1BQU0sZUFBZSxHQUF5QixJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUMsU0FBUyxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsQ0FBQztRQUVyVyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDckUsTUFBTSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssSUFBSSxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7UUFDN08sSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNqRCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRyxRQUFRLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2QixRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVM7b0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVM7aUJBQ25DLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ1Q7UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEtBQUssSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRTtZQUNySixPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztTQUM1QzthQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BFLGVBQWUsQ0FBQyxjQUFjLEdBQUcsZUFBZSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDL0Y7UUFDRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRW5JLE1BQU0sVUFBVSxHQUFrQjtZQUNoQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN0RSxPQUFPLEVBQUUsT0FBTztZQUNoQixRQUFRLEVBQUU7Z0JBQ1IsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNwQixlQUFlLEVBQUUsZ0JBQWdCO2dCQUNqQyxxQkFBcUIsRUFBRTtvQkFDckIsZ0JBQWdCLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxvQkFBb0IsRUFBRSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztpQkFDMUY7YUFDRjtZQUNELHFCQUFxQixFQUFFO2dCQUNyQixnQkFBZ0IsRUFBRSxlQUFlLENBQUMsY0FBYzthQUNqRDtZQUNELG9CQUFvQixFQUFFO2dCQUNwQixlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQzNJO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLElBQUksRUFBRSxRQUFRO2FBQ2Y7WUFDRCxNQUFNLEVBQUU7Z0JBQ04sVUFBVSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNyQixTQUFTLEVBQUUsSUFBSTthQUNoQjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtZQUUxRSxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztZQUNsQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUF3QixFQUFFLEVBQUU7Z0JBQzVFLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixJQUFJLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM3SCxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM1QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUM7Z0JBQ25ILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUMzQjtJQUNILENBQUM7SUFFRCxlQUFlLENBQUMsT0FBUTtRQUN0QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7Z0JBQzVHLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxXQUFXLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3BCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQUVELGtCQUFrQixDQUFDLFNBQTRDO1FBQzdELElBQUksaUJBQWlCLENBQUM7UUFDdEIsSUFBSSxTQUFTLENBQUMsYUFBYSxFQUFFO1lBQzNCLGlCQUFpQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDM0UsT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDbEMsT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFDbEQsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELDRDQUE0QztRQUM1QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxpQkFBaUIsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLHFEQUFxRDtRQUMzUCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsSUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztTQUN4QztRQUNELElBQUksU0FBUyxDQUFDLFNBQVMsRUFBRTtZQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztTQUM5QztRQUVELElBQUksU0FBUyxDQUFDLFVBQVUsRUFBRTtZQUN4QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQztTQUNoRDtRQUNELElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtZQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztTQUM1QztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxTQUFTLENBQUMsVUFBVSxDQUFDO2VBQ3ZHLENBQUMsU0FBUyxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsS0FBSyxJQUFJLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQztlQUM1SCxDQUFDLFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssSUFBSSxJQUFJLFNBQVMsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUN6SCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQztZQUN2QyxJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUM7WUFDakQsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO1lBQzdDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMvRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNwQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMzQjthQUFNO1lBQ0wsSUFBSSxTQUFTLENBQUMsVUFBVSxFQUFFO2dCQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7YUFDeEI7WUFDRCxJQUFJLFNBQVMsQ0FBQyxlQUFlLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQzthQUNsRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQzthQUM3QjtZQUNELElBQUksU0FBUyxDQUFDLGFBQWEsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsYUFBYSxDQUFDO2FBQzlDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzNCO1lBQ0QsSUFBSSxTQUFTLENBQUMsU0FBUyxFQUFFO2dCQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUM7YUFDdEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7YUFDdkI7WUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1lBQ2hDLElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUN0QixJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFO29CQUN6RixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM1QjtxQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtvQkFDbEQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2lCQUN4QjthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBYUQsd0JBQXdCO0lBRXhCLG9EQUFvRDtJQUNwRCxhQUFhLENBQUMsU0FBUyxFQUFFLE9BQU87UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDO1FBQ2xDLE1BQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQyxDQUFDLDBDQUEwQztRQUNuRSxNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsQ0FBQyxzQ0FBc0M7UUFDM0QsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxxQkFBcUI7UUFDbkIsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3RCLE9BQU8sR0FBRyxNQUFNLENBQUM7YUFDbEI7WUFDRCxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ2pCO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELFdBQVcsQ0FBQyxhQUFhO1FBQ3ZCLElBQUksUUFBUSxDQUFDO1FBQ2IsSUFBSSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQ3ZELFFBQVEsR0FBRyxFQUFFLENBQUM7U0FDZjthQUFNLElBQUksaUJBQWlCLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUM3RCxRQUFRLEdBQUcsRUFBRSxDQUFDO1NBQ2Y7YUFBTSxJQUFJLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDOUQ7NENBQ2dDO1lBQ2hDLFFBQVEsR0FBRyxFQUFFLENBQUM7U0FDZjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxjQUFjLENBQUMsUUFBUSxFQUFFLGVBQWU7UUFDdEMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixJQUFJLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztZQUM5QixNQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyx5QkFBeUIsQ0FBQztZQUN2TixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7WUFDOUIsb0JBQW9CLEdBQUc7Z0JBQ3JCLGNBQWMsRUFBRSxhQUFhLENBQUMsb0JBQW9CO2dCQUNsRCxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU87Z0JBQ3RCLE1BQU0sRUFBRSxFQUFFO2dCQUNWLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDO1lBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtvQkFDckQsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsTUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN0RCxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUN0QixNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7Z0NBQ2IsS0FBSyxDQUFDLElBQUksQ0FBQztvQ0FDVCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO29DQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtpQ0FDdkIsQ0FBQyxDQUFDOzZCQUNKO3dCQUNILENBQUMsQ0FBQyxDQUFDO3FCQUNKO29CQUNELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUM7aUJBQ2xDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7aUJBQy9CO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNOLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVEsQ0FBQyxRQUFRLEVBQUUsZUFBZTtRQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtRQUVwRSxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBb0NJO0lBQ0osY0FBYztJQUNkLHFCQUFxQixDQUFDLElBQUksRUFBRSxNQUFNO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDdkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtnQkFDakYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO2dCQUNyRSwyQ0FBMkM7Z0JBQzNDLHVFQUF1RTthQUN4RTtpQkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtnQkFDeEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2FBQ3RGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN0RjtRQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7UUFDdkYsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsSUFBSSxFQUFFLE1BQU07UUFDNUIsSUFBSSxpQkFBaUIsQ0FBQztRQUN0QixpQkFBaUIsR0FBRztZQUNsQixlQUFlLEVBQUUsS0FBSztZQUN0Qix1QkFBdUIsRUFBRTtnQkFDdkIsWUFBWSxFQUFFO29CQUNaLFFBQVEsRUFBRSxNQUFNO2lCQUNqQjthQUNGO1lBQ0QsZUFBZSxFQUFFO2dCQUNmLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTthQUNkO1NBQ0YsQ0FBQTtRQUNELE1BQU0sVUFBVSxHQUFHO1lBQ2pCLGlCQUFpQixFQUFFLGlCQUFpQjtZQUNwQyxvQkFBb0IsRUFBRTtnQkFDcEIsbUJBQW1CLEVBQUUsRUFBRTthQUN4QjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDO2FBQ2xELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSTttQkFDekcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDbkssSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO2dCQUNyRSwyQ0FBMkM7Z0JBQzNDLHVFQUF1RTthQUN4RTtpQkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7bUJBQ2pILFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7Z0JBQ3BGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDekU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO2FBQ3RGO1FBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztRQUN2RixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU07UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtZQUNyRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQzFDO0lBQ0gsQ0FBQztJQUVELHNCQUFzQixDQUFDLEtBQUs7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxNQUFNO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7SUFDOUIsQ0FBQztJQUVEOzs7UUFHSTtJQUVKLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxPQUFPO1FBQ2xDLElBQUksRUFBRSxHQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNwQyxJQUFJLEVBQUUsR0FBUSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQsOENBQThDO0lBQzlDLHNCQUFzQjtJQUN0QixrQ0FBa0M7SUFDbEMsOERBQThEO0lBQzlELG1CQUFtQjtJQUNuQixtQ0FBbUM7SUFDbkMsOEVBQThFO0lBQzlFLG1CQUFtQjtJQUNuQixJQUFJO0lBRUosb0RBQW9EO0lBQ3BELFFBQVE7UUFDTixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDbEksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUM7UUFFcEYsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUNoSSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBRXJJLE1BQU0sZUFBZSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDbkMsTUFBTSxXQUFXLEdBQUcsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xELE1BQU0sWUFBWSxHQUFHLGVBQWUsQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDcEQsTUFBTSxXQUFXLEdBQUcsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLEdBQUcsWUFBWSxHQUFHLEdBQUcsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztRQUV4RSxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQztRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRCw4RUFBOEU7UUFDOUUsa0dBQWtHO1FBQ2xHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLE1BQU0sR0FBRyxXQUFXLEVBQUUsWUFBWSxHQUFHLEdBQUcsR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQyxDQUFDO1FBQzNHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUV4RCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQzNHLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUU7d0JBQ3RILElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztxQkFDeEI7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7cUJBQzNCO2lCQUNGO2FBQ0Y7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwSixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7aUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNwQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7aUJBQ3hCO2FBQ0Y7WUFDRCxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUNILENBQUM7UUFFRixXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNyQixJQUFJLENBQUMsdUJBQXVCLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3BFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7WUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ3hDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNuRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO3dCQUN2RSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO3FCQUM1QjtpQkFDRjthQUNGO2lCQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDeEQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM5QixJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNoQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztvQkFDNUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztpQkFDeEI7YUFDRjtZQUNELFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQ0gsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNyQixJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN6QyxJQUFJLFNBQVMsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFdBQVcsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3BDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwQztRQUNILENBQUMsQ0FBQyxDQUNILENBQUM7SUFFSixDQUFDO0lBRUQsV0FBVztRQUNULElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN2RSxNQUFNLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDN0QsQ0FBQztDQU9GLENBQUE7O1lBMStCdUIsV0FBVztZQUNDLHVCQUF1QjtZQUNsQyxZQUFZO1lBQ1YsY0FBYztZQUNkLGNBQWM7WUFDcEIsU0FBUztZQUNaLFlBQVk7WUFDQSxpQkFBaUI7WUFDVix3QkFBd0I7WUFDbEIsOEJBQThCO1lBQzNDLGlCQUFpQjtZQUNyQixhQUFhO1lBQ1AsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNyQixpQkFBaUI7WUFDcEIsY0FBYztZQUNmLGFBQWE7WUFDZixXQUFXO1lBQ1osVUFBVTtZQUNGLGtCQUFrQjtZQUN6QixXQUFXO1lBQ0osa0JBQWtCOztBQXJKckM7SUFBVCxNQUFNLEVBQUU7Z0ZBQWdEO0FBNkN6RDtJQURDLFlBQVksQ0FBQyxZQUFZLENBQUM7MkVBQ1k7QUFHdkM7SUFEQyxZQUFZLENBQUMsY0FBYyxDQUFDO3lFQUNRO0FBR3JDO0lBREMsWUFBWSxDQUFDLGdCQUFnQixDQUFDOzJFQUNRO0FBR3ZDO0lBREMsU0FBUyxDQUFDLGNBQWMsQ0FBQzt3RUFDRDtBQTdEZCxrQ0FBa0M7SUFiOUMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGlDQUFpQztRQUMzQyw0ejhCQUEyRDtRQUUzRCxVQUFVLEVBQUU7WUFDVixPQUFPLENBQUMsY0FBYyxFQUFFO2dCQUN0QixLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsQ0FBQyxDQUFDO2dCQUN0RCxLQUFLLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7Z0JBQ3hELFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDM0QsVUFBVSxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUMzRCxDQUFDO1NBQ0g7O0tBQ0YsQ0FBQztHQUNXLGtDQUFrQyxDQWluQzlDO1NBam5DWSxrQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkcmVuLCBRdWVyeUxpc3QsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENoYW5nZURldGVjdG9yUmVmLCBFbGVtZW50UmVmLCBPbkRlc3Ryb3ksIFJlbmRlcmVyMiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGV4dGVuZE1vbWVudCB9IGZyb20gJ21vbWVudC1yYW5nZSc7XHJcbmltcG9ydCAqIGFzIE1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJzY3JpcHRpb24sIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGYWNldENoYW5nZUV2ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL2ZhY2V0L3NlcnZpY2VzL2ZhY2V0LWNoYW5nZS1ldmVudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtTWFwIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWVkaWFNYXRjaGVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCc7XHJcbmltcG9ydCB7IFNlYXJjaENoYW5nZUV2ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtY2hhbmdlLWV2ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVRhc2tNZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9kZWxpdmVyYWJsZS10YXNrL3NlcnZpY2UvZGVsaXZlcmFibGUudGFzay5tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvdmlldy1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoRGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZWFyY2gvc2VydmljZXMvc2VhcmNoLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IEluZGV4ZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvaW5kZXhlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGl0bGVDYXNlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3QvdGFza3MvdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FsZW5kYXJDb25zdGFudHMgfSBmcm9tICcuLi9zaGFyZWQvY29uc3RhbnRzL0NhbGVuZGFyLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1hdFNvcnREaXJlY3Rpb25zIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTWF0U29ydERpcmVjdGlvbic7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1NlYXJjaENvbmZpZ0NvbnN0YW50cyc7XHJcbmltcG9ydCB7IEN1c3RvbVNlbGVjdCB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1zZWxlY3Qvb2JqZWN0cy9DdXN0b21TZWxlY3RPYmplY3QnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNlYXJjaFJlcXVlc3QgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlcXVlc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBPVE1NTVBNRGF0YVR5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvT1RNTU1QTURhdGFUeXBlcyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9EZWxpdmVyYWJsZVR5cGVzJztcclxuaW1wb3J0IHsgVGFza0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Byb2plY3QvdGFza3MvdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIGFuaW1hdGUgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFJlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFBhcmFtcywgVGFza1NlYXJjaENvbmRpdGlvbnMgfSBmcm9tICcuLi9vYmplY3QvUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zJztcclxuaW1wb3J0IHsgR1JPVVBfQllfQUxMX0ZJTFRFUlMsIEdST1VQX0JZX0ZJTFRFUlMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL0dyb3VwQnlGaWx0ZXInO1xyXG5pbXBvcnQgeyBHcm91cEJ5RmlsdGVyVHlwZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL0dyb3VwQnlGaWx0ZXJUeXBlcyc7XHJcbmltcG9ydCB7IFJvdXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9yb3V0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmlsdGVySGVscGVyU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWx0ZXItaGVscGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTeW5jaHJvbml6ZVRhc2tGaWx0ZXJzQ29tcG9uZW50IH0gZnJvbSAnLi4vc3luY2hyb25pemUtdGFzay1maWx0ZXJzL3N5bmNocm9uaXplLXRhc2stZmlsdGVycy5jb21wb25lbnQnO1xyXG5cclxuY29uc3QgbW9tZW50ID0gZXh0ZW5kTW9tZW50KE1vbWVudCk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1yZXNvdXJjZS1tYW5hZ2VtZW50LXRvb2xiYXInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9yZXNvdXJjZS1tYW5hZ2VtZW50LXRvb2xiYXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3Jlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhci5jb21wb25lbnQuY3NzJ10sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcigncm90YXRlZFN0YXRlJywgW1xyXG4gICAgICBzdGF0ZSgnZGVmYXVsdCcsIHN0eWxlKHsgdHJhbnNmb3JtOiAncm90YXRlKDBkZWcpJyB9KSksXHJcbiAgICAgIHN0YXRlKCdyb3RhdGVkJywgc3R5bGUoeyB0cmFuc2Zvcm06ICdyb3RhdGUoMTgwZGVnKScgfSkpLFxyXG4gICAgICB0cmFuc2l0aW9uKCdyb3RhdGVkID0+IGRlZmF1bHQnLCBhbmltYXRlKCcyNTBtcyBlYXNlLW91dCcpKSxcclxuICAgICAgdHJhbnNpdGlvbignZGVmYXVsdCA9PiByb3RhdGVkJywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1pbicpKVxyXG4gICAgXSlcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZXNvdXJjZU1hbmFnZW1lbnRUb29sYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgcmVhZG9ubHkgY29tcG9uZW50UGFyYW1zOiBPYnNlcnZhYmxlPFJlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFBhcmFtcz4gPSB0aGlzLmFjdGl2YXRlZFJvdXRlLnF1ZXJ5UGFyYW1NYXAucGlwZShcclxuICAgIG1hcChwYXJhbXMgPT4gdGhpcy5yZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRSb3V0ZShwYXJhbXMpKSxcclxuICApO1xyXG4gIHByaXZhdGUgc3Vic2NyaXB0aW9uczogQXJyYXk8U3Vic2NyaXB0aW9uPiA9IFtdO1xyXG5cclxuICBAT3V0cHV0KCkgYWRkZWRSZXNvdXJjZUhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgLyogICBAVmlld0NoaWxkKFN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQpIHN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQ6IFN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQ7ICovXHJcblxyXG5cclxuICBlbmFibGVDYW1wYWlnbjtcclxuICBhcHBDb25maWc6IGFueTtcclxuICBncm91cEJ5RmlsdGVycyA9IFtdO1xyXG4gIGlzUmVzZXQgPSBmYWxzZTtcclxuICBpc1JvdXRlID0gZmFsc2U7XHJcbiAgc2VsZWN0ZWRHcm91cEJ5RmlsdGVyO1xyXG4gIGZhY2V0UmVzdHJpY3Rpb25MaXN0O1xyXG4gIGxldmVsO1xyXG4gIHZpZXdDb25maWc7XHJcbiAgLyogbW9tZW50UHRyID0gbW9tZW50O1xyXG4gIHRvZGF5ID0gdGhpcy5tb21lbnRQdHIoKS5mb3JtYXQoJ1lZWVktTU0tREQnKTsgKi9cclxuICBzZWFyY2hOYW1lO1xyXG4gIHNhdmVkU2VhcmNoTmFtZTtcclxuICBhZHZTZWFyY2hEYXRhO1xyXG4gIGZhY2V0RGF0YTtcclxuXHJcbiAgc2VsZWN0ZWRMaXN0RmlsdGVyO1xyXG4gIHNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlcjtcclxuICBlbWFpbExpbmtJZDtcclxuICBzZWxlY3RlZFNvcnQ7XHJcbiAgc2VsZWN0ZWRTb3J0Qnk7XHJcbiAgc2VsZWN0ZWRTb3J0T3JkZXI7XHJcblxyXG4gIHBhZ2VTaXplO1xyXG4gIHBhZ2U7XHJcbiAgc2VsZWN0ZWRQYWdlTnVtYmVyO1xyXG4gIHNraXA7XHJcbiAgdG9wOiBudW1iZXI7XHJcblxyXG4gIHRvdGFsTGlzdERhdGFDb3VudCA9IDA7XHJcblxyXG4gIGRpc3BsYXlUeXBlID0gJ21vbnRoJztcclxuICB2aWV3VHlwZUNlbGxTaXplID0gMTUwOy8vIDM1O1xyXG4gIG1vbWVudFB0ciA9IG1vbWVudDtcclxuICBjdXJyZW50Vmlld1N0YXJ0RGF0ZTogbW9tZW50Lk1vbWVudDtcclxuICBjdXJyZW50Vmlld0VuZERhdGU6IG1vbWVudC5Nb21lbnQ7XHJcbiAgZGF0ZXMgPSBbXTtcclxuICBzaG93V2Vla2VuZHMgPSB0cnVlO1xyXG5cclxuICBAVmlld0NoaWxkcmVuKCdkYXRlSGVhZGVyJylcclxuICBjYWxlbmRhckhlYWRlcnM6IFF1ZXJ5TGlzdDxFbGVtZW50UmVmPjtcclxuXHJcbiAgQFZpZXdDaGlsZHJlbignY2FsZW5kYXJDZWxsJylcclxuICBjYWxlbmRhckNlbGxzOiBRdWVyeUxpc3Q8RWxlbWVudFJlZj47XHJcblxyXG4gIEBWaWV3Q2hpbGRyZW4oJ3BhbmVsQ29udGFpbmVyJylcclxuICBwYW5lbENvbnRhaW5lcnM6IFF1ZXJ5TGlzdDxFbGVtZW50UmVmPjtcclxuXHJcbiAgQFZpZXdDaGlsZCgnY2FsZW5kYXJWaWV3JylcclxuICBjYWxlbmRhclZpZXc6IEVsZW1lbnRSZWY7XHJcblxyXG4gIHRhc2tBbGxvY2F0aW9uT3ZlcmxvYWRTdHlsZSA9IHt9O1xyXG4gIHRhc2tBbGxvY2F0aW9uU3R5bGUgPSB7fTtcclxuICBjZWxsU3R5bGUgPSB7fTtcclxuXHJcbiAgY3VycmVudERhdGU7XHJcblxyXG4gIHNlbGVjdGVkTW9udGg7XHJcbiAvKiAgbW9udGhzID0gWydKYW51YXJ5JywgJ2ZlYnJ1YXJ5JywgJ01hcmNoJywgJ0FwcmlsJywgJ01heScsICdKdW5lJywgJ0p1bHknLCAnQXVndXN0JywgJ1NlcHRlbWJlcicsICdPY3RvYmVyJywgJ05vdmVtYmVyJywgJ0RlY2VtYmVyJ107XHJcbiAgKi8gZmFjZXRFeHBhbnNpb25TdGF0ZSA9IHRydWU7XHJcblxyXG4gIG15VGFza1ZpZXdDb25maWdEZXRhaWxzOiBWaWV3Q29uZmlnID0gbnVsbDtcclxuICBzZWFyY2hDb25maWdJZCA9IC0xO1xyXG5cclxuICBzZWxlY3RlZFBhZ2VTaXplO1xyXG4gIGNvbXBvbmVudFJlbmRlcmluZyA9IGZhbHNlO1xyXG4gIGlzUGFnZVJlZnJlc2ggPSB0cnVlO1xyXG4gIGlzRGF0YVJlZnJlc2ggPSB0cnVlO1xyXG4gIHByZXZpb3VzUmVxdWVzdDtcclxuXHJcbiAgc3RvcmVTZWFyY2hjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gbnVsbDtcclxuXHJcbiAgc29ydGFibGVGaWVsZHM6IEFycmF5PGFueT4gPSBbXTtcclxuXHJcbiAgc2VhcmNoQ29uZGl0aW9ucyA9IFtdO1xyXG4gIGVtYWlsU2VhcmNoQ29uZGl0aW9uID0gW107XHJcblxyXG4gIGlzQXBwcm92ZXI7XHJcbiAgaXNNZW1iZXI7XHJcbiAgaXNSZXZpZXdlcjtcclxuXHJcbiAgdmFsaWRhdGVGYWNldCA9IGZhbHNlO1xyXG4gIHNpZGViYXJTdHlsZSA9IHt9O1xyXG5cclxuICBzY3JvbGxlZCA9IGZhbHNlO1xyXG4gIGlzV2Vla1ZpZXcgPSBmYWxzZTtcclxuICBwcmV2aW91c0ZsYWcgPSBmYWxzZTtcclxuICBuZXh0RmxhZyA9IGZhbHNlO1xyXG5cclxuICBtb2JpbGVRdWVyeTogTWVkaWFRdWVyeUxpc3Q7XHJcbiAgcHJpdmF0ZSBtb2JpbGVRdWVyeUxpc3RlbmVyOiAoKSA9PiB2b2lkO1xyXG4gIGFsbFVzZXJzID0gW107XHJcbiAgVU5BU1NJR05FRF9UQVNLX1NFQVJDSF9MSU1JVCA9IDEwMDtcclxuXHJcbiAgZXZlbnREaXNwbGF5ID0ge1xyXG4gICAgZGlzcGxheVZpZXc6ICdkYXlWaWV3JyxcclxuICAgIGR1cmF0aW9uOiB7IGRheXM6IDMwIH0sXHJcbiAgICByZXNvdXJjZXM6IFtdXHJcbiAgfTtcclxuICBhbGxGaWx0ZXJlZFJlc291cmNlcyA9IFtdO1xyXG4gIHVzZXJzVW5hdmFpbGFiaXR5ID0gW107XHJcbiAgYWxsVXNlcnNDYXBhY2l0eSA9IFtdO1xyXG4gIHByb2plY3RMaXN0RGF0YSA9IFtdO1xyXG4gIGRlbGl2ZXJhYmxlRGF0YSA9IFtdO1xyXG5cclxuICBoZWlnaHQ7XHJcblxyXG4gIHRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcblxyXG4gIGRlbGl2ZXJhYmxlRGF0YXM6IGFueTtcclxuXHJcbiAgc2VsZWN0ZWRVc2VycztcclxuICBzZWxlY3RlZEl0ZW1zO1xyXG5cclxuICByZW1vdmVkRmlsdGVyO1xyXG4gIGFsbG9jYXRpb247XHJcblxyXG4gIHllYXI7XHJcblxyXG5cclxuICBNYXRoID0gTWF0aDtcclxuICBcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZmFjZXRDaGFuZ2VFdmVudFNlcnZpY2U6IEZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlLFxyXG4gICAgcHVibGljIHJvdXRlU2VydmljZTogUm91dGVTZXJ2aWNlLFxyXG4gICAgcHVibGljIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgIHB1YmxpYyBtZWRpYTogTWVkaWFNYXRjaGVyLFxyXG4gICAgcHVibGljIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIHB1YmxpYyBzZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2U6IFNlYXJjaENoYW5nZUV2ZW50U2VydmljZSxcclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVRhc2tNZXRhZGF0YVNlcnZpY2U6IERlbGl2ZXJhYmxlVGFza01ldGFkYXRhU2VydmljZSxcclxuICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpbHRlckhlbHBlclNlcnZpY2U6IEZpbHRlckhlbHBlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2VhcmNoRGF0YVNlcnZpY2U6IFNlYXJjaERhdGFTZXJ2aWNlLFxyXG4gICAgcHVibGljIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZSxcclxuICAgIHB1YmxpYyB0aXRsZWNhc2VQaXBlOiBUaXRsZUNhc2VQaXBlLFxyXG4gICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5tb2JpbGVRdWVyeSA9IHRoaXMubWVkaWEubWF0Y2hNZWRpYSgnKG1heC13aWR0aDogNjAwcHgpJyk7XHJcbiAgICB0aGlzLm1vYmlsZVF1ZXJ5TGlzdGVuZXIgPSAoKSA9PiB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgIGlmICh0aGlzLm1vYmlsZVF1ZXJ5Lm1hdGNoZXMpIHtcclxuICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkZXByZWNhdGlvblxyXG4gICAgICB0aGlzLm1vYmlsZVF1ZXJ5LmFkZExpc3RlbmVyKHRoaXMubW9iaWxlUXVlcnlMaXN0ZW5lcik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyBQUkVWSU9VUyBuYXZpZ2F0aW9uIGZvciBDYWxlbmRhclxyXG4gIGNhbGVuZGFyUHJldmlvdXMoKSB7XHJcblxyXG4gICAgdGhpcy5wcmV2aW91c0ZsYWcgPSBmYWxzZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRNb250aCk7XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBDYWxlbmRhckNvbnN0YW50cy5tb250aHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tpICsgMV0gPT09IHRoaXMuc2VsZWN0ZWRNb250aCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tpXSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE1vbnRoID0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2ldO1xyXG4gICAgICAgIHRoaXMucHJldmlvdXNGbGFnID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfSBlbHNlIGlmIChDYWxlbmRhckNvbnN0YW50cy5tb250aHNbMF0gPT09IHRoaXMuc2VsZWN0ZWRNb250aCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRNb250aCA9IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tDYWxlbmRhckNvbnN0YW50cy5tb250aHMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgdGhpcy55ZWFyID0gdGhpcy55ZWFyIC0gMTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIHRvIGhhdmUgZXhhY3QgYWxsIGRhdGVcclxuICAgIGxldCBub09mRGF5cyA9IHRoaXMuZ2V0Tm9PZkRheXModGhpcy5zZWxlY3RlZE1vbnRoKTtcclxuICAgIGxldCBkYXlzO1xyXG5cclxuICAgIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnd2VlaycpIHtcclxuICAgICAgY29uc3Qgc3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuc3RhcnRPZignd2VlaycpLnN1YnRyYWN0KDEsICd3ZWVrJyk7XHJcbiAgICAgIGNvbnN0IGVuZERhdGUgPSB0aGlzLm1vbWVudFB0cih0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlKS5zdWJ0cmFjdCgxLCAnZGF5Jyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gc3RhcnREYXRlO1xyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IGVuZERhdGU7XHJcblxyXG4gICAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKHN0YXJ0RGF0ZSwgZW5kRGF0ZSk7XHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnbW9udGgnKSB7XHJcbiAgICAgIGNvbnN0IHN0YXJ0RGF0ZSA9IHRoaXMubW9tZW50UHRyKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUpLnN1YnRyYWN0KG5vT2ZEYXlzLCAnZGF5cycpOyAvLyAzMFxyXG4gICAgICBjb25zdCBlbmREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuc3VidHJhY3QoMSwgJ2RheScpO1xyXG5cclxuICAgICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IHN0YXJ0RGF0ZTtcclxuICAgICAgdGhpcy5jdXJyZW50Vmlld0VuZERhdGUgPSBlbmREYXRlO1xyXG5cclxuICAgICAgY29uc3QgcmFuZ2UgPSB0aGlzLm1vbWVudFB0ci5yYW5nZShzdGFydERhdGUsIGVuZERhdGUpO1xyXG4gICAgICBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5zaG93V2Vla2VuZHMpIHtcclxuXHJcbiAgICAgIHRoaXMuZGF0ZXMgPSBkYXlzO1xyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgIC8vIHJlbW92ZSBzYXQgYW5kIHN1biBmcm9tIGRhdGUgcmFuZ2VcclxuICAgICAgdGhpcy5kYXRlcyA9IGRheXMuZmlsdGVyKGQgPT4gZC5pc29XZWVrZGF5KCkgIT09IDYgJiYgZC5pc29XZWVrZGF5KCkgIT09IDcpO1xyXG4gICAgfVxyXG4gICAgLy8gIHRoaXMucmVzaXplQ2FsZW5kYXIoKTtcclxuICAgIHRoaXMucmVsb2FkKGZhbHNlKTtcclxuICB9XHJcblxyXG4gIC8vIE5FWFQgbmF2aWdhdGlvbiBmb3IgQ2FsZW5kYXJcclxuICBjYWxlbmRhck5leHQoKSB7XHJcbiAgICB0aGlzLm5leHRGbGFnID0gZmFsc2U7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkTW9udGgpO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRNb250aCA9PT0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW0NhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGggLSAxXSkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkTW9udGggPSBDYWxlbmRhckNvbnN0YW50cy5tb250aHNbMF07XHJcbiAgICAgIHRoaXMueWVhciA9IHRoaXMueWVhciArIDE7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGggJiYgdGhpcy5zZWxlY3RlZE1vbnRoICE9PSBDYWxlbmRhckNvbnN0YW50cy5tb250aHNbQ2FsZW5kYXJDb25zdGFudHMubW9udGhzLmxlbmd0aCAtIDFdOyBpKyspIHtcclxuICAgICAgICBpZiAoQ2FsZW5kYXJDb25zdGFudHMubW9udGhzW2ldID09PSB0aGlzLnNlbGVjdGVkTW9udGgpIHtcclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhDYWxlbmRhckNvbnN0YW50cy5tb250aHNbaSArIDFdKTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRNb250aCA9IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tpICsgMV07XHJcbiAgICAgICAgICB0aGlzLm5leHRGbGFnID0gdHJ1ZTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qIGlmICh0aGlzLnNlbGVjdGVkTW9udGggPT09IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tDYWxlbmRhckNvbnN0YW50cy5tb250aHMubGVuZ3RoIC0gMV0pIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZE1vbnRoID0gQ2FsZW5kYXJDb25zdGFudHMubW9udGhzWzBdO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGxldCBub09mRGF5cyA9IHRoaXMuZ2V0Tm9PZkRheXModGhpcy5zZWxlY3RlZE1vbnRoKTtcclxuXHJcbiAgICBsZXQgZGF5cztcclxuXHJcbiAgICBpZiAodGhpcy5kaXNwbGF5VHlwZSA9PT0gJ3dlZWsnKSB7XHJcbiAgICAgIGNvbnN0IHN0YXJ0RGF0ZSA9IHRoaXMubW9tZW50UHRyKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUpLmVuZE9mKCd3ZWVrJykuYWRkKDEsICdkYXknKTtcclxuICAgICAgY29uc3QgZW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKHN0YXJ0RGF0ZSkuYWRkKDEsICd3ZWVrJyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gc3RhcnREYXRlO1xyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IGVuZERhdGU7XHJcblxyXG4gICAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKHN0YXJ0RGF0ZSwgZW5kRGF0ZSk7XHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmRpc3BsYXlUeXBlID09PSAnbW9udGgnKSB7XHJcbiAgICAgIGNvbnN0IHN0YXJ0RGF0ZSA9IHRoaXMubW9tZW50UHRyKHRoaXMuY3VycmVudFZpZXdFbmREYXRlKS5hZGQoMSwgJ2RheScpO1xyXG4gICAgICBjb25zdCBlbmREYXRlID0gdGhpcy5tb21lbnRQdHIodGhpcy5jdXJyZW50Vmlld0VuZERhdGUpLmFkZChub09mRGF5cywgJ2RheXMnKTsgLy8gMzBcclxuXHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUgPSBzdGFydERhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gZW5kRGF0ZTtcclxuXHJcbiAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2Uoc3RhcnREYXRlLCBlbmREYXRlKTtcclxuICAgICAgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuc2hvd1dlZWtlbmRzKSB7XHJcbiAgICAgIHRoaXMuZGF0ZXMgPSBkYXlzO1xyXG4gICAgfSBlbHNlIHtcclxuXHJcbiAgICAgIC8vIHJlbW92ZSBzYXQgYW5kIHN1biBmcm9tIHRoZSBkYXRlIHJhbmdlXHJcbiAgICAgIHRoaXMuZGF0ZXMgPSBkYXlzLmZpbHRlcihkID0+IGQuaXNvV2Vla2RheSgpICE9PSA2ICYmIGQuaXNvV2Vla2RheSgpICE9PSA3KTtcclxuICAgIH1cclxuICAgIC8vIHRoaXMucmVzaXplQ2FsZW5kYXIoKTtcclxuICAgIHRoaXMucmVsb2FkKGZhbHNlKTtcclxuICB9XHJcblxyXG4gIGZhY2V0VG9nZ2xlZChldmVudCkge1xyXG4gICAgdGhpcy5mYWNldEV4cGFuc2lvblN0YXRlID0gZXZlbnQ7XHJcblxyXG4gIH1cclxuXHJcbiAgc2VsZWN0ZWRVc2Vycih1c2VyKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkVXNlcnMgPSB1c2VyO1xyXG4gIH1cclxuXHJcbiAgc2VsZWN0ZWRGaWx0ZXIoZmlsdGVyKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkSXRlbXMgPSBmaWx0ZXI7XHJcbiAgfVxyXG5cclxuICByZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRSb3V0ZShwYXJhbXM6IFBhcmFtTWFwKTogUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zIHtcclxuICAgIGNvbnN0IGdyb3VwQnlGaWx0ZXIgPSBwYXJhbXMuZ2V0KCdncm91cEJ5RmlsdGVyJykgfHwgJyc7XHJcbiAgICBjb25zdCBkZWxpdmVyYWJsZVRhc2tGaWx0ZXIgPSBwYXJhbXMuZ2V0KCdkZWxpdmVyYWJsZVRhc2tGaWx0ZXInKSB8fCAnJztcclxuICAgIGNvbnN0IGxpc3REZXBlbmRlbnRGaWx0ZXIgPSBwYXJhbXMuZ2V0KCdsaXN0RGVwZW5kZW50RmlsdGVyJykgfHwgJyc7XHJcbiAgICBjb25zdCBlbWFpbExpbmtJZCA9IHBhcmFtcy5nZXQoJ2VtYWlsTGlua0lkJykgfHwgJyc7XHJcbiAgICBjb25zdCBzb3J0QnkgPSBwYXJhbXMuZ2V0KCdzb3J0QnknKSB8fCAnJztcclxuICAgIGNvbnN0IHNvcnRPcmRlciA9IHBhcmFtcy5nZXQoJ3NvcnRPcmRlcicpID09PSAnYXNjJyA/IE1hdFNvcnREaXJlY3Rpb25zLkFTQyA6IChwYXJhbXMuZ2V0KCdzb3J0T3JkZXInKSA9PT0gJ2Rlc2MnID8gTWF0U29ydERpcmVjdGlvbnMuREVTQyA6IG51bGwpO1xyXG4gICAgY29uc3QgcGFnZU51bWJlciA9IHBhcmFtcy5nZXQoJ3BhZ2VOdW1iZXInKSA/IE51bWJlcihwYXJhbXMuZ2V0KCdwYWdlTnVtYmVyJykpIDogbnVsbDtcclxuICAgIGNvbnN0IHBhZ2VTaXplID0gcGFyYW1zLmdldCgncGFnZVNpemUnKSA/IE51bWJlcihwYXJhbXMuZ2V0KCdwYWdlU2l6ZScpKSA6IG51bGw7XHJcbiAgICBjb25zdCBzZWFyY2hOYW1lID0gcGFyYW1zLmdldCgnc2VhcmNoTmFtZScpID8gcGFyYW1zLmdldCgnc2VhcmNoTmFtZScpIDogbnVsbDtcclxuICAgIGNvbnN0IHNhdmVkU2VhcmNoTmFtZSA9IHBhcmFtcy5nZXQoJ3NhdmVkU2VhcmNoTmFtZScpID8gcGFyYW1zLmdldCgnc2F2ZWRTZWFyY2hOYW1lJykgOiBudWxsO1xyXG4gICAgY29uc3QgYWR2U2VhcmNoRGF0YSA9IHBhcmFtcy5nZXQoJ2FkdlNlYXJjaERhdGEnKSA/IHBhcmFtcy5nZXQoJ2FkdlNlYXJjaERhdGEnKSA6IG51bGw7XHJcbiAgICBjb25zdCBmYWNldERhdGEgPSBwYXJhbXMuZ2V0KCdmYWNldERhdGEnKSA/IHBhcmFtcy5nZXQoJ2ZhY2V0RGF0YScpIDogbnVsbDtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGdyb3VwQnlGaWx0ZXIsXHJcbiAgICAgIGRlbGl2ZXJhYmxlVGFza0ZpbHRlcixcclxuICAgICAgbGlzdERlcGVuZGVudEZpbHRlcixcclxuICAgICAgZW1haWxMaW5rSWQsXHJcbiAgICAgIHNvcnRCeSxcclxuICAgICAgc29ydE9yZGVyLFxyXG4gICAgICBwYWdlTnVtYmVyLFxyXG4gICAgICBwYWdlU2l6ZSxcclxuICAgICAgc2VhcmNoTmFtZSxcclxuICAgICAgc2F2ZWRTZWFyY2hOYW1lLFxyXG4gICAgICBhZHZTZWFyY2hEYXRhLFxyXG4gICAgICBmYWNldERhdGFcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBzZXRMZXZlbCgpIHtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkR3JvdXBCeUZpbHRlci52YWx1ZSA9PT0gR3JvdXBCeUZpbHRlclR5cGVzLkdST1VQX0JZX0NBTVBBSUdOKSB7XHJcbiAgICAgIHRoaXMubGV2ZWwgPSBNUE1fTEVWRUxTLkNBTVBBSUdOO1xyXG4gICAgICB0aGlzLnZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuUk1fR1JPVVBfQllfQ0FNUEFJR047XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyLnZhbHVlID09PSBHcm91cEJ5RmlsdGVyVHlwZXMuR1JPVVBfQllfUFJPSkVDVCkge1xyXG4gICAgICB0aGlzLmxldmVsID0gTVBNX0xFVkVMUy5QUk9KRUNUO1xyXG4gICAgICB0aGlzLnZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuUk1fR1JPVVBfQllfUFJPSkVDVDtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5zZWxlY3RlZEdyb3VwQnlGaWx0ZXIudmFsdWUgPT09IEdyb3VwQnlGaWx0ZXJUeXBlcy5HUk9VUF9CWV9UQVNLKSB7XHJcbiAgICAgIHRoaXMubGV2ZWwgPSBNUE1fTEVWRUxTLlRBU0s7XHJcbiAgICAgIHRoaXMudmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5STV9HUk9VUF9CWV9UQVNLO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ3JvdXBCeUZpbHRlckNoYW5nZShvcHRpb246IEN1c3RvbVNlbGVjdCkge1xyXG4gICAgdGhpcy5pc1Jlc2V0ID0gdHJ1ZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyID0gb3B0aW9uO1xyXG4gICAgdGhpcy5mYWNldFJlc3RyaWN0aW9uTGlzdCA9IHt9O1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIHRoaXMuZmFjZXRDaGFuZ2VFdmVudFNlcnZpY2UucmVzZXRGYWNldCgpO1xyXG4gICAgdGhpcy5zZXRMZXZlbCgpO1xyXG4gICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgIGlmICghKHRoaXMuc2VhcmNoTmFtZSB8fCB0aGlzLnNhdmVkU2VhcmNoTmFtZSB8fCB0aGlzLmFkdlNlYXJjaERhdGEgfHwgdGhpcy5mYWNldERhdGEpKSB7XHJcbiAgICAgIHRoaXMuaXNSb3V0ZSA9IHRydWU7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJvdXRlQ29tcG9uZW50VmlldygpO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICB9XHJcblxyXG4gIHJlc2V0UGFnaW5hdGlvbigpIHtcclxuICAgIHRoaXMucGFnZSA9IDE7XHJcbiAgICB0aGlzLnNraXAgPSAwO1xyXG4gIH1cclxuXHJcbiAgcm91dGVDb21wb25lbnRWaWV3KCkge1xyXG4gICAgY29uc3QgcXVlcnlQYXJhbXM6IFJlc291cmNlTWFuYWdlbWVudENvbXBvbmVudFBhcmFtcyA9IHt9O1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcm91cEJ5RmlsdGVyLnZhbHVlKSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydncm91cEJ5RmlsdGVyJ10gPSB0aGlzLnNlbGVjdGVkR3JvdXBCeUZpbHRlci52YWx1ZTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNlbGVjdGVkU29ydCkge1xyXG4gICAgICBxdWVyeVBhcmFtc1snc29ydEJ5J10gPSB0aGlzLnNlbGVjdGVkU29ydEJ5ID8gdGhpcy5zZWxlY3RlZFNvcnRCeSA6IG51bGw7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydzb3J0T3JkZXInXSA9IHRoaXMuc2VsZWN0ZWRTb3J0T3JkZXIgPyB0aGlzLnNlbGVjdGVkU29ydE9yZGVyIDogbnVsbDtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNlYXJjaE5hbWUpIHtcclxuICAgICAgcXVlcnlQYXJhbXNbJ3NlYXJjaE5hbWUnXSA9IHRoaXMuc2VhcmNoTmFtZTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNhdmVkU2VhcmNoTmFtZSkge1xyXG4gICAgICBxdWVyeVBhcmFtc1snc2F2ZWRTZWFyY2hOYW1lJ10gPSB0aGlzLnNhdmVkU2VhcmNoTmFtZTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmFkdlNlYXJjaERhdGEpIHtcclxuICAgICAgcXVlcnlQYXJhbXNbJ2FkdlNlYXJjaERhdGEnXSA9IHRoaXMuYWR2U2VhcmNoRGF0YTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmZhY2V0RGF0YSkge1xyXG4gICAgICBxdWVyeVBhcmFtc1snZmFjZXREYXRhJ10gPSB0aGlzLmZhY2V0RGF0YTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnBhZ2VTaXplKSB7XHJcbiAgICAgIHF1ZXJ5UGFyYW1zWydwYWdlU2l6ZSddID0gdGhpcy5wYWdlU2l6ZTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnBhZ2UpIHtcclxuICAgICAgaWYgKHRoaXMuaXNSZXNldCkge1xyXG4gICAgICAgIHRoaXMucmVzZXRQYWdpbmF0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgdGhpcy5pc1Jlc2V0ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgcXVlcnlQYXJhbXNbJ3BhZ2VOdW1iZXInXSA9IHRoaXMucGFnZTtcclxuICAgIH1cclxuICAgIHRoaXMucm91dGVTZXJ2aWNlLmdvVG9SZXNvdXJjZU1hbmFnZW1lbnQocXVlcnlQYXJhbXMsIHRoaXMuc2VhcmNoTmFtZSwgdGhpcy5zYXZlZFNlYXJjaE5hbWUsIHRoaXMuYWR2U2VhcmNoRGF0YSwgdGhpcy5mYWNldERhdGEpO1xyXG4gIH1cclxuXHJcbiAgLy8gdG9nZ2xlIG1vbnRoICwgd2VlayBhbmQgZGF5IHZpZXdcclxuICB0b2dnbGVWaWV3KHZpZXdUeXBlKSB7XHJcblxyXG4gICAgY29uc29sZS5sb2coJ3ZpZXdUeXBlJywgdmlld1R5cGUpXHJcbiAgICBsZXQgZGF5cztcclxuXHJcbiAgICB0aGlzLmRpc3BsYXlUeXBlID0gdmlld1R5cGU7XHJcbiAgICBjb25zdCBpc1JlaW5pdGlhbGl6ZSA9IHRydWU7XHJcblxyXG4gICAgaWYgKHRoaXMuZGlzcGxheVR5cGUgPT09ICd3ZWVrJykge1xyXG5cclxuICAgICAgdGhpcy52aWV3VHlwZUNlbGxTaXplID0gMTUwO1xyXG5cclxuICAgICAgY29uc3QgdG9kYXkgPSB0aGlzLm1vbWVudFB0cih7fSk7XHJcblxyXG4gICAgICBjb25zdCBmcm9tRGF0ZSA9IHRoaXMubW9tZW50UHRyKHt9KS5zdGFydE9mKCd3ZWVrJyk7XHJcbiAgICAgIGNvbnN0IHRvRGF0ZSA9IHRoaXMubW9tZW50UHRyKHt9KS5lbmRPZignd2VlaycpO1xyXG5cclxuICAgICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IGZyb21EYXRlO1xyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IHRvRGF0ZTtcclxuXHJcbiAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2UoZnJvbURhdGUsIHRvRGF0ZSk7XHJcblxyXG4gICAgICBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5kaXNwbGF5VHlwZSA9PT0gJ2RheScpIHtcclxuICAgICAgY29uc3QgZnJvbURhdGUgPSB0aGlzLm1vbWVudFB0cih7fSk7XHJcbiAgICAgIGNvbnN0IHRvRGF0ZSA9IHRoaXMubW9tZW50UHRyKCkuYWRkKDAsICdkYXlzJyk7XHJcblxyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlID0gZnJvbURhdGU7XHJcbiAgICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gdG9EYXRlO1xyXG5cclxuICAgICAgY29uc3QgcmFuZ2UgPSB0aGlzLm1vbWVudFB0ci5yYW5nZShmcm9tRGF0ZSwgdG9EYXRlKTtcclxuXHJcbiAgICAgIGRheXMgPSBBcnJheS5mcm9tKHJhbmdlLmJ5KCdkYXlzJykpO1xyXG5cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudmlld1R5cGVDZWxsU2l6ZSA9IDM1O1xyXG5cclxuICAgICAgY29uc3QgZnJvbURhdGUgPSB0aGlzLm1vbWVudFB0cih7fSk7XHJcbiAgICAgIGNvbnN0IHRvRGF0ZSA9IHRoaXMubW9tZW50UHRyKCkuYWRkKDI5LCAnZGF5cycpO1xyXG5cclxuICAgICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IGZyb21EYXRlO1xyXG4gICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IHRvRGF0ZTtcclxuXHJcbiAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5tb21lbnRQdHIucmFuZ2UoZnJvbURhdGUsIHRvRGF0ZSk7XHJcblxyXG4gICAgICBkYXlzID0gQXJyYXkuZnJvbShyYW5nZS5ieSgnZGF5cycpKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5kYXRlcyA9IGRheXM7XHJcblxyXG4gICAgaWYgKCF0aGlzLnNob3dXZWVrZW5kcykgeyAvLyBkbyBub3Qgc2hvdyB3ZWVrZW5kc1xyXG4gICAgICAvLyByZW1vdmUgc2F0IGFuZCBzdW4gZnJvbSB0aGUgZGF0ZSByYW5nZVxyXG4gICAgICB0aGlzLmRhdGVzID0gZGF5cy5maWx0ZXIoZCA9PiBkLmlzb1dlZWtkYXkoKSAhPT0gNiAmJiBkLmlzb1dlZWtkYXkoKSAhPT0gNyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdGhpcy5yZXNpemVDYWxlbmRhcigpO1xyXG4gIH1cclxuXHJcbiAgLy8gUmVzaXppbmcgdGhlIGNhbGVuZGFyIHNpemUgYmFzZWQgb24gdGhlIHVzZXJzIGRpc3BsYXkgYW5kIGZvciBtb250aCBhbmQgd2VlayB2aWV3XHJcbiAgcmVzaXplQ2FsZW5kYXIoKSB7XHJcblxyXG4gICAgdGhpcy5jYWxjdWxhdGVWaWV3U2l6ZSgpO1xyXG5cclxuICAgIHRoaXMuY2FsZW5kYXJIZWFkZXJzLmZvckVhY2goKChlbGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGVsZW0ubmF0aXZlRWxlbWVudCwgJ3dpZHRoJywgdGhpcy52aWV3VHlwZUNlbGxTaXplICsgJ3B4Jyk7XHJcbiAgICB9KSk7XHJcblxyXG4gICAgdGhpcy5jYWxlbmRhckNlbGxzLmZvckVhY2goKChlbGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKGVsZW0ubmF0aXZlRWxlbWVudCwgJ3dpZHRoJywgdGhpcy52aWV3VHlwZUNlbGxTaXplICsgJ3B4Jyk7XHJcbiAgICB9KSk7XHJcblxyXG4gIH1cclxuXHJcbiAgLy8gY2FsY3VsYXRlIHRoZSB2aWV3IHNpemUgZm9yIGNhbGVuZGFyIGNvbnRhaW5lclxyXG4gIGNhbGN1bGF0ZVZpZXdTaXplKCkge1xyXG5cclxuICAgIGNvbnN0IHdpbmRvd1dpZHRoID0gd2luZG93LmlubmVyV2lkdGg7XHJcblxyXG4gICAgLy8gaXRzIGFsd2F5cyBjb25zdGFudCB2YWx1ZSB0aGFuIHRoZSB3aW5kb3cgd2lkdGggdG8gZml0IGluIGNvcnJlY3RseSBpbiBweFxyXG4gICAgY29uc3QgcmlnaHRNYXJnaW4gPSAyNTU7XHJcblxyXG4gICAgLy8gdGhpcyBpcyBnb2luZyB0aGUgYmUgdGhlIGNhbGVuZGFyIGNvbnRhaW5lciB3aWR0aFxyXG4gICAgY29uc3QgY2FsZW5kYXJWaWV3V2lkdGggPSBNYXRoLmZsb29yKHdpbmRvd1dpZHRoIC0gcmlnaHRNYXJnaW4pO1xyXG5cclxuICAgIHRoaXMudmlld1R5cGVDZWxsU2l6ZSA9IE1hdGguZmxvb3IoY2FsZW5kYXJWaWV3V2lkdGggLyB0aGlzLmRhdGVzLmxlbmd0aCk7XHJcblxyXG4gICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmNhbGVuZGFyVmlldy5uYXRpdmVFbGVtZW50LCAnd2lkdGgnLCBjYWxlbmRhclZpZXdXaWR0aCArICdweCcpO1xyXG5cclxuICAgIHRoaXMudGFza0FsbG9jYXRpb25PdmVybG9hZFN0eWxlID0ge1xyXG4gICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICdyZWQnLFxyXG4gICAgICB3aWR0aDogKHRoaXMudmlld1R5cGVDZWxsU2l6ZSkgKyAncHgnXHJcbiAgICB9O1xyXG5cclxuICAgIHRoaXMudGFza0FsbG9jYXRpb25TdHlsZSA9IHtcclxuICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnIzA4OTVDRScsXHJcbiAgICAgIHdpZHRoOiAodGhpcy52aWV3VHlwZUNlbGxTaXplKSArICdweCdcclxuICAgIH07XHJcblxyXG4gICAgdGhpcy5jZWxsU3R5bGUgPSB7XHJcbiAgICAgIHdpZHRoOiAodGhpcy52aWV3VHlwZUNlbGxTaXplICsgMSkgKyAncHgnXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgZ2V0UHJvcGVydHkoZGVsaXZlcmFibGVEYXRhLCBtYXBwZXJOYW1lKSB7XHJcbiAgICAvLyBjb25zdCBsZXZlbCA9IHRoaXMuaXNSZXZpZXdlciA/IE1QTV9MRVZFTFMuREVMSVZFUkFCTEVfUkVWSUVXIDogTVBNX0xFVkVMUy5ERUxJVkVSQUJMRTtcclxuICAgIGNvbnN0IGRpc3BsYXlDb2x1bW4gPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBtYXBwZXJOYW1lLCBNUE1fTEVWRUxTLkRFTElWRVJBQkxFKTtcclxuICAgIHJldHVybiB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bW4sIGRlbGl2ZXJhYmxlRGF0YSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgICAqIEBzaW5jZSBNUE1WMy05NDZcclxuICAgICAqIEBwYXJhbSBkZWxpdmVyYWJsZVR5cGVcclxuICAgICAqL1xyXG4gIGdldERlbGl2ZXJhYmxlVHlwZUljb24oZGVsaXZlcmFibGVUeXBlKSB7XHJcbiAgICByZXR1cm4gdGhpcy5kZWxpdmVyYWJsZVNlcnZpY2UuZ2V0RGVsaXZlcmFibGVUeXBlSWNvbihkZWxpdmVyYWJsZVR5cGUpO1xyXG4gIH1cclxuXHJcblxyXG4gIGV4cGFuZFJvdyhldmVudCwgcmVzb3VyY2UpIHtcclxuICAgIGNvbnNvbGUubG9nKHJlc291cmNlKTtcclxuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgaWYgKHJlc291cmNlLmRlbGl2ZXJhYmxlRGF0YSAmJiBBcnJheS5pc0FycmF5KHJlc291cmNlLmRlbGl2ZXJhYmxlRGF0YSkgJiYgcmVzb3VyY2UuZGVsaXZlcmFibGVEYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3QgZGlzcGxheUZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSxcclxuICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgdGhpcy5sZXZlbCA9PT0gTVBNX0xFVkVMUy5DQU1QQUlHTiA/IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfQ0FNUEFJR05fSUQgOiB0aGlzLmxldmVsID09PSBNUE1fTEVWRUxTLlBST0pFQ1QgPyBNUE1GaWVsZENvbnN0YW50cy5ERUxJVkVSQUJMRV9NUE1fRklFTERTLkRFTElWRVJBQkxFX1BST0pFQ1RfSUQgOiBNUE1GaWVsZENvbnN0YW50cy5ERUxJVkVSQUJMRV9NUE1fRklFTERTLkRFTElWRVJBQkxFX1RBU0tfSUQsIE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpO1xyXG4gICAgaWYgKCFkaXNwbGF5RmllbGQgfHwgIXJlc291cmNlWydJVEVNX0lEJ10pIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gTVBNLVYzLTIyMjQgLSBjb21tZW50IGJlbG93IG9uZVxyXG4gICAgLy8gY29uc3QgZGF0ZXMgPSB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onICsgJyBhbmQgJyArIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onO1xyXG5cclxuICAgIC8vIGNvbnN0IGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgdGhpcy5sZXZlbCwgZGF0ZXMsIHVuZGVmaW5lZCwgdHJ1ZSk7XHJcbiAgICAvLyBNUE0tVjMtMjIyNFxyXG4gICAgY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCB0aGlzLmxldmVsLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGUpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJyx1bmRlZmluZWQsZmFsc2UsdHJ1ZSk7XHJcblxyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgIGNvbnN0IG90bW1NUE1EYXRhVHlwZXMgPSAoKHRoaXMuaXNBcHByb3ZlciB8fCB0aGlzLmlzTWVtYmVyKSAmJiB0aGlzLnNlbGVjdGVkTGlzdEZpbHRlci52YWx1ZSAhPT0gJ01ZX05BX1JFVklFV19UQVNLUycpID8gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRSA6IHRoaXMuaXNSZXZpZXdlciA/IE9UTU1NUE1EYXRhVHlwZXMuREVMSVZFUkFCTEVfUkVWSUVXIDogT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRTtcclxuXHJcbiAgICBjb25zdCBjb25kaXRpb24gPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5jcmVhdGVDb25kaXRpb25PYmplY3QoJ0NPTlRFTlRfVFlQRScsIE1QTVNlYXJjaE9wZXJhdG9ycy5JUywgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUywgTVBNU2VhcmNoT3BlcmF0b3JzLkFORCwgb3RtbU1QTURhdGFUeXBlcyk7XHJcbiAgICBjb25kaXRpb24gPyBjb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT04ucHVzaChjb25kaXRpb24pIDogY29uc29sZS5sb2coJycpO1xyXG4gICAgY29uZGl0aW9uT2JqZWN0LkRFTElWRVJBQkxFX0NPTkRUSU9OLnB1c2goe1xyXG4gICAgICB0eXBlOiBkaXNwbGF5RmllbGQgPyBkaXNwbGF5RmllbGQuREFUQV9UWVBFIDogJ3N0cmluZycsXHJcbiAgICAgIGZpZWxkX2lkOiBkaXNwbGF5RmllbGQuSU5ERVhFUl9GSUVMRF9JRCxcclxuICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogTVBNU2VhcmNoT3BlcmF0b3JzLklTLFxyXG4gICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6ICdhbmQnLFxyXG4gICAgICB2YWx1ZTogcmVzb3VyY2VbJ0lURU1fSUQnXVxyXG4gICAgfSk7XHJcbiAgICBpZiAodGhpcy52aWV3Q29uZmlnKSB7XHJcbiAgICAgIHZpZXdDb25maWcgPSB0aGlzLnZpZXdDb25maWc7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlRBU0s7XHJcbiAgICB9XHJcbiAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld0NvbmZpZykuc3Vic2NyaWJlKCh2aWV3RGV0YWlsczogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgJiYgdHlwZW9mIHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyA9PT0gJ3N0cmluZydcclxuICAgICAgICA/IHBhcnNlSW50KHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApIDogbnVsbDtcclxuICAgICAgY29uc3QgcGFyYW1ldGVyczogU2VhcmNoUmVxdWVzdCA9IHtcclxuICAgICAgICBzZWFyY2hfY29uZmlnX2lkOiBzZWFyY2hDb25maWcgPyBzZWFyY2hDb25maWcgOiBudWxsLFxyXG4gICAgICAgIGtleXdvcmQ6ICcnLFxyXG4gICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogY29uZGl0aW9uT2JqZWN0LkRFTElWRVJBQkxFX0NPTkRUSU9OXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgZmFjZXRfY29uZGl0aW9uOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc29ydGluZ19saXN0OiB7XHJcbiAgICAgICAgICBzb3J0OiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICBwYWdlX2luZGV4OiAwLFxyXG4gICAgICAgICAgcGFnZV9zaXplOiAxMDBcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG5cclxuICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2goXHJcbiAgICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVycykuc3Vic2NyaWJlKChkYXRhOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICByZXNvdXJjZS5kZWxpdmVyYWJsZURhdGEgPSBbXTtcclxuICAgICAgICAgIGlmIChkYXRhLmRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBkRm9ybWF0ID0gJ1lZWVktTU0tREQnO1xyXG4gICAgICAgICAgICBkYXRhLmRhdGEuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICBsZXQgc3R5bGVzID0ge307XHJcbiAgICAgICAgICAgICAgbGV0IHRvcCA9IDQ0ICogaW5kZXg7IC8vIDMwXHJcbiAgICAgICAgICAgICAgY29uc3QgZXZlbnRTdGFydERhdGUgPSB0aGlzLm1vbWVudFB0cihpdGVtLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUsIGRGb3JtYXQpO1xyXG4gICAgICAgICAgICAgIGNvbnN0IGV2ZW50RW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKGl0ZW0uREVMSVZFUkFCTEVfRFVFX0RBVEUsIGRGb3JtYXQpO1xyXG4gICAgICAgICAgICAgIGNvbnN0IGRpZmYgPSAoZXZlbnRFbmREYXRlLmRpZmYoZXZlbnRTdGFydERhdGUpKSAvICgxMDAwICogNjAgKiA2MCAqIDI0KTtcclxuICAgICAgICAgICAgICBjb25zdCBleHRyYVBpeGVsID0gKChkaWZmICsgMSkgJSAyKSA9PT0gMCA/IDIgOiAwOy8vIDAgOiAyO1xyXG4gICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gKChkaWZmICsgMSkgKiAodGhpcy52aWV3VHlwZUNlbGxTaXplICsgZXh0cmFQaXhlbCkpOyAvLyAoKGRpZmYgKyAxKSAqICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgLSAxKSk7XHJcbiAgICAgICAgICAgICAgLy8gIHN0eWxlc1snd2lkdGgnXSA9ICgoKGRpZmYgKyAxKSAlIDIpID09PSAwKSA/ICh3aWR0aCArICdweCcpIDogKHdpZHRoICsgNCkgKyAncHgnO1xyXG4gICAgICAgICAgICAgIHN0eWxlc1sndG9wJ10gPSB0b3AgKyAncHgnO1xyXG4gICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlRGV0YWlsID0ge1xyXG4gICAgICAgICAgICAgICAgZGF0YTogaXRlbSxcclxuICAgICAgICAgICAgICAgIGljb246IHRoaXMuZ2V0RGVsaXZlcmFibGVUeXBlSWNvbih0aGlzLmdldFByb3BlcnR5KGl0ZW0sICdERUxJVkVSQUJMRV9UWVBFJykpLklDT04sXHJcbiAgICAgICAgICAgICAgICBzdHlsZTogc3R5bGVzXHJcbiAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICByZXNvdXJjZS5kZWxpdmVyYWJsZURhdGEucHVzaChkZWxpdmVyYWJsZURldGFpbCk7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzb3VyY2UuZGVsaXZlcmFibGVEYXRhKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIC8qICByZXNvdXJjZS5oZWlnaHQgPSAzNSAqIGRhdGEuZGF0YS5sZW5ndGggKyAncHgnOyAqL1xyXG4gICAgICAgICAgICByZXNvdXJjZS5oZWlnaHQgPSA1MCAqIGRhdGEuZGF0YS5sZW5ndGggKyAncHgnO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICk7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG5cclxuICB1cGRhdGVTZWFyY2hDb25maWdJZCgpIHtcclxuICAgIGlmICh0aGlzLm15VGFza1ZpZXdDb25maWdEZXRhaWxzKSB7XHJcbiAgICAgIGNvbnN0IGxhc3RWYWx1ZSA9IHRoaXMuc2VhcmNoQ29uZmlnSWQ7XHJcbiAgICAgIHRoaXMuc2VhcmNoQ29uZmlnSWQgPSBwYXJzZUludCh0aGlzLm15VGFza1ZpZXdDb25maWdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApO1xyXG4gICAgICBpZiAodGhpcy5zZWFyY2hDb25maWdJZCA+IDAgJiYgbGFzdFZhbHVlICE9PSB0aGlzLnNlYXJjaENvbmZpZ0lkKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UudXBkYXRlKHRoaXMubXlUYXNrVmlld0NvbmZpZ0RldGFpbHMpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWxvYWQocmVzZXRQYWdpbmF0aW9uOiBib29sZWFuLCB2YWx1ZT86IGFueSkge1xyXG5cclxuICAgIC8qIHZhciB3aWR0aCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxlbmRhckNlbGwnKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aDtcclxuICAgIGFsZXJ0KHdpZHRoKTsgKi9cclxuICAgIHRoaXMuZ2V0bGlzdERhdGFJbmRleChyZXNldFBhZ2luYXRpb24pO1xyXG4gIH1cclxuXHJcblxyXG4gIGdldGxpc3REYXRhSW5kZXgodmFsdWU/OiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5wcm9qZWN0TGlzdERhdGEgPSBbXTtcclxuICAgIHRoaXMudG90YWxMaXN0RGF0YUNvdW50ID0gMDtcclxuICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICB0aGlzLnByZXZpb3VzUmVxdWVzdCA9IG51bGw7XHJcbiAgICB9XHJcbiAgICBsZXQga2V5V29yZCA9ICcnO1xyXG4gICAgY29uc3QgZGF0ZXMgPSB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onICsgJyBhbmQgJyArIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onO1xyXG5cclxuICAgIC8qICBjb25zdCBjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gdGhpcy5maWx0ZXJIZWxwZXJTZXJ2aWNlLmdldFNlYXJjaENvbmRpdGlvbk5ldyh0aGlzLnNlbGVjdGVkTGlzdEZpbHRlciwgdGhpcy5zZWxlY3RlZExpc3REZXBlbmRlbnRGaWx0ZXIsIHRoaXMubGV2ZWwsIGRhdGVzLCB1bmRlZmluZWQsIHRydWUpOyAqLyAvLyB0aGlzLmxldmVsXHJcbiAgICAvLyBNUE0tVjMtMjIyNFxyXG4gICAgY29uc3QgY29uZGl0aW9uT2JqZWN0OiBUYXNrU2VhcmNoQ29uZGl0aW9ucyA9IHRoaXMuZmlsdGVySGVscGVyU2VydmljZS5nZXRTZWFyY2hDb25kaXRpb25OZXcodGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIsIHRoaXMuc2VsZWN0ZWRMaXN0RGVwZW5kZW50RmlsdGVyLCB0aGlzLmxldmVsLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSkuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGUpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJyx1bmRlZmluZWQsZmFsc2UsdHJ1ZSk7XHJcblxyXG4gICAgdGhpcy5zdG9yZVNlYXJjaGNvbmRpdGlvbk9iamVjdCA9IE9iamVjdC5hc3NpZ24oe30sIGNvbmRpdGlvbk9iamVjdCk7XHJcbiAgICBjb25zdCBvdG1tTVBNRGF0YVR5cGVzID0gKCh0aGlzLmlzQXBwcm92ZXIgfHwgdGhpcy5pc01lbWJlcikgJiYgdGhpcy5zZWxlY3RlZExpc3RGaWx0ZXIudmFsdWUgIT0gJ01ZX05BX1JFVklFV19UQVNLUycpID8gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRSA6IHRoaXMuaXNSZXZpZXdlciA/IE9UTU1NUE1EYXRhVHlwZXMuREVMSVZFUkFCTEVfUkVWSUVXIDogT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRTtcclxuICAgIGxldCBzb3J0VGVtcCA9IFtdO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRTb3J0ICYmIHRoaXMuc2VsZWN0ZWRTb3J0LmFjdGl2ZSkge1xyXG4gICAgICBjb25zdCBzb3J0T3B0aW9uID0gdGhpcy5zb3J0YWJsZUZpZWxkcy5maW5kKG9wdGlvbiA9PiBvcHRpb24ubmFtZSA9PT0gdGhpcy5zZWxlY3RlZFNvcnQuYWN0aXZlKTtcclxuICAgICAgc29ydFRlbXAgPSBzb3J0T3B0aW9uID8gW3tcclxuICAgICAgICBmaWVsZF9pZDogc29ydE9wdGlvbi5pbmRleGVySWQsXHJcbiAgICAgICAgb3JkZXI6IHRoaXMuc2VsZWN0ZWRTb3J0LmRpcmVjdGlvblxyXG4gICAgICB9XSA6IFtdO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2VhcmNoQ29uZGl0aW9ucyAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnMubGVuZ3RoID4gMCAmJiB0aGlzLnNlYXJjaENvbmRpdGlvbnNbMF0ubWV0YWRhdGFfZmllbGRfaWQgPT09IHRoaXMuc2VhcmNoRGF0YVNlcnZpY2UuS0VZV09SRF9NQVBQRVIpIHtcclxuICAgICAga2V5V29yZCA9IHRoaXMuc2VhcmNoQ29uZGl0aW9uc1swXS5rZXl3b3JkO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNlYXJjaENvbmRpdGlvbnMgJiYgdGhpcy5zZWFyY2hDb25kaXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgY29uZGl0aW9uT2JqZWN0LlRBU0tfQ09ORElUSU9OID0gY29uZGl0aW9uT2JqZWN0LlRBU0tfQ09ORElUSU9OLmNvbmNhdCh0aGlzLnNlYXJjaENvbmRpdGlvbnMpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbiA9IHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24gJiYgQXJyYXkuaXNBcnJheSh0aGlzLmVtYWlsU2VhcmNoQ29uZGl0aW9uKSA/IHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24gOiBbXTtcclxuXHJcbiAgICBjb25zdCBwYXJhbWV0ZXJzOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICBzZWFyY2hfY29uZmlnX2lkOiB0aGlzLnNlYXJjaENvbmZpZ0lkID4gMCA/IHRoaXMuc2VhcmNoQ29uZmlnSWQgOiBudWxsLFxyXG4gICAgICBrZXl3b3JkOiBrZXlXb3JkLFxyXG4gICAgICBncm91cGluZzoge1xyXG4gICAgICAgIGdyb3VwX2J5OiB0aGlzLmxldmVsLFxyXG4gICAgICAgIGdyb3VwX2Zyb21fdHlwZTogb3RtbU1QTURhdGFUeXBlcyxcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IFsuLi5jb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT04sIC4uLnRoaXMuZW1haWxTZWFyY2hDb25kaXRpb25dXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBjb25kaXRpb25PYmplY3QuVEFTS19DT05ESVRJT05cclxuICAgICAgfSxcclxuICAgICAgZmFjZXRfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICBmYWNldF9jb25kaXRpb246ICh0aGlzLmZhY2V0UmVzdHJpY3Rpb25MaXN0ICYmIHRoaXMuZmFjZXRSZXN0cmljdGlvbkxpc3QuZmFjZXRfY29uZGl0aW9uKSA/IHRoaXMuZmFjZXRSZXN0cmljdGlvbkxpc3QuZmFjZXRfY29uZGl0aW9uIDogW11cclxuICAgICAgfSxcclxuICAgICAgc29ydGluZ19saXN0OiB7XHJcbiAgICAgICAgc29ydDogc29ydFRlbXBcclxuICAgICAgfSxcclxuICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgcGFnZV9pbmRleDogdGhpcy5za2lwLFxyXG4gICAgICAgIHBhZ2Vfc2l6ZTogMTAwMFxyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIGlmICghKEpTT04uc3RyaW5naWZ5KHRoaXMucHJldmlvdXNSZXF1ZXN0KSA9PT0gSlNPTi5zdHJpbmdpZnkocGFyYW1ldGVycykpKSB7XHJcblxyXG4gICAgICB0aGlzLnByZXZpb3VzUmVxdWVzdCA9IHBhcmFtZXRlcnM7XHJcbiAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5pc0RhdGFSZWZyZXNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS51cGRhdGVGYWNldEZpbHRlcihyZXNwb25zZS5mYWNldF9maWVsZF9yZXNwb25zZV9saXN0ID8gcmVzcG9uc2UuZmFjZXRfZmllbGRfcmVzcG9uc2VfbGlzdCA6IFtdKTtcclxuICAgICAgICB0aGlzLnByb2plY3RMaXN0RGF0YSA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgdGhpcy50b3RhbExpc3REYXRhQ291bnQgPSByZXNwb25zZS5jdXJzb3IudG90YWxfcmVjb3JkcztcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdGYWlsZWQgd2hpbGUgZ2V0dGluZyAnICsgdGhpcy50aXRsZWNhc2VQaXBlLnRyYW5zZm9ybSh0aGlzLnZpZXdDb25maWcpICsgJyBsaXN0cycpO1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldExldmVsRGV0YWlscyh0cmlnZ2VyPykge1xyXG4gICAgaWYgKHRoaXMudmlld0NvbmZpZykge1xyXG4gICAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodGhpcy52aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgdGhpcy5teVRhc2tWaWV3Q29uZmlnRGV0YWlscyA9IHZpZXdEZXRhaWxzO1xyXG4gICAgICAgIHRoaXMudXBkYXRlU2VhcmNoQ29uZmlnSWQoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICBpZiAoISh0cmlnZ2VyID09PSB0cnVlKSkge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKGZhbHNlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaCgpIHtcclxuICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gIH1cclxuXHJcbiAgaW5pdGFsaXplQ29tcG9uZW50KHJvdXRlRGF0YTogUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50UGFyYW1zKSB7XHJcbiAgICBsZXQgcm91dGVkR3JvdXBGaWx0ZXI7XHJcbiAgICBpZiAocm91dGVEYXRhLmdyb3VwQnlGaWx0ZXIpIHtcclxuICAgICAgcm91dGVkR3JvdXBGaWx0ZXIgPSB0aGlzLmVuYWJsZUNhbXBhaWduID8gR1JPVVBfQllfQUxMX0ZJTFRFUlMuZmluZChmaWx0ZXIgPT4ge1xyXG4gICAgICAgIHJldHVybiBmaWx0ZXIudmFsdWUgPT09IHJvdXRlRGF0YS5ncm91cEJ5RmlsdGVyO1xyXG4gICAgICB9KSA6IEdST1VQX0JZX0ZJTFRFUlMuZmluZChmaWx0ZXIgPT4ge1xyXG4gICAgICAgIHJldHVybiBmaWx0ZXIudmFsdWUgPT09IHJvdXRlRGF0YS5ncm91cEJ5RmlsdGVyO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICB0aGlzLnNlbGVjdGVkR3JvdXBCeUZpbHRlciA9IChyb3V0ZWRHcm91cEZpbHRlciA9PT0gdW5kZWZpbmVkKSA/IHRoaXMuZW5hYmxlQ2FtcGFpZ24gPyBHUk9VUF9CWV9BTExfRklMVEVSU1swXSA6IHJvdXRlZEdyb3VwRmlsdGVyIHx8IEdST1VQX0JZX0ZJTFRFUlNbMF0gOiByb3V0ZWRHcm91cEZpbHRlciB8fCBHUk9VUF9CWV9GSUxURVJTWzBdOyAvKiB0aGlzLmVuYWJsZUNhbXBhaWduID8gR1JPVVBfQllfQUxMX0ZJTFRFUlNbMF0gOiAqL1xyXG4gICAgdGhpcy5zZXRMZXZlbCgpO1xyXG5cclxuICAgIGlmIChyb3V0ZURhdGEuc29ydEJ5KSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRTb3J0QnkgPSByb3V0ZURhdGEuc29ydEJ5O1xyXG4gICAgfVxyXG4gICAgaWYgKHJvdXRlRGF0YS5zb3J0T3JkZXIpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFNvcnRPcmRlciA9IHJvdXRlRGF0YS5zb3J0T3JkZXI7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHJvdXRlRGF0YS5wYWdlTnVtYmVyKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRQYWdlTnVtYmVyID0gcm91dGVEYXRhLnBhZ2VOdW1iZXI7XHJcbiAgICB9XHJcbiAgICBpZiAocm91dGVEYXRhLnBhZ2VTaXplKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRQYWdlU2l6ZSA9IHJvdXRlRGF0YS5wYWdlU2l6ZTtcclxuICAgIH1cclxuICAgIGlmICgocm91dGVEYXRhLnNlYXJjaE5hbWUgPT09IG51bGwgJiYgdGhpcy5zZWFyY2hOYW1lKSB8fCAodGhpcy5zZWFyY2hOYW1lID09PSBudWxsICYmIHJvdXRlRGF0YS5zZWFyY2hOYW1lKVxyXG4gICAgICB8fCAocm91dGVEYXRhLnNhdmVkU2VhcmNoTmFtZSA9PT0gbnVsbCAmJiB0aGlzLnNhdmVkU2VhcmNoTmFtZSkgfHwgKHRoaXMuc2F2ZWRTZWFyY2hOYW1lID09PSBudWxsICYmIHJvdXRlRGF0YS5zYXZlZFNlYXJjaE5hbWUpXHJcbiAgICAgIHx8IChyb3V0ZURhdGEuYWR2U2VhcmNoRGF0YSA9PT0gbnVsbCAmJiB0aGlzLmFkdlNlYXJjaERhdGEpIHx8ICh0aGlzLmFkdlNlYXJjaERhdGEgPT09IG51bGwgJiYgcm91dGVEYXRhLmFkdlNlYXJjaERhdGEpKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRQYWdlTnVtYmVyID0gMTtcclxuICAgICAgdGhpcy5zZWFyY2hOYW1lID0gcm91dGVEYXRhLnNlYXJjaE5hbWU7XHJcbiAgICAgIHRoaXMuc2F2ZWRTZWFyY2hOYW1lID0gcm91dGVEYXRhLnNhdmVkU2VhcmNoTmFtZTtcclxuICAgICAgdGhpcy5hZHZTZWFyY2hEYXRhID0gcm91dGVEYXRhLmFkdlNlYXJjaERhdGE7XHJcbiAgICAgIHRoaXMuaXNSZXNldCA9IHRydWU7XHJcbiAgICAgIHRoaXMucm91dGVDb21wb25lbnRWaWV3KCk7XHJcbiAgICB9IGVsc2UgaWYgKChyb3V0ZURhdGEuZmFjZXREYXRhID09PSBudWxsICYmIHRoaXMuZmFjZXREYXRhKSB8fCAodGhpcy5mYWNldERhdGEgPT09IG51bGwgJiYgcm91dGVEYXRhLmZhY2V0RGF0YSkpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZFBhZ2VOdW1iZXIgPSAxO1xyXG4gICAgICB0aGlzLmZhY2V0RGF0YSA9IHJvdXRlRGF0YS5mYWNldERhdGE7XHJcbiAgICAgIHRoaXMuaXNSZXNldCA9IHRydWU7XHJcbiAgICAgIHRoaXMucm91dGVDb21wb25lbnRWaWV3KCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAocm91dGVEYXRhLnNlYXJjaE5hbWUpIHtcclxuICAgICAgICB0aGlzLnNlYXJjaE5hbWUgPSByb3V0ZURhdGEuc2VhcmNoTmFtZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNlYXJjaE5hbWUgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChyb3V0ZURhdGEuc2F2ZWRTZWFyY2hOYW1lKSB7XHJcbiAgICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSByb3V0ZURhdGEuc2F2ZWRTZWFyY2hOYW1lO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hOYW1lID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgICBpZiAocm91dGVEYXRhLmFkdlNlYXJjaERhdGEpIHtcclxuICAgICAgICB0aGlzLmFkdlNlYXJjaERhdGEgPSByb3V0ZURhdGEuYWR2U2VhcmNoRGF0YTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmFkdlNlYXJjaERhdGEgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChyb3V0ZURhdGEuZmFjZXREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5mYWNldERhdGEgPSByb3V0ZURhdGEuZmFjZXREYXRhO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZmFjZXREYXRhID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmNvbXBvbmVudFJlbmRlcmluZyA9IGZhbHNlO1xyXG4gICAgICBpZiAocm91dGVEYXRhLmVtYWlsTGlua0lkKSB7XHJcbiAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmVtYWlsTGlua0lkID0gJyc7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNQYWdlUmVmcmVzaCAmJiAodGhpcy5zZWFyY2hOYW1lIHx8IHRoaXMuYWR2U2VhcmNoRGF0YSB8fCB0aGlzLnNhdmVkU2VhcmNoTmFtZSkpIHtcclxuICAgICAgICAgIHRoaXMuZ2V0TGV2ZWxEZXRhaWxzKHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoISh0aGlzLmlzUGFnZVJlZnJlc2ggJiYgdGhpcy5mYWNldERhdGEpKSB7XHJcbiAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqICogU2Nyb2xsIGZvciBGaXhpbmcgdGhlIGRhdGVzIGFuZCBoZWFkZXIgKi9cclxuXHJcbiAgc2Nyb2xsaW5nID0gKGVsKTogdm9pZCA9PiB7XHJcbiAgICBjb25zdCBzY3JvbGxUb3AgPSBlbC5zcmNFbGVtZW50LnNjcm9sbFRvcDtcclxuICAgIGlmIChzY3JvbGxUb3AgPj0gMTAwKSB7XHJcbiAgICAgIHRoaXMuc2Nyb2xsZWQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zY3JvbGxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqIENBTEVOREFSIE1FVEhPRFMgKiovXHJcblxyXG4gIC8vIGJ1aWxkIGNhbGVuZGFyIGJhc2VkIG9uIGdpdmVuIHN0YXJ0ICBhbmQgZW5kIGRhdGVcclxuICBidWlsZENhbGVuZGFyKHN0YXJ0RGF0ZSwgZW5kRGF0ZSkge1xyXG4gICAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IHN0YXJ0RGF0ZTtcclxuICAgIHRoaXMuY3VycmVudFZpZXdFbmREYXRlID0gZW5kRGF0ZTtcclxuICAgIGNvbnN0IHN0YXJ0ID0gc3RhcnREYXRlOyAvLyB0aGlzLm1vbWVudFB0cih7fSk7IC8vZ2V0IGN1cnJyZW50IGRhdGVcclxuICAgIGNvbnN0IGVuZCA9IGVuZERhdGU7IC8vIHRoaXMubW9tZW50UHRyKHt9KS5hZGQoMzAsICdkYXlzJyk7XHJcbiAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKHN0YXJ0LCBlbmQpO1xyXG4gICAgY29uc3QgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcbiAgICB0aGlzLmRhdGVzID0gZGF5cztcclxuICB9XHJcblxyXG4gIGdldER5bmFtaWNXb3JkU3BhY2luZygpIHtcclxuICAgIGxldCBzcGFjaW5nID0gJzhweCc7XHJcbiAgICBpZiAodGhpcy5kaXNwbGF5VHlwZSA9PT0gJ21vbnRoJykge1xyXG4gICAgICBpZiAoIXRoaXMuc2hvd1dlZWtlbmRzKSB7XHJcbiAgICAgICAgc3BhY2luZyA9ICcxNnB4JztcclxuICAgICAgfVxyXG4gICAgICBzcGFjaW5nID0gJzBweCc7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc3BhY2luZztcclxuICB9XHJcblxyXG4gIGdldE5vT2ZEYXlzKHNlbGVjdGVkTW9udGgpIHtcclxuICAgIGxldCBub09mRGF5cztcclxuICAgIGlmIChDYWxlbmRhckNvbnN0YW50cy5ldmVuTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgbm9PZkRheXMgPSAzMTtcclxuICAgIH0gZWxzZSBpZiAoQ2FsZW5kYXJDb25zdGFudHMub2RkTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgbm9PZkRheXMgPSAzMDtcclxuICAgIH0gZWxzZSBpZiAoQ2FsZW5kYXJDb25zdGFudHMubGVhcE1vbnRoLmluY2x1ZGVzKHNlbGVjdGVkTW9udGgpKSB7XHJcbiAgICAgIC8qIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSh0aGlzLnllYXIsIDEsIDI5KTtcclxuICAgICAgcmV0dXJuIGRhdGUuZ2V0TW9udGgoKSA9PT0gMTsgKi9cclxuICAgICAgbm9PZkRheXMgPSAyODtcclxuICAgIH1cclxuICAgIHJldHVybiBub09mRGF5cztcclxuICB9XHJcblxyXG4gIGdldFVzZXJzQnlSb2xlKHJlc291cmNlLCBkZWxpdmVyYWJsZURhdGEpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgbGV0IGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge307XHJcbiAgICAgIGNvbnN0IHJvbGVJZCA9IHJlc291cmNlLlRBU0tfUk9MRV9JRCA/IHJlc291cmNlLlRBU0tfUk9MRV9JRCA6IERlbGl2ZXJhYmxlVHlwZXMuUkVWSUVXID09PSBkZWxpdmVyYWJsZURhdGEuREVMSVZFUkFCTEVfVFlQRSA/IGRlbGl2ZXJhYmxlRGF0YS5ERUxJVkVSQUJMRV9BUFBST1ZFUl9ST0xFX0lEIDogZGVsaXZlcmFibGVEYXRhLkRFTElWRVJBQkxFX09XTkVSX1JPTEVfSUQ7XHJcbiAgICAgIGNvbnN0IHJvbGVEbiA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0VGVhbVJvbGVETkJ5Um9sZUlkKHJvbGVJZCk7XHJcbiAgICAgIHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucyA9IFtdO1xyXG4gICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICBhbGxvY2F0aW9uVHlwZTogVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSxcclxuICAgICAgICByb2xlRE46IHJvbGVEbi5ST0xFX0ROLFxyXG4gICAgICAgIHRlYW1JRDogJycsXHJcbiAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldFVzZXJCeVJvbGVSZXF1ZXN0KVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgdXNlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICBjb25zdCB1c2VycyA9IFtdO1xyXG4gICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICB1c2VyTGlzdC5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSB1c2Vycy5maW5kKGUgPT4gZS52YWx1ZSA9PT0gdXNlci5kbik7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWZpZWxkT2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgIHVzZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHVzZXIuZG4sXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHVzZXIuY24sXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHVzZXIubmFtZVxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMgPSB1c2VycztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0VXNlcnMocmVzb3VyY2UsIGRlbGl2ZXJhYmxlRGF0YSkge1xyXG4gICAgY29uc29sZS5sb2coZGVsaXZlcmFibGVEYXRhKTtcclxuICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUocmVzb3VyY2UsIGRlbGl2ZXJhYmxlRGF0YSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qIHVwZGF0ZVRhc2tEZWxpdmVyYWJsZShkYXRhLCB1c2VySWQpIHtcclxuICAgIGxldCBUYXNrT2JqZWN0O1xyXG4gICAgVGFza09iamVjdCA9IHtcclxuICAgICAgJ0J1bGtPcGVyYXRpb24nOiBmYWxzZSxcclxuICAgICAgJ1JQT093bmVyVXNlcic6IHtcclxuICAgICAgICAnVXNlcic6IHtcclxuICAgICAgICAgICd1c2VySUQnOiB1c2VySWRcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgICdUYXNrSWQnOiB7XHJcbiAgICAgICAgJ0lkJzogZGF0YS5UQVNLX0lURU1fSUQuc3BsaXQoJy4nKVsxXVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zdCByZXF1ZXN0T2JqID0ge1xyXG4gICAgICBUYXNrT2JqZWN0OiBUYXNrT2JqZWN0LFxyXG4gICAgICBEZWxpdmVyYWJsZVJldmlld2Vyczoge1xyXG4gICAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXI6ICcnXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMudGFza1NlcnZpY2UudXBkYXRlVGFzayhyZXF1ZXN0T2JqKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcpIHtcclxuICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVGFzayB1cGRhdGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICc1MDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yXHJcbiAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JDb2RlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSkge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihyZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgdGFzaycpO1xyXG4gICAgICB9XHJcbiAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIHRhc2snKTtcclxuICAgIH0pO1xyXG4gIH0gKi9cclxuICAvLyBNUE0tVjMtMjIxN1xyXG4gIHVwZGF0ZVRhc2tEZWxpdmVyYWJsZShkYXRhLCB1c2VySWQpIHtcclxuICAgIHRoaXMudGFza1NlcnZpY2UuYXNzaWduRGVsaXZlcmFibGUoZGF0YS5JRCwgdXNlcklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcpIHtcclxuICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnRGVsaXZlcmFibGUgdXBkYXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAvLyAgdGhpcy5hZGRlZFJlc291cmNlSGFuZGxlci5uZXh0KHVzZXJJZCk7XHJcbiAgICAgICAgLy8gICB0aGlzLnN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQuZ2V0RGVsaXZlcmFibGUodXNlcklkLGZhbHNlKTtcclxuICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnNTAwJykge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMucmVsb2FkKHRydWUpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnKTtcclxuICAgICAgfVxyXG4gICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSB1cGRhdGluZyBkZWxpdmVyYWJsZScpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVEZWxpdmVyYWJsZShkYXRhLCB1c2VySWQpIHtcclxuICAgIGxldCBkZWxpdmVyYWJsZU9iamVjdDtcclxuICAgIGRlbGl2ZXJhYmxlT2JqZWN0ID0ge1xyXG4gICAgICAnQnVsa09wZXJhdGlvbic6IGZhbHNlLFxyXG4gICAgICAnUlBPRGVsaXZlcmFibGVPd25lcklEJzoge1xyXG4gICAgICAgICdJZGVudGl0eUlEJzoge1xyXG4gICAgICAgICAgJ3VzZXJJRCc6IHVzZXJJZFxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgJ0RlbGl2ZXJhYmxlSWQnOiB7XHJcbiAgICAgICAgJ0lkJzogZGF0YS5JRFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zdCByZXF1ZXN0T2JqID0ge1xyXG4gICAgICBEZWxpdmVyYWJsZU9iamVjdDogZGVsaXZlcmFibGVPYmplY3QsXHJcbiAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXJzOiB7XHJcbiAgICAgICAgRGVsaXZlcmFibGVSZXZpZXdlcjogJydcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHRoaXMuZGVsaXZlcmFibGVTZXJ2aWNlLnVwZGF0ZURlbGl2ZXJhYmxlKHJlcXVlc3RPYmopXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGFcclxuICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZVsnRGVsaXZlcmFibGUtaWQnXSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddLklkKSB7XHJcbiAgICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdEZWxpdmVyYWJsZSB1cGRhdGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgICAgLy8gIHRoaXMuYWRkZWRSZXNvdXJjZUhhbmRsZXIubmV4dCh1c2VySWQpO1xyXG4gICAgICAgICAgLy8gIHRoaXMuc3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudC5nZXREZWxpdmVyYWJsZSh1c2VySWQsIGZhbHNlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICc1MDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yXHJcbiAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvckNvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlKSB7XHJcbiAgICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihyZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgdGhpcy5yZWxvYWQodHJ1ZSk7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIGRlbGl2ZXJhYmxlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICB0aGlzLnJlbG9hZCh0cnVlKTtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIGRlbGl2ZXJhYmxlJyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlKGRhdGEsIHVzZXJJZCkge1xyXG4gICAgY29uc29sZS5sb2codXNlcklkKTtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICBpZiAoZGF0YS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlVQTE9BRCkge1xyXG4gICAgICB0aGlzLnVwZGF0ZURlbGl2ZXJhYmxlKGRhdGEsIHVzZXJJZCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnVwZGF0ZVRhc2tEZWxpdmVyYWJsZShkYXRhLCB1c2VySWQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZGVsaXZlcmFibGVEYXRhSGFuZGxlcihldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgdGhpcy5kZWxpdmVyYWJsZURhdGFzID0gZXZlbnQ7XHJcbiAgfVxyXG5cclxuICBvbkNoaWNrbGV0UmVtb3ZlRmlsdGVyKGZpbHRlcikge1xyXG4gICAgY29uc29sZS5sb2coZmlsdGVyKTtcclxuICAgIHRoaXMucmVtb3ZlZEZpbHRlciA9IGZpbHRlcjtcclxuICB9XHJcblxyXG4gIC8qIHJlc291cmNlQWxsb2NhdGlvbkhhbmRsZXIoYWxsb2NhdGlvbikge1xyXG4gICAgY29uc29sZS5sb2coYWxsb2NhdGlvbik7XHJcbiAgICB0aGlzLmFsbG9jYXRpb24gPSBhbGxvY2F0aW9uO1xyXG4gIH0gKi9cclxuXHJcbiAgZ2V0RGF0ZURpZmZlcmVuY2Uoc3RhcnREYXRlLCBlbmREYXRlKSB7XHJcbiAgICBsZXQgc2Q6IGFueSA9IChuZXcgRGF0ZShzdGFydERhdGUpKTtcclxuICAgIGxldCBlZDogYW55ID0gbmV3IERhdGUoZW5kRGF0ZSk7XHJcbiAgICByZXR1cm4gKE1hdGguZmxvb3IoZWQgLSBzZCkgLyAoMTAwMCAqIDYwICogNjAgKiAyNCkpICsgMTtcclxuICB9XHJcblxyXG4gIC8vIG5nT25DaGFuZ2VzKHNpbXBsZUNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAvLyAgIHZhciBhbmd1bGFyOiBhbnk7XHJcbiAgLy8gICBjb25zb2xlLmxvZyhcIkNvbnRlbnQtd2lkdGg6IFwiXHJcbiAgLy8gICAgICsgKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2FsZW5kYXJDZWxsXCIpLnNjcm9sbFdpZHRoKVxyXG4gIC8vICAgICArIFwicHg8YnI+XCIpO1xyXG4gIC8vICAgY29uc29sZS5sb2coXCJDb250ZW50LXdpZHRoMTogXCJcclxuICAvLyAgICAgKyBhbmd1bGFyLmVsZW1lbnQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjYWxlbmRhckNlbGxcIikuY2xpZW50SGVpZ2h0KVxyXG4gIC8vICAgICArIFwicHg8YnI+XCIpO1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiB1c2UtbGlmZWN5Y2xlLWludGVyZmFjZVxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jb21wb25lbnRSZW5kZXJpbmcgPSB0cnVlO1xyXG4gICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgdGhpcy5lbmFibGVDYW1wYWlnbiA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVOQUJMRV9DQU1QQUlHTl0pO1xyXG4gICAgdGhpcy5ncm91cEJ5RmlsdGVycyA9IHRoaXMuZW5hYmxlQ2FtcGFpZ24gPyBHUk9VUF9CWV9BTExfRklMVEVSUyA6IEdST1VQX0JZX0ZJTFRFUlM7XHJcblxyXG4gICAgdGhpcy50b3AgPSB0aGlzLnV0aWxTZXJ2aWNlLmNvbnZlcnRTdHJpbmdUb051bWJlcih0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1BBR0lOQVRJT05dKTtcclxuICAgIHRoaXMucGFnZVNpemUgPSB0aGlzLnV0aWxTZXJ2aWNlLmNvbnZlcnRTdHJpbmdUb051bWJlcih0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1BBR0lOQVRJT05dKTtcclxuXHJcbiAgICBjb25zdCBjdXJyZW50RnVsbERhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gICAgY29uc3QgY3VycmVudFllYXIgPSBjdXJyZW50RnVsbERhdGUuZ2V0RnVsbFllYXIoKTtcclxuICAgIGNvbnN0IGN1cnJlbnRNb250aCA9IGN1cnJlbnRGdWxsRGF0ZS5nZXRNb250aCgpICsgMTtcclxuICAgIGNvbnN0IGN1cnJlbnREYXRlID0gY3VycmVudEZ1bGxEYXRlLmdldERhdGUoKTtcclxuICAgIHRoaXMuY3VycmVudERhdGUgPSBjdXJyZW50TW9udGggKyAnLycgKyBjdXJyZW50RGF0ZSArICcvJyArIGN1cnJlbnRZZWFyO1xyXG5cclxuICAgIHRoaXMuc2VsZWN0ZWRNb250aCA9IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRoc1tjdXJyZW50RnVsbERhdGUuZ2V0TW9udGgoKV07XHJcbiAgICB0aGlzLnllYXIgPSBjdXJyZW50WWVhcjtcclxuICAgIGxldCBub09mRGF5cyA9IHRoaXMuZ2V0Tm9PZkRheXModGhpcy5zZWxlY3RlZE1vbnRoKTtcclxuICAgIC8vIHRoaXMuYnVpbGRDYWxlbmRhcih0aGlzLm1vbWVudFB0cih7fSksIHRoaXMubW9tZW50UHRyKHt9KS5hZGQoMjksICdkYXlzJykpO1xyXG4gICAgLyogdGhpcy5idWlsZENhbGVuZGFyKGN1cnJlbnRNb250aCArICcvMDEvJyArIGN1cnJlbnRZZWFyLCB0aGlzLm1vbWVudFB0cih7fSkuYWRkKDI5LCAnZGF5cycpKTsgKi9cclxuICAgIHRoaXMuYnVpbGRDYWxlbmRhcihjdXJyZW50TW9udGggKyAnLzAxLycgKyBjdXJyZW50WWVhciwgY3VycmVudE1vbnRoICsgJy8nICsgbm9PZkRheXMgKyAnLycgKyBjdXJyZW50WWVhcik7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5zY3JvbGxpbmcsIHRydWUpO1xyXG5cclxuICAgIGxldCBpbml0aWFsTG9hZCA9IHRydWU7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgdGhpcy5zZWFyY2hEYXRhU2VydmljZS5zZWFyY2hEYXRhLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICB0aGlzLnNlYXJjaENvbmRpdGlvbnMgPSAoZGF0YSAmJiBBcnJheS5pc0FycmF5KGRhdGEpID8gdGhpcy5vdG1tU2VydmljZS5hZGRSZWxhdGlvbmFsT3BlcmF0b3IoZGF0YSkgOiBbXSk7XHJcbiAgICAgICAgaWYgKCh0aGlzLnNlYXJjaE5hbWUgfHwgdGhpcy5zYXZlZFNlYXJjaE5hbWUgfHwgdGhpcy5hZHZTZWFyY2hEYXRhIHx8IHRoaXMuZmFjZXREYXRhKSAmJiB0aGlzLmlzRGF0YVJlZnJlc2gpIHtcclxuICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuZmFjZXREYXRhIHx8ICgodGhpcy5mYWNldERhdGEgJiYgdGhpcy5mYWNldFJlc3RyaWN0aW9uTGlzdCkgJiYgKHRoaXMuYWR2U2VhcmNoRGF0YSB8fCB0aGlzLnNhdmVkU2VhcmNoTmFtZSkpKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRlRmFjZXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnNlYXJjaE5hbWUgPT09IG51bGwgJiYgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPT09IG51bGwgJiYgdGhpcy5hZHZTZWFyY2hEYXRhID09PSBudWxsICYmIHRoaXMuZmFjZXREYXRhID09PSBudWxsICYmIHRoaXMuaXNEYXRhUmVmcmVzaCkge1xyXG4gICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmlzRGF0YVJlZnJlc2gpIHtcclxuICAgICAgICAgIGlmICghaW5pdGlhbExvYWQpIHtcclxuICAgICAgICAgICAgdGhpcy5pc1Jlc2V0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaW5pdGlhbExvYWQgPSBmYWxzZTtcclxuICAgICAgfSlcclxuICAgICk7XHJcblxyXG4gICAgaW5pdGlhbExvYWQgPSB0cnVlO1xyXG4gICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2goXHJcbiAgICAgIHRoaXMuZmFjZXRDaGFuZ2VFdmVudFNlcnZpY2Uub25GYWNldENvbmRpZHRpb25DaGFuZ2Uuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMuZmFjZXRSZXN0cmljdGlvbkxpc3QgPSBkYXRhO1xyXG4gICAgICAgIGlmICh0aGlzLmZhY2V0RGF0YSAmJiB0aGlzLmlzRGF0YVJlZnJlc2gpIHtcclxuICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEuZmFjZXRfY29uZGl0aW9uICYmIGRhdGEuZmFjZXRfY29uZGl0aW9uLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgaWYgKCEodGhpcy5hZHZTZWFyY2hEYXRhIHx8IHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSB8fCB0aGlzLnZhbGlkYXRlRmFjZXQpIHtcclxuICAgICAgICAgICAgICB0aGlzLmdldExldmVsRGV0YWlscygpO1xyXG4gICAgICAgICAgICAgIHRoaXMudmFsaWRhdGVGYWNldCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZhY2V0RGF0YSA9PT0gbnVsbCAmJiB0aGlzLmlzRGF0YVJlZnJlc2gpIHtcclxuICAgICAgICAgIHRoaXMuZ2V0TGV2ZWxEZXRhaWxzKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5pc0RhdGFSZWZyZXNoKSB7XHJcbiAgICAgICAgICBpZiAoIWluaXRpYWxMb2FkKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJldmlvdXNSZXF1ZXN0ID0gbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5pc1Jlc2V0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5nZXRMZXZlbERldGFpbHMoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaW5pdGlhbExvYWQgPSBmYWxzZTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgdGhpcy5jb21wb25lbnRQYXJhbXMuc3Vic2NyaWJlKHJvdXRlRGF0YSA9PiB7XHJcbiAgICAgICAgaWYgKHJvdXRlRGF0YS5lbWFpbExpbmtJZCkge1xyXG4gICAgICAgICAgdGhpcy5lbWFpbExpbmtJZCA9IHJvdXRlRGF0YS5lbWFpbExpbmtJZDtcclxuICAgICAgICAgIHRoaXMuaW5pdGFsaXplQ29tcG9uZW50KHJvdXRlRGF0YSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuaW5pdGFsaXplQ29tcG9uZW50KHJvdXRlRGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgKTtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMuc2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLnVwZGF0ZShudWxsKTtcclxuICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKHN1YnNjcmlwdGlvbiA9PiBzdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKSk7XHJcbiAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgdGhpcy5zY3JvbGxpbmcsIHRydWUpO1xyXG4gIH1cclxuXHJcbiAgLyogbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgdmFyIHdpZHRoID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbGVuZGFyQ2VsbCcpLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoO1xyXG4gICAgYWxlcnQod2lkdGgpO1xyXG4gIH0gKi9cclxuXHJcbn1cclxuIl19