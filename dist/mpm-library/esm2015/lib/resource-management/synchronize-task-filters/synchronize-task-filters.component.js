import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { Observable } from 'rxjs';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { LoaderService } from '../../loader/loader.service';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { TaskConstants } from '../../project/tasks/task.constants';
import * as acronui from './../../mpm-utils/auth/utility';
import { SearchDataService } from '../../search/services/search-data.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import * as $ from 'jquery';
import { DeliverableService } from '../../project/tasks/deliverable/deliverable.service';
const moment = extendMoment(Moment);
let SynchronizeTaskFiltersComponent = class SynchronizeTaskFiltersComponent {
    constructor(sharingService, appService, entityAppDefService, utilService, fieldConfigService, filterHelperService, loaderService, viewConfigService, indexerService, deliverableService, otmmService, searchDataService) {
        this.sharingService = sharingService;
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.utilService = utilService;
        this.fieldConfigService = fieldConfigService;
        this.filterHelperService = filterHelperService;
        this.loaderService = loaderService;
        this.viewConfigService = viewConfigService;
        this.indexerService = indexerService;
        this.deliverableService = deliverableService;
        this.otmmService = otmmService;
        this.searchDataService = searchDataService;
        this.deliverableData = new EventEmitter();
        this.selectedUser = new EventEmitter();
        this.selectedFilters = new EventEmitter();
        this.teams = [];
        this.roles = [];
        this.users = [];
        this.momentPtr = moment;
        this.viewTypeCellSize = 35;
        this.userId = [];
        this.selectedItems = [];
        this.allUsers = [];
        this.userIds = [];
        this.OwnerArray = []; // {}
        this.searchConditions = [];
        this.emailSearchCondition = [];
        this.convertedDate = [];
        this.storeSearchconditionObject = null;
        this.subscriptions = [];
        this.teamInitial = '';
        this.roleInitial = '';
        this.userInitial = '';
        this.previousUserId = [];
    }
    GetRoleDNByRoleId(roleId) {
        const allRoles = this.sharingService.getAllRoles();
        return allRoles.find(role => role['MPM_APP_Roles-id'].Id === roleId);
    }
    getRolesAndUser(team) {
        this.selectedTeam = team;
        if (this.selectedItems.indexOf(team) !== -1) {
            console.log("Value exists!");
        }
        else {
            this.selectedItems.push(team);
        }
        /* this.selectedItems.push(team); */
        this.selectedFilters.emit(this.selectedItems);
        console.log(team);
        const allTeamRoles = this.sharingService.getAllTeamRoles();
        console.log(allTeamRoles);
        this.allRoles = [];
        this.users = [];
        allTeamRoles.forEach(teamRole => {
            if (teamRole && teamRole.R_PO_TEAM !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"] !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id !== undefined && teamRole.R_PO_TEAM["MPM_Teams-id"].Id === team['MPM_Teams-id'].Id) {
                const teamRoles = teamRole; //this.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id); // this.utilService.GetRoleDNByRoleId(teamRole['MPM_Team_Role_Mapping-id'].Id);
                console.log(teamRoles);
                if (teamRoles !== undefined) {
                    this.allRoles.push(teamRoles);
                }
            }
        });
        this.getUsersForTeam(team).subscribe(response => {
            console.log(response);
        });
        /*  const getUserByRoleRequest = {
           allocationType: 'ROLE',
           roleDN: team.ROLE_DN,
           teamID: team['MPM_Teams-id'].Id,
           isApproveTask: false,
           isUploadTask: true
         };
         this.appService.getUsersForTeam(getUserByRoleRequest)
           .subscribe(allUsers => {
             if (allUsers && allUsers.users && allUsers.users.user) {
               allUsers.users.user.forEach(user => {
                 this.users.push(user);
               });
             }
           }); */
    }
    getUsersForTeam(team) {
        return new Observable(observer => {
            const getUserByRoleRequest = {
                allocationType: 'ROLE',
                roleDN: team.ROLE_DN,
                teamID: team['MPM_Teams-id'].Id,
                isApproveTask: false,
                isUploadTask: true
            };
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(allUsers => {
                if (allUsers && allUsers.users && allUsers.users.user) {
                    allUsers.users.user.forEach(user => {
                        this.users.push(user);
                    });
                }
                observer.next(true);
                observer.complete();
            });
        });
    }
    getTeamsAndUser(role) {
        this.selectedRole = role;
        //  role.ROLE_NAME = role.ROLE_NAME.replace(/_/g, ' ');
        if (this.selectedItems.indexOf(role) !== -1) {
            console.log("Value exists!");
        }
        else {
            this.selectedItems.push(role);
        }
        /* this.selectedItems.push(role); */
        this.selectedFilters.emit(this.selectedItems);
        console.log(role);
        this.getUsersByRole(role, this.selectedTeam).subscribe(response => {
        });
    }
    getUsersByRole(role, selectedTeam) {
        return new Observable(observer => {
            let getUserByRoleRequest = {};
            this.users = [];
            getUserByRoleRequest = {
                allocationType: TaskConstants.ALLOCATION_TYPE_ROLE,
                roleDN: role.ROLE_DN,
                teamID: selectedTeam ? selectedTeam['MPM_Teams-id'].Id : '',
                isApproveTask: false,
                isUploadTask: true
            };
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(response => {
                if (response && response.users && response.users.user) {
                    const userList = acronui.findObjectsByProp(response, 'user');
                    const users = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(user => {
                            const fieldObj = users.find(e => e.value === user.dn);
                            if (!fieldObj) {
                                users.push({
                                    name: user.name,
                                    value: user.cn,
                                    displayName: user.name,
                                    cn: user.cn
                                });
                            }
                        });
                    }
                    this.users = users;
                }
                else {
                    this.users = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                observer.next(false);
                observer.complete();
            });
        });
    }
    allocationTask(user) {
        const capacity = 8;
        const taskAllocation = 1;
        return taskAllocation + '/' + capacity;
    }
    resourceTaskAllocation(user, currentViewStartDates, currentViewEndDates) {
        const firstDate = moment(currentViewStartDates);
        const secondDate = moment(currentViewEndDates);
        const diffInDays = Math.abs(firstDate.diff(secondDate, 'days'));
        const allocation = [];
        for (let i = 0; i <= diffInDays; i++) {
            allocation[i] = this.allocationTask(user); // '0/8';
        }
        return allocation;
    }
    /* getDeliverable(user, removingFilterValue) {
      console.log(user);
      this.loaderService.show();
      this.userId = [];
      if (removingFilterValue) {
        let userid, itemid;
        this.allUsers.forEach((item, index) => {
          userid = user && user[0] && user[0]['Identity-id'] !== undefined && user[0]['Identity-id'].Id ? user[0]['Identity-id'].Id : user && user[0] && user[0].cn ? user[0].cn : '';
          itemid = item && item['Identity-id'] !== undefined && item['Identity-id'].Id ? item['Identity-id'].Id : item && item.cn ? item.cn : '';
          if (itemid === userid) {
            this.allUsers.splice(index, 1);
  
          }
        });
        this.userId.forEach((id, idx) => {
          const ids = id && id[0] && id[0]['Identity-id'] !== undefined && id[0]['Identity-id'].Id ? id[0]['Identity-id'].Id : id && id[0] && id[0].cn ? id[0].cn : ''
          if (ids === userid) {
            this.userId.splice(idx, 1);
          }
        });
      } else {
        this.allUsers.push(user);
        this.selectedItems.push(user);
        this.selectedFilters.emit(this.selectedItems);
        this.selectedUser.emit(user);
      }
  
      let viewConfig;
      this.allUsers.forEach(element => {
        if (element.cn) {
          this.userId.push(this.allUser.filter(users => users.UserId === element.cn));
        }
      });
     
      let UserId = [];
      if (this.userId.length > 0) {
        this.userId.forEach(id => {
          if (UserId.indexOf(id[0]['Identity-id'].Id) === -1) {
            UserId.push(id[0]['Identity-id'].Id);
          }
  
        })
      } else {
        this.allUsers.forEach(users => {
          UserId.push(users['Identity-id'].Id);
        });
      }
  
      const dates = this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z';
  
      const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, dates, UserId);// user['Identity-id'].Id);
      const otmmMPMDataTypes = OTMMMPMDataTypes.DELIVERABLE;
  
      const condition = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
      condition ? conditionObject.DELIVERABLE_CONDTION.push(condition) : console.log('');
  
  
  
  
      if (this.viewConfigs) {
        viewConfig = this.viewConfigs;
      } else {
        viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
      }
      this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails: ViewConfig) => {
        const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
          ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
        const parameters: SearchRequest = {
          search_config_id: searchConfig ? searchConfig : null,
          keyword: '',
          search_condition_list: {
            search_condition: conditionObject.DELIVERABLE_CONDTION
          },
          facet_condition_list: {
            facet_condition: []
          },
          sorting_list: {
            sort: []
          },
          cursor: {
            page_index: 0,
            page_size: 100
          }
        };
  
        this.loaderService.show();
        if (UserId.length > 0) {
          this.subscriptions.push(
            this.indexerService.search(parameters).subscribe((data: SearchResponse) => {
              console.log(data);
              this.resource = [];
              this.OwnerArray = [];
              if (data.data.length > 0) {
                const dFormat = 'YYYY-MM-DD';
                data.data.forEach((item, index) => {
                  let styles = {};
                  const eventStartDate = this.momentPtr(item.DELIVERABLE_START_DATE, dFormat);
                  const eventEndDate = this.momentPtr(item.DELIVERABLE_DUE_DATE, dFormat);
                  const diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                  const extraPixel = ((diff + 1) % 2) === 0 ? 0 : 2;
                  const width = ((diff + 1) * (this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
  
                  let OwnerName = item.DELIVERABLE_ACTIVE_USER_NAME;
                  if (!this.OwnerArray.hasOwnProperty(OwnerName)) {
                    this.OwnerArray[OwnerName] = [];
                    //  this.resource['allocation'] = [0 / 8];
                    this.resourceTaskAllocation(OwnerName, this.currentViewStartDates, this.currentViewEndDates);
                    //   this.OwnerArray = { [OwnerName]: [] };
                  }
                  let top = 30 * this.OwnerArray[OwnerName].length;
                  styles['width'] = width + 'px';
                  styles['top'] = top + 'px';
  
                  const deliverableDetail = {
                    data: item,
                    style: styles
                  };
                  let k: any, v: any;
                  for ([k, v] of Object.entries(this.OwnerArray)) {
                    if (v.length === 0) {
                      const date = this.datee
                      this.OwnerArray[OwnerName]['date'] = date;// this.datess;
                    }
                  }
                  this.OwnerArray[OwnerName].push({ deliverableDetail });
  
  
                  console.log(this.OwnerArray);
  
                  //  this.resource.push(this.OwnerArray);
                  console.log(this.resource);
                });
                //  this.resource.height = 35 * data.data.length + 'px';
                this.OwnerArray.height = 35 * data.data.length + 'px';
              }
              // this.deliverableData.emit(this.resource);
              this.deliverableData.emit(this.OwnerArray);
              this.resourceAllocation.emit(this.resource);
            })
          );
        } else {
          this.OwnerArray = [];
          this.deliverableData.emit(this.OwnerArray);
        }
  
        this.loaderService.hide();
      });
  
    } */
    getProperty(deliverableData, mapperName) {
        // const level = this.isReviewer ? MPM_LEVELS.DELIVERABLE_REVIEW : MPM_LEVELS.DELIVERABLE;
        const displayColumn = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.DELIVERABLE);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, deliverableData);
    }
    /**
       * @since MPMV3-946
       * @param deliverableType
       */
    getDeliverableTypeIcon(deliverableType) {
        return this.deliverableService.getDeliverableTypeIcon(deliverableType);
    }
    reload(user) {
        console.log("reload");
        console.log(this.selectedItems);
        console.log(user);
        // this.getDeliverable(user, false)
    }
    getDeliverableList(UserId) {
        let viewConfig;
        const dates = this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z' + ' and ' + this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z';
        //MPM-2224
        /*  const conditionObject: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, MPM_LEVELS.TASK, dates, UserId); // this.levels// user['Identity-id'].Id);
       */
        const conditionObject = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, MPM_LEVELS.TASK, this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', UserId, true); // this.levels// user['Identity-id'].Id);
        this.storeSearchconditionObject = Object.assign({}, conditionObject);
        const otmmMPMDataTypes = OTMMMPMDataTypes.DELIVERABLE;
        //MPM-2224
        /* const conditionObjects: TaskSearchConditions = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, dates, UserId); // this.levels// user['Identity-id'].Id);
        */ const conditionObjects = this.filterHelperService.getSearchConditionNew(this.selectedListFilter, this.selectedListDependentFilter, this.levels, this.momentPtr.utc(this.currentViewStartDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', this.momentPtr.utc(this.currentViewEndDates).format('YYYY-MM-DDT00:00:00.000') + 'Z', UserId, true); // this.levels// user['Identity-id'].Id);
        //  this.storeSearchconditionObject = Object.assign({}, conditionObject);
        const otmmMPMDataTypess = OTMMMPMDataTypes.DELIVERABLE;
        const conditions = this.fieldConfigService.createConditionObject('CONTENT_TYPE', MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, otmmMPMDataTypes);
        conditions ? conditionObjects.DELIVERABLE_CONDTION.push(conditions) : console.log('');
        if (this.searchConditions && this.searchConditions.length > 0 && this.searchConditions[0].metadata_field_id === this.searchDataService.KEYWORD_MAPPER) {
            //keyWord = this.searchConditions[0].keyword;
        }
        else if (this.searchConditions && this.searchConditions.length > 0) {
            conditionObject.TASK_CONDITION = conditionObject.TASK_CONDITION.concat(this.searchConditions);
        }
        this.emailSearchCondition = this.emailSearchCondition && Array.isArray(this.emailSearchCondition) ? this.emailSearchCondition : [];
        if (this.viewConfigs) {
            viewConfig = this.viewConfigs;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
            const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
            const parameters = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                grouping: {
                    group_by: 'TASK',
                    group_from_type: otmmMPMDataTypes,
                    search_condition_list: {
                        search_condition: [...conditionObject.DELIVERABLE_CONDTION, ...this.emailSearchCondition]
                    }
                },
                search_condition_list: {
                    search_condition: conditionObject.TASK_CONDITION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 1000
                }
            };
            const parameterss = {
                search_config_id: searchConfig ? searchConfig : null,
                keyword: '',
                search_condition_list: {
                    search_condition: conditionObjects.DELIVERABLE_CONDTION
                },
                facet_condition_list: {
                    facet_condition: []
                },
                sorting_list: {
                    sort: []
                },
                cursor: {
                    page_index: 0,
                    page_size: 100
                }
            };
            this.loaderService.show();
            if (UserId.length > 0) {
                this.subscriptions.push(this.indexerService.search(parameters).subscribe((data) => {
                    this.indexerService.search(parameterss).subscribe((deliverableData) => {
                        console.log(data);
                        console.log(deliverableData);
                        this.resource = [];
                        this.OwnerArray = [];
                        if (data.data.length > 0) {
                            const dFormat = 'YYYY-MM-DD';
                            data.data.forEach((item, itemIndex) => {
                                //checking -deliverableData to deliverable
                                deliverableData.data.forEach((deliverable, itemIndex) => {
                                    if (item.ID === deliverable.TASK_ITEM_ID.split('.')[1]) { //deliverableData.data[itemIndex].TASK_ITEM_ID.split('.')[1]) {
                                        let styles = {};
                                        /*   const eventStartDate = this.momentPtr(deliverableData.data[itemIndex].DELIVERABLE_START_DATE, dFormat);
                                          const eventEndDate = this.momentPtr(deliverableData.data[itemIndex].DELIVERABLE_DUE_DATE, dFormat); */
                                        const eventStartDate = this.momentPtr(deliverable.DELIVERABLE_START_DATE, dFormat);
                                        const eventEndDate = this.momentPtr(deliverable.DELIVERABLE_DUE_DATE, dFormat);
                                        const diff = (eventEndDate.diff(eventStartDate)) / (1000 * 60 * 60 * 24);
                                        const extraPixel = ((diff + 1) % 2) === 0 ? 0 : 1; // 0 : 2;
                                        /*   const extraPixel = ((diff + 1) % 2) === 0 ? 0 : 2; */
                                        const width = ((diff + 1) * (this.viewTypeCellSize + extraPixel)); // ((diff + 1) * (this.viewTypeCellSize - 1));
                                        /* let OwnerName = deliverableData.data[itemIndex].DELIVERABLE_ACTIVE_USER_NAME; */
                                        let OwnerName = deliverable.DELIVERABLE_ACTIVE_USER_NAME;
                                        if (!this.OwnerArray.hasOwnProperty(OwnerName)) {
                                            this.OwnerArray[OwnerName] = [];
                                            this.resourceTaskAllocation(OwnerName, this.currentViewStartDates, this.currentViewEndDates);
                                        }
                                        let top = 44 * this.OwnerArray[OwnerName].length; // 30
                                        //    styles['width'] = (diff !== 1 ? (((diff + 1) % 2) === 0) ? ((width) + 'px') : (width + 4) + 'px' : (width) + 'px');
                                        styles['top'] = top + 'px';
                                        const deliverableDetail = {
                                            data: deliverable,
                                            name: (this.levels.includes('TASK')) ? item.TASK_NAME : item.PROJECT_NAME,
                                            /* icon: this.getDeliverableTypeIcon(this.getProperty(deliverableData.data[itemIndex], 'DELIVERABLE_TYPE')).ICON,
                                             */
                                            icon: this.getDeliverableTypeIcon(this.getProperty(deliverable, 'DELIVERABLE_TYPE')).ICON,
                                            style: styles
                                        };
                                        let k, v;
                                        for ([k, v] of Object.entries(this.OwnerArray)) {
                                            if (v.length === 0) {
                                                const date = $.extend(true, [], this.convertedDate); // this.convertedDate;//JSON.parse(JSON.stringify(this.datee))//$.extend(true,[],this.datee)
                                                this.OwnerArray[OwnerName]['date'] = date;
                                                this.OwnerArray[OwnerName]['allocation'] = this.resourceTaskAllocation(OwnerName, this.currentViewStartDates, this.currentViewEndDates);
                                            }
                                        }
                                        this.OwnerArray[OwnerName].push({ deliverableDetail });
                                    }
                                });
                            });
                            this.OwnerArray.height = 50 * data.data.length + 'px'; // 35 //50
                        }
                        this.loaderService.hide();
                        this.deliverableData.emit(this.OwnerArray);
                        //   this.resourceAllocation.emit(this.resource);
                    });
                }));
            }
            else {
                this.loaderService.hide();
                this.OwnerArray = [];
                this.deliverableData.emit(this.OwnerArray);
            }
        });
    }
    // search by task-grouping and map the name to deliverable search
    getDeliverable(user, removingFilterValue, isParent) {
        console.log(user);
        this.loaderService.show();
        let UserId = [];
        if (isParent) {
            const userid = this.allUser.find(users => users.UserId === user);
            // if (this.previousUserId.includes(userid['Identity-id'].Id)) {
            if ((userid && userid['Identity-id'] && userid['Identity-id'].Id !== undefined && this.previousUserId.includes(userid['Identity-id'].Id)) ||
                (userid === undefined)) {
                UserId = this.previousUserId;
                this.getDeliverableList(UserId);
            }
            else {
                this.loaderService.hide();
            }
        }
        else {
            this.userId = [];
            if (removingFilterValue) {
                let userid, itemid;
                this.allUsers.forEach((item, index) => {
                    userid = user && user[0] && user[0]['Identity-id'] !== undefined && user[0]['Identity-id'].Id ? user[0]['Identity-id'].Id : user && user[0] && user[0].cn ? user[0].cn : '';
                    itemid = item && item['Identity-id'] !== undefined && item['Identity-id'].Id ? item['Identity-id'].Id : item && item.cn ? item.cn : '';
                    if (itemid === userid) {
                        this.allUsers.splice(index, 1);
                    }
                });
                this.userId.forEach((id, idx) => {
                    const ids = id && id[0] && id[0]['Identity-id'] !== undefined && id[0]['Identity-id'].Id ? id[0]['Identity-id'].Id : id && id[0] && id[0].cn ? id[0].cn : '';
                    if (ids === userid) {
                        this.userId.splice(idx, 1);
                    }
                });
            }
            else {
                this.allUsers.push(user);
                if (this.selectedItems.indexOf(user) !== -1) {
                    console.log("Value exists!");
                }
                else {
                    this.selectedItems.push(user);
                }
                /* this.selectedItems.push(user); */
                this.selectedFilters.emit(this.selectedItems);
                this.selectedUser.emit(user);
            }
            this.allUsers.forEach(element => {
                if (element.cn) {
                    this.userId.push(this.allUser.filter(users => users.UserId === element.cn));
                    //   this.userIds.push(this.userId);
                }
            });
            /* let UserId = []; */
            if (this.userId.length > 0) {
                this.userId.forEach(id => {
                    if (UserId.indexOf(id[0]['Identity-id'].Id) === -1) {
                        UserId.push(id[0]['Identity-id'].Id);
                    }
                });
            }
            else {
                this.allUsers.forEach(users => {
                    UserId.push(users['Identity-id'].Id);
                });
            }
            this.previousUserId = UserId;
            this.getDeliverableList(UserId);
        }
    }
    ngOnChanges(changes) {
        console.log(changes);
        const currentStartDate = this.currentViewStartDates && this.currentViewStartDates._i !== undefined ? this.currentViewStartDates._i : this.currentViewStartDates;
        if (changes && changes.datee && changes.datee.currentValue && changes.datee.currentValue[0] && changes.datee.currentValue[0]._i &&
            !changes.datee.firstChange) {
            this.convertedDate = [];
            changes.datee.currentValue.forEach(element => {
                const x = element.format('YYYY-MM-DD');
                const date = {
                    date: x,
                    element: element.format('dd DD'),
                    isOverlap: 'false'
                };
                this.convertedDate.push(date);
            });
            console.log(this.convertedDate);
        }
        this.getDeliverable(this.removedFilter, true);
        // To clear the selected value
        if (this.removedFilter && this.removedFilter.length > 0 && this.removedFilter[0] === 'TEAM') {
            this.teamInitial = '';
            this.selectedTeam = '';
            if (this.selectedRole) {
            }
            else {
                this.roleInitial = '';
                this.userInitial = '';
                if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                    this.inputName.nativeElement.value = 'Users';
                }
                // added back all roles and users
                this.allRoles = this.sharingService.getAllTeamRoles(); //this.sharingService.getAllRoles();
                this.appService.getAllUsers().subscribe(allUsers => {
                    if (allUsers && allUsers.User) {
                        this.allUser = allUsers.User;
                        allUsers.User.forEach(user => {
                            if (user && user.FullName && typeof (user.FullName) !== 'object') {
                                this.users.push(user);
                            }
                        });
                    }
                });
            }
        }
        else if (this.removedFilter && this.removedFilter.length > 0 && this.removedFilter[0] === 'ROLE') {
            this.roleInitial = '';
            this.selectedRole = '';
            if (this.selectedTeam) {
                /*  if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                   this.inputName.nativeElement.value = 'Users';
                 } */
                this.getUsersForTeam(this.selectedTeam).subscribe(response => {
                    console.log(response);
                });
            }
            else {
                this.userInitial = '';
                if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                    this.inputName.nativeElement.value = 'Users';
                }
                this.appService.getAllUsers().subscribe(allUsers => {
                    if (allUsers && allUsers.User) {
                        this.allUser = allUsers.User;
                        allUsers.User.forEach(user => {
                            if (user && user.FullName && typeof (user.FullName) !== 'object') {
                                this.users.push(user);
                            }
                        });
                    }
                });
            }
            /*  this.getUsersByRole(this.selectedRole, this.selectedTeam).subscribe(response => {
       
             }); */
        }
        else if (this.removedFilter && this.removedFilter.length > 0 && (this.removedFilter[0].name !== undefined || this.removedFilter[0].FullName !== undefined)) {
            if (this.inputName && this.inputName.nativeElement && this.inputName.nativeElement.value) {
                this.inputName.nativeElement.value = 'Users';
            }
            this.userInitial = '';
        }
        this.removedFilter = [];
    }
    ngOnInit() {
        // this.level = this.groupBy && this.groupBy.value && this.groupBy.value.split('_')[2] ? this.groupBy.value.split('_')[2] : '';
        this.allTeams = this.sharingService.getAllTeams();
        /* this.allTeams.forEach(team => {
          this.teams.push(team);
        }); */
        this.allRoles = this.sharingService.getAllTeamRoles(); //this.sharingService.getAllRoles();
        /*  this.allRoles.forEach(role => {
           role.ROLE_NAME = role.ROLE_NAME.replace(/_/g, ' ');
         }); */
        this.appService.getAllUsers().subscribe(allUsers => {
            if (allUsers && allUsers.User) {
                this.allUser = allUsers.User;
                allUsers.User.forEach(user => {
                    if (user && user.FullName && typeof (user.FullName) !== 'object') {
                        this.users.push(user);
                    }
                });
            }
        });
        this.datee.forEach(element => {
            const x = element.format('YYYY-MM-DD');
            const date = {
                date: x,
                element: element.format('dd DD'),
                isOverlap: 'false'
            };
            this.convertedDate.push(date);
        });
        console.log(this.convertedDate);
        // to change search
        this.subscriptions.push(this.searchDataService.searchData.subscribe(data => {
            this.searchConditions = (data && Array.isArray(data) ? this.otmmService.addRelationalOperator(data) : []);
        }));
    }
    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
};
SynchronizeTaskFiltersComponent.ctorParameters = () => [
    { type: SharingService },
    { type: AppService },
    { type: EntityAppDefService },
    { type: UtilService },
    { type: FieldConfigService },
    { type: FilterHelperService },
    { type: LoaderService },
    { type: ViewConfigService },
    { type: IndexerService },
    { type: DeliverableService },
    { type: OTMMService },
    { type: SearchDataService }
];
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "groupBy", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "selectedMonth", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "currentViewStartDates", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "currentViewEndDates", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "levels", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "viewConfigs", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "removedFilter", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "datess", void 0);
__decorate([
    Input()
], SynchronizeTaskFiltersComponent.prototype, "datee", void 0);
__decorate([
    Output()
], SynchronizeTaskFiltersComponent.prototype, "deliverableData", void 0);
__decorate([
    Output()
], SynchronizeTaskFiltersComponent.prototype, "selectedUser", void 0);
__decorate([
    Output()
], SynchronizeTaskFiltersComponent.prototype, "selectedFilters", void 0);
__decorate([
    ViewChild('name')
], SynchronizeTaskFiltersComponent.prototype, "inputName", void 0);
SynchronizeTaskFiltersComponent = __decorate([
    Component({
        selector: 'mpm-synchronize-task-filters',
        template: "<p>synchronize-task-filters works!</p>\r\n",
        styles: [""]
    })
], SynchronizeTaskFiltersComponent);
export { SynchronizeTaskFiltersComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvc3luY2hyb25pemUtdGFzay1maWx0ZXJzL3N5bmNocm9uaXplLXRhc2stZmlsdGVycy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxZQUFZLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRTNELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDNUMsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLFVBQVUsRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFFaEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUVBQXFFLENBQUM7QUFDekcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFDakgsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFHdEYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkUsT0FBTyxLQUFLLE9BQU8sTUFBTSxnQ0FBZ0MsQ0FBQztBQUMxRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFekYsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBT3BDLElBQWEsK0JBQStCLEdBQTVDLE1BQWEsK0JBQStCO0lBc0QxQyxZQUNTLGNBQThCLEVBQzlCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixrQkFBc0MsRUFDdEMsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLGlCQUFvQyxFQUNwQyxjQUE4QixFQUM5QixrQkFBc0MsRUFDdEMsV0FBd0IsRUFDeEIsaUJBQW9DO1FBWHBDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQXREbkMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2QyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFLcEQsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gsY0FBUyxHQUFHLE1BQU0sQ0FBQztRQUluQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFdEIsV0FBTSxHQUFHLEVBQUUsQ0FBQztRQUdaLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBRXhCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFDZCxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2IsZUFBVSxHQUFRLEVBQUUsQ0FBQyxDQUFDLEtBQUs7UUFFM0IscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUUxQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUVuQiwrQkFBMEIsR0FBeUIsSUFBSSxDQUFDO1FBRWhELGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUloRCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUVqQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztJQWVoQixDQUFDO0lBRUwsaUJBQWlCLENBQUMsTUFBTTtRQUN0QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25ELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsZUFBZSxDQUFDLElBQUk7UUFDbEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMvQjtRQUNELG9DQUFvQztRQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsQixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzNELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDaEIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM5QixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsU0FBUyxLQUFLLFNBQVMsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxLQUFLLFNBQVMsSUFBSSxRQUFRLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxTQUFTLElBQUksUUFBUSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDaE8sTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFBLENBQUEsa0pBQWtKO2dCQUM1SyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO2dCQUN0QixJQUFJLFNBQVMsS0FBSyxTQUFTLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUMvQjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ0g7Ozs7Ozs7Ozs7Ozs7O2lCQWNTO0lBQ1gsQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFJO1FBQ2xCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsTUFBTSxvQkFBb0IsR0FBRztnQkFDM0IsY0FBYyxFQUFFLE1BQU07Z0JBQ3RCLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDcEIsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO2dCQUMvQixhQUFhLEVBQUUsS0FBSztnQkFDcEIsWUFBWSxFQUFFLElBQUk7YUFDbkIsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDO2lCQUNsRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7b0JBQ3JELFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDakMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFJO1FBQ2xCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLHVEQUF1RDtRQUN2RCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUE7U0FDN0I7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9CO1FBQ0Qsb0NBQW9DO1FBQ3BDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWxCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFFbEUsQ0FBQyxDQUFDLENBQUM7SUFHTCxDQUFDO0lBRUQsY0FBYyxDQUFDLElBQUksRUFBRSxZQUFZO1FBQy9CLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxvQkFBb0IsR0FBRyxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsb0JBQW9CLEdBQUc7Z0JBQ3JCLGNBQWMsRUFBRSxhQUFhLENBQUMsb0JBQW9CO2dCQUNsRCxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU87Z0JBQ3BCLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzNELGFBQWEsRUFBRSxLQUFLO2dCQUNwQixZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDO1lBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtvQkFDckQsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsTUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN0RCxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUN0QixNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7Z0NBQ2IsS0FBSyxDQUFDLElBQUksQ0FBQztvQ0FDVCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0NBQ2YsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO29DQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtvQ0FDdEIsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFO2lDQUNaLENBQUMsQ0FBQzs2QkFDSjt3QkFDSCxDQUFDLENBQUMsQ0FBQztxQkFDSjtvQkFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztpQkFDcEI7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7aUJBQ2pCO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNOLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJO1FBQ2pCLE1BQU0sUUFBUSxHQUFHLENBQUMsQ0FBQztRQUNuQixNQUFNLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDekIsT0FBTyxjQUFjLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQztJQUN6QyxDQUFDO0lBRUQsc0JBQXNCLENBQUMsSUFBSSxFQUFFLHFCQUFxQixFQUFFLG1CQUFtQjtRQUNyRSxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNoRCxNQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMvQyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDaEUsTUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEMsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxTQUFTO1NBQ3BEO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBb0pJO0lBRUosV0FBVyxDQUFDLGVBQWUsRUFBRSxVQUFVO1FBQ3JDLDBGQUEwRjtRQUMxRixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9ILE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGFBQWEsRUFBRSxlQUFlLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQ7OztTQUdLO0lBQ0wsc0JBQXNCLENBQUMsZUFBZTtRQUNwQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsTUFBTSxDQUFDLElBQUk7UUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEIsbUNBQW1DO0lBQ3JDLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxNQUFNO1FBQ3ZCLElBQUksVUFBVSxDQUFDO1FBQ2YsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLENBQUM7UUFDdE0sVUFBVTtRQUNWO1NBQ0M7UUFDRCxNQUFNLGVBQWUsR0FBeUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLHlDQUF5QztRQUMvWSxJQUFJLENBQUMsMEJBQTBCLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDckUsTUFBTSxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7UUFFMUQsVUFBVTtRQUNOO1VBQ0UsQ0FBQSxNQUFNLGdCQUFnQixHQUF5QixJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsR0FBRyxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMseUNBQXlDO1FBRTlZLHlFQUF5RTtRQUN6RSxNQUFNLGlCQUFpQixHQUFHLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztRQUV2RCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLEVBQUUsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsa0JBQWtCLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDN0ssVUFBVSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFdEYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUU7WUFDckosNkNBQTZDO1NBQzlDO2FBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEUsZUFBZSxDQUFDLGNBQWMsR0FBRyxlQUFlLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMvRjtRQUVELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFbkksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQy9CO2FBQU07WUFDTCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztTQUNyRDtRQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7WUFDdkcsTUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixJQUFJLE9BQU8sV0FBVyxDQUFDLDJCQUEyQixLQUFLLFFBQVE7Z0JBQ3pILENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDakUsTUFBTSxVQUFVLEdBQWtCO2dCQUNoQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDcEQsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsUUFBUSxFQUFFO29CQUNSLFFBQVEsRUFBRSxNQUFNO29CQUNoQixlQUFlLEVBQUUsZ0JBQWdCO29CQUNqQyxxQkFBcUIsRUFBRTt3QkFDckIsZ0JBQWdCLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxvQkFBb0IsRUFBRSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztxQkFDMUY7aUJBQ0Y7Z0JBQ0QscUJBQXFCLEVBQUU7b0JBQ3JCLGdCQUFnQixFQUFFLGVBQWUsQ0FBQyxjQUFjO2lCQUNqRDtnQkFDRCxvQkFBb0IsRUFBRTtvQkFDcEIsZUFBZSxFQUFFLEVBQUU7aUJBQ3BCO2dCQUNELFlBQVksRUFBRTtvQkFDWixJQUFJLEVBQUUsRUFBRTtpQkFDVDtnQkFDRCxNQUFNLEVBQUU7b0JBQ04sVUFBVSxFQUFFLENBQUM7b0JBQ2IsU0FBUyxFQUFFLElBQUk7aUJBQ2hCO2FBQ0YsQ0FBQztZQUVGLE1BQU0sV0FBVyxHQUFrQjtnQkFDakMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUk7Z0JBQ3BELE9BQU8sRUFBRSxFQUFFO2dCQUNYLHFCQUFxQixFQUFFO29CQUNyQixnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQyxvQkFBb0I7aUJBQ3hEO2dCQUNELG9CQUFvQixFQUFFO29CQUNwQixlQUFlLEVBQUUsRUFBRTtpQkFDcEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLElBQUksRUFBRSxFQUFFO2lCQUNUO2dCQUNELE1BQU0sRUFBRTtvQkFDTixVQUFVLEVBQUUsQ0FBQztvQkFDYixTQUFTLEVBQUUsR0FBRztpQkFDZjthQUNGLENBQUM7WUFFRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNyQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFvQixFQUFFLEVBQUU7b0JBQ3hFLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGVBQStCLEVBQUUsRUFBRTt3QkFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7d0JBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO3dCQUNyQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDeEIsTUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDOzRCQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsRUFBRTtnQ0FDcEMsMENBQTBDO2dDQUMxQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFdBQVcsRUFBRSxTQUFTLEVBQUUsRUFBRTtvQ0FDdEQsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUMsK0RBQStEO3dDQUN0SCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7d0NBQ2hCO2dKQUN3Rzt3Q0FDeEcsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLEVBQUUsT0FBTyxDQUFDLENBQUM7d0NBQ25GLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxDQUFDO3dDQUMvRSxNQUFNLElBQUksR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dDQUN6RSxNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxTQUFTO3dDQUMzRCwwREFBMEQ7d0NBQzFELE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLDhDQUE4Qzt3Q0FFakgsbUZBQW1GO3dDQUNuRixJQUFJLFNBQVMsR0FBRyxXQUFXLENBQUMsNEJBQTRCLENBQUM7d0NBQ3pELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsRUFBRTs0Q0FDOUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7NENBQ2hDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3lDQUU5Rjt3Q0FFRCxJQUFJLEdBQUcsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQSxLQUFLO3dDQUN0RCx5SEFBeUg7d0NBRXpILE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDO3dDQUUzQixNQUFNLGlCQUFpQixHQUFHOzRDQUN4QixJQUFJLEVBQUUsV0FBVzs0Q0FDakIsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVk7NENBQ3pFOytDQUNHOzRDQUNILElBQUksRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUk7NENBRXpGLEtBQUssRUFBRSxNQUFNO3lDQUNkLENBQUM7d0NBQ0YsSUFBSSxDQUFNLEVBQUUsQ0FBTSxDQUFDO3dDQUNuQixLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRDQUM5QyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dEQUVsQixNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBLENBQUEsNEZBQTRGO2dEQUMvSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztnREFDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzs2Q0FDekk7eUNBQ0Y7d0NBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUM7cUNBQ3hEO2dDQUNILENBQUMsQ0FBQyxDQUFBOzRCQUVKLENBQUMsQ0FBQyxDQUFDOzRCQUNILElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxVQUFVO3lCQUNsRTt3QkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQzNDLGlEQUFpRDtvQkFDbkQsQ0FBQyxDQUFDLENBQUE7Z0JBQ0osQ0FBQyxDQUFDLENBQ0gsQ0FBQzthQUNIO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dCQUNyQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDNUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpRUFBaUU7SUFDakUsY0FBYyxDQUFDLElBQUksRUFBRSxtQkFBbUIsRUFBRSxRQUFTO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUUxQixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxRQUFRLEVBQUU7WUFDWixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLENBQUM7WUFDakUsZ0VBQWdFO1lBQ2hFLElBQUksQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdkksQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLEVBQUU7Z0JBQ3hCLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2dCQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDakM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUMzQjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNqQixJQUFJLG1CQUFtQixFQUFFO2dCQUN2QixJQUFJLE1BQU0sRUFBRSxNQUFNLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNwQyxNQUFNLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUM1SyxNQUFNLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDdkksSUFBSSxNQUFNLEtBQUssTUFBTSxFQUFFO3dCQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBRWhDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFO29CQUM5QixNQUFNLEdBQUcsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsS0FBSyxTQUFTLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7b0JBQzVKLElBQUksR0FBRyxLQUFLLE1BQU0sRUFBRTt3QkFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUM1QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFBO2lCQUM3QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDL0I7Z0JBQ0Qsb0NBQW9DO2dCQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzlCO1lBR0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzlCLElBQUksT0FBTyxDQUFDLEVBQUUsRUFBRTtvQkFDZCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzVFLG9DQUFvQztpQkFDckM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVILHNCQUFzQjtZQUN0QixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUU7b0JBQ3ZCLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ2xELE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3FCQUN0QztnQkFFSCxDQUFDLENBQUMsQ0FBQTthQUNIO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDO1lBRTdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUVqQztJQUVILENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQixNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDO1FBQ2hLLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxZQUFZLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUM3SCxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO1lBQzVCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDM0MsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDdkMsTUFBTSxJQUFJLEdBQUc7b0JBQ1gsSUFBSSxFQUFFLENBQUM7b0JBQ1AsT0FBTyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO29CQUNoQyxTQUFTLEVBQUUsT0FBTztpQkFDbkIsQ0FBQTtnQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDhCQUE4QjtRQUM5QixJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEtBQUssTUFBTSxFQUFFO1lBQzNGLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTthQUV0QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQ3hGLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUM7aUJBQzlDO2dCQUNELGlDQUFpQztnQkFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUEsb0NBQW9DO2dCQUMxRixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDakQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTt3QkFDN0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO3dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDM0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLFFBQVEsRUFBRTtnQ0FDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBQ3ZCO3dCQUNILENBQUMsQ0FBQyxDQUFDO3FCQUNKO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FFRjthQUFNLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLEVBQUU7WUFDbEcsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdkIsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNyQjs7cUJBRUs7Z0JBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUN0QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFO29CQUN4RixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO2lCQUM5QztnQkFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDakQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTt3QkFDN0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO3dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDM0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLFFBQVEsRUFBRTtnQ0FDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBQ3ZCO3dCQUNILENBQUMsQ0FBQyxDQUFDO3FCQUNKO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFDRDs7bUJBRU87U0FFUjthQUFNLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssU0FBUyxDQUFDLEVBQUU7WUFDNUosSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtnQkFDeEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQzthQUM5QztZQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELFFBQVE7UUFDTiwrSEFBK0g7UUFDL0gsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2xEOztjQUVNO1FBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUEsb0NBQW9DO1FBQzFGOztlQUVPO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDakQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLFFBQVEsRUFBRTt3QkFDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3ZCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQzNCLE1BQU0sQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdkMsTUFBTSxJQUFJLEdBQUc7Z0JBQ1gsSUFBSSxFQUFFLENBQUM7Z0JBQ1AsT0FBTyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUNoQyxTQUFTLEVBQUUsT0FBTzthQUNuQixDQUFBO1lBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoQyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3JCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU1RyxDQUFDLENBQUMsQ0FDSCxDQUFDO0lBRUosQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7Q0FDRixDQUFBOztZQXJyQjBCLGNBQWM7WUFDbEIsVUFBVTtZQUNELG1CQUFtQjtZQUMzQixXQUFXO1lBQ0osa0JBQWtCO1lBQ2pCLG1CQUFtQjtZQUN6QixhQUFhO1lBQ1QsaUJBQWlCO1lBQ3BCLGNBQWM7WUFDVixrQkFBa0I7WUFDekIsV0FBVztZQUNMLGlCQUFpQjs7QUFoRXBDO0lBQVIsS0FBSyxFQUFFO2dFQUFTO0FBQ1I7SUFBUixLQUFLLEVBQUU7c0VBQWU7QUFDZDtJQUFSLEtBQUssRUFBRTs4RUFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7NEVBQXFCO0FBQ3BCO0lBQVIsS0FBSyxFQUFFOytEQUFRO0FBQ1A7SUFBUixLQUFLLEVBQUU7b0VBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTtzRUFBZTtBQUNkO0lBQVIsS0FBSyxFQUFFOytEQUFRO0FBQ1A7SUFBUixLQUFLLEVBQUU7OERBQU87QUFFTDtJQUFULE1BQU0sRUFBRTt3RUFBMkM7QUFDMUM7SUFBVCxNQUFNLEVBQUU7cUVBQXdDO0FBQ3ZDO0lBQVQsTUFBTSxFQUFFO3dFQUEyQztBQWdDakM7SUFBbEIsU0FBUyxDQUFDLE1BQU0sQ0FBQztrRUFBVztBQTlDbEIsK0JBQStCO0lBTDNDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSw4QkFBOEI7UUFDeEMsc0RBQXdEOztLQUV6RCxDQUFDO0dBQ1csK0JBQStCLENBNHVCM0M7U0E1dUJZLCtCQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFNpbXBsZUNoYW5nZXMsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgZXh0ZW5kTW9tZW50IH0gZnJvbSAnbW9tZW50LXJhbmdlJztcclxuaW1wb3J0ICogYXMgTW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUYXNrU2VhcmNoQ29uZGl0aW9ucyB9IGZyb20gJy4uL29iamVjdC9SZXNvdXJjZU1hbmFnZW1lbnRDb21wb25lbnRQYXJhbXMnO1xyXG5pbXBvcnQgeyBGaWx0ZXJIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpbHRlci1oZWxwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9ycyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvci5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvck5hbWVzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yTmFtZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TZWFyY2hDb25maWdDb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXF1ZXN0IH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9TZWFyY2hSZXF1ZXN0JztcclxuaW1wb3J0IHsgU2VhcmNoUmVzcG9uc2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlc3BvbnNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1NUE1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kZXhlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9pbmRleGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vcHJvamVjdC90YXNrcy90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgU2VhcmNoRGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZWFyY2gvc2VydmljZXMvc2VhcmNoLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuXHJcbmNvbnN0IG1vbWVudCA9IGV4dGVuZE1vbWVudChNb21lbnQpO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tc3luY2hyb25pemUtdGFzay1maWx0ZXJzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vc3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9zeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTeW5jaHJvbml6ZVRhc2tGaWx0ZXJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgZ3JvdXBCeTtcclxuICBASW5wdXQoKSBzZWxlY3RlZE1vbnRoO1xyXG4gIEBJbnB1dCgpIGN1cnJlbnRWaWV3U3RhcnREYXRlcztcclxuICBASW5wdXQoKSBjdXJyZW50Vmlld0VuZERhdGVzO1xyXG4gIEBJbnB1dCgpIGxldmVscztcclxuICBASW5wdXQoKSB2aWV3Q29uZmlncztcclxuICBASW5wdXQoKSByZW1vdmVkRmlsdGVyO1xyXG4gIEBJbnB1dCgpIGRhdGVzcztcclxuICBASW5wdXQoKSBkYXRlZTtcclxuXHJcbiAgQE91dHB1dCgpIGRlbGl2ZXJhYmxlRGF0YSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZFVzZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgc2VsZWN0ZWRGaWx0ZXJzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgLy8gQE91dHB1dCgpIHJlc291cmNlQWxsb2NhdGlvbiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBhbGxUZWFtcztcclxuICBhbGxSb2xlcztcclxuICB0ZWFtcyA9IFtdO1xyXG4gIHJvbGVzID0gW107XHJcbiAgdXNlcnMgPSBbXTtcclxuICBtb21lbnRQdHIgPSBtb21lbnQ7XHJcbiAgcmVzb3VyY2U7XHJcbiAgc2VsZWN0ZWRMaXN0RmlsdGVyO1xyXG4gIHNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlcjtcclxuICB2aWV3VHlwZUNlbGxTaXplID0gMzU7XHJcbiAgYWxsVXNlcjtcclxuICB1c2VySWQgPSBbXTtcclxuICBzZWxlY3RlZFRlYW07XHJcbiAgc2VsZWN0ZWRSb2xlO1xyXG4gIHNlbGVjdGVkSXRlbXM6IGFueSA9IFtdO1xyXG5cclxuICBhbGxVc2VycyA9IFtdO1xyXG4gIHVzZXJJZHMgPSBbXTtcclxuICBPd25lckFycmF5OiBhbnkgPSBbXTsgLy8ge31cclxuXHJcbiAgc2VhcmNoQ29uZGl0aW9ucyA9IFtdO1xyXG4gIGVtYWlsU2VhcmNoQ29uZGl0aW9uID0gW107XHJcblxyXG4gIGNvbnZlcnRlZERhdGUgPSBbXTtcclxuXHJcbiAgc3RvcmVTZWFyY2hjb25kaXRpb25PYmplY3Q6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gbnVsbDtcclxuXHJcbiAgcHJpdmF0ZSBzdWJzY3JpcHRpb25zOiBBcnJheTxTdWJzY3JpcHRpb24+ID0gW107XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ25hbWUnKSBpbnB1dE5hbWU7XHJcblxyXG4gIHRlYW1Jbml0aWFsID0gJyc7XHJcbiAgcm9sZUluaXRpYWwgPSAnJztcclxuICB1c2VySW5pdGlhbCA9ICcnO1xyXG5cclxuICBwcmV2aW91c1VzZXJJZCA9IFtdO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBmaWx0ZXJIZWxwZXJTZXJ2aWNlOiBGaWx0ZXJIZWxwZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZSxcclxuICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2VhcmNoRGF0YVNlcnZpY2U6IFNlYXJjaERhdGFTZXJ2aWNlLFxyXG4gICkgeyB9XHJcblxyXG4gIEdldFJvbGVETkJ5Um9sZUlkKHJvbGVJZCkge1xyXG4gICAgY29uc3QgYWxsUm9sZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFJvbGVzKCk7XHJcbiAgICByZXR1cm4gYWxsUm9sZXMuZmluZChyb2xlID0+IHJvbGVbJ01QTV9BUFBfUm9sZXMtaWQnXS5JZCA9PT0gcm9sZUlkKTtcclxuICB9XHJcblxyXG4gIGdldFJvbGVzQW5kVXNlcih0ZWFtKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkVGVhbSA9IHRlYW07XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZEl0ZW1zLmluZGV4T2YodGVhbSkgIT09IC0xKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiVmFsdWUgZXhpc3RzIVwiKVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zLnB1c2godGVhbSk7XHJcbiAgICB9XHJcbiAgICAvKiB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaCh0ZWFtKTsgKi9cclxuICAgIHRoaXMuc2VsZWN0ZWRGaWx0ZXJzLmVtaXQodGhpcy5zZWxlY3RlZEl0ZW1zKTtcclxuICAgIGNvbnNvbGUubG9nKHRlYW0pO1xyXG4gICAgY29uc3QgYWxsVGVhbVJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKTtcclxuICAgIGNvbnNvbGUubG9nKGFsbFRlYW1Sb2xlcyk7XHJcbiAgICB0aGlzLmFsbFJvbGVzID0gW107XHJcbiAgICB0aGlzLnVzZXJzID0gW107XHJcbiAgICBhbGxUZWFtUm9sZXMuZm9yRWFjaCh0ZWFtUm9sZSA9PiB7XHJcbiAgICAgIGlmICh0ZWFtUm9sZSAmJiB0ZWFtUm9sZS5SX1BPX1RFQU0gIT09IHVuZGVmaW5lZCAmJiB0ZWFtUm9sZS5SX1BPX1RFQU1bXCJNUE1fVGVhbXMtaWRcIl0gIT09IHVuZGVmaW5lZCAmJiB0ZWFtUm9sZS5SX1BPX1RFQU1bXCJNUE1fVGVhbXMtaWRcIl0uSWQgIT09IHVuZGVmaW5lZCAmJiB0ZWFtUm9sZS5SX1BPX1RFQU1bXCJNUE1fVGVhbXMtaWRcIl0uSWQgPT09IHRlYW1bJ01QTV9UZWFtcy1pZCddLklkKSB7XHJcbiAgICAgICAgY29uc3QgdGVhbVJvbGVzID0gdGVhbVJvbGUvL3RoaXMuR2V0Um9sZUROQnlSb2xlSWQodGVhbVJvbGVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkKTsgLy8gdGhpcy51dGlsU2VydmljZS5HZXRSb2xlRE5CeVJvbGVJZCh0ZWFtUm9sZVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRlYW1Sb2xlcylcclxuICAgICAgICBpZiAodGVhbVJvbGVzICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgIHRoaXMuYWxsUm9sZXMucHVzaCh0ZWFtUm9sZXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmdldFVzZXJzRm9yVGVhbSh0ZWFtKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICB9KTtcclxuICAgIC8qICBjb25zdCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgIGFsbG9jYXRpb25UeXBlOiAnUk9MRScsXHJcbiAgICAgICByb2xlRE46IHRlYW0uUk9MRV9ETixcclxuICAgICAgIHRlYW1JRDogdGVhbVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgIH07XHJcbiAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgIC5zdWJzY3JpYmUoYWxsVXNlcnMgPT4ge1xyXG4gICAgICAgICBpZiAoYWxsVXNlcnMgJiYgYWxsVXNlcnMudXNlcnMgJiYgYWxsVXNlcnMudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgIGFsbFVzZXJzLnVzZXJzLnVzZXIuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgIHRoaXMudXNlcnMucHVzaCh1c2VyKTtcclxuICAgICAgICAgICB9KTtcclxuICAgICAgICAgfVxyXG4gICAgICAgfSk7ICovXHJcbiAgfVxyXG5cclxuICBnZXRVc2Vyc0ZvclRlYW0odGVhbSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICBhbGxvY2F0aW9uVHlwZTogJ1JPTEUnLFxyXG4gICAgICAgIHJvbGVETjogdGVhbS5ST0xFX0ROLFxyXG4gICAgICAgIHRlYW1JRDogdGVhbVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0VXNlckJ5Um9sZVJlcXVlc3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShhbGxVc2VycyA9PiB7XHJcbiAgICAgICAgICBpZiAoYWxsVXNlcnMgJiYgYWxsVXNlcnMudXNlcnMgJiYgYWxsVXNlcnMudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICBhbGxVc2Vycy51c2Vycy51c2VyLmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy51c2Vycy5wdXNoKHVzZXIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIGdldFRlYW1zQW5kVXNlcihyb2xlKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkUm9sZSA9IHJvbGU7XHJcbiAgICAvLyAgcm9sZS5ST0xFX05BTUUgPSByb2xlLlJPTEVfTkFNRS5yZXBsYWNlKC9fL2csICcgJyk7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZEl0ZW1zLmluZGV4T2Yocm9sZSkgIT09IC0xKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiVmFsdWUgZXhpc3RzIVwiKVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zLnB1c2gocm9sZSk7XHJcbiAgICB9XHJcbiAgICAvKiB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaChyb2xlKTsgKi9cclxuICAgIHRoaXMuc2VsZWN0ZWRGaWx0ZXJzLmVtaXQodGhpcy5zZWxlY3RlZEl0ZW1zKTtcclxuICAgIGNvbnNvbGUubG9nKHJvbGUpO1xyXG5cclxuICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUocm9sZSwgdGhpcy5zZWxlY3RlZFRlYW0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcblxyXG4gICAgfSk7XHJcblxyXG5cclxuICB9XHJcblxyXG4gIGdldFVzZXJzQnlSb2xlKHJvbGUsIHNlbGVjdGVkVGVhbSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBsZXQgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7fTtcclxuICAgICAgdGhpcy51c2VycyA9IFtdO1xyXG4gICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICBhbGxvY2F0aW9uVHlwZTogVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSxcclxuICAgICAgICByb2xlRE46IHJvbGUuUk9MRV9ETixcclxuICAgICAgICB0ZWFtSUQ6IHNlbGVjdGVkVGVhbSA/IHNlbGVjdGVkVGVhbVsnTVBNX1RlYW1zLWlkJ10uSWQgOiAnJyxcclxuICAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgICBpc1VwbG9hZFRhc2s6IHRydWVcclxuICAgICAgfTtcclxuXHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0VXNlckJ5Um9sZVJlcXVlc3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcnMgJiYgcmVzcG9uc2UudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICBjb25zdCB1c2VyTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICd1c2VyJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgIGlmICh1c2VyTGlzdCAmJiB1c2VyTGlzdC5sZW5ndGggJiYgdXNlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgY246IHVzZXIuY25cclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy51c2VycyA9IHVzZXJzO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy51c2VycyA9IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYWxsb2NhdGlvblRhc2sodXNlcikge1xyXG4gICAgY29uc3QgY2FwYWNpdHkgPSA4O1xyXG4gICAgY29uc3QgdGFza0FsbG9jYXRpb24gPSAxO1xyXG4gICAgcmV0dXJuIHRhc2tBbGxvY2F0aW9uICsgJy8nICsgY2FwYWNpdHk7XHJcbiAgfVxyXG5cclxuICByZXNvdXJjZVRhc2tBbGxvY2F0aW9uKHVzZXIsIGN1cnJlbnRWaWV3U3RhcnREYXRlcywgY3VycmVudFZpZXdFbmREYXRlcykge1xyXG4gICAgY29uc3QgZmlyc3REYXRlID0gbW9tZW50KGN1cnJlbnRWaWV3U3RhcnREYXRlcyk7XHJcbiAgICBjb25zdCBzZWNvbmREYXRlID0gbW9tZW50KGN1cnJlbnRWaWV3RW5kRGF0ZXMpO1xyXG4gICAgY29uc3QgZGlmZkluRGF5cyA9IE1hdGguYWJzKGZpcnN0RGF0ZS5kaWZmKHNlY29uZERhdGUsICdkYXlzJykpO1xyXG4gICAgY29uc3QgYWxsb2NhdGlvbiA9IFtdO1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPD0gZGlmZkluRGF5czsgaSsrKSB7XHJcbiAgICAgIGFsbG9jYXRpb25baV0gPSB0aGlzLmFsbG9jYXRpb25UYXNrKHVzZXIpOy8vICcwLzgnO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFsbG9jYXRpb247XHJcbiAgfVxyXG5cclxuICAvKiBnZXREZWxpdmVyYWJsZSh1c2VyLCByZW1vdmluZ0ZpbHRlclZhbHVlKSB7XHJcbiAgICBjb25zb2xlLmxvZyh1c2VyKTtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICB0aGlzLnVzZXJJZCA9IFtdO1xyXG4gICAgaWYgKHJlbW92aW5nRmlsdGVyVmFsdWUpIHtcclxuICAgICAgbGV0IHVzZXJpZCwgaXRlbWlkO1xyXG4gICAgICB0aGlzLmFsbFVzZXJzLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgdXNlcmlkID0gdXNlciAmJiB1c2VyWzBdICYmIHVzZXJbMF1bJ0lkZW50aXR5LWlkJ10gIT09IHVuZGVmaW5lZCAmJiB1c2VyWzBdWydJZGVudGl0eS1pZCddLklkID8gdXNlclswXVsnSWRlbnRpdHktaWQnXS5JZCA6IHVzZXIgJiYgdXNlclswXSAmJiB1c2VyWzBdLmNuID8gdXNlclswXS5jbiA6ICcnO1xyXG4gICAgICAgIGl0ZW1pZCA9IGl0ZW0gJiYgaXRlbVsnSWRlbnRpdHktaWQnXSAhPT0gdW5kZWZpbmVkICYmIGl0ZW1bJ0lkZW50aXR5LWlkJ10uSWQgPyBpdGVtWydJZGVudGl0eS1pZCddLklkIDogaXRlbSAmJiBpdGVtLmNuID8gaXRlbS5jbiA6ICcnO1xyXG4gICAgICAgIGlmIChpdGVtaWQgPT09IHVzZXJpZCkge1xyXG4gICAgICAgICAgdGhpcy5hbGxVc2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnVzZXJJZC5mb3JFYWNoKChpZCwgaWR4KSA9PiB7XHJcbiAgICAgICAgY29uc3QgaWRzID0gaWQgJiYgaWRbMF0gJiYgaWRbMF1bJ0lkZW50aXR5LWlkJ10gIT09IHVuZGVmaW5lZCAmJiBpZFswXVsnSWRlbnRpdHktaWQnXS5JZCA/IGlkWzBdWydJZGVudGl0eS1pZCddLklkIDogaWQgJiYgaWRbMF0gJiYgaWRbMF0uY24gPyBpZFswXS5jbiA6ICcnXHJcbiAgICAgICAgaWYgKGlkcyA9PT0gdXNlcmlkKSB7XHJcbiAgICAgICAgICB0aGlzLnVzZXJJZC5zcGxpY2UoaWR4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hbGxVc2Vycy5wdXNoKHVzZXIpO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSXRlbXMucHVzaCh1c2VyKTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEZpbHRlcnMuZW1pdCh0aGlzLnNlbGVjdGVkSXRlbXMpO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVXNlci5lbWl0KHVzZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgdGhpcy5hbGxVc2Vycy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICBpZiAoZWxlbWVudC5jbikge1xyXG4gICAgICAgIHRoaXMudXNlcklkLnB1c2godGhpcy5hbGxVc2VyLmZpbHRlcih1c2VycyA9PiB1c2Vycy5Vc2VySWQgPT09IGVsZW1lbnQuY24pKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgIFxyXG4gICAgbGV0IFVzZXJJZCA9IFtdO1xyXG4gICAgaWYgKHRoaXMudXNlcklkLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy51c2VySWQuZm9yRWFjaChpZCA9PiB7XHJcbiAgICAgICAgaWYgKFVzZXJJZC5pbmRleE9mKGlkWzBdWydJZGVudGl0eS1pZCddLklkKSA9PT0gLTEpIHtcclxuICAgICAgICAgIFVzZXJJZC5wdXNoKGlkWzBdWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICB9KVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hbGxVc2Vycy5mb3JFYWNoKHVzZXJzID0+IHtcclxuICAgICAgICBVc2VySWQucHVzaCh1c2Vyc1snSWRlbnRpdHktaWQnXS5JZCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0IGRhdGVzID0gdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicgKyAnIGFuZCAnICsgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdFbmREYXRlcykuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onO1xyXG5cclxuICAgIGNvbnN0IGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgdGhpcy5sZXZlbHMsIGRhdGVzLCBVc2VySWQpOy8vIHVzZXJbJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgY29uc3Qgb3RtbU1QTURhdGFUeXBlcyA9IE9UTU1NUE1EYXRhVHlwZXMuREVMSVZFUkFCTEU7XHJcblxyXG4gICAgY29uc3QgY29uZGl0aW9uID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuY3JlYXRlQ29uZGl0aW9uT2JqZWN0KCdDT05URU5UX1RZUEUnLCBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsIE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsIE1QTVNlYXJjaE9wZXJhdG9ycy5BTkQsIG90bW1NUE1EYXRhVHlwZXMpO1xyXG4gICAgY29uZGl0aW9uID8gY29uZGl0aW9uT2JqZWN0LkRFTElWRVJBQkxFX0NPTkRUSU9OLnB1c2goY29uZGl0aW9uKSA6IGNvbnNvbGUubG9nKCcnKTtcclxuXHJcblxyXG5cclxuXHJcbiAgICBpZiAodGhpcy52aWV3Q29uZmlncykge1xyXG4gICAgICB2aWV3Q29uZmlnID0gdGhpcy52aWV3Q29uZmlncztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuVEFTSztcclxuICAgIH1cclxuICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICBjb25zdCBwYXJhbWV0ZXJzOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBjb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT05cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgIHNvcnQ6IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICBwYWdlX3NpemU6IDEwMFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgIGlmIChVc2VySWQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxyXG4gICAgICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVycykuc3Vic2NyaWJlKChkYXRhOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5yZXNvdXJjZSA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLk93bmVyQXJyYXkgPSBbXTtcclxuICAgICAgICAgICAgaWYgKGRhdGEuZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgZEZvcm1hdCA9ICdZWVlZLU1NLUREJztcclxuICAgICAgICAgICAgICBkYXRhLmRhdGEuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBzdHlsZXMgPSB7fTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50U3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIoaXRlbS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFLCBkRm9ybWF0KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RW5kRGF0ZSA9IHRoaXMubW9tZW50UHRyKGl0ZW0uREVMSVZFUkFCTEVfRFVFX0RBVEUsIGRGb3JtYXQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGlmZiA9IChldmVudEVuZERhdGUuZGlmZihldmVudFN0YXJ0RGF0ZSkpIC8gKDEwMDAgKiA2MCAqIDYwICogMjQpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXh0cmFQaXhlbCA9ICgoZGlmZiArIDEpICUgMikgPT09IDAgPyAwIDogMjtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gKChkaWZmICsgMSkgKiAodGhpcy52aWV3VHlwZUNlbGxTaXplICsgZXh0cmFQaXhlbCkpOyAvLyAoKGRpZmYgKyAxKSAqICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgLSAxKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IE93bmVyTmFtZSA9IGl0ZW0uREVMSVZFUkFCTEVfQUNUSVZFX1VTRVJfTkFNRTtcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5Pd25lckFycmF5Lmhhc093blByb3BlcnR5KE93bmVyTmFtZSkpIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV0gPSBbXTtcclxuICAgICAgICAgICAgICAgICAgLy8gIHRoaXMucmVzb3VyY2VbJ2FsbG9jYXRpb24nXSA9IFswIC8gOF07XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMucmVzb3VyY2VUYXNrQWxsb2NhdGlvbihPd25lck5hbWUsIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzLCB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZXMpO1xyXG4gICAgICAgICAgICAgICAgICAvLyAgIHRoaXMuT3duZXJBcnJheSA9IHsgW093bmVyTmFtZV06IFtdIH07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgdG9wID0gMzAgKiB0aGlzLk93bmVyQXJyYXlbT3duZXJOYW1lXS5sZW5ndGg7XHJcbiAgICAgICAgICAgICAgICBzdHlsZXNbJ3dpZHRoJ10gPSB3aWR0aCArICdweCc7XHJcbiAgICAgICAgICAgICAgICBzdHlsZXNbJ3RvcCddID0gdG9wICsgJ3B4JztcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZURldGFpbCA9IHtcclxuICAgICAgICAgICAgICAgICAgZGF0YTogaXRlbSxcclxuICAgICAgICAgICAgICAgICAgc3R5bGU6IHN0eWxlc1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIGxldCBrOiBhbnksIHY6IGFueTtcclxuICAgICAgICAgICAgICAgIGZvciAoW2ssIHZdIG9mIE9iamVjdC5lbnRyaWVzKHRoaXMuT3duZXJBcnJheSkpIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHYubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0ZSA9IHRoaXMuZGF0ZWUgXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV1bJ2RhdGUnXSA9IGRhdGU7Ly8gdGhpcy5kYXRlc3M7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdLnB1c2goeyBkZWxpdmVyYWJsZURldGFpbCB9KTtcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5Pd25lckFycmF5KTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyAgdGhpcy5yZXNvdXJjZS5wdXNoKHRoaXMuT3duZXJBcnJheSk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnJlc291cmNlKTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAvLyAgdGhpcy5yZXNvdXJjZS5oZWlnaHQgPSAzNSAqIGRhdGEuZGF0YS5sZW5ndGggKyAncHgnO1xyXG4gICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheS5oZWlnaHQgPSAzNSAqIGRhdGEuZGF0YS5sZW5ndGggKyAncHgnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZGVsaXZlcmFibGVEYXRhLmVtaXQodGhpcy5yZXNvdXJjZSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVEYXRhLmVtaXQodGhpcy5Pd25lckFycmF5KTtcclxuICAgICAgICAgICAgdGhpcy5yZXNvdXJjZUFsbG9jYXRpb24uZW1pdCh0aGlzLnJlc291cmNlKTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLk93bmVyQXJyYXkgPSBbXTtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRGF0YS5lbWl0KHRoaXMuT3duZXJBcnJheSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgfSAqL1xyXG5cclxuICBnZXRQcm9wZXJ0eShkZWxpdmVyYWJsZURhdGEsIG1hcHBlck5hbWUpIHtcclxuICAgIC8vIGNvbnN0IGxldmVsID0gdGhpcy5pc1Jldmlld2VyID8gTVBNX0xFVkVMUy5ERUxJVkVSQUJMRV9SRVZJRVcgOiBNUE1fTEVWRUxTLkRFTElWRVJBQkxFO1xyXG4gICAgY29uc3QgZGlzcGxheUNvbHVtbiA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIG1hcHBlck5hbWUsIE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpO1xyXG4gICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtbiwgZGVsaXZlcmFibGVEYXRhKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAgICogQHNpbmNlIE1QTVYzLTk0NlxyXG4gICAgICogQHBhcmFtIGRlbGl2ZXJhYmxlVHlwZVxyXG4gICAgICovXHJcbiAgZ2V0RGVsaXZlcmFibGVUeXBlSWNvbihkZWxpdmVyYWJsZVR5cGUpIHtcclxuICAgIHJldHVybiB0aGlzLmRlbGl2ZXJhYmxlU2VydmljZS5nZXREZWxpdmVyYWJsZVR5cGVJY29uKGRlbGl2ZXJhYmxlVHlwZSk7XHJcbiAgfVxyXG5cclxuICByZWxvYWQodXNlcikge1xyXG4gICAgY29uc29sZS5sb2coXCJyZWxvYWRcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkSXRlbXMpXHJcbiAgICBjb25zb2xlLmxvZyh1c2VyKTtcclxuICAgIC8vIHRoaXMuZ2V0RGVsaXZlcmFibGUodXNlciwgZmFsc2UpXHJcbiAgfVxyXG5cclxuICBnZXREZWxpdmVyYWJsZUxpc3QoVXNlcklkKSB7XHJcbiAgICBsZXQgdmlld0NvbmZpZztcclxuICAgIGNvbnN0IGRhdGVzID0gdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicgKyAnIGFuZCAnICsgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdFbmREYXRlcykuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onO1xyXG4gICAgLy9NUE0tMjIyNFxyXG4gICAgLyogIGNvbnN0IGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgTVBNX0xFVkVMUy5UQVNLLCBkYXRlcywgVXNlcklkKTsgLy8gdGhpcy5sZXZlbHMvLyB1c2VyWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgKi9cclxuICAgIGNvbnN0IGNvbmRpdGlvbk9iamVjdDogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgTVBNX0xFVkVMUy5UQVNLLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMpLmZvcm1hdCgnWVlZWS1NTS1ERFQwMDowMDowMC4wMDAnKSArICdaJywgdGhpcy5tb21lbnRQdHIudXRjKHRoaXMuY3VycmVudFZpZXdFbmREYXRlcykuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLCBVc2VySWQsIHRydWUpOyAvLyB0aGlzLmxldmVscy8vIHVzZXJbJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgdGhpcy5zdG9yZVNlYXJjaGNvbmRpdGlvbk9iamVjdCA9IE9iamVjdC5hc3NpZ24oe30sIGNvbmRpdGlvbk9iamVjdCk7XHJcbiAgICBjb25zdCBvdG1tTVBNRGF0YVR5cGVzID0gT1RNTU1QTURhdGFUeXBlcy5ERUxJVkVSQUJMRTtcclxuXHJcbi8vTVBNLTIyMjRcclxuICAgIC8qIGNvbnN0IGNvbmRpdGlvbk9iamVjdHM6IFRhc2tTZWFyY2hDb25kaXRpb25zID0gdGhpcy5maWx0ZXJIZWxwZXJTZXJ2aWNlLmdldFNlYXJjaENvbmRpdGlvbk5ldyh0aGlzLnNlbGVjdGVkTGlzdEZpbHRlciwgdGhpcy5zZWxlY3RlZExpc3REZXBlbmRlbnRGaWx0ZXIsIHRoaXMubGV2ZWxzLCBkYXRlcywgVXNlcklkKTsgLy8gdGhpcy5sZXZlbHMvLyB1c2VyWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgICovY29uc3QgY29uZGl0aW9uT2JqZWN0czogVGFza1NlYXJjaENvbmRpdGlvbnMgPSB0aGlzLmZpbHRlckhlbHBlclNlcnZpY2UuZ2V0U2VhcmNoQ29uZGl0aW9uTmV3KHRoaXMuc2VsZWN0ZWRMaXN0RmlsdGVyLCB0aGlzLnNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlciwgdGhpcy5sZXZlbHMsIHRoaXMubW9tZW50UHRyLnV0Yyh0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcykuZm9ybWF0KCdZWVlZLU1NLUREVDAwOjAwOjAwLjAwMCcpICsgJ1onLCB0aGlzLm1vbWVudFB0ci51dGModGhpcy5jdXJyZW50Vmlld0VuZERhdGVzKS5mb3JtYXQoJ1lZWVktTU0tRERUMDA6MDA6MDAuMDAwJykgKyAnWicsIFVzZXJJZCwgdHJ1ZSk7IC8vIHRoaXMubGV2ZWxzLy8gdXNlclsnSWRlbnRpdHktaWQnXS5JZCk7XHJcblxyXG4gICAgLy8gIHRoaXMuc3RvcmVTZWFyY2hjb25kaXRpb25PYmplY3QgPSBPYmplY3QuYXNzaWduKHt9LCBjb25kaXRpb25PYmplY3QpO1xyXG4gICAgY29uc3Qgb3RtbU1QTURhdGFUeXBlc3MgPSBPVE1NTVBNRGF0YVR5cGVzLkRFTElWRVJBQkxFO1xyXG5cclxuICAgIGNvbnN0IGNvbmRpdGlvbnMgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5jcmVhdGVDb25kaXRpb25PYmplY3QoJ0NPTlRFTlRfVFlQRScsIE1QTVNlYXJjaE9wZXJhdG9ycy5JUywgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUywgTVBNU2VhcmNoT3BlcmF0b3JzLkFORCwgb3RtbU1QTURhdGFUeXBlcyk7XHJcbiAgICBjb25kaXRpb25zID8gY29uZGl0aW9uT2JqZWN0cy5ERUxJVkVSQUJMRV9DT05EVElPTi5wdXNoKGNvbmRpdGlvbnMpIDogY29uc29sZS5sb2coJycpO1xyXG5cclxuICAgIGlmICh0aGlzLnNlYXJjaENvbmRpdGlvbnMgJiYgdGhpcy5zZWFyY2hDb25kaXRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5zZWFyY2hDb25kaXRpb25zWzBdLm1ldGFkYXRhX2ZpZWxkX2lkID09PSB0aGlzLnNlYXJjaERhdGFTZXJ2aWNlLktFWVdPUkRfTUFQUEVSKSB7XHJcbiAgICAgIC8va2V5V29yZCA9IHRoaXMuc2VhcmNoQ29uZGl0aW9uc1swXS5rZXl3b3JkO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLnNlYXJjaENvbmRpdGlvbnMgJiYgdGhpcy5zZWFyY2hDb25kaXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgY29uZGl0aW9uT2JqZWN0LlRBU0tfQ09ORElUSU9OID0gY29uZGl0aW9uT2JqZWN0LlRBU0tfQ09ORElUSU9OLmNvbmNhdCh0aGlzLnNlYXJjaENvbmRpdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZW1haWxTZWFyY2hDb25kaXRpb24gPSB0aGlzLmVtYWlsU2VhcmNoQ29uZGl0aW9uICYmIEFycmF5LmlzQXJyYXkodGhpcy5lbWFpbFNlYXJjaENvbmRpdGlvbikgPyB0aGlzLmVtYWlsU2VhcmNoQ29uZGl0aW9uIDogW107XHJcblxyXG4gICAgaWYgKHRoaXMudmlld0NvbmZpZ3MpIHtcclxuICAgICAgdmlld0NvbmZpZyA9IHRoaXMudmlld0NvbmZpZ3M7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlRBU0s7XHJcbiAgICB9XHJcbiAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld0NvbmZpZykuc3Vic2NyaWJlKCh2aWV3RGV0YWlsczogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgJiYgdHlwZW9mIHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyA9PT0gJ3N0cmluZydcclxuICAgICAgICA/IHBhcnNlSW50KHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApIDogbnVsbDtcclxuICAgICAgY29uc3QgcGFyYW1ldGVyczogU2VhcmNoUmVxdWVzdCA9IHtcclxuICAgICAgICBzZWFyY2hfY29uZmlnX2lkOiBzZWFyY2hDb25maWcgPyBzZWFyY2hDb25maWcgOiBudWxsLFxyXG4gICAgICAgIGtleXdvcmQ6ICcnLFxyXG4gICAgICAgIGdyb3VwaW5nOiB7XHJcbiAgICAgICAgICBncm91cF9ieTogJ1RBU0snLFxyXG4gICAgICAgICAgZ3JvdXBfZnJvbV90eXBlOiBvdG1tTVBNRGF0YVR5cGVzLFxyXG4gICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IFsuLi5jb25kaXRpb25PYmplY3QuREVMSVZFUkFCTEVfQ09ORFRJT04sIC4uLnRoaXMuZW1haWxTZWFyY2hDb25kaXRpb25dXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IGNvbmRpdGlvbk9iamVjdC5UQVNLX0NPTkRJVElPTlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmFjZXRfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgc29ydDogW11cclxuICAgICAgICB9LFxyXG4gICAgICAgIGN1cnNvcjoge1xyXG4gICAgICAgICAgcGFnZV9pbmRleDogMCxcclxuICAgICAgICAgIHBhZ2Vfc2l6ZTogMTAwMFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIGNvbnN0IHBhcmFtZXRlcnNzOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBjb25kaXRpb25PYmplY3RzLkRFTElWRVJBQkxFX0NPTkRUSU9OXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgZmFjZXRfY29uZGl0aW9uOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc29ydGluZ19saXN0OiB7XHJcbiAgICAgICAgICBzb3J0OiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICBwYWdlX2luZGV4OiAwLFxyXG4gICAgICAgICAgcGFnZV9zaXplOiAxMDBcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICBpZiAoVXNlcklkLmxlbmd0aCA+IDApIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcclxuICAgICAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgoZGF0YTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVyc3MpLnN1YnNjcmliZSgoZGVsaXZlcmFibGVEYXRhOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRlbGl2ZXJhYmxlRGF0YSk7XHJcbiAgICAgICAgICAgICAgdGhpcy5yZXNvdXJjZSA9IFtdO1xyXG4gICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheSA9IFtdO1xyXG4gICAgICAgICAgICAgIGlmIChkYXRhLmRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZEZvcm1hdCA9ICdZWVlZLU1NLUREJztcclxuICAgICAgICAgICAgICAgIGRhdGEuZGF0YS5mb3JFYWNoKChpdGVtLCBpdGVtSW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgLy9jaGVja2luZyAtZGVsaXZlcmFibGVEYXRhIHRvIGRlbGl2ZXJhYmxlXHJcbiAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlRGF0YS5kYXRhLmZvckVhY2goKGRlbGl2ZXJhYmxlLCBpdGVtSW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS5JRCA9PT0gZGVsaXZlcmFibGUuVEFTS19JVEVNX0lELnNwbGl0KCcuJylbMV0pIHsvL2RlbGl2ZXJhYmxlRGF0YS5kYXRhW2l0ZW1JbmRleF0uVEFTS19JVEVNX0lELnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgIGxldCBzdHlsZXMgPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICAgIC8qICAgY29uc3QgZXZlbnRTdGFydERhdGUgPSB0aGlzLm1vbWVudFB0cihkZWxpdmVyYWJsZURhdGEuZGF0YVtpdGVtSW5kZXhdLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUsIGRGb3JtYXQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudEVuZERhdGUgPSB0aGlzLm1vbWVudFB0cihkZWxpdmVyYWJsZURhdGEuZGF0YVtpdGVtSW5kZXhdLkRFTElWRVJBQkxFX0RVRV9EQVRFLCBkRm9ybWF0KTsgKi9cclxuICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50U3RhcnREYXRlID0gdGhpcy5tb21lbnRQdHIoZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfU1RBUlRfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudEVuZERhdGUgPSB0aGlzLm1vbWVudFB0cihkZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9EVUVfREFURSwgZEZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkaWZmID0gKGV2ZW50RW5kRGF0ZS5kaWZmKGV2ZW50U3RhcnREYXRlKSkgLyAoMTAwMCAqIDYwICogNjAgKiAyNCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCBleHRyYVBpeGVsID0gKChkaWZmICsgMSkgJSAyKSA9PT0gMCA/IDAgOiAxOy8vIDAgOiAyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgLyogICBjb25zdCBleHRyYVBpeGVsID0gKChkaWZmICsgMSkgJSAyKSA9PT0gMCA/IDAgOiAyOyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgY29uc3Qgd2lkdGggPSAoKGRpZmYgKyAxKSAqICh0aGlzLnZpZXdUeXBlQ2VsbFNpemUgKyBleHRyYVBpeGVsKSk7IC8vICgoZGlmZiArIDEpICogKHRoaXMudmlld1R5cGVDZWxsU2l6ZSAtIDEpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAvKiBsZXQgT3duZXJOYW1lID0gZGVsaXZlcmFibGVEYXRhLmRhdGFbaXRlbUluZGV4XS5ERUxJVkVSQUJMRV9BQ1RJVkVfVVNFUl9OQU1FOyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgbGV0IE93bmVyTmFtZSA9IGRlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX0FDVElWRV9VU0VSX05BTUU7XHJcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuT3duZXJBcnJheS5oYXNPd25Qcm9wZXJ0eShPd25lck5hbWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVzb3VyY2VUYXNrQWxsb2NhdGlvbihPd25lck5hbWUsIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGVzLCB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICBsZXQgdG9wID0gNDQgKiB0aGlzLk93bmVyQXJyYXlbT3duZXJOYW1lXS5sZW5ndGg7Ly8gMzBcclxuICAgICAgICAgICAgICAgICAgICAgIC8vICAgIHN0eWxlc1snd2lkdGgnXSA9IChkaWZmICE9PSAxID8gKCgoZGlmZiArIDEpICUgMikgPT09IDApID8gKCh3aWR0aCkgKyAncHgnKSA6ICh3aWR0aCArIDQpICsgJ3B4JyA6ICh3aWR0aCkgKyAncHgnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZXNbJ3RvcCddID0gdG9wICsgJ3B4JztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZURldGFpbCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogZGVsaXZlcmFibGUsIC8vIGRlbGl2ZXJhYmxlRGF0YS5kYXRhW2l0ZW1JbmRleF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICh0aGlzLmxldmVscy5pbmNsdWRlcygnVEFTSycpKSA/IGl0ZW0uVEFTS19OQU1FIDogaXRlbS5QUk9KRUNUX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qIGljb246IHRoaXMuZ2V0RGVsaXZlcmFibGVUeXBlSWNvbih0aGlzLmdldFByb3BlcnR5KGRlbGl2ZXJhYmxlRGF0YS5kYXRhW2l0ZW1JbmRleF0sICdERUxJVkVSQUJMRV9UWVBFJykpLklDT04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiB0aGlzLmdldERlbGl2ZXJhYmxlVHlwZUljb24odGhpcy5nZXRQcm9wZXJ0eShkZWxpdmVyYWJsZSwgJ0RFTElWRVJBQkxFX1RZUEUnKSkuSUNPTixcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlOiBzdHlsZXNcclxuICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICBsZXQgazogYW55LCB2OiBhbnk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBmb3IgKFtrLCB2XSBvZiBPYmplY3QuZW50cmllcyh0aGlzLk93bmVyQXJyYXkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2Lmxlbmd0aCA9PT0gMCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkYXRlID0gJC5leHRlbmQodHJ1ZSwgW10sIHRoaXMuY29udmVydGVkRGF0ZSkvLyB0aGlzLmNvbnZlcnRlZERhdGU7Ly9KU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHRoaXMuZGF0ZWUpKS8vJC5leHRlbmQodHJ1ZSxbXSx0aGlzLmRhdGVlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdWydkYXRlJ10gPSBkYXRlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheVtPd25lck5hbWVdWydhbGxvY2F0aW9uJ10gPSB0aGlzLnJlc291cmNlVGFza0FsbG9jYXRpb24oT3duZXJOYW1lLCB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcywgdGhpcy5jdXJyZW50Vmlld0VuZERhdGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5Pd25lckFycmF5W093bmVyTmFtZV0ucHVzaCh7IGRlbGl2ZXJhYmxlRGV0YWlsIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuT3duZXJBcnJheS5oZWlnaHQgPSA1MCAqIGRhdGEuZGF0YS5sZW5ndGggKyAncHgnOyAvLyAzNSAvLzUwXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZURhdGEuZW1pdCh0aGlzLk93bmVyQXJyYXkpO1xyXG4gICAgICAgICAgICAgIC8vICAgdGhpcy5yZXNvdXJjZUFsbG9jYXRpb24uZW1pdCh0aGlzLnJlc291cmNlKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuT3duZXJBcnJheSA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVEYXRhLmVtaXQodGhpcy5Pd25lckFycmF5KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvLyBzZWFyY2ggYnkgdGFzay1ncm91cGluZyBhbmQgbWFwIHRoZSBuYW1lIHRvIGRlbGl2ZXJhYmxlIHNlYXJjaFxyXG4gIGdldERlbGl2ZXJhYmxlKHVzZXIsIHJlbW92aW5nRmlsdGVyVmFsdWUsIGlzUGFyZW50Pykge1xyXG4gICAgY29uc29sZS5sb2codXNlcik7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG5cclxuICAgIGxldCBVc2VySWQgPSBbXTtcclxuICAgIGlmIChpc1BhcmVudCkge1xyXG4gICAgICBjb25zdCB1c2VyaWQgPSB0aGlzLmFsbFVzZXIuZmluZCh1c2VycyA9PiB1c2Vycy5Vc2VySWQgPT09IHVzZXIpO1xyXG4gICAgICAvLyBpZiAodGhpcy5wcmV2aW91c1VzZXJJZC5pbmNsdWRlcyh1c2VyaWRbJ0lkZW50aXR5LWlkJ10uSWQpKSB7XHJcbiAgICAgIGlmICgodXNlcmlkICYmIHVzZXJpZFsnSWRlbnRpdHktaWQnXSAmJiB1c2VyaWRbJ0lkZW50aXR5LWlkJ10uSWQgIT09IHVuZGVmaW5lZCAmJiB0aGlzLnByZXZpb3VzVXNlcklkLmluY2x1ZGVzKHVzZXJpZFsnSWRlbnRpdHktaWQnXS5JZCkpIHx8XHJcbiAgICAgICAgKHVzZXJpZCA9PT0gdW5kZWZpbmVkKSkge1xyXG4gICAgICAgIFVzZXJJZCA9IHRoaXMucHJldmlvdXNVc2VySWQ7XHJcbiAgICAgICAgdGhpcy5nZXREZWxpdmVyYWJsZUxpc3QoVXNlcklkKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnVzZXJJZCA9IFtdO1xyXG4gICAgICBpZiAocmVtb3ZpbmdGaWx0ZXJWYWx1ZSkge1xyXG4gICAgICAgIGxldCB1c2VyaWQsIGl0ZW1pZDtcclxuICAgICAgICB0aGlzLmFsbFVzZXJzLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICB1c2VyaWQgPSB1c2VyICYmIHVzZXJbMF0gJiYgdXNlclswXVsnSWRlbnRpdHktaWQnXSAhPT0gdW5kZWZpbmVkICYmIHVzZXJbMF1bJ0lkZW50aXR5LWlkJ10uSWQgPyB1c2VyWzBdWydJZGVudGl0eS1pZCddLklkIDogdXNlciAmJiB1c2VyWzBdICYmIHVzZXJbMF0uY24gPyB1c2VyWzBdLmNuIDogJyc7XHJcbiAgICAgICAgICBpdGVtaWQgPSBpdGVtICYmIGl0ZW1bJ0lkZW50aXR5LWlkJ10gIT09IHVuZGVmaW5lZCAmJiBpdGVtWydJZGVudGl0eS1pZCddLklkID8gaXRlbVsnSWRlbnRpdHktaWQnXS5JZCA6IGl0ZW0gJiYgaXRlbS5jbiA/IGl0ZW0uY24gOiAnJztcclxuICAgICAgICAgIGlmIChpdGVtaWQgPT09IHVzZXJpZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFsbFVzZXJzLnNwbGljZShpbmRleCwgMSk7XHJcblxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudXNlcklkLmZvckVhY2goKGlkLCBpZHgpID0+IHtcclxuICAgICAgICAgIGNvbnN0IGlkcyA9IGlkICYmIGlkWzBdICYmIGlkWzBdWydJZGVudGl0eS1pZCddICE9PSB1bmRlZmluZWQgJiYgaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQgPyBpZFswXVsnSWRlbnRpdHktaWQnXS5JZCA6IGlkICYmIGlkWzBdICYmIGlkWzBdLmNuID8gaWRbMF0uY24gOiAnJ1xyXG4gICAgICAgICAgaWYgKGlkcyA9PT0gdXNlcmlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcklkLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYWxsVXNlcnMucHVzaCh1c2VyKTtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZEl0ZW1zLmluZGV4T2YodXNlcikgIT09IC0xKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIlZhbHVlIGV4aXN0cyFcIilcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zLnB1c2godXNlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8qIHRoaXMuc2VsZWN0ZWRJdGVtcy5wdXNoKHVzZXIpOyAqL1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRGaWx0ZXJzLmVtaXQodGhpcy5zZWxlY3RlZEl0ZW1zKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVXNlci5lbWl0KHVzZXIpO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgdGhpcy5hbGxVc2Vycy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgIGlmIChlbGVtZW50LmNuKSB7XHJcbiAgICAgICAgICB0aGlzLnVzZXJJZC5wdXNoKHRoaXMuYWxsVXNlci5maWx0ZXIodXNlcnMgPT4gdXNlcnMuVXNlcklkID09PSBlbGVtZW50LmNuKSk7XHJcbiAgICAgICAgICAvLyAgIHRoaXMudXNlcklkcy5wdXNoKHRoaXMudXNlcklkKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLyogbGV0IFVzZXJJZCA9IFtdOyAqL1xyXG4gICAgICBpZiAodGhpcy51c2VySWQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMudXNlcklkLmZvckVhY2goaWQgPT4ge1xyXG4gICAgICAgICAgaWYgKFVzZXJJZC5pbmRleE9mKGlkWzBdWydJZGVudGl0eS1pZCddLklkKSA9PT0gLTEpIHtcclxuICAgICAgICAgICAgVXNlcklkLnB1c2goaWRbMF1bJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYWxsVXNlcnMuZm9yRWFjaCh1c2VycyA9PiB7XHJcbiAgICAgICAgICBVc2VySWQucHVzaCh1c2Vyc1snSWRlbnRpdHktaWQnXS5JZCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMucHJldmlvdXNVc2VySWQgPSBVc2VySWQ7XHJcblxyXG4gICAgICB0aGlzLmdldERlbGl2ZXJhYmxlTGlzdChVc2VySWQpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhjaGFuZ2VzKTtcclxuICAgIGNvbnN0IGN1cnJlbnRTdGFydERhdGUgPSB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcyAmJiB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcy5faSAhPT0gdW5kZWZpbmVkID8gdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZXMuX2kgOiB0aGlzLmN1cnJlbnRWaWV3U3RhcnREYXRlcztcclxuICAgIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuZGF0ZWUgJiYgY2hhbmdlcy5kYXRlZS5jdXJyZW50VmFsdWUgJiYgY2hhbmdlcy5kYXRlZS5jdXJyZW50VmFsdWVbMF0gJiYgY2hhbmdlcy5kYXRlZS5jdXJyZW50VmFsdWVbMF0uX2kgJiZcclxuICAgICAgIWNoYW5nZXMuZGF0ZWUuZmlyc3RDaGFuZ2UpIHtcclxuICAgICAgdGhpcy5jb252ZXJ0ZWREYXRlID0gW107XHJcbiAgICAgIGNoYW5nZXMuZGF0ZWUuY3VycmVudFZhbHVlLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgY29uc3QgeCA9IGVsZW1lbnQuZm9ybWF0KCdZWVlZLU1NLUREJyk7XHJcbiAgICAgICAgY29uc3QgZGF0ZSA9IHtcclxuICAgICAgICAgIGRhdGU6IHgsXHJcbiAgICAgICAgICBlbGVtZW50OiBlbGVtZW50LmZvcm1hdCgnZGQgREQnKSxcclxuICAgICAgICAgIGlzT3ZlcmxhcDogJ2ZhbHNlJ1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNvbnZlcnRlZERhdGUucHVzaChkYXRlKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuY29udmVydGVkRGF0ZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdldERlbGl2ZXJhYmxlKHRoaXMucmVtb3ZlZEZpbHRlciwgdHJ1ZSk7XHJcbiAgICAvLyBUbyBjbGVhciB0aGUgc2VsZWN0ZWQgdmFsdWVcclxuICAgIGlmICh0aGlzLnJlbW92ZWRGaWx0ZXIgJiYgdGhpcy5yZW1vdmVkRmlsdGVyLmxlbmd0aCA+IDAgJiYgdGhpcy5yZW1vdmVkRmlsdGVyWzBdID09PSAnVEVBTScpIHtcclxuICAgICAgdGhpcy50ZWFtSW5pdGlhbCA9ICcnO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVGVhbSA9ICcnO1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZFJvbGUpIHtcclxuXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5yb2xlSW5pdGlhbCA9ICcnO1xyXG4gICAgICAgIHRoaXMudXNlckluaXRpYWwgPSAnJztcclxuICAgICAgICBpZiAodGhpcy5pbnB1dE5hbWUgJiYgdGhpcy5pbnB1dE5hbWUubmF0aXZlRWxlbWVudCAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlKSB7XHJcbiAgICAgICAgICB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlID0gJ1VzZXJzJztcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gYWRkZWQgYmFjayBhbGwgcm9sZXMgYW5kIHVzZXJzXHJcbiAgICAgICAgdGhpcy5hbGxSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbVJvbGVzKCk7Ly90aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFJvbGVzKCk7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEFsbFVzZXJzKCkuc3Vic2NyaWJlKGFsbFVzZXJzID0+IHtcclxuICAgICAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy5Vc2VyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsVXNlciA9IGFsbFVzZXJzLlVzZXI7XHJcbiAgICAgICAgICAgIGFsbFVzZXJzLlVzZXIuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICBpZiAodXNlciAmJiB1c2VyLkZ1bGxOYW1lICYmIHR5cGVvZiAodXNlci5GdWxsTmFtZSkgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgIH0gZWxzZSBpZiAodGhpcy5yZW1vdmVkRmlsdGVyICYmIHRoaXMucmVtb3ZlZEZpbHRlci5sZW5ndGggPiAwICYmIHRoaXMucmVtb3ZlZEZpbHRlclswXSA9PT0gJ1JPTEUnKSB7XHJcbiAgICAgIHRoaXMucm9sZUluaXRpYWwgPSAnJztcclxuICAgICAgdGhpcy5zZWxlY3RlZFJvbGUgPSAnJztcclxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRUZWFtKSB7XHJcbiAgICAgICAgLyogIGlmICh0aGlzLmlucHV0TmFtZSAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50ICYmIHRoaXMuaW5wdXROYW1lLm5hdGl2ZUVsZW1lbnQudmFsdWUpIHtcclxuICAgICAgICAgICB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlID0gJ1VzZXJzJztcclxuICAgICAgICAgfSAqL1xyXG4gICAgICAgIHRoaXMuZ2V0VXNlcnNGb3JUZWFtKHRoaXMuc2VsZWN0ZWRUZWFtKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMudXNlckluaXRpYWwgPSAnJztcclxuICAgICAgICBpZiAodGhpcy5pbnB1dE5hbWUgJiYgdGhpcy5pbnB1dE5hbWUubmF0aXZlRWxlbWVudCAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlKSB7XHJcbiAgICAgICAgICB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlID0gJ1VzZXJzJztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEFsbFVzZXJzKCkuc3Vic2NyaWJlKGFsbFVzZXJzID0+IHtcclxuICAgICAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy5Vc2VyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsVXNlciA9IGFsbFVzZXJzLlVzZXI7XHJcbiAgICAgICAgICAgIGFsbFVzZXJzLlVzZXIuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICBpZiAodXNlciAmJiB1c2VyLkZ1bGxOYW1lICYmIHR5cGVvZiAodXNlci5GdWxsTmFtZSkgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICAvKiAgdGhpcy5nZXRVc2Vyc0J5Um9sZSh0aGlzLnNlbGVjdGVkUm9sZSwgdGhpcy5zZWxlY3RlZFRlYW0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiBcclxuICAgICAgIH0pOyAqL1xyXG5cclxuICAgIH0gZWxzZSBpZiAodGhpcy5yZW1vdmVkRmlsdGVyICYmIHRoaXMucmVtb3ZlZEZpbHRlci5sZW5ndGggPiAwICYmICh0aGlzLnJlbW92ZWRGaWx0ZXJbMF0ubmFtZSAhPT0gdW5kZWZpbmVkIHx8IHRoaXMucmVtb3ZlZEZpbHRlclswXS5GdWxsTmFtZSAhPT0gdW5kZWZpbmVkKSkge1xyXG4gICAgICBpZiAodGhpcy5pbnB1dE5hbWUgJiYgdGhpcy5pbnB1dE5hbWUubmF0aXZlRWxlbWVudCAmJiB0aGlzLmlucHV0TmFtZS5uYXRpdmVFbGVtZW50LnZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5pbnB1dE5hbWUubmF0aXZlRWxlbWVudC52YWx1ZSA9ICdVc2Vycyc7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy51c2VySW5pdGlhbCA9ICcnO1xyXG4gICAgfVxyXG4gICAgdGhpcy5yZW1vdmVkRmlsdGVyID0gW107XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIC8vIHRoaXMubGV2ZWwgPSB0aGlzLmdyb3VwQnkgJiYgdGhpcy5ncm91cEJ5LnZhbHVlICYmIHRoaXMuZ3JvdXBCeS52YWx1ZS5zcGxpdCgnXycpWzJdID8gdGhpcy5ncm91cEJ5LnZhbHVlLnNwbGl0KCdfJylbMl0gOiAnJztcclxuICAgIHRoaXMuYWxsVGVhbXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFRlYW1zKCk7XHJcbiAgICAvKiB0aGlzLmFsbFRlYW1zLmZvckVhY2godGVhbSA9PiB7XHJcbiAgICAgIHRoaXMudGVhbXMucHVzaCh0ZWFtKTtcclxuICAgIH0pOyAqL1xyXG4gICAgdGhpcy5hbGxSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbVJvbGVzKCk7Ly90aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFJvbGVzKCk7XHJcbiAgICAvKiAgdGhpcy5hbGxSb2xlcy5mb3JFYWNoKHJvbGUgPT4ge1xyXG4gICAgICAgcm9sZS5ST0xFX05BTUUgPSByb2xlLlJPTEVfTkFNRS5yZXBsYWNlKC9fL2csICcgJyk7XHJcbiAgICAgfSk7ICovXHJcbiAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVXNlcnMoKS5zdWJzY3JpYmUoYWxsVXNlcnMgPT4ge1xyXG4gICAgICBpZiAoYWxsVXNlcnMgJiYgYWxsVXNlcnMuVXNlcikge1xyXG4gICAgICAgIHRoaXMuYWxsVXNlciA9IGFsbFVzZXJzLlVzZXI7XHJcbiAgICAgICAgYWxsVXNlcnMuVXNlci5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgaWYgKHVzZXIgJiYgdXNlci5GdWxsTmFtZSAmJiB0eXBlb2YgKHVzZXIuRnVsbE5hbWUpICE9PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJzLnB1c2godXNlcik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5kYXRlZS5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICBjb25zdCB4ID0gZWxlbWVudC5mb3JtYXQoJ1lZWVktTU0tREQnKTtcclxuICAgICAgY29uc3QgZGF0ZSA9IHtcclxuICAgICAgICBkYXRlOiB4LFxyXG4gICAgICAgIGVsZW1lbnQ6IGVsZW1lbnQuZm9ybWF0KCdkZCBERCcpLFxyXG4gICAgICAgIGlzT3ZlcmxhcDogJ2ZhbHNlJ1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuY29udmVydGVkRGF0ZS5wdXNoKGRhdGUpO1xyXG4gICAgfSk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNvbnZlcnRlZERhdGUpO1xyXG4gICAgLy8gdG8gY2hhbmdlIHNlYXJjaFxyXG4gICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2goXHJcbiAgICAgIHRoaXMuc2VhcmNoRGF0YVNlcnZpY2Uuc2VhcmNoRGF0YS5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hDb25kaXRpb25zID0gKGRhdGEgJiYgQXJyYXkuaXNBcnJheShkYXRhKSA/IHRoaXMub3RtbVNlcnZpY2UuYWRkUmVsYXRpb25hbE9wZXJhdG9yKGRhdGEpIDogW10pO1xyXG5cclxuICAgICAgfSlcclxuICAgICk7XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaChzdWJzY3JpcHRpb24gPT4gc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCkpO1xyXG4gIH1cclxufVxyXG4iXX0=