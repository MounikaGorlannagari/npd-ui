import { __decorate } from "tslib";
import { Component, OnInit, ElementRef, ViewChild, ViewChildren, QueryList, Renderer2, Input, Output, EventEmitter, SimpleChanges, ChangeDetectorRef, HostListener } from '@angular/core';
import { extendMoment } from 'moment-range';
import * as Moment from 'moment';
import { CalendarConstants } from '../shared/constants/Calendar.constants';
import { MediaMatcher } from '@angular/cdk/layout';
const moment = extendMoment(Moment);
let ResourceManagementResourceAllocationComponent = class ResourceManagementResourceAllocationComponent {
    constructor(renderer, media, changeDetectorRef) {
        this.renderer = renderer;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.removed = new EventEmitter();
        this.scrolled = false;
        this.dates = [];
        this.viewTypeCellSize = 150; //35;
        this.momentPtr = moment;
        this.sidebarStyle = {};
        this.Object = Object;
        this.Math = Math;
        /** * Scroll for Fixing the dates and header */
        this.scrolling = (el) => {
            const scrollTop = el.srcElement.scrollTop;
            if (scrollTop >= 100) {
                this.scrolled = true;
            }
            else {
                this.scrolled = false;
            }
        };
        //191
        this.getHeightForScreen();
    }
    onResize(event) {
        this.getHeightForScreen();
    }
    //191
    getHeightForScreen() {
        const width = 1536;
        const height = 26;
        const max_height = 39;
        this.screenWidth = window.innerWidth;
        const diff = Math.floor((this.screenWidth - width) / 171);
        this.height = height + (diff * 2) <= max_height ? height + (diff * 2) : 38;
    }
    // to remove the filter
    removeFilter(filter) {
        if (filter.ROLE_NAME || filter.NAME) {
            this.selectedItems.forEach((item, index) => {
                if ((item.ROLE_NAME !== undefined && filter.ROLE_NAME !== undefined && item.ROLE_NAME === filter.ROLE_NAME) || (item.NAME !== undefined && filter.NAME !== undefined && item.NAME === filter.NAME)) {
                    this.selectedItems.splice(index, 1);
                    this.removed.next(filter.NAME !== undefined && filter['MPM_Teams-id'] !== undefined && filter['MPM_Teams-id'].Id !== undefined ? ['TEAM'] : ['ROLE']);
                    // this.removed.next(filter.ROLE_NAME !== undefined ? ['ROLE'] : ['TEAM']);
                }
            });
        }
        else {
            this.selectedItems.forEach((item, index) => {
                if ((item.FullName !== undefined && filter.FullName !== undefined && item.FullName === filter.FullName) || (item.name !== undefined && filter.name !== undefined && item.name === filter.name)) {
                    this.selectedItems.splice(index, 1);
                }
            });
        }
        if (filter.FullName || filter.name) {
            console.log("###");
            console.log(filter);
            this.removed.next([filter]);
        }
    }
    /** CALENDAR METHODS **/
    // build calendar based on given start  and end date
    buildCalendar(startDate, endDate) {
        this.currentViewStartDate = startDate;
        this.currentViewEndDate = endDate;
        const start = startDate;
        const end = endDate;
        const range = this.momentPtr.range(start, end);
        const days = Array.from(range.by('days'));
        this.dates = days;
    }
    getNoOfDays(selectedMonth) {
        let noOfDays;
        if (CalendarConstants.evenMonth.includes(selectedMonth)) {
            noOfDays = 31;
        }
        else if (CalendarConstants.oddMonth.includes(selectedMonth)) {
            noOfDays = 30;
        }
        else if (CalendarConstants.leapMonth.includes(selectedMonth)) {
            /* const date = new Date(this.year, 1, 29);
            return date.getMonth() === 1; */
            noOfDays = 28;
        }
        return noOfDays;
    }
    getDateDifference(startDate, endDate) {
        let sd = (new Date(startDate));
        let ed = new Date(endDate);
        return (Math.floor(ed - sd) / (1000 * 60 * 60 * 24)) + 1;
    }
    ngOnChanges(changes) {
        if (changes && changes.selectedMonth && !changes.selectedMonth.firstChange) {
            for (let i = 0; i < CalendarConstants.months.length; i++) {
                if (CalendarConstants.months[i] === changes.selectedMonth.currentValue) {
                    this.buildCalendar(i + 1 + '/01/' + this.selectedYear, i + 1 + '/' + this.getNoOfDays(changes.selectedMonth.currentValue) + '/' + this.selectedYear);
                    /*  this.currentViewStartDate = i + 1 + '/01/' + this.selectedYear;
                       this.currentViewEndDate = i + 1 + '/' + this.getNoOfDays(changes.selectedMonth.currentValue) + '/' + this.selectedYear; */
                }
            }
        }
        if (changes && changes.data && changes.data.currentValue) {
            this.datas = Object.keys(changes.data.currentValue).length - 1;
            let k, v, startdate, enddate;
            // to compare the date and update the overlap of date
            for ([k, v] of Object.entries(changes.data.currentValue)) {
                if (typeof (v) !== 'string') {
                    for (let i = 0; i < v.length; i++) {
                        for (let j = i + 1; j <= v.length - 1; j++) {
                            if (v[j] && v[j].deliverableDetail && v[j].deliverableDetail.data) {
                                if (v[i].deliverableDetail.data.DELIVERABLE_DUE_DATE >= v[j].deliverableDetail.data.DELIVERABLE_DUE_DATE) {
                                    enddate = v[j].deliverableDetail.data.DELIVERABLE_DUE_DATE;
                                    if (v[j].deliverableDetail.data.DELIVERABLE_START_DATE <= v[i].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                        startdate = v[i].deliverableDetail.data.DELIVERABLE_START_DATE;
                                    }
                                    else if (v[i].deliverableDetail.data.DELIVERABLE_START_DATE <= v[j].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                        startdate = v[j].deliverableDetail.data.DELIVERABLE_START_DATE;
                                    }
                                }
                                else {
                                    enddate = v[i].deliverableDetail.data.DELIVERABLE_DUE_DATE;
                                    if (v[i].deliverableDetail.data.DELIVERABLE_START_DATE <= v[j].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                        startdate = v[j].deliverableDetail.data.DELIVERABLE_START_DATE;
                                    }
                                    else if (v[j].deliverableDetail.data.DELIVERABLE_START_DATE <= v[i].deliverableDetail.data.DELIVERABLE_START_DATE) {
                                        startdate = v[i].deliverableDetail.data.DELIVERABLE_START_DATE;
                                    }
                                }
                                let sd = parseInt(startdate.split('T')[0].split('-')[2] < 10 ? startdate.split('T')[0].split('-')[2].split('0')[1] : startdate.split('T')[0].split('-')[2]);
                                let ed = parseInt(enddate.split('T')[0].split('-')[2] < 10 ? enddate.split('T')[0].split('-')[2].split('0')[1] : enddate.split('T')[0].split('-')[2]);
                                for (let m = sd - 1; m < ed; m++) {
                                    console.log(changes.data.currentValue[k].date[m]);
                                    changes.data.currentValue[k].date[m].isOverlap = 'true';
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    ngOnInit() {
        const currentFullDate = new Date();
        const currentYear = currentFullDate.getFullYear();
        const currentMonth = currentFullDate.getMonth() + 1;
        const currentDate = currentFullDate.getDate();
        this.currentDate = currentMonth + '/' + currentDate + '/' + currentYear;
        let noOfDays = this.getNoOfDays(this.selectedMonth);
        this.buildCalendar(currentMonth + '/01/' + currentYear, currentMonth + '/' + noOfDays + '/' + currentYear);
        window.addEventListener('scroll', this.scrolling, true);
    }
};
ResourceManagementResourceAllocationComponent.ctorParameters = () => [
    { type: Renderer2 },
    { type: MediaMatcher },
    { type: ChangeDetectorRef }
];
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "selectedMonth", void 0);
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "data", void 0);
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "users", void 0);
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "selectedItems", void 0);
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "allocation", void 0);
__decorate([
    Input()
], ResourceManagementResourceAllocationComponent.prototype, "selectedYear", void 0);
__decorate([
    Output()
], ResourceManagementResourceAllocationComponent.prototype, "removed", void 0);
__decorate([
    ViewChild('calendarView')
], ResourceManagementResourceAllocationComponent.prototype, "calendarView", void 0);
__decorate([
    ViewChildren('dateHeader')
], ResourceManagementResourceAllocationComponent.prototype, "calendarHeaders", void 0);
__decorate([
    ViewChildren('panelContainer')
], ResourceManagementResourceAllocationComponent.prototype, "panelContainers", void 0);
__decorate([
    ViewChildren('calendarCell')
], ResourceManagementResourceAllocationComponent.prototype, "calendarCells", void 0);
__decorate([
    HostListener('window:resize', ['$event'])
], ResourceManagementResourceAllocationComponent.prototype, "onResize", null);
ResourceManagementResourceAllocationComponent = __decorate([
    Component({
        selector: 'mpm-resource-management-resource-allocation',
        template: "<p>resource-management-resource-allocation works!</p>\r\n",
        styles: [""]
    })
], ResourceManagementResourceAllocationComponent);
export { ResourceManagementResourceAllocationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Jlc291cmNlLW1hbmFnZW1lbnQvcmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uL3Jlc291cmNlLW1hbmFnZW1lbnQtcmVzb3VyY2UtYWxsb2NhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxTCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzVDLE9BQU8sS0FBSyxNQUFNLE1BQU0sUUFBUSxDQUFDO0FBQ2pDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVuRCxNQUFNLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7QUFPcEMsSUFBYSw2Q0FBNkMsR0FBMUQsTUFBYSw2Q0FBNkM7SUEyQ3hELFlBQ1MsUUFBbUIsRUFDbkIsS0FBbUIsRUFDbkIsaUJBQW9DO1FBRnBDLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsVUFBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBcENuQyxZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUU5QyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDWCxxQkFBZ0IsR0FBRyxHQUFHLENBQUMsQ0FBQSxLQUFLO1FBQzVCLGNBQVMsR0FBRyxNQUFNLENBQUM7UUFrQm5CLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBR2xCLFdBQU0sR0FBRyxNQUFNLENBQUM7UUFFaEIsU0FBSSxHQUFHLElBQUksQ0FBQztRQXNEWiwrQ0FBK0M7UUFDL0MsY0FBUyxHQUFHLENBQUMsRUFBRSxFQUFRLEVBQUU7WUFDdkIsTUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7WUFDMUMsSUFBSSxTQUFTLElBQUksR0FBRyxFQUFFO2dCQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQTtRQXBEQyxLQUFLO1FBQ0wsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUlELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELEtBQUs7SUFDTCxrQkFBa0I7UUFDaEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ25CLE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNsQixNQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3JDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDN0UsQ0FBQztJQUVELHVCQUF1QjtJQUN2QixZQUFZLENBQUMsTUFBTTtRQUNqQixJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLElBQUksRUFBRTtZQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxJQUFJLE1BQU0sQ0FBQyxTQUFTLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2xNLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksS0FBSyxTQUFTLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUN0SiwyRUFBMkU7aUJBQzVFO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM5TCxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3JDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO1lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNwQixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBWUQsd0JBQXdCO0lBRXhCLG9EQUFvRDtJQUNwRCxhQUFhLENBQUMsU0FBUyxFQUFFLE9BQU87UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFNBQVMsQ0FBQztRQUN0QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDO1FBQ2xDLE1BQU0sS0FBSyxHQUFHLFNBQVMsQ0FBQztRQUN4QixNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUM7UUFDcEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxXQUFXLENBQUMsYUFBYTtRQUN2QixJQUFJLFFBQVEsQ0FBQztRQUNiLElBQUksaUJBQWlCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUN2RCxRQUFRLEdBQUcsRUFBRSxDQUFDO1NBQ2Y7YUFBTSxJQUFJLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDN0QsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUNmO2FBQU0sSUFBSSxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQzlEOzRDQUNnQztZQUNoQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLFFBQVEsQ0FBQztJQUNsQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsU0FBUyxFQUFFLE9BQU87UUFDbEMsSUFBSSxFQUFFLEdBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3BDLElBQUksRUFBRSxHQUFRLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLGFBQWEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQzFFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN4RCxJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRTtvQkFDdEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNySjtpSkFDNkg7aUJBQzlIO2FBQ0Y7U0FDRjtRQUVELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDeEQsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQU0sRUFBRSxDQUFNLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQztZQUN2QyxxREFBcUQ7WUFDckQsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3hELElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTtvQkFDM0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQzFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFO2dDQUNqRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtvQ0FDeEcsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUE7b0NBQzFELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO3dDQUM1RyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQTtxQ0FDL0Q7eUNBQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7d0NBQ25ILFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFBO3FDQUMvRDtpQ0FDRjtxQ0FBTTtvQ0FDTCxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQTtvQ0FDMUQsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7d0NBQzVHLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFBO3FDQUMvRDt5Q0FBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTt3Q0FDbkgsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUE7cUNBQy9EO2lDQUNGO2dDQUNELElBQUksRUFBRSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQ0FDM0osSUFBSSxFQUFFLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO2dDQUNySixLQUFLLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQ0FDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQ0FDakQsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7aUNBQ3pEOzZCQUNGO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sTUFBTSxlQUFlLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNuQyxNQUFNLFdBQVcsR0FBRyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbEQsTUFBTSxZQUFZLEdBQUcsZUFBZSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNwRCxNQUFNLFdBQVcsR0FBRyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLEdBQUcsR0FBRyxHQUFHLFdBQVcsR0FBRyxHQUFHLEdBQUcsV0FBVyxDQUFDO1FBQ3hFLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLE1BQU0sR0FBRyxXQUFXLEVBQUUsWUFBWSxHQUFHLEdBQUcsR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQyxDQUFDO1FBQzNHLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMxRCxDQUFDO0NBRUYsQ0FBQTs7WUF2Sm9CLFNBQVM7WUFDWixZQUFZO1lBQ0EsaUJBQWlCOztBQTVDcEM7SUFBUixLQUFLLEVBQUU7b0ZBQW9CO0FBQ25CO0lBQVIsS0FBSyxFQUFFOzJFQUFNO0FBRUw7SUFBUixLQUFLLEVBQUU7NEVBQU87QUFDTjtJQUFSLEtBQUssRUFBRTtvRkFBZTtBQUNkO0lBQVIsS0FBSyxFQUFFO2lGQUFZO0FBQ1g7SUFBUixLQUFLLEVBQUU7bUZBQWM7QUFFWjtJQUFULE1BQU0sRUFBRTs4RUFBcUM7QUFVOUM7SUFEQyxTQUFTLENBQUMsY0FBYyxDQUFDO21GQUNEO0FBR3pCO0lBREMsWUFBWSxDQUFDLFlBQVksQ0FBQztzRkFDWTtBQUd2QztJQURDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQztzRkFDUTtBQUd2QztJQURDLFlBQVksQ0FBQyxjQUFjLENBQUM7b0ZBQ1E7QUF5QnJDO0lBRkMsWUFBWSxDQUFDLGVBQWUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZFQUl6QztBQXhEVSw2Q0FBNkM7SUFMekQsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLDZDQUE2QztRQUN2RCxxRUFBdUU7O0tBRXhFLENBQUM7R0FDVyw2Q0FBNkMsQ0FtTXpEO1NBbk1ZLDZDQUE2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGQsIFZpZXdDaGlsZHJlbiwgUXVlcnlMaXN0LCBSZW5kZXJlcjIsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgU2ltcGxlQ2hhbmdlcywgQ2hhbmdlRGV0ZWN0b3JSZWYsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBleHRlbmRNb21lbnQgfSBmcm9tICdtb21lbnQtcmFuZ2UnO1xyXG5pbXBvcnQgKiBhcyBNb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgQ2FsZW5kYXJDb25zdGFudHMgfSBmcm9tICcuLi9zaGFyZWQvY29uc3RhbnRzL0NhbGVuZGFyLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1lZGlhTWF0Y2hlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9sYXlvdXQnO1xyXG5cclxuY29uc3QgbW9tZW50ID0gZXh0ZW5kTW9tZW50KE1vbWVudCk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1yZXNvdXJjZS1tYW5hZ2VtZW50LXJlc291cmNlLWFsbG9jYXRpb24nLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9yZXNvdXJjZS1tYW5hZ2VtZW50LXJlc291cmNlLWFsbG9jYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3Jlc291cmNlLW1hbmFnZW1lbnQtcmVzb3VyY2UtYWxsb2NhdGlvbi5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFJlc291cmNlTWFuYWdlbWVudFJlc291cmNlQWxsb2NhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHNlbGVjdGVkTW9udGg6IGFueTtcclxuICBASW5wdXQoKSBkYXRhO1xyXG4gIC8vIEBJbnB1dCgpIGRhdGVzc3M7XHJcbiAgQElucHV0KCkgdXNlcnM7XHJcbiAgQElucHV0KCkgc2VsZWN0ZWRJdGVtcztcclxuICBASW5wdXQoKSBhbGxvY2F0aW9uO1xyXG4gIEBJbnB1dCgpIHNlbGVjdGVkWWVhcjtcclxuXHJcbiAgQE91dHB1dCgpIHJlbW92ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG5cclxuICBzY3JvbGxlZCA9IGZhbHNlO1xyXG4gIGRhdGVzID0gW107XHJcbiAgdmlld1R5cGVDZWxsU2l6ZSA9IDE1MDsvLzM1O1xyXG4gIG1vbWVudFB0ciA9IG1vbWVudDtcclxuICBjdXJyZW50Vmlld1N0YXJ0RGF0ZTogbW9tZW50Lk1vbWVudDtcclxuICBjdXJyZW50Vmlld0VuZERhdGU6IG1vbWVudC5Nb21lbnQ7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ2NhbGVuZGFyVmlldycpXHJcbiAgY2FsZW5kYXJWaWV3OiBFbGVtZW50UmVmO1xyXG5cclxuICBAVmlld0NoaWxkcmVuKCdkYXRlSGVhZGVyJylcclxuICBjYWxlbmRhckhlYWRlcnM6IFF1ZXJ5TGlzdDxFbGVtZW50UmVmPjtcclxuXHJcbiAgQFZpZXdDaGlsZHJlbigncGFuZWxDb250YWluZXInKVxyXG4gIHBhbmVsQ29udGFpbmVyczogUXVlcnlMaXN0PEVsZW1lbnRSZWY+O1xyXG5cclxuICBAVmlld0NoaWxkcmVuKCdjYWxlbmRhckNlbGwnKVxyXG4gIGNhbGVuZGFyQ2VsbHM6IFF1ZXJ5TGlzdDxFbGVtZW50UmVmPjtcclxuXHJcbiAgY3VycmVudERhdGU7XHJcblxyXG4gIHNpZGViYXJTdHlsZSA9IHt9O1xyXG5cclxuICBkYXRhcztcclxuICBPYmplY3QgPSBPYmplY3Q7XHJcblxyXG4gIE1hdGggPSBNYXRoO1xyXG5cclxuICBzY3JlZW5XaWR0aDogYW55O1xyXG4gIGhlaWdodDtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgIHB1YmxpYyBtZWRpYTogTWVkaWFNYXRjaGVyLFxyXG4gICAgcHVibGljIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICApIHtcclxuICAgIC8vMTkxXHJcbiAgICB0aGlzLmdldEhlaWdodEZvclNjcmVlbigpO1xyXG4gIH1cclxuXHJcbiAgQEhvc3RMaXN0ZW5lcignd2luZG93OnJlc2l6ZScsIFsnJGV2ZW50J10pXHJcblxyXG4gIG9uUmVzaXplKGV2ZW50KSB7XHJcbiAgICB0aGlzLmdldEhlaWdodEZvclNjcmVlbigpO1xyXG4gIH1cclxuXHJcbiAgLy8xOTFcclxuICBnZXRIZWlnaHRGb3JTY3JlZW4oKSB7XHJcbiAgICBjb25zdCB3aWR0aCA9IDE1MzY7XHJcbiAgICBjb25zdCBoZWlnaHQgPSAyNjtcclxuICAgIGNvbnN0IG1heF9oZWlnaHQgPSAzOTtcclxuICAgIHRoaXMuc2NyZWVuV2lkdGggPSB3aW5kb3cuaW5uZXJXaWR0aDtcclxuICAgIGNvbnN0IGRpZmYgPSBNYXRoLmZsb29yKCh0aGlzLnNjcmVlbldpZHRoIC0gd2lkdGgpIC8gMTcxKTtcclxuICAgIHRoaXMuaGVpZ2h0ID0gaGVpZ2h0ICsgKGRpZmYgKiAyKSA8PSBtYXhfaGVpZ2h0ID8gaGVpZ2h0ICsgKGRpZmYgKiAyKSA6IDM4O1xyXG4gIH1cclxuXHJcbiAgLy8gdG8gcmVtb3ZlIHRoZSBmaWx0ZXJcclxuICByZW1vdmVGaWx0ZXIoZmlsdGVyKSB7XHJcbiAgICBpZiAoZmlsdGVyLlJPTEVfTkFNRSB8fCBmaWx0ZXIuTkFNRSkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSXRlbXMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICBpZiAoKGl0ZW0uUk9MRV9OQU1FICE9PSB1bmRlZmluZWQgJiYgZmlsdGVyLlJPTEVfTkFNRSAhPT0gdW5kZWZpbmVkICYmIGl0ZW0uUk9MRV9OQU1FID09PSBmaWx0ZXIuUk9MRV9OQU1FKSB8fCAoaXRlbS5OQU1FICE9PSB1bmRlZmluZWQgJiYgZmlsdGVyLk5BTUUgIT09IHVuZGVmaW5lZCAmJiBpdGVtLk5BTUUgPT09IGZpbHRlci5OQU1FKSkge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1zLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICB0aGlzLnJlbW92ZWQubmV4dChmaWx0ZXIuTkFNRSAhPT0gdW5kZWZpbmVkICYmIGZpbHRlclsnTVBNX1RlYW1zLWlkJ10gIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXJbJ01QTV9UZWFtcy1pZCddLklkICE9PSB1bmRlZmluZWQgPyBbJ1RFQU0nXSA6IFsnUk9MRSddKTtcclxuICAgICAgICAgIC8vIHRoaXMucmVtb3ZlZC5uZXh0KGZpbHRlci5ST0xFX05BTUUgIT09IHVuZGVmaW5lZCA/IFsnUk9MRSddIDogWydURUFNJ10pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSXRlbXMuZm9yRWFjaCgoaXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICBpZiAoKGl0ZW0uRnVsbE5hbWUgIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXIuRnVsbE5hbWUgIT09IHVuZGVmaW5lZCAmJiBpdGVtLkZ1bGxOYW1lID09PSBmaWx0ZXIuRnVsbE5hbWUpIHx8IChpdGVtLm5hbWUgIT09IHVuZGVmaW5lZCAmJiBmaWx0ZXIubmFtZSAhPT0gdW5kZWZpbmVkICYmIGl0ZW0ubmFtZSA9PT0gZmlsdGVyLm5hbWUpKSB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkSXRlbXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGZpbHRlci5GdWxsTmFtZSB8fCBmaWx0ZXIubmFtZSkge1xyXG4gICAgICBjb25zb2xlLmxvZyhcIiMjI1wiKVxyXG4gICAgICBjb25zb2xlLmxvZyhmaWx0ZXIpO1xyXG4gICAgICB0aGlzLnJlbW92ZWQubmV4dChbZmlsdGVyXSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKiogKiBTY3JvbGwgZm9yIEZpeGluZyB0aGUgZGF0ZXMgYW5kIGhlYWRlciAqL1xyXG4gIHNjcm9sbGluZyA9IChlbCk6IHZvaWQgPT4ge1xyXG4gICAgY29uc3Qgc2Nyb2xsVG9wID0gZWwuc3JjRWxlbWVudC5zY3JvbGxUb3A7XHJcbiAgICBpZiAoc2Nyb2xsVG9wID49IDEwMCkge1xyXG4gICAgICB0aGlzLnNjcm9sbGVkID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2Nyb2xsZWQgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKiBDQUxFTkRBUiBNRVRIT0RTICoqL1xyXG5cclxuICAvLyBidWlsZCBjYWxlbmRhciBiYXNlZCBvbiBnaXZlbiBzdGFydCAgYW5kIGVuZCBkYXRlXHJcbiAgYnVpbGRDYWxlbmRhcihzdGFydERhdGUsIGVuZERhdGUpIHtcclxuICAgIHRoaXMuY3VycmVudFZpZXdTdGFydERhdGUgPSBzdGFydERhdGU7XHJcbiAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IGVuZERhdGU7XHJcbiAgICBjb25zdCBzdGFydCA9IHN0YXJ0RGF0ZTtcclxuICAgIGNvbnN0IGVuZCA9IGVuZERhdGU7XHJcbiAgICBjb25zdCByYW5nZSA9IHRoaXMubW9tZW50UHRyLnJhbmdlKHN0YXJ0LCBlbmQpO1xyXG4gICAgY29uc3QgZGF5cyA9IEFycmF5LmZyb20ocmFuZ2UuYnkoJ2RheXMnKSk7XHJcbiAgICB0aGlzLmRhdGVzID0gZGF5cztcclxuICB9XHJcblxyXG4gIGdldE5vT2ZEYXlzKHNlbGVjdGVkTW9udGgpIHtcclxuICAgIGxldCBub09mRGF5cztcclxuICAgIGlmIChDYWxlbmRhckNvbnN0YW50cy5ldmVuTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgbm9PZkRheXMgPSAzMTtcclxuICAgIH0gZWxzZSBpZiAoQ2FsZW5kYXJDb25zdGFudHMub2RkTW9udGguaW5jbHVkZXMoc2VsZWN0ZWRNb250aCkpIHtcclxuICAgICAgbm9PZkRheXMgPSAzMDtcclxuICAgIH0gZWxzZSBpZiAoQ2FsZW5kYXJDb25zdGFudHMubGVhcE1vbnRoLmluY2x1ZGVzKHNlbGVjdGVkTW9udGgpKSB7XHJcbiAgICAgIC8qIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSh0aGlzLnllYXIsIDEsIDI5KTtcclxuICAgICAgcmV0dXJuIGRhdGUuZ2V0TW9udGgoKSA9PT0gMTsgKi9cclxuICAgICAgbm9PZkRheXMgPSAyODtcclxuICAgIH1cclxuICAgIHJldHVybiBub09mRGF5cztcclxuICB9XHJcblxyXG4gIGdldERhdGVEaWZmZXJlbmNlKHN0YXJ0RGF0ZSwgZW5kRGF0ZSkge1xyXG4gICAgbGV0IHNkOiBhbnkgPSAobmV3IERhdGUoc3RhcnREYXRlKSk7XHJcbiAgICBsZXQgZWQ6IGFueSA9IG5ldyBEYXRlKGVuZERhdGUpO1xyXG4gICAgcmV0dXJuIChNYXRoLmZsb29yKGVkIC0gc2QpIC8gKDEwMDAgKiA2MCAqIDYwICogMjQpKSArIDE7XHJcbiAgfVxyXG5cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICBpZiAoY2hhbmdlcyAmJiBjaGFuZ2VzLnNlbGVjdGVkTW9udGggJiYgIWNoYW5nZXMuc2VsZWN0ZWRNb250aC5maXJzdENoYW5nZSkge1xyXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IENhbGVuZGFyQ29uc3RhbnRzLm1vbnRocy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGlmIChDYWxlbmRhckNvbnN0YW50cy5tb250aHNbaV0gPT09IGNoYW5nZXMuc2VsZWN0ZWRNb250aC5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuYnVpbGRDYWxlbmRhcihpICsgMSArICcvMDEvJyArIHRoaXMuc2VsZWN0ZWRZZWFyLCBpICsgMSArICcvJyArIHRoaXMuZ2V0Tm9PZkRheXMoY2hhbmdlcy5zZWxlY3RlZE1vbnRoLmN1cnJlbnRWYWx1ZSkgKyAnLycgKyB0aGlzLnNlbGVjdGVkWWVhcik7XHJcbiAgICAgICAgICAvKiAgdGhpcy5jdXJyZW50Vmlld1N0YXJ0RGF0ZSA9IGkgKyAxICsgJy8wMS8nICsgdGhpcy5zZWxlY3RlZFllYXI7XHJcbiAgICAgICAgICAgICB0aGlzLmN1cnJlbnRWaWV3RW5kRGF0ZSA9IGkgKyAxICsgJy8nICsgdGhpcy5nZXROb09mRGF5cyhjaGFuZ2VzLnNlbGVjdGVkTW9udGguY3VycmVudFZhbHVlKSArICcvJyArIHRoaXMuc2VsZWN0ZWRZZWFyOyAqL1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuZGF0YSAmJiBjaGFuZ2VzLmRhdGEuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgIHRoaXMuZGF0YXMgPSBPYmplY3Qua2V5cyhjaGFuZ2VzLmRhdGEuY3VycmVudFZhbHVlKS5sZW5ndGggLSAxO1xyXG4gICAgICBsZXQgazogYW55LCB2OiBhbnksIHN0YXJ0ZGF0ZSwgZW5kZGF0ZTtcclxuICAgICAgLy8gdG8gY29tcGFyZSB0aGUgZGF0ZSBhbmQgdXBkYXRlIHRoZSBvdmVybGFwIG9mIGRhdGVcclxuICAgICAgZm9yIChbaywgdl0gb2YgT2JqZWN0LmVudHJpZXMoY2hhbmdlcy5kYXRhLmN1cnJlbnRWYWx1ZSkpIHtcclxuICAgICAgICBpZiAodHlwZW9mICh2KSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdi5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBqID0gaSArIDE7IGogPD0gdi5sZW5ndGggLSAxOyBqKyspIHtcclxuICAgICAgICAgICAgICBpZiAodltqXSAmJiB2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsICYmIHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZbaV0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9EVUVfREFURSA+PSB2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfRFVFX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgZW5kZGF0ZSA9IHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9EVUVfREFURVxyXG4gICAgICAgICAgICAgICAgICBpZiAodltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUgPD0gdltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdGFydGRhdGUgPSB2W2ldLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURVxyXG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZbaV0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFIDw9IHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnRkYXRlID0gdltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEVcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgZW5kZGF0ZSA9IHZbaV0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9EVUVfREFURVxyXG4gICAgICAgICAgICAgICAgICBpZiAodltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUgPD0gdltqXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdGFydGRhdGUgPSB2W2pdLmRlbGl2ZXJhYmxlRGV0YWlsLmRhdGEuREVMSVZFUkFCTEVfU1RBUlRfREFURVxyXG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZbal0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFIDw9IHZbaV0uZGVsaXZlcmFibGVEZXRhaWwuZGF0YS5ERUxJVkVSQUJMRV9TVEFSVF9EQVRFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnRkYXRlID0gdltpXS5kZWxpdmVyYWJsZURldGFpbC5kYXRhLkRFTElWRVJBQkxFX1NUQVJUX0RBVEVcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbGV0IHNkID0gcGFyc2VJbnQoc3RhcnRkYXRlLnNwbGl0KCdUJylbMF0uc3BsaXQoJy0nKVsyXSA8IDEwID8gc3RhcnRkYXRlLnNwbGl0KCdUJylbMF0uc3BsaXQoJy0nKVsyXS5zcGxpdCgnMCcpWzFdIDogc3RhcnRkYXRlLnNwbGl0KCdUJylbMF0uc3BsaXQoJy0nKVsyXSlcclxuICAgICAgICAgICAgICAgIGxldCBlZCA9IHBhcnNlSW50KGVuZGRhdGUuc3BsaXQoJ1QnKVswXS5zcGxpdCgnLScpWzJdIDwgMTAgPyBlbmRkYXRlLnNwbGl0KCdUJylbMF0uc3BsaXQoJy0nKVsyXS5zcGxpdCgnMCcpWzFdIDogZW5kZGF0ZS5zcGxpdCgnVCcpWzBdLnNwbGl0KCctJylbMl0pXHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBtID0gc2QgLSAxOyBtIDwgZWQ7IG0rKykge1xyXG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjaGFuZ2VzLmRhdGEuY3VycmVudFZhbHVlW2tdLmRhdGVbbV0pXHJcbiAgICAgICAgICAgICAgICAgIGNoYW5nZXMuZGF0YS5jdXJyZW50VmFsdWVba10uZGF0ZVttXS5pc092ZXJsYXAgPSAndHJ1ZSc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGNvbnN0IGN1cnJlbnRGdWxsRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICBjb25zdCBjdXJyZW50WWVhciA9IGN1cnJlbnRGdWxsRGF0ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgY29uc3QgY3VycmVudE1vbnRoID0gY3VycmVudEZ1bGxEYXRlLmdldE1vbnRoKCkgKyAxO1xyXG4gICAgY29uc3QgY3VycmVudERhdGUgPSBjdXJyZW50RnVsbERhdGUuZ2V0RGF0ZSgpO1xyXG4gICAgdGhpcy5jdXJyZW50RGF0ZSA9IGN1cnJlbnRNb250aCArICcvJyArIGN1cnJlbnREYXRlICsgJy8nICsgY3VycmVudFllYXI7XHJcbiAgICBsZXQgbm9PZkRheXMgPSB0aGlzLmdldE5vT2ZEYXlzKHRoaXMuc2VsZWN0ZWRNb250aCk7XHJcbiAgICB0aGlzLmJ1aWxkQ2FsZW5kYXIoY3VycmVudE1vbnRoICsgJy8wMS8nICsgY3VycmVudFllYXIsIGN1cnJlbnRNb250aCArICcvJyArIG5vT2ZEYXlzICsgJy8nICsgY3VycmVudFllYXIpO1xyXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuc2Nyb2xsaW5nLCB0cnVlKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==