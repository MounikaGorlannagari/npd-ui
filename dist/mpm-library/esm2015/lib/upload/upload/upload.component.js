import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../notification/notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AssetUploadService } from '../services/asset.upload.service';
import { LoaderService } from '../../loader/loader.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AssetConstants } from '../../project/assets/asset_constants';
let UploadComponent = class UploadComponent {
    constructor(dialogRef, data, assetUploadService, loaderService, notificationService, otmmService, sharingService, utilService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.assetUploadService = assetUploadService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.uploadedFiles = [];
        this.uploadProcessing = false;
        this.selected = 'Project';
        this.referenceAsset = ['Project', 'DeliverableName1', 'DeliverableName2'];
    }
    closeDialog() {
        this.dialogRef.close(false);
    }
    udploadFileEvent(files) {
        this.uploadedFiles = files;
    }
    validateFile() {
        let isValidFile = false;
        let count = 0;
        this.uploadedFiles.map(file => {
            if (file) {
                const fileExtensionData = this.assetConfig[ApplicationConfigConstants.MPM_ASSET_CONFIG.ALLOWED_FILE_TYPES];
                let isvalid = false;
                if (!(this.utilService.isNullOrEmpty(fileExtensionData))) {
                    const fileExtensionList = fileExtensionData.split(',');
                    if (fileExtensionList && fileExtensionList.length > 0) {
                        fileExtensionList.map(fileExtension => {
                            /*  if (fileExtensionListSplit && fileExtensionListSplit.length > 0) {
                                 fileExtensionListSplit.map(fileExtension => { */
                            if (file.name.substr(file.name.length - fileExtension.length, fileExtension.length).toLowerCase() ===
                                fileExtension.toLowerCase()) {
                                count++;
                                isvalid = true;
                            }
                        });
                        if (isvalid) {
                            isValidFile = true;
                        }
                    }
                    else {
                        isValidFile = true;
                    }
                }
                else {
                    isValidFile = true;
                }
            }
        });
        if (count === this.uploadedFiles.length) {
            isValidFile = true;
        }
        else {
            isValidFile = false;
        }
        return isValidFile;
    }
    // validateFile() {
    //     let hasValidFiles = false;
    //     this.uploadedFiles.map(file => {
    //         if (file) {
    //             const fileExtensionList = FileTypes;
    //             for (const [key, value] of Object.entries(fileExtensionList)) {
    //                 if (file.name.substr(file.name.length - value.length, value.length).toLowerCase() ===
    //                     value.toLowerCase()) {
    //                     hasValidFiles = true;
    //                     break;
    //                 }
    //             }
    //         }
    //     });
    //     return hasValidFiles;
    // }
    uploadFiles() {
        this.loaderService.show();
        console.log(this.data);
        if (this.validateFile()) {
            if (this.uploadedFiles.length > 0) {
                this.uploadProcessing = true;
                this.otmmService.checkOtmmSession(true)
                    .subscribe(checkOtmmSessionResponse => {
                    this.assetUploadService.startUpload(this.uploadedFiles, this.data.deliverable, this.data.isRevisionUpload, this.data.folderId, this.data.project)
                        .subscribe(response => {
                        this.loaderService.hide();
                        this.dialogRef.close(true);
                        this.notificationService.info('Upload has been initiated');
                    }, error => {
                        this.loaderService.hide();
                        this.uploadProcessing = false;
                        this.notificationService.error('Something went wrong while uploading the file(s)');
                    });
                }, checkOtmmSessionError => {
                    this.loaderService.hide();
                    this.notificationService.error('Something went wrong while getting OTMM session');
                    this.uploadProcessing = false;
                });
            }
            else {
                this.loaderService.hide();
                this.notificationService.info('Please select or drag and drop files to upload');
            }
        }
        else {
            this.loaderService.hide();
            if (this.uploadedFiles.length > 0) {
                this.notificationService.info('Uploaded file format is not supported');
            }
            else {
                this.notificationService.info('Please select or drag and drop files to upload');
            }
        }
    }
    changeReferenceType(option) {
    }
    ngOnInit() {
        this.assetConfig = this.sharingService.getAssetConfig();
        this.referenceTypeObj = AssetConstants.REFERENCE_TYPES_OBJECTS;
    }
};
UploadComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: AssetUploadService },
    { type: LoaderService },
    { type: NotificationService },
    { type: OTMMService },
    { type: SharingService },
    { type: UtilService }
];
UploadComponent = __decorate([
    Component({
        selector: 'mpm-upload',
        template: "<div>\r\n    <div class=\"flex-row\">\r\n        <h1 mat-dialog-title class=\"upload-modal-title\">{{data.modalName}}</h1>\r\n        <mat-icon class=\"modal-close\" matTooltip=\"Close\" (click)=\"closeDialog()\">\r\n            close\r\n        </mat-icon>\r\n    </div>\r\n    <div class=\"flex-row\" *ngIf=\"data.needReference\">\r\n        <div class=\"referenceContainer\">\r\n            <div>Reference To</div>&nbsp;&nbsp;&nbsp;\r\n            <div>\r\n                <button  class=\"sort-btn\" [disabled]=\"true\"\r\n                    mat-stroked-button matTooltip=\"Select delivery format\" [matMenuTriggerFor]=\"referenceType\">\r\n                    <span> {{referenceTypeObj.selected.name}}</span>\r\n                    <mat-icon color=\"accent\">arrow_drop_down</mat-icon>\r\n                </button>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n    <mat-dialog-content>\r\n        <mpm-file-upload (queuedFiles)=\"udploadFileEvent($event)\" [isRevisionUpload]=\"data.isRevisionUpload\"\r\n            [fileToRevision]=\"data.fileToRevision\" [maxFiles]=\"data.maxFiles\" [maxFileSize]=\"data.maxFileSize\">\r\n        </mpm-file-upload>\r\n    </mat-dialog-content>\r\n    <mat-dialog-actions align=\"end\">\r\n        <button mat-stroked-button (click)=\"closeDialog()\">\r\n            <span>Close</span>\r\n        </button>\r\n        <button color=\"primary\" mat-flat-button (click)=\"uploadFiles()\" [disabled]=\"uploadProcessing\">\r\n            <span>Finish</span>\r\n        </button>\r\n    </mat-dialog-actions>\r\n</div>\r\n\r\n<mat-menu #referenceType=\"matMenu\">\r\n    <span *ngFor=\"let option of referenceTypeObj.options\">\r\n        <button *ngIf=\"option.name != referenceTypeObj.selected\" mat-menu-item (click)=\"changeReferenceType(option)\">\r\n            <span>{{option.name}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
        styles: [".upload-modal-title{font-weight:700;flex-grow:1}button{margin:10px}mat-dialog-content{height:350px}.modal-close{cursor:pointer}.referenceContainer{display:flex;flex-wrap:nowrap;align-items:baseline}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], UploadComponent);
export { UploadComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3VwbG9hZC91cGxvYWQvdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBU3RFLElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFFeEIsWUFDVyxTQUF3QyxFQUNmLElBQVMsRUFDbEMsa0JBQXNDLEVBQ3RDLGFBQTRCLEVBQzVCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixjQUE4QixFQUM5QixXQUF3QjtRQVB4QixjQUFTLEdBQVQsU0FBUyxDQUErQjtRQUNmLFNBQUksR0FBSixJQUFJLENBQUs7UUFDbEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUduQyxrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFFekIsYUFBUSxHQUFHLFNBQVMsQ0FBQztRQUNyQixtQkFBYyxHQUFHLENBQUMsU0FBUyxFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFOakUsQ0FBQztJQVFMLFdBQVc7UUFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsS0FBSztRQUNsQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUMvQixDQUFDO0lBRUQsWUFBWTtRQUNSLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMxQixJQUFJLElBQUksRUFBRTtnQkFFTixNQUFNLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsMEJBQTBCLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDM0csSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7b0JBQ3RELE1BQU0saUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN2RCxJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ25ELGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBRTs0QkFDbEM7aUZBQ3FEOzRCQUNyRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLGFBQWEsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsRUFBRTtnQ0FDN0YsYUFBYSxDQUFDLFdBQVcsRUFBRSxFQUFFO2dDQUM3QixLQUFLLEVBQUUsQ0FBQztnQ0FDUixPQUFPLEdBQUcsSUFBSSxDQUFDOzZCQUNsQjt3QkFDTCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLE9BQU8sRUFBRTs0QkFDVCxXQUFXLEdBQUcsSUFBSSxDQUFDO3lCQUN0QjtxQkFDSjt5QkFBTTt3QkFDSCxXQUFXLEdBQUcsSUFBSSxDQUFDO3FCQUN0QjtpQkFDSjtxQkFBTTtvQkFDSCxXQUFXLEdBQUcsSUFBSSxDQUFDO2lCQUN0QjthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUNyQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO2FBQU07WUFDSCxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQUVELG1CQUFtQjtJQUNuQixpQ0FBaUM7SUFDakMsdUNBQXVDO0lBQ3ZDLHNCQUFzQjtJQUN0QixtREFBbUQ7SUFDbkQsOEVBQThFO0lBQzlFLHdHQUF3RztJQUN4Ryw2Q0FBNkM7SUFDN0MsNENBQTRDO0lBQzVDLDZCQUE2QjtJQUM3QixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixVQUFVO0lBRVYsNEJBQTRCO0lBQzVCLElBQUk7SUFFSixXQUFXO1FBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUNyQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7cUJBQ2xDLFNBQVMsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFO29CQUNsQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQ3pFLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7eUJBQ2pFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzNCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztvQkFDL0QsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7d0JBQzlCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztvQkFDdkYsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxFQUFFLHFCQUFxQixDQUFDLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztvQkFDbEYsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7YUFDbkY7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO2FBQzFFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0RBQWdELENBQUMsQ0FBQzthQUNuRjtTQUNKO0lBQ0wsQ0FBQztJQUVELG1CQUFtQixDQUFDLE1BQU07SUFFMUIsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDeEQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQztJQUNuRSxDQUFDO0NBRUosQ0FBQTs7WUFoSXlCLFlBQVk7NENBQzdCLE1BQU0sU0FBQyxlQUFlO1lBQ0ksa0JBQWtCO1lBQ3ZCLGFBQWE7WUFDUCxtQkFBbUI7WUFDM0IsV0FBVztZQUNSLGNBQWM7WUFDakIsV0FBVzs7QUFWMUIsZUFBZTtJQU4zQixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsWUFBWTtRQUN0Qix3NERBQXNDOztLQUV6QyxDQUFDO0lBTU8sV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FKbkIsZUFBZSxDQW1JM0I7U0FuSVksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1BVF9ESUFMT0dfREFUQSwgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQXNzZXRVcGxvYWRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYXNzZXQudXBsb2FkLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWxlVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9GaWxlVHlwZXMnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBc3NldENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Byb2plY3QvYXNzZXRzL2Fzc2V0X2NvbnN0YW50cyc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS11cGxvYWQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VwbG9hZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi91cGxvYWQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFVwbG9hZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPFVwbG9hZENvbXBvbmVudD4sXHJcbiAgICAgICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBhbnksXHJcbiAgICAgICAgcHVibGljIGFzc2V0VXBsb2FkU2VydmljZTogQXNzZXRVcGxvYWRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHVwbG9hZGVkRmlsZXMgPSBbXTtcclxuICAgIHVwbG9hZFByb2Nlc3NpbmcgPSBmYWxzZTtcclxuICAgIGFzc2V0Q29uZmlnOiBhbnk7XHJcbiAgICBzZWxlY3RlZCA9ICdQcm9qZWN0JztcclxuICAgIHJlZmVyZW5jZUFzc2V0ID0gWydQcm9qZWN0JywgJ0RlbGl2ZXJhYmxlTmFtZTEnLCAnRGVsaXZlcmFibGVOYW1lMiddO1xyXG4gICAgcmVmZXJlbmNlVHlwZU9iajtcclxuICAgIGNsb3NlRGlhbG9nKCkge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICB1ZHBsb2FkRmlsZUV2ZW50KGZpbGVzKSB7XHJcbiAgICAgICAgdGhpcy51cGxvYWRlZEZpbGVzID0gZmlsZXM7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGVGaWxlKCkge1xyXG4gICAgICAgIGxldCBpc1ZhbGlkRmlsZSA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgdGhpcy51cGxvYWRlZEZpbGVzLm1hcChmaWxlID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpbGUpIHtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWxlRXh0ZW5zaW9uRGF0YSA9IHRoaXMuYXNzZXRDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FTU0VUX0NPTkZJRy5BTExPV0VEX0ZJTEVfVFlQRVNdO1xyXG4gICAgICAgICAgICAgICAgbGV0IGlzdmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmICghKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShmaWxlRXh0ZW5zaW9uRGF0YSkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmlsZUV4dGVuc2lvbkxpc3QgPSBmaWxlRXh0ZW5zaW9uRGF0YS5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmaWxlRXh0ZW5zaW9uTGlzdCAmJiBmaWxlRXh0ZW5zaW9uTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVFeHRlbnNpb25MaXN0Lm1hcChmaWxlRXh0ZW5zaW9uID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qICBpZiAoZmlsZUV4dGVuc2lvbkxpc3RTcGxpdCAmJiBmaWxlRXh0ZW5zaW9uTGlzdFNwbGl0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZUV4dGVuc2lvbkxpc3RTcGxpdC5tYXAoZmlsZUV4dGVuc2lvbiA9PiB7ICovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmlsZS5uYW1lLnN1YnN0cihmaWxlLm5hbWUubGVuZ3RoIC0gZmlsZUV4dGVuc2lvbi5sZW5ndGgsIGZpbGVFeHRlbnNpb24ubGVuZ3RoKS50b0xvd2VyQ2FzZSgpID09PVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVFeHRlbnNpb24udG9Mb3dlckNhc2UoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvdW50Kys7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXN2YWxpZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXN2YWxpZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNWYWxpZEZpbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNWYWxpZEZpbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNWYWxpZEZpbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGNvdW50ID09PSB0aGlzLnVwbG9hZGVkRmlsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGlzVmFsaWRGaWxlID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpc1ZhbGlkRmlsZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNWYWxpZEZpbGU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdmFsaWRhdGVGaWxlKCkge1xyXG4gICAgLy8gICAgIGxldCBoYXNWYWxpZEZpbGVzID0gZmFsc2U7XHJcbiAgICAvLyAgICAgdGhpcy51cGxvYWRlZEZpbGVzLm1hcChmaWxlID0+IHtcclxuICAgIC8vICAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgIC8vICAgICAgICAgICAgIGNvbnN0IGZpbGVFeHRlbnNpb25MaXN0ID0gRmlsZVR5cGVzO1xyXG4gICAgLy8gICAgICAgICAgICAgZm9yIChjb25zdCBba2V5LCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMoZmlsZUV4dGVuc2lvbkxpc3QpKSB7XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgaWYgKGZpbGUubmFtZS5zdWJzdHIoZmlsZS5uYW1lLmxlbmd0aCAtIHZhbHVlLmxlbmd0aCwgdmFsdWUubGVuZ3RoKS50b0xvd2VyQ2FzZSgpID09PVxyXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICB2YWx1ZS50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIGhhc1ZhbGlkRmlsZXMgPSB0cnVlO1xyXG4gICAgLy8gICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgIC8vICAgICAgICAgICAgICAgICB9XHJcbiAgICAvLyAgICAgICAgICAgICB9XHJcbiAgICAvLyAgICAgICAgIH1cclxuICAgIC8vICAgICB9KTtcclxuXHJcbiAgICAvLyAgICAgcmV0dXJuIGhhc1ZhbGlkRmlsZXM7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgdXBsb2FkRmlsZXMoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmRhdGEpO1xyXG4gICAgICAgIGlmICh0aGlzLnZhbGlkYXRlRmlsZSgpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVwbG9hZGVkRmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGxvYWRQcm9jZXNzaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY2hlY2tPdG1tU2Vzc2lvbih0cnVlKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY2hlY2tPdG1tU2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldFVwbG9hZFNlcnZpY2Uuc3RhcnRVcGxvYWQodGhpcy51cGxvYWRlZEZpbGVzLCB0aGlzLmRhdGEuZGVsaXZlcmFibGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEuaXNSZXZpc2lvblVwbG9hZCwgdGhpcy5kYXRhLmZvbGRlcklkLCB0aGlzLmRhdGEucHJvamVjdClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1VwbG9hZCBoYXMgYmVlbiBpbml0aWF0ZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkUHJvY2Vzc2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBjaGVja090bW1TZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgT1RNTSBzZXNzaW9uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkUHJvY2Vzc2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdQbGVhc2Ugc2VsZWN0IG9yIGRyYWcgYW5kIGRyb3AgZmlsZXMgdG8gdXBsb2FkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy51cGxvYWRlZEZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdVcGxvYWRlZCBmaWxlIGZvcm1hdCBpcyBub3Qgc3VwcG9ydGVkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnUGxlYXNlIHNlbGVjdCBvciBkcmFnIGFuZCBkcm9wIGZpbGVzIHRvIHVwbG9hZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVJlZmVyZW5jZVR5cGUob3B0aW9uKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFzc2V0Q29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5yZWZlcmVuY2VUeXBlT2JqID0gQXNzZXRDb25zdGFudHMuUkVGRVJFTkNFX1RZUEVTX09CSkVDVFM7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==