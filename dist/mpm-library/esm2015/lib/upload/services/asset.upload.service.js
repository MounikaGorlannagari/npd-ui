import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { AssetConstants } from '../../project/assets/asset_constants';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { QdsService } from './qds.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../mpm-utils/services/entity.appdef.service";
import * as i4 from "../../mpm-utils/services/util.service";
import * as i5 from "../../project/shared/services/category.service";
import * as i6 from "../../shared/services/field-config.service";
import * as i7 from "../../mpm-utils/services/sharing.service";
import * as i8 from "./qds.service";
let AssetUploadService = class AssetUploadService {
    constructor(appService, otmmService, entityAppDefService, utilService, categoryService, fieldConfigService, sharingService, qdsService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.entityAppDefService = entityAppDefService;
        this.utilService = utilService;
        this.categoryService = categoryService;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.qdsService = qdsService;
        this.IMPORT_DELIVERABLE_ASSET_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.IMPORT_DELIVERABLE_ASSET_WS = 'ImportDeliverableAsset';
    }
    constructMetadataFieldValuesFromEventHandler(metadataValues) {
        const metadataFieldValues = [];
        for (const prop of Object.keys(metadataValues)) {
            metadataFieldValues[prop] = metadataValues[prop];
        }
        return metadataFieldValues;
    }
    excludeFileFormat(name) {
        if (name.indexOf('.') !== -1) {
            return name.slice(0, name.lastIndexOf('.'));
        }
        else {
            return name;
        }
    }
    uploadCallBack(deliverable, job, uploadType) {
        return new Observable(observer => {
            if (deliverable && deliverable.deliverableItemId && job && uploadType) {
                const parameters = {
                    operation: uploadType,
                    jobId: job.process_instance_id,
                    applicationId: this.entityAppDefService.getApplicationID(),
                    deliverableDetails: {
                        deliverable: deliverable.deliverableItemId
                    }
                };
                this.appService.invokeRequest(this.IMPORT_DELIVERABLE_ASSET_NS, this.IMPORT_DELIVERABLE_ASSET_WS, parameters)
                    .subscribe(response => {
                    if (response) {
                        observer.next(true);
                        observer.complete();
                    }
                    else {
                        observer.error('Error while triggering Import OTMM Asset BPM.');
                    }
                }, error => {
                    observer.error(error);
                });
            }
            else {
                observer.error('Mandatory parameters are missing to trigger Import OTMM Asset BPM.');
            }
        });
    }
    checkInAsset(filesToCheckIn, JobId, folderId) {
        return new Observable(observer => {
            this.otmmService.assetCheckIn(filesToCheckIn, JobId)
                .subscribe(response => {
                const uploadDetails = {
                    files: filesToCheckIn,
                    folderId: folderId,
                    processId: response && this.utilService.isValid(response['job_handle'].process_instance_id) ?
                        response['job_handle'].process_instance_id : response['job_handle'].job_id
                };
                observer.next(uploadDetails);
            }, error => {
                observer.error(error);
            });
        });
    }
    checkOutAsset(filesToCheckOut) {
        return new Observable(observer => {
            this.otmmService.assetCheckout(filesToCheckOut)
                .subscribe(assetCheckoutResponse => {
                this.otmmService.lockAssets(filesToCheckOut)
                    .subscribe(lockAssetResponse => {
                    observer.next(lockAssetResponse);
                }, lockAssetError => {
                    observer.error(lockAssetError);
                });
            }, assetCheckoutError => {
                observer.error(assetCheckoutError);
            });
        });
    }
    uploadFilesViaHTTP(filesToUpload, eventData, isRevision) {
        return new Observable(observer => {
            this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(createOTMMJobResponse => {
                const jobId = createOTMMJobResponse['job_handle'].job_id;
                this.otmmService.assetsRendition(filesToUpload, jobId).subscribe(res => {
                    this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                        .subscribe(importOTMMJobResponse => {
                        eventData.files = filesToUpload;
                        eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                        if (eventData && eventData.data) {
                            if (eventData.processId && eventData.files) {
                                // this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                                observer.next(eventData);
                            }
                            else {
                                observer.error('Something went wrong while uploading the file(s).');
                            }
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }, importOTMMJobError => {
                        observer.error('Something went wrong while uploading the file(s).');
                    });
                }, createOTMMJobError => {
                    observer.error('Something went wrong while creating jobs to upload the file(s).');
                });
            }, error => {
                console.log(error);
            });
        });
    }
    uploadVersionViaHTTP(filesToUpload, eventData, isRevision) {
        return new Observable(observer => {
            this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(createOTMMJobResponse => {
                const jobId = createOTMMJobResponse['job_handle'].job_id;
                if (isRevision) {
                    filesToUpload[0].assetId = eventData.data.UPLOADED_ASSET_ID;
                }
                this.otmmService.assetsRendition(filesToUpload, jobId).subscribe(res => {
                    this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                        .subscribe(importOTMMJobResponse => {
                        eventData.files = filesToUpload;
                        eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                        if (eventData && eventData.data) {
                            if (eventData.processId && eventData.files) {
                                // this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                                observer.next(eventData);
                            }
                            else {
                                observer.error('Something went wrong while uploading the file(s).');
                            }
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }, importOTMMJobError => {
                        observer.error('Something went wrong while uploading the file(s).');
                    });
                }, createOTMMJobError => {
                    observer.error('Something went wrong while creating jobs to upload the file(s).');
                });
            }, error => {
                console.log(error);
            });
        });
    }
    uploadFilesViaQDS(filesToUpload, eventData, isRevision) {
        return new Observable(observer => {
            this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(createOTMMJobResponse => {
                const jobId = createOTMMJobResponse['job_handle'].job_id;
                this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                    .subscribe(importOTMMJobResponse => {
                    eventData.files = filesToUpload;
                    eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                    if (eventData && eventData.data) {
                        if (eventData.processId && eventData.files) {
                            this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                            observer.next(eventData);
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }
                    else {
                        observer.error('Something went wrong while uploading the file(s).');
                    }
                }, importOTMMJobError => {
                    observer.error('Something went wrong while uploading the file(s).');
                });
            }, createOTMMJobError => {
                observer.error('Something went wrong while creating jobs to upload the file(s).');
            });
        });
    }
    uploadVersionsViaQDS(files, eventData, isRevision) {
        return new Observable(observer => {
            this.otmmService.createOTMMJob(files[0].name)
                .subscribe(createOTMMJobResponse => {
                const jobId = createOTMMJobResponse['job_handle'].job_id;
                if (isRevision) {
                    files[0].assetId = eventData.data.UPLOADED_ASSET_ID;
                }
                this.otmmService.importOTMMJob(files, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                    .subscribe(importOTMMJobResponse => {
                    eventData.files = files;
                    eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                    if (eventData && eventData.data) {
                        if (eventData.processId && eventData.files) {
                            this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                            observer.next(eventData);
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }
                    else {
                        observer.error('Something went wrong while uploading the file(s).');
                    }
                }, importOTMMJobError => {
                    observer.error('Something went wrong while uploading the file(s).');
                });
            }, createOTMMJobError => {
                observer.error('Something went wrong while uploading the file(s).');
            });
        });
    }
    startUpload(files, deliverable, isRevision, folderId, project) {
        return new Observable(observer => {
            const uploadType = isRevision ? 'UPLOAD_REVISION' : 'UPLOAD';
            const metadata = this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.ASSET);
            let formAssetMetadata;
            if (deliverable) {
                deliverable.metadata = this.formAssetMetadataModel(deliverable);
            }
            else {
                formAssetMetadata = this.formAssetMetadataModel(deliverable, project);
            }
            /** Following is used to add the extra metadata update while uploading */
            if (Array.isArray(deliverable.customMetadataFields) && deliverable.metadata &&
                deliverable.metadata.metadata_element_list
                && deliverable.metadata.metadata_element_list[0]
                && deliverable.metadata.metadata_element_list[0].metadata_element_list) {
                deliverable.metadata.metadata_element_list[0].metadata_element_list =
                    deliverable.metadata.metadata_element_list[0].metadata_element_list.concat(deliverable.customMetadataFields);
            }
            const eventData = {
                otmmFieldValues: deliverable ? deliverable.metadata : formAssetMetadata,
                metadataModel: metadata.METADATA_MODEL_ID,
                templateid: metadata.TEMPLATE_ID,
                securityPolicyID: metadata.SECURTIY_POLICY_IDS,
                folderId: deliverable ? deliverable.otmmProjectFolderId : folderId,
                data: deliverable ? deliverable : project
            };
            if (eventData.folderId) {
                if (!isRevision) {
                    if (!otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
                        this.uploadFilesViaHTTP(files, eventData, isRevision)
                            .subscribe(uploadResponse => {
                            const job = {
                                process_instance_id: uploadResponse.processId
                            };
                            if (deliverable) {
                                this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(uploadCallbackResponse => {
                                    observer.next(uploadCallbackResponse);
                                }, uploadCallbackError => {
                                    observer.error(uploadCallbackError);
                                });
                            }
                            else {
                                observer.next(uploadResponse);
                            }
                        }, uploadError => {
                            observer.error(uploadError);
                        });
                    }
                    else {
                        this.uploadFilesViaQDS(files, eventData, isRevision)
                            .subscribe(uploadResponse => {
                            const job = {
                                process_instance_id: uploadResponse.processId
                            };
                            if (deliverable) {
                                this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(uploadCallbackResponse => {
                                    observer.next(uploadCallbackResponse);
                                }, uploadCallbackError => {
                                    observer.error(uploadCallbackError);
                                });
                            }
                            else {
                                observer.next(uploadResponse);
                            }
                        }, uploadError => {
                            observer.error(uploadError);
                        });
                    }
                }
                else {
                    this.checkOutAsset([deliverable.asset.asset_id])
                        .subscribe(checkOutAssetResponse => {
                        if (!otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
                            this.uploadVersionViaHTTP(files, eventData, isRevision)
                                .subscribe(uploadResponse => {
                                const job = {
                                    process_instance_id: uploadResponse.processId
                                };
                                this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(uploadCallbackResponse => {
                                    observer.next(uploadCallbackResponse);
                                }, uploadCallbackError => {
                                    observer.error(uploadCallbackError);
                                });
                            }, uploadError => {
                                observer.error(uploadError);
                            });
                        }
                        else {
                            this.uploadVersionsViaQDS(files, eventData, isRevision)
                                .subscribe(uploadResponse => {
                                const job = {
                                    process_instance_id: uploadResponse.processId
                                };
                                this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(uploadCallbackResponse => {
                                    observer.next(uploadCallbackResponse);
                                }, uploadCallbackError => {
                                    observer.error(uploadCallbackError);
                                });
                            }, uploadError => {
                                observer.error(uploadError);
                            });
                        }
                    }, checkOutAssetError => {
                        observer.error(checkOutAssetError);
                    });
                }
            }
            else {
                observer.error('No folder found to upload the asset(s).');
            }
        });
    }
    formAssetMetadataModel(deliverableData, projectData) {
        const metadata = this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.ASSET);
        const currStatus = this.sharingService.getAllStatusConfig().find(data => {
            return data['STATUS_TYPE'] === StatusTypes.INITIAL && data.R_PO_CATAGORY_LEVEL['MPM_Category_Level-id'].Id === metadata['MPM_Category_Level-id'].Id;
        });
        const projectItemId = projectData ? projectData['Project-id'].ItemId : '';
        this.assetConstants = deliverableData ? AssetConstants.ASSET_UPLOAD_FIELDS : projectData ? AssetConstants.REFERENCE_ASSET_UPLOAD_FIELDS : '';
        const metadataFields = this.assetConstants.map(element => {
            const fieldName = this.fieldConfigService.getIndexerIdByMapperValue(element.mapperName);
            return {
                id: element.id,
                type: 'com.artesia.metadata.MetadataField',
                value: {
                    value: {
                        type: 'string',
                        value: deliverableData[fieldName] ? deliverableData[fieldName] : projectData && fieldName === 'ID' ? projectItemId : ''
                    }
                }
            };
        });
        metadataFields.push({
            id: 'MPM.ASSET.IS_CLONED',
            type: 'com.artesia.metadata.MetadataField',
            value: {
                value: {
                    type: 'string',
                    value: 'false'
                }
            }
        });
        metadataFields.push({
            id: 'MPM.ASSET.IS_REFERENCE_ASSET',
            type: 'com.artesia.metadata.MetadataField',
            value: {
                value: {
                    type: 'string',
                    value: deliverableData ? 'false' : 'true'
                }
            }
        });
        return {
            metadata_element_list: [{
                    metadata_element_list: metadataFields
                }],
            metadata_model_id: 'MPM.ASSET',
            security_policy_list: [
                {
                    id: metadata.SECURTIY_POLICY_IDS
                }
            ]
        };
    }
};
AssetUploadService.ctorParameters = () => [
    { type: AppService },
    { type: OTMMService },
    { type: EntityAppDefService },
    { type: UtilService },
    { type: CategoryService },
    { type: FieldConfigService },
    { type: SharingService },
    { type: QdsService }
];
AssetUploadService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AssetUploadService_Factory() { return new AssetUploadService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.EntityAppDefService), i0.ɵɵinject(i4.UtilService), i0.ɵɵinject(i5.CategoryService), i0.ɵɵinject(i6.FieldConfigService), i0.ɵɵinject(i7.SharingService), i0.ɵɵinject(i8.QdsService)); }, token: AssetUploadService, providedIn: "root" });
AssetUploadService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AssetUploadService);
export { AssetUploadService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQudXBsb2FkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi91cGxvYWQvc2VydmljZXMvYXNzZXQudXBsb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR2hGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsV0FBVyxFQUFnQixNQUFNLG9DQUFvQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUVwRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7O0FBTTNDLElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBQzNCLFlBQ1csVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLFdBQXdCLEVBQ3hCLGVBQWdDLEVBQ2hDLGtCQUFzQyxFQUN0QyxjQUE4QixFQUM5QixVQUFzQjtRQVB0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFHakMsZ0NBQTJCLEdBQUcsc0RBQXNELENBQUM7UUFDckYsZ0NBQTJCLEdBQUcsd0JBQXdCLENBQUM7SUFIbkQsQ0FBQztJQU1MLDRDQUE0QyxDQUFDLGNBQWM7UUFDdkQsTUFBTSxtQkFBbUIsR0FBRyxFQUFFLENBQUM7UUFDL0IsS0FBSyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFO1lBQzVDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwRDtRQUNELE9BQU8sbUJBQW1CLENBQUM7SUFDL0IsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDbEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQy9DO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxFQUFFLFVBQVU7UUFDdkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsaUJBQWlCLElBQUksR0FBRyxJQUFJLFVBQVUsRUFBRTtnQkFDbkUsTUFBTSxVQUFVLEdBQUc7b0JBQ2YsU0FBUyxFQUFFLFVBQVU7b0JBQ3JCLEtBQUssRUFBRSxHQUFHLENBQUMsbUJBQW1CO29CQUM5QixhQUFhLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFO29CQUMxRCxrQkFBa0IsRUFBRTt3QkFDaEIsV0FBVyxFQUFFLFdBQVcsQ0FBQyxpQkFBaUI7cUJBQzdDO2lCQUNKLENBQUM7Z0JBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxVQUFVLENBQUM7cUJBQ3hHLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxRQUFRLEVBQUU7d0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2Qjt5QkFBTTt3QkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7cUJBQ25FO2dCQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQzthQUNWO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsb0VBQW9FLENBQUMsQ0FBQzthQUN4RjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxFQUFFLFFBQVE7UUFDeEMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDO2lCQUMvQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLE1BQU0sYUFBYSxHQUFHO29CQUNsQixLQUFLLEVBQUUsY0FBYztvQkFDckIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFNBQVMsRUFBRSxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDekYsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTTtpQkFDakYsQ0FBQztnQkFFRixRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2pDLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYSxDQUFDLGVBQWU7UUFDekIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUM7aUJBQzFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO2dCQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7cUJBQ3ZDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO29CQUMzQixRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3JDLENBQUMsRUFBRSxjQUFjLENBQUMsRUFBRTtvQkFDaEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtnQkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxVQUFVO1FBQ25ELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDaEQsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sS0FBSyxHQUFHLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDekQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDbEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsRUFDekYsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEtBQUssRUFBRSxVQUFVLENBQUM7eUJBQ2hILFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUMvQixTQUFTLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzt3QkFDaEMsU0FBUyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7d0JBQ2pFLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUU7NEJBQzdCLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxTQUFTLENBQUMsS0FBSyxFQUFFO2dDQUN4QywyR0FBMkc7Z0NBRTNHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NkJBQzVCO2lDQUFNO2dDQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQzs2QkFDdkU7eUJBQ0E7NkJBQU07NEJBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3lCQUN2RTtvQkFDTCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTt3QkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO29CQUN4RSxDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtvQkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO2dCQUN0RixDQUFDLENBQUMsQ0FBQztZQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1FBQ2YsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsYUFBYSxFQUFDLFNBQVMsRUFBQyxVQUFVO1FBQ25ELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDaEQsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0JBQy9CLE1BQU0sS0FBSyxHQUFHLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDekQsSUFBRyxVQUFVLEVBQUM7b0JBQ1YsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2lCQUMvRDtnQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNsRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxFQUN6RixTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBQzt5QkFDaEgsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQy9CLFNBQVMsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO3dCQUNoQyxTQUFTLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQzt3QkFDakUsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksRUFBRTs0QkFDN0IsSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEVBQUU7Z0NBQ3hDLDJHQUEyRztnQ0FFM0csUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzs2QkFDNUI7aUNBQU07Z0NBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDOzZCQUN2RTt5QkFDQTs2QkFBTTs0QkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7eUJBQ3ZFO29CQUNMLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFO3dCQUNwQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7b0JBQ3hFLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFO29CQUNwQixRQUFRLENBQUMsS0FBSyxDQUFDLGlFQUFpRSxDQUFDLENBQUM7Z0JBQ3RGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNmLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLFVBQVU7UUFDbEQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2lCQUNoRCxTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQkFDL0IsTUFBTSxLQUFLLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUN6RCxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxFQUN6RixTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBQztxQkFDaEgsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7b0JBQy9CLFNBQVMsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO29CQUNoQyxTQUFTLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQztvQkFDakUsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksRUFBRTt3QkFDN0IsSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEVBQUU7NEJBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs0QkFDeEcsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDNUI7NkJBQU07NEJBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3lCQUN2RTtxQkFDQTt5QkFBTTt3QkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7cUJBQ3ZFO2dCQUNMLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFO29CQUNwQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7Z0JBQ3hFLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxFQUFFLGtCQUFrQixDQUFDLEVBQUU7Z0JBQ3BCLFFBQVEsQ0FBQyxLQUFLLENBQUMsaUVBQWlFLENBQUMsQ0FBQztZQUN0RixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFvQixDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVTtRQUM3QyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ3hDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO2dCQUMvQixNQUFNLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pELElBQUksVUFBVSxFQUFFO29CQUNaLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztpQkFDdkQ7Z0JBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsRUFDakYsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEtBQUssRUFBRSxVQUFVLENBQUM7cUJBQ2hILFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO29CQUMvQixTQUFTLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztvQkFDeEIsU0FBUyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQ2pFLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUU7d0JBQzdCLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxTQUFTLENBQUMsS0FBSyxFQUFFOzRCQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7NEJBQ3hHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQzVCOzZCQUFNOzRCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQzt5QkFDdkU7cUJBQ0o7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3FCQUN2RTtnQkFDTCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtvQkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO2dCQUN4RSxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFO2dCQUNwQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7WUFDeEUsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBYyxFQUFFLE9BQWE7UUFDckUsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFVBQVUsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDN0QsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEYsSUFBSSxpQkFBaUIsQ0FBQztZQUN0QixJQUFJLFdBQVcsRUFBRTtnQkFDYixXQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNuRTtpQkFBTTtnQkFDSCxpQkFBaUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ3pFO1lBRUQseUVBQXlFO1lBQ3pFLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsSUFBSSxXQUFXLENBQUMsUUFBUTtnQkFDdkUsV0FBVyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUI7bUJBQ3ZDLFdBQVcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO21CQUM3QyxXQUFXLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixFQUFFO2dCQUN4RSxXQUFXLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQjtvQkFDL0QsV0FBVyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUM7YUFDcEg7WUFFRCxNQUFNLFNBQVMsR0FBRztnQkFDZCxlQUFlLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxpQkFBaUI7Z0JBQ3ZFLGFBQWEsRUFBRSxRQUFRLENBQUMsaUJBQWlCO2dCQUN6QyxVQUFVLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQ2hDLGdCQUFnQixFQUFFLFFBQVEsQ0FBQyxtQkFBbUI7Z0JBQzlDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsUUFBUTtnQkFDbEUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxPQUFPO2FBQzVDLENBQUM7WUFDRixJQUFJLFNBQVMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRTt3QkFDM0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDOzZCQUNoRCxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7NEJBQ3hCLE1BQU0sR0FBRyxHQUFHO2dDQUNSLG1CQUFtQixFQUFFLGNBQWMsQ0FBQyxTQUFTOzZCQUNoRCxDQUFDOzRCQUNGLElBQUksV0FBVyxFQUFFO2dDQUNiLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO3FDQUNwRCxTQUFTLENBQUMsc0JBQXNCLENBQUMsRUFBRTtvQ0FDaEMsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dDQUMxQyxDQUFDLEVBQUUsbUJBQW1CLENBQUMsRUFBRTtvQ0FDckIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUN4QyxDQUFDLENBQUMsQ0FBQzs2QkFDVjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzZCQUNqQzt3QkFDTCxDQUFDLEVBQUUsV0FBVyxDQUFDLEVBQUU7NEJBQ2IsUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDaEMsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDOzZCQUMvQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7NEJBQ3hCLE1BQU0sR0FBRyxHQUFHO2dDQUNSLG1CQUFtQixFQUFFLGNBQWMsQ0FBQyxTQUFTOzZCQUNoRCxDQUFDOzRCQUNGLElBQUksV0FBVyxFQUFFO2dDQUNiLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO3FDQUNwRCxTQUFTLENBQUMsc0JBQXNCLENBQUMsRUFBRTtvQ0FDaEMsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dDQUMxQyxDQUFDLEVBQUUsbUJBQW1CLENBQUMsRUFBRTtvQ0FDckIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUN4QyxDQUFDLENBQUMsQ0FBQzs2QkFDVjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzZCQUNqQzt3QkFDTCxDQUFDLEVBQUUsV0FBVyxDQUFDLEVBQUU7NEJBQ2IsUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDaEMsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQzNDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUMvQixJQUFJLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFOzRCQUMzRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUM7aUNBQ2xELFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQ0FDeEIsTUFBTSxHQUFHLEdBQUc7b0NBQ1IsbUJBQW1CLEVBQUUsY0FBYyxDQUFDLFNBQVM7aUNBQ2hELENBQUM7Z0NBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUM7cUNBQ3BELFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO29DQUNoQyxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0NBQzFDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxFQUFFO29DQUNyQixRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0NBQ3hDLENBQUMsQ0FBQyxDQUFDOzRCQUNYLENBQUMsRUFBRSxXQUFXLENBQUMsRUFBRTtnQ0FDYixRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNoQyxDQUFDLENBQUMsQ0FBQzt5QkFDVjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUM7aUNBQ2xELFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQ0FDeEIsTUFBTSxHQUFHLEdBQUc7b0NBQ1IsbUJBQW1CLEVBQUUsY0FBYyxDQUFDLFNBQVM7aUNBQ2hELENBQUM7Z0NBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUM7cUNBQ3BELFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO29DQUNoQyxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0NBQzFDLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxFQUFFO29DQUNyQixRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0NBQ3hDLENBQUMsQ0FBQyxDQUFDOzRCQUNYLENBQUMsRUFBRSxXQUFXLENBQUMsRUFBRTtnQ0FDYixRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNoQyxDQUFDLENBQUMsQ0FBQzt5QkFDVjtvQkFDTCxDQUFDLEVBQUUsa0JBQWtCLENBQUMsRUFBRTt3QkFDcEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2QyxDQUFDLENBQUMsQ0FBQztpQkFDVjthQUNKO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQzthQUM3RDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELHNCQUFzQixDQUFDLGVBQWUsRUFBRSxXQUFpQjtRQUNyRCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3BFLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN4SixDQUFDLENBQUMsQ0FBQztRQUNILE1BQU0sYUFBYSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFFLElBQUksQ0FBQyxjQUFjLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDN0ksTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDckQsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4RixPQUFPO2dCQUNILEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsb0NBQW9DO2dCQUMxQyxLQUFLLEVBQUU7b0JBQ0gsS0FBSyxFQUFFO3dCQUNILElBQUksRUFBRSxRQUFRO3dCQUNkLEtBQUssRUFBRSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLFNBQVMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRTtxQkFDMUg7aUJBQ0o7YUFDSixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDSCxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ2hCLEVBQUUsRUFBRSxxQkFBcUI7WUFDekIsSUFBSSxFQUFFLG9DQUFvQztZQUMxQyxLQUFLLEVBQUU7Z0JBQ0gsS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxPQUFPO2lCQUNqQjthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsY0FBYyxDQUFDLElBQUksQ0FBQztZQUNoQixFQUFFLEVBQUUsOEJBQThCO1lBQ2xDLElBQUksRUFBRSxvQ0FBb0M7WUFDMUMsS0FBSyxFQUFFO2dCQUNILEtBQUssRUFBRTtvQkFDSCxJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsZUFBZSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU07aUJBQzVDO2FBQ0o7U0FDSixDQUFDLENBQUM7UUFFSCxPQUFPO1lBQ0gscUJBQXFCLEVBQUUsQ0FBQztvQkFDcEIscUJBQXFCLEVBQUUsY0FBYztpQkFDeEMsQ0FBQztZQUNGLGlCQUFpQixFQUFFLFdBQVc7WUFDOUIsb0JBQW9CLEVBQUU7Z0JBQ2xCO29CQUNJLEVBQUUsRUFBRSxRQUFRLENBQUMsbUJBQW1CO2lCQUNuQzthQUNKO1NBQ0osQ0FBQztJQUNOLENBQUM7Q0FFSixDQUFBOztZQXZZMEIsVUFBVTtZQUNULFdBQVc7WUFDSCxtQkFBbUI7WUFDM0IsV0FBVztZQUNQLGVBQWU7WUFDWixrQkFBa0I7WUFDdEIsY0FBYztZQUNsQixVQUFVOzs7QUFUeEIsa0JBQWtCO0lBSjlCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxrQkFBa0IsQ0F5WTlCO1NBellZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9jYXRlZ29yeS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzLCBTdGF0dXNMZXZlbHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgQXNzZXRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9wcm9qZWN0L2Fzc2V0cy9hc3NldF9jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgc3Vic2NyaWJlT24gfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuL3Fkcy5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFzc2V0VXBsb2FkU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNhdGVnb3J5U2VydmljZTogQ2F0ZWdvcnlTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIElNUE9SVF9ERUxJVkVSQUJMRV9BU1NFVF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIElNUE9SVF9ERUxJVkVSQUJMRV9BU1NFVF9XUyA9ICdJbXBvcnREZWxpdmVyYWJsZUFzc2V0JztcclxuXHJcbiAgICBhc3NldENvbnN0YW50cztcclxuICAgIGNvbnN0cnVjdE1ldGFkYXRhRmllbGRWYWx1ZXNGcm9tRXZlbnRIYW5kbGVyKG1ldGFkYXRhVmFsdWVzKSB7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGFGaWVsZFZhbHVlcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgcHJvcCBvZiBPYmplY3Qua2V5cyhtZXRhZGF0YVZhbHVlcykpIHtcclxuICAgICAgICAgICAgbWV0YWRhdGFGaWVsZFZhbHVlc1twcm9wXSA9IG1ldGFkYXRhVmFsdWVzW3Byb3BdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbWV0YWRhdGFGaWVsZFZhbHVlcztcclxuICAgIH1cclxuXHJcbiAgICBleGNsdWRlRmlsZUZvcm1hdChuYW1lKSB7XHJcbiAgICAgICAgaWYgKG5hbWUuaW5kZXhPZignLicpICE9PSAtMSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmFtZS5zbGljZSgwLCBuYW1lLmxhc3RJbmRleE9mKCcuJykpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1cGxvYWRDYWxsQmFjayhkZWxpdmVyYWJsZSwgam9iLCB1cGxvYWRUeXBlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGVsaXZlcmFibGUgJiYgZGVsaXZlcmFibGUuZGVsaXZlcmFibGVJdGVtSWQgJiYgam9iICYmIHVwbG9hZFR5cGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3BlcmF0aW9uOiB1cGxvYWRUeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIGpvYklkOiBqb2IucHJvY2Vzc19pbnN0YW5jZV9pZCxcclxuICAgICAgICAgICAgICAgICAgICBhcHBsaWNhdGlvbklkOiB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0QXBwbGljYXRpb25JRCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlRGV0YWlsczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZTogZGVsaXZlcmFibGUuZGVsaXZlcmFibGVJdGVtSWRcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuSU1QT1JUX0RFTElWRVJBQkxFX0FTU0VUX05TLCB0aGlzLklNUE9SVF9ERUxJVkVSQUJMRV9BU1NFVF9XUywgcGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdFcnJvciB3aGlsZSB0cmlnZ2VyaW5nIEltcG9ydCBPVE1NIEFzc2V0IEJQTS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ01hbmRhdG9yeSBwYXJhbWV0ZXJzIGFyZSBtaXNzaW5nIHRvIHRyaWdnZXIgSW1wb3J0IE9UTU0gQXNzZXQgQlBNLicpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tJbkFzc2V0KGZpbGVzVG9DaGVja0luLCBKb2JJZCwgZm9sZGVySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuYXNzZXRDaGVja0luKGZpbGVzVG9DaGVja0luLCBKb2JJZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVwbG9hZERldGFpbHMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVzOiBmaWxlc1RvQ2hlY2tJbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9sZGVySWQ6IGZvbGRlcklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9jZXNzSWQ6IHJlc3BvbnNlICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChyZXNwb25zZVsnam9iX2hhbmRsZSddLnByb2Nlc3NfaW5zdGFuY2VfaWQpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlWydqb2JfaGFuZGxlJ10ucHJvY2Vzc19pbnN0YW5jZV9pZCA6IHJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1cGxvYWREZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja091dEFzc2V0KGZpbGVzVG9DaGVja091dCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5hc3NldENoZWNrb3V0KGZpbGVzVG9DaGVja091dClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoYXNzZXRDaGVja291dFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmxvY2tBc3NldHMoZmlsZXNUb0NoZWNrT3V0KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGxvY2tBc3NldFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQobG9ja0Fzc2V0UmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBsb2NrQXNzZXRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihsb2NrQXNzZXRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSwgYXNzZXRDaGVja291dEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihhc3NldENoZWNrb3V0RXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGxvYWRGaWxlc1ZpYUhUVFAoZmlsZXNUb1VwbG9hZCwgZXZlbnREYXRhLCBpc1JldmlzaW9uKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmNyZWF0ZU9UTU1Kb2IoZmlsZXNUb1VwbG9hZFswXS5uYW1lKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShjcmVhdGVPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGpvYklkID0gY3JlYXRlT1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuYXNzZXRzUmVuZGl0aW9uKGZpbGVzVG9VcGxvYWQsam9iSWQpLnN1YnNjcmliZShyZXMgPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuaW1wb3J0T1RNTUpvYihmaWxlc1RvVXBsb2FkLCBldmVudERhdGEub3RtbUZpZWxkcywgZXZlbnREYXRhLm90bW1GaWVsZFZhbHVlcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5mb2xkZXJJZCwgZXZlbnREYXRhLnRlbXBsYXRlaWQsIGV2ZW50RGF0YS5tZXRhZGF0YU1vZGVsLCBldmVudERhdGEuc2VjdXJpdHlQb2xpY3lJRCwgam9iSWQsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGltcG9ydE9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZpbGVzID0gZmlsZXNUb1VwbG9hZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEucHJvY2Vzc0lkID0gaW1wb3J0T1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudERhdGEgJiYgZXZlbnREYXRhLmRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YS5wcm9jZXNzSWQgJiYgZXZlbnREYXRhLmZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnFkc1NlcnZpY2UuY3JlYXRlUURTSW1wb3J0Sm9iKGV2ZW50RGF0YS5wcm9jZXNzSWQsIHRoaXMub3RtbVNlcnZpY2UuZ2V0RmlsZVBhdGhzKGV2ZW50RGF0YS5maWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgaW1wb3J0T1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgY3JlYXRlT1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGNyZWF0aW5nIGpvYnMgdG8gdXBsb2FkIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZFZlcnNpb25WaWFIVFRQKGZpbGVzVG9VcGxvYWQsZXZlbnREYXRhLGlzUmV2aXNpb24pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY3JlYXRlT1RNTUpvYihmaWxlc1RvVXBsb2FkWzBdLm5hbWUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZU9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iSWQgPSBjcmVhdGVPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoaXNSZXZpc2lvbil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVzVG9VcGxvYWRbMF0uYXNzZXRJZCA9IGV2ZW50RGF0YS5kYXRhLlVQTE9BREVEX0FTU0VUX0lEO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmFzc2V0c1JlbmRpdGlvbihmaWxlc1RvVXBsb2FkLGpvYklkKS5zdWJzY3JpYmUocmVzID0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmltcG9ydE9UTU1Kb2IoZmlsZXNUb1VwbG9hZCwgZXZlbnREYXRhLm90bW1GaWVsZHMsIGV2ZW50RGF0YS5vdG1tRmllbGRWYWx1ZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEuZm9sZGVySWQsIGV2ZW50RGF0YS50ZW1wbGF0ZWlkLCBldmVudERhdGEubWV0YWRhdGFNb2RlbCwgZXZlbnREYXRhLnNlY3VyaXR5UG9saWN5SUQsIGpvYklkLCBpc1JldmlzaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShpbXBvcnRPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5maWxlcyA9IGZpbGVzVG9VcGxvYWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLnByb2Nlc3NJZCA9IGltcG9ydE9UTU1Kb2JSZXNwb25zZVsnam9iX2hhbmRsZSddLmpvYl9pZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudERhdGEucHJvY2Vzc0lkICYmIGV2ZW50RGF0YS5maWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5xZHNTZXJ2aWNlLmNyZWF0ZVFEU0ltcG9ydEpvYihldmVudERhdGEucHJvY2Vzc0lkLCB0aGlzLm90bW1TZXJ2aWNlLmdldEZpbGVQYXRocyhldmVudERhdGEuZmlsZXMpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGltcG9ydE9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGNyZWF0ZU9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBjcmVhdGluZyBqb2JzIHRvIHVwbG9hZCB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZEZpbGVzVmlhUURTKGZpbGVzVG9VcGxvYWQsIGV2ZW50RGF0YSwgaXNSZXZpc2lvbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jcmVhdGVPVE1NSm9iKGZpbGVzVG9VcGxvYWRbMF0ubmFtZSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JlYXRlT1RNTUpvYlJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2JJZCA9IGNyZWF0ZU9UTU1Kb2JSZXNwb25zZVsnam9iX2hhbmRsZSddLmpvYl9pZDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmltcG9ydE9UTU1Kb2IoZmlsZXNUb1VwbG9hZCwgZXZlbnREYXRhLm90bW1GaWVsZHMsIGV2ZW50RGF0YS5vdG1tRmllbGRWYWx1ZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5mb2xkZXJJZCwgZXZlbnREYXRhLnRlbXBsYXRlaWQsIGV2ZW50RGF0YS5tZXRhZGF0YU1vZGVsLCBldmVudERhdGEuc2VjdXJpdHlQb2xpY3lJRCwgam9iSWQsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoaW1wb3J0T1RNTUpvYlJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5maWxlcyA9IGZpbGVzVG9VcGxvYWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEucHJvY2Vzc0lkID0gaW1wb3J0T1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YSAmJiBldmVudERhdGEuZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudERhdGEucHJvY2Vzc0lkICYmIGV2ZW50RGF0YS5maWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnFkc1NlcnZpY2UuY3JlYXRlUURTSW1wb3J0Sm9iKGV2ZW50RGF0YS5wcm9jZXNzSWQsIHRoaXMub3RtbVNlcnZpY2UuZ2V0RmlsZVBhdGhzKGV2ZW50RGF0YS5maWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBpbXBvcnRPVE1NSm9iRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCBjcmVhdGVPVE1NSm9iRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBjcmVhdGluZyBqb2JzIHRvIHVwbG9hZCB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZFZlcnNpb25zVmlhUURTKGZpbGVzLCBldmVudERhdGEsIGlzUmV2aXNpb24pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY3JlYXRlT1RNTUpvYihmaWxlc1swXS5uYW1lKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShjcmVhdGVPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGpvYklkID0gY3JlYXRlT1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc1JldmlzaW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVzWzBdLmFzc2V0SWQgPSBldmVudERhdGEuZGF0YS5VUExPQURFRF9BU1NFVF9JRDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5pbXBvcnRPVE1NSm9iKGZpbGVzLCBldmVudERhdGEub3RtbUZpZWxkcywgZXZlbnREYXRhLm90bW1GaWVsZFZhbHVlcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZvbGRlcklkLCBldmVudERhdGEudGVtcGxhdGVpZCwgZXZlbnREYXRhLm1ldGFkYXRhTW9kZWwsIGV2ZW50RGF0YS5zZWN1cml0eVBvbGljeUlELCBqb2JJZCwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShpbXBvcnRPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZpbGVzID0gZmlsZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEucHJvY2Vzc0lkID0gaW1wb3J0T1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YSAmJiBldmVudERhdGEuZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudERhdGEucHJvY2Vzc0lkICYmIGV2ZW50RGF0YS5maWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnFkc1NlcnZpY2UuY3JlYXRlUURTSW1wb3J0Sm9iKGV2ZW50RGF0YS5wcm9jZXNzSWQsIHRoaXMub3RtbVNlcnZpY2UuZ2V0RmlsZVBhdGhzKGV2ZW50RGF0YS5maWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGltcG9ydE9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0sIGNyZWF0ZU9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0VXBsb2FkKGZpbGVzLCBkZWxpdmVyYWJsZSwgaXNSZXZpc2lvbiwgZm9sZGVySWQ/OiBhbnksIHByb2plY3Q/OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVwbG9hZFR5cGUgPSBpc1JldmlzaW9uID8gJ1VQTE9BRF9SRVZJU0lPTicgOiAnVVBMT0FEJztcclxuICAgICAgICAgICAgY29uc3QgbWV0YWRhdGEgPSB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeUxldmVsRGV0YWlsc0J5VHlwZShNUE1fTEVWRUxTLkFTU0VUKTtcclxuICAgICAgICAgICAgbGV0IGZvcm1Bc3NldE1ldGFkYXRhO1xyXG4gICAgICAgICAgICBpZiAoZGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLm1ldGFkYXRhID0gdGhpcy5mb3JtQXNzZXRNZXRhZGF0YU1vZGVsKGRlbGl2ZXJhYmxlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvcm1Bc3NldE1ldGFkYXRhID0gdGhpcy5mb3JtQXNzZXRNZXRhZGF0YU1vZGVsKGRlbGl2ZXJhYmxlLCBwcm9qZWN0KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLyoqIEZvbGxvd2luZyBpcyB1c2VkIHRvIGFkZCB0aGUgZXh0cmEgbWV0YWRhdGEgdXBkYXRlIHdoaWxlIHVwbG9hZGluZyAqL1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShkZWxpdmVyYWJsZS5jdXN0b21NZXRhZGF0YUZpZWxkcykgJiYgZGVsaXZlcmFibGUubWV0YWRhdGEgJiZcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFxyXG4gICAgICAgICAgICAgICAgJiYgZGVsaXZlcmFibGUubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0WzBdXHJcbiAgICAgICAgICAgICAgICAmJiBkZWxpdmVyYWJsZS5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZS5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0ID1cclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZS5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0LmNvbmNhdChkZWxpdmVyYWJsZS5jdXN0b21NZXRhZGF0YUZpZWxkcyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIG90bW1GaWVsZFZhbHVlczogZGVsaXZlcmFibGUgPyBkZWxpdmVyYWJsZS5tZXRhZGF0YSA6IGZvcm1Bc3NldE1ldGFkYXRhLFxyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFNb2RlbDogbWV0YWRhdGEuTUVUQURBVEFfTU9ERUxfSUQsXHJcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZWlkOiBtZXRhZGF0YS5URU1QTEFURV9JRCxcclxuICAgICAgICAgICAgICAgIHNlY3VyaXR5UG9saWN5SUQ6IG1ldGFkYXRhLlNFQ1VSVElZX1BPTElDWV9JRFMsXHJcbiAgICAgICAgICAgICAgICBmb2xkZXJJZDogZGVsaXZlcmFibGUgPyBkZWxpdmVyYWJsZS5vdG1tUHJvamVjdEZvbGRlcklkIDogZm9sZGVySWQsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkZWxpdmVyYWJsZSA/IGRlbGl2ZXJhYmxlIDogcHJvamVjdFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBpZiAoZXZlbnREYXRhLmZvbGRlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWlzUmV2aXNpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIW90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmlzUURTVXBsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkRmlsZXNWaWFIVFRQKGZpbGVzLCBldmVudERhdGEsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVwbG9hZFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2IgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfaW5zdGFuY2VfaWQ6IHVwbG9hZFJlc3BvbnNlLnByb2Nlc3NJZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkQ2FsbEJhY2sodXBsb2FkUmVzcG9uc2UuZGF0YSwgam9iLCB1cGxvYWRUeXBlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVwbG9hZENhbGxiYWNrUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXBsb2FkQ2FsbGJhY2tFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkQ2FsbGJhY2tFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVwbG9hZFJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRGaWxlc1ZpYVFEUyhmaWxlcywgZXZlbnREYXRhLCBpc1JldmlzaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9jZXNzX2luc3RhbmNlX2lkOiB1cGxvYWRSZXNwb25zZS5wcm9jZXNzSWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZENhbGxCYWNrKHVwbG9hZFJlc3BvbnNlLmRhdGEsIGpvYiwgdXBsb2FkVHlwZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVwbG9hZENhbGxiYWNrRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZENhbGxiYWNrRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1cGxvYWRSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXBsb2FkRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja091dEFzc2V0KFtkZWxpdmVyYWJsZS5hc3NldC5hc3NldF9pZF0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY2hlY2tPdXRBc3NldFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZFZlcnNpb25WaWFIVFRQKGZpbGVzLCBldmVudERhdGEsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfaW5zdGFuY2VfaWQ6IHVwbG9hZFJlc3BvbnNlLnByb2Nlc3NJZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkQ2FsbEJhY2sodXBsb2FkUmVzcG9uc2UuZGF0YSwgam9iLCB1cGxvYWRUeXBlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXBsb2FkQ2FsbGJhY2tFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZENhbGxiYWNrRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcih1cGxvYWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZFZlcnNpb25zVmlhUURTKGZpbGVzLCBldmVudERhdGEsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfaW5zdGFuY2VfaWQ6IHVwbG9hZFJlc3BvbnNlLnByb2Nlc3NJZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkQ2FsbEJhY2sodXBsb2FkUmVzcG9uc2UuZGF0YSwgam9iLCB1cGxvYWRUeXBlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXBsb2FkQ2FsbGJhY2tFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZENhbGxiYWNrRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcih1cGxvYWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBjaGVja091dEFzc2V0RXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoY2hlY2tPdXRBc3NldEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignTm8gZm9sZGVyIGZvdW5kIHRvIHVwbG9hZCB0aGUgYXNzZXQocykuJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGZvcm1Bc3NldE1ldGFkYXRhTW9kZWwoZGVsaXZlcmFibGVEYXRhLCBwcm9qZWN0RGF0YT86IGFueSkge1xyXG4gICAgICAgIGNvbnN0IG1ldGFkYXRhID0gdGhpcy5jYXRlZ29yeVNlcnZpY2UuZ2V0Q2F0ZWdvcnlMZXZlbERldGFpbHNCeVR5cGUoTVBNX0xFVkVMUy5BU1NFVCk7XHJcbiAgICAgICAgY29uc3QgY3VyclN0YXR1cyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsU3RhdHVzQ29uZmlnKCkuZmluZChkYXRhID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGFbJ1NUQVRVU19UWVBFJ10gPT09IFN0YXR1c1R5cGVzLklOSVRJQUwgJiYgZGF0YS5SX1BPX0NBVEFHT1JZX0xFVkVMWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXS5JZCA9PT0gbWV0YWRhdGFbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnN0IHByb2plY3RJdGVtSWQgPSBwcm9qZWN0RGF0YSA/IHByb2plY3REYXRhWydQcm9qZWN0LWlkJ10uSXRlbUlkIDogJyc7XHJcbiAgICAgICAgdGhpcy5hc3NldENvbnN0YW50cyA9IGRlbGl2ZXJhYmxlRGF0YSA/IEFzc2V0Q29uc3RhbnRzLkFTU0VUX1VQTE9BRF9GSUVMRFMgOiBwcm9qZWN0RGF0YSA/IEFzc2V0Q29uc3RhbnRzLlJFRkVSRU5DRV9BU1NFVF9VUExPQURfRklFTERTIDogJyc7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGFGaWVsZHMgPSB0aGlzLmFzc2V0Q29uc3RhbnRzLm1hcChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZmllbGROYW1lID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlcklkQnlNYXBwZXJWYWx1ZShlbGVtZW50Lm1hcHBlck5hbWUpO1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgaWQ6IGVsZW1lbnQuaWQsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFGaWVsZCcsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogZGVsaXZlcmFibGVEYXRhW2ZpZWxkTmFtZV0gPyBkZWxpdmVyYWJsZURhdGFbZmllbGROYW1lXSA6IHByb2plY3REYXRhICYmIGZpZWxkTmFtZSA9PT0gJ0lEJyA/IHByb2plY3RJdGVtSWQgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBtZXRhZGF0YUZpZWxkcy5wdXNoKHtcclxuICAgICAgICAgICAgaWQ6ICdNUE0uQVNTRVQuSVNfQ0xPTkVEJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLm1ldGFkYXRhLk1ldGFkYXRhRmllbGQnLFxyXG4gICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ2ZhbHNlJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbWV0YWRhdGFGaWVsZHMucHVzaCh7XHJcbiAgICAgICAgICAgIGlkOiAnTVBNLkFTU0VULklTX1JFRkVSRU5DRV9BU1NFVCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5tZXRhZGF0YS5NZXRhZGF0YUZpZWxkJyxcclxuICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGRlbGl2ZXJhYmxlRGF0YSA/ICdmYWxzZScgOiAndHJ1ZSdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBtZXRhZGF0YV9lbGVtZW50X2xpc3Q6IFt7XHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YV9lbGVtZW50X2xpc3Q6IG1ldGFkYXRhRmllbGRzXHJcbiAgICAgICAgICAgIH1dLFxyXG4gICAgICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogJ01QTS5BU1NFVCcsXHJcbiAgICAgICAgICAgIHNlY3VyaXR5X3BvbGljeV9saXN0OiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IG1ldGFkYXRhLlNFQ1VSVElZX1BPTElDWV9JRFNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==