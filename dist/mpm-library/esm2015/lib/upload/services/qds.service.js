import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { GloabalConfig } from '../../mpm-utils/config/config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../mpm-utils/services/entity.appdef.service";
import * as i3 from "../../notification/notification.service";
let QdsService = class QdsService {
    constructor(http, entityAppDefService, notificationService) {
        this.http = http;
        this.entityAppDefService = entityAppDefService;
        this.notificationService = notificationService;
        this.inlineDownload = function (downloadParams) {
            downloadParams = downloadParams || {};
            //var notification = downloadParams.notification;
            var resolve, reject;
            var self = this;
            var inlineDownloadPromise = new Promise(function (res, rej) {
                resolve = res;
                reject = rej;
            });
            self.notificationService.info("Files are being downloaded via QDS client, Check the download status in file transfer window");
            this._qdsConnect.createDownloadJob(downloadParams.assetObjects, self.postInlineDownload.bind(this, resolve, reject));
            inlineDownloadPromise.then(function (downloadJob) {
                downloadJob.startDownload(undefined, undefined, self.downloadError.bind(self));
                downloadJob.detach();
                resolve();
            });
            return inlineDownloadPromise;
        };
    }
    getQDSConnect() {
        return this._qdsConnect;
    }
    setQDSConnect(qdsConnect) {
        this._qdsConnect = qdsConnect;
    }
    getQDSSession(userSessionId) {
        return new Observable(observer => {
            const baseUrl = isDevMode() ? './' : this.entityAppDefService.getOtmmAssetUploadDetails().url;
            const url = baseUrl + otmmServicesConstants.otmmapiBaseUrl +
                this.entityAppDefService.getOtmmAssetUploadDetails().apiVersion + '/' + otmmServicesConstants.qdsSessionUrl;
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-OTMM-Locale': 'en_US',
                    'X-Requested-By': userSessionId.toString(),
                    'X-Requested-With': 'XMLHttpRequest'
                })
            };
            this.http.post(url, null, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    connectToQDS(qdsResource, isUpload) {
        return new Observable(observer => {
            const that = this;
            if (qds_otmm.isClientConnected && !isUpload) { //
                otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                console.log('QDS client already connected.');
                observer.next('1000');
                observer.complete();
            }
            else {
                if (GloabalConfig.config.qdsVersion === '1.5.4') {
                    qds_otmm.qdsConnect(this.entityAppDefService.getOtmmAssetUploadDetails().qdsServerURL, qdsResource.authentication_token, function (qdsConnector, statusCode, msg, installPref) {
                        that.setQDSConnect(qdsConnector);
                        if (statusCode === '200' && qdsConnector) {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            that.notificationService.info('Connected to QDS, All the uploads/downloads will use QDS tranfer');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1000') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            console.log('QDS client already connected');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1001' || statusCode === '1002' || statusCode === '1008') {
                            that.notificationService.error(`Unable to connect to QDS,
                      All the uploads/downloads will use basic tranfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1003' || statusCode === '1007') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.warn(`You have chosen not to install QDS Transfer Manager plugin,
                      All the uploads/downloads will use basic transfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1005') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.error(`An unexpexted error occurred while connecting to QDS,
                      All the uploads/downloads will use basic tranfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1006') {
                            that.notificationService.warn('QDS client requires update');
                            observer.next(statusCode);
                            observer.complete();
                        }
                    }, qdsResource.user_id, false);
                    observer.next('1000');
                    observer.complete();
                }
                else if (GloabalConfig.config.qdsVersion === '1.3.0') {
                    qds_otmm.qdsConnect(this.entityAppDefService.getOtmmAssetUploadDetails().qdsServerURL, qdsResource.authentication_token, function (qdsConnector, statusCode) {
                        that.setQDSConnect(qdsConnector);
                        if (statusCode === '200' && qdsConnector) {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            that.notificationService.info('Connected to QDS, All the uploads/downloads will use QDS tranfer');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1000') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            console.log('QDS client already connected');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1001' || statusCode === '1002') {
                            that.notificationService.error(`Unable to connect to QDS,
                          Hence forth all the uploads/downloads will use basic tranfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1003') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.warn(`You have chosen not to install QDS Transfer Manager plugin,
                          All the uploads/downloads will use basic transfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1005') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.error(`An unexpexted error occurred while connecting to QDS,
                          All the uploads/downloads will use basic tranfer`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1006') {
                            that.notificationService.warn(`QDS client requires update`);
                            observer.next(statusCode);
                            observer.complete();
                        }
                    }, qdsResource.user_id);
                }
            }
        });
    }
    getJobs(includeCompleted) {
        return new Observable(observer => {
            qds_otmm.connector.listJobs(function (jobs) {
                observer.next(jobs);
                observer.complete();
            }, includeCompleted);
        });
    }
    createQDSImportJob(jobId, filePaths) {
        qds_otmm.connector.createImportJob('IMPORT_WORKING_AREA', jobId, function (currentJob) {
            currentJob.startImport();
            currentJob.detach();
        }, filePaths);
    }
    toggleQDSImportJob(jobId, toggleToState) {
        return new Observable(observer => {
            qds_otmm.connector.reloadImportJob(jobId, function (response) {
                const importJob = response.importJob;
                const isRunning = response.jobInfo.running;
                if (toggleToState === 'PAUSE') {
                    if (isRunning) {
                        importJob.detach();
                        importJob.pauseImport(function (status) {
                            observer.next(status);
                            observer.complete();
                        });
                    }
                    else {
                        const job_status = {
                            'status': 'job paused'
                        };
                    }
                }
                else {
                    importJob.startImport(function (name, status) {
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        if (status === undefined) {
                            status = 'COMPLETE';
                        }
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        observer.next(status);
                        observer.complete();
                    });
                }
            });
        });
    }
    toggleQDSExportJob(jobId, toggleToState) {
        return new Observable(observer => {
            qds_otmm.connector.reloadExportJob(jobId, function (response) {
                const exportJob = response.exportJob;
                const isRunning = response.jobInfo.running;
                if (toggleToState === 'PAUSE') {
                    if (isRunning) {
                        exportJob.detach();
                        exportJob.pauseExport(function (status) {
                            observer.next(status);
                            observer.complete();
                        });
                    }
                    else {
                        const job_status = {
                            'status': 'job paused'
                        };
                    }
                }
                else {
                    exportJob.startExport(function (name, status) {
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        if (status === undefined) {
                            status = 'COMPLETE';
                        }
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        observer.next(status);
                        observer.complete();
                    });
                }
            });
        });
    }
    retryFailedJob(job) {
        return new Observable(observer => {
            const isImportJob = job.isImport;
            const currJobId = {
                'otmmJobId': job.otmmJobId,
                'qdsJobId': job.qdsJobId
            };
            if (isImportJob) {
                qds_otmm.connector.reloadImportJob(currJobId, function (response) {
                    const currentJob = response.importJob;
                    currentJob.detach();
                    currentJob.startImport();
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                qds_otmm.connector.reloadExportJob(currJobId, function (response) {
                    const currentJob = response.exportJob;
                    currentJob.detach();
                    currentJob.startExport();
                    observer.next(true);
                    observer.complete();
                });
            }
        });
    }
    download(jobId, files) {
        return new Observable(observer => {
            qds_otmm.connector.createExportJob('DEFAULT_EXPORT_AREA', jobId, function (exportJob) {
                exportJob.startExport(undefined, undefined, function () {
                    observer.next('Unable to download files via QDS.');
                });
                exportJob.detach();
                observer.next('Files are being downloaded via QDS client. Check the download status in file transfer window.');
                observer.complete();
            }, files);
        });
    }
    getDownloadFolder() {
        return new Observable(observer => {
            qds_otmm.connector.getDownloadFolder(function (downloadLocation) {
                observer.next(downloadLocation);
                observer.complete();
            });
        });
    }
    chooseDownloadFolder() {
        return new Observable(observer => {
            qds_otmm.connector.chooseDownloadFolder(function (downloadLocation) {
                observer.next(downloadLocation);
                observer.complete();
            });
        });
    }
    getFileTransferProgress(size, transferred) {
        return Math.round(transferred / size * 100);
    }
    postInlineDownload(resolve, reject, downloadJob) {
        //this.currentJob = downloadJob;
        resolve(downloadJob);
    }
    ;
    downloadError(errorHandler) {
        if (errorHandler instanceof Function)
            errorHandler();
        this.notificationService.info('Unable to download files via QDS');
    }
    ;
};
QdsService.ctorParameters = () => [
    { type: HttpClient },
    { type: EntityAppDefService },
    { type: NotificationService }
];
QdsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function QdsService_Factory() { return new QdsService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.NotificationService)); }, token: QdsService, providedIn: "root" });
QdsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], QdsService);
export { QdsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3RELE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDL0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7Ozs7O0FBTzlELElBQWEsVUFBVSxHQUF2QixNQUFhLFVBQVU7SUFJckIsWUFDUyxJQUFnQixFQUNoQixtQkFBd0MsRUFDeEMsbUJBQXdDO1FBRnhDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDaEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBdVJqRCxtQkFBYyxHQUFHLFVBQVUsY0FBYztZQUN2QyxjQUFjLEdBQUcsY0FBYyxJQUFJLEVBQUUsQ0FBQztZQUN0QyxpREFBaUQ7WUFDakQsSUFBSSxPQUFPLEVBQUUsTUFBTSxDQUFDO1lBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLHFCQUFxQixHQUFHLElBQUksT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLEdBQUc7Z0JBQ3hELE9BQU8sR0FBRyxHQUFHLENBQUM7Z0JBQ2QsTUFBTSxHQUFHLEdBQUcsQ0FBQztZQUNmLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyw4RkFBOEYsQ0FBQyxDQUFDO1lBQzlILElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNySCxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxXQUFnQjtnQkFDbkQsV0FBVyxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQy9FLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDckIsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8scUJBQXFCLENBQUM7UUFDL0IsQ0FBQyxDQUFBO0lBdFNHLENBQUM7SUFFTCxhQUFhO1FBQ1gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFCLENBQUM7SUFFRCxhQUFhLENBQUMsVUFBVTtRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsYUFBYSxDQUFDLGFBQWE7UUFDekIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDOUYsTUFBTSxHQUFHLEdBQUcsT0FBTyxHQUFHLHFCQUFxQixDQUFDLGNBQWM7Z0JBQ3hELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcscUJBQXFCLENBQUMsYUFBYSxDQUFDO1lBRTlHLE1BQU0sV0FBVyxHQUFHO2dCQUNsQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUN2QixjQUFjLEVBQUUsa0RBQWtEO29CQUNsRSxlQUFlLEVBQUUsT0FBTztvQkFDeEIsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDLFFBQVEsRUFBRTtvQkFDMUMsa0JBQWtCLEVBQUUsZ0JBQWdCO2lCQUNyQyxDQUFDO2FBQ0gsQ0FBQztZQUVGLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDO2lCQUNuQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFlBQVksQ0FBQyxXQUFnQixFQUFFLFFBQVM7UUFDdEMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUM7WUFDbEIsSUFBSSxRQUFRLENBQUMsaUJBQWlCLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxFQUFFO2dCQUM5QyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixDQUFDLENBQUM7Z0JBQzdDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDTCxJQUFJLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtvQkFDL0MsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLG9CQUFvQixFQUNySCxVQUFVLFlBQWlCLEVBQUUsVUFBZSxFQUFFLEdBQVEsRUFBRSxXQUFnQjt3QkFDdEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxVQUFVLEtBQUssS0FBSyxJQUFJLFlBQVksRUFBRTs0QkFDeEMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzs0QkFDaEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxrRUFBa0UsQ0FBQyxDQUFDOzRCQUNsRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sRUFBRTs0QkFDaEMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzs0QkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDOzRCQUM1QyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2xGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7dUVBQ3dCLENBQUMsQ0FBQzs0QkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sRUFBRTs0QkFDekQscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzs0QkFDbkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzt3RUFDMEIsQ0FBQyxDQUFDOzRCQUMxRCxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sRUFBRTs0QkFDaEMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzs0QkFDbkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQzt1RUFDd0IsQ0FBQyxDQUFDOzRCQUN6RCxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sRUFBRTs0QkFDaEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDOzRCQUM1RCxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCO29CQUNILENBQUMsRUFBRSxXQUFXLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3JCO3FCQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssT0FBTyxFQUFFO29CQUN0RCxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLFlBQVksRUFBRSxXQUFXLENBQUMsb0JBQW9CLEVBQ3JILFVBQVUsWUFBaUIsRUFBRSxVQUFlO3dCQUMxQyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUNqQyxJQUFJLFVBQVUsS0FBSyxLQUFLLElBQUksWUFBWSxFQUFFOzRCQUN4QyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDOzRCQUNoRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGtFQUFrRSxDQUFDLENBQUM7NEJBQ2xHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUNoQyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDOzRCQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7NEJBQzVDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ3pELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7dUZBQ3dDLENBQUMsQ0FBQzs0QkFDekUsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7NEJBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7NEVBQzhCLENBQUMsQ0FBQzs0QkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7NEJBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7MkVBQzRCLENBQUMsQ0FBQzs0QkFDN0QsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQzs0QkFDNUQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjtvQkFDSCxDQUFDLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUMzQjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsT0FBTyxDQUFDLGdCQUFnQjtRQUN0QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsSUFBUztnQkFDN0MsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQUssRUFBRSxTQUFTO1FBQ2pDLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxVQUFVLFVBQVU7WUFDbkYsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pCLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQUssRUFBRSxhQUFhO1FBQ3JDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLFVBQVUsUUFBYTtnQkFDL0QsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDckMsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0JBQzNDLElBQUksYUFBYSxLQUFLLE9BQU8sRUFBRTtvQkFDN0IsSUFBSSxTQUFTLEVBQUU7d0JBQ2IsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNuQixTQUFTLENBQUMsV0FBVyxDQUFDLFVBQVUsTUFBTTs0QkFDcEMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUN0QixDQUFDLENBQUMsQ0FBQztxQkFDSjt5QkFBTTt3QkFDTCxNQUFNLFVBQVUsR0FBRzs0QkFDakIsUUFBUSxFQUFFLFlBQVk7eUJBQ3ZCLENBQUM7cUJBQ0g7aUJBQ0Y7cUJBQU07b0JBQ0wsU0FBUyxDQUFDLFdBQVcsQ0FBQyxVQUFVLElBQUksRUFBRSxNQUFNO3dCQUMxQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3RCLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxNQUFNO3dCQUN2QixJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7NEJBQ3hCLE1BQU0sR0FBRyxVQUFVLENBQUM7eUJBQ3JCO3dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxFQUFFLFVBQVUsSUFBSSxFQUFFLE1BQU07d0JBQ3ZCLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQUssRUFBRSxhQUFhO1FBQ3JDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLFVBQVUsUUFBYTtnQkFDL0QsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDckMsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0JBQzNDLElBQUksYUFBYSxLQUFLLE9BQU8sRUFBRTtvQkFDN0IsSUFBSSxTQUFTLEVBQUU7d0JBQ2IsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNuQixTQUFTLENBQUMsV0FBVyxDQUFDLFVBQVUsTUFBTTs0QkFDcEMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUN0QixDQUFDLENBQUMsQ0FBQztxQkFDSjt5QkFBTTt3QkFDTCxNQUFNLFVBQVUsR0FBRzs0QkFDakIsUUFBUSxFQUFFLFlBQVk7eUJBQ3ZCLENBQUM7cUJBQ0g7aUJBQ0Y7cUJBQU07b0JBQ0wsU0FBUyxDQUFDLFdBQVcsQ0FBQyxVQUFVLElBQUksRUFBRSxNQUFNO3dCQUMxQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3RCLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxNQUFNO3dCQUN2QixJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7NEJBQ3hCLE1BQU0sR0FBRyxVQUFVLENBQUM7eUJBQ3JCO3dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxFQUFFLFVBQVUsSUFBSSxFQUFFLE1BQU07d0JBQ3ZCLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxHQUFRO1FBQ3JCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsTUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztZQUNqQyxNQUFNLFNBQVMsR0FBRztnQkFDaEIsV0FBVyxFQUFFLEdBQUcsQ0FBQyxTQUFTO2dCQUMxQixVQUFVLEVBQUUsR0FBRyxDQUFDLFFBQVE7YUFDekIsQ0FBQztZQUNGLElBQUksV0FBVyxFQUFFO2dCQUNmLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxVQUFVLFFBQVE7b0JBQzlELE1BQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQ3RDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDcEIsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN6QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsUUFBUSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLFVBQVUsUUFBUTtvQkFDOUQsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztvQkFDdEMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUNwQixVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFLLEVBQUUsS0FBSztRQUNuQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxVQUFVLFNBQVM7Z0JBQ2xGLFNBQVMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRTtvQkFDMUMsUUFBUSxDQUFDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUMsQ0FBQztnQkFDSCxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBRW5CLFFBQVEsQ0FBQyxJQUFJLENBQUMsK0ZBQStGLENBQUMsQ0FBQztnQkFDL0csUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNaLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlCQUFpQjtRQUNmLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsUUFBUSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLGdCQUFnQjtnQkFDN0QsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNoQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxvQkFBb0I7UUFDbEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixRQUFRLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLFVBQVUsZ0JBQWdCO2dCQUNoRSxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2hDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHVCQUF1QixDQUFDLElBQVksRUFBRSxXQUFtQjtRQUN2RCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBcUJELGtCQUFrQixDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsV0FBVztRQUM3QyxnQ0FBZ0M7UUFDaEMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFBQSxDQUFDO0lBRUYsYUFBYSxDQUFDLFlBQVk7UUFDeEIsSUFBSSxZQUFZLFlBQVksUUFBUTtZQUNsQyxZQUFZLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7SUFFcEUsQ0FBQztJQUFBLENBQUM7Q0FDSCxDQUFBOztZQXZUZ0IsVUFBVTtZQUNLLG1CQUFtQjtZQUNuQixtQkFBbUI7OztBQVB0QyxVQUFVO0lBSHRCLFVBQVUsQ0FBQztRQUNWLFVBQVUsRUFBRSxNQUFNO0tBQ25CLENBQUM7R0FDVyxVQUFVLENBNFR0QjtTQTVUWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgaXNEZXZNb2RlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBHbG9hYmFsQ29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9jb25maWcnO1xyXG5cclxuZGVjbGFyZSBjb25zdCBxZHNfb3RtbTogYW55O1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUWRzU2VydmljZSB7XHJcblxyXG4gIHB1YmxpYyBfcWRzQ29ubmVjdDtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgaHR0cDogSHR0cENsaWVudCxcclxuICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2VcclxuXHJcbiAgKSB7IH1cclxuXHJcbiAgZ2V0UURTQ29ubmVjdCgpIHtcclxuICAgIHJldHVybiB0aGlzLl9xZHNDb25uZWN0O1xyXG4gIH1cclxuXHJcbiAgc2V0UURTQ29ubmVjdChxZHNDb25uZWN0KSB7XHJcbiAgICB0aGlzLl9xZHNDb25uZWN0ID0gcWRzQ29ubmVjdDtcclxuICB9XHJcblxyXG4gIGdldFFEU1Nlc3Npb24odXNlclNlc3Npb25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldE90bW1Bc3NldFVwbG9hZERldGFpbHMoKS51cmw7XHJcbiAgICAgIGNvbnN0IHVybCA9IGJhc2VVcmwgKyBvdG1tU2VydmljZXNDb25zdGFudHMub3RtbWFwaUJhc2VVcmwgK1xyXG4gICAgICAgIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRPdG1tQXNzZXRVcGxvYWREZXRhaWxzKCkuYXBpVmVyc2lvbiArICcvJyArIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5xZHNTZXNzaW9uVXJsO1xyXG5cclxuICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZDsgY2hhcnNldD1VVEYtOCcsXHJcbiAgICAgICAgICAnWC1PVE1NLUxvY2FsZSc6ICdlbl9VUycsXHJcbiAgICAgICAgICAnWC1SZXF1ZXN0ZWQtQnknOiB1c2VyU2Vzc2lvbklkLnRvU3RyaW5nKCksXHJcbiAgICAgICAgICAnWC1SZXF1ZXN0ZWQtV2l0aCc6ICdYTUxIdHRwUmVxdWVzdCdcclxuICAgICAgICB9KVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgdGhpcy5odHRwLnBvc3QodXJsLCBudWxsLCBodHRwT3B0aW9ucylcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbm5lY3RUb1FEUyhxZHNSZXNvdXJjZTogYW55LCBpc1VwbG9hZD8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcbiAgICAgIGlmIChxZHNfb3RtbS5pc0NsaWVudENvbm5lY3RlZCAmJiAhaXNVcGxvYWQpIHsvL1xyXG4gICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmlzUURTVXBsb2FkID0gdHJ1ZTtcclxuICAgICAgICBjb25zb2xlLmxvZygnUURTIGNsaWVudCBhbHJlYWR5IGNvbm5lY3RlZC4nKTtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KCcxMDAwJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoR2xvYWJhbENvbmZpZy5jb25maWcucWRzVmVyc2lvbiA9PT0gJzEuNS40Jykge1xyXG4gICAgICAgICAgcWRzX290bW0ucWRzQ29ubmVjdCh0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0T3RtbUFzc2V0VXBsb2FkRGV0YWlscygpLnFkc1NlcnZlclVSTCwgcWRzUmVzb3VyY2UuYXV0aGVudGljYXRpb25fdG9rZW4sXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIChxZHNDb25uZWN0b3I6IGFueSwgc3RhdHVzQ29kZTogYW55LCBtc2c6IGFueSwgaW5zdGFsbFByZWY6IGFueSkge1xyXG4gICAgICAgICAgICAgIHRoYXQuc2V0UURTQ29ubmVjdChxZHNDb25uZWN0b3IpO1xyXG4gICAgICAgICAgICAgIGlmIChzdGF0dXNDb2RlID09PSAnMjAwJyAmJiBxZHNDb25uZWN0b3IpIHtcclxuICAgICAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmlzUURTVXBsb2FkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdDb25uZWN0ZWQgdG8gUURTLCBBbGwgdGhlIHVwbG9hZHMvZG93bmxvYWRzIHdpbGwgdXNlIFFEUyB0cmFuZmVyJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDAwJykge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ1FEUyBjbGllbnQgYWxyZWFkeSBjb25uZWN0ZWQnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDEnIHx8IHN0YXR1c0NvZGUgPT09ICcxMDAyJyB8fCBzdGF0dXNDb2RlID09PSAnMTAwOCcpIHtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgVW5hYmxlIHRvIGNvbm5lY3QgdG8gUURTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgQWxsIHRoZSB1cGxvYWRzL2Rvd25sb2FkcyB3aWxsIHVzZSBiYXNpYyB0cmFuZmVyYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDAzJyB8fCBzdGF0dXNDb2RlID09PSAnMTAwNycpIHtcclxuICAgICAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmZpbmRRRFNDbGllbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS53YXJuKGBZb3UgaGF2ZSBjaG9zZW4gbm90IHRvIGluc3RhbGwgUURTIFRyYW5zZmVyIE1hbmFnZXIgcGx1Z2luLFxyXG4gICAgICAgICAgICAgICAgICAgICAgQWxsIHRoZSB1cGxvYWRzL2Rvd25sb2FkcyB3aWxsIHVzZSBiYXNpYyB0cmFuc2ZlcmApO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwNScpIHtcclxuICAgICAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmZpbmRRRFNDbGllbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgQW4gdW5leHBleHRlZCBlcnJvciBvY2N1cnJlZCB3aGlsZSBjb25uZWN0aW5nIHRvIFFEUyxcclxuICAgICAgICAgICAgICAgICAgICAgIEFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgYmFzaWMgdHJhbmZlcmApO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwNicpIHtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS53YXJuKCdRRFMgY2xpZW50IHJlcXVpcmVzIHVwZGF0ZScpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBxZHNSZXNvdXJjZS51c2VyX2lkLCBmYWxzZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KCcxMDAwJyk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoR2xvYWJhbENvbmZpZy5jb25maWcucWRzVmVyc2lvbiA9PT0gJzEuMy4wJykge1xyXG4gICAgICAgICAgcWRzX290bW0ucWRzQ29ubmVjdCh0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0T3RtbUFzc2V0VXBsb2FkRGV0YWlscygpLnFkc1NlcnZlclVSTCwgcWRzUmVzb3VyY2UuYXV0aGVudGljYXRpb25fdG9rZW4sXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIChxZHNDb25uZWN0b3I6IGFueSwgc3RhdHVzQ29kZTogYW55KSB7XHJcbiAgICAgICAgICAgICAgdGhhdC5zZXRRRFNDb25uZWN0KHFkc0Nvbm5lY3Rvcik7XHJcbiAgICAgICAgICAgICAgaWYgKHN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHFkc0Nvbm5lY3Rvcikge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ0Nvbm5lY3RlZCB0byBRRFMsIEFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgUURTIHRyYW5mZXInKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDAnKSB7XHJcbiAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnUURTIGNsaWVudCBhbHJlYWR5IGNvbm5lY3RlZCcpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwMScgfHwgc3RhdHVzQ29kZSA9PT0gJzEwMDInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGF0Lm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoYFVuYWJsZSB0byBjb25uZWN0IHRvIFFEUyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBIZW5jZSBmb3J0aCBhbGwgdGhlIHVwbG9hZHMvZG93bmxvYWRzIHdpbGwgdXNlIGJhc2ljIHRyYW5mZXJgKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDMnKSB7XHJcbiAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5maW5kUURTQ2xpZW50ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGF0Lm5vdGlmaWNhdGlvblNlcnZpY2Uud2FybihgWW91IGhhdmUgY2hvc2VuIG5vdCB0byBpbnN0YWxsIFFEUyBUcmFuc2ZlciBNYW5hZ2VyIHBsdWdpbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBBbGwgdGhlIHVwbG9hZHMvZG93bmxvYWRzIHdpbGwgdXNlIGJhc2ljIHRyYW5zZmVyYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDA1Jykge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBBbiB1bmV4cGV4dGVkIGVycm9yIG9jY3VycmVkIHdoaWxlIGNvbm5lY3RpbmcgdG8gUURTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIEFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgYmFzaWMgdHJhbmZlcmApO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwNicpIHtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS53YXJuKGBRRFMgY2xpZW50IHJlcXVpcmVzIHVwZGF0ZWApO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBxZHNSZXNvdXJjZS51c2VyX2lkKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0Sm9icyhpbmNsdWRlQ29tcGxldGVkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5saXN0Sm9icyhmdW5jdGlvbiAoam9iczogYW55KSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChqb2JzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBpbmNsdWRlQ29tcGxldGVkKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlUURTSW1wb3J0Sm9iKGpvYklkLCBmaWxlUGF0aHMpIHtcclxuICAgIHFkc19vdG1tLmNvbm5lY3Rvci5jcmVhdGVJbXBvcnRKb2IoJ0lNUE9SVF9XT1JLSU5HX0FSRUEnLCBqb2JJZCwgZnVuY3Rpb24gKGN1cnJlbnRKb2IpIHtcclxuICAgICAgY3VycmVudEpvYi5zdGFydEltcG9ydCgpO1xyXG4gICAgICBjdXJyZW50Sm9iLmRldGFjaCgpO1xyXG4gICAgfSwgZmlsZVBhdGhzKTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZVFEU0ltcG9ydEpvYihqb2JJZCwgdG9nZ2xlVG9TdGF0ZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBxZHNfb3RtbS5jb25uZWN0b3IucmVsb2FkSW1wb3J0Sm9iKGpvYklkLCBmdW5jdGlvbiAocmVzcG9uc2U6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGltcG9ydEpvYiA9IHJlc3BvbnNlLmltcG9ydEpvYjtcclxuICAgICAgICBjb25zdCBpc1J1bm5pbmcgPSByZXNwb25zZS5qb2JJbmZvLnJ1bm5pbmc7XHJcbiAgICAgICAgaWYgKHRvZ2dsZVRvU3RhdGUgPT09ICdQQVVTRScpIHtcclxuICAgICAgICAgIGlmIChpc1J1bm5pbmcpIHtcclxuICAgICAgICAgICAgaW1wb3J0Sm9iLmRldGFjaCgpO1xyXG4gICAgICAgICAgICBpbXBvcnRKb2IucGF1c2VJbXBvcnQoZnVuY3Rpb24gKHN0YXR1cykge1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzKTtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGpvYl9zdGF0dXMgPSB7XHJcbiAgICAgICAgICAgICAgJ3N0YXR1cyc6ICdqb2IgcGF1c2VkJ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpbXBvcnRKb2Iuc3RhcnRJbXBvcnQoZnVuY3Rpb24gKG5hbWUsIHN0YXR1cykge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBmdW5jdGlvbiAobmFtZSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgIHN0YXR1cyA9ICdDT01QTEVURSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXMpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZnVuY3Rpb24gKG5hbWUsIHN0YXR1cykge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVRRFNFeHBvcnRKb2Ioam9iSWQsIHRvZ2dsZVRvU3RhdGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgcWRzX290bW0uY29ubmVjdG9yLnJlbG9hZEV4cG9ydEpvYihqb2JJZCwgZnVuY3Rpb24gKHJlc3BvbnNlOiBhbnkpIHtcclxuICAgICAgICBjb25zdCBleHBvcnRKb2IgPSByZXNwb25zZS5leHBvcnRKb2I7XHJcbiAgICAgICAgY29uc3QgaXNSdW5uaW5nID0gcmVzcG9uc2Uuam9iSW5mby5ydW5uaW5nO1xyXG4gICAgICAgIGlmICh0b2dnbGVUb1N0YXRlID09PSAnUEFVU0UnKSB7XHJcbiAgICAgICAgICBpZiAoaXNSdW5uaW5nKSB7XHJcbiAgICAgICAgICAgIGV4cG9ydEpvYi5kZXRhY2goKTtcclxuICAgICAgICAgICAgZXhwb3J0Sm9iLnBhdXNlRXhwb3J0KGZ1bmN0aW9uIChzdGF0dXMpIHtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBqb2Jfc3RhdHVzID0ge1xyXG4gICAgICAgICAgICAgICdzdGF0dXMnOiAnam9iIHBhdXNlZCdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgZXhwb3J0Sm9iLnN0YXJ0RXhwb3J0KGZ1bmN0aW9uIChuYW1lLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXMpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZnVuY3Rpb24gKG5hbWUsIHN0YXR1cykge1xyXG4gICAgICAgICAgICBpZiAoc3RhdHVzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICBzdGF0dXMgPSAnQ09NUExFVEUnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0sIGZ1bmN0aW9uIChuYW1lLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXMpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmV0cnlGYWlsZWRKb2Ioam9iOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgY29uc3QgaXNJbXBvcnRKb2IgPSBqb2IuaXNJbXBvcnQ7XHJcbiAgICAgIGNvbnN0IGN1cnJKb2JJZCA9IHtcclxuICAgICAgICAnb3RtbUpvYklkJzogam9iLm90bW1Kb2JJZCxcclxuICAgICAgICAncWRzSm9iSWQnOiBqb2IucWRzSm9iSWRcclxuICAgICAgfTtcclxuICAgICAgaWYgKGlzSW1wb3J0Sm9iKSB7XHJcbiAgICAgICAgcWRzX290bW0uY29ubmVjdG9yLnJlbG9hZEltcG9ydEpvYihjdXJySm9iSWQsIGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgY29uc3QgY3VycmVudEpvYiA9IHJlc3BvbnNlLmltcG9ydEpvYjtcclxuICAgICAgICAgIGN1cnJlbnRKb2IuZGV0YWNoKCk7XHJcbiAgICAgICAgICBjdXJyZW50Sm9iLnN0YXJ0SW1wb3J0KCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBxZHNfb3RtbS5jb25uZWN0b3IucmVsb2FkRXhwb3J0Sm9iKGN1cnJKb2JJZCwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyZW50Sm9iID0gcmVzcG9uc2UuZXhwb3J0Sm9iO1xyXG4gICAgICAgICAgY3VycmVudEpvYi5kZXRhY2goKTtcclxuICAgICAgICAgIGN1cnJlbnRKb2Iuc3RhcnRFeHBvcnQoKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGRvd25sb2FkKGpvYklkLCBmaWxlcyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBxZHNfb3RtbS5jb25uZWN0b3IuY3JlYXRlRXhwb3J0Sm9iKCdERUZBVUxUX0VYUE9SVF9BUkVBJywgam9iSWQsIGZ1bmN0aW9uIChleHBvcnRKb2IpIHtcclxuICAgICAgICBleHBvcnRKb2Iuc3RhcnRFeHBvcnQodW5kZWZpbmVkLCB1bmRlZmluZWQsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoJ1VuYWJsZSB0byBkb3dubG9hZCBmaWxlcyB2aWEgUURTLicpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGV4cG9ydEpvYi5kZXRhY2goKTtcclxuXHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dCgnRmlsZXMgYXJlIGJlaW5nIGRvd25sb2FkZWQgdmlhIFFEUyBjbGllbnQuIENoZWNrIHRoZSBkb3dubG9hZCBzdGF0dXMgaW4gZmlsZSB0cmFuc2ZlciB3aW5kb3cuJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZmlsZXMpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXREb3dubG9hZEZvbGRlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgcWRzX290bW0uY29ubmVjdG9yLmdldERvd25sb2FkRm9sZGVyKGZ1bmN0aW9uIChkb3dubG9hZExvY2F0aW9uKSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChkb3dubG9hZExvY2F0aW9uKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2hvb3NlRG93bmxvYWRGb2xkZXIoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5jaG9vc2VEb3dubG9hZEZvbGRlcihmdW5jdGlvbiAoZG93bmxvYWRMb2NhdGlvbikge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQoZG93bmxvYWRMb2NhdGlvbik7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEZpbGVUcmFuc2ZlclByb2dyZXNzKHNpemU6IG51bWJlciwgdHJhbnNmZXJyZWQ6IG51bWJlcik6IG51bWJlciB7XHJcbiAgICByZXR1cm4gTWF0aC5yb3VuZCh0cmFuc2ZlcnJlZCAvIHNpemUgKiAxMDApO1xyXG4gIH1cclxuXHJcbiAgaW5saW5lRG93bmxvYWQgPSBmdW5jdGlvbiAoZG93bmxvYWRQYXJhbXMpIHtcclxuICAgIGRvd25sb2FkUGFyYW1zID0gZG93bmxvYWRQYXJhbXMgfHwge307XHJcbiAgICAvL3ZhciBub3RpZmljYXRpb24gPSBkb3dubG9hZFBhcmFtcy5ub3RpZmljYXRpb247XHJcbiAgICB2YXIgcmVzb2x2ZSwgcmVqZWN0O1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgdmFyIGlubGluZURvd25sb2FkUHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXMsIHJlaikge1xyXG4gICAgICByZXNvbHZlID0gcmVzO1xyXG4gICAgICByZWplY3QgPSByZWo7XHJcbiAgICB9KTtcclxuICAgIHNlbGYubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKFwiRmlsZXMgYXJlIGJlaW5nIGRvd25sb2FkZWQgdmlhIFFEUyBjbGllbnQsIENoZWNrIHRoZSBkb3dubG9hZCBzdGF0dXMgaW4gZmlsZSB0cmFuc2ZlciB3aW5kb3dcIik7XHJcbiAgICB0aGlzLl9xZHNDb25uZWN0LmNyZWF0ZURvd25sb2FkSm9iKGRvd25sb2FkUGFyYW1zLmFzc2V0T2JqZWN0cywgc2VsZi5wb3N0SW5saW5lRG93bmxvYWQuYmluZCh0aGlzLCByZXNvbHZlLCByZWplY3QpKTtcclxuICAgIGlubGluZURvd25sb2FkUHJvbWlzZS50aGVuKGZ1bmN0aW9uIChkb3dubG9hZEpvYjogYW55KSB7XHJcbiAgICAgIGRvd25sb2FkSm9iLnN0YXJ0RG93bmxvYWQodW5kZWZpbmVkLCB1bmRlZmluZWQsIHNlbGYuZG93bmxvYWRFcnJvci5iaW5kKHNlbGYpKTtcclxuICAgICAgZG93bmxvYWRKb2IuZGV0YWNoKCk7XHJcbiAgICAgIHJlc29sdmUoKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGlubGluZURvd25sb2FkUHJvbWlzZTtcclxuICB9XHJcblxyXG4gIHBvc3RJbmxpbmVEb3dubG9hZChyZXNvbHZlLCByZWplY3QsIGRvd25sb2FkSm9iKSB7XHJcbiAgICAvL3RoaXMuY3VycmVudEpvYiA9IGRvd25sb2FkSm9iO1xyXG4gICAgcmVzb2x2ZShkb3dubG9hZEpvYik7XHJcbiAgfTtcclxuXHJcbiAgZG93bmxvYWRFcnJvcihlcnJvckhhbmRsZXIpIHtcclxuICAgIGlmIChlcnJvckhhbmRsZXIgaW5zdGFuY2VvZiBGdW5jdGlvbilcclxuICAgICAgZXJyb3JIYW5kbGVyKCk7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnVW5hYmxlIHRvIGRvd25sb2FkIGZpbGVzIHZpYSBRRFMnKTtcclxuXHJcbiAgfTtcclxufVxyXG4iXX0=