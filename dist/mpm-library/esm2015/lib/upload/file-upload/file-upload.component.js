import { __decorate } from "tslib";
import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef, HostListener, isDevMode } from '@angular/core';
import { NotificationService } from '../../notification/notification.service';
import { AssetFileConfigService } from '../services/asset.file.config.service';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SharingService } from '../../mpm-utils/services/sharing.service';
let FileUploadComponent = class FileUploadComponent {
    constructor(element, notificationService, assetFileConfigService, dialog, sharingService) {
        this.element = element;
        this.notificationService = notificationService;
        this.assetFileConfigService = assetFileConfigService;
        this.dialog = dialog;
        this.sharingService = sharingService;
        this.filesAdded = new EventEmitter();
        this.queuedFiles = new EventEmitter();
        this.thumbnailFormats = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];
        this.selectedFiles = [];
        this.isQDSUpload = otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload;
        this.htmlElement = this.element.nativeElement;
    }
    getTypeOf(obj) {
        return typeof obj;
    }
    openQDSFileChooser() {
        const that = this;
        qds_otmm.connector.openFileChooser((fileInfoList) => {
            let fileObj;
            const transferFileObjectList = [];
            if (!fileInfoList) {
                return;
            }
            if (!Array.isArray(fileInfoList)) {
                fileInfoList = [fileInfoList];
            }
            for (const fileInfo of fileInfoList) {
                fileObj = {};
                fileObj.name = fileInfo.name;
                fileObj.path = fileInfo.path;
                fileObj.size = fileInfo.size;
                fileObj.type = fileInfo.type;
                fileObj.url = fileInfo.thumbnail;
                fileObj.assetUrl = fileInfo.thumbnail;
                fileObj.width = fileInfo.imageWidth;
                fileObj.height = fileInfo.imageHeight;
                transferFileObjectList.push(fileObj);
            }
            that.processFileSelection(transferFileObjectList);
        });
    }
    calculateSelectedFileSize() {
        let fileSize = 0;
        if (Array.isArray(this.selectedFiles) && this.selectedFiles.length > 0) {
            for (const selectedFile of this.selectedFiles) {
                if (selectedFile && selectedFile.size
                    && selectedFile.size != null && selectedFile.size > 0) {
                    fileSize += selectedFile.size;
                }
            }
        }
        return fileSize;
    }
    formatBytes(bytes) {
        if (bytes === 0) {
            return '0 Bytes';
        }
        const k = 1024;
        const dm = 2;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
    checkFilesSizeCosntraint(files) {
        if (!this.maxFileSize) {
            return true;
        }
        let validity = true;
        let currentSelectedFileSize = 0;
        if (files && files.length) {
            for (const file of files) {
                if (file.size && file.size != null && file.size > 0) {
                    currentSelectedFileSize += file.size;
                }
            }
            if (currentSelectedFileSize <= this.maxFileSize) {
                const existingFileSize = this.calculateSelectedFileSize();
                if (currentSelectedFileSize + existingFileSize <= this.maxFileSize) {
                    validity = true;
                }
                else {
                    validity = false;
                }
            }
            else {
                validity = false;
            }
        }
        if (!validity) {
            this.notificationService.info('Cannot upload file(s) of size more than ' + (this.maxFileSize / 1000) + 'KB');
        }
        return validity;
    }
    filterFiles(currentSelectedFiles, existingFiles) {
        const validFiles = [];
        const duplicates = [];
        if (currentSelectedFiles && currentSelectedFiles != null && currentSelectedFiles.length && currentSelectedFiles.length > 0) {
            for (const currentSelectedFile of currentSelectedFiles) {
                let alreadyExist = false;
                const currentFile = currentSelectedFile;
                for (const existingFile of existingFiles) {
                    if (currentFile.name === existingFile.name) {
                        alreadyExist = true;
                        duplicates.push(currentSelectedFile);
                        break;
                    }
                }
                if (!alreadyExist) {
                    validFiles.push(currentSelectedFile);
                }
            }
            if (duplicates.length > 0) {
                this.notificationService.info('File was not added because its name matches an existing file');
            }
            return validFiles;
        }
        else {
            return validFiles;
        }
    }
    getFilesWebkitDataTransferItems(dataTransferItems) {
        const files = [];
        function traverseFileTreePromise(item, path = '') {
            return new Promise(resolve => {
                if (item.isFile) {
                    item.file(file => {
                        file.filepath = path + file.name;
                        files.push(file);
                        resolve(file);
                    });
                }
                else if (item.isDirectory) {
                    const dirReader = item.createReader();
                    dirReader.readEntries(entries => {
                        const entriesPromises = [];
                        for (const entr of entries) {
                            entriesPromises.push(traverseFileTreePromise(entr, path + item.name + '/'));
                        }
                        resolve(Promise.all(entriesPromises));
                    });
                }
            });
        }
        return new Promise((resolve, reject) => {
            const entriesPromises = [];
            for (const it of dataTransferItems) {
                entriesPromises.push(traverseFileTreePromise(it.webkitGetAsEntry()));
            }
            Promise.all(entriesPromises).then(entries => {
                resolve(files);
            });
        });
    }
    createFileObj(file, data) {
        if (file && file !== null) {
            if (data && data !== null) {
                file.data = data.split('base64,')[1];
                file.url = data;
            }
            return file;
        }
        else {
            return null;
        }
    }
    checkNumberOfFileConstraint(files) {
        if (!this.maxFiles || files.length === 0) {
            return true;
        }
        let isValid = true;
        const currentSelectedFilesCount = files.length;
        const alreadySelectedFilesCount = this.selectedFiles.length;
        if (this.maxFiles === alreadySelectedFilesCount) {
            isValid = false;
        }
        else if (this.maxFiles < (currentSelectedFilesCount + alreadySelectedFilesCount)) {
            isValid = false;
        }
        if (!isValid) {
            if (this.isRevisionUpload) {
                this.notificationService.info('You can select only maximum of '
                    + this.maxFiles + ' file in Revision Upload');
            }
            else if (alreadySelectedFilesCount === 0) {
                this.notificationService.info('You can select only a maximum '
                    + this.maxFiles + ' files, But you are trying to select ' + currentSelectedFilesCount + ' file(s)');
            }
            else if (alreadySelectedFilesCount > 0) {
                this.notificationService.info('You can select maximum of ' + this.maxFiles
                    + ' file(s), You have already selected ' + alreadySelectedFilesCount + ' files');
            }
            else {
                this.notificationService.info('You you can select only a maximum ' + this.maxFiles + ' files');
            }
        }
        return isValid;
    }
    checkForSpecialCharacter(files) {
        if (files) {
            const filesWithoutSpecialCharacter = [];
            const filesWithSpecialCharacter = [];
            for (const fileInfo of files) {
                (fileInfo && fileInfo.name && (fileInfo.name.indexOf('\'') >= 0)) ?
                    filesWithSpecialCharacter.push(fileInfo) : filesWithoutSpecialCharacter.push(fileInfo);
            }
            if (filesWithSpecialCharacter.length > 0) {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'The special character $ and \' will be replaced with _.Do you want to continue?',
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result && result.isTrue) {
                        this.processFileSelection([...filesWithoutSpecialCharacter, ...filesWithSpecialCharacter]);
                    }
                });
            }
            else {
                this.processFileSelection(files);
            }
        }
    }
    processFileSelection(files) {
        if ((this.isQDSUpload && this.checkNumberOfFileConstraint(files)) ||
            (this.checkNumberOfFileConstraint(files) && this.checkFilesSizeCosntraint(files))) {
            const validFiles = this.filterFiles(files, this.selectedFiles);
            this.handleFileProcessing(validFiles);
        }
    }
    handleFileProcessing(validFiles) {
        this.filesAdded.next(validFiles);
        for (let i = 0; i < validFiles.length; i++) {
            const fileObj = validFiles[i];
            const objectUrl = fileObj.assetUrl;
            if (objectUrl instanceof Promise) {
                objectUrl
                    .then(obj => {
                    fileObj.width = obj.imageWidth,
                        fileObj.height = obj.imageHeight,
                        fileObj.url = obj.thumbnail,
                        fileObj.assetUrl = obj.thumbnail;
                })
                    .catch(err => {
                    console.log('Unable to generate thumbnail :' + err);
                });
            }
            if (fileObj && fileObj != null) {
                if (this.thumbnailFormats.indexOf(fileObj.type) !== -1 && i < 10) {
                    if (!this.isQDSUpload) {
                        const reader = new FileReader();
                        reader.onload = (e) => {
                            const data = reader.result;
                            const obj = this.createFileObj(fileObj, data);
                            if (obj && obj !== null) {
                                this.selectedFiles.push(obj);
                            }
                        };
                        reader.readAsDataURL(fileObj);
                    }
                    else {
                        if (!fileObj.url || typeof fileObj.url === 'object') {
                            fileObj.icon = this.assetFileConfigService.findIconByName(fileObj.name);
                        }
                        if (this.isQDSUpload && fileObj.name && (fileObj.name.indexOf('$') >= 0 || fileObj.name.indexOf('\'') >= 0)) {
                            this.notificationService.error("Selected file(s) name has special characters dollar($) or single quote(') which is not allowed. Please change the file name and upload again.");
                        }
                        else {
                            this.selectedFiles.push(fileObj);
                        }
                    }
                }
                else {
                    fileObj.icon = this.assetFileConfigService.findIconByName(fileObj.name);
                    this.selectedFiles.push(fileObj);
                }
            }
        }
        this.queuedFiles.next(this.selectedFiles);
    }
    onDrop(event) {
        const items = event.dataTransfer.items;
        this.getFilesWebkitDataTransferItems(items).then(files => {
            this.checkForSpecialCharacter(files);
        });
        event.preventDefault();
        event.stopPropagation();
        this.element.nativeElement.value = '';
    }
    onDropOver(event) {
        event.preventDefault();
    }
    onFileSelectionChange(event) {
        const files = event.target.files;
        this.checkForSpecialCharacter(files);
        event.target.value = '';
    }
    removeAll() {
        this.selectedFiles = [];
        this.queuedFiles.next(this.selectedFiles);
    }
    removeFile(fileObj, index, event) {
        if (event) {
            event.stopPropagation();
        }
        this.selectedFiles.splice(index, 1);
        this.queuedFiles.next(this.selectedFiles);
    }
    createQDSDropArea() {
        const that = this;
        const dropzoneEl = this.element.nativeElement.querySelector('div.acron-file-upload-wrapper');
        dropzoneEl.addEventListener('qds_dragdrop', (event) => {
            event = event || {};
            const eventType = (event.detail || {}).qdsEventType;
            if (eventType === 'dragleave' || eventType === 'addfile') {
                //   $(dropzoneEl).trigger('dragDropHideWindow');
            }
        });
        qds_otmm.connector.createDropArea(dropzoneEl, (fileInfoList) => {
            let fileObj;
            const transferFileObjectList = [];
            if (!fileInfoList) {
                return;
            }
            if (!Array.isArray(fileInfoList)) {
                fileInfoList = [fileInfoList];
            }
            for (const fileInfo of fileInfoList) {
                fileObj = {};
                fileObj.name = fileInfo.name;
                fileObj.path = fileInfo.path;
                fileObj.size = fileInfo.size;
                fileObj.type = fileInfo.type;
                fileObj.url = fileInfo.thumbnail;
                fileObj.assetUrl = fileInfo.thumbnail;
                fileObj.width = fileInfo.imageWidth;
                fileObj.height = fileInfo.imageHeight;
                transferFileObjectList.push(fileObj);
            }
            that.processFileSelection(transferFileObjectList);
        });
    }
    renameFileName(fileName) {
        return fileName.replace(new RegExp('\''), '_').split('$').join('_');
    }
    ngOnInit() {
        this.selectedFiles = [];
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        // if (!this.maxFileSize) {
        //     this.maxFileSize = 15000000;
        // }
    }
    ngAfterViewInit() {
        if (this.isQDSUpload) {
            this.createQDSDropArea();
        }
    }
};
FileUploadComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: NotificationService },
    { type: AssetFileConfigService },
    { type: MatDialog },
    { type: SharingService }
];
__decorate([
    Input()
], FileUploadComponent.prototype, "fileFormats", void 0);
__decorate([
    Input()
], FileUploadComponent.prototype, "maxFiles", void 0);
__decorate([
    Input()
], FileUploadComponent.prototype, "maxFileSize", void 0);
__decorate([
    Input()
], FileUploadComponent.prototype, "isRevisionUpload", void 0);
__decorate([
    Input()
], FileUploadComponent.prototype, "fileToRevision", void 0);
__decorate([
    Output()
], FileUploadComponent.prototype, "filesAdded", void 0);
__decorate([
    Output()
], FileUploadComponent.prototype, "queuedFiles", void 0);
__decorate([
    HostListener('drop', ['$event'])
], FileUploadComponent.prototype, "onDrop", null);
__decorate([
    HostListener('dragover', ['$event'])
], FileUploadComponent.prototype, "onDropOver", null);
FileUploadComponent = __decorate([
    Component({
        selector: 'mpm-file-upload',
        template: "<div class=\"acron-file-upload-container\">\r\n    <div class=\"acron-file-upload-action\">\r\n        <div class=\"flex-row actions-align\">\r\n            <div class=\"flex-item\">\r\n                <mat-chip-list matTooltip=\"No. of Files Selected\">\r\n                    <mat-chip *ngIf=\"selectedFiles.length\">Selected ({{selectedFiles.length}}<span\r\n                            *ngIf=\"this.maxFiles > 0\">/{{this.maxFiles}}</span>)</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            <div class=\"actions-holder flex-item\">\r\n                <button mat-raised-button class=\"actions\" *ngIf=\"selectedFiles.length > 1\" (click)=\"removeAll()\"\r\n                    matTooltip=\"Click here to remove all selected files\">Remove All</button>\r\n                <button mat-raised-button class=\"actions\" (click)=\"isQDSUpload ? openQDSFileChooser() : fileBtn.click()\"\r\n                    matTooltip=\"Click here to select Files\">Select\r\n                    Files</button>\r\n                <input mat-raised-button class=\"actions\" #fileBtn type=\"file\" multiple\r\n                    (change)=\"onFileSelectionChange($event)\" style=\"display: none;\" />\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"qds-thumbnail-note\" *ngIf=\"isQDSUpload\">\r\n        <span>Note: Thumbnail preview will be shown only for first 10 image files for each selection.</span>\r\n    </div>\r\n    <div class=\"existing-file-holder\" *ngIf=\"isRevisionUpload && fileToRevision\">\r\n        <div class=\"file-items-holder\">\r\n            <mat-card>\r\n                <div class=\"card-file-contents\">\r\n                    <div class=\"file-icon\" matTooltip=\"Preview\">\r\n                        <img src=\"{{otmmBaseUrl + fileToRevision.url}}\" *ngIf=\"fileToRevision.url\" />\r\n                        <mat-icon *ngIf=\"fileToRevision.icon\">{{fileToRevision.icon}}</mat-icon>\r\n                    </div>\r\n                    <div class=\"file-details name\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"{{renameFileName(fileToRevision.name)}}\">{{renameFileName(fileToRevision.name)}}</span>\r\n                    </div>\r\n                </div>\r\n            </mat-card>\r\n        </div>\r\n    </div>\r\n    <div class=\"acron-file-upload-wrapper\" (click)=\"isQDSUpload ? openQDSFileChooser() : fileBtn.click()\">\r\n        <div class=\"file-items-holder\" *ngIf=\"selectedFiles.length > 0\">\r\n            <mat-card *ngFor=\"let file of selectedFiles\">\r\n                <div class=\"card-file-contents\">\r\n                    <div class=\"file-icon\" matTooltip=\"Preview\">\r\n                        <img *ngIf=\"file.url && getTypeOf(file.url) !== 'object'\" src=\"{{file.url}}\" />\r\n                        <mat-icon class=\"mat-preview-size\"\r\n                            *ngIf=\"file.icon && !(file.url || getTypeOf(file.url) === 'object')\">{{file.icon}}\r\n                        </mat-icon>\r\n                    </div>\r\n                    <div class=\"file-details name\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"File Name: {{renameFileName(file.name)}}\">{{renameFileName(file.name)}}</span>\r\n                    </div>\r\n                    <div class=\"file-details size\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"File Size: {{formatBytes(file.size)}}\">{{formatBytes(file.size)}}</span>\r\n                    </div>\r\n                    <div class=\"file-details type\">\r\n                        <span class=\"file-info\" matTooltip=\"File Type: {{file.type}}\">{{file.type}}</span>\r\n                    </div>\r\n                    <div class=\"file-actions\">\r\n                        <a class=\"action\" mat-icon-button matTooltip=\"Click here to remove this file\">\r\n                            <mat-icon class=\"action\" (click)=\"removeFile(file, selectedFiles.indexOf(file), $event)\">\r\n                                cancel</mat-icon>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n            </mat-card>\r\n        </div>\r\n        <div *ngIf=\"!selectedFiles.length\">\r\n            <p>Click or Drag and Drop Files here to upload</p>\r\n        </div>\r\n    </div>\r\n</div>",
        styles: [".acron-file-upload-container{display:flex;flex-direction:column;height:100%;width:100%}.acron-file-upload-container .qds-thumbnail-note{font-size:small;color:#f44336}.acron-file-upload-container .existing-file-holder{margin-right:-4px}.acron-file-upload-action,.acron-file-upload-wrapper{display:flex;flex-direction:row}.actions-align{width:100%;align-items:baseline}.acron-file-upload-wrapper{padding:0;margin:0 0 16px;align-items:center;justify-content:center;width:100%;height:100%;overflow:auto;border:2px dashed}.acron-file-upload-wrapper p{display:flex;align-items:center;justify-content:center}.file-items-holder{width:100%;height:100%}.mat-preview-size{font-size:40px;padding-right:15px}.file-icon img{height:40px;width:40px}.card-file-contents,.file-actions,.file-details{display:flex;flex-direction:row}.file-actions,.file-details{flex-grow:1}.file-details{margin:0 15px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:inline-block}.file-details.name{width:25em}.file-details.size{width:5em}.file-details.type{width:10em}.acron-file-upload-action .actions-holder,.file-actions .action{margin-left:auto}.acron-file-upload-action .actions-holder .actions{margin:10px}.card-file-contents{align-items:center}"]
    })
], FileUploadComponent);
export { FileUploadComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11cGxvYWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvdXBsb2FkL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25JLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3JILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFVMUUsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFrQjVCLFlBQ1csT0FBbUIsRUFDbkIsbUJBQXdDLEVBQ3hDLHNCQUE4QyxFQUM5QyxNQUFpQixFQUNqQixjQUE4QjtRQUo5QixZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQ25CLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQWZ4QixlQUFVLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7UUFDOUQsZ0JBQVcsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUdoRixxQkFBZ0IsR0FBRyxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBR3pFLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDO1FBU25FLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7SUFDbEQsQ0FBQztJQUVELFNBQVMsQ0FBQyxHQUFHO1FBQ1QsT0FBTyxPQUFPLEdBQUcsQ0FBQztJQUN0QixDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsWUFBWSxFQUFFLEVBQUU7WUFFaEQsSUFBSSxPQUFPLENBQUM7WUFDWixNQUFNLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztZQUVsQyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNmLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUM5QixZQUFZLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNqQztZQUVELEtBQUssTUFBTSxRQUFRLElBQUksWUFBWSxFQUFFO2dCQUNqQyxPQUFPLEdBQUcsRUFBRSxDQUFDO2dCQUNiLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDN0IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUM3QixPQUFPLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQzdCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDN0IsT0FBTyxDQUFDLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUNqQyxPQUFPLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3RDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQztnQkFDcEMsT0FBTyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO2dCQUN0QyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDeEM7WUFFRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUV0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5QkFBeUI7UUFDckIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BFLEtBQUssTUFBTSxZQUFZLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDM0MsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLElBQUk7dUJBQzlCLFlBQVksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLFlBQVksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29CQUN2RCxRQUFRLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQztpQkFDakM7YUFDSjtTQUNKO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFhO1FBQ3JCLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTtZQUFFLE9BQU8sU0FBUyxDQUFDO1NBQUU7UUFDdEMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ2YsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsTUFBTSxLQUFLLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hFLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEQsT0FBTyxVQUFVLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxLQUFhO1FBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ25CLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSx1QkFBdUIsR0FBRyxDQUFDLENBQUM7UUFDaEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUN2QixLQUFLLE1BQU0sSUFBSSxJQUFJLEtBQUssRUFBRTtnQkFDdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO29CQUNqRCx1QkFBdUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUN4QzthQUNKO1lBQ0QsSUFBSSx1QkFBdUIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUM3QyxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO2dCQUMxRCxJQUFJLHVCQUF1QixHQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hFLFFBQVEsR0FBRyxJQUFJLENBQUM7aUJBQ25CO3FCQUFNO29CQUNILFFBQVEsR0FBRyxLQUFLLENBQUM7aUJBQ3BCO2FBQ0o7aUJBQU07Z0JBQ0gsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUNwQjtTQUNKO1FBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsMENBQTBDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1NBQ2hIO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELFdBQVcsQ0FBQyxvQkFBb0IsRUFBRSxhQUFhO1FBQzNDLE1BQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUN0QixNQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxvQkFBb0IsSUFBSSxvQkFBb0IsSUFBSSxJQUFJLElBQUksb0JBQW9CLENBQUMsTUFBTSxJQUFJLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDeEgsS0FBSyxNQUFNLG1CQUFtQixJQUFJLG9CQUFvQixFQUFFO2dCQUNwRCxJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE1BQU0sV0FBVyxHQUFHLG1CQUFtQixDQUFDO2dCQUN4QyxLQUFLLE1BQU0sWUFBWSxJQUFJLGFBQWEsRUFBRTtvQkFDdEMsSUFBSSxXQUFXLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQyxJQUFJLEVBQUU7d0JBQ3hDLFlBQVksR0FBRyxJQUFJLENBQUM7d0JBQ3BCLFVBQVUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzt3QkFDckMsTUFBTTtxQkFDVDtpQkFDSjtnQkFDRCxJQUFJLENBQUMsWUFBWSxFQUFFO29CQUNmLFVBQVUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztpQkFDeEM7YUFDSjtZQUNELElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsOERBQThELENBQUMsQ0FBQzthQUNqRztZQUNELE9BQU8sVUFBVSxDQUFDO1NBQ3JCO2FBQU07WUFDSCxPQUFPLFVBQVUsQ0FBQztTQUNyQjtJQUNMLENBQUM7SUFFRCwrQkFBK0IsQ0FBQyxpQkFBaUI7UUFDN0MsTUFBTSxLQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ3pCLFNBQVMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLElBQUksR0FBRyxFQUFFO1lBQzVDLE9BQU8sSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNiLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ2pDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEIsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUN6QixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ3RDLFNBQVMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQzVCLE1BQU0sZUFBZSxHQUFHLEVBQUUsQ0FBQzt3QkFDM0IsS0FBSyxNQUFNLElBQUksSUFBSSxPQUFPLEVBQUU7NEJBQ3hCLGVBQWUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7eUJBQy9FO3dCQUNELE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7b0JBQzFDLENBQUMsQ0FBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO1FBQ0QsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNuQyxNQUFNLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDM0IsS0FBSyxNQUFNLEVBQUUsSUFBSSxpQkFBaUIsRUFBRTtnQkFDaEMsZUFBZSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDeEU7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDeEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25CLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJO1FBQ3BCLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUU7WUFDdkIsSUFBSSxJQUFJLElBQUksSUFBSSxLQUFLLElBQUksRUFBRTtnQkFDdkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQzthQUNuQjtZQUNELE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsMkJBQTJCLENBQUMsS0FBYTtRQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QyxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ25CLE1BQU0seUJBQXlCLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMvQyxNQUFNLHlCQUF5QixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQzVELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyx5QkFBeUIsRUFBRTtZQUM3QyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ25CO2FBQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMseUJBQXlCLEdBQUcseUJBQXlCLENBQUMsRUFBRTtZQUNoRixPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ25CO1FBQ0QsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN2QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlDQUFpQztzQkFDekQsSUFBSSxDQUFDLFFBQVEsR0FBRywwQkFBMEIsQ0FBQyxDQUFDO2FBQ3JEO2lCQUFNLElBQUkseUJBQXlCLEtBQUssQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdDQUFnQztzQkFDeEQsSUFBSSxDQUFDLFFBQVEsR0FBRyx1Q0FBdUMsR0FBRyx5QkFBeUIsR0FBRyxVQUFVLENBQUMsQ0FBQzthQUMzRztpQkFBTSxJQUFJLHlCQUF5QixHQUFHLENBQUMsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsUUFBUTtzQkFDcEUsc0NBQXNDLEdBQUcseUJBQXlCLEdBQUcsUUFBUSxDQUFDLENBQUM7YUFDeEY7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxvQ0FBb0MsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxDQUFDO2FBQ2xHO1NBQ0o7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBQ0Qsd0JBQXdCLENBQUMsS0FBYTtRQUNsQyxJQUFJLEtBQUssRUFBRTtZQUNQLE1BQU0sNEJBQTRCLEdBQUcsRUFBRSxDQUFDO1lBQ3hDLE1BQU0seUJBQXlCLEdBQUcsRUFBRSxDQUFDO1lBQ3JDLEtBQUssTUFBTSxRQUFRLElBQUksS0FBSyxFQUFFO2dCQUMxQixDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvRCx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUM5RjtZQUNELElBQUkseUJBQXlCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdEMsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7b0JBQzNELEtBQUssRUFBRSxLQUFLO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixJQUFJLEVBQUU7d0JBQ0YsT0FBTyxFQUFFLGlGQUFpRjt3QkFDMUYsWUFBWSxFQUFFLEtBQUs7d0JBQ25CLFlBQVksRUFBRSxJQUFJO3FCQUNyQjtpQkFDSixDQUFDLENBQUM7Z0JBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDdkMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTt3QkFDekIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsR0FBRyw0QkFBNEIsRUFBRSxHQUFHLHlCQUF5QixDQUFDLENBQUMsQ0FBQztxQkFDOUY7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDcEM7U0FDSjtJQUNMLENBQUM7SUFDRCxvQkFBb0IsQ0FBQyxLQUFLO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3RCxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNuRixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3pDO0lBQ0wsQ0FBQztJQUVELG9CQUFvQixDQUFDLFVBQVU7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsTUFBTSxPQUFPLEdBQVEsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25DLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFFbkMsSUFBSSxTQUFTLFlBQVksT0FBTyxFQUFFO2dCQUM5QixTQUFTO3FCQUNKLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDUixPQUFPLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxVQUFVO3dCQUMxQixPQUFPLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxXQUFXO3dCQUNoQyxPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxTQUFTO3dCQUMzQixPQUFPLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUM7Z0JBQ3pDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsR0FBRyxHQUFHLENBQUMsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLENBQUM7YUFDVjtZQUVELElBQUksT0FBTyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQzVCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRTtvQkFDOUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7d0JBQ25CLE1BQU0sTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7d0JBQ2hDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTs0QkFDbEIsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQzs0QkFDM0IsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQzlDLElBQUksR0FBRyxJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7Z0NBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDOzZCQUNoQzt3QkFDTCxDQUFDLENBQUM7d0JBQ0YsTUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDakM7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksT0FBTyxPQUFPLENBQUMsR0FBRyxLQUFLLFFBQVEsRUFBRTs0QkFDakQsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDM0U7d0JBQ0QsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUM7NEJBQ3ZHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0lBQStJLENBQUMsQ0FBQzt5QkFDbkw7NkJBQUk7NEJBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ3BDO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3hFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNwQzthQUNKO1NBQ0o7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUdNLE1BQU0sQ0FBQyxLQUFVO1FBQ3BCLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDckQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQzFDLENBQUM7SUFHTSxVQUFVLENBQUMsS0FBVTtRQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVNLHFCQUFxQixDQUFDLEtBQUs7UUFDOUIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsU0FBUztRQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsVUFBVSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSztRQUM1QixJQUFJLEtBQUssRUFBRTtZQUNQLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMzQjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELGlCQUFpQjtRQUNiLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQztRQUVsQixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUM3RixVQUFVLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDbEQsS0FBSyxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7WUFDcEIsTUFBTSxTQUFTLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQztZQUNwRCxJQUFJLFNBQVMsS0FBSyxXQUFXLElBQUksU0FBUyxLQUFLLFNBQVMsRUFBRTtnQkFDdEQsaURBQWlEO2FBQ3BEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUMzRCxJQUFJLE9BQU8sQ0FBQztZQUNaLE1BQU0sc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1lBRWxDLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ2YsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQzlCLFlBQVksR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pDO1lBRUQsS0FBSyxNQUFNLFFBQVEsSUFBSSxZQUFZLEVBQUU7Z0JBQ2pDLE9BQU8sR0FBRyxFQUFFLENBQUM7Z0JBQ2IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUM3QixPQUFPLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7Z0JBQzdCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDN0IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUM3QixPQUFPLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ2pDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztnQkFDdEMsT0FBTyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO2dCQUNwQyxPQUFPLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7Z0JBQ3RDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUN4QztZQUVELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWMsQ0FBQyxRQUFnQjtRQUMzQixPQUFPLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBQ0QsUUFBUTtRQUNKLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUN4RiwyQkFBMkI7UUFDM0IsbUNBQW1DO1FBQ25DLElBQUk7SUFDUixDQUFDO0lBRUQsZUFBZTtRQUNYLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUM1QjtJQUNMLENBQUM7Q0FFSixDQUFBOztZQXhYdUIsVUFBVTtZQUNFLG1CQUFtQjtZQUNoQixzQkFBc0I7WUFDdEMsU0FBUztZQUNELGNBQWM7O0FBckJoQztJQUFSLEtBQUssRUFBRTt3REFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7cURBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFO3dEQUFxQjtBQUNwQjtJQUFSLEtBQUssRUFBRTs2REFBMkI7QUFDMUI7SUFBUixLQUFLLEVBQUU7MkRBQXFCO0FBRW5CO0lBQVQsTUFBTSxFQUFFO3VEQUFzRTtBQUNyRTtJQUFULE1BQU0sRUFBRTt3REFBdUU7QUFzU2hGO0lBREMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lEQVNoQztBQUdEO0lBREMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FEQUdwQztBQTVUUSxtQkFBbUI7SUFOL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGlCQUFpQjtRQUMzQixvMklBQTJDOztLQUU5QyxDQUFDO0dBRVcsbUJBQW1CLENBMlkvQjtTQTNZWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBFbGVtZW50UmVmLCBIb3N0TGlzdGVuZXIsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRGaWxlQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Fzc2V0LmZpbGUuY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuXHJcbmRlY2xhcmUgY29uc3QgcWRzX290bW06IGFueTtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tZmlsZS11cGxvYWQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBmaWxlRm9ybWF0czogc3RyaW5nW107XHJcbiAgICBASW5wdXQoKSBtYXhGaWxlczogbnVtYmVyO1xyXG4gICAgQElucHV0KCkgbWF4RmlsZVNpemU6IG51bWJlcjtcclxuICAgIEBJbnB1dCgpIGlzUmV2aXNpb25VcGxvYWQ6IGJvb2xlYW47XHJcbiAgICBASW5wdXQoKSBmaWxlVG9SZXZpc2lvbjogYW55O1xyXG5cclxuICAgIEBPdXRwdXQoKSBwdWJsaWMgZmlsZXNBZGRlZDogRXZlbnRFbWl0dGVyPEZpbGVbXT4gPSBuZXcgRXZlbnRFbWl0dGVyPEZpbGVbXT4oKTtcclxuICAgIEBPdXRwdXQoKSBwdWJsaWMgcXVldWVkRmlsZXM6IEV2ZW50RW1pdHRlcjxGaWxlW10+ID0gbmV3IEV2ZW50RW1pdHRlcjxGaWxlW10+KCk7XHJcblxyXG4gICAgb3RtbUJhc2VVcmw6IHN0cmluZztcclxuICAgIHRodW1ibmFpbEZvcm1hdHMgPSBbJ2ltYWdlL2pwZWcnLCAnaW1hZ2UvcG5nJywgJ2ltYWdlL2pwZycsICdpbWFnZS9naWYnXTtcclxuXHJcbiAgICBwdWJsaWMgaHRtbEVsZW1lbnQ6IEhUTUxFbGVtZW50O1xyXG4gICAgc2VsZWN0ZWRGaWxlcyA9IFtdO1xyXG4gICAgaXNRRFNVcGxvYWQgPSBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZWxlbWVudDogRWxlbWVudFJlZixcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXNzZXRGaWxlQ29uZmlnU2VydmljZTogQXNzZXRGaWxlQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5odG1sRWxlbWVudCA9IHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIGdldFR5cGVPZihvYmopIHtcclxuICAgICAgICByZXR1cm4gdHlwZW9mIG9iajtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuUURTRmlsZUNob29zZXIoKSB7XHJcbiAgICAgICAgY29uc3QgdGhhdCA9IHRoaXM7XHJcbiAgICAgICAgcWRzX290bW0uY29ubmVjdG9yLm9wZW5GaWxlQ2hvb3NlcigoZmlsZUluZm9MaXN0KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBsZXQgZmlsZU9iajtcclxuICAgICAgICAgICAgY29uc3QgdHJhbnNmZXJGaWxlT2JqZWN0TGlzdCA9IFtdO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFmaWxlSW5mb0xpc3QpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KGZpbGVJbmZvTGlzdCkpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVJbmZvTGlzdCA9IFtmaWxlSW5mb0xpc3RdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGZpbGVJbmZvIG9mIGZpbGVJbmZvTGlzdCkge1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5uYW1lID0gZmlsZUluZm8ubmFtZTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoucGF0aCA9IGZpbGVJbmZvLnBhdGg7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnNpemUgPSBmaWxlSW5mby5zaXplO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai50eXBlID0gZmlsZUluZm8udHlwZTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoudXJsID0gZmlsZUluZm8udGh1bWJuYWlsO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5hc3NldFVybCA9IGZpbGVJbmZvLnRodW1ibmFpbDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoud2lkdGggPSBmaWxlSW5mby5pbWFnZVdpZHRoO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5oZWlnaHQgPSBmaWxlSW5mby5pbWFnZUhlaWdodDtcclxuICAgICAgICAgICAgICAgIHRyYW5zZmVyRmlsZU9iamVjdExpc3QucHVzaChmaWxlT2JqKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhhdC5wcm9jZXNzRmlsZVNlbGVjdGlvbih0cmFuc2ZlckZpbGVPYmplY3RMaXN0KTtcclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FsY3VsYXRlU2VsZWN0ZWRGaWxlU2l6ZSgpOiBudW1iZXIge1xyXG4gICAgICAgIGxldCBmaWxlU2l6ZSA9IDA7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5zZWxlY3RlZEZpbGVzKSAmJiB0aGlzLnNlbGVjdGVkRmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHNlbGVjdGVkRmlsZSBvZiB0aGlzLnNlbGVjdGVkRmlsZXMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZEZpbGUgJiYgc2VsZWN0ZWRGaWxlLnNpemVcclxuICAgICAgICAgICAgICAgICAgICAmJiBzZWxlY3RlZEZpbGUuc2l6ZSAhPSBudWxsICYmIHNlbGVjdGVkRmlsZS5zaXplID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbGVTaXplICs9IHNlbGVjdGVkRmlsZS5zaXplO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmaWxlU2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtYXRCeXRlcyhieXRlczogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAoYnl0ZXMgPT09IDApIHsgcmV0dXJuICcwIEJ5dGVzJzsgfVxyXG4gICAgICAgIGNvbnN0IGsgPSAxMDI0O1xyXG4gICAgICAgIGNvbnN0IGRtID0gMjtcclxuICAgICAgICBjb25zdCBzaXplcyA9IFsnQnl0ZXMnLCAnS0InLCAnTUInLCAnR0InLCAnVEInLCAnUEInLCAnRUInLCAnWkInLCAnWUInXTtcclxuICAgICAgICBjb25zdCBpID0gTWF0aC5mbG9vcihNYXRoLmxvZyhieXRlcykgLyBNYXRoLmxvZyhrKSk7XHJcbiAgICAgICAgcmV0dXJuIHBhcnNlRmxvYXQoKGJ5dGVzIC8gTWF0aC5wb3coaywgaSkpLnRvRml4ZWQoZG0pKSArICcgJyArIHNpemVzW2ldO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrRmlsZXNTaXplQ29zbnRyYWludChmaWxlczogRmlsZVtdKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm1heEZpbGVTaXplKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgdmFsaWRpdHkgPSB0cnVlO1xyXG4gICAgICAgIGxldCBjdXJyZW50U2VsZWN0ZWRGaWxlU2l6ZSA9IDA7XHJcbiAgICAgICAgaWYgKGZpbGVzICYmIGZpbGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGZpbGUgb2YgZmlsZXMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChmaWxlLnNpemUgJiYgZmlsZS5zaXplICE9IG51bGwgJiYgZmlsZS5zaXplID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRTZWxlY3RlZEZpbGVTaXplICs9IGZpbGUuc2l6ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoY3VycmVudFNlbGVjdGVkRmlsZVNpemUgPD0gdGhpcy5tYXhGaWxlU2l6ZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXhpc3RpbmdGaWxlU2l6ZSA9IHRoaXMuY2FsY3VsYXRlU2VsZWN0ZWRGaWxlU2l6ZSgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRTZWxlY3RlZEZpbGVTaXplICsgZXhpc3RpbmdGaWxlU2l6ZSA8PSB0aGlzLm1heEZpbGVTaXplKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRpdHkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGl0eSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFsaWRpdHkgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXZhbGlkaXR5KSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdDYW5ub3QgdXBsb2FkIGZpbGUocykgb2Ygc2l6ZSBtb3JlIHRoYW4gJyArICh0aGlzLm1heEZpbGVTaXplIC8gMTAwMCkgKyAnS0InKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbGlkaXR5O1xyXG4gICAgfVxyXG5cclxuICAgIGZpbHRlckZpbGVzKGN1cnJlbnRTZWxlY3RlZEZpbGVzLCBleGlzdGluZ0ZpbGVzKTogRmlsZVtdIHtcclxuICAgICAgICBjb25zdCB2YWxpZEZpbGVzID0gW107XHJcbiAgICAgICAgY29uc3QgZHVwbGljYXRlcyA9IFtdO1xyXG4gICAgICAgIGlmIChjdXJyZW50U2VsZWN0ZWRGaWxlcyAmJiBjdXJyZW50U2VsZWN0ZWRGaWxlcyAhPSBudWxsICYmIGN1cnJlbnRTZWxlY3RlZEZpbGVzLmxlbmd0aCAmJiBjdXJyZW50U2VsZWN0ZWRGaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgY3VycmVudFNlbGVjdGVkRmlsZSBvZiBjdXJyZW50U2VsZWN0ZWRGaWxlcykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGFscmVhZHlFeGlzdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudEZpbGUgPSBjdXJyZW50U2VsZWN0ZWRGaWxlO1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBleGlzdGluZ0ZpbGUgb2YgZXhpc3RpbmdGaWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50RmlsZS5uYW1lID09PSBleGlzdGluZ0ZpbGUubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbHJlYWR5RXhpc3QgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXBsaWNhdGVzLnB1c2goY3VycmVudFNlbGVjdGVkRmlsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghYWxyZWFkeUV4aXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRGaWxlcy5wdXNoKGN1cnJlbnRTZWxlY3RlZEZpbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkdXBsaWNhdGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdGaWxlIHdhcyBub3QgYWRkZWQgYmVjYXVzZSBpdHMgbmFtZSBtYXRjaGVzIGFuIGV4aXN0aW5nIGZpbGUnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdmFsaWRGaWxlcztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdmFsaWRGaWxlcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmlsZXNXZWJraXREYXRhVHJhbnNmZXJJdGVtcyhkYXRhVHJhbnNmZXJJdGVtcyk6IFByb21pc2U8RmlsZVtdPiB7XHJcbiAgICAgICAgY29uc3QgZmlsZXM6IEZpbGVbXSA9IFtdO1xyXG4gICAgICAgIGZ1bmN0aW9uIHRyYXZlcnNlRmlsZVRyZWVQcm9taXNlKGl0ZW0sIHBhdGggPSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXRlbS5pc0ZpbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtLmZpbGUoZmlsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGUuZmlsZXBhdGggPSBwYXRoICsgZmlsZS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlcy5wdXNoKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChpdGVtLmlzRGlyZWN0b3J5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGlyUmVhZGVyID0gaXRlbS5jcmVhdGVSZWFkZXIoKTtcclxuICAgICAgICAgICAgICAgICAgICBkaXJSZWFkZXIucmVhZEVudHJpZXMoZW50cmllcyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGVudHJpZXNQcm9taXNlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IGVudHIgb2YgZW50cmllcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cmllc1Byb21pc2VzLnB1c2godHJhdmVyc2VGaWxlVHJlZVByb21pc2UoZW50ciwgcGF0aCArIGl0ZW0ubmFtZSArICcvJykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoUHJvbWlzZS5hbGwoZW50cmllc1Byb21pc2VzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBlbnRyaWVzUHJvbWlzZXMgPSBbXTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBpdCBvZiBkYXRhVHJhbnNmZXJJdGVtcykge1xyXG4gICAgICAgICAgICAgICAgZW50cmllc1Byb21pc2VzLnB1c2godHJhdmVyc2VGaWxlVHJlZVByb21pc2UoaXQud2Via2l0R2V0QXNFbnRyeSgpKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgUHJvbWlzZS5hbGwoZW50cmllc1Byb21pc2VzKS50aGVuKGVudHJpZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZShmaWxlcyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZUZpbGVPYmooZmlsZSwgZGF0YSkge1xyXG4gICAgICAgIGlmIChmaWxlICYmIGZpbGUgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgZmlsZS5kYXRhID0gZGF0YS5zcGxpdCgnYmFzZTY0LCcpWzFdO1xyXG4gICAgICAgICAgICAgICAgZmlsZS51cmwgPSBkYXRhO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBmaWxlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjaGVja051bWJlck9mRmlsZUNvbnN0cmFpbnQoZmlsZXM6IEZpbGVbXSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICghdGhpcy5tYXhGaWxlcyB8fCBmaWxlcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCBjdXJyZW50U2VsZWN0ZWRGaWxlc0NvdW50ID0gZmlsZXMubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IGFscmVhZHlTZWxlY3RlZEZpbGVzQ291bnQgPSB0aGlzLnNlbGVjdGVkRmlsZXMubGVuZ3RoO1xyXG4gICAgICAgIGlmICh0aGlzLm1heEZpbGVzID09PSBhbHJlYWR5U2VsZWN0ZWRGaWxlc0NvdW50KSB7XHJcbiAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMubWF4RmlsZXMgPCAoY3VycmVudFNlbGVjdGVkRmlsZXNDb3VudCArIGFscmVhZHlTZWxlY3RlZEZpbGVzQ291bnQpKSB7XHJcbiAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFpc1ZhbGlkKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzUmV2aXNpb25VcGxvYWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdZb3UgY2FuIHNlbGVjdCBvbmx5IG1heGltdW0gb2YgJ1xyXG4gICAgICAgICAgICAgICAgICAgICsgdGhpcy5tYXhGaWxlcyArICcgZmlsZSBpbiBSZXZpc2lvbiBVcGxvYWQnKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChhbHJlYWR5U2VsZWN0ZWRGaWxlc0NvdW50ID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnWW91IGNhbiBzZWxlY3Qgb25seSBhIG1heGltdW0gJ1xyXG4gICAgICAgICAgICAgICAgICAgICsgdGhpcy5tYXhGaWxlcyArICcgZmlsZXMsIEJ1dCB5b3UgYXJlIHRyeWluZyB0byBzZWxlY3QgJyArIGN1cnJlbnRTZWxlY3RlZEZpbGVzQ291bnQgKyAnIGZpbGUocyknKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChhbHJlYWR5U2VsZWN0ZWRGaWxlc0NvdW50ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1lvdSBjYW4gc2VsZWN0IG1heGltdW0gb2YgJyArIHRoaXMubWF4RmlsZXNcclxuICAgICAgICAgICAgICAgICAgICArICcgZmlsZShzKSwgWW91IGhhdmUgYWxyZWFkeSBzZWxlY3RlZCAnICsgYWxyZWFkeVNlbGVjdGVkRmlsZXNDb3VudCArICcgZmlsZXMnKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdZb3UgeW91IGNhbiBzZWxlY3Qgb25seSBhIG1heGltdW0gJyArIHRoaXMubWF4RmlsZXMgKyAnIGZpbGVzJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWQ7XHJcbiAgICB9XHJcbiAgICBjaGVja0ZvclNwZWNpYWxDaGFyYWN0ZXIoZmlsZXM6IEZpbGVbXSkge1xyXG4gICAgICAgIGlmIChmaWxlcykge1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlc1dpdGhvdXRTcGVjaWFsQ2hhcmFjdGVyID0gW107XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGVzV2l0aFNwZWNpYWxDaGFyYWN0ZXIgPSBbXTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlSW5mbyBvZiBmaWxlcykge1xyXG4gICAgICAgICAgICAgICAgKGZpbGVJbmZvICYmIGZpbGVJbmZvLm5hbWUgJiYgKGZpbGVJbmZvLm5hbWUuaW5kZXhPZignXFwnJykgPj0gMCkpID9cclxuICAgICAgICAgICAgICAgICAgICBmaWxlc1dpdGhTcGVjaWFsQ2hhcmFjdGVyLnB1c2goZmlsZUluZm8pIDogZmlsZXNXaXRob3V0U3BlY2lhbENoYXJhY3Rlci5wdXNoKGZpbGVJbmZvKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZmlsZXNXaXRoU3BlY2lhbENoYXJhY3Rlci5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdUaGUgc3BlY2lhbCBjaGFyYWN0ZXIgJCBhbmQgXFwnIHdpbGwgYmUgcmVwbGFjZWQgd2l0aCBfLkRvIHlvdSB3YW50IHRvIGNvbnRpbnVlPycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzc0ZpbGVTZWxlY3Rpb24oWy4uLmZpbGVzV2l0aG91dFNwZWNpYWxDaGFyYWN0ZXIsIC4uLmZpbGVzV2l0aFNwZWNpYWxDaGFyYWN0ZXJdKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzc0ZpbGVTZWxlY3Rpb24oZmlsZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcHJvY2Vzc0ZpbGVTZWxlY3Rpb24oZmlsZXMpIHtcclxuICAgICAgICBpZiAoKHRoaXMuaXNRRFNVcGxvYWQgJiYgdGhpcy5jaGVja051bWJlck9mRmlsZUNvbnN0cmFpbnQoZmlsZXMpKSB8fFxyXG4gICAgICAgICAgICAodGhpcy5jaGVja051bWJlck9mRmlsZUNvbnN0cmFpbnQoZmlsZXMpICYmIHRoaXMuY2hlY2tGaWxlc1NpemVDb3NudHJhaW50KGZpbGVzKSkpIHtcclxuICAgICAgICAgICAgY29uc3QgdmFsaWRGaWxlcyA9IHRoaXMuZmlsdGVyRmlsZXMoZmlsZXMsIHRoaXMuc2VsZWN0ZWRGaWxlcyk7XHJcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlRmlsZVByb2Nlc3NpbmcodmFsaWRGaWxlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZUZpbGVQcm9jZXNzaW5nKHZhbGlkRmlsZXMpIHtcclxuICAgICAgICB0aGlzLmZpbGVzQWRkZWQubmV4dCh2YWxpZEZpbGVzKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHZhbGlkRmlsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsZU9iajogYW55ID0gdmFsaWRGaWxlc1tpXTtcclxuICAgICAgICAgICAgY29uc3Qgb2JqZWN0VXJsID0gZmlsZU9iai5hc3NldFVybDtcclxuXHJcbiAgICAgICAgICAgIGlmIChvYmplY3RVcmwgaW5zdGFuY2VvZiBQcm9taXNlKSB7XHJcbiAgICAgICAgICAgICAgICBvYmplY3RVcmxcclxuICAgICAgICAgICAgICAgICAgICAudGhlbihvYmogPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlT2JqLndpZHRoID0gb2JqLmltYWdlV2lkdGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlT2JqLmhlaWdodCA9IG9iai5pbWFnZUhlaWdodCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVPYmoudXJsID0gb2JqLnRodW1ibmFpbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVPYmouYXNzZXRVcmwgPSBvYmoudGh1bWJuYWlsO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdVbmFibGUgdG8gZ2VuZXJhdGUgdGh1bWJuYWlsIDonICsgZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpbGVPYmogJiYgZmlsZU9iaiAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50aHVtYm5haWxGb3JtYXRzLmluZGV4T2YoZmlsZU9iai50eXBlKSAhPT0gLTEgJiYgaSA8IDEwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLmlzUURTVXBsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0YSA9IHJlYWRlci5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBvYmogPSB0aGlzLmNyZWF0ZUZpbGVPYmooZmlsZU9iaiwgZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob2JqICYmIG9iaiAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlcy5wdXNoKG9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGVPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZmlsZU9iai51cmwgfHwgdHlwZW9mIGZpbGVPYmoudXJsID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZU9iai5pY29uID0gdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGVPYmoubmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5pc1FEU1VwbG9hZCAmJiBmaWxlT2JqLm5hbWUgJiYgKGZpbGVPYmoubmFtZS5pbmRleE9mKCckJykgPj0gMCB8fCBmaWxlT2JqLm5hbWUuaW5kZXhPZignXFwnJykgPj0gMCkpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKFwiU2VsZWN0ZWQgZmlsZShzKSBuYW1lIGhhcyBzcGVjaWFsIGNoYXJhY3RlcnMgZG9sbGFyKCQpIG9yIHNpbmdsZSBxdW90ZSgnKSB3aGljaCBpcyBub3QgYWxsb3dlZC4gUGxlYXNlIGNoYW5nZSB0aGUgZmlsZSBuYW1lIGFuZCB1cGxvYWQgYWdhaW4uXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlcy5wdXNoKGZpbGVPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlT2JqLmljb24gPSB0aGlzLmFzc2V0RmlsZUNvbmZpZ1NlcnZpY2UuZmluZEljb25CeU5hbWUoZmlsZU9iai5uYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZXMucHVzaChmaWxlT2JqKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnF1ZXVlZEZpbGVzLm5leHQodGhpcy5zZWxlY3RlZEZpbGVzKTtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdkcm9wJywgWyckZXZlbnQnXSlcclxuICAgIHB1YmxpYyBvbkRyb3AoZXZlbnQ6IGFueSk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgaXRlbXMgPSBldmVudC5kYXRhVHJhbnNmZXIuaXRlbXM7XHJcbiAgICAgICAgdGhpcy5nZXRGaWxlc1dlYmtpdERhdGFUcmFuc2Zlckl0ZW1zKGl0ZW1zKS50aGVuKGZpbGVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jaGVja0ZvclNwZWNpYWxDaGFyYWN0ZXIoZmlsZXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnJztcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdkcmFnb3ZlcicsIFsnJGV2ZW50J10pXHJcbiAgICBwdWJsaWMgb25Ecm9wT3ZlcihldmVudDogYW55KTogYW55IHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbkZpbGVTZWxlY3Rpb25DaGFuZ2UoZXZlbnQpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGZpbGVzID0gZXZlbnQudGFyZ2V0LmZpbGVzO1xyXG4gICAgICAgIHRoaXMuY2hlY2tGb3JTcGVjaWFsQ2hhcmFjdGVyKGZpbGVzKTtcclxuICAgICAgICBldmVudC50YXJnZXQudmFsdWUgPSAnJztcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVBbGwoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEZpbGVzID0gW107XHJcbiAgICAgICAgdGhpcy5xdWV1ZWRGaWxlcy5uZXh0KHRoaXMuc2VsZWN0ZWRGaWxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlRmlsZShmaWxlT2JqLCBpbmRleCwgZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIHRoaXMucXVldWVkRmlsZXMubmV4dCh0aGlzLnNlbGVjdGVkRmlsZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVFEU0Ryb3BBcmVhKCkge1xyXG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzO1xyXG5cclxuICAgICAgICBjb25zdCBkcm9wem9uZUVsID0gdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignZGl2LmFjcm9uLWZpbGUtdXBsb2FkLXdyYXBwZXInKTtcclxuICAgICAgICBkcm9wem9uZUVsLmFkZEV2ZW50TGlzdGVuZXIoJ3Fkc19kcmFnZHJvcCcsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBldmVudCA9IGV2ZW50IHx8IHt9O1xyXG4gICAgICAgICAgICBjb25zdCBldmVudFR5cGUgPSAoZXZlbnQuZGV0YWlsIHx8IHt9KS5xZHNFdmVudFR5cGU7XHJcbiAgICAgICAgICAgIGlmIChldmVudFR5cGUgPT09ICdkcmFnbGVhdmUnIHx8IGV2ZW50VHlwZSA9PT0gJ2FkZGZpbGUnKSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICQoZHJvcHpvbmVFbCkudHJpZ2dlcignZHJhZ0Ryb3BIaWRlV2luZG93Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcWRzX290bW0uY29ubmVjdG9yLmNyZWF0ZURyb3BBcmVhKGRyb3B6b25lRWwsIChmaWxlSW5mb0xpc3QpID0+IHtcclxuICAgICAgICAgICAgbGV0IGZpbGVPYmo7XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYW5zZmVyRmlsZU9iamVjdExpc3QgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIGlmICghZmlsZUluZm9MaXN0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShmaWxlSW5mb0xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlSW5mb0xpc3QgPSBbZmlsZUluZm9MaXN0XTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlSW5mbyBvZiBmaWxlSW5mb0xpc3QpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmogPSB7fTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoubmFtZSA9IGZpbGVJbmZvLm5hbWU7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnBhdGggPSBmaWxlSW5mby5wYXRoO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5zaXplID0gZmlsZUluZm8uc2l6ZTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoudHlwZSA9IGZpbGVJbmZvLnR5cGU7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnVybCA9IGZpbGVJbmZvLnRodW1ibmFpbDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmouYXNzZXRVcmwgPSBmaWxlSW5mby50aHVtYm5haWw7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLndpZHRoID0gZmlsZUluZm8uaW1hZ2VXaWR0aDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmouaGVpZ2h0ID0gZmlsZUluZm8uaW1hZ2VIZWlnaHQ7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2ZlckZpbGVPYmplY3RMaXN0LnB1c2goZmlsZU9iaik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoYXQucHJvY2Vzc0ZpbGVTZWxlY3Rpb24odHJhbnNmZXJGaWxlT2JqZWN0TGlzdCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVuYW1lRmlsZU5hbWUoZmlsZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGZpbGVOYW1lLnJlcGxhY2UobmV3IFJlZ0V4cCgnXFwnJyksICdfJykuc3BsaXQoJyQnKS5qb2luKCdfJyk7XHJcbiAgICB9XHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZXMgPSBbXTtcclxuICAgICAgICB0aGlzLm90bW1CYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICAgICAgLy8gaWYgKCF0aGlzLm1heEZpbGVTaXplKSB7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubWF4RmlsZVNpemUgPSAxNTAwMDAwMDtcclxuICAgICAgICAvLyB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzUURTVXBsb2FkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlUURTRHJvcEFyZWEoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==