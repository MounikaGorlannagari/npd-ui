import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
let LoaderService = class LoaderService {
    constructor() {
        this.loaderSubject = new Subject();
        this.loaderState = this.loaderSubject.asObservable();
    }
    show() {
        this.loaderSubject.next({ show: true });
    }
    hide() {
        this.loaderSubject.next({ show: false });
    }
};
LoaderService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(); }, token: LoaderService, providedIn: "root" });
LoaderService = __decorate([
    Injectable({
        providedIn: 'root',
    })
], LoaderService);
export { LoaderService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFPL0IsSUFBYSxhQUFhLEdBQTFCLE1BQWEsYUFBYTtJQUt0QjtRQUhPLGtCQUFhLEdBQUcsSUFBSSxPQUFPLEVBQWUsQ0FBQztRQUNsRCxnQkFBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxFQUFFLENBQUM7SUFFaEMsQ0FBQztJQUVqQixJQUFJO1FBQ0EsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFpQixDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELElBQUk7UUFDQSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQWlCLENBQUMsQ0FBQztJQUM1RCxDQUFDO0NBQ0osQ0FBQTs7QUFkWSxhQUFhO0lBSnpCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxhQUFhLENBY3pCO1NBZFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBMb2FkZXJTdGF0ZSB9IGZyb20gJy4vbG9hZGVyJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290JyxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBMb2FkZXJTZXJ2aWNlIHtcclxuXHJcbiAgICBwdWJsaWMgbG9hZGVyU3ViamVjdCA9IG5ldyBTdWJqZWN0PExvYWRlclN0YXRlPigpO1xyXG4gICAgbG9hZGVyU3RhdGUgPSB0aGlzLmxvYWRlclN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICBzaG93KCkge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU3ViamVjdC5uZXh0KHsgc2hvdzogdHJ1ZSB9IGFzIExvYWRlclN0YXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU3ViamVjdC5uZXh0KHsgc2hvdzogZmFsc2UgfSBhcyBMb2FkZXJTdGF0ZSk7XHJcbiAgICB9XHJcbn1cclxuIl19