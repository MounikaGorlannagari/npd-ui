import { __decorate } from "tslib";
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { LoaderService } from './loader.service';
let LoaderComponent = class LoaderComponent {
    constructor(loaderService, changeDetectorRef) {
        this.loaderService = loaderService;
        this.changeDetectorRef = changeDetectorRef;
        this.show = false;
    }
    ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state) => {
            this.show = state.show;
            this.changeDetectorRef.detectChanges();
        });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
};
LoaderComponent.ctorParameters = () => [
    { type: LoaderService },
    { type: ChangeDetectorRef }
];
LoaderComponent = __decorate([
    Component({
        selector: 'mpm-loader',
        template: "<div class=\"loading-wrapper\" [class.loader-hidden]=\"!show\">\r\n    <div class=\"loading-holder\">\r\n        <mat-spinner color=\"primary\" [diameter]=\"70\"></mat-spinner>\r\n    </div>\r\n</div>",
        styles: [".loading-wrapper{top:0;left:0;display:block;position:fixed;width:100%;height:100%;align-items:center;justify-content:center;z-index:10000}.loading-wrapper .loading-holder{position:fixed;top:50%;left:50%;margin-top:-70px;margin-left:-70px}.loading-wrapper .loading-holder img,.loading-wrapper .loading-holder mat-spinner,.loading-wrapper .loading-holder p{position:absolute;display:inline-block}.loading-wrapper .loading-holder mat-spinner{height:100px;width:100px}.loading-wrapper .loading-holder img{height:40px;width:40px;margin:28px 0 0 28px}.loading-wrapper .loading-holder p{margin:110px 0 0 15px}.loader-hidden{display:none}"]
    })
], LoaderComponent);
export { LoaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2xvYWRlci9sb2FkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHaEYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBUWpELElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUFLeEIsWUFDVyxhQUE0QixFQUM1QixpQkFBb0M7UUFEcEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUwvQyxTQUFJLEdBQUcsS0FBSyxDQUFDO0lBTVQsQ0FBQztJQUVMLFFBQVE7UUFDSixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVzthQUM3QyxTQUFTLENBQUMsQ0FBQyxLQUFrQixFQUFFLEVBQUU7WUFDOUIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0NBRUosQ0FBQTs7WUFoQjZCLGFBQWE7WUFDVCxpQkFBaUI7O0FBUHRDLGVBQWU7SUFOM0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLFlBQVk7UUFDdEIsb05BQXNDOztLQUV6QyxDQUFDO0dBRVcsZUFBZSxDQXNCM0I7U0F0QlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBMb2FkZXJTdGF0ZSB9IGZyb20gJy4vbG9hZGVyJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4vbG9hZGVyLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1sb2FkZXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvYWRlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9sb2FkZXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIExvYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICBzaG93ID0gZmFsc2U7XHJcbiAgICBwdWJsaWMgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMubG9hZGVyU2VydmljZS5sb2FkZXJTdGF0ZVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChzdGF0ZTogTG9hZGVyU3RhdGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvdyA9IHN0YXRlLnNob3c7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19