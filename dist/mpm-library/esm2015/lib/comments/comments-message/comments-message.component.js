import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { CommentsService } from '../services/comments.service';
let CommentsMessageComponent = class CommentsMessageComponent {
    constructor(commentUtilService, sharingService, commentsService) {
        this.commentUtilService = commentUtilService;
        this.sharingService = sharingService;
        this.commentsService = commentsService;
        this.replyToComment = new EventEmitter();
        this.clickingOnReplyMessage = new EventEmitter();
        this.commentTextTemp = null;
        this.currUserId = null;
        this.arrayInfo = [];
        this.reasonInfo = [];
        this.restartReasonInfo = [];
        this.currUserId = this.sharingService.getCurrentUserItemID();
    }
    ngOnInit() {
        this.hover = false;
        this.commentTextTemp = this.handleUserInfoDetailsTemp(this.commentData.commentText, this.userListData.map(data => data.displayName));
    }
    onclickToReply(data) {
        if (this.replyToComment && typeof this.replyToComment.emit === 'function') {
            this.replyToComment.emit(data);
        }
    }
    toggleEvent(event) {
        this.hover = event;
    }
    handleDateManipulation(dateString) {
        if (!dateString) {
            return '';
        }
        if (dateString && !(dateString instanceof Date)) {
            dateString = new Date(dateString);
        }
        let numericValueDiff = 0;
        numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'day');
        if (numericValueDiff < 1) {
            numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours');
            if (numericValueDiff < 1) {
                return Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'minutes')) + ' mins ago.';
            }
            else {
                return Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours')) + ' hours ago.';
            }
        }
        return this.commentUtilService.getDateByGiveFormat(dateString, 'MMM D Y, h:mm a');
    }
    onParentCommentNavigation(parentCommentId) {
        this.clickingOnReplyMessage.emit(parentCommentId);
    }
    handleUserInfoDetailsTemp(commentText, displayName) {
        if (this.commentData.commentId) {
            this.reasonInfo = [];
            // removed for code hardening
            //    this.commentsService.getReasonsByCommentId(this.commentData.commentId).subscribe(reasonResponse => {
            /*    console.log(reasonResponse);
               const reasons = reasonResponse && reasonResponse.MPM_Status_Reason ? reasonResponse.MPM_Status_Reason : null;
               if (reasons) {
                 if (Array.isArray(reasons)) {
                   reasons.forEach(reason => {
                     this.reasonInfo.push(reason.NAME);
                   });
                 } else {
                   this.reasonInfo.push(reasons.NAME);
                 }
               }
               console.log(this.reasonInfo); */
            if (!commentText) {
                return 'SUCCESS';
            }
            const positionofAt = commentText.indexOf('@');
            if (positionofAt >= 0) {
                this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
                commentText = commentText.substr(positionofAt + 1);
                let isUserThere = false;
                displayName.forEach(data => {
                    if (commentText.indexOf(data) === 0) {
                        isUserThere = true;
                        this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
                        commentText = commentText.substr(data.length);
                    }
                });
                if (!isUserThere) {
                    this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
                }
            }
            else {
                if (commentText && commentText.split(':') && commentText.split(':').length > 1) {
                    this.restartReasonInfo = [];
                    this.arrayInfo.push({ text: commentText.split(':')[1], isUser: false });
                    let reasons;
                    reasons = commentText.split(':') && commentText.split(':')[0] && commentText.split(':')[0].split(',') ? commentText.split(':')[0].split(',') : '';
                    if (reasons) {
                        reasons.forEach(reason => {
                            this.restartReasonInfo.push(reason);
                        });
                    }
                    commentText = '';
                }
                else {
                    this.arrayInfo.push({ text: commentText, isUser: false });
                    commentText = '';
                }
            }
            return this.handleUserInfoDetailsTemp(commentText, displayName);
            //  });
        }
        /*    if (!commentText) {
             return 'SUCCESS';
           }
           const positionofAt = commentText.indexOf('@');
           if (positionofAt >= 0) {
             this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
             commentText = commentText.substr(positionofAt + 1);
             let isUserThere = false;
             displayName.forEach(data => {
               if (commentText.indexOf(data) === 0) {
                 isUserThere = true;
                 this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
                 commentText = commentText.substr(data.length);
               }
             });
             if (!isUserThere) {
               this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
             }
           } else {
             this.arrayInfo.push({ text: commentText, isUser: false });
             commentText = '';
           }
           return this.handleUserInfoDetailsTemp(commentText, displayName); */
    }
};
CommentsMessageComponent.ctorParameters = () => [
    { type: CommentsUtilService },
    { type: SharingService },
    { type: CommentsService }
];
__decorate([
    Input()
], CommentsMessageComponent.prototype, "commentData", void 0);
__decorate([
    Input()
], CommentsMessageComponent.prototype, "isReply", void 0);
__decorate([
    Input()
], CommentsMessageComponent.prototype, "userListData", void 0);
__decorate([
    Output()
], CommentsMessageComponent.prototype, "replyToComment", void 0);
__decorate([
    Output()
], CommentsMessageComponent.prototype, "clickingOnReplyMessage", void 0);
CommentsMessageComponent = __decorate([
    Component({
        selector: 'mpm-comments-message',
        template: "<mat-card class=\"comment-message-item \" *ngIf=\"commentData\" [ngClass]=\"{'comment-parent-item': !isReply}\"\r\n    (mouseover)=\"toggleEvent(true)\" (mouseleave)=\"toggleEvent(false)\" id=\"message_{{commentData.commentId}}\">\r\n    <mat-card-header class=\"comment-header\">\r\n        <!-- <div mat-card-avatar class=\"comment-header-image\">\r\n            <span>{{commentData.userInfo.displayName | uppercase | slice:0:2 }}</span>\r\n        </div> -->\r\n        <mat-card-title class=\"comment-header-title\">\r\n            <span\r\n                class=\"comment-user\">{{currUserId != commentData.userInfo.id ? commentData.userInfo.displayName : 'Me'}}</span>\r\n            <span class=\"comment-date\" *ngIf=\"isReply === true\"\r\n                matTooltip=\"{{commentData.timeStamp | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(commentData.timeStamp)}}</span>\r\n            <span>\r\n                <mat-chip class=\"status-chip\" *ngIf=\"commentData.projectStatus\">{{commentData.projectStatus}}\r\n                </mat-chip>\r\n            </span>\r\n        </mat-card-title>\r\n    </mat-card-header>\r\n\r\n    <mat-card-content class=\"reply-comment-item\" *ngIf=\"commentData.refComment\">\r\n        <p (click)=\"onParentCommentNavigation(commentData.refComment.refCommentId)\">\r\n            {{commentData.refComment.refCommentText}}\r\n        </p>\r\n    </mat-card-content>\r\n    <mat-card-content class=\"original-comment-item\">\r\n        <p class=\"original-comment-item-p\">\r\n            <!-- *ngIf=\"commentTextTemp\" -->\r\n            <span *ngFor=\"let text of arrayInfo\"\r\n                [ngClass]=\"{'user-info-container': text.isUser}\">{{text.isUser?'@'+text.text:text.text}}</span>\r\n        </p>\r\n    </mat-card-content>\r\n\r\n    <mat-card-content class=\"original-comment-item\">\r\n\r\n        <mat-label style=\"font-weight: bold;\" *ngIf=\"restartReasonInfo.length > 0\">Reasons</mat-label>\r\n            <mat-option class=\"matOption\" *ngFor=\"let reason of restartReasonInfo\" value=\"{{reason}}\">\r\n                <span matTooltip=\"{{reason}}\">{{reason}}</span>\r\n            </mat-option>\r\n</mat-card-content>\r\n\r\n    <mat-card-content class=\"original-comment-item\">\r\n\r\n            <mat-label style=\"font-weight: bold;\" *ngIf=\"reasonInfo.length > 0\">Reasons</mat-label>\r\n                <mat-option class=\"matOption\" *ngFor=\"let reason of reasonInfo\" value=\"{{reason}}\">\r\n                    <span matTooltip=\"{{reason}}\">{{reason}}</span>\r\n                </mat-option>\r\n    </mat-card-content>\r\n\r\n    <div *ngIf=\"hover === true\" class=\"comment-message-item-action\">\r\n        <mat-icon (click)=\"onclickToReply(commentData)\"\r\n            matTooltip=\"{{isReply === true ? 'Reply to this message' : 'Remove this message'}}\" class=\"active-icon\">\r\n            {{isReply === true ? 'reply' : 'highlight_off'}}</mat-icon>\r\n    </div>\r\n</mat-card>",
        styles: [".comment-message-item{padding:5px 15px 5px 10px;margin:10px 10px 0 1px;position:relative;max-width:90%}.comment-message-item mat-card-title{margin-bottom:5px;font-size:14px;margin-top:10px}.comment-message-item mat-card-title .comment-date{color:rgba(0,0,0,.54);font-size:12px;padding:8px 10px;font-weight:400;cursor:default}.comment-message-item .active-icon{padding:0;cursor:pointer}.comment-message-item .comment-message-item-action{position:absolute;right:1%;padding:5px;margin:0;z-index:999;top:0;cursor:pointer}.comment-message-item mat-card-content{margin-left:18px;margin-top:5px}.comment-message-item .original-comment-item{margin-bottom:5px;overflow-wrap:anywhere}.comment-message-item .original-comment-item p{white-space:pre-wrap;line-height:20px}.comment-message-item .original-comment-item p span.user-info-container{color:rgba(0,0,0,.87);padding:2px 8px 3px;border-radius:16px;align-items:center;cursor:default;position:relative;font-weight:500}.comment-message-item .comment-header{position:relative}.comment-message-item .comment-header-image{background-size:cover}.comment-message-item .comment-header-image span{position:absolute;left:.7%;top:18%;font-size:20px}.comment-parent-item{position:relative;border-radius:5px 5px 0 0;margin-right:1px!important;background-color:#efefef;max-width:100%!important;padding-right:0}.comment-parent-item .reply-comment-item{background-color:#fff}.comment-user{cursor:default}.reply-comment-item{margin-bottom:5px;background-color:#efefef70;padding:2px 10px;margin-left:18px!important;border-radius:10px;min-height:28px;max-height:28px;display:flex;align-items:center;overflow:hidden;flex-grow:1;width:92%}.reply-comment-item p{font-size:13.2px;overflow:hidden;flex-grow:1;width:200px;max-height:20px;text-overflow:ellipsis;white-space:nowrap}.matOption{height:25px!important;cursor:auto}.status-chip{display:inline-block;padding:6px;border-radius:10px}"]
    })
], CommentsMessageComponent);
export { CommentsMessageComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbWVzc2FnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50cy9jb21tZW50cy1tZXNzYWdlL2NvbW1lbnRzLW1lc3NhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUxRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFRL0QsSUFBYSx3QkFBd0IsR0FBckMsTUFBYSx3QkFBd0I7SUFZbkMsWUFDUyxrQkFBdUMsRUFDdkMsY0FBOEIsRUFDOUIsZUFBZ0M7UUFGaEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUN2QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBWC9CLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN6QywyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzNELG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFFbEIsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFDaEIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO1FBTXJCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQy9ELENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUN2SSxDQUFDO0lBQ0QsY0FBYyxDQUFDLElBQUk7UUFDakIsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO1lBQ3pFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUNELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUNELHNCQUFzQixDQUFDLFVBQVU7UUFDL0IsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNmLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFDRCxJQUFJLFVBQVUsSUFBSSxDQUFDLENBQUMsVUFBVSxZQUFZLElBQUksQ0FBQyxFQUFFO1lBQy9DLFVBQVUsR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNuQztRQUNELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLGdCQUFnQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNqRyxJQUFJLGdCQUFnQixHQUFHLENBQUMsRUFBRTtZQUN4QixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbkcsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUM7YUFDckg7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxHQUFHLGFBQWEsQ0FBQzthQUNwSDtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDcEYsQ0FBQztJQUNELHlCQUF5QixDQUFDLGVBQWU7UUFDdkMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQseUJBQXlCLENBQUMsV0FBbUIsRUFBRSxXQUF1QjtRQUNwRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFO1lBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBQ3JCLDZCQUE2QjtZQUM3QiwwR0FBMEc7WUFDMUc7Ozs7Ozs7Ozs7OytDQVdtQztZQUVuQyxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNoQixPQUFPLFNBQVMsQ0FBQzthQUNsQjtZQUNELE1BQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDOUMsSUFBSSxZQUFZLElBQUksQ0FBQyxFQUFFO2dCQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7Z0JBQ3BGLFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUN4QixXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUN6QixJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNuQyxXQUFXLEdBQUcsSUFBSSxDQUFDO3dCQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ2hGLFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQkFDL0M7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQztpQkFDekc7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDOUUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztvQkFDNUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztvQkFFeEUsSUFBSSxPQUFPLENBQUM7b0JBQ1osT0FBTyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDbEosSUFBSSxPQUFPLEVBQUU7d0JBQ1gsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTs0QkFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDdEMsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsV0FBVyxHQUFHLEVBQUUsQ0FBQztpQkFFbEI7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO29CQUMxRCxXQUFXLEdBQUcsRUFBRSxDQUFDO2lCQUNsQjthQUNGO1lBQ0QsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQ2hFLE9BQU87U0FDUjtRQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzhFQXNCc0U7SUFDeEUsQ0FBQztDQUNGLENBQUE7O1lBNUg4QixtQkFBbUI7WUFDdkIsY0FBYztZQUNiLGVBQWU7O0FBZGhDO0lBQVIsS0FBSyxFQUFFOzZEQUE0QjtBQUMzQjtJQUFSLEtBQUssRUFBRTt5REFBa0I7QUFDakI7SUFBUixLQUFLLEVBQUU7OERBQW9DO0FBQ2xDO0lBQVQsTUFBTSxFQUFFO2dFQUEwQztBQUN6QztJQUFULE1BQU0sRUFBRTt3RUFBa0Q7QUFMaEQsd0JBQXdCO0lBTHBDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxzQkFBc0I7UUFDaEMsazdGQUFnRDs7S0FFakQsQ0FBQztHQUNXLHdCQUF3QixDQXlJcEM7U0F6SVksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlckluZm9Nb2RhbCwgQ29tbWVudHNNb2RhbCB9IGZyb20gJy4uL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmltcG9ydCB7IENvbW1lbnRzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2NvbW1lbnRzLnNlcnZpY2UnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWNvbW1lbnRzLW1lc3NhZ2UnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50cy1tZXNzYWdlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50cy1tZXNzYWdlLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzTWVzc2FnZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgY29tbWVudERhdGE6IENvbW1lbnRzTW9kYWw7XHJcbiAgQElucHV0KCkgaXNSZXBseTogYm9vbGVhbjtcclxuICBASW5wdXQoKSB1c2VyTGlzdERhdGE6IEFycmF5PFVzZXJJbmZvTW9kYWw+O1xyXG4gIEBPdXRwdXQoKSByZXBseVRvQ29tbWVudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBjbGlja2luZ09uUmVwbHlNZXNzYWdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgY29tbWVudFRleHRUZW1wID0gbnVsbDtcclxuICBjdXJyVXNlcklkID0gbnVsbDtcclxuICBob3ZlcjogYm9vbGVhbjtcclxuICBhcnJheUluZm8gPSBbXTtcclxuICByZWFzb25JbmZvID0gW107XHJcbiAgcmVzdGFydFJlYXNvbkluZm8gPSBbXTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGNvbW1lbnRzU2VydmljZTogQ29tbWVudHNTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLmN1cnJVc2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuaG92ZXIgPSBmYWxzZTtcclxuICAgIHRoaXMuY29tbWVudFRleHRUZW1wID0gdGhpcy5oYW5kbGVVc2VySW5mb0RldGFpbHNUZW1wKHRoaXMuY29tbWVudERhdGEuY29tbWVudFRleHQsIHRoaXMudXNlckxpc3REYXRhLm1hcChkYXRhID0+IGRhdGEuZGlzcGxheU5hbWUpKTtcclxuICB9XHJcbiAgb25jbGlja1RvUmVwbHkoZGF0YSkge1xyXG4gICAgaWYgKHRoaXMucmVwbHlUb0NvbW1lbnQgJiYgdHlwZW9mIHRoaXMucmVwbHlUb0NvbW1lbnQuZW1pdCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICB0aGlzLnJlcGx5VG9Db21tZW50LmVtaXQoZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHRvZ2dsZUV2ZW50KGV2ZW50KSB7XHJcbiAgICB0aGlzLmhvdmVyID0gZXZlbnQ7XHJcbiAgfVxyXG4gIGhhbmRsZURhdGVNYW5pcHVsYXRpb24oZGF0ZVN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBpZiAoIWRhdGVTdHJpbmcpIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gICAgaWYgKGRhdGVTdHJpbmcgJiYgIShkYXRlU3RyaW5nIGluc3RhbmNlb2YgRGF0ZSkpIHtcclxuICAgICAgZGF0ZVN0cmluZyA9IG5ldyBEYXRlKGRhdGVTdHJpbmcpO1xyXG4gICAgfVxyXG4gICAgbGV0IG51bWVyaWNWYWx1ZURpZmYgPSAwO1xyXG4gICAgbnVtZXJpY1ZhbHVlRGlmZiA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ2RheScpO1xyXG4gICAgaWYgKG51bWVyaWNWYWx1ZURpZmYgPCAxKSB7XHJcbiAgICAgIG51bWVyaWNWYWx1ZURpZmYgPSB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXREYXlzQmV0d2VlblR3b0RhdGVzKG5ldyBEYXRlKCksIGRhdGVTdHJpbmcsICdob3VycycpO1xyXG4gICAgICBpZiAobnVtZXJpY1ZhbHVlRGlmZiA8IDEpIHtcclxuICAgICAgICByZXR1cm4gTWF0aC5mbG9vcih0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXREYXlzQmV0d2VlblR3b0RhdGVzKG5ldyBEYXRlKCksIGRhdGVTdHJpbmcsICdtaW51dGVzJykpICsgJyBtaW5zIGFnby4nO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ2hvdXJzJykpICsgJyBob3VycyBhZ28uJztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldERhdGVCeUdpdmVGb3JtYXQoZGF0ZVN0cmluZywgJ01NTSBEIFksIGg6bW0gYScpO1xyXG4gIH1cclxuICBvblBhcmVudENvbW1lbnROYXZpZ2F0aW9uKHBhcmVudENvbW1lbnRJZCkge1xyXG4gICAgdGhpcy5jbGlja2luZ09uUmVwbHlNZXNzYWdlLmVtaXQocGFyZW50Q29tbWVudElkKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZVVzZXJJbmZvRGV0YWlsc1RlbXAoY29tbWVudFRleHQ6IHN0cmluZywgZGlzcGxheU5hbWU6IEFycmF5PGFueT4pIHtcclxuICAgIGlmICh0aGlzLmNvbW1lbnREYXRhLmNvbW1lbnRJZCkge1xyXG4gICAgICB0aGlzLnJlYXNvbkluZm8gPSBbXTtcclxuICAgICAgLy8gcmVtb3ZlZCBmb3IgY29kZSBoYXJkZW5pbmdcclxuICAgICAgLy8gICAgdGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0UmVhc29uc0J5Q29tbWVudElkKHRoaXMuY29tbWVudERhdGEuY29tbWVudElkKS5zdWJzY3JpYmUocmVhc29uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAvKiAgICBjb25zb2xlLmxvZyhyZWFzb25SZXNwb25zZSk7XHJcbiAgICAgICAgIGNvbnN0IHJlYXNvbnMgPSByZWFzb25SZXNwb25zZSAmJiByZWFzb25SZXNwb25zZS5NUE1fU3RhdHVzX1JlYXNvbiA/IHJlYXNvblJlc3BvbnNlLk1QTV9TdGF0dXNfUmVhc29uIDogbnVsbDtcclxuICAgICAgICAgaWYgKHJlYXNvbnMpIHtcclxuICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShyZWFzb25zKSkge1xyXG4gICAgICAgICAgICAgcmVhc29ucy5mb3JFYWNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICAgICAgIHRoaXMucmVhc29uSW5mby5wdXNoKHJlYXNvbi5OQU1FKTtcclxuICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICB0aGlzLnJlYXNvbkluZm8ucHVzaChyZWFzb25zLk5BTUUpO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgICAgfVxyXG4gICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnJlYXNvbkluZm8pOyAqL1xyXG5cclxuICAgICAgaWYgKCFjb21tZW50VGV4dCkge1xyXG4gICAgICAgIHJldHVybiAnU1VDQ0VTUyc7XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgcG9zaXRpb25vZkF0ID0gY29tbWVudFRleHQuaW5kZXhPZignQCcpO1xyXG4gICAgICBpZiAocG9zaXRpb25vZkF0ID49IDApIHtcclxuICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQuc3Vic3RyKDAsIChwb3NpdGlvbm9mQXQpKSwgaXNVc2VyOiBmYWxzZSB9KTtcclxuICAgICAgICBjb21tZW50VGV4dCA9IGNvbW1lbnRUZXh0LnN1YnN0cihwb3NpdGlvbm9mQXQgKyAxKTtcclxuICAgICAgICBsZXQgaXNVc2VyVGhlcmUgPSBmYWxzZTtcclxuICAgICAgICBkaXNwbGF5TmFtZS5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICAgICAgaWYgKGNvbW1lbnRUZXh0LmluZGV4T2YoZGF0YSkgPT09IDApIHtcclxuICAgICAgICAgICAgaXNVc2VyVGhlcmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQuc3Vic3RyKDAsIGRhdGEubGVuZ3RoKSwgaXNVc2VyOiB0cnVlIH0pO1xyXG4gICAgICAgICAgICBjb21tZW50VGV4dCA9IGNvbW1lbnRUZXh0LnN1YnN0cihkYXRhLmxlbmd0aCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKCFpc1VzZXJUaGVyZSkge1xyXG4gICAgICAgICAgdGhpcy5hcnJheUluZm9bdGhpcy5hcnJheUluZm8ubGVuZ3RoIC0gMV0udGV4dCA9ICh0aGlzLmFycmF5SW5mb1t0aGlzLmFycmF5SW5mby5sZW5ndGggLSAxXS50ZXh0ICsgJ0AnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKGNvbW1lbnRUZXh0ICYmIGNvbW1lbnRUZXh0LnNwbGl0KCc6JykgJiYgY29tbWVudFRleHQuc3BsaXQoJzonKS5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICB0aGlzLnJlc3RhcnRSZWFzb25JbmZvID0gW107XHJcbiAgICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQuc3BsaXQoJzonKVsxXSwgaXNVc2VyOiBmYWxzZSB9KTtcclxuXHJcbiAgICAgICAgICBsZXQgcmVhc29ucztcclxuICAgICAgICAgIHJlYXNvbnMgPSBjb21tZW50VGV4dC5zcGxpdCgnOicpICYmIGNvbW1lbnRUZXh0LnNwbGl0KCc6JylbMF0gJiYgY29tbWVudFRleHQuc3BsaXQoJzonKVswXS5zcGxpdCgnLCcpID8gY29tbWVudFRleHQuc3BsaXQoJzonKVswXS5zcGxpdCgnLCcpIDogJyc7XHJcbiAgICAgICAgICBpZiAocmVhc29ucykge1xyXG4gICAgICAgICAgICByZWFzb25zLmZvckVhY2gocmVhc29uID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLnJlc3RhcnRSZWFzb25JbmZvLnB1c2gocmVhc29uKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjb21tZW50VGV4dCA9ICcnO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5hcnJheUluZm8ucHVzaCh7IHRleHQ6IGNvbW1lbnRUZXh0LCBpc1VzZXI6IGZhbHNlIH0pO1xyXG4gICAgICAgICAgY29tbWVudFRleHQgPSAnJztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMuaGFuZGxlVXNlckluZm9EZXRhaWxzVGVtcChjb21tZW50VGV4dCwgZGlzcGxheU5hbWUpO1xyXG4gICAgICAvLyAgfSk7XHJcbiAgICB9XHJcbiAgICAvKiAgICBpZiAoIWNvbW1lbnRUZXh0KSB7XHJcbiAgICAgICAgIHJldHVybiAnU1VDQ0VTUyc7XHJcbiAgICAgICB9XHJcbiAgICAgICBjb25zdCBwb3NpdGlvbm9mQXQgPSBjb21tZW50VGV4dC5pbmRleE9mKCdAJyk7XHJcbiAgICAgICBpZiAocG9zaXRpb25vZkF0ID49IDApIHtcclxuICAgICAgICAgdGhpcy5hcnJheUluZm8ucHVzaCh7IHRleHQ6IGNvbW1lbnRUZXh0LnN1YnN0cigwLCAocG9zaXRpb25vZkF0KSksIGlzVXNlcjogZmFsc2UgfSk7XHJcbiAgICAgICAgIGNvbW1lbnRUZXh0ID0gY29tbWVudFRleHQuc3Vic3RyKHBvc2l0aW9ub2ZBdCArIDEpO1xyXG4gICAgICAgICBsZXQgaXNVc2VyVGhlcmUgPSBmYWxzZTtcclxuICAgICAgICAgZGlzcGxheU5hbWUuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICBpZiAoY29tbWVudFRleHQuaW5kZXhPZihkYXRhKSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgaXNVc2VyVGhlcmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgdGhpcy5hcnJheUluZm8ucHVzaCh7IHRleHQ6IGNvbW1lbnRUZXh0LnN1YnN0cigwLCBkYXRhLmxlbmd0aCksIGlzVXNlcjogdHJ1ZSB9KTtcclxuICAgICAgICAgICAgIGNvbW1lbnRUZXh0ID0gY29tbWVudFRleHQuc3Vic3RyKGRhdGEubGVuZ3RoKTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgICAgICBpZiAoIWlzVXNlclRoZXJlKSB7XHJcbiAgICAgICAgICAgdGhpcy5hcnJheUluZm9bdGhpcy5hcnJheUluZm8ubGVuZ3RoIC0gMV0udGV4dCA9ICh0aGlzLmFycmF5SW5mb1t0aGlzLmFycmF5SW5mby5sZW5ndGggLSAxXS50ZXh0ICsgJ0AnKTtcclxuICAgICAgICAgfVxyXG4gICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgdGhpcy5hcnJheUluZm8ucHVzaCh7IHRleHQ6IGNvbW1lbnRUZXh0LCBpc1VzZXI6IGZhbHNlIH0pO1xyXG4gICAgICAgICBjb21tZW50VGV4dCA9ICcnO1xyXG4gICAgICAgfVxyXG4gICAgICAgcmV0dXJuIHRoaXMuaGFuZGxlVXNlckluZm9EZXRhaWxzVGVtcChjb21tZW50VGV4dCwgZGlzcGxheU5hbWUpOyAqL1xyXG4gIH1cclxufVxyXG4iXX0=