import { __decorate } from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { CommentsService } from './services/comments.service';
import { CommentsUtilService } from './services/comments.util.service';
import { forkJoin } from 'rxjs';
import { ProjectService } from '../project/shared/services/project.service';
import { LoaderService } from '../loader/loader.service';
import { NotificationService } from '../notification/notification.service';
import { DeliverableService } from '../project/tasks/deliverable/deliverable.service';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { UtilService } from '../mpm-utils/services/util.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
let CommentsComponent = class CommentsComponent {
    constructor(commentsService, commentUtilService, projectService, loaderService, notification, deliverableService, sharingService, utilService, activatedRoute, route) {
        this.commentsService = commentsService;
        this.commentUtilService = commentUtilService;
        this.projectService = projectService;
        this.loaderService = loaderService;
        this.notification = notification;
        this.deliverableService = deliverableService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.activatedRoute = activatedRoute;
        this.route = route;
        this.commentsConfig = null;
        this.titleValue = null;
        this.isScrollDown = false;
        this.userListData = [];
        this.isReadonly = false;
        this.projectOwerId = null;
        this.isExternalUser = false;
        this.teamId = null;
        this.checked = false;
        this.deliverableIdFromUrl = 0;
        this.formNotification = false;
        this.commentsService.clearUserProjectDeliverableMapper();
    }
    initComments() {
        if (this.projectId) {
            this.commentsConfig = {
                commentList: [],
                deliverableList: [],
                pageDetails: this.commentUtilService.resetPageDetails(),
            };
            this.projectService.getProjectById((this.projectId + '')).subscribe(response => {
                let tempModal = null;
                tempModal = {
                    titleName: this.commentUtilService.checkValueIsNullOrEmpty(response['PROJECT_NAME'], ''),
                    icon: 'fiber_manual_record',
                    isActive: this.isManager && this.deliverableIdFromUrl == 0 ? true : false,
                    isProject: true,
                    projectId: this.projectId,
                    deliverableId: null,
                    status: this.commentUtilService.getStatusNameById(this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_STATUS', 'MPM_Status-id', 'Id'], 0, ''), this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_CATEGORY', 'MPM_Category-id', 'Id'], 0, '')),
                    owner: this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_PROJECT_OWNER', 'Identity-id', 'Id'], 0, '')
                };
                this.projectOwerId = tempModal.owner;
                this.teamId = this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_TEAM', 'MPM_Teams-id', 'Id'], 0, null);
                if (this.teamId) {
                    const currTeam = this.sharingService.getCRActions().find(data => {
                        return data['MPM_Teams-id']['Id'] === this.teamId;
                    });
                    this.isExternalUser = this.utilService.getBooleanValue(this.commentUtilService.getValueFormObjectByKeyList(currTeam, ['MPM_Team_Role_Mapping', 'IS_EXTERNAL'], 0, false));
                }
                if (this.formNotification) {
                    this.loadDataOnRefresh(this.projectId, this.deliverableIdFromUrl, false, false, tempModal);
                }
                else {
                    this.loadDataOnRefresh(this.projectId, this.deliverableId, false, false, tempModal);
                }
            });
        }
    }
    loadDataOnRefresh(projecId, deliverableId, isContextChange, isLoadMore, tempModal) {
        this.loaderService.show();
        this.isScrollDown = false;
        const httpArrayList = [
            this.commentsService.getCommentsForProject(projecId, deliverableId, this.isExternalUser, this.commentsConfig.pageDetails.skip)
        ];
        deliverableId ? httpArrayList.push(this.commentsService.getUsersForDeliverable(this.reviewDeliverableId ? this.reviewDeliverableId : deliverableId)) : httpArrayList.push(this.commentsService.getUsersForProject(projecId));
        if (this.isManager && !isContextChange) {
            httpArrayList.push(this.commentsService.getProjectAndDeliverableDetails(projecId));
        }
        else if (!this.isManager && !isContextChange) {
            httpArrayList.push(this.deliverableService.readDeliverable((deliverableId + '')));
        }
        forkJoin(httpArrayList).subscribe(responses => {
            this.loaderService.hide();
            if (responses && responses.length >= 2) {
                this.commentsConfig.commentList = !isLoadMore ? responses[0] : [...responses[0], ...this.commentsConfig.commentList];
                this.userListData = responses[1];
                if (responses[2]) {
                    if (tempModal != null) {
                        this.projectOwerId = tempModal.owner;
                    }
                    if (this.isManager) {
                        this.commentsConfig.deliverableList = responses[2] ? responses[2] : [];
                        this.commentsConfig.deliverableList = [tempModal, ...this.commentsConfig.deliverableList];
                    }
                    else {
                        this.commentsConfig.deliverableList = responses[2] ? this.commentUtilService.formProjectDeliverableModal([responses[2]['Deliverable']], projecId, true) : [];
                        if (this.commentsConfig.deliverableList.length > 0) {
                            this.commentsConfig.deliverableList[0].isActive = true;
                        }
                        this.commentsConfig.deliverableList = [...this.commentsConfig.deliverableList, tempModal];
                    }
                    if (this.commentsConfig.deliverableList.length > 0) {
                        this.updateTheCommentTitle(this.commentsConfig.deliverableList[0]);
                    }
                }
                this.isScrollDown = responses[0].length > 0 ? true : false;
            }
        }, (errorDetail) => {
            this.notification.error('Something went wrong while getting comments!');
            this.commentsConfig = null;
            this.loaderService.hide();
        });
    }
    // notificationHandle(menuItem:ProjectDeliverableModal):
    onCommentsIndexChange(menuItem) {
        this.commentsConfig.deliverableList = this.commentsConfig.deliverableList.map(element => {
            element.isActive = ((element.isProject && menuItem.isProject) ? menuItem.projectId === element.projectId : menuItem.deliverableId === element.deliverableId);
            return element;
        });
        this.commentsConfig.pageDetails = this.commentUtilService.resetPageDetails();
        this.loadDataOnRefresh(menuItem.projectId, menuItem.deliverableId, true, false, null);
        this.updateTheCommentTitle(menuItem);
    }
    onCreatingNewComment(commentDetails) {
        const currCommentItem = this.commentsConfig.deliverableList.find((data) => data.isActive);
        if (currCommentItem) {
            commentDetails = Object.assign(commentDetails, { ProjectID: currCommentItem.projectId, DeliverableID: currCommentItem.deliverableId });
            this.commentsService.createNewComment(commentDetails).subscribe(response => {
                if (response) {
                    this.commentsConfig.pageDetails = this.commentUtilService.resetPageDetails();
                    this.loadDataOnRefresh(currCommentItem.projectId, currCommentItem.deliverableId, true, false, null);
                }
            });
        }
    }
    onLoadingMoreComments(pageDetails) {
        this.commentsConfig.pageDetails = pageDetails;
        const currCommentItem = this.commentsConfig.deliverableList.find((data) => data.isActive);
        this.loadDataOnRefresh(currCommentItem.projectId, currCommentItem.deliverableId, true, true, null);
    }
    updateTheCommentTitle(indexChangeData) {
        this.titleValue = indexChangeData.isProject ?
            ('Project: ' + indexChangeData.titleName) : ('Deliverable: ' + indexChangeData.titleName);
        this.isReadonly = this.isManager ? !(this.projectOwerId === this.sharingService.getCurrentUserItemID()) : false;
        this.isReadonly = (indexChangeData.status && (indexChangeData.status === 'COMPLETED' && this.isManager)) ? true : this.isReadonly;
    }
    handleToggleTask(eventData) {
        const currCommentItem = this.commentsConfig.deliverableList.find((data) => !data.isActive);
        this.checked = !this.checked;
        this.onCommentsIndexChange(currCommentItem);
    }
    refreshComments(eventData) {
        const currCommentItem = this.commentsConfig.deliverableList.find((data) => data.isActive);
        this.onCommentsIndexChange(currCommentItem);
    }
    ngOnInit() {
        if (this.activatedRoute.parent != null) {
            this.activatedRoute.parent.params.subscribe(parentRouteParams => {
                this.projectId = parentRouteParams.projectId;
            });
        }
        this.activatedRoute.queryParams.subscribe(params => {
            if (params.deliverableId != undefined) {
                this.deliverableIdFromUrl = params.deliverableId;
                this.formNotification = true;
            }
            else {
                this.formNotification = false;
                this.deliverableIdFromUrl = 0;
            }
            this.initComments();
        });
    }
};
CommentsComponent.ctorParameters = () => [
    { type: CommentsService },
    { type: CommentsUtilService },
    { type: ProjectService },
    { type: LoaderService },
    { type: NotificationService },
    { type: DeliverableService },
    { type: SharingService },
    { type: UtilService },
    { type: ActivatedRoute },
    { type: Router }
];
__decorate([
    Input()
], CommentsComponent.prototype, "projectId", void 0);
__decorate([
    Input()
], CommentsComponent.prototype, "deliverableId", void 0);
__decorate([
    Input()
], CommentsComponent.prototype, "reviewDeliverableId", void 0);
__decorate([
    Input()
], CommentsComponent.prototype, "isManager", void 0);
__decorate([
    Input()
], CommentsComponent.prototype, "enableToComment", void 0);
CommentsComponent = __decorate([
    Component({
        selector: 'mpm-comments',
        template: "<div class=\"comment-container\">\r\n    <div class=\"flex-row\" *ngIf=\"commentsConfig\">\r\n        <div class=\"flex-row-item width-80\">\r\n            <mat-card class=\"comment-layout-container\">\r\n                <mat-card-title class=\"comment-title\" matTooltip=\"{{titleValue}}\" matTooltipClass=\"custom-tooltip\">\r\n                    <div>{{titleValue}}</div>\r\n                    <div class=\"approver-view\">\r\n                        <div class=\"mat-icon-refresh-btn\" matTooltip=\"Refresh Comments\"\r\n                            (click)=\"refreshComments($event)\">\r\n                            <mat-icon>refresh</mat-icon>\r\n                        </div>\r\n                        <div class=\"mat-icon-toggle-btn\" *ngIf=\"!isManager\">\r\n                            <mat-slide-toggle\r\n                                matTooltip=\"{{ !checked ? 'View Project Comments' : 'View Deliverable Comments'}}\"\r\n                                class=\"slider-internal active-icon\" color=\"primary\" formControlName=\"IsExternal\"\r\n                                [checked]=\"checked\" (change)=handleToggleTask($event)>\r\n                            </mat-slide-toggle>\r\n                        </div>\r\n                    </div>\r\n                </mat-card-title>\r\n                <mat-card-content class=\"comment-layout-container-scroll\">\r\n                    <mpm-comments-layout [commentDataList]=\"commentsConfig.commentList\"\r\n                        [pageDetails]=\"commentsConfig.pageDetails\" (creatingNewComment)=\"onCreatingNewComment($event)\"\r\n                        (loadingMoreComments)=\"onLoadingMoreComments($event)\" [isScrollDown]=\"isScrollDown\"\r\n                        [userListData]=\"userListData\" [isReadonly]=\"isReadonly\" [enableToComment]=\"enableToComment\"\r\n                        [isExternalUser]=\"isExternalUser\" [projectId]=\"projectId\">\r\n                    </mpm-comments-layout>\r\n                </mat-card-content>\r\n            </mat-card>\r\n        </div>\r\n        <div class=\"flex-row-item comment-index-container-flex\" *ngIf=\"isManager\">\r\n            <mat-card class=\"comment-index-container\">\r\n                <mat-card-content>\r\n                    <mpm-comments-index [commentDeliverableList]=\"commentsConfig.deliverableList\"\r\n                    [deliverableIdFromUrl]=\"deliverableIdFromUrl\"\r\n                    (commentsIndexChange)=\"onCommentsIndexChange($event)\">\r\n                </mpm-comments-index>\r\n                </mat-card-content>\r\n            </mat-card>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- [notifiactionValue]=\"notifiactionValue\"-->\r\n<!--  -->",
        encapsulation: ViewEncapsulation.None,
        styles: [".comment-container{display:flex;flex-direction:column;max-height:83vh;top:0;bottom:0;left:0;right:0;margin:1em auto;padding-bottom:0}.comment-container .width-80{width:80%}.comment-container .comment-index-container-flex{width:20%}.comment-container .comment-index-container,.comment-container .comment-layout-container{flex:1;margin:10px 10px 0;padding-bottom:0;top:8px}.comment-container .comment-index-container{margin-left:0!important;max-height:81vh;overflow-y:auto}.comment-container .comment-title{text-align:center;font-size:20px;display:flex;margin-bottom:8px;font-weight:500;position:relative}.comment-container span.approver-view{position:absolute;right:0}.comment-container span.approver-view .mat-icon-toggle-btn{position:absolute;top:0;right:2em}.comment-container span.approver-view .mat-icon-refresh-btn{cursor:pointer}.comment-title>div{width:60%}"]
    })
], CommentsComponent);
export { CommentsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBd0IsaUJBQWlCLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQzVILE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFNUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDakUsT0FBTyxFQUFFLGNBQWMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFXaEUsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFtQjVCLFlBQ1MsZUFBZ0MsRUFDaEMsa0JBQXVDLEVBQ3ZDLGNBQThCLEVBQzlCLGFBQTRCLEVBQzVCLFlBQWlDLEVBQ2pDLGtCQUFzQyxFQUN0QyxjQUE4QixFQUM5QixXQUF3QixFQUN4QixjQUE4QixFQUM5QixLQUFhO1FBVGIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBcUI7UUFDdkMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUNqQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQXZCdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLHlCQUFvQixHQUFHLENBQUMsQ0FBQztRQUV6QixxQkFBZ0IsR0FBWSxLQUFLLENBQUE7UUFjL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO0lBQzNELENBQUM7SUFFRCxZQUFZO1FBQ1YsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxjQUFjLEdBQUc7Z0JBQ3BCLFdBQVcsRUFBRSxFQUFFO2dCQUNmLGVBQWUsRUFBRSxFQUFFO2dCQUNuQixXQUFXLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixFQUFFO2FBQ3hELENBQUM7WUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzdFLElBQUksU0FBUyxHQUE0QixJQUFJLENBQUM7Z0JBQzlDLFNBQVMsR0FBRztvQkFDVixTQUFTLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQ3hGLElBQUksRUFBRSxxQkFBcUI7b0JBQzNCLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDekUsU0FBUyxFQUFFLElBQUk7b0JBQ2YsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO29CQUN6QixhQUFhLEVBQUUsSUFBSTtvQkFDbkIsTUFBTSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FDL0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxDQUFDLGFBQWEsRUFBRSxlQUFlLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUM1RyxJQUFJLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLENBQUMsZUFBZSxFQUFFLGlCQUFpQixFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDbkgsS0FBSyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztpQkFDekgsQ0FBQztnQkFDRixJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMxSCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2YsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQzlELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3BELENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxDQUFDLHVCQUF1QixFQUFFLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO2lCQUMzSztnQkFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7aUJBQzVGO3FCQUNJO29CQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQTtpQkFDcEY7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELGlCQUFpQixDQUFDLFFBQWdCLEVBQUUsYUFBcUIsRUFBRSxlQUF3QixFQUFFLFVBQW1CLEVBQUUsU0FBa0M7UUFDMUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixNQUFNLGFBQWEsR0FBRztZQUNwQixJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7U0FDL0gsQ0FBQztRQUNGLGFBQWEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUM3TixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLCtCQUErQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDcEY7YUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUM5QyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25GO1FBQ0QsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckgsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNoQixJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7d0JBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztxQkFDdEM7b0JBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO3dCQUVsQixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUN2RSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUM7cUJBQzNGO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQzdKLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDbEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzt5QkFDeEQ7d0JBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLFNBQVMsQ0FBQyxDQUFDO3FCQUMzRjtvQkFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ2xELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNwRTtpQkFDRjtnQkFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUM1RDtRQUNILENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQztJQUlMLENBQUM7SUFFRCx3REFBd0Q7SUFFeEQscUJBQXFCLENBQUMsUUFBaUM7UUFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3RGLE9BQU8sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLEtBQUssT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzdKLE9BQU8sT0FBTyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDN0UsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsb0JBQW9CLENBQUMsY0FBK0I7UUFDbEQsTUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQTZCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1SSxJQUFJLGVBQWUsRUFBRTtZQUNuQixjQUFjLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBRSxTQUFTLEVBQUUsZUFBZSxDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUUsZUFBZSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDdkksSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3pFLElBQUksUUFBUSxFQUFFO29CQUNaLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUM3RSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ3JHO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxXQUFnQjtRQUNwQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUM7UUFDOUMsTUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQTZCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1SSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDckcsQ0FBQztJQUVELHFCQUFxQixDQUFDLGVBQXdDO1FBQzVELElBQUksQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzNDLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNoSCxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEtBQUssV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDcEksQ0FBQztJQUVELGdCQUFnQixDQUFDLFNBQVM7UUFDeEIsTUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQTZCLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdJLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQzdCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQ0QsZUFBZSxDQUFDLFNBQVM7UUFDdkIsTUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQTZCLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1SSxJQUFJLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxJQUFFLElBQUksRUFBQztZQUNwQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0JBQ3hELElBQUksQ0FBQyxTQUFTLEdBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFBO1lBQ3hDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQ3ZDLE1BQU0sQ0FBQyxFQUFFO1lBQ1AsSUFBSSxNQUFNLENBQUMsYUFBYSxJQUFJLFNBQVMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUE7Z0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUE7YUFDN0I7aUJBQ0k7Z0JBQ0gsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtnQkFDN0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLENBQUMsQ0FBQTthQUM5QjtZQUNELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN0QixDQUFDLENBQ0YsQ0FBQTtJQUNILENBQUM7Q0FDRixDQUFBOztZQXpLMkIsZUFBZTtZQUNaLG1CQUFtQjtZQUN2QixjQUFjO1lBQ2YsYUFBYTtZQUNkLG1CQUFtQjtZQUNiLGtCQUFrQjtZQUN0QixjQUFjO1lBQ2pCLFdBQVc7WUFDUixjQUFjO1lBQ3ZCLE1BQU07O0FBNUJiO0lBQVIsS0FBSyxFQUFFO29EQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTt3REFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7OERBQTZCO0FBQzVCO0lBQVIsS0FBSyxFQUFFO29EQUFvQjtBQUNuQjtJQUFSLEtBQUssRUFBRTswREFBMEI7QUFMdkIsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxjQUFjO1FBQ3hCLDRxRkFBd0M7UUFFeEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O0tBQ3RDLENBQUM7R0FDVyxpQkFBaUIsQ0E2TDdCO1NBN0xZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgSW5qZWN0LCBIb3N0TGlzdGVuZXIsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbWVudHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb21tZW50cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tbWVudHNVdGlsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgZm9ya0pvaW4sIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3REZWxpdmVyYWJsZU1vZGFsLCBOZXdDb21tZW50TW9kYWwgfSBmcm9tICcuL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBSb3V0aW5nQ29uc3RhbnRzIH0gZnJvbSAnLi4vc2hhcmVkL2NvbnN0YW50cy9yb3V0aW5nQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgVEhJU19FWFBSIH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXIvc3JjL291dHB1dC9vdXRwdXRfYXN0JztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jb21tZW50cycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnRzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50cy5jb21wb25lbnQuc2NzcyddLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBwcm9qZWN0SWQ6IG51bWJlcjtcclxuICBASW5wdXQoKSBkZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgQElucHV0KCkgcmV2aWV3RGVsaXZlcmFibGVJZDogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIGlzTWFuYWdlcjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVUb0NvbW1lbnQ6IGJvb2xlYW47XHJcbiAgY29tbWVudHNDb25maWcgPSBudWxsO1xyXG4gIHRpdGxlVmFsdWUgPSBudWxsO1xyXG4gIGlzU2Nyb2xsRG93biA9IGZhbHNlO1xyXG4gIHVzZXJMaXN0RGF0YSA9IFtdO1xyXG4gIGlzUmVhZG9ubHkgPSBmYWxzZTtcclxuICBwcm9qZWN0T3dlcklkID0gbnVsbDtcclxuICBpc0V4dGVybmFsVXNlciA9IGZhbHNlO1xyXG4gIHRlYW1JZCA9IG51bGw7XHJcbiAgY2hlY2tlZCA9IGZhbHNlO1xyXG4gIGRlbGl2ZXJhYmxlSWRGcm9tVXJsID0gMDtcclxuICBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvblxyXG4gIGZvcm1Ob3RpZmljYXRpb246IGJvb2xlYW4gPSBmYWxzZVxyXG4gIG5vdGlmaWNhdGlvbkRldGFpbHM7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgY29tbWVudFV0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHB1YmxpYyByb3V0ZTogUm91dGVyXHJcbiAgKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5jbGVhclVzZXJQcm9qZWN0RGVsaXZlcmFibGVNYXBwZXIoKTtcclxuICB9XHJcblxyXG4gIGluaXRDb21tZW50cygpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnByb2plY3RJZCkge1xyXG4gICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnID0ge1xyXG4gICAgICAgIGNvbW1lbnRMaXN0OiBbXSxcclxuICAgICAgICBkZWxpdmVyYWJsZUxpc3Q6IFtdLFxyXG4gICAgICAgIHBhZ2VEZXRhaWxzOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5yZXNldFBhZ2VEZXRhaWxzKCksXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvamVjdEJ5SWQoKHRoaXMucHJvamVjdElkICsgJycpKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGxldCB0ZW1wTW9kYWw6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsID0gbnVsbDtcclxuICAgICAgICB0ZW1wTW9kYWwgPSB7XHJcbiAgICAgICAgICB0aXRsZU5hbWU6IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmNoZWNrVmFsdWVJc051bGxPckVtcHR5KHJlc3BvbnNlWydQUk9KRUNUX05BTUUnXSwgJycpLFxyXG4gICAgICAgICAgaWNvbjogJ2ZpYmVyX21hbnVhbF9yZWNvcmQnLFxyXG4gICAgICAgICAgaXNBY3RpdmU6IHRoaXMuaXNNYW5hZ2VyICYmIHRoaXMuZGVsaXZlcmFibGVJZEZyb21VcmwgPT0gMCA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgIGlzUHJvamVjdDogdHJ1ZSxcclxuICAgICAgICAgIHByb2plY3RJZDogdGhpcy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICBkZWxpdmVyYWJsZUlkOiBudWxsLFxyXG4gICAgICAgICAgc3RhdHVzOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRTdGF0dXNOYW1lQnlJZChcclxuICAgICAgICAgICAgdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0VmFsdWVGb3JtT2JqZWN0QnlLZXlMaXN0KHJlc3BvbnNlLCBbJ1JfUE9fU1RBVFVTJywgJ01QTV9TdGF0dXMtaWQnLCAnSWQnXSwgMCwgJycpLFxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRWYWx1ZUZvcm1PYmplY3RCeUtleUxpc3QocmVzcG9uc2UsIFsnUl9QT19DQVRFR09SWScsICdNUE1fQ2F0ZWdvcnktaWQnLCAnSWQnXSwgMCwgJycpKSxcclxuICAgICAgICAgIG93bmVyOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRWYWx1ZUZvcm1PYmplY3RCeUtleUxpc3QocmVzcG9uc2UsIFsnUl9QT19QUk9KRUNUX09XTkVSJywgJ0lkZW50aXR5LWlkJywgJ0lkJ10sIDAsICcnKVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0T3dlcklkID0gdGVtcE1vZGFsLm93bmVyO1xyXG4gICAgICAgIHRoaXMudGVhbUlkID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0VmFsdWVGb3JtT2JqZWN0QnlLZXlMaXN0KHJlc3BvbnNlLCBbJ1JfUE9fVEVBTScsICdNUE1fVGVhbXMtaWQnLCAnSWQnXSwgMCwgbnVsbCk7XHJcbiAgICAgICAgaWYgKHRoaXMudGVhbUlkKSB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyVGVhbSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q1JBY3Rpb25zKCkuZmluZChkYXRhID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGFbJ01QTV9UZWFtcy1pZCddWydJZCddID09PSB0aGlzLnRlYW1JZDtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5pc0V4dGVybmFsVXNlciA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldFZhbHVlRm9ybU9iamVjdEJ5S2V5TGlzdChjdXJyVGVhbSwgWydNUE1fVGVhbV9Sb2xlX01hcHBpbmcnLCAnSVNfRVhURVJOQUwnXSwgMCwgZmFsc2UpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybU5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgICAgdGhpcy5sb2FkRGF0YU9uUmVmcmVzaCh0aGlzLnByb2plY3RJZCwgdGhpcy5kZWxpdmVyYWJsZUlkRnJvbVVybCwgZmFsc2UsIGZhbHNlLCB0ZW1wTW9kYWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9hZERhdGFPblJlZnJlc2godGhpcy5wcm9qZWN0SWQsIHRoaXMuZGVsaXZlcmFibGVJZCwgZmFsc2UsIGZhbHNlLCB0ZW1wTW9kYWwpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGxvYWREYXRhT25SZWZyZXNoKHByb2plY0lkOiBudW1iZXIsIGRlbGl2ZXJhYmxlSWQ6IG51bWJlciwgaXNDb250ZXh0Q2hhbmdlOiBib29sZWFuLCBpc0xvYWRNb3JlOiBib29sZWFuLCB0ZW1wTW9kYWw6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5pc1Njcm9sbERvd24gPSBmYWxzZTtcclxuICAgIGNvbnN0IGh0dHBBcnJheUxpc3QgPSBbXHJcbiAgICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmdldENvbW1lbnRzRm9yUHJvamVjdChwcm9qZWNJZCwgZGVsaXZlcmFibGVJZCwgdGhpcy5pc0V4dGVybmFsVXNlciwgdGhpcy5jb21tZW50c0NvbmZpZy5wYWdlRGV0YWlscy5za2lwKVxyXG4gICAgXTtcclxuICAgIGRlbGl2ZXJhYmxlSWQgPyBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0VXNlcnNGb3JEZWxpdmVyYWJsZSh0aGlzLnJldmlld0RlbGl2ZXJhYmxlSWQgPyB0aGlzLnJldmlld0RlbGl2ZXJhYmxlSWQgOiBkZWxpdmVyYWJsZUlkKSkgOiBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0VXNlcnNGb3JQcm9qZWN0KHByb2plY0lkKSk7XHJcbiAgICBpZiAodGhpcy5pc01hbmFnZXIgJiYgIWlzQ29udGV4dENoYW5nZSkge1xyXG4gICAgICBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0UHJvamVjdEFuZERlbGl2ZXJhYmxlRGV0YWlscyhwcm9qZWNJZCkpO1xyXG4gICAgfSBlbHNlIGlmICghdGhpcy5pc01hbmFnZXIgJiYgIWlzQ29udGV4dENoYW5nZSkge1xyXG4gICAgICBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5kZWxpdmVyYWJsZVNlcnZpY2UucmVhZERlbGl2ZXJhYmxlKChkZWxpdmVyYWJsZUlkICsgJycpKSk7XHJcbiAgICB9XHJcbiAgICBmb3JrSm9pbihodHRwQXJyYXlMaXN0KS5zdWJzY3JpYmUocmVzcG9uc2VzID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgaWYgKHJlc3BvbnNlcyAmJiByZXNwb25zZXMubGVuZ3RoID49IDIpIHtcclxuICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmNvbW1lbnRMaXN0ID0gIWlzTG9hZE1vcmUgPyByZXNwb25zZXNbMF0gOiBbLi4ucmVzcG9uc2VzWzBdLCAuLi50aGlzLmNvbW1lbnRzQ29uZmlnLmNvbW1lbnRMaXN0XTtcclxuICAgICAgICB0aGlzLnVzZXJMaXN0RGF0YSA9IHJlc3BvbnNlc1sxXTtcclxuICAgICAgICBpZiAocmVzcG9uc2VzWzJdKSB7XHJcbiAgICAgICAgICBpZiAodGVtcE1vZGFsICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3dlcklkID0gdGVtcE1vZGFsLm93bmVyO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHRoaXMuaXNNYW5hZ2VyKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdCA9IHJlc3BvbnNlc1syXSA/IHJlc3BvbnNlc1syXSA6IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdCA9IFt0ZW1wTW9kYWwsIC4uLnRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0XTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0ID0gcmVzcG9uc2VzWzJdID8gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZm9ybVByb2plY3REZWxpdmVyYWJsZU1vZGFsKFtyZXNwb25zZXNbMl1bJ0RlbGl2ZXJhYmxlJ11dLCBwcm9qZWNJZCwgdHJ1ZSkgOiBbXTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdFswXS5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QgPSBbLi4udGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QsIHRlbXBNb2RhbF07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAodGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVRoZUNvbW1lbnRUaXRsZSh0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdFswXSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNTY3JvbGxEb3duID0gcmVzcG9uc2VzWzBdLmxlbmd0aCA+IDAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0sIChlcnJvckRldGFpbCkgPT4ge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbi5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBjb21tZW50cyEnKTtcclxuICAgICAgdGhpcy5jb21tZW50c0NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICB9KTtcclxuXHJcblxyXG5cclxuICB9XHJcblxyXG4gIC8vIG5vdGlmaWNhdGlvbkhhbmRsZShtZW51SXRlbTpQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCk6XHJcblxyXG4gIG9uQ29tbWVudHNJbmRleENoYW5nZShtZW51SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwpOiB2b2lkIHtcclxuICAgIHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0ID0gdGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QubWFwKGVsZW1lbnQgPT4ge1xyXG4gICAgICBlbGVtZW50LmlzQWN0aXZlID0gKChlbGVtZW50LmlzUHJvamVjdCAmJiBtZW51SXRlbS5pc1Byb2plY3QpID8gbWVudUl0ZW0ucHJvamVjdElkID09PSBlbGVtZW50LnByb2plY3RJZCA6IG1lbnVJdGVtLmRlbGl2ZXJhYmxlSWQgPT09IGVsZW1lbnQuZGVsaXZlcmFibGVJZCk7XHJcbiAgICAgIHJldHVybiBlbGVtZW50O1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLnBhZ2VEZXRhaWxzID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UucmVzZXRQYWdlRGV0YWlscygpO1xyXG4gICAgdGhpcy5sb2FkRGF0YU9uUmVmcmVzaChtZW51SXRlbS5wcm9qZWN0SWQsIG1lbnVJdGVtLmRlbGl2ZXJhYmxlSWQsIHRydWUsIGZhbHNlLCBudWxsKTtcclxuICAgIHRoaXMudXBkYXRlVGhlQ29tbWVudFRpdGxlKG1lbnVJdGVtKTtcclxuICB9XHJcblxyXG4gIG9uQ3JlYXRpbmdOZXdDb21tZW50KGNvbW1lbnREZXRhaWxzOiBOZXdDb21tZW50TW9kYWwpOiB2b2lkIHtcclxuICAgIGNvbnN0IGN1cnJDb21tZW50SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwgPSB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdC5maW5kKChkYXRhOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCkgPT4gZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICBpZiAoY3VyckNvbW1lbnRJdGVtKSB7XHJcbiAgICAgIGNvbW1lbnREZXRhaWxzID0gT2JqZWN0LmFzc2lnbihjb21tZW50RGV0YWlscywgeyBQcm9qZWN0SUQ6IGN1cnJDb21tZW50SXRlbS5wcm9qZWN0SWQsIERlbGl2ZXJhYmxlSUQ6IGN1cnJDb21tZW50SXRlbS5kZWxpdmVyYWJsZUlkIH0pO1xyXG4gICAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5jcmVhdGVOZXdDb21tZW50KGNvbW1lbnREZXRhaWxzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgdGhpcy5jb21tZW50c0NvbmZpZy5wYWdlRGV0YWlscyA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLnJlc2V0UGFnZURldGFpbHMoKTtcclxuICAgICAgICAgIHRoaXMubG9hZERhdGFPblJlZnJlc2goY3VyckNvbW1lbnRJdGVtLnByb2plY3RJZCwgY3VyckNvbW1lbnRJdGVtLmRlbGl2ZXJhYmxlSWQsIHRydWUsIGZhbHNlLCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25Mb2FkaW5nTW9yZUNvbW1lbnRzKHBhZ2VEZXRhaWxzOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuY29tbWVudHNDb25maWcucGFnZURldGFpbHMgPSBwYWdlRGV0YWlscztcclxuICAgIGNvbnN0IGN1cnJDb21tZW50SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwgPSB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdC5maW5kKChkYXRhOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCkgPT4gZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICB0aGlzLmxvYWREYXRhT25SZWZyZXNoKGN1cnJDb21tZW50SXRlbS5wcm9qZWN0SWQsIGN1cnJDb21tZW50SXRlbS5kZWxpdmVyYWJsZUlkLCB0cnVlLCB0cnVlLCBudWxsKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZVRoZUNvbW1lbnRUaXRsZShpbmRleENoYW5nZURhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKTogdm9pZCB7XHJcbiAgICB0aGlzLnRpdGxlVmFsdWUgPSBpbmRleENoYW5nZURhdGEuaXNQcm9qZWN0ID9cclxuICAgICAgKCdQcm9qZWN0OiAnICsgaW5kZXhDaGFuZ2VEYXRhLnRpdGxlTmFtZSkgOiAoJ0RlbGl2ZXJhYmxlOiAnICsgaW5kZXhDaGFuZ2VEYXRhLnRpdGxlTmFtZSk7XHJcbiAgICB0aGlzLmlzUmVhZG9ubHkgPSB0aGlzLmlzTWFuYWdlciA/ICEodGhpcy5wcm9qZWN0T3dlcklkID09PSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCkpIDogZmFsc2U7XHJcbiAgICB0aGlzLmlzUmVhZG9ubHkgPSAoaW5kZXhDaGFuZ2VEYXRhLnN0YXR1cyAmJiAoaW5kZXhDaGFuZ2VEYXRhLnN0YXR1cyA9PT0gJ0NPTVBMRVRFRCcgJiYgdGhpcy5pc01hbmFnZXIpKSA/IHRydWUgOiB0aGlzLmlzUmVhZG9ubHk7XHJcbiAgfVxyXG5cclxuICBoYW5kbGVUb2dnbGVUYXNrKGV2ZW50RGF0YSk6IHZvaWQge1xyXG4gICAgY29uc3QgY3VyckNvbW1lbnRJdGVtOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCA9IHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0LmZpbmQoKGRhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKSA9PiAhZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICB0aGlzLmNoZWNrZWQgPSAhdGhpcy5jaGVja2VkO1xyXG4gICAgdGhpcy5vbkNvbW1lbnRzSW5kZXhDaGFuZ2UoY3VyckNvbW1lbnRJdGVtKTtcclxuICB9XHJcbiAgcmVmcmVzaENvbW1lbnRzKGV2ZW50RGF0YSk6IHZvaWQge1xyXG4gICAgY29uc3QgY3VyckNvbW1lbnRJdGVtOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCA9IHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0LmZpbmQoKGRhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKSA9PiBkYXRhLmlzQWN0aXZlKTtcclxuICAgIHRoaXMub25Db21tZW50c0luZGV4Q2hhbmdlKGN1cnJDb21tZW50SXRlbSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGlmKHRoaXMuYWN0aXZhdGVkUm91dGUucGFyZW50IT1udWxsKXtcclxuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucGFyZW50LnBhcmFtcy5zdWJzY3JpYmUocGFyZW50Um91dGVQYXJhbXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RJZD1wYXJlbnRSb3V0ZVBhcmFtcy5wcm9qZWN0SWRcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5xdWVyeVBhcmFtcy5zdWJzY3JpYmUoXHJcbiAgICAgIHBhcmFtcyA9PiB7XHJcbiAgICAgICAgaWYgKHBhcmFtcy5kZWxpdmVyYWJsZUlkICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUlkRnJvbVVybCA9IHBhcmFtcy5kZWxpdmVyYWJsZUlkXHJcbiAgICAgICAgICB0aGlzLmZvcm1Ob3RpZmljYXRpb24gPSB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5mb3JtTm90aWZpY2F0aW9uID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZEZyb21VcmwgPSAwXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaW5pdENvbW1lbnRzKCk7XHJcbiAgICAgIH1cclxuICAgIClcclxuICB9XHJcbn1cclxuIl19