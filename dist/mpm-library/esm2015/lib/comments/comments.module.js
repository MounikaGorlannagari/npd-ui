import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsLayoutComponent } from './comments-layout/comments-layout.component';
import { CommentsIndexComponent } from './comments-index/comments-index.component';
import { CommentsMessageComponent } from './comments-message/comments-message.component';
import { CommentsTextComponent } from './comments-text/comments-text.component';
import { CommentsComponent } from './comments.component';
import { MaterialModule } from '../material.module';
import { CommentsMessageLayoutComponent } from './comments-message-layout/comments-message-layout.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommentsModalComponent } from './comments-modal/comments-modal.component';
import { BulkCommentsComponent } from './bulk-comments/bulk-comments.component';
let CommentsModule = class CommentsModule {
};
CommentsModule = __decorate([
    NgModule({
        declarations: [
            CommentsLayoutComponent,
            CommentsIndexComponent,
            CommentsMessageComponent,
            CommentsTextComponent,
            CommentsComponent,
            CommentsMessageLayoutComponent,
            CommentsModalComponent,
            BulkCommentsComponent
        ],
        imports: [
            CommonModule,
            MaterialModule
        ],
        exports: [
            CommentsLayoutComponent,
            CommentsIndexComponent,
            CommentsMessageComponent,
            CommentsTextComponent,
            CommentsComponent,
            CommentsMessageLayoutComponent,
            CommentsModalComponent,
            BulkCommentsComponent
        ],
        providers: [
            { provide: MatDialogRef, useValue: null },
            { provide: MAT_DIALOG_DATA, useValue: null }
        ]
    })
], CommentsModule);
export { CommentsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUN0RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNuRixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sNkRBQTZELENBQUM7QUFDN0csT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNuRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQWdDaEYsSUFBYSxjQUFjLEdBQTNCLE1BQWEsY0FBYztDQUFJLENBQUE7QUFBbEIsY0FBYztJQTlCMUIsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFO1lBQ1osdUJBQXVCO1lBQ3ZCLHNCQUFzQjtZQUN0Qix3QkFBd0I7WUFDeEIscUJBQXFCO1lBQ3JCLGlCQUFpQjtZQUNqQiw4QkFBOEI7WUFDOUIsc0JBQXNCO1lBQ3RCLHFCQUFxQjtTQUN0QjtRQUNELE9BQU8sRUFBRTtZQUNQLFlBQVk7WUFDWixjQUFjO1NBQ2Y7UUFDRCxPQUFPLEVBQUU7WUFDUCx1QkFBdUI7WUFDdkIsc0JBQXNCO1lBQ3RCLHdCQUF3QjtZQUN4QixxQkFBcUI7WUFDckIsaUJBQWlCO1lBQ2pCLDhCQUE4QjtZQUM5QixzQkFBc0I7WUFDdEIscUJBQXFCO1NBQ3RCO1FBQ0QsU0FBUyxFQUFFO1lBQ1QsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7WUFDekMsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7U0FDN0M7S0FDRixDQUFDO0dBQ1csY0FBYyxDQUFJO1NBQWxCLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDb21tZW50c0xheW91dENvbXBvbmVudCB9IGZyb20gJy4vY29tbWVudHMtbGF5b3V0L2NvbW1lbnRzLWxheW91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50c0luZGV4Q29tcG9uZW50IH0gZnJvbSAnLi9jb21tZW50cy1pbmRleC9jb21tZW50cy1pbmRleC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50c01lc3NhZ2VDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLW1lc3NhZ2UvY29tbWVudHMtbWVzc2FnZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50c1RleHRDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLXRleHQvY29tbWVudHMtdGV4dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tZW50c0NvbXBvbmVudCB9IGZyb20gJy4vY29tbWVudHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBDb21tZW50c01lc3NhZ2VMYXlvdXRDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLW1lc3NhZ2UtbGF5b3V0L2NvbW1lbnRzLW1lc3NhZ2UtbGF5b3V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1BVF9ESUFMT0dfREFUQSwgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQ29tbWVudHNNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tbWVudHMtbW9kYWwvY29tbWVudHMtbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQnVsa0NvbW1lbnRzQ29tcG9uZW50IH0gZnJvbSAnLi9idWxrLWNvbW1lbnRzL2J1bGstY29tbWVudHMuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBDb21tZW50c0xheW91dENvbXBvbmVudCxcclxuICAgIENvbW1lbnRzSW5kZXhDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01lc3NhZ2VDb21wb25lbnQsXHJcbiAgICBDb21tZW50c1RleHRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c0NvbXBvbmVudCxcclxuICAgIENvbW1lbnRzTWVzc2FnZUxheW91dENvbXBvbmVudCxcclxuICAgIENvbW1lbnRzTW9kYWxDb21wb25lbnQsXHJcbiAgICBCdWxrQ29tbWVudHNDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBDb21tZW50c0xheW91dENvbXBvbmVudCxcclxuICAgIENvbW1lbnRzSW5kZXhDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01lc3NhZ2VDb21wb25lbnQsXHJcbiAgICBDb21tZW50c1RleHRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c0NvbXBvbmVudCxcclxuICAgIENvbW1lbnRzTWVzc2FnZUxheW91dENvbXBvbmVudCxcclxuICAgIENvbW1lbnRzTW9kYWxDb21wb25lbnQsXHJcbiAgICBCdWxrQ29tbWVudHNDb21wb25lbnRcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgeyBwcm92aWRlOiBNYXREaWFsb2dSZWYsIHVzZVZhbHVlOiBudWxsIH0sXHJcbiAgICB7IHByb3ZpZGU6IE1BVF9ESUFMT0dfREFUQSwgdXNlVmFsdWU6IG51bGwgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzTW9kdWxlIHsgfVxyXG4iXX0=