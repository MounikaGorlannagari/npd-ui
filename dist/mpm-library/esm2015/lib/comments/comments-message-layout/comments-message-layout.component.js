import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, HostListener } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
let CommentsMessageLayoutComponent = class CommentsMessageLayoutComponent {
    constructor(commentUtilService) {
        this.commentUtilService = commentUtilService;
        this.replyToComment = new EventEmitter();
        this.loadingMoreComments = new EventEmitter();
        this.loadMore = true;
        this.navigateToReply = null;
    }
    onScrollEvent(eventData) {
        this.loadMore = !this.loadMore ? this.isScrollDown : this.loadMore;
        if (this.loadMore && this.myScrollContainer.nativeElement.scrollTop <= 1000 && this.commentListData.length >= 100) {
            this.onloadMoreComments(null);
            this.loadMore = false;
        }
    }
    ngAfterViewChecked() {
        if (this.isScrollDown && this.myScrollContainer.nativeElement.scrollTop === 0) {
            this.scrollToBottom(this.myScrollContainer.nativeElement.scrollHeight);
            this.loadMore = true;
        }
    }
    onloadMoreComments(eventData) {
        let tempPageDetails = this.pageDetails;
        if (tempPageDetails) {
            tempPageDetails.page = tempPageDetails.page + 1;
            tempPageDetails.skip = tempPageDetails.skip + 100;
        }
        else {
            tempPageDetails = this.commentUtilService.resetPageDetails();
        }
        this.loadingMoreComments.emit(tempPageDetails);
    }
    onReplyToComment(parentComment) {
        this.replyToComment.emit(parentComment);
    }
    scrollToBottom(height) {
        try {
            this.myScrollContainer.nativeElement.scrollTop = height;
        }
        catch (err) { }
    }
    onClickingOnReplyMessage(parentCommentId) {
        if (document.getElementById('message_' + parentCommentId)) {
            document.getElementById('message_' + parentCommentId).scrollIntoView({ behavior: 'smooth', block: 'center' });
            this.navigateToReply = null;
        }
        else {
            this.navigateToReply = parentCommentId;
            this.onloadMoreComments(null);
        }
    }
    ngOnInit() {
        this.loadMore = this.isScrollDown;
        if (this.navigateToReply && this.isScrollDown) {
            this.onClickingOnReplyMessage(this.navigateToReply);
        }
    }
};
CommentsMessageLayoutComponent.ctorParameters = () => [
    { type: CommentsUtilService }
];
__decorate([
    Input()
], CommentsMessageLayoutComponent.prototype, "commentListData", void 0);
__decorate([
    Input()
], CommentsMessageLayoutComponent.prototype, "pageDetails", void 0);
__decorate([
    Input()
], CommentsMessageLayoutComponent.prototype, "isScrollDown", void 0);
__decorate([
    Input()
], CommentsMessageLayoutComponent.prototype, "userListData", void 0);
__decorate([
    Output()
], CommentsMessageLayoutComponent.prototype, "replyToComment", void 0);
__decorate([
    Output()
], CommentsMessageLayoutComponent.prototype, "loadingMoreComments", void 0);
__decorate([
    ViewChild('scrollToBottom')
], CommentsMessageLayoutComponent.prototype, "myScrollContainer", void 0);
__decorate([
    HostListener('scroll', ['$event'])
], CommentsMessageLayoutComponent.prototype, "onScrollEvent", null);
CommentsMessageLayoutComponent = __decorate([
    Component({
        selector: 'mpm-comments-message-layout',
        template: "<div class=\"comment-message-container\" #scrollToBottom (scroll)=\"onScrollEvent($event)\">\r\n    <div class=\"comment-message-item central-info\" *ngIf=\"pageDetails.totalCount > commentListData.length\"><button\r\n            color=\"primary width-100\" mat-flat-button (click)=\"onloadMoreComments($event)\">Load More Comments</button>\r\n    </div>\r\n    <div class=\"comment-message-item\" *ngFor=\"let commentData of commentListData\">\r\n        <mpm-comments-message [commentData]=\"commentData\" (replyToComment)=\"onReplyToComment(commentData)\"\r\n            [isReply]=\"true\" (clickingOnReplyMessage)=\"onClickingOnReplyMessage($event)\" [userListData]=\"userListData\">\r\n        </mpm-comments-message>\r\n    </div>\r\n    <div class=\"comment-message-item central-info\" *ngIf=\"commentListData.length === 0\"><button color=\"primary\" disabled\r\n            mat-flat-button>No Comments</button></div>\r\n</div>",
        styles: [".comment-message-container{min-height:55vh;height:55vh;overflow-y:auto;overflow-x:hidden;padding-top:15px}.comment-message-container .central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-top:10px}.comment-message-container button.width-100{width:100%}"]
    })
], CommentsMessageLayoutComponent);
export { CommentsMessageLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbWVzc2FnZS1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMtbWVzc2FnZS1sYXlvdXQvY29tbWVudHMtbWVzc2FnZS1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBZ0MsWUFBWSxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2pKLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBUXhFLElBQWEsOEJBQThCLEdBQTNDLE1BQWEsOEJBQThCO0lBaUJ6QyxZQUNTLGtCQUF1QztRQUF2Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQXFCO1FBYnRDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN6Qyx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRXhELGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7SUFVbkIsQ0FBQztJQVQrQixhQUFhLENBQUMsU0FBUztRQUN6RCxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNuRSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxTQUFTLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtZQUNqSCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBS0Qsa0JBQWtCO1FBQ2hCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsS0FBSyxDQUFDLEVBQUU7WUFDN0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQUVELGtCQUFrQixDQUFDLFNBQVM7UUFDMUIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxJQUFJLGVBQWUsRUFBRTtZQUNuQixlQUFlLENBQUMsSUFBSSxHQUFHLGVBQWUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQ2hELGVBQWUsQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7U0FDbkQ7YUFBTTtZQUNMLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUM5RDtRQUNELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGdCQUFnQixDQUFDLGFBQWE7UUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELGNBQWMsQ0FBQyxNQUFNO1FBQ25CLElBQUk7WUFDRixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7U0FDekQ7UUFBQyxPQUFPLEdBQUcsRUFBRSxHQUFHO0lBQ25CLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxlQUFlO1FBQ3RDLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDLEVBQUU7WUFDekQsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsZUFBZSxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUM5RyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztTQUM3QjthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7WUFDdkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbEMsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDN0MsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNyRDtJQUNILENBQUM7Q0FDRixDQUFBOztZQS9DOEIsbUJBQW1COztBQWpCdkM7SUFBUixLQUFLLEVBQUU7dUVBQTZCO0FBQzVCO0lBQVIsS0FBSyxFQUFFO21FQUFrQjtBQUNqQjtJQUFSLEtBQUssRUFBRTtvRUFBdUI7QUFDdEI7SUFBUixLQUFLLEVBQUU7b0VBQW9DO0FBQ2xDO0lBQVQsTUFBTSxFQUFFO3NFQUEwQztBQUN6QztJQUFULE1BQU0sRUFBRTsyRUFBK0M7QUFDM0I7SUFBNUIsU0FBUyxDQUFDLGdCQUFnQixDQUFDO3lFQUFzQztBQUc5QjtJQUFuQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7bUVBTWxDO0FBaEJVLDhCQUE4QjtJQUwxQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsNkJBQTZCO1FBQ3ZDLGs3QkFBdUQ7O0tBRXhELENBQUM7R0FDVyw4QkFBOEIsQ0FpRTFDO1NBakVZLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgRWxlbWVudFJlZiwgQWZ0ZXJWaWV3Q2hlY2tlZCwgSG9zdExpc3RlbmVyLCBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbWVudHNVdGlsU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2NvbW1lbnRzLnV0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJJbmZvTW9kYWwgfSBmcm9tICcuLi9vYmplY3RzL2NvbW1lbnQubW9kYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY29tbWVudHMtbWVzc2FnZS1sYXlvdXQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50cy1tZXNzYWdlLWxheW91dC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29tbWVudHMtbWVzc2FnZS1sYXlvdXQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudHNNZXNzYWdlTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdDaGVja2VkIHtcclxuICBASW5wdXQoKSBjb21tZW50TGlzdERhdGE6IEFycmF5PGFueT47XHJcbiAgQElucHV0KCkgcGFnZURldGFpbHM6IGFueTtcclxuICBASW5wdXQoKSBpc1Njcm9sbERvd246IGJvb2xlYW47XHJcbiAgQElucHV0KCkgdXNlckxpc3REYXRhOiBBcnJheTxVc2VySW5mb01vZGFsPjtcclxuICBAT3V0cHV0KCkgcmVwbHlUb0NvbW1lbnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgbG9hZGluZ01vcmVDb21tZW50cyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBWaWV3Q2hpbGQoJ3Njcm9sbFRvQm90dG9tJykgcHVibGljIG15U2Nyb2xsQ29udGFpbmVyOiBFbGVtZW50UmVmO1xyXG4gIGxvYWRNb3JlID0gdHJ1ZTtcclxuICBuYXZpZ2F0ZVRvUmVwbHkgPSBudWxsO1xyXG4gIEBIb3N0TGlzdGVuZXIoJ3Njcm9sbCcsIFsnJGV2ZW50J10pIG9uU2Nyb2xsRXZlbnQoZXZlbnREYXRhKSB7XHJcbiAgICB0aGlzLmxvYWRNb3JlID0gIXRoaXMubG9hZE1vcmUgPyB0aGlzLmlzU2Nyb2xsRG93biA6IHRoaXMubG9hZE1vcmU7XHJcbiAgICBpZiAodGhpcy5sb2FkTW9yZSAmJiB0aGlzLm15U2Nyb2xsQ29udGFpbmVyLm5hdGl2ZUVsZW1lbnQuc2Nyb2xsVG9wIDw9IDEwMDAgJiYgdGhpcy5jb21tZW50TGlzdERhdGEubGVuZ3RoID49IDEwMCkge1xyXG4gICAgICB0aGlzLm9ubG9hZE1vcmVDb21tZW50cyhudWxsKTtcclxuICAgICAgdGhpcy5sb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBuZ0FmdGVyVmlld0NoZWNrZWQoKSB7XHJcbiAgICBpZiAodGhpcy5pc1Njcm9sbERvd24gJiYgdGhpcy5teVNjcm9sbENvbnRhaW5lci5uYXRpdmVFbGVtZW50LnNjcm9sbFRvcCA9PT0gMCkge1xyXG4gICAgICB0aGlzLnNjcm9sbFRvQm90dG9tKHRoaXMubXlTY3JvbGxDb250YWluZXIubmF0aXZlRWxlbWVudC5zY3JvbGxIZWlnaHQpO1xyXG4gICAgICB0aGlzLmxvYWRNb3JlID0gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9ubG9hZE1vcmVDb21tZW50cyhldmVudERhdGEpOiB2b2lkIHtcclxuICAgIGxldCB0ZW1wUGFnZURldGFpbHMgPSB0aGlzLnBhZ2VEZXRhaWxzO1xyXG4gICAgaWYgKHRlbXBQYWdlRGV0YWlscykge1xyXG4gICAgICB0ZW1wUGFnZURldGFpbHMucGFnZSA9IHRlbXBQYWdlRGV0YWlscy5wYWdlICsgMTtcclxuICAgICAgdGVtcFBhZ2VEZXRhaWxzLnNraXAgPSB0ZW1wUGFnZURldGFpbHMuc2tpcCArIDEwMDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRlbXBQYWdlRGV0YWlscyA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLnJlc2V0UGFnZURldGFpbHMoKTtcclxuICAgIH1cclxuICAgIHRoaXMubG9hZGluZ01vcmVDb21tZW50cy5lbWl0KHRlbXBQYWdlRGV0YWlscyk7XHJcbiAgfVxyXG5cclxuICBvblJlcGx5VG9Db21tZW50KHBhcmVudENvbW1lbnQpOiB2b2lkIHtcclxuICAgIHRoaXMucmVwbHlUb0NvbW1lbnQuZW1pdChwYXJlbnRDb21tZW50KTtcclxuICB9XHJcblxyXG4gIHNjcm9sbFRvQm90dG9tKGhlaWdodCk6IHZvaWQge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy5teVNjcm9sbENvbnRhaW5lci5uYXRpdmVFbGVtZW50LnNjcm9sbFRvcCA9IGhlaWdodDtcclxuICAgIH0gY2F0Y2ggKGVycikgeyB9XHJcbiAgfVxyXG5cclxuICBvbkNsaWNraW5nT25SZXBseU1lc3NhZ2UocGFyZW50Q29tbWVudElkKTogdm9pZCB7XHJcbiAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21lc3NhZ2VfJyArIHBhcmVudENvbW1lbnRJZCkpIHtcclxuICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21lc3NhZ2VfJyArIHBhcmVudENvbW1lbnRJZCkuc2Nyb2xsSW50b1ZpZXcoeyBiZWhhdmlvcjogJ3Ntb290aCcsIGJsb2NrOiAnY2VudGVyJyB9KTtcclxuICAgICAgdGhpcy5uYXZpZ2F0ZVRvUmVwbHkgPSBudWxsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5uYXZpZ2F0ZVRvUmVwbHkgPSBwYXJlbnRDb21tZW50SWQ7XHJcbiAgICAgIHRoaXMub25sb2FkTW9yZUNvbW1lbnRzKG51bGwpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvYWRNb3JlID0gdGhpcy5pc1Njcm9sbERvd247XHJcbiAgICBpZiAodGhpcy5uYXZpZ2F0ZVRvUmVwbHkgJiYgdGhpcy5pc1Njcm9sbERvd24pIHtcclxuICAgICAgdGhpcy5vbkNsaWNraW5nT25SZXBseU1lc3NhZ2UodGhpcy5uYXZpZ2F0ZVRvUmVwbHkpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=