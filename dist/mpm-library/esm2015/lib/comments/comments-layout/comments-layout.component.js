import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
let CommentsLayoutComponent = class CommentsLayoutComponent {
    constructor() {
        this.creatingNewComment = new EventEmitter();
        this.loadingMoreComments = new EventEmitter();
        this.parentComment = null;
    }
    ngOnInit() {
    }
    onReplyToComment(parentComment) {
        this.parentComment = Object.assign({}, parentComment);
    }
    onCreatingNewComment(newComment) {
        this.creatingNewComment.emit(newComment);
    }
    onLoadingMoreComments(pageDetail) {
        this.loadingMoreComments.emit(pageDetail);
    }
};
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "commentDataList", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "pageDetails", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "isScrollDown", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "isReadonly", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "userListData", void 0);
__decorate([
    Output()
], CommentsLayoutComponent.prototype, "creatingNewComment", void 0);
__decorate([
    Output()
], CommentsLayoutComponent.prototype, "loadingMoreComments", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "enableToComment", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "isExternalUser", void 0);
__decorate([
    Input()
], CommentsLayoutComponent.prototype, "projectId", void 0);
CommentsLayoutComponent = __decorate([
    Component({
        selector: 'mpm-comments-layout',
        template: "<mpm-comments-message-layout [commentListData]=\"commentDataList\" (replyToComment)=\"onReplyToComment($event)\"\r\n    [pageDetails]=\"pageDetails\" (loadingMoreComments)=\"onLoadingMoreComments($event)\" [isScrollDown]=\"isScrollDown\"\r\n    [userListData]=\"userListData\">\r\n</mpm-comments-message-layout>\r\n<mpm-comments-text [parentComment]=\"parentComment\" (creatingNewComment)=\"onCreatingNewComment($event)\"\r\n    [userListData]=\"userListData\" *ngIf=\"!isReadonly\" [isExternalUser]=\"isExternalUser\" [enableToComment]=\"enableToComment\" [projectId]=\"projectId\">\r\n</mpm-comments-text>",
        styles: [""]
    })
], CommentsLayoutComponent);
export { CommentsLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2NvbW1lbnRzL2NvbW1lbnRzLWxheW91dC9jb21tZW50cy1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUS9FLElBQWEsdUJBQXVCLEdBQXBDLE1BQWEsdUJBQXVCO0lBWWxDO1FBTlUsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3Qyx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBSXhELGtCQUFhLEdBQUcsSUFBSSxDQUFDO0lBR3JCLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQztJQUNELGdCQUFnQixDQUFDLGFBQWE7UUFDNUIsSUFBSSxDQUFDLGFBQWEscUJBQVEsYUFBYSxDQUFFLENBQUM7SUFDNUMsQ0FBQztJQUNELG9CQUFvQixDQUFDLFVBQTJCO1FBQzlDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUNELHFCQUFxQixDQUFDLFVBQVU7UUFDOUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM1QyxDQUFDO0NBQ0YsQ0FBQTtBQTFCVTtJQUFSLEtBQUssRUFBRTtnRUFBNkI7QUFDNUI7SUFBUixLQUFLLEVBQUU7NERBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFOzZEQUF1QjtBQUN0QjtJQUFSLEtBQUssRUFBRTsyREFBcUI7QUFDcEI7SUFBUixLQUFLLEVBQUU7NkRBQW9DO0FBQ2xDO0lBQVQsTUFBTSxFQUFFO21FQUE4QztBQUM3QztJQUFULE1BQU0sRUFBRTtvRUFBK0M7QUFDL0M7SUFBUixLQUFLLEVBQUU7Z0VBQTBCO0FBQ3pCO0lBQVIsS0FBSyxFQUFFOytEQUFxQjtBQUNwQjtJQUFSLEtBQUssRUFBRTswREFBVztBQVZSLHVCQUF1QjtJQUxuQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUscUJBQXFCO1FBQy9CLDJtQkFBK0M7O0tBRWhELENBQUM7R0FDVyx1QkFBdUIsQ0EyQm5DO1NBM0JZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmV3Q29tbWVudE1vZGFsLCBVc2VySW5mb01vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9jb21tZW50Lm1vZGFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWNvbW1lbnRzLWxheW91dCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnRzLWxheW91dC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29tbWVudHMtbGF5b3V0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBjb21tZW50RGF0YUxpc3Q6IEFycmF5PGFueT47XHJcbiAgQElucHV0KCkgcGFnZURldGFpbHM6IGFueTtcclxuICBASW5wdXQoKSBpc1Njcm9sbERvd246IGJvb2xlYW47XHJcbiAgQElucHV0KCkgaXNSZWFkb25seTogYm9vbGVhbjtcclxuICBASW5wdXQoKSB1c2VyTGlzdERhdGE6IEFycmF5PFVzZXJJbmZvTW9kYWw+O1xyXG4gIEBPdXRwdXQoKSBjcmVhdGluZ05ld0NvbW1lbnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgbG9hZGluZ01vcmVDb21tZW50cyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBJbnB1dCgpIGVuYWJsZVRvQ29tbWVudDogYm9vbGVhbjtcclxuICBASW5wdXQoKSBpc0V4dGVybmFsVXNlcjogYW55O1xyXG4gIEBJbnB1dCgpIHByb2plY3RJZDtcclxuICBwYXJlbnRDb21tZW50ID0gbnVsbDtcclxuICBjb25zdHJ1Y3RvcihcclxuICApIHtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gIH1cclxuICBvblJlcGx5VG9Db21tZW50KHBhcmVudENvbW1lbnQpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyZW50Q29tbWVudCA9IHsgLi4ucGFyZW50Q29tbWVudCB9O1xyXG4gIH1cclxuICBvbkNyZWF0aW5nTmV3Q29tbWVudChuZXdDb21tZW50OiBOZXdDb21tZW50TW9kYWwpOiB2b2lkIHtcclxuICAgIHRoaXMuY3JlYXRpbmdOZXdDb21tZW50LmVtaXQobmV3Q29tbWVudCk7XHJcbiAgfVxyXG4gIG9uTG9hZGluZ01vcmVDb21tZW50cyhwYWdlRGV0YWlsKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvYWRpbmdNb3JlQ29tbWVudHMuZW1pdChwYWdlRGV0YWlsKTtcclxuICB9XHJcbn1cclxuIl19