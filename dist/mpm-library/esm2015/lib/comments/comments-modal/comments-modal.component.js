import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
let CommentsModalComponent = class CommentsModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    closeCommentsPopup(data) {
        if (this.dialogRef) {
            this.dialogRef.close(data);
        }
    }
    ngOnInit() {
        if (this.data) {
            this.projectId = this.data.projectId;
            this.isManager = this.data.isManager;
            this.deliverableId = this.data.deliverableId;
            this.reviewDeliverableId = this.data.reviewDeliverableId;
            this.enableToComment = this.data.enableToComment;
            // this.isExternalUser = this.data.isExternalUser;
        }
    }
};
CommentsModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
CommentsModalComponent = __decorate([
    Component({
        selector: 'mpm-comments-modal',
        template: "<mat-dialog-content>\r\n    <button matTooltip=\"Close Comments\" mat-icon-button color=\"primary\" [mat-dialog-close]=\"true\"\r\n        class=\"mat-icon-close-btn\">\r\n        <mat-icon>close</mat-icon>\r\n    </button>\r\n    <mpm-comments [projectId]=\"projectId\" [isManager]=\"isManager\" [deliverableId]=\"deliverableId\" [reviewDeliverableId]=\"reviewDeliverableId\" [enableToComment]=\"enableToComment\"></mpm-comments>\r\n</mat-dialog-content>",
        styles: ["mat-dialog-content{margin:0;padding:0;max-height:90vh;height:88vh;position:relative}.mat-icon-close-btn{top:-4px;right:.5em;position:absolute}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], CommentsModalComponent);
export { CommentsModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMtbW9kYWwvY29tbWVudHMtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBT2pDLFlBQ1MsU0FBK0MsRUFDdEIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUFzQztRQUN0QixTQUFJLEdBQUosSUFBSSxDQUFBO0lBQ2xDLENBQUM7SUFFTCxrQkFBa0IsQ0FBQyxJQUFJO1FBQ3JCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDN0MsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDekQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUNsRCxrREFBa0Q7U0FDbEQ7SUFDSCxDQUFDO0NBRUYsQ0FBQTs7WUFyQnFCLFlBQVk7NENBQzdCLE1BQU0sU0FBQyxlQUFlOztBQVRkLHNCQUFzQjtJQUxsQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLG1kQUE4Qzs7S0FFL0MsQ0FBQztJQVVHLFdBQUEsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO0dBVGYsc0JBQXNCLENBNkJsQztTQTdCWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWNvbW1lbnRzLW1vZGFsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudHMtbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1lbnRzLW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHByb2plY3RJZDogbnVtYmVyO1xyXG4gIGlzTWFuYWdlcjogYm9vbGVhbjtcclxuICBkZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgcmV2aWV3RGVsaXZlcmFibGVJZDogbnVtYmVyO1xyXG4gIGVuYWJsZVRvQ29tbWVudDogYm9vbGVhbjtcclxuICBpc0V4dGVybmFsVXNlcjogYW55O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPENvbW1lbnRzTW9kYWxDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhLFxyXG4gICkgeyB9XHJcblxyXG4gIGNsb3NlQ29tbWVudHNQb3B1cChkYXRhKSB7XHJcbiAgICBpZiAodGhpcy5kaWFsb2dSZWYpIHtcclxuICAgICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmRhdGEpIHtcclxuICAgICAgdGhpcy5wcm9qZWN0SWQgPSB0aGlzLmRhdGEucHJvamVjdElkO1xyXG4gICAgICB0aGlzLmlzTWFuYWdlciA9IHRoaXMuZGF0YS5pc01hbmFnZXI7XHJcbiAgICAgIHRoaXMuZGVsaXZlcmFibGVJZCA9IHRoaXMuZGF0YS5kZWxpdmVyYWJsZUlkO1xyXG4gICAgICB0aGlzLnJldmlld0RlbGl2ZXJhYmxlSWQgPSB0aGlzLmRhdGEucmV2aWV3RGVsaXZlcmFibGVJZDtcclxuICAgICAgdGhpcy5lbmFibGVUb0NvbW1lbnQgPSB0aGlzLmRhdGEuZW5hYmxlVG9Db21tZW50O1xyXG4gICAgIC8vIHRoaXMuaXNFeHRlcm5hbFVzZXIgPSB0aGlzLmRhdGEuaXNFeHRlcm5hbFVzZXI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=