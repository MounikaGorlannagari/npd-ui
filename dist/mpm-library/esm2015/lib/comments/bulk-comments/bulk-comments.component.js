import { __decorate, __param } from "tslib";
import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../notification/notification.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
let BulkCommentsComponent = class BulkCommentsComponent {
    constructor(dialogRef, data, projectData, notificationService, formBuilder) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.projectData = projectData;
        this.notificationService = notificationService;
        this.formBuilder = formBuilder;
        this.mandatory = false;
        this.bulkCRAction = new EventEmitter();
    }
    closeDialog() {
        this.dialogRef.close(false);
    }
    bulkAction() {
        if (this.data.statusData) {
            const comment = this.commentFormGroup.value.CommentText;
            const reasons = this.commentFormGroup.value.reason;
            const SelectedReasons = []; //any;
            if (reasons) {
                reasons.forEach(reason => {
                    if (this.crStatusReasons !== undefined) {
                        const tempReason = this.crStatusReasons.find(element => element.VALUE === reason);
                        SelectedReasons.push({
                            Name: tempReason.NAME,
                            Value: tempReason.VALUE
                        });
                    }
                    else {
                        const tempReason = this.statusReasons.find(element => element.NAME === reason);
                        SelectedReasons.push({
                            //  'MPM_Status_Reason-id': {
                            Id: tempReason && tempReason['MPM_Status_Reason-id'] ? tempReason['MPM_Status_Reason-id'].Id :
                                (tempReason && tempReason['MPM_CR_Status_Reason-id'] ? tempReason['MPM_CR_Status_Reason-id'].Id : null),
                        });
                    }
                });
            }
            const actionData = {
                comment: '',
                SelectedReasons: []
            };
            const hasCommentData = comment.trim() === '' ? false : true;
            actionData.comment = hasCommentData ? comment.trim() : '';
            actionData.SelectedReasons = SelectedReasons;
            if ((this.commentFormGroup && this.commentFormGroup.status === 'INVALID') || (this.requireComments && !hasCommentData)) {
                this.notificationService.info('Kindly fill the comments field');
            }
            else {
                this.dialogRef.close(actionData);
            }
        }
        else {
            const bulkComment = this.commentFormGroup.value.CommentText;
            this.system = bulkComment.trim() === '' ? true : bulkComment;
            const data = {
                comment: bulkComment.trim()
            };
            if (this.commentFormGroup && this.commentFormGroup.status === 'INVALID' && this.system) {
                this.notificationService.info('Kindly fill the comments field');
            } /* else if (this.commentFormGroup.status === 'VALID' && bulkComment.trim() === '') {
      
            } */
            else {
                this.dialogRef.close(this.system);
            }
        }
    }
    ngOnInit() {
        if (this.data.statusData) {
            this.requireComments = this.data.statusData.REQUIRE_COMMENTS === 'true' ? true : false;
            this.requireReasons = this.data.statusData.REQUIRE_REASON === 'true' ? true : false;
            this.mandatory = this.data.status === '' ? true : false;
            if (this.data.statusData.MPM_CR_Status_Reason) {
                this.enableReason = true;
                if (Array.isArray(this.data.statusData.MPM_CR_Status_Reason)) {
                    this.crStatusReasons = this.data.statusData.MPM_CR_Status_Reason;
                }
                else {
                    this.crStatusReasons = [this.data.statusData.MPM_CR_Status_Reason];
                }
                this.commentFormGroup = this.formBuilder.group({
                    CommentText: new FormControl('', [Validators.maxLength(500)]),
                    reason: new FormControl()
                });
            }
            else if (this.data.statusData.MPM_Status_Reason) {
                this.enableReason = true;
                if (this.data.statusData.MPM_Status_Reason && this.data.statusData.MPM_Status_Reason.MPM_Status_Reason && Array.isArray(this.data.statusData.MPM_Status_Reason.MPM_Status_Reason)) {
                    this.statusReasons = this.data.statusData.MPM_Status_Reason.MPM_Status_Reason;
                }
                else {
                    this.statusReasons = this.data.statusData.MPM_Status_Reason && this.data.statusData.MPM_Status_Reason.MPM_Status_Reason ? [this.data.statusData.MPM_Status_Reason.MPM_Status_Reason] : [];
                }
                if (!(this.statusReasons && this.statusReasons.length > 0)) {
                    if (this.data.statusData.MPM_Status_Reason && this.data.statusData.MPM_Status_Reason.MPM_CR_Status_Reason && Array.isArray(this.data.statusData.MPM_Status_Reason.MPM_CR_Status_Reason)) {
                        this.statusReasons = this.data.statusData.MPM_Status_Reason.MPM_CR_Status_Reason;
                    }
                    else {
                        this.statusReasons = this.data.statusData.MPM_Status_Reason && this.data.statusData.MPM_Status_Reason.MPM_CR_Status_Reason ? [this.data.statusData.MPM_Status_Reason.MPM_CR_Status_Reason] : [];
                    }
                }
                this.commentFormGroup = this.formBuilder.group({
                    CommentText: new FormControl('', [Validators.maxLength(500)]),
                    reason: new FormControl()
                });
            }
            else {
                this.enableReason = false;
                this.commentFormGroup = this.formBuilder.group({
                    CommentText: new FormControl('', [Validators.maxLength(500)])
                });
            }
        }
        else if (this.data.status === 'Requested Changes' || this.data.status === '') {
            this.mandatory = true;
            this.commentFormGroup = this.formBuilder.group({
                CommentText: new FormControl('', [Validators.maxLength(500), Validators.required]),
            });
        }
        else {
            this.mandatory = false;
            this.commentFormGroup = this.formBuilder.group({
                CommentText: new FormControl('', [Validators.maxLength(500)]),
            });
        }
    }
};
BulkCommentsComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: NotificationService },
    { type: FormBuilder }
];
__decorate([
    Output()
], BulkCommentsComponent.prototype, "bulkCRAction", void 0);
BulkCommentsComponent = __decorate([
    Component({
        selector: 'mpm-bulk-comments',
        template: "<h1 mat-dialog-title class=\"bulk-comments-modal-title\">{{data.message}}</h1>\r\n<button matTooltip=\"Close\" mat-icon-button color=\"primary\" (click)=\"closeDialog()\" [mat-dialog-close]=\"true\"\r\n    class=\"mat-icon-close-btn\">\r\n    <mat-icon>close</mat-icon>\r\n</button>\r\n<mat-divider></mat-divider>\r\n<div class=\"bulk-comments-state\" *ngIf=\"data.status != '' \">\r\n    <h4>{{data.statusMessage}} :\r\n        {{data.statusData ? data.statusData.STATUS_NAME ? data.statusData.STATUS_NAME : data.statusData ?\r\n        data.statusData.NAME : data.statusData : data.statusData != undefined && data.statusData.NAME ?\r\n        data.statusData.NAME : data.status}}\r\n    </h4>\r\n</div>\r\n<form [formGroup]=\"commentFormGroup\" class=\"comment-form\">\r\n    <div class=\"flex-row\" *ngIf=\"enableReason\">\r\n\r\n        <div class=\"flex-row-item\" *ngIf=\"crStatusReasons !==  undefined\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Reason</mat-label>\r\n                <mat-select formControlName=\"reason\" placeholder=\"Reason\" name=\"reason\"\r\n                    [required]=\"mandatory || requireReasons\" multiple>\r\n                    <mat-option *ngFor=\"let reason of crStatusReasons\" value=\"{{reason.VALUE}}\">\r\n                        <span matTooltip=\"{{reason.NAME}}\"> {{reason.NAME}}</span>\r\n                    </mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"flex-row-item\" *ngIf=\"statusReasons !== undefined && statusReasons.length > 0\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Reason</mat-label>\r\n\r\n                <mat-select id=\"form-item\" formControlName=\"reason\" placeholder=\"Reason\" name=\"reason\"\r\n                    [required]=\"mandatory || requireReasons\" multiple>\r\n                    <mat-option *ngFor=\"let reason of statusReasons\" value=\"{{reason.NAME}}\">\r\n                        <span matTooltip=\"{{reason.NAME}}\"> {{reason.NAME}}</span>\r\n                    </mat-option>\r\n                </mat-select>\r\n\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <!-- class=\"text-comment-area\" -->\r\n                <mat-label>Comments</mat-label>\r\n                <textarea matInput placeholder=\"Type a Comment\" formControlName=\"CommentText\" aria-label=\"label\"\r\n                    rows=\"3\" [required]=\"mandatory || requireComments\">\r\n            </textarea>\r\n                <mat-hint align=\"end\"\r\n                    *ngIf=\"commentFormGroup.get('CommentText').errors && commentFormGroup.get('CommentText').errors.maxlength\">\r\n                    {{commentFormGroup.get('CommentText').errors.maxlength.actualLength}}/{{commentFormGroup.get('CommentText').errors.maxlength.requiredLength}}\r\n                </mat-hint>\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\" *ngIf=\"data.note\">\r\n        <div class=\"flex-row-item\">\r\n            <span style=\"color: red;\">Note: {{data.note}}</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-col-item\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"form-actions\" id=\"button-action\">\r\n                    <button mat-stroked-button type=\"button\" (click)=\"closeDialog()\">\r\n                        Close\r\n                    </button>\r\n                    <button id=\"bulk-comments-button\" mat-flat-button color=\"primary\" type=\"button\"\r\n                        (click)=\"bulkAction()\" [disabled]=\"(commentFormGroup.invalid)\">\r\n                        Done\r\n                    </button>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>",
        styles: [".mat-icon-close-btn{float:right;top:-57px}.text-comment-area{width:100%}.bulk-comments-modal-title{font-weight:700;flex-grow:1}.mat-form-field{width:100%}.bulk-comments-state{padding-top:10px}.form-actions{margin-left:auto}#bulk-comments-button{margin-right:0;border-radius:4px;margin-left:15px}.comment-form{display:flex;flex-direction:column;width:100%}#button-action{margin-bottom:5px}::ng-deep div#form-item-panel{min-width:calc(100% + 14px)!important;margin-left:33px}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA)),
    __param(2, Inject(MAT_DIALOG_DATA))
], BulkCommentsComponent);
export { BulkCommentsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVsay1jb21tZW50cy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50cy9idWxrLWNvbW1lbnRzL2J1bGstY29tbWVudHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBT2pGLElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBYWhDLFlBQ1MsU0FBOEMsRUFDckIsSUFBUyxFQUNULFdBQWdCLEVBQ3hDLG1CQUF3QyxFQUN4QyxXQUF3QjtRQUp6QixjQUFTLEdBQVQsU0FBUyxDQUFxQztRQUNyQixTQUFJLEdBQUosSUFBSSxDQUFLO1FBQ1QsZ0JBQVcsR0FBWCxXQUFXLENBQUs7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWRsQyxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBT1IsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBUTdDLENBQUM7SUFFTCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQztJQUVELFVBQVU7UUFDUixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3hCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1lBQ3hELE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ25ELE1BQU0sZUFBZSxHQUFHLEVBQUUsQ0FBQSxDQUFDLE1BQU07WUFDakMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDdkIsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLFNBQVMsRUFBRTt3QkFDdEMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxDQUFDO3dCQUNsRixlQUFlLENBQUMsSUFBSSxDQUFDOzRCQUNuQixJQUFJLEVBQUUsVUFBVSxDQUFDLElBQUk7NEJBQ3JCLEtBQUssRUFBRSxVQUFVLENBQUMsS0FBSzt5QkFDeEIsQ0FBQyxDQUFDO3FCQUNKO3lCQUFNO3dCQUNMLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxNQUFNLENBQUMsQ0FBQzt3QkFDL0UsZUFBZSxDQUFDLElBQUksQ0FBQzs0QkFDbkIsNkJBQTZCOzRCQUM3QixFQUFFLEVBQUUsVUFBVSxJQUFJLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQ0FDNUYsQ0FBQyxVQUFVLElBQUksVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO3lCQUsxRyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUNELE1BQU0sVUFBVSxHQUFHO2dCQUNqQixPQUFPLEVBQUUsRUFBRTtnQkFDWCxlQUFlLEVBQUUsRUFBRTthQUNwQixDQUFDO1lBQ0YsTUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDNUQsVUFBVSxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzFELFVBQVUsQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDO1lBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDdEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2xDO1NBQ0Y7YUFBTTtZQUNMLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1lBRTVELElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFDN0QsTUFBTSxJQUFJLEdBQUc7Z0JBQ1gsT0FBTyxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQUU7YUFDNUIsQ0FBQztZQUVGLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQzthQUNqRSxDQUFDOztnQkFFRTtpQkFBTTtnQkFDUixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDbkM7U0FDRjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN4QixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDdkYsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwRixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDeEQsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDN0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO29CQUM1RCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDO2lCQUNsRTtxQkFBTTtvQkFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztpQkFDcEU7Z0JBQ0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29CQUM3QyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUM3RCxNQUFNLEVBQUUsSUFBSSxXQUFXLEVBQUU7aUJBQzFCLENBQUMsQ0FBQzthQUNKO2lCQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsRUFBRTtvQkFDakwsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDL0U7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzNMO2dCQUNELElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQzFELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO3dCQUN2TCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDO3FCQUNsRjt5QkFBTTt3QkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQkFDak07aUJBQ0Y7Z0JBQ0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29CQUM3QyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUM3RCxNQUFNLEVBQUUsSUFBSSxXQUFXLEVBQUU7aUJBQzFCLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7b0JBQzdDLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzlELENBQUMsQ0FBQzthQUNKO1NBQ0Y7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLG1CQUFtQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLEVBQUUsRUFBRTtZQUM5RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzdDLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNuRixDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUM3QyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQzlELENBQUMsQ0FBQztTQUNKO0lBRUgsQ0FBQztDQUNGLENBQUE7O1lBekhxQixZQUFZOzRDQUM3QixNQUFNLFNBQUMsZUFBZTs0Q0FDdEIsTUFBTSxTQUFDLGVBQWU7WUFDTSxtQkFBbUI7WUFDM0IsV0FBVzs7QUFQeEI7SUFBVCxNQUFNLEVBQUU7MkRBQXdDO0FBWHRDLHFCQUFxQjtJQUxqQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLGk5SEFBNkM7O0tBRTlDLENBQUM7SUFnQkcsV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7SUFDdkIsV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FoQmYscUJBQXFCLENBdUlqQztTQXZJWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWJ1bGstY29tbWVudHMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9idWxrLWNvbW1lbnRzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9idWxrLWNvbW1lbnRzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEJ1bGtDb21tZW50c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgY29tbWVudEZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gIHN5c3RlbTtcclxuICBjclJvbGU6IGFueTtcclxuICBtYW5kYXRvcnkgPSBmYWxzZTtcclxuICByZXF1aXJlQ29tbWVudHM6IGJvb2xlYW47XHJcbiAgcmVxdWlyZVJlYXNvbnM6IGJvb2xlYW47XHJcbiAgZW5hYmxlUmVhc29uOiBib29sZWFuO1xyXG4gIGNyU3RhdHVzUmVhc29ucztcclxuICBzdGF0dXNSZWFzb25zO1xyXG5cclxuICBAT3V0cHV0KCkgYnVsa0NSQWN0aW9uID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEJ1bGtDb21tZW50c0NvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgcHJvamVjdERhdGE6IGFueSxcclxuICAgIHByaXZhdGUgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyXHJcbiAgKSB7IH1cclxuXHJcbiAgY2xvc2VEaWFsb2coKSB7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShmYWxzZSk7XHJcbiAgfVxyXG5cclxuICBidWxrQWN0aW9uKCkge1xyXG4gICAgaWYgKHRoaXMuZGF0YS5zdGF0dXNEYXRhKSB7XHJcbiAgICAgIGNvbnN0IGNvbW1lbnQgPSB0aGlzLmNvbW1lbnRGb3JtR3JvdXAudmFsdWUuQ29tbWVudFRleHQ7XHJcbiAgICAgIGNvbnN0IHJlYXNvbnMgPSB0aGlzLmNvbW1lbnRGb3JtR3JvdXAudmFsdWUucmVhc29uO1xyXG4gICAgICBjb25zdCBTZWxlY3RlZFJlYXNvbnMgPSBbXSAvL2FueTtcclxuICAgICAgaWYgKHJlYXNvbnMpIHtcclxuICAgICAgICByZWFzb25zLmZvckVhY2gocmVhc29uID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLmNyU3RhdHVzUmVhc29ucyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRlbXBSZWFzb24gPSB0aGlzLmNyU3RhdHVzUmVhc29ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5WQUxVRSA9PT0gcmVhc29uKTtcclxuICAgICAgICAgICAgU2VsZWN0ZWRSZWFzb25zLnB1c2goe1xyXG4gICAgICAgICAgICAgIE5hbWU6IHRlbXBSZWFzb24uTkFNRSxcclxuICAgICAgICAgICAgICBWYWx1ZTogdGVtcFJlYXNvbi5WQUxVRVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRlbXBSZWFzb24gPSB0aGlzLnN0YXR1c1JlYXNvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQuTkFNRSA9PT0gcmVhc29uKTtcclxuICAgICAgICAgICAgU2VsZWN0ZWRSZWFzb25zLnB1c2goe1xyXG4gICAgICAgICAgICAgIC8vICAnTVBNX1N0YXR1c19SZWFzb24taWQnOiB7XHJcbiAgICAgICAgICAgICAgSWQ6IHRlbXBSZWFzb24gJiYgdGVtcFJlYXNvblsnTVBNX1N0YXR1c19SZWFzb24taWQnXSA/IHRlbXBSZWFzb25bJ01QTV9TdGF0dXNfUmVhc29uLWlkJ10uSWQgOlxyXG4gICAgICAgICAgICAgICAgKHRlbXBSZWFzb24gJiYgdGVtcFJlYXNvblsnTVBNX0NSX1N0YXR1c19SZWFzb24taWQnXSA/IHRlbXBSZWFzb25bJ01QTV9DUl9TdGF0dXNfUmVhc29uLWlkJ10uSWQgOiBudWxsKSxcclxuICAgICAgICAgICAgIC8qICBOYW1lOiB0ZW1wUmVhc29uLk5BTUUsXHJcbiAgICAgICAgICAgICAgVmFsdWU6IHRlbXBSZWFzb24uVkFMVUUgKi9cclxuICAgICAgICAgICAgICAvLyBNUE1fQ1JfU3RhdHVzX1JlYXNvbi1pZFxyXG4gICAgICAgICAgICAgIC8vfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBhY3Rpb25EYXRhID0ge1xyXG4gICAgICAgIGNvbW1lbnQ6ICcnLFxyXG4gICAgICAgIFNlbGVjdGVkUmVhc29uczogW11cclxuICAgICAgfTtcclxuICAgICAgY29uc3QgaGFzQ29tbWVudERhdGEgPSBjb21tZW50LnRyaW0oKSA9PT0gJycgPyBmYWxzZSA6IHRydWU7XHJcbiAgICAgIGFjdGlvbkRhdGEuY29tbWVudCA9IGhhc0NvbW1lbnREYXRhID8gY29tbWVudC50cmltKCkgOiAnJztcclxuICAgICAgYWN0aW9uRGF0YS5TZWxlY3RlZFJlYXNvbnMgPSBTZWxlY3RlZFJlYXNvbnM7XHJcbiAgICAgIGlmICgodGhpcy5jb21tZW50Rm9ybUdyb3VwICYmIHRoaXMuY29tbWVudEZvcm1Hcm91cC5zdGF0dXMgPT09ICdJTlZBTElEJykgfHwgKHRoaXMucmVxdWlyZUNvbW1lbnRzICYmICFoYXNDb21tZW50RGF0YSkpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IGZpbGwgdGhlIGNvbW1lbnRzIGZpZWxkJyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoYWN0aW9uRGF0YSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGJ1bGtDb21tZW50ID0gdGhpcy5jb21tZW50Rm9ybUdyb3VwLnZhbHVlLkNvbW1lbnRUZXh0O1xyXG5cclxuICAgICAgdGhpcy5zeXN0ZW0gPSBidWxrQ29tbWVudC50cmltKCkgPT09ICcnID8gdHJ1ZSA6IGJ1bGtDb21tZW50O1xyXG4gICAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICAgIGNvbW1lbnQ6IGJ1bGtDb21tZW50LnRyaW0oKVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHRoaXMuY29tbWVudEZvcm1Hcm91cCAmJiB0aGlzLmNvbW1lbnRGb3JtR3JvdXAuc3RhdHVzID09PSAnSU5WQUxJRCcgJiYgdGhpcy5zeXN0ZW0pIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IGZpbGwgdGhlIGNvbW1lbnRzIGZpZWxkJyk7XHJcbiAgICAgIH0gLyogZWxzZSBpZiAodGhpcy5jb21tZW50Rm9ybUdyb3VwLnN0YXR1cyA9PT0gJ1ZBTElEJyAmJiBidWxrQ29tbWVudC50cmltKCkgPT09ICcnKSB7XHJcblxyXG4gICAgICB9ICovIGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHRoaXMuc3lzdGVtKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5kYXRhLnN0YXR1c0RhdGEpIHtcclxuICAgICAgdGhpcy5yZXF1aXJlQ29tbWVudHMgPSB0aGlzLmRhdGEuc3RhdHVzRGF0YS5SRVFVSVJFX0NPTU1FTlRTID09PSAndHJ1ZScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIHRoaXMucmVxdWlyZVJlYXNvbnMgPSB0aGlzLmRhdGEuc3RhdHVzRGF0YS5SRVFVSVJFX1JFQVNPTiA9PT0gJ3RydWUnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICB0aGlzLm1hbmRhdG9yeSA9IHRoaXMuZGF0YS5zdGF0dXMgPT09ICcnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICBpZiAodGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX0NSX1N0YXR1c19SZWFzb24pIHtcclxuICAgICAgICB0aGlzLmVuYWJsZVJlYXNvbiA9IHRydWU7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX0NSX1N0YXR1c19SZWFzb24pKSB7XHJcbiAgICAgICAgICB0aGlzLmNyU3RhdHVzUmVhc29ucyA9IHRoaXMuZGF0YS5zdGF0dXNEYXRhLk1QTV9DUl9TdGF0dXNfUmVhc29uO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmNyU3RhdHVzUmVhc29ucyA9IFt0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fQ1JfU3RhdHVzX1JlYXNvbl07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgICAgQ29tbWVudFRleHQ6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwMCldKSxcclxuICAgICAgICAgIHJlYXNvbjogbmV3IEZvcm1Db250cm9sKClcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fU3RhdHVzX1JlYXNvbikge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlUmVhc29uID0gdHJ1ZTtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24gJiYgdGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24uTVBNX1N0YXR1c19SZWFzb24gJiYgQXJyYXkuaXNBcnJheSh0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fU3RhdHVzX1JlYXNvbi5NUE1fU3RhdHVzX1JlYXNvbikpIHtcclxuICAgICAgICAgIHRoaXMuc3RhdHVzUmVhc29ucyA9IHRoaXMuZGF0YS5zdGF0dXNEYXRhLk1QTV9TdGF0dXNfUmVhc29uLk1QTV9TdGF0dXNfUmVhc29uO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnN0YXR1c1JlYXNvbnMgPSB0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fU3RhdHVzX1JlYXNvbiAmJiB0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fU3RhdHVzX1JlYXNvbi5NUE1fU3RhdHVzX1JlYXNvbiA/IFt0aGlzLmRhdGEuc3RhdHVzRGF0YS5NUE1fU3RhdHVzX1JlYXNvbi5NUE1fU3RhdHVzX1JlYXNvbl0gOiBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCEodGhpcy5zdGF0dXNSZWFzb25zICYmIHRoaXMuc3RhdHVzUmVhc29ucy5sZW5ndGggPiAwKSkge1xyXG4gICAgICAgICAgaWYgKHRoaXMuZGF0YS5zdGF0dXNEYXRhLk1QTV9TdGF0dXNfUmVhc29uICYmIHRoaXMuZGF0YS5zdGF0dXNEYXRhLk1QTV9TdGF0dXNfUmVhc29uLk1QTV9DUl9TdGF0dXNfUmVhc29uICYmIEFycmF5LmlzQXJyYXkodGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24uTVBNX0NSX1N0YXR1c19SZWFzb24pKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdHVzUmVhc29ucyA9IHRoaXMuZGF0YS5zdGF0dXNEYXRhLk1QTV9TdGF0dXNfUmVhc29uLk1QTV9DUl9TdGF0dXNfUmVhc29uO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zdGF0dXNSZWFzb25zID0gdGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24gJiYgdGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24uTVBNX0NSX1N0YXR1c19SZWFzb24gPyBbdGhpcy5kYXRhLnN0YXR1c0RhdGEuTVBNX1N0YXR1c19SZWFzb24uTVBNX0NSX1N0YXR1c19SZWFzb25dIDogW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgICAgQ29tbWVudFRleHQ6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwMCldKSxcclxuICAgICAgICAgIHJlYXNvbjogbmV3IEZvcm1Db250cm9sKClcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmVuYWJsZVJlYXNvbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgICAgQ29tbWVudFRleHQ6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDUwMCldKVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZGF0YS5zdGF0dXMgPT09ICdSZXF1ZXN0ZWQgQ2hhbmdlcycgfHwgdGhpcy5kYXRhLnN0YXR1cyA9PT0gJycpIHtcclxuICAgICAgdGhpcy5tYW5kYXRvcnkgPSB0cnVlO1xyXG4gICAgICB0aGlzLmNvbW1lbnRGb3JtR3JvdXAgPSB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgICBDb21tZW50VGV4dDogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTAwKSwgVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubWFuZGF0b3J5ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgIENvbW1lbnRUZXh0OiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCg1MDApXSksXHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICB9XHJcbn1cclxuIl19