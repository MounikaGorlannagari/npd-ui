import * as $ from 'jquery';
export function getSAMLAssertionByUserDetails(username, password) {
    const defer = $.Deferred();
    $.soap({
        header: {
            'wsse:Security': {
                '@xmlns:wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                'wsse:UsernameToken': {
                    '@xmlns:wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                    'wsse:Username': username,
                    'wsse:Password': password
                }
            }
        },
        body: {
            'samlp:Request': {
                '@xmlns:samlp': 'urn:oasis:names:tc:SAML:1.0:protocol',
                '@MajorVersion': '1',
                '@MinorVersion': '1',
                '@IssueInstant': new Date().toString(),
                'samlp:AuthenticationQuery': {
                    'saml:Subject': {
                        '@xmlns:saml': 'urn:oasis:names:tc:SAML:1.0:assertion',
                        'saml:NameIdentifier': {
                            '@Format': 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
                            __text: username
                        }
                    }
                }
            }
        }
    }).done((result) => {
        defer.resolve(result.Response.AssertionArtifact.__text, (username && password));
    }).fail((err) => {
        defer.reject();
    });
    return defer.promise();
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0U0FNTEFzc2VydGlvbkJ5VXNlckRldGFpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9tcG0tdXRpbHMvYXV0aC9nZXRTQU1MQXNzZXJ0aW9uQnlVc2VyRGV0YWlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixNQUFNLFVBQVUsNkJBQTZCLENBQUMsUUFBUSxFQUFFLFFBQVE7SUFDNUQsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBRTNCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDSCxNQUFNLEVBQUU7WUFDSixlQUFlLEVBQUU7Z0JBQ2IsYUFBYSxFQUFFLG1GQUFtRjtnQkFDbEcsb0JBQW9CLEVBQUU7b0JBQ2xCLGFBQWEsRUFBRSxtRkFBbUY7b0JBQ2xHLGVBQWUsRUFBRSxRQUFRO29CQUN6QixlQUFlLEVBQUUsUUFBUTtpQkFDNUI7YUFDSjtTQUNKO1FBQ0QsSUFBSSxFQUFFO1lBQ0YsZUFBZSxFQUFFO2dCQUNiLGNBQWMsRUFBRSxzQ0FBc0M7Z0JBQ3RELGVBQWUsRUFBRSxHQUFHO2dCQUNwQixlQUFlLEVBQUUsR0FBRztnQkFDcEIsZUFBZSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUN0QywyQkFBMkIsRUFBRTtvQkFDekIsY0FBYyxFQUFFO3dCQUNaLGFBQWEsRUFBRSx1Q0FBdUM7d0JBQ3RELHFCQUFxQixFQUFFOzRCQUNuQixTQUFTLEVBQUUsdURBQXVEOzRCQUNsRSxNQUFNLEVBQUUsUUFBUTt5QkFDbkI7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1FBQ2YsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3BGLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1FBQ1osS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25CLENBQUMsQ0FBQyxDQUFDO0lBRUgsT0FBTyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7QUFDM0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRTQU1MQXNzZXJ0aW9uQnlVc2VyRGV0YWlscyh1c2VybmFtZSwgcGFzc3dvcmQpIHtcclxuICAgIGNvbnN0IGRlZmVyID0gJC5EZWZlcnJlZCgpO1xyXG5cclxuICAgICQuc29hcCh7XHJcbiAgICAgICAgaGVhZGVyOiB7XHJcbiAgICAgICAgICAgICd3c3NlOlNlY3VyaXR5Jzoge1xyXG4gICAgICAgICAgICAgICAgJ0B4bWxuczp3c3NlJzogJ2h0dHA6Ly9kb2NzLm9hc2lzLW9wZW4ub3JnL3dzcy8yMDA0LzAxL29hc2lzLTIwMDQwMS13c3Mtd3NzZWN1cml0eS1zZWNleHQtMS4wLnhzZCcsXHJcbiAgICAgICAgICAgICAgICAnd3NzZTpVc2VybmFtZVRva2VuJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICdAeG1sbnM6d3NzZSc6ICdodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktc2VjZXh0LTEuMC54c2QnLFxyXG4gICAgICAgICAgICAgICAgICAgICd3c3NlOlVzZXJuYW1lJzogdXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgJ3dzc2U6UGFzc3dvcmQnOiBwYXNzd29yZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBib2R5OiB7XHJcbiAgICAgICAgICAgICdzYW1scDpSZXF1ZXN0Jzoge1xyXG4gICAgICAgICAgICAgICAgJ0B4bWxuczpzYW1scCc6ICd1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjA6cHJvdG9jb2wnLFxyXG4gICAgICAgICAgICAgICAgJ0BNYWpvclZlcnNpb24nOiAnMScsXHJcbiAgICAgICAgICAgICAgICAnQE1pbm9yVmVyc2lvbic6ICcxJyxcclxuICAgICAgICAgICAgICAgICdASXNzdWVJbnN0YW50JzogbmV3IERhdGUoKS50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgJ3NhbWxwOkF1dGhlbnRpY2F0aW9uUXVlcnknOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ3NhbWw6U3ViamVjdCc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0B4bWxuczpzYW1sJzogJ3VybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMDphc3NlcnRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnc2FtbDpOYW1lSWRlbnRpZmllcic6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdARm9ybWF0JzogJ3VybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9fdGV4dDogdXNlcm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pLmRvbmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgIGRlZmVyLnJlc29sdmUocmVzdWx0LlJlc3BvbnNlLkFzc2VydGlvbkFydGlmYWN0Ll9fdGV4dCwgKHVzZXJuYW1lICYmIHBhc3N3b3JkKSk7XHJcbiAgICB9KS5mYWlsKChlcnIpID0+IHtcclxuICAgICAgICBkZWZlci5yZWplY3QoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBkZWZlci5wcm9taXNlKCk7XHJcbn1cclxuIl19