import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as acronui from '../auth/utility';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { GloabalConfig as config } from '../config/config';
import { SharingService } from './sharing.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "./sharing.service";
let AppService = class AppService {
    constructor(sharingService) {
        this.sharingService = sharingService;
        this.USER_NS = 'http://schemas.cordys.com/1.1/ldap';
        this.GET_ALL_APPLICATION_DETAILS_NS = 'http://schemas.acheron.com/mpm/app/bpm/1.0';
        this.PERSON_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.MPM_APP_ROLES_NS = 'http://schemas/AcheronMPMOrganizationModel/MPM_APP_Roles/operations';
        this.MEDIA_MANAGER_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Media_Manager_Config/operations';
        this.WSAPP_CORE_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.GET_PERSON_BY_USER_ID_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.GET_PERSON_BY_IDENTITY_USER_ID_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.TEAMS_BPM_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
        this.REVIEW_BPM_NS = 'http://schemas.acheron.com/mpm/review/bpm/1.0';
        this.APP_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_App_Config/operations';
        this.PROJECT_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Project_Config/operations';
        this.ASSET_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Asset_Config/operations';
        this.BRAND_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Brand_Config/operations';
        this.COMMENT_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Comments_Config/operations';
        this.BULK_CR_APPROVAL_NS = 'http://schemas.acheron.com/mpm/review/bpm/1.0';
        this.VIEW_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_View_Config/operations';
        this.TEAM_ROLE_MAPPING_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Team_Role_Mapping/operations';
        this.GET_ALL_TEAMS_NS = 'http://schemas/AcheronMPMCore/MPM_Teams/operations';
        this.GET_ALL_PERSONS_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.DELIVERABLE_REVIEW_METHOD_NS = 'http://schemas/AcheronMPMCore/DeliverableReview/operations';
        this.TASK_REVIEW_METHOD_NS = 'http://schemas/AcheronMPMCore/DeliverableReview/operations';
        this.MPM_EVENTS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Events/operations';
        this.MPM_WORKFLOW_ACTIONS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Workflow_Actions/operations';
        this.GET_EMAIL_EVENTS_FOR_MANAGER_NS = 'http://schemas/AcheronMPMCore/Email_Event_Template/operations';
        this.GET_EMAIL_EVENTS_FOR_USER_NS = 'http://schemas/AcheronMPMCore/Email_Event_Template/operations';
        this.GET_MAIL_PREFERENCE_BY_USER_ID_NS = 'http://schemas/AcheronMPMCore/MPM_Email_Preference/operations';
        this.GET_USER_PREFERENCE_MAIL_TEMPLATES_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.MPM_WORKFLOW_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.DELETE_MPM_EMAIL_PREFERENCE_NS = 'http://schemas/AcheronMPMCore/MPM_Email_Preference/operations';
        this.MPM_USER_PREFERENCE_NS = 'http://schemas/AcheronMPMCore/MPM_User_Preference/operations';
        this.CAMPAIGN_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';
        this.STATUS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Status/operations';
        this.GET_ALL_USERS_NS = 'http://schemas/OpenTextEntityIdentityComponents/User/operations';
        this.GET_EXPORT_DATA_BY_USER_NS = 'http://schemas/AcheronMPMCore/MPM_Export_Data/operations';
        this.DOWNLOAD_FILE_BY_PATH_NS = 'http://schemas.acheron.com/mpm/wsapp/excel/1.0';
        this.MPM_NOTIFICATION_NS = 'http://schemas/AcheronMPMCore/Notification/operations';
        this.EMAIL_CONFIG_NS = 'http://schemas/AcheronMPMCore/General_Email_Config/operations';
        this.CURSOR_NS = 'http://schemas.opentext.com/bps/entity/core';
        this.GET_PERSON_DETAILS_BY_USER_ID_WS = 'GetPersonByUserId';
        this.GET_ALL_APPLICATION_DETAILS_WS = 'GetApplicationForUser';
        this.GET_ALL_ROLES_WS = 'GetAllRoles';
        this.GET_MEDIA_MANAGER_CONFIG_WS = 'GetMediaManagerConfig';
        this.GET_PERSON_BY_IDENTITY_USER_ID_WS = 'GetPersonByIdentityUserId';
        this.GET_TEAM_ROLES_WS = 'GetTeamRoles';
        this.GET_USES_FOR_TEAM_WS = 'GetUsersForTeam';
        this.GET_POSSIBLE_CR_ACTIONS_FOR_USER_WS = 'GetPossibileCRActionsForUser';
        this.OPEN_FILE_PROOFING_SESSION_WS = 'OpenFileProofSession';
        this.GET_APP_CONFIG_BY_ID_WS = 'GetAppConfigById';
        this.GET_PROJECT_CONFIG_BY_ID_WS = 'GetProjectConfigById';
        this.GET_ASSET_CONFIG_BY_ID_WS = 'GetAssetConfigById';
        this.GET_BRAND_CONFIG_BY_ID_WS = 'GetBrandConfigById';
        this.GET_COMMENT_CONFIG_BY_ID_WS = 'GetCommentsConfigById';
        this.HANDEL_CR_APPROVAL_WS = 'HandleCRApproval';
        this.GET_ALL_MPM_VIEW_CONFIG_WS = 'GetAllMPMViewConfig';
        this.GET_ALL_TEAM_ROLES_WS = 'GetAllTeamRoles';
        this.GET_FILE_PROOFING_SESSION_WS = 'GetFileProofingSession';
        this.GET_ALL_TEAMS_WS = 'GetAllTeams ';
        this.GET_ALL_PERSONS_WS = 'GetAllPersons';
        this.GET_PERSONS_BY_KEYWORD_WS = 'GetPersonsByKeyword';
        this.GET_DELIVERABLE_REVIEW_BY_DELIVERABLE_ID_WS = 'GetDeliverableReviewByDeliverableID';
        this.GET_DELIVERABLE_REVIEW_BY_TASK_ID_WS = 'GetDeliverableReviewByTaskID';
        this.GET_USERS_FOR_TEAM_ROLE_WS = 'GetUsersForTeamRole';
        this.GET_MPM_EVENTS_BY_CATEGORY_LEVEL_WS = 'GetMPMEventsByCategoryLevel';
        this.GET_WORKFLOW_ACTIONS_BY_CATEGORY_WS = 'GetWorkflowActionsByCategory';
        this.GET_EMAIL_EVENTS_FOR_MANAGER_WS = 'GetEmailEventsForManager';
        this.GET_EMAIL_EVENTS_FOR_USER_WS = 'GetEmailEventsForUser';
        this.GET_MAIL_PREFERENCE_BY_USER_ID_WS = 'GetMailPreferenceByUserId';
        this.GET_USER_PREFERENCE_MAIL_TEMPLATES_WS = 'GetUserPreferenceMailTemplates';
        this.MAIL_PREFERENCE_WS = 'MailPreference';
        this.DELETE_MPM_EMAIL_PREFERENCE_WS = 'DeleteMPM_Email_Preference ';
        this.EXPORT_DATA_WS = 'ExportData';
        this.GET_EXPORT_DATA_BY_USER_WS = 'GetExportDataByUser';
        this.DOWNLOAD_FILE_BY_PATH_WS = 'DownloadFileByPath';
        this.GET_MPM_USER_PREFERENCE_BY_USER_WS = 'GetMPMUserPreferenceByUser';
        this.CREATE_MPM_USER_PREFERENCE_WS = 'CreateMPM_User_Preference';
        this.UPDATE_MPM_USER_PREFERENCE_WS = 'UpdateMPM_User_Preference';
        this.DELETE_MPM_USER_PREFERENCE_WS = 'DeleteMPM_User_Preference';
        this.GET_ALL_CAMPAIGN_WS_METHOD_NAME = 'GetAllCampaign';
        this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS = 'GetAllStatusForCategoryLevel';
        this.GET_ALL_USERS = 'GetAllUsers';
        this.GET_ALL_STATUS = 'GetAllStatus';
        this.UPDATE_DEFAULT_USER_PREFERENCE_WS = 'UpdateDefaultUserPreference';
        this.GET_NOTIFICATION_BY_USER_WS = 'GetNotificationByUser';
        this.UPDATE_NOTIFICATION_WS = 'UpdateNotification';
        this.GET_DEFAULT_EMAIL_CONFIG_WS = 'GetDefaultEmailConfig';
        this.DELETE_MPM_EXPORT_DATA_BY_ID_WS = 'DeleteMPM_Export_Data';
        this.DELETE_MPM_EXPORT_DATA_BY_ID_NS = 'http://schemas/AcheronMPMCore/MPM_Export_Data/operations';
    }
    invokeRequest(namespace, method, parameters) {
        if (namespace == null || namespace === '' || typeof (namespace) === 'undefined') {
            console.warn('Please provide valid namespace.');
            return;
        }
        if (method == null || method === '' || typeof (method) === 'undefined') {
            console.warn('Please provide valid method name.');
            return;
        }
        if (parameters == null || parameters === '' || typeof (parameters) === 'undefined') {
            parameters = '';
        }
        return new Observable(appServiceObserver => {
            $.soap({
                namespace,
                method,
                parameters
            }).then((data) => {
                appServiceObserver.next(data);
                appServiceObserver.complete();
            }, (error) => {
                appServiceObserver.error(new Error(error));
            });
        });
    }
    // MPMV3-2008
    getAllUsers(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_USERS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_USERS)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.GET_ALL_USERS_NS, this.GET_ALL_USERS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_USERS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    // added for code hardeniing -keerthana
    getStatusByCategoryLevelName(categoryLevelName) {
        const getRequestObject = {
            categoryLevelName: categoryLevelName,
            categoryLevelID: ''
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.STATUS_METHOD_NS, this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS, getRequestObject)
                    .subscribe(response => {
                    const statuses = acronui.findObjectsByProp(response, 'MPM_Status');
                    sessionStorage.setItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME, JSON.stringify(statuses));
                    observer.next(statuses);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
    // added for code hardeniing -keerthana
    /* getAllCampaign(parameters?): Observable<any> {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_CAMPAIGN) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_CAMPAIGN)));
                observer.complete();
            } else {
                this.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_ALL_CAMPAIGN_WS_METHOD_NAME, null).subscribe(campaignDetails => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_CAMPAIGN, JSON.stringify(campaignDetails));
                    observer.next(campaignDetails);
                    observer.complete();
                });
            }
        });
    } */
    // added for code hardeniing 
    getUserDetailsLDAP(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.USER_NS, 'GetUserDetails', '').subscribe(userDetails => {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_LDAP, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    }
    logIn(callback) {
        /*  const getUser: Observable<any> = this.invokeRequest(this.USER_NS, 'GetUserDetails', '');
         getUser.subscribe(response => { */
        this.getUserDetailsLDAP().subscribe(response => {
            const userObject = acronui.findObjectsByProp(response, 'user');
            const userName = userObject[0].authuserdn.split(',')[0].split('=')[1];
            if (userName === 'anonymous') {
                window.location.href = config.config.logoutUrl;
            }
            else {
                if (callback) {
                    callback(true);
                }
            }
        }, errData => {
            console.error('Error while firing GetUserDetails : ' + errData.status);
            if (callback) {
                callback(false);
            }
        });
    }
    checkPSSession(callback) {
        this.logIn(callback);
    }
    getPersonCastedObj(data) {
        const userDetails = {
            Id: '',
            ItemId: '',
            userCN: '',
            fullName: '',
            userId: ''
        };
        if (data) {
            if (data.Person && data.Person.User_ID && data.Person.User_ID.__text) {
                userDetails.userCN = data.Person.User_ID.__text;
            }
            if (data.Person && data.Person['Person-id'] && data.Person['Person-id'].Id) {
                userDetails.Id = data.Person['Person-id'].Id;
            }
            if (data.Person && data.Person['Person-id'] && data.Person['Person-id'].ItemId) {
                userDetails.ItemId = data.Person['Person-id'].ItemId;
            }
            if (data.Person && data.Person.DisplayName && data.Person.DisplayName.__text) {
                userDetails.fullName = data.Person.DisplayName.__text;
            }
            if (data.Person && data.Person.PersonToUser && data.Person.PersonToUser['Identity-id'] && data.Person.PersonToUser['Identity-id'].Id) {
                userDetails.userId = data.Person.PersonToUser['Identity-id'].Id;
            }
        }
        return userDetails;
    }
    getPersonByIdentityUserId(parameters) {
        return new Observable(observer => {
            this.invokeRequest(this.GET_PERSON_BY_IDENTITY_USER_ID_NS, this.GET_PERSON_BY_IDENTITY_USER_ID_WS, parameters)
                .subscribe(data => {
                const userDetails = this.getPersonCastedObj(data);
                observer.next(userDetails);
                observer.complete();
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
    /* getPersonByIdentityUserId(parameters?): Observable<Person> {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.GET_PERSON_BY_IDENTITY_USER_ID_NS, this.GET_PERSON_BY_IDENTITY_USER_ID_WS, parameters).subscribe(data => {
                    const userDetails: Person = this.getPersonCastedObj(data);
                    sessionStorage.setItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    }
 */
    getPersonByUserId(parameters) {
        return new Observable(observer => {
            this.invokeRequest(this.GET_PERSON_BY_USER_ID_NS, this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters)
                .subscribe(data => {
                const userDetails = this.getPersonCastedObj(data);
                observer.next(userDetails);
                observer.complete();
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
    /*  getPersonByUserId(parameters?): Observable<Person> {
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.PERSON_BY_USER_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PERSON_BY_USER_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.GET_PERSON_BY_USER_ID_NS, this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters)
                     .subscribe(data => {
                         const userDetails: Person = this.getPersonCastedObj(data);
                         sessionStorage.setItem(SessionStorageConstants.PERSON_BY_USER_ID, JSON.stringify(userDetails));
                         observer.next(userDetails);
                         observer.complete();
                     }, error => {
                         observer.error(error);
                         observer.complete();
                     });
             }
         });
     } */
    /* getAllApplicationDetails(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_APPLICATION_DETAILS_NS, this.GET_ALL_APPLICATION_DETAILS_WS, parameters);
    } */
    getAllApplicationDetails(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_APPLICATION_DETAILS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_APPLICATION_DETAILS)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.GET_ALL_APPLICATION_DETAILS_NS, this.GET_ALL_APPLICATION_DETAILS_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_APPLICATION_DETAILS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    getProjectConfigById(id) {
        const parameters = {
            projectConfigId: id
        };
        return this.invokeRequest(this.PROJECT_CONFIG_NS, this.GET_PROJECT_CONFIG_BY_ID_WS, parameters);
    }
    /* getProjectConfigById(id): Observable<any> {
        const parameters = {
            projectConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.PROJECT_CONFIG_NS, this.GET_PROJECT_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    getAppConfigById(id) {
        const parameters = {
            appConfigId: id
        };
        return this.invokeRequest(this.APP_CONFIG_NS, this.GET_APP_CONFIG_BY_ID_WS, parameters);
    }
    /* getAppConfigById(id): Observable<any> {
        const parameters = {
            appConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.APP_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.APP_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.APP_CONFIG_NS, this.GET_APP_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.APP_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    getAssetConfigById(id) {
        const parameters = {
            assetConfigId: id
        };
        return this.invokeRequest(this.ASSET_CONFIG_NS, this.GET_ASSET_CONFIG_BY_ID_WS, parameters);
    }
    /*  getAssetConfigById(id): Observable<any> {
         const parameters = {
             assetConfigId: id
         };
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.ASSET_CONFIG_BY_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ASSET_CONFIG_BY_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.ASSET_CONFIG_NS, this.GET_ASSET_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                     sessionStorage.setItem(SessionStorageConstants.ASSET_CONFIG_BY_ID, JSON.stringify(response));
                     observer.next(response);
                     observer.complete();
                 });
             }
         });
     } */
    getCommentConfigById(id) {
        const parameters = {
            commentConfigId: id
        };
        return this.invokeRequest(this.COMMENT_CONFIG_NS, this.GET_COMMENT_CONFIG_BY_ID_WS, parameters);
    }
    /*  getCommentConfigById(id): Observable<any> {
         const parameters = {
             commentConfigId: id
         };
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.COMMENT_CONFIG_NS, this.GET_COMMENT_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                     sessionStorage.setItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID, JSON.stringify(response));
                     observer.next(response);
                     observer.complete();
                 });
             }
         });
     } */
    getBrandConfigById(id) {
        const parameters = {
            brandConfigId: id
        };
        return this.invokeRequest(this.BRAND_CONFIG_NS, this.GET_BRAND_CONFIG_BY_ID_WS, parameters);
    }
    /* getBrandConfigById(id): Observable<any> {
        const parameters = {
            brandConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.BRAND_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.BRAND_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.BRAND_CONFIG_NS, this.GET_BRAND_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.BRAND_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    /* getAllViewConfig(parameters?): Observable<any> {
        return this.invokeRequest(this.VIEW_CONFIG_NS, this.GET_ALL_MPM_VIEW_CONFIG_WS, '');
    } */
    getAllViewConfig(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_VIEW_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_VIEW_CONFIG)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.VIEW_CONFIG_NS, this.GET_ALL_MPM_VIEW_CONFIG_WS, '').subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_VIEW_CONFIG, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    getUsersForRole(parameters) {
        return this.invokeRequest(this.USER_NS, 'GetUsersForRole', parameters);
    }
    getOTDSTicketForUser(parameters) {
        return this.invokeRequest(this.WSAPP_CORE_NS, 'GetOTDSTicketForUser', parameters);
    }
    getPersonDetailsByUserCN(parameters) {
        return this.invokeRequest(this.PERSON_NS, this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters);
    }
    /* getAllRoles(parameters?) {
        return this.invokeRequest(this.MPM_APP_ROLES_NS, this.GET_ALL_ROLES_WS, parameters);
    } */
    getAllRoles(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_ROLES) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_ROLES)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.MPM_APP_ROLES_NS, this.GET_ALL_ROLES_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_ROLES, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    getTaskEvents() {
        const parameters = {
            CategoryLevel: 'TASK'
        };
        return this.invokeRequest(this.MPM_EVENTS_METHOD_NS, this.GET_MPM_EVENTS_BY_CATEGORY_LEVEL_WS, parameters);
    }
    getTaskWorkflowActions() {
        const parameters = {
            categoryLevel: 'TASK'
        };
        return this.invokeRequest(this.MPM_WORKFLOW_ACTIONS_METHOD_NS, this.GET_WORKFLOW_ACTIONS_BY_CATEGORY_WS, parameters);
    }
    /*  getMediaManagerConfig(parameters): Observable<any> {
         return this.invokeRequest(this.MEDIA_MANAGER_CONFIG_NS, this.GET_MEDIA_MANAGER_CONFIG_WS, parameters);
     } */
    getMediaManagerConfig(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.MEDIA_MANAGER_CONFIG_NS, this.GET_MEDIA_MANAGER_CONFIG_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    getTeamRoles(parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_TEAM_ROLES_WS, parameters);
    }
    /*     getAllTeamRoles(parameters?): Observable<any> {
            return this.invokeRequest(this.TEAM_ROLE_MAPPING_METHOD_NS, this.GET_ALL_TEAM_ROLES_WS, parameters);
        } */
    getAllTeamRoles(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_TEAM_ROLES) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_TEAM_ROLES)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.TEAM_ROLE_MAPPING_METHOD_NS, this.GET_ALL_TEAM_ROLES_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_TEAM_ROLES, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    /* getAllTeams(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_TEAMS_NS, this.GET_ALL_TEAMS_WS, parameters);
    } */
    getAllTeams(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_TEAMS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_TEAMS)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.GET_ALL_TEAMS_NS, this.GET_ALL_TEAMS_WS, parameters).subscribe(response => {
                    console.log(response);
                    sessionStorage.setItem(SessionStorageConstants.ALL_TEAMS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    /* getAllPersons(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_PERSONS_NS, this.GET_ALL_PERSONS_WS, parameters);
    } */
    getAllPersons(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_PERSONS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_PERSONS)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.GET_ALL_PERSONS_NS, this.GET_ALL_PERSONS_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_PERSONS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    }
    getPersonsByKeyword(userKeyword) {
        const parameters = {
            userKeyword: userKeyword,
            Cursor: {
                '@xmlns': this.CURSOR_NS,
                '@offset': 0,
                '@limit': 30
            }
        };
        return new Observable(observer => {
            this.invokeRequest(this.GET_ALL_PERSONS_NS, this.GET_PERSONS_BY_KEYWORD_WS, parameters).subscribe(response => {
                observer.next(response);
                observer.complete();
            });
        });
    }
    getUsersForTeam(parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_USES_FOR_TEAM_WS, parameters);
    }
    getUsersForTeamRole(parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_USERS_FOR_TEAM_ROLE_WS, parameters);
    }
    getDeliverableReviewByDeliverableID(id) {
        const parameters = {
            DeliverableID: id
        };
        return this.invokeRequest(this.DELIVERABLE_REVIEW_METHOD_NS, this.GET_DELIVERABLE_REVIEW_BY_DELIVERABLE_ID_WS, parameters);
    }
    getDeliverableReviewByTaskID(id) {
        const parameters = {
            TaskID: id
        };
        return this.invokeRequest(this.TASK_REVIEW_METHOD_NS, this.GET_DELIVERABLE_REVIEW_BY_TASK_ID_WS, parameters);
    }
    getPossibleCRActionsForUser() {
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_POSSIBLE_CR_ACTIONS_FOR_USER_WS, null);
    }
    openFileProofingSession(appId, deliverableId, assetId, taskName, reviewRoleId, versioning) {
        const parameters = {
            openFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: {
                        deliverableId: deliverableId,
                        versioning: versioning
                    }
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    }
                }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.OPEN_FILE_PROOFING_SESSION_WS, parameters);
    }
    getFileProofingSession(appId, deliverableId, assetId, taskName, reviewRoleId, versioning) {
        const parameters = {
            getFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: {
                        deliverableId: deliverableId,
                        versioning: versioning
                    }
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    },
                },
                person: null
                // {
                //     firstName: this.sharingService.getCurrentUserDisplayName() ,
                //     lastName: '',
                //     id: this.sharingService.getCurrentUserItemID()
                // }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_FILE_PROOFING_SESSION_WS, parameters);
    }
    getMultipleFileProofingSession(appId, deliverable, assetId, taskName, reviewRoleId, versioning) {
        const parameters = {
            getFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: deliverable
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    },
                },
                person: null
                // {
                //     firstName: this.sharingService.getCurrentUserDisplayName() ,
                //     lastName: '',
                //     id: this.sharingService.getCurrentUserItemID()
                // }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_FILE_PROOFING_SESSION_WS, parameters);
    }
    bulkDeliverableId(deliverableId, versioning, taskName) {
        const deliverables = [];
        deliverableId.forEach(element => {
            deliverables.push({
                deliverableId: element,
                versioning: versioning,
                taskName: taskName
            });
        });
        return deliverables;
    }
    handleCRApproval(appId, deliverableId, assetId, taskName, reviewRoleId, versioning, commentText, isBulkApproval, statusId, selectedReasonsData, userCN) {
        const parameters = {
            SetStatusApproval: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                statusId: statusId,
                deliverableDetails: {
                    deliverable: this.bulkDeliverableId(deliverableId, versioning, taskName)
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    }
                },
                commentText: commentText,
                selectedReasons: {
                    ApprovalReason: selectedReasonsData
                },
                isBulkApproval: isBulkApproval,
                reviewerUserCN: userCN
            }
        };
        return this.invokeRequest(this.BULK_CR_APPROVAL_NS, this.HANDEL_CR_APPROVAL_WS, parameters);
    }
    /*  getCurrentUserPreference(): Observable<any> {
         const parameter = {
             UserId: this.sharingService.getcurrentUserItemID()
         };
         return this.invokeRequest(this.MPM_USER_PREFERENCE_NS, this.GET_MPM_USER_PREFERENCE_BY_USER_WS, parameter);
     } */
    getCurrentUserPreference() {
        const parameter = {
            UserId: this.sharingService.getcurrentUserItemID()
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.MPM_USER_PREFERENCE_NS, this.GET_MPM_USER_PREFERENCE_BY_USER_WS, parameter).subscribe(response => {
                    // sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(response));
                    const userPreferenceResponse = response.MPM_User_Preference ? response.MPM_User_Preference : [];
                    observer.next(userPreferenceResponse);
                    observer.complete();
                });
            }
        });
    }
    updateUserPreference(requestObj, existingUserPreferenceId) {
        return new Observable(observer => {
            let parameter;
            if (existingUserPreferenceId) {
                parameter = {
                    'MPM_User_Preference-id': {
                        Id: existingUserPreferenceId
                    },
                    'MPM_User_Preference-update': requestObj
                };
            }
            else {
                parameter = {
                    'MPM_User_Preference-create': requestObj
                };
            }
            this.invokeRequest(this.MPM_USER_PREFERENCE_NS, existingUserPreferenceId ? this.UPDATE_MPM_USER_PREFERENCE_WS : this.CREATE_MPM_USER_PREFERENCE_WS, parameter).subscribe(response => {
                if (response && response.MPM_User_Preference) {
                    this.sharingService.updateCurrentUserPreference(response.MPM_User_Preference);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
    updateDefaultUserPreference(viewId, filterName, menuId, fieldData) {
        return new Observable(observer => {
            const parameter = {
                ViewId: viewId,
                FilterName: filterName,
                PersonId: this.sharingService.getCurrentPerson().Id,
                MenuId: menuId,
                FieldData: fieldData
            };
            this.invokeRequest(this.MPM_WORKFLOW_NS, this.UPDATE_DEFAULT_USER_PREFERENCE_WS, parameter).subscribe(response => {
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data && response.APIResponse.data.MPM_User_Preference) {
                    this.sharingService.updateCurrentUserPreference(response.APIResponse.data.MPM_User_Preference);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
    deleteUserPreference(userPreferenceId) {
        return new Observable(observer => {
            const parameter = {
                MPM_User_Preference: {
                    'MPM_User_Preference-id': {
                        Id: userPreferenceId
                    }
                }
            };
            this.invokeRequest(this.MPM_USER_PREFERENCE_NS, this.DELETE_MPM_USER_PREFERENCE_WS, parameter).subscribe(response => {
                if (response) {
                    this.sharingService.removeCurrentUserPreference(userPreferenceId);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, error => {
                observer.error(error);
                observer.complete();
            });
        });
    }
    /*
    MPMV3-1448
    */
    GetEmailEventsForManager(parameters) {
        return this.invokeRequest(this.GET_EMAIL_EVENTS_FOR_MANAGER_NS, this.GET_EMAIL_EVENTS_FOR_MANAGER_WS, '');
    }
    /*
    MPMV3-1448
    */
    GetEmailEventsForUser(parameters) {
        return this.invokeRequest(this.GET_EMAIL_EVENTS_FOR_USER_NS, this.GET_EMAIL_EVENTS_FOR_USER_WS, '');
    }
    GetMailPreferenceByUserId(userId) {
        const parameters = {
            userId: userId
        };
        return this.invokeRequest(this.GET_MAIL_PREFERENCE_BY_USER_ID_NS, this.GET_MAIL_PREFERENCE_BY_USER_ID_WS, parameters);
    }
    GetUserPreferenceMailTemplates(userId) {
        const parameters = {
            userId: userId
        };
        return this.invokeRequest(this.GET_USER_PREFERENCE_MAIL_TEMPLATES_NS, this.GET_USER_PREFERENCE_MAIL_TEMPLATES_WS, parameters);
    }
    MailPreference(userId, enabledEvents, disabledEvents) {
        const parameters = {
            UserId: userId,
            EnabledEvents: {
                // email: enabledEvents
                'Email_Event_Template-id': enabledEvents
            },
            DisabledEvents: {
                // disable: disabledEvents
                'Email_Event_Template-id': disabledEvents
            }
        };
        return this.invokeRequest(this.MPM_WORKFLOW_NS, this.MAIL_PREFERENCE_WS, parameters);
    }
    DeleteMPM_Email_Preference(userEmailPreferenceId) {
        const parameters = {
            MPM_Email_Preference: {
                'MPM_Email_Preference-id': userEmailPreferenceId
            }
        };
        return this.invokeRequest(this.DELETE_MPM_EMAIL_PREFERENCE_NS, this.DELETE_MPM_EMAIL_PREFERENCE_WS, parameters);
    }
    exportData(columns, fileName, exportType, itemId, filePath, searchRequest, pageLimit, totalCount, exportDataMinCount) {
        const otdsTicket = sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET);
        const currentUserId = sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID);
        const parameters = {
            columns: columns,
            fileName: fileName,
            exportType: exportType,
            filePath: filePath,
            IndexerSearchRequest: searchRequest,
            pageLimit: pageLimit,
            totalCount: totalCount,
            exportDataMinCount: exportDataMinCount,
            otdsTicket: (otdsTicket && otdsTicket != null) ? JSON.parse(otdsTicket) : "",
            currentUserId: (currentUserId && currentUserId != null) ? JSON.parse(currentUserId) : "",
            data: {
                ITEM_ID: itemId
            }
        };
        return this.invokeRequest(this.MPM_WORKFLOW_NS, this.EXPORT_DATA_WS, parameters);
    }
    getExportDataByUser(userCN) {
        const parameters = {
            UserCN: userCN
        };
        return this.invokeRequest(this.GET_EXPORT_DATA_BY_USER_NS, this.GET_EXPORT_DATA_BY_USER_WS, parameters);
    }
    // MPMV3-2368
    deleteExportDataById(id) {
        const parameters = {
            MPM_Export_Data: {
                'MPM_Export_Data-id': {
                    Id: id
                }
            }
        };
        return this.invokeRequest(this.DELETE_MPM_EXPORT_DATA_BY_ID_NS, this.DELETE_MPM_EXPORT_DATA_BY_ID_WS, parameters);
    }
    getFileEncodedString(filePath) {
        const parameter = {
            filePath: filePath
        };
        return new Observable(observer => {
            this.invokeRequest(this.DOWNLOAD_FILE_BY_PATH_NS, this.DOWNLOAD_FILE_BY_PATH_WS, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getNotificationForCurrentUser(skip, top) {
        const parameters = {
            UserId: this.sharingService.getCurrentPerson().Id,
            Cursor: {
                '@xmlns': this.CURSOR_NS,
                '@offset': skip,
                '@limit': top
            }
        };
        return this.invokeRequest(this.MPM_NOTIFICATION_NS, this.GET_NOTIFICATION_BY_USER_WS, parameters);
    }
    updateNotification(parameters) {
        return this.invokeRequest(this.MPM_NOTIFICATION_NS, this.UPDATE_NOTIFICATION_WS, parameters);
    }
    getDefaultEmailConfig() {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.EMAIL_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.EMAIL_CONFIG)));
                observer.complete();
            }
            else {
                this.invokeRequest(this.EMAIL_CONFIG_NS, this.GET_DEFAULT_EMAIL_CONFIG_WS, null).subscribe(response => {
                    if (response && response.General_Email_Config) {
                        observer.next(response.General_Email_Config);
                        observer.complete();
                    }
                    else {
                        observer.next(false);
                        observer.complete();
                    }
                });
            }
        });
    }
};
AppService.ctorParameters = () => [
    { type: SharingService }
];
AppService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppService_Factory() { return new AppService(i0.ɵɵinject(i1.SharingService)); }, token: AppService, providedIn: "root" });
AppService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AppService);
export { AppService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxLQUFLLE9BQU8sTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOzs7QUFPM0YsSUFBYSxVQUFVLEdBQXZCLE1BQWEsVUFBVTtJQXNHbkIsWUFDVyxjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFyR2xDLFlBQU8sR0FBRyxvQ0FBb0MsQ0FBQztRQUMvQyxtQ0FBOEIsR0FBRyw0Q0FBNEMsQ0FBQztRQUM5RSxjQUFTLEdBQUcsbUVBQW1FLENBQUM7UUFDaEYscUJBQWdCLEdBQUcscUVBQXFFLENBQUM7UUFDekYsNEJBQXVCLEdBQUcsbUVBQW1FLENBQUM7UUFDOUYsa0JBQWEsR0FBRywrQ0FBK0MsQ0FBQztRQUNoRSw2QkFBd0IsR0FBRyxtRUFBbUUsQ0FBQztRQUMvRixzQ0FBaUMsR0FBRyxtRUFBbUUsQ0FBQztRQUN4RyxpQkFBWSxHQUFHLDhDQUE4QyxDQUFDO1FBQzlELGtCQUFhLEdBQUcsK0NBQStDLENBQUM7UUFDaEUsa0JBQWEsR0FBRyx5REFBeUQsQ0FBQztRQUMxRSxzQkFBaUIsR0FBRyw2REFBNkQsQ0FBQztRQUNsRixvQkFBZSxHQUFHLDJEQUEyRCxDQUFDO1FBQzlFLG9CQUFlLEdBQUcsMkRBQTJELENBQUM7UUFDOUUsc0JBQWlCLEdBQUcsOERBQThELENBQUM7UUFDbkYsd0JBQW1CLEdBQUcsK0NBQStDLENBQUM7UUFDdEUsbUJBQWMsR0FBRywwREFBMEQsQ0FBQztRQUM1RSxnQ0FBMkIsR0FBRyxnRUFBZ0UsQ0FBQztRQUMvRixxQkFBZ0IsR0FBRyxvREFBb0QsQ0FBQztRQUN4RSx1QkFBa0IsR0FBRyxtRUFBbUUsQ0FBQztRQUN6RixpQ0FBNEIsR0FBRyw0REFBNEQsQ0FBQztRQUM1RiwwQkFBcUIsR0FBRyw0REFBNEQsQ0FBQztRQUNyRix5QkFBb0IsR0FBRyxxREFBcUQsQ0FBQztRQUM3RSxtQ0FBOEIsR0FBRywrREFBK0QsQ0FBQztRQUNqRyxvQ0FBK0IsR0FBRywrREFBK0QsQ0FBQztRQUNsRyxpQ0FBNEIsR0FBRywrREFBK0QsQ0FBQztRQUMvRixzQ0FBaUMsR0FBRywrREFBK0QsQ0FBQztRQUNwRywwQ0FBcUMsR0FBRywrQ0FBK0MsQ0FBQztRQUN4RixvQkFBZSxHQUFHLHNEQUFzRCxDQUFDO1FBQ3pFLG1DQUE4QixHQUFHLCtEQUErRCxDQUFDO1FBQ2pHLDJCQUFzQixHQUFHLDhEQUE4RCxDQUFDO1FBRXhGLCtCQUEwQixHQUFHLG1EQUFtRCxDQUFDO1FBQ2pGLHFCQUFnQixHQUFHLHFEQUFxRCxDQUFDO1FBRXpFLHFCQUFnQixHQUFHLGlFQUFpRSxDQUFDO1FBQ3JGLCtCQUEwQixHQUFHLDBEQUEwRCxDQUFDO1FBQ3hGLDZCQUF3QixHQUFHLGdEQUFnRCxDQUFDO1FBRTVFLHdCQUFtQixHQUFHLHVEQUF1RCxDQUFDO1FBQzlFLG9CQUFlLEdBQUcsK0RBQStELENBQUM7UUFFbEYsY0FBUyxHQUFHLDZDQUE2QyxDQUFDO1FBRTFELHFDQUFnQyxHQUFHLG1CQUFtQixDQUFDO1FBQ3ZELG1DQUE4QixHQUFHLHVCQUF1QixDQUFDO1FBQ3pELHFCQUFnQixHQUFHLGFBQWEsQ0FBQztRQUNqQyxnQ0FBMkIsR0FBRyx1QkFBdUIsQ0FBQztRQUN0RCxzQ0FBaUMsR0FBRywyQkFBMkIsQ0FBQztRQUNoRSxzQkFBaUIsR0FBRyxjQUFjLENBQUM7UUFDbkMseUJBQW9CLEdBQUcsaUJBQWlCLENBQUM7UUFDekMsd0NBQW1DLEdBQUcsOEJBQThCLENBQUM7UUFDckUsa0NBQTZCLEdBQUcsc0JBQXNCLENBQUM7UUFDdkQsNEJBQXVCLEdBQUcsa0JBQWtCLENBQUM7UUFDN0MsZ0NBQTJCLEdBQUcsc0JBQXNCLENBQUM7UUFDckQsOEJBQXlCLEdBQUcsb0JBQW9CLENBQUM7UUFDakQsOEJBQXlCLEdBQUcsb0JBQW9CLENBQUM7UUFDakQsZ0NBQTJCLEdBQUcsdUJBQXVCLENBQUM7UUFDdEQsMEJBQXFCLEdBQUcsa0JBQWtCLENBQUM7UUFDM0MsK0JBQTBCLEdBQUcscUJBQXFCLENBQUM7UUFDbkQsMEJBQXFCLEdBQUcsaUJBQWlCLENBQUM7UUFDMUMsaUNBQTRCLEdBQUcsd0JBQXdCLENBQUM7UUFDeEQscUJBQWdCLEdBQUcsY0FBYyxDQUFDO1FBQ2xDLHVCQUFrQixHQUFHLGVBQWUsQ0FBQztRQUNyQyw4QkFBeUIsR0FBRyxxQkFBcUIsQ0FBQztRQUNsRCxnREFBMkMsR0FBRyxxQ0FBcUMsQ0FBQztRQUNwRix5Q0FBb0MsR0FBRyw4QkFBOEIsQ0FBQztRQUN0RSwrQkFBMEIsR0FBRyxxQkFBcUIsQ0FBQztRQUNuRCx3Q0FBbUMsR0FBRyw2QkFBNkIsQ0FBQztRQUNwRSx3Q0FBbUMsR0FBRyw4QkFBOEIsQ0FBQztRQUVyRSxvQ0FBK0IsR0FBRywwQkFBMEIsQ0FBQztRQUM3RCxpQ0FBNEIsR0FBRyx1QkFBdUIsQ0FBQztRQUN2RCxzQ0FBaUMsR0FBRywyQkFBMkIsQ0FBQztRQUNoRSwwQ0FBcUMsR0FBRyxnQ0FBZ0MsQ0FBQztRQUN6RSx1QkFBa0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUN0QyxtQ0FBOEIsR0FBRyw2QkFBNkIsQ0FBQztRQUMvRCxtQkFBYyxHQUFHLFlBQVksQ0FBQztRQUM5QiwrQkFBMEIsR0FBRyxxQkFBcUIsQ0FBQztRQUNuRCw2QkFBd0IsR0FBRyxvQkFBb0IsQ0FBQztRQUVoRCx1Q0FBa0MsR0FBRyw0QkFBNEIsQ0FBQztRQUNsRSxrQ0FBNkIsR0FBRywyQkFBMkIsQ0FBQztRQUM1RCxrQ0FBNkIsR0FBRywyQkFBMkIsQ0FBQztRQUM1RCxrQ0FBNkIsR0FBRywyQkFBMkIsQ0FBQztRQUU1RCxvQ0FBK0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUNuRCx5Q0FBb0MsR0FBRyw4QkFBOEIsQ0FBQztRQUV0RSxrQkFBYSxHQUFHLGFBQWEsQ0FBQztRQUM5QixtQkFBYyxHQUFHLGNBQWMsQ0FBQztRQUNoQyxzQ0FBaUMsR0FBRyw2QkFBNkIsQ0FBQztRQUNsRSxnQ0FBMkIsR0FBRyx1QkFBdUIsQ0FBQztRQUN0RCwyQkFBc0IsR0FBRyxvQkFBb0IsQ0FBQztRQUM5QyxnQ0FBMkIsR0FBRyx1QkFBdUIsQ0FBQztRQUV0RCxvQ0FBK0IsR0FBRyx1QkFBdUIsQ0FBQztRQUMxRCxvQ0FBK0IsR0FBRywwREFBMEQsQ0FBQztJQUtoRyxDQUFDO0lBRUwsYUFBYSxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsVUFBVTtRQUN2QyxJQUFJLFNBQVMsSUFBSSxJQUFJLElBQUksU0FBUyxLQUFLLEVBQUUsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssV0FBVyxFQUFFO1lBQzdFLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUNBQWlDLENBQUMsQ0FBQztZQUNoRCxPQUFPO1NBQ1Y7UUFFRCxJQUFJLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxLQUFLLEVBQUUsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssV0FBVyxFQUFFO1lBQ3BFLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUNBQW1DLENBQUMsQ0FBQztZQUNsRCxPQUFPO1NBQ1Y7UUFFRCxJQUFJLFVBQVUsSUFBSSxJQUFJLElBQUksVUFBVSxLQUFLLEVBQUUsSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssV0FBVyxFQUFFO1lBQ2hGLFVBQVUsR0FBRyxFQUFFLENBQUM7U0FDbkI7UUFFRCxPQUFPLElBQUksVUFBVSxDQUFDLGtCQUFrQixDQUFDLEVBQUU7WUFDdkMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDSCxTQUFTO2dCQUNULE1BQU07Z0JBQ04sVUFBVTthQUNiLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDYixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlCLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xDLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNULGtCQUFrQixDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYTtJQUNiLFdBQVcsQ0FBQyxVQUFXO1FBQ25CLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDcEUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzNGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDcEYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsdUNBQXVDO0lBQ3ZDLDRCQUE0QixDQUFDLGlCQUF5QjtRQUNsRCxNQUFNLGdCQUFnQixHQUFHO1lBQ3JCLGlCQUFpQixFQUFFLGlCQUFpQjtZQUNwQyxlQUFlLEVBQUUsRUFBRTtTQUN0QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsNkJBQTZCLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3hGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6RyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLG9DQUFvQyxFQUMvRSxnQkFBZ0IsQ0FBQztxQkFDaEIsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixNQUFNLFFBQVEsR0FBa0IsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQztvQkFDbEYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyw2QkFBNkIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3hHLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1Q0FBdUM7SUFDdkM7Ozs7Ozs7Ozs7Ozs7UUFhSTtJQUVKLDZCQUE2QjtJQUM3QixrQkFBa0IsQ0FBQyxVQUFXO1FBQzFCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0YsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQzNFLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUMvRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxLQUFLLENBQUMsUUFBUTtRQUNWOzJDQUNtQztRQUNuQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDM0MsTUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMvRCxNQUFNLFFBQVEsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxRQUFRLEtBQUssV0FBVyxFQUFFO2dCQUMxQixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQzthQUNsRDtpQkFBTTtnQkFDSCxJQUFJLFFBQVEsRUFBRTtvQkFDVixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2xCO2FBQ0o7UUFDTCxDQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUU7WUFDVCxPQUFPLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2RSxJQUFJLFFBQVEsRUFBRTtnQkFDVixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjLENBQUMsUUFBUTtRQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxJQUFJO1FBQ25CLE1BQU0sV0FBVyxHQUFXO1lBQ3hCLEVBQUUsRUFBRSxFQUFFO1lBQ04sTUFBTSxFQUFFLEVBQUU7WUFDVixNQUFNLEVBQUUsRUFBRTtZQUNWLFFBQVEsRUFBRSxFQUFFO1lBQ1osTUFBTSxFQUFFLEVBQUU7U0FDYixDQUFDO1FBQ0YsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUNsRSxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQzthQUNuRDtZQUNELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUN4RSxXQUFXLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ2hEO1lBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLEVBQUU7Z0JBQzVFLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUM7YUFDeEQ7WUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO2dCQUMxRSxXQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUN6RDtZQUNELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2xJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ25FO1NBQ0o7UUFDRCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBRUQseUJBQXlCLENBQUMsVUFBVztRQUNqQyxPQUFPLElBQUksVUFBVSxDQUNqQixRQUFRLENBQUMsRUFBRTtZQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGlDQUFpQyxFQUFFLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxVQUFVLENBQUM7aUJBQ3pHLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDZCxNQUFNLFdBQVcsR0FBVyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7OztHQWVEO0lBQ0MsaUJBQWlCLENBQUMsVUFBVztRQUN6QixPQUFPLElBQUksVUFBVSxDQUNqQixRQUFRLENBQUMsRUFBRTtZQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxnQ0FBZ0MsRUFBRSxVQUFVLENBQUM7aUJBQy9GLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDZCxNQUFNLFdBQVcsR0FBVyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQWtCSztJQUVMOztRQUVJO0lBRUosd0JBQXdCLENBQUMsVUFBVztRQUNoQyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDbEYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25HLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxJQUFJLENBQUMsOEJBQThCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMxSCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDbEcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsRUFBRTtRQUNuQixNQUFNLFVBQVUsR0FBRztZQUNmLGVBQWUsRUFBRSxFQUFFO1NBQ3RCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNwRyxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7UUFnQkk7SUFFSixnQkFBZ0IsQ0FBQyxFQUFFO1FBQ2YsTUFBTSxVQUFVLEdBQUc7WUFDZixXQUFXLEVBQUUsRUFBRTtTQUNsQixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7OztRQWdCSTtJQUVKLGtCQUFrQixDQUFDLEVBQUU7UUFDakIsTUFBTSxVQUFVLEdBQUc7WUFDZixhQUFhLEVBQUUsRUFBRTtTQUNwQixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLHlCQUF5QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7OztTQWdCSztJQUVMLG9CQUFvQixDQUFDLEVBQUU7UUFDbkIsTUFBTSxVQUFVLEdBQUc7WUFDZixlQUFlLEVBQUUsRUFBRTtTQUN0QixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEcsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7O1NBZ0JLO0lBRUwsa0JBQWtCLENBQUMsRUFBRTtRQUNqQixNQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxFQUFFO1NBQ3BCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMseUJBQXlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDaEcsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7O1FBZ0JJO0lBRUo7O1FBRUk7SUFFSixnQkFBZ0IsQ0FBQyxVQUFXO1FBQ3hCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDMUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQywwQkFBMEIsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzlGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDMUYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLFVBQVU7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVELG9CQUFvQixDQUFDLFVBQVU7UUFDM0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsc0JBQXNCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQUVELHdCQUF3QixDQUFDLFVBQVU7UUFDL0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGdDQUFnQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRDs7UUFFSTtJQUVKLFdBQVcsQ0FBQyxVQUFXO1FBQ25CLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDcEUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDOUYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNwRixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxhQUFhO1FBQ1QsTUFBTSxVQUFVLEdBQUc7WUFDZixhQUFhLEVBQUUsTUFBTTtTQUN4QixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsbUNBQW1DLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDL0csQ0FBQztJQUVELHNCQUFzQjtRQUNsQixNQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxNQUFNO1NBQ3hCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDhCQUE4QixFQUFFLElBQUksQ0FBQyxtQ0FBbUMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUN6SCxDQUFDO0lBRUQ7O1NBRUs7SUFFTCxxQkFBcUIsQ0FBQyxVQUFXO1FBQzdCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUMvRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDaEcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2hILGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUMvRixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZLENBQUMsVUFBVTtRQUNuQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVEOztZQUVRO0lBRVIsZUFBZSxDQUFDLFVBQVc7UUFDdkIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN6RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFGLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMscUJBQXFCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM5RyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztRQUVJO0lBRUosV0FBVyxDQUFDLFVBQVc7UUFDbkIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUNwRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JGLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM5RixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3BGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztRQUVJO0lBRUosYUFBYSxDQUFDLFVBQVc7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN0RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3RGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1CQUFtQixDQUFDLFdBQVk7UUFDNUIsTUFBTSxVQUFVLEdBQUc7WUFDZixXQUFXLEVBQUUsV0FBVztZQUN4QixNQUFNLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTO2dCQUN4QixTQUFTLEVBQUUsQ0FBQztnQkFDWixRQUFRLEVBQUUsRUFBRTthQUNmO1NBQ0osQ0FBQTtRQUNELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLHlCQUF5QixFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDekcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLFVBQVU7UUFDdEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxVQUFVO1FBQzFCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQywwQkFBMEIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM5RixDQUFDO0lBRUQsbUNBQW1DLENBQUMsRUFBRTtRQUNsQyxNQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxFQUFFO1NBQ3BCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDRCQUE0QixFQUFFLElBQUksQ0FBQywyQ0FBMkMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMvSCxDQUFDO0lBRUQsNEJBQTRCLENBQUMsRUFBRTtRQUMzQixNQUFNLFVBQVUsR0FBRztZQUNmLE1BQU0sRUFBRSxFQUFFO1NBQ2IsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLG9DQUFvQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pILENBQUM7SUFFRCwyQkFBMkI7UUFDdkIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLG1DQUFtQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxLQUFhLEVBQUUsYUFBcUIsRUFBRSxPQUFlLEVBQUUsUUFBZ0IsRUFBRSxZQUFvQixFQUFFLFVBQWtCO1FBQ3JJLE1BQU0sVUFBVSxHQUFHO1lBQ2YsdUJBQXVCLEVBQUU7Z0JBQ3JCLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixrQkFBa0IsRUFBRTtvQkFDaEIsV0FBVyxFQUFFO3dCQUNULGFBQWEsRUFBRSxhQUFhO3dCQUM1QixVQUFVLEVBQUUsVUFBVTtxQkFDekI7aUJBQ0o7Z0JBQ0QsVUFBVSxFQUFFO29CQUNSLElBQUksRUFBRTt3QkFDRixXQUFXLEVBQUU7NEJBQ1QsT0FBTyxFQUFFLE9BQU87NEJBQ2hCLG1CQUFtQixFQUFFLEVBQUU7eUJBQzFCO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLDZCQUE2QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxLQUFhLEVBQUUsYUFBcUIsRUFBRSxPQUFlLEVBQUUsUUFBZ0IsRUFBRSxZQUFvQixFQUFFLFVBQWtCO1FBQ3BJLE1BQU0sVUFBVSxHQUFHO1lBQ2Ysc0JBQXNCLEVBQUU7Z0JBQ3BCLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixrQkFBa0IsRUFBRTtvQkFDaEIsV0FBVyxFQUFFO3dCQUNULGFBQWEsRUFBRSxhQUFhO3dCQUM1QixVQUFVLEVBQUUsVUFBVTtxQkFDekI7aUJBQ0o7Z0JBQ0QsVUFBVSxFQUFFO29CQUNSLElBQUksRUFBRTt3QkFDRixXQUFXLEVBQUU7NEJBQ1QsT0FBTyxFQUFFLE9BQU87NEJBQ2hCLG1CQUFtQixFQUFFLEVBQUU7eUJBQzFCO3FCQUNKO2lCQUVKO2dCQUNELE1BQU0sRUFBRSxJQUFJO2dCQUNaLElBQUk7Z0JBQ0osbUVBQW1FO2dCQUNuRSxvQkFBb0I7Z0JBQ3BCLHFEQUFxRDtnQkFDckQsSUFBSTthQUNQO1NBQ0osQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBSUQsOEJBQThCLENBQUMsS0FBYSxFQUFFLFdBQW1CLEVBQUUsT0FBZSxFQUFFLFFBQWdCLEVBQUUsWUFBb0IsRUFBRSxVQUFrQjtRQUMxSSxNQUFNLFVBQVUsR0FBRztZQUNmLHNCQUFzQixFQUFFO2dCQUNwQixhQUFhLEVBQUUsS0FBSztnQkFDcEIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFlBQVksRUFBRSxZQUFZO2dCQUMxQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsa0JBQWtCLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxXQUFXO2lCQUMzQjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsSUFBSSxFQUFFO3dCQUNGLFdBQVcsRUFBRTs0QkFDVCxPQUFPLEVBQUUsT0FBTzs0QkFDaEIsbUJBQW1CLEVBQUUsRUFBRTt5QkFDMUI7cUJBQ0o7aUJBRUo7Z0JBQ0QsTUFBTSxFQUFFLElBQUk7Z0JBQ1osSUFBSTtnQkFDSixtRUFBbUU7Z0JBQ25FLG9CQUFvQjtnQkFDcEIscURBQXFEO2dCQUNyRCxJQUFJO2FBQ1A7U0FDSixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLFFBQVE7UUFDakQsTUFBTSxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLGFBQWEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDNUIsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDZCxhQUFhLEVBQUUsT0FBTztnQkFDdEIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFFBQVEsRUFBRSxRQUFRO2FBQ3JCLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxZQUFZLENBQUM7SUFDeEIsQ0FBQztJQUVELGdCQUFnQixDQUFDLEtBQWEsRUFBRSxhQUFxQyxFQUFFLE9BQWUsRUFBRSxRQUFnQixFQUFFLFlBQW9CLEVBQUUsVUFBa0IsRUFDOUksV0FBbUIsRUFBRSxjQUF1QixFQUFFLFFBQWdCLEVBQUUsbUJBQW1CLEVBQUUsTUFBTTtRQUMzRixNQUFNLFVBQVUsR0FBRztZQUNmLGlCQUFpQixFQUFFO2dCQUNmLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsa0JBQWtCLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUM7aUJBQzNFO2dCQUNELFVBQVUsRUFBRTtvQkFDUixJQUFJLEVBQUU7d0JBQ0YsV0FBVyxFQUFFOzRCQUNULE9BQU8sRUFBRSxPQUFPOzRCQUNoQixtQkFBbUIsRUFBRSxFQUFFO3lCQUMxQjtxQkFDSjtpQkFDSjtnQkFDRCxXQUFXLEVBQUUsV0FBVztnQkFDeEIsZUFBZSxFQUFFO29CQUNiLGNBQWMsRUFBRSxtQkFBbUI7aUJBQ3RDO2dCQUNELGNBQWMsRUFBRSxjQUFjO2dCQUM5QixjQUFjLEVBQUUsTUFBTTthQUN6QjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQ7Ozs7O1NBS0s7SUFFTCx3QkFBd0I7UUFDcEIsTUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRTtTQUNyRCxDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsMkJBQTJCLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3RGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2RyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDckgseUdBQXlHO29CQUN6RyxNQUFNLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2hHLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztvQkFDdEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsVUFBVSxFQUFFLHdCQUF3QjtRQUNyRCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksU0FBUyxDQUFDO1lBQ2QsSUFBSSx3QkFBd0IsRUFBRTtnQkFDMUIsU0FBUyxHQUFHO29CQUNSLHdCQUF3QixFQUFFO3dCQUN0QixFQUFFLEVBQUUsd0JBQXdCO3FCQUMvQjtvQkFDRCw0QkFBNEIsRUFBRSxVQUFVO2lCQUMzQyxDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsU0FBUyxHQUFHO29CQUNSLDRCQUE0QixFQUFFLFVBQVU7aUJBQzNDLENBQUM7YUFDTDtZQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2hMLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDOUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJCQUEyQixDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFNBQVM7UUFDN0QsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFNBQVMsR0FBRztnQkFDZCxNQUFNLEVBQUUsTUFBTTtnQkFDZCxVQUFVLEVBQUUsVUFBVTtnQkFDdEIsUUFBUSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxFQUFFO2dCQUNuRCxNQUFNLEVBQUUsTUFBTTtnQkFDZCxTQUFTLEVBQUUsU0FBUzthQUN2QixDQUFBO1lBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzdHLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO29CQUM3SixJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7b0JBQy9GLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxnQkFBZ0I7UUFDakMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFNBQVMsR0FBRztnQkFDZCxtQkFBbUIsRUFBRTtvQkFDakIsd0JBQXdCLEVBQUU7d0JBQ3RCLEVBQUUsRUFBRSxnQkFBZ0I7cUJBQ3ZCO2lCQUNKO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2hILElBQUksUUFBUSxFQUFFO29CQUNWLElBQUksQ0FBQyxjQUFjLENBQUMsMkJBQTJCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDbEUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztNQUVFO0lBQ0Ysd0JBQXdCLENBQUMsVUFBVztRQUNoQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLCtCQUErQixFQUFFLElBQUksQ0FBQywrQkFBK0IsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM5RyxDQUFDO0lBRUQ7O01BRUU7SUFDRixxQkFBcUIsQ0FBQyxVQUFXO1FBQzdCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3hHLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyxNQUFNO1FBQzVCLE1BQU0sVUFBVSxHQUFHO1lBQ2YsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsaUNBQWlDLEVBQUUsSUFBSSxDQUFDLGlDQUFpQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzFILENBQUM7SUFFRCw4QkFBOEIsQ0FBQyxNQUFNO1FBQ2pDLE1BQU0sVUFBVSxHQUFHO1lBQ2YsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUNBQXFDLEVBQUUsSUFBSSxDQUFDLHFDQUFxQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2xJLENBQUM7SUFFRCxjQUFjLENBQUMsTUFBTSxFQUFFLGFBQWEsRUFBRSxjQUFjO1FBQ2hELE1BQU0sVUFBVSxHQUFHO1lBQ2YsTUFBTSxFQUFFLE1BQU07WUFDZCxhQUFhLEVBQUU7Z0JBQ1gsdUJBQXVCO2dCQUN2Qix5QkFBeUIsRUFBRSxhQUFhO2FBQzNDO1lBQ0QsY0FBYyxFQUFFO2dCQUNaLDBCQUEwQjtnQkFDMUIseUJBQXlCLEVBQUUsY0FBYzthQUM1QztTQUNKLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELDBCQUEwQixDQUFDLHFCQUFxQjtRQUM1QyxNQUFNLFVBQVUsR0FBRztZQUNmLG9CQUFvQixFQUFFO2dCQUNsQix5QkFBeUIsRUFBRSxxQkFBcUI7YUFDbkQ7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxJQUFJLENBQUMsOEJBQThCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEgsQ0FBQztJQUVELFVBQVUsQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLGtCQUFrQjtRQUNoSCxNQUFNLFVBQVUsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9FLE1BQU0sYUFBYSxHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbkYsTUFBTSxVQUFVLEdBQUc7WUFDZixPQUFPLEVBQUUsT0FBTztZQUNoQixRQUFRLEVBQUUsUUFBUTtZQUNsQixVQUFVLEVBQUUsVUFBVTtZQUN0QixRQUFRLEVBQUUsUUFBUTtZQUNsQixvQkFBb0IsRUFBRSxhQUFhO1lBQ25DLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLGtCQUFrQixFQUFFLGtCQUFrQjtZQUN0QyxVQUFVLEVBQUUsQ0FBQyxVQUFVLElBQUksVUFBVSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVFLGFBQWEsRUFBRSxDQUFDLGFBQWEsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDeEYsSUFBSSxFQUFFO2dCQUNGLE9BQU8sRUFBRSxNQUFNO2FBQ2xCO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVELG1CQUFtQixDQUFDLE1BQU07UUFDdEIsTUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsTUFBTTtTQUNqQixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMsMEJBQTBCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDNUcsQ0FBQztJQUdELGFBQWE7SUFDYixvQkFBb0IsQ0FBQyxFQUFFO1FBQ25CLE1BQU0sVUFBVSxHQUFHO1lBQ2YsZUFBZSxFQUFFO2dCQUNiLG9CQUFvQixFQUFFO29CQUNsQixFQUFFLEVBQUUsRUFBRTtpQkFDVDthQUNKO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLCtCQUErQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBRXRILENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxRQUFnQjtRQUNqQyxNQUFNLFNBQVMsR0FBRztZQUNkLFFBQVEsRUFBRSxRQUFRO1NBRXJCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxTQUFTLENBQUM7aUJBQ3RGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkJBQTZCLENBQUMsSUFBSSxFQUFFLEdBQUc7UUFDbkMsTUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEVBQUU7WUFDakQsTUFBTSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDeEIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsUUFBUSxFQUFFLEdBQUc7YUFDaEI7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDdEcsQ0FBQztJQUVELGtCQUFrQixDQUFDLFVBQVU7UUFDekIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDakcsQ0FBQztJQUVELHFCQUFxQjtRQUNqQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3ZFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEYsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsRyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsb0JBQW9CLEVBQUU7d0JBQzNDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7d0JBQzdDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUFoN0I4QixjQUFjOzs7QUF2R2hDLFVBQVU7SUFIdEIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUNXLFVBQVUsQ0F1aEN0QjtTQXZoQ1ksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgR2xvYWJhbENvbmZpZyBhcyBjb25maWcgfSBmcm9tICcuLi9jb25maWcvY29uZmlnJztcclxuaW1wb3J0IHsgUGVyc29uIH0gZnJvbSAnLi4vb2JqZWN0cy9QZXJzb24nO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4vc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTdGF0dXMgfSBmcm9tICcuLi9vYmplY3RzL1N0YXR1cyc7XHJcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIFVTRVJfTlMgPSAnaHR0cDovL3NjaGVtYXMuY29yZHlzLmNvbS8xLjEvbGRhcCc7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9hcHAvYnBtLzEuMCc7XHJcbiAgICBwdWJsaWMgUEVSU09OX05TID0gJ2h0dHA6Ly9zY2hlbWFzL01QTUN1c3RvbWl6ZWRBcHBsaWNhdGlvblBhY2thZ2VzL1BlcnNvbi9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBNUE1fQVBQX1JPTEVTX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Pcmdhbml6YXRpb25Nb2RlbC9NUE1fQVBQX1JvbGVzL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIE1FRElBX01BTkFHRVJfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9NZWRpYV9NYW5hZ2VyX0NvbmZpZy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBXU0FQUF9DT1JFX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcbiAgICBwdWJsaWMgR0VUX1BFUlNPTl9CWV9VU0VSX0lEX05TID0gJ2h0dHA6Ly9zY2hlbWFzL01QTUN1c3RvbWl6ZWRBcHBsaWNhdGlvblBhY2thZ2VzL1BlcnNvbi9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBHRVRfUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSURfTlMgPSAnaHR0cDovL3NjaGVtYXMvTVBNQ3VzdG9taXplZEFwcGxpY2F0aW9uUGFja2FnZXMvUGVyc29uL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIFRFQU1TX0JQTV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vdGVhbXMvYnBtLzEuMCc7XHJcbiAgICBwdWJsaWMgUkVWSUVXX0JQTV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vcmV2aWV3L2JwbS8xLjAnO1xyXG4gICAgcHVibGljIEFQUF9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0FwcF9Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgUFJPSkVDVF9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1Byb2plY3RfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEFTU0VUX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQXNzZXRfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEJSQU5EX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQnJhbmRfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIENPTU1FTlRfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9Db21tZW50c19Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgQlVMS19DUl9BUFBST1ZBTF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vcmV2aWV3L2JwbS8xLjAnO1xyXG4gICAgcHVibGljIFZJRVdfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9WaWV3X0NvbmZpZy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBURUFNX1JPTEVfTUFQUElOR19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1RlYW1fUm9sZV9NYXBwaW5nL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9BTExfVEVBTVNfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1RlYW1zL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9BTExfUEVSU09OU19OUyA9ICdodHRwOi8vc2NoZW1hcy9NUE1DdXN0b21pemVkQXBwbGljYXRpb25QYWNrYWdlcy9QZXJzb24vb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgREVMSVZFUkFCTEVfUkVWSUVXX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9EZWxpdmVyYWJsZVJldmlldy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBUQVNLX1JFVklFV19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRGVsaXZlcmFibGVSZXZpZXcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgTVBNX0VWRU5UU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0V2ZW50cy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBNUE1fV09SS0ZMT1dfQUNUSU9OU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1dvcmtmbG93X0FjdGlvbnMvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgR0VUX0VNQUlMX0VWRU5UU19GT1JfTUFOQUdFUl9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9FbWFpbF9FdmVudF9UZW1wbGF0ZS9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBHRVRfRU1BSUxfRVZFTlRTX0ZPUl9VU0VSX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0VtYWlsX0V2ZW50X1RlbXBsYXRlL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9NQUlMX1BSRUZFUkVOQ0VfQllfVVNFUl9JRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fRW1haWxfUHJlZmVyZW5jZS9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBHRVRfVVNFUl9QUkVGRVJFTkNFX01BSUxfVEVNUExBVEVTX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcbiAgICBwdWJsaWMgTVBNX1dPUktGTE9XX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgcHVibGljIERFTEVURV9NUE1fRU1BSUxfUFJFRkVSRU5DRV9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fRW1haWxfUHJlZmVyZW5jZS9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBNUE1fVVNFUl9QUkVGRVJFTkNFX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9Vc2VyX1ByZWZlcmVuY2Uvb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgcHVibGljIENBTVBBSUdOX0RFVEFJTFNfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0NhbXBhaWduL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIFNUQVRVU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1N0YXR1cy9vcGVyYXRpb25zJztcclxuXHJcbiAgICBwdWJsaWMgR0VUX0FMTF9VU0VSU19OUyA9ICdodHRwOi8vc2NoZW1hcy9PcGVuVGV4dEVudGl0eUlkZW50aXR5Q29tcG9uZW50cy9Vc2VyL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9FWFBPUlRfREFUQV9CWV9VU0VSX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9FeHBvcnRfRGF0YS9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBET1dOTE9BRF9GSUxFX0JZX1BBVEhfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2V4Y2VsLzEuMCc7XHJcblxyXG4gICAgcHVibGljIE1QTV9OT1RJRklDQVRJT05fTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTm90aWZpY2F0aW9uL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEVNQUlMX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9HZW5lcmFsX0VtYWlsX0NvbmZpZy9vcGVyYXRpb25zJztcclxuXHJcbiAgICBwdWJsaWMgQ1VSU09SX05TID0gJ2h0dHA6Ly9zY2hlbWFzLm9wZW50ZXh0LmNvbS9icHMvZW50aXR5L2NvcmUnO1xyXG5cclxuICAgIHB1YmxpYyBHRVRfUEVSU09OX0RFVEFJTFNfQllfVVNFUl9JRF9XUyA9ICdHZXRQZXJzb25CeVVzZXJJZCc7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTX1dTID0gJ0dldEFwcGxpY2F0aW9uRm9yVXNlcic7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9ST0xFU19XUyA9ICdHZXRBbGxSb2xlcyc7XHJcbiAgICBwdWJsaWMgR0VUX01FRElBX01BTkFHRVJfQ09ORklHX1dTID0gJ0dldE1lZGlhTWFuYWdlckNvbmZpZyc7XHJcbiAgICBwdWJsaWMgR0VUX1BFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEX1dTID0gJ0dldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQnO1xyXG4gICAgcHVibGljIEdFVF9URUFNX1JPTEVTX1dTID0gJ0dldFRlYW1Sb2xlcyc7XHJcbiAgICBwdWJsaWMgR0VUX1VTRVNfRk9SX1RFQU1fV1MgPSAnR2V0VXNlcnNGb3JUZWFtJztcclxuICAgIHB1YmxpYyBHRVRfUE9TU0lCTEVfQ1JfQUNUSU9OU19GT1JfVVNFUl9XUyA9ICdHZXRQb3NzaWJpbGVDUkFjdGlvbnNGb3JVc2VyJztcclxuICAgIHB1YmxpYyBPUEVOX0ZJTEVfUFJPT0ZJTkdfU0VTU0lPTl9XUyA9ICdPcGVuRmlsZVByb29mU2Vzc2lvbic7XHJcbiAgICBwdWJsaWMgR0VUX0FQUF9DT05GSUdfQllfSURfV1MgPSAnR2V0QXBwQ29uZmlnQnlJZCc7XHJcbiAgICBwdWJsaWMgR0VUX1BST0pFQ1RfQ09ORklHX0JZX0lEX1dTID0gJ0dldFByb2plY3RDb25maWdCeUlkJztcclxuICAgIHB1YmxpYyBHRVRfQVNTRVRfQ09ORklHX0JZX0lEX1dTID0gJ0dldEFzc2V0Q29uZmlnQnlJZCc7XHJcbiAgICBwdWJsaWMgR0VUX0JSQU5EX0NPTkZJR19CWV9JRF9XUyA9ICdHZXRCcmFuZENvbmZpZ0J5SWQnO1xyXG4gICAgcHVibGljIEdFVF9DT01NRU5UX0NPTkZJR19CWV9JRF9XUyA9ICdHZXRDb21tZW50c0NvbmZpZ0J5SWQnO1xyXG4gICAgcHVibGljIEhBTkRFTF9DUl9BUFBST1ZBTF9XUyA9ICdIYW5kbGVDUkFwcHJvdmFsJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX01QTV9WSUVXX0NPTkZJR19XUyA9ICdHZXRBbGxNUE1WaWV3Q29uZmlnJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX1RFQU1fUk9MRVNfV1MgPSAnR2V0QWxsVGVhbVJvbGVzJztcclxuICAgIHB1YmxpYyBHRVRfRklMRV9QUk9PRklOR19TRVNTSU9OX1dTID0gJ0dldEZpbGVQcm9vZmluZ1Nlc3Npb24nO1xyXG4gICAgcHVibGljIEdFVF9BTExfVEVBTVNfV1MgPSAnR2V0QWxsVGVhbXMgJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX1BFUlNPTlNfV1MgPSAnR2V0QWxsUGVyc29ucyc7XHJcbiAgICBwdWJsaWMgR0VUX1BFUlNPTlNfQllfS0VZV09SRF9XUyA9ICdHZXRQZXJzb25zQnlLZXl3b3JkJztcclxuICAgIHB1YmxpYyBHRVRfREVMSVZFUkFCTEVfUkVWSUVXX0JZX0RFTElWRVJBQkxFX0lEX1dTID0gJ0dldERlbGl2ZXJhYmxlUmV2aWV3QnlEZWxpdmVyYWJsZUlEJztcclxuICAgIHB1YmxpYyBHRVRfREVMSVZFUkFCTEVfUkVWSUVXX0JZX1RBU0tfSURfV1MgPSAnR2V0RGVsaXZlcmFibGVSZXZpZXdCeVRhc2tJRCc7XHJcbiAgICBwdWJsaWMgR0VUX1VTRVJTX0ZPUl9URUFNX1JPTEVfV1MgPSAnR2V0VXNlcnNGb3JUZWFtUm9sZSc7XHJcbiAgICBwdWJsaWMgR0VUX01QTV9FVkVOVFNfQllfQ0FURUdPUllfTEVWRUxfV1MgPSAnR2V0TVBNRXZlbnRzQnlDYXRlZ29yeUxldmVsJztcclxuICAgIHB1YmxpYyBHRVRfV09SS0ZMT1dfQUNUSU9OU19CWV9DQVRFR09SWV9XUyA9ICdHZXRXb3JrZmxvd0FjdGlvbnNCeUNhdGVnb3J5JztcclxuXHJcbiAgICBwdWJsaWMgR0VUX0VNQUlMX0VWRU5UU19GT1JfTUFOQUdFUl9XUyA9ICdHZXRFbWFpbEV2ZW50c0Zvck1hbmFnZXInO1xyXG4gICAgcHVibGljIEdFVF9FTUFJTF9FVkVOVFNfRk9SX1VTRVJfV1MgPSAnR2V0RW1haWxFdmVudHNGb3JVc2VyJztcclxuICAgIHB1YmxpYyBHRVRfTUFJTF9QUkVGRVJFTkNFX0JZX1VTRVJfSURfV1MgPSAnR2V0TWFpbFByZWZlcmVuY2VCeVVzZXJJZCc7XHJcbiAgICBwdWJsaWMgR0VUX1VTRVJfUFJFRkVSRU5DRV9NQUlMX1RFTVBMQVRFU19XUyA9ICdHZXRVc2VyUHJlZmVyZW5jZU1haWxUZW1wbGF0ZXMnO1xyXG4gICAgcHVibGljIE1BSUxfUFJFRkVSRU5DRV9XUyA9ICdNYWlsUHJlZmVyZW5jZSc7XHJcbiAgICBwdWJsaWMgREVMRVRFX01QTV9FTUFJTF9QUkVGRVJFTkNFX1dTID0gJ0RlbGV0ZU1QTV9FbWFpbF9QcmVmZXJlbmNlICc7XHJcbiAgICBwdWJsaWMgRVhQT1JUX0RBVEFfV1MgPSAnRXhwb3J0RGF0YSc7XHJcbiAgICBwdWJsaWMgR0VUX0VYUE9SVF9EQVRBX0JZX1VTRVJfV1MgPSAnR2V0RXhwb3J0RGF0YUJ5VXNlcic7XHJcbiAgICBwdWJsaWMgRE9XTkxPQURfRklMRV9CWV9QQVRIX1dTID0gJ0Rvd25sb2FkRmlsZUJ5UGF0aCc7XHJcblxyXG4gICAgcHVibGljIEdFVF9NUE1fVVNFUl9QUkVGRVJFTkNFX0JZX1VTRVJfV1MgPSAnR2V0TVBNVXNlclByZWZlcmVuY2VCeVVzZXInO1xyXG4gICAgcHVibGljIENSRUFURV9NUE1fVVNFUl9QUkVGRVJFTkNFX1dTID0gJ0NyZWF0ZU1QTV9Vc2VyX1ByZWZlcmVuY2UnO1xyXG4gICAgcHVibGljIFVQREFURV9NUE1fVVNFUl9QUkVGRVJFTkNFX1dTID0gJ1VwZGF0ZU1QTV9Vc2VyX1ByZWZlcmVuY2UnO1xyXG4gICAgcHVibGljIERFTEVURV9NUE1fVVNFUl9QUkVGRVJFTkNFX1dTID0gJ0RlbGV0ZU1QTV9Vc2VyX1ByZWZlcmVuY2UnO1xyXG5cclxuICAgIHB1YmxpYyBHRVRfQUxMX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbENhbXBhaWduJztcclxuICAgIHB1YmxpYyBHRVRfU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUVfV1MgPSAnR2V0QWxsU3RhdHVzRm9yQ2F0ZWdvcnlMZXZlbCc7XHJcblxyXG4gICAgcHVibGljIEdFVF9BTExfVVNFUlMgPSAnR2V0QWxsVXNlcnMnO1xyXG4gICAgcHVibGljIEdFVF9BTExfU1RBVFVTID0gJ0dldEFsbFN0YXR1cyc7XHJcbiAgICBwdWJsaWMgVVBEQVRFX0RFRkFVTFRfVVNFUl9QUkVGRVJFTkNFX1dTID0gJ1VwZGF0ZURlZmF1bHRVc2VyUHJlZmVyZW5jZSc7XHJcbiAgICBwdWJsaWMgR0VUX05PVElGSUNBVElPTl9CWV9VU0VSX1dTID0gJ0dldE5vdGlmaWNhdGlvbkJ5VXNlcic7XHJcbiAgICBwdWJsaWMgVVBEQVRFX05PVElGSUNBVElPTl9XUyA9ICdVcGRhdGVOb3RpZmljYXRpb24nO1xyXG4gICAgcHVibGljIEdFVF9ERUZBVUxUX0VNQUlMX0NPTkZJR19XUyA9ICdHZXREZWZhdWx0RW1haWxDb25maWcnO1xyXG5cclxuICAgIHB1YmxpYyBERUxFVEVfTVBNX0VYUE9SVF9EQVRBX0JZX0lEX1dTID0gJ0RlbGV0ZU1QTV9FeHBvcnRfRGF0YSc7XHJcbiAgICBwdWJsaWMgREVMRVRFX01QTV9FWFBPUlRfREFUQV9CWV9JRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fRXhwb3J0X0RhdGEvb3BlcmF0aW9ucyc7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGludm9rZVJlcXVlc3QobmFtZXNwYWNlLCBtZXRob2QsIHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGlmIChuYW1lc3BhY2UgPT0gbnVsbCB8fCBuYW1lc3BhY2UgPT09ICcnIHx8IHR5cGVvZiAobmFtZXNwYWNlKSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdQbGVhc2UgcHJvdmlkZSB2YWxpZCBuYW1lc3BhY2UuJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChtZXRob2QgPT0gbnVsbCB8fCBtZXRob2QgPT09ICcnIHx8IHR5cGVvZiAobWV0aG9kKSA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdQbGVhc2UgcHJvdmlkZSB2YWxpZCBtZXRob2QgbmFtZS4nKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHBhcmFtZXRlcnMgPT0gbnVsbCB8fCBwYXJhbWV0ZXJzID09PSAnJyB8fCB0eXBlb2YgKHBhcmFtZXRlcnMpID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICBwYXJhbWV0ZXJzID0gJyc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoYXBwU2VydmljZU9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgJC5zb2FwKHtcclxuICAgICAgICAgICAgICAgIG5hbWVzcGFjZSxcclxuICAgICAgICAgICAgICAgIG1ldGhvZCxcclxuICAgICAgICAgICAgICAgIHBhcmFtZXRlcnNcclxuICAgICAgICAgICAgfSkudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgYXBwU2VydmljZU9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBhcHBTZXJ2aWNlT2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBhcHBTZXJ2aWNlT2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE1QTVYzLTIwMDhcclxuICAgIGdldEFsbFVzZXJzKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVVNFUlMpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVVNFUlMpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9VU0VSU19OUywgdGhpcy5HRVRfQUxMX1VTRVJTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1VTRVJTLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgLy8gYWRkZWQgZm9yIGNvZGUgaGFyZGVuaWluZyAta2VlcnRoYW5hXHJcbiAgICBnZXRTdGF0dXNCeUNhdGVnb3J5TGV2ZWxOYW1lKGNhdGVnb3J5TGV2ZWxOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPEFycmF5PFN0YXR1cz4+IHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICBjYXRlZ29yeUxldmVsTmFtZTogY2F0ZWdvcnlMZXZlbE5hbWUsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxJRDogJydcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlNUQVRVU19CWV9DQVRFR09SWV9MRVZFTF9OQU1FKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUUpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuU1RBVFVTX01FVEhPRF9OUywgdGhpcy5HRVRfU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUVfV1MsXHJcbiAgICAgICAgICAgICAgICAgICAgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzZXM6IEFycmF5PFN0YXR1cz4gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1N0YXR1cycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlNUQVRVU19CWV9DQVRFR09SWV9MRVZFTF9OQU1FLCBKU09OLnN0cmluZ2lmeShzdGF0dXNlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c2VzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGFkZGVkIGZvciBjb2RlIGhhcmRlbmlpbmcgLWtlZXJ0aGFuYVxyXG4gICAgLyogZ2V0QWxsQ2FtcGFpZ24ocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQU1QQUlHTikgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQU1QQUlHTikpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5DQU1QQUlHTl9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FLCBudWxsKS5zdWJzY3JpYmUoY2FtcGFpZ25EZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQU1QQUlHTiwgSlNPTi5zdHJpbmdpZnkoY2FtcGFpZ25EZXRhaWxzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChjYW1wYWlnbkRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIC8vIGFkZGVkIGZvciBjb2RlIGhhcmRlbmlpbmcgXHJcbiAgICBnZXRVc2VyRGV0YWlsc0xEQVAocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19MREFQKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX0xEQVApKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVVNFUl9OUywgJ0dldFVzZXJEZXRhaWxzJywgJycpLnN1YnNjcmliZSh1c2VyRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5VU0VSX0RFVEFJTFNfTERBUCwgSlNPTi5zdHJpbmdpZnkodXNlckRldGFpbHMpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVzZXJEZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dJbihjYWxsYmFjaykge1xyXG4gICAgICAgIC8qICBjb25zdCBnZXRVc2VyOiBPYnNlcnZhYmxlPGFueT4gPSB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5VU0VSX05TLCAnR2V0VXNlckRldGFpbHMnLCAnJyk7XHJcbiAgICAgICAgIGdldFVzZXIuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHsgKi9cclxuICAgICAgICB0aGlzLmdldFVzZXJEZXRhaWxzTERBUCgpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJPYmplY3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICBjb25zdCB1c2VyTmFtZSA9IHVzZXJPYmplY3RbMF0uYXV0aHVzZXJkbi5zcGxpdCgnLCcpWzBdLnNwbGl0KCc9JylbMV07XHJcbiAgICAgICAgICAgIGlmICh1c2VyTmFtZSA9PT0gJ2Fub255bW91cycpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY29uZmlnLmNvbmZpZy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIGVyckRhdGEgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdFcnJvciB3aGlsZSBmaXJpbmcgR2V0VXNlckRldGFpbHMgOiAnICsgZXJyRGF0YS5zdGF0dXMpO1xyXG4gICAgICAgICAgICBpZiAoY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrUFNTZXNzaW9uKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgdGhpcy5sb2dJbihjYWxsYmFjayk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UGVyc29uQ2FzdGVkT2JqKGRhdGEpOiBQZXJzb24ge1xyXG4gICAgICAgIGNvbnN0IHVzZXJEZXRhaWxzOiBQZXJzb24gPSB7XHJcbiAgICAgICAgICAgIElkOiAnJyxcclxuICAgICAgICAgICAgSXRlbUlkOiAnJyxcclxuICAgICAgICAgICAgdXNlckNOOiAnJyxcclxuICAgICAgICAgICAgZnVsbE5hbWU6ICcnLFxyXG4gICAgICAgICAgICB1c2VySWQ6ICcnXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5QZXJzb24gJiYgZGF0YS5QZXJzb24uVXNlcl9JRCAmJiBkYXRhLlBlcnNvbi5Vc2VyX0lELl9fdGV4dCkge1xyXG4gICAgICAgICAgICAgICAgdXNlckRldGFpbHMudXNlckNOID0gZGF0YS5QZXJzb24uVXNlcl9JRC5fX3RleHQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGRhdGEuUGVyc29uICYmIGRhdGEuUGVyc29uWydQZXJzb24taWQnXSAmJiBkYXRhLlBlcnNvblsnUGVyc29uLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgIHVzZXJEZXRhaWxzLklkID0gZGF0YS5QZXJzb25bJ1BlcnNvbi1pZCddLklkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkYXRhLlBlcnNvbiAmJiBkYXRhLlBlcnNvblsnUGVyc29uLWlkJ10gJiYgZGF0YS5QZXJzb25bJ1BlcnNvbi1pZCddLkl0ZW1JZCkge1xyXG4gICAgICAgICAgICAgICAgdXNlckRldGFpbHMuSXRlbUlkID0gZGF0YS5QZXJzb25bJ1BlcnNvbi1pZCddLkl0ZW1JZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZGF0YS5QZXJzb24gJiYgZGF0YS5QZXJzb24uRGlzcGxheU5hbWUgJiYgZGF0YS5QZXJzb24uRGlzcGxheU5hbWUuX190ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICB1c2VyRGV0YWlscy5mdWxsTmFtZSA9IGRhdGEuUGVyc29uLkRpc3BsYXlOYW1lLl9fdGV4dDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZGF0YS5QZXJzb24gJiYgZGF0YS5QZXJzb24uUGVyc29uVG9Vc2VyICYmIGRhdGEuUGVyc29uLlBlcnNvblRvVXNlclsnSWRlbnRpdHktaWQnXSAmJiBkYXRhLlBlcnNvbi5QZXJzb25Ub1VzZXJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgIHVzZXJEZXRhaWxzLnVzZXJJZCA9IGRhdGEuUGVyc29uLlBlcnNvblRvVXNlclsnSWRlbnRpdHktaWQnXS5JZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdXNlckRldGFpbHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8UGVyc29uPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKFxyXG4gICAgICAgICAgICBvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSURfTlMsIHRoaXMuR0VUX1BFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEX1dTLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJEZXRhaWxzOiBQZXJzb24gPSB0aGlzLmdldFBlcnNvbkNhc3RlZE9iaihkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPFBlcnNvbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlBFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSUQpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX1BFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEX05TLCB0aGlzLkdFVF9QRVJTT05fQllfSURFTlRJVFlfVVNFUl9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJEZXRhaWxzOiBQZXJzb24gPSB0aGlzLmdldFBlcnNvbkNhc3RlZE9iaihkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlBFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lELCBKU09OLnN0cmluZ2lmeSh1c2VyRGV0YWlscykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gKi9cclxuICAgIGdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxQZXJzb24+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoXHJcbiAgICAgICAgICAgIG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9QRVJTT05fQllfVVNFUl9JRF9OUywgdGhpcy5HRVRfUEVSU09OX0RFVEFJTFNfQllfVVNFUl9JRF9XUywgcGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VyRGV0YWlsczogUGVyc29uID0gdGhpcy5nZXRQZXJzb25DYXN0ZWRPYmooZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPFBlcnNvbj4ge1xyXG4gICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUEVSU09OX0JZX1VTRVJfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUEVSU09OX0JZX1VTRVJfSUQpKSk7XHJcbiAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX1BFUlNPTl9CWV9VU0VSX0lEX05TLCB0aGlzLkdFVF9QRVJTT05fREVUQUlMU19CWV9VU0VSX0lEX1dTLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckRldGFpbHM6IFBlcnNvbiA9IHRoaXMuZ2V0UGVyc29uQ2FzdGVkT2JqKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QRVJTT05fQllfVVNFUl9JRCwgSlNPTi5zdHJpbmdpZnkodXNlckRldGFpbHMpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgIH0gKi9cclxuXHJcbiAgICAvKiBnZXRBbGxBcHBsaWNhdGlvbkRldGFpbHMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFNfTlMsIHRoaXMuR0VUX0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxBcHBsaWNhdGlvbkRldGFpbHMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9BUFBMSUNBVElPTl9ERVRBSUxTKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFMpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTX05TLCB0aGlzLkdFVF9BTExfQVBQTElDQVRJT05fREVUQUlMU19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9BUFBMSUNBVElPTl9ERVRBSUxTLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBwcm9qZWN0Q29uZmlnSWQ6IGlkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9DT05GSUdfTlMsIHRoaXMuR0VUX1BST0pFQ1RfQ09ORklHX0JZX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBnZXRQcm9qZWN0Q29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgcHJvamVjdENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUFJPSkVDVF9DT05GSUdfQllfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QUk9KRUNUX0NPTkZJR19CWV9JRCkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0NPTkZJR19OUywgdGhpcy5HRVRfUFJPSkVDVF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QUk9KRUNUX0NPTkZJR19CWV9JRCwgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBcHBDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBhcHBDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5BUFBfQ09ORklHX05TLCB0aGlzLkdFVF9BUFBfQ09ORklHX0JZX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBnZXRBcHBDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBhcHBDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFQUF9DT05GSUdfQllfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BUFBfQ09ORklHX0JZX0lEKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkFQUF9DT05GSUdfTlMsIHRoaXMuR0VUX0FQUF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BUFBfQ09ORklHX0JZX0lELCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFzc2V0Q29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgYXNzZXRDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5BU1NFVF9DT05GSUdfTlMsIHRoaXMuR0VUX0FTU0VUX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogIGdldEFzc2V0Q29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICBhc3NldENvbmZpZ0lkOiBpZFxyXG4gICAgICAgICB9O1xyXG4gICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQVNTRVRfQ09ORklHX0JZX0lEKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFTU0VUX0NPTkZJR19CWV9JRCkpKTtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5BU1NFVF9DT05GSUdfTlMsIHRoaXMuR0VUX0FTU0VUX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BU1NFVF9DT05GSUdfQllfSUQsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgICB9KTtcclxuICAgICB9ICovXHJcblxyXG4gICAgZ2V0Q29tbWVudENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGNvbW1lbnRDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5DT01NRU5UX0NPTkZJR19OUywgdGhpcy5HRVRfQ09NTUVOVF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qICBnZXRDb21tZW50Q29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICBjb21tZW50Q29uZmlnSWQ6IGlkXHJcbiAgICAgICAgIH07XHJcbiAgICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DT01NRU5UX0NPTkZJR19CWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DT01NRU5UX0NPTkZJR19CWV9JRCkpKTtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5DT01NRU5UX0NPTkZJR19OUywgdGhpcy5HRVRfQ09NTUVOVF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ09NTUVOVF9DT05GSUdfQllfSUQsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgICB9KTtcclxuICAgICB9ICovXHJcblxyXG4gICAgZ2V0QnJhbmRDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBicmFuZENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkJSQU5EX0NPTkZJR19OUywgdGhpcy5HRVRfQlJBTkRfQ09ORklHX0JZX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBnZXRCcmFuZENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGJyYW5kQ29uZmlnSWQ6IGlkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5CUkFORF9DT05GSUdfQllfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5CUkFORF9DT05GSUdfQllfSUQpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuQlJBTkRfQ09ORklHX05TLCB0aGlzLkdFVF9CUkFORF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5CUkFORF9DT05GSUdfQllfSUQsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgLyogZ2V0QWxsVmlld0NvbmZpZyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlZJRVdfQ09ORklHX05TLCB0aGlzLkdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX1dTLCAnJyk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgZ2V0QWxsVmlld0NvbmZpZyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1ZJRVdfQ09ORklHKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1ZJRVdfQ09ORklHKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlZJRVdfQ09ORklHX05TLCB0aGlzLkdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX1dTLCAnJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9WSUVXX0NPTkZJRywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2Vyc0ZvclJvbGUocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlVTRVJfTlMsICdHZXRVc2Vyc0ZvclJvbGUnLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRPVERTVGlja2V0Rm9yVXNlcihwYXJhbWV0ZXJzKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLldTQVBQX0NPUkVfTlMsICdHZXRPVERTVGlja2V0Rm9yVXNlcicsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFBlcnNvbkRldGFpbHNCeVVzZXJDTihwYXJhbWV0ZXJzKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlBFUlNPTl9OUywgdGhpcy5HRVRfUEVSU09OX0RFVEFJTFNfQllfVVNFUl9JRF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogZ2V0QWxsUm9sZXMocGFyYW1ldGVycz8pIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0FQUF9ST0xFU19OUywgdGhpcy5HRVRfQUxMX1JPTEVTX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxSb2xlcyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1JPTEVTKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1JPTEVTKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9BUFBfUk9MRVNfTlMsIHRoaXMuR0VUX0FMTF9ST0xFU19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9ST0xFUywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrRXZlbnRzKCkge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIENhdGVnb3J5TGV2ZWw6ICdUQVNLJ1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9FVkVOVFNfTUVUSE9EX05TLCB0aGlzLkdFVF9NUE1fRVZFTlRTX0JZX0NBVEVHT1JZX0xFVkVMX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrV29ya2Zsb3dBY3Rpb25zKCkge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWw6ICdUQVNLJ1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9XT1JLRkxPV19BQ1RJT05TX01FVEhPRF9OUywgdGhpcy5HRVRfV09SS0ZMT1dfQUNUSU9OU19CWV9DQVRFR09SWV9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogIGdldE1lZGlhTWFuYWdlckNvbmZpZyhwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1FRElBX01BTkFHRVJfQ09ORklHX05TLCB0aGlzLkdFVF9NRURJQV9NQU5BR0VSX0NPTkZJR19XUywgcGFyYW1ldGVycyk7XHJcbiAgICAgfSAqL1xyXG5cclxuICAgIGdldE1lZGlhTWFuYWdlckNvbmZpZyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuTUVESUFfTUFOQUdFUl9DT05GSUcpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5NRURJQV9NQU5BR0VSX0NPTkZJRykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NRURJQV9NQU5BR0VSX0NPTkZJR19OUywgdGhpcy5HRVRfTUVESUFfTUFOQUdFUl9DT05GSUdfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5NRURJQV9NQU5BR0VSX0NPTkZJRywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUZWFtUm9sZXMocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1TX0JQTV9OUywgdGhpcy5HRVRfVEVBTV9ST0xFU19XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogICAgIGdldEFsbFRlYW1Sb2xlcyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5URUFNX1JPTEVfTUFQUElOR19NRVRIT0RfTlMsIHRoaXMuR0VUX0FMTF9URUFNX1JPTEVTX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgICAgICB9ICovXHJcblxyXG4gICAgZ2V0QWxsVGVhbVJvbGVzKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVEVBTV9ST0xFUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9URUFNX1JPTEVTKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1fUk9MRV9NQVBQSU5HX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX1RFQU1fUk9MRVNfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVEVBTV9ST0xFUywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBnZXRBbGxUZWFtcyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfVEVBTVNfTlMsIHRoaXMuR0VUX0FMTF9URUFNU19XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgZ2V0QWxsVGVhbXMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9URUFNUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9URUFNUykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX1RFQU1TX05TLCB0aGlzLkdFVF9BTExfVEVBTVNfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1RFQU1TLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEFsbFBlcnNvbnMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX1BFUlNPTlNfTlMsIHRoaXMuR0VUX0FMTF9QRVJTT05TX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxQZXJzb25zKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUEVSU09OUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9QRVJTT05TKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfUEVSU09OU19OUywgdGhpcy5HRVRfQUxMX1BFUlNPTlNfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUEVSU09OUywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQZXJzb25zQnlLZXl3b3JkKHVzZXJLZXl3b3JkPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgdXNlcktleXdvcmQ6IHVzZXJLZXl3b3JkLFxyXG4gICAgICAgICAgICBDdXJzb3I6IHtcclxuICAgICAgICAgICAgICAgICdAeG1sbnMnOiB0aGlzLkNVUlNPUl9OUyxcclxuICAgICAgICAgICAgICAgICdAb2Zmc2V0JzogMCxcclxuICAgICAgICAgICAgICAgICdAbGltaXQnOiAzMFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfUEVSU09OU19OUywgdGhpcy5HRVRfUEVSU09OU19CWV9LRVlXT1JEX1dTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2Vyc0ZvclRlYW0ocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1TX0JQTV9OUywgdGhpcy5HRVRfVVNFU19GT1JfVEVBTV9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlcnNGb3JUZWFtUm9sZShwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTVNfQlBNX05TLCB0aGlzLkdFVF9VU0VSU19GT1JfVEVBTV9ST0xFX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWxpdmVyYWJsZVJldmlld0J5RGVsaXZlcmFibGVJRChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgRGVsaXZlcmFibGVJRDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5ERUxJVkVSQUJMRV9SRVZJRVdfTUVUSE9EX05TLCB0aGlzLkdFVF9ERUxJVkVSQUJMRV9SRVZJRVdfQllfREVMSVZFUkFCTEVfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlbGl2ZXJhYmxlUmV2aWV3QnlUYXNrSUQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIFRhc2tJRDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX1JFVklFV19NRVRIT0RfTlMsIHRoaXMuR0VUX0RFTElWRVJBQkxFX1JFVklFV19CWV9UQVNLX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQb3NzaWJsZUNSQWN0aW9uc0ZvclVzZXIoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUkVWSUVXX0JQTV9OUywgdGhpcy5HRVRfUE9TU0lCTEVfQ1JfQUNUSU9OU19GT1JfVVNFUl9XUywgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkZpbGVQcm9vZmluZ1Nlc3Npb24oYXBwSWQ6IHN0cmluZywgZGVsaXZlcmFibGVJZDogc3RyaW5nLCBhc3NldElkOiBzdHJpbmcsIHRhc2tOYW1lOiBzdHJpbmcsIHJldmlld1JvbGVJZDogc3RyaW5nLCB2ZXJzaW9uaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIG9wZW5GaWxlUHJvb2ZpbmdTZXNzaW9uOiB7XHJcbiAgICAgICAgICAgICAgICBhcHBsaWNhdGlvbklkOiBhcHBJZCxcclxuICAgICAgICAgICAgICAgIHZlcnNpb25pbmc6IHZlcnNpb25pbmcsXHJcbiAgICAgICAgICAgICAgICByZXZpZXdSb2xlSWQ6IHJldmlld1JvbGVJZCxcclxuICAgICAgICAgICAgICAgIHRhc2tOYW1lOiB0YXNrTmFtZSxcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlRGV0YWlsczoge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IGRlbGl2ZXJhYmxlSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZlcnNpb25pbmc6IHZlcnNpb25pbmdcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY3VzdG9tRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0SWQ6IGFzc2V0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXZpZXdEZWxpdmVyYWJsZUlkOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlJFVklFV19CUE1fTlMsIHRoaXMuT1BFTl9GSUxFX1BST09GSU5HX1NFU1NJT05fV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZpbGVQcm9vZmluZ1Nlc3Npb24oYXBwSWQ6IHN0cmluZywgZGVsaXZlcmFibGVJZDogc3RyaW5nLCBhc3NldElkOiBzdHJpbmcsIHRhc2tOYW1lOiBzdHJpbmcsIHJldmlld1JvbGVJZDogc3RyaW5nLCB2ZXJzaW9uaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGdldEZpbGVQcm9vZmluZ1Nlc3Npb246IHtcclxuICAgICAgICAgICAgICAgIGFwcGxpY2F0aW9uSWQ6IGFwcElkLFxyXG4gICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZyxcclxuICAgICAgICAgICAgICAgIHJldmlld1JvbGVJZDogcmV2aWV3Um9sZUlkLFxyXG4gICAgICAgICAgICAgICAgdGFza05hbWU6IHRhc2tOYW1lLFxyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVJZDogZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBjdXN0b21EYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZDogYXNzZXRJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmlld0RlbGl2ZXJhYmxlSWQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlzUm9vdERlbGl2ZXJhYmxlU2Vzc2lvbjogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHBlcnNvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgLy8ge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIGZpcnN0TmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckRpc3BsYXlOYW1lKCkgLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIGxhc3ROYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIC8vICAgICBpZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpXHJcbiAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUkVWSUVXX0JQTV9OUywgdGhpcy5HRVRfRklMRV9QUk9PRklOR19TRVNTSU9OX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcblxyXG5cclxuICAgIGdldE11bHRpcGxlRmlsZVByb29maW5nU2Vzc2lvbihhcHBJZDogc3RyaW5nLCBkZWxpdmVyYWJsZTogc3RyaW5nLCBhc3NldElkOiBzdHJpbmcsIHRhc2tOYW1lOiBzdHJpbmcsIHJldmlld1JvbGVJZDogc3RyaW5nLCB2ZXJzaW9uaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGdldEZpbGVQcm9vZmluZ1Nlc3Npb246IHtcclxuICAgICAgICAgICAgICAgIGFwcGxpY2F0aW9uSWQ6IGFwcElkLFxyXG4gICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZyxcclxuICAgICAgICAgICAgICAgIHJldmlld1JvbGVJZDogcmV2aWV3Um9sZUlkLFxyXG4gICAgICAgICAgICAgICAgdGFza05hbWU6IHRhc2tOYW1lLFxyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IGRlbGl2ZXJhYmxlXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY3VzdG9tRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0SWQ6IGFzc2V0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXZpZXdEZWxpdmVyYWJsZUlkOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAvLyBpc1Jvb3REZWxpdmVyYWJsZVNlc3Npb246IHRydWVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBwZXJzb246IG51bGxcclxuICAgICAgICAgICAgICAgIC8vIHtcclxuICAgICAgICAgICAgICAgIC8vICAgICBmaXJzdE5hbWU6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJEaXNwbGF5TmFtZSgpICxcclxuICAgICAgICAgICAgICAgIC8vICAgICBsYXN0TmFtZTogJycsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgaWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlJFVklFV19CUE1fTlMsIHRoaXMuR0VUX0ZJTEVfUFJPT0ZJTkdfU0VTU0lPTl9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgYnVsa0RlbGl2ZXJhYmxlSWQoZGVsaXZlcmFibGVJZCwgdmVyc2lvbmluZywgdGFza05hbWUpIHtcclxuICAgICAgICBjb25zdCBkZWxpdmVyYWJsZXMgPSBbXTtcclxuICAgICAgICBkZWxpdmVyYWJsZUlkLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IGVsZW1lbnQsXHJcbiAgICAgICAgICAgICAgICB2ZXJzaW9uaW5nOiB2ZXJzaW9uaW5nLFxyXG4gICAgICAgICAgICAgICAgdGFza05hbWU6IHRhc2tOYW1lXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBkZWxpdmVyYWJsZXM7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlQ1JBcHByb3ZhbChhcHBJZDogc3RyaW5nLCBkZWxpdmVyYWJsZUlkOiBzdHJpbmcgfCBBcnJheTxzdHJpbmc+LCBhc3NldElkOiBzdHJpbmcsIHRhc2tOYW1lOiBzdHJpbmcsIHJldmlld1JvbGVJZDogc3RyaW5nLCB2ZXJzaW9uaW5nOiBzdHJpbmcsXHJcbiAgICAgICAgY29tbWVudFRleHQ6IHN0cmluZywgaXNCdWxrQXBwcm92YWw6IGJvb2xlYW4sIHN0YXR1c0lkOiBzdHJpbmcsIHNlbGVjdGVkUmVhc29uc0RhdGEsIHVzZXJDTik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgU2V0U3RhdHVzQXBwcm92YWw6IHtcclxuICAgICAgICAgICAgICAgIGFwcGxpY2F0aW9uSWQ6IGFwcElkLFxyXG4gICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZyxcclxuICAgICAgICAgICAgICAgIHJldmlld1JvbGVJZDogcmV2aWV3Um9sZUlkLFxyXG4gICAgICAgICAgICAgICAgdGFza05hbWU6IHRhc2tOYW1lLFxyXG4gICAgICAgICAgICAgICAgc3RhdHVzSWQ6IHN0YXR1c0lkLFxyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHRoaXMuYnVsa0RlbGl2ZXJhYmxlSWQoZGVsaXZlcmFibGVJZCwgdmVyc2lvbmluZywgdGFza05hbWUpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY3VzdG9tRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0SWQ6IGFzc2V0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXZpZXdEZWxpdmVyYWJsZUlkOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGNvbW1lbnRUZXh0OiBjb21tZW50VGV4dCxcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkUmVhc29uczoge1xyXG4gICAgICAgICAgICAgICAgICAgIEFwcHJvdmFsUmVhc29uOiBzZWxlY3RlZFJlYXNvbnNEYXRhXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgaXNCdWxrQXBwcm92YWw6IGlzQnVsa0FwcHJvdmFsLFxyXG4gICAgICAgICAgICAgICAgcmV2aWV3ZXJVc2VyQ046IHVzZXJDTlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuQlVMS19DUl9BUFBST1ZBTF9OUywgdGhpcy5IQU5ERUxfQ1JfQVBQUk9WQUxfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2UoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgVXNlcklkOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldGN1cnJlbnRVc2VySXRlbUlEKClcclxuICAgICAgICAgfTtcclxuICAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9VU0VSX1BSRUZFUkVOQ0VfTlMsIHRoaXMuR0VUX01QTV9VU0VSX1BSRUZFUkVOQ0VfQllfVVNFUl9XUywgcGFyYW1ldGVyKTtcclxuICAgICB9ICovXHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBVc2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ1VSUkVOVF9NUE1fVVNFUl9QUkVGRVJFTkNFKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ1VSUkVOVF9NUE1fVVNFUl9QUkVGRVJFTkNFKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9VU0VSX1BSRUZFUkVOQ0VfTlMsIHRoaXMuR0VUX01QTV9VU0VSX1BSRUZFUkVOQ0VfQllfVVNFUl9XUywgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ1VSUkVOVF9NUE1fVVNFUl9QUkVGRVJFTkNFLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJQcmVmZXJlbmNlUmVzcG9uc2UgPSByZXNwb25zZS5NUE1fVXNlcl9QcmVmZXJlbmNlID8gcmVzcG9uc2UuTVBNX1VzZXJfUHJlZmVyZW5jZSA6IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlclByZWZlcmVuY2VSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlVXNlclByZWZlcmVuY2UocmVxdWVzdE9iaiwgZXhpc3RpbmdVc2VyUHJlZmVyZW5jZUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IHBhcmFtZXRlcjtcclxuICAgICAgICAgICAgaWYgKGV4aXN0aW5nVXNlclByZWZlcmVuY2VJZCkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICdNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogZXhpc3RpbmdVc2VyUHJlZmVyZW5jZUlkXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAnTVBNX1VzZXJfUHJlZmVyZW5jZS11cGRhdGUnOiByZXF1ZXN0T2JqXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICdNUE1fVXNlcl9QcmVmZXJlbmNlLWNyZWF0ZSc6IHJlcXVlc3RPYmpcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1VTRVJfUFJFRkVSRU5DRV9OUywgZXhpc3RpbmdVc2VyUHJlZmVyZW5jZUlkID8gdGhpcy5VUERBVEVfTVBNX1VTRVJfUFJFRkVSRU5DRV9XUyA6IHRoaXMuQ1JFQVRFX01QTV9VU0VSX1BSRUZFUkVOQ0VfV1MsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5NUE1fVXNlcl9QcmVmZXJlbmNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS51cGRhdGVDdXJyZW50VXNlclByZWZlcmVuY2UocmVzcG9uc2UuTVBNX1VzZXJfUHJlZmVyZW5jZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVEZWZhdWx0VXNlclByZWZlcmVuY2Uodmlld0lkLCBmaWx0ZXJOYW1lLCBtZW51SWQsIGZpZWxkRGF0YSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIFZpZXdJZDogdmlld0lkLFxyXG4gICAgICAgICAgICAgICAgRmlsdGVyTmFtZTogZmlsdGVyTmFtZSxcclxuICAgICAgICAgICAgICAgIFBlcnNvbklkOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRQZXJzb24oKS5JZCxcclxuICAgICAgICAgICAgICAgIE1lbnVJZDogbWVudUlkLFxyXG4gICAgICAgICAgICAgICAgRmllbGREYXRhOiBmaWVsZERhdGFcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NUE1fV09SS0ZMT1dfTlMsIHRoaXMuVVBEQVRFX0RFRkFVTFRfVVNFUl9QUkVGRVJFTkNFX1dTLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLk1QTV9Vc2VyX1ByZWZlcmVuY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnVwZGF0ZUN1cnJlbnRVc2VyUHJlZmVyZW5jZShyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLk1QTV9Vc2VyX1ByZWZlcmVuY2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlVXNlclByZWZlcmVuY2UodXNlclByZWZlcmVuY2VJZCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIE1QTV9Vc2VyX1ByZWZlcmVuY2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAnTVBNX1VzZXJfUHJlZmVyZW5jZS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHVzZXJQcmVmZXJlbmNlSWRcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9VU0VSX1BSRUZFUkVOQ0VfTlMsIHRoaXMuREVMRVRFX01QTV9VU0VSX1BSRUZFUkVOQ0VfV1MsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2UucmVtb3ZlQ3VycmVudFVzZXJQcmVmZXJlbmNlKHVzZXJQcmVmZXJlbmNlSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLypcclxuICAgIE1QTVYzLTE0NDhcclxuICAgICovXHJcbiAgICBHZXRFbWFpbEV2ZW50c0Zvck1hbmFnZXIocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfRU1BSUxfRVZFTlRTX0ZPUl9NQU5BR0VSX05TLCB0aGlzLkdFVF9FTUFJTF9FVkVOVFNfRk9SX01BTkFHRVJfV1MsICcnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgTVBNVjMtMTQ0OFxyXG4gICAgKi9cclxuICAgIEdldEVtYWlsRXZlbnRzRm9yVXNlcihwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9FTUFJTF9FVkVOVFNfRk9SX1VTRVJfTlMsIHRoaXMuR0VUX0VNQUlMX0VWRU5UU19GT1JfVVNFUl9XUywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIEdldE1haWxQcmVmZXJlbmNlQnlVc2VySWQodXNlcklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICB1c2VySWQ6IHVzZXJJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9NQUlMX1BSRUZFUkVOQ0VfQllfVVNFUl9JRF9OUywgdGhpcy5HRVRfTUFJTF9QUkVGRVJFTkNFX0JZX1VTRVJfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIEdldFVzZXJQcmVmZXJlbmNlTWFpbFRlbXBsYXRlcyh1c2VySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX1VTRVJfUFJFRkVSRU5DRV9NQUlMX1RFTVBMQVRFU19OUywgdGhpcy5HRVRfVVNFUl9QUkVGRVJFTkNFX01BSUxfVEVNUExBVEVTX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBNYWlsUHJlZmVyZW5jZSh1c2VySWQsIGVuYWJsZWRFdmVudHMsIGRpc2FibGVkRXZlbnRzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBVc2VySWQ6IHVzZXJJZCxcclxuICAgICAgICAgICAgRW5hYmxlZEV2ZW50czoge1xyXG4gICAgICAgICAgICAgICAgLy8gZW1haWw6IGVuYWJsZWRFdmVudHNcclxuICAgICAgICAgICAgICAgICdFbWFpbF9FdmVudF9UZW1wbGF0ZS1pZCc6IGVuYWJsZWRFdmVudHNcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgRGlzYWJsZWRFdmVudHM6IHtcclxuICAgICAgICAgICAgICAgIC8vIGRpc2FibGU6IGRpc2FibGVkRXZlbnRzXHJcbiAgICAgICAgICAgICAgICAnRW1haWxfRXZlbnRfVGVtcGxhdGUtaWQnOiBkaXNhYmxlZEV2ZW50c1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1dPUktGTE9XX05TLCB0aGlzLk1BSUxfUFJFRkVSRU5DRV9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgRGVsZXRlTVBNX0VtYWlsX1ByZWZlcmVuY2UodXNlckVtYWlsUHJlZmVyZW5jZUlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBNUE1fRW1haWxfUHJlZmVyZW5jZToge1xyXG4gICAgICAgICAgICAgICAgJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJzogdXNlckVtYWlsUHJlZmVyZW5jZUlkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfTVBNX0VNQUlMX1BSRUZFUkVOQ0VfTlMsIHRoaXMuREVMRVRFX01QTV9FTUFJTF9QUkVGRVJFTkNFX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBleHBvcnREYXRhKGNvbHVtbnMsIGZpbGVOYW1lLCBleHBvcnRUeXBlLCBpdGVtSWQsIGZpbGVQYXRoLCBzZWFyY2hSZXF1ZXN0LCBwYWdlTGltaXQsIHRvdGFsQ291bnQsIGV4cG9ydERhdGFNaW5Db3VudCkge1xyXG4gICAgICAgIGNvbnN0IG90ZHNUaWNrZXQgPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVElDS0VUKTtcclxuICAgICAgICBjb25zdCBjdXJyZW50VXNlcklkID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1VTRVJfSUQpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGNvbHVtbnM6IGNvbHVtbnMsXHJcbiAgICAgICAgICAgIGZpbGVOYW1lOiBmaWxlTmFtZSxcclxuICAgICAgICAgICAgZXhwb3J0VHlwZTogZXhwb3J0VHlwZSxcclxuICAgICAgICAgICAgZmlsZVBhdGg6IGZpbGVQYXRoLFxyXG4gICAgICAgICAgICBJbmRleGVyU2VhcmNoUmVxdWVzdDogc2VhcmNoUmVxdWVzdCxcclxuICAgICAgICAgICAgcGFnZUxpbWl0OiBwYWdlTGltaXQsXHJcbiAgICAgICAgICAgIHRvdGFsQ291bnQ6IHRvdGFsQ291bnQsXHJcbiAgICAgICAgICAgIGV4cG9ydERhdGFNaW5Db3VudDogZXhwb3J0RGF0YU1pbkNvdW50LFxyXG4gICAgICAgICAgICBvdGRzVGlja2V0OiAob3Rkc1RpY2tldCAmJiBvdGRzVGlja2V0ICE9IG51bGwpID8gSlNPTi5wYXJzZShvdGRzVGlja2V0KSA6IFwiXCIsXHJcbiAgICAgICAgICAgIGN1cnJlbnRVc2VySWQ6IChjdXJyZW50VXNlcklkICYmIGN1cnJlbnRVc2VySWQgIT0gbnVsbCkgPyBKU09OLnBhcnNlKGN1cnJlbnRVc2VySWQpIDogXCJcIixcclxuICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgSVRFTV9JRDogaXRlbUlkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NUE1fV09SS0ZMT1dfTlMsIHRoaXMuRVhQT1JUX0RBVEFfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEV4cG9ydERhdGFCeVVzZXIodXNlckNOKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgVXNlckNOOiB1c2VyQ05cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfRVhQT1JUX0RBVEFfQllfVVNFUl9OUywgdGhpcy5HRVRfRVhQT1JUX0RBVEFfQllfVVNFUl9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8vIE1QTVYzLTIzNjhcclxuICAgIGRlbGV0ZUV4cG9ydERhdGFCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBNUE1fRXhwb3J0X0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICdNUE1fRXhwb3J0X0RhdGEtaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IGlkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfTVBNX0VYUE9SVF9EQVRBX0JZX0lEX05TLCB0aGlzLkRFTEVURV9NUE1fRVhQT1JUX0RBVEFfQllfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWxlRW5jb2RlZFN0cmluZyhmaWxlUGF0aDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIGZpbGVQYXRoOiBmaWxlUGF0aFxyXG5cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkRPV05MT0FEX0ZJTEVfQllfUEFUSF9OUywgdGhpcy5ET1dOTE9BRF9GSUxFX0JZX1BBVEhfV1MsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Tm90aWZpY2F0aW9uRm9yQ3VycmVudFVzZXIoc2tpcCwgdG9wKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBVc2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFBlcnNvbigpLklkLFxyXG4gICAgICAgICAgICBDdXJzb3I6IHtcclxuICAgICAgICAgICAgICAgICdAeG1sbnMnOiB0aGlzLkNVUlNPUl9OUyxcclxuICAgICAgICAgICAgICAgICdAb2Zmc2V0Jzogc2tpcCxcclxuICAgICAgICAgICAgICAgICdAbGltaXQnOiB0b3BcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9OT1RJRklDQVRJT05fTlMsIHRoaXMuR0VUX05PVElGSUNBVElPTl9CWV9VU0VSX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVOb3RpZmljYXRpb24ocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9OT1RJRklDQVRJT05fTlMsIHRoaXMuVVBEQVRFX05PVElGSUNBVElPTl9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVmYXVsdEVtYWlsQ29uZmlnKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkVNQUlMX0NPTkZJRykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkVNQUlMX0NPTkZJRykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5FTUFJTF9DT05GSUdfTlMsIHRoaXMuR0VUX0RFRkFVTFRfRU1BSUxfQ09ORklHX1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5HZW5lcmFsX0VtYWlsX0NvbmZpZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLkdlbmVyYWxfRW1haWxfQ29uZmlnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=