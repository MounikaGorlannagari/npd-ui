import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { AppService } from './app.service';
import { Observable, forkJoin } from 'rxjs';
import { EntityAppDefService } from './entity.appdef.service';
import { UtilService } from './util.service';
import { SharingService } from './sharing.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { otmmServicesConstants } from '../config/otmmService.constant';
import { GloabalConfig as config } from '../config/config';
import { NotificationService } from '../../notification/notification.service';
import { OTMMMPMDataTypes } from '../objects/OTMMMPMDataTypes';
import { QdsService } from '../../upload/services/qds.service';
import { FileTypes } from '../objects/FileTypes';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import * as i0 from "@angular/core";
import * as i1 from "./entity.appdef.service";
import * as i2 from "./app.service";
import * as i3 from "./util.service";
import * as i4 from "./sharing.service";
import * as i5 from "../../notification/notification.service";
import * as i6 from "@angular/common/http";
import * as i7 from "../../upload/services/qds.service";
let OTMMService = class OTMMService {
    constructor(entityAppDefService, appService, utilService, sharingService, notificationService, http, qdsService) {
        this.entityAppDefService = entityAppDefService;
        this.appService = appService;
        this.utilService = utilService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.http = http;
        this.qdsService = qdsService;
        this.renditionCount = 0;
        this.otmmSessionExists = false;
        this.executedCount = 0;
        this.isSessionMasking = false;
        this.INTERVAL_DELAY = 15000;
        this.STANDARD_SESSION_TIMEOUT_INTERVAL = 600000;
        this.userDetailsObj = {
            email: ''
        };
    }
    getAssetsbyAssetUrl(url, parameters) {
        return this.fireGetRestRequest(url, parameters);
    }
    setMasking(isMasking) {
        this.isSessionMasking = isMasking;
    }
    getMasking() {
        return this.isSessionMasking;
    }
    getMaskingType() {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.maskingType;
    }
    getMediaManagerUrl() {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.mediaManagerConfigUrl;
    }
    getJessionIdFromWebServer() {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.httpOTMMTicket;
    }
    fireGetRestRequest(url, parameters) {
        return new Observable(observer => {
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            this.http.get(this.formRestUrl(url, parameters), httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    firePostRestRequest(url, parameters, contentType, headerObject) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': contentType
            })
        };
        return new Observable(observer => {
            this.http.post(this.formRestUrl(url, null, false), parameters, headerObject)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    fireRequestforJsessionCookie(url, parameters, contentType) {
        const httpOptions = {
            withCredentials: true,
            responseType: 'text',
            headers: new HttpHeaders({
                'Content-Type': contentType,
            })
        };
        return new Observable(observer => {
            this.http.post(url, parameters, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.next(error);
                observer.complete();
            });
        });
    }
    shareAssetsPostRequest(otmmapiurl, parameters, contentType) {
        const mediaManagerConfig = this.sharingService.getMediaManagerConfig();
        const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        if (!mediaManagerConfig) {
            throw new Error('Media Manager config not found');
            return;
        }
        const urlToBeFired = baseUrl + otmmServicesConstants.otmmapiBaseUrl + mediaManagerConfig.apiVersion + '/' + otmmapiurl;
        const httpOptions = {
            withCredentials: true,
            headers: new HttpHeaders({
                'Content-Type': contentType,
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()
            }),
        };
        return new Observable(observer => {
            this.http.post(urlToBeFired, parameters, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.next(error);
                observer.complete();
            });
        });
    }
    fireDeleteRestRequest(url, httpOptions) {
        httpOptions = {
            withCredentials: true,
            responseType: 'text',
            headers: new HttpHeaders({
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
            })
        };
        return new Observable(observer => {
            this.http.delete(this.formRestUrl(url, null, false), httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    firePatchRestRequest(url, parameters, contentType, headerObject) {
        const httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString())
                .set('Content-Type', 'application/json'),
            withCredentials: true
        };
        return new Observable(observer => {
            this.http.patch(this.formRestUrl(url, null, false), parameters, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    firePutRestRequest(url, parameters, contentType, headerObject) {
        let postReq;
        const httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        postReq = this.http.put(this.formRestUrl(url, null), parameters, httpOptions);
        return new Observable(observer => {
            postReq.subscribe((response) => {
                observer.next(response);
                observer.complete();
            }, (error) => {
                this.appService.checkPSSession(() => {
                    observer.error(new Error(error));
                });
            });
        });
    }
    getCookie(cname) {
        const name = cname + '=';
        const cookies = document.cookie.split(';');
        for (const cookie of cookies) {
            let c = cookie;
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }
    formRestParams(parameters) {
        let param = '?';
        for (let i = 0; i < parameters.length; i++) {
            if (parameters[i]['key'] === 'json') {
                param += i === 0 ? parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value)) :
                    '&' + parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value));
            }
            else {
                param += i === 0 ? parameters[i].key + '=' + parameters[i].value : '&' + parameters[i].key + '=' + parameters[i].value;
            }
        }
        return param;
    }
    formRestUrl(otmmapiurl, urlparam, isRendition) {
        const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        if (isRendition === true) {
            return baseUrl + otmmapiurl;
        }
        else {
            const mediaManagerConfig = this.sharingService.getMediaManagerConfig();
            if (!mediaManagerConfig) {
                throw new Error('Media Manager config not found');
                return;
            }
            return baseUrl + otmmServicesConstants.otmmapiBaseUrl +
                this.sharingService.getMediaManagerConfig().apiVersion + '/' + otmmapiurl +
                (urlparam != null ? this.formRestParams(urlparam) : '');
        }
    }
    doPostRequest(url, parameters, headerObject, isOnlyResponse) {
        return new Observable(observer => {
            if (headerObject && headerObject != null) {
                this.http.post(url, parameters, headerObject).subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(new Error(error));
                });
            }
            else {
                const headerOptions = {
                    withCredentials: true,
                    headers: new HttpHeaders({
                        'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()
                    })
                };
                this.http.post(url, parameters, headerOptions).subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(new Error(error));
                });
            }
        });
    }
    doPutRequest(url, parameters, headerObject, isOnlyResponse) {
        return new Observable(observer => {
            if (headerObject) {
                this.http.put(url, parameters, headerObject).subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(new Error(error));
                });
            }
            else {
                const httpOptions = {
                    withCredentials: true,
                    headers: new HttpHeaders({
                        'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    })
                };
                this.http.put(url, parameters, httpOptions)
                    .subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(new Error(error));
                });
            }
        });
    }
    getExportJob() {
        const params = [{
                key: 'load_asset_details',
                value: 'true'
            }, {
                key: 'sort',
                value: 'desc_last_updated_date_time'
            }, {
                key: 'load_job_details',
                value: 'true'
            }, {
                key: 'last_updated_date_time',
                value: 'TODAY'
            }, {
                key: 'limit',
                value: '5000'
            }];
        this.getExportJobs(params)
            .subscribe(response => {
            if (response && response.jobs_resource && response.jobs_resource.collection_summary &&
                response.jobs_resource.collection_summary.actual_count_of_items &&
                response.jobs_resource.collection_summary.actual_count_of_items > 0) {
                console.log("get export response");
            }
            else {
                console.log("No items");
            }
        }, error => {
            console.log("Unable to get data from QDS.");
        });
    }
    checkOtmmSession(isUpload, openQDSTray) {
        return new Observable(observer => {
            // MSV-239
            // if (!isUpload) {
            this.fireGetRestRequest(otmmServicesConstants.otmmSessionUrl, null)
                .subscribe(getSessionResponse => {
                if (getSessionResponse && getSessionResponse.session_resource && getSessionResponse.session_resource.session.id) {
                    // MSV-239
                    this.getSessionResponse = getSessionResponse;
                    otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId = getSessionResponse.session_resource.session.id;
                }
            }, getSessionError => {
                this.startOTMMSession(true) // MVS-239 true
                    .subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
            });
            // } else { // MSV -239
            if (this.sharingService.getMediaManagerConfig().enableQDS &&
                otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
                // tslint:disable-next-line: max-line-length
                this.qdsService.getQDSSession(otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId)
                    .subscribe(qdsSessionResponse => {
                    // tslint:disable-next-line: max-line-length
                    // MSV-239
                    this.qdsSessionResponse = qdsSessionResponse;
                    this.getExportJob();
                    this.qdsService.connectToQDS(qdsSessionResponse.transferschemes_session_resource.transfer_session, isUpload)
                        .subscribe(response => {
                        observer.next(this.getSessionResponse);
                        observer.complete();
                    }, error => {
                        observer.next(this.getSessionResponse);
                        observer.complete();
                    });
                }, qdsSessionError => {
                    this.notificationService.error('Something went wrong while getting QDS session');
                    observer.next(this.getSessionResponse);
                    observer.complete();
                });
            }
            else {
                observer.next(this.getSessionResponse);
                observer.complete();
            }
            // }
        });
    }
    getLoggedInUserDetails(userId) {
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.userDetailsUrl + userId, null)
                .subscribe(response => {
                if (response && response.user_resource && response.user_resource.user) {
                    this.userDetailsObj.email = response.user_resource.user.email_address;
                    observer.next(response.user_resource.user);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while getting user details from OTMM.');
                }
            }, error => {
                observer.error(error);
            });
        });
    }
    matchSessionAttributes(compareUserId, loginname) {
        const userId = this.sharingService.getCurrentUserID();
        let flag = false;
        if (this.utilService.isValid(userId) && this.utilService.isValid(compareUserId)) {
            if (this.utilService.isEqualWithCase(userId, compareUserId) || (compareUserId.indexOf('@') >= 0 && this.utilService.isEqualWithCase(userId, compareUserId.split('@')[0]))) {
                flag = true;
            }
        }
        if (this.utilService.isValid(userId) && this.utilService.isValid(loginname)) {
            if (this.utilService.isEqualWithCase(userId, loginname) || (loginname.indexOf('@') >= 0 && this.utilService.isEqualWithCase(userId, loginname.split('@')[0]))) {
                flag = true;
            }
        }
        return flag;
    }
    startOTMMSession(initialLogin) {
        const parameters = {
            organization: config.config.organizationName,
            // sourceResourceName: '__OTDS#Organizational#Platform#Resource__',
            sourceResourceName: this.entityAppDefService.applicationDetails.OTDS_APPWORKS_RESOURCE_NAME,
            targetResourceName: this.sharingService.getMediaManagerConfig().otdsResourceName,
            isOrgSpace: 'true'
        };
        return new Observable(observer => {
            this.appService.getOTDSTicketForUser(parameters)
                .subscribe(otdsTicketResponse => {
                let ticket = 'OTDSTicket=';
                const token = otdsTicketResponse.tuple.old.OTDSAccessTicket.otmmTicket;
                if (token != null) {
                    ticket += token;
                    let otmmUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
                    otmmUrl = otmmUrl.concat('otmm/ux-html/index.html');
                    // if (initialLogin) {
                    this.fireRequestforJsessionCookie(otmmUrl, ticket, otmmServicesConstants.CONTENT_TYPE.URLENCODED)
                        .subscribe(jSessionResponse => {
                        this.fireGetRestRequest(otmmServicesConstants.otmmSessionUrl, null)
                            .subscribe(getSessionResponse => {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId =
                                this.utilService.isValid(getSessionResponse) &&
                                    this.utilService.isValid(getSessionResponse.session_resource) &&
                                    this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                    this.utilService.isValid(getSessionResponse.session_resource.session.id) ?
                                    getSessionResponse.session_resource.session.id : '';
                            const loginName = this.utilService.isValid(getSessionResponse) &&
                                this.utilService.isValid(getSessionResponse.session_resource) &&
                                this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                this.utilService.isValid(getSessionResponse.session_resource.session.login_name) ?
                                getSessionResponse.session_resource.session.login_name : '';
                            const userId = this.utilService.isValid(getSessionResponse) &&
                                this.utilService.isValid(getSessionResponse.session_resource) &&
                                this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                this.utilService.isValid(getSessionResponse.session_resource.session.user_id) ?
                                getSessionResponse.session_resource.session.user_id : '';
                            this.getLoggedInUserDetails(userId)
                                .subscribe(userResource => {
                                const attrsMatched = this.matchSessionAttributes(userResource.otds_user_id, loginName);
                                if (!attrsMatched) {
                                    this.otmmSessionExists = false;
                                    observer.next(this.destroyOtmmSession().subscribe());
                                    observer.complete();
                                }
                                else {
                                    this.otmmSessionExists = true;
                                    if (this.sharingService.getMediaManagerConfig().enableQDS &&
                                        otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient && initialLogin) { //MVS-239 && initialLogin
                                        this.qdsService.getQDSSession(otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId)
                                            .subscribe(qdsSessionResponse => {
                                            // tslint:disable-next-line: max-line-length
                                            this.qdsService.connectToQDS(qdsSessionResponse.transferschemes_session_resource.transfer_session)
                                                .subscribe(response => {
                                                observer.next(response);
                                                observer.complete();
                                            }, error => {
                                                observer.next(error);
                                                observer.complete();
                                            });
                                        }, qdsSessionError => {
                                            // tslint:disable-next-line: max-line-length
                                            this.notificationService.error('Something went wrong while getting QDS session');
                                            observer.next(this.otmmSessionExists);
                                            observer.complete();
                                        });
                                        //observer.next(this.otmmSessionExists);
                                        //observer.complete();
                                    }
                                    else {
                                        observer.next(this.otmmSessionExists);
                                        observer.complete();
                                    }
                                }
                            }, emailIdError => {
                                observer.error(emailIdError);
                            });
                        }, getSessionError => {
                            observer.error(getSessionError);
                        });
                    }, jSessionError => {
                        observer.error(jSessionError);
                    });
                    //  }
                }
            }, otdsTicketError => {
                observer.error(otdsTicketError);
            });
        });
    }
    destroyOtmmSession() {
        return new Observable(observer => {
            otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId = '';
            this.fireDeleteRestRequest(otmmServicesConstants.otmmSessionUrl)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error('Error while deleting OTMM session.');
            });
        });
    }
    getLoginEmailId(userId) {
        return new Observable(observer => {
            let emailId = '';
            const req = this.fireGetRestRequest(otmmServicesConstants.userDetailsUrl + userId, null);
            req.subscribe(response => {
                if (response && response.user_resource && response.user_resource.user &&
                    response.user_resource.user.email_address) {
                    emailId = response.user_resource.user.email_address;
                }
                observer.next(emailId);
                observer.complete();
            }, error => {
                observer.next(emailId);
                observer.complete();
            });
        });
    }
    getUploadManifest(files) {
        return this.createUploadManifest(files);
    }
    createUploadManifest(files) {
        let uploadManifest = null;
        if (files) {
            if (files.length === 1) {
                uploadManifest = {
                    upload_manifest: {
                        master_files: [{
                                file: {
                                    file_name: files[0].name,
                                    file_type: files[0].type
                                }
                            }]
                    }
                };
            }
            else {
                const masterFiles = new Array();
                for (let i = 0; i < files.length; i++) {
                    masterFiles.push({
                        file: {
                            file_name: files[i].name,
                            file_type: files[i].type
                        }
                    });
                }
                uploadManifest = {
                    upload_manifest: {
                        master_files: masterFiles
                    }
                };
            }
        }
        else {
            throw 'Unable to create upload manifest:\n parameter files is either undefined or invalid.';
        }
        return uploadManifest;
    }
    createUploadManifestForRevision(files) {
        let uploadManifest = null;
        if (files) {
            uploadManifest = {
                upload_manifest: {
                    master_files: [{
                            file: {
                                file_name: files[0].name
                            },
                            uoi_id: files[0].assetId
                        }]
                }
            };
        }
        else {
            const masterFiles = new Array();
            for (let i = 0; i < files.length; i++) {
                masterFiles.push({
                    file: {
                        file_name: files[i].name
                    },
                    uoi_id: files[i].assetId
                });
            }
            uploadManifest = {
                upload_manifest: {
                    master_files: masterFiles
                }
            };
        }
        return uploadManifest;
    }
    createTransferDetails(scheme, files) {
        let transferDetails = null;
        const transferFileItems = new Array();
        for (let i = 0; i < files.length; i++) {
            transferFileItems.push({
                scheme_file_id: '',
                scheme_file_name: ''
            });
        }
        transferDetails = {
            transfer_details_param: {
                transfer_file_items: transferFileItems,
                transfer_scheme: scheme
            }
        };
        return transferDetails;
    }
    getAssetRepresentation(metadataFields, metaDataFieldValues, metadataModel) {
        return this.createAssetRepresentation(metadataFields, metaDataFieldValues, metadataModel, false);
    }
    createAssetRepresentation(metadataFields, metaDataFieldValues, metaDataModel, securityPolicyID) {
        let assetRepresentation = null;
        const metadataFieldList = new Array();
        let metaDataElement = null;
        const securityPolicies = [];
        if (metadataFields && Array.isArray(metadataFields) && metaDataModel && metaDataModel != null && metaDataModel !== '') {
            const defaultSecPolicyId = 1;
            const securityPolicy = defaultSecPolicyId;
            if (securityPolicyID && securityPolicyID != null && securityPolicyID !== '') {
                const ids = securityPolicyID.split(',');
                for (let i = 0; i < ids.length; i++) {
                    const tempObj = {};
                    tempObj['id'] = ids[i];
                    securityPolicies.push(tempObj);
                }
            }
            else {
                let policy = {
                    id: defaultSecPolicyId,
                };
                securityPolicies.push(policy);
                policy = null;
            }
            // iterate all configured metadatafields and prepare metadata field configuration to send with request.
            for (let i = 0; i < metadataFields.length; i++) {
                metaDataElement = {};
                metaDataElement.id = metadataFields[i].id;
                let type = metadataFields[i].type;
                if (!type || type == null || type.trim() === '') {
                    type = 'com.artesia.metadata.MetadataField';
                }
                metaDataElement.type = type;
                // if metaDataFieldValues is defined and field from entity is of cascading type (eg: metadataFields[i].fieldRef = 'brand')
                if (metaDataFieldValues && metaDataFieldValues != null && metadataFields[i].fieldRef &&
                    metadataFields[i].fieldRef !== '' && metadataFields[i].fieldRef != null &&
                    metadataFields[i].fieldRef.indexOf('^') === -1 && metaDataFieldValues[metadataFields[i].fieldRef] !== '') {
                    let metadataValue = metaDataFieldValues[metadataFields[i].fieldRef];
                    // array of value inside values
                    if (type === 'com.artesia.metadata.MetadataTableField') {
                        const values = [];
                        if (Array.isArray(metadataValue) && metadataValue.length > 0) {
                            for (let index = 0; index < metadataValue.length; index++) {
                                if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string') {
                                    metadataValue[index] = metadataValue[index].toString();
                                }
                                if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number') {
                                    metadataValue[index] = metadataValue[index].parseInt();
                                }
                                const value = {
                                    value: {
                                        value: metadataValue[index],
                                        type: metadataFields[i].datatype
                                    }
                                };
                                values.push(value);
                            }
                        }
                        else {
                            if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string' &&
                                this.utilService.isValid(metadataValue)) {
                                metadataValue = metadataValue.toString();
                            }
                            else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number'
                                && this.utilService.isValid(metadataValue)) {
                                metadataValue = metadataValue.parseInt();
                            }
                            const value = {
                                value: {
                                    value: metadataValue,
                                    type: metadataFields[i].datatype
                                }
                            };
                            values.push(value);
                        }
                        metaDataElement.values = values;
                    }
                    else {
                        if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string' &&
                            this.utilService.isValid(metadataValue)) {
                            metadataValue = metadataValue.toString();
                        }
                        else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number'
                            && this.utilService.isValid(metadataValue)) {
                            metadataValue = metadataValue.parseInt();
                        }
                        else {
                        }
                        const value = {
                            value: {
                                value: metadataValue,
                                type: metadataFields[i].datatype
                            }
                        };
                        metaDataElement.value = value;
                    }
                }
                else if (metaDataFieldValues && metaDataFieldValues != null && metadataFields[i].fieldRef &&
                    metadataFields[i].fieldRef !== '' && metadataFields[i].fieldRef != null &&
                    metadataFields[i].fieldRef.indexOf('^') !== -1 && metaDataFieldValues[metadataFields[i].fieldRef] !== '') {
                    const fields = metadataFields[i].fieldRef.split('^');
                    const value = {
                        value: {
                            value: '',
                            type: metadataFields[i].datatype
                        }
                    };
                    // iterate all cascade fields
                    for (let x = 0; x < fields.length; x++) {
                        let metadatavalue = metaDataFieldValues[fields[x]];
                        // each cascaded field value only one value
                        if (Array.isArray(metadatavalue)) {
                            metadatavalue = metadatavalue[0];
                        }
                        // on metadatavalue is received
                        if (metadatavalue !== '' && metadatavalue != null && metadatavalue) {
                            // condition to check already value has appended for cascading field
                            if (value.value.value !== '' && value.value.value != null && value.value.value) {
                                value.value.value = value.value.value + '^' + metadatavalue;
                            }
                            else {
                                value.value.value = metadatavalue;
                            }
                        }
                    }
                }
                else {
                    let defvalue;
                    // if default value is present
                    if (metadataFields[i].default !== '' && metadataFields[i].default != null && metadataFields[i].default) {
                        defvalue = metadataFields[i].default;
                    }
                    else {
                        if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string') {
                            defvalue = 'NA';
                        }
                        else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number') {
                            defvalue = 0;
                        }
                        else {
                        }
                    }
                    const value = {
                        value: {
                            value: defvalue,
                            type: metadataFields[i].datatype
                        }
                    };
                    metaDataElement.value = value;
                }
                metadataFieldList.push(metaDataElement);
            }
            let metadataModelId = '';
            if (metaDataModel && metaDataModel != null && metaDataModel.trim() !== '') {
                metadataModelId = metaDataModel;
            }
            else {
                throw 'MetaData ModelID is missing.';
            }
            assetRepresentation = {
                asset_resource: {
                    asset: {
                        metadata: {
                            metadata_element_list: metadataFieldList
                        },
                        metadata_model_id: metadataModelId,
                        security_policy_list: securityPolicies
                    }
                }
            };
        }
        else {
            throw 'Unable to create asset representation'
                + ' the parameters metadataFields & metaDataFieldValues are not correct or undefined.';
        }
        return assetRepresentation;
    }
    uploadAssets(files, metadataFields, metadataFieldValues, parentForlderID, importTemplateID, metadataModel, securityPolicyID) {
        const url = this.formRestUrl(otmmServicesConstants.asssetUrl, null, false);
        const param = new FormData();
        let blob = null;
        let content = null;
        if (files && files != null && metadataFieldValues && metadataFieldValues != null && metadataFields && parentForlderID &&
            importTemplateID) {
            content = JSON.stringify(this.createAssetRepresentation(metadataFields, metadataFieldValues, metadataModel, securityPolicyID));
            blob = new Blob([content], {
                type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
            });
            param.append('asset_representation', blob);
            param.append('parent_folder_id', parentForlderID);
            param.append('import_template_id', importTemplateID);
            param.append('no_content', 'false');
            content = JSON.stringify(this.createUploadManifest(files));
            blob = new Blob([content], {
                type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
            });
            param.append('manifest', blob);
            for (const file of files) {
                param.append('files', file);
            }
        }
        else {
            throw 'Unable to upload assets:\nbecause the Files are meta data values are not passed.';
            return null;
        }
    }
    formAssetRepresentation(metadata, metaDataModel, securityPolicyID) {
        let assetRepresentation = null;
        const metadataElementList = new Array();
        const securityPolicies = [];
        if (metadata && metaDataModel && metaDataModel != null && metaDataModel !== '') {
            const defaultSecPolicyId = 1;
            if (securityPolicyID && securityPolicyID != null && securityPolicyID !== '') {
                const ids = securityPolicyID.split(';');
                for (const id of ids) {
                    const tempObj = {};
                    tempObj['id'] = Number(id);
                    securityPolicies.push(tempObj);
                }
            }
            else {
                let policy = {
                    id: defaultSecPolicyId,
                };
                securityPolicies.push(policy);
                policy = null;
            }
            let metadataModelId = '';
            if (metaDataModel && metaDataModel != null && metaDataModel.trim() !== '') {
                metadataModelId = metaDataModel;
            }
            else {
                throw new Error('Metadata ModelID is missing.');
            }
            if (metadata && metadata.metadata_element_list && Array.isArray(metadata.metadata_element_list)) {
                for (const metadataGroup of metadata.metadata_element_list) {
                    for (const metadataElement of metadataGroup.metadata_element_list) {
                        if (metadataElement && metadataElement.id === 'MPM.UTILS.DATA_TYPE' && metadataElement.value && metadataElement.value.value) {
                            metadataElement.value.value.value = OTMMMPMDataTypes.ASSET;
                        }
                        metadataElementList.push(metadataElement);
                    }
                }
            }
            assetRepresentation = {
                asset_resource: {
                    asset: {
                        metadata: {
                            metadata_element_list: metadataElementList
                        },
                        metadata_model_id: metadataModelId,
                        security_policy_list: securityPolicies
                    }
                }
            };
        }
        else {
            console.error(`Unable to create asset representation:
            the parameters metadataFields & metaDataFieldValues are not correct or undefined.`);
        }
        return assetRepresentation;
    }
    uploadSingleAsset(files, metaDataFieldValues, parentFolderID, importTemplateID, metadataModel, securityPolicy) {
        const url = this.formRestUrl(otmmServicesConstants.asssetUrl, null, false);
        const param = new FormData();
        let blob = null;
        let content = null;
        if ((files && Array.isArray(files)) && metaDataFieldValues && parentFolderID && importTemplateID) {
            for (const file of files) {
                content = JSON.stringify(this.formAssetRepresentation(metaDataFieldValues, metadataModel, securityPolicy));
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                param.append('asset_representation', blob);
                param.append('parent_folder_id', parentFolderID);
                param.append('import_template_id', importTemplateID);
                param.append('no_content', 'false');
                content = JSON.stringify(this.createUploadManifest(files));
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                param.append('manifest', blob);
                param.append('files', file);
            }
            return this.doPostRequest(url, param, null, false);
        }
        else {
            console.error('Unable to upload assets:\nbecause the parameters are either incorrect or undefined.');
            return null;
        }
    }
    createOTMMJob(fileName) {
        return new Observable(observer => {
            const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
            const version = this.sharingService.getMediaManagerConfig().apiVersion;
            const url = baseUrl + 'otmmapi/' + version + '/jobs/imports';
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.MULTIPARTFORMDATA,
                    'X-OTMM-Request': 'true',
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    'X-USES-CHUNKS': 'false'
                })
            };
            const formData = new FormData();
            formData.append('file_name', fileName);
            this.http.post(url, formData, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    importOTMMJob(files, metadataFields, metaDataFieldValues, parentForlderID, importTemplateID, metadataModel, securityPolicy, importJobId, isRevision) {
        const url = this.formRestUrl((isRevision ? otmmServicesConstants.checkinUrl : otmmServicesConstants.importJobUrl) + '/' + importJobId, null, false);
        const param = new FormData();
        let blob = null;
        let content = null;
        if ((files && Array.isArray(files)) && metaDataFieldValues &&
            parentForlderID && importTemplateID) {
            for (let i = 0; i < files.length; i++) {
                content = JSON.stringify(this.formAssetRepresentation(metaDataFieldValues, metadataModel, securityPolicy));
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                if (!isRevision) {
                    param.append('asset_representation', blob);
                    param.append('parent_folder_id', parentForlderID);
                }
                param.append('import_job_id', importJobId);
                param.append('file_name', files[i].name);
                if (isRevision) {
                    content = JSON.stringify(this.createUploadManifestForRevision(files));
                }
                else {
                    content = JSON.stringify(this.createUploadManifest(files));
                }
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                param.append('manifest', blob);
                if (this.sharingService.getMediaManagerConfig().enableQDS && otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
                    content = JSON.stringify(this.createTransferDetails('QDS', files));
                    blob = new Blob([content], {
                        type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                    });
                    param.append('transfer_details', blob);
                }
                else {
                    // content = JSON.stringify(this.createUploadManifest(files));
                    param.append('files', files[i]);
                }
            }
            return this.doPutRequest(url, param, null, false);
        }
        else {
            console.error('Unable to upload assets:\nbecause the parameters are either incorrect or undefined.');
            return null;
        }
    }
    validateAssetLockState(assetList) {
        let fileName = '';
        let fileCounter = 0;
        for (let k = 0; k < assetList.length; k++) {
            if (assetList[k].asset_state === 'LOCKED' &&
                assetList[k].metadata_lock_state_user_name !== this.sharingService.getCurrentUserID()) {
                fileCounter++;
                if (k > 0) {
                    fileName += ',';
                }
                fileName += fileCounter + ')' + assetList[k].name + ' - Locked By : ' + assetList[k].metadata_lock_state_user_name;
                fileName += '\n';
            }
        }
        if (fileName.length > 0) {
            this.notificationService.info(`Following file(s) are locked by different user and cannot proceed with revision upload,
             Kindly contact the respective users or administrator to remove lock on the file(s) \n` + fileName);
            return false;
        }
        else {
            return true;
        }
    }
    assetCheckout(assetIds) {
        return new Observable(observer => {
            if (!assetIds || !assetIds.length || assetIds.length === 0) {
                this.notificationService.info('No asset ids are available to check out');
                observer.next(false);
                observer.complete();
            }
            const urlParam = [{
                    key: 'action',
                    value: 'check_out'
                }, {
                    key: 'json',
                    value: {
                        key: 'selection_context',
                        value: {
                            selection_context_param: {
                                selection_context: {
                                    asset_ids: assetIds,
                                    type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                                    include_descendants: 'IMMEDIATE'
                                }
                            }
                        }
                    }
                }];
            const url = this.formRestUrl('assets/state', null, false);
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED
                }),
                transformRequest: 'xWwwFormUrlencoded'
            };
            this.doPutRequest(url, this.formRestParams(urlParam).substring(1), httpOptions, false)
                .subscribe(response => {
                if (response && response['bulk_asset_result_representation'] &&
                    response['bulk_asset_result_representation'].bulk_asset_result &&
                    response['bulk_asset_result_representation'].bulk_asset_result) {
                    if (response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list &&
                        response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length &&
                        response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length > 0) {
                        observer.error(new Error('Unable to check out ' +
                            response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length + ' assets.'));
                    }
                    else if (response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list &&
                        response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length &&
                        response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length > 0 &&
                        // tslint:disable-next-line: max-line-length
                        assetIds.length === response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length) {
                        observer.next(response);
                        observer.complete();
                    }
                    else {
                        observer.error(new Error('Unable to check out ' + assetIds.length + ' assets.'));
                    }
                }
                else {
                    observer.error(new Error('Invalid Asset Ids.'));
                }
            });
        });
    }
    lockAssets(assetIds) {
        const urlParam = [{
                key: 'action',
                value: 'lock'
            }, {
                key: 'json',
                value: {
                    key: 'selection_context',
                    value: {
                        selection_context_param: {
                            selection_context: {
                                asset_ids: assetIds,
                                type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                                include_descendants: 'NONE'
                            }
                        }
                    }
                }
            }];
        const url = this.formRestUrl('assets/state', null, false);
        return this.doPutRequest(url, this.formRestParams(urlParam).substring(1), {
            withCredentials: true,
            headers: new HttpHeaders({
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED
            })
        }, false);
    }
    assetImport(fileName) {
        const url = this.formRestUrl(otmmServicesConstants.importJobUrl, null, false);
        let param;
        if (fileName) {
            param = this.formRestParams([{
                    key: 'file_name',
                    value: fileName
                }
            ]);
        }
        return this.doPostRequest(url, param, false, false);
    }
    assetRendition(file, importJobID) {
        const url = this.formRestUrl(otmmServicesConstants.renditionsUrl, null, false);
        const param = new FormData();
        param.append('import_job_id', importJobID);
        param.append('file', file);
        param.append('file_name', file.name);
        return this.doPostRequest(url, param, false, false);
    }
    assetsRendition(files, importJobID) {
        const promises = [];
        for (const file of files) {
            promises.push(this.assetRendition(file, importJobID));
        }
        return new Observable(observer => {
            forkJoin(promises).subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    assetCheckIn(assetDetail, importJobID) {
        const url = this.formRestUrl(otmmServicesConstants.importJobUrl + '/' + importJobID, null, false);
        const param = new FormData();
        let blob = null;
        let content = null;
        param.append('import_job_id', importJobID);
        let uploadManifest = null;
        uploadManifest = {
            upload_manifest: {
                master_files: [{
                        file: {
                            file_name: assetDetail.name
                        },
                        uoi_id: assetDetail.assetId
                    }]
            }
        };
        param.append('file_name', assetDetail.name);
        content = uploadManifest;
        blob = new Blob([JSON.stringify(content)], {
            type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
        });
        param.append('manifest', blob);
        return this.doPutRequest(url, param, false, false);
    }
    getFilePaths(files) {
        const filePaths = [];
        for (let i = 0; i < files.length; i++) {
            if (files[i].path) {
                filePaths.push(files[i].path);
            }
        }
        return filePaths;
    }
    formDownloadName(downloadType) {
        console.log('Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + (downloadType ? '.' + downloadType : '.zip'));
        return 'Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + (downloadType ? '.' + downloadType : '.zip');
    }
    createExportJob(assetIds, downloadType) {
        return new Observable(observer => {
            const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
            const version = this.sharingService.getMediaManagerConfig().apiVersion;
            const url = baseUrl + 'otmmapi/' + version + '/jobs/exports';
            const httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED,
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            let exportRequest;
            if (downloadType.toUpperCase() == FileTypes.ZIP) {
                exportRequest = {
                    export_request_param: {
                        export_request: {
                            content_request: {
                                export_preview_content: false,
                                export_master_content: true,
                                export_supporting_content: false
                            },
                            create_collection_folders: false,
                            delivery_template: {
                                attribute_values: [
                                    {
                                        argument_number: 1,
                                        value: '2'
                                    },
                                    {
                                        argument_number: 4,
                                        value: 'N'
                                    }
                                ],
                                id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD.DEFAULT',
                                transformer_id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD'
                            },
                            error_handling_action: 'DEFAULT_HANDLING',
                            export_job_transformers: [
                                {
                                    transformer_id: 'ARTESIA.TRANSFORMER.ZIP COMPRESSION.DEFAULT',
                                    arguments: [
                                        null,
                                        this.formDownloadName()
                                    ]
                                }
                            ],
                            process_asynchronously: true,
                            remove_working_directory: true,
                            replace_export_dir: true,
                            write_xml: true
                        }
                    }
                };
            }
            else {
                exportRequest = {
                    export_request_param: {
                        export_request: {
                            content_request: {
                                export_master_content: true,
                                export_supporting_content: false
                            },
                            create_collection_folders: false,
                            delivery_template: {
                                attribute_values: [
                                    {
                                        argument_number: 1,
                                        value: '2'
                                    },
                                ],
                                id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD.DEFAULT',
                                transformer_id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD'
                            },
                            error_handling_action: 'DEFAULT_HANDLING',
                            process_asynchronously: true,
                            remove_working_directory: true,
                            replace_export_dir: true,
                            write_xml: false
                        }
                    }
                };
            }
            const selectionContext = {
                selection_context_param: {
                    selection_context: {
                        asset_ids: assetIds,
                        type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                        include_descendants: 'ALL',
                        child_type: 'ASSETS'
                    }
                }
            };
            const params = 'export_request=' + JSON.stringify(exportRequest) +
                '&selection_context=' + encodeURIComponent(JSON.stringify(selectionContext));
            this.http.post(url, params, httpOptions)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getExportJobs(params) {
        return new Observable(observer => {
            const url = 'jobs'; // MSV-239 -> 'jobs/'
            this.fireGetRestRequest(url, params)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getOTMMSystemDetails() {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS) != null) {
                var otmmVersion = JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS));
                if (otmmVersion == null || otmmVersion == undefined || parseFloat(otmmVersion) < 0) {
                    JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS)).system_details_resource.system_details_map.entry.forEach(e => {
                        if (e.key.toString() === ApplicationConfigConstants.OTMM_SYSTEM_DETAILS_CONSTANTS.BUILD_VERSION)
                            sessionStorage.setItem(SessionStorageConstants.OTMM_VERSION, JSON.stringify(e.value));
                    });
                }
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS)));
                observer.complete();
            }
            else {
                this.fireGetRestRequest(otmmServicesConstants.systemdetailsUrl, null)
                    .subscribe(response => {
                    if (response != null) {
                        response.system_details_resource.system_details_map.entry.forEach(e => {
                            if (e.key.toString() === ApplicationConfigConstants.OTMM_SYSTEM_DETAILS_CONSTANTS.BUILD_VERSION)
                                sessionStorage.setItem(SessionStorageConstants.OTMM_VERSION, JSON.stringify(e.value));
                        });
                        sessionStorage.setItem(SessionStorageConstants.SYSTEM_DETAILS, JSON.stringify(response));
                        observer.next(response);
                        observer.complete();
                    }
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
    getOTMMAssetById(assetId, loadType) {
        let assetDetails = '';
        const dataLoadRequest = {
            data_load_request: {
                load_multilingual_field_values: true,
                load_subscribed_to: true,
                load_asset_content_info: true,
                load_metadata: true,
                load_inherited_metadata: true,
                load_thumbnail_info: true,
                load_preview_info: true,
                load_pdf_preview_info: true,
                load_3d_preview_info: true,
                load_destination_links: true,
                load_security_policies: true,
                load_path: true
            }
        };
        const urlParam = [{
                key: 'load_type',
                value: loadType
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        if (loadType === 'custom') {
            urlParam.push({
                key: 'data_load_request',
                value: encodeURIComponent(JSON.stringify(dataLoadRequest))
            });
        }
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId, urlParam)
                .subscribe(response => {
                if (response && response.asset_resource.asset) {
                    assetDetails = response.asset_resource.asset;
                }
                observer.next(assetDetails);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getOTMMFodlerById(folderId) {
        let folderDetails = '';
        const urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'load_multilingual_values',
                value: 'true'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }, {
                key: 'after',
                value: 'after'
            }, {
                key: 'limit',
                value: '50'
            }];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.foldersUrl + folderId, urlParam)
                .subscribe(response => {
                if (response && response.folder_resource && response.folder_resource.folder) {
                    folderDetails = response.folder_resource.folder;
                }
                observer.next(folderDetails);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getSearchOperatorsList() {
        let searchOperatorList = '';
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.searchoperators, null)
                .subscribe(response => {
                if (response && response.search_operators_resource && response.search_operators_resource.search_operator_list) {
                    searchOperatorList = response.search_operators_resource.search_operator_list;
                }
                observer.next(searchOperatorList);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getAllMetadatModelList() {
        let metaDataModelList = '';
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.metadatamodels, null)
                .subscribe(response => {
                if (response) {
                    metaDataModelList = response.metadata_models_resource;
                }
                observer.next(metaDataModelList);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getMetadatModelById(modelId) {
        let metaDataModelDetails = '';
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.metadatamodels + modelId, null)
                .subscribe(response => {
                if (response) {
                    metaDataModelDetails = response.metadata_model_resource.metadata_model;
                }
                observer.next(metaDataModelDetails);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    saveSearch(searchConfig, skip, top, userSessionId) {
        const url = otmmServicesConstants.savedSearchUrl;
        /* if (searchConfig.savedSearchId) {
            url = otmmServicesConstants.savedsearches + searchConfig.savedSearchId
        } */
        const formData = [];
        if (searchConfig.searchKey) {
            formData.push(searchConfig.searchKey);
        }
        if (searchConfig.sortString) {
            formData.push('sort=' + searchConfig.sortString);
        }
        if (searchConfig.keyword_query) {
            formData.push('keyword_query=' + encodeURIComponent(searchConfig.keyword_query));
        }
        if (searchConfig.load_type === 'custom') {
            formData.push('data_load_request=' + '{"data_load_request":{"child_count_load_type":' + '"' + searchConfig.child_count_load_type + '"' + ' }}');
        }
        formData.push('after=' + skip);
        formData.push('limit=' + top);
        formData.push('multilingual_language_code=en_US');
        if (searchConfig.folder_filter) {
            formData.push('folder_filter=' + searchConfig.folder_filter);
        }
        if (searchConfig.folder_filter_type) {
            formData.push('folder_filter_type=' + searchConfig.folder_filter_type);
        }
        if (searchConfig.facet_restriction_list) {
            formData.push('facet_restriction_list=' + encodeURIComponent(JSON.stringify(searchConfig.facet_restriction_list)));
        }
        if (searchConfig.search_config_id) {
            formData.push('search_config_id=' + searchConfig.search_config_id);
        }
        if (typeof searchConfig.is_favorite === 'boolean') {
            formData.push('is_favorite=' + searchConfig.is_favorite);
        }
        if (searchConfig.name) {
            formData.push('name=' + encodeURIComponent(searchConfig.name));
        }
        if (searchConfig.description) {
            formData.push('description=' + encodeURIComponent(searchConfig.description));
        }
        if (typeof searchConfig.is_public === 'boolean') {
            formData.push('is_public=' + searchConfig.is_public);
        }
        if (searchConfig.preference_id) {
            formData.push('preference_id=' + (searchConfig.preference_id ? searchConfig.preference_id : 'ARTESIA.PREFERENCE.GALLERYVIEW.DISPLAYED_FIELDS'));
        }
        if (searchConfig.metadata_to_return) {
            formData.push('metadata_to_return=' + searchConfig.metadata_to_return);
        }
        if (searchConfig.search_condition_list) {
            formData.push('search_condition_list=' + encodeURIComponent(JSON.stringify(searchConfig.search_condition_list)));
        }
        if (searchConfig.savedSearchId) {
            formData.push('id=' + searchConfig.savedSearchId);
        }
        const param = formData.join('&');
        const httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        /* if (searchConfig.savedSearchId) {
            return this.firePatchRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
        } */
        return this.firePostRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
    }
    searchCustomText(searchConfig, skip, top, userSessionId) {
        let url = otmmServicesConstants.textAssetSearchUrl;
        if (searchConfig.isSavedSearch) {
            url = otmmServicesConstants.savedSearchUrl;
        }
        const formData = [];
        if (searchConfig.searchKey) {
            formData.push(searchConfig.searchKey);
        }
        if (searchConfig.sortString) {
            formData.push('sort=' + searchConfig.sortString);
        }
        if (searchConfig.keyword_query) {
            formData.push('keyword_query=' + encodeURIComponent(searchConfig.keyword_query));
        }
        formData.push('load_type=' + (searchConfig.load_type ? searchConfig.load_type : 'metadata'));
        formData.push('load_multilingual_values=true');
        formData.push('level_of_detail=' + (searchConfig.level_of_detail ? searchConfig.level_of_detail : 'slim'));
        if (searchConfig.load_type === 'custom') {
            formData.push('data_load_request=' + '{"data_load_request":{"child_count_load_type":' + '"' + searchConfig.child_count_load_type + '"' + ' }}');
        }
        formData.push('after=' + skip);
        formData.push('limit=' + top);
        formData.push('multilingual_language_code=en_US');
        if (searchConfig.folder_filter) {
            formData.push('folder_filter=' + searchConfig.folder_filter);
        }
        if (searchConfig.folder_filter_type) {
            formData.push('folder_filter_type=' + searchConfig.folder_filter_type);
        }
        if (searchConfig.facet_restriction_list) {
            formData.push('facet_restriction_list=' + encodeURIComponent(JSON.stringify(searchConfig.facet_restriction_list)));
        }
        if (searchConfig.search_config_id) {
            formData.push('search_config_id=' + searchConfig.search_config_id);
        }
        if (searchConfig.is_favorite) {
            formData.push('is_favorite=' + searchConfig.is_favorite);
        }
        if (searchConfig.name) {
            formData.push('name=' + encodeURIComponent(searchConfig.name));
        }
        if (searchConfig.is_public) {
            formData.push('is_public=' + searchConfig.is_public);
        }
        if (searchConfig.preference_id) {
            formData.push('preference_id=' + (searchConfig.preference_id ? searchConfig.preference_id : 'ARTESIA.PREFERENCE.GALLERYVIEW.DISPLAYED_FIELDS'));
        }
        if (searchConfig.metadata_to_return) {
            formData.push('metadata_to_return=' + searchConfig.metadata_to_return);
        }
        if (searchConfig.search_condition_list) {
            formData.push('search_condition_list=' + encodeURIComponent(JSON.stringify(searchConfig.search_condition_list)));
        }
        const param = formData.join('&');
        const httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        return this.firePostRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
    }
    updateMetadata(assetId, type, metadataParameters) {
        return new Observable(observer => {
            const contentType = 'application/x-www-form-urlencoded';
            const lockFormData = [];
            lockFormData.push('action=lock');
            const param = lockFormData.join('&');
            this.firePutRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/state', param, null, null)
                .subscribe(lockResponse => {
                this.firePatchRestRequest(((type === 'FOLDER' ? otmmServicesConstants.foldersUrl : (otmmServicesConstants.asssetUrl + '/')) + assetId), metadataParameters, null, null)
                    .subscribe(response => {
                    const unlockFormData = [];
                    unlockFormData.push('action=unlock');
                    const parameter = unlockFormData.join('&');
                    this.firePutRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/state', parameter, null, null)
                        .subscribe(unlockResponse => {
                        observer.next(unlockResponse);
                        observer.complete();
                    }, error => {
                        observer.error(error);
                    });
                });
            });
        });
    }
    getAllSavedSearches() {
        let savedsearches = [];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.savedsearches, null)
                .subscribe(response => {
                if (response && response.saved_searches_resource && response.saved_searches_resource.saved_search_list) {
                    savedsearches = response.saved_searches_resource.saved_search_list;
                }
                observer.next(savedsearches);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getSavedSearchById(id) {
        let savedsearch = {};
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.savedsearches + id, null)
                .subscribe(response => {
                if (response && response.saved_search_resource && response.saved_search_resource.saved_search) {
                    savedsearch = response.saved_search_resource.saved_search;
                }
                observer.next(savedsearch);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    deleteSavedSearch(id) {
        return new Observable(observer => {
            this.fireDeleteRestRequest(otmmServicesConstants.savedsearches + id, null)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    addRelationalOperator(searchConditions) {
        searchConditions.forEach(searchCondition => {
            if (searchCondition.relational_operator && searchCondition.relational_operator !== '') {
                return;
            }
            searchCondition.relational_operator = 'and';
        });
        return searchConditions;
    }
    getAssetVersionsByAssetId(assetId) {
        let assetVersionDetails = '';
        const urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/versions', urlParam)
                .subscribe(response => {
                if (response && response.assets_resource && response.assets_resource.asset_list) {
                    assetVersionDetails = response.assets_resource.asset_list;
                }
                observer.next(assetVersionDetails);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getAssetByAssetId(assetId) {
        let assetVersionDetails = '';
        const urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId, urlParam)
                .subscribe(response => {
                if (response && response.asset_resource) {
                    assetVersionDetails = response.asset_resource.asset;
                }
                observer.next(assetVersionDetails);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getsearchconfigurations() {
        return new Observable(observer => {
            const urlParam = [{
                    key: 'retrieval_type',
                    value: 'full'
                }];
            this.fireGetRestRequest(otmmServicesConstants.searchconfigurations, urlParam)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getAllLookupDomains() {
        let lookupDomains = [];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.lookupdomains, null)
                .subscribe(response => {
                if (response && response.lookup_domains_resource && response.lookup_domains_resource.lookup_domains) {
                    lookupDomains = response.lookup_domains_resource.lookup_domains;
                }
                observer.next(lookupDomains);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getLookupDomain(lookupDomainId) {
        let lookupDomainValues = [];
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.lookupdomains + lookupDomainId, null)
                .subscribe(response => {
                if (response && response.lookup_domain_resource && response.lookup_domain_resource.lookup_domain &&
                    response.lookup_domain_resource.lookup_domain.domainValues) {
                    lookupDomainValues = response.lookup_domain_resource.lookup_domain.domainValues;
                }
                observer.next(lookupDomainValues);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getFacetConfigurations() {
        return new Observable(observer => {
            this.fireGetRestRequest(otmmServicesConstants.facetconfigurations, null).subscribe(response => {
                let facetConfigurations = [];
                if (response && response.facet_configurations_resource &&
                    Array(response.facet_configurations_resource.facet_configuration_list)) {
                    facetConfigurations = response.facet_configurations_resource.facet_configuration_list;
                }
                observer.next(facetConfigurations);
                observer.complete();
            }, error => {
                this.notificationService.error('Error while getting facet configurations');
                observer.error(error);
            });
        });
    }
};
OTMMService.ctorParameters = () => [
    { type: EntityAppDefService },
    { type: AppService },
    { type: UtilService },
    { type: SharingService },
    { type: NotificationService },
    { type: HttpClient },
    { type: QdsService }
];
OTMMService.ɵprov = i0.ɵɵdefineInjectable({ factory: function OTMMService_Factory() { return new OTMMService(i0.ɵɵinject(i1.EntityAppDefService), i0.ɵɵinject(i2.AppService), i0.ɵɵinject(i3.UtilService), i0.ɵɵinject(i4.SharingService), i0.ɵɵinject(i5.NotificationService), i0.ɵɵinject(i6.HttpClient), i0.ɵɵinject(i7.QdsService)); }, token: OTMMService, providedIn: "root" });
OTMMService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], OTMMService);
export { OTMMService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RtbS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQzs7Ozs7Ozs7O0FBTWpHLElBQWEsV0FBVyxHQUF4QixNQUFhLFdBQVc7SUFFcEIsWUFDVyxtQkFBd0MsRUFDeEMsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsbUJBQXdDLEVBQ3hDLElBQWdCLEVBQ2hCLFVBQXNCO1FBTnRCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFHakMsbUJBQWMsR0FBRyxDQUFDLENBQUM7UUFDbkIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGtCQUFhLEdBQUcsQ0FBQyxDQUFDO1FBRWxCLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUN6QixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUV2QixzQ0FBaUMsR0FBRyxNQUFNLENBQUM7UUFFM0MsbUJBQWMsR0FBRztZQUNiLEtBQUssRUFBRSxFQUFFO1NBQ1osQ0FBQztJQWJFLENBQUM7SUFrQkwsbUJBQW1CLENBQUMsR0FBRyxFQUFFLFVBQVU7UUFDL0IsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxVQUFVLENBQUMsU0FBUztRQUNoQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO0lBQ3RDLENBQUM7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQztJQUVELGNBQWM7UUFDVixPQUFPLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsQ0FBQztJQUNwRSxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsT0FBTyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQztJQUM5RSxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLE9BQU8scUJBQXFCLENBQUMsc0JBQXNCLENBQUMsY0FBYyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsVUFBVTtRQUM5QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sV0FBVyxHQUFHO2dCQUNoQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO2lCQUMxRixDQUFDO2FBQ0wsQ0FBQztZQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxFQUFFLFdBQVcsQ0FBQztpQkFDeEQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxZQUFZO1FBQzFELE1BQU0sV0FBVyxHQUFHO1lBQ2hCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDckIsY0FBYyxFQUFFLFdBQVc7YUFDOUIsQ0FBQztTQUNMLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsRUFBRSxVQUFVLEVBQUUsWUFBWSxDQUFDO2lCQUN2RSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRCQUE0QixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsV0FBVztRQUNyRCxNQUFNLFdBQVcsR0FBRztZQUNoQixlQUFlLEVBQUUsSUFBSTtZQUNyQixZQUFZLEVBQUUsTUFBZ0I7WUFDOUIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixjQUFjLEVBQUUsV0FBVzthQUM5QixDQUFDO1NBQ0wsQ0FBQztRQUVGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUM7aUJBQ3ZDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVc7UUFDdEQsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDdkUsTUFBTSxPQUFPLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUNyRixJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO1lBQ2xELE9BQU87U0FDVjtRQUNELE1BQU0sWUFBWSxHQUFHLE9BQU8sR0FBRyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsa0JBQWtCLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUM7UUFFdkgsTUFBTSxXQUFXLEdBQUc7WUFDaEIsZUFBZSxFQUFFLElBQUk7WUFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixjQUFjLEVBQUUsV0FBVztnQkFDM0IsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTthQUMxRixDQUFDO1NBQ0wsQ0FBQztRQUVGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUM7aUJBQ2hELFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxHQUFHLEVBQUUsV0FBWTtRQUNuQyxXQUFXLEdBQUc7WUFDVixlQUFlLEVBQUUsSUFBSTtZQUNyQixZQUFZLEVBQUUsTUFBZ0I7WUFDOUIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO2FBQzFGLENBQUM7U0FDTCxDQUFDO1FBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsV0FBVyxDQUFDO2lCQUM1RCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFvQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFlBQVk7UUFDM0QsTUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDbEgsR0FBRyxDQUFDLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQztZQUM1QyxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQztpQkFDdkUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxZQUF5QjtRQUN0RSxJQUFJLE9BQXdCLENBQUM7UUFDN0IsTUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDbEgsR0FBRyxDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQztZQUM3RCxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBQ0YsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUM5RSxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDM0IsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNULElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRTtvQkFDaEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsU0FBUyxDQUFDLEtBQUs7UUFDWCxNQUFNLElBQUksR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ3pCLE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLEtBQUssTUFBTSxNQUFNLElBQUksT0FBTyxFQUFFO1lBQzFCLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQztZQUNmLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7Z0JBQ3hCLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3RCO1lBQ0QsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDdkIsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzdDO1NBQ0o7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxjQUFjLENBQUMsVUFBVTtRQUNyQixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDaEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssTUFBTSxFQUFFO2dCQUNqQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDM0c7aUJBQU07Z0JBQ0gsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQzFIO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsV0FBVyxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsV0FBWTtRQUMxQyxNQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1FBRXJGLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtZQUN0QixPQUFPLE9BQU8sR0FBRyxVQUFVLENBQUM7U0FDL0I7YUFBTTtZQUNILE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO2dCQUNsRCxPQUFPO2FBQ1Y7WUFDRCxPQUFPLE9BQU8sR0FBRyxxQkFBcUIsQ0FBQyxjQUFjO2dCQUNqRCxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxVQUFVO2dCQUN6RSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQy9EO0lBQ0wsQ0FBQztJQUVELGFBQWEsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxjQUFjO1FBQ3ZELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxZQUFZLElBQUksWUFBWSxJQUFJLElBQUksRUFBRTtnQkFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQy9ELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxNQUFNLGFBQWEsR0FBRztvQkFDbEIsZUFBZSxFQUFFLElBQUk7b0JBQ3JCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDckIsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTtxQkFDMUYsQ0FBQztpQkFDTCxDQUFDO2dCQUVGLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNoRSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsY0FBYztRQUN0RCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksWUFBWSxFQUFFO2dCQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsWUFBWSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM5RCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsTUFBTSxXQUFXLEdBQUc7b0JBQ2hCLGVBQWUsRUFBRSxJQUFJO29CQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3JCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7cUJBQzFGLENBQUM7aUJBQ0wsQ0FBQztnQkFDRixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQztxQkFDdEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxZQUFZO1FBRVIsTUFBTSxNQUFNLEdBQUcsQ0FBQztnQkFFWixHQUFHLEVBQUUsb0JBQW9CO2dCQUN6QixLQUFLLEVBQUUsTUFBTTthQUNoQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxNQUFNO2dCQUNYLEtBQUssRUFBRSw2QkFBNkI7YUFDdkMsRUFBRTtnQkFDQyxHQUFHLEVBQUUsa0JBQWtCO2dCQUN2QixLQUFLLEVBQUUsTUFBTTthQUNoQixFQUFFO2dCQUNDLEdBQUcsRUFBRSx3QkFBd0I7Z0JBQzdCLEtBQUssRUFBRSxPQUFPO2FBQ2pCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLE9BQU87Z0JBQ1osS0FBSyxFQUFFLE1BQU07YUFDaEIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7YUFDckIsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0I7Z0JBQy9FLFFBQVEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMscUJBQXFCO2dCQUMvRCxRQUFRLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixHQUFHLENBQUMsRUFBRTtnQkFDckUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQ3RDO2lCQUFNO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDM0I7UUFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7WUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBR0QsZ0JBQWdCLENBQUMsUUFBUyxFQUFFLFdBQVk7UUFDcEMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixVQUFVO1lBQ1YsbUJBQW1CO1lBQ2YsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUM7aUJBQzlELFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO2dCQUM1QixJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLGdCQUFnQixJQUFJLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUU7b0JBQzdHLFVBQVU7b0JBQ1YsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDO29CQUM3QyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztpQkFDL0c7WUFDTCxDQUFDLEVBQUUsZUFBZSxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxlQUFlO3FCQUN0QyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7WUFDWCx1QkFBdUI7WUFDbkIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUztnQkFDckQscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxFQUFFO2dCQUM1RCw0Q0FBNEM7Z0JBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQztxQkFDcEYsU0FBUyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7b0JBQzVCLDRDQUE0QztvQkFDNUMsVUFBVTtvQkFDVixJQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7b0JBQzdDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsZ0NBQWdDLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDO3lCQUN2RyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxFQUFFLGVBQWUsQ0FBQyxFQUFFO29CQUNqQixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7b0JBQ2pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7b0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUN2QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7WUFDTCxJQUFJO1FBQ1IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsTUFBTTtRQUN6QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsTUFBTSxFQUFFLElBQUksQ0FBQztpQkFDdkUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFO29CQUNuRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7b0JBQ3RFLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDM0MsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLDREQUE0RCxDQUFDLENBQUM7aUJBQ2hGO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxhQUFhLEVBQUUsU0FBUztRQUMzQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDdEQsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDN0UsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3ZLLElBQUksR0FBRyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN6RSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDM0osSUFBSSxHQUFHLElBQUksQ0FBQzthQUNmO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsZ0JBQWdCLENBQUMsWUFBYTtRQUMxQixNQUFNLFVBQVUsR0FBRztZQUNmLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQjtZQUM1QyxtRUFBbUU7WUFDbkUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLDJCQUEyQjtZQUMzRixrQkFBa0IsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsZ0JBQWdCO1lBQ2hGLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7UUFHRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDO2lCQUMzQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTtnQkFDNUIsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFDO2dCQUMzQixNQUFNLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztnQkFFdkUsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO29CQUNmLE1BQU0sSUFBSSxLQUFLLENBQUM7b0JBRWhCLElBQUksT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7b0JBQ25GLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7b0JBQ3BELHNCQUFzQjtvQkFDdEIsSUFBSSxDQUFDLDRCQUE0QixDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQzt5QkFDNUYsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7d0JBQzFCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDOzZCQUM5RCxTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTs0QkFDNUIscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYTtnQ0FDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUM7b0NBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDO29DQUM3RCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7b0NBQ3JFLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29DQUMxRSxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQzVELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDO2dDQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQztnQ0FDN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO2dDQUNyRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQ0FDbEYsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUNoRSxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQztnQ0FDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7Z0NBQzdELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztnQ0FDckUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0NBQy9FLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFFN0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sQ0FBQztpQ0FDOUIsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dDQUN0QixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBQyxTQUFTLENBQUMsQ0FBQztnQ0FDdEYsSUFBSSxDQUFDLFlBQVksRUFBRTtvQ0FDZixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO29DQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7b0NBQ3JELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDdkI7cUNBQU07b0NBQ0gsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztvQ0FDOUIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUzt3Q0FDckQscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxJQUFJLFlBQVksRUFBRSxFQUFFLHlCQUF5Qjt3Q0FDdkcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDOzZDQUNwRixTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTs0Q0FDNUIsNENBQTRDOzRDQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxnQkFBZ0IsQ0FBQztpREFDN0YsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dEQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dEQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NENBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnREFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dEQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NENBQ3hCLENBQUMsQ0FBQyxDQUFDO3dDQUNYLENBQUMsRUFBRSxlQUFlLENBQUMsRUFBRTs0Q0FDakIsNENBQTRDOzRDQUM1QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7NENBQ2pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7NENBQ3RDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3Q0FDeEIsQ0FBQyxDQUFDLENBQUM7d0NBQ1Asd0NBQXdDO3dDQUN4QyxzQkFBc0I7cUNBQ3pCO3lDQUFNO3dDQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7d0NBQ3RDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQ0FDdkI7aUNBQ0o7NEJBQ0wsQ0FBQyxFQUFFLFlBQVksQ0FBQyxFQUFFO2dDQUNkLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7NEJBQ2pDLENBQUMsQ0FBQyxDQUFDO3dCQUNYLENBQUMsRUFBRSxlQUFlLENBQUMsRUFBRTs0QkFDakIsUUFBUSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDcEMsQ0FBQyxDQUFDLENBQUM7b0JBQ1gsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxFQUFFO3dCQUNmLFFBQVEsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO29CQUNQLEtBQUs7aUJBQ1I7WUFDTCxDQUFDLEVBQUUsZUFBZSxDQUFDLEVBQUU7Z0JBQ2pCLFFBQVEsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0I7UUFDZCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDaEUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztpQkFDM0QsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQztZQUN6RCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGVBQWUsQ0FBQyxNQUFNO1FBQ2xCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pGLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJO29CQUNqRSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQzNDLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7aUJBQ3ZEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3ZCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSztRQUNuQixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsb0JBQW9CLENBQUMsS0FBSztRQUN0QixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUNwQixjQUFjLEdBQUc7b0JBQ2IsZUFBZSxFQUFFO3dCQUNiLFlBQVksRUFBRSxDQUFDO2dDQUNYLElBQUksRUFBRTtvQ0FDRixTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0NBQ3hCLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtpQ0FDM0I7NkJBQ0osQ0FBQztxQkFDTDtpQkFDSixDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsTUFBTSxXQUFXLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztnQkFDaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ25DLFdBQVcsQ0FBQyxJQUFJLENBQUM7d0JBQ2IsSUFBSSxFQUFFOzRCQUNGLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs0QkFDeEIsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO3lCQUMzQjtxQkFDSixDQUFDLENBQUM7aUJBQ047Z0JBQ0QsY0FBYyxHQUFHO29CQUNiLGVBQWUsRUFBRTt3QkFDYixZQUFZLEVBQUUsV0FBVztxQkFDNUI7aUJBQ0osQ0FBQzthQUNMO1NBQ0o7YUFBTTtZQUNILE1BQU0scUZBQXFGLENBQUM7U0FDL0Y7UUFDRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBRUQsK0JBQStCLENBQUMsS0FBSztRQUNqQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxLQUFLLEVBQUU7WUFDUCxjQUFjLEdBQUc7Z0JBQ2IsZUFBZSxFQUFFO29CQUNiLFlBQVksRUFBRSxDQUFDOzRCQUNYLElBQUksRUFBRTtnQ0FDRixTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7NkJBQzNCOzRCQUNELE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTzt5QkFDM0IsQ0FBQztpQkFDTDthQUNKLENBQUM7U0FDTDthQUFNO1lBQ0gsTUFBTSxXQUFXLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztZQUNoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsV0FBVyxDQUFDLElBQUksQ0FBQztvQkFDYixJQUFJLEVBQUU7d0JBQ0YsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO3FCQUMzQjtvQkFDRCxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU87aUJBQzNCLENBQUMsQ0FBQzthQUNOO1lBQ0QsY0FBYyxHQUFHO2dCQUNiLGVBQWUsRUFBRTtvQkFDYixZQUFZLEVBQUUsV0FBVztpQkFDNUI7YUFDSixDQUFDO1NBQ0w7UUFDRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBRUQscUJBQXFCLENBQUMsTUFBYyxFQUFFLEtBQWlCO1FBQ25ELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQztRQUUzQixNQUFNLGlCQUFpQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO2dCQUNuQixjQUFjLEVBQUUsRUFBRTtnQkFDbEIsZ0JBQWdCLEVBQUUsRUFBRTthQUN2QixDQUFDLENBQUM7U0FDTjtRQUNELGVBQWUsR0FBRztZQUNkLHNCQUFzQixFQUFFO2dCQUNwQixtQkFBbUIsRUFBRSxpQkFBaUI7Z0JBQ3RDLGVBQWUsRUFBRSxNQUFNO2FBQzFCO1NBQ0osQ0FBQztRQUVGLE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxjQUFjLEVBQUUsbUJBQW1CLEVBQUUsYUFBYTtRQUNyRSxPQUFPLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxjQUFjLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3JHLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyxjQUFjLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLGdCQUFnQjtRQUMxRixJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUMvQixNQUFNLGlCQUFpQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDdEMsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzNCLE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTVCLElBQUksY0FBYyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksYUFBYSxJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsRUFBRTtZQUVuSCxNQUFNLGtCQUFrQixHQUFHLENBQUMsQ0FBQztZQUM3QixNQUFNLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQztZQUMxQyxJQUFJLGdCQUFnQixJQUFJLGdCQUFnQixJQUFJLElBQUksSUFBSSxnQkFBZ0IsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pFLE1BQU0sR0FBRyxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ2pDLE1BQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDbkIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdkIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNsQzthQUNKO2lCQUFNO2dCQUNILElBQUksTUFBTSxHQUFHO29CQUNULEVBQUUsRUFBRSxrQkFBa0I7aUJBQ3pCLENBQUM7Z0JBQ0YsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QixNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ2pCO1lBQ0QsdUdBQXVHO1lBQ3ZHLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM1QyxlQUFlLEdBQUcsRUFBRSxDQUFDO2dCQUNyQixlQUFlLENBQUMsRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzFDLElBQUksSUFBSSxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUM3QyxJQUFJLEdBQUcsb0NBQW9DLENBQUM7aUJBQy9DO2dCQUNELGVBQWUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUM1QiwwSEFBMEg7Z0JBQzFILElBQUksbUJBQW1CLElBQUksbUJBQW1CLElBQUksSUFBSSxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO29CQUNoRixjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLEVBQUUsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxJQUFJLElBQUk7b0JBQ3ZFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBRTFHLElBQUksYUFBYSxHQUFHLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDcEUsK0JBQStCO29CQUMvQixJQUFJLElBQUksS0FBSyx5Q0FBeUMsRUFBRTt3QkFDcEQsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO3dCQUNsQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQzFELEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dDQUN2RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVEsRUFBRTtvQ0FDakcsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDMUQ7Z0NBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7b0NBQ2pHLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7aUNBQzFEO2dDQUNELE1BQU0sS0FBSyxHQUFHO29DQUNWLEtBQUssRUFBRTt3Q0FDSCxLQUFLLEVBQUUsYUFBYSxDQUFDLEtBQUssQ0FBQzt3Q0FDM0IsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO3FDQUNuQztpQ0FDSixDQUFDO2dDQUVGLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NkJBQ3RCO3lCQUNKOzZCQUFNOzRCQUNILElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUTtnQ0FDL0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0NBQ3pDLGFBQWEsR0FBRyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7NkJBQzVDO2lDQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUTttQ0FDbkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0NBQzVDLGFBQWEsR0FBRyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7NkJBQzVDOzRCQUNELE1BQU0sS0FBSyxHQUFHO2dDQUNWLEtBQUssRUFBRTtvQ0FDSCxLQUFLLEVBQUUsYUFBYTtvQ0FDcEIsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO2lDQUNuQzs2QkFDSixDQUFDOzRCQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ3RCO3dCQUNELGVBQWUsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO3FCQUNuQzt5QkFBTTt3QkFDSCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVE7NEJBQy9GLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFOzRCQUN6QyxhQUFhLEdBQUcsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUM1Qzs2QkFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVE7K0JBQ25HLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFOzRCQUM1QyxhQUFhLEdBQUcsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUM1Qzs2QkFBTTt5QkFFTjt3QkFDRCxNQUFNLEtBQUssR0FBRzs0QkFDVixLQUFLLEVBQUU7Z0NBQ0gsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTs2QkFDbkM7eUJBQ0osQ0FBQzt3QkFDRixlQUFlLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztxQkFDakM7aUJBQ0o7cUJBQU0sSUFBSSxtQkFBbUIsSUFBSSxtQkFBbUIsSUFBSSxJQUFJLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7b0JBQ3ZGLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssRUFBRSxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksSUFBSTtvQkFDdkUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDMUcsTUFBTSxNQUFNLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JELE1BQU0sS0FBSyxHQUFHO3dCQUNWLEtBQUssRUFBRTs0QkFDSCxLQUFLLEVBQUUsRUFBRTs0QkFDVCxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7eUJBQ25DO3FCQUNKLENBQUM7b0JBRUYsNkJBQTZCO29CQUM3QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDcEMsSUFBSSxhQUFhLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ25ELDJDQUEyQzt3QkFDM0MsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFOzRCQUM5QixhQUFhLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUNwQzt3QkFDRCwrQkFBK0I7d0JBQy9CLElBQUksYUFBYSxLQUFLLEVBQUUsSUFBSSxhQUFhLElBQUksSUFBSSxJQUFJLGFBQWEsRUFBRTs0QkFDaEUsb0VBQW9FOzRCQUNwRSxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7Z0NBQzVFLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUM7NkJBQy9EO2lDQUFNO2dDQUNILEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzs2QkFDckM7eUJBQ0o7cUJBQ0o7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBSSxRQUFRLENBQUM7b0JBQ2IsOEJBQThCO29CQUM5QixJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssRUFBRSxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksSUFBSSxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUU7d0JBQ3BHLFFBQVEsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO3FCQUN4Qzt5QkFBTTt3QkFDSCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVEsRUFBRTs0QkFDakcsUUFBUSxHQUFHLElBQUksQ0FBQzt5QkFDbkI7NkJBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7NEJBQ3hHLFFBQVEsR0FBRyxDQUFDLENBQUM7eUJBQ2hCOzZCQUFNO3lCQUVOO3FCQUNKO29CQUNELE1BQU0sS0FBSyxHQUFHO3dCQUNWLEtBQUssRUFBRTs0QkFDSCxLQUFLLEVBQUUsUUFBUTs0QkFDZixJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7eUJBQ25DO3FCQUNKLENBQUM7b0JBQ0YsZUFBZSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7aUJBQ2pDO2dCQUNELGlCQUFpQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUMzQztZQUVELElBQUksZUFBZSxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLGFBQWEsSUFBSSxhQUFhLElBQUksSUFBSSxJQUFJLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ3ZFLGVBQWUsR0FBRyxhQUFhLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0gsTUFBTSw4QkFBOEIsQ0FBQzthQUN4QztZQUNELG1CQUFtQixHQUFHO2dCQUNsQixjQUFjLEVBQUU7b0JBQ1osS0FBSyxFQUFFO3dCQUNILFFBQVEsRUFBRTs0QkFDTixxQkFBcUIsRUFBRSxpQkFBaUI7eUJBQzNDO3dCQUNELGlCQUFpQixFQUFFLGVBQWU7d0JBQ2xDLG9CQUFvQixFQUFFLGdCQUFnQjtxQkFDekM7aUJBQ0o7YUFDSixDQUFDO1NBQ0w7YUFBTTtZQUNILE1BQU0sdUNBQXVDO2tCQUMzQyxvRkFBb0YsQ0FBQztTQUMxRjtRQUNELE9BQU8sbUJBQW1CLENBQUM7SUFDL0IsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLLEVBQUUsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGVBQWUsRUFBRSxnQkFBZ0IsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCO1FBQ3ZILE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMzRSxNQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxLQUFLLElBQUksS0FBSyxJQUFJLElBQUksSUFBSSxtQkFBbUIsSUFBSSxtQkFBbUIsSUFBSSxJQUFJLElBQUksY0FBYyxJQUFJLGVBQWU7WUFDakgsZ0JBQWdCLEVBQUU7WUFFbEIsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQy9ILElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN2QixJQUFJLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFFBQVE7YUFDcEQsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMzQyxLQUFLLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ2xELEtBQUssQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztZQUNyRCxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNwQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUMzRCxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDdkIsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO2FBQ3BELENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQy9CLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO2dCQUN0QixLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUMvQjtTQUNKO2FBQU07WUFDSCxNQUFNLGtGQUFrRixDQUFDO1lBQ3pGLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsdUJBQXVCLENBQUMsUUFBUSxFQUFFLGFBQWEsRUFBRSxnQkFBZ0I7UUFDN0QsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDL0IsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBQ3hDLE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTVCLElBQUksUUFBUSxJQUFJLGFBQWEsSUFBSSxhQUFhLElBQUksSUFBSSxJQUFJLGFBQWEsS0FBSyxFQUFFLEVBQUU7WUFFNUUsTUFBTSxrQkFBa0IsR0FBRyxDQUFDLENBQUM7WUFDN0IsSUFBSSxnQkFBZ0IsSUFBSSxnQkFBZ0IsSUFBSSxJQUFJLElBQUksZ0JBQWdCLEtBQUssRUFBRSxFQUFFO2dCQUN6RSxNQUFNLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3hDLEtBQUssTUFBTSxFQUFFLElBQUksR0FBRyxFQUFFO29CQUNsQixNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzNCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDbEM7YUFDSjtpQkFBTTtnQkFDSCxJQUFJLE1BQU0sR0FBRztvQkFDVCxFQUFFLEVBQUUsa0JBQWtCO2lCQUN6QixDQUFDO2dCQUNGLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDOUIsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNqQjtZQUVELElBQUksZUFBZSxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLGFBQWEsSUFBSSxhQUFhLElBQUksSUFBSSxJQUFJLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ3ZFLGVBQWUsR0FBRyxhQUFhLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0gsTUFBTSxJQUFJLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO2FBQ25EO1lBRUQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLHFCQUFxQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0JBQzdGLEtBQUssTUFBTSxhQUFhLElBQUksUUFBUSxDQUFDLHFCQUFxQixFQUFFO29CQUN4RCxLQUFLLE1BQU0sZUFBZSxJQUFJLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRTt3QkFDL0QsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLEVBQUUsS0FBSyxxQkFBcUIsSUFBSSxlQUFlLENBQUMsS0FBSyxJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFOzRCQUN6SCxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO3lCQUM5RDt3QkFDRCxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7cUJBQzdDO2lCQUNKO2FBQ0o7WUFFRCxtQkFBbUIsR0FBRztnQkFDbEIsY0FBYyxFQUFFO29CQUNaLEtBQUssRUFBRTt3QkFDSCxRQUFRLEVBQUU7NEJBQ04scUJBQXFCLEVBQUUsbUJBQW1CO3lCQUM3Qzt3QkFDRCxpQkFBaUIsRUFBRSxlQUFlO3dCQUNsQyxvQkFBb0IsRUFBRSxnQkFBZ0I7cUJBQ3pDO2lCQUNKO2FBQ0osQ0FBQztTQUNMO2FBQU07WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDOzhGQUNvRSxDQUFDLENBQUM7U0FDdkY7UUFDRCxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxjQUFjO1FBQ3pHLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMzRSxNQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CLElBQUksY0FBYyxJQUFJLGdCQUFnQixFQUFFO1lBQzlGLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO2dCQUN0QixPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNHLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUN2QixJQUFJLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFFBQVE7aUJBQ3BELENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxLQUFLLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLGNBQWMsQ0FBQyxDQUFDO2dCQUNqRCxLQUFLLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3JELEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNwQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ3ZCLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsUUFBUTtpQkFDcEQsQ0FBQyxDQUFDO2dCQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMvQixLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUMvQjtZQUVELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUN0RDthQUFNO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxxRkFBcUYsQ0FBQyxDQUFDO1lBQ3JHLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsYUFBYSxDQUFDLFFBQWdCO1FBQzFCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxPQUFPLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztZQUNyRixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsVUFBVSxDQUFDO1lBQ3ZFLE1BQU0sR0FBRyxHQUFHLE9BQU8sR0FBRyxVQUFVLEdBQUMsT0FBTyxHQUFDLGVBQWUsQ0FBQztZQUV6RCxNQUFNLFdBQVcsR0FBRztnQkFDaEIsZUFBZSxFQUFFLElBQUk7Z0JBQ3JCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztvQkFDckIsY0FBYyxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxpQkFBaUI7b0JBQ3BFLGdCQUFnQixFQUFFLE1BQU07b0JBQ3hCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7b0JBQ3ZGLGVBQWUsRUFBRSxPQUFPO2lCQUMzQixDQUFDO2FBQ0wsQ0FBQztZQUVGLE1BQU0sUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDaEMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFFdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsRUFBRSxXQUFXLENBQUM7aUJBQ3JDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYSxDQUFDLEtBQUssRUFBRSxjQUFjLEVBQUUsbUJBQW1CLEVBQUUsZUFBZSxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFDdEcsY0FBYyxFQUFFLFdBQVcsRUFBRSxVQUFXO1FBRXhDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLEdBQUcsR0FBRyxHQUFHLFdBQVcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDcEosTUFBTSxLQUFLLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBRW5CLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLG1CQUFtQjtZQUN0RCxlQUFlLElBQUksZ0JBQWdCLEVBQUU7WUFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25DLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsRUFBRSxhQUFhLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDM0csSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ3ZCLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsUUFBUTtpQkFDcEQsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ2IsS0FBSyxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDM0MsS0FBSyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxlQUFlLENBQUMsQ0FBQztpQkFDckQ7Z0JBQ0QsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQzNDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFekMsSUFBSSxVQUFVLEVBQUU7b0JBQ1osT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7aUJBQ3pFO3FCQUFNO29CQUNILE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2lCQUM5RDtnQkFDRCxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTtvQkFDdkIsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO2lCQUNwRCxDQUFDLENBQUM7Z0JBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLElBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFNBQVMsSUFBSSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEVBQUM7b0JBQ25ILE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDbkUsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ3ZCLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsUUFBUTtxQkFDcEQsQ0FBQyxDQUFDO29CQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQzFDO3FCQUFJO29CQUNELDhEQUE4RDtvQkFDOUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2xDO2FBRUo7WUFDRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDckQ7YUFBTTtZQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMscUZBQXFGLENBQUMsQ0FBQztZQUNyRyxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELHNCQUFzQixDQUFDLFNBQVM7UUFDNUIsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNwQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssUUFBUTtnQkFDckMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtnQkFDdkYsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNQLFFBQVEsSUFBSSxHQUFHLENBQUM7aUJBQ25CO2dCQUNELFFBQVEsSUFBSSxXQUFXLEdBQUcsR0FBRyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixDQUFDO2dCQUNuSCxRQUFRLElBQUksSUFBSSxDQUFDO2FBQ3BCO1NBQ0o7UUFDRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7bUdBQ3lELEdBQUcsUUFBUSxDQUFDLENBQUM7WUFDcEcsT0FBTyxLQUFLLENBQUM7U0FDaEI7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsYUFBYSxDQUFDLFFBQXVCO1FBQ2pDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3hELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMseUNBQXlDLENBQUMsQ0FBQztnQkFDekUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1lBRUQsTUFBTSxRQUFRLEdBQUcsQ0FBQztvQkFDZCxHQUFHLEVBQUUsUUFBUTtvQkFDYixLQUFLLEVBQUUsV0FBVztpQkFDckIsRUFBRTtvQkFDQyxHQUFHLEVBQUUsTUFBTTtvQkFDWCxLQUFLLEVBQUU7d0JBQ0gsR0FBRyxFQUFFLG1CQUFtQjt3QkFDeEIsS0FBSyxFQUFFOzRCQUNILHVCQUF1QixFQUFFO2dDQUNyQixpQkFBaUIsRUFBRTtvQ0FDZixTQUFTLEVBQUUsUUFBUTtvQ0FDbkIsSUFBSSxFQUFFLHNEQUFzRDtvQ0FDNUQsbUJBQW1CLEVBQUUsV0FBVztpQ0FDbkM7NkJBQ0o7eUJBQ0o7cUJBQ0o7aUJBQ0osQ0FBQyxDQUFDO1lBRUgsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzFELE1BQU0sV0FBVyxHQUFHO2dCQUNoQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO29CQUN2RixjQUFjLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFVBQVU7aUJBQ2hFLENBQUM7Z0JBQ0YsZ0JBQWdCLEVBQUUsb0JBQW9CO2FBQ3pDLENBQUM7WUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDO2lCQUNqRixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQztvQkFDeEQsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCO29CQUM5RCxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDaEUsSUFBSSxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0I7d0JBQ2pGLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLE1BQU07d0JBQ3hGLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBRTlGLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsc0JBQXNCOzRCQUMzQyxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQztxQkFFL0c7eUJBQU0sSUFBSSxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0I7d0JBQzVGLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLE1BQU07d0JBQzVGLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLE1BQU0sR0FBRyxDQUFDO3dCQUNoRyw0Q0FBNEM7d0JBQzVDLFFBQVEsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsTUFBTSxFQUFFO3dCQUNsSCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUJBQ3ZCO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsc0JBQXNCLEdBQUcsUUFBUSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO3FCQUNwRjtpQkFDSjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztpQkFDbkQ7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFVBQVUsQ0FBQyxRQUF1QjtRQUM5QixNQUFNLFFBQVEsR0FBRyxDQUFDO2dCQUNkLEdBQUcsRUFBRSxRQUFRO2dCQUNiLEtBQUssRUFBRSxNQUFNO2FBQ2hCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLE1BQU07Z0JBQ1gsS0FBSyxFQUFFO29CQUNILEdBQUcsRUFBRSxtQkFBbUI7b0JBQ3hCLEtBQUssRUFBRTt3QkFDSCx1QkFBdUIsRUFBRTs0QkFDckIsaUJBQWlCLEVBQUU7Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLElBQUksRUFBRSxzREFBc0Q7Z0NBQzVELG1CQUFtQixFQUFFLE1BQU07NkJBQzlCO3lCQUNKO3FCQUNKO2lCQUNKO2FBQ0osQ0FBQyxDQUFDO1FBQ0gsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzFELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEUsZUFBZSxFQUFFLElBQUk7WUFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO2dCQUN2RixjQUFjLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFVBQVU7YUFDaEUsQ0FBQztTQUNMLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDZCxDQUFDO0lBRUQsV0FBVyxDQUFDLFFBQVE7UUFDaEIsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlFLElBQUksS0FBSyxDQUFDO1FBQ1YsSUFBSSxRQUFRLEVBQUU7WUFDVixLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUN6QixHQUFHLEVBQUUsV0FBVztvQkFDaEIsS0FBSyxFQUFFLFFBQVE7aUJBQ2xCO2FBQ0EsQ0FBQyxDQUFDO1NBQ047UUFDRCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJLEVBQUUsV0FBVztRQUM1QixNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDL0UsTUFBTSxLQUFLLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM3QixLQUFLLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMzQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzQixLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxlQUFlLENBQUMsS0FBSyxFQUFFLFdBQVc7UUFDOUIsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLEtBQUssTUFBTSxJQUFJLElBQUksS0FBSyxFQUFFO1lBQ3RCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQztTQUN6RDtRQUNELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsWUFBWSxDQUFDLFdBQVcsRUFBRSxXQUFXO1FBQ2pDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsWUFBWSxHQUFHLEdBQUcsR0FBRyxXQUFXLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2xHLE1BQU0sS0FBSyxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7UUFDN0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQztRQUNuQixLQUFLLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMzQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDMUIsY0FBYyxHQUFHO1lBQ2IsZUFBZSxFQUFFO2dCQUNiLFlBQVksRUFBRSxDQUFDO3dCQUNYLElBQUksRUFBRTs0QkFDRixTQUFTLEVBQUUsV0FBVyxDQUFDLElBQUk7eUJBQzlCO3dCQUNELE1BQU0sRUFBRSxXQUFXLENBQUMsT0FBTztxQkFDOUIsQ0FBQzthQUNMO1NBQ0osQ0FBQztRQUNGLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QyxPQUFPLEdBQUcsY0FBYyxDQUFDO1FBQ3pCLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRTtZQUN2QyxJQUFJLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFFBQVE7U0FDcEQsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxZQUFZLENBQUMsS0FBaUI7UUFDMUIsTUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtnQkFDZixTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqQztTQUNKO1FBRUQsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELGdCQUFnQixDQUFDLFlBQWE7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFDLFlBQVksQ0FBQSxDQUFDLENBQUEsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUM3SCxPQUFPLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUMsWUFBWSxDQUFBLENBQUMsQ0FBQSxNQUFNLENBQUMsQ0FBQztJQUMzSCxDQUFDO0lBRUQsZUFBZSxDQUFDLFFBQW9CLEVBQUUsWUFBb0I7UUFDdEQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1lBQ3JGLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDdkUsTUFBTSxHQUFHLEdBQUcsT0FBTyxHQUFHLFVBQVUsR0FBQyxPQUFPLEdBQUMsZUFBZSxDQUFDO1lBRXpELE1BQU0sV0FBVyxHQUFHO2dCQUNoQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUNyQixjQUFjLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFVBQVU7b0JBQzdELGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7aUJBQzFGLENBQUM7YUFDTCxDQUFDO1lBQ0YsSUFBSSxhQUFhLENBQUM7WUFDbEIsSUFBRyxZQUFZLENBQUMsV0FBVyxFQUFFLElBQUksU0FBUyxDQUFDLEdBQUcsRUFBQztnQkFDOUMsYUFBYSxHQUFHO29CQUNiLG9CQUFvQixFQUFFO3dCQUNsQixjQUFjLEVBQUU7NEJBQ1osZUFBZSxFQUFFO2dDQUNiLHNCQUFzQixFQUFFLEtBQUs7Z0NBQzdCLHFCQUFxQixFQUFFLElBQUk7Z0NBQzNCLHlCQUF5QixFQUFFLEtBQUs7NkJBQ25DOzRCQUNELHlCQUF5QixFQUFFLEtBQUs7NEJBQ2hDLGlCQUFpQixFQUFFO2dDQUNmLGdCQUFnQixFQUFFO29DQUNkO3dDQUNJLGVBQWUsRUFBRSxDQUFDO3dDQUNsQixLQUFLLEVBQUUsR0FBRztxQ0FDYjtvQ0FDRDt3Q0FDSSxlQUFlLEVBQUUsQ0FBQzt3Q0FDbEIsS0FBSyxFQUFFLEdBQUc7cUNBQ2I7aUNBQ0o7Z0NBQ0QsRUFBRSxFQUFFLDhDQUE4QztnQ0FDbEQsY0FBYyxFQUFFLHNDQUFzQzs2QkFDekQ7NEJBQ0QscUJBQXFCLEVBQUUsa0JBQWtCOzRCQUN6Qyx1QkFBdUIsRUFBRTtnQ0FDckI7b0NBQ0ksY0FBYyxFQUFFLDZDQUE2QztvQ0FDN0QsU0FBUyxFQUFFO3dDQUNQLElBQUk7d0NBQ0osSUFBSSxDQUFDLGdCQUFnQixFQUFFO3FDQUMxQjtpQ0FDSjs2QkFDSjs0QkFDRCxzQkFBc0IsRUFBRSxJQUFJOzRCQUM1Qix3QkFBd0IsRUFBRSxJQUFJOzRCQUM5QixrQkFBa0IsRUFBRSxJQUFJOzRCQUN4QixTQUFTLEVBQUUsSUFBSTt5QkFDbEI7cUJBQ0o7aUJBQ0osQ0FBQzthQUNEO2lCQUFJO2dCQUNELGFBQWEsR0FBRztvQkFDWixvQkFBb0IsRUFBRTt3QkFDbEIsY0FBYyxFQUFFOzRCQUNaLGVBQWUsRUFBRTtnQ0FDYixxQkFBcUIsRUFBRSxJQUFJO2dDQUMzQix5QkFBeUIsRUFBRSxLQUFLOzZCQUNuQzs0QkFDRCx5QkFBeUIsRUFBRSxLQUFLOzRCQUNoQyxpQkFBaUIsRUFBRTtnQ0FDZixnQkFBZ0IsRUFBRTtvQ0FDZDt3Q0FDSSxlQUFlLEVBQUUsQ0FBQzt3Q0FDbEIsS0FBSyxFQUFFLEdBQUc7cUNBQ2I7aUNBQ0o7Z0NBQ0QsRUFBRSxFQUFFLDhDQUE4QztnQ0FDbEQsY0FBYyxFQUFFLHNDQUFzQzs2QkFDekQ7NEJBQ0QscUJBQXFCLEVBQUUsa0JBQWtCOzRCQUN6QyxzQkFBc0IsRUFBRSxJQUFJOzRCQUM1Qix3QkFBd0IsRUFBRSxJQUFJOzRCQUM5QixrQkFBa0IsRUFBRSxJQUFJOzRCQUN4QixTQUFTLEVBQUUsS0FBSzt5QkFDbkI7cUJBQ0o7aUJBQ0osQ0FBQzthQUNMO1lBQ0QsTUFBTSxnQkFBZ0IsR0FBRztnQkFDckIsdUJBQXVCLEVBQUU7b0JBQ3JCLGlCQUFpQixFQUFFO3dCQUNmLFNBQVMsRUFBRSxRQUFRO3dCQUNuQixJQUFJLEVBQUUsc0RBQXNEO3dCQUM1RCxtQkFBbUIsRUFBRSxLQUFLO3dCQUMxQixVQUFVLEVBQUUsUUFBUTtxQkFDdkI7aUJBQ0o7YUFDSixDQUFDO1lBRUYsTUFBTSxNQUFNLEdBQUcsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7Z0JBQzVELHFCQUFxQixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBRWpGLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDO2lCQUNuQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWEsQ0FBQyxNQUFXO1FBQ3JCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMscUJBQXFCO1lBQ3pDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDO2lCQUMvQixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFvQjtRQUVoQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLEVBQUc7Z0JBQ3pFLElBQUksV0FBVyxHQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBO2dCQUMzRixJQUFHLFdBQVcsSUFBRyxJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxVQUFVLENBQUMsV0FBVyxDQUFDLEdBQUUsQ0FBQyxFQUFDO29CQUM1RSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxFQUFFO3dCQUNuSSxJQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssMEJBQTBCLENBQUMsNkJBQTZCLENBQUMsYUFBYTs0QkFDOUYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtvQkFDeEYsQ0FBQyxDQUFDLENBQUE7aUJBQ047Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBRUgsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQztxQkFDcEUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixJQUFHLFFBQVEsSUFBRyxJQUFJLEVBQUM7d0JBQ2xCLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxFQUFFOzRCQUNsRSxJQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssMEJBQTBCLENBQUMsNkJBQTZCLENBQUMsYUFBYTtnQ0FDOUYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTt3QkFDeEYsQ0FBQyxDQUFDLENBQUE7d0JBQ0gsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO3dCQUN4RixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUJBQ25CO2dCQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0QsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFFBQVE7UUFDOUIsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLE1BQU0sZUFBZSxHQUFHO1lBQ3BCLGlCQUFpQixFQUFFO2dCQUNmLDhCQUE4QixFQUFFLElBQUk7Z0JBQ3BDLGtCQUFrQixFQUFFLElBQUk7Z0JBQ3hCLHVCQUF1QixFQUFFLElBQUk7Z0JBQzdCLGFBQWEsRUFBRSxJQUFJO2dCQUNuQix1QkFBdUIsRUFBRSxJQUFJO2dCQUM3QixtQkFBbUIsRUFBRSxJQUFJO2dCQUN6QixpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixxQkFBcUIsRUFBRSxJQUFJO2dCQUMzQixvQkFBb0IsRUFBRSxJQUFJO2dCQUMxQixzQkFBc0IsRUFBRSxJQUFJO2dCQUM1QixzQkFBc0IsRUFBRSxJQUFJO2dCQUM1QixTQUFTLEVBQUUsSUFBSTthQUNsQjtTQUNKLENBQUM7UUFDRixNQUFNLFFBQVEsR0FBRyxDQUFDO2dCQUNkLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixLQUFLLEVBQUUsUUFBUTthQUNsQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztRQUNILElBQUksUUFBUSxLQUFLLFFBQVEsRUFBRTtZQUN2QixRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUNWLEdBQUcsRUFBRSxtQkFBbUI7Z0JBQ3hCLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQzdELENBQUMsQ0FBQztTQUNOO1FBRUQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLEVBQUUsUUFBUSxDQUFDO2lCQUM3RSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFO29CQUMzQyxZQUFZLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUM7aUJBQ2hEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzVCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlCQUFpQixDQUFDLFFBQVE7UUFDdEIsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLE1BQU0sUUFBUSxHQUFHLENBQUM7Z0JBQ2QsR0FBRyxFQUFFLFdBQVc7Z0JBQ2hCLEtBQUssRUFBRSxVQUFVO2FBQ3BCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLDBCQUEwQjtnQkFDL0IsS0FBSyxFQUFFLE1BQU07YUFDaEIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsaUJBQWlCO2dCQUN0QixLQUFLLEVBQUUsTUFBTTthQUNoQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxPQUFPO2dCQUNaLEtBQUssRUFBRSxPQUFPO2FBQ2pCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLE9BQU87Z0JBQ1osS0FBSyxFQUFFLElBQUk7YUFDZCxDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLEdBQUcsUUFBUSxFQUFFLFFBQVEsQ0FBQztpQkFDekUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsZUFBZSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO29CQUN6RSxhQUFhLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7aUJBQ25EO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNCQUFzQjtRQUNsQixJQUFJLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM1QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDO2lCQUMvRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyx5QkFBeUIsSUFBSSxRQUFRLENBQUMseUJBQXlCLENBQUMsb0JBQW9CLEVBQUU7b0JBQzNHLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUNsQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQkFBc0I7UUFDbEIsSUFBSSxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDM0IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQztpQkFDOUQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsRUFBRTtvQkFDVixpQkFBaUIsR0FBRyxRQUFRLENBQUMsd0JBQXdCLENBQUM7aUJBQ3pEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDakMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsbUJBQW1CLENBQUMsT0FBTztRQUN2QixJQUFJLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUU5QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsT0FBTyxFQUFFLElBQUksQ0FBQztpQkFDeEUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsRUFBRTtvQkFDVixvQkFBb0IsR0FBRyxRQUFRLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDO2lCQUMxRTtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBQ3BDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFVBQVUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxhQUFhO1FBQzdDLE1BQU0sR0FBRyxHQUFHLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztRQUVqRDs7WUFFSTtRQUVKLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7WUFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUU7WUFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDcEY7UUFDRCxJQUFJLFlBQVksQ0FBQyxTQUFTLEtBQUssUUFBUSxFQUFFO1lBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsZ0RBQWdELEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUM7U0FDbko7UUFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUM5QixRQUFRLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxZQUFZLENBQUMsa0JBQWtCLEVBQUU7WUFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksWUFBWSxDQUFDLHNCQUFzQixFQUFFO1lBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMseUJBQXlCLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEg7UUFDRCxJQUFJLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3RFO1FBQ0QsSUFBSSxPQUFPLFlBQVksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFO1lBQy9DLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUM1RDtRQUNELElBQUksWUFBWSxDQUFDLElBQUksRUFBRTtZQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNsRTtRQUNELElBQUksWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztTQUNoRjtRQUNELElBQUksT0FBTyxZQUFZLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRTtZQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDeEQ7UUFDRCxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLGlEQUFpRCxDQUFDLENBQUMsQ0FBQztTQUNuSjtRQUNELElBQUksWUFBWSxDQUFDLGtCQUFrQixFQUFFO1lBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLFlBQVksQ0FBQyxxQkFBcUIsRUFBRTtZQUNwQyxRQUFRLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3BIO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNyRDtRQUVELE1BQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsTUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDckUsR0FBRyxDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQztZQUM3RCxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBQ0Y7O1lBRUk7UUFDSixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLG1DQUFtQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxhQUFhO1FBQ25ELElBQUksR0FBRyxHQUFHLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDO1FBQ25ELElBQUksWUFBWSxDQUFDLGFBQWEsRUFBRTtZQUM1QixHQUFHLEdBQUcscUJBQXFCLENBQUMsY0FBYyxDQUFDO1NBQzlDO1FBQ0QsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksWUFBWSxDQUFDLFNBQVMsRUFBRTtZQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN6QztRQUNELElBQUksWUFBWSxDQUFDLFVBQVUsRUFBRTtZQUN6QixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDcEQ7UUFDRCxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztTQUNwRjtRQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUM3RixRQUFRLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFDL0MsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDM0csSUFBSSxZQUFZLENBQUMsU0FBUyxLQUFLLFFBQVEsRUFBRTtZQUNyQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGdEQUFnRCxHQUFHLEdBQUcsR0FBRyxZQUFZLENBQUMscUJBQXFCLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDO1NBQ25KO1FBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFDL0IsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDOUIsUUFBUSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBQ2xELElBQUksWUFBWSxDQUFDLGFBQWEsRUFBRTtZQUM1QixRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNoRTtRQUNELElBQUksWUFBWSxDQUFDLGtCQUFrQixFQUFFO1lBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLFlBQVksQ0FBQyxzQkFBc0IsRUFBRTtZQUNyQyxRQUFRLENBQUMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3RIO1FBQ0QsSUFBSSxZQUFZLENBQUMsZ0JBQWdCLEVBQUU7WUFDL0IsUUFBUSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUN0RTtRQUNELElBQUksWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDNUQ7UUFDRCxJQUFJLFlBQVksQ0FBQyxJQUFJLEVBQUU7WUFDbkIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsa0JBQWtCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDbEU7UUFDRCxJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7WUFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3hEO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxpREFBaUQsQ0FBQyxDQUFDLENBQUM7U0FDbko7UUFDRCxJQUFJLFlBQVksQ0FBQyxrQkFBa0IsRUFBRTtZQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxZQUFZLENBQUMscUJBQXFCLEVBQUU7WUFDcEMsUUFBUSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNwSDtRQUVELE1BQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsTUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDckUsR0FBRyxDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQztZQUM3RCxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxtQ0FBbUMsRUFBRSxXQUFXLENBQUMsQ0FBQztJQUNsRyxDQUFDO0lBRUQsY0FBYyxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsa0JBQWtCO1FBQzVDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxXQUFXLEdBQUcsbUNBQW1DLENBQUM7WUFDeEQsTUFBTSxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDakMsTUFBTSxLQUFLLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO2lCQUNqRyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxFQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRSxJQUFJLENBQUM7cUJBQ2xLLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbEIsTUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO29CQUMxQixjQUFjLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUNyQyxNQUFNLFNBQVMsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMzQyxJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsUUFBUSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO3lCQUNyRyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7d0JBQ3hCLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBQzlCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzFCLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQkFBbUI7UUFDZixJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDdkIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQztpQkFDN0QsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsdUJBQXVCLElBQUksUUFBUSxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixFQUFFO29CQUNwRyxhQUFhLEdBQUcsUUFBUSxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDO2lCQUN0RTtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxFQUFPO1FBQ3RCLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUNyQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEdBQUcsRUFBRSxFQUFFLElBQUksQ0FBQztpQkFDbEUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMscUJBQXFCLElBQUksUUFBUSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRTtvQkFDM0YsV0FBVyxHQUFHLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUM7aUJBQzdEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlCQUFpQixDQUFDLEVBQU87UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMscUJBQXFCLENBQUMscUJBQXFCLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxJQUFJLENBQUM7aUJBQ3JFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUJBQXFCLENBQUMsZ0JBQXVCO1FBQ3pDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUN2QyxJQUFJLGVBQWUsQ0FBQyxtQkFBbUIsSUFBSSxlQUFlLENBQUMsbUJBQW1CLEtBQUssRUFBRSxFQUFFO2dCQUNuRixPQUFPO2FBQ1Y7WUFFRCxlQUFlLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxnQkFBZ0IsQ0FBQztJQUM1QixDQUFDO0lBRUQseUJBQXlCLENBQUMsT0FBTztRQUM3QixJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM3QixNQUFNLFFBQVEsR0FBRyxDQUFDO2dCQUNkLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixLQUFLLEVBQUUsVUFBVTthQUNwQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLFdBQVcsRUFBRSxRQUFRLENBQUM7aUJBQzNGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGVBQWUsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRTtvQkFDN0UsbUJBQW1CLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7aUJBQzdEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsT0FBTztRQUNyQixJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM3QixNQUFNLFFBQVEsR0FBRyxDQUFDO2dCQUNkLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixLQUFLLEVBQUUsVUFBVTthQUNwQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsT0FBTyxFQUFFLFFBQVEsQ0FBQztpQkFDN0UsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsY0FBYyxFQUFFO29CQUNyQyxtQkFBbUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQztpQkFDdkQ7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNuQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1QkFBdUI7UUFDbkIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFFBQVEsR0FBRyxDQUFDO29CQUNkLEdBQUcsRUFBRSxnQkFBZ0I7b0JBQ3JCLEtBQUssRUFBRSxNQUFNO2lCQUNoQixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsb0JBQW9CLEVBQUUsUUFBUSxDQUFDO2lCQUN4RSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1CQUFtQjtRQUNmLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN2QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDO2lCQUM3RCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyx1QkFBdUIsSUFBSSxRQUFRLENBQUMsdUJBQXVCLENBQUMsY0FBYyxFQUFFO29CQUNqRyxhQUFhLEdBQUcsUUFBUSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQztpQkFDbkU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLGNBQXNCO1FBQ2xDLElBQUksa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1FBQzVCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxjQUFjLEVBQUUsSUFBSSxDQUFDO2lCQUM5RSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxzQkFBc0IsSUFBSSxRQUFRLENBQUMsc0JBQXNCLENBQUMsYUFBYTtvQkFDNUYsUUFBUSxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUU7b0JBQzVELGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO2lCQUNuRjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ2xDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNCQUFzQjtRQUNsQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzFGLElBQUksbUJBQW1CLEdBQUcsRUFBRSxDQUFDO2dCQUM3QixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsNkJBQTZCO29CQUNsRCxLQUFLLENBQUMsUUFBUSxDQUFDLDZCQUE2QixDQUFDLHdCQUF3QixDQUFDLEVBQUU7b0JBQ3hFLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyw2QkFBNkIsQ0FBQyx3QkFBd0IsQ0FBQztpQkFDekY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNuQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztnQkFDM0UsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKLENBQUE7O1lBbHhEbUMsbUJBQW1CO1lBQzVCLFVBQVU7WUFDVCxXQUFXO1lBQ1IsY0FBYztZQUNULG1CQUFtQjtZQUNsQyxVQUFVO1lBQ0osVUFBVTs7O0FBVHhCLFdBQVc7SUFKdkIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLFdBQVcsQ0FxeER2QjtTQXJ4RFksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4vc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vY29uZmlnL2NvbmZpZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NTVBNRGF0YVR5cGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpbGVUeXBlcyB9IGZyb20gJy4uL29iamVjdHMvRmlsZVR5cGVzJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBPVE1NU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHJlbmRpdGlvbkNvdW50ID0gMDtcclxuICAgIG90bW1TZXNzaW9uRXhpc3RzID0gZmFsc2U7XHJcbiAgICBleGVjdXRlZENvdW50ID0gMDtcclxuICAgIGludGVydmFsSUQ7XHJcbiAgICBpc1Nlc3Npb25NYXNraW5nID0gZmFsc2U7XHJcbiAgICBJTlRFUlZBTF9ERUxBWSA9IDE1MDAwO1xyXG4gICAgbWFza2luZ1VzZXJJZDtcclxuICAgIFNUQU5EQVJEX1NFU1NJT05fVElNRU9VVF9JTlRFUlZBTCA9IDYwMDAwMDtcclxuICAgIHN0YW5kYXJkSW50ZXJ2YWxJRDtcclxuICAgIHVzZXJEZXRhaWxzT2JqID0ge1xyXG4gICAgICAgIGVtYWlsOiAnJ1xyXG4gICAgfTtcclxuXHJcbiAgICBnZXRTZXNzaW9uUmVzcG9uc2U7XHJcbiAgICBxZHNTZXNzaW9uUmVzcG9uc2VcclxuXHJcbiAgICBnZXRBc3NldHNieUFzc2V0VXJsKHVybCwgcGFyYW1ldGVycykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldE1hc2tpbmcoaXNNYXNraW5nKSB7XHJcbiAgICAgICAgdGhpcy5pc1Nlc3Npb25NYXNraW5nID0gaXNNYXNraW5nO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1hc2tpbmcoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTZXNzaW9uTWFza2luZztcclxuICAgIH1cclxuXHJcbiAgICBnZXRNYXNraW5nVHlwZSgpIHtcclxuICAgICAgICByZXR1cm4gb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMubWFza2luZ1R5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWVkaWFNYW5hZ2VyVXJsKCkge1xyXG4gICAgICAgIHJldHVybiBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5tZWRpYU1hbmFnZXJDb25maWdVcmw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SmVzc2lvbklkRnJvbVdlYlNlcnZlcigpIHtcclxuICAgICAgICByZXR1cm4gb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaHR0cE9UTU1UaWNrZXQ7XHJcbiAgICB9XHJcblxyXG4gICAgZmlyZUdldFJlc3RSZXF1ZXN0KHVybCwgcGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5nZXQodGhpcy5mb3JtUmVzdFVybCh1cmwsIHBhcmFtZXRlcnMpLCBodHRwT3B0aW9ucylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZmlyZVBvc3RSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMsIGNvbnRlbnRUeXBlLCBoZWFkZXJPYmplY3QpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IGNvbnRlbnRUeXBlXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5odHRwLnBvc3QodGhpcy5mb3JtUmVzdFVybCh1cmwsIG51bGwsIGZhbHNlKSwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUmVxdWVzdGZvckpzZXNzaW9uQ29va2llKHVybCwgcGFyYW1ldGVycywgY29udGVudFR5cGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgIHJlc3BvbnNlVHlwZTogJ3RleHQnIGFzICd0ZXh0JyxcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBjb250ZW50VHlwZSxcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmh0dHAucG9zdCh1cmwsIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hhcmVBc3NldHNQb3N0UmVxdWVzdChvdG1tYXBpdXJsLCBwYXJhbWV0ZXJzLCBjb250ZW50VHlwZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgbWVkaWFNYW5hZ2VyQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKTtcclxuICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICAgICAgaWYgKCFtZWRpYU1hbmFnZXJDb25maWcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZWRpYSBNYW5hZ2VyIGNvbmZpZyBub3QgZm91bmQnKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmxUb0JlRmlyZWQgPSBiYXNlVXJsICsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLm90bW1hcGlCYXNlVXJsICsgbWVkaWFNYW5hZ2VyQ29uZmlnLmFwaVZlcnNpb24gKyAnLycgKyBvdG1tYXBpdXJsO1xyXG5cclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IGNvbnRlbnRUeXBlLFxyXG4gICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpXHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybFRvQmVGaXJlZCwgcGFyYW1ldGVycywgaHR0cE9wdGlvbnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlRGVsZXRlUmVzdFJlcXVlc3QodXJsLCBodHRwT3B0aW9ucz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgIHJlc3BvbnNlVHlwZTogJ3RleHQnIGFzICd0ZXh0JyxcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuZm9ybVJlc3RVcmwodXJsLCBudWxsLCBmYWxzZSksIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUGF0Y2hSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMsIGNvbnRlbnRUeXBlLCBoZWFkZXJPYmplY3QpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ1gtUmVxdWVzdGVkLUJ5Jywgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpKVxyXG4gICAgICAgICAgICAgICAgLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKSxcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5odHRwLnBhdGNoKHRoaXMuZm9ybVJlc3RVcmwodXJsLCBudWxsLCBmYWxzZSksIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUHV0UmVzdFJlcXVlc3QodXJsLCBwYXJhbWV0ZXJzLCBjb250ZW50VHlwZSwgaGVhZGVyT2JqZWN0OiBIdHRwSGVhZGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IHBvc3RSZXE6IE9ic2VydmFibGU8YW55PjtcclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKCkuc2V0KCdYLVJlcXVlc3RlZC1CeScsIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSlcclxuICAgICAgICAgICAgICAgIC5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKSxcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBwb3N0UmVxID0gdGhpcy5odHRwLnB1dCh0aGlzLmZvcm1SZXN0VXJsKHVybCwgbnVsbCksIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBwb3N0UmVxLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuY2hlY2tQU1Nlc3Npb24oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvb2tpZShjbmFtZSkge1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSBjbmFtZSArICc9JztcclxuICAgICAgICBjb25zdCBjb29raWVzID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICAgICAgZm9yIChjb25zdCBjb29raWUgb2YgY29va2llcykge1xyXG4gICAgICAgICAgICBsZXQgYyA9IGNvb2tpZTtcclxuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIHtcclxuICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoYy5pbmRleE9mKG5hbWUpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZS5sZW5ndGgsIGMubGVuZ3RoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVJlc3RQYXJhbXMocGFyYW1ldGVycykge1xyXG4gICAgICAgIGxldCBwYXJhbSA9ICc/JztcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmFtZXRlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKHBhcmFtZXRlcnNbaV1bJ2tleSddID09PSAnanNvbicpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtICs9IGkgPT09IDAgPyBwYXJhbWV0ZXJzW2ldLnZhbHVlLmtleSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShwYXJhbWV0ZXJzW2ldLnZhbHVlLnZhbHVlKSkgOlxyXG4gICAgICAgICAgICAgICAgICAgICcmJyArIHBhcmFtZXRlcnNbaV0udmFsdWUua2V5ICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHBhcmFtZXRlcnNbaV0udmFsdWUudmFsdWUpKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtICs9IGkgPT09IDAgPyBwYXJhbWV0ZXJzW2ldLmtleSArICc9JyArIHBhcmFtZXRlcnNbaV0udmFsdWUgOiAnJicgKyBwYXJhbWV0ZXJzW2ldLmtleSArICc9JyArIHBhcmFtZXRlcnNbaV0udmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHBhcmFtO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1SZXN0VXJsKG90bW1hcGl1cmwsIHVybHBhcmFtLCBpc1JlbmRpdGlvbj8pIHtcclxuICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcblxyXG4gICAgICAgIGlmIChpc1JlbmRpdGlvbiA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gYmFzZVVybCArIG90bW1hcGl1cmw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgbWVkaWFNYW5hZ2VyQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKTtcclxuICAgICAgICAgICAgaWYgKCFtZWRpYU1hbmFnZXJDb25maWcpIHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTWVkaWEgTWFuYWdlciBjb25maWcgbm90IGZvdW5kJyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGJhc2VVcmwgKyBvdG1tU2VydmljZXNDb25zdGFudHMub3RtbWFwaUJhc2VVcmwgK1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5hcGlWZXJzaW9uICsgJy8nICsgb3RtbWFwaXVybCArXHJcbiAgICAgICAgICAgICAgICAodXJscGFyYW0gIT0gbnVsbCA/IHRoaXMuZm9ybVJlc3RQYXJhbXModXJscGFyYW0pIDogJycpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkb1Bvc3RSZXF1ZXN0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0LCBpc09ubHlSZXNwb25zZSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChoZWFkZXJPYmplY3QgJiYgaGVhZGVyT2JqZWN0ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhlYWRlck9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT3B0aW9ucykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkb1B1dFJlcXVlc3QodXJsLCBwYXJhbWV0ZXJzLCBoZWFkZXJPYmplY3QsIGlzT25seVJlc3BvbnNlKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKGhlYWRlck9iamVjdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5odHRwLnB1dCh1cmwsIHBhcmFtZXRlcnMsIGhlYWRlck9iamVjdCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5odHRwLnB1dCh1cmwsIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldEV4cG9ydEpvYigpIHtcclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW1zID0gW3tcclxuXHJcbiAgICAgICAgICAgIGtleTogJ2xvYWRfYXNzZXRfZGV0YWlscycsXHJcbiAgICAgICAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ3NvcnQnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2Rlc2NfbGFzdF91cGRhdGVkX2RhdGVfdGltZSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xvYWRfam9iX2RldGFpbHMnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ3RydWUnXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBrZXk6ICdsYXN0X3VwZGF0ZWRfZGF0ZV90aW1lJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdUT0RBWSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xpbWl0JyxcclxuICAgICAgICAgICAgdmFsdWU6ICc1MDAwJ1xyXG4gICAgICAgIH1dO1xyXG5cclxuICAgICAgICB0aGlzLmdldEV4cG9ydEpvYnMocGFyYW1zKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5qb2JzX3Jlc291cmNlICYmIHJlc3BvbnNlLmpvYnNfcmVzb3VyY2UuY29sbGVjdGlvbl9zdW1tYXJ5ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zICYmXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2V0IGV4cG9ydCByZXNwb25zZVwiKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJObyBpdGVtc1wiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZ2V0IGRhdGEgZnJvbSBRRFMuXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgY2hlY2tPdG1tU2Vzc2lvbihpc1VwbG9hZD8sIG9wZW5RRFNUcmF5Pyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgLy8gTVNWLTIzOVxyXG4gICAgICAgICAgICAvLyBpZiAoIWlzVXBsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMub3RtbVNlc3Npb25VcmwsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShnZXRTZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZ2V0U2Vzc2lvblJlc3BvbnNlICYmIGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlICYmIGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIE1TVi0yMzlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlID0gZ2V0U2Vzc2lvblJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZCA9IGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCBnZXRTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0T1RNTVNlc3Npb24odHJ1ZSkgLy8gTVZTLTIzOSB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyB9IGVsc2UgeyAvLyBNU1YgLTIzOVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuZW5hYmxlUURTICYmXHJcbiAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xZHNTZXJ2aWNlLmdldFFEU1Nlc3Npb24ob3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShxZHNTZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIE1TVi0yMzlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2Vzc2lvblJlc3BvbnNlID0gcWRzU2Vzc2lvblJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRFeHBvcnRKb2IoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5jb25uZWN0VG9RRFMocWRzU2Vzc2lvblJlc3BvbnNlLnRyYW5zZmVyc2NoZW1lc19zZXNzaW9uX3Jlc291cmNlLnRyYW5zZmVyX3Nlc3Npb24sIGlzVXBsb2FkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5nZXRTZXNzaW9uUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBxZHNTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIFFEUyBzZXNzaW9uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldExvZ2dlZEluVXNlckRldGFpbHModXNlcklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMudXNlckRldGFpbHNVcmwgKyB1c2VySWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcl9yZXNvdXJjZSAmJiByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRGV0YWlsc09iai5lbWFpbCA9IHJlc3BvbnNlLnVzZXJfcmVzb3VyY2UudXNlci5lbWFpbF9hZGRyZXNzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLnVzZXJfcmVzb3VyY2UudXNlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgdXNlciBkZXRhaWxzIGZyb20gT1RNTS4nKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbWF0Y2hTZXNzaW9uQXR0cmlidXRlcyhjb21wYXJlVXNlcklkLCBsb2dpbm5hbWUpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySUQoKTtcclxuICAgICAgICBsZXQgZmxhZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQodXNlcklkKSAmJiB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoY29tcGFyZVVzZXJJZCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNFcXVhbFdpdGhDYXNlKHVzZXJJZCwgY29tcGFyZVVzZXJJZCkgfHwgKGNvbXBhcmVVc2VySWQuaW5kZXhPZignQCcpID49IDAgJiYgdGhpcy51dGlsU2VydmljZS5pc0VxdWFsV2l0aENhc2UodXNlcklkLCBjb21wYXJlVXNlcklkLnNwbGl0KCdAJylbMF0pKSkge1xyXG4gICAgICAgICAgICAgICAgZmxhZyA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZCh1c2VySWQpICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChsb2dpbm5hbWUpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzRXF1YWxXaXRoQ2FzZSh1c2VySWQsIGxvZ2lubmFtZSkgfHwgKGxvZ2lubmFtZS5pbmRleE9mKCdAJykgPj0gMCAmJiB0aGlzLnV0aWxTZXJ2aWNlLmlzRXF1YWxXaXRoQ2FzZSh1c2VySWQsIGxvZ2lubmFtZS5zcGxpdCgnQCcpWzBdKSkpIHtcclxuICAgICAgICAgICAgICAgIGZsYWcgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmxhZztcclxuICAgIH1cclxuXHJcbiAgICBzdGFydE9UTU1TZXNzaW9uKGluaXRpYWxMb2dpbj8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIG9yZ2FuaXphdGlvbjogY29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lLFxyXG4gICAgICAgICAgICAvLyBzb3VyY2VSZXNvdXJjZU5hbWU6ICdfX09URFMjT3JnYW5pemF0aW9uYWwjUGxhdGZvcm0jUmVzb3VyY2VfXycsXHJcbiAgICAgICAgICAgIHNvdXJjZVJlc291cmNlTmFtZTogdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmFwcGxpY2F0aW9uRGV0YWlscy5PVERTX0FQUFdPUktTX1JFU09VUkNFX05BTUUsXHJcbiAgICAgICAgICAgIHRhcmdldFJlc291cmNlTmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5vdGRzUmVzb3VyY2VOYW1lLFxyXG4gICAgICAgICAgICBpc09yZ1NwYWNlOiAndHJ1ZSdcclxuICAgICAgICB9O1xyXG5cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldE9URFNUaWNrZXRGb3JVc2VyKHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKG90ZHNUaWNrZXRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRpY2tldCA9ICdPVERTVGlja2V0PSc7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9rZW4gPSBvdGRzVGlja2V0UmVzcG9uc2UudHVwbGUub2xkLk9URFNBY2Nlc3NUaWNrZXQub3RtbVRpY2tldDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRva2VuICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGlja2V0ICs9IHRva2VuO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG90bW1VcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVVybCA9IG90bW1VcmwuY29uY2F0KCdvdG1tL3V4LWh0bWwvaW5kZXguaHRtbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiAoaW5pdGlhbExvZ2luKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVJlcXVlc3Rmb3JKc2Vzc2lvbkNvb2tpZShvdG1tVXJsLCB0aWNrZXQsIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuVVJMRU5DT0RFRClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoalNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLm90bW1TZXNzaW9uVXJsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGdldFNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy51c2VyU2Vzc2lvbklkID1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2UpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZS5zZXNzaW9uKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5pZCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZS5zZXNzaW9uLmlkIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBsb2dpbk5hbWUgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5sb2dpbl9uYW1lKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5sb2dpbl9uYW1lIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi51c2VyX2lkKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi51c2VyX2lkIDogJyc7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRMb2dnZWRJblVzZXJEZXRhaWxzKHVzZXJJZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVzZXJSZXNvdXJjZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dHJzTWF0Y2hlZCA9IHRoaXMubWF0Y2hTZXNzaW9uQXR0cmlidXRlcyh1c2VyUmVzb3VyY2Uub3Rkc191c2VyX2lkLGxvZ2luTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghYXR0cnNNYXRjaGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXNzaW9uRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZGVzdHJveU90bW1TZXNzaW9uKCkuc3Vic2NyaWJlKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlc3Npb25FeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuZW5hYmxlUURTICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCAmJiBpbml0aWFsTG9naW4pIHsgLy9NVlMtMjM5ICYmIGluaXRpYWxMb2dpblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5nZXRRRFNTZXNzaW9uKG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocWRzU2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnFkc1NlcnZpY2UuY29ubmVjdFRvUURTKHFkc1Nlc3Npb25SZXNwb25zZS50cmFuc2ZlcnNjaGVtZXNfc2Vzc2lvbl9yZXNvdXJjZS50cmFuc2Zlcl9zZXNzaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBxZHNTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBRRFMgc2Vzc2lvbicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLm90bW1TZXNzaW9uRXhpc3RzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vb2JzZXJ2ZXIubmV4dCh0aGlzLm90bW1TZXNzaW9uRXhpc3RzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL29ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5vdG1tU2Vzc2lvbkV4aXN0cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVtYWlsSWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVtYWlsSWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGdldFNlc3Npb25FcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihnZXRTZXNzaW9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGpTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGpTZXNzaW9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgb3Rkc1RpY2tldEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihvdGRzVGlja2V0RXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVzdHJveU90bW1TZXNzaW9uKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZCA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVEZWxldGVSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMub3RtbVNlc3Npb25VcmwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdFcnJvciB3aGlsZSBkZWxldGluZyBPVE1NIHNlc3Npb24uJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRMb2dpbkVtYWlsSWQodXNlcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IGVtYWlsSWQgPSAnJztcclxuICAgICAgICAgICAgY29uc3QgcmVxID0gdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnVzZXJEZXRhaWxzVXJsICsgdXNlcklkLCBudWxsKTtcclxuICAgICAgICAgICAgcmVxLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcl9yZXNvdXJjZSAmJiByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIuZW1haWxfYWRkcmVzcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGVtYWlsSWQgPSByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIuZW1haWxfYWRkcmVzcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZW1haWxJZCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGVtYWlsSWQpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXBsb2FkTWFuaWZlc3QoZmlsZXMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpIHtcclxuICAgICAgICBsZXQgdXBsb2FkTWFuaWZlc3QgPSBudWxsO1xyXG4gICAgICAgIGlmIChmaWxlcykge1xyXG4gICAgICAgICAgICBpZiAoZmlsZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWRNYW5pZmVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICB1cGxvYWRfbWFuaWZlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFzdGVyX2ZpbGVzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogZmlsZXNbMF0ubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlX3R5cGU6IGZpbGVzWzBdLnR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWFzdGVyRmlsZXMgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXN0ZXJGaWxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZV9uYW1lOiBmaWxlc1tpXS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZV90eXBlOiBmaWxlc1tpXS50eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVwbG9hZF9tYW5pZmVzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXN0ZXJfZmlsZXM6IG1hc3RlckZpbGVzXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93ICdVbmFibGUgdG8gY3JlYXRlIHVwbG9hZCBtYW5pZmVzdDpcXG4gcGFyYW1ldGVyIGZpbGVzIGlzIGVpdGhlciB1bmRlZmluZWQgb3IgaW52YWxpZC4nO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdXBsb2FkTWFuaWZlc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVXBsb2FkTWFuaWZlc3RGb3JSZXZpc2lvbihmaWxlcykge1xyXG4gICAgICAgIGxldCB1cGxvYWRNYW5pZmVzdCA9IG51bGw7XHJcbiAgICAgICAgaWYgKGZpbGVzKSB7XHJcbiAgICAgICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgdXBsb2FkX21hbmlmZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFzdGVyX2ZpbGVzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlX25hbWU6IGZpbGVzWzBdLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBmaWxlc1swXS5hc3NldElkXHJcbiAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBtYXN0ZXJGaWxlcyA9IG5ldyBBcnJheSgpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBtYXN0ZXJGaWxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogZmlsZXNbaV0ubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBmaWxlc1tpXS5hc3NldElkXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB1cGxvYWRNYW5pZmVzdCA9IHtcclxuICAgICAgICAgICAgICAgIHVwbG9hZF9tYW5pZmVzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hc3Rlcl9maWxlczogbWFzdGVyRmlsZXNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHVwbG9hZE1hbmlmZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVRyYW5zZmVyRGV0YWlscyhzY2hlbWU6IHN0cmluZywgZmlsZXM6IEFycmF5PGFueT4pIHtcclxuICAgICAgICBsZXQgdHJhbnNmZXJEZXRhaWxzID0gbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgdHJhbnNmZXJGaWxlSXRlbXMgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRyYW5zZmVyRmlsZUl0ZW1zLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgc2NoZW1lX2ZpbGVfaWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgc2NoZW1lX2ZpbGVfbmFtZTogJydcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyYW5zZmVyRGV0YWlscyA9IHtcclxuICAgICAgICAgICAgdHJhbnNmZXJfZGV0YWlsc19wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgdHJhbnNmZXJfZmlsZV9pdGVtczogdHJhbnNmZXJGaWxlSXRlbXMsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zlcl9zY2hlbWU6IHNjaGVtZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRyYW5zZmVyRGV0YWlscztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldFJlcHJlc2VudGF0aW9uKG1ldGFkYXRhRmllbGRzLCBtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YUZpZWxkcywgbWV0YURhdGFGaWVsZFZhbHVlcywgbWV0YWRhdGFNb2RlbCwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZUFzc2V0UmVwcmVzZW50YXRpb24obWV0YWRhdGFGaWVsZHMsIG1ldGFEYXRhRmllbGRWYWx1ZXMsIG1ldGFEYXRhTW9kZWwsIHNlY3VyaXR5UG9saWN5SUQpIHtcclxuICAgICAgICBsZXQgYXNzZXRSZXByZXNlbnRhdGlvbiA9IG51bGw7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGFGaWVsZExpc3QgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICBsZXQgbWV0YURhdGFFbGVtZW50ID0gbnVsbDtcclxuICAgICAgICBjb25zdCBzZWN1cml0eVBvbGljaWVzID0gW107XHJcblxyXG4gICAgICAgIGlmIChtZXRhZGF0YUZpZWxkcyAmJiBBcnJheS5pc0FycmF5KG1ldGFkYXRhRmllbGRzKSAmJiBtZXRhRGF0YU1vZGVsICYmIG1ldGFEYXRhTW9kZWwgIT0gbnVsbCAmJiBtZXRhRGF0YU1vZGVsICE9PSAnJykge1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZGVmYXVsdFNlY1BvbGljeUlkID0gMTtcclxuICAgICAgICAgICAgY29uc3Qgc2VjdXJpdHlQb2xpY3kgPSBkZWZhdWx0U2VjUG9saWN5SWQ7XHJcbiAgICAgICAgICAgIGlmIChzZWN1cml0eVBvbGljeUlEICYmIHNlY3VyaXR5UG9saWN5SUQgIT0gbnVsbCAmJiBzZWN1cml0eVBvbGljeUlEICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaWRzID0gc2VjdXJpdHlQb2xpY3lJRC5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialsnaWQnXSA9IGlkc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICBzZWN1cml0eVBvbGljaWVzLnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9saWN5ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBkZWZhdWx0U2VjUG9saWN5SWQsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgc2VjdXJpdHlQb2xpY2llcy5wdXNoKHBvbGljeSk7XHJcbiAgICAgICAgICAgICAgICBwb2xpY3kgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGl0ZXJhdGUgYWxsIGNvbmZpZ3VyZWQgbWV0YWRhdGFmaWVsZHMgYW5kIHByZXBhcmUgbWV0YWRhdGEgZmllbGQgY29uZmlndXJhdGlvbiB0byBzZW5kIHdpdGggcmVxdWVzdC5cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBtZXRhZGF0YUZpZWxkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgbWV0YURhdGFFbGVtZW50ID0ge307XHJcbiAgICAgICAgICAgICAgICBtZXRhRGF0YUVsZW1lbnQuaWQgPSBtZXRhZGF0YUZpZWxkc1tpXS5pZDtcclxuICAgICAgICAgICAgICAgIGxldCB0eXBlID0gbWV0YWRhdGFGaWVsZHNbaV0udHlwZTtcclxuICAgICAgICAgICAgICAgIGlmICghdHlwZSB8fCB0eXBlID09IG51bGwgfHwgdHlwZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZSA9ICdjb20uYXJ0ZXNpYS5tZXRhZGF0YS5NZXRhZGF0YUZpZWxkJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC50eXBlID0gdHlwZTtcclxuICAgICAgICAgICAgICAgIC8vIGlmIG1ldGFEYXRhRmllbGRWYWx1ZXMgaXMgZGVmaW5lZCBhbmQgZmllbGQgZnJvbSBlbnRpdHkgaXMgb2YgY2FzY2FkaW5nIHR5cGUgKGVnOiBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiA9ICdicmFuZCcpXHJcbiAgICAgICAgICAgICAgICBpZiAobWV0YURhdGFGaWVsZFZhbHVlcyAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICE9IG51bGwgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgJiZcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRzW2ldLmZpZWxkUmVmLmluZGV4T2YoJ14nKSA9PT0gLTEgJiYgbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl0gIT09ICcnKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBtZXRhZGF0YVZhbHVlID0gbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl07XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gYXJyYXkgb2YgdmFsdWUgaW5zaWRlIHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlID09PSAnY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFUYWJsZUZpZWxkJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkobWV0YWRhdGFWYWx1ZSkgJiYgbWV0YWRhdGFWYWx1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgbWV0YWRhdGFWYWx1ZS5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZVtpbmRleF0gPSBtZXRhZGF0YVZhbHVlW2luZGV4XS50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZVtpbmRleF0gPSBtZXRhZGF0YVZhbHVlW2luZGV4XS5wYXJzZUludCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBtZXRhZGF0YVZhbHVlW2luZGV4XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMucHVzaCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ3N0cmluZycgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFWYWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YVZhbHVlID0gbWV0YWRhdGFWYWx1ZS50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUpICYmIG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlID09PSAnbnVtYmVyJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YVZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhVmFsdWUgPSBtZXRhZGF0YVZhbHVlLnBhcnNlSW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbWV0YWRhdGFWYWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzLnB1c2godmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC52YWx1ZXMgPSB2YWx1ZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSkgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUgPT09ICdzdHJpbmcnICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFWYWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhVmFsdWUgPSBtZXRhZGF0YVZhbHVlLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ251bWJlcidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YVZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZSA9IG1ldGFkYXRhVmFsdWUucGFyc2VJbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBtZXRhZGF0YVZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobWV0YURhdGFGaWVsZFZhbHVlcyAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICE9IG51bGwgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgJiZcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRzW2ldLmZpZWxkUmVmLmluZGV4T2YoJ14nKSAhPT0gLTEgJiYgbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl0gIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRzID0gbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYuc3BsaXQoJ14nKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBpdGVyYXRlIGFsbCBjYXNjYWRlIGZpZWxkc1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IHggPSAwOyB4IDwgZmllbGRzLmxlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBtZXRhZGF0YXZhbHVlID0gbWV0YURhdGFGaWVsZFZhbHVlc1tmaWVsZHNbeF1dO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlYWNoIGNhc2NhZGVkIGZpZWxkIHZhbHVlIG9ubHkgb25lIHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KG1ldGFkYXRhdmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YXZhbHVlID0gbWV0YWRhdGF2YWx1ZVswXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvbiBtZXRhZGF0YXZhbHVlIGlzIHJlY2VpdmVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YXZhbHVlICE9PSAnJyAmJiBtZXRhZGF0YXZhbHVlICE9IG51bGwgJiYgbWV0YWRhdGF2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uZGl0aW9uIHRvIGNoZWNrIGFscmVhZHkgdmFsdWUgaGFzIGFwcGVuZGVkIGZvciBjYXNjYWRpbmcgZmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZS52YWx1ZS52YWx1ZSAhPT0gJycgJiYgdmFsdWUudmFsdWUudmFsdWUgIT0gbnVsbCAmJiB2YWx1ZS52YWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlLnZhbHVlLnZhbHVlID0gdmFsdWUudmFsdWUudmFsdWUgKyAnXicgKyBtZXRhZGF0YXZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZS52YWx1ZS52YWx1ZSA9IG1ldGFkYXRhdmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWZ2YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBpZiBkZWZhdWx0IHZhbHVlIGlzIHByZXNlbnRcclxuICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZHNbaV0uZGVmYXVsdCAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGVmYXVsdCAhPSBudWxsICYmIG1ldGFkYXRhRmllbGRzW2ldLmRlZmF1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmdmFsdWUgPSBtZXRhZGF0YUZpZWxkc1tpXS5kZWZhdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUpICYmIG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmdmFsdWUgPSAnTkEnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSkgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZ2YWx1ZSA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGRlZnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YURhdGFFbGVtZW50LnZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkTGlzdC5wdXNoKG1ldGFEYXRhRWxlbWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBtZXRhZGF0YU1vZGVsSWQgPSAnJztcclxuICAgICAgICAgICAgaWYgKG1ldGFEYXRhTW9kZWwgJiYgbWV0YURhdGFNb2RlbCAhPSBudWxsICYmIG1ldGFEYXRhTW9kZWwudHJpbSgpICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFNb2RlbElkID0gbWV0YURhdGFNb2RlbDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRocm93ICdNZXRhRGF0YSBNb2RlbElEIGlzIG1pc3NpbmcuJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhc3NldFJlcHJlc2VudGF0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgYXNzZXRfcmVzb3VyY2U6IHtcclxuICAgICAgICAgICAgICAgICAgICBhc3NldDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFfZWxlbWVudF9saXN0OiBtZXRhZGF0YUZpZWxkTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogbWV0YWRhdGFNb2RlbElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWN1cml0eV9wb2xpY3lfbGlzdDogc2VjdXJpdHlQb2xpY2llc1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyAnVW5hYmxlIHRvIGNyZWF0ZSBhc3NldCByZXByZXNlbnRhdGlvbidcclxuICAgICAgICAgICAgKyAnIHRoZSBwYXJhbWV0ZXJzIG1ldGFkYXRhRmllbGRzICYgbWV0YURhdGFGaWVsZFZhbHVlcyBhcmUgbm90IGNvcnJlY3Qgb3IgdW5kZWZpbmVkLic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhc3NldFJlcHJlc2VudGF0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZEFzc2V0cyhmaWxlcywgbWV0YWRhdGFGaWVsZHMsIG1ldGFkYXRhRmllbGRWYWx1ZXMsIHBhcmVudEZvcmxkZXJJRCwgaW1wb3J0VGVtcGxhdGVJRCwgbWV0YWRhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmFzc3NldFVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuICAgICAgICBpZiAoZmlsZXMgJiYgZmlsZXMgIT0gbnVsbCAmJiBtZXRhZGF0YUZpZWxkVmFsdWVzICYmIG1ldGFkYXRhRmllbGRWYWx1ZXMgIT0gbnVsbCAmJiBtZXRhZGF0YUZpZWxkcyAmJiBwYXJlbnRGb3JsZGVySUQgJiZcclxuICAgICAgICAgICAgaW1wb3J0VGVtcGxhdGVJRCkge1xyXG5cclxuICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YUZpZWxkcywgbWV0YWRhdGFGaWVsZFZhbHVlcywgbWV0YWRhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkpO1xyXG4gICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2Fzc2V0X3JlcHJlc2VudGF0aW9uJywgYmxvYik7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgncGFyZW50X2ZvbGRlcl9pZCcsIHBhcmVudEZvcmxkZXJJRCk7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X3RlbXBsYXRlX2lkJywgaW1wb3J0VGVtcGxhdGVJRCk7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbm9fY29udGVudCcsICdmYWxzZScpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcykpO1xyXG4gICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ21hbmlmZXN0JywgYmxvYik7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgZmlsZSBvZiBmaWxlcykge1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdmaWxlcycsIGZpbGUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgJ1VuYWJsZSB0byB1cGxvYWQgYXNzZXRzOlxcbmJlY2F1c2UgdGhlIEZpbGVzIGFyZSBtZXRhIGRhdGEgdmFsdWVzIGFyZSBub3QgcGFzc2VkLic7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YSwgbWV0YURhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkge1xyXG4gICAgICAgIGxldCBhc3NldFJlcHJlc2VudGF0aW9uID0gbnVsbDtcclxuICAgICAgICBjb25zdCBtZXRhZGF0YUVsZW1lbnRMaXN0ID0gbmV3IEFycmF5KCk7XHJcbiAgICAgICAgY29uc3Qgc2VjdXJpdHlQb2xpY2llcyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAobWV0YWRhdGEgJiYgbWV0YURhdGFNb2RlbCAmJiBtZXRhRGF0YU1vZGVsICE9IG51bGwgJiYgbWV0YURhdGFNb2RlbCAhPT0gJycpIHtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGRlZmF1bHRTZWNQb2xpY3lJZCA9IDE7XHJcbiAgICAgICAgICAgIGlmIChzZWN1cml0eVBvbGljeUlEICYmIHNlY3VyaXR5UG9saWN5SUQgIT0gbnVsbCAmJiBzZWN1cml0eVBvbGljeUlEICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaWRzID0gc2VjdXJpdHlQb2xpY3lJRC5zcGxpdCgnOycpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBpZCBvZiBpZHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialsnaWQnXSA9IE51bWJlcihpZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VjdXJpdHlQb2xpY2llcy5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvbGljeSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGVmYXVsdFNlY1BvbGljeUlkLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHNlY3VyaXR5UG9saWNpZXMucHVzaChwb2xpY3kpO1xyXG4gICAgICAgICAgICAgICAgcG9saWN5ID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IG1ldGFkYXRhTW9kZWxJZCA9ICcnO1xyXG4gICAgICAgICAgICBpZiAobWV0YURhdGFNb2RlbCAmJiBtZXRhRGF0YU1vZGVsICE9IG51bGwgJiYgbWV0YURhdGFNb2RlbC50cmltKCkgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YU1vZGVsSWQgPSBtZXRhRGF0YU1vZGVsO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZXRhZGF0YSBNb2RlbElEIGlzIG1pc3NpbmcuJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChtZXRhZGF0YSAmJiBtZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QgJiYgQXJyYXkuaXNBcnJheShtZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IG1ldGFkYXRhR3JvdXAgb2YgbWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBtZXRhZGF0YUVsZW1lbnQgb2YgbWV0YWRhdGFHcm91cC5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRWxlbWVudCAmJiBtZXRhZGF0YUVsZW1lbnQuaWQgPT09ICdNUE0uVVRJTFMuREFUQV9UWVBFJyAmJiBtZXRhZGF0YUVsZW1lbnQudmFsdWUgJiYgbWV0YWRhdGFFbGVtZW50LnZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUVsZW1lbnQudmFsdWUudmFsdWUudmFsdWUgPSBPVE1NTVBNRGF0YVR5cGVzLkFTU0VUO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRWxlbWVudExpc3QucHVzaChtZXRhZGF0YUVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgYXNzZXRSZXByZXNlbnRhdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgIGFzc2V0X3Jlc291cmNlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhX2VsZW1lbnRfbGlzdDogbWV0YWRhdGFFbGVtZW50TGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogbWV0YWRhdGFNb2RlbElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWN1cml0eV9wb2xpY3lfbGlzdDogc2VjdXJpdHlQb2xpY2llc1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGBVbmFibGUgdG8gY3JlYXRlIGFzc2V0IHJlcHJlc2VudGF0aW9uOlxyXG4gICAgICAgICAgICB0aGUgcGFyYW1ldGVycyBtZXRhZGF0YUZpZWxkcyAmIG1ldGFEYXRhRmllbGRWYWx1ZXMgYXJlIG5vdCBjb3JyZWN0IG9yIHVuZGVmaW5lZC5gKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFzc2V0UmVwcmVzZW50YXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgdXBsb2FkU2luZ2xlQXNzZXQoZmlsZXMsIG1ldGFEYXRhRmllbGRWYWx1ZXMsIHBhcmVudEZvbGRlcklELCBpbXBvcnRUZW1wbGF0ZUlELCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmFzc3NldFVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuICAgICAgICBpZiAoKGZpbGVzICYmIEFycmF5LmlzQXJyYXkoZmlsZXMpKSAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICYmIHBhcmVudEZvbGRlcklEICYmIGltcG9ydFRlbXBsYXRlSUQpIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlIG9mIGZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5mb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkpO1xyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdhc3NldF9yZXByZXNlbnRhdGlvbicsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdwYXJlbnRfZm9sZGVyX2lkJywgcGFyZW50Rm9sZGVySUQpO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdpbXBvcnRfdGVtcGxhdGVfaWQnLCBpbXBvcnRUZW1wbGF0ZUlEKTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbm9fY29udGVudCcsICdmYWxzZScpO1xyXG4gICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpKTtcclxuICAgICAgICAgICAgICAgIGJsb2IgPSBuZXcgQmxvYihbY29udGVudF0sIHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbWFuaWZlc3QnLCBibG9iKTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnZmlsZXMnLCBmaWxlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZG9Qb3N0UmVxdWVzdCh1cmwsIHBhcmFtLCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwbG9hZCBhc3NldHM6XFxuYmVjYXVzZSB0aGUgcGFyYW1ldGVycyBhcmUgZWl0aGVyIGluY29ycmVjdCBvciB1bmRlZmluZWQuJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVPVE1NSm9iKGZpbGVOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICAgICAgY29uc3QgdmVyc2lvbiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuYXBpVmVyc2lvbjtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gYmFzZVVybCArICdvdG1tYXBpLycrdmVyc2lvbisnL2pvYnMvaW1wb3J0cyc7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuTVVMVElQQVJURk9STURBVEEsXHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtT1RNTS1SZXF1ZXN0JzogJ3RydWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgICAgICAnWC1VU0VTLUNIVU5LUyc6ICdmYWxzZSdcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGVOYW1lKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgZm9ybURhdGEsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbXBvcnRPVE1NSm9iKGZpbGVzLCBtZXRhZGF0YUZpZWxkcywgbWV0YURhdGFGaWVsZFZhbHVlcywgcGFyZW50Rm9ybGRlcklELCBpbXBvcnRUZW1wbGF0ZUlELCBtZXRhZGF0YU1vZGVsLFxyXG4gICAgICAgIHNlY3VyaXR5UG9saWN5LCBpbXBvcnRKb2JJZCwgaXNSZXZpc2lvbj8pIHtcclxuXHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5mb3JtUmVzdFVybCgoaXNSZXZpc2lvbiA/IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5jaGVja2luVXJsIDogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCkgKyAnLycgKyBpbXBvcnRKb2JJZCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKChmaWxlcyAmJiBBcnJheS5pc0FycmF5KGZpbGVzKSkgJiYgbWV0YURhdGFGaWVsZFZhbHVlcyAmJlxyXG4gICAgICAgICAgICBwYXJlbnRGb3JsZGVySUQgJiYgaW1wb3J0VGVtcGxhdGVJRCkge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5mb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkpO1xyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFpc1JldmlzaW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdhc3NldF9yZXByZXNlbnRhdGlvbicsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgncGFyZW50X2ZvbGRlcl9pZCcsIHBhcmVudEZvcmxkZXJJRCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ltcG9ydF9qb2JfaWQnLCBpbXBvcnRKb2JJZCk7XHJcbiAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGVzW2ldLm5hbWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpc1JldmlzaW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3RGb3JSZXZpc2lvbihmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdtYW5pZmVzdCcsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5lbmFibGVRRFMgJiYgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCl7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVHJhbnNmZXJEZXRhaWxzKCdRRFMnLCBmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJsb2IgPSBuZXcgQmxvYihbY29udGVudF0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLkNPTlRFTlRfVFlQRS5KU09OREFUQVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgndHJhbnNmZXJfZGV0YWlscycsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpKTtcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVzJyxmaWxlc1tpXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRvUHV0UmVxdWVzdCh1cmwsIHBhcmFtLCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwbG9hZCBhc3NldHM6XFxuYmVjYXVzZSB0aGUgcGFyYW1ldGVycyBhcmUgZWl0aGVyIGluY29ycmVjdCBvciB1bmRlZmluZWQuJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZUFzc2V0TG9ja1N0YXRlKGFzc2V0TGlzdCkge1xyXG4gICAgICAgIGxldCBmaWxlTmFtZSA9ICcnO1xyXG4gICAgICAgIGxldCBmaWxlQ291bnRlciA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQgayA9IDA7IGsgPCBhc3NldExpc3QubGVuZ3RoOyBrKyspIHtcclxuICAgICAgICAgICAgaWYgKGFzc2V0TGlzdFtrXS5hc3NldF9zdGF0ZSA9PT0gJ0xPQ0tFRCcgJiZcclxuICAgICAgICAgICAgICAgIGFzc2V0TGlzdFtrXS5tZXRhZGF0YV9sb2NrX3N0YXRlX3VzZXJfbmFtZSAhPT0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKCkpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVDb3VudGVyKys7XHJcbiAgICAgICAgICAgICAgICBpZiAoayA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSAnLCc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSBmaWxlQ291bnRlciArICcpJyArIGFzc2V0TGlzdFtrXS5uYW1lICsgJyAtIExvY2tlZCBCeSA6ICcgKyBhc3NldExpc3Rba10ubWV0YWRhdGFfbG9ja19zdGF0ZV91c2VyX25hbWU7XHJcbiAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSAnXFxuJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmlsZU5hbWUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbyhgRm9sbG93aW5nIGZpbGUocykgYXJlIGxvY2tlZCBieSBkaWZmZXJlbnQgdXNlciBhbmQgY2Fubm90IHByb2NlZWQgd2l0aCByZXZpc2lvbiB1cGxvYWQsXHJcbiAgICAgICAgICAgICBLaW5kbHkgY29udGFjdCB0aGUgcmVzcGVjdGl2ZSB1c2VycyBvciBhZG1pbmlzdHJhdG9yIHRvIHJlbW92ZSBsb2NrIG9uIHRoZSBmaWxlKHMpIFxcbmAgKyBmaWxlTmFtZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXNzZXRDaGVja291dChhc3NldElkczogQXJyYXk8c3RyaW5nPikge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghYXNzZXRJZHMgfHwgIWFzc2V0SWRzLmxlbmd0aCB8fCBhc3NldElkcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdObyBhc3NldCBpZHMgYXJlIGF2YWlsYWJsZSB0byBjaGVjayBvdXQnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICAgICAga2V5OiAnYWN0aW9uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiAnY2hlY2tfb3V0J1xyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBrZXk6ICdqc29uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAga2V5OiAnc2VsZWN0aW9uX2NvbnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbl9jb250ZXh0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLmFzc2V0LnNlbGVjdGlvbi5Bc3NldElkc1NlbGVjdGlvbkNvbnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluY2x1ZGVfZGVzY2VuZGFudHM6ICdJTU1FRElBVEUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1dO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsID0gdGhpcy5mb3JtUmVzdFVybCgnYXNzZXRzL3N0YXRlJywgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLlVSTEVOQ09ERURcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtUmVxdWVzdDogJ3hXd3dGb3JtVXJsZW5jb2RlZCdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5kb1B1dFJlcXVlc3QodXJsLCB0aGlzLmZvcm1SZXN0UGFyYW1zKHVybFBhcmFtKS5zdWJzdHJpbmcoMSksIGh0dHBPcHRpb25zLCBmYWxzZSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VbJ2J1bGtfYXNzZXRfcmVzdWx0X3JlcHJlc2VudGF0aW9uJ10uYnVsa19hc3NldF9yZXN1bHQuZmFpbGVkX29iamVjdF9saXN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoID4gMCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcignVW5hYmxlIHRvIGNoZWNrIG91dCAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoICsgJyBhc3NldHMuJykpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0Lmxlbmd0aCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbJ2J1bGtfYXNzZXRfcmVzdWx0X3JlcHJlc2VudGF0aW9uJ10uYnVsa19hc3NldF9yZXN1bHQuc3VjY2Vzc2Z1bF9vYmplY3RfbGlzdC5sZW5ndGggPiAwICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZHMubGVuZ3RoID09PSByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKCdVbmFibGUgdG8gY2hlY2sgb3V0ICcgKyBhc3NldElkcy5sZW5ndGggKyAnIGFzc2V0cy4nKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ0ludmFsaWQgQXNzZXQgSWRzLicpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2NrQXNzZXRzKGFzc2V0SWRzOiBBcnJheTxzdHJpbmc+KSB7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdhY3Rpb24nLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2xvY2snXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBrZXk6ICdqc29uJyxcclxuICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgIGtleTogJ3NlbGVjdGlvbl9jb250ZXh0JyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uX2NvbnRleHRfcGFyYW06IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uX2NvbnRleHQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEuYXNzZXQuc2VsZWN0aW9uLkFzc2V0SWRzU2VsZWN0aW9uQ29udGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmNsdWRlX2Rlc2NlbmRhbnRzOiAnTk9ORSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwoJ2Fzc2V0cy9zdGF0ZScsIG51bGwsIGZhbHNlKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5kb1B1dFJlcXVlc3QodXJsLCB0aGlzLmZvcm1SZXN0UGFyYW1zKHVybFBhcmFtKS5zdWJzdHJpbmcoMSksIHtcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuVVJMRU5DT0RFRFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0sIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3NldEltcG9ydChmaWxlTmFtZSkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGxldCBwYXJhbTtcclxuICAgICAgICBpZiAoZmlsZU5hbWUpIHtcclxuICAgICAgICAgICAgcGFyYW0gPSB0aGlzLmZvcm1SZXN0UGFyYW1zKFt7XHJcbiAgICAgICAgICAgICAgICBrZXk6ICdmaWxlX25hbWUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IGZpbGVOYW1lXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmRvUG9zdFJlcXVlc3QodXJsLCBwYXJhbSwgZmFsc2UsIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3NldFJlbmRpdGlvbihmaWxlLCBpbXBvcnRKb2JJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnJlbmRpdGlvbnNVcmwsIG51bGwsIGZhbHNlKTtcclxuICAgICAgICBjb25zdCBwYXJhbSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X2pvYl9pZCcsIGltcG9ydEpvYklEKTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGUnLCBmaWxlKTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGUubmFtZSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZG9Qb3N0UmVxdWVzdCh1cmwsIHBhcmFtLCBmYWxzZSwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGFzc2V0c1JlbmRpdGlvbihmaWxlcywgaW1wb3J0Sm9iSUQpIHtcclxuICAgICAgICBjb25zdCBwcm9taXNlcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgZmlsZSBvZiBmaWxlcykge1xyXG4gICAgICAgICAgICBwcm9taXNlcy5wdXNoKHRoaXMuYXNzZXRSZW5kaXRpb24oZmlsZSwgaW1wb3J0Sm9iSUQpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgZm9ya0pvaW4ocHJvbWlzZXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXNzZXRDaGVja0luKGFzc2V0RGV0YWlsLCBpbXBvcnRKb2JJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCArICcvJyArIGltcG9ydEpvYklELCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgY29uc3QgcGFyYW0gPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgICBsZXQgYmxvYiA9IG51bGw7XHJcbiAgICAgICAgbGV0IGNvbnRlbnQgPSBudWxsO1xyXG4gICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X2pvYl9pZCcsIGltcG9ydEpvYklEKTtcclxuICAgICAgICBsZXQgdXBsb2FkTWFuaWZlc3QgPSBudWxsO1xyXG4gICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICB1cGxvYWRfbWFuaWZlc3Q6IHtcclxuICAgICAgICAgICAgICAgIG1hc3Rlcl9maWxlczogW3tcclxuICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogYXNzZXREZXRhaWwubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBhc3NldERldGFpbC5hc3NldElkXHJcbiAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGFzc2V0RGV0YWlsLm5hbWUpO1xyXG4gICAgICAgIGNvbnRlbnQgPSB1cGxvYWRNYW5pZmVzdDtcclxuICAgICAgICBibG9iID0gbmV3IEJsb2IoW0pTT04uc3RyaW5naWZ5KGNvbnRlbnQpXSwge1xyXG4gICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcGFyYW0uYXBwZW5kKCdtYW5pZmVzdCcsIGJsb2IpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5kb1B1dFJlcXVlc3QodXJsLCBwYXJhbSwgZmFsc2UsIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWxlUGF0aHMoZmlsZXM6IEFycmF5PGFueT4pIHtcclxuICAgICAgICBjb25zdCBmaWxlUGF0aHMgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWxlc1tpXS5wYXRoKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlUGF0aHMucHVzaChmaWxlc1tpXS5wYXRoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZpbGVQYXRocztcclxuICAgIH1cclxuXHJcbiAgICBmb3JtRG93bmxvYWROYW1lKGRvd25sb2FkVHlwZT8pOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdEb3dubG9hZF8nICsgbmV3IERhdGUoKS50b0pTT04oKS5yZXBsYWNlKC9bLTpdL2csICcnKS5zcGxpdCgnLicpWzBdICsgKGRvd25sb2FkVHlwZSA/ICcuJytkb3dubG9hZFR5cGU6Jy56aXAnKSk7XHJcbiAgICAgICAgcmV0dXJuICdEb3dubG9hZF8nICsgbmV3IERhdGUoKS50b0pTT04oKS5yZXBsYWNlKC9bLTpdL2csICcnKS5zcGxpdCgnLicpWzBdICsgKGRvd25sb2FkVHlwZSA/ICcuJytkb3dubG9hZFR5cGU6Jy56aXAnKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVFeHBvcnRKb2IoYXNzZXRJZHM6IEFycmF5PGFueT4sIGRvd25sb2FkVHlwZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICAgICAgICAgIGNvbnN0IHZlcnNpb24gPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLmFwaVZlcnNpb247XHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IGJhc2VVcmwgKyAnb3RtbWFwaS8nK3ZlcnNpb24rJy9qb2JzL2V4cG9ydHMnO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLlVSTEVOQ09ERUQsXHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgbGV0IGV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgICAgICAgIGlmKGRvd25sb2FkVHlwZS50b1VwcGVyQ2FzZSgpID09IEZpbGVUeXBlcy5aSVApe1xyXG4gICAgICAgICAgICAgZXhwb3J0UmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3JlcXVlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfcHJldmlld19jb250ZW50OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9tYXN0ZXJfY29udGVudDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9zdXBwb3J0aW5nX2NvbnRlbnQ6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZV9jb2xsZWN0aW9uX2ZvbGRlcnM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyeV90ZW1wbGF0ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cmlidXRlX3ZhbHVlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJzInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogNCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdOJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRC5ERUZBVUxUJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybWVyX2lkOiAnQVJURVNJQS5UUkFOU0ZPUk1FUi5QUk9GSUxFLkRPV05MT0FEJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcl9oYW5kbGluZ19hY3Rpb246ICdERUZBVUxUX0hBTkRMSU5HJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X2pvYl90cmFuc2Zvcm1lcnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1lcl9pZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuWklQIENPTVBSRVNTSU9OLkRFRkFVTFQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50czogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Eb3dubG9hZE5hbWUoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc19hc3luY2hyb25vdXNseTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlX3dvcmtpbmdfZGlyZWN0b3J5OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlX2V4cG9ydF9kaXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdyaXRlX3htbDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBleHBvcnRSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50X3JlcXVlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfbWFzdGVyX2NvbnRlbnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3N1cHBvcnRpbmdfY29udGVudDogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVfY29sbGVjdGlvbl9mb2xkZXJzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJ5X3RlbXBsYXRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cmlidXRlX3ZhbHVlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJzInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRC5ERUZBVUxUJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1lcl9pZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcl9oYW5kbGluZ19hY3Rpb246ICdERUZBVUxUX0hBTkRMSU5HJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfYXN5bmNocm9ub3VzbHk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1vdmVfd29ya2luZ19kaXJlY3Rvcnk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlX2V4cG9ydF9kaXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3cml0ZV94bWw6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGlvbkNvbnRleHQgPSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dF9wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbl9jb250ZXh0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5hc3NldC5zZWxlY3Rpb24uQXNzZXRJZHNTZWxlY3Rpb25Db250ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5jbHVkZV9kZXNjZW5kYW50czogJ0FMTCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkX3R5cGU6ICdBU1NFVFMnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gJ2V4cG9ydF9yZXF1ZXN0PScgKyBKU09OLnN0cmluZ2lmeShleHBvcnRSZXF1ZXN0KSArXHJcbiAgICAgICAgICAgICAgICAnJnNlbGVjdGlvbl9jb250ZXh0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc2VsZWN0aW9uQ29udGV4dCkpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5odHRwLnBvc3QodXJsLCBwYXJhbXMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRFeHBvcnRKb2JzKHBhcmFtczogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSAnam9icyc7IC8vIE1TVi0yMzkgLT4gJ2pvYnMvJ1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdCh1cmwsIHBhcmFtcylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T1RNTVN5c3RlbURldGFpbHMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1lTVEVNX0RFVEFJTFMpICE9IG51bGwgKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgb3RtbVZlcnNpb249IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5TWVNURU1fREVUQUlMUykpXHJcbiAgICAgICAgICAgICAgICBpZihvdG1tVmVyc2lvbj09IG51bGwgfHwgb3RtbVZlcnNpb24gPT0gdW5kZWZpbmVkIHx8IHBhcnNlRmxvYXQob3RtbVZlcnNpb24pPCAwKXtcclxuICAgICAgICAgICAgICAgICAgICBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1lTVEVNX0RFVEFJTFMpKS5zeXN0ZW1fZGV0YWlsc19yZXNvdXJjZS5zeXN0ZW1fZGV0YWlsc19tYXAuZW50cnkuZm9yRWFjaChlPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGUua2V5LnRvU3RyaW5nKCkgPT09IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk9UTU1fU1lTVEVNX0RFVEFJTFNfQ09OU1RBTlRTLkJVSUxEX1ZFUlNJT04pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1RNTV9WRVJTSU9OLCBKU09OLnN0cmluZ2lmeShlLnZhbHVlKSlcclxuICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlNZU1RFTV9ERVRBSUxTKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zeXN0ZW1kZXRhaWxzVXJsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2UhPSBudWxsKXtcclxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuc3lzdGVtX2RldGFpbHNfcmVzb3VyY2Uuc3lzdGVtX2RldGFpbHNfbWFwLmVudHJ5LmZvckVhY2goZT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihlLmtleS50b1N0cmluZygpID09PSBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5PVE1NX1NZU1RFTV9ERVRBSUxTX0NPTlNUQU5UUy5CVUlMRF9WRVJTSU9OKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTiwgSlNPTi5zdHJpbmdpZnkoZS52YWx1ZSkpXHJcbiAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5TWVNURU1fREVUQUlMUywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldE9UTU1Bc3NldEJ5SWQoYXNzZXRJZCwgbG9hZFR5cGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBhc3NldERldGFpbHMgPSAnJztcclxuICAgICAgICBjb25zdCBkYXRhTG9hZFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgIGRhdGFfbG9hZF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICBsb2FkX211bHRpbGluZ3VhbF9maWVsZF92YWx1ZXM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3N1YnNjcmliZWRfdG86IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX2Fzc2V0X2NvbnRlbnRfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfbWV0YWRhdGE6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX2luaGVyaXRlZF9tZXRhZGF0YTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfdGh1bWJuYWlsX2luZm86IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3ByZXZpZXdfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfcGRmX3ByZXZpZXdfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfM2RfcHJldmlld19pbmZvOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgbG9hZF9kZXN0aW5hdGlvbl9saW5rczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfc2VjdXJpdHlfcG9saWNpZXM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3BhdGg6IHRydWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogbG9hZFR5cGVcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xldmVsX29mX2RldGFpbCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnc2xpbSdcclxuICAgICAgICB9XTtcclxuICAgICAgICBpZiAobG9hZFR5cGUgPT09ICdjdXN0b20nKSB7XHJcbiAgICAgICAgICAgIHVybFBhcmFtLnB1c2goe1xyXG4gICAgICAgICAgICAgICAga2V5OiAnZGF0YV9sb2FkX3JlcXVlc3QnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShkYXRhTG9hZFJlcXVlc3QpKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkLCB1cmxQYXJhbSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5hc3NldF9yZXNvdXJjZS5hc3NldCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NldERldGFpbHMgPSByZXNwb25zZS5hc3NldF9yZXNvdXJjZS5hc3NldDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhc3NldERldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T1RNTUZvZGxlckJ5SWQoZm9sZGVySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBmb2xkZXJEZXRhaWxzID0gJyc7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ21ldGFkYXRhJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbG9hZF9tdWx0aWxpbmd1YWxfdmFsdWVzJyxcclxuICAgICAgICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGV2ZWxfb2ZfZGV0YWlsJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdzbGltJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnYWZ0ZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2FmdGVyJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGltaXQnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJzUwJ1xyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mb2xkZXJzVXJsICsgZm9sZGVySWQsIHVybFBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmZvbGRlcl9yZXNvdXJjZSAmJiByZXNwb25zZS5mb2xkZXJfcmVzb3VyY2UuZm9sZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbGRlckRldGFpbHMgPSByZXNwb25zZS5mb2xkZXJfcmVzb3VyY2UuZm9sZGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZvbGRlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2VhcmNoT3BlcmF0b3JzTGlzdCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzZWFyY2hPcGVyYXRvckxpc3QgPSAnJztcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuc2VhcmNob3BlcmF0b3JzLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnNlYXJjaF9vcGVyYXRvcnNfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuc2VhcmNoX29wZXJhdG9yc19yZXNvdXJjZS5zZWFyY2hfb3BlcmF0b3JfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hPcGVyYXRvckxpc3QgPSByZXNwb25zZS5zZWFyY2hfb3BlcmF0b3JzX3Jlc291cmNlLnNlYXJjaF9vcGVyYXRvcl9saXN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNlYXJjaE9wZXJhdG9yTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxNZXRhZGF0TW9kZWxMaXN0KCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IG1ldGFEYXRhTW9kZWxMaXN0ID0gJyc7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLm1ldGFkYXRhbW9kZWxzLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhTW9kZWxMaXN0ID0gcmVzcG9uc2UubWV0YWRhdGFfbW9kZWxzX3Jlc291cmNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KG1ldGFEYXRhTW9kZWxMaXN0KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1ldGFkYXRNb2RlbEJ5SWQobW9kZWxJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IG1ldGFEYXRhTW9kZWxEZXRhaWxzID0gJyc7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5tZXRhZGF0YW1vZGVscyArIG1vZGVsSWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWV0YURhdGFNb2RlbERldGFpbHMgPSByZXNwb25zZS5tZXRhZGF0YV9tb2RlbF9yZXNvdXJjZS5tZXRhZGF0YV9tb2RlbDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChtZXRhRGF0YU1vZGVsRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzYXZlU2VhcmNoKHNlYXJjaENvbmZpZywgc2tpcCwgdG9wLCB1c2VyU2Vzc2lvbklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSBvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRTZWFyY2hVcmw7XHJcblxyXG4gICAgICAgIC8qIGlmIChzZWFyY2hDb25maWcuc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICB1cmwgPSBvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRzZWFyY2hlcyArIHNlYXJjaENvbmZpZy5zYXZlZFNlYXJjaElkXHJcbiAgICAgICAgfSAqL1xyXG5cclxuICAgICAgICBjb25zdCBmb3JtRGF0YSA9IFtdO1xyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc2VhcmNoS2V5KSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goc2VhcmNoQ29uZmlnLnNlYXJjaEtleSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc29ydFN0cmluZykge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzb3J0PScgKyBzZWFyY2hDb25maWcuc29ydFN0cmluZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcua2V5d29yZF9xdWVyeSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdrZXl3b3JkX3F1ZXJ5PScgKyBlbmNvZGVVUklDb21wb25lbnQoc2VhcmNoQ29uZmlnLmtleXdvcmRfcXVlcnkpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5sb2FkX3R5cGUgPT09ICdjdXN0b20nKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2RhdGFfbG9hZF9yZXF1ZXN0PScgKyAne1wiZGF0YV9sb2FkX3JlcXVlc3RcIjp7XCJjaGlsZF9jb3VudF9sb2FkX3R5cGVcIjonICsgJ1wiJyArIHNlYXJjaENvbmZpZy5jaGlsZF9jb3VudF9sb2FkX3R5cGUgKyAnXCInICsgJyB9fScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdhZnRlcj0nICsgc2tpcCk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbGltaXQ9JyArIHRvcCk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbXVsdGlsaW5ndWFsX2xhbmd1YWdlX2NvZGU9ZW5fVVMnKTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXIpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZm9sZGVyX2ZpbHRlcj0nICsgc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXJfdHlwZSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmb2xkZXJfZmlsdGVyX3R5cGU9JyArIHNlYXJjaENvbmZpZy5mb2xkZXJfZmlsdGVyX3R5cGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZhY2V0X3Jlc3RyaWN0aW9uX2xpc3QpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZmFjZXRfcmVzdHJpY3Rpb25fbGlzdD0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNlYXJjaENvbmZpZy5mYWNldF9yZXN0cmljdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25maWdfaWQpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnc2VhcmNoX2NvbmZpZ19pZD0nICsgc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25maWdfaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodHlwZW9mIHNlYXJjaENvbmZpZy5pc19mYXZvcml0ZSA9PT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2lzX2Zhdm9yaXRlPScgKyBzZWFyY2hDb25maWcuaXNfZmF2b3JpdGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm5hbWUpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnbmFtZT0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHNlYXJjaENvbmZpZy5uYW1lKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZGVzY3JpcHRpb249JyArIGVuY29kZVVSSUNvbXBvbmVudChzZWFyY2hDb25maWcuZGVzY3JpcHRpb24pKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiBzZWFyY2hDb25maWcuaXNfcHVibGljID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfcHVibGljPScgKyBzZWFyY2hDb25maWcuaXNfcHVibGljKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ3ByZWZlcmVuY2VfaWQ9JyArIChzZWFyY2hDb25maWcucHJlZmVyZW5jZV9pZCA/IHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkIDogJ0FSVEVTSUEuUFJFRkVSRU5DRS5HQUxMRVJZVklFVy5ESVNQTEFZRURfRklFTERTJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm1ldGFkYXRhX3RvX3JldHVybikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdtZXRhZGF0YV90b19yZXR1cm49JyArIHNlYXJjaENvbmZpZy5tZXRhZGF0YV90b19yZXR1cm4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25kaXRpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZGl0aW9uX2xpc3Q9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzZWFyY2hDb25maWcuc2VhcmNoX2NvbmRpdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNhdmVkU2VhcmNoSWQpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaWQ9JyArIHNlYXJjaENvbmZpZy5zYXZlZFNlYXJjaElkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gZm9ybURhdGEuam9pbignJicpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ1gtUmVxdWVzdGVkLUJ5JywgdXNlclNlc3Npb25JZC50b1N0cmluZygpKVxyXG4gICAgICAgICAgICAgICAgLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcpLFxyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8qIGlmIChzZWFyY2hDb25maWcuc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5maXJlUGF0Y2hSZXN0UmVxdWVzdCh1cmwsIHBhcmFtLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJywgaHR0cE9wdGlvbnMpO1xyXG4gICAgICAgIH0gKi9cclxuICAgICAgICByZXR1cm4gdGhpcy5maXJlUG9zdFJlc3RSZXF1ZXN0KHVybCwgcGFyYW0sICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLCBodHRwT3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoQ3VzdG9tVGV4dChzZWFyY2hDb25maWcsIHNraXAsIHRvcCwgdXNlclNlc3Npb25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IHVybCA9IG90bW1TZXJ2aWNlc0NvbnN0YW50cy50ZXh0QXNzZXRTZWFyY2hVcmw7XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5pc1NhdmVkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgIHVybCA9IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zYXZlZFNlYXJjaFVybDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgZm9ybURhdGEgPSBbXTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaEtleSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKHNlYXJjaENvbmZpZy5zZWFyY2hLZXkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNvcnRTdHJpbmcpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnc29ydD0nICsgc2VhcmNoQ29uZmlnLnNvcnRTdHJpbmcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmtleXdvcmRfcXVlcnkpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgna2V5d29yZF9xdWVyeT0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHNlYXJjaENvbmZpZy5rZXl3b3JkX3F1ZXJ5KSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2xvYWRfdHlwZT0nICsgKHNlYXJjaENvbmZpZy5sb2FkX3R5cGUgPyBzZWFyY2hDb25maWcubG9hZF90eXBlIDogJ21ldGFkYXRhJykpO1xyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2xvYWRfbXVsdGlsaW5ndWFsX3ZhbHVlcz10cnVlJyk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbGV2ZWxfb2ZfZGV0YWlsPScgKyAoc2VhcmNoQ29uZmlnLmxldmVsX29mX2RldGFpbCA/IHNlYXJjaENvbmZpZy5sZXZlbF9vZl9kZXRhaWwgOiAnc2xpbScpKTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmxvYWRfdHlwZSA9PT0gJ2N1c3RvbScpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZGF0YV9sb2FkX3JlcXVlc3Q9JyArICd7XCJkYXRhX2xvYWRfcmVxdWVzdFwiOntcImNoaWxkX2NvdW50X2xvYWRfdHlwZVwiOicgKyAnXCInICsgc2VhcmNoQ29uZmlnLmNoaWxkX2NvdW50X2xvYWRfdHlwZSArICdcIicgKyAnIH19Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2FmdGVyPScgKyBza2lwKTtcclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdsaW1pdD0nICsgdG9wKTtcclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdtdWx0aWxpbmd1YWxfbGFuZ3VhZ2VfY29kZT1lbl9VUycpO1xyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmb2xkZXJfZmlsdGVyPScgKyBzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcl90eXBlKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2ZvbGRlcl9maWx0ZXJfdHlwZT0nICsgc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXJfdHlwZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZmFjZXRfcmVzdHJpY3Rpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmYWNldF9yZXN0cmljdGlvbl9saXN0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc2VhcmNoQ29uZmlnLmZhY2V0X3Jlc3RyaWN0aW9uX2xpc3QpKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc2VhcmNoX2NvbmZpZ19pZCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZmlnX2lkPScgKyBzZWFyY2hDb25maWcuc2VhcmNoX2NvbmZpZ19pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuaXNfZmF2b3JpdGUpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfZmF2b3JpdGU9JyArIHNlYXJjaENvbmZpZy5pc19mYXZvcml0ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcubmFtZSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCduYW1lPScgKyBlbmNvZGVVUklDb21wb25lbnQoc2VhcmNoQ29uZmlnLm5hbWUpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5pc19wdWJsaWMpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfcHVibGljPScgKyBzZWFyY2hDb25maWcuaXNfcHVibGljKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ3ByZWZlcmVuY2VfaWQ9JyArIChzZWFyY2hDb25maWcucHJlZmVyZW5jZV9pZCA/IHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkIDogJ0FSVEVTSUEuUFJFRkVSRU5DRS5HQUxMRVJZVklFVy5ESVNQTEFZRURfRklFTERTJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm1ldGFkYXRhX3RvX3JldHVybikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdtZXRhZGF0YV90b19yZXR1cm49JyArIHNlYXJjaENvbmZpZy5tZXRhZGF0YV90b19yZXR1cm4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25kaXRpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZGl0aW9uX2xpc3Q9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzZWFyY2hDb25maWcuc2VhcmNoX2NvbmRpdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW0gPSBmb3JtRGF0YS5qb2luKCcmJyk7XHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpLnNldCgnWC1SZXF1ZXN0ZWQtQnknLCB1c2VyU2Vzc2lvbklkLnRvU3RyaW5nKCkpXHJcbiAgICAgICAgICAgICAgICAuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyksXHJcbiAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmlyZVBvc3RSZXN0UmVxdWVzdCh1cmwsIHBhcmFtLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJywgaHR0cE9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU1ldGFkYXRhKGFzc2V0SWQsIHR5cGUsIG1ldGFkYXRhUGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudFR5cGUgPSAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJztcclxuICAgICAgICAgICAgY29uc3QgbG9ja0Zvcm1EYXRhID0gW107XHJcbiAgICAgICAgICAgIGxvY2tGb3JtRGF0YS5wdXNoKCdhY3Rpb249bG9jaycpO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbSA9IGxvY2tGb3JtRGF0YS5qb2luKCcmJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZVB1dFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy9zdGF0ZScsIHBhcmFtLCBudWxsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb2NrUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVBhdGNoUmVzdFJlcXVlc3QoKCh0eXBlID09PSAnRk9MREVSJyA/IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mb2xkZXJzVXJsIDogKG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycpKSArIGFzc2V0SWQpLCBtZXRhZGF0YVBhcmFtZXRlcnMsIG51bGwsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdW5sb2NrRm9ybURhdGEgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVubG9ja0Zvcm1EYXRhLnB1c2goJ2FjdGlvbj11bmxvY2snKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHVubG9ja0Zvcm1EYXRhLmpvaW4oJyYnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVB1dFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy9zdGF0ZScsIHBhcmFtZXRlciwgbnVsbCwgbnVsbClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVubG9ja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1bmxvY2tSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsU2F2ZWRTZWFyY2hlcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzYXZlZHNlYXJjaGVzID0gW107XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnNhdmVkc2VhcmNoZXMsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoZXNfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoZXNfcmVzb3VyY2Uuc2F2ZWRfc2VhcmNoX2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZWRzZWFyY2hlcyA9IHJlc3BvbnNlLnNhdmVkX3NlYXJjaGVzX3Jlc291cmNlLnNhdmVkX3NlYXJjaF9saXN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNhdmVkc2VhcmNoZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2F2ZWRTZWFyY2hCeUlkKGlkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzYXZlZHNlYXJjaCA9IHt9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zYXZlZHNlYXJjaGVzICsgaWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoX3Jlc291cmNlICYmIHJlc3BvbnNlLnNhdmVkX3NlYXJjaF9yZXNvdXJjZS5zYXZlZF9zZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZWRzZWFyY2ggPSByZXNwb25zZS5zYXZlZF9zZWFyY2hfcmVzb3VyY2Uuc2F2ZWRfc2VhcmNoO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNhdmVkc2VhcmNoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVNhdmVkU2VhcmNoKGlkOiBhbnkpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVEZWxldGVSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRzZWFyY2hlcyArIGlkLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRSZWxhdGlvbmFsT3BlcmF0b3Ioc2VhcmNoQ29uZGl0aW9uczogYW55W10pIHtcclxuICAgICAgICBzZWFyY2hDb25kaXRpb25zLmZvckVhY2goc2VhcmNoQ29uZGl0aW9uID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlYXJjaENvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yICYmIHNlYXJjaENvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBzZWFyY2hDb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvciA9ICdhbmQnO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gc2VhcmNoQ29uZGl0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldFZlcnNpb25zQnlBc3NldElkKGFzc2V0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBhc3NldFZlcnNpb25EZXRhaWxzID0gJyc7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ21ldGFkYXRhJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGV2ZWxfb2ZfZGV0YWlsJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdzbGltJ1xyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy92ZXJzaW9ucycsIHVybFBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmFzc2V0c19yZXNvdXJjZSAmJiByZXNwb25zZS5hc3NldHNfcmVzb3VyY2UuYXNzZXRfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NldFZlcnNpb25EZXRhaWxzID0gcmVzcG9uc2UuYXNzZXRzX3Jlc291cmNlLmFzc2V0X2xpc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYXNzZXRWZXJzaW9uRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldEJ5QXNzZXRJZChhc3NldElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBsZXQgYXNzZXRWZXJzaW9uRGV0YWlscyA9ICcnO1xyXG4gICAgICAgIGNvbnN0IHVybFBhcmFtID0gW3tcclxuICAgICAgICAgICAga2V5OiAnbG9hZF90eXBlJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdtZXRhZGF0YSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xldmVsX29mX2RldGFpbCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnc2xpbSdcclxuICAgICAgICB9XTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuYXNzc2V0VXJsICsgJy8nICsgYXNzZXRJZCwgdXJsUGFyYW0pXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuYXNzZXRfcmVzb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRWZXJzaW9uRGV0YWlscyA9IHJlc3BvbnNlLmFzc2V0X3Jlc291cmNlLmFzc2V0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGFzc2V0VmVyc2lvbkRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0c2VhcmNoY29uZmlndXJhdGlvbnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICAgICAga2V5OiAncmV0cmlldmFsX3R5cGUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdmdWxsJ1xyXG4gICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnNlYXJjaGNvbmZpZ3VyYXRpb25zLCB1cmxQYXJhbSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsTG9va3VwRG9tYWlucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBsb29rdXBEb21haW5zID0gW107XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmxvb2t1cGRvbWFpbnMsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbnNfcmVzb3VyY2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbnNfcmVzb3VyY2UubG9va3VwX2RvbWFpbnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9va3VwRG9tYWlucyA9IHJlc3BvbnNlLmxvb2t1cF9kb21haW5zX3Jlc291cmNlLmxvb2t1cF9kb21haW5zO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGxvb2t1cERvbWFpbnMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TG9va3VwRG9tYWluKGxvb2t1cERvbWFpbklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBsb29rdXBEb21haW5WYWx1ZXMgPSBbXTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMubG9va3VwZG9tYWlucyArIGxvb2t1cERvbWFpbklkLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmxvb2t1cF9kb21haW5fcmVzb3VyY2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbl9yZXNvdXJjZS5sb29rdXBfZG9tYWluICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmxvb2t1cF9kb21haW5fcmVzb3VyY2UubG9va3VwX2RvbWFpbi5kb21haW5WYWx1ZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9va3VwRG9tYWluVmFsdWVzID0gcmVzcG9uc2UubG9va3VwX2RvbWFpbl9yZXNvdXJjZS5sb29rdXBfZG9tYWluLmRvbWFpblZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChsb29rdXBEb21haW5WYWx1ZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmFjZXRDb25maWd1cmF0aW9ucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mYWNldGNvbmZpZ3VyYXRpb25zLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGZhY2V0Q29uZmlndXJhdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5mYWNldF9jb25maWd1cmF0aW9uc19yZXNvdXJjZSAmJlxyXG4gICAgICAgICAgICAgICAgICAgIEFycmF5KHJlc3BvbnNlLmZhY2V0X2NvbmZpZ3VyYXRpb25zX3Jlc291cmNlLmZhY2V0X2NvbmZpZ3VyYXRpb25fbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmYWNldENvbmZpZ3VyYXRpb25zID0gcmVzcG9uc2UuZmFjZXRfY29uZmlndXJhdGlvbnNfcmVzb3VyY2UuZmFjZXRfY29uZmlndXJhdGlvbl9saXN0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWNldENvbmZpZ3VyYXRpb25zKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgZ2V0dGluZyBmYWNldCBjb25maWd1cmF0aW9ucycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=