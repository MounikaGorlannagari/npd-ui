import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
let SharingService = class SharingService {
    constructor() {
        this.recentSearches = [];
        this.lookupDomains = [];
        this.taskEvents = [];
        this.taskWorkflowActions = [];
        this.allApplicationFields = [];
        this.allApplicationViewConfig = [];
        this.persons = [];
        this.categoryMetadata = [];
        this.currentUserPreference = [];
        this.userPreferences = [];
        this.subscribableBrandConfig = new BehaviorSubject('');
        this.resetView = new BehaviorSubject('');
        this.notificationPass = new BehaviorSubject('');
        this.isMenuChanged = new BehaviorSubject(false);
    }
    setMenuChanged(menuchanged) {
        this.isMenuChanged.next(menuchanged);
    }
    getMenuChanged() {
        return this.isMenuChanged.asObservable();
    }
    unsubscribeMenuChangeObserable() {
        return this.isMenuChanged.unsubscribe();
    }
    getNotificationDetails(data) {
        this.notificationPass.next(data);
    }
    getCategoryMetadata() {
        return this.categoryMetadata;
    }
    setCategoryMetadata(categoryMetadata) {
        this.categoryMetadata = categoryMetadata;
    }
    getdisplayName() {
        return this.displayName;
    }
    getcurrentUserItemID() {
        return this.currentUserItemID;
    }
    setCurrentUserObject(currentUserObject) {
        this.currentUserObject = currentUserObject;
    }
    getCurrentUserObject() {
        return this.currentUserObject;
    }
    setCurrentUserID(currentUserId) {
        this.currentUserID = currentUserId;
    }
    getCurrentUserID() {
        return this.currentUserID;
    }
    getCurrentUserItemID() {
        return this.currentUserItemID;
    }
    setCurrentUserItemID(currentUserItemId) {
        this.currentUserItemID = currentUserItemId;
    }
    setCurrentUserDN(currentUserDN) {
        this.currentUserDN = currentUserDN;
    }
    getCurrentUserDN() {
        return this.currentUserDN;
    }
    setCurrentUserDisplayName(displayName) {
        this.displayName = displayName;
    }
    getCurrentOrgDN() {
        return this.currentOrgDN;
    }
    setCurrentOrgDN(orgDN) {
        this.currentOrgDN = orgDN;
        if (this.currentOrgDN && typeof this.currentOrgDN === 'string') {
            const instanceIdentifier = this.currentOrgDN.split(',');
            this.instanceIdentifier = instanceIdentifier.slice(1, instanceIdentifier.length).join(',');
        }
    }
    setCurrentUserEmailId(emailId) {
        this.currentUserEmailId = emailId;
    }
    getCurrentUserEmailId() {
        return this.currentUserEmailId;
    }
    getInstanceIdentifier() {
        return this.instanceIdentifier;
    }
    getCurrentUserDisplayName() {
        return this.displayName;
    }
    getCurrentUserCN() {
        if (this.currentUserDN) {
            return this.currentUserDN.split(',')[0].split('=')[1];
        }
        else {
            return '';
        }
    }
    setRecentSearch(recentSearch) {
        this.recentSearches.unshift(recentSearch);
    }
    getRecentSearches() {
        return this.recentSearches;
    }
    getRecentSearchesByView(view) {
        return this.recentSearches.filter(search => search[0].view === view || typeof search === 'string');
    }
    setMenu(menuList) {
        this.menu = menuList;
    }
    getAllMenu() {
        return this.menu;
    }
    getMenuByType(type) {
        return this.menu.filter(tmpMenu => tmpMenu.TYPE === type);
    }
    setTaskEvents(events) {
        if (events && events.length > 0) {
            this.taskEvents = events;
        }
        else if (events) {
            this.taskEvents = [events];
        }
        else {
            this.taskEvents = [];
        }
    }
    getTaskEvents() {
        return this.taskEvents;
    }
    setTaskWorkflowActions(workflowActions) {
        if (workflowActions && workflowActions.length > 0) {
            this.taskWorkflowActions = workflowActions;
        }
        else if (workflowActions) {
            this.taskWorkflowActions = [workflowActions];
        }
        else {
            this.taskWorkflowActions = [];
        }
    }
    getTaskWorkflowActions() {
        return this.taskWorkflowActions;
    }
    setAllCategories(categories) {
        this.allCategories = categories;
    }
    getAllCategories() {
        return this.allCategories;
    }
    /* setDefaultCategoryDetails(category) {
        this.defaultCategoryDetails = category;
    }

    setCategoryDetails(category) {
        this.categoryDetails.push(category);
    }

    getCategoryDetailsById(categoryId) {
        return this.categoryDetails.find(category => category.id === categoryId);
    }

    getDefaultCategoryDetails() {
        return this.defaultCategoryDetails;
    } */
    setSelectedCategory(selectedCategory) {
        this.selectedCategory = selectedCategory;
    }
    getSelectedCategory() {
        return this.selectedCategory;
    }
    setAllLookupDomains(lookupDomains) {
        this.lookupDomains = lookupDomains;
    }
    getAllLookupDomains() {
        return this.lookupDomains;
    }
    setAllRoles(roles) {
        this.roles = roles;
    }
    getAllRoles() {
        return this.roles;
    }
    setUserRoles(roles) {
        this.userRoles = roles;
    }
    getUserRoles() {
        return this.userRoles;
    }
    getRoleByName(roleName) {
        return this.roles.find(role => role.ROLE_NAME === roleName);
    }
    setMediaManagerConfig(mediaManagerConfig) {
        this.mediaManagerConfig = mediaManagerConfig;
    }
    getMediaManagerConfig() {
        return this.mediaManagerConfig;
    }
    setCategoryLevels(categoryLevels) {
        this.categoryLevels = categoryLevels;
    }
    getCategoryLevels() {
        return this.categoryLevels ? this.categoryLevels : [];
    }
    getCategoryLevelsById() {
        let arr_categoryLevel = [];
        this.categoryLevels.forEach(categoryLevel => {
            arr_categoryLevel.push({
                id: categoryLevel['MPM_Category_Level-id'].Id,
                categoryLevelType: categoryLevel.CATEGORY_LEVEL_TYPE
            });
        });
        return arr_categoryLevel;
    }
    setAllPriorities(allPriorities) {
        this.allPriorities = allPriorities;
    }
    getAllPriorities() {
        return this.allPriorities;
    }
    setCRActions(actions) {
        this.crActions = actions ? JSON.parse(JSON.stringify(actions)) : [];
    }
    getCRActions() {
        return this.crActions ? this.crActions : [];
    }
    setAllStatusConfig(statusList) {
        this.allStatusConfig = statusList;
    }
    getAllStatusConfig() {
        return this.allStatusConfig;
    }
    setAllApplicationFields(otmmFields) {
        this.allApplicationFields = otmmFields ? otmmFields : [];
    }
    getAllApplicationFields() {
        return (this.allApplicationFields && this.allApplicationFields.length > 0) ? this.allApplicationFields : null;
    }
    setAllApplicationViewConfig(viewConfigs) {
        this.allApplicationViewConfig = viewConfigs ? viewConfigs : [];
    }
    getAllApplicationViewConfig() {
        return (this.allApplicationViewConfig && this.allApplicationViewConfig.length > 0) ? this.allApplicationViewConfig : null;
    }
    setProjectConfig(data) {
        this.projectConfig = data;
    }
    getProjectConfig() {
        return this.projectConfig;
    }
    setBrandConfig(data) {
        this.brandConfig = data;
        this.subscribableBrandConfig.next(data);
    }
    getBrandConfig() {
        return this.brandConfig;
    }
    setAllViewConfig(data) {
        this.viewConfig = data;
    }
    getAllViewConfig() {
        return this.viewConfig;
    }
    getViewConfigById(id) {
        return this.viewConfig.find(view => view['MPM_View_Config-id'].Id === id);
    }
    getViewConfigByName(viewName) {
        const selectedViewConfig = this.viewConfig.find(view => view.VIEW === viewName);
        return selectedViewConfig ? selectedViewConfig : null;
    }
    getSubscribableBrandConfig() {
        return this.subscribableBrandConfig.asObservable();
    }
    resetViewData() {
        return this.resetView.asObservable();
    }
    onResetView(viewId) {
        this.resetView.next(viewId);
    }
    setAppConfig(data) {
        this.appConfig = data;
    }
    getAppConfig() {
        return this.appConfig;
    }
    setCommentConfig(data) {
        this.commentConfig = data;
    }
    getCommentConfig() {
        return this.commentConfig;
    }
    setAssetConfig(data) {
        this.assetConfig = data;
    }
    getAssetConfig() {
        return this.assetConfig;
    }
    setAllTeamRoles(data) {
        console.log(data);
        this.teamRoles = data;
        console.log(this.teamRoles);
    }
    getAllTeamRoles() {
        return this.teamRoles;
    }
    setAllTeams(data) {
        if (Array.isArray(data)) {
            this.teams = data;
        }
        else {
            this.teams = [data];
        }
    }
    getAllTeams() {
        return this.teams;
    }
    setAllPersons(data) {
        this.persons = data;
    }
    getAllPersons() {
        return this.persons;
    }
    setCurrentPerson(data) {
        this.currentPerson = data;
    }
    getCurrentPerson() {
        return this.currentPerson;
    }
    getDefaultEmailConfig() {
        return this.emailConfig;
    }
    setDefaultEmailConfig(data) {
        this.emailConfig = data;
        sessionStorage.setItem('EMAIL_CONFIG', JSON.stringify(this.emailConfig));
    }
    updateCurrentUserPreference(userPreferenceData) {
        console.log(userPreferenceData);
        if (Array.isArray(userPreferenceData)) {
            userPreferenceData.forEach(userPreference => {
                const selectedUserPreferenceIndex = this.currentUserPreference.findIndex(selectedUserPreference => selectedUserPreference['MPM_User_Preference-id'].Id === userPreference['MPM_User_Preference-id'].Id);
                if (selectedUserPreferenceIndex >= 0) {
                    this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
                }
                this.currentUserPreference.push(userPreference);
            });
        }
        else {
            const selectedUserPreferenceIndex = this.currentUserPreference.findIndex(userPreference => userPreference['MPM_User_Preference-id'].Id === userPreferenceData['MPM_User_Preference-id'].Id);
            if (selectedUserPreferenceIndex >= 0) {
                this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
            }
            this.currentUserPreference.push(userPreferenceData);
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    }
    removeCurrentUserPreference(userPreferenceId) {
        const selectedUserPreferenceIndex = this.currentUserPreference.findIndex(userPreference => userPreference['MPM_User_Preference-id'].Id === userPreferenceId);
        if (selectedUserPreferenceIndex >= 0) {
            this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    }
    setCurrentUserPreference(data) {
        if (Array.isArray(data)) {
            this.currentUserPreference = data;
        }
        else {
            this.currentUserPreference = [data];
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    }
    getCurrentUserPreference() {
        return this.currentUserPreference;
    }
    getCurrentUserPreferenceByViewId(viewId) {
        console.log(this.currentUserPreference);
        if (this.userPreferences.length <= 0 && this.currentUserPreference.length > 0) {
            this.userPreferences.push(this.currentUserPreference);
        }
        if (this.currentUserPreference && this.currentUserPreference.length > 0) {
            return this.currentUserPreference.find(userPreference => userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId);
        }
        else {
            return '';
        }
    }
    getUserPreferencesByViewId() {
        return this.userPreferences;
    }
    getDefaultCurrentUserPreferenceByMenu(menuId) {
        return this.currentUserPreference.find(userPreference => (userPreference.IS_DEFAULT_VIEW === 'true' || userPreference.IS_DEFAULT_VIEW === true)
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId);
    }
    getCurrentUserPreferenceByViewMenu(menuId, viewId) {
        return this.currentUserPreference.find(userPreference => userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId);
    }
    getDefaultCurrentUserPreferenceByListFilter(menuId, taskName) {
        return this.currentUserPreference.find(userPreference => (userPreference.IS_DEFAULT_VIEW === 'true' || userPreference.IS_DEFAULT_VIEW === true)
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName);
    }
    getCurrentUserPreferenceByList(menuId, taskName) {
        return this.currentUserPreference.find(userPreference => (userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName));
    }
    getCurrentUserPreferenceByListView(menuId, taskName, viewId) {
        return this.currentUserPreference.find(userPreference => (userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName
            && userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId));
    }
    setCampaignRole(data) {
        this.campaignRole = data;
    }
    getCampaignRole() {
        return this.campaignRole;
    }
    setReviewerRole(data) {
        this.reviewerRole = data;
    }
    getReviewerRole() {
        return this.reviewerRole;
    }
    setIsReviewer(data) {
        this.isReviewer = data;
    }
    getIsReviewer() {
        return this.isReviewer;
    }
    setApproverRole(data) {
        this.approverRole = data;
    }
    getApproverRole() {
        return this.approverRole;
    }
    setIsApprover(data) {
        this.isApprover = data;
    }
    getIsApprover() {
        return this.isApprover;
    }
    setManagerRole(data) {
        this.ManagerRole = data;
    }
    getManagerRole() {
        return this.ManagerRole;
    }
    setIsManager(data) {
        this.isManager = data;
    }
    getIsManager() {
        return this.isManager;
    }
    /* setCampaignManagerRole(data): any {
        this.campaignManagerRole = data;
    }

    getCampaignManagerRole(): any {
        return this.campaignManagerRole;
    }

    setIsCampaignManager(data): any {
        this.isCampaignManager = data;
    }

    getIsCampaignManager(): any {
        return this.isCampaignManager;
    } */ // - MVSS-128
    setMemberRole(data) {
        this.memberRole = data;
    }
    getMemberRole() {
        return this.memberRole;
    }
    setIsMember(data) {
        this.isMember = data;
    }
    getIsMember() {
        return this.isMember;
    }
    setIsUploaded(data) {
        this.isUploaded = data;
    }
    getIsUploaded() {
        return this.isUploaded;
    }
    getTeamRoleIdByRoleDN(roleDN) {
        this.getAllTeams().subscribe(allTeams => {
            console.log(allTeams);
        });
    }
    setCurrentMenu(menu) {
        this.currentMenu = menu;
    }
    getCurrentMenu() {
        return this.currentMenu;
    }
};
SharingService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SharingService_Factory() { return new SharingService(); }, token: SharingService, providedIn: "root" });
SharingService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SharingService);
export { SharingService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQVl0RCxPQUFPLEVBQWMsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOztBQU0zRixJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBNER2QjtRQWxETyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUluQixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQU96Qix5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLDZCQUF3QixHQUFlLEVBQUUsQ0FBQztRQVUxQyxZQUFPLEdBQVEsRUFBRSxDQUFDO1FBWWxCLHFCQUFnQixHQUFlLEVBQUUsQ0FBQztRQUdsQywwQkFBcUIsR0FBZSxFQUFFLENBQUM7UUFPdkMsb0JBQWUsR0FBZSxFQUFFLENBQUM7UUFJcEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksZUFBZSxDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxlQUFlLENBQU0sRUFBRSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksZUFBZSxDQUFNLEVBQUUsQ0FBQyxDQUFBO1FBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7SUFFN0QsQ0FBQztJQUVELGNBQWMsQ0FBQyxXQUFXO1FBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxjQUFjO1FBQ1YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFBO0lBQzVDLENBQUM7SUFFRCw4QkFBOEI7UUFDMUIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFBO0lBQzNDLENBQUM7SUFHRCxzQkFBc0IsQ0FBQyxJQUFJO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDcEMsQ0FBQztJQUVELG1CQUFtQjtRQUNmLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxnQkFBNEI7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDO0lBQzdDLENBQUM7SUFFRCxjQUFjO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxvQkFBb0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDbEMsQ0FBQztJQUVELG9CQUFvQixDQUFDLGlCQUE4QjtRQUMvQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUM7SUFDL0MsQ0FBQztJQUVELG9CQUFvQjtRQUNoQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztJQUNsQyxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsYUFBcUI7UUFDbEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7SUFDdkMsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQsb0JBQW9CO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ2xDLENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxpQkFBeUI7UUFDMUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDO0lBQy9DLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxhQUFxQjtRQUNsQyxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztJQUN2QyxDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyxXQUFtQjtRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDO0lBRUQsZUFBZTtRQUNYLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsZUFBZSxDQUFDLEtBQWE7UUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLE9BQU8sSUFBSSxDQUFDLFlBQVksS0FBSyxRQUFRLEVBQUU7WUFDNUQsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDOUY7SUFDTCxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBZTtRQUNqQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDO0lBQ3RDLENBQUM7SUFFRCxxQkFBcUI7UUFDakIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbkMsQ0FBQztJQUVELHFCQUFxQjtRQUNqQixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNuQyxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3pEO2FBQU07WUFDSCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0wsQ0FBQztJQUVELGVBQWUsQ0FBQyxZQUFZO1FBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxpQkFBaUI7UUFDYixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELHVCQUF1QixDQUFDLElBQUk7UUFDeEIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZHLENBQUM7SUFFRCxPQUFPLENBQUMsUUFBd0I7UUFDNUIsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFpQjtRQUMzQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsYUFBYSxDQUFDLE1BQU07UUFDaEIsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUM7U0FDNUI7YUFBTSxJQUFJLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsc0JBQXNCLENBQUMsZUFBZTtRQUNsQyxJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMvQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDO1NBQzlDO2FBQU0sSUFBSSxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDaEQ7YUFBTTtZQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7U0FDakM7SUFDTCxDQUFDO0lBRUQsc0JBQXNCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO0lBQ3BDLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxVQUEyQjtRQUN4QyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7UUFjSTtJQUVKLG1CQUFtQixDQUFDLGdCQUEwQjtRQUMxQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDN0MsQ0FBQztJQUVELG1CQUFtQjtRQUNmLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxhQUFhO1FBQzdCLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxtQkFBbUI7UUFDZixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFtQjtRQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQsV0FBVztRQUNQLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQW1CO1FBQzVCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCxZQUFZO1FBQ1IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRCxhQUFhLENBQUMsUUFBa0I7UUFDNUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssUUFBUSxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVELHFCQUFxQixDQUFDLGtCQUFzQztRQUN4RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7SUFDakQsQ0FBQztJQUVELHFCQUFxQjtRQUNqQixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNuQyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsY0FBb0M7UUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7SUFDekMsQ0FBQztJQUVELGlCQUFpQjtRQUNiLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzFELENBQUM7SUFFRCxxQkFBcUI7UUFDakIsSUFBSSxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDeEMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO2dCQUNuQixFQUFFLEVBQUUsYUFBYSxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRTtnQkFDN0MsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLG1CQUFtQjthQUN2RCxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8saUJBQWlCLENBQUM7SUFDN0IsQ0FBQztJQUVELGdCQUFnQixDQUFDLGFBQXlCO1FBQ3RDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxnQkFBZ0I7UUFDWixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELFlBQVksQ0FBQyxPQUFtQjtRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ2hELENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxVQUF5QjtRQUN4QyxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztJQUN0QyxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxVQUEyQjtRQUMvQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUM3RCxDQUFDO0lBRUQsdUJBQXVCO1FBQ25CLE9BQU8sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDbEgsQ0FBQztJQUVELDJCQUEyQixDQUFDLFdBQThCO1FBQ3RELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ25FLENBQUM7SUFFRCwyQkFBMkI7UUFDdkIsT0FBTyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUM5SCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsSUFBSTtRQUNqQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCxjQUFjLENBQUMsSUFBSTtRQUNmLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELGNBQWM7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVELGdCQUFnQixDQUFDLElBQUk7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsaUJBQWlCLENBQUMsRUFBRTtRQUNoQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxRQUFRO1FBQ3hCLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDO1FBQ2hGLE9BQU8sa0JBQWtCLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDMUQsQ0FBQztJQUVELDBCQUEwQjtRQUN0QixPQUFPLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN2RCxDQUFDO0lBRUQsYUFBYTtRQUNULE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQsV0FBVyxDQUFDLE1BQU07UUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsWUFBWSxDQUFDLElBQUk7UUFDYixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQsZ0JBQWdCLENBQUMsSUFBSTtRQUNqQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCxjQUFjLENBQUMsSUFBSTtRQUNmLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCxjQUFjO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxlQUFlLENBQUMsSUFBSTtRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxlQUFlO1FBQ1gsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRCxXQUFXLENBQUMsSUFBSTtRQUNaLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFJO1FBQ2QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELGdCQUFnQixDQUFDLElBQUk7UUFDakIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7SUFDOUIsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQscUJBQXFCO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQscUJBQXFCLENBQUMsSUFBSTtRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFRCwyQkFBMkIsQ0FBQyxrQkFBa0I7UUFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBQy9CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1lBQ25DLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDeEMsTUFBTSwyQkFBMkIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsQ0FDOUYsc0JBQXNCLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3pHLElBQUksMkJBQTJCLElBQUksQ0FBQyxFQUFFO29CQUNsQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLDJCQUEyQixFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUNyRTtnQkFDRCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILE1BQU0sMkJBQTJCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUN0RixjQUFjLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFLEtBQUssa0JBQWtCLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyRyxJQUFJLDJCQUEyQixJQUFJLENBQUMsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQywyQkFBMkIsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNyRTtZQUNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUN2RDtRQUNELGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO0lBQzVILENBQUM7SUFFRCwyQkFBMkIsQ0FBQyxnQkFBZ0I7UUFDeEMsTUFBTSwyQkFBMkIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQ3RGLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3RFLElBQUksMkJBQTJCLElBQUksQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsMkJBQTJCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDckU7UUFDRCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM1SCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsSUFBSTtRQUN6QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztTQUNyQzthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7UUFDRCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM1SCxDQUFDO0lBRUQsd0JBQXdCO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO0lBQ3RDLENBQUM7SUFFRCxnQ0FBZ0MsQ0FBQyxNQUFNO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDeEMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7U0FDekQ7UUFDRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyRSxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLENBQUM7U0FDckk7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsMEJBQTBCO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoQyxDQUFDO0lBRUQscUNBQXFDLENBQUMsTUFBTTtRQUN4QyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxlQUFlLEtBQUssTUFBTSxJQUFJLGNBQWMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDO2VBQ3hJLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELGtDQUFrQyxDQUFDLE1BQU0sRUFBRSxNQUFNO1FBQzdDLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNO2VBQ3pILGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELDJDQUEyQyxDQUFDLE1BQU0sRUFBRSxRQUFRO1FBQ3hELE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLGVBQWUsS0FBSyxNQUFNLElBQUksY0FBYyxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUM7ZUFDeEksY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNO2VBQzVELGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELDhCQUE4QixDQUFDLE1BQU0sRUFBRSxRQUFRO1FBQzNDLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNO2VBQy9HLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsa0NBQWtDLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxNQUFNO1FBQ3ZELE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNO2VBQy9HLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUTtlQUN2QyxjQUFjLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNyRixDQUFDO0lBRUQsZUFBZSxDQUFDLElBQUk7UUFDaEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDN0IsQ0FBQztJQUVELGVBQWU7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFJO1FBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCxlQUFlO1FBQ1gsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCxhQUFhLENBQUMsSUFBSTtRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFFRCxhQUFhO1FBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7SUFFRCxlQUFlLENBQUMsSUFBSTtRQUNoQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUQsZUFBZTtRQUNYLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsYUFBYSxDQUFDLElBQUk7UUFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUMzQixDQUFDO0lBRUQsYUFBYTtRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsY0FBYyxDQUFDLElBQUk7UUFDZixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDO0lBRUQsY0FBYztRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQsWUFBWSxDQUFDLElBQUk7UUFDYixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7O1FBY0ksQ0FBQyxhQUFhO0lBRWxCLGFBQWEsQ0FBQyxJQUFJO1FBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELFdBQVcsQ0FBQyxJQUFJO1FBQ1osSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDekIsQ0FBQztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFJO1FBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELHFCQUFxQixDQUFDLE1BQU07UUFDeEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFCLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQztJQUVELGNBQWM7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztDQUVKLENBQUE7O0FBMXBCWSxjQUFjO0lBSjFCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxjQUFjLENBMHBCMUI7U0ExcEJZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVXNlckRldGFpbHMgfSBmcm9tICcuLi9vYmplY3RzL1VzZXJEZXRhaWxzJztcclxuaW1wb3J0IHsgTVBNTWVudVR5cGUgfSBmcm9tICcuLi9vYmplY3RzL01lbnVUeXBlcyc7XHJcbmltcG9ydCB7IFJvbGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9Sb2xlcyc7XHJcbmltcG9ydCB7IE1lZGlhTWFuYWdlckNvbmZpZyB9IGZyb20gJy4uL29iamVjdHMvTWVkaWFNYW5hZ2VyQ29uZmlnJztcclxuaW1wb3J0IHsgQ2F0ZWdvcnkgfSBmcm9tICcuLi9vYmplY3RzL0NhdGVnb3J5JztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlMZXZlbCB9IGZyb20gJy4uL29iamVjdHMvQ2F0ZWdvcnlMZXZlbCc7XHJcbmltcG9ydCB7IE1QTV9ST0xFIH0gZnJvbSAnLi4vb2JqZWN0cy9Sb2xlJztcclxuaW1wb3J0IHsgTVBNTWVudSB9IGZyb20gJy4uL29iamVjdHMvTWVudSc7XHJcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gJy4uL29iamVjdHMvU3RhdHVzJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyaW5nU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIGN1cnJlbnRVc2VyT2JqZWN0OiBVc2VyRGV0YWlscztcclxuICAgIHB1YmxpYyBjdXJyZW50VXNlcklEOiBzdHJpbmc7XHJcbiAgICBwdWJsaWMgY3VycmVudFVzZXJJdGVtSUQ6IHN0cmluZztcclxuICAgIHB1YmxpYyBjdXJyZW50VXNlckROOiBzdHJpbmc7XHJcbiAgICBwdWJsaWMgZGlzcGxheU5hbWU6IHN0cmluZztcclxuICAgIHB1YmxpYyBjdXJyZW50T3JnRE46IHN0cmluZztcclxuICAgIHB1YmxpYyBjdXJyZW50VXNlckVtYWlsSWQ6IHN0cmluZztcclxuICAgIHB1YmxpYyBpbnN0YW5jZUlkZW50aWZpZXI6IHN0cmluZztcclxuICAgIHB1YmxpYyByZWNlbnRTZWFyY2hlcyA9IFtdO1xyXG4gICAgcHVibGljIG1lbnU6IEFycmF5PE1QTU1lbnU+O1xyXG4gICAgcHVibGljIGxvb2t1cERvbWFpbnMgPSBbXTtcclxuICAgIHB1YmxpYyByb2xlczogQXJyYXk8Um9sZXM+O1xyXG4gICAgcHVibGljIG1lZGlhTWFuYWdlckNvbmZpZzogTWVkaWFNYW5hZ2VyQ29uZmlnO1xyXG4gICAgcHVibGljIGFsbENhdGVnb3JpZXM6IEFycmF5PENhdGVnb3J5PjtcclxuICAgIHB1YmxpYyB0YXNrRXZlbnRzID0gW107XHJcbiAgICBwdWJsaWMgdGFza1dvcmtmbG93QWN0aW9ucyA9IFtdO1xyXG4gICAgcHVibGljIHNlbGVjdGVkQ2F0ZWdvcnk6IENhdGVnb3J5O1xyXG4gICAgcHVibGljIGNhdGVnb3J5TGV2ZWxzOiBBcnJheTxDYXRlZ29yeUxldmVsPjtcclxuICAgIHB1YmxpYyBhbGxQcmlvcml0aWVzOiBBcnJheTxhbnk+O1xyXG4gICAgcHVibGljIHVzZXJSb2xlczogQXJyYXk8Um9sZXM+O1xyXG4gICAgcHVibGljIGNyQWN0aW9uczogYW55O1xyXG4gICAgcHVibGljIGFsbFN0YXR1c0NvbmZpZzogQXJyYXk8U3RhdHVzPjtcclxuICAgIHB1YmxpYyBhbGxBcHBsaWNhdGlvbkZpZWxkczogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcbiAgICBwdWJsaWMgYWxsQXBwbGljYXRpb25WaWV3Q29uZmlnOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICBwdWJsaWMgcHJvamVjdENvbmZpZzogYW55O1xyXG4gICAgcHVibGljIGFwcENvbmZpZzogYW55O1xyXG4gICAgcHVibGljIGFzc2V0Q29uZmlnOiBhbnk7XHJcbiAgICBwdWJsaWMgYnJhbmRDb25maWc6IGFueTtcclxuICAgIHB1YmxpYyBjb21tZW50Q29uZmlnOiBhbnk7XHJcbiAgICBwdWJsaWMgdmlld0NvbmZpZzogYW55O1xyXG4gICAgcHVibGljIHRlYW1Sb2xlczogYW55O1xyXG4gICAgcHVibGljIGNhbXBhaWduUm9sZTogYW55O1xyXG4gICAgcHVibGljIHRlYW1zOiBhbnk7XHJcbiAgICBwdWJsaWMgcGVyc29uczogYW55ID0gW107XHJcbiAgICBwdWJsaWMgcmV2aWV3ZXJSb2xlOiBhbnk7XHJcbiAgICBwdWJsaWMgaXNSZXZpZXdlcjogYW55O1xyXG4gICAgcHVibGljIGFwcHJvdmVyUm9sZTogYW55O1xyXG4gICAgcHVibGljIGlzQXBwcm92ZXI6IGFueTtcclxuICAgIHB1YmxpYyBNYW5hZ2VyUm9sZTogYW55O1xyXG4gICAgLy8gcHVibGljIGNhbXBhaWduTWFuYWdlclJvbGU6IGFueTsgLSBNVlNTLTEyOFxyXG4gICAgcHVibGljIGlzTWFuYWdlcjogYW55O1xyXG4gICAgLy8gcHVibGljIGlzQ2FtcGFpZ25NYW5hZ2VyOiBhbnk7IC0gTVZTUy0xMjhcclxuICAgIHB1YmxpYyBpc1VwbG9hZGVkOiBhbnk7XHJcbiAgICBwdWJsaWMgbWVtYmVyUm9sZTogYW55O1xyXG4gICAgcHVibGljIGlzTWVtYmVyOiBhbnk7XHJcbiAgICBwdWJsaWMgY2F0ZWdvcnlNZXRhZGF0YTogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgcHVibGljIGN1cnJlbnRQZXJzb246IGFueTtcclxuICAgIHB1YmxpYyBjdXJyZW50TWVudTogYW55O1xyXG4gICAgcHVibGljIGN1cnJlbnRVc2VyUHJlZmVyZW5jZTogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgLy8gcHVibGljIGNhdGVnb3J5RGV0YWlscyA9IFtdO1xyXG4gICAgLy8gcHVibGljIGRlZmF1bHRDYXRlZ29yeURldGFpbHM6IGFueTtcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25QYXNzOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICAgIHB1YmxpYyBzdWJzY3JpYmFibGVCcmFuZENvbmZpZzogQmVoYXZpb3JTdWJqZWN0PGFueT47XHJcbiAgICBwdWJsaWMgcmVzZXRWaWV3OiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICAgIHB1YmxpYyBlbWFpbENvbmZpZztcclxuICAgIHB1YmxpYyB1c2VyUHJlZmVyZW5jZXM6IEFycmF5PGFueT4gPSBbXTtcclxuICAgIHB1YmxpYyBpc01lbnVDaGFuZ2VkOiBCZWhhdmlvclN1YmplY3Q8Qm9vbGVhbj47XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpYmFibGVCcmFuZENvbmZpZyA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PignJyk7XHJcbiAgICAgICAgdGhpcy5yZXNldFZpZXcgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4oJycpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uUGFzcyA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PignJylcclxuICAgICAgICB0aGlzLmlzTWVudUNoYW5nZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PEJvb2xlYW4+KGZhbHNlKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgc2V0TWVudUNoYW5nZWQobWVudWNoYW5nZWQpIHtcclxuICAgICAgICB0aGlzLmlzTWVudUNoYW5nZWQubmV4dChtZW51Y2hhbmdlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWVudUNoYW5nZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc01lbnVDaGFuZ2VkLmFzT2JzZXJ2YWJsZSgpXHJcbiAgICB9XHJcblxyXG4gICAgdW5zdWJzY3JpYmVNZW51Q2hhbmdlT2JzZXJhYmxlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTWVudUNoYW5nZWQudW5zdWJzY3JpYmUoKVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBnZXROb3RpZmljYXRpb25EZXRhaWxzKGRhdGEpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblBhc3MubmV4dChkYXRhKVxyXG4gICAgfVxyXG5cclxuICAgIGdldENhdGVnb3J5TWV0YWRhdGEoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2F0ZWdvcnlNZXRhZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDYXRlZ29yeU1ldGFkYXRhKGNhdGVnb3J5TWV0YWRhdGE6IEFycmF5PGFueT4pIHtcclxuICAgICAgICB0aGlzLmNhdGVnb3J5TWV0YWRhdGEgPSBjYXRlZ29yeU1ldGFkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldGRpc3BsYXlOYW1lKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGdldGN1cnJlbnRVc2VySXRlbUlEKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VySXRlbUlEO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRVc2VyT2JqZWN0KGN1cnJlbnRVc2VyT2JqZWN0OiBVc2VyRGV0YWlscykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXJPYmplY3QgPSBjdXJyZW50VXNlck9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlck9iamVjdCgpOiBVc2VyRGV0YWlscyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJPYmplY3Q7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudFVzZXJJRChjdXJyZW50VXNlcklkOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VySUQgPSBjdXJyZW50VXNlcklkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VySUQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlcklEO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VySXRlbUlEKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJJdGVtSUQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudFVzZXJJdGVtSUQoY3VycmVudFVzZXJJdGVtSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXJJdGVtSUQgPSBjdXJyZW50VXNlckl0ZW1JZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50VXNlckROKGN1cnJlbnRVc2VyRE46IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXJETiA9IGN1cnJlbnRVc2VyRE47XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJETigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyRE47XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudFVzZXJEaXNwbGF5TmFtZShkaXNwbGF5TmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRPcmdETigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRPcmdETjtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50T3JnRE4ob3JnRE46IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudE9yZ0ROID0gb3JnRE47XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudE9yZ0ROICYmIHR5cGVvZiB0aGlzLmN1cnJlbnRPcmdETiA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgY29uc3QgaW5zdGFuY2VJZGVudGlmaWVyID0gdGhpcy5jdXJyZW50T3JnRE4uc3BsaXQoJywnKTtcclxuICAgICAgICAgICAgdGhpcy5pbnN0YW5jZUlkZW50aWZpZXIgPSBpbnN0YW5jZUlkZW50aWZpZXIuc2xpY2UoMSwgaW5zdGFuY2VJZGVudGlmaWVyLmxlbmd0aCkuam9pbignLCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50VXNlckVtYWlsSWQoZW1haWxJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlckVtYWlsSWQgPSBlbWFpbElkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyRW1haWxJZCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyRW1haWxJZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJbnN0YW5jZUlkZW50aWZpZXIoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnN0YW5jZUlkZW50aWZpZXI7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJEaXNwbGF5TmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlOYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyQ04oKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50VXNlckROKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyRE4uc3BsaXQoJywnKVswXS5zcGxpdCgnPScpWzFdO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2V0UmVjZW50U2VhcmNoKHJlY2VudFNlYXJjaCkge1xyXG4gICAgICAgIHRoaXMucmVjZW50U2VhcmNoZXMudW5zaGlmdChyZWNlbnRTZWFyY2gpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJlY2VudFNlYXJjaGVzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlY2VudFNlYXJjaGVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJlY2VudFNlYXJjaGVzQnlWaWV3KHZpZXcpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZWNlbnRTZWFyY2hlcy5maWx0ZXIoc2VhcmNoID0+IHNlYXJjaFswXS52aWV3ID09PSB2aWV3IHx8IHR5cGVvZiBzZWFyY2ggPT09ICdzdHJpbmcnKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRNZW51KG1lbnVMaXN0OiBBcnJheTxNUE1NZW51Pikge1xyXG4gICAgICAgIHRoaXMubWVudSA9IG1lbnVMaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbE1lbnUoKTogQXJyYXk8TVBNTWVudT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1lbnU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWVudUJ5VHlwZSh0eXBlOiBNUE1NZW51VHlwZSk6IEFycmF5PE1QTU1lbnU+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tZW51LmZpbHRlcih0bXBNZW51ID0+IHRtcE1lbnUuVFlQRSA9PT0gdHlwZSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VGFza0V2ZW50cyhldmVudHMpIHtcclxuICAgICAgICBpZiAoZXZlbnRzICYmIGV2ZW50cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0V2ZW50cyA9IGV2ZW50cztcclxuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50cykge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tFdmVudHMgPSBbZXZlbnRzXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tFdmVudHMgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza0V2ZW50cygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNrRXZlbnRzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFRhc2tXb3JrZmxvd0FjdGlvbnMod29ya2Zsb3dBY3Rpb25zKSB7XHJcbiAgICAgICAgaWYgKHdvcmtmbG93QWN0aW9ucyAmJiB3b3JrZmxvd0FjdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tXb3JrZmxvd0FjdGlvbnMgPSB3b3JrZmxvd0FjdGlvbnM7XHJcbiAgICAgICAgfSBlbHNlIGlmICh3b3JrZmxvd0FjdGlvbnMpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrV29ya2Zsb3dBY3Rpb25zID0gW3dvcmtmbG93QWN0aW9uc107XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrV29ya2Zsb3dBY3Rpb25zID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tXb3JrZmxvd0FjdGlvbnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGFza1dvcmtmbG93QWN0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxDYXRlZ29yaWVzKGNhdGVnb3JpZXM6IEFycmF5PENhdGVnb3J5Pikge1xyXG4gICAgICAgIHRoaXMuYWxsQ2F0ZWdvcmllcyA9IGNhdGVnb3JpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsQ2F0ZWdvcmllcygpOiBBcnJheTxDYXRlZ29yeT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsbENhdGVnb3JpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgLyogc2V0RGVmYXVsdENhdGVnb3J5RGV0YWlscyhjYXRlZ29yeSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdENhdGVnb3J5RGV0YWlscyA9IGNhdGVnb3J5O1xyXG4gICAgfVxyXG5cclxuICAgIHNldENhdGVnb3J5RGV0YWlscyhjYXRlZ29yeSkge1xyXG4gICAgICAgIHRoaXMuY2F0ZWdvcnlEZXRhaWxzLnB1c2goY2F0ZWdvcnkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENhdGVnb3J5RGV0YWlsc0J5SWQoY2F0ZWdvcnlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhdGVnb3J5RGV0YWlscy5maW5kKGNhdGVnb3J5ID0+IGNhdGVnb3J5LmlkID09PSBjYXRlZ29yeUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0Q2F0ZWdvcnlEZXRhaWxzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRDYXRlZ29yeURldGFpbHM7XHJcbiAgICB9ICovXHJcblxyXG4gICAgc2V0U2VsZWN0ZWRDYXRlZ29yeShzZWxlY3RlZENhdGVnb3J5OiBDYXRlZ29yeSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDYXRlZ29yeSA9IHNlbGVjdGVkQ2F0ZWdvcnk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2VsZWN0ZWRDYXRlZ29yeSgpOiBDYXRlZ29yeSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRDYXRlZ29yeTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxMb29rdXBEb21haW5zKGxvb2t1cERvbWFpbnMpIHtcclxuICAgICAgICB0aGlzLmxvb2t1cERvbWFpbnMgPSBsb29rdXBEb21haW5zO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbExvb2t1cERvbWFpbnMoKTogQXJyYXk8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubG9va3VwRG9tYWlucztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxSb2xlcyhyb2xlczogQXJyYXk8Um9sZXM+KSB7XHJcbiAgICAgICAgdGhpcy5yb2xlcyA9IHJvbGVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFJvbGVzKCk6IEFycmF5PFJvbGVzPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucm9sZXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VXNlclJvbGVzKHJvbGVzOiBBcnJheTxSb2xlcz4pIHtcclxuICAgICAgICB0aGlzLnVzZXJSb2xlcyA9IHJvbGVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJSb2xlcygpOiBBcnJheTxSb2xlcz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnVzZXJSb2xlcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRSb2xlQnlOYW1lKHJvbGVOYW1lOiBNUE1fUk9MRSk6IFJvbGVzIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yb2xlcy5maW5kKHJvbGUgPT4gcm9sZS5ST0xFX05BTUUgPT09IHJvbGVOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRNZWRpYU1hbmFnZXJDb25maWcobWVkaWFNYW5hZ2VyQ29uZmlnOiBNZWRpYU1hbmFnZXJDb25maWcpIHtcclxuICAgICAgICB0aGlzLm1lZGlhTWFuYWdlckNvbmZpZyA9IG1lZGlhTWFuYWdlckNvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBnZXRNZWRpYU1hbmFnZXJDb25maWcoKTogTWVkaWFNYW5hZ2VyQ29uZmlnIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tZWRpYU1hbmFnZXJDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q2F0ZWdvcnlMZXZlbHMoY2F0ZWdvcnlMZXZlbHM6IEFycmF5PENhdGVnb3J5TGV2ZWw+KSB7XHJcbiAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVscyA9IGNhdGVnb3J5TGV2ZWxzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENhdGVnb3J5TGV2ZWxzKCk6IEFycmF5PENhdGVnb3J5TGV2ZWw+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYXRlZ29yeUxldmVscyA/IHRoaXMuY2F0ZWdvcnlMZXZlbHMgOiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDYXRlZ29yeUxldmVsc0J5SWQoKTogQXJyYXk8YW55PiB7XHJcbiAgICAgICAgbGV0IGFycl9jYXRlZ29yeUxldmVsID0gW107XHJcbiAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVscy5mb3JFYWNoKGNhdGVnb3J5TGV2ZWwgPT4ge1xyXG4gICAgICAgICAgICBhcnJfY2F0ZWdvcnlMZXZlbC5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGlkOiBjYXRlZ29yeUxldmVsWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxUeXBlOiBjYXRlZ29yeUxldmVsLkNBVEVHT1JZX0xFVkVMX1RZUEVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGFycl9jYXRlZ29yeUxldmVsO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFByaW9yaXRpZXMoYWxsUHJpb3JpdGllczogQXJyYXk8YW55Pik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuYWxsUHJpb3JpdGllcyA9IGFsbFByaW9yaXRpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsUHJpb3JpdGllcygpOiBBcnJheTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hbGxQcmlvcml0aWVzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENSQWN0aW9ucyhhY3Rpb25zOiBBcnJheTxhbnk+KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jckFjdGlvbnMgPSBhY3Rpb25zID8gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShhY3Rpb25zKSkgOiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDUkFjdGlvbnMoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jckFjdGlvbnMgPyB0aGlzLmNyQWN0aW9ucyA6IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFN0YXR1c0NvbmZpZyhzdGF0dXNMaXN0OiBBcnJheTxTdGF0dXM+KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hbGxTdGF0dXNDb25maWcgPSBzdGF0dXNMaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFN0YXR1c0NvbmZpZygpOiBBcnJheTxTdGF0dXM+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hbGxTdGF0dXNDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsQXBwbGljYXRpb25GaWVsZHMob3RtbUZpZWxkczogQXJyYXk8TVBNRmllbGQ+KSB7XHJcbiAgICAgICAgdGhpcy5hbGxBcHBsaWNhdGlvbkZpZWxkcyA9IG90bW1GaWVsZHMgPyBvdG1tRmllbGRzIDogW107XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsQXBwbGljYXRpb25GaWVsZHMoKTogQXJyYXk8TVBNRmllbGQ+IHtcclxuICAgICAgICByZXR1cm4gKHRoaXMuYWxsQXBwbGljYXRpb25GaWVsZHMgJiYgdGhpcy5hbGxBcHBsaWNhdGlvbkZpZWxkcy5sZW5ndGggPiAwKSA/IHRoaXMuYWxsQXBwbGljYXRpb25GaWVsZHMgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZyh2aWV3Q29uZmlnczogQXJyYXk8Vmlld0NvbmZpZz4pIHtcclxuICAgICAgICB0aGlzLmFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZyA9IHZpZXdDb25maWdzID8gdmlld0NvbmZpZ3MgOiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcoKTogQXJyYXk8Vmlld0NvbmZpZz4ge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5hbGxBcHBsaWNhdGlvblZpZXdDb25maWcgJiYgdGhpcy5hbGxBcHBsaWNhdGlvblZpZXdDb25maWcubGVuZ3RoID4gMCkgPyB0aGlzLmFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZyA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0UHJvamVjdENvbmZpZyhkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLnByb2plY3RDb25maWcgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RDb25maWcoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9qZWN0Q29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEJyYW5kQ29uZmlnKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuYnJhbmRDb25maWcgPSBkYXRhO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaWJhYmxlQnJhbmRDb25maWcubmV4dChkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRCcmFuZENvbmZpZygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5icmFuZENvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxWaWV3Q29uZmlnKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMudmlld0NvbmZpZyA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsVmlld0NvbmZpZygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy52aWV3Q29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZpZXdDb25maWdCeUlkKGlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmlld0NvbmZpZy5maW5kKHZpZXcgPT4gdmlld1snTVBNX1ZpZXdfQ29uZmlnLWlkJ10uSWQgPT09IGlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRWaWV3Q29uZmlnQnlOYW1lKHZpZXdOYW1lKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRWaWV3Q29uZmlnID0gdGhpcy52aWV3Q29uZmlnLmZpbmQodmlldyA9PiB2aWV3LlZJRVcgPT09IHZpZXdOYW1lKTtcclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRWaWV3Q29uZmlnID8gc2VsZWN0ZWRWaWV3Q29uZmlnIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdWJzY3JpYmFibGVCcmFuZENvbmZpZygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN1YnNjcmliYWJsZUJyYW5kQ29uZmlnLmFzT2JzZXJ2YWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0Vmlld0RhdGEoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZXNldFZpZXcuYXNPYnNlcnZhYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNldFZpZXcodmlld0lkKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldFZpZXcubmV4dCh2aWV3SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFwcENvbmZpZyhkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXBwQ29uZmlnKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwQ29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENvbW1lbnRDb25maWcoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5jb21tZW50Q29uZmlnID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb21tZW50Q29uZmlnKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29tbWVudENvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBc3NldENvbmZpZyhkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmFzc2V0Q29uZmlnID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldENvbmZpZygpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFzc2V0Q29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFRlYW1Sb2xlcyhkYXRhKTogYW55IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICB0aGlzLnRlYW1Sb2xlcyA9IGRhdGE7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy50ZWFtUm9sZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFRlYW1Sb2xlcygpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRlYW1Sb2xlcztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxUZWFtcyhkYXRhKTogYW55IHtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShkYXRhKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1zID0gZGF0YTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1zID0gW2RhdGFdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxUZWFtcygpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRlYW1zO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFBlcnNvbnMoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5wZXJzb25zID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxQZXJzb25zKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucGVyc29ucztcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50UGVyc29uKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFBlcnNvbiA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFBlcnNvbigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRQZXJzb247XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVmYXVsdEVtYWlsQ29uZmlnKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVtYWlsQ29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldERlZmF1bHRFbWFpbENvbmZpZyhkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5lbWFpbENvbmZpZyA9IGRhdGE7XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnRU1BSUxfQ09ORklHJywgSlNPTi5zdHJpbmdpZnkodGhpcy5lbWFpbENvbmZpZykpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZUN1cnJlbnRVc2VyUHJlZmVyZW5jZSh1c2VyUHJlZmVyZW5jZURhdGEpOiBhbnkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHVzZXJQcmVmZXJlbmNlRGF0YSlcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1c2VyUHJlZmVyZW5jZURhdGEpKSB7XHJcbiAgICAgICAgICAgIHVzZXJQcmVmZXJlbmNlRGF0YS5mb3JFYWNoKHVzZXJQcmVmZXJlbmNlID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCA9IHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmRJbmRleChzZWxlY3RlZFVzZXJQcmVmZXJlbmNlID0+XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZVsnTVBNX1VzZXJfUHJlZmVyZW5jZS1pZCddLklkID09PSB1c2VyUHJlZmVyZW5jZVsnTVBNX1VzZXJfUHJlZmVyZW5jZS1pZCddLklkKTtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFVzZXJQcmVmZXJlbmNlSW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLnNwbGljZShzZWxlY3RlZFVzZXJQcmVmZXJlbmNlSW5kZXgsIDEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UucHVzaCh1c2VyUHJlZmVyZW5jZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCA9IHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmRJbmRleCh1c2VyUHJlZmVyZW5jZSA9PlxyXG4gICAgICAgICAgICAgICAgdXNlclByZWZlcmVuY2VbJ01QTV9Vc2VyX1ByZWZlcmVuY2UtaWQnXS5JZCA9PT0gdXNlclByZWZlcmVuY2VEYXRhWydNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLnNwbGljZShzZWxlY3RlZFVzZXJQcmVmZXJlbmNlSW5kZXgsIDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLnB1c2godXNlclByZWZlcmVuY2VEYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UsIEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlQ3VycmVudFVzZXJQcmVmZXJlbmNlKHVzZXJQcmVmZXJlbmNlSWQpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCA9IHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmRJbmRleCh1c2VyUHJlZmVyZW5jZSA9PlxyXG4gICAgICAgICAgICB1c2VyUHJlZmVyZW5jZVsnTVBNX1VzZXJfUHJlZmVyZW5jZS1pZCddLklkID09PSB1c2VyUHJlZmVyZW5jZUlkKTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2Uuc3BsaWNlKHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ1VSUkVOVF9NUE1fVVNFUl9QUkVGRVJFTkNFLCBKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRVc2VyUHJlZmVyZW5jZShkYXRhKTogYW55IHtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShkYXRhKSkge1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZSA9IGRhdGE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UgPSBbZGF0YV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQ1VSUkVOVF9NUE1fVVNFUl9QUkVGRVJFTkNFLCBKU09OLnN0cmluZ2lmeSh0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyUHJlZmVyZW5jZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2VCeVZpZXdJZCh2aWV3SWQpOiBhbnkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlKTtcclxuICAgICAgICBpZiAodGhpcy51c2VyUHJlZmVyZW5jZXMubGVuZ3RoIDw9IDAgJiYgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlcy5wdXNoKHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlICYmIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmQodXNlclByZWZlcmVuY2UgPT4gdXNlclByZWZlcmVuY2UuUl9QT19NUE1fVklFV19DT05GSUdbJ01QTV9WaWV3X0NvbmZpZy1pZCddLklkID09PSB2aWV3SWQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlclByZWZlcmVuY2VzQnlWaWV3SWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudXNlclByZWZlcmVuY2VzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlZmF1bHRDdXJyZW50VXNlclByZWZlcmVuY2VCeU1lbnUobWVudUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmQodXNlclByZWZlcmVuY2UgPT4gKHVzZXJQcmVmZXJlbmNlLklTX0RFRkFVTFRfVklFVyA9PT0gJ3RydWUnIHx8IHVzZXJQcmVmZXJlbmNlLklTX0RFRkFVTFRfVklFVyA9PT0gdHJ1ZSlcclxuICAgICAgICAgICAgJiYgdXNlclByZWZlcmVuY2UuUl9QT19NUE1fTUVOVV9JRFsnTVBNX01lbnUtaWQnXS5JZCA9PT0gbWVudUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2VCeVZpZXdNZW51KG1lbnVJZCwgdmlld0lkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmQodXNlclByZWZlcmVuY2UgPT4gdXNlclByZWZlcmVuY2UuUl9QT19NUE1fVklFV19DT05GSUdbJ01QTV9WaWV3X0NvbmZpZy1pZCddLklkID09PSB2aWV3SWRcclxuICAgICAgICAgICAgJiYgdXNlclByZWZlcmVuY2UuUl9QT19NUE1fTUVOVV9JRFsnTVBNX01lbnUtaWQnXS5JZCA9PT0gbWVudUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0Q3VycmVudFVzZXJQcmVmZXJlbmNlQnlMaXN0RmlsdGVyKG1lbnVJZCwgdGFza05hbWUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZCh1c2VyUHJlZmVyZW5jZSA9PiAodXNlclByZWZlcmVuY2UuSVNfREVGQVVMVF9WSUVXID09PSAndHJ1ZScgfHwgdXNlclByZWZlcmVuY2UuSVNfREVGQVVMVF9WSUVXID09PSB0cnVlKVxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9NRU5VX0lEWydNUE1fTWVudS1pZCddLklkID09PSBtZW51SWRcclxuICAgICAgICAgICAgJiYgdXNlclByZWZlcmVuY2UuRklMVEVSX05BTUUgPT09IHRhc2tOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2VCeUxpc3QobWVudUlkLCB0YXNrTmFtZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5maW5kKHVzZXJQcmVmZXJlbmNlID0+ICh1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9NRU5VX0lEWydNUE1fTWVudS1pZCddLklkID09PSBtZW51SWRcclxuICAgICAgICAgICAgJiYgdXNlclByZWZlcmVuY2UuRklMVEVSX05BTUUgPT09IHRhc2tOYW1lKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlQnlMaXN0VmlldyhtZW51SWQsIHRhc2tOYW1lLCB2aWV3SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZCh1c2VyUHJlZmVyZW5jZSA9PiAodXNlclByZWZlcmVuY2UuUl9QT19NUE1fTUVOVV9JRFsnTVBNX01lbnUtaWQnXS5JZCA9PT0gbWVudUlkXHJcbiAgICAgICAgICAgICYmIHVzZXJQcmVmZXJlbmNlLkZJTFRFUl9OQU1FID09PSB0YXNrTmFtZVxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9WSUVXX0NPTkZJR1snTVBNX1ZpZXdfQ29uZmlnLWlkJ10uSWQgPT09IHZpZXdJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENhbXBhaWduUm9sZShkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmNhbXBhaWduUm9sZSA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2FtcGFpZ25Sb2xlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FtcGFpZ25Sb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFJldmlld2VyUm9sZShkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLnJldmlld2VyUm9sZSA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmV2aWV3ZXJSb2xlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmV2aWV3ZXJSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldElzUmV2aWV3ZXIoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5pc1Jldmlld2VyID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJc1Jldmlld2VyKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNSZXZpZXdlcjtcclxuICAgIH1cclxuXHJcbiAgICBzZXRBcHByb3ZlclJvbGUoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5hcHByb3ZlclJvbGUgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFwcHJvdmVyUm9sZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcHJvdmVyUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRJc0FwcHJvdmVyKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuaXNBcHByb3ZlciA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SXNBcHByb3ZlcigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzQXBwcm92ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0TWFuYWdlclJvbGUoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5NYW5hZ2VyUm9sZSA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWFuYWdlclJvbGUoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5NYW5hZ2VyUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRJc01hbmFnZXIoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5pc01hbmFnZXIgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElzTWFuYWdlcigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTWFuYWdlcjtcclxuICAgIH1cclxuXHJcbiAgICAvKiBzZXRDYW1wYWlnbk1hbmFnZXJSb2xlKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuY2FtcGFpZ25NYW5hZ2VyUm9sZSA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2FtcGFpZ25NYW5hZ2VyUm9sZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhbXBhaWduTWFuYWdlclJvbGU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0SXNDYW1wYWlnbk1hbmFnZXIoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5pc0NhbXBhaWduTWFuYWdlciA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SXNDYW1wYWlnbk1hbmFnZXIoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0NhbXBhaWduTWFuYWdlcjtcclxuICAgIH0gKi8gLy8gLSBNVlNTLTEyOFxyXG5cclxuICAgIHNldE1lbWJlclJvbGUoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5tZW1iZXJSb2xlID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNZW1iZXJSb2xlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubWVtYmVyUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRJc01lbWJlcihkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmlzTWVtYmVyID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJc01lbWJlcigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTWVtYmVyO1xyXG4gICAgfVxyXG5cclxuICAgIHNldElzVXBsb2FkZWQoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5pc1VwbG9hZGVkID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJc1VwbG9hZGVkKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNVcGxvYWRlZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUZWFtUm9sZUlkQnlSb2xlRE4ocm9sZUROKTogYW55IHtcclxuICAgICAgICB0aGlzLmdldEFsbFRlYW1zKCkuc3Vic2NyaWJlKGFsbFRlYW1zID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYWxsVGVhbXMpO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudE1lbnUobWVudSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50TWVudSA9IG1lbnU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudE1lbnUoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50TWVudTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19