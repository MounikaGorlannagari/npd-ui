import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from './app.service';
import { GloabalConfig as config } from '../config/config';
import { Observable } from 'rxjs';
import { EntityAppDefService } from './entity.appdef.service';
import { SharingService } from './sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "./app.service";
import * as i2 from "./entity.appdef.service";
import * as i3 from "./sharing.service";
let OTDSTicketService = class OTDSTicketService {
    constructor(appService, entityAppDefService, sharingService) {
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.sharingService = sharingService;
        this.otdsticket = '';
    }
    setOTDSTicketForUser() {
        this.otdsticket = '';
        return new Observable(observer => {
            if (this.otdsticket) {
                observer.next(this.otdsticket);
                observer.complete();
                return;
            }
            const parameters = {
                organization: config.config.organizationName,
                // sourceResourceName: '__OTDS#Organizational#Platform#Resource__',
                sourceResourceName: this.entityAppDefService.applicationDetails.OTDS_APPWORKS_RESOURCE_NAME,
                targetResourceName: this.sharingService.getMediaManagerConfig().otdsResourceName,
                isOrgSpace: 'true'
            };
            this.appService.getOTDSTicketForUser(parameters)
                .subscribe(otdsTicketResponse => {
                const ticket = otdsTicketResponse.tuple.old.OTDSAccessTicket.otmmTicket;
                this.otdsticket = ticket;
                observer.next(ticket);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
    getOTDSTicketForUser() {
        return this.otdsticket;
    }
};
OTDSTicketService.ctorParameters = () => [
    { type: AppService },
    { type: EntityAppDefService },
    { type: SharingService }
];
OTDSTicketService.ɵprov = i0.ɵɵdefineInjectable({ factory: function OTDSTicketService_Factory() { return new OTDSTicketService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.SharingService)); }, token: OTDSTicketService, providedIn: "root" });
OTDSTicketService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], OTDSTicketService);
export { OTDSTicketService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT1REU1RpY2tldC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL09URFNUaWNrZXQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7Ozs7O0FBTW5ELElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRTFCLFlBQ1csVUFBc0IsRUFDdEIsbUJBQXdDLEVBQ3hDLGNBQThCO1FBRjlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFJekMsZUFBVSxHQUFHLEVBQUUsQ0FBQztJQUZaLENBQUM7SUFJTCxvQkFBb0I7UUFDaEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMvQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLE9BQU87YUFDVjtZQUNELE1BQU0sVUFBVSxHQUFHO2dCQUNmLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQjtnQkFDNUMsbUVBQW1FO2dCQUNuRSxrQkFBa0IsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCO2dCQUMzRixrQkFBa0IsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsZ0JBQWdCO2dCQUNoRixVQUFVLEVBQUUsTUFBTTthQUNyQixDQUFDO1lBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO2dCQUM1QixNQUFNLE1BQU0sR0FBRyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUM7Z0JBQ3pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQkFBb0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7Q0FFSixDQUFBOztZQXpDMEIsVUFBVTtZQUNELG1CQUFtQjtZQUN4QixjQUFjOzs7QUFMaEMsaUJBQWlCO0lBSjdCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxpQkFBaUIsQ0E0QzdCO1NBNUNZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4vYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBHbG9hYmFsQ29uZmlnIGFzIGNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zaGFyaW5nLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgT1REU1RpY2tldFNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuXHJcbiAgICApIHsgfVxyXG5cclxuICAgIG90ZHN0aWNrZXQgPSAnJztcclxuXHJcbiAgICBzZXRPVERTVGlja2V0Rm9yVXNlcigpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xyXG4gICAgICAgIHRoaXMub3Rkc3RpY2tldCA9ICcnO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm90ZHN0aWNrZXQpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5vdGRzdGlja2V0KTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIG9yZ2FuaXphdGlvbjogY29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lLFxyXG4gICAgICAgICAgICAgICAgLy8gc291cmNlUmVzb3VyY2VOYW1lOiAnX19PVERTI09yZ2FuaXphdGlvbmFsI1BsYXRmb3JtI1Jlc291cmNlX18nLFxyXG4gICAgICAgICAgICAgICAgc291cmNlUmVzb3VyY2VOYW1lOiB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuYXBwbGljYXRpb25EZXRhaWxzLk9URFNfQVBQV09SS1NfUkVTT1VSQ0VfTkFNRSxcclxuICAgICAgICAgICAgICAgIHRhcmdldFJlc291cmNlTmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5vdGRzUmVzb3VyY2VOYW1lLFxyXG4gICAgICAgICAgICAgICAgaXNPcmdTcGFjZTogJ3RydWUnXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0T1REU1RpY2tldEZvclVzZXIocGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUob3Rkc1RpY2tldFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0aWNrZXQgPSBvdGRzVGlja2V0UmVzcG9uc2UudHVwbGUub2xkLk9URFNBY2Nlc3NUaWNrZXQub3RtbVRpY2tldDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm90ZHN0aWNrZXQgPSB0aWNrZXQ7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aWNrZXQpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T1REU1RpY2tldEZvclVzZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3Rkc3RpY2tldDtcclxuICAgIH1cclxuXHJcbn1cclxuIl19