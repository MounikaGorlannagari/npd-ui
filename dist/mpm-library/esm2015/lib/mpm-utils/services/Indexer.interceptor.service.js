import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { OTDSTicketService } from './OTDSTicket.service';
import * as i0 from "@angular/core";
import * as i1 from "./OTDSTicket.service";
let IndexerInterceptor = class IndexerInterceptor {
    constructor(otdsTicketService) {
        this.otdsTicketService = otdsTicketService;
    }
    intercept(req, next) {
        const ticket = this.otdsTicketService.getOTDSTicketForUser();
        let newHeaders = req.headers;
        if (ticket) {
            newHeaders = newHeaders.append('OTDSTicket', ticket);
        }
        // We have to clone our request with our new headers
        // This is required because HttpRequests are immutable
        const authReq = req.clone({ headers: newHeaders });
        return next.handle(authReq);
    }
};
IndexerInterceptor.ctorParameters = () => [
    { type: OTDSTicketService }
];
IndexerInterceptor.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerInterceptor_Factory() { return new IndexerInterceptor(i0.ɵɵinject(i1.OTDSTicketService)); }, token: IndexerInterceptor, providedIn: "root" });
IndexerInterceptor = __decorate([
    Injectable({
        providedIn: 'root'
    })
], IndexerInterceptor);
export { IndexerInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5kZXhlci5pbnRlcmNlcHRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL0luZGV4ZXIuaW50ZXJjZXB0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBTXpELElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBRTNCLFlBQ1csaUJBQW9DO1FBQXBDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7SUFDM0MsQ0FBQztJQUVMLFNBQVMsQ0FBQyxHQUFxQixFQUFFLElBQWlCO1FBQzlDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzdELElBQUksVUFBVSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDN0IsSUFBSSxNQUFNLEVBQUU7WUFDUixVQUFVLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDeEQ7UUFDRCxvREFBb0Q7UUFDcEQsc0RBQXNEO1FBQ3RELE1BQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUNuRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQztDQUVKLENBQUE7O1lBZmlDLGlCQUFpQjs7O0FBSHRDLGtCQUFrQjtJQUo5QixVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDO0dBRVcsa0JBQWtCLENBa0I5QjtTQWxCWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgT1REU1RpY2tldFNlcnZpY2UgfSBmcm9tICcuL09URFNUaWNrZXQuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBJbmRleGVySW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBvdGRzVGlja2V0U2VydmljZTogT1REU1RpY2tldFNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAgICAgY29uc3QgdGlja2V0ID0gdGhpcy5vdGRzVGlja2V0U2VydmljZS5nZXRPVERTVGlja2V0Rm9yVXNlcigpO1xyXG4gICAgICAgIGxldCBuZXdIZWFkZXJzID0gcmVxLmhlYWRlcnM7XHJcbiAgICAgICAgaWYgKHRpY2tldCkge1xyXG4gICAgICAgICAgICBuZXdIZWFkZXJzID0gbmV3SGVhZGVycy5hcHBlbmQoJ09URFNUaWNrZXQnLCB0aWNrZXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBXZSBoYXZlIHRvIGNsb25lIG91ciByZXF1ZXN0IHdpdGggb3VyIG5ldyBoZWFkZXJzXHJcbiAgICAgICAgLy8gVGhpcyBpcyByZXF1aXJlZCBiZWNhdXNlIEh0dHBSZXF1ZXN0cyBhcmUgaW1tdXRhYmxlXHJcbiAgICAgICAgY29uc3QgYXV0aFJlcSA9IHJlcS5jbG9uZSh7IGhlYWRlcnM6IG5ld0hlYWRlcnMgfSk7XHJcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKGF1dGhSZXEpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=