var AuthenticationService_1;
import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { GatewayModule } from '../auth/gateway';
import { clearAuthCookies } from '../auth/clearAuthenticationCookies';
import { SessionInfo } from '../auth/sessionUtil';
import { getPreLoginInfo } from '../auth/getPreLoginInfo';
import { GetUserProfile } from '../auth/getUserProfile';
import { HandleOTDSRESTAuthentication } from '../auth/handleOTDSRESTAuthentication';
import * as _ from 'underscore';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { setAuthContextCookie } from '../auth/setAuthContextCookie';
import { HttpClient } from '@angular/common/http';
import { GloabalConfig as config } from '../config/config';
import * as acronui from '../auth/utility';
import { soapServicesConstants } from '../config/soapServicesConstants';
import * as X2JS from 'x2js';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
const x2js = new X2JS();
let AuthenticationService = AuthenticationService_1 = class AuthenticationService {
    constructor(http) {
        this.http = http;
        this.setSessionInfo = (data) => {
            if (data && data.Authenticator) {
                AuthenticationService_1.sessionUtil.setSessionInfo(data.Authenticator);
            }
        };
        this.getPreLoginDetails = () => {
            return new Observable(observer => {
                getPreLoginInfo().then((data) => {
                    observer.next(data);
                    observer.complete();
                }, (error) => {
                    observer.error(new Error(error));
                });
            });
        };
        this.getUserInfo = () => {
            return new Observable(observer => {
                GetUserProfile(soapServicesConstants.USER_NSPC).then((data) => {
                    observer.next(data);
                    observer.complete();
                }, (error) => {
                    observer.error(new Error(error));
                });
            });
        };
        this.authenticateInOtds = (username, password) => {
            return new Observable((observer) => {
                HandleOTDSRESTAuthentication(username, password).then((data) => {
                    observer.next(data);
                    observer.complete();
                }, (error) => {
                    observer.error(new Error(error));
                });
            });
        };
        this.logout = () => {
            return new Observable(observer => {
                clearAuthCookies().then((data) => {
                    observer.next(data);
                    observer.complete();
                }, (error) => {
                    observer.error(new Error(error));
                });
            });
        };
        this.setPreLoginCookie = () => {
            return setAuthContextCookie();
        };
        _.extend($, GatewayModule.extensions);
        $.ajaxSetup(GatewayModule.ajaxSetup);
    }
    fireRestUrl(url, urlparam) {
        return this.http.get(url + '?' + urlparam);
    }
    getauthenticationCookie(url, paramters) {
        return this.fireRestUrl(url + config.config.preLoginUrl, paramters);
    }
    getCookieValue(cname) {
        const name = cname + '=';
        const ca = document.cookie.split(';');
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }
    getUrlParams() {
        const urlParams = {};
        const params = window.location.search.substring(1).split('&');
        if (params && params.length > 0) {
            // tslint:disable-next-line: prefer-for-of
            for (let i = 0; i < params.length; i++) {
                if (params[i] !== '' && params[i].indexOf('=') !== -1) {
                    const tmp = params[i].split('=');
                    urlParams[tmp[0]] = decodeURI(tmp[1]);
                }
            }
        }
        return urlParams;
    }
    isValidCookie(currentCookie) {
        const orgDn = config.getOrganizationDN();
        return new Observable(observer => {
            this.getauthenticationCookie(config.webServiceUrl, encodeURIComponent(orgDn))
                .subscribe(response => {
                let xml = $.parseXML(response.toString());
                xml = x2js.xml2json(xml);
                const preLoginCookie = acronui.findObjectsByProp(xml, 'SamlArtifactCookieName');
                if (currentCookie && currentCookie !== undefined) {
                    const pspCookie = this.getCookieValue(preLoginCookie[0].text);
                    if (pspCookie == null || pspCookie === '') {
                        const params = this.getUrlParams();
                        const viewToRedirect = params['redirectView'];
                        const projectToRedirect = params['task'];
                        const taskToRedirect = params['project'];
                        const deliverableToRedirect = params['deliverable'];
                        const popupType = params['type'];
                        const assetKey = params['assetKey'];
                        const taskId = params['taskId'];
                        const projectId = params['projectId'];
                        const transitTo = params['goTo'];
                        if (transitTo && transitTo != null && transitTo !== '') {
                            window.sessionStorage.setItem('transitTo', transitTo);
                        }
                        else if (viewToRedirect != null && viewToRedirect) {
                            window.sessionStorage.setItem('appRedirectView', viewToRedirect);
                        }
                        else if (projectToRedirect != null && projectToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('ProjectRedirectView', projectToRedirect);
                        }
                        else if (taskToRedirect != null && taskToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('TaskRedirectView', taskToRedirect);
                        }
                        else if (popupType && popupType != null && popupType === 'deliverable' && assetKey && assetKey != null) {
                            window.sessionStorage.setItem('PopupDeliverable', assetKey);
                        }
                        else if (popupType && popupType != null && popupType === 'task' && taskId && taskId != null &&
                            projectId && projectId != null) {
                            const popupTask = { taskId, projectId };
                            window.sessionStorage.setItem('PopupTask', JSON.stringify(popupTask));
                        }
                        else if (deliverableToRedirect != null && deliverableToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('DeliverableRedirectView', deliverableToRedirect);
                        }
                        window.location.href = config.getOTDSLoginForm();
                        observer.next(true);
                        observer.complete();
                    }
                }
            }, error => {
                observer.next(false);
                observer.complete();
                console.warn('Unable to fire pre-login session to get cookie name.');
            });
        });
    }
    logIn() {
        const self = this;
        return new Observable(observer => {
            GetUserProfile(soapServicesConstants.USER_LDAP).then((data) => {
                const userObject = acronui.findObjectsByProp(data, 'user');
                const userDN = userObject[0].authuserdn;
                const userName = userObject[0].authuserdn.split(',')[0].split('=')[1];
                const org = userObject[0].authuserdn.split('cn=anonymous,cn=authenticated users,')[1];
                let orgDN = 'o=' + config.config.organizationName + ',';
                orgDN = orgDN.concat(org);
                if (userName === 'anonymous') {
                    self.isValidCookie(document.cookie)
                        .subscribe(response => {
                        if (!response) {
                            observer.next(false);
                            observer.error(new Error());
                        }
                    }, error => {
                        observer.next(false);
                        observer.error(new Error(error));
                    });
                }
                else {
                    observer.next(true);
                    observer.complete();
                }
            }, error => {
                observer.next(false);
                observer.error(new Error(error));
            });
        });
    }
};
AuthenticationService.sessionUtil = SessionInfo.getInstance();
AuthenticationService.ctorParameters = () => [
    { type: HttpClient }
];
AuthenticationService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AuthenticationService_Factory() { return new AuthenticationService(i0.ɵɵinject(i1.HttpClient)); }, token: AuthenticationService, providedIn: "root" });
AuthenticationService = AuthenticationService_1 = __decorate([
    Injectable({
        providedIn: 'root'
    })
], AuthenticationService);
export { AuthenticationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDaEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDdEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEYsT0FBTyxLQUFLLENBQUMsTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGFBQWEsSUFBSSxNQUFNLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUMzRCxPQUFPLEtBQUssT0FBTyxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3hFLE9BQU8sS0FBSyxJQUFJLE1BQU0sTUFBTSxDQUFDOzs7QUFFN0IsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztBQU14QixJQUFhLHFCQUFxQiw2QkFBbEMsTUFBYSxxQkFBcUI7SUFJOUIsWUFDVyxJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBTTNCLG1CQUFjLEdBQUcsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUN0QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM1Qix1QkFBcUIsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN4RTtRQUNMLENBQUMsQ0FBQTtRQUVELHVCQUFrQixHQUFHLEdBQUcsRUFBRTtZQUN0QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUM3QixlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtvQkFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7UUFFRCxnQkFBVyxHQUFHLEdBQUcsRUFBRTtZQUNmLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzdCLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtvQkFDMUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7UUFFRCx1QkFBa0IsR0FBRyxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsRUFBRTtZQUN4QyxPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQy9CLDRCQUE0QixDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtvQkFDM0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7UUFFRCxXQUFNLEdBQUcsR0FBRyxFQUFFO1lBQ1YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDN0IsZ0JBQWdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtvQkFDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDVCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUE7UUFFRCxzQkFBaUIsR0FBRyxHQUFHLEVBQUU7WUFDckIsT0FBTyxvQkFBb0IsRUFBRSxDQUFDO1FBQ2xDLENBQUMsQ0FBQTtRQXhERyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQXdERCxXQUFXLENBQUMsR0FBRyxFQUFFLFFBQVE7UUFDckIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsU0FBUztRQUNsQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCxjQUFjLENBQUMsS0FBSztRQUNoQixNQUFNLElBQUksR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDO1FBQ3pCLE1BQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLDBDQUEwQztRQUMxQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNoQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDZCxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO2dCQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQUU7WUFDbkQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFBRSxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7YUFBRTtTQUM1RTtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELFlBQVk7UUFDUixNQUFNLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDckIsTUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5RCxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QiwwQ0FBMEM7WUFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3BDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUNuRCxNQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNqQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN6QzthQUNKO1NBQ0o7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRUQsYUFBYSxDQUFDLGFBQWE7UUFDdkIsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDekMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDeEUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsTUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO2dCQUNoRixJQUFJLGFBQWEsSUFBSSxhQUFhLEtBQUssU0FBUyxFQUFFO29CQUM5QyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsS0FBSyxFQUFFLEVBQUU7d0JBQ3ZDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzt3QkFDbkMsTUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUM5QyxNQUFNLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDekMsTUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUN6QyxNQUFNLHFCQUFxQixHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDcEQsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNqQyxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ3BDLE1BQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDaEMsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUN0QyxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ2pDLElBQUksU0FBUyxJQUFJLFNBQVMsSUFBSSxJQUFJLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTs0QkFDcEQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUN6RDs2QkFBTSxJQUFJLGNBQWMsSUFBSSxJQUFJLElBQUksY0FBYyxFQUFFOzRCQUNqRCxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxjQUFjLENBQUMsQ0FBQzt5QkFDcEU7NkJBQU0sSUFBSSxpQkFBaUIsSUFBSSxJQUFJLElBQUksaUJBQWlCLEVBQUU7NEJBQ3ZELGdHQUFnRzs0QkFDaEcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQzt5QkFDM0U7NkJBQU0sSUFBSSxjQUFjLElBQUksSUFBSSxJQUFJLGNBQWMsRUFBRTs0QkFDakQsZ0dBQWdHOzRCQUNoRyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxjQUFjLENBQUMsQ0FBQzt5QkFDckU7NkJBQU0sSUFBSSxTQUFTLElBQUksU0FBUyxJQUFJLElBQUksSUFBSSxTQUFTLEtBQUssYUFBYSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFOzRCQUN0RyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxRQUFRLENBQUMsQ0FBQzt5QkFDL0Q7NkJBQU0sSUFBSSxTQUFTLElBQUksU0FBUyxJQUFJLElBQUksSUFBSSxTQUFTLEtBQUssTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksSUFBSTs0QkFDekYsU0FBUyxJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7NEJBQ2hDLE1BQU0sU0FBUyxHQUFHLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxDQUFDOzRCQUN4QyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO3lCQUN6RTs2QkFBTSxJQUFJLHFCQUFxQixJQUFJLElBQUksSUFBSSxxQkFBcUIsRUFBRTs0QkFDL0QsZ0dBQWdHOzRCQUNoRyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO3lCQUNuRjt3QkFFRCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzt3QkFDakQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtpQkFDSjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsc0RBQXNELENBQUMsQ0FBQztZQUN6RSxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEtBQUs7UUFDRCxNQUFNLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixjQUFjLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQzFELE1BQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQzNELE1BQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7Z0JBQ3hDLE1BQU0sUUFBUSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEUsTUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEYsSUFBSSxLQUFLLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDO2dCQUN4RCxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUIsSUFBSSxRQUFRLEtBQUssV0FBVyxFQUFFO29CQUMxQixJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7eUJBQzlCLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbEIsSUFBSSxDQUFDLFFBQVEsRUFBRTs0QkFDWCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNyQixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxFQUFFLENBQUMsQ0FBQzt5QkFDL0I7b0JBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDckMsQ0FBQyxDQUFDLENBQUM7aUJBQ1Y7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FFSixDQUFBO0FBekxpQixpQ0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7WUFHckMsVUFBVTs7O0FBTGxCLHFCQUFxQjtJQUpqQyxVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDO0dBRVcscUJBQXFCLENBMkxqQztTQTNMWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEdhdGV3YXlNb2R1bGUgfSBmcm9tICcuLi9hdXRoL2dhdGV3YXknO1xyXG5pbXBvcnQgeyBjbGVhckF1dGhDb29raWVzIH0gZnJvbSAnLi4vYXV0aC9jbGVhckF1dGhlbnRpY2F0aW9uQ29va2llcyc7XHJcbmltcG9ydCB7IFNlc3Npb25JbmZvIH0gZnJvbSAnLi4vYXV0aC9zZXNzaW9uVXRpbCc7XHJcbmltcG9ydCB7IGdldFByZUxvZ2luSW5mbyB9IGZyb20gJy4uL2F1dGgvZ2V0UHJlTG9naW5JbmZvJztcclxuaW1wb3J0IHsgR2V0VXNlclByb2ZpbGUgfSBmcm9tICcuLi9hdXRoL2dldFVzZXJQcm9maWxlJztcclxuaW1wb3J0IHsgSGFuZGxlT1REU1JFU1RBdXRoZW50aWNhdGlvbiB9IGZyb20gJy4uL2F1dGgvaGFuZGxlT1REU1JFU1RBdXRoZW50aWNhdGlvbic7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAndW5kZXJzY29yZSc7XHJcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBzZXRBdXRoQ29udGV4dENvb2tpZSB9IGZyb20gJy4uL2F1dGgvc2V0QXV0aENvbnRleHRDb29raWUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBHbG9hYmFsQ29uZmlnIGFzIGNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IHNvYXBTZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uL2NvbmZpZy9zb2FwU2VydmljZXNDb25zdGFudHMnO1xyXG5pbXBvcnQgKiBhcyBYMkpTIGZyb20gJ3gyanMnO1xyXG5cclxuY29uc3QgeDJqcyA9IG5ldyBYMkpTKCk7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGlvblNlcnZpY2Uge1xyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgc2Vzc2lvblV0aWwgPSBTZXNzaW9uSW5mby5nZXRJbnN0YW5jZSgpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgKSB7XHJcbiAgICAgICAgXy5leHRlbmQoJCwgR2F0ZXdheU1vZHVsZS5leHRlbnNpb25zKTtcclxuICAgICAgICAkLmFqYXhTZXR1cChHYXRld2F5TW9kdWxlLmFqYXhTZXR1cCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U2Vzc2lvbkluZm8gPSAoZGF0YSkgPT4ge1xyXG4gICAgICAgIGlmIChkYXRhICYmIGRhdGEuQXV0aGVudGljYXRvcikge1xyXG4gICAgICAgICAgICBBdXRoZW50aWNhdGlvblNlcnZpY2Uuc2Vzc2lvblV0aWwuc2V0U2Vzc2lvbkluZm8oZGF0YS5BdXRoZW50aWNhdG9yKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJlTG9naW5EZXRhaWxzID0gKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGdldFByZUxvZ2luSW5mbygpLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VySW5mbyA9ICgpID0+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBHZXRVc2VyUHJvZmlsZShzb2FwU2VydmljZXNDb25zdGFudHMuVVNFUl9OU1BDKS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZXJyb3IpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXV0aGVudGljYXRlSW5PdGRzID0gKHVzZXJuYW1lLCBwYXNzd29yZCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgSGFuZGxlT1REU1JFU1RBdXRoZW50aWNhdGlvbih1c2VybmFtZSwgcGFzc3dvcmQpLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dvdXQgPSAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY2xlYXJBdXRoQ29va2llcygpLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRQcmVMb2dpbkNvb2tpZSA9ICgpID0+IHtcclxuICAgICAgICByZXR1cm4gc2V0QXV0aENvbnRleHRDb29raWUoKTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUmVzdFVybCh1cmwsIHVybHBhcmFtKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodXJsICsgJz8nICsgdXJscGFyYW0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldGF1dGhlbnRpY2F0aW9uQ29va2llKHVybCwgcGFyYW10ZXJzKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmlyZVJlc3RVcmwodXJsICsgY29uZmlnLmNvbmZpZy5wcmVMb2dpblVybCwgcGFyYW10ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb29raWVWYWx1ZShjbmFtZSkge1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSBjbmFtZSArICc9JztcclxuICAgICAgICBjb25zdCBjYSA9IGRvY3VtZW50LmNvb2tpZS5zcGxpdCgnOycpO1xyXG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogcHJlZmVyLWZvci1vZlxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2EubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGMgPSBjYVtpXTtcclxuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIHsgYyA9IGMuc3Vic3RyaW5nKDEpOyB9XHJcbiAgICAgICAgICAgIGlmIChjLmluZGV4T2YobmFtZSkgPT09IDApIHsgcmV0dXJuIGMuc3Vic3RyaW5nKG5hbWUubGVuZ3RoLCBjLmxlbmd0aCk7IH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVybFBhcmFtcygpIHtcclxuICAgICAgICBjb25zdCB1cmxQYXJhbXMgPSB7fTtcclxuICAgICAgICBjb25zdCBwYXJhbXMgPSB3aW5kb3cubG9jYXRpb24uc2VhcmNoLnN1YnN0cmluZygxKS5zcGxpdCgnJicpO1xyXG4gICAgICAgIGlmIChwYXJhbXMgJiYgcGFyYW1zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBwcmVmZXItZm9yLW9mXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFyYW1zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyYW1zW2ldICE9PSAnJyAmJiBwYXJhbXNbaV0uaW5kZXhPZignPScpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRtcCA9IHBhcmFtc1tpXS5zcGxpdCgnPScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHVybFBhcmFtc1t0bXBbMF1dID0gZGVjb2RlVVJJKHRtcFsxXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHVybFBhcmFtcztcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbGlkQ29va2llKGN1cnJlbnRDb29raWUpIHtcclxuICAgICAgICBjb25zdCBvcmdEbiA9IGNvbmZpZy5nZXRPcmdhbml6YXRpb25ETigpO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0YXV0aGVudGljYXRpb25Db29raWUoY29uZmlnLndlYlNlcnZpY2VVcmwsIGVuY29kZVVSSUNvbXBvbmVudChvcmdEbikpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgeG1sID0gJC5wYXJzZVhNTChyZXNwb25zZS50b1N0cmluZygpKTtcclxuICAgICAgICAgICAgICAgICAgICB4bWwgPSB4MmpzLnhtbDJqc29uKHhtbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJlTG9naW5Db29raWUgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHhtbCwgJ1NhbWxBcnRpZmFjdENvb2tpZU5hbWUnKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudENvb2tpZSAmJiBjdXJyZW50Q29va2llICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHNwQ29va2llID0gdGhpcy5nZXRDb29raWVWYWx1ZShwcmVMb2dpbkNvb2tpZVswXS50ZXh0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBzcENvb2tpZSA9PSBudWxsIHx8IHBzcENvb2tpZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHRoaXMuZ2V0VXJsUGFyYW1zKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2aWV3VG9SZWRpcmVjdCA9IHBhcmFtc1sncmVkaXJlY3RWaWV3J107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9qZWN0VG9SZWRpcmVjdCA9IHBhcmFtc1sndGFzayddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGFza1RvUmVkaXJlY3QgPSBwYXJhbXNbJ3Byb2plY3QnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlVG9SZWRpcmVjdCA9IHBhcmFtc1snZGVsaXZlcmFibGUnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBvcHVwVHlwZSA9IHBhcmFtc1sndHlwZSddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXNzZXRLZXkgPSBwYXJhbXNbJ2Fzc2V0S2V5J107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0YXNrSWQgPSBwYXJhbXNbJ3Rhc2tJZCddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvamVjdElkID0gcGFyYW1zWydwcm9qZWN0SWQnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRyYW5zaXRUbyA9IHBhcmFtc1snZ29UbyddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRyYW5zaXRUbyAmJiB0cmFuc2l0VG8gIT0gbnVsbCAmJiB0cmFuc2l0VG8gIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ3RyYW5zaXRUbycsIHRyYW5zaXRUbyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZpZXdUb1JlZGlyZWN0ICE9IG51bGwgJiYgdmlld1RvUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnYXBwUmVkaXJlY3RWaWV3Jywgdmlld1RvUmVkaXJlY3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwcm9qZWN0VG9SZWRpcmVjdCAhPSBudWxsICYmIHByb2plY3RUb1JlZGlyZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbGlua3MgZnJvbSBlbWFpbCBtYXkgaGF2ZSBQcm9qZWN0LCBUYXNrIGFuZCBEZWxpdmVyYWJsZSB0byByZWRpcmVjdCB0aGUgdXNlciB0byBjb3JyZXNwb25kaW5nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ1Byb2plY3RSZWRpcmVjdFZpZXcnLCBwcm9qZWN0VG9SZWRpcmVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRhc2tUb1JlZGlyZWN0ICE9IG51bGwgJiYgdGFza1RvUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBsaW5rcyBmcm9tIGVtYWlsIG1heSBoYXZlIFByb2plY3QsIFRhc2sgYW5kIERlbGl2ZXJhYmxlIHRvIHJlZGlyZWN0IHRoZSB1c2VyIHRvIGNvcnJlc3BvbmRpbmdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnVGFza1JlZGlyZWN0VmlldycsIHRhc2tUb1JlZGlyZWN0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocG9wdXBUeXBlICYmIHBvcHVwVHlwZSAhPSBudWxsICYmIHBvcHVwVHlwZSA9PT0gJ2RlbGl2ZXJhYmxlJyAmJiBhc3NldEtleSAmJiBhc3NldEtleSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ1BvcHVwRGVsaXZlcmFibGUnLCBhc3NldEtleSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHBvcHVwVHlwZSAmJiBwb3B1cFR5cGUgIT0gbnVsbCAmJiBwb3B1cFR5cGUgPT09ICd0YXNrJyAmJiB0YXNrSWQgJiYgdGFza0lkICE9IG51bGwgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0SWQgJiYgcHJvamVjdElkICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwb3B1cFRhc2sgPSB7IHRhc2tJZCwgcHJvamVjdElkIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LnNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ1BvcHVwVGFzaycsIEpTT04uc3RyaW5naWZ5KHBvcHVwVGFzaykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkZWxpdmVyYWJsZVRvUmVkaXJlY3QgIT0gbnVsbCAmJiBkZWxpdmVyYWJsZVRvUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBsaW5rcyBmcm9tIGVtYWlsIG1heSBoYXZlIFByb2plY3QsIFRhc2sgYW5kIERlbGl2ZXJhYmxlIHRvIHJlZGlyZWN0IHRoZSB1c2VyIHRvIGNvcnJlc3BvbmRpbmdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnRGVsaXZlcmFibGVSZWRpcmVjdFZpZXcnLCBkZWxpdmVyYWJsZVRvUmVkaXJlY3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gY29uZmlnLmdldE9URFNMb2dpbkZvcm0oKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdVbmFibGUgdG8gZmlyZSBwcmUtbG9naW4gc2Vzc2lvbiB0byBnZXQgY29va2llIG5hbWUuJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dJbigpIHtcclxuICAgICAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBHZXRVc2VyUHJvZmlsZShzb2FwU2VydmljZXNDb25zdGFudHMuVVNFUl9MREFQKS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1c2VyT2JqZWN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChkYXRhLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXNlckROID0gdXNlck9iamVjdFswXS5hdXRodXNlcmRuO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXNlck5hbWUgPSB1c2VyT2JqZWN0WzBdLmF1dGh1c2VyZG4uc3BsaXQoJywnKVswXS5zcGxpdCgnPScpWzFdO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3JnID0gdXNlck9iamVjdFswXS5hdXRodXNlcmRuLnNwbGl0KCdjbj1hbm9ueW1vdXMsY249YXV0aGVudGljYXRlZCB1c2VycywnKVsxXTtcclxuICAgICAgICAgICAgICAgIGxldCBvcmdETiA9ICdvPScgKyBjb25maWcuY29uZmlnLm9yZ2FuaXphdGlvbk5hbWUgKyAnLCc7XHJcbiAgICAgICAgICAgICAgICBvcmdETiA9IG9yZ0ROLmNvbmNhdChvcmcpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHVzZXJOYW1lID09PSAnYW5vbnltb3VzJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuaXNWYWxpZENvb2tpZShkb2N1bWVudC5jb29raWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcigpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZXJyb3IpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZXJyb3IpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==