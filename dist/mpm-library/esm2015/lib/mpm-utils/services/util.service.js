import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { EntityAppDefService } from './entity.appdef.service';
import { SharingService } from './sharing.service';
import { AppService } from './app.service';
import { Observable, forkJoin } from 'rxjs';
import { CategoryService } from '../../project/shared/services/category.service';
import * as acronui from '../auth/utility';
import * as moment_ from 'moment';
import { StatusService } from '../../shared/services/status.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { IndexerConfigService } from '../../shared/services/indexer/mpm-indexer.config.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import { GloabalConfig as config } from '../config/config';
import * as $ from 'jquery';
import * as i0 from "@angular/core";
import * as i1 from "./app.service";
import * as i2 from "./entity.appdef.service";
import * as i3 from "./sharing.service";
import * as i4 from "../../project/shared/services/category.service";
import * as i5 from "../../shared/services/status.service";
import * as i6 from "../../shared/services/field-config.service";
import * as i7 from "../../shared/services/view-config.service";
import * as i8 from "../../shared/services/indexer/mpm-indexer.config.service";
const moment = moment_;
let UtilService = class UtilService {
    constructor(appService, entityAppDefService, sharingService, categoryService, statusService, fieldConfigService, viewConfigService, indexerConfigService) {
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.sharingService = sharingService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.indexerConfigService = indexerConfigService;
        this.rolesForUser = [];
        this.WL_NSPC = 'http://schemas.cordys.com/notification/workflow/1.0';
        this.USER_NSPC = 'http://schemas.cordys.com/1.1/ldap';
        this.GET_ALL_WORKLIST_FOR_USER = 'GetAllWorklistsForUser';
    }
    setProjectCustomMetadata(projectCustomMetadata) {
        this.newProjectCustomMetadata = projectCustomMetadata;
    }
    getProjectCustomMetadata() {
        return this.newProjectCustomMetadata;
    }
    navigateToApp() {
        window.location.href = this.APP_PATH;
    }
    setUserMappedRoles(roleMappers) {
        const mappedRoles = [];
        for (const roleMapper of roleMappers) {
            if (this.entityAppDefService.hasRole(roleMapper.ROLE_DN)) {
                mappedRoles.push(roleMapper);
            }
        }
        this.sharingService.setUserRoles(mappedRoles);
        console.log('sharing user roles', this.sharingService.getUserRoles());
    }
    // added for code hardeniing 
    getUserDetailsLDAP(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP)));
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.USER_NSPC, 'GetUserDetails', null).subscribe(userDetails => {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_LDAP, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    }
    // added for code hardeniing 
    getUserDetailsWorkflow(parameters) {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_WORKFLOW) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_WORKFLOW)));
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.WL_NSPC, 'GetUserDetails', null).subscribe(userDetails => {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_WORKFLOW, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    }
    formRestUrl(otmmapiurl) {
        const baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        return baseUrl + otmmapiurl;
    }
    // MPMV3-2237
    indexer() {
        const defer = $.Deferred();
        $.ajax({
            url: config.getOTDSRESTAuthEndpointwithHeaders(),
            data: {},
            crossDomain: true,
            contentType: 'application/json',
            dataType: 'json',
            method: 'GET',
        }).done((result) => {
            const otdsAuthInfo = {
                resourceId: result.resourceID,
                ticket: result.ticket,
                token: result.token,
                userId: result.userId,
                failureReason: result.failureReason,
                error: result.error
            };
            console.log("#####################");
            console.log(otdsAuthInfo);
            // store otds-ticket and user id in session storage for headers purpose 
            // console.log('Otds-ticket--  ',otdsAuthInfo)
            sessionStorage.setItem(SessionStorageConstants.OTDS_TICKET, JSON.stringify(otdsAuthInfo.ticket));
            sessionStorage.setItem(SessionStorageConstants.OTDS_USER_ID, JSON.stringify(otdsAuthInfo.userId));
            if (typeof result.error === 'undefined') {
                defer.resolve(otdsAuthInfo);
            }
            else {
                defer.reject(otdsAuthInfo);
            }
        }).fail((error) => {
            console.log(error);
            defer.reject(error);
        });
        return defer.promise();
    }
    setUserInfo() {
        return new Observable(setUserObserver => {
            /* const userDetails = this.appService.invokeRequest(this.WL_NSPC, 'GetUserDetails', null);
            userDetails.subscribe(response => { */
            this.getUserDetailsWorkflow().subscribe(response => {
                const defer = $.Deferred();
                // MPMV3-2237
                if (!isDevMode()) {
                    this.indexer().then((ticketObj) => {
                        console.log(ticketObj);
                    }).fail((errCode) => {
                        console.log(errCode);
                        defer.reject(errCode);
                    });
                }
                this.entityAppDefService.setUserDetails(response);
                this.entityAppDefService.setUserDN(response);
                const currentUser = acronui.findObjectByProp(response, 'User');
                if (currentUser) {
                    if (currentUser.ManagerFor && currentUser.ManagerFor.Target && Array.isArray(currentUser.ManagerFor.Target)) {
                        this.rolesForUser = currentUser.ManagerFor.Target;
                    }
                    else if (currentUser.ManagerFor && currentUser.ManagerFor.Target &&
                        typeof (currentUser.ManagerFor.Target) === 'object') {
                        this.rolesForUser.push(currentUser.ManagerFor.Target);
                    }
                    this.sharingService.setCurrentUserID(currentUser.UserDN.split('=')[1].split(',')[0]);
                    this.sharingService.setCurrentUserObject(currentUser);
                    this.sharingService.setCurrentUserDN(currentUser.UserDN);
                    this.sharingService.setCurrentUserDisplayName(currentUser.UserDisplayName);
                    this.sharingService.setCurrentOrgDN(currentUser.UserDN.split(',').slice(2).join(','));
                    const parameters = {
                        userCN: this.sharingService.getCurrentUserID()
                    };
                    this.appService.getPersonDetailsByUserCN(parameters)
                        .subscribe(userDetailsResponse => {
                        this.sharingService.setCurrentUserItemID(userDetailsResponse.Person.PersonToUser['Identity-id'].Id);
                        this.sharingService.setCurrentUserEmailId(userDetailsResponse.Person.Email.__text);
                        this.appService.getAllRoles()
                            .subscribe(rolesResonse => {
                            const roles = acronui.findObjectsByProp(rolesResonse, 'MPM_APP_Roles');
                            this.sharingService.setAllRoles(roles);
                            // code hardening -Keerthana
                            // this.appService.invokeRequest(this.USER_NSPC, 'GetUserDetails', null)
                            this.getUserDetailsLDAP()
                                .subscribe(getUserDetailsResponse => {
                                this.entityAppDefService.setMasterRoles(getUserDetailsResponse);
                                this.setUserMappedRoles(roles);
                                setUserObserver.next(currentUser);
                                setUserObserver.complete();
                            }, getUserDetailsError => {
                                setUserObserver.error(new Error(getUserDetailsError));
                            });
                        }, rolesError => {
                            setUserObserver.error('NOACCESS');
                            //setUserObserver.error(new Error(rolesError));
                        });
                    }, userDetailsError => {
                        setUserObserver.error('NOACCESS');
                        //setUserObserver.error(new Error(userDetailsError));
                    });
                }
                else {
                    setUserObserver.error(new Error('No user details available.'));
                }
            }, error => {
                setUserObserver.error(new Error(error));
            });
        });
    }
    getUserRoles() {
        if (this.rolesForUser.length > 0) {
            return this.rolesForUser;
        }
        else {
            console.error('No roles are available.');
        }
    }
    getUserGroups() {
        return this.appService.invokeRequest(this.WL_NSPC, this.GET_ALL_WORKLIST_FOR_USER, null);
    }
    setSelectedApp(application) {
        return new Observable(observer => {
            if (application != null && application) {
                this.APP_ID = application.APPLICATION_ID;
                this.entityAppDefService.setApplicationDetails(application)
                    .subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
            }
            else {
                observer.error('No Application ID property is available for application: ' +
                    application.applicationId);
            }
        });
    }
    setConfigsForApp() {
        return new Observable(observer => {
            forkJoin([this.entityAppDefService.getMenuForUser(),
                this.categoryService.getAllCategories(),
                this.entityAppDefService.getAllPriorities(),
                this.statusService.getAllStatusConfigDetails(),
                this.fieldConfigService.getAllMPMFieldConfig(),
            ]).subscribe(response => {
                response[0].sort((a, b) => (parseInt(a.SEQUENCE) > parseInt(b.SEQUENCE)) ? 1 : ((parseInt(b.SEQUENCE) > parseInt(a.SEQUENCE)) ? -1 : 0));
                this.sharingService.setMenu(response[0]);
                this.sharingService.setAllCategories(response[1]);
                this.sharingService.setAllPriorities(response[2]);
                this.sharingService.setAllStatusConfig(response[3] ? response[3] : []);
                this.sharingService.setAllApplicationFields(response[4] ? response[4] : []);
                // this.sharingService.setDefaultCategoryDetails(response[5] ? response[5] : null);
                // const defaultCategory = this.sharingService.getDefaultCategoryDetails();
                const categories = this.sharingService.getAllCategories();
                this.sharingService.setSelectedCategory(categories[0]);
                // const selectedCategory = categories.find(category => category['MPM_Category-id'].Id === defaultCategory.id);
                // this.sharingService.setSelectedCategory(selectedCategory);
                /* forkJoin([
                    this.viewConfigService.GetAllMPMViewConfigBPM(defaultCategory.id),
                    this.indexerConfigService.setIndexerConfigByCategoryId(defaultCategory.id),
                ]).subscribe(viewResponse => {
                    this.sharingService.setAllApplicationViewConfig(viewResponse[0] ? viewResponse[0] : []);
                    this.categoryService.getAllCategoryLevelDetailsByCategoryId(defaultCategory.id)
                        .subscribe(categoryLevelResponse => {
                            this.sharingService.setCategoryLevels(categoryLevelResponse);
                            observer.next(true);
                            observer.complete();
                        }, error => {
                            observer.error(error);
                        });
                }, error => {
                    observer.error(error);
                }); */
                forkJoin([
                    this.viewConfigService.GetAllMPMViewConfigBPM(categories[0]['MPM_Category-id'].Id),
                    this.indexerConfigService.setIndexerConfigByCategoryId(categories[0]['MPM_Category-id'].Id),
                ]).subscribe(viewResponse => {
                    this.sharingService.setAllApplicationViewConfig(viewResponse[0] ? viewResponse[0] : []);
                    this.categoryService.getAllCategoryLevelDetailsByCategoryId(categories[0]['MPM_Category-id'].Id)
                        .subscribe(categoryLevelResponse => {
                        this.sharingService.setCategoryLevels(categoryLevelResponse);
                        observer.next(true);
                        observer.complete();
                    }, error => {
                        observer.error(error);
                    });
                }, error => {
                    observer.error(error);
                });
            });
        });
    }
    initiateDownload(fileURL, fileName) {
        // for non-IE
        if (!window['ActiveXObject']) {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', fileURL, true);
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.withCredentials = true;
            xhr.responseType = 'blob';
            xhr.onload = function () {
                const urlCreator = window.URL || window['webkitURL'];
                const imageUrl = urlCreator.createObjectURL(this.response);
                const tag = document.createElement('a');
                tag.href = imageUrl;
                tag.download = fileName;
                document.body.appendChild(tag);
                tag.click();
                document.body.removeChild(tag);
            };
            xhr.send();
        }
        else if (window['ActiveXObject'] && document.execCommand) {
            const newWindow = window.open(fileURL, '_blank');
            newWindow.document.close();
            newWindow.document.execCommand('SaveAs', true, fileName || fileURL);
            newWindow.close();
        }
    }
    isNullOrEmpty(data) {
        if ((data['@xsi:nil'] === 'true') || (data['@nil'] === 'true') || (data === '')) {
            return true;
        }
        else {
            return false;
        }
    }
    getBooleanValue(data) {
        return data === 'true' ? true : false;
    }
    isValid(variableToCheck) {
        if (variableToCheck && variableToCheck != null && variableToCheck !== '') {
            return true;
        }
        return false;
    }
    isArray(variableToCheck) {
        if (variableToCheck && variableToCheck != null && Array.isArray(variableToCheck)) {
            return true;
        }
        return false;
    }
    currentUserHasRole(roleName) {
        let hasRole = false;
        if (!roleName) {
            return hasRole;
        }
        let currentUserRoles = this.sharingService.getCurrentUserObject().ManagerFor.Target;
        if (currentUserRoles && !Array.isArray(currentUserRoles)) {
            const obj = currentUserRoles;
            currentUserRoles = [obj];
        }
        for (const currentUserRole of currentUserRoles) {
            if (currentUserRole.Name && (currentUserRole.Name.toLowerCase() === roleName.toLowerCase())) {
                hasRole = true;
                break;
            }
        }
        return hasRole;
    }
    isEqualWithCase(string1, string2) {
        if (string1 && string2 && (string1 === string2)) {
            return true;
        }
        return false;
    }
    openWindow(urlToOpen, windowName, features) {
        let windowHandle = null;
        let previousURL = null;
        if (windowHandle == null || windowHandle.closed) {
            windowHandle = window.open(urlToOpen, windowName, features);
        }
        else if (previousURL !== urlToOpen) {
            windowHandle = window.open(urlToOpen, windowName, features);
            windowHandle.focus();
        }
        else {
            windowHandle.focus();
        }
        previousURL = urlToOpen;
    }
    getAllQueryParametersFromURL(url) {
        let queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        const obj = {};
        if (queryString) {
            queryString = queryString.split('#')[0];
            const queries = queryString.split('&');
            for (const query of queries) {
                const a = query.split('=');
                let paramName = a[0];
                let paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
                paramName = paramName.toLowerCase();
                if (typeof paramValue === 'string') {
                    paramValue = paramValue.toLowerCase();
                }
                if (paramName.match(/\[(\d+)?\]$/)) {
                    const key = paramName.replace(/\[(\d+)?\]/, '');
                    if (!obj[key]) {
                        obj[key] = [];
                    }
                    if (paramName.match(/\[\d+\]$/)) {
                        const index = /\[(\d+)\]/.exec(paramName)[1];
                        obj[key][index] = paramValue;
                    }
                    else {
                        obj[key].push(paramValue);
                    }
                }
                else {
                    if (!obj[paramName]) {
                        obj[paramName] = paramValue;
                    }
                    else if (obj[paramName] && typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                        obj[paramName].push(paramValue);
                    }
                    else {
                        obj[paramName].push(paramValue);
                    }
                }
            }
        }
        return obj;
    }
    getQueryParameterFromURL(parameter, url) {
        const reg = new RegExp('[?&]' + parameter + '=([^&#]*)', 'i');
        const str = reg.exec(url);
        return str ? str[1] : undefined;
    }
    getPriorityIcon(name, categoryLevelType) {
        const currentCategoryLevel = this.sharingService.getCategoryLevels().find((eachCategoryLevel) => {
            return eachCategoryLevel.CATEGORY_LEVEL_TYPE === categoryLevelType;
        });
        let priorityObj = null;
        const priorityReturnObj = { icon: '', color: '', tooltip: '' };
        if (currentCategoryLevel && currentCategoryLevel['MPM_Category_Level-id']) {
            priorityObj = this.sharingService.getAllPriorities()
                .find(priority => name === priority.NAME && priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currentCategoryLevel['MPM_Category_Level-id'].Id);
            if (!priorityObj) {
                return priorityReturnObj;
            }
            return {
                icon: priorityObj.ICON_DIRECTION === 'Up' ? 'arrow_upward' : 'arrow_downward',
                color: priorityObj.ICON_COLOR,
                tooltip: priorityObj.DESCRIPTION
            };
        }
        return priorityReturnObj;
    }
    /* Define function for escaping user input to be treated as
        a literal string within a regular expression */
    escapeRegExp(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    /* Define functin to find and replace specified term with replacement string */
    replaceAll(str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    }
    getLookupDomainValuesById(lookupDomainId) {
        const lookupDomains = this.sharingService.getAllLookupDomains();
        if (Array.isArray(lookupDomains) && lookupDomains.length === 0) {
            return {};
        }
        const tmpLookupDomains = lookupDomains.find(lookupDomain => lookupDomain.domainId === lookupDomainId);
        if (tmpLookupDomains && !Array.isArray(tmpLookupDomains)) {
            return tmpLookupDomains.domainValues;
        }
        return {};
    }
    GetRoleDNByRoleId(roleId) {
        const allRoles = this.sharingService.getAllRoles();
        return allRoles.find(role => role['MPM_APP_Roles-id'].Id === roleId);
    }
    getTeamRoleDNByRoleId(roleId) {
        const allRoles = this.sharingService.getAllTeamRoles();
        return allRoles.find(role => parseInt(role['MPM_Team_Role_Mapping-id'].Id) === roleId);
    }
    convertToOTMMDateFormat(date) {
        date = new Date(date);
        return moment(date).format('MM/DD/YYYYT00:00:00');
    }
    removeSpclCharsFromAssetName(assetName) {
        assetName = assetName.trim();
        if (assetName.includes('$') || assetName.includes('\'')) {
            assetName = assetName.replace(/[$']/g, '_');
        }
        return assetName;
    }
    convertStringToNumber(data) {
        var pageSize = data;
        var pageNumber = +pageSize;
        return pageNumber;
    }
    getDisplayOrderData(data, displayOrder) {
        const fieldObjArray = [];
        if (this.isValid(displayOrder) && !displayOrder['@null']) {
            const arrDisplayOrder = displayOrder.split(',');
            arrDisplayOrder.forEach(element => {
                fieldObjArray.push(data.find(fieldData => {
                    return fieldData.MAPPER_NAME === element;
                }));
            });
        }
        return (fieldObjArray.length > 0 && data.length === fieldObjArray.length) ? fieldObjArray : data;
    }
};
UtilService.ctorParameters = () => [
    { type: AppService },
    { type: EntityAppDefService },
    { type: SharingService },
    { type: CategoryService },
    { type: StatusService },
    { type: FieldConfigService },
    { type: ViewConfigService },
    { type: IndexerConfigService }
];
UtilService.ɵprov = i0.ɵɵdefineInjectable({ factory: function UtilService_Factory() { return new UtilService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.SharingService), i0.ɵɵinject(i4.CategoryService), i0.ɵɵinject(i5.StatusService), i0.ɵɵinject(i6.FieldConfigService), i0.ɵɵinject(i7.ViewConfigService), i0.ɵɵinject(i8.IndexerConfigService)); }, token: UtilService, providedIn: "root" });
UtilService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], UtilService);
export { UtilService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBRWpGLE9BQU8sS0FBSyxPQUFPLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7QUFJbEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBRTNGLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7Ozs7Ozs7Ozs7QUFFNUIsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBTXZCLElBQWEsV0FBVyxHQUF4QixNQUFhLFdBQVc7SUFFcEIsWUFDVyxVQUFzQixFQUN0QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsZUFBZ0MsRUFDaEMsYUFBNEIsRUFDNUIsa0JBQXNDLEVBQ3RDLGlCQUFvQyxFQUNwQyxvQkFBMEM7UUFQMUMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFLckQsaUJBQVksR0FBZSxFQUFFLENBQUM7UUFDOUIsWUFBTyxHQUFHLHFEQUFxRCxDQUFDO1FBQ2hFLGNBQVMsR0FBRyxvQ0FBb0MsQ0FBQztRQUNqRCw4QkFBeUIsR0FBRyx3QkFBd0IsQ0FBQztJQVBqRCxDQUFDO0lBVUwsd0JBQXdCLENBQUMscUJBQTBCO1FBQy9DLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxxQkFBcUIsQ0FBQztJQUMxRCxDQUFDO0lBRUQsd0JBQXdCO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLHdCQUF3QixDQUFDO0lBQ3pDLENBQUM7SUFFRCxhQUFhO1FBQ1QsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QyxDQUFDO0lBRUQsa0JBQWtCLENBQUMsV0FBeUI7UUFDeEMsTUFBTSxXQUFXLEdBQWlCLEVBQUUsQ0FBQztRQUNyQyxLQUFLLE1BQU0sVUFBVSxJQUFJLFdBQVcsRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN0RCxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDO1NBQ0o7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsNkJBQTZCO0lBQzdCLGtCQUFrQixDQUFDLFVBQVc7UUFDMUIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQzVFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM3RixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQzFGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUMvRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2QkFBNkI7SUFDN0Isc0JBQXNCLENBQUMsVUFBVztRQUM5QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDaEYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pHLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBRTtvQkFDeEYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQ25HLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELFdBQVcsQ0FBQyxVQUFVO1FBQ2xCLE1BQU0sT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFHckYsT0FBTyxPQUFPLEdBQUcsVUFBVSxDQUFDO0lBRWhDLENBQUM7SUFFRCxhQUFhO0lBQ2IsT0FBTztRQUNILE1BQU0sS0FBSyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ0gsR0FBRyxFQUFFLE1BQU0sQ0FBQyxrQ0FBa0MsRUFBRTtZQUNoRCxJQUFJLEVBQUUsRUFBRTtZQUNSLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFdBQVcsRUFBRSxrQkFBa0I7WUFDL0IsUUFBUSxFQUFFLE1BQU07WUFDaEIsTUFBTSxFQUFFLEtBQUs7U0FJaEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ2YsTUFBTSxZQUFZLEdBQUc7Z0JBQ2pCLFVBQVUsRUFBRSxNQUFNLENBQUMsVUFBVTtnQkFDN0IsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNO2dCQUNyQixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7Z0JBQ25CLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTTtnQkFDckIsYUFBYSxFQUFFLE1BQU0sQ0FBQyxhQUFhO2dCQUNuQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7YUFDdEIsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRTFCLHdFQUF3RTtZQUN4RSw4Q0FBOEM7WUFDOUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNqRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2xHLElBQUksT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFdBQVcsRUFBRTtnQkFDckMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUMvQjtpQkFBTTtnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzlCO1FBRUwsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ2xCLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsV0FBVztRQUNQLE9BQU8sSUFBSSxVQUFVLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDcEM7a0RBQ3NDO1lBQ3RDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDL0MsTUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUMzQixhQUFhO2dCQUNYLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQkFDZCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUNmLENBQUMsU0FBUyxFQUFFLEVBQUU7d0JBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDM0IsQ0FBQyxDQUNKLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7d0JBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDckIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLENBQUM7aUJBQ047Z0JBRUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDN0MsTUFBTSxXQUFXLEdBQVEsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxXQUFXLEVBQUU7b0JBQ2IsSUFBSSxXQUFXLENBQUMsVUFBVSxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDekcsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztxQkFDckQ7eUJBQU0sSUFBSSxXQUFXLENBQUMsVUFBVSxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTTt3QkFDOUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssUUFBUSxFQUFFO3dCQUNyRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN6RDtvQkFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyRixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDekQsSUFBSSxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQzNFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDdEYsTUFBTSxVQUFVLEdBQUc7d0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7cUJBQ2pELENBQUM7b0JBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUM7eUJBQy9DLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO3dCQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQ3BHLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDbkYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7NkJBQ3hCLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTs0QkFDdEIsTUFBTSxLQUFLLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsQ0FBQzs0QkFDdkUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3ZDLDRCQUE0Qjs0QkFDNUIsd0VBQXdFOzRCQUN4RSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7aUNBQ3BCLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO2dDQUNoQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0NBQ2hFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDL0IsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQ0FDbEMsZUFBZSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUMvQixDQUFDLEVBQUUsbUJBQW1CLENBQUMsRUFBRTtnQ0FDckIsZUFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7NEJBQzFELENBQUMsQ0FBQyxDQUFDO3dCQUNYLENBQUMsRUFBRSxVQUFVLENBQUMsRUFBRTs0QkFDWixlQUFlLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUNsQywrQ0FBK0M7d0JBQ25ELENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsRUFBRSxnQkFBZ0IsQ0FBQyxFQUFFO3dCQUNsQixlQUFlLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUNsQyxxREFBcUQ7b0JBQ3pELENBQUMsQ0FBQyxDQUFDO2lCQUNWO3FCQUFNO29CQUNILGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO2lCQUNsRTtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxlQUFlLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZO1FBQ1IsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDOUIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1NBQzVCO2FBQU07WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsYUFBYTtRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDN0YsQ0FBQztJQUVELGNBQWMsQ0FBQyxXQUFXO1FBQ3RCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxXQUFXLElBQUksSUFBSSxJQUFJLFdBQVcsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsY0FBYyxDQUFDO2dCQUN6QyxJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDO3FCQUN0RCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQywyREFBMkQ7b0JBQ3RFLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUNsQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFO2dCQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLHlCQUF5QixFQUFFO2dCQUM5QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CLEVBQUU7YUFFN0MsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDdkIsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDbkksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUUsbUZBQW1GO2dCQUNuRiwyRUFBMkU7Z0JBQzNFLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkQsK0dBQStHO2dCQUMvRyw2REFBNkQ7Z0JBQzdEOzs7Ozs7Ozs7Ozs7Ozs7c0JBZU07Z0JBQ04sUUFBUSxDQUFDO29CQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyw0QkFBNEIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzlGLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsMkJBQTJCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUN4RixJQUFJLENBQUMsZUFBZSxDQUFDLHNDQUFzQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt5QkFDM0YsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQzt3QkFDN0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7d0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsUUFBUTtRQUM5QixhQUFhO1FBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUMxQixNQUFNLEdBQUcsR0FBRyxJQUFJLGNBQWMsRUFBRSxDQUFDO1lBQ2pDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvQixHQUFHLENBQUMsZ0JBQWdCLENBQUMsNkJBQTZCLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDekQsR0FBRyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDM0IsR0FBRyxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7WUFDMUIsR0FBRyxDQUFDLE1BQU0sR0FBRztnQkFDVCxNQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckQsTUFBTSxRQUFRLEdBQUcsVUFBVSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNELE1BQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3hDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO2dCQUNwQixHQUFHLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQy9CLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDWixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQyxDQUFDLENBQUM7WUFDRixHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZDthQUFNLElBQUksTUFBTSxDQUFDLGVBQWUsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7WUFDeEQsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDakQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMzQixTQUFTLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLFFBQVEsSUFBSSxPQUFPLENBQUMsQ0FBQztZQUNwRSxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDckI7SUFDTCxDQUFDO0lBRUQsYUFBYSxDQUFDLElBQUk7UUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxFQUFFO1lBQzdFLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQztJQUVELGVBQWUsQ0FBQyxJQUFJO1FBQ2hCLE9BQU8sSUFBSSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDMUMsQ0FBQztJQUVELE9BQU8sQ0FBQyxlQUFlO1FBQ25CLElBQUksZUFBZSxJQUFJLGVBQWUsSUFBSSxJQUFJLElBQUksZUFBZSxLQUFLLEVBQUUsRUFBRTtZQUN0RSxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELE9BQU8sQ0FBQyxlQUFlO1FBQ25CLElBQUksZUFBZSxJQUFJLGVBQWUsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUM5RSxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGtCQUFrQixDQUFDLFFBQVE7UUFDdkIsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxPQUFPLE9BQU8sQ0FBQztTQUNsQjtRQUNELElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7UUFDcEYsSUFBSSxnQkFBZ0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtZQUN0RCxNQUFNLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQztZQUM3QixnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzVCO1FBQ0QsS0FBSyxNQUFNLGVBQWUsSUFBSSxnQkFBZ0IsRUFBRTtZQUM1QyxJQUFJLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxFQUFFO2dCQUN6RixPQUFPLEdBQUcsSUFBSSxDQUFDO2dCQUNmLE1BQU07YUFDVDtTQUNKO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFPLEVBQUUsT0FBTztRQUM1QixJQUFJLE9BQU8sSUFBSSxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssT0FBTyxDQUFDLEVBQUU7WUFDN0MsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxVQUFVLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRO1FBQ3RDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxZQUFZLElBQUksSUFBSSxJQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDN0MsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUMvRDthQUFNLElBQUksV0FBVyxLQUFLLFNBQVMsRUFBRTtZQUNsQyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzVELFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN4QjthQUFNO1lBQ0gsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3hCO1FBQ0QsV0FBVyxHQUFHLFNBQVMsQ0FBQztJQUM1QixDQUFDO0lBRUQsNEJBQTRCLENBQUMsR0FBRztRQUM1QixJQUFJLFdBQVcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM1RSxNQUFNLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFFZixJQUFJLFdBQVcsRUFBRTtZQUNiLFdBQVcsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sT0FBTyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFdkMsS0FBSyxNQUFNLEtBQUssSUFBSSxPQUFPLEVBQUU7Z0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsSUFBSSxVQUFVLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRTdELFNBQVMsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3BDLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxFQUFFO29CQUFFLFVBQVUsR0FBRyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQUU7Z0JBRTlFLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsRUFBRTtvQkFDaEMsTUFBTSxHQUFHLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ2hELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQkFBRTtvQkFFakMsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFFO3dCQUM3QixNQUFNLEtBQUssR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM3QyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsVUFBVSxDQUFDO3FCQUNoQzt5QkFBTTt3QkFDSCxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUM3QjtpQkFDSjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUNqQixHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsVUFBVSxDQUFDO3FCQUMvQjt5QkFBTSxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxRQUFRLEVBQUU7d0JBQzdELEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO3dCQUNsQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUNuQzt5QkFBTTt3QkFDSCxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUNuQztpQkFDSjthQUNKO1NBQ0o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxTQUFTLEVBQUUsR0FBRztRQUNuQyxNQUFNLEdBQUcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsU0FBUyxHQUFHLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5RCxNQUFNLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzFCLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZUFBZSxDQUFDLElBQVksRUFBRSxpQkFBNEI7UUFDdEQsTUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsaUJBQWlCLEVBQUUsRUFBRTtZQUM1RixPQUFPLGlCQUFpQixDQUFDLG1CQUFtQixLQUFLLGlCQUFpQixDQUFDO1FBQ3ZFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLE1BQU0saUJBQWlCLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQy9ELElBQUksb0JBQW9CLElBQUksb0JBQW9CLENBQUMsdUJBQXVCLENBQUMsRUFBRTtZQUN2RSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRTtpQkFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxLQUFLLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxLQUFLLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDL0osSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDZCxPQUFPLGlCQUFpQixDQUFDO2FBQzVCO1lBQ0QsT0FBTztnQkFDSCxJQUFJLEVBQUUsV0FBVyxDQUFDLGNBQWMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCO2dCQUM3RSxLQUFLLEVBQUUsV0FBVyxDQUFDLFVBQVU7Z0JBQzdCLE9BQU8sRUFBRSxXQUFXLENBQUMsV0FBVzthQUNuQyxDQUFDO1NBQ0w7UUFDRCxPQUFPLGlCQUFpQixDQUFDO0lBQzdCLENBQUM7SUFFRDt1REFDbUQ7SUFDbkQsWUFBWSxDQUFDLEdBQUc7UUFDWixPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELCtFQUErRTtJQUMvRSxVQUFVLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxXQUFXO1FBQzdCLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyxjQUFzQjtRQUM1QyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDaEUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzVELE9BQU8sRUFBRSxDQUFDO1NBQ2I7UUFDRCxNQUFNLGdCQUFnQixHQUFHLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxLQUFLLGNBQWMsQ0FBQyxDQUFDO1FBQ3RHLElBQUksZ0JBQWdCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7WUFDdEQsT0FBTyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7U0FDeEM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxNQUFNO1FBQ3BCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRSxLQUFLLE1BQU0sQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxNQUFNO1FBQ3hCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxDQUFDO0lBQzNGLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxJQUFJO1FBQ3hCLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0QixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsNEJBQTRCLENBQUMsU0FBUztRQUNsQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzdCLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JELFNBQVMsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztTQUMvQztRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxJQUFJO1FBQ3RCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLFVBQVUsR0FBVyxDQUFDLFFBQVEsQ0FBQztRQUNuQyxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDO0lBRUQsbUJBQW1CLENBQUMsSUFBSSxFQUFFLFlBQVk7UUFDbEMsTUFBTSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN0RCxNQUFNLGVBQWUsR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hELGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQzlCLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtvQkFDckMsT0FBTyxTQUFTLENBQUMsV0FBVyxLQUFLLE9BQU8sQ0FBQztnQkFDN0MsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxPQUFPLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3JHLENBQUM7Q0FFSixDQUFBOztZQTdmMEIsVUFBVTtZQUNELG1CQUFtQjtZQUN4QixjQUFjO1lBQ2IsZUFBZTtZQUNqQixhQUFhO1lBQ1Isa0JBQWtCO1lBQ25CLGlCQUFpQjtZQUNkLG9CQUFvQjs7O0FBVjVDLFdBQVc7SUFKdkIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLFdBQVcsQ0FnZ0J2QjtTQWhnQlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4vc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4vYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9jYXRlZ29yeS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vYXV0aC91dGlsaXR5JztcclxuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgUm9sZXMgfSBmcm9tICcuLi9vYmplY3RzL1JvbGVzJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zZWFyY2gtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3N0YXR1cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kZXhlckNvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9tcG0taW5kZXhlci5jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcblxyXG5pbXBvcnQgeyBHbG9hYmFsQ29uZmlnIGFzIGNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcblxyXG5jb25zdCBtb21lbnQgPSBtb21lbnRfO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVXRpbFNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNhdGVnb3J5U2VydmljZTogQ2F0ZWdvcnlTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBpbmRleGVyQ29uZmlnU2VydmljZTogSW5kZXhlckNvbmZpZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgQVBQX1BBVEg6IHN0cmluZztcclxuICAgIEFQUF9JRDogc3RyaW5nO1xyXG4gICAgcm9sZXNGb3JVc2VyOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICBXTF9OU1BDID0gJ2h0dHA6Ly9zY2hlbWFzLmNvcmR5cy5jb20vbm90aWZpY2F0aW9uL3dvcmtmbG93LzEuMCc7XHJcbiAgICBVU0VSX05TUEMgPSAnaHR0cDovL3NjaGVtYXMuY29yZHlzLmNvbS8xLjEvbGRhcCc7XHJcbiAgICBHRVRfQUxMX1dPUktMSVNUX0ZPUl9VU0VSID0gJ0dldEFsbFdvcmtsaXN0c0ZvclVzZXInO1xyXG4gICAgbmV3UHJvamVjdEN1c3RvbU1ldGFkYXRhOiBhbnk7XHJcblxyXG4gICAgc2V0UHJvamVjdEN1c3RvbU1ldGFkYXRhKHByb2plY3RDdXN0b21NZXRhZGF0YTogYW55KSB7XHJcbiAgICAgICAgdGhpcy5uZXdQcm9qZWN0Q3VzdG9tTWV0YWRhdGEgPSBwcm9qZWN0Q3VzdG9tTWV0YWRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdEN1c3RvbU1ldGFkYXRhKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubmV3UHJvamVjdEN1c3RvbU1ldGFkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIG5hdmlnYXRlVG9BcHAoKSB7XHJcbiAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB0aGlzLkFQUF9QQVRIO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFVzZXJNYXBwZWRSb2xlcyhyb2xlTWFwcGVyczogQXJyYXk8Um9sZXM+KSB7XHJcbiAgICAgICAgY29uc3QgbWFwcGVkUm9sZXM6IEFycmF5PFJvbGVzPiA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3Qgcm9sZU1hcHBlciBvZiByb2xlTWFwcGVycykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmhhc1JvbGUocm9sZU1hcHBlci5ST0xFX0ROKSkge1xyXG4gICAgICAgICAgICAgICAgbWFwcGVkUm9sZXMucHVzaChyb2xlTWFwcGVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFVzZXJSb2xlcyhtYXBwZWRSb2xlcyk7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ3NoYXJpbmcgdXNlciByb2xlcycsIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0VXNlclJvbGVzKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGFkZGVkIGZvciBjb2RlIGhhcmRlbmlpbmcgXHJcbiAgICBnZXRVc2VyRGV0YWlsc0xEQVAocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19MREFQKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX0xEQVApKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5VU0VSX05TUEMsICdHZXRVc2VyRGV0YWlscycsIG51bGwpLnN1YnNjcmliZSh1c2VyRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5VU0VSX0RFVEFJTFNfTERBUCwgSlNPTi5zdHJpbmdpZnkodXNlckRldGFpbHMpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVzZXJEZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBhZGRlZCBmb3IgY29kZSBoYXJkZW5paW5nIFxyXG4gICAgZ2V0VXNlckRldGFpbHNXb3JrZmxvdyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX1dPUktGTE9XKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX1dPUktGTE9XKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuV0xfTlNQQywgJ0dldFVzZXJEZXRhaWxzJywgbnVsbCkuc3Vic2NyaWJlKHVzZXJEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19XT1JLRkxPVywgSlNPTi5zdHJpbmdpZnkodXNlckRldGFpbHMpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVzZXJEZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGZvcm1SZXN0VXJsKG90bW1hcGl1cmwpIHtcclxuICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcblxyXG5cclxuICAgICAgICByZXR1cm4gYmFzZVVybCArIG90bW1hcGl1cmw7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8vIE1QTVYzLTIyMzdcclxuICAgIGluZGV4ZXIoKSB7XHJcbiAgICAgICAgY29uc3QgZGVmZXIgPSAkLkRlZmVycmVkKCk7XHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgdXJsOiBjb25maWcuZ2V0T1REU1JFU1RBdXRoRW5kcG9pbnR3aXRoSGVhZGVycygpLCAvL3RoaXMuZm9ybVJlc3RVcmwoJ290ZHN3cy9yZXN0L2F1dGhlbnRpY2F0aW9uL2hlYWRlcnMnKSwgLy9jb25maWcuZ2V0T1REU1JFU1RBdXRoRW5kcG9pbnR3aXRoSGVhZGVycygpLCBcclxuICAgICAgICAgICAgZGF0YToge30sXHJcbiAgICAgICAgICAgIGNyb3NzRG9tYWluOiB0cnVlLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nLFxyXG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICAgICAgIC8vIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAvLyAgICAgJ09URFNUaWNrZXQnOiBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19USUNLRVRTKSlcclxuICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgfSkuZG9uZSgocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IG90ZHNBdXRoSW5mbyA9IHtcclxuICAgICAgICAgICAgICAgIHJlc291cmNlSWQ6IHJlc3VsdC5yZXNvdXJjZUlELFxyXG4gICAgICAgICAgICAgICAgdGlja2V0OiByZXN1bHQudGlja2V0LFxyXG4gICAgICAgICAgICAgICAgdG9rZW46IHJlc3VsdC50b2tlbixcclxuICAgICAgICAgICAgICAgIHVzZXJJZDogcmVzdWx0LnVzZXJJZCxcclxuICAgICAgICAgICAgICAgIGZhaWx1cmVSZWFzb246IHJlc3VsdC5mYWlsdXJlUmVhc29uLFxyXG4gICAgICAgICAgICAgICAgZXJyb3I6IHJlc3VsdC5lcnJvclxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiMjIyMjIyMjIyMjIyMjIyMjIyMjI1wiKVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhvdGRzQXV0aEluZm8pO1xyXG5cclxuICAgICAgICAgICAgLy8gc3RvcmUgb3Rkcy10aWNrZXQgYW5kIHVzZXIgaWQgaW4gc2Vzc2lvbiBzdG9yYWdlIGZvciBoZWFkZXJzIHB1cnBvc2UgXHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdPdGRzLXRpY2tldC0tICAnLG90ZHNBdXRoSW5mbylcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1RJQ0tFVCwgSlNPTi5zdHJpbmdpZnkob3Rkc0F1dGhJbmZvLnRpY2tldCkpO1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVVNFUl9JRCwgSlNPTi5zdHJpbmdpZnkob3Rkc0F1dGhJbmZvLnVzZXJJZCkpO1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdC5lcnJvciA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgIGRlZmVyLnJlc29sdmUob3Rkc0F1dGhJbmZvKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRlZmVyLnJlamVjdChvdGRzQXV0aEluZm8pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0pLmZhaWwoKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICBkZWZlci5yZWplY3QoZXJyb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBkZWZlci5wcm9taXNlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VXNlckluZm8oKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoc2V0VXNlck9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgLyogY29uc3QgdXNlckRldGFpbHMgPSB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLldMX05TUEMsICdHZXRVc2VyRGV0YWlscycsIG51bGwpO1xyXG4gICAgICAgICAgICB1c2VyRGV0YWlscy5zdWJzY3JpYmUocmVzcG9uc2UgPT4geyAqL1xyXG4gICAgICAgICAgICB0aGlzLmdldFVzZXJEZXRhaWxzV29ya2Zsb3coKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGVmZXIgPSAkLkRlZmVycmVkKCk7XHJcbiAgICAgICAgICAgICAgICAvLyBNUE1WMy0yMjM3XHJcbiAgICAgICAgICAgICAgICAgIGlmICghaXNEZXZNb2RlKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlcigpLnRoZW4oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKHRpY2tldE9iaikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aWNrZXRPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICkuZmFpbCgoZXJyQ29kZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVyckNvZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmVyLnJlamVjdChlcnJDb2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLnNldFVzZXJEZXRhaWxzKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5zZXRVc2VyRE4ocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudFVzZXI6IGFueSA9IGFjcm9udWkuZmluZE9iamVjdEJ5UHJvcChyZXNwb25zZSwgJ1VzZXInKTtcclxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50VXNlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50VXNlci5NYW5hZ2VyRm9yICYmIGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0ICYmIEFycmF5LmlzQXJyYXkoY3VycmVudFVzZXIuTWFuYWdlckZvci5UYXJnZXQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm9sZXNGb3JVc2VyID0gY3VycmVudFVzZXIuTWFuYWdlckZvci5UYXJnZXQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjdXJyZW50VXNlci5NYW5hZ2VyRm9yICYmIGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGVvZiAoY3VycmVudFVzZXIuTWFuYWdlckZvci5UYXJnZXQpID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvbGVzRm9yVXNlci5wdXNoKGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDdXJyZW50VXNlcklEKGN1cnJlbnRVc2VyLlVzZXJETi5zcGxpdCgnPScpWzFdLnNwbGl0KCcsJylbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJPYmplY3QoY3VycmVudFVzZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJETihjdXJyZW50VXNlci5Vc2VyRE4pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJEaXNwbGF5TmFtZShjdXJyZW50VXNlci5Vc2VyRGlzcGxheU5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudE9yZ0ROKGN1cnJlbnRVc2VyLlVzZXJETi5zcGxpdCgnLCcpLnNsaWNlKDIpLmpvaW4oJywnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlckNOOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySUQoKVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkRldGFpbHNCeVVzZXJDTihwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVzZXJEZXRhaWxzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDdXJyZW50VXNlckl0ZW1JRCh1c2VyRGV0YWlsc1Jlc3BvbnNlLlBlcnNvbi5QZXJzb25Ub1VzZXJbJ0lkZW50aXR5LWlkJ10uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDdXJyZW50VXNlckVtYWlsSWQodXNlckRldGFpbHNSZXNwb25zZS5QZXJzb24uRW1haWwuX190ZXh0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRBbGxSb2xlcygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyb2xlc1Jlc29uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByb2xlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aocm9sZXNSZXNvbnNlLCAnTVBNX0FQUF9Sb2xlcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFJvbGVzKHJvbGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29kZSBoYXJkZW5pbmcgLUtlZXJ0aGFuYVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlVTRVJfTlNQQywgJ0dldFVzZXJEZXRhaWxzJywgbnVsbClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRVc2VyRGV0YWlsc0xEQVAoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShnZXRVc2VyRGV0YWlsc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2Uuc2V0TWFzdGVyUm9sZXMoZ2V0VXNlckRldGFpbHNSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRVc2VyTWFwcGVkUm9sZXMocm9sZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFVzZXJPYnNlcnZlci5uZXh0KGN1cnJlbnRVc2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGdldFVzZXJEZXRhaWxzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFVzZXJPYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZ2V0VXNlckRldGFpbHNFcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgcm9sZXNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFVzZXJPYnNlcnZlci5lcnJvcignTk9BQ0NFU1MnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9zZXRVc2VyT2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKHJvbGVzRXJyb3IpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXNlckRldGFpbHNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIuZXJyb3IoJ05PQUNDRVNTJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL3NldFVzZXJPYnNlcnZlci5lcnJvcihuZXcgRXJyb3IodXNlckRldGFpbHNFcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VXNlck9ic2VydmVyLmVycm9yKG5ldyBFcnJvcignTm8gdXNlciBkZXRhaWxzIGF2YWlsYWJsZS4nKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHNldFVzZXJPYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZXJyb3IpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlclJvbGVzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnJvbGVzRm9yVXNlci5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnJvbGVzRm9yVXNlcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdObyByb2xlcyBhcmUgYXZhaWxhYmxlLicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VyR3JvdXBzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuV0xfTlNQQywgdGhpcy5HRVRfQUxMX1dPUktMSVNUX0ZPUl9VU0VSLCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRTZWxlY3RlZEFwcChhcHBsaWNhdGlvbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKGFwcGxpY2F0aW9uICE9IG51bGwgJiYgYXBwbGljYXRpb24pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuQVBQX0lEID0gYXBwbGljYXRpb24uQVBQTElDQVRJT05fSUQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2Uuc2V0QXBwbGljYXRpb25EZXRhaWxzKGFwcGxpY2F0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdObyBBcHBsaWNhdGlvbiBJRCBwcm9wZXJ0eSBpcyBhdmFpbGFibGUgZm9yIGFwcGxpY2F0aW9uOiAnICtcclxuICAgICAgICAgICAgICAgICAgICBhcHBsaWNhdGlvbi5hcHBsaWNhdGlvbklkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENvbmZpZ3NGb3JBcHAoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBmb3JrSm9pbihbdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldE1lbnVGb3JVc2VyKCksXHJcbiAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldEFsbENhdGVnb3JpZXMoKSxcclxuICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldEFsbFByaW9yaXRpZXMoKSxcclxuICAgICAgICAgICAgdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0NvbmZpZ0RldGFpbHMoKSxcclxuICAgICAgICAgICAgdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0QWxsTVBNRmllbGRDb25maWcoKSxcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3J5RGV0YWlscygpXHJcbiAgICAgICAgICAgIF0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICByZXNwb25zZVswXS5zb3J0KChhLGIpID0+IChwYXJzZUludChhLlNFUVVFTkNFKT4gcGFyc2VJbnQoYi5TRVFVRU5DRSkpID8gMSA6ICgocGFyc2VJbnQoYi5TRVFVRU5DRSkgPiBwYXJzZUludChhLlNFUVVFTkNFKSkgPyAtMSA6IDApKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRNZW51KHJlc3BvbnNlWzBdKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsQ2F0ZWdvcmllcyhyZXNwb25zZVsxXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFByaW9yaXRpZXMocmVzcG9uc2VbMl0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxTdGF0dXNDb25maWcocmVzcG9uc2VbM10gPyByZXNwb25zZVszXSA6IFtdKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsQXBwbGljYXRpb25GaWVsZHMocmVzcG9uc2VbNF0gPyByZXNwb25zZVs0XSA6IFtdKTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0RGVmYXVsdENhdGVnb3J5RGV0YWlscyhyZXNwb25zZVs1XSA/IHJlc3BvbnNlWzVdIDogbnVsbCk7XHJcbiAgICAgICAgICAgICAgICAvLyBjb25zdCBkZWZhdWx0Q2F0ZWdvcnkgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldERlZmF1bHRDYXRlZ29yeURldGFpbHMoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhdGVnb3JpZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbENhdGVnb3JpZXMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0U2VsZWN0ZWRDYXRlZ29yeShjYXRlZ29yaWVzWzBdKTtcclxuICAgICAgICAgICAgICAgIC8vIGNvbnN0IHNlbGVjdGVkQ2F0ZWdvcnkgPSBjYXRlZ29yaWVzLmZpbmQoY2F0ZWdvcnkgPT4gY2F0ZWdvcnlbJ01QTV9DYXRlZ29yeS1pZCddLklkID09PSBkZWZhdWx0Q2F0ZWdvcnkuaWQpO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5zaGFyaW5nU2VydmljZS5zZXRTZWxlY3RlZENhdGVnb3J5KHNlbGVjdGVkQ2F0ZWdvcnkpO1xyXG4gICAgICAgICAgICAgICAgLyogZm9ya0pvaW4oW1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuR2V0QWxsTVBNVmlld0NvbmZpZ0JQTShkZWZhdWx0Q2F0ZWdvcnkuaWQpLFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlckNvbmZpZ1NlcnZpY2Uuc2V0SW5kZXhlckNvbmZpZ0J5Q2F0ZWdvcnlJZChkZWZhdWx0Q2F0ZWdvcnkuaWQpLFxyXG4gICAgICAgICAgICAgICAgXSkuc3Vic2NyaWJlKHZpZXdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcodmlld1Jlc3BvbnNlWzBdID8gdmlld1Jlc3BvbnNlWzBdIDogW10pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldEFsbENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlDYXRlZ29yeUlkKGRlZmF1bHRDYXRlZ29yeS5pZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjYXRlZ29yeUxldmVsUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDYXRlZ29yeUxldmVscyhjYXRlZ29yeUxldmVsUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgICAgICAgICBmb3JrSm9pbihbXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5HZXRBbGxNUE1WaWV3Q29uZmlnQlBNKGNhdGVnb3JpZXNbMF1bJ01QTV9DYXRlZ29yeS1pZCddLklkKSxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXJDb25maWdTZXJ2aWNlLnNldEluZGV4ZXJDb25maWdCeUNhdGVnb3J5SWQoY2F0ZWdvcmllc1swXVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQpLFxyXG4gICAgICAgICAgICAgICAgXSkuc3Vic2NyaWJlKHZpZXdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcodmlld1Jlc3BvbnNlWzBdID8gdmlld1Jlc3BvbnNlWzBdIDogW10pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldEFsbENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlDYXRlZ29yeUlkKGNhdGVnb3JpZXNbMF1bJ01QTV9DYXRlZ29yeS1pZCddLklkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNhdGVnb3J5TGV2ZWxSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldENhdGVnb3J5TGV2ZWxzKGNhdGVnb3J5TGV2ZWxSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhdGVEb3dubG9hZChmaWxlVVJMLCBmaWxlTmFtZSkge1xyXG4gICAgICAgIC8vIGZvciBub24tSUVcclxuICAgICAgICBpZiAoIXdpbmRvd1snQWN0aXZlWE9iamVjdCddKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICAgICAgICB4aHIub3BlbignR0VUJywgZmlsZVVSTCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW4nLCAnKicpO1xyXG4gICAgICAgICAgICB4aHIud2l0aENyZWRlbnRpYWxzID0gdHJ1ZTtcclxuICAgICAgICAgICAgeGhyLnJlc3BvbnNlVHlwZSA9ICdibG9iJztcclxuICAgICAgICAgICAgeGhyLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHVybENyZWF0b3IgPSB3aW5kb3cuVVJMIHx8IHdpbmRvd1snd2Via2l0VVJMJ107XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbWFnZVVybCA9IHVybENyZWF0b3IuY3JlYXRlT2JqZWN0VVJMKHRoaXMucmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFnID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG4gICAgICAgICAgICAgICAgdGFnLmhyZWYgPSBpbWFnZVVybDtcclxuICAgICAgICAgICAgICAgIHRhZy5kb3dubG9hZCA9IGZpbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0YWcpO1xyXG4gICAgICAgICAgICAgICAgdGFnLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRhZyk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh3aW5kb3dbJ0FjdGl2ZVhPYmplY3QnXSAmJiBkb2N1bWVudC5leGVjQ29tbWFuZCkge1xyXG4gICAgICAgICAgICBjb25zdCBuZXdXaW5kb3cgPSB3aW5kb3cub3BlbihmaWxlVVJMLCAnX2JsYW5rJyk7XHJcbiAgICAgICAgICAgIG5ld1dpbmRvdy5kb2N1bWVudC5jbG9zZSgpO1xyXG4gICAgICAgICAgICBuZXdXaW5kb3cuZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ1NhdmVBcycsIHRydWUsIGZpbGVOYW1lIHx8IGZpbGVVUkwpO1xyXG4gICAgICAgICAgICBuZXdXaW5kb3cuY2xvc2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNOdWxsT3JFbXB0eShkYXRhKSB7XHJcbiAgICAgICAgaWYgKChkYXRhWydAeHNpOm5pbCddID09PSAndHJ1ZScpIHx8IChkYXRhWydAbmlsJ10gPT09ICd0cnVlJykgfHwgKGRhdGEgPT09ICcnKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEJvb2xlYW5WYWx1ZShkYXRhKSB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGEgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbGlkKHZhcmlhYmxlVG9DaGVjaykge1xyXG4gICAgICAgIGlmICh2YXJpYWJsZVRvQ2hlY2sgJiYgdmFyaWFibGVUb0NoZWNrICE9IG51bGwgJiYgdmFyaWFibGVUb0NoZWNrICE9PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQXJyYXkodmFyaWFibGVUb0NoZWNrKSB7XHJcbiAgICAgICAgaWYgKHZhcmlhYmxlVG9DaGVjayAmJiB2YXJpYWJsZVRvQ2hlY2sgIT0gbnVsbCAmJiBBcnJheS5pc0FycmF5KHZhcmlhYmxlVG9DaGVjaykpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBjdXJyZW50VXNlckhhc1JvbGUocm9sZU5hbWUpIHtcclxuICAgICAgICBsZXQgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICghcm9sZU5hbWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGhhc1JvbGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBjdXJyZW50VXNlclJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlck9iamVjdCgpLk1hbmFnZXJGb3IuVGFyZ2V0O1xyXG4gICAgICAgIGlmIChjdXJyZW50VXNlclJvbGVzICYmICFBcnJheS5pc0FycmF5KGN1cnJlbnRVc2VyUm9sZXMpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9iaiA9IGN1cnJlbnRVc2VyUm9sZXM7XHJcbiAgICAgICAgICAgIGN1cnJlbnRVc2VyUm9sZXMgPSBbb2JqXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yIChjb25zdCBjdXJyZW50VXNlclJvbGUgb2YgY3VycmVudFVzZXJSb2xlcykge1xyXG4gICAgICAgICAgICBpZiAoY3VycmVudFVzZXJSb2xlLk5hbWUgJiYgKGN1cnJlbnRVc2VyUm9sZS5OYW1lLnRvTG93ZXJDYXNlKCkgPT09IHJvbGVOYW1lLnRvTG93ZXJDYXNlKCkpKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNSb2xlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBoYXNSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRXF1YWxXaXRoQ2FzZShzdHJpbmcxLCBzdHJpbmcyKSB7XHJcbiAgICAgICAgaWYgKHN0cmluZzEgJiYgc3RyaW5nMiAmJiAoc3RyaW5nMSA9PT0gc3RyaW5nMikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuV2luZG93KHVybFRvT3Blbiwgd2luZG93TmFtZSwgZmVhdHVyZXMpIHtcclxuICAgICAgICBsZXQgd2luZG93SGFuZGxlID0gbnVsbDtcclxuICAgICAgICBsZXQgcHJldmlvdXNVUkwgPSBudWxsO1xyXG4gICAgICAgIGlmICh3aW5kb3dIYW5kbGUgPT0gbnVsbCB8fCB3aW5kb3dIYW5kbGUuY2xvc2VkKSB7XHJcbiAgICAgICAgICAgIHdpbmRvd0hhbmRsZSA9IHdpbmRvdy5vcGVuKHVybFRvT3Blbiwgd2luZG93TmFtZSwgZmVhdHVyZXMpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocHJldmlvdXNVUkwgIT09IHVybFRvT3Blbikge1xyXG4gICAgICAgICAgICB3aW5kb3dIYW5kbGUgPSB3aW5kb3cub3Blbih1cmxUb09wZW4sIHdpbmRvd05hbWUsIGZlYXR1cmVzKTtcclxuICAgICAgICAgICAgd2luZG93SGFuZGxlLmZvY3VzKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgd2luZG93SGFuZGxlLmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHByZXZpb3VzVVJMID0gdXJsVG9PcGVuO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFF1ZXJ5UGFyYW1ldGVyc0Zyb21VUkwodXJsKSB7XHJcbiAgICAgICAgbGV0IHF1ZXJ5U3RyaW5nID0gdXJsID8gdXJsLnNwbGl0KCc/JylbMV0gOiB3aW5kb3cubG9jYXRpb24uc2VhcmNoLnNsaWNlKDEpO1xyXG4gICAgICAgIGNvbnN0IG9iaiA9IHt9O1xyXG5cclxuICAgICAgICBpZiAocXVlcnlTdHJpbmcpIHtcclxuICAgICAgICAgICAgcXVlcnlTdHJpbmcgPSBxdWVyeVN0cmluZy5zcGxpdCgnIycpWzBdO1xyXG4gICAgICAgICAgICBjb25zdCBxdWVyaWVzID0gcXVlcnlTdHJpbmcuc3BsaXQoJyYnKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcXVlcnkgb2YgcXVlcmllcykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYSA9IHF1ZXJ5LnNwbGl0KCc9Jyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcGFyYW1OYW1lID0gYVswXTtcclxuICAgICAgICAgICAgICAgIGxldCBwYXJhbVZhbHVlID0gdHlwZW9mIChhWzFdKSA9PT0gJ3VuZGVmaW5lZCcgPyB0cnVlIDogYVsxXTtcclxuXHJcbiAgICAgICAgICAgICAgICBwYXJhbU5hbWUgPSBwYXJhbU5hbWUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcGFyYW1WYWx1ZSA9PT0gJ3N0cmluZycpIHsgcGFyYW1WYWx1ZSA9IHBhcmFtVmFsdWUudG9Mb3dlckNhc2UoKTsgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbU5hbWUubWF0Y2goL1xcWyhcXGQrKT9cXF0kLykpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBrZXkgPSBwYXJhbU5hbWUucmVwbGFjZSgvXFxbKFxcZCspP1xcXS8sICcnKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIW9ialtrZXldKSB7IG9ialtrZXldID0gW107IH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmFtTmFtZS5tYXRjaCgvXFxbXFxkK1xcXSQvKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpbmRleCA9IC9cXFsoXFxkKylcXF0vLmV4ZWMocGFyYW1OYW1lKVsxXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqW2tleV1baW5kZXhdID0gcGFyYW1WYWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmpba2V5XS5wdXNoKHBhcmFtVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFvYmpbcGFyYW1OYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmpbcGFyYW1OYW1lXSA9IHBhcmFtVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChvYmpbcGFyYW1OYW1lXSAmJiB0eXBlb2Ygb2JqW3BhcmFtTmFtZV0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ialtwYXJhbU5hbWVdID0gW29ialtwYXJhbU5hbWVdXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0ucHVzaChwYXJhbVZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmpbcGFyYW1OYW1lXS5wdXNoKHBhcmFtVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb2JqO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFF1ZXJ5UGFyYW1ldGVyRnJvbVVSTChwYXJhbWV0ZXIsIHVybCkge1xyXG4gICAgICAgIGNvbnN0IHJlZyA9IG5ldyBSZWdFeHAoJ1s/Jl0nICsgcGFyYW1ldGVyICsgJz0oW14mI10qKScsICdpJyk7XHJcbiAgICAgICAgY29uc3Qgc3RyID0gcmVnLmV4ZWModXJsKTtcclxuICAgICAgICByZXR1cm4gc3RyID8gc3RyWzFdIDogdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByaW9yaXR5SWNvbihuYW1lOiBzdHJpbmcsIGNhdGVnb3J5TGV2ZWxUeXBlOiBNUE1fTEVWRUwpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50Q2F0ZWdvcnlMZXZlbCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q2F0ZWdvcnlMZXZlbHMoKS5maW5kKChlYWNoQ2F0ZWdvcnlMZXZlbCkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZWFjaENhdGVnb3J5TGV2ZWwuQ0FURUdPUllfTEVWRUxfVFlQRSA9PT0gY2F0ZWdvcnlMZXZlbFR5cGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IHByaW9yaXR5T2JqID0gbnVsbDtcclxuICAgICAgICBjb25zdCBwcmlvcml0eVJldHVybk9iaiA9IHsgaWNvbjogJycsIGNvbG9yOiAnJywgdG9vbHRpcDogJycgfTtcclxuICAgICAgICBpZiAoY3VycmVudENhdGVnb3J5TGV2ZWwgJiYgY3VycmVudENhdGVnb3J5TGV2ZWxbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddKSB7XHJcbiAgICAgICAgICAgIHByaW9yaXR5T2JqID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxQcmlvcml0aWVzKClcclxuICAgICAgICAgICAgICAgIC5maW5kKHByaW9yaXR5ID0+IG5hbWUgPT09IHByaW9yaXR5Lk5BTUUgJiYgcHJpb3JpdHkuUl9QT19DQVRFR09SWV9MRVZFTFsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQgPT09IGN1cnJlbnRDYXRlZ29yeUxldmVsWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXS5JZCk7XHJcbiAgICAgICAgICAgIGlmICghcHJpb3JpdHlPYmopIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwcmlvcml0eVJldHVybk9iajtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgaWNvbjogcHJpb3JpdHlPYmouSUNPTl9ESVJFQ1RJT04gPT09ICdVcCcgPyAnYXJyb3dfdXB3YXJkJyA6ICdhcnJvd19kb3dud2FyZCcsXHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcHJpb3JpdHlPYmouSUNPTl9DT0xPUixcclxuICAgICAgICAgICAgICAgIHRvb2x0aXA6IHByaW9yaXR5T2JqLkRFU0NSSVBUSU9OXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwcmlvcml0eVJldHVybk9iajtcclxuICAgIH1cclxuXHJcbiAgICAvKiBEZWZpbmUgZnVuY3Rpb24gZm9yIGVzY2FwaW5nIHVzZXIgaW5wdXQgdG8gYmUgdHJlYXRlZCBhc1xyXG4gICAgICAgIGEgbGl0ZXJhbCBzdHJpbmcgd2l0aGluIGEgcmVndWxhciBleHByZXNzaW9uICovXHJcbiAgICBlc2NhcGVSZWdFeHAoc3RyKSB7XHJcbiAgICAgICAgcmV0dXJuIHN0ci5yZXBsYWNlKC9bLiorP14ke30oKXxbXFxdXFxcXF0vZywgJ1xcXFwkJicpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIERlZmluZSBmdW5jdGluIHRvIGZpbmQgYW5kIHJlcGxhY2Ugc3BlY2lmaWVkIHRlcm0gd2l0aCByZXBsYWNlbWVudCBzdHJpbmcgKi9cclxuICAgIHJlcGxhY2VBbGwoc3RyLCB0ZXJtLCByZXBsYWNlbWVudCkge1xyXG4gICAgICAgIHJldHVybiBzdHIucmVwbGFjZShuZXcgUmVnRXhwKHRoaXMuZXNjYXBlUmVnRXhwKHRlcm0pLCAnZycpLCByZXBsYWNlbWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChsb29rdXBEb21haW5JZDogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgbG9va3VwRG9tYWlucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsTG9va3VwRG9tYWlucygpO1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGxvb2t1cERvbWFpbnMpICYmIGxvb2t1cERvbWFpbnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgdG1wTG9va3VwRG9tYWlucyA9IGxvb2t1cERvbWFpbnMuZmluZChsb29rdXBEb21haW4gPT4gbG9va3VwRG9tYWluLmRvbWFpbklkID09PSBsb29rdXBEb21haW5JZCk7XHJcbiAgICAgICAgaWYgKHRtcExvb2t1cERvbWFpbnMgJiYgIUFycmF5LmlzQXJyYXkodG1wTG9va3VwRG9tYWlucykpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRtcExvb2t1cERvbWFpbnMuZG9tYWluVmFsdWVzO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcblxyXG4gICAgR2V0Um9sZUROQnlSb2xlSWQocm9sZUlkKSB7XHJcbiAgICAgICAgY29uc3QgYWxsUm9sZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFJvbGVzKCk7XHJcbiAgICAgICAgcmV0dXJuIGFsbFJvbGVzLmZpbmQocm9sZSA9PiByb2xlWydNUE1fQVBQX1JvbGVzLWlkJ10uSWQgPT09IHJvbGVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGVhbVJvbGVETkJ5Um9sZUlkKHJvbGVJZCkge1xyXG4gICAgICAgIGNvbnN0IGFsbFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKTtcclxuICAgICAgICByZXR1cm4gYWxsUm9sZXMuZmluZChyb2xlID0+IHBhcnNlSW50KHJvbGVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkKSA9PT0gcm9sZUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBjb252ZXJ0VG9PVE1NRGF0ZUZvcm1hdChkYXRlKSB7XHJcbiAgICAgICAgZGF0ZSA9IG5ldyBEYXRlKGRhdGUpO1xyXG4gICAgICAgIHJldHVybiBtb21lbnQoZGF0ZSkuZm9ybWF0KCdNTS9ERC9ZWVlZVDAwOjAwOjAwJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlU3BjbENoYXJzRnJvbUFzc2V0TmFtZShhc3NldE5hbWUpIHtcclxuICAgICAgICBhc3NldE5hbWUgPSBhc3NldE5hbWUudHJpbSgpO1xyXG4gICAgICAgIGlmIChhc3NldE5hbWUuaW5jbHVkZXMoJyQnKSB8fCBhc3NldE5hbWUuaW5jbHVkZXMoJ1xcJycpKSB7XHJcbiAgICAgICAgICAgIGFzc2V0TmFtZSA9IGFzc2V0TmFtZS5yZXBsYWNlKC9bJCddL2csICdfJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYXNzZXROYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlcnRTdHJpbmdUb051bWJlcihkYXRhKSB7XHJcbiAgICAgICAgdmFyIHBhZ2VTaXplID0gZGF0YTtcclxuICAgICAgICB2YXIgcGFnZU51bWJlcjogbnVtYmVyID0gK3BhZ2VTaXplO1xyXG4gICAgICAgIHJldHVybiBwYWdlTnVtYmVyO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERpc3BsYXlPcmRlckRhdGEoZGF0YSwgZGlzcGxheU9yZGVyKSB7XHJcbiAgICAgICAgY29uc3QgZmllbGRPYmpBcnJheSA9IFtdO1xyXG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQoZGlzcGxheU9yZGVyKSAmJiAhZGlzcGxheU9yZGVyWydAbnVsbCddKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFyckRpc3BsYXlPcmRlciA9IGRpc3BsYXlPcmRlci5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICBhcnJEaXNwbGF5T3JkZXIuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgIGZpZWxkT2JqQXJyYXkucHVzaChkYXRhLmZpbmQoZmllbGREYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmllbGREYXRhLk1BUFBFUl9OQU1FID09PSBlbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAoZmllbGRPYmpBcnJheS5sZW5ndGggPiAwICYmIGRhdGEubGVuZ3RoID09PSBmaWVsZE9iakFycmF5Lmxlbmd0aCkgPyBmaWVsZE9iakFycmF5IDogZGF0YTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19