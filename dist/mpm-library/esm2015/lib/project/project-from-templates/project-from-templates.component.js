import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { ProjectConstant } from '../project-overview/project.constants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ProjectFromTemplateService } from '../shared/services/project-from-template.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SaveOptionConstant } from '../../shared/constants/save-options.constants';
import { DataTypeConstants } from '../shared/constants/data-type.constants';
import { LoaderService } from '../../loader/loader.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { Router } from '@angular/router';
import { ProjectService } from '../shared/services/project.service';
import { DatePipe } from '@angular/common';
let ProjectFromTemplatesComponent = class ProjectFromTemplatesComponent {
    constructor(fb, sharingService, loaderService, projectFromTemplateService, dialog, fieldConfigService, utilService, appService, router, projectService, datePipe) {
        this.fb = fb;
        this.sharingService = sharingService;
        this.loaderService = loaderService;
        this.projectFromTemplateService = projectFromTemplateService;
        this.dialog = dialog;
        this.fieldConfigService = fieldConfigService;
        this.utilService = utilService;
        this.appService = appService;
        this.router = router;
        this.projectService = projectService;
        this.datePipe = datePipe;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.customMetadataObj = {
            customMetadataFields: [],
            customFieldsGroup: [],
            fieldsetGroup: {},
            fieldGroups: []
        };
        this.mpmFieldConstants = MPMFieldConstants;
        this.minStartDate = new Date();
        this.saveOptions = SaveOptionConstant.templateSaveOptions;
        this.disableTemplateSave = false;
        this.projectOwner = {
            name: this.sharingService.getdisplayName(),
            value: this.sharingService.getcurrentUserItemID(),
            displayName: this.sharingService.getdisplayName() + ' (' + this.sharingService.getCurrentUserCN() + ')',
            cn: this.sharingService.getCurrentUserCN()
        };
        this.projectDependency = {
            copyTaskChecked: false,
            copyTaskDisabled: true,
            copyDeliverableChecked: false,
            copyDeliverableDisabled: true,
            copyWorfklowRulesChecked: false,
            copyWorfklowRulesDisabled: true,
            copyCustomMetaDataChecked: false,
            copyCustomMetaDataDisabled: true,
            selectedObject: {},
            isNoTasks: false,
            isNoDeliverables: false,
            isTaskWithTaskWithDeliverableDependency: false
        };
        this.isCreateAnother = false;
        this.SELECTED_SAVE_OPTION = {
            value: 'CREATE',
            name: 'Create'
        };
        this.deliverableViewName = null;
        this.CAMPAIGN_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';
        this.GET_ALL_CAMPAIGN_WS_METHOD_NAME = 'GetAllCampaign';
        this.GET_ALL_cAMPAIGN_BY_NAME_WS_METHOD_NAME = 'GetAllCampaignByName';
        this.GET_CAMPAIGN_WS_METHOD_NAME = 'GetCampaignById';
        this.isCampaignProject = false;
        this.dateFilter = (date) => {
            if (this.enableWorkWeek) {
                return true;
            }
            else {
                const day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
    }
    initializeProjectTemplateForm() {
        if (this.customMetadataObj && this.customMetadataObj.customFieldsGroup && this.customMetadataObj.customFieldsGroup['controls']) {
            this.customMetadataObj.customFieldsGroup['controls'] = null;
        }
        this.templateform = null;
        let description = '';
        if (this.selectedProject) {
            description = this.fieldConfigService.getFeildValueByMapperName(this.selectedProject, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DESCRIPTION);
            description = (!description || description === 'NA') ? '' : description;
        }
        if (this.isCopyTemplate) {
            this.templateform = this.fb.group({
                projectName: new FormControl('', [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                projectOwner: new FormControl({ value: this.isCampaignProject ? '' : this.projectOwner.displayName, disabled: 'true' }),
                campaign: new FormControl(''),
                campaignOwner: new FormControl({ value: '', disabled: 'true' }),
                description: new FormControl(description, [Validators.maxLength(1000)]),
                projectTeam: new FormControl({ value: '', disabled: 'true' }, [Validators.required]),
                copyTask: new FormControl({ value: this.projectDependency.copyTaskChecked, disabled: this.projectDependency.copyTaskDisabled }),
                copyDeliverable: new FormControl({
                    value: this.projectDependency.copyDeliverableChecked, disabled: this.projectDependency.copyDeliverableDisabled
                }),
                copyWorflowRules: new FormControl({
                    value: this.projectDependency.copyWorfklowRulesChecked,
                    disabled: this.projectDependency.copyWorfklowRulesDisabled
                }),
                categoryMetadata: new FormControl({ value: '', disabled: true }),
            });
        }
        else {
            this.templateform = this.fb.group({
                projectName: new FormControl('', [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                projectOwner: new FormControl({ value: this.isCampaignProject ? '' : this.projectOwner.displayName, disabled: 'true' }),
                campaign: new FormControl(''),
                campaignOwner: new FormControl({ value: '', disabled: 'true' }),
                description: new FormControl(description, [Validators.maxLength(1000)]),
                expectedDuration: new FormControl('', [Validators.pattern('^[0-9]*$')]),
                projectTeam: new FormControl({ value: '', disabled: 'true' }, [Validators.required]),
                projectStartDate: new FormControl('', [Validators.required]),
                projectEndDate: new FormControl('', [Validators.required]),
                taskStartDate: new FormControl('', [Validators.required]),
                // 205
                //taskEndDate: new FormControl({ value: '', disabled: this.enableDuration }, [Validators.required]),
                taskEndDate: new FormControl({ value: '', disabled: 'true' }, [Validators.required]),
                copyTask: new FormControl({ value: this.projectDependency.copyTaskChecked, disabled: this.projectDependency.copyTaskDisabled }),
                copyDeliverable: new FormControl({
                    value: this.projectDependency.copyDeliverableChecked, disabled: this.projectDependency.copyDeliverableDisabled
                }),
                copyWorflowRules: new FormControl({
                    value: this.projectDependency.copyWorfklowRulesChecked,
                    disabled: this.projectDependency.copyWorfklowRulesDisabled
                }),
                categoryMetadata: new FormControl({ value: '', disabled: true }),
            });
        }
        //this.templateform.controls.projectTeam.disable();
        this.templateform.updateValueAndValidity();
        this.setCategoryMetadataAutoComplete();
        if (this.campaignId) {
            this.templateform.controls.campaign.setValue(this.formCampaign(this.campaignId));
            this.templateform.controls.campaign.disable();
        }
        if (this.createProjectFromTemplate) {
            this.templateform.addControl('template', new FormControl('', [Validators.required]));
            this.setTemplateAutoComplete();
            this.mapCustomMetadata('', null);
        }
        else {
            if (this.selectedProject) {
                const categoryMetadataId = this.fieldConfigService.getFeildValueByMapperName(this.selectedProject, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_CATEGORY_METADATA_ID);
                if (categoryMetadataId === '0') {
                    this.templateform.controls.categoryMetadata.enable();
                    this.templateform.controls.categoryMetadata.setValue('');
                }
                else {
                    this.templateform.controls.categoryMetadata.disable();
                    const selectedCategoryMetadata = this.formCategoryMetadata(categoryMetadataId);
                    this.templateform.controls.categoryMetadata.setValue(selectedCategoryMetadata);
                }
                /** custom workflow-copyworkflowrules implement */
                if (this.utilService.getBooleanValue(this.selectedProject.IS_CUSTOM_WORKFLOW)) {
                    this.templateform.controls.copyWorflowRules.enable();
                    this.templateform.controls.copyWorflowRules.setValue(true);
                }
                this.setTeam(this.selectedProject);
                if (this.isCampaignProject) {
                    this.setOwner(this.selectedProject);
                }
                const fromProjectName = this.fieldConfigService.getFeildValueByMapperName(this.selectedProject, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                if (fromProjectName) {
                    this.templateform.controls.projectName.setValue(fromProjectName);
                }
                this.mapCustomMetadata(this.selectedProject, null);
                this.projectFromTemplateService.validateProjectTaskandDeliverable(this.selectedProject, this.templateform, this.projectDependency, this.deliverableViewName);
            }
        }
        this.setOwnerAutoComplete();
        this.templateform.controls.campaign.valueChanges.subscribe(campaign => {
            if (campaign) {
                this.selectedCampaign = this.campaignList.find(data => data['Campaign-id'].Id === campaign.value);
                if (this.selectedCampaign) {
                    this.loadingHandler.next(true);
                    this.minStartDate = this.datePipe.transform(this.selectedCampaign.CAMPAIGN_START_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd') ? this.selectedCampaign.CAMPAIGN_START_DATE : new Date();
                    this.getCampaignOwnerDetailsId(this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(response => {
                        this.loadingHandler.next(false);
                        this.templateform.controls.campaignOwner.setValue(this.formUserDetails({
                            UserId: this.campaignOwnerDetails.userCN,
                            FullName: this.campaignOwnerDetails.fullName
                        }, this.campaignOwnerDetails.userCN));
                    });
                }
                else {
                    this.templateform.controls.campaignOwner.setValue('');
                }
            }
            else {
                this.selectedCampaign = null;
                this.templateform.controls.campaignOwner.setValue('');
            }
        });
        this.templateform.controls.projectStartDate.valueChanges.subscribe(() => {
            if (this.selectedCampaign) {
                if ((new Date(this.templateform.controls.projectStartDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                    new Date(this.templateform.controls.projectStartDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) &&
                    this.datePipe.transform(new Date(this.templateform.controls.projectStartDate.value), 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                    this.startDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                }
                else {
                    this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                }
            }
            else {
                this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
            }
        });
        this.templateform.controls.projectEndDate.valueChanges.subscribe(() => {
            // 205
            //    if (this.enableDuration) {
            this.templateform.controls.taskEndDate.setValue(this.templateform.controls.projectEndDate.value);
            this.templateform.controls.taskEndDate.updateValueAndValidity();
            //   }
            if (this.selectedCampaign) {
                if (new Date(this.templateform.controls.projectEndDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                    new Date(this.templateform.controls.projectEndDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) {
                    this.endDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                }
                else {
                    this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                }
            }
            else {
                this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
            }
        });
        this.templateform.controls.taskStartDate.valueChanges.subscribe(() => {
            if ((this.templateform.controls.projectStartDate.value || this.templateform.controls.projectEndDate.value) &&
                (new Date(this.templateform.controls.taskStartDate.value) < new Date(this.templateform.controls.projectStartDate.value) ||
                    new Date(this.templateform.controls.taskStartDate.value) > new Date(this.templateform.controls.projectEndDate.value))) {
                this.taskStartDateErrorMessage = ProjectConstant.TASK_DATE_ERROR_MESSAGE;
            }
        });
        this.templateform.controls.taskEndDate.valueChanges.subscribe(() => {
            if (this.templateform.controls.taskStartDate.value &&
                new Date(this.templateform.controls.taskEndDate.value) < new Date(this.templateform.controls.taskStartDate.value)) {
                this.taskEndDateErrorMessage = ProjectConstant.TASK_START_DATE_ERROR_MESSAGE;
            }
            else {
                this.taskEndDateErrorMessage = ProjectConstant.TASK_DATE_ERROR_MESSAGE;
            }
        });
        if (this.templateform.controls.categoryMetadata) {
            this.templateform.controls.categoryMetadata.valueChanges.subscribe(data => {
                if (data) {
                    console.log(data);
                    this.mapCustomMetadata(this.selectedProject, data.value);
                }
            });
        }
    }
    formCampaignValue(campaign) {
        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id) {
            return campaign['Campaign-id'].Id + '-' + campaign.CAMPAIGN_NAME;
        }
    }
    formCampaign(campaignId) {
        if (campaignId) {
            const selectedCampaign = this.campaignDataFilterList.find(element => element.value === campaignId);
            return selectedCampaign ? selectedCampaign : '';
            /* return selectedCampaign && Array.isArray(selectedCampaign) ? selectedCampaign : selectedCampaign ? [selectedCampaign] : ''; */
        }
        else {
            return '';
        }
    }
    formUserDetails(userObj, userId) {
        if (userObj && userObj.UserDisplayName && userId && userId !== '') {
            return {
                name: userObj.UserDisplayName,
                value: userId,
                displayName: userObj.UserDisplayName + ' (' + userId + ')',
                cn: userId
            };
        }
        else if (userObj && userObj.UserId && userObj.UserId !== '') {
            return {
                name: userObj.Description || userObj.FullName,
                value: userObj.UserId,
                displayName: (userObj.Description || userObj.FullName) + ' (' + userObj.UserId + ')',
                cn: userObj.userId
            };
        }
        else {
            return null;
        }
    }
    getCampaign() {
        return new Observable(observer => {
            this.loaderService.show();
            this.appService.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_ALL_CAMPAIGN_WS_METHOD_NAME, null)
                // this.appService.getAllCampaign()
                .subscribe(response => {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    this.campaignList = response.Campaign;
                    if (this.campaignId) {
                        this.selectedCampaign = this.campaignList.find(data => data['Campaign-id'].Id === this.campaignId);
                    }
                    const campaignFilterList = [];
                    this.campaignList.map(campaign => {
                        /* if (this.taskModalConfig.taskId && task['Task-id'].Id === this.taskModalConfig.taskId) {
                            this.taskModalConfig.selectedTask = task;
                        } */
                        const value = this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    this.campaignDataFilterList = campaignFilterList;
                }
                else {
                    this.campaignList = [];
                    this.campaignDataFilterList = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            });
        });
    }
    getCampaignOwnerDetailsId(userId) {
        return new Observable(observer => {
            if (!userId) {
                observer.error();
            }
            const parameters = {
                userId: userId
            };
            this.appService.getPersonByIdentityUserId(parameters).subscribe(personDetails => {
                this.campaignOwnerDetails = personDetails;
                observer.next();
                observer.complete();
            });
        });
    }
    mapCustomMetadata(template, modelId) {
        this.loaderService.show();
        const metadataModelId = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_CATEGORY_METADATA_ID);
        const categoryLevelDetails = modelId === null ? this.formCategoryMetadata(metadataModelId) : this.formCategoryMetadata(modelId);
        this.projectFromTemplateService.getOTMMCustomMetaData(this.customMetadataObj, this.fb, template, categoryLevelDetails)
            .subscribe(customMetadataObj => {
            this.loaderService.hide();
            if (customMetadataObj) {
                if (customMetadataObj.customMetadataFields && Array.isArray(customMetadataObj.customMetadataFields)
                    && customMetadataObj.customMetadataFields.length > 0) {
                    customMetadataObj.fieldGroups.forEach(fieldGroup => {
                        fieldGroup.controls.fieldset.controls.forEach(formControl => {
                            if (formControl.fieldset.data_type === DataTypeConstants.DATE && formControl.value) {
                                formControl.value = new Date(formControl.value);
                            }
                        });
                    });
                    customMetadataObj.customFieldsGroup = this.fb.group({ fieldset: new FormArray(customMetadataObj.fieldsetGroup) });
                    if (this.templateform.get('CustomFieldGroup')) {
                        this.templateform.removeControl('CustomFieldGroup');
                    }
                    this.templateform.addControl('CustomFieldGroup', new FormArray(customMetadataObj.fieldGroups, { updateOn: 'blur' }));
                }
            }
        }, error => {
            this.loaderService.hide();
        });
    }
    setOwnerAutoComplete() {
        this.campaignOwnerAutocompleteField = {
            formControl: this.templateform.controls.campaignOwner,
            label: 'Campaign Owner',
            filterOptions: []
        };
        this.campaignAutocompleteField = {
            formControl: this.templateform.controls.campaign,
            label: 'Campaign Name',
            filterOptions: this.campaignDataFilterList
        };
        if (!this.projectOwnerList || this.projectOwnerList.length === 0) {
            return;
        }
        // this.templateform.controls.projectOwner.enable();
        this.projectOwnerList.map(user => {
            if (user.cn === this.projectOwner.cn) {
                user.displayName = this.projectOwner.displayName;
                this.templateform.controls.projectOwner.patchValue(user); // .displayName + ' (' + user.cn + ')'
            }
        });
        this.ownerAutocompleteField = {
            formControl: this.templateform.controls.projectOwner,
            label: 'Project Owner',
            filterOptions: this.projectOwnerList
        };
    }
    setTemplateAutoComplete() {
        this.templateAutocompleteField = {
            formControl: this.templateform.controls.template,
            label: 'Template',
            filterOptions: this.allTemplateList
        };
    }
    setCategoryMetadataAutoComplete() {
        this.categoryMetadataField = {
            formControl: this.templateform.controls.categoryMetadata,
            label: 'Category Metadata',
            filterOptions: this.allcategoryMetadataList
        };
    }
    onCustomMetadataFieldChange(event) {
        this.templateform.get('CustomFieldGroup')['controls'].forEach(formGroup => {
            formGroup['controls']['fieldset'].updateValueAndValidity();
        });
    }
    createProject(saveOption) {
        if (this.templateform.status === 'VALID' || true) {
            if (this.templateform.controls.projectName.value.trim() === '') {
                const eventData = { message: 'Please provide a valid project name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                this.notificationHandler.next(eventData);
            }
            else {
                this.templateform.controls.projectName.setValue(this.templateform.controls.projectName.value.trim());
                const customMetadata = this.projectFromTemplateService.getCustomMetadata(this.customMetadataObj.customFieldsGroup);
                let sourceId = '';
                if (this.createProjectFromTemplate) {
                    sourceId = this.fieldConfigService.getFeildValueByMapperName(this.templateform.value.template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
                }
                else {
                    sourceId = this.fieldConfigService.getFeildValueByMapperName(this.selectedProject, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
                }
                this.loadingHandler.next(true);
                if (this.isCopyTemplate) {
                    this.projectFromTemplateService.copyTemplate(sourceId, this.templateform, this.templateSearchConditions.templateSearchCondition, customMetadata, this.projectOwner, this.viewConfig, this.enableDuplicateNames)
                        .subscribe(createProjectResponse => {
                        this.notificationHandler.next(createProjectResponse);
                        this.loadingHandler.next(false);
                        if (createProjectResponse['type'] === ProjectConstant.NOTIFICATION_LABELS.SUCCESS) {
                            if (saveOption && saveOption.value === 'CREATE') {
                                this.closeCallbackHandler.next(true);
                            }
                            else {
                                this.isCreateAnother = true;
                            }
                        }
                    }, error => {
                        this.loadingHandler.next(false);
                    });
                }
                else {
                    this.projectFromTemplateService.createProject(sourceId, this.templateform, this.templateSearchConditions.projectSearchCondition, customMetadata, this.projectOwner, this.viewConfig, this.enableDuplicateNames)
                        .subscribe(createProjectResponse => {
                        this.notificationHandler.next(createProjectResponse);
                        this.loadingHandler.next(false);
                        if (createProjectResponse['type'] === ProjectConstant.NOTIFICATION_LABELS.SUCCESS) {
                            if (saveOption && saveOption.value === 'CREATE') {
                                this.closeCallbackHandler.next(true);
                            }
                            else {
                                this.isCreateAnother = true;
                            }
                        }
                    }, error => {
                        this.loadingHandler.next(false);
                    });
                }
            }
        }
    }
    setOwner(template) {
        const ownerCN = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_CN);
        const ownerId = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_ID);
        const ownerName = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_NAME);
        this.projectOwner = {
            name: ownerName,
            value: ownerId,
            displayName: ownerName + ' (' + ownerCN + ')',
            cn: ownerCN
        };
    }
    setTeam(template) {
        const teamsValue = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM);
        this.templateform.controls.projectTeam.setValue(teamsValue);
        this.templateform.controls.projectTeam.updateValueAndValidity();
    }
    setProjectName(template) {
        const projectName = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
        this.templateform.controls.projectName.setValue(projectName);
        const description = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DESCRIPTION);
        this.templateform.controls.description.setValue((!description || description === 'NA') ? '' : description);
    }
    setTemplate(template) {
        this.projectDependency.selectedObject = template;
        this.setTeam(template);
        if (this.isCampaignProject) {
            this.setOwner(template);
        }
        if (template) {
            this.setProjectName(template);
        }
        const teamName = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM);
        const categoryMetadataId = this.fieldConfigService.getFeildValueByMapperName(template, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_CATEGORY_METADATA_ID);
        if (categoryMetadataId === '0') {
            this.templateform.controls.categoryMetadata.enable();
            this.templateform.controls.categoryMetadata.setValue('');
            this.mapCustomMetadata(template, null);
        }
        else {
            this.templateform.controls.categoryMetadata.disable();
            const selectedCategoryMetadata = this.formCategoryMetadata(categoryMetadataId);
            this.templateform.controls.categoryMetadata.setValue(selectedCategoryMetadata);
            this.mapCustomMetadata(template, selectedCategoryMetadata.value);
        }
        this.loadingHandler.next(true);
        this.projectFromTemplateService.getOwnerListByTeam(teamName, this.isCampaignProject)
            .subscribe(response => {
            this.projectOwnerList = response;
            this.setOwnerAutoComplete();
            this.loadingHandler.next(false);
        });
        this.projectFromTemplateService.validateProjectTaskandDeliverable(this.projectDependency.selectedObject, this.templateform, this.projectDependency, this.deliverableViewName);
    }
    checkTaskAccess() {
        this.note = this.projectFromTemplateService.checkTaskAccess(this.projectDependency, this.templateform);
    }
    checkDeliverableAccess() {
        this.note = this.projectFromTemplateService.checkTaskAccess(this.projectDependency, this.templateform);
    }
    formCategoryMetadata(metadataId) {
        let selectedCategoryMetadata;
        if (this.categoryMetadataField && this.categoryMetadataField.filterOptions) {
            selectedCategoryMetadata = this.categoryMetadataField.filterOptions.find(categoryMetadata => categoryMetadata.value === metadataId);
        }
        return selectedCategoryMetadata ? selectedCategoryMetadata : null;
    }
    closeDialog() {
        if (this.templateform.pristine) {
            this.closeCallbackHandler.next(this.isCreateAnother);
        }
        else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    this.closeCallbackHandler.next(this.isCreateAnother);
                }
            });
        }
    }
    //201
    getAllCampaignByName(campaignName) {
        return new Observable(observer => {
            const getRequestObject = {
                CampaignName: campaignName
            };
            this.appService.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_ALL_cAMPAIGN_BY_NAME_WS_METHOD_NAME, getRequestObject)
                // this.appService.getAllCampaign()
                .subscribe(response => {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    this.campaignList = response.Campaign;
                    this.templateform.controls.campaign.valueChanges.subscribe(campaign => {
                        if (campaign) {
                            this.selectedCampaign = this.campaignList.find(data => data['Campaign-id'].Id === campaign.split(/-(.+)/)[0]);
                            if (this.selectedCampaign) {
                                this.loadingHandler.next(true);
                                this.minStartDate = this.datePipe.transform(this.selectedCampaign.CAMPAIGN_START_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd') ? this.selectedCampaign.CAMPAIGN_START_DATE : new Date();
                                this.getCampaignOwnerDetailsId(this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(response => {
                                    this.loadingHandler.next(false);
                                    this.templateform.controls.campaignOwner.setValue(this.formUserDetails({
                                        UserId: this.campaignOwnerDetails.userCN,
                                        FullName: this.campaignOwnerDetails.fullName
                                    }, this.campaignOwnerDetails.userCN));
                                });
                            }
                            else {
                                this.templateform.controls.campaignOwner.setValue('');
                            }
                        }
                        else {
                            this.selectedCampaign = null;
                            this.templateform.controls.campaignOwner.setValue('');
                        }
                    });
                    this.templateform.controls.projectStartDate.valueChanges.subscribe(() => {
                        if (this.selectedCampaign) {
                            if ((new Date(this.templateform.controls.projectStartDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                new Date(this.templateform.controls.projectStartDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) &&
                                this.datePipe.transform(new Date(this.templateform.controls.projectStartDate.value), 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                                this.startDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                            }
                            else {
                                this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                            }
                        }
                        else {
                            this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                        }
                    });
                    this.templateform.controls.projectEndDate.valueChanges.subscribe(() => {
                        if (this.enableDuration) {
                            this.templateform.controls.taskEndDate.setValue(this.templateform.controls.projectEndDate.value);
                            this.templateform.controls.taskEndDate.updateValueAndValidity();
                        }
                        if (this.selectedCampaign) {
                            if (new Date(this.templateform.controls.projectEndDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                new Date(this.templateform.controls.projectEndDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) {
                                this.endDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                            }
                            else {
                                this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                            }
                        }
                        else {
                            this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                        }
                    });
                    /*
                                        if (this.projectOverviewForm.controls.campaign) {
                                            this.projectOverviewForm.controls.campaign.valueChanges.subscribe(campaign => {
                                                if (campaign) {
                                                    this.selectedCampaign = this.campaignList.find(data => data['Campaign-id'].Id === campaign.split(/-(.+)/)[0]);
                                                    if (this.selectedCampaign) {
                                                        this.loadingHandler.next(true);
                                                        this.minStartDate = this.datePipe.transform(this.selectedCampaign.CAMPAIGN_START_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd') ? this.selectedCampaign.CAMPAIGN_START_DATE : new Date();
                                                        this.getCampaignOwnerDetailsId(this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(response => {
                                                            this.loadingHandler.next(false);
                                                            this.projectOverviewForm.controls.campaignOwner.setValue(this.formUserDetails({
                                                                UserId: this.campaignOwnerDetails.userCN,
                                                                FullName: this.campaignOwnerDetails.fullName
                                                            }, this.campaignOwnerDetails.userCN));
                                                        });
                                                    } else {
                                                        this.projectOverviewForm.controls.campaignOwner.setValue('');
                                                    }
                                                } else {
                                                    this.selectedCampaign = null;
                                                    this.projectOverviewForm.controls.campaignOwner.setValue('');
                                                }
                                            });
                                        }
                                        if (this.projectOverviewForm.controls.startDate) {
                                            this.projectOverviewForm.controls.startDate.valueChanges.subscribe(() => {
                                                if (this.selectedCampaign) {
                                                    if ((new Date(this.projectOverviewForm.controls.startDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                                        new Date(this.projectOverviewForm.controls.startDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) &&
                                                        this.datePipe.transform(new Date(this.projectOverviewForm.controls.startDate.value), 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                                                        this.startDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                                                    } else {
                                                        this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                                                    }
                                                } else {
                                                    this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                                                }
                                            });
                                        }
                                        if (this.projectOverviewForm.controls.endDate) {
                                            this.projectOverviewForm.controls.endDate.valueChanges.subscribe(() => {
                                                if (this.selectedCampaign) {
                                                    if (new Date(this.projectOverviewForm.controls.endDate.value) < new Date(this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                                        new Date(this.projectOverviewForm.controls.endDate.value) > new Date(this.selectedCampaign.CAMPAIGN_END_DATE)) {
                                                        this.endDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                                                    } else {
                                                        this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                                                    }
                                                } else {
                                                    if (this.isCampaign) {
                                                        this.endDateErrorMessage = ProjectConstant.CAMPAIGN_START_DATE_ERROR_MESSAGE;
                                                    } else {
                                                        this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                                                    }
                                                }
                                            });
                                        } */
                    const campaignFilterList = [];
                    this.campaignList.map(campaign => {
                        const value = this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value,
                                ownerId: campaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id
                            });
                        }
                    });
                    this.filterOptions = campaignFilterList;
                }
                else {
                    this.campaignList = [];
                    this.filterOptions = [];
                }
                this.loadingHandler.next(false);
                observer.next(this.filterOptions);
                observer.complete();
            });
        });
    }
    getCampaigns(campaignName) {
        if (campaignName.target.value.length > 2) {
            this.getAllCampaignByName(campaignName.target.value).subscribe(campaignResponse => {
                this.campaignAutocompleteField = campaignResponse;
            });
        }
    }
    changeAutoComplete(event) {
        console.log("selected");
        /* this.campaignId = event.option.value.split(/-(.+)/)[0];
        const selectedCampaign = this.filterOptions.find(element => element.name === event.option.value.split(/-(.+)/)[1]);
        this.getCampaignOwnerDetailsId(selectedCampaign.ownerId).subscribe(response => {
            this.projectOverviewForm.controls.campaignOwner.setValue(this.formUserDetails({
                UserId: this.campaignOwnerDetails.userCN,
                FullName: this.campaignOwnerDetails.fullName
            }, this.campaignOwnerDetails.userCN));
    
            this.overViewConfig.campaignOwnerFieldConfig.formControl = this.projectOverviewForm.controls.campaignOwner;
        }); */
    }
    // 201
    getCampaignById(campaignId) {
        return new Observable(observer => {
            const getRequestObject = {
                CampaignId: campaignId
            };
            this.appService.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_CAMPAIGN_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    this.campaignList = response.Campaign;
                    const campaignFilterList = [];
                    this.campaignList.map(campaign => {
                        const value = this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    this.campaignDataFilterList = campaignFilterList;
                }
                else {
                    this.campaignList = [];
                    this.campaignDataFilterList = [];
                }
                observer.next(this.campaignDataFilterList);
                observer.complete();
            });
        });
    }
    ngOnInit() {
        if (this.projectType) {
            this.createProjectFromTemplate = false;
        }
        else {
            this.createProjectFromTemplate = true;
        }
        this.appConfig = this.sharingService.getAppConfig();
        this.isCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.projectConfig = this.sharingService.getProjectConfig();
        this.enableWorkWeek = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        this.enableDuration = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_DURATION]);
        this.enableDuplicateNames = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_DUPLICATE_NAMES]);
        if (this.router.url.includes('/apps/mpm/campaign/')) {
            this.isCampaignProject = true;
        }
        this.projectService.getAllCategoryMetadata()
            .subscribe(categoryDetails => {
            this.allcategoryMetadataList = categoryDetails.map(data => {
                return Object.assign({ displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME }, data);
            });
        });
        if (this.campaignId) {
            this.getCampaignById(this.campaignId).subscribe(response => {
                console.log(response);
            });
        }
        // 201
        //this.getCampaign().subscribe(res => {
        if (this.createProjectFromTemplate) {
            this.loaderService.show();
            this.projectFromTemplateService.getAllTemplates(this.templateSearchConditions.templateSearchCondition, this.viewConfig)
                .subscribe(templateObj => {
                this.loaderService.hide();
                if (templateObj.allTemplateCount > 0) {
                    this.allTemplateList = templateObj.allTemplateList;
                    const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                    this.allTemplateList.map(data => {
                        data.displayName = data[indexerField];
                        data.name = data[indexerField];
                    });
                    this.initializeProjectTemplateForm();
                }
                else {
                    const eventData = {
                        message: 'There are no templates',
                        type: ProjectConstant.NOTIFICATION_LABELS.INFO
                    };
                    this.notificationHandler.next(eventData);
                    this.closeCallbackHandler.next(true);
                }
            }, error => {
                this.loaderService.hide();
            });
        }
        else {
            const teamsValue = this.fieldConfigService.getFeildValueByMapperName(this.selectedProject, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TEAM);
            this.projectDependency.selectedObject = this.selectedProject;
            if (teamsValue) {
                this.loaderService.show();
                this.projectFromTemplateService.getOwnerListByTeam(teamsValue, this.isCampaignProject).subscribe(response => {
                    this.loaderService.hide();
                    this.projectOwnerList = response;
                    this.initializeProjectTemplateForm();
                }, error => {
                    this.loaderService.hide();
                    this.initializeProjectTemplateForm();
                });
            }
            else {
                this.initializeProjectTemplateForm();
            }
        }
        // });
    }
};
ProjectFromTemplatesComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: SharingService },
    { type: LoaderService },
    { type: ProjectFromTemplateService },
    { type: MatDialog },
    { type: FieldConfigService },
    { type: UtilService },
    { type: AppService },
    { type: Router },
    { type: ProjectService },
    { type: DatePipe }
];
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "templateSearchConditions", void 0);
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "projectType", void 0);
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "viewConfig", void 0);
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "selectedProject", void 0);
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "campaignId", void 0);
__decorate([
    Input()
], ProjectFromTemplatesComponent.prototype, "isCopyTemplate", void 0);
__decorate([
    Output()
], ProjectFromTemplatesComponent.prototype, "closeCallbackHandler", void 0);
__decorate([
    Output()
], ProjectFromTemplatesComponent.prototype, "saveCallBackHandler", void 0);
__decorate([
    Output()
], ProjectFromTemplatesComponent.prototype, "notificationHandler", void 0);
__decorate([
    Output()
], ProjectFromTemplatesComponent.prototype, "loadingHandler", void 0);
ProjectFromTemplatesComponent = __decorate([
    Component({
        selector: 'mpm-project-from-templates',
        template: "<div class=\"flex-row\">\r\n    <div class=\"full-width\" *ngIf=\"templateform\">\r\n        <form [formGroup]=\"templateform\">\r\n            <!-- <div class=\"flex-row\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Template</mat-label>\r\n                    <input matInput placeholder=\"Template\" formControlName=\"template\" required>\r\n                </mat-form-field>\r\n            </div> -->\r\n            <mpm-mpm-auto-complete *ngIf=\"createProjectFromTemplate\" (valueChangeEvent)='setTemplate($event)'\r\n                class=\"flex-row\" [searchFieldConfig]=\"templateAutocompleteField\" [required]=\"true\" [autoFocus]=\"true\">\r\n            </mpm-mpm-auto-complete>\r\n\r\n            <div class=\"flex-row\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>{{isCopyTemplate? 'Template Name' : 'Project Name'}}</mat-label>\r\n                    <input matInput placeholder=\"{{isCopyTemplate? 'Template Name' : 'Project Name'}}\"\r\n                        formControlName=\"projectName\" required>\r\n                    <mat-hint align=\"end\"\r\n                        *ngIf=\"templateform.get('projectName').errors && templateform.get('projectName').errors.maxlength\">\r\n                        {{templateform.get('projectName').errors.maxlength.actualLength}}/{{templateform.get('projectName').errors.maxlength.requiredLength}}\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n            <mpm-mpm-auto-complete class=\"flex-row\" [searchFieldConfig]=\"categoryMetadataField\">\r\n            </mpm-mpm-auto-complete>\r\n            <div class=\"flex-row\" *ngIf=\"!isCopyTemplate\">\r\n                <mat-form-field *ngIf=\"campaignAutocompleteField && !campaignId\" class=\"flex-row-item\"\r\n                    appearance=\"outline\">\r\n                    <mat-label>Campaign Name</mat-label>\r\n                    <input matInput placeholder=\"Campaign Name\" formControlName=\"campaign\"\r\n                        (keyup)=\"getCampaigns($event)\" [matAutocomplete]=\"userAuto\">\r\n\r\n                    <!-- 201 -->\r\n                    <mat-autocomplete #userAuto=\"matAutocomplete\" (optionSelected)=\"changeAutoComplete($event)\">\r\n                        <mat-option *ngFor=\"let option of campaignAutocompleteField\" [value]=\"option.displayName\">\r\n                            <div #optionposition\r\n                                [matTooltip]=\"spanposition.getBoundingClientRect().width > optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                                <span #spanposition\r\n                                    [matTooltip]=\"spanposition.getBoundingClientRect().width <= optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                                    {{option.displayName}}\r\n                                </span>\r\n                            </div>\r\n                        </mat-option>\r\n                    </mat-autocomplete>\r\n\r\n                </mat-form-field>\r\n                <mpm-mpm-auto-complete *ngIf=\"campaignAutocompleteField && campaignId && isCampaign\" class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"campaignAutocompleteField\" [required]=\"false\">\r\n                </mpm-mpm-auto-complete>\r\n                <mat-form-field *ngIf=\"!campaignOwnerAutocompleteField && isCampaign\" class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Campaign Owner</mat-label>\r\n                    <input matInput placeholder=\"Campaign Owner\" formControlName=\"campaignOwner\">\r\n                </mat-form-field>\r\n                <mpm-mpm-auto-complete *ngIf=\"campaignOwnerAutocompleteField && isCampaign\" class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"campaignOwnerAutocompleteField\" [required]=\"false\">\r\n                </mpm-mpm-auto-complete>\r\n            </div>\r\n            <div class=\"flex-row\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>{{isCopyTemplate? 'Team' : 'Project Team'}}</mat-label>\r\n                    <input matInput placeholder=\"{{isCopyTemplate? 'Team' : 'Project Team'}}\"\r\n                        formControlName=\"projectTeam\" [disabled]=\"true\" required>\r\n                </mat-form-field>\r\n                <mat-form-field *ngIf=\"!ownerAutocompleteField\" class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>{{isCopyTemplate? 'Owner' : 'Project Owner'}}</mat-label>\r\n                    <input matInput placeholder=\"{{isCopyTemplate? 'Owner' : 'Project Owner'}}\"\r\n                        formControlName=\"projectOwner\" required>\r\n                </mat-form-field>\r\n                <mpm-mpm-auto-complete *ngIf=\"ownerAutocompleteField\" class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"ownerAutocompleteField\" [required]=\"true\">\r\n                </mpm-mpm-auto-complete>\r\n            </div>\r\n\r\n            <div class=\"flex-row\" *ngIf=\"!isCopyTemplate\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Project Start Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"projectStartDate\" [matDatepickerFilter]=\"dateFilter\"\r\n                        [min]=\"minStartDate\"\r\n                        [max]=\"selectedCampaign && selectedCampaign.CAMPAIGN_END_DATE ? selectedCampaign.CAMPAIGN_END_DATE : null\"\r\n                        placeholder=\"Project Start Date (MM/DD/YYYY)\" formControlName=\"projectStartDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"projectStartDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #projectStartDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"templateform.get('projectStartDate') && (templateform.get('projectStartDate').hasError('matDatepickerMax') || templateform.get('projectStartDate').hasError('matDatepickerMin'))\">\r\n                        {{startDateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Project End Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"projectEndDate\" [matDatepickerFilter]=\"dateFilter\"\r\n                        [min]=\"templateform.value.projectStartDate\"\r\n                        [max]=\"selectedCampaign && selectedCampaign.CAMPAIGN_END_DATE ? selectedCampaign.CAMPAIGN_END_DATE : null\"\r\n                        placeholder=\"Project End Date (MM/DD/YYYY)\" formControlName=\"projectEndDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"projectEndDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #projectEndDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"templateform.get('projectEndDate') && (templateform.get('projectEndDate').hasError('matDatepickerMax') || templateform.get('projectEndDate').hasError('matDatepickerMin'))\">\r\n                        {{endDateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"flex-row\" *ngIf=\"!isCopyTemplate\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Task Start Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"taskStartDate\" [matDatepickerFilter]=\"dateFilter\"\r\n                        [min]=\"templateform.value.projectStartDate\" [max]=\"templateform.value.projectEndDate\"\r\n                        placeholder=\"Task Start Date (MM/DD/YYYY)\" formControlName=\"taskStartDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"taskStartDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #taskStartDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"templateform.get('taskStartDate') && (templateform.get('taskStartDate').hasError('matDatepickerMax') || templateform.get('taskStartDate').hasError('matDatepickerMin'))\">\r\n                        {{taskStartDateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Task End Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"taskEndDate\" [matDatepickerFilter]=\"dateFilter\"\r\n                        [min]=\"templateform.value.taskStartDate\" [max]=\"templateform.value.projectEndDate\"\r\n                        placeholder=\"Task End Date (MM/DD/YYYY)\" formControlName=\"taskEndDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"taskEndDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #taskEndDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"templateform.get('taskEndDate') && (templateform.get('taskEndDate').hasError('matDatepickerMax') || templateform.get('taskEndDate').hasError('matDatepickerMin'))\">\r\n                        {{taskEndDateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"flex-row\" *ngIf=\"!isCopyTemplate\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Expected Duration</mat-label>\r\n                    <input matInput placeholder=\"Expected Duration\" formControlName=\"expectedDuration\" type=\"number\">\r\n                    <mat-hint align=\"end\" *ngIf=\"templateform.get('expectedDuration').errors\">\r\n                        Please provide a valid duration\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"flex-row description-hint\">\r\n                <mat-form-field class=\"flex-row-item\" appearance=\"outline\">\r\n                    <mat-label>Description</mat-label>\r\n                    <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\r\n                    <mat-error align=\"end\"\r\n                        *ngIf=\"templateform && templateform.get('description').errors && templateform.get('description').errors.maxlength\">\r\n                        {{templateform.get('description').errors.maxlength.actualLength}}/{{templateform.get('description').errors.maxlength.requiredLength}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n\r\n            <div class=\"flex-row custom-field-wrapper\" *ngIf=\"templateform.controls.CustomFieldGroup\"\r\n                formGroupName=\"CustomFieldGroup\">\r\n                <mat-accordion style=\"width: 100%;\" displayMode=\"flat\" togglePosition=\"before\" multi=\"true\">\r\n                    <mat-expansion-panel class=\"mat-elevation-z0 form-group-panel\" [expanded]=\"true\"\r\n                        *ngFor=\"let formGroup of templateform.get('CustomFieldGroup')['controls']\">\r\n                        <mat-expansion-panel-header>\r\n                            <mat-panel-title>{{formGroup.name}}</mat-panel-title>\r\n                        </mat-expansion-panel-header>\r\n\r\n                        <div class=\"flex-row\" *ngFor=\"let formControl of formGroup['controls']['fieldset']['controls']\">\r\n                            <div class=\"flex-row-item\">\r\n                                <mpm-custom-metadata-field style=\"width: 100%;\" [metadataFieldFormControl]=\"formControl\"\r\n                                    (valueChanges)=\"onCustomMetadataFieldChange($event)\"></mpm-custom-metadata-field>\r\n                            </div>\r\n                        </div>\r\n                    </mat-expansion-panel>\r\n                </mat-accordion>\r\n            </div>\r\n\r\n            <div [ngStyle]=\"{'margin-left': isCopyTemplate ? '7em' : '17em'}\" class=\"flex-row toggle-action-row\">\r\n                <mat-slide-toggle color=\"primary\" [checked]=\"projectDependency.copyTaskChecked\"\r\n                    (change)=\"checkTaskAccess()\" formControlName=\"copyTask\">\r\n                    <!-- class=\"flex-row-item\" -->\r\n                    Copy Task\r\n                </mat-slide-toggle>\r\n\r\n                <mat-slide-toggle style=\"padding-left: 6%;\" color=\"primary\"\r\n                    [checked]=\"projectDependency.copyDeliverableChecked\" (change)=\"checkDeliverableAccess()\"\r\n                    formControlName=\"copyDeliverable\">\r\n                    <!-- class=\"flex-row-item\" -->\r\n                    Copy Deliverable\r\n                </mat-slide-toggle>\r\n\r\n                <mat-slide-toggle style=\"padding-left: 6%;\" color=\"primary\"\r\n                    *ngIf=\"isCopyTemplate && selectedProject.IS_CUSTOM_WORKFLOW\"\r\n                    [checked]=\"projectDependency.copyWorfklowRulesChecked\" formControlName=\"copyWorflowRules\">\r\n                    <!-- class=\"flex-row-item\" -->\r\n                    Copy Workflow Rules\r\n                </mat-slide-toggle>\r\n                <!-- <mat-slide-toggle style=\"padding-left: 6%;\" class=\"flex-row-item\" color=\"primary\"\r\n                    [checked]=\"projectDependency.copyCustomMetaDataChecked\" formControlName=\"copyCustomMetaData\">\r\n                    Copy Custom Metadata\r\n                </mat-slide-toggle> -->\r\n            </div>\r\n            <div class=\"flex-row\" *ngIf=\"note && note != ''\">\r\n                <div class=\"flex-row-item note\">\r\n                    <span style=\"color: red;\">Note: </span>\r\n                    <span>{{note}}</span>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item right\">\r\n                        <span class=\"form-actions\">\r\n                            <button type=\"button\" mat-stroked-button (click)=\"closeDialog()\">Cancel</button>\r\n\r\n                            <button type=\"button\" id=\"template-save-btn\" mat-flat-button color=\"primary\"\r\n                                matTooltip=\"{{SELECTED_SAVE_OPTION.name}}\" (click)=\"createProject(SELECTED_SAVE_OPTION)\"\r\n                                [disabled]=\"templateform.invalid || disableTemplateSave\">\r\n                                {{SELECTED_SAVE_OPTION.name}}\r\n                            </button>\r\n\r\n                            <button type=\"button\" id=\"template-save-option\" mat-flat-button color=\"primary\"\r\n                                [matMenuTriggerFor]=\"saveOptionsMenu\"\r\n                                [disabled]=\"templateform.invalid || disableTemplateSave\">\r\n                                <mat-icon>arrow_drop_down</mat-icon>\r\n                            </button>\r\n\r\n                            <mat-menu #saveOptionsMenu=\"matMenu\">\r\n                                <span *ngFor=\"let saveOption of saveOptions\">\r\n                                    <button type=\"button\" *ngIf=\"saveOption.value != SELECTED_SAVE_OPTION.value\"\r\n                                        mat-menu-item (click)=\"createProject(saveOption)\">\r\n                                        <span>{{saveOption.name}}</span>\r\n                                    </button>\r\n                                </span>\r\n                            </mat-menu>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>\r\n",
        styles: ["mat-form-field{margin:0 5px}.form-actions{margin-top:2%}.right{justify-content:flex-end}.toggle-action-row{margin-top:12px}.custom-field-wrapper{margin:-20px -20px 0}#template-save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}#template-save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:15px}.full-width{width:100%}.description-hint{margin-bottom:2%}::ng-deep .mat-hint{color:red}"]
    })
], ProjectFromTemplatesComponent);
export { ProjectFromTemplatesComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1mcm9tLXRlbXBsYXRlcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Byb2plY3QtZnJvbS10ZW1wbGF0ZXMvcHJvamVjdC1mcm9tLXRlbXBsYXRlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBRTFFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQzlGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3JILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM1RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBRWxFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDcEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBTzNDLElBQWEsNkJBQTZCLEdBQTFDLE1BQWEsNkJBQTZCO0lBaUd4QyxZQUNTLEVBQWUsRUFDZixjQUE4QixFQUM5QixhQUE0QixFQUM1QiwwQkFBc0QsRUFDdEQsTUFBaUIsRUFDakIsa0JBQXNDLEVBQ3RDLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3RCLE1BQWMsRUFDZCxjQUE4QixFQUM5QixRQUFrQjtRQVZsQixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLCtCQUEwQixHQUExQiwwQkFBMEIsQ0FBNEI7UUFDdEQsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBcEdqQix5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFHbkQsc0JBQWlCLEdBQXNCO1lBQ3JDLG9CQUFvQixFQUFFLEVBQUU7WUFDeEIsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixhQUFhLEVBQUUsRUFBRTtZQUNqQixXQUFXLEVBQUUsRUFBRTtTQUNoQixDQUFDO1FBT0Ysc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFVdEMsaUJBQVksR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRWhDLGdCQUFXLEdBQUcsa0JBQWtCLENBQUMsbUJBQW1CLENBQUM7UUFDckQsd0JBQW1CLEdBQUcsS0FBSyxDQUFDO1FBRTVCLGlCQUFZLEdBQUc7WUFDYixJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLEVBQUU7WUFDMUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7WUFDakQsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUFFLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxHQUFHO1lBQ3ZHLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFO1NBQzNDLENBQUM7UUFFRixzQkFBaUIsR0FBRztZQUNsQixlQUFlLEVBQUUsS0FBSztZQUN0QixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLHNCQUFzQixFQUFFLEtBQUs7WUFDN0IsdUJBQXVCLEVBQUUsSUFBSTtZQUM3Qix3QkFBd0IsRUFBRSxLQUFLO1lBQy9CLHlCQUF5QixFQUFFLElBQUk7WUFDL0IseUJBQXlCLEVBQUUsS0FBSztZQUNoQywwQkFBMEIsRUFBRSxJQUFJO1lBQ2hDLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLFNBQVMsRUFBRSxLQUFLO1lBQ2hCLGdCQUFnQixFQUFFLEtBQUs7WUFDdkIsdUNBQXVDLEVBQUUsS0FBSztTQUMvQyxDQUFDO1FBRUYsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFFeEIseUJBQW9CLEdBQUc7WUFDckIsS0FBSyxFQUFFLFFBQVE7WUFDZixJQUFJLEVBQUUsUUFBUTtTQUNmLENBQUM7UUFDRix3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFNM0IsK0JBQTBCLEdBQUcsbURBQW1ELENBQUM7UUFDakYsb0NBQStCLEdBQUcsZ0JBQWdCLENBQUM7UUFFbkQsNENBQXVDLEdBQUcsc0JBQXNCLENBQUM7UUFDakUsZ0NBQTJCLEdBQUcsaUJBQWlCLENBQUM7UUFTaEQsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBa3lCMUIsZUFBVSxHQUNSLENBQUMsSUFBaUIsRUFBRSxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDdkIsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQzFCLE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUM5QixnQkFBZ0I7Z0JBQ2hCLGtCQUFrQjthQUNuQjtRQUNILENBQUMsQ0FBQTtJQXZ4QkMsQ0FBQztJQUVMLDZCQUE2QjtRQUMzQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQzlILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDN0Q7UUFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFDbEYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDakUsV0FBVyxHQUFHLENBQUMsQ0FBQyxXQUFXLElBQUksV0FBVyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztTQUN6RTtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUNoQyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDOUgsWUFBWSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUM7Z0JBQ3ZILFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUM7Z0JBQzdCLGFBQWEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxDQUFDO2dCQUMvRCxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUN2RSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDcEYsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUMvSCxlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7b0JBQy9CLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLEVBQUUsUUFBUSxFQUM1RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsdUJBQXVCO2lCQUNqRCxDQUFDO2dCQUNGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDO29CQUNoQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHdCQUF3QjtvQkFDdEQsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyx5QkFBeUI7aUJBQzNELENBQUM7Z0JBQ0YsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQzthQUNqRSxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDaEMsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7Z0JBQzlILFlBQVksRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxDQUFDO2dCQUN2SCxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO2dCQUM3QixhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQztnQkFDL0QsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdkUsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUN2RSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDcEYsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM1RCxjQUFjLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMxRCxhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN6RCxNQUFNO2dCQUNOLG9HQUFvRztnQkFFcEcsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3BGLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDL0gsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO29CQUMvQixLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixFQUFFLFFBQVEsRUFDNUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QjtpQkFDakQsQ0FBQztnQkFDRixnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQztvQkFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyx3QkFBd0I7b0JBQ3RELFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMseUJBQXlCO2lCQUMzRCxDQUFDO2dCQUNGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUM7YUFDakUsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxtREFBbUQ7UUFDbkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzNDLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1FBRXZDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDakYsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckYsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO2dCQUMzSyxJQUFJLGtCQUFrQixLQUFLLEdBQUcsRUFBRTtvQkFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDMUQ7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3RELE1BQU0sd0JBQXdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGtCQUFrQixDQUFDLENBQUM7b0JBQy9FLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2lCQUNoRjtnQkFDRCxrREFBa0Q7Z0JBQ2xELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO29CQUM3RSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDckQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUM1RDtnQkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUNyQztnQkFDRCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFDNUYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLGVBQWUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztpQkFDbEU7Z0JBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQywwQkFBMEIsQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2FBQzlKO1NBQ0Y7UUFDRCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwRSxJQUFJLFFBQVEsRUFBRTtnQkFDWixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDbEcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMvQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRSxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7b0JBQ25OLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUMvRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDOzRCQUNyRSxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU07NEJBQ3hDLFFBQVEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUTt5QkFDN0MsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDeEMsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDdkQ7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUN0RSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQztvQkFDcEgsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQ2hILElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxFQUFFLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7b0JBQ3pKLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxlQUFlLENBQUMsbUNBQW1DLENBQUM7aUJBQ2xGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUM7aUJBQ3ZFO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQzthQUN2RTtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ3BFLE1BQU07WUFDTixnQ0FBZ0M7WUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDaEUsTUFBTTtZQUNOLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN6QixJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUM7b0JBQ2pILElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsRUFBRTtvQkFDL0csSUFBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxtQ0FBbUMsQ0FBQztpQkFDaEY7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxnQ0FBZ0MsQ0FBQztpQkFDN0U7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGdDQUFnQyxDQUFDO2FBQzdFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO2dCQUN4RyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7b0JBQ3JILElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDekgsSUFBSSxDQUFDLHlCQUF5QixHQUFHLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQzthQUMxRTtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2pFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUs7Z0JBQ2hELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ25ILElBQUksQ0FBQyx1QkFBdUIsR0FBRyxlQUFlLENBQUMsNkJBQTZCLENBQUM7YUFDOUU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHVCQUF1QixHQUFHLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQzthQUN4RTtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtZQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN4RSxJQUFJLElBQUksRUFBRTtvQkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzFEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxRQUFRO1FBQ3hCLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNuRixPQUFPLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7U0FDbEU7SUFDSCxDQUFDO0lBRUQsWUFBWSxDQUFDLFVBQVU7UUFDckIsSUFBSSxVQUFVLEVBQUU7WUFDZCxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxLQUFLLFVBQVUsQ0FBQyxDQUFDO1lBQ25HLE9BQU8sZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDaEQsaUlBQWlJO1NBQ2xJO2FBQU07WUFDTCxPQUFPLEVBQUUsQ0FBQztTQUNYO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFZLEVBQUUsTUFBZTtRQUMzQyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsZUFBZSxJQUFJLE1BQU0sSUFBSSxNQUFNLEtBQUssRUFBRSxFQUFFO1lBQ2pFLE9BQU87Z0JBQ0wsSUFBSSxFQUFFLE9BQU8sQ0FBQyxlQUFlO2dCQUM3QixLQUFLLEVBQUUsTUFBTTtnQkFDYixXQUFXLEVBQUUsT0FBTyxDQUFDLGVBQWUsR0FBRyxJQUFJLEdBQUcsTUFBTSxHQUFHLEdBQUc7Z0JBQzFELEVBQUUsRUFBRSxNQUFNO2FBQ1gsQ0FBQztTQUNIO2FBQU0sSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxPQUFPLENBQUMsTUFBTSxLQUFLLEVBQUUsRUFBRTtZQUM3RCxPQUFPO2dCQUNMLElBQUksRUFBRSxPQUFPLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxRQUFRO2dCQUM3QyxLQUFLLEVBQUUsT0FBTyxDQUFDLE1BQU07Z0JBQ3JCLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksR0FBRyxPQUFPLENBQUMsTUFBTSxHQUFHLEdBQUc7Z0JBQ3BGLEVBQUUsRUFBRSxPQUFPLENBQUMsTUFBTTthQUNuQixDQUFDO1NBQ0g7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsV0FBVztRQUNULE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUUsSUFBSSxDQUFDLCtCQUErQixFQUFFLElBQUksQ0FBQztnQkFDeEcsbUNBQW1DO2lCQUNsQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRTtvQkFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNyQyxRQUFRLENBQUMsUUFBUSxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUN6QztvQkFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7b0JBQ3RDLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQ3BHO29CQUNELE1BQU0sa0JBQWtCLEdBQUcsRUFBRSxDQUFDO29CQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDL0I7OzRCQUVJO3dCQUNKLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDL0MsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTs0QkFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7NEJBQ3hILGtCQUFrQixDQUFDLElBQUksQ0FBQztnQ0FDdEIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxhQUFhO2dDQUM1QixLQUFLLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0NBQ2pDLFdBQVcsRUFBRSxLQUFLOzZCQUNuQixDQUFDLENBQUM7eUJBQ0o7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixHQUFHLGtCQUFrQixDQUFDO2lCQUNsRDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztpQkFDbEM7Z0JBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQseUJBQXlCLENBQUMsTUFBYztRQUN0QyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ1gsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ2xCO1lBQ0QsTUFBTSxVQUFVLEdBQUc7Z0JBQ2pCLE1BQU0sRUFBRSxNQUFNO2FBQ2YsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUM5RSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsYUFBYSxDQUFDO2dCQUMxQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUVMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELGlCQUFpQixDQUFDLFFBQVEsRUFBRSxPQUFPO1FBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsQ0FBQztRQUM1SixNQUFNLG9CQUFvQixHQUFHLE9BQU8sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2hJLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsb0JBQW9CLENBQUM7YUFDbkgsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLGlCQUFpQixFQUFFO2dCQUNyQixJQUFJLGlCQUFpQixDQUFDLG9CQUFvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLENBQUM7dUJBQzlGLGlCQUFpQixDQUFDLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3RELGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7d0JBQ2pELFVBQVUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUU7NEJBQzFELElBQUksV0FBVyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssaUJBQWlCLENBQUMsSUFBSSxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUU7Z0NBQ2xGLFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDOzZCQUNqRDt3QkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDTCxDQUFDLENBQUMsQ0FBQztvQkFDSCxpQkFBaUIsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2xILElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsRUFBRTt3QkFDN0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQztxQkFDckQ7b0JBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxTQUFTLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDdEg7YUFDRjtRQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0Qsb0JBQW9CO1FBQ2xCLElBQUksQ0FBQyw4QkFBOEIsR0FBRztZQUNwQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYTtZQUNyRCxLQUFLLEVBQUUsZ0JBQWdCO1lBQ3ZCLGFBQWEsRUFBRSxFQUFFO1NBQ2xCLENBQUM7UUFDRixJQUFJLENBQUMseUJBQXlCLEdBQUc7WUFDL0IsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVE7WUFDaEQsS0FBSyxFQUFFLGVBQWU7WUFDdEIsYUFBYSxFQUFFLElBQUksQ0FBQyxzQkFBc0I7U0FDM0MsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDaEUsT0FBTztTQUNSO1FBQ0Qsb0RBQW9EO1FBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDO2dCQUNqRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsc0NBQXNDO2FBQ2pHO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsc0JBQXNCLEdBQUc7WUFDNUIsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFlBQVk7WUFDcEQsS0FBSyxFQUFFLGVBQWU7WUFDdEIsYUFBYSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDckMsQ0FBQztJQUNKLENBQUM7SUFDRCx1QkFBdUI7UUFDckIsSUFBSSxDQUFDLHlCQUF5QixHQUFHO1lBQy9CLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRO1lBQ2hELEtBQUssRUFBRSxVQUFVO1lBQ2pCLGFBQWEsRUFBRSxJQUFJLENBQUMsZUFBZTtTQUNwQyxDQUFDO0lBQ0osQ0FBQztJQUVELCtCQUErQjtRQUM3QixJQUFJLENBQUMscUJBQXFCLEdBQUc7WUFDM0IsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQjtZQUN4RCxLQUFLLEVBQUUsbUJBQW1CO1lBQzFCLGFBQWEsRUFBRSxJQUFJLENBQUMsdUJBQXVCO1NBQzVDLENBQUM7SUFDSixDQUFDO0lBRUQsMkJBQTJCLENBQUMsS0FBSztRQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUN4RSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM3RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxhQUFhLENBQUMsVUFBVTtRQUN0QixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxLQUFLLE9BQU8sSUFBSSxJQUFJLEVBQUU7WUFDaEQsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDOUQsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUscUNBQXFDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDckgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMxQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDckcsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUNuSCxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7Z0JBQ2xCLElBQUksSUFBSSxDQUFDLHlCQUF5QixFQUFFO29CQUNsQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDM0YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUM5RDtxQkFBTTtvQkFDTCxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxlQUFlLEVBQy9FLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztpQkFDOUQ7Z0JBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUM7eUJBQzVNLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUNqQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7d0JBQ3JELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNoQyxJQUFJLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUU7NEJBQ2pGLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO2dDQUMvQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUN0QztpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQzs2QkFDN0I7eUJBQ0Y7b0JBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNULElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNsQyxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsMEJBQTBCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxzQkFBc0IsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQzt5QkFDNU0sU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ2pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQzt3QkFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ2hDLElBQUkscUJBQXFCLENBQUMsTUFBTSxDQUFDLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRTs0QkFDakYsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7Z0NBQy9DLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBQ3RDO2lDQUFNO2dDQUNMLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDOzZCQUM3Qjt5QkFDRjtvQkFDSCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7d0JBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO2lCQUNOO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxRQUFRLENBQUMsUUFBUTtRQUNmLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDeEksTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN4SSxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzVJLElBQUksQ0FBQyxZQUFZLEdBQUc7WUFDbEIsSUFBSSxFQUFFLFNBQVM7WUFDZixLQUFLLEVBQUUsT0FBTztZQUNkLFdBQVcsRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUFHLE9BQU8sR0FBRyxHQUFHO1lBQzdDLEVBQUUsRUFBRSxPQUFPO1NBQ1osQ0FBQztJQUNKLENBQUM7SUFFRCxPQUFPLENBQUMsUUFBUTtRQUNkLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3ZJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDbEUsQ0FBQztJQUVELGNBQWMsQ0FBQyxRQUFRO1FBQ3JCLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hJLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0QsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFDNUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLFdBQVcsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUM3RyxDQUFDO0lBRUQsV0FBVyxDQUFDLFFBQWE7UUFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUM7UUFDakQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxRQUFRLEVBQUU7WUFDWixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQy9CO1FBQ0QsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDckksTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1FBQy9KLElBQUksa0JBQWtCLEtBQUssR0FBRyxFQUFFO1lBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3JELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN6RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3hDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN0RCxNQUFNLHdCQUF3QixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQy9FLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQy9FLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbEU7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQzthQUNqRixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztZQUNqQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUM1QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxpQ0FBaUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUNyRyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBQ0QsZUFBZTtRQUNiLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3pHLENBQUM7SUFFRCxzQkFBc0I7UUFDcEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDekcsQ0FBQztJQUVELG9CQUFvQixDQUFDLFVBQWU7UUFDbEMsSUFBSSx3QkFBd0IsQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFO1lBQzFFLHdCQUF3QixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEtBQUssVUFBVSxDQUFDLENBQUM7U0FDckk7UUFDRCxPQUFPLHdCQUF3QixDQUFDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3BFLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRTtZQUM5QixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN0RDthQUFNO1lBQ0wsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzdELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0osT0FBTyxFQUFFLGlDQUFpQztvQkFDMUMsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNuQjthQUNGLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUN0RDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQsS0FBSztJQUVMLG9CQUFvQixDQUFDLFlBQVk7UUFDL0IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLGdCQUFnQixHQUFHO2dCQUN2QixZQUFZLEVBQUUsWUFBWTthQUMzQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFLElBQUksQ0FBQyx1Q0FBdUMsRUFBRSxnQkFBZ0IsQ0FBQztnQkFDNUgsbUNBQW1DO2lCQUNsQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRTtvQkFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNyQyxRQUFRLENBQUMsUUFBUSxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUN6QztvQkFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7b0JBRXRDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNwRSxJQUFJLFFBQVEsRUFBRTs0QkFDWixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDOUcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0NBQ3pCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUMvQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRSxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7Z0NBQ25OLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29DQUMvRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQ0FDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO3dDQUNyRSxNQUFNLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU07d0NBQ3hDLFFBQVEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUTtxQ0FDN0MsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQ0FDeEMsQ0FBQyxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzs2QkFDdkQ7eUJBQ0Y7NkJBQU07NEJBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs0QkFDN0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzt5QkFDdkQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7d0JBQ3RFLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUN6QixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDO2dDQUNwSCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQ0FDaEgsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEVBQUUsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxZQUFZLENBQUMsRUFBRTtnQ0FDekosSUFBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyxtQ0FBbUMsQ0FBQzs2QkFDbEY7aUNBQU07Z0NBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQzs2QkFDdkU7eUJBQ0Y7NkJBQU07NEJBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQzt5QkFDdkU7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO3dCQUNwRSxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNqRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzt5QkFDakU7d0JBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQ3pCLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQztnQ0FDakgsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO2dDQUMvRyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLG1DQUFtQyxDQUFDOzZCQUNoRjtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGdDQUFnQyxDQUFDOzZCQUM3RTt5QkFDRjs2QkFBTTs0QkFDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGdDQUFnQyxDQUFDO3lCQUM3RTtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7NENBd0R3QjtvQkFFeEIsTUFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7b0JBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUMvQixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQy9DLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7NEJBQ2pGLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxFQUFFOzRCQUN4SCxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0NBQ3RCLElBQUksRUFBRSxRQUFRLENBQUMsYUFBYTtnQ0FDNUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO2dDQUNqQyxXQUFXLEVBQUUsS0FBSztnQ0FDbEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFOzZCQUN4RCxDQUFDLENBQUM7eUJBQ0o7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxrQkFBa0IsQ0FBQztpQkFDekM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDaEMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ2xDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFlBQVksQ0FBQyxZQUFZO1FBQ3ZCLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN4QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtnQkFDaEYsSUFBSSxDQUFDLHlCQUF5QixHQUFHLGdCQUFnQixDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBSztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBO1FBQ3ZCOzs7Ozs7Ozs7Y0FTTTtJQUNSLENBQUM7SUFFRCxNQUFNO0lBQ04sZUFBZSxDQUFDLFVBQVU7UUFDeEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixNQUFNLGdCQUFnQixHQUFHO2dCQUN2QixVQUFVLEVBQUUsVUFBVTthQUN2QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDL0csU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNwQixJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDckMsUUFBUSxDQUFDLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDekM7b0JBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO29CQUN0QyxNQUFNLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQy9CLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDL0MsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTs0QkFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7NEJBQ3hILGtCQUFrQixDQUFDLElBQUksQ0FBQztnQ0FDdEIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxhQUFhO2dDQUM1QixLQUFLLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0NBQ2pDLFdBQVcsRUFBRSxLQUFLOzZCQUNuQixDQUFDLENBQUM7eUJBQ0o7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixHQUFHLGtCQUFrQixDQUFDO2lCQUNsRDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztpQkFDbEM7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDM0MsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLENBQUMseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1NBQ3hDO2FBQU07WUFDTCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO1NBQ3ZDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztRQUM5SCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBRTNJLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdJLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDMUksSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ3ZKLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7WUFDbkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztTQUMvQjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUU7YUFDekMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQzNCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxlQUFlLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN4RCx1QkFBUyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxJQUFLLElBQUksRUFBRztZQUM1SCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsTUFBTTtRQUNOLHVDQUF1QztRQUN2QyxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtZQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUM7aUJBQ3BILFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxFQUFFO29CQUNwQyxJQUFJLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7b0JBQ25ELE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDMUgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN0QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDakMsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLDZCQUE2QixFQUFFLENBQUM7aUJBQ3RDO3FCQUFNO29CQUNMLE1BQU0sU0FBUyxHQUFHO3dCQUNoQixPQUFPLEVBQUUsd0JBQXdCO3dCQUNqQyxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUk7cUJBQy9DLENBQUM7b0JBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDdEM7WUFDSCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFDckYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUM3RCxJQUFJLFVBQVUsRUFBRTtnQkFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDMUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQztvQkFDakMsSUFBSSxDQUFDLDZCQUE2QixFQUFFLENBQUM7Z0JBQ3ZDLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDVCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLENBQUMsNkJBQTZCLEVBQUUsQ0FBQztnQkFDdkMsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsNkJBQTZCLEVBQUUsQ0FBQzthQUN0QztTQUNGO1FBQ0QsTUFBTTtJQUNSLENBQUM7Q0FhRixDQUFBOztZQW55QmMsV0FBVztZQUNDLGNBQWM7WUFDZixhQUFhO1lBQ0EsMEJBQTBCO1lBQzlDLFNBQVM7WUFDRyxrQkFBa0I7WUFDekIsV0FBVztZQUNaLFVBQVU7WUFDZCxNQUFNO1lBQ0UsY0FBYztZQUNwQixRQUFROztBQTFHbEI7SUFBUixLQUFLLEVBQUU7K0VBQTBCO0FBQ3pCO0lBQVIsS0FBSyxFQUFFO2tFQUFhO0FBQ1o7SUFBUixLQUFLLEVBQUU7aUVBQVk7QUFDWDtJQUFSLEtBQUssRUFBRTtzRUFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7aUVBQVk7QUFDWDtJQUFSLEtBQUssRUFBRTtxRUFBZ0I7QUFDZDtJQUFULE1BQU0sRUFBRTsyRUFBZ0Q7QUFDL0M7SUFBVCxNQUFNLEVBQUU7MEVBQStDO0FBQzlDO0lBQVQsTUFBTSxFQUFFOzBFQUErQztBQUM5QztJQUFULE1BQU0sRUFBRTtxRUFBMEM7QUFYeEMsNkJBQTZCO0lBTHpDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSw0QkFBNEI7UUFDdEMsbzJmQUFzRDs7S0FFdkQsQ0FBQztHQUNXLDZCQUE2QixDQXE0QnpDO1NBcjRCWSw2QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMsIEZvcm1BcnJheSwgRm9ybUJ1aWxkZXIgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDdXN0b21NZXRhZGF0YU9iaiB9IGZyb20gJy4vcHJvamVjdC5mcm9tLnRlbXBsYXRlJztcclxuaW1wb3J0IHsgUHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC1mcm9tLXRlbXBsYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBTYXZlT3B0aW9uQ29uc3RhbnQgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3NhdmUtb3B0aW9ucy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBEYXRhVHlwZUNvbnN0YW50cyB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvZGF0YS10eXBlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFBlcnNvbiB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1BlcnNvbic7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLXByb2plY3QtZnJvbS10ZW1wbGF0ZXMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9wcm9qZWN0LWZyb20tdGVtcGxhdGVzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9wcm9qZWN0LWZyb20tdGVtcGxhdGVzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFByb2plY3RGcm9tVGVtcGxhdGVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgdGVtcGxhdGVTZWFyY2hDb25kaXRpb25zO1xyXG4gIEBJbnB1dCgpIHByb2plY3RUeXBlO1xyXG4gIEBJbnB1dCgpIHZpZXdDb25maWc7XHJcbiAgQElucHV0KCkgc2VsZWN0ZWRQcm9qZWN0O1xyXG4gIEBJbnB1dCgpIGNhbXBhaWduSWQ7XHJcbiAgQElucHV0KCkgaXNDb3B5VGVtcGxhdGU7XHJcbiAgQE91dHB1dCgpIGNsb3NlQ2FsbGJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHNhdmVDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgbm90aWZpY2F0aW9uSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBsb2FkaW5nSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICB0ZW1wbGF0ZWZvcm06IEZvcm1Hcm91cDtcclxuICBjdXN0b21NZXRhZGF0YU9iajogQ3VzdG9tTWV0YWRhdGFPYmogPSB7XHJcbiAgICBjdXN0b21NZXRhZGF0YUZpZWxkczogW10sXHJcbiAgICBjdXN0b21GaWVsZHNHcm91cDogW10sXHJcbiAgICBmaWVsZHNldEdyb3VwOiB7fSxcclxuICAgIGZpZWxkR3JvdXBzOiBbXVxyXG4gIH07XHJcbiAgdGVtcGxhdGVBdXRvY29tcGxldGVGaWVsZDtcclxuICBjYXRlZ29yeU1ldGFkYXRhRmllbGQ7XHJcbiAgb3duZXJBdXRvY29tcGxldGVGaWVsZDtcclxuICBjYW1wYWlnbk93bmVyQXV0b2NvbXBsZXRlRmllbGQ7XHJcbiAgY2FtcGFpZ25BdXRvY29tcGxldGVGaWVsZDtcclxuICBub3RlO1xyXG4gIG1wbUZpZWxkQ29uc3RhbnRzID0gTVBNRmllbGRDb25zdGFudHM7XHJcbiAgY3JlYXRlUHJvamVjdEZyb21UZW1wbGF0ZTogYm9vbGVhbjtcclxuICBwcm9qZWN0T3duZXJMaXN0O1xyXG4gIGFsbFRlbXBsYXRlTGlzdDtcclxuICBpc0NhbXBhaWduOiBhbnk7XHJcblxyXG4gIGFwcENvbmZpZztcclxuICBwcm9qZWN0Q29uZmlnO1xyXG4gIG5hbWVTdHJpbmdQYXR0ZXJuO1xyXG5cclxuICBtaW5TdGFydERhdGU6IERhdGUgPSBuZXcgRGF0ZSgpO1xyXG5cclxuICBzYXZlT3B0aW9ucyA9IFNhdmVPcHRpb25Db25zdGFudC50ZW1wbGF0ZVNhdmVPcHRpb25zO1xyXG4gIGRpc2FibGVUZW1wbGF0ZVNhdmUgPSBmYWxzZTtcclxuXHJcbiAgcHJvamVjdE93bmVyID0ge1xyXG4gICAgbmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRkaXNwbGF5TmFtZSgpLFxyXG4gICAgdmFsdWU6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKSxcclxuICAgIGRpc3BsYXlOYW1lOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldGRpc3BsYXlOYW1lKCkgKyAnICgnICsgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckNOKCkgKyAnKScsXHJcbiAgICBjbjogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckNOKClcclxuICB9O1xyXG5cclxuICBwcm9qZWN0RGVwZW5kZW5jeSA9IHtcclxuICAgIGNvcHlUYXNrQ2hlY2tlZDogZmFsc2UsXHJcbiAgICBjb3B5VGFza0Rpc2FibGVkOiB0cnVlLFxyXG4gICAgY29weURlbGl2ZXJhYmxlQ2hlY2tlZDogZmFsc2UsXHJcbiAgICBjb3B5RGVsaXZlcmFibGVEaXNhYmxlZDogdHJ1ZSxcclxuICAgIGNvcHlXb3Jma2xvd1J1bGVzQ2hlY2tlZDogZmFsc2UsXHJcbiAgICBjb3B5V29yZmtsb3dSdWxlc0Rpc2FibGVkOiB0cnVlLFxyXG4gICAgY29weUN1c3RvbU1ldGFEYXRhQ2hlY2tlZDogZmFsc2UsXHJcbiAgICBjb3B5Q3VzdG9tTWV0YURhdGFEaXNhYmxlZDogdHJ1ZSxcclxuICAgIHNlbGVjdGVkT2JqZWN0OiB7fSxcclxuICAgIGlzTm9UYXNrczogZmFsc2UsXHJcbiAgICBpc05vRGVsaXZlcmFibGVzOiBmYWxzZSxcclxuICAgIGlzVGFza1dpdGhUYXNrV2l0aERlbGl2ZXJhYmxlRGVwZW5kZW5jeTogZmFsc2VcclxuICB9O1xyXG5cclxuICBpc0NyZWF0ZUFub3RoZXIgPSBmYWxzZTtcclxuXHJcbiAgU0VMRUNURURfU0FWRV9PUFRJT04gPSB7XHJcbiAgICB2YWx1ZTogJ0NSRUFURScsXHJcbiAgICBuYW1lOiAnQ3JlYXRlJ1xyXG4gIH07XHJcbiAgZGVsaXZlcmFibGVWaWV3TmFtZSA9IG51bGw7XHJcblxyXG4gIHNlbGVjdGVkQ2FtcGFpZ247XHJcbiAgY2FtcGFpZ25MaXN0O1xyXG4gIGNhbXBhaWduRGF0YUZpbHRlckxpc3Q7XHJcbiAgY2FtcGFpZ25Pd25lckRldGFpbHM7XHJcbiAgQ0FNUEFJR05fREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvQ2FtcGFpZ24vb3BlcmF0aW9ucyc7XHJcbiAgR0VUX0FMTF9DQU1QQUlHTl9XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxDYW1wYWlnbic7XHJcblxyXG4gIEdFVF9BTExfY0FNUEFJR05fQllfTkFNRV9XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxDYW1wYWlnbkJ5TmFtZSc7XHJcbiAgR0VUX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FID0gJ0dldENhbXBhaWduQnlJZCc7XHJcblxyXG4gIHN0YXJ0RGF0ZUVycm9yTWVzc2FnZTtcclxuICBlbmREYXRlRXJyb3JNZXNzYWdlO1xyXG4gIHRhc2tTdGFydERhdGVFcnJvck1lc3NhZ2U7XHJcbiAgdGFza0VuZERhdGVFcnJvck1lc3NhZ2U7XHJcbiAgZW5hYmxlRHVyYXRpb247XHJcbiAgZW5hYmxlRHVwbGljYXRlTmFtZXM7XHJcblxyXG4gIGlzQ2FtcGFpZ25Qcm9qZWN0ID0gZmFsc2U7XHJcbiAgYWxsY2F0ZWdvcnlNZXRhZGF0YUxpc3Q6IGFueTtcclxuICBlbmFibGVDYXRlZ29yeU1ldGFkYXRhOiBhbnk7XHJcblxyXG4gIGVuYWJsZVdvcmtXZWVrO1xyXG5cclxuICBmaWx0ZXJPcHRpb25zO1xyXG4gIGVuYWJsZUNhbXBhaWduO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBmYjogRm9ybUJ1aWxkZXIsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgcHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2U6IFByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgIHB1YmxpYyBkYXRlUGlwZTogRGF0ZVBpcGVcclxuICApIHsgfVxyXG5cclxuICBpbml0aWFsaXplUHJvamVjdFRlbXBsYXRlRm9ybSgpIHtcclxuICAgIGlmICh0aGlzLmN1c3RvbU1ldGFkYXRhT2JqICYmIHRoaXMuY3VzdG9tTWV0YWRhdGFPYmouY3VzdG9tRmllbGRzR3JvdXAgJiYgdGhpcy5jdXN0b21NZXRhZGF0YU9iai5jdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXSkge1xyXG4gICAgICB0aGlzLmN1c3RvbU1ldGFkYXRhT2JqLmN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddID0gbnVsbDtcclxuICAgIH1cclxuICAgIHRoaXMudGVtcGxhdGVmb3JtID0gbnVsbDtcclxuICAgIGxldCBkZXNjcmlwdGlvbiA9ICcnO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgIGRlc2NyaXB0aW9uID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmVpbGRWYWx1ZUJ5TWFwcGVyTmFtZSh0aGlzLnNlbGVjdGVkUHJvamVjdCxcclxuICAgICAgICB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0RFU0NSSVBUSU9OKTtcclxuICAgICAgZGVzY3JpcHRpb24gPSAoIWRlc2NyaXB0aW9uIHx8IGRlc2NyaXB0aW9uID09PSAnTkEnKSA/ICcnIDogZGVzY3JpcHRpb247XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5pc0NvcHlUZW1wbGF0ZSkge1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgIHByb2plY3ROYW1lOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICBwcm9qZWN0T3duZXI6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLmlzQ2FtcGFpZ25Qcm9qZWN0ID8gJycgOiB0aGlzLnByb2plY3RPd25lci5kaXNwbGF5TmFtZSwgZGlzYWJsZWQ6ICd0cnVlJyB9KSxcclxuICAgICAgICBjYW1wYWlnbjogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICBjYW1wYWlnbk93bmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiAndHJ1ZScgfSksXHJcbiAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbChkZXNjcmlwdGlvbiwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgcHJvamVjdFRlYW06IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6ICd0cnVlJyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgIGNvcHlUYXNrOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5jb3B5VGFza0NoZWNrZWQsIGRpc2FibGVkOiB0aGlzLnByb2plY3REZXBlbmRlbmN5LmNvcHlUYXNrRGlzYWJsZWQgfSksXHJcbiAgICAgICAgY29weURlbGl2ZXJhYmxlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgdmFsdWU6IHRoaXMucHJvamVjdERlcGVuZGVuY3kuY29weURlbGl2ZXJhYmxlQ2hlY2tlZCwgZGlzYWJsZWQ6XHJcbiAgICAgICAgICAgIHRoaXMucHJvamVjdERlcGVuZGVuY3kuY29weURlbGl2ZXJhYmxlRGlzYWJsZWRcclxuICAgICAgICB9KSxcclxuICAgICAgICBjb3B5V29yZmxvd1J1bGVzOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgdmFsdWU6IHRoaXMucHJvamVjdERlcGVuZGVuY3kuY29weVdvcmZrbG93UnVsZXNDaGVja2VkLFxyXG4gICAgICAgICAgZGlzYWJsZWQ6IHRoaXMucHJvamVjdERlcGVuZGVuY3kuY29weVdvcmZrbG93UnVsZXNEaXNhYmxlZFxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNhdGVnb3J5TWV0YWRhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IHRydWUgfSksXHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgICBwcm9qZWN0TmFtZTogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgcHJvamVjdE93bmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5pc0NhbXBhaWduUHJvamVjdCA/ICcnIDogdGhpcy5wcm9qZWN0T3duZXIuZGlzcGxheU5hbWUsIGRpc2FibGVkOiAndHJ1ZScgfSksXHJcbiAgICAgICAgY2FtcGFpZ246IG5ldyBGb3JtQ29udHJvbCgnJyksXHJcbiAgICAgICAgY2FtcGFpZ25Pd25lcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogJ3RydWUnIH0pLFxyXG4gICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woZGVzY3JpcHRpb24sIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgIGV4cGVjdGVkRHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucGF0dGVybignXlswLTldKiQnKV0pLFxyXG4gICAgICAgIHByb2plY3RUZWFtOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiAndHJ1ZScgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICBwcm9qZWN0U3RhcnREYXRlOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgcHJvamVjdEVuZERhdGU6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICB0YXNrU3RhcnREYXRlOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgLy8gMjA1XHJcbiAgICAgICAgLy90YXNrRW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogdGhpcy5lbmFibGVEdXJhdGlvbiB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG5cclxuICAgICAgICB0YXNrRW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogJ3RydWUnIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgY29weVRhc2s6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnByb2plY3REZXBlbmRlbmN5LmNvcHlUYXNrQ2hlY2tlZCwgZGlzYWJsZWQ6IHRoaXMucHJvamVjdERlcGVuZGVuY3kuY29weVRhc2tEaXNhYmxlZCB9KSxcclxuICAgICAgICBjb3B5RGVsaXZlcmFibGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICB2YWx1ZTogdGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5jb3B5RGVsaXZlcmFibGVDaGVja2VkLCBkaXNhYmxlZDpcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5jb3B5RGVsaXZlcmFibGVEaXNhYmxlZFxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNvcHlXb3JmbG93UnVsZXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICB2YWx1ZTogdGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5jb3B5V29yZmtsb3dSdWxlc0NoZWNrZWQsXHJcbiAgICAgICAgICBkaXNhYmxlZDogdGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5jb3B5V29yZmtsb3dSdWxlc0Rpc2FibGVkXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgY2F0ZWdvcnlNZXRhZGF0YTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogdHJ1ZSB9KSxcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy90aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0VGVhbS5kaXNhYmxlKCk7XHJcbiAgICB0aGlzLnRlbXBsYXRlZm9ybS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICB0aGlzLnNldENhdGVnb3J5TWV0YWRhdGFBdXRvQ29tcGxldGUoKTtcclxuXHJcbiAgICBpZiAodGhpcy5jYW1wYWlnbklkKSB7XHJcbiAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhbXBhaWduLnNldFZhbHVlKHRoaXMuZm9ybUNhbXBhaWduKHRoaXMuY2FtcGFpZ25JZCkpO1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYW1wYWlnbi5kaXNhYmxlKCk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jcmVhdGVQcm9qZWN0RnJvbVRlbXBsYXRlKSB7XHJcbiAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmFkZENvbnRyb2woJ3RlbXBsYXRlJywgbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pKTtcclxuICAgICAgdGhpcy5zZXRUZW1wbGF0ZUF1dG9Db21wbGV0ZSgpO1xyXG4gICAgICB0aGlzLm1hcEN1c3RvbU1ldGFkYXRhKCcnLCBudWxsKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgIGNvbnN0IGNhdGVnb3J5TWV0YWRhdGFJZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGhpcy5zZWxlY3RlZFByb2plY3QsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfQ0FURUdPUllfTUVUQURBVEFfSUQpO1xyXG4gICAgICAgIGlmIChjYXRlZ29yeU1ldGFkYXRhSWQgPT09ICcwJykge1xyXG4gICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY2F0ZWdvcnlNZXRhZGF0YS5lbmFibGUoKTtcclxuICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhdGVnb3J5TWV0YWRhdGEuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLmRpc2FibGUoKTtcclxuICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQ2F0ZWdvcnlNZXRhZGF0YSA9IHRoaXMuZm9ybUNhdGVnb3J5TWV0YWRhdGEoY2F0ZWdvcnlNZXRhZGF0YUlkKTtcclxuICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhdGVnb3J5TWV0YWRhdGEuc2V0VmFsdWUoc2VsZWN0ZWRDYXRlZ29yeU1ldGFkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyoqIGN1c3RvbSB3b3JrZmxvdy1jb3B5d29ya2Zsb3dydWxlcyBpbXBsZW1lbnQgKi9cclxuICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5zZWxlY3RlZFByb2plY3QuSVNfQ1VTVE9NX1dPUktGTE9XKSkge1xyXG4gICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVdvcmZsb3dSdWxlcy5lbmFibGUoKTtcclxuICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlXb3JmbG93UnVsZXMuc2V0VmFsdWUodHJ1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0VGVhbSh0aGlzLnNlbGVjdGVkUHJvamVjdCk7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnblByb2plY3QpIHtcclxuICAgICAgICAgIHRoaXMuc2V0T3duZXIodGhpcy5zZWxlY3RlZFByb2plY3QpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBmcm9tUHJvamVjdE5hbWUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRoaXMuc2VsZWN0ZWRQcm9qZWN0LFxyXG4gICAgICAgICAgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9OQU1FKTtcclxuICAgICAgICBpZiAoZnJvbVByb2plY3ROYW1lKSB7XHJcbiAgICAgICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0TmFtZS5zZXRWYWx1ZShmcm9tUHJvamVjdE5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm1hcEN1c3RvbU1ldGFkYXRhKHRoaXMuc2VsZWN0ZWRQcm9qZWN0LCBudWxsKTtcclxuICAgICAgICB0aGlzLnByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLnZhbGlkYXRlUHJvamVjdFRhc2thbmREZWxpdmVyYWJsZSh0aGlzLnNlbGVjdGVkUHJvamVjdCwgdGhpcy50ZW1wbGF0ZWZvcm0sIHRoaXMucHJvamVjdERlcGVuZGVuY3ksIHRoaXMuZGVsaXZlcmFibGVWaWV3TmFtZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuc2V0T3duZXJBdXRvQ29tcGxldGUoKTtcclxuICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhbXBhaWduLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoY2FtcGFpZ24gPT4ge1xyXG4gICAgICBpZiAoY2FtcGFpZ24pIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSB0aGlzLmNhbXBhaWduTGlzdC5maW5kKGRhdGEgPT4gZGF0YVsnQ2FtcGFpZ24taWQnXS5JZCA9PT0gY2FtcGFpZ24udmFsdWUpO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIHRoaXMubWluU3RhcnREYXRlID0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0odGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUsICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSA/IHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFIDogbmV3IERhdGUoKTtcclxuICAgICAgICAgIHRoaXMuZ2V0Q2FtcGFpZ25Pd25lckRldGFpbHNJZCh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uUl9QT19DQU1QQUlHTl9PV05FUlsnSWRlbnRpdHktaWQnXS5JZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICBGdWxsTmFtZTogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy5mdWxsTmFtZVxyXG4gICAgICAgICAgICB9LCB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTikpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSBudWxsO1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RTdGFydERhdGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICBpZiAoKG5ldyBEYXRlKHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RTdGFydERhdGUudmFsdWUpIDwgbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUpIHx8XHJcbiAgICAgICAgICBuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0U3RhcnREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSkpICYmXHJcbiAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0U3RhcnREYXRlLnZhbHVlKSwgJ3l5eXktTU0tZGQnKSA+PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICB0aGlzLnN0YXJ0RGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX0NBTVBBSUdOX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnN0YXJ0RGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdEVuZERhdGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgIC8vIDIwNVxyXG4gICAgICAvLyAgICBpZiAodGhpcy5lbmFibGVEdXJhdGlvbikge1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy50YXNrRW5kRGF0ZS5zZXRWYWx1ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSk7XHJcbiAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tFbmREYXRlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgLy8gICB9XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICBpZiAobmV3IERhdGUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdEVuZERhdGUudmFsdWUpIDwgbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUpIHx8XHJcbiAgICAgICAgICBuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fRU5EX0RBVEUpKSB7XHJcbiAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9DQU1QQUlHTl9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5lbmREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlBST0pFQ1RfU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tTdGFydERhdGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgIGlmICgodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdFN0YXJ0RGF0ZS52YWx1ZSB8fCB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSkgJiZcclxuICAgICAgICAobmV3IERhdGUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMudGFza1N0YXJ0RGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0U3RhcnREYXRlLnZhbHVlKSB8fFxyXG4gICAgICAgICAgbmV3IERhdGUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMudGFza1N0YXJ0RGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSkpKSB7XHJcbiAgICAgICAgdGhpcy50YXNrU3RhcnREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlRBU0tfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tFbmREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMudGFza1N0YXJ0RGF0ZS52YWx1ZSAmJlxyXG4gICAgICAgIG5ldyBEYXRlKHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tFbmREYXRlLnZhbHVlKSA8IG5ldyBEYXRlKHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tTdGFydERhdGUudmFsdWUpKSB7XHJcbiAgICAgICAgdGhpcy50YXNrRW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5UQVNLX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnRhc2tFbmREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlRBU0tfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGlmICh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhKSB7XHJcbiAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhdGVnb3J5TWV0YWRhdGEudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICB0aGlzLm1hcEN1c3RvbU1ldGFkYXRhKHRoaXMuc2VsZWN0ZWRQcm9qZWN0LCBkYXRhLnZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZm9ybUNhbXBhaWduVmFsdWUoY2FtcGFpZ24pIHtcclxuICAgIGlmIChjYW1wYWlnbi5DQU1QQUlHTl9OQU1FICYmIGNhbXBhaWduWydDYW1wYWlnbi1pZCddICYmIGNhbXBhaWduWydDYW1wYWlnbi1pZCddLklkKSB7XHJcbiAgICAgIHJldHVybiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCArICctJyArIGNhbXBhaWduLkNBTVBBSUdOX05BTUU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmb3JtQ2FtcGFpZ24oY2FtcGFpZ25JZCkge1xyXG4gICAgaWYgKGNhbXBhaWduSWQpIHtcclxuICAgICAgY29uc3Qgc2VsZWN0ZWRDYW1wYWlnbiA9IHRoaXMuY2FtcGFpZ25EYXRhRmlsdGVyTGlzdC5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC52YWx1ZSA9PT0gY2FtcGFpZ25JZCk7XHJcbiAgICAgIHJldHVybiBzZWxlY3RlZENhbXBhaWduID8gc2VsZWN0ZWRDYW1wYWlnbiA6ICcnO1xyXG4gICAgICAvKiByZXR1cm4gc2VsZWN0ZWRDYW1wYWlnbiAmJiBBcnJheS5pc0FycmF5KHNlbGVjdGVkQ2FtcGFpZ24pID8gc2VsZWN0ZWRDYW1wYWlnbiA6IHNlbGVjdGVkQ2FtcGFpZ24gPyBbc2VsZWN0ZWRDYW1wYWlnbl0gOiAnJzsgKi9cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZvcm1Vc2VyRGV0YWlscyh1c2VyT2JqOiBhbnksIHVzZXJJZD86IHN0cmluZykge1xyXG4gICAgaWYgKHVzZXJPYmogJiYgdXNlck9iai5Vc2VyRGlzcGxheU5hbWUgJiYgdXNlcklkICYmIHVzZXJJZCAhPT0gJycpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICBuYW1lOiB1c2VyT2JqLlVzZXJEaXNwbGF5TmFtZSxcclxuICAgICAgICB2YWx1ZTogdXNlcklkLFxyXG4gICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyT2JqLlVzZXJEaXNwbGF5TmFtZSArICcgKCcgKyB1c2VySWQgKyAnKScsXHJcbiAgICAgICAgY246IHVzZXJJZFxyXG4gICAgICB9O1xyXG4gICAgfSBlbHNlIGlmICh1c2VyT2JqICYmIHVzZXJPYmouVXNlcklkICYmIHVzZXJPYmouVXNlcklkICE9PSAnJykge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIG5hbWU6IHVzZXJPYmouRGVzY3JpcHRpb24gfHwgdXNlck9iai5GdWxsTmFtZSxcclxuICAgICAgICB2YWx1ZTogdXNlck9iai5Vc2VySWQsXHJcbiAgICAgICAgZGlzcGxheU5hbWU6ICh1c2VyT2JqLkRlc2NyaXB0aW9uIHx8IHVzZXJPYmouRnVsbE5hbWUpICsgJyAoJyArIHVzZXJPYmouVXNlcklkICsgJyknLFxyXG4gICAgICAgIGNuOiB1c2VyT2JqLnVzZXJJZFxyXG4gICAgICB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRDYW1wYWlnbigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQU1QQUlHTl9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FLCBudWxsKVxyXG4gICAgICAgIC8vIHRoaXMuYXBwU2VydmljZS5nZXRBbGxDYW1wYWlnbigpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UuQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLkNhbXBhaWduKSkge1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlLkNhbXBhaWduID0gW3Jlc3BvbnNlLkNhbXBhaWduXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IHJlc3BvbnNlLkNhbXBhaWduO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jYW1wYWlnbklkKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduID0gdGhpcy5jYW1wYWlnbkxpc3QuZmluZChkYXRhID0+IGRhdGFbJ0NhbXBhaWduLWlkJ10uSWQgPT09IHRoaXMuY2FtcGFpZ25JZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25GaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0Lm1hcChjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgLyogaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJZCAmJiB0YXNrWydUYXNrLWlkJ10uSWQgPT09IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJZCkge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgPSB0YXNrO1xyXG4gICAgICAgICAgICAgIH0gKi9cclxuICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZm9ybUNhbXBhaWduVmFsdWUoY2FtcGFpZ24pO1xyXG4gICAgICAgICAgICAgIGlmIChjYW1wYWlnbi5DQU1QQUlHTl9OQU1FICYmIGNhbXBhaWduWydDYW1wYWlnbi1pZCddICYmIGNhbXBhaWduWydDYW1wYWlnbi1pZCddLklkICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSwgJ3l5eXktTU0tZGQnKSA+PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICAgICAgICBjYW1wYWlnbkZpbHRlckxpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgIG5hbWU6IGNhbXBhaWduLkNBTVBBSUdOX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHZhbHVlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduRGF0YUZpbHRlckxpc3QgPSBjYW1wYWlnbkZpbHRlckxpc3Q7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduRGF0YUZpbHRlckxpc3QgPSBbXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q2FtcGFpZ25Pd25lckRldGFpbHNJZCh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8UGVyc29uPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAoIXVzZXJJZCkge1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICB1c2VySWQ6IHVzZXJJZFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocGVyc29uRGV0YWlscyA9PiB7XHJcbiAgICAgICAgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscyA9IHBlcnNvbkRldGFpbHM7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dCgpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIH0pO1xyXG4gIH1cclxuICBtYXBDdXN0b21NZXRhZGF0YSh0ZW1wbGF0ZSwgbW9kZWxJZCkge1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIGNvbnN0IG1ldGFkYXRhTW9kZWxJZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGVtcGxhdGUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfQ0FURUdPUllfTUVUQURBVEFfSUQpO1xyXG4gICAgY29uc3QgY2F0ZWdvcnlMZXZlbERldGFpbHMgPSBtb2RlbElkID09PSBudWxsID8gdGhpcy5mb3JtQ2F0ZWdvcnlNZXRhZGF0YShtZXRhZGF0YU1vZGVsSWQpIDogdGhpcy5mb3JtQ2F0ZWdvcnlNZXRhZGF0YShtb2RlbElkKTtcclxuICAgIHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuZ2V0T1RNTUN1c3RvbU1ldGFEYXRhKHRoaXMuY3VzdG9tTWV0YWRhdGFPYmosIHRoaXMuZmIsIHRlbXBsYXRlLCBjYXRlZ29yeUxldmVsRGV0YWlscylcclxuICAgICAgLnN1YnNjcmliZShjdXN0b21NZXRhZGF0YU9iaiA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICBpZiAoY3VzdG9tTWV0YWRhdGFPYmopIHtcclxuICAgICAgICAgIGlmIChjdXN0b21NZXRhZGF0YU9iai5jdXN0b21NZXRhZGF0YUZpZWxkcyAmJiBBcnJheS5pc0FycmF5KGN1c3RvbU1ldGFkYXRhT2JqLmN1c3RvbU1ldGFkYXRhRmllbGRzKVxyXG4gICAgICAgICAgICAmJiBjdXN0b21NZXRhZGF0YU9iai5jdXN0b21NZXRhZGF0YUZpZWxkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmZpZWxkR3JvdXBzLmZvckVhY2goZmllbGRHcm91cCA9PiB7XHJcbiAgICAgICAgICAgICAgZmllbGRHcm91cC5jb250cm9scy5maWVsZHNldC5jb250cm9scy5mb3JFYWNoKGZvcm1Db250cm9sID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChmb3JtQ29udHJvbC5maWVsZHNldC5kYXRhX3R5cGUgPT09IERhdGFUeXBlQ29uc3RhbnRzLkRBVEUgJiYgZm9ybUNvbnRyb2wudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2wudmFsdWUgPSBuZXcgRGF0ZShmb3JtQ29udHJvbC52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5jdXN0b21GaWVsZHNHcm91cCA9IHRoaXMuZmIuZ3JvdXAoeyBmaWVsZHNldDogbmV3IEZvcm1BcnJheShjdXN0b21NZXRhZGF0YU9iai5maWVsZHNldEdyb3VwKSB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGVtcGxhdGVmb3JtLmdldCgnQ3VzdG9tRmllbGRHcm91cCcpKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0ucmVtb3ZlQ29udHJvbCgnQ3VzdG9tRmllbGRHcm91cCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmFkZENvbnRyb2woJ0N1c3RvbUZpZWxkR3JvdXAnLCBuZXcgRm9ybUFycmF5KGN1c3RvbU1ldGFkYXRhT2JqLmZpZWxkR3JvdXBzLCB7IHVwZGF0ZU9uOiAnYmx1cicgfSkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG4gIHNldE93bmVyQXV0b0NvbXBsZXRlKCkge1xyXG4gICAgdGhpcy5jYW1wYWlnbk93bmVyQXV0b2NvbXBsZXRlRmllbGQgPSB7XHJcbiAgICAgIGZvcm1Db250cm9sOiB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYW1wYWlnbk93bmVyLFxyXG4gICAgICBsYWJlbDogJ0NhbXBhaWduIE93bmVyJyxcclxuICAgICAgZmlsdGVyT3B0aW9uczogW11cclxuICAgIH07XHJcbiAgICB0aGlzLmNhbXBhaWduQXV0b2NvbXBsZXRlRmllbGQgPSB7XHJcbiAgICAgIGZvcm1Db250cm9sOiB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYW1wYWlnbixcclxuICAgICAgbGFiZWw6ICdDYW1wYWlnbiBOYW1lJyxcclxuICAgICAgZmlsdGVyT3B0aW9uczogdGhpcy5jYW1wYWlnbkRhdGFGaWx0ZXJMaXN0XHJcbiAgICB9O1xyXG4gICAgaWYgKCF0aGlzLnByb2plY3RPd25lckxpc3QgfHwgdGhpcy5wcm9qZWN0T3duZXJMaXN0Lmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0T3duZXIuZW5hYmxlKCk7XHJcbiAgICB0aGlzLnByb2plY3RPd25lckxpc3QubWFwKHVzZXIgPT4ge1xyXG4gICAgICBpZiAodXNlci5jbiA9PT0gdGhpcy5wcm9qZWN0T3duZXIuY24pIHtcclxuICAgICAgICB1c2VyLmRpc3BsYXlOYW1lID0gdGhpcy5wcm9qZWN0T3duZXIuZGlzcGxheU5hbWU7XHJcbiAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdE93bmVyLnBhdGNoVmFsdWUodXNlcik7IC8vIC5kaXNwbGF5TmFtZSArICcgKCcgKyB1c2VyLmNuICsgJyknXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMub3duZXJBdXRvY29tcGxldGVGaWVsZCA9IHtcclxuICAgICAgZm9ybUNvbnRyb2w6IHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RPd25lcixcclxuICAgICAgbGFiZWw6ICdQcm9qZWN0IE93bmVyJyxcclxuICAgICAgZmlsdGVyT3B0aW9uczogdGhpcy5wcm9qZWN0T3duZXJMaXN0XHJcbiAgICB9O1xyXG4gIH1cclxuICBzZXRUZW1wbGF0ZUF1dG9Db21wbGV0ZSgpIHtcclxuICAgIHRoaXMudGVtcGxhdGVBdXRvY29tcGxldGVGaWVsZCA9IHtcclxuICAgICAgZm9ybUNvbnRyb2w6IHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRlbXBsYXRlLFxyXG4gICAgICBsYWJlbDogJ1RlbXBsYXRlJyxcclxuICAgICAgZmlsdGVyT3B0aW9uczogdGhpcy5hbGxUZW1wbGF0ZUxpc3RcclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBzZXRDYXRlZ29yeU1ldGFkYXRhQXV0b0NvbXBsZXRlKCkge1xyXG4gICAgdGhpcy5jYXRlZ29yeU1ldGFkYXRhRmllbGQgPSB7XHJcbiAgICAgIGZvcm1Db250cm9sOiB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLFxyXG4gICAgICBsYWJlbDogJ0NhdGVnb3J5IE1ldGFkYXRhJyxcclxuICAgICAgZmlsdGVyT3B0aW9uczogdGhpcy5hbGxjYXRlZ29yeU1ldGFkYXRhTGlzdFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIG9uQ3VzdG9tTWV0YWRhdGFGaWVsZENoYW5nZShldmVudCkge1xyXG4gICAgdGhpcy50ZW1wbGF0ZWZvcm0uZ2V0KCdDdXN0b21GaWVsZEdyb3VwJylbJ2NvbnRyb2xzJ10uZm9yRWFjaChmb3JtR3JvdXAgPT4ge1xyXG4gICAgICBmb3JtR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVQcm9qZWN0KHNhdmVPcHRpb24pIHtcclxuICAgIGlmICh0aGlzLnRlbXBsYXRlZm9ybS5zdGF0dXMgPT09ICdWQUxJRCcgfHwgdHJ1ZSkge1xyXG4gICAgICBpZiAodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdE5hbWUudmFsdWUudHJpbSgpID09PSAnJykge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgcHJvamVjdCBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdE5hbWUuc2V0VmFsdWUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdE5hbWUudmFsdWUudHJpbSgpKTtcclxuICAgICAgICBjb25zdCBjdXN0b21NZXRhZGF0YSA9IHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuZ2V0Q3VzdG9tTWV0YWRhdGEodGhpcy5jdXN0b21NZXRhZGF0YU9iai5jdXN0b21GaWVsZHNHcm91cCk7XHJcbiAgICAgICAgbGV0IHNvdXJjZUlkID0gJyc7XHJcbiAgICAgICAgaWYgKHRoaXMuY3JlYXRlUHJvamVjdEZyb21UZW1wbGF0ZSkge1xyXG4gICAgICAgICAgc291cmNlSWQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRoaXMudGVtcGxhdGVmb3JtLnZhbHVlLnRlbXBsYXRlLFxyXG4gICAgICAgICAgICB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0lURU1fSUQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBzb3VyY2VJZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGhpcy5zZWxlY3RlZFByb2plY3QsXHJcbiAgICAgICAgICAgIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICBpZiAodGhpcy5pc0NvcHlUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgdGhpcy5wcm9qZWN0RnJvbVRlbXBsYXRlU2VydmljZS5jb3B5VGVtcGxhdGUoc291cmNlSWQsIHRoaXMudGVtcGxhdGVmb3JtLCB0aGlzLnRlbXBsYXRlU2VhcmNoQ29uZGl0aW9ucy50ZW1wbGF0ZVNlYXJjaENvbmRpdGlvbiwgY3VzdG9tTWV0YWRhdGEsIHRoaXMucHJvamVjdE93bmVyLCB0aGlzLnZpZXdDb25maWcsIHRoaXMuZW5hYmxlRHVwbGljYXRlTmFtZXMpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JlYXRlUHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChjcmVhdGVQcm9qZWN0UmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgaWYgKGNyZWF0ZVByb2plY3RSZXNwb25zZVsndHlwZSddID09PSBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2F2ZU9wdGlvbiAmJiBzYXZlT3B0aW9uLnZhbHVlID09PSAnQ1JFQVRFJykge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlQ2FsbGJhY2tIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmlzQ3JlYXRlQW5vdGhlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuY3JlYXRlUHJvamVjdChzb3VyY2VJZCwgdGhpcy50ZW1wbGF0ZWZvcm0sIHRoaXMudGVtcGxhdGVTZWFyY2hDb25kaXRpb25zLnByb2plY3RTZWFyY2hDb25kaXRpb24sIGN1c3RvbU1ldGFkYXRhLCB0aGlzLnByb2plY3RPd25lciwgdGhpcy52aWV3Q29uZmlnLCB0aGlzLmVuYWJsZUR1cGxpY2F0ZU5hbWVzKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZVByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoY3JlYXRlUHJvamVjdFJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgIGlmIChjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ3R5cGUnXSA9PT0gUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNhdmVPcHRpb24gJiYgc2F2ZU9wdGlvbi52YWx1ZSA9PT0gJ0NSRUFURScpIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5pc0NyZWF0ZUFub3RoZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgc2V0T3duZXIodGVtcGxhdGUpIHtcclxuICAgIGNvbnN0IG93bmVyQ04gPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX09XTkVSX0NOKTtcclxuICAgIGNvbnN0IG93bmVySWQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX09XTkVSX0lEKTtcclxuICAgIGNvbnN0IG93bmVyTmFtZSA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGVtcGxhdGUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfT1dORVJfTkFNRSk7XHJcbiAgICB0aGlzLnByb2plY3RPd25lciA9IHtcclxuICAgICAgbmFtZTogb3duZXJOYW1lLFxyXG4gICAgICB2YWx1ZTogb3duZXJJZCxcclxuICAgICAgZGlzcGxheU5hbWU6IG93bmVyTmFtZSArICcgKCcgKyBvd25lckNOICsgJyknLFxyXG4gICAgICBjbjogb3duZXJDTlxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIHNldFRlYW0odGVtcGxhdGUpIHtcclxuICAgIGNvbnN0IHRlYW1zVmFsdWUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX1RFQU0pO1xyXG4gICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdFRlYW0uc2V0VmFsdWUodGVhbXNWYWx1ZSk7XHJcbiAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0VGVhbS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgfVxyXG5cclxuICBzZXRQcm9qZWN0TmFtZSh0ZW1wbGF0ZSkge1xyXG4gICAgY29uc3QgcHJvamVjdE5hbWUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX05BTUUpO1xyXG4gICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdE5hbWUuc2V0VmFsdWUocHJvamVjdE5hbWUpO1xyXG4gICAgY29uc3QgZGVzY3JpcHRpb24gPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLFxyXG4gICAgICB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0RFU0NSSVBUSU9OKTtcclxuICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmRlc2NyaXB0aW9uLnNldFZhbHVlKCghZGVzY3JpcHRpb24gfHwgZGVzY3JpcHRpb24gPT09ICdOQScpID8gJycgOiBkZXNjcmlwdGlvbik7XHJcbiAgfVxyXG5cclxuICBzZXRUZW1wbGF0ZSh0ZW1wbGF0ZTogYW55KSB7XHJcbiAgICB0aGlzLnByb2plY3REZXBlbmRlbmN5LnNlbGVjdGVkT2JqZWN0ID0gdGVtcGxhdGU7XHJcbiAgICB0aGlzLnNldFRlYW0odGVtcGxhdGUpO1xyXG4gICAgaWYgKHRoaXMuaXNDYW1wYWlnblByb2plY3QpIHtcclxuICAgICAgdGhpcy5zZXRPd25lcih0ZW1wbGF0ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGVtcGxhdGUpIHtcclxuICAgICAgdGhpcy5zZXRQcm9qZWN0TmFtZSh0ZW1wbGF0ZSk7XHJcbiAgICB9XHJcbiAgICBjb25zdCB0ZWFtTmFtZSA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGVtcGxhdGUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfVEVBTSk7XHJcbiAgICBjb25zdCBjYXRlZ29yeU1ldGFkYXRhSWQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRlbXBsYXRlLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0NBVEVHT1JZX01FVEFEQVRBX0lEKTtcclxuICAgIGlmIChjYXRlZ29yeU1ldGFkYXRhSWQgPT09ICcwJykge1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLmVuYWJsZSgpO1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLnNldFZhbHVlKCcnKTtcclxuICAgICAgdGhpcy5tYXBDdXN0b21NZXRhZGF0YSh0ZW1wbGF0ZSwgbnVsbCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLmRpc2FibGUoKTtcclxuICAgICAgY29uc3Qgc2VsZWN0ZWRDYXRlZ29yeU1ldGFkYXRhID0gdGhpcy5mb3JtQ2F0ZWdvcnlNZXRhZGF0YShjYXRlZ29yeU1ldGFkYXRhSWQpO1xyXG4gICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLnNldFZhbHVlKHNlbGVjdGVkQ2F0ZWdvcnlNZXRhZGF0YSk7XHJcbiAgICAgIHRoaXMubWFwQ3VzdG9tTWV0YWRhdGEodGVtcGxhdGUsIHNlbGVjdGVkQ2F0ZWdvcnlNZXRhZGF0YS52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICB0aGlzLnByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLmdldE93bmVyTGlzdEJ5VGVhbSh0ZWFtTmFtZSwgdGhpcy5pc0NhbXBhaWduUHJvamVjdClcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0T3duZXJMaXN0ID0gcmVzcG9uc2U7XHJcbiAgICAgICAgdGhpcy5zZXRPd25lckF1dG9Db21wbGV0ZSgpO1xyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5wcm9qZWN0RnJvbVRlbXBsYXRlU2VydmljZS52YWxpZGF0ZVByb2plY3RUYXNrYW5kRGVsaXZlcmFibGUodGhpcy5wcm9qZWN0RGVwZW5kZW5jeS5zZWxlY3RlZE9iamVjdCxcclxuICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0sIHRoaXMucHJvamVjdERlcGVuZGVuY3ksIHRoaXMuZGVsaXZlcmFibGVWaWV3TmFtZSk7XHJcbiAgfVxyXG4gIGNoZWNrVGFza0FjY2VzcygpIHtcclxuICAgIHRoaXMubm90ZSA9IHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuY2hlY2tUYXNrQWNjZXNzKHRoaXMucHJvamVjdERlcGVuZGVuY3ksIHRoaXMudGVtcGxhdGVmb3JtKTtcclxuICB9XHJcblxyXG4gIGNoZWNrRGVsaXZlcmFibGVBY2Nlc3MoKSB7XHJcbiAgICB0aGlzLm5vdGUgPSB0aGlzLnByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLmNoZWNrVGFza0FjY2Vzcyh0aGlzLnByb2plY3REZXBlbmRlbmN5LCB0aGlzLnRlbXBsYXRlZm9ybSk7XHJcbiAgfVxyXG5cclxuICBmb3JtQ2F0ZWdvcnlNZXRhZGF0YShtZXRhZGF0YUlkOiBhbnkpIHtcclxuICAgIGxldCBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGE7XHJcbiAgICBpZiAodGhpcy5jYXRlZ29yeU1ldGFkYXRhRmllbGQgJiYgdGhpcy5jYXRlZ29yeU1ldGFkYXRhRmllbGQuZmlsdGVyT3B0aW9ucykge1xyXG4gICAgICBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgPSB0aGlzLmNhdGVnb3J5TWV0YWRhdGFGaWVsZC5maWx0ZXJPcHRpb25zLmZpbmQoY2F0ZWdvcnlNZXRhZGF0YSA9PiBjYXRlZ29yeU1ldGFkYXRhLnZhbHVlID09PSBtZXRhZGF0YUlkKTtcclxuICAgIH1cclxuICAgIHJldHVybiBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgPyBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgOiBudWxsO1xyXG4gIH1cclxuXHJcbiAgY2xvc2VEaWFsb2coKSB7XHJcbiAgICBpZiAodGhpcy50ZW1wbGF0ZWZvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRoaXMuaXNDcmVhdGVBbm90aGVyKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gY2xvc2U/JyxcclxuICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCh0aGlzLmlzQ3JlYXRlQW5vdGhlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vMjAxXHJcblxyXG4gIGdldEFsbENhbXBhaWduQnlOYW1lKGNhbXBhaWduTmFtZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgIENhbXBhaWduTmFtZTogY2FtcGFpZ25OYW1lXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuQ0FNUEFJR05fREVUQUlMU19NRVRIT0RfTlMsIHRoaXMuR0VUX0FMTF9jQU1QQUlHTl9CWV9OQU1FX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgIC8vIHRoaXMuYXBwU2VydmljZS5nZXRBbGxDYW1wYWlnbigpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UuQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLkNhbXBhaWduKSkge1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlLkNhbXBhaWduID0gW3Jlc3BvbnNlLkNhbXBhaWduXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IHJlc3BvbnNlLkNhbXBhaWduO1xyXG5cclxuICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY2FtcGFpZ24udmFsdWVDaGFuZ2VzLnN1YnNjcmliZShjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKGNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSB0aGlzLmNhbXBhaWduTGlzdC5maW5kKGRhdGEgPT4gZGF0YVsnQ2FtcGFpZ24taWQnXS5JZCA9PT0gY2FtcGFpZ24uc3BsaXQoLy0oLispLylbMF0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMubWluU3RhcnREYXRlID0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0odGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUsICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSA/IHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFIDogbmV3IERhdGUoKTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5nZXRDYW1wYWlnbk93bmVyRGV0YWlsc0lkKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBTVBBSUdOX09XTkVSWydJZGVudGl0eS1pZCddLklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICBVc2VySWQ6IHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgRnVsbE5hbWU6IHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMuZnVsbE5hbWVcclxuICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTikpO1xyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdFN0YXJ0RGF0ZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoKG5ldyBEYXRlKHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RTdGFydERhdGUudmFsdWUpIDwgbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUpIHx8XHJcbiAgICAgICAgICAgICAgICAgIG5ldyBEYXRlKHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RTdGFydERhdGUudmFsdWUpID4gbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFKSkgJiZcclxuICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdFN0YXJ0RGF0ZS52YWx1ZSksICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSkge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0RGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX0NBTVBBSUdOX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuc3RhcnREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMudGVtcGxhdGVmb3JtLmNvbnRyb2xzLnByb2plY3RFbmREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgIGlmICh0aGlzLmVuYWJsZUR1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy50YXNrRW5kRGF0ZS5zZXRWYWx1ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy50YXNrRW5kRGF0ZS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgIGlmIChuZXcgRGF0ZSh0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5wcm9qZWN0RW5kRGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSkgfHxcclxuICAgICAgICAgICAgICAgICAgbmV3IERhdGUodGhpcy50ZW1wbGF0ZWZvcm0uY29udHJvbHMucHJvamVjdEVuZERhdGUudmFsdWUpID4gbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFKSkge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9DQU1QQUlHTl9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbi52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGNhbXBhaWduID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRDYW1wYWlnbiA9IHRoaXMuY2FtcGFpZ25MaXN0LmZpbmQoZGF0YSA9PiBkYXRhWydDYW1wYWlnbi1pZCddLklkID09PSBjYW1wYWlnbi5zcGxpdCgvLSguKykvKVswXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWluU3RhcnREYXRlID0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0odGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUsICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSA/IHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFIDogbmV3IERhdGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRDYW1wYWlnbk93bmVyRGV0YWlsc0lkKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBTVBBSUdOX09XTkVSWydJZGVudGl0eS1pZCddLklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbk93bmVyLnNldFZhbHVlKHRoaXMuZm9ybVVzZXJEZXRhaWxzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBVc2VySWQ6IHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbk93bmVyLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5zdGFydERhdGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKChuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSA8IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSkpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5zdGFydERhdGUudmFsdWUpLCAneXl5eS1NTS1kZCcpID49IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9DQU1QQUlHTl9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5lbmREYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmVuZERhdGUudmFsdWUpID4gbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9DQU1QQUlHTl9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbmREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlBST0pFQ1RfU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuQ0FNUEFJR05fU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gKi9cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduRmlsdGVyTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdC5tYXAoY2FtcGFpZ24gPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5mb3JtQ2FtcGFpZ25WYWx1ZShjYW1wYWlnbik7XHJcbiAgICAgICAgICAgICAgaWYgKGNhbXBhaWduLkNBTVBBSUdOX05BTUUgJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10gJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFLCAneXl5eS1NTS1kZCcpID49IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgICAgICAgIGNhbXBhaWduRmlsdGVyTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgbmFtZTogY2FtcGFpZ24uQ0FNUEFJR05fTkFNRSxcclxuICAgICAgICAgICAgICAgICAgdmFsdWU6IGNhbXBhaWduWydDYW1wYWlnbi1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgIG93bmVySWQ6IGNhbXBhaWduLlJfUE9fQ0FNUEFJR05fT1dORVJbJ0lkZW50aXR5LWlkJ10uSWRcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsdGVyT3B0aW9ucyA9IGNhbXBhaWduRmlsdGVyTGlzdDtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0ID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuZmlsdGVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5maWx0ZXJPcHRpb25zKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldENhbXBhaWducyhjYW1wYWlnbk5hbWUpIHtcclxuICAgIGlmIChjYW1wYWlnbk5hbWUudGFyZ2V0LnZhbHVlLmxlbmd0aCA+IDIpIHtcclxuICAgICAgdGhpcy5nZXRBbGxDYW1wYWlnbkJ5TmFtZShjYW1wYWlnbk5hbWUudGFyZ2V0LnZhbHVlKS5zdWJzY3JpYmUoY2FtcGFpZ25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgdGhpcy5jYW1wYWlnbkF1dG9jb21wbGV0ZUZpZWxkID0gY2FtcGFpZ25SZXNwb25zZTtcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coXCJzZWxlY3RlZFwiKVxyXG4gICAgLyogdGhpcy5jYW1wYWlnbklkID0gZXZlbnQub3B0aW9uLnZhbHVlLnNwbGl0KC8tKC4rKS8pWzBdO1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRDYW1wYWlnbiA9IHRoaXMuZmlsdGVyT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSBldmVudC5vcHRpb24udmFsdWUuc3BsaXQoLy0oLispLylbMV0pO1xyXG4gICAgdGhpcy5nZXRDYW1wYWlnbk93bmVyRGV0YWlsc0lkKHNlbGVjdGVkQ2FtcGFpZ24ub3duZXJJZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgIFVzZXJJZDogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04sXHJcbiAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgfSwgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04pKTtcclxuXHJcbiAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbk93bmVyRmllbGRDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lcjtcclxuICAgIH0pOyAqL1xyXG4gIH1cclxuXHJcbiAgLy8gMjAxXHJcbiAgZ2V0Q2FtcGFpZ25CeUlkKGNhbXBhaWduSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICBDYW1wYWlnbklkOiBjYW1wYWlnbklkXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuQ0FNUEFJR05fREVUQUlMU19NRVRIT0RfTlMsIHRoaXMuR0VUX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlLkNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5DYW1wYWlnbikpIHtcclxuICAgICAgICAgICAgICByZXNwb25zZS5DYW1wYWlnbiA9IFtyZXNwb25zZS5DYW1wYWlnbl07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkxpc3QgPSByZXNwb25zZS5DYW1wYWlnbjtcclxuICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25GaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0Lm1hcChjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLmZvcm1DYW1wYWlnblZhbHVlKGNhbXBhaWduKTtcclxuICAgICAgICAgICAgICBpZiAoY2FtcGFpZ24uQ0FNUEFJR05fTkFNRSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCAmJlxyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0oY2FtcGFpZ24uQ0FNUEFJR05fRU5EX0RBVEUsICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSkge1xyXG4gICAgICAgICAgICAgICAgY2FtcGFpZ25GaWx0ZXJMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICBuYW1lOiBjYW1wYWlnbi5DQU1QQUlHTl9OQU1FLFxyXG4gICAgICAgICAgICAgICAgICB2YWx1ZTogY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkRhdGFGaWx0ZXJMaXN0ID0gY2FtcGFpZ25GaWx0ZXJMaXN0O1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkxpc3QgPSBbXTtcclxuICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkRhdGFGaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuY2FtcGFpZ25EYXRhRmlsdGVyTGlzdCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMucHJvamVjdFR5cGUpIHtcclxuICAgICAgdGhpcy5jcmVhdGVQcm9qZWN0RnJvbVRlbXBsYXRlID0gZmFsc2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNyZWF0ZVByb2plY3RGcm9tVGVtcGxhdGUgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgdGhpcy5pc0NhbXBhaWduID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX0NBTVBBSUdOXSk7XHJcbiAgICB0aGlzLnByb2plY3RDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFByb2plY3RDb25maWcoKTtcclxuICAgIHRoaXMuZW5hYmxlV29ya1dlZWsgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0aGlzLnByb2plY3RDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX1BST0pFQ1RfQ09ORklHLkVOQUJMRV9XT1JLX1dFRUtdKTtcclxuXHJcbiAgICB0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dKSA/ICcuKicgOlxyXG4gICAgICB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5OQU1FX1NUUklOR19QQVRURVJOXTtcclxuICAgIHRoaXMuZW5hYmxlRHVyYXRpb24gPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0aGlzLnByb2plY3RDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX1BST0pFQ1RfQ09ORklHLkVOQUJMRV9EVVJBVElPTl0pO1xyXG4gICAgdGhpcy5lbmFibGVEdXBsaWNhdGVOYW1lcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMucHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX0RVUExJQ0FURV9OQU1FU10pO1xyXG4gICAgaWYgKHRoaXMucm91dGVyLnVybC5pbmNsdWRlcygnL2FwcHMvbXBtL2NhbXBhaWduLycpKSB7XHJcbiAgICAgIHRoaXMuaXNDYW1wYWlnblByb2plY3QgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRBbGxDYXRlZ29yeU1ldGFkYXRhKClcclxuICAgICAgLnN1YnNjcmliZShjYXRlZ29yeURldGFpbHMgPT4ge1xyXG4gICAgICAgIHRoaXMuYWxsY2F0ZWdvcnlNZXRhZGF0YUxpc3QgPSBjYXRlZ29yeURldGFpbHMubWFwKGRhdGEgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHsgZGlzcGxheU5hbWU6IGRhdGEuTUVUQURBVEFfTkFNRSwgdmFsdWU6IGRhdGFbJ01QTV9DYXRlZ29yeV9NZXRhZGF0YS1pZCddLklkLCBuYW1lOiBkYXRhLk1FVEFEQVRBX05BTUUsIC4uLmRhdGEgfTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICBpZiAodGhpcy5jYW1wYWlnbklkKSB7XHJcbiAgICAgIHRoaXMuZ2V0Q2FtcGFpZ25CeUlkKHRoaXMuY2FtcGFpZ25JZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgLy8gMjAxXHJcbiAgICAvL3RoaXMuZ2V0Q2FtcGFpZ24oKS5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgIGlmICh0aGlzLmNyZWF0ZVByb2plY3RGcm9tVGVtcGxhdGUpIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgdGhpcy5wcm9qZWN0RnJvbVRlbXBsYXRlU2VydmljZS5nZXRBbGxUZW1wbGF0ZXModGhpcy50ZW1wbGF0ZVNlYXJjaENvbmRpdGlvbnMudGVtcGxhdGVTZWFyY2hDb25kaXRpb24sIHRoaXMudmlld0NvbmZpZylcclxuICAgICAgICAuc3Vic2NyaWJlKHRlbXBsYXRlT2JqID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBpZiAodGVtcGxhdGVPYmouYWxsVGVtcGxhdGVDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5hbGxUZW1wbGF0ZUxpc3QgPSB0ZW1wbGF0ZU9iai5hbGxUZW1wbGF0ZUxpc3Q7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ZXJGaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEluZGV4ZXJJZEJ5TWFwcGVyVmFsdWUoTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfTkFNRSk7XHJcbiAgICAgICAgICAgIHRoaXMuYWxsVGVtcGxhdGVMaXN0Lm1hcChkYXRhID0+IHtcclxuICAgICAgICAgICAgICBkYXRhLmRpc3BsYXlOYW1lID0gZGF0YVtpbmRleGVyRmllbGRdO1xyXG4gICAgICAgICAgICAgIGRhdGEubmFtZSA9IGRhdGFbaW5kZXhlckZpZWxkXTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbGl6ZVByb2plY3RUZW1wbGF0ZUZvcm0oKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICBtZXNzYWdlOiAnVGhlcmUgYXJlIG5vIHRlbXBsYXRlcycsXHJcbiAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GT1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlQ2FsbGJhY2tIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IHRlYW1zVmFsdWUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGZWlsZFZhbHVlQnlNYXBwZXJOYW1lKHRoaXMuc2VsZWN0ZWRQcm9qZWN0XHJcbiAgICAgICAgLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX1RFQU0pO1xyXG4gICAgICB0aGlzLnByb2plY3REZXBlbmRlbmN5LnNlbGVjdGVkT2JqZWN0ID0gdGhpcy5zZWxlY3RlZFByb2plY3Q7XHJcbiAgICAgIGlmICh0ZWFtc1ZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICB0aGlzLnByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLmdldE93bmVyTGlzdEJ5VGVhbSh0ZWFtc1ZhbHVlLCB0aGlzLmlzQ2FtcGFpZ25Qcm9qZWN0KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIHRoaXMucHJvamVjdE93bmVyTGlzdCA9IHJlc3BvbnNlO1xyXG4gICAgICAgICAgdGhpcy5pbml0aWFsaXplUHJvamVjdFRlbXBsYXRlRm9ybSgpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICB0aGlzLmluaXRpYWxpemVQcm9qZWN0VGVtcGxhdGVGb3JtKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5pbml0aWFsaXplUHJvamVjdFRlbXBsYXRlRm9ybSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAvLyB9KTtcclxuICB9XHJcblxyXG4gIGRhdGVGaWx0ZXI6IChkYXRlOiBEYXRlIHwgbnVsbCkgPT4gYm9vbGVhbiA9XHJcbiAgICAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IHtcclxuICAgICAgaWYgKHRoaXMuZW5hYmxlV29ya1dlZWspIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBkYXkgPSBkYXRlLmdldERheSgpO1xyXG4gICAgICAgIHJldHVybiBkYXkgIT09IDAgJiYgZGF5ICE9PSA2O1xyXG4gICAgICAgIC8vMCBtZWFucyBzdW5kYXlcclxuICAgICAgICAvLzYgbWVhbnMgc2F0dXJkYXlcclxuICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==