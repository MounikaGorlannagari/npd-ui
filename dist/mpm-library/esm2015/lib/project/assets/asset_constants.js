import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export const AssetConstants = {
    ASSET_KEYWORD: OTMMMPMDataTypes.ASSET,
    ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: [
                {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.PROJECT_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT EMPTY',
                    relational_operator_name: 'is not empty',
                },
                {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.IS_CLONED',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS EMPTY',
                    relational_operator_name: 'is empty',
                    relational_operator: 'or'
                }
            ]
        }
    },
    REFERENCE_ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.PROJECT_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT EMPTY',
                    relational_operator_name: 'is not empty',
                },
                {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.IS_REFERENCE_ASSET',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    relational_operator: 'and',
                    value: 'true'
                }]
        }
    },
    ADD_REFERENCE_ASSET_SEARCH_CONDITION: {
        search_condition_list: {
            search_condition: [{
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.PROJECT_ID',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS NOT EMPTY',
                    relational_operator_name: 'is not empty',
                },
                {
                    type: 'com.artesia.search.SearchScalarCondition',
                    metadata_field_id: 'MPM.ASSET.IS_REFERENCE_ASSET',
                    relational_operator_id: 'ARTESIA.OPERATOR.CHAR.IS',
                    relational_operator_name: 'is',
                    relational_operator: 'and',
                    value: 'false'
                }]
        }
    },
    ASSET_DETAILS_CONDITION: {
        search_condition_list: {
            search_condition: [{
                    type: IndexerDataTypes.STRING,
                    field_id: 'ITEM_ID',
                    relational_operator_id: MPMSearchOperators.IS,
                    relational_operator_name: MPMSearchOperatorNames.IS,
                    value: 'ASSET_PROJECT_ID'
                }, {
                    type: IndexerDataTypes.STRING,
                    field_id: 'ITEM_ID',
                    relational_operator_id: MPMSearchOperators.IS,
                    relational_operator_name: MPMSearchOperatorNames.IS,
                    value: 'ASSET_TASK_ID',
                    relational_operator: 'or'
                }, {
                    type: IndexerDataTypes.STRING,
                    field_id: 'ITEM_ID',
                    relational_operator_id: MPMSearchOperators.IS,
                    relational_operator_name: MPMSearchOperatorNames.IS,
                    value: 'ASSET_DELIVERABLE_ID',
                    relational_operator: 'or'
                }]
        }
    },
    REFERENCE_ASSET_DETAILS_CONDITION: {
        search_condition_list: {
            search_condition: [{
                    type: IndexerDataTypes.STRING,
                    field_id: 'ITEM_ID',
                    relational_operator_id: MPMSearchOperators.IS,
                    relational_operator_name: MPMSearchOperatorNames.IS,
                    value: 'ASSET_PROJECT_ID'
                }]
        }
    },
    ASSET_DETAILS_VIEW_CONFIG: [{
            type: 'PROJECT',
            content_type: 'MPM_PROJECT',
            group_display_name: 'Project Information'
        }, {
            type: 'TASK',
            content_type: 'MPM_TASK',
            group_display_name: 'Task Information'
        }, {
            type: 'DELIVERABLE',
            content_type: 'MPM_DELIVERABLE',
            group_display_name: 'Deliverable Information'
        }],
    REFERENCE_ASSET_DETAILS_VIEW_CONFIG: [{
            type: 'PROJECT',
            content_type: 'MPM_PROJECT',
            group_display_name: 'Project Information'
        }],
    ASSET_UPLOAD_FIELDS: [{
            id: 'MPM.ASSET.DELIVERABLE_ID',
            mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_ITEM_ID
        }, {
            id: 'MPM.ASSET.ASSET_STATUS',
            mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS
        }, {
            id: 'MPM.ASSET.TASK_ID',
            mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_TASK_ID
        }, {
            id: 'MPM.ASSET.PROJECT_ID',
            mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID
        }, {
            id: 'MPM.ASSET.DELIVERABLE_NAME',
            mapperName: MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_NAME
        }],
    REFERENCE_ASSET_UPLOAD_FIELDS: [
        {
            id: 'MPM.ASSET.PROJECT_ID',
            mapperName: MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ID
        } /* , {
        id: 'MPM.ASSET.DELIVERABLE_NAME',
        mapperName: MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME
    } */
    ],
    REFERENCE_TYPES_OBJECTS: {
        selected: {
            name: 'Project',
            value: 'project'
        },
        options: [{
                name: 'Project',
                value: 'project'
            }, {
                name: 'Deliverable1',
                value: 'deliverable'
            }]
    },
    DOWNLOAD_TYPES_OBJECTS: {
        selected: {
            name: 'Download As Individual Files',
            value: 'individualFiles'
        },
        options: [{
                name: 'Download As Individual Files',
                value: 'individualFiles'
            }, {
                name: 'Download As Zip Files',
                value: 'zip'
            }]
    },
    DOWNLOAD_TYPES: {
        ZIP: 'zip',
        INDIVIDUAL_FILES: 'individualFiles'
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXRfY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC9hc3NldHMvYXNzZXRfY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBR2pILE1BQU0sQ0FBQyxNQUFNLGNBQWMsR0FBRztJQUMxQixhQUFhLEVBQUUsZ0JBQWdCLENBQUMsS0FBSztJQUNyQyxzQkFBc0IsRUFBRTtRQUNwQixxQkFBcUIsRUFBRTtZQUNuQixnQkFBZ0IsRUFBRTtnQkFLYjtvQkFDRyxJQUFJLEVBQUUsMENBQTBDO29CQUNoRCxpQkFBaUIsRUFBRSxzQkFBc0I7b0JBQ3pDLHNCQUFzQixFQUFFLG9DQUFvQztvQkFDNUQsd0JBQXdCLEVBQUUsY0FBYztpQkFDM0M7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLDBDQUEwQztvQkFDaEQsaUJBQWlCLEVBQUUscUJBQXFCO29CQUN4QyxzQkFBc0IsRUFBRSxnQ0FBZ0M7b0JBQ3hELHdCQUF3QixFQUFFLFVBQVU7b0JBQ3BDLG1CQUFtQixFQUFFLElBQUk7aUJBQzVCO2FBQUM7U0FDVDtLQUNKO0lBQ0QsZ0NBQWdDLEVBQUU7UUFDOUIscUJBQXFCLEVBQUU7WUFDbkIsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDZixJQUFJLEVBQUUsMENBQTBDO29CQUNoRCxpQkFBaUIsRUFBRSxzQkFBc0I7b0JBQ3pDLHNCQUFzQixFQUFFLG9DQUFvQztvQkFDNUQsd0JBQXdCLEVBQUUsY0FBYztpQkFDM0M7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLDBDQUEwQztvQkFDaEQsaUJBQWlCLEVBQUUsOEJBQThCO29CQUNqRCxzQkFBc0IsRUFBRSwwQkFBMEI7b0JBQ2xELHdCQUF3QixFQUFFLElBQUk7b0JBQzlCLG1CQUFtQixFQUFFLEtBQUs7b0JBQzFCLEtBQUssRUFBRSxNQUFNO2lCQUNoQixDQUFDO1NBQ0w7S0FDSjtJQUNELG9DQUFvQyxFQUFFO1FBQ2xDLHFCQUFxQixFQUFFO1lBQ25CLGdCQUFnQixFQUFFLENBQUM7b0JBQ2YsSUFBSSxFQUFFLDBDQUEwQztvQkFDaEQsaUJBQWlCLEVBQUUsc0JBQXNCO29CQUN6QyxzQkFBc0IsRUFBRSxvQ0FBb0M7b0JBQzVELHdCQUF3QixFQUFFLGNBQWM7aUJBQzNDO2dCQUNEO29CQUNJLElBQUksRUFBRSwwQ0FBMEM7b0JBQ2hELGlCQUFpQixFQUFFLDhCQUE4QjtvQkFDakQsc0JBQXNCLEVBQUUsMEJBQTBCO29CQUNsRCx3QkFBd0IsRUFBRSxJQUFJO29CQUM5QixtQkFBbUIsRUFBRSxLQUFLO29CQUMxQixLQUFLLEVBQUUsT0FBTztpQkFDakIsQ0FBQztTQUNMO0tBQ0o7SUFDRCx1QkFBdUIsRUFBRTtRQUNyQixxQkFBcUIsRUFBRTtZQUNuQixnQkFBZ0IsRUFBRSxDQUFDO29CQUNmLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNO29CQUM3QixRQUFRLEVBQUUsU0FBUztvQkFDbkIsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtvQkFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtvQkFDbkQsS0FBSyxFQUFFLGtCQUFrQjtpQkFDNUIsRUFBRTtvQkFDQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsTUFBTTtvQkFDN0IsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLEVBQUU7b0JBQzdDLHdCQUF3QixFQUFFLHNCQUFzQixDQUFDLEVBQUU7b0JBQ25ELEtBQUssRUFBRSxlQUFlO29CQUN0QixtQkFBbUIsRUFBRSxJQUFJO2lCQUM1QixFQUFFO29CQUNDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNO29CQUM3QixRQUFRLEVBQUUsU0FBUztvQkFDbkIsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtvQkFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtvQkFDbkQsS0FBSyxFQUFFLHNCQUFzQjtvQkFDN0IsbUJBQW1CLEVBQUUsSUFBSTtpQkFDNUIsQ0FBQztTQUNMO0tBQ0o7SUFDRCxpQ0FBaUMsRUFBRTtRQUMvQixxQkFBcUIsRUFBRTtZQUNuQixnQkFBZ0IsRUFBRSxDQUFDO29CQUNmLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNO29CQUM3QixRQUFRLEVBQUUsU0FBUztvQkFDbkIsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtvQkFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtvQkFDbkQsS0FBSyxFQUFFLGtCQUFrQjtpQkFDNUIsQ0FBQztTQUNMO0tBQ0o7SUFDRCx5QkFBeUIsRUFBRSxDQUFDO1lBQ3hCLElBQUksRUFBRSxTQUFTO1lBQ2YsWUFBWSxFQUFFLGFBQWE7WUFDM0Isa0JBQWtCLEVBQUUscUJBQXFCO1NBQzVDLEVBQUU7WUFDQyxJQUFJLEVBQUUsTUFBTTtZQUNaLFlBQVksRUFBRSxVQUFVO1lBQ3hCLGtCQUFrQixFQUFFLGtCQUFrQjtTQUN6QyxFQUFFO1lBQ0MsSUFBSSxFQUFFLGFBQWE7WUFDbkIsWUFBWSxFQUFFLGlCQUFpQjtZQUMvQixrQkFBa0IsRUFBRSx5QkFBeUI7U0FDaEQsQ0FBQztJQUNGLG1DQUFtQyxFQUFFLENBQUM7WUFDbEMsSUFBSSxFQUFFLFNBQVM7WUFDZixZQUFZLEVBQUUsYUFBYTtZQUMzQixrQkFBa0IsRUFBRSxxQkFBcUI7U0FDNUMsQ0FBQztJQUNGLG1CQUFtQixFQUFFLENBQUM7WUFDbEIsRUFBRSxFQUFFLDBCQUEwQjtZQUM5QixVQUFVLEVBQUUsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CO1NBQzNFLEVBQUU7WUFDQyxFQUFFLEVBQUUsd0JBQXdCO1lBQzVCLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxrQkFBa0I7U0FDMUUsRUFBRTtZQUNDLEVBQUUsRUFBRSxtQkFBbUI7WUFDdkIsVUFBVSxFQUFFLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLG1CQUFtQjtTQUMzRSxFQUFFO1lBQ0MsRUFBRSxFQUFFLHNCQUFzQjtZQUMxQixVQUFVLEVBQUUsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCO1NBQzlFLEVBQUU7WUFDQyxFQUFFLEVBQUUsNEJBQTRCO1lBQ2hDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxnQkFBZ0I7U0FDeEUsQ0FBQztJQUNGLDZCQUE2QixFQUFFO1FBTXpCO1lBQ0UsRUFBRSxFQUFFLHNCQUFzQjtZQUMxQixVQUFVLEVBQUUsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsVUFBVTtTQUM5RCxDQUFBOzs7UUFHRDtLQUFDO0lBQ0wsdUJBQXVCLEVBQUU7UUFDckIsUUFBUSxFQUFFO1lBQ04sSUFBSSxFQUFFLFNBQVM7WUFDZixLQUFLLEVBQUUsU0FBUztTQUNuQjtRQUNELE9BQU8sRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxTQUFTO2FBQ25CLEVBQUU7Z0JBQ0MsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLEtBQUssRUFBRSxhQUFhO2FBQ3ZCLENBQUM7S0FDTDtJQUNELHNCQUFzQixFQUFFO1FBQ3BCLFFBQVEsRUFBRTtZQUNOLElBQUksRUFBRSw4QkFBOEI7WUFDcEMsS0FBSyxFQUFFLGlCQUFpQjtTQUMzQjtRQUNELE9BQU8sRUFBRSxDQUFDO2dCQUNOLElBQUksRUFBRSw4QkFBOEI7Z0JBQ3BDLEtBQUssRUFBRSxpQkFBaUI7YUFDM0IsRUFBRTtnQkFDQyxJQUFJLEVBQUUsdUJBQXVCO2dCQUM3QixLQUFLLEVBQUUsS0FBSzthQUNmLENBQUM7S0FDTDtJQUNELGNBQWMsRUFBRTtRQUNaLEdBQUcsRUFBRSxLQUFLO1FBQ1YsZ0JBQWdCLEVBQUUsaUJBQWlCO0tBQ3RDO0NBQ0osQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9UTU1NUE1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBJbmRleGVyRGF0YVR5cGVzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9JbmRleGVyRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcblxyXG5cclxuZXhwb3J0IGNvbnN0IEFzc2V0Q29uc3RhbnRzID0ge1xyXG4gICAgQVNTRVRfS0VZV09SRDogT1RNTU1QTURhdGFUeXBlcy5BU1NFVCxcclxuICAgIEFTU0VUX1NFQVJDSF9DT05ESVRJT046IHtcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogWy8qIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5zZWFyY2guU2VhcmNoU2NhbGFyQ29uZGl0aW9uJyxcclxuICAgICAgICAgICAgICAgIG1ldGFkYXRhX2ZpZWxkX2lkOiAnTVBNLkFTU0VULkRFTElWRVJBQkxFX0lEJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6ICdBUlRFU0lBLk9QRVJBVE9SLkNIQVIuSVMgTk9UIEVNUFRZJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogJ2lzIG5vdCBlbXB0eScsXHJcbiAgICAgICAgICAgIH0sICove1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5zZWFyY2guU2VhcmNoU2NhbGFyQ29uZGl0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9maWVsZF9pZDogJ01QTS5BU1NFVC5QUk9KRUNUX0lEJyxcclxuICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiAnQVJURVNJQS5PUEVSQVRPUi5DSEFSLklTIE5PVCBFTVBUWScsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiAnaXMgbm90IGVtcHR5JyxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLnNlYXJjaC5TZWFyY2hTY2FsYXJDb25kaXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhX2ZpZWxkX2lkOiAnTVBNLkFTU0VULklTX0NMT05FRCcsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogJ0FSVEVTSUEuT1BFUkFUT1IuQ0hBUi5JUyBFTVBUWScsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiAnaXMgZW1wdHknLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6ICdvcidcclxuICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIFJFRkVSRU5DRV9BU1NFVF9TRUFSQ0hfQ09ORElUSU9OOiB7XHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IFt7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEuc2VhcmNoLlNlYXJjaFNjYWxhckNvbmRpdGlvbicsXHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YV9maWVsZF9pZDogJ01QTS5BU1NFVC5QUk9KRUNUX0lEJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6ICdBUlRFU0lBLk9QRVJBVE9SLkNIQVIuSVMgTk9UIEVNUFRZJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogJ2lzIG5vdCBlbXB0eScsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5zZWFyY2guU2VhcmNoU2NhbGFyQ29uZGl0aW9uJyxcclxuICAgICAgICAgICAgICAgIG1ldGFkYXRhX2ZpZWxkX2lkOiAnTVBNLkFTU0VULklTX1JFRkVSRU5DRV9BU1NFVCcsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiAnQVJURVNJQS5PUEVSQVRPUi5DSEFSLklTJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogJ2lzJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6ICdhbmQnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgICAgICAgICB9XVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBBRERfUkVGRVJFTkNFX0FTU0VUX1NFQVJDSF9DT05ESVRJT046IHtcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogW3tcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5zZWFyY2guU2VhcmNoU2NhbGFyQ29uZGl0aW9uJyxcclxuICAgICAgICAgICAgICAgIG1ldGFkYXRhX2ZpZWxkX2lkOiAnTVBNLkFTU0VULlBST0pFQ1RfSUQnLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogJ0FSVEVTSUEuT1BFUkFUT1IuQ0hBUi5JUyBOT1QgRU1QVFknLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiAnaXMgbm90IGVtcHR5JyxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLnNlYXJjaC5TZWFyY2hTY2FsYXJDb25kaXRpb24nLFxyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFfZmllbGRfaWQ6ICdNUE0uQVNTRVQuSVNfUkVGRVJFTkNFX0FTU0VUJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6ICdBUlRFU0lBLk9QRVJBVE9SLkNIQVIuSVMnLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiAnaXMnLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogJ2FuZCcsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2ZhbHNlJ1xyXG4gICAgICAgICAgICB9XVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBBU1NFVF9ERVRBSUxTX0NPTkRJVElPTjoge1xyXG4gICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBbe1xyXG4gICAgICAgICAgICAgICAgdHlwZTogSW5kZXhlckRhdGFUeXBlcy5TVFJJTkcsXHJcbiAgICAgICAgICAgICAgICBmaWVsZF9pZDogJ0lURU1fSUQnLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogTVBNU2VhcmNoT3BlcmF0b3JzLklTLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdBU1NFVF9QUk9KRUNUX0lEJ1xyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBJbmRleGVyRGF0YVR5cGVzLlNUUklORyxcclxuICAgICAgICAgICAgICAgIGZpZWxkX2lkOiAnSVRFTV9JRCcsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ0FTU0VUX1RBU0tfSUQnLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogJ29yJ1xyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBJbmRleGVyRGF0YVR5cGVzLlNUUklORyxcclxuICAgICAgICAgICAgICAgIGZpZWxkX2lkOiAnSVRFTV9JRCcsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ0FTU0VUX0RFTElWRVJBQkxFX0lEJyxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6ICdvcidcclxuICAgICAgICAgICAgfV1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgUkVGRVJFTkNFX0FTU0VUX0RFVEFJTFNfQ09ORElUSU9OOiB7XHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IFt7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBJbmRleGVyRGF0YVR5cGVzLlNUUklORyxcclxuICAgICAgICAgICAgICAgIGZpZWxkX2lkOiAnSVRFTV9JRCcsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ0FTU0VUX1BST0pFQ1RfSUQnXHJcbiAgICAgICAgICAgIH1dXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIEFTU0VUX0RFVEFJTFNfVklFV19DT05GSUc6IFt7XHJcbiAgICAgICAgdHlwZTogJ1BST0pFQ1QnLFxyXG4gICAgICAgIGNvbnRlbnRfdHlwZTogJ01QTV9QUk9KRUNUJyxcclxuICAgICAgICBncm91cF9kaXNwbGF5X25hbWU6ICdQcm9qZWN0IEluZm9ybWF0aW9uJ1xyXG4gICAgfSwge1xyXG4gICAgICAgIHR5cGU6ICdUQVNLJyxcclxuICAgICAgICBjb250ZW50X3R5cGU6ICdNUE1fVEFTSycsXHJcbiAgICAgICAgZ3JvdXBfZGlzcGxheV9uYW1lOiAnVGFzayBJbmZvcm1hdGlvbidcclxuICAgIH0sIHtcclxuICAgICAgICB0eXBlOiAnREVMSVZFUkFCTEUnLFxyXG4gICAgICAgIGNvbnRlbnRfdHlwZTogJ01QTV9ERUxJVkVSQUJMRScsXHJcbiAgICAgICAgZ3JvdXBfZGlzcGxheV9uYW1lOiAnRGVsaXZlcmFibGUgSW5mb3JtYXRpb24nXHJcbiAgICB9XSxcclxuICAgIFJFRkVSRU5DRV9BU1NFVF9ERVRBSUxTX1ZJRVdfQ09ORklHOiBbe1xyXG4gICAgICAgIHR5cGU6ICdQUk9KRUNUJyxcclxuICAgICAgICBjb250ZW50X3R5cGU6ICdNUE1fUFJPSkVDVCcsXHJcbiAgICAgICAgZ3JvdXBfZGlzcGxheV9uYW1lOiAnUHJvamVjdCBJbmZvcm1hdGlvbidcclxuICAgIH1dLFxyXG4gICAgQVNTRVRfVVBMT0FEX0ZJRUxEUzogW3tcclxuICAgICAgICBpZDogJ01QTS5BU1NFVC5ERUxJVkVSQUJMRV9JRCcsXHJcbiAgICAgICAgbWFwcGVyTmFtZTogTVBNRmllbGRDb25zdGFudHMuREVMSVZFUkFCTEVfTVBNX0ZJRUxEUy5ERUxJVkVSQUJMRV9JVEVNX0lEXHJcbiAgICB9LCB7XHJcbiAgICAgICAgaWQ6ICdNUE0uQVNTRVQuQVNTRVRfU1RBVFVTJyxcclxuICAgICAgICBtYXBwZXJOYW1lOiBNUE1GaWVsZENvbnN0YW50cy5ERUxJVkVSQUJMRV9NUE1fRklFTERTLkRFTElWRVJBQkxFX1NUQVRVU1xyXG4gICAgfSwge1xyXG4gICAgICAgIGlkOiAnTVBNLkFTU0VULlRBU0tfSUQnLFxyXG4gICAgICAgIG1hcHBlck5hbWU6IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfVEFTS19JRFxyXG4gICAgfSwge1xyXG4gICAgICAgIGlkOiAnTVBNLkFTU0VULlBST0pFQ1RfSUQnLFxyXG4gICAgICAgIG1hcHBlck5hbWU6IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfUFJPSkVDVF9JRFxyXG4gICAgfSwge1xyXG4gICAgICAgIGlkOiAnTVBNLkFTU0VULkRFTElWRVJBQkxFX05BTUUnLFxyXG4gICAgICAgIG1hcHBlck5hbWU6IE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfTkFNRVxyXG4gICAgfV0sXHJcbiAgICBSRUZFUkVOQ0VfQVNTRVRfVVBMT0FEX0ZJRUxEUzogWy8qIHtcclxuICAgICAgICBpZDogJ01QTS5BU1NFVC5ERUxJVkVSQUJMRV9JRCcsXHJcbiAgICAgICAgbWFwcGVyTmFtZTogTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRFxyXG4gICAgfSwge1xyXG4gICAgICAgIGlkOiAnTVBNLkFTU0VULkFTU0VUX1NUQVRVUycsXHJcbiAgICAgICAgbWFwcGVyTmFtZTogTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfU1RBVFVTXHJcbiAgICB9LCAqLyB7XHJcbiAgICAgICAgICAgIGlkOiAnTVBNLkFTU0VULlBST0pFQ1RfSUQnLFxyXG4gICAgICAgICAgICBtYXBwZXJOYW1lOiBNUE1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JRFxyXG4gICAgICAgIH0vKiAsIHtcclxuICAgICAgICBpZDogJ01QTS5BU1NFVC5ERUxJVkVSQUJMRV9OQU1FJyxcclxuICAgICAgICBtYXBwZXJOYW1lOiBNUE1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9OQU1FXHJcbiAgICB9ICovXSxcclxuICAgIFJFRkVSRU5DRV9UWVBFU19PQkpFQ1RTOiB7XHJcbiAgICAgICAgc2VsZWN0ZWQ6IHtcclxuICAgICAgICAgICAgbmFtZTogJ1Byb2plY3QnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ3Byb2plY3QnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBvcHRpb25zOiBbe1xyXG4gICAgICAgICAgICBuYW1lOiAnUHJvamVjdCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiAncHJvamVjdCdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIG5hbWU6ICdEZWxpdmVyYWJsZTEnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2RlbGl2ZXJhYmxlJ1xyXG4gICAgICAgIH1dXHJcbiAgICB9LFxyXG4gICAgRE9XTkxPQURfVFlQRVNfT0JKRUNUUzoge1xyXG4gICAgICAgIHNlbGVjdGVkOiB7XHJcbiAgICAgICAgICAgIG5hbWU6ICdEb3dubG9hZCBBcyBJbmRpdmlkdWFsIEZpbGVzJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdpbmRpdmlkdWFsRmlsZXMnXHJcbiAgICAgICAgfSxcclxuICAgICAgICBvcHRpb25zOiBbe1xyXG4gICAgICAgICAgICBuYW1lOiAnRG93bmxvYWQgQXMgSW5kaXZpZHVhbCBGaWxlcycsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnaW5kaXZpZHVhbEZpbGVzJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAgbmFtZTogJ0Rvd25sb2FkIEFzIFppcCBGaWxlcycsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnemlwJ1xyXG4gICAgICAgIH1dXHJcbiAgICB9LFxyXG4gICAgRE9XTkxPQURfVFlQRVM6IHtcclxuICAgICAgICBaSVA6ICd6aXAnLFxyXG4gICAgICAgIElORElWSURVQUxfRklMRVM6ICdpbmRpdmlkdWFsRmlsZXMnXHJcbiAgICB9XHJcbn07XHJcbiJdfQ==