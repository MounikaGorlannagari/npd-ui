import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, isDevMode } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { SelectionModel } from '@angular/cdk/collections';
import { AssetService } from '../shared/services/asset.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { TaskConstants } from '../tasks/task.constants';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as acronui from '../../../lib/mpm-utils/auth/utility';
import { MPM_ROLES } from '../../mpm-utils/objects/Role';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { QdsService } from '../../upload/services/qds.service';
let AssetsComponent = class AssetsComponent {
    constructor(taskService, entityAppdefService, sharingService, assetService, loaderService, qdsService, notificationService, otmmService, appService) {
        this.taskService = taskService;
        this.entityAppdefService = entityAppdefService;
        this.sharingService = sharingService;
        this.assetService = assetService;
        this.loaderService = loaderService;
        this.qdsService = qdsService;
        this.notificationService = notificationService;
        this.otmmService = otmmService;
        this.appService = appService;
        this.notificationHandler = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.assetsCallBackHandler = new EventEmitter();
        this.selectedAssetsCallbackHandler = new EventEmitter();
        this.selectAllDeliverables = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.downloadAssetCallBackHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.OrderedDisplayableFields = new EventEmitter();
        this.openCreativeReviewHandler = new EventEmitter();
        this.skip = 0;
        this.top = 100;
        this.allSelectedAssets = [];
        this.allSelectedAssetsVal = false;
        this.selectedDeliverables = new SelectionModel(true, []);
    }
    getSelectedDeliverable(asset) {
        const selectedAsset = this.selectedDeliverables.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
        if (!selectedAsset) {
            this.selectedDeliverables.select(asset);
        }
        this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
    }
    getUnSelectedDeliverable(asset) {
        const selectedAsset = this.selectedDeliverables.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
        if (selectedAsset) {
            this.selectedDeliverables.deselect(selectedAsset);
        }
        this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
    }
    /* isAllSelected() {
        return this.selectedDeliverables.selected.length === this.totalAssetsCount;
    } */
    isAllSelected() {
        const selectedAssets = [];
        this.assetList.forEach(asset => {
            const selectedAsset = this.selectedDeliverables.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length === this.totalAssetsCount) {
            this.selectAllChecked = true;
        }
        return selectedAssets.length === this.totalAssetsCount ? true : false;
    }
    isPartialSelected() {
        const selectedAssets = [];
        this.assetList.forEach(asset => {
            const selectedAsset = this.selectedDeliverables.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length !== this.totalAssetsCount) {
            this.selectAllChecked = false;
        }
        return selectedAssets.length > 0 && selectedAssets.length !== this.totalAssetsCount ? true : false;
    }
    masterToggle() {
        this.selectAllChecked ? this.checkAllDeliverables() : this.uncheckAllDeliverables();
    }
    checkAllDeliverables() {
        this.selectAllDeliverables.emit(true);
    }
    uncheckAllDeliverables() {
        this.selectAllDeliverables.emit(false);
    }
    retrieveAssetData(asset) {
        this.assetDetailsCallBackHandler.next(asset);
    }
    downloadAsset(event) {
        this.downloadAssetCallBackHandler.next(event);
    }
    retrieveAssetVersionsData(asset) {
        this.assetVersionsCallBackHandler.next(asset);
    }
    retrieveAssetPreviewData(asset) {
        this.assetPreviewCallBackHandler.next(asset);
    }
    shareAsset(event) {
        this.shareAssetsCallbackHandler.next(event);
    }
    download(asset) {
        const assetURL = this.otmmBaseUrl + asset.master_content_info.url.slice(1);
        //this.assetService.initiateDownload(assetURL, this.asset.name);
        if (otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
            const downloadParams = {
                assetObjects: [{
                        id: asset.asset_id,
                        type: 'ORIGINAL'
                    }]
            };
            this.qdsService.inlineDownload(downloadParams);
        }
        else {
            const assetConfig = this.sharingService.getAssetConfig();
            const assetSize = asset.asset_content_info.master_content.content_size / 1024;
            if (assetSize < assetConfig.MIN_EXPORT_SIZE_HTTP) {
                this.assetService.initiateDownload(assetURL, asset.name);
            }
            else {
                const downloadType = asset.mime_type.split("/")[1];
                this.loaderService.show();
                this.otmmService.createExportJob([asset.asset_id], downloadType)
                    .subscribe(response => {
                    this.loaderService.hide();
                    if (response && response.export_job_handle && response.export_job_handle.export_response &&
                        response.export_job_handle.export_response.exportable_count &&
                        response.export_job_handle.export_response.exportable_count > 0) {
                        this.notificationService.info('Download has been initiated, Please check the download tray');
                    }
                }, error => {
                    this.loaderService.hide();
                });
            }
        }
        // this.downloadCallBackHandler.emit({ assetURL: assetURL, assetName: this.asset.name });
    }
    openCR(asset) {
        const appId = this.entityAppdefService.getApplicationID();
        const deliverableId = this.taskService.getProperty(asset, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        const taskId = this.taskService.getProperty(asset, TaskConstants.TASK_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        const assetId = asset.original_asset_id;
        const teamItemId = this.projectData.R_PO_TEAM['MPM_Teams-id'].Id;
        const crActions = this.sharingService.getCRActions();
        this.taskService.getTaskById(taskId).subscribe((task) => {
            this.taskData = task;
            this.taskName = this.taskData.NAME;
        });
        if (crActions && crActions.length > 0) {
            const crAction = crActions.find(el => el['MPM_Teams-id'].Id === teamItemId);
            if (crAction) {
                const userRoles = acronui.findObjectsByProp(crAction, 'MPM_Team_Role_Mapping');
                let isManager = false;
                userRoles.forEach(role => {
                    const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                    const mangerRoleObj = appRoles.find(appRole => {
                        return appRole.ROLE_NAME === MPM_ROLES.MANAGER;
                    });
                    if (mangerRoleObj) {
                        isManager = true;
                    }
                });
                this.assetService.GetCRIsViewer(true).subscribe(crResponse => {
                    this.crRole = crResponse.MPM_CR_Role;
                    this.loaderService.show();
                    this.getFileProofingSessionData(appId, deliverableId, assetId, this.crRole.ROLE_ID, 'true', this.taskName, asset)
                        .subscribe(crData => {
                        this.loaderService.hide();
                        this.crHandler.next(crData);
                    }, error => {
                        this.loaderService.hide();
                        this.notificationService.error('Something went wrong while opening Creative Review');
                    });
                });
            }
            else {
                this.notificationService.error('Something went wrong while opening Creative Review');
            }
        }
        else {
            this.notificationService.error('Something went wrong while opening Creative Review');
        }
    }
    getProperty(asset, property) {
        return this.taskService.getProperty(asset, property);
    }
    getFileProofingSessionData(appId, deliverableId, assetId, crRoleId, versioning, taskName, asset) {
        return new Observable(observer => {
            this.appService.getFileProofingSession(appId, deliverableId, assetId, taskName, crRoleId, versioning)
                .subscribe(response => {
                if (response && response.sessionDetails && response.sessionDetails.sessionUrl) {
                    this.loaderService.hide();
                    const crData = {
                        url: response.sessionDetails.sessionUrl,
                        headerInfos: [{
                                title: 'Name',
                                value: this.getProperty(asset, DeliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID),
                                type: 'string'
                            }]
                    };
                    console.log(crData);
                    observer.next(crData);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while opening Creative Review.');
                }
            }, error => {
                observer.error(error);
            });
        });
    }
    getOrderedDisplayableFields(columns) {
        this.OrderedDisplayableFields.next(columns);
    }
    ngOnChanges(changes) {
        if (changes && changes.assets && (changes.assets.previousValue !== changes.assets.currentValue)) {
            this.assetList = changes.assets.currentValue;
            this.totalAssetsCount = this.assetList.length;
        }
    }
    ngOnInit() {
        this.assetList = this.assets;
        this.selectAllChecked = false;
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.totalAssetsCount = this.assets.length;
        if (this.selectedAssetData && this.selectedAssetData.length > 0) {
            this.selectedAssetData.forEach(assetId => {
                const selectedAsset = this.allAssetData.find(asset => asset.asset_id === assetId);
                if (selectedAsset) {
                    this.selectedDeliverables.select(selectedAsset);
                }
            });
            if (this.selectedDeliverables.selected && this.selectedDeliverables.selected.length > 0) {
                this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
            }
        }
    }
};
AssetsComponent.ctorParameters = () => [
    { type: TaskService },
    { type: EntityAppDefService },
    { type: SharingService },
    { type: AssetService },
    { type: LoaderService },
    { type: QdsService },
    { type: NotificationService },
    { type: OTMMService },
    { type: AppService }
];
__decorate([
    Input()
], AssetsComponent.prototype, "assets", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "selectedAssetData", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "isCampaignAssetView", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "allAssetData", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "isAssetListView", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "assetDisplayableFields", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "assetConfig", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "assetCardFields", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "projectData", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "taskData", void 0);
__decorate([
    Input()
], AssetsComponent.prototype, "freezeCount", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "notificationHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "loadingHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "assetDetailsCallBackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "assetVersionsCallBackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "assetsCallBackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "selectedAssetsCallbackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "selectAllDeliverables", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "shareAssetsCallbackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "assetPreviewCallBackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "downloadAssetCallBackHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "crHandler", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "OrderedDisplayableFields", void 0);
__decorate([
    Output()
], AssetsComponent.prototype, "openCreativeReviewHandler", void 0);
AssetsComponent = __decorate([
    Component({
        selector: 'mpm-assets',
        template: "<div class=\"flex-row align-center\">\r\n    <div class=\"flex-row-item padding-left-10\" *ngIf=\"totalAssetsCount > 0 || assetList.length > 0\">\r\n        <!-- || assets.length > 0 -->\r\n        <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"selectedDeliverables.hasValue() && isAllSelected()\" [indeterminate]=\"selectedDeliverables.hasValue() && !isAllSelected()\"> Select all assets in this page\r\n        </mat-checkbox>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"assets-wrapper\">\r\n    <div class=\"assets\">\r\n        <mpm-asset-card *ngFor=\"let asset of assetList\" [asset]=\"asset\" [isVersion]=\"false\" [selectAllDeliverables]=\"selectAllDeliverables\" (assetVersionsCallBackHandler)=\"retrieveAssetVersionsData($event)\" (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\"\r\n            (selectedAssetsCallBackHandler)=\"getSelectedDeliverable(asset)\" (unSelectedAssetsCallBackHandler)=\"getUnSelectedDeliverable(asset)\" (shareAssetsCallbackHandler)=\"shareAsset($event)\" (assetPreviewCallBackHandler)=\"retrieveAssetPreviewData($event)\"\r\n            (downloadCallBackHandler)=\"downloadAsset($event)\" (openCreativeReview)=\"openCR($event)\"></mpm-asset-card>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"assets-wrapper\" *ngIf=\"isAssetListView\">\r\n    <div class=\"assets\">\r\n        <mpm-asset-list [assetConfig]=\"assetConfig\" (orderedDisplayableFields)=\"getOrderedDisplayableFields($event)\" [assets]=\"assets\" [taskData]=\"taskData\" [projectData]=\"projectData\" [assetDisplayableFields]=\"assetDisplayableFields\" [selectedAssetData]=\"selectedAssetData\"\r\n            [isVersion]=\"false\" [selectAllDeliverables]=\"selectAllDeliverables\" (shareAssetsCallbackHandler)=\"shareAsset($event)\" (openCreativeReview)=\"openCR($event)\"></mpm-asset-list>\r\n    </div>\r\n</div>",
        styles: [".align-center{align-items:center}.padding-left-10{padding-left:10px}.no-asset-found{text-align:center;font-weight:700}.assets-wrapper{padding:.5rem;position:relative;outline:0;display:flex;flex-flow:column;flex:1 0 0px;overflow-x:hidden;overflow-y:auto;height:calc(100vh - 256px);overflow:auto}.assets-wrapper .assets{display:flex;flex-wrap:wrap;margin:-.5rem;position:relative;flex-direction:row;flex-grow:1}.assets-wrapper mpm-asset-card{min-width:230px;box-sizing:border-box;position:relative;margin:.5rem;display:flex;flex-direction:column}"]
    })
], AssetsComponent);
export { AssetsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvYXNzZXRzL2Fzc2V0cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQWlCLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFFcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxLQUFLLE9BQU8sTUFBTSxxQ0FBcUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBUS9ELElBQWEsZUFBZSxHQUE1QixNQUFhLGVBQWU7SUF5Q3hCLFlBQ1csV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGNBQThCLEVBQzlCLFlBQTBCLEVBQzFCLGFBQTRCLEVBQzVCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixVQUFzQjtRQVJ0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQXBDdkIsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDekMsZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0RCxpQ0FBNEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZELDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDaEQsa0NBQTZCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN4RCwwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBQ3BELCtCQUEwQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckQsZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0RCxpQ0FBNEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZELGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BDLDZCQUF3QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkQsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUk5RCxTQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ1QsUUFBRyxHQUFHLEdBQUcsQ0FBQztRQUVWLHNCQUFpQixHQUFVLEVBQUUsQ0FBQztRQUM5Qix5QkFBb0IsR0FBRyxLQUFLLENBQUM7UUFDN0IseUJBQW9CLEdBQUcsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBZ0JoRCxDQUFDO0lBRUwsc0JBQXNCLENBQUMsS0FBVTtRQUM3QixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFILElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDaEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELHdCQUF3QixDQUFDLEtBQVU7UUFDL0IsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxSCxJQUFJLGFBQWEsRUFBRTtZQUNmLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDckQ7UUFDRCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7UUFFSTtJQUVKLGFBQWE7UUFDVCxNQUFNLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDM0IsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxSCxJQUFJLGFBQWEsRUFBRTtnQkFDZixjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3RDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7UUFDRCxPQUFPLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMxRSxDQUFDO0lBRUQsaUJBQWlCO1FBQ2IsTUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzNCLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUgsSUFBSSxhQUFhLEVBQUU7Z0JBQ2YsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN0QztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUNqRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkcsQ0FBQztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUN4RixDQUFDO0lBRUQsb0JBQW9CO1FBQ2hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELHNCQUFzQjtRQUNsQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUFLO1FBQ25CLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGFBQWEsQ0FBQyxLQUFLO1FBQ2YsSUFBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQseUJBQXlCLENBQUMsS0FBSztRQUMzQixJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxLQUFLO1FBQzFCLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQUs7UUFDVixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLGdFQUFnRTtRQUNoRSxJQUFJLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRTtZQUMxRCxNQUFNLGNBQWMsR0FBRztnQkFDbkIsWUFBWSxFQUFFLENBQUM7d0JBQ1gsRUFBRSxFQUFFLEtBQUssQ0FBQyxRQUFRO3dCQUNsQixJQUFJLEVBQUUsVUFBVTtxQkFDbkIsQ0FBQzthQUNMLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUNsRDthQUFNO1lBQ0gsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN6RCxNQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLFlBQVksR0FBQyxJQUFJLENBQUM7WUFDNUUsSUFBRyxTQUFTLEdBQUcsV0FBVyxDQUFDLG9CQUFvQixFQUFDO2dCQUM1QyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUQ7aUJBQUk7Z0JBQ0QsTUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLFlBQVksQ0FBQztxQkFDM0QsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsaUJBQWlCLElBQUksUUFBUSxDQUFDLGlCQUFpQixDQUFDLGVBQWU7d0JBQ3BGLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCO3dCQUMzRCxRQUFRLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixHQUFHLENBQUMsRUFBRTt3QkFDakUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO3FCQUNoRztnQkFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7b0JBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNKO1FBQ0QseUZBQXlGO0lBQzdGLENBQUM7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNSLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzFELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxvQkFBb0IsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuSSxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLDZCQUE2QixDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlHLE1BQU0sT0FBTyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztRQUN4QyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDakUsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUMsRUFBRTtZQUMvQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFBO1FBQ1IsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkMsTUFBTSxRQUFRLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVSxDQUFDLENBQUM7WUFDNUUsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUMvRSxJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3JCLE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7b0JBQ2xFLE1BQU0sYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQzFDLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDO29CQUNuRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLGFBQWEsRUFBRTt3QkFDZixTQUFTLEdBQUcsSUFBSSxDQUFDO3FCQUNwQjtnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQUU7b0JBQ3pELElBQUksQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLFdBQVcsQ0FBQztvQkFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEtBQUssRUFBRSxhQUFhLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLEtBQUssQ0FBQzt5QkFDM0csU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNoQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dCQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztvQkFDekYsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7YUFDeEY7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO1NBQ3hGO0lBQ0wsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLLEVBQUUsUUFBUTtRQUN2QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsMEJBQTBCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUMsS0FBSztRQUMxRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7aUJBQ2hHLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtvQkFDM0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsTUFBTSxNQUFNLEdBQUc7d0JBQ1gsR0FBRyxFQUFFLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVTt3QkFDdkMsV0FBVyxFQUFFLENBQUM7Z0NBQ1YsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLG9CQUFvQixDQUFDLGlDQUFpQyxDQUFDO2dDQUN0RixJQUFJLEVBQUUsUUFBUTs2QkFDakIsQ0FBQztxQkFDTCxDQUFDO29CQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO2lCQUN6RTtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMkJBQTJCLENBQUMsT0FBTztRQUMvQixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDOUIsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDN0YsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztZQUM3QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM3QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUN4RixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDM0MsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDckMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQyxDQUFDO2dCQUNsRixJQUFJLGFBQWEsRUFBRTtvQkFDZixJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUNuRDtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDckYsSUFBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQzthQUN0RTtTQUNKO0lBQ0wsQ0FBQztDQUNKLENBQUE7O1lBdE8yQixXQUFXO1lBQ0gsbUJBQW1CO1lBQ3hCLGNBQWM7WUFDaEIsWUFBWTtZQUNYLGFBQWE7WUFDaEIsVUFBVTtZQUNELG1CQUFtQjtZQUMzQixXQUFXO1lBQ1osVUFBVTs7QUFoRHhCO0lBQVIsS0FBSyxFQUFFOytDQUFRO0FBQ1A7SUFBUixLQUFLLEVBQUU7MERBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFOzREQUFxQjtBQUNwQjtJQUFSLEtBQUssRUFBRTtxREFBYztBQUNiO0lBQVIsS0FBSyxFQUFFO3dEQUEwQjtBQUN6QjtJQUFSLEtBQUssRUFBRTsrREFBd0I7QUFDdkI7SUFBUixLQUFLLEVBQUU7b0RBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTt3REFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7b0RBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTtpREFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO29EQUFhO0FBRVg7SUFBVCxNQUFNLEVBQUU7NERBQStDO0FBQzlDO0lBQVQsTUFBTSxFQUFFO3VEQUEwQztBQUN6QztJQUFULE1BQU0sRUFBRTtvRUFBdUQ7QUFDdEQ7SUFBVCxNQUFNLEVBQUU7cUVBQXdEO0FBQ3ZEO0lBQVQsTUFBTSxFQUFFOzhEQUFpRDtBQUNoRDtJQUFULE1BQU0sRUFBRTtzRUFBeUQ7QUFDeEQ7SUFBVCxNQUFNLEVBQUU7OERBQXFEO0FBQ3BEO0lBQVQsTUFBTSxFQUFFO21FQUFzRDtBQUNyRDtJQUFULE1BQU0sRUFBRTtvRUFBdUQ7QUFDdEQ7SUFBVCxNQUFNLEVBQUU7cUVBQXdEO0FBQ3ZEO0lBQVQsTUFBTSxFQUFFO2tEQUFxQztBQUNwQztJQUFULE1BQU0sRUFBRTtpRUFBb0Q7QUFDbkQ7SUFBVCxNQUFNLEVBQUU7a0VBQXFEO0FBMUJyRCxlQUFlO0lBTDNCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxZQUFZO1FBQ3RCLHczREFBc0M7O0tBRXpDLENBQUM7R0FDVyxlQUFlLENBZ1IzQjtTQWhSWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgU2ltcGxlQ2hhbmdlcywgaXNEZXZNb2RlLCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IEFzc2V0U2VydmljZSB9IGZyb20gJy4uL3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrcy90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrcy90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IE1QTV9ST0xFUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1JvbGUnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuLi8uLi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tYXNzZXRzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hc3NldHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYXNzZXRzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKSBhc3NldHM7XHJcbiAgICBASW5wdXQoKSBzZWxlY3RlZEFzc2V0RGF0YTtcclxuICAgIEBJbnB1dCgpIGlzQ2FtcGFpZ25Bc3NldFZpZXc7XHJcbiAgICBASW5wdXQoKSBhbGxBc3NldERhdGE7XHJcbiAgICBASW5wdXQoKSBpc0Fzc2V0TGlzdFZpZXc6IGJvb2xlYW47XHJcbiAgICBASW5wdXQoKSBhc3NldERpc3BsYXlhYmxlRmllbGRzO1xyXG4gICAgQElucHV0KCkgYXNzZXRDb25maWc7XHJcbiAgICBASW5wdXQoKSBhc3NldENhcmRGaWVsZHM7XHJcbiAgICBASW5wdXQoKSBwcm9qZWN0RGF0YTtcclxuICAgIEBJbnB1dCgpIHRhc2tEYXRhO1xyXG4gICAgQElucHV0KCkgZnJlZXplQ291bnQ7XHJcbiAgICBcclxuICAgIEBPdXRwdXQoKSBub3RpZmljYXRpb25IYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgbG9hZGluZ0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldFZlcnNpb25zQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgYXNzZXRzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWRBc3NldHNDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RBbGxEZWxpdmVyYWJsZXMgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldFByZXZpZXdDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBkb3dubG9hZEFzc2V0Q2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3JIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgT3JkZXJlZERpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3BlbkNyZWF0aXZlUmV2aWV3SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIGFzc2V0TGlzdDtcclxuICAgIHRvdGFsQXNzZXRzQ291bnQ7XHJcbiAgICBza2lwID0gMDtcclxuICAgIHRvcCA9IDEwMDtcclxuXHJcbiAgICBhbGxTZWxlY3RlZEFzc2V0czogYW55W10gPSBbXTtcclxuICAgIGFsbFNlbGVjdGVkQXNzZXRzVmFsID0gZmFsc2U7XHJcbiAgICBzZWxlY3RlZERlbGl2ZXJhYmxlcyA9IG5ldyBTZWxlY3Rpb25Nb2RlbCh0cnVlLCBbXSk7XHJcbiAgICBzZWxlY3RBbGxDaGVja2VkOiBib29sZWFuO1xyXG4gICAgdGFza05hbWU7XHJcbiAgICBjclJvbGU6IGFueTtcclxuICAgIG90bW1CYXNlVXJsOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcWRzU2VydmljZTogUWRzU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBnZXRTZWxlY3RlZERlbGl2ZXJhYmxlKGFzc2V0OiBhbnkpIHtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0ID0gdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3RlZC5maW5kKHNlbGVjdGVkQXNzZXQgPT4gc2VsZWN0ZWRBc3NldC5hc3NldF9pZCA9PT0gYXNzZXQuYXNzZXRfaWQpO1xyXG4gICAgICAgIGlmICghc2VsZWN0ZWRBc3NldCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdChhc3NldCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHNDYWxsYmFja0hhbmRsZXIubmV4dCh0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVblNlbGVjdGVkRGVsaXZlcmFibGUoYXNzZXQ6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdGVkLmZpbmQoc2VsZWN0ZWRBc3NldCA9PiBzZWxlY3RlZEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5kZXNlbGVjdChzZWxlY3RlZEFzc2V0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGlzQWxsU2VsZWN0ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMuc2VsZWN0ZWQubGVuZ3RoID09PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQ7XHJcbiAgICB9ICovXHJcblxyXG4gICAgaXNBbGxTZWxlY3RlZCgpIHtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuYXNzZXRMaXN0LmZvckVhY2goYXNzZXQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0ID0gdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3RlZC5maW5kKHNlbGVjdGVkQXNzZXQgPT4gc2VsZWN0ZWRBc3NldC5hc3NldF9pZCA9PT0gYXNzZXQuYXNzZXRfaWQpO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldCkge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRBc3NldHMucHVzaChzZWxlY3RlZEFzc2V0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZEFzc2V0cy5sZW5ndGggPT09IHRoaXMudG90YWxBc3NldHNDb3VudCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRBc3NldHMubGVuZ3RoID09PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNQYXJ0aWFsU2VsZWN0ZWQoKSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRBc3NldHMgPSBbXTtcclxuICAgICAgICB0aGlzLmFzc2V0TGlzdC5mb3JFYWNoKGFzc2V0ID0+IHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBc3NldCA9IHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMuc2VsZWN0ZWQuZmluZChzZWxlY3RlZEFzc2V0ID0+IHNlbGVjdGVkQXNzZXQuYXNzZXRfaWQgPT09IGFzc2V0LmFzc2V0X2lkKTtcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXQpIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkQXNzZXRzLnB1c2goc2VsZWN0ZWRBc3NldCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldHMubGVuZ3RoICE9PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RBbGxDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZEFzc2V0cy5sZW5ndGggPiAwICYmIHNlbGVjdGVkQXNzZXRzLmxlbmd0aCAhPT0gdGhpcy50b3RhbEFzc2V0c0NvdW50ID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG1hc3RlclRvZ2dsZSgpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPyB0aGlzLmNoZWNrQWxsRGVsaXZlcmFibGVzKCkgOiB0aGlzLnVuY2hlY2tBbGxEZWxpdmVyYWJsZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja0FsbERlbGl2ZXJhYmxlcygpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcy5lbWl0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHVuY2hlY2tBbGxEZWxpdmVyYWJsZXMoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMuZW1pdChmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0cmlldmVBc3NldERhdGEoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0RGV0YWlsc0NhbGxCYWNrSGFuZGxlci5uZXh0KGFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICBkb3dubG9hZEFzc2V0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5kb3dubG9hZEFzc2V0Q2FsbEJhY2tIYW5kbGVyLm5leHQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHJpZXZlQXNzZXRWZXJzaW9uc0RhdGEoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0VmVyc2lvbnNDYWxsQmFja0hhbmRsZXIubmV4dChhc3NldCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0cmlldmVBc3NldFByZXZpZXdEYXRhKGFzc2V0KSB7XHJcbiAgICAgICAgdGhpcy5hc3NldFByZXZpZXdDYWxsQmFja0hhbmRsZXIubmV4dChhc3NldCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hhcmVBc3NldChldmVudCkge1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXIubmV4dChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZG93bmxvYWQoYXNzZXQpIHtcclxuICAgICAgICBjb25zdCBhc3NldFVSTCA9IHRoaXMub3RtbUJhc2VVcmwgKyBhc3NldC5tYXN0ZXJfY29udGVudF9pbmZvLnVybC5zbGljZSgxKTtcclxuICAgICAgICAvL3RoaXMuYXNzZXRTZXJ2aWNlLmluaXRpYXRlRG93bmxvYWQoYXNzZXRVUkwsIHRoaXMuYXNzZXQubmFtZSk7XHJcbiAgICAgICAgaWYgKG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmlzUURTVXBsb2FkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRvd25sb2FkUGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgYXNzZXRPYmplY3RzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBhc3NldC5hc3NldF9pZCxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnT1JJR0lOQUwnXHJcbiAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLnFkc1NlcnZpY2UuaW5saW5lRG93bmxvYWQoZG93bmxvYWRQYXJhbXMpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFzc2V0Q29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBc3NldENvbmZpZygpO1xyXG4gICAgICAgICAgICBjb25zdCBhc3NldFNpemUgPSBhc3NldC5hc3NldF9jb250ZW50X2luZm8ubWFzdGVyX2NvbnRlbnQuY29udGVudF9zaXplLzEwMjQ7XHJcbiAgICAgICAgICAgIGlmKGFzc2V0U2l6ZSA8IGFzc2V0Q29uZmlnLk1JTl9FWFBPUlRfU0laRV9IVFRQKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXNzZXRTZXJ2aWNlLmluaXRpYXRlRG93bmxvYWQoYXNzZXRVUkwsIGFzc2V0Lm5hbWUpO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRvd25sb2FkVHlwZSA9IGFzc2V0Lm1pbWVfdHlwZS5zcGxpdChcIi9cIilbMV07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jcmVhdGVFeHBvcnRKb2IoW2Fzc2V0LmFzc2V0X2lkXSwgZG93bmxvYWRUeXBlKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmV4cG9ydF9qb2JfaGFuZGxlICYmIHJlc3BvbnNlLmV4cG9ydF9qb2JfaGFuZGxlLmV4cG9ydF9yZXNwb25zZSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5leHBvcnRfam9iX2hhbmRsZS5leHBvcnRfcmVzcG9uc2UuZXhwb3J0YWJsZV9jb3VudCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5leHBvcnRfam9iX2hhbmRsZS5leHBvcnRfcmVzcG9uc2UuZXhwb3J0YWJsZV9jb3VudCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ0Rvd25sb2FkIGhhcyBiZWVuIGluaXRpYXRlZCwgUGxlYXNlIGNoZWNrIHRoZSBkb3dubG9hZCB0cmF5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0aGlzLmRvd25sb2FkQ2FsbEJhY2tIYW5kbGVyLmVtaXQoeyBhc3NldFVSTDogYXNzZXRVUkwsIGFzc2V0TmFtZTogdGhpcy5hc3NldC5uYW1lIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5DUihhc3NldCkge1xyXG4gICAgICAgIGNvbnN0IGFwcElkID0gdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldEFwcGxpY2F0aW9uSUQoKTtcclxuICAgICAgICBjb25zdCBkZWxpdmVyYWJsZUlkID0gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eShhc3NldCwgRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfSVRFTV9JRF9NRVRBREFUQUZJRUxEX0lEKS5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgIGNvbnN0IHRhc2tJZCA9IHRoaXMudGFza1NlcnZpY2UuZ2V0UHJvcGVydHkoYXNzZXQsIFRhc2tDb25zdGFudHMuVEFTS19JVEVNX0lEX01FVEFEQVRBRklFTERfSUQpLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgY29uc3QgYXNzZXRJZCA9IGFzc2V0Lm9yaWdpbmFsX2Fzc2V0X2lkO1xyXG4gICAgICAgIGNvbnN0IHRlYW1JdGVtSWQgPSB0aGlzLnByb2plY3REYXRhLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQ7XHJcbiAgICAgICAgY29uc3QgY3JBY3Rpb25zID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDUkFjdGlvbnMoKTtcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmdldFRhc2tCeUlkKHRhc2tJZCkuc3Vic2NyaWJlKCh0YXNrKT0+e1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRGF0YSA9IHRhc2s7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tOYW1lID0gdGhpcy50YXNrRGF0YS5OQU1FO1xyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgaWYgKGNyQWN0aW9ucyAmJiBjckFjdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBjckFjdGlvbiA9IGNyQWN0aW9ucy5maW5kKGVsID0+IGVsWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGVhbUl0ZW1JZCk7XHJcbiAgICAgICAgICAgIGlmIChjckFjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXNlclJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChjckFjdGlvbiwgJ01QTV9UZWFtX1JvbGVfTWFwcGluZycpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGlzTWFuYWdlciA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdXNlclJvbGVzLmZvckVhY2gocm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJvbGUsICdNUE1fQVBQX1JvbGVzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWFuZ2VyUm9sZU9iaiA9IGFwcFJvbGVzLmZpbmQoYXBwUm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcHBSb2xlLlJPTEVfTkFNRSA9PT0gTVBNX1JPTEVTLk1BTkFHRVI7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG1hbmdlclJvbGVPYmopIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNNYW5hZ2VyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmFzc2V0U2VydmljZS5HZXRDUklzVmlld2VyKHRydWUpLnN1YnNjcmliZShjclJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNyUm9sZSA9IGNyUmVzcG9uc2UuTVBNX0NSX1JvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEZpbGVQcm9vZmluZ1Nlc3Npb25EYXRhKGFwcElkLCBkZWxpdmVyYWJsZUlkLCBhc3NldElkLCB0aGlzLmNyUm9sZS5ST0xFX0lELCAndHJ1ZScsIHRoaXMudGFza05hbWUsYXNzZXQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JEYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNySGFuZGxlci5uZXh0KGNyRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIG9wZW5pbmcgQ3JlYXRpdmUgUmV2aWV3Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIG9wZW5pbmcgQ3JlYXRpdmUgUmV2aWV3Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIG9wZW5pbmcgQ3JlYXRpdmUgUmV2aWV3Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KGFzc2V0LCBwcm9wZXJ0eSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRhc2tTZXJ2aWNlLmdldFByb3BlcnR5KGFzc2V0LCBwcm9wZXJ0eSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmlsZVByb29maW5nU2Vzc2lvbkRhdGEoYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIGNyUm9sZUlkLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSxhc3NldCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEZpbGVQcm9vZmluZ1Nlc3Npb24oYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIHRhc2tOYW1lLCBjclJvbGVJZCwgdmVyc2lvbmluZylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5zZXNzaW9uRGV0YWlscyAmJiByZXNwb25zZS5zZXNzaW9uRGV0YWlscy5zZXNzaW9uVXJsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNyRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogcmVzcG9uc2Uuc2Vzc2lvbkRldGFpbHMuc2Vzc2lvblVybCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlckluZm9zOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnTmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0UHJvcGVydHkoYXNzZXQsIERlbGl2ZXJhYmxlQ29uc3RhbnRzLkRFTElWRVJBQkxFX05BTUVfTUVUQURBVEFGSUVMRF9JRCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N0cmluZydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNyRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoY3JEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgb3BlbmluZyBDcmVhdGl2ZSBSZXZpZXcuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE9yZGVyZWREaXNwbGF5YWJsZUZpZWxkcyhjb2x1bW5zKXtcclxuICAgICAgICB0aGlzLk9yZGVyZWREaXNwbGF5YWJsZUZpZWxkcy5uZXh0KGNvbHVtbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoY2hhbmdlcyAmJiBjaGFuZ2VzLmFzc2V0cyAmJiAoY2hhbmdlcy5hc3NldHMucHJldmlvdXNWYWx1ZSAhPT0gY2hhbmdlcy5hc3NldHMuY3VycmVudFZhbHVlKSkge1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0TGlzdCA9IGNoYW5nZXMuYXNzZXRzLmN1cnJlbnRWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy50b3RhbEFzc2V0c0NvdW50ID0gdGhpcy5hc3NldExpc3QubGVuZ3RoO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmFzc2V0TGlzdCA9IHRoaXMuYXNzZXRzO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0QWxsQ2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMub3RtbUJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPSB0aGlzLmFzc2V0cy5sZW5ndGg7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRBc3NldERhdGEgJiYgdGhpcy5zZWxlY3RlZEFzc2V0RGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldERhdGEuZm9yRWFjaChhc3NldElkID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLmFsbEFzc2V0RGF0YS5maW5kKGFzc2V0ID0+IGFzc2V0LmFzc2V0X2lkID09PSBhc3NldElkKTtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZEFzc2V0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3Qoc2VsZWN0ZWRBc3NldCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3RlZCAmJiB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdGVkLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHNDYWxsYmFja0hhbmRsZXIubmV4dCh0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=