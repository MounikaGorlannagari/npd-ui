import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let ItemCountComponent = class ItemCountComponent {
    constructor() { }
    ngOnInit() {
    }
};
__decorate([
    Input()
], ItemCountComponent.prototype, "name", void 0);
__decorate([
    Input()
], ItemCountComponent.prototype, "count", void 0);
ItemCountComponent = __decorate([
    Component({
        selector: 'mpm-item-count',
        template: "<span class=\"task-count flex-row-item\">{{name}} ({{count}})</span>",
        styles: [""]
    })
], ItemCountComponent);
export { ItemCountComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1jb3VudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2l0ZW0tY291bnQvaXRlbS1jb3VudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBT3pELElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBSTdCLGdCQUFnQixDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDO0NBRUYsQ0FBQTtBQVJVO0lBQVIsS0FBSyxFQUFFO2dEQUFNO0FBQ0w7SUFBUixLQUFLLEVBQUU7aURBQU87QUFGSixrQkFBa0I7SUFMOUIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGdCQUFnQjtRQUMxQixnRkFBMEM7O0tBRTNDLENBQUM7R0FDVyxrQkFBa0IsQ0FTOUI7U0FUWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWl0ZW0tY291bnQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9pdGVtLWNvdW50LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9pdGVtLWNvdW50LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEl0ZW1Db3VudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgbmFtZTtcclxuICBASW5wdXQoKSBjb3VudDtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=