import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { ProjectConstant } from '../../project-overview/project.constants';
import { Observable, forkJoin } from 'rxjs';
import { TaskConstants } from '../task.constants';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { UtilService } from '../../../mpm-utils/services/util.service';
import * as acronui from '../../../mpm-utils/auth/utility';
import { TaskService } from '../task.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { EntityAppDefService } from '../../../mpm-utils/services/entity.appdef.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { LoaderService } from '../../../loader/loader.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { SaveOptionConstant } from '../../../shared/constants/save-options.constants';
import { ProjectUtilService } from '../../shared/services/project-util.service';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { map, startWith, distinctUntilChanged } from 'rxjs/operators';
import { NotificationService } from '../../../notification/notification.service';
let TaskCreationComponent = class TaskCreationComponent {
    constructor(appService, sharingService, adapter, utilService, taskService, entityAppdefService, loaderService, dialog, projectUtilService, commentsUtilService, notificationService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.adapter = adapter;
        this.utilService = utilService;
        this.taskService = taskService;
        this.entityAppdefService = entityAppdefService;
        this.loaderService = loaderService;
        this.dialog = dialog;
        this.projectUtilService = projectUtilService;
        this.commentsUtilService = commentsUtilService;
        this.notificationService = notificationService;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.isInvalidTaskOperation = false;
        this.ownerProjectCount = false;
        this.ownerProjectCountMessage = '';
        this.isZeroOwnerProjectCount = false;
        this.disableTaskSave = false;
        this.existingTaskTypeDetails = null;
        // existingAssignmentTypeDetails = null;
        this.taskConstants = TaskConstants;
        this.savedReviewers = [];
        this.allReviewers = [];
        // reviewers = [];
        this.deliverableReviewers = [];
        // reviewerDetails = [];
        this.reviewerStatusData = [];
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = false;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.newReviewers = [];
        this.removedReviewers = [];
        this.isPM = false;
        this.saveOptions = SaveOptionConstant.taskSaveOptions;
        this.SELECTED_SAVE_OPTION = {
            name: 'Save',
            value: 'SAVE'
        };
        this.DurationType = [
            { value: 'days', viewValue: 'Days' },
            { value: 'weeks', viewValue: 'Weeks' }
        ];
        this.enableDuration = false;
        this.enableWorkWeek = false;
        this.keyRestriction = {
            NUMBER: [46, 69, 186, 187, 188, 107]
        };
        this.dateFilter = (date) => {
            if (this.enableWorkWeek) {
                return true;
            }
            else {
                const day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
    }
    filterReviewers(value) {
        if (value && typeof value === 'string' && value.trim() !== '') {
            const filterValue = value.toLowerCase();
            return this.allReviewers.filter(reviewer => reviewer.value.toLowerCase().indexOf(filterValue) === 0);
        }
    }
    restrictKeysOnType(event, datatype) {
        if (this.keyRestriction[datatype] && this.keyRestriction[datatype].includes(event.keyCode)) {
            event.preventDefault();
        }
    }
    selected(event) {
        const reviewerIndex = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === event.option.value);
        const reviewer = this.allReviewers.find(reviewerData => reviewerData.value === event.option.value);
        if (this.taskForm.value.deliverableApprover &&
            event.option.value === this.taskForm.value.deliverableApprover.value) {
            this.notificationService.error(reviewer.displayName + ' is already the Approver');
        }
        else if (reviewerIndex > -1) {
            this.notificationService.error(reviewer.displayName + ' is already selected');
        }
        else {
            this.savedReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            this.newReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            const removeReviewerIndex = this.removedReviewers.findIndex(removedReviewer => removedReviewer.value === event.option.value);
            if (removeReviewerIndex >= 0) {
                this.removedReviewers.splice(removeReviewerIndex, 1);
            }
            this.reviewerStatusData.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName,
                isCompleted: false
            });
            this.reviewerInput.nativeElement.value = '';
            this.taskForm.controls.deliverableReviewers.setValue(null);
        }
    }
    addReviewer(event) {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            if (input) {
                input.value = '';
            }
            this.taskForm.controls.deliverableReviewers.setValue(null);
        }
    }
    onRemoveReviewer(reviewerValue) {
        const index = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === reviewerValue);
        const newReviewerIndex = this.newReviewers.findIndex(newReviewer => newReviewer.value === reviewerValue);
        const reviewerIndex = this.reviewerStatusData.findIndex(reviewerData => reviewerData.value === reviewerValue);
        const reviewer = this.allReviewers.find(reviewerData => reviewerData.value === reviewerValue);
        this.removedReviewers.push({
            name: reviewer.displayName,
            value: reviewer.value,
            displayName: reviewer.displayName
        });
        if (index >= 0) {
            this.taskForm.markAsDirty();
            this.savedReviewers.splice(index, 1);
        }
        if (reviewerIndex >= 0) {
            this.reviewerStatusData.splice(reviewerIndex, 1);
        }
        if (newReviewerIndex >= 0) {
            this.newReviewers.splice(newReviewerIndex, 1);
        }
    }
    getProjectDetails(projectObj) {
        return new Observable(observer => {
            if (projectObj && projectObj['Project-id'] && projectObj['Project-id'].Id) {
                if (projectObj.R_PO_CATEGORY && projectObj.R_PO_CATEGORY['MPM_Category-id']) {
                    this.taskModalConfig.categoryId = projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
                }
                if (projectObj.R_PO_TEAM && projectObj.R_PO_TEAM['MPM_Teams-id']) {
                    this.taskModalConfig.teamId = projectObj.R_PO_TEAM['MPM_Teams-id'].Id;
                }
                this.taskModalConfig.selectedProject = projectObj;
                const currentUSerID = this.sharingService.getCurrentUserItemID();
                if (currentUSerID && this.taskModalConfig.selectedProject &&
                    currentUSerID === this.taskModalConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    this.taskModalConfig.isProjectOwner = true;
                }
                observer.next(true);
                observer.complete();
            }
        });
    }
    getPriorities() {
        return new Observable(observer => {
            /*  this.entityAppdefService.getPriorities(MPM_LEVELS.TASK)
                 .subscribe(priorityResponse => {
                     if (priorityResponse) {
                         if (!Array.isArray(priorityResponse)) {
                             priorityResponse = [priorityResponse];
                         }
                         this.taskModalConfig.taskPriorityList = priorityResponse;
                     } else {
                         this.taskModalConfig.taskPriorityList = [];
                     }
                     observer.next(true);
                     observer.complete();
                 }, () => {
                     const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                     this.notificationHandler.next(eventData);
                     observer.next(false);
                     observer.complete();
                 }); */
            let priority = this.entityAppdefService.getPriorities(MPM_LEVELS.TASK);
            // .subscribe(priorityResponse => {
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                this.taskModalConfig.taskPriorityList = priority;
            }
            else {
                this.taskModalConfig.taskPriorityList = [];
            }
            observer.next(true);
            observer.complete();
            /*  }, () => {
                 const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                 this.notificationHandler.next(eventData);
                 observer.next(false);
                 observer.complete();
             }); */
        });
    }
    getStatus() {
        return new Observable(observer => {
            this.entityAppdefService.getStatusByCategoryLevel(MPM_LEVELS.TASK)
                .subscribe(statusResponse => {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    this.taskModalConfig.taskStatusList = statusResponse;
                }
                else {
                    this.taskModalConfig.taskStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getInitialStatus() {
        return new Observable(observer => {
            this.entityAppdefService.getStatus(MPM_LEVELS.TASK, StatusTypes.INITIAL)
                .subscribe(statusResponse => {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    this.taskModalConfig.taskInitialStatusList = statusResponse;
                }
                else {
                    this.taskModalConfig.taskInitialStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getAllRoles() {
        const getRequestObject = {
            teamID: this.taskModalConfig.teamId,
            isApproverTask: this.taskConfig.isApprovalTask,
            isMemberTask: !this.taskConfig.isApprovalTask
        };
        return new Observable(observer => {
            this.loaderService.show();
            this.appService.getTeamRoles(getRequestObject)
                .subscribe(response => {
                if (response && response.Teams && response.Teams.MPM_Teams && response.Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    response = response.Teams.MPM_Teams;
                    if (!Array.isArray(response.MPM_Team_Role_Mapping)) {
                        response.MPM_Team_Role_Mapping = [response.MPM_Team_Role_Mapping];
                    }
                    let roleList;
                    roleList = response.MPM_Team_Role_Mapping;
                    const roles = [];
                    if (roleList && roleList.length && roleList.length > 0) {
                        roleList.map(role => {
                            roles.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    this.teamRolesOptions = roles;
                    this.taskModalConfig.taskRoleConfig.filterOptions = this.teamRolesOptions;
                }
                else {
                    this.teamRolesOptions = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getReviewerById(taskUserID) {
        return new Observable(observer => {
            if (!this.utilService.isNullOrEmpty(taskUserID)) {
                const parameters = {
                    userId: taskUserID
                };
                this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    const eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    }
    getDeliverableReviewByTaskId(TaskId) {
        return new Observable(observer => {
            this.appService.getDeliverableReviewByTaskID(TaskId)
                .subscribe(response => {
                if (response && response.DeliverableReview) {
                    const reviewersList = acronui.findObjectsByProp(response, 'DeliverableReview');
                    if (reviewersList && reviewersList.length && reviewersList.length > 0) {
                        reviewersList.forEach(reviewer => {
                            if (!this.utilService.isNullOrEmpty(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id)) {
                                this.getReviewerById(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id).subscribe(reviewerData => {
                                    if (reviewerData) {
                                        const hasReviewerData = this.savedReviewers.find(savedReviewer => savedReviewer.value === reviewerData.userCN);
                                        if (!hasReviewerData) {
                                            this.savedReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                            this.deliverableReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                        }
                                    }
                                    this.entityAppdefService.getStatusById(reviewer.R_PO_REVIEW_STATUS['MPM_Status-id'].Id).subscribe(statusResponse => {
                                        const isReviewerCompleted = statusResponse.STATUS_LEVEL === 'FINAL' ? true : false;
                                        const reviewerIndex = this.reviewerStatusData.findIndex(data => data.value === reviewerData.userCN);
                                        if (reviewerIndex >= 0) {
                                            this.reviewerStatusData[reviewerIndex].isCompleted = isReviewerCompleted;
                                        }
                                        else {
                                            this.reviewerStatusData.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName,
                                                isCompleted: isReviewerCompleted
                                            });
                                        }
                                    });
                                    observer.next(this.savedReviewers);
                                    observer.complete();
                                });
                            }
                            else {
                                observer.next('');
                                observer.complete();
                            }
                        });
                    }
                    else {
                        observer.next('');
                        observer.complete();
                    }
                }
                else {
                    observer.next('');
                    observer.complete();
                }
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Deliverable Review Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getReviewers() {
        return new Observable(observer => {
            if (this.taskConfig.isApprovalTask) {
                let getReviewerUserRequest = {};
                getReviewerUserRequest = {
                    allocationType: TaskConstants.ALLOCATION_TYPE_USER,
                    roleDN: '',
                    teamID: this.taskModalConfig.teamId,
                    isApproveTask: true,
                    isUploadTask: false,
                    isReview: true
                };
                this.appService.getUsersForTeam(getReviewerUserRequest)
                    .subscribe(response => {
                    if (response && response.users && response.users.user) {
                        const userList = acronui.findObjectsByProp(response, 'user');
                        const users = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(user => {
                                const fieldObj = users.find(e => e.value === user.dn);
                                if (!fieldObj) {
                                    users.push({
                                        name: user.dn,
                                        value: user.cn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        this.allReviewers = users;
                    }
                    else {
                        this.allReviewers = [];
                    }
                    // this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, () => {
                    const eventData = { message: 'Something went wrong while fetching Reviewer Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    }
    getUsersByRole() {
        return new Observable(observer => {
            this.loaderService.show();
            let getUserByRoleRequest = {};
            if (this.taskForm) {
                const taskFormValue = this.taskForm.getRawValue();
                getUserByRoleRequest = {
                    allocationType: taskFormValue.allocation,
                    roleDN: taskFormValue.allocation === TaskConstants.ALLOCATION_TYPE_ROLE ? taskFormValue.role.value : '',
                    teamID: taskFormValue.allocation === TaskConstants.ALLOCATION_TYPE_USER ? this.taskModalConfig.teamId : '',
                    isApproveTask: this.taskConfig.isApprovalTask,
                    isUploadTask: !this.taskConfig.isApprovalTask
                };
            }
            else if (this.taskModalConfig.selectedTask) {
                const roleId = this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE && this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id']
                    ? this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                const role = this.formRoleValue(roleId);
                getUserByRoleRequest = {
                    allocationType: this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE,
                    roleDN: this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE === TaskConstants.ALLOCATION_TYPE_ROLE ? (role ? role.value : '') : '',
                    teamID: this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE === TaskConstants.ALLOCATION_TYPE_USER ? this.taskModalConfig.teamId : '',
                    isApproveTask: this.taskConfig.isApprovalTask,
                    isUploadTask: !this.taskConfig.isApprovalTask
                };
            }
            else {
                getUserByRoleRequest = {
                    allocationType: TaskConstants.ALLOCATION_TYPE_USER,
                    roleDN: '',
                    teamID: this.taskModalConfig.teamId,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(response => {
                if (response && response.users && response.users.user) {
                    const userList = acronui.findObjectsByProp(response, 'user');
                    const users = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.map(user => {
                            const fieldObj = users.find(e => e.value === user.dn);
                            if (!fieldObj) {
                                users.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    this.teamRoleUserOptions = users;
                    this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                    this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
                }
                else {
                    this.teamRoleUserOptions = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getTaskTypeDetails(taskTypeId) {
        this.existingTaskTypeDetails = null;
        return new Observable(observer => {
            const params = {
                'Task_Type-id': {
                    Id: taskTypeId
                }
            };
            this.appService.invokeRequest('http://schemas/AcheronMPMCore/Task_Type/operations', 'ReadTask_Type', params).subscribe(tasktypeDetailResponse => {
                const taskTypeDetails = acronui.findObjectsByProp(tasktypeDetailResponse, 'Task_Type');
                if (Array.isArray(taskTypeDetails) && taskTypeDetails.length > 0) {
                    this.existingTaskTypeDetails = taskTypeDetails[0];
                }
                else {
                    const eventData = { message: 'No Task Type Details found', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                }
                observer.next(tasktypeDetailResponse);
                observer.complete();
            }, fail => {
                const eventData = { message: 'Something went wrong while fetching Task Type', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.error(fail);
            });
        });
    }
    /*  getAssignmentTypeDetails(assignmentTypeId: string) {
         this.existingAssignmentTypeDetails = null;
         return new Observable(
             observer => {
                 const params = {
                     'Assignment_Type-id': {
                         Id: assignmentTypeId
                     }
                 };
                 this.appService.invokeRequest('http://schemas/AcheronMPMCore/Assignment_Type/operations', 'ReadAssignment_Type', params).subscribe(
                     assignmentTypeDetailsResponse => {
                         const assignmentTypeDetails = acronui.findObjectsByProp(assignmentTypeDetailsResponse, 'Assignment_Type');
                         if (Array.isArray(assignmentTypeDetails) && assignmentTypeDetails.length > 0) {
                             this.existingAssignmentTypeDetails = assignmentTypeDetails[0];
                         } else {
                             const eventData = { message: 'No Assignment Type Details found', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                             this.notificationHandler.next(eventData);
                         }
                         observer.next(assignmentTypeDetailsResponse);
                         observer.complete();
                     },
                     fail => {
                         const eventData = { message: 'Something went wrong while fetching Task Type', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                         this.notificationHandler.next(eventData);
                         observer.error(fail);
                     }
                 );
             }
         );
     } */
    getDependentTasks(isApprovalTask) {
        return new Observable(observer => {
            const requestParameters = {
                ProjectID: this.taskConfig.projectId,
                IsApprovalTask: isApprovalTask,
                AllowActionTask: this.allowActionTask
            };
            this.taskService.getDependentTask(requestParameters)
                .subscribe(dependentTaskResponse => {
                if (dependentTaskResponse) {
                    if (!Array.isArray(dependentTaskResponse)) {
                        dependentTaskResponse = [dependentTaskResponse];
                    }
                    this.taskModalConfig.taskList = dependentTaskResponse;
                    if (this.taskModalConfig.selectedTask) {
                        // removing the selected task from task list
                        this.taskModalConfig.taskList = this.taskModalConfig.taskList.filter(task => {
                            return task !== this.taskModalConfig.selectedTask;
                        });
                    }
                    const predecessorFilterList = [];
                    this.taskModalConfig.taskList.map(task => {
                        if (this.taskModalConfig.taskId && task['Task-id'].Id === this.taskModalConfig.taskId) {
                            this.taskModalConfig.selectedTask = task;
                        }
                        const value = this.formPredecessorValue(task);
                        if (task.NAME && task['Task-id'] && task['Task-id'].Id) {
                            predecessorFilterList.push({
                                name: task.NAME,
                                value: task['Task-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    this.taskModalConfig.deliveryPackageConfig.filterOptions = predecessorFilterList;
                    this.taskModalConfig.predecessorConfig.filterOptions = predecessorFilterList;
                }
                else {
                    if (this.taskForm) {
                        this.taskForm.controls.predecessor.patchValue('');
                        this.taskModalConfig.taskList = [];
                        if (this.taskConfig.isApprovalTask) {
                            this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
                        }
                        else {
                            this.taskModalConfig.predecessorConfig.filterOptions = [];
                        }
                    }
                    else {
                        this.taskModalConfig.taskList = [];
                        this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
                        this.taskModalConfig.predecessorConfig.filterOptions = [];
                    }
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            });
        });
    }
    getPreDetails() {
        this.getProjectDetails(this.taskConfig.projectObj)
            .subscribe(() => {
            this.getDependentTasks(this.taskConfig.isApprovalTask)
                .subscribe(() => {
                if (this.taskModalConfig.categoryId && this.taskModalConfig.teamId) {
                    this.getAllRoles().subscribe(() => {
                        forkJoin([this.getPriorities(), this.getStatus(), this.getInitialStatus(), this.getUsersByRole(), this.getReviewers()])
                            .subscribe(() => {
                            this.taskModalConfig.taskAssignmentTypes = TaskConstants.ALLOCATION_TYPE;
                            this.taskModalConfig.taskTypes = TaskConstants.TASK_TYPE_LIST;
                            if (this.taskModalConfig.isNewTask) {
                                this.loaderService.hide();
                                this.initialiseTaskForm();
                            }
                            else {
                                const ownerId = (this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.R_PO_OWNER_ID
                                    && this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id']) ? this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'].Id : '';
                                forkJoin([
                                    this.taskService.getTaskDataValues(this.taskConfig.taskId),
                                    this.formOwnerValue(ownerId),
                                    this.getDeliverableReviewByTaskId(this.taskConfig.taskId)
                                ]).subscribe((response) => {
                                    this.loaderService.hide();
                                    this.dateValidation = response[0];
                                    TaskConstants.TASK_TYPE_LIST.forEach(element => {
                                        if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.TASK_TYPE && element.NAME === this.taskModalConfig.selectedTask.TASK_TYPE) {
                                            this.existingTaskTypeDetails = element;
                                        }
                                    });
                                    TaskConstants.ALLOCATION_TYPE.forEach(element => {
                                        if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.ALLOCATION_TYPE && element.NAME === this.taskModalConfig.selectedTask.ALLOCATION_TYPE) {
                                            this.existingTaskTypeDetails = element;
                                        }
                                    });
                                    this.initialiseTaskForm();
                                }, () => {
                                    this.loaderService.hide();
                                });
                            }
                        });
                    });
                }
            }, () => {
                this.loaderService.hide();
                const eventData = { message: 'Something went wrong while fetching Tasks', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
            });
        }, () => {
            this.loaderService.hide();
            const eventData = { message: 'Something went wrong while fetching Project Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            this.notificationHandler.next(eventData);
        });
    }
    getTaskFields(task) {
        const taskField = {
            isApprovalTaskSelected: false,
            showOtherComments: false,
            isOtherCommentsSelected: false,
            showPredecessor: false,
            enableDeliverableApprover: false,
            showDeliveryPackage: false
        };
        if (task.IS_APPROVAL_TASK === 'true') {
            if (!this.utilService.isNullOrEmpty(task.PARENT_TASK_ID)) {
                taskField.isOtherCommentsSelected = false;
                taskField.showOtherComments = false;
                taskField.showPredecessor = false;
                taskField.showDeliveryPackage = true;
                taskField.enableDeliverableApprover = false;
                taskField.isApprovalTaskSelected = true;
            }
        }
        else {
            taskField.isApprovalTaskSelected = false;
            taskField.showOtherComments = false;
            taskField.isOtherCommentsSelected = false;
            taskField.showDeliveryPackage = false;
            taskField.showPredecessor = true;
            taskField.enableDeliverableApprover = false;
        }
        return taskField;
    }
    getPredecessortask(predecessorId) {
        let predecessorTask = null;
        this.taskModalConfig.taskList.map(task => {
            if (task['Task-id'].Id === predecessorId) {
                predecessorTask = task;
            }
        });
        return predecessorTask;
    }
    initialiseTaskForm() {
        const disableField = false;
        this.taskForm = null;
        this.taskModalConfig.minDate = this.taskModalConfig.selectedProject.START_DATE;
        this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
        const pattern = ProjectConstant.NAME_STRING_PATTERN;
        if (!this.taskModalConfig.isTemplate) {
            if (this.taskModalConfig.isNewTask) {
                this.taskStatus = this.taskModalConfig.taskInitialStatusList;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                    status: new FormControl({
                        value: this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                            this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    owner: new FormControl({ value: '', disabled: disableField }),
                    role: new FormControl({ value: '', disabled: disableField }),
                    priority: new FormControl({
                        value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                            this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    startDate: new FormControl({
                        value: this.taskModalConfig.selectedProject.START_DATE, disabled: disableField
                    }, [Validators.required]),
                    endDate: new FormControl({
                        value: this.taskModalConfig.selectedProject.DUE_DATE, disabled: (disableField || this.enableDuration)
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                    isMilestone: new FormControl({ value: false, disabled: disableField }),
                    viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                    predecessor: new FormControl({ value: '', disabled: disableField }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    allocation: new FormControl({ value: '', disabled: disableField })
                });
                this.taskForm.updateValueAndValidity();
            }
            else if (this.taskModalConfig.selectedTask) {
                this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
                this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
                // if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
                // this.overViewConfig.isReadOnly = true;
                // disableFields = true;
                // this.disableStatusOptions = true;
                // }
                const task = this.taskModalConfig.selectedTask;
                const taskField = this.getTaskFields(task);
                const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                // const taskOwnerId = task.R_PO_OWNER_ID && task.R_PO_OWNER_ID['Identity-id'] ? task.R_PO_OWNER_ID['Identity-id'].Id : '';
                task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: task.NAME, disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || !this.enableDuration)
                    }),
                    status: new FormControl({
                        value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                    }),
                    owner: new FormControl({
                        value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    role: new FormControl({
                        value: this.formRoleValue(taskRoleId), disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    priority: new FormControl({
                        value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                        disabled: disableField
                    }, [Validators.required]),
                    startDate: new FormControl({
                        value: this.commentsUtilService.addUnitsToDate(new Date(task.START_DATE), 0, 'days', this.enableWorkWeek), disabled: (disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL))
                    }, [Validators.required]),
                    endDate: new FormControl({ value: task.DUE_DATE, disabled: (disableField || this.enableDuration) }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                    isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                    viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                    predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                    // disabled: taskField.isApprovalTaskSelected || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                    deliverableApprover: new FormControl({
                        value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                    allocation: new FormControl({
                        value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    })
                });
                this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
                this.taskForm.controls.predecessor.disable();
                if (this.taskModalConfig.selectedpredecessor) {
                    const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                }
                this.getTaskOwnerAssignmentCount();
                // this.onTaskSelection();
                this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
                this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject(this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
                this.taskForm.updateValueAndValidity();
            }
        }
        else {
            if (this.taskModalConfig.isNewTask) {
                this.taskStatus = this.taskModalConfig.taskInitialStatusList;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    // 205
                    /*   duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                      durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                      */
                    duration: new FormControl({ value: '', disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }),
                    status: new FormControl(this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                        this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : ''),
                    owner: new FormControl({ value: '', disabled: disableField }),
                    role: new FormControl({ value: '', disabled: disableField }),
                    priority: new FormControl({
                        value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                            this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                    isMilestone: new FormControl({ value: false, disabled: disableField }),
                    viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                    predecessor: new FormControl({ value: '', disabled: disableField }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    allocation: new FormControl({ value: '', disabled: disableField })
                });
            }
            else if (this.taskModalConfig.selectedTask) {
                this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
                this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
                const task = this.taskModalConfig.selectedTask;
                const taskField = this.getTaskFields(task);
                const taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: task.NAME, disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    // 205
                    /* duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || !this.enableDuration)
                    }), */
                    duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                    }),
                    status: new FormControl({
                        value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: true
                    }),
                    owner: new FormControl({
                        value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    role: new FormControl({
                        value: this.formRoleValue(taskRoleId), disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    priority: new FormControl({
                        value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                    isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                    viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                    predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                    deliverableApprover: new FormControl({
                        value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                    allocation: new FormControl({
                        value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    })
                });
                this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
                this.taskForm.controls.predecessor.disable();
                if (this.taskModalConfig.selectedpredecessor) {
                    const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                }
                this.getTaskOwnerAssignmentCount();
                // this.onTaskSelection();
                this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
                this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject(this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
            }
        }
        this.filteredReviewers = this.taskForm.get('deliverableReviewers').valueChanges.pipe(startWith(''), map((reviewer) => reviewer ? this.filterReviewers(reviewer) : this.allReviewers.slice()));
        this.taskModalConfig.selectedStatusItemId = this.taskForm.value.status;
        // this.taskForm.controls.status.disable();
        this.taskModalConfig.predecessorConfig.formControl = this.taskForm.controls.predecessor;
        this.taskModalConfig.taskOwnerConfig.formControl = this.taskForm.controls.owner;
        this.taskModalConfig.taskRoleConfig.formControl = this.taskForm.controls.role;
        this.taskModalConfig.deliverableApprover.formControl = this.taskForm.controls.deliverableApprover;
        this.taskModalConfig.deliverableReviewer.formControl = this.taskForm.controls.deliverableReviewer;
        this.taskModalConfig.deliveryPackageConfig.formControl = this.taskForm.controls.predecessor;
        if (this.taskForm.controls.allocation.value === TaskConstants.ALLOCATION_TYPE_ROLE) {
            if (this.taskForm.controls.owner && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.deliverableApprover.disable();
            }
            if (this.taskForm.controls.deliverableReviewer && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.deliverableReviewer.disable();
            }
            /* if (this.taskForm.controls.role.value !== '') {
                this.getUsersByRole().subscribe(() => {
                    this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                    this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
                });
            } */
        }
        this.taskForm.controls.predecessor.valueChanges.subscribe(predecessor => {
            // TODO predecessor change method
            if (!this.taskModalConfig.selectedTask && typeof predecessor === 'object' && predecessor && predecessor.value) {
                this.taskModalConfig.taskList.map(task => {
                    if (task['Task-id'].Id === predecessor.value) {
                        /*if (task.REVISION_REVIEW_REQUIRED) {
                            this.taskForm.controls.revisionReviewRequired.patchValue(true);
                        }*/
                        const minDateObject = this.commentsUtilService.addUnitsToDate(new Date(task.DUE_DATE), 1, 'days', this.enableWorkWeek);
                        this.taskModalConfig.minDate = minDateObject;
                        this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
                        if (!this.taskModalConfig.isTemplate) {
                            this.taskForm.controls.startDate.setValue(minDateObject);
                            this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
                        }
                    }
                });
            }
            else {
                this.taskModalConfig.minDate = this.taskModalConfig.selectedpredecessor ?
                    this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek) : this.taskModalConfig.selectedProject.START_DATE;
                this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
                let selectedPredecessor;
                if (this.taskConfig.isApprovalTask) {
                    selectedPredecessor = this.taskModalConfig.deliveryPackageConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
                }
                else {
                    selectedPredecessor = this.taskModalConfig.predecessorConfig.filterOptions.find(predecessorValue => predecessorValue.displayName === predecessor);
                }
                if (selectedPredecessor) {
                    this.taskForm.controls.predecessor.setValue(selectedPredecessor);
                }
                // if (!this.taskModalConfig.isTemplate) {
                //     this.taskForm.controls.startDate.setValue(this.taskModalConfig.selectedProject.START_DATE);
                //     this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
                // }
            }
        });
        if (this.taskForm.controls.owner) {
            this.taskForm.controls.owner.valueChanges.subscribe(ownerValue => {
                if (ownerValue && typeof ownerValue === 'object' && !this.taskModalConfig.isTemplate) {
                    this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                        if (taskOwnerName.value === ownerValue.value) {
                            this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    this.ownerProjectCountMessage = '';
                    const selectedOwner = this.taskModalConfig.taskOwnerConfig.filterOptions.find(owner => owner.displayName === ownerValue);
                    if (selectedOwner) {
                        this.taskForm.controls.owner.setValue(selectedOwner);
                    }
                }
            });
        }
        this.taskForm.controls.allocation.valueChanges.subscribe(allocationTypeValue => {
            if (this.taskForm.controls.role) {
                this.taskForm.controls.role.setValue('');
            }
            if (this.taskForm.controls.owner) {
                this.taskForm.controls.owner.setValue('');
            }
            if (this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.setValue('');
            }
            if (this.taskForm.controls.deliverableReviewers) {
                this.taskForm.controls.deliverableReviewers.setValue('');
            }
            this.onChangeofAllocationType(allocationTypeValue, null, null, true);
        });
        /* this.taskForm.controls.deliverableReviewers.valueChanges.subscribe(deliverableReviewers => {
            console.log(deliverableReviewers);
        }); */
        this.taskForm.controls.role.valueChanges.subscribe(role => {
            if (role && typeof role === 'object') {
                this.onChangeofRole(role);
            }
            else {
                const selectedRole = this.taskModalConfig.taskRoleConfig.filterOptions.find(taskRole => taskRole.displayName === role);
                if (selectedRole) {
                    this.taskForm.controls.role.setValue(selectedRole);
                }
            }
        });
        if (this.taskForm.controls.startDate) {
            this.taskForm.controls.startDate.valueChanges.subscribe(() => {
                if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                    this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                        if (taskOwnerName.value === this.taskForm.value.owner.value) {
                            this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    this.ownerProjectCountMessage = '';
                }
                if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                    this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else {
                    this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
                if (new Date(this.taskForm.controls.endDate.value) > new Date(this.taskModalConfig.selectedProject.DUE_DATE)) {
                    this.durationErrorMessage = this.taskConstants.DURATION_ERROR_MESSAGE;
                }
                if (this.enableDuration) {
                    this.validateDuration();
                }
                if (this.dateValidation && this.dateValidation.startMaxDate && new Date(this.taskForm.controls.startDate.value) > new Date(this.dateValidation.startMaxDate)) {
                    this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else {
                    this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
            });
            this.taskForm.controls.endDate.valueChanges.subscribe(() => {
                if (this.taskForm.value.owner && this.taskForm.value.owner.value) {
                    this.taskModalConfig.taskOwnerConfig.filterOptions.map(taskOwnerName => {
                        if (taskOwnerName.value === this.taskForm.value.owner.value) {
                            this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    this.ownerProjectCountMessage = '';
                }
                if (this.dateValidation && this.dateValidation.endMinDate && new Date(this.taskForm.controls.endDate.value) < new Date(this.dateValidation.endMinDate)) {
                    this.dateErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else if (new Date(this.taskForm.controls.endDate.value) < new Date(this.taskForm.controls.startDate.value)) {
                    this.dateErrorMessage = this.taskConstants.START_DATE_ERROR_MESSAGE;
                }
                else {
                    this.dateErrorMessage = this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
            });
        }
        if (this.enableDuration) {
            this.taskForm.controls.duration.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
                if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
                    this.validateDuration();
                }
            });
            this.taskForm.controls.durationType.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
                if (this.taskForm.controls.startDate && this.taskForm.controls.startDate.value && this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
                    this.validateDuration();
                }
            });
        }
        this.taskModalConfig.hasAllConfig = true;
    }
    /* onTaskSelection() {
        const roles = [];
        const roleIds = [];
        if (this.taskConfig.isApprovalTask) {
            this.teamRolesOptions.find(element => {
                if (element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_APPROVER) !== -1) {
                    roles.push(element);
                    roleIds.push(element.name);
                }
            });
        } else {
            this.teamRolesOptions.find(element => {
                if (element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MEMBER) !== -1) {
                    roles.push(element);
                    roleIds.push(element.name);
                }
            });
        }
        this.taskModalConfig.taskRoleConfig.filterOptions = roles;
        this.getUsersByRole().subscribe(() => {
            this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
        });
    } */
    validateDuration() {
        if (this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
            if (this.taskForm.controls.duration.value < 1 || this.taskForm.controls.duration.value > 999) {
                this.durationErrorMessage = this.taskConstants.DURATION_MIN_ERROR_MESSAGE;
            }
            else {
                let endDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskForm.controls.startDate.value), this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
                this.taskForm.controls.endDate.setValue(endDateObject);
                this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
                this.taskForm.controls.duration.updateValueAndValidity();
                if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                    if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                        const minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                        this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                        this.taskForm.controls.duration.updateValueAndValidity();
                        this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                    }
                }
            }
        }
    }
    onChangeofAllocationType(allocationTypeValue, ownerValue, roleValue, changeDeliverableApprover) {
        if (allocationTypeValue === TaskConstants.ALLOCATION_TYPE_ROLE) {
            if (this.taskForm.controls.owner) {
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.disable();
            }
            this.taskForm.controls.role.setValidators([Validators.required]);
            this.taskForm.controls.owner.clearValidators();
            this.taskForm.controls.owner.updateValueAndValidity();
            if (this.taskConfig.isApprovalTask) {
                this.taskForm.controls.deliverableApprover.clearValidators();
                this.taskForm.controls.deliverableApprover.updateValueAndValidity();
            }
            this.taskForm.controls.role.patchValue(roleValue ? roleValue : '');
            this.taskForm.controls.owner.patchValue(ownerValue ? ownerValue : '');
            if (changeDeliverableApprover && this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.patchValue('');
            }
        }
        else if (allocationTypeValue === TaskConstants.ALLOCATION_TYPE_USER) {
            this.getUsersByRole().subscribe(() => {
                this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
                this.taskForm.controls.owner.patchValue(ownerValue ? ownerValue : '');
                if (changeDeliverableApprover && this.taskForm.controls.deliverableApprover) {
                    this.taskForm.controls.deliverableApprover.patchValue('');
                }
                this.taskForm.controls.role.clearValidators();
                this.taskForm.controls.role.updateValueAndValidity();
                if (this.taskConfig.isApprovalTask) {
                    this.taskForm.controls.deliverableApprover.setValidators([Validators.required]);
                    this.taskForm.controls.deliverableApprover.updateValueAndValidity();
                }
                else {
                    this.taskForm.controls.owner.setValidators([Validators.required]);
                    this.taskForm.controls.owner.updateValueAndValidity();
                }
                if (this.taskForm.controls.owner) {
                    if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                        this.taskForm.controls.owner.enable();
                    }
                }
                if (this.taskForm.controls.deliverableApprover) {
                    if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                        this.taskForm.controls.deliverableApprover.enable();
                    }
                }
            });
        }
    }
    onChangeofRole(role) {
        if (role && role.value) {
            if (this.taskForm.controls.owner) {
                if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                    this.taskForm.controls.owner.enable();
                }
            }
            if (this.taskForm.controls.deliverableApprover) {
                if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                    this.taskForm.controls.deliverableApprover.enable();
                }
            }
            this.getUsersByRole().subscribe(() => {
                this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
                if (this.taskForm.controls.owner) {
                    this.taskForm.controls.owner.patchValue('');
                }
                if (this.taskForm.controls.deliverableApprover) {
                    this.taskForm.controls.deliverableApprover.patchValue('');
                }
            });
        }
        else {
            if (this.taskForm.controls.owner) {
                this.taskForm.controls.owner.patchValue((role ? role : ''));
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.patchValue((role ? role : ''));
                this.taskForm.controls.deliverableApprover.disable();
            }
        }
    }
    onApprovalStateChange() {
        this.taskForm.controls.predecessor.setValue('');
        forkJoin([this.getAllRoles(), this.getDependentTasks(this.taskConfig.isApprovalTask)])
            .subscribe(() => {
            this.taskForm.controls.role.updateValueAndValidity();
            this.taskForm.controls.predecessor.updateValueAndValidity();
        });
    }
    formPredecessorValue(task) {
        if (task.NAME && task['Task-id'] && task['Task-id'].Id) {
            return task['Task-id'].Id + '-' + task.NAME;
        }
    }
    formPredecessor(taskId) {
        if (taskId) {
            const selectedPredecessor = this.taskModalConfig.predecessorConfig.filterOptions.find(element => element.value === taskId);
            return selectedPredecessor ? selectedPredecessor : '';
        }
        else {
            return '';
        }
    }
    formOwnerValue(taskUserID) {
        return new Observable(observer => {
            if (!this.utilService.isNullOrEmpty(taskUserID)) {
                const parameters = {
                    userId: taskUserID
                };
                this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
                    const selectedUser = this.teamRoleUserOptions.find(element => element.value === response.userCN);
                    if (selectedUser) {
                        this.selectedOwner = selectedUser;
                    }
                    observer.next(true);
                    observer.complete();
                }, error => {
                    const eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    }
    formRoleValue(taskRoleID) {
        if (taskRoleID) {
            const selectedRole = this.teamRolesOptions.find(element => element.name === taskRoleID);
            return (selectedRole) ? selectedRole : '';
        }
        else {
            return '';
        }
    }
    getTaskOwnerAssignmentCount() {
    }
    cancelTaskCreation() {
        if (this.taskForm.pristine) {
            this.closeCallbackHandler.next(true);
        }
        else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    this.closeCallbackHandler.next(true);
                }
            });
        }
    }
    updateTask(saveOption) {
        if (this.taskForm.getRawValue().deliverableApprover && this.taskForm.getRawValue().deliverableApprover.value) {
            const data = this.savedReviewers.find(reviewer => reviewer.value === this.taskForm.getRawValue().deliverableApprover.value);
            if (data) {
                const eventData = { message: 'Selected reviewer is already the approver', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.disableTaskSave = false;
                return;
            }
        }
        if (this.taskForm.pristine && !(this.chipChanged)) {
            const eventData = { message: 'Kindly make changes to update the task', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            return;
        }
        const nameSplit = this.taskForm.value.taskName.split('_');
        if (nameSplit[0].length === 0) {
            const eventData = { message: 'Name should not start with "_"', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        this.disableTaskSave = true;
        const taskEndDateWarning = false;
        if (taskEndDateWarning) {
            this.taskForm.patchValue({
                endDate: this.taskModalConfig.selectedTask.DUE_DATE
            });
            const eventData = { message: 'Task\'s end date should not be less than the deliverables\' due date', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            return;
        }
        if (this.taskForm.status === 'VALID') {
            if (this.taskForm.controls.taskName.value.trim() === '') {
                const eventData = { message: 'Please provide a valid task name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                this.notificationHandler.next(eventData);
                this.disableTaskSave = false;
            }
            else {
                this.taskForm.controls.taskName.setValue(this.taskForm.controls.taskName.value.trim());
                this.disableTaskSave = true;
                this.updateTaskDetails(saveOption);
            }
        }
        else {
            const eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
        }
    }
    formReviewerObj() {
        const addedReviewers = [];
        const removedReviewers = [];
        if (this.newReviewers && this.deliverableReviewers) {
            this.newReviewers.forEach(newReviewer => {
                if (this.deliverableReviewers.length === 0) {
                    const addedReviewerIndex = addedReviewers.findIndex(reviewer => reviewer.value === newReviewer.value);
                    if (addedReviewerIndex === -1) {
                        addedReviewers.push(newReviewer);
                    }
                }
                else {
                    const deliverableReviewerIndex = this.deliverableReviewers.findIndex(deliverableReviewer => deliverableReviewer.value === newReviewer.value);
                    if (deliverableReviewerIndex === -1) {
                        const addedReviewerIndex = addedReviewers.findIndex(reviewer => reviewer.value === newReviewer.value);
                        if (addedReviewerIndex === -1) {
                            addedReviewers.push(newReviewer);
                        }
                    }
                }
            });
            this.newreviewersObj = addedReviewers.map((addedReviewer) => {
                return {
                    DeliverableId: '',
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: addedReviewer.value,
                        action: 'ADD'
                    },
                    TaskId: this.taskConfig.taskId,
                    IsPMAssignedReviewer: this.isPM
                };
            });
        }
        if (this.removedReviewers && this.savedReviewers) {
            this.removedReviewers.forEach(removedReviewer => {
                if (this.savedReviewers.length === 0) {
                    const deliverableReviewerIndex = this.deliverableReviewers.findIndex(deliverableReviewer => deliverableReviewer.value === removedReviewer.value);
                    if (deliverableReviewerIndex >= 0) {
                        const reviewerIndex = removedReviewers.findIndex(reviewer => reviewer.value === removedReviewer.value);
                        if (reviewerIndex === -1) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
                else {
                    const savedReviewerIndex = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === removedReviewer.value);
                    if (savedReviewerIndex === -1) {
                        const removedReviewerIndex = removedReviewers.findIndex(reviewer => reviewer.value === removedReviewer.value);
                        const deliverableReviewerIndex = this.deliverableReviewers.findIndex(data => data.value === removedReviewer.value);
                        if (removedReviewerIndex === -1 && deliverableReviewerIndex >= 0) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
            });
            this.removedreviewersObj = removedReviewers.map((reviewer) => {
                return {
                    DeliverableId: '',
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: reviewer.value,
                        action: 'REMOVE'
                    },
                    TaskId: this.taskConfig.taskId,
                    IsPMAssignedReviewer: this.isPM
                };
            });
        }
        this.selectedReviewersObj = this.savedReviewers.map((reviewer) => {
            return {
                Id: '',
                ItemId: '',
                userID: reviewer.value
            };
        });
        this.reviewersObj = this.newreviewersObj ? this.newreviewersObj.concat(this.removedreviewersObj) : this.removedreviewersObj.concat(this.newreviewersObj);
    }
    updateTaskDetails(saveOption) {
        const taskFormValues = this.taskForm.getRawValue();
        this.formReviewerObj();
        const TaskObject = this.projectUtilService.createNewTaskUpdateObject(taskFormValues, this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.selectedReviewersObj);
        if (this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask['Task-id']
            && this.taskModalConfig.selectedTask['Task-id'].Id) {
            TaskObject.TaskId.Id = this.taskModalConfig.selectedTask['Task-id'].Id;
            TaskObject.RevisionReviewRequired = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED)
                ? '' : this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED;
            TaskObject.IsMilestone = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.IS_MILESTONE)
                ? '' : this.taskModalConfig.selectedTask.IS_MILESTONE;
            TaskObject.IsMilestone = this.utilService.isNullOrEmpty(taskFormValues.isMilestone)
                ? '' : taskFormValues.isMilestone;
        }
        this.saveTask(TaskObject, saveOption);
    }
    saveTask(TaskObject, saveOption) {
        this.loaderService.show();
        if (this.taskModalConfig.selectedTask) {
            TaskObject = this.projectUtilService.compareTwoProjectDetails(TaskObject, this.oldTaskObject);
            if (Object.keys(TaskObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
                TaskObject.BulkOperation = false;
                const requestObj = {
                    TaskObject: TaskObject,
                    DeliverableReviewers: {
                        DeliverableReviewer: this.reviewersObj
                    }
                };
                this.taskService.updateTask(requestObj).subscribe(response => {
                    this.loaderService.hide();
                    this.disableTaskSave = false;
                    if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                        const eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        this.notificationHandler.next(eventData);
                        this.saveCallBackHandler.next({
                            taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: this.taskModalConfig.projectId,
                            isTemplate: this.taskModalConfig.isTemplate, saveOption: saveOption
                        });
                    }
                    else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                        const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        this.notificationHandler.next(eventData);
                    }
                    else {
                        const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        this.notificationHandler.next(eventData);
                    }
                }, error => {
                    const eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                });
            }
            else {
                this.loaderService.hide();
                this.disableTaskSave = false;
            }
        }
        else {
            const requestObj = {
                TaskObject: TaskObject
            };
            this.taskService.createTask(requestObj)
                .subscribe(response => {
                this.loaderService.hide();
                this.disableTaskSave = false;
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                    && response.APIResponse.data.Task && response.APIResponse.data.Task['Task-id'] && response.APIResponse.data.Task['Task-id'].Id) {
                    const eventData = { message: 'Task saved successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    this.notificationHandler.next(eventData);
                    this.disableTaskSave = false;
                    this.saveCallBackHandler.next({
                        taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: this.taskModalConfig.projectId,
                        isTemplate: this.taskModalConfig.isTemplate, saveOption: saveOption
                    });
                }
                else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                    && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                    const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                }
                else {
                    const eventData = { message: 'Something went wrong on while creating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    this.notificationHandler.next(eventData);
                }
            }, () => {
                this.loaderService.hide();
                const eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.disableTaskSave = false;
            });
        }
    }
    initialiseTaskConfig() {
        this.taskModalConfig = {
            hasAllConfig: false,
            taskStatusList: null,
            taskInitialStatusList: null,
            taskPriorityList: null,
            predecessor: null,
            taskList: this.taskConfig.taskList,
            selectedProject: this.taskConfig.selectedProject,
            defaultStatus: null,
            defaultPriority: null,
            selectedStatusItemId: null,
            minDate: '',
            maxDate: '',
            isNewTask: (this.taskConfig.taskId) ? false : true,
            taskId: this.taskConfig.taskId,
            selectedTask: this.taskConfig.selectedTask,
            selectedpredecessor: null,
            isTemplate: this.taskConfig.isTemplate,
            projectId: this.taskConfig.projectId,
            taskOwnerConfig: {
                label: 'Task Owner',
                filterOptions: [],
                formControl: null,
            },
            taskRoleConfig: {
                label: 'Role',
                filterOptions: [],
                formControl: null,
            },
            predecessorConfig: {
                label: 'Predecessor',
                filterOptions: [],
                formControl: null
            },
            deliverableApprover: {
                label: 'Deliverable Approver',
                filterOptions: [],
                formControl: null
            },
            deliverableReviewer: {
                label: 'Deliverable Reviewer',
                filterOptions: [],
                formControl: null
            },
            deliveryPackageConfig: {
                label: 'Delivery Package',
                filterOptions: [],
                formControl: null
            },
            taskAssignmentTypes: null,
        };
    }
    ngOnInit() {
        const projectConfig = this.sharingService.getProjectConfig();
        this.enableDuration = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_DURATION]);
        this.enableWorkWeek = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.showRevisionReviewRequired = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_REVISION_REVIEW_REQUIRED]);
        this.projectConfig = this.sharingService.getProjectConfig();
        this.appConfig = this.sharingService.getAppConfig();
        this.defaultRevisionRequiredStatus = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.DEFAULT_REVISION_REQUIRED_STATUS]);
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        this.allowActionTask = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ALLOW_ACTION_TASK]);
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach(user => {
            if (user.Name === 'MPM Project Manager') {
                this.isPM = true;
            }
        });
        // const statuses = acronui.findObjectsByProp(response, 'Comments');
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        if (this.taskConfig && this.taskConfig.projectId) {
            this.initialiseTaskConfig();
            this.getPreDetails();
        }
    }
};
TaskCreationComponent.ctorParameters = () => [
    { type: AppService },
    { type: SharingService },
    { type: DateAdapter },
    { type: UtilService },
    { type: TaskService },
    { type: EntityAppDefService },
    { type: LoaderService },
    { type: MatDialog },
    { type: ProjectUtilService },
    { type: CommentsUtilService },
    { type: NotificationService }
];
__decorate([
    Input()
], TaskCreationComponent.prototype, "taskConfig", void 0);
__decorate([
    Output()
], TaskCreationComponent.prototype, "closeCallbackHandler", void 0);
__decorate([
    Output()
], TaskCreationComponent.prototype, "saveCallBackHandler", void 0);
__decorate([
    Output()
], TaskCreationComponent.prototype, "notificationHandler", void 0);
__decorate([
    ViewChild('reviewerInput')
], TaskCreationComponent.prototype, "reviewerInput", void 0);
__decorate([
    ViewChild('auto')
], TaskCreationComponent.prototype, "matAutocomplete", void 0);
TaskCreationComponent = __decorate([
    Component({
        selector: 'mpm-task-creation',
        template: "<div class=\"overview-content\">\r\n    <form class=\"task-creation-form\" *ngIf=\"taskForm\" [formGroup]=\"taskForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Task Name</mat-label>\r\n                    <input [appAutoFocus]=\"taskModalConfig && taskModalConfig.hasAllConfig\" matInput\r\n                        placeholder=\"Task Name\" formControlName=\"taskName\" required>\r\n                    <mat-hint align=\"end\"\r\n                        *ngIf=\"taskForm.get('taskName').errors && taskForm.get('taskName').errors.maxlength\">\r\n                        {{taskForm.get('taskName').errors.maxlength.actualLength}}/{{taskForm.get('taskName').errors.maxlength.requiredLength}}\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\"\r\n                *ngIf=\"!(taskConfig.taskType == taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE) && (taskConfig.isApprovalTask) && showRevisionReviewRequired\">\r\n                <div class=\"approval-section-holder\">\r\n\r\n                    <section>\r\n                        <mat-checkbox color=\"primary\" formControlName=\"revisionReviewRequired\"><span>Review Required For\r\n                                Revision</span>\r\n                        </mat-checkbox>\r\n                    </section>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Allocation</mat-label>\r\n                    <mat-select formControlName=\"allocation\" placeholder=\"Allocation\" [required]=\"true\"\r\n                        name=\"allocation\">\r\n                        <mat-option *ngFor=\"let allocation of taskModalConfig.taskAssignmentTypes\"\r\n                            value=\"{{allocation.NAME}}\">\r\n                            {{allocation.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE\"\r\n                [searchFieldConfig]=\"taskModalConfig.taskRoleConfig\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"!taskConfig.isApprovalTask && (taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE || taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER)\"\r\n                [searchFieldConfig]=\"taskModalConfig.taskOwnerConfig\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"taskConfig.isApprovalTask && (taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE || taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER)\"\r\n                [searchFieldConfig]=\"taskModalConfig.deliverableApprover\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\" *ngIf=\"!taskConfig.isApprovalTask && this.taskModalConfig.predecessorConfig.filterOptions.length > 0\"\r\n                [searchFieldConfig]=\"taskModalConfig.predecessorConfig\" [required]=\"false\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\" flex-row-item\" *ngIf=\"taskConfig.isApprovalTask\"\r\n                [searchFieldConfig]=\"taskModalConfig.deliveryPackageConfig\" [required]=\"true\">\r\n            </mpm-custom-search-field>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Status</mat-label>\r\n                    <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"status\" required>\r\n                        <mat-option *ngFor=\"let status of taskStatus\" value=\"{{status['MPM_Status-id'].Id}}\">\r\n                            {{status.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Priority</mat-label>\r\n                    <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" required>\r\n                        <mat-option *ngFor=\"let priority of taskModalConfig.taskPriorityList\"\r\n                            value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                            {{priority.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"task-owner-task-count\" *ngIf=\"ownerProjectCount\">\r\n            <mat-error *ngIf=\"!isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-error>\r\n            <mat-label *ngIf=\"isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-label>\r\n        </div>\r\n        <div class=\"flex-row\" *ngIf=\"!taskModalConfig.isTemplate\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Task Start Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"startDate\" placeholder=\"Task Start Date (MM/DD/YYYY)\"\r\n                        formControlName=\"startDate\" [min]=\"taskModalConfig.minDate\"\r\n                        [max]=\"dateValidation && dateValidation.startMaxDate ? dateValidation.startMaxDate : taskModalConfig.maxDate\"\r\n                        required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #startDate\r\n                        [disabled]=\"selectedStatusInfo && selectedStatusInfo.STATUS_TYPE !== taskConstants.STATUS_TYPE_INITIAL\">\r\n                    </mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"(taskForm.get('startDate').hasError('matDatepickerMax') || taskForm.get('startDate').hasError('matDatepickerMin'))\">\r\n                        {{dateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" *ngIf=\"!taskModalConfig.isTemplate\">\r\n                    <mat-label>Task End Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"endDate\"\r\n                        [min]=\"(dateValidation && dateValidation.endMinDate) ? dateValidation.endMinDate : (taskForm.getRawValue().startDate ? taskForm.getRawValue().startDate : taskModalConfig.minDate)\"\r\n                        [max]=\"taskModalConfig.maxDate\" placeholder=\"Task End Date (MM/DD/YYYY)\"\r\n                        formControlName=\"endDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #endDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"taskForm.get('endDate') && (taskForm.get('endDate').hasError('matDatepickerMax') || taskForm.get('endDate').hasError('matDatepickerMin'))\">\r\n                        {{dateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Description</mat-label>\r\n                    <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\r\n                    <mat-hint align=\"end\"\r\n                        *ngIf=\"taskForm.get('description').errors && taskForm.get('description').errors.maxlength\">\r\n                        {{taskForm.get('description').errors.maxlength.actualLength}}/{{taskForm.get('description').errors.maxlength.requiredLength}}\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item\">\r\n                    <span class=\"form-actions\">\r\n                        <button type=\"button\"  mat-stroked-button (click)=\"cancelTaskCreation()\">Cancel</button>\r\n                        <button id=\"task-save-btn\" color='primary' mat-flat-button\r\n                            (click)=\"updateTask(SELECTED_SAVE_OPTION)\" type=\"submit\" class=\"ang-btn\"\r\n                            matTooltip=\"{{SELECTED_SAVE_OPTION.name}}\"\r\n                            [disabled]=\"(taskForm.pristine || taskForm.invalid)\">{{SELECTED_SAVE_OPTION.name}}</button>\r\n                        <span *ngIf=\"!taskConfig.taskId\">\r\n                            <button id=\"task-save-option\" mat-flat-button color=\"primary\"\r\n                                [matMenuTriggerFor]=\"saveOptionsMenu\"\r\n                                [disabled]=\"taskForm.pristine || taskForm.invalid\">\r\n                                <mat-icon>arrow_drop_down</mat-icon>\r\n                            </button>\r\n                            <mat-menu #saveOptionsMenu=\"matMenu\">\r\n                                <span *ngFor=\"let saveOption of saveOptions\">\r\n                                    <button *ngIf=\"saveOption.value != SELECTED_SAVE_OPTION.value\" mat-menu-item\r\n                                        (click)=\"updateTask(saveOption)\">\r\n                                        <span>{{saveOption.name}}</span>\r\n                                    </button>\r\n                                </span>\r\n                            </mat-menu>\r\n                        </span>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>",
        styles: [".overview-content{padding:0 20px}.flex-row-item{margin:.666667em 15px 0;font-size:14px}.flex-row-item div.approval-section-holder{padding-bottom:1.34375em}.flex-row-item section.approval-task-check{margin-right:90px}.project-overview-form{display:flex;flex-direction:column;flex-wrap:wrap;justify-content:center;padding:10px 50px;width:50vw}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-actions button{margin:10px}.task-owner-task-count{margin-left:16px;margin-bottom:16px;font-size:14px;margin-top:-16px}.task-owner-task-count mat-label{color:green}#task-save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}#task-save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:15px}.form-actions{margin-left:auto}mat-form-field{width:100%}.form-group-panel{background:0 0}.form-group-panel ::ng-deep mpm-custom-metadata-field{width:100%}.form-group-panel ::ng-deep .mat-expansion-panel-header{padding:0 4px}.form-group-panel ::ng-deep .mat-expansion-panel-body{padding:0 16px 16px 0}"]
    })
], TaskCreationComponent);
export { TaskCreationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jcmVhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL3Rhc2stY3JlYXRpb24vdGFzay1jcmVhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFHckQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sS0FBSyxPQUFPLE1BQU0saUNBQWlDLENBQUM7QUFDM0QsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUN4RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQztBQUN4SCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN0RixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUVoRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUNwRyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBb0IsTUFBTSx1QkFBdUIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBS3RFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBT2pGLElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBaUY5QixZQUNXLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLE9BQXlCLEVBQ3pCLFdBQXdCLEVBQ3hCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxhQUE0QixFQUM1QixNQUFpQixFQUNqQixrQkFBc0MsRUFDdEMsbUJBQXdDLEVBQ3hDLG1CQUF3QztRQVZ4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixZQUFPLEdBQVAsT0FBTyxDQUFrQjtRQUN6QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUF6RnpDLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDL0Msd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5Qyx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBVXhELDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMvQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsNkJBQXdCLEdBQUcsRUFBRSxDQUFDO1FBQzlCLDRCQUF1QixHQUFHLEtBQUssQ0FBQztRQUVoQyxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4Qiw0QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDL0Isd0NBQXdDO1FBQ3hDLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBaUI5QixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixrQkFBa0I7UUFDbEIseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQzFCLHdCQUF3QjtRQUN4Qix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFNeEIsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLHVCQUFrQixHQUFHLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUd0QixTQUFJLEdBQUcsS0FBSyxDQUFDO1FBRWIsZ0JBQVcsR0FBRyxrQkFBa0IsQ0FBQyxlQUFlLENBQUM7UUFDakQseUJBQW9CLEdBQUc7WUFDbkIsSUFBSSxFQUFFLE1BQU07WUFDWixLQUFLLEVBQUUsTUFBTTtTQUNoQixDQUFDO1FBQ0YsaUJBQVksR0FBRztZQUNYLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFO1lBQ3BDLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFO1NBQ3pDLENBQUM7UUFDRixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUd2QixtQkFBYyxHQUFHO1lBQ2IsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7U0FDdkMsQ0FBQztRQW9sREYsZUFBVSxHQUNOLENBQUMsSUFBaUIsRUFBRSxFQUFFO1lBQ2xCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsT0FBTyxJQUFJLENBQUM7YUFDZjtpQkFBTTtnQkFDSCxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQzFCLE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUM5QixnQkFBZ0I7Z0JBQ2hCLGtCQUFrQjthQUNyQjtRQUNMLENBQUMsQ0FBQTtJQTNrREwsQ0FBQztJQUVELGVBQWUsQ0FBQyxLQUFVO1FBQ3RCLElBQUksS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQzNELE1BQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUV4QyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDeEc7SUFDTCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBSyxFQUFFLFFBQVE7UUFDOUIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN4RixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDMUI7SUFDTCxDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQW1DO1FBQ3hDLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pILE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRW5HLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsbUJBQW1CO1lBQ3ZDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRTtZQUN0RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsMEJBQTBCLENBQUMsQ0FBQztTQUNyRjthQUFNLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQzNCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxzQkFBc0IsQ0FBQyxDQUFDO1NBQ2pGO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnQkFDckIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxXQUFXO2dCQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JCLFdBQVcsRUFBRSxRQUFRLENBQUMsV0FBVzthQUNwQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDbkIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxXQUFXO2dCQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JCLFdBQVcsRUFBRSxRQUFRLENBQUMsV0FBVzthQUNwQyxDQUFDLENBQUM7WUFDSCxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0gsSUFBSSxtQkFBbUIsSUFBSSxDQUFDLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDeEQ7WUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDO2dCQUN6QixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO2dCQUNqQyxXQUFXLEVBQUUsS0FBSzthQUNyQixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5RDtJQUNMLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBd0I7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO1lBQzlCLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFFMUIsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsS0FBSyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7YUFDcEI7WUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDOUQ7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsYUFBa0I7UUFDL0IsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFLLGFBQWEsQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLGFBQWEsQ0FBQyxDQUFDO1FBQ3pHLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsS0FBSyxLQUFLLGFBQWEsQ0FBQyxDQUFDO1FBQzlHLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsQ0FBQztRQUM5RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztZQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7WUFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO1NBQ3BDLENBQUMsQ0FBQztRQUVILElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxhQUFhLElBQUksQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxnQkFBZ0IsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsVUFBVTtRQUN4QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUN2RSxJQUFJLFVBQVUsQ0FBQyxhQUFhLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO29CQUN6RSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNwRjtnQkFDRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDOUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ3pFO2dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztnQkFDbEQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNqRSxJQUFJLGFBQWEsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWU7b0JBQ3JELGFBQWEsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQzdGLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztpQkFDOUM7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYTtRQUNULE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0I7Ozs7Ozs7Ozs7Ozs7Ozs7O3VCQWlCVztZQUNYLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQ3RFLG1DQUFtQztZQUNuQyxJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDMUIsUUFBUSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3pCO2dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDO2FBQ3BEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO2FBQzlDO1lBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDcEI7Ozs7O21CQUtPO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsU0FBUztRQUNMLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7aUJBQzdELFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDeEIsSUFBSSxjQUFjLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dCQUNoQyxjQUFjLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDckM7b0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO2lCQUN4RDtxQkFBTTtvQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7aUJBQzVDO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNKLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdEQUFnRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUM7aUJBQ25FLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDeEIsSUFBSSxjQUFjLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dCQUNoQyxjQUFjLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDckM7b0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsR0FBRyxjQUFjLENBQUM7aUJBQy9EO3FCQUFNO29CQUNILElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO2lCQUNuRDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEdBQUcsRUFBRTtnQkFDSixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnREFBZ0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXO1FBQ1AsTUFBTSxnQkFBZ0IsR0FBRztZQUNyQixNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNO1lBQ25DLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWM7WUFDOUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO1NBQ2hELENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUM7aUJBQ3pDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRTtvQkFDMUcsUUFBUSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO29CQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsRUFBRTt3QkFDaEQsUUFBUSxDQUFDLHFCQUFxQixHQUFHLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUM7cUJBQ3JFO29CQUNELElBQUksUUFBUSxDQUFDO29CQUNiLFFBQVEsR0FBRyxRQUFRLENBQUMscUJBQXFCLENBQUM7b0JBQzFDLE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDcEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDaEIsS0FBSyxDQUFDLElBQUksQ0FBQztnQ0FDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRTtnQ0FDekMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPO2dDQUNuQixXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0NBQ3RCLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYTs2QkFDL0IsQ0FBQyxDQUFDO3dCQUNQLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7b0JBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7aUJBQzdFO3FCQUFNO29CQUNILElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7aUJBQzlCO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNKLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdEQUFnRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGVBQWUsQ0FBQyxVQUFVO1FBQ3RCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUM3QyxNQUFNLFVBQVUsR0FBRztvQkFDZixNQUFNLEVBQUUsVUFBVTtpQkFDckIsQ0FBQztnQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDdkUsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7b0JBQ1AsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ25GLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0QkFBNEIsQ0FBQyxNQUFNO1FBQy9CLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLENBQUM7aUJBQy9DLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGlCQUFpQixFQUFFO29CQUN4QyxNQUFNLGFBQWEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLENBQUM7b0JBQy9FLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ25FLGFBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7NEJBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0NBQ2hGLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTtvQ0FDekYsSUFBSSxZQUFZLEVBQUU7d0NBQ2QsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFLLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3Q0FDL0csSUFBSSxDQUFDLGVBQWUsRUFBRTs0Q0FDbEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0RBQ3JCLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDM0IsS0FBSyxFQUFFLFlBQVksQ0FBQyxNQUFNO2dEQUMxQixXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVE7NkNBQ3JDLENBQUMsQ0FBQzs0Q0FDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO2dEQUMzQixJQUFJLEVBQUUsWUFBWSxDQUFDLFFBQVE7Z0RBQzNCLEtBQUssRUFBRSxZQUFZLENBQUMsTUFBTTtnREFDMUIsV0FBVyxFQUFFLFlBQVksQ0FBQyxRQUFROzZDQUNyQyxDQUFDLENBQUM7eUNBQ047cUNBQ0o7b0NBQ0QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dDQUMvRyxNQUFNLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxZQUFZLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt3Q0FDbkYsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dDQUNwRyxJQUFJLGFBQWEsSUFBSSxDQUFDLEVBQUU7NENBQ3BCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUM7eUNBQzVFOzZDQUFNOzRDQUNILElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0RBQ3pCLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDM0IsS0FBSyxFQUFFLFlBQVksQ0FBQyxNQUFNO2dEQUMxQixXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVE7Z0RBQ2xDLFdBQVcsRUFBRSxtQkFBbUI7NkNBQ25DLENBQUMsQ0FBQzt5Q0FDTjtvQ0FDTCxDQUFDLENBQUMsQ0FBQztvQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztvQ0FDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dDQUN4QixDQUFDLENBQUMsQ0FBQzs2QkFDTjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NkJBQ3ZCO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7aUJBQ0o7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUVMLENBQUMsRUFBRSxHQUFHLEVBQUU7Z0JBQ0osTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0VBQWdFLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakosSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsWUFBWTtRQUNSLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtnQkFDaEMsSUFBSSxzQkFBc0IsR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLHNCQUFzQixHQUFHO29CQUNyQixjQUFjLEVBQUUsYUFBYSxDQUFDLG9CQUFvQjtvQkFDbEQsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTTtvQkFDbkMsYUFBYSxFQUFFLElBQUk7b0JBQ25CLFlBQVksRUFBRSxLQUFLO29CQUNuQixRQUFRLEVBQUUsSUFBSTtpQkFDakIsQ0FBQztnQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsQ0FBQztxQkFDbEQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO3dCQUNuRCxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3dCQUM3RCxNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7d0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3BELFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0NBQ3BCLE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQ0FDdEQsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQ0FDWCxLQUFLLENBQUMsSUFBSSxDQUFDO3dDQUNQLElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTt3Q0FDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7d0NBQ2QsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO3FDQUN6QixDQUFDLENBQUM7aUNBQ047NEJBQ0wsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7cUJBQzdCO3lCQUFNO3dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO3FCQUMxQjtvQkFDRCw2QkFBNkI7b0JBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEdBQUcsRUFBRTtvQkFDSixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxvREFBb0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNySSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsY0FBYztRQUNWLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztZQUM5QixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2YsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDbEQsb0JBQW9CLEdBQUc7b0JBQ25CLGNBQWMsRUFBRSxhQUFhLENBQUMsVUFBVTtvQkFDeEMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxVQUFVLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdkcsTUFBTSxFQUFFLGFBQWEsQ0FBQyxVQUFVLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDMUcsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYztvQkFDN0MsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO2lCQUNoRCxDQUFDO2FBQ0w7aUJBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDMUMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQztvQkFDM0ksQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUMzRixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxvQkFBb0IsR0FBRztvQkFDbkIsY0FBYyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGVBQWU7b0JBQ2pFLE1BQU0sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxlQUFlLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ2hJLE1BQU0sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxlQUFlLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDbkksYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYztvQkFDN0MsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO2lCQUNoRCxDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsb0JBQW9CLEdBQUc7b0JBQ25CLGNBQWMsRUFBRSxhQUFhLENBQUMsb0JBQW9CO29CQUNsRCxNQUFNLEVBQUUsRUFBRTtvQkFDVixNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNO29CQUNuQyxhQUFhLEVBQUUsS0FBSztvQkFDcEIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCLENBQUM7YUFDTDtZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDO2lCQUNoRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7b0JBQ25ELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQzdELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDcEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDaEIsTUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzRCQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO2dDQUNYLEtBQUssQ0FBQyxJQUFJLENBQUM7b0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFO29DQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7aUNBQ3pCLENBQUMsQ0FBQzs2QkFDTjt3QkFDTCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFDRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO29CQUNqQyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO29CQUM5RSxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7aUJBQ3JGO3FCQUFNO29CQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7aUJBQ2pDO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNKLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3ZJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtCQUFrQixDQUFDLFVBQWtCO1FBQ2pDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDcEMsT0FBTyxJQUFJLFVBQVUsQ0FDakIsUUFBUSxDQUFDLEVBQUU7WUFDUCxNQUFNLE1BQU0sR0FBRztnQkFDWCxjQUFjLEVBQUU7b0JBQ1osRUFBRSxFQUFFLFVBQVU7aUJBQ2pCO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLG9EQUFvRCxFQUFFLGVBQWUsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQ2xILHNCQUFzQixDQUFDLEVBQUU7Z0JBQ3JCLE1BQU0sZUFBZSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDdkYsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUM5RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNyRDtxQkFBTTtvQkFDSCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUM3RyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM1QztnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3RDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQ0QsSUFBSSxDQUFDLEVBQUU7Z0JBQ0gsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsK0NBQStDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDaEksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QixDQUFDLENBQ0osQ0FBQztRQUNOLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQTZCSztJQUVMLGlCQUFpQixDQUFDLGNBQXVCO1FBQ3JDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxpQkFBaUIsR0FBRztnQkFDdEIsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUztnQkFDcEMsY0FBYyxFQUFFLGNBQWM7Z0JBQzlCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZTthQUN4QyxDQUFDO1lBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDL0MsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0JBQy9CLElBQUkscUJBQXFCLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ3ZDLHFCQUFxQixHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztxQkFDbkQ7b0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcscUJBQXFCLENBQUM7b0JBQ3RELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUU7d0JBQ25DLDRDQUE0Qzt3QkFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUN4RSxPQUFPLElBQUksS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQzt3QkFDdEQsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsTUFBTSxxQkFBcUIsR0FBRyxFQUFFLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDckMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFOzRCQUNuRixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7eUJBQzVDO3dCQUNELE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDOUMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFOzRCQUNwRCxxQkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0NBQ3ZCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQ0FDZixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pCLFdBQVcsRUFBRSxLQUFLOzZCQUNyQixDQUFDLENBQUM7eUJBQ047b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEdBQUcscUJBQXFCLENBQUM7b0JBQ2pGLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLHFCQUFxQixDQUFDO2lCQUVoRjtxQkFBTTtvQkFDSCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ2YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFOzRCQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7eUJBQ2pFOzZCQUFNOzRCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQzt5QkFDN0Q7cUJBQ0o7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7d0JBQzlELElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztxQkFDN0Q7aUJBQ0o7Z0JBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQzthQUM3QyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDO2lCQUNqRCxTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNaLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7b0JBQ2hFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO3dCQUM5QixRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQzs2QkFDbEgsU0FBUyxDQUFDLEdBQUcsRUFBRTs0QkFDWixJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUM7NEJBQ3pFLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxjQUFjLENBQUM7NEJBQzlELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUU7Z0NBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDOzZCQUM3QjtpQ0FBTTtnQ0FDSCxNQUFNLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGFBQWE7dUNBQzlGLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0NBQ2hKLFFBQVEsQ0FBQztvQ0FDTCxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO29DQUMxRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztvQ0FDNUIsSUFBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO2lDQUM1RCxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7b0NBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0NBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUNsQyxhQUFhLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTt3Q0FDM0MsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRTs0Q0FDMUssSUFBSSxDQUFDLHVCQUF1QixHQUFHLE9BQU8sQ0FBQzt5Q0FDMUM7b0NBQ0wsQ0FBQyxDQUFDLENBQUM7b0NBQ0gsYUFBYSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7d0NBQzVDLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxlQUFlLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUU7NENBQ3RMLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxPQUFPLENBQUM7eUNBQzFDO29DQUNMLENBQUMsQ0FBQyxDQUFDO29DQUNILElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dDQUM5QixDQUFDLEVBQUUsR0FBRyxFQUFFO29DQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzlCLENBQUMsQ0FBQyxDQUFDOzZCQUNOO3dCQUNMLENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsQ0FBQyxDQUFDO2lCQUVOO1lBQ0wsQ0FBQyxFQUFFLEdBQUcsRUFBRTtnQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUM1SCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFFLEdBQUcsRUFBRTtZQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUscURBQXFELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN0SSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFJO1FBQ2QsTUFBTSxTQUFTLEdBQUc7WUFDZCxzQkFBc0IsRUFBRSxLQUFLO1lBQzdCLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsdUJBQXVCLEVBQUUsS0FBSztZQUM5QixlQUFlLEVBQUUsS0FBSztZQUN0Qix5QkFBeUIsRUFBRSxLQUFLO1lBQ2hDLG1CQUFtQixFQUFFLEtBQUs7U0FDN0IsQ0FBQztRQUVGLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBRTtZQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUN0RCxTQUFTLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDO2dCQUMxQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO2dCQUNwQyxTQUFTLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztnQkFDbEMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztnQkFDckMsU0FBUyxDQUFDLHlCQUF5QixHQUFHLEtBQUssQ0FBQztnQkFDNUMsU0FBUyxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQzthQUMzQztTQUNKO2FBQU07WUFDSCxTQUFTLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1lBQ3pDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDcEMsU0FBUyxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztZQUMxQyxTQUFTLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1lBQ3RDLFNBQVMsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQ2pDLFNBQVMsQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7U0FDL0M7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRUQsa0JBQWtCLENBQUMsYUFBYTtRQUM1QixJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxhQUFhLEVBQUU7Z0JBQ3RDLGVBQWUsR0FBRyxJQUFJLENBQUM7YUFDMUI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFFRCxrQkFBa0I7UUFDZCxNQUFNLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1FBQy9FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQztRQUM3RSxNQUFNLE9BQU8sR0FBRyxlQUFlLENBQUMsbUJBQW1CLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFO1lBQ2xDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDMUIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUM5SixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDekosWUFBWSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO29CQUN0SCxNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDOzRCQUM5RixJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJO3FCQUM5SixDQUNBO29CQUNELEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM3RCxJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDNUQsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzs0QkFDcEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ2hLLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdkIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsWUFBWTtxQkFDakYsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNyQixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDO3FCQUN4RyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDakcsc0JBQXNCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDcEosV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3RFLGlCQUFpQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3pFLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUNuRSxtQkFBbUIsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUMzRSxvQkFBb0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM1RSxVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztpQkFDckUsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMxQztpQkFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkwsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzlLLDJGQUEyRjtnQkFDM0YseUNBQXlDO2dCQUN6Qyx3QkFBd0I7Z0JBQ3hCLG9DQUFvQztnQkFDcEMsSUFBSTtnQkFDSixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQztnQkFDL0MsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0MsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDcEosMkhBQTJIO2dCQUMzSCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxPQUFPLElBQUksQ0FBQyxjQUFjLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDakgsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDMUIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUNsRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLE9BQU8sSUFBSSxDQUFDLGFBQWEsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3ZFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7cUJBQ25ELEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxZQUFZLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzFCLEtBQUssRUFBRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLO3dCQUN6RyxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO3FCQUNuRCxDQUFDO29CQUNGLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3hHLFFBQVEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQjtxQkFDdEgsQ0FBQztvQkFDRixLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ25CLEtBQUssRUFBRSxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWE7d0JBQ2pFLFFBQVEsRUFBRSxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUN4SSxDQUFDO29CQUNGLElBQUksRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDbEIsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEVBQUUsUUFBUSxFQUFFLFlBQVk7NEJBQ3pELENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDbEgsQ0FBQztvQkFDRixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ3BILFFBQVEsRUFBRSxZQUFZO3FCQUN6QixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZOzRCQUM5SCxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQztxQkFDbkgsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMxSCxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUN2SyxzQkFBc0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUM7b0JBQ25JLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUNwSCxpQkFBaUIsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUNsRyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3BFLDJKQUEySjtvQkFDM0osbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2pDLEtBQUssRUFBRSxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ2pFLFFBQVEsRUFBRSxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUN4SSxDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDckUsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN4QixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxRQUFRLEVBQUUsWUFBWTs0QkFDL0MsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUNsSCxDQUFDO2lCQUNMLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDN0MsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixFQUFFO29CQUMxQyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQzNKLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQztpQkFDaEQ7Z0JBQ0QsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7Z0JBQ25DLDBCQUEwQjtnQkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FDakUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ2pKLElBQUksQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUMxQztTQUNKO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUM7Z0JBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxTQUFTLENBQUM7b0JBQzFCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDOUosTUFBTTtvQkFDTjs7d0JBRUk7b0JBQ0osUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzFNLFlBQVksRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztvQkFDdkssTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7d0JBQy9HLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQzVJLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM3RCxJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDNUQsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzs0QkFDcEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ2hLLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNqRyxzQkFBc0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUNwSixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDdEUsaUJBQWlCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDekUsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ25FLG1CQUFtQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzNFLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzVFLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO2lCQUNyRSxDQUFDLENBQUM7YUFDTjtpQkFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUMxQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdkwsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBRTlLLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDO2dCQUMvQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNwSixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsSUFBSSxPQUFPLElBQUksQ0FBQyxjQUFjLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDakgsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDMUIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUNsRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLE1BQU07b0JBQ047Ozs7Ozs7MEJBT007b0JBQ04sUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsT0FBTyxJQUFJLENBQUMsYUFBYSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDdkUsUUFBUSxFQUFFLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztxQkFDcEcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2pFLFlBQVksRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDMUIsS0FBSyxFQUFFLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7d0JBQ3pHLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7cUJBQ3BHLENBQUM7b0JBQ0YsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNwQixLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDeEcsUUFBUSxFQUFFLElBQUk7cUJBQ2pCLENBQUM7b0JBQ0YsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNuQixLQUFLLEVBQUUsU0FBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhO3dCQUNqRSxRQUFRLEVBQUUsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDeEksQ0FBQztvQkFDRixJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2xCLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZOzRCQUN6RCxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7cUJBQ2xILENBQUM7b0JBQ0YsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUNwSCxRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDdkssc0JBQXNCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO29CQUNuSSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDcEgsaUJBQWlCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDbEcsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwRSxtQkFBbUIsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDakMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDakUsUUFBUSxFQUFFLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7cUJBQ3hJLENBQUM7b0JBQ0Ysb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUNyRSxVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxZQUFZOzRCQUMvQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7cUJBQ2xILENBQUM7aUJBRUwsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUM3QyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLEVBQUU7b0JBQzFDLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDM0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDO2lCQUNoRDtnQkFDRCxJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztnQkFDbkMsMEJBQTBCO2dCQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUNqRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUNwSjtTQUNKO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDaEYsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLEdBQUcsQ0FBQyxDQUFDLFFBQXVCLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFN0csSUFBSSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDdkUsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUN4RixJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUM7UUFDbEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUM7UUFDbEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1FBQzVGLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDaEYsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDcEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzFDO1lBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUNsRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN4RDtZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDbEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDeEQ7WUFDRDs7Ozs7Z0JBS0k7U0FDUDtRQUVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3BFLGlDQUFpQztZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksT0FBTyxXQUFXLEtBQUssUUFBUSxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFO2dCQUMzRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXLENBQUMsS0FBSyxFQUFFO3dCQUMxQzs7MkJBRUc7d0JBQ0gsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBQ3ZILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQzt3QkFDN0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDO3dCQUM3RSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUU7NEJBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7NEJBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQzFGO3FCQUNKO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUNyRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQztnQkFDM0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDO2dCQUM3RSxJQUFJLG1CQUFtQixDQUFDO2dCQUN4QixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO29CQUNoQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsQ0FBQztpQkFDeko7cUJBQU07b0JBQ0gsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLENBQUM7aUJBQ3JKO2dCQUNELElBQUksbUJBQW1CLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztpQkFDcEU7Z0JBQ0QsMENBQTBDO2dCQUMxQyxrR0FBa0c7Z0JBQ2xHLDhGQUE4RjtnQkFDOUYsSUFBSTthQUNQO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDN0QsSUFBSSxVQUFVLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUU7b0JBQ2xGLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLEVBQUU7d0JBQ25FLElBQUksYUFBYSxDQUFDLEtBQUssS0FBSyxVQUFVLENBQUMsS0FBSyxFQUFFOzRCQUMxQyxJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQzt5QkFDdEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU07b0JBQ0gsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztvQkFDbkMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLENBQUM7b0JBQ3pILElBQUksYUFBYSxFQUFFO3dCQUNmLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7cUJBQ3hEO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7WUFDM0UsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDNUM7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM3QztZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMzRDtZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzdDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM1RDtZQUNELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3pFLENBQUMsQ0FBQyxDQUFDO1FBRUg7O2NBRU07UUFFTixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0RCxJQUFJLElBQUksSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0gsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLENBQUM7Z0JBQ3ZILElBQUksWUFBWSxFQUFFO29CQUNkLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ3REO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtnQkFDekQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQkFDOUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBRTt3QkFDbkUsSUFBSSxhQUFhLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7NEJBQ3pELElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO3lCQUN0QztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO2lCQUN0QztnQkFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQzFKLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDO2lCQUM3RTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQztpQkFDekU7Z0JBRUQsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzFHLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDO2lCQUN6RTtnQkFFRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2lCQUMzQjtnQkFFRCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQzFKLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDO2lCQUM3RTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQztpQkFDekU7WUFHTCxDQUFDLENBQUMsQ0FBQztZQUtILElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtnQkFDdkQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQkFDOUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBRTt3QkFDbkUsSUFBSSxhQUFhLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7NEJBQ3pELElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO3lCQUN0QztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO2lCQUN0QztnQkFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQUU7b0JBQ3BKLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDO2lCQUM3RTtxQkFBTSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQzFHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDO2lCQUN2RTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQztpQkFDekU7WUFJTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUNyRixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUV4SixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDM0I7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUN6RixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUV4SixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDM0I7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBR0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzdDLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQXNCSTtJQUVKLGdCQUFnQjtRQUNaLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7WUFDMUUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLEdBQUcsRUFBRTtnQkFDMUYsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUM7YUFDN0U7aUJBQU07Z0JBQ0gsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzdULElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUN6RCxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLEVBQUU7b0JBQ3JILElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ3BILE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxFQUNoSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMvRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDM0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFLENBQUM7d0JBQ3pELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDO3FCQUNqRjtpQkFDSjthQUNKO1NBQ0o7SUFDTCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSx5QkFBeUI7UUFFMUYsSUFBSSxtQkFBbUIsS0FBSyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDNUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMxQztZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ3hEO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBRWpFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUV0RCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzthQUN2RTtZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFLElBQUkseUJBQXlCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3pFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM3RDtTQUNKO2FBQU0sSUFBSSxtQkFBbUIsS0FBSyxhQUFhLENBQUMsb0JBQW9CLEVBQUU7WUFDbkUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztnQkFDbEYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RFLElBQUkseUJBQXlCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7b0JBQ3pFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDN0Q7Z0JBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUM5QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFFckQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtvQkFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ2hGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ3ZFO3FCQUFNO29CQUNILElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDbEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ3pEO2dCQUVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUM5QixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ3RFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDekM7aUJBQ0o7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDNUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO3dCQUN0RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDdkQ7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJO1FBQ2YsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNwQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUN0RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ3pDO2FBQ0o7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUM1QyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3RFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO2lCQUN2RDthQUNKO1lBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztnQkFDbEYsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7b0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQy9DO2dCQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7b0JBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDN0Q7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDMUM7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUM1QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDMUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDeEQ7U0FDSjtJQUNMLENBQUM7SUFFRCxxQkFBcUI7UUFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNoRCxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUNqRixTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDaEUsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsSUFBSTtRQUNyQixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQy9DO0lBQ0wsQ0FBQztJQUVELGVBQWUsQ0FBQyxNQUFNO1FBQ2xCLElBQUksTUFBTSxFQUFFO1lBQ1IsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxDQUFDO1lBQzNILE9BQU8sbUJBQW1CLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDekQ7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsY0FBYyxDQUFDLFVBQVU7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQzdDLE1BQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxVQUFVO2lCQUNyQixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN2RSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2pHLElBQUksWUFBWSxFQUFFO3dCQUNkLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO3FCQUNyQztvQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDbkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWEsQ0FBQyxVQUFVO1FBQ3BCLElBQUksVUFBVSxFQUFFO1lBQ1osTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDLENBQUM7WUFDeEYsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUM3QzthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFFRCwyQkFBMkI7SUFDM0IsQ0FBQztJQUVELGtCQUFrQjtRQUNkLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7WUFDeEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0gsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzNELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxFQUFFLGlDQUFpQztvQkFDMUMsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNyQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3ZDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsVUFBVTtRQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUU7WUFDMUcsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUgsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQzdCLE9BQU87YUFDVjtTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQy9DLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDeEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPO1NBQ1Y7UUFFRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzFELElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDM0IsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNoSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pDLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLE1BQU0sa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBRWpDLElBQUksa0JBQWtCLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxRQUFRO2FBQ3RELENBQUMsQ0FBQztZQUNILE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNFQUFzRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdEosSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPO1NBQ1Y7UUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUNyRCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNsSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUNoQztpQkFBTTtnQkFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDdkYsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUV0QztTQUNKO2FBQU07WUFDSCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2xILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7U0FDaEM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNYLE1BQU0sY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUMxQixNQUFNLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUU1QixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQ2hELElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN4QyxNQUFNLGtCQUFrQixHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDdEcsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDM0IsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDcEM7aUJBQ0o7cUJBQU07b0JBQ0gsTUFBTSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEtBQUssV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUM3SSxJQUFJLHdCQUF3QixLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUNqQyxNQUFNLGtCQUFrQixHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdEcsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTs0QkFDM0IsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt5QkFDcEM7cUJBQ0o7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLGFBQWEsRUFBRSxFQUFFO2dCQUN4RCxPQUFPO29CQUNILGFBQWEsRUFBRSxFQUFFO29CQUNqQixRQUFRLEVBQUU7d0JBQ04sRUFBRSxFQUFFLEVBQUU7d0JBQ04sTUFBTSxFQUFFLEVBQUU7d0JBQ1YsTUFBTSxFQUFFLGFBQWEsQ0FBQyxLQUFLO3dCQUMzQixNQUFNLEVBQUUsS0FBSztxQkFDaEI7b0JBQ0QsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTTtvQkFDOUIsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLElBQUk7aUJBRWxDLENBQUM7WUFDTixDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUM5QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUM1QyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDbEMsTUFBTSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNqSixJQUFJLHdCQUF3QixJQUFJLENBQUMsRUFBRTt3QkFDL0IsTUFBTSxhQUFhLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3ZHLElBQUksYUFBYSxLQUFLLENBQUMsQ0FBQyxFQUFFOzRCQUN0QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7eUJBQzFDO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDekgsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDM0IsTUFBTSxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDOUcsTUFBTSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ25ILElBQUksb0JBQW9CLEtBQUssQ0FBQyxDQUFDLElBQUksd0JBQXdCLElBQUksQ0FBQyxFQUFFOzRCQUM5RCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7eUJBQzFDO3FCQUNKO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ3pELE9BQU87b0JBQ0gsYUFBYSxFQUFFLEVBQUU7b0JBQ2pCLFFBQVEsRUFBRTt3QkFDTixFQUFFLEVBQUUsRUFBRTt3QkFDTixNQUFNLEVBQUUsRUFBRTt3QkFDVixNQUFNLEVBQUUsUUFBUSxDQUFDLEtBQUs7d0JBQ3RCLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtvQkFDRCxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNO29CQUM5QixvQkFBb0IsRUFBRSxJQUFJLENBQUMsSUFBSTtpQkFFbEMsQ0FBQztZQUNOLENBQUMsQ0FBQyxDQUFDO1NBRU47UUFFRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUM3RCxPQUFPO2dCQUNILEVBQUUsRUFBRSxFQUFFO2dCQUNOLE1BQU0sRUFBRSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxRQUFRLENBQUMsS0FBSzthQUN6QixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM3SixDQUFDO0lBQ0QsaUJBQWlCLENBQUMsVUFBVTtRQUN4QixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25ELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixNQUFNLFVBQVUsR0FBa0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFDeEosSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDL0QsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7ZUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BELFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUN2RSxVQUFVLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsd0JBQXdCLENBQUM7Z0JBQzFILENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLHdCQUF3QixDQUFDO1lBQ3RFLFVBQVUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDO2dCQUNuRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUM7WUFDMUQsVUFBVSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO2dCQUMvRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO1NBQ3pDO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELFFBQVEsQ0FBQyxVQUFVLEVBQUUsVUFBVTtRQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUU7WUFDbkMsVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzlGLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO2dCQUMzSCxVQUFVLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDakMsTUFBTSxVQUFVLEdBQUc7b0JBQ2YsVUFBVSxFQUFFLFVBQVU7b0JBQ3RCLG9CQUFvQixFQUFFO3dCQUNsQixtQkFBbUIsRUFBRSxJQUFJLENBQUMsWUFBWTtxQkFDekM7aUJBQ0osQ0FBQztnQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ3pELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO29CQUM3QixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3QkFDL0UsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDOUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDekMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzs0QkFDMUIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUzs0QkFDL0YsVUFBVSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxVQUFVO3lCQUN0RSxDQUFDLENBQUM7cUJBQ047eUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLOzJCQUMvRyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO3dCQUNwRixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDeEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7eUJBQU07d0JBQ0gsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDaEksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7Z0JBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDBDQUEwQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdDLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7YUFDaEM7U0FDSjthQUFNO1lBQ0gsTUFBTSxVQUFVLEdBQUc7Z0JBQ2YsVUFBVSxFQUFFLFVBQVU7YUFDekIsQ0FBQztZQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztpQkFDbEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztnQkFDN0IsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJO3VCQUN2RyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ2hJLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQzVHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO29CQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDO3dCQUMxQixNQUFNLEVBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTO3dCQUMvRixVQUFVLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLFVBQVU7cUJBQ3RFLENBQUMsQ0FBQztpQkFDTjtxQkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7dUJBQy9HLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7b0JBQ3BGLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUN4SCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM1QztxQkFBTTtvQkFDSCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSw2Q0FBNkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUNoSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM1QztZQUNMLENBQUMsRUFBRSxHQUFHLEVBQUU7Z0JBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDOUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7U0FDVjtJQUNMLENBQUM7SUFFRCxvQkFBb0I7UUFDaEIsSUFBSSxDQUFDLGVBQWUsR0FBRztZQUNuQixZQUFZLEVBQUUsS0FBSztZQUNuQixjQUFjLEVBQUUsSUFBSTtZQUNwQixxQkFBcUIsRUFBRSxJQUFJO1lBQzNCLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsV0FBVyxFQUFFLElBQUk7WUFDakIsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUTtZQUNsQyxlQUFlLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlO1lBQ2hELGFBQWEsRUFBRSxJQUFJO1lBQ25CLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLG9CQUFvQixFQUFFLElBQUk7WUFDMUIsT0FBTyxFQUFFLEVBQUU7WUFDWCxPQUFPLEVBQUUsRUFBRTtZQUNYLFNBQVMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUNsRCxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNO1lBQzlCLFlBQVksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVk7WUFDMUMsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVO1lBQ3RDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVM7WUFDcEMsZUFBZSxFQUFFO2dCQUNiLEtBQUssRUFBRSxZQUFZO2dCQUNuQixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxjQUFjLEVBQUU7Z0JBQ1osS0FBSyxFQUFFLE1BQU07Z0JBQ2IsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsaUJBQWlCLEVBQUU7Z0JBQ2YsS0FBSyxFQUFFLGFBQWE7Z0JBQ3BCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELG1CQUFtQixFQUFFO2dCQUNqQixLQUFLLEVBQUUsc0JBQXNCO2dCQUM3QixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxtQkFBbUIsRUFBRTtnQkFDakIsS0FBSyxFQUFFLHNCQUFzQjtnQkFDN0IsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QscUJBQXFCLEVBQUU7Z0JBQ25CLEtBQUssRUFBRSxrQkFBa0I7Z0JBQ3pCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELG1CQUFtQixFQUFFLElBQUk7U0FDNUIsQ0FBQztJQUNOLENBQUM7SUFFRCxRQUFRO1FBQ0osTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDckksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3RJLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQy9KLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDMUssSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0ksSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUNySSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9DLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxxQkFBcUIsRUFBRTtnQkFDckMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7YUFDcEI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILG9FQUFvRTtRQUNwRSxJQUFJLFNBQVMsQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUM5QyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUM1QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDO0NBYUosQ0FBQTs7WUExbEQwQixVQUFVO1lBQ04sY0FBYztZQUNyQixXQUFXO1lBQ1AsV0FBVztZQUNYLFdBQVc7WUFDSCxtQkFBbUI7WUFDekIsYUFBYTtZQUNwQixTQUFTO1lBQ0csa0JBQWtCO1lBQ2pCLG1CQUFtQjtZQUNuQixtQkFBbUI7O0FBMUYxQztJQUFSLEtBQUssRUFBRTt5REFBaUM7QUFDL0I7SUFBVCxNQUFNLEVBQUU7bUVBQWdEO0FBQy9DO0lBQVQsTUFBTSxFQUFFO2tFQUErQztBQUM5QztJQUFULE1BQU0sRUFBRTtrRUFBK0M7QUFrQzVCO0lBQTNCLFNBQVMsQ0FBQyxlQUFlLENBQUM7NERBQTJCO0FBd0NuQztJQUFsQixTQUFTLENBQUMsTUFBTSxDQUFDOzhEQUFrQztBQS9FM0MscUJBQXFCO0lBTGpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxtQkFBbUI7UUFDN0Isb3NWQUE2Qzs7S0FFaEQsQ0FBQztHQUNXLHFCQUFxQixDQTRxRGpDO1NBNXFEWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uLy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBWYWxpZGF0b3JzLCBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY29yZSc7XHJcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IFRhc2tVcGRhdGVPYmogfSBmcm9tICcuL3Rhc2sudXBkYXRlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2F2ZU9wdGlvbkNvbnN0YW50IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zYXZlLW9wdGlvbnMuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgUHJvamVjdFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza0NvbmZpZ0ludGVyZmFjZSB9IGZyb20gJy4uL3Rhc2suY29uZmlnLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQ29tbWVudHNVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2NvbW1lbnRzL3NlcnZpY2VzL2NvbW1lbnRzLnV0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEVOVEVSLCBDT01NQSwgU0VNSUNPTE9OLCBTUEFDRSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7IG1hcCwgc3RhcnRXaXRoLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0Q2hpcElucHV0RXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jaGlwcyc7XHJcbmltcG9ydCB7IE5vZGVXaXRoSTE4biB9IGZyb20gJ0Bhbmd1bGFyL2NvbXBpbGVyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXRhc2stY3JlYXRpb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Rhc2stY3JlYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdGFzay1jcmVhdGlvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYXNrQ3JlYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHRhc2tDb25maWc6IFRhc2tDb25maWdJbnRlcmZhY2U7XHJcbiAgICBAT3V0cHV0KCkgY2xvc2VDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzYXZlQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgbm90aWZpY2F0aW9uSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuXHJcbiAgICB0YXNrRm9ybTogRm9ybUdyb3VwO1xyXG5cclxuICAgIHRhc2tNb2RhbENvbmZpZztcclxuICAgIHRlYW1Sb2xlc09wdGlvbnM7XHJcbiAgICB0ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgdGFza1N0YXR1cztcclxuICAgIHNlbGVjdGVkT3duZXI7XHJcbiAgICBpc0ludmFsaWRUYXNrT3BlcmF0aW9uID0gZmFsc2U7XHJcbiAgICBvd25lclByb2plY3RDb3VudCA9IGZhbHNlO1xyXG4gICAgb3duZXJQcm9qZWN0Q291bnRNZXNzYWdlID0gJyc7XHJcbiAgICBpc1plcm9Pd25lclByb2plY3RDb3VudCA9IGZhbHNlO1xyXG4gICAgc2hvd1JldmlzaW9uUmV2aWV3UmVxdWlyZWQ6IGJvb2xlYW47XHJcbiAgICBkaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgIGV4aXN0aW5nVGFza1R5cGVEZXRhaWxzID0gbnVsbDtcclxuICAgIC8vIGV4aXN0aW5nQXNzaWdubWVudFR5cGVEZXRhaWxzID0gbnVsbDtcclxuICAgIHRhc2tDb25zdGFudHMgPSBUYXNrQ29uc3RhbnRzO1xyXG4gICAgc2VsZWN0ZWRTdGF0dXNJbmZvO1xyXG4gICAgb2xkVGFza09iamVjdDogVGFza1VwZGF0ZU9iajtcclxuICAgIGRhdGVWYWxpZGF0aW9uOiB7XHJcbiAgICAgICAgc3RhcnRNYXhEYXRlOiBEYXRlO1xyXG4gICAgICAgIGVuZE1pbkRhdGU6IERhdGU7XHJcbiAgICB9O1xyXG4gICAgbWF4RHVyYXRpb247XHJcbiAgICBkdXJhdGlvbkVycm9yTWVzc2FnZTtcclxuXHJcbiAgICBkZWZhdWx0UmV2aXNpb25SZXF1aXJlZFN0YXR1czogYm9vbGVhbjtcclxuICAgIHByb2plY3RDb25maWc6IGFueTtcclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgbmFtZVN0cmluZ1BhdHRlcm47XHJcbiAgICBhbGxvd0FjdGlvblRhc2s7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgncmV2aWV3ZXJJbnB1dCcpIHJldmlld2VySW5wdXQ6IEVsZW1lbnRSZWY7XHJcbiAgICBzYXZlZFJldmlld2VycyA9IFtdO1xyXG4gICAgYWxsUmV2aWV3ZXJzID0gW107XHJcbiAgICAvLyByZXZpZXdlcnMgPSBbXTtcclxuICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzID0gW107XHJcbiAgICAvLyByZXZpZXdlckRldGFpbHMgPSBbXTtcclxuICAgIHJldmlld2VyU3RhdHVzRGF0YSA9IFtdO1xyXG4gICAgZmlsdGVyZWRSZXZpZXdlcnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gICAgbmV3cmV2aWV3ZXJzT2JqO1xyXG4gICAgcmVtb3ZlZHJldmlld2Vyc09iajtcclxuICAgIHJldmlld2Vyc09iajtcclxuICAgIHNlbGVjdGVkUmV2aWV3ZXJzT2JqO1xyXG4gICAgdmlzaWJsZSA9IHRydWU7XHJcbiAgICBzZWxlY3RhYmxlID0gdHJ1ZTtcclxuICAgIHJlbW92YWJsZSA9IHRydWU7XHJcbiAgICBhZGRPbkJsdXIgPSBmYWxzZTtcclxuICAgIHNlcGFyYXRvcktleXNDb2RlcyA9IFtFTlRFUiwgQ09NTUFdO1xyXG4gICAgbmV3UmV2aWV3ZXJzID0gW107XHJcbiAgICByZW1vdmVkUmV2aWV3ZXJzID0gW107XHJcbiAgICBsb2dnZWRJblVzZXI7XHJcbiAgICBkYXRlRXJyb3JNZXNzYWdlO1xyXG4gICAgaXNQTSA9IGZhbHNlO1xyXG4gICAgY2hpcENoYW5nZWQ7XHJcbiAgICBzYXZlT3B0aW9ucyA9IFNhdmVPcHRpb25Db25zdGFudC50YXNrU2F2ZU9wdGlvbnM7XHJcbiAgICBTRUxFQ1RFRF9TQVZFX09QVElPTiA9IHtcclxuICAgICAgICBuYW1lOiAnU2F2ZScsXHJcbiAgICAgICAgdmFsdWU6ICdTQVZFJ1xyXG4gICAgfTtcclxuICAgIER1cmF0aW9uVHlwZSA9IFtcclxuICAgICAgICB7IHZhbHVlOiAnZGF5cycsIHZpZXdWYWx1ZTogJ0RheXMnIH0sXHJcbiAgICAgICAgeyB2YWx1ZTogJ3dlZWtzJywgdmlld1ZhbHVlOiAnV2Vla3MnIH1cclxuICAgIF07XHJcbiAgICBlbmFibGVEdXJhdGlvbiA9IGZhbHNlO1xyXG4gICAgZW5hYmxlV29ya1dlZWsgPSBmYWxzZTtcclxuICAgIHJldmlld2VyQ29tcGxldGVkO1xyXG5cclxuICAgIGtleVJlc3RyaWN0aW9uID0ge1xyXG4gICAgICAgIE5VTUJFUjogWzQ2LCA2OSwgMTg2LCAxODcsIDE4OCwgMTA3XVxyXG4gICAgfTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdhdXRvJykgbWF0QXV0b2NvbXBsZXRlOiBNYXRBdXRvY29tcGxldGU7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYWRhcHRlcjogRGF0ZUFkYXB0ZXI8YW55PixcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcGRlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0VXRpbFNlcnZpY2U6IFByb2plY3RVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgY29tbWVudHNVdGlsU2VydmljZTogQ29tbWVudHNVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgKSB7XHJcblxyXG5cclxuICAgIH1cclxuXHJcbiAgICBmaWx0ZXJSZXZpZXdlcnModmFsdWU6IGFueSk6IGFueVtdIHtcclxuICAgICAgICBpZiAodmFsdWUgJiYgdHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJyAmJiB2YWx1ZS50cmltKCkgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlclZhbHVlID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFsbFJldmlld2Vycy5maWx0ZXIocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKGZpbHRlclZhbHVlKSA9PT0gMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlc3RyaWN0S2V5c09uVHlwZShldmVudCwgZGF0YXR5cGUpIHtcclxuICAgICAgICBpZiAodGhpcy5rZXlSZXN0cmljdGlvbltkYXRhdHlwZV0gJiYgdGhpcy5rZXlSZXN0cmljdGlvbltkYXRhdHlwZV0uaW5jbHVkZXMoZXZlbnQua2V5Q29kZSkpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VsZWN0ZWQoZXZlbnQ6IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCByZXZpZXdlckluZGV4ID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kSW5kZXgoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSBldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IHJldmlld2VyID0gdGhpcy5hbGxSZXZpZXdlcnMuZmluZChyZXZpZXdlckRhdGEgPT4gcmV2aWV3ZXJEYXRhLnZhbHVlID09PSBldmVudC5vcHRpb24udmFsdWUpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy50YXNrRm9ybS52YWx1ZS5kZWxpdmVyYWJsZUFwcHJvdmVyICYmXHJcbiAgICAgICAgICAgIGV2ZW50Lm9wdGlvbi52YWx1ZSA9PT0gdGhpcy50YXNrRm9ybS52YWx1ZS5kZWxpdmVyYWJsZUFwcHJvdmVyLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihyZXZpZXdlci5kaXNwbGF5TmFtZSArICcgaXMgYWxyZWFkeSB0aGUgQXBwcm92ZXInKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHJldmlld2VySW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IocmV2aWV3ZXIuZGlzcGxheU5hbWUgKyAnIGlzIGFscmVhZHkgc2VsZWN0ZWQnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVkUmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXIudmFsdWUsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMubmV3UmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXIudmFsdWUsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlbW92ZVJldmlld2VySW5kZXggPSB0aGlzLnJlbW92ZWRSZXZpZXdlcnMuZmluZEluZGV4KHJlbW92ZWRSZXZpZXdlciA9PiByZW1vdmVkUmV2aWV3ZXIudmFsdWUgPT09IGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcbiAgICAgICAgICAgIGlmIChyZW1vdmVSZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVtb3ZlZFJldmlld2Vycy5zcGxpY2UocmVtb3ZlUmV2aWV3ZXJJbmRleCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5yZXZpZXdlclN0YXR1c0RhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiBmYWxzZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5yZXZpZXdlcklucHV0Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSAnJztcclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYWRkUmV2aWV3ZXIoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm1hdEF1dG9jb21wbGV0ZS5pc09wZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgaW5wdXQgPSBldmVudC5pbnB1dDtcclxuXHJcbiAgICAgICAgICAgIGlmIChpbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25SZW1vdmVSZXZpZXdlcihyZXZpZXdlclZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZEluZGV4KHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgY29uc3QgbmV3UmV2aWV3ZXJJbmRleCA9IHRoaXMubmV3UmV2aWV3ZXJzLmZpbmRJbmRleChuZXdSZXZpZXdlciA9PiBuZXdSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgY29uc3QgcmV2aWV3ZXJJbmRleCA9IHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLmZpbmRJbmRleChyZXZpZXdlckRhdGEgPT4gcmV2aWV3ZXJEYXRhLnZhbHVlID09PSByZXZpZXdlclZhbHVlKTtcclxuICAgICAgICBjb25zdCByZXZpZXdlciA9IHRoaXMuYWxsUmV2aWV3ZXJzLmZpbmQocmV2aWV3ZXJEYXRhID0+IHJldmlld2VyRGF0YS52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5tYXJrQXNEaXJ0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVkUmV2aWV3ZXJzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChyZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5yZXZpZXdlclN0YXR1c0RhdGEuc3BsaWNlKHJldmlld2VySW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobmV3UmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMubmV3UmV2aWV3ZXJzLnNwbGljZShuZXdSZXZpZXdlckluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdERldGFpbHMocHJvamVjdE9iaik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHByb2plY3RPYmogJiYgcHJvamVjdE9ialsnUHJvamVjdC1pZCddICYmIHByb2plY3RPYmpbJ1Byb2plY3QtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHByb2plY3RPYmouUl9QT19DQVRFR09SWSAmJiBwcm9qZWN0T2JqLlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuY2F0ZWdvcnlJZCA9IHByb2plY3RPYmouUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvamVjdE9iai5SX1BPX1RFQU0gJiYgcHJvamVjdE9iai5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkID0gcHJvamVjdE9iai5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0ID0gcHJvamVjdE9iajtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRVU2VySUQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFVTZXJJRCAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QgJiZcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VVNlcklEID09PSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuaXNQcm9qZWN0T3duZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmlvcml0aWVzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgLyogIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKE1QTV9MRVZFTFMuVEFTSylcclxuICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHByaW9yaXR5UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICBpZiAocHJpb3JpdHlSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHByaW9yaXR5UmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHlSZXNwb25zZSA9IFtwcmlvcml0eVJlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3QgPSBwcmlvcml0eVJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBQcmlvcml0aWVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgICAgIGxldCBwcmlvcml0eSA9IHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKE1QTV9MRVZFTFMuVEFTSylcclxuICAgICAgICAgICAgLy8gLnN1YnNjcmliZShwcmlvcml0eVJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgaWYgKHByaW9yaXR5KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocHJpb3JpdHkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHkgPSBbcHJpb3JpdHldO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdCA9IHByaW9yaXR5O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdCA9IFtdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIC8qICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUHJpb3JpdGllcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U3RhdHVzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbChNUE1fTEVWRUxTLlRBU0spXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN0YXR1c1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHN0YXR1c1Jlc3BvbnNlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzUmVzcG9uc2UgPSBbc3RhdHVzUmVzcG9uc2VdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tTdGF0dXNMaXN0ID0gc3RhdHVzUmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEluaXRpYWxTdGF0dXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzKE1QTV9MRVZFTFMuVEFTSywgU3RhdHVzVHlwZXMuSU5JVElBTClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3RhdHVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXNSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoc3RhdHVzUmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNSZXNwb25zZSA9IFtzdGF0dXNSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0ID0gc3RhdHVzUmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBQcmlvcml0aWVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxSb2xlcygpIHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICB0ZWFtSUQ6IHRoaXMudGFza01vZGFsQ29uZmlnLnRlYW1JZCxcclxuICAgICAgICAgICAgaXNBcHByb3ZlclRhc2s6IHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayxcclxuICAgICAgICAgICAgaXNNZW1iZXJUYXNrOiAhdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VGVhbVJvbGVzKGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuVGVhbXMgJiYgcmVzcG9uc2UuVGVhbXMuTVBNX1RlYW1zICYmIHJlc3BvbnNlLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgPSByZXNwb25zZS5UZWFtcy5NUE1fVGVhbXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmcgPSBbcmVzcG9uc2UuTVBNX1RlYW1fUm9sZV9NYXBwaW5nXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcm9sZUxpc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvbGVMaXN0ID0gcmVzcG9uc2UuTVBNX1RlYW1fUm9sZV9NYXBwaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByb2xlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocm9sZUxpc3QgJiYgcm9sZUxpc3QubGVuZ3RoICYmIHJvbGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvbGVMaXN0Lm1hcChyb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb2xlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcm9sZVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByb2xlLlJPTEVfRE4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByb2xlLk5BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcFJvbGVzOiByb2xlLk1QTV9BUFBfUm9sZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVzT3B0aW9ucyA9IHJvbGVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZXNPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVzT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBQcmlvcml0aWVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZXZpZXdlckJ5SWQodGFza1VzZXJJRCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGFza1VzZXJJRCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklkOiB0YXNrVXNlcklEXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWxpdmVyYWJsZVJldmlld0J5VGFza0lkKFRhc2tJZCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXREZWxpdmVyYWJsZVJldmlld0J5VGFza0lEKFRhc2tJZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5EZWxpdmVyYWJsZVJldmlldykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXZpZXdlcnNMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ0RlbGl2ZXJhYmxlUmV2aWV3Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXZpZXdlcnNMaXN0ICYmIHJldmlld2Vyc0xpc3QubGVuZ3RoICYmIHJldmlld2Vyc0xpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV2aWV3ZXJzTGlzdC5mb3JFYWNoKHJldmlld2VyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShyZXZpZXdlci5SX1BPX1JFVklFV0VSX1VTRVJbJ0lkZW50aXR5LWlkJ10uSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UmV2aWV3ZXJCeUlkKHJldmlld2VyLlJfUE9fUkVWSUVXRVJfVVNFUlsnSWRlbnRpdHktaWQnXS5JZCkuc3Vic2NyaWJlKHJldmlld2VyRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaGFzUmV2aWV3ZXJEYXRhID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kKHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJEYXRhLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNSZXZpZXdlckRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZChyZXZpZXdlci5SX1BPX1JFVklFV19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCkuc3Vic2NyaWJlKHN0YXR1c1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpc1Jldmlld2VyQ29tcGxldGVkID0gc3RhdHVzUmVzcG9uc2UuU1RBVFVTX0xFVkVMID09PSAnRklOQUwnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5maW5kSW5kZXgoZGF0YSA9PiBkYXRhLnZhbHVlID09PSByZXZpZXdlckRhdGEudXNlckNOKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhW3Jldmlld2VySW5kZXhdLmlzQ29tcGxldGVkID0gaXNSZXZpZXdlckNvbXBsZXRlZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiBpc1Jldmlld2VyQ29tcGxldGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnNhdmVkUmV2aWV3ZXJzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBEZWxpdmVyYWJsZSBSZXZpZXcgRGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmV2aWV3ZXJzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGdldFJldmlld2VyVXNlclJlcXVlc3QgPSB7fTtcclxuICAgICAgICAgICAgICAgIGdldFJldmlld2VyVXNlclJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1VTRVIsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUROOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICB0ZWFtSUQ6IHRoaXMudGFza01vZGFsQ29uZmlnLnRlYW1JZCxcclxuICAgICAgICAgICAgICAgICAgICBpc0FwcHJvdmVUYXNrOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNSZXZpZXc6IHRydWVcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldFJldmlld2VyVXNlclJlcXVlc3QpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS51c2VycyAmJiByZXNwb25zZS51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VyTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICd1c2VyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHVzZXJMaXN0ICYmIHVzZXJMaXN0Lmxlbmd0aCAmJiB1c2VyTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlckxpc3QuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSB1c2Vycy5maW5kKGUgPT4gZS52YWx1ZSA9PT0gdXNlci5kbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHVzZXIuZG4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHVzZXIuY24sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHVzZXIubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbFJldmlld2VycyA9IHVzZXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxSZXZpZXdlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUmV2aWV3ZXIgVXNlcnMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZ2V0VXNlcnNCeVJvbGUoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICBsZXQgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7fTtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tGb3JtVmFsdWUgPSB0aGlzLnRhc2tGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgICAgICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogdGFza0Zvcm1WYWx1ZS5hbGxvY2F0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogdGFza0Zvcm1WYWx1ZS5hbGxvY2F0aW9uID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFID8gdGFza0Zvcm1WYWx1ZS5yb2xlLnZhbHVlIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0YXNrRm9ybVZhbHVlLmFsbG9jYXRpb24gPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1VTRVIgPyB0aGlzLnRhc2tNb2RhbENvbmZpZy50ZWFtSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBpc0FwcHJvdmVUYXNrOiB0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2ssXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiAhdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9sZUlkID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fVEVBTV9ST0xFICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ11cclxuICAgICAgICAgICAgICAgICAgICA/IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvbGUgPSB0aGlzLmZvcm1Sb2xlVmFsdWUocm9sZUlkKTtcclxuICAgICAgICAgICAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQVNTSUdOTUVOVF9UWVBFLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkFTU0lHTk1FTlRfVFlQRSA9PT0gVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSA/IChyb2xlID8gcm9sZS52YWx1ZSA6ICcnKSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkFTU0lHTk1FTlRfVFlQRSA9PT0gVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUiA/IHRoaXMudGFza01vZGFsQ29uZmlnLnRlYW1JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayxcclxuICAgICAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6ICF0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2tcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUixcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS51c2VycyAmJiByZXNwb25zZS51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHVzZXJMaXN0ICYmIHVzZXJMaXN0Lmxlbmd0aCAmJiB1c2VyTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyTGlzdC5tYXAodXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSB1c2Vycy5maW5kKGUgPT4gZS52YWx1ZSA9PT0gdXNlci5kbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHVzZXIuZG4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gdXNlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIEluZGl2aWR1YWwgVXNlcnMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tUeXBlRGV0YWlscyh0YXNrVHlwZUlkOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmV4aXN0aW5nVGFza1R5cGVEZXRhaWxzID0gbnVsbDtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoXHJcbiAgICAgICAgICAgIG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAnVGFza19UeXBlLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogdGFza1R5cGVJZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCgnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvVGFza19UeXBlL29wZXJhdGlvbnMnLCAnUmVhZFRhc2tfVHlwZScsIHBhcmFtcykuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgIHRhc2t0eXBlRGV0YWlsUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0YXNrVHlwZURldGFpbHMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHRhc2t0eXBlRGV0YWlsUmVzcG9uc2UsICdUYXNrX1R5cGUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGFza1R5cGVEZXRhaWxzKSAmJiB0YXNrVHlwZURldGFpbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5leGlzdGluZ1Rhc2tUeXBlRGV0YWlscyA9IHRhc2tUeXBlRGV0YWlsc1swXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ05vIFRhc2sgVHlwZSBEZXRhaWxzIGZvdW5kJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0YXNrdHlwZURldGFpbFJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGZhaWwgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUYXNrIFR5cGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihmYWlsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgZ2V0QXNzaWdubWVudFR5cGVEZXRhaWxzKGFzc2lnbm1lbnRUeXBlSWQ6IHN0cmluZykge1xyXG4gICAgICAgICB0aGlzLmV4aXN0aW5nQXNzaWdubWVudFR5cGVEZXRhaWxzID0gbnVsbDtcclxuICAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKFxyXG4gICAgICAgICAgICAgb2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgJ0Fzc2lnbm1lbnRfVHlwZS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIElkOiBhc3NpZ25tZW50VHlwZUlkXHJcbiAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KCdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Bc3NpZ25tZW50X1R5cGUvb3BlcmF0aW9ucycsICdSZWFkQXNzaWdubWVudF9UeXBlJywgcGFyYW1zKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgIGFzc2lnbm1lbnRUeXBlRGV0YWlsc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFzc2lnbm1lbnRUeXBlRGV0YWlscyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoYXNzaWdubWVudFR5cGVEZXRhaWxzUmVzcG9uc2UsICdBc3NpZ25tZW50X1R5cGUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGFzc2lnbm1lbnRUeXBlRGV0YWlscykgJiYgYXNzaWdubWVudFR5cGVEZXRhaWxzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmV4aXN0aW5nQXNzaWdubWVudFR5cGVEZXRhaWxzID0gYXNzaWdubWVudFR5cGVEZXRhaWxzWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdObyBBc3NpZ25tZW50IFR5cGUgRGV0YWlscyBmb3VuZCcsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYXNzaWdubWVudFR5cGVEZXRhaWxzUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgZmFpbCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUYXNrIFR5cGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGZhaWwpO1xyXG4gICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgKTtcclxuICAgICB9ICovXHJcblxyXG4gICAgZ2V0RGVwZW5kZW50VGFza3MoaXNBcHByb3ZhbFRhc2s6IGJvb2xlYW4pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RQYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElEOiB0aGlzLnRhc2tDb25maWcucHJvamVjdElkLFxyXG4gICAgICAgICAgICAgICAgSXNBcHByb3ZhbFRhc2s6IGlzQXBwcm92YWxUYXNrLFxyXG4gICAgICAgICAgICAgICAgQWxsb3dBY3Rpb25UYXNrOiB0aGlzLmFsbG93QWN0aW9uVGFza1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmdldERlcGVuZGVudFRhc2socmVxdWVzdFBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRlcGVuZGVudFRhc2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlcGVuZGVudFRhc2tSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoZGVwZW5kZW50VGFza1Jlc3BvbnNlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVwZW5kZW50VGFza1Jlc3BvbnNlID0gW2RlcGVuZGVudFRhc2tSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0xpc3QgPSBkZXBlbmRlbnRUYXNrUmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlbW92aW5nIHRoZSBzZWxlY3RlZCB0YXNrIGZyb20gdGFzayBsaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdCA9IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tMaXN0LmZpbHRlcih0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGFzayAhPT0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJlZGVjZXNzb3JGaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tMaXN0Lm1hcCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSWQgJiYgdGFza1snVGFzay1pZCddLklkID09PSB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgPSB0YXNrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLmZvcm1QcmVkZWNlc3NvclZhbHVlKHRhc2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2suTkFNRSAmJiB0YXNrWydUYXNrLWlkJ10gJiYgdGFza1snVGFzay1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlZGVjZXNzb3JGaWx0ZXJMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLk5BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrWydUYXNrLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcnlQYWNrYWdlQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBwcmVkZWNlc3NvckZpbHRlckxpc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBwcmVkZWNlc3NvckZpbHRlckxpc3Q7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnByZWRlY2Vzc29yLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0xpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyeVBhY2thZ2VDb25maWcuZmlsdGVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcmVkZWNlc3NvckNvbmZpZy5maWx0ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcnlQYWNrYWdlQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByZURldGFpbHMoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLnRhc2tDb25maWcucHJvamVjdE9iailcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldERlcGVuZGVudFRhc2tzKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaylcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLmNhdGVnb3J5SWQgJiYgdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEFsbFJvbGVzKCkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JrSm9pbihbdGhpcy5nZXRQcmlvcml0aWVzKCksIHRoaXMuZ2V0U3RhdHVzKCksIHRoaXMuZ2V0SW5pdGlhbFN0YXR1cygpLCB0aGlzLmdldFVzZXJzQnlSb2xlKCksIHRoaXMuZ2V0UmV2aWV3ZXJzKCldKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tBc3NpZ25tZW50VHlwZXMgPSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tUeXBlcyA9IFRhc2tDb25zdGFudHMuVEFTS19UWVBFX0xJU1Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuaXNOZXdUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VUYXNrRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBvd25lcklkID0gKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzayAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRFsnSWRlbnRpdHktaWQnXSkgPyB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRFsnSWRlbnRpdHktaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcmtKb2luKFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5nZXRUYXNrRGF0YVZhbHVlcyh0aGlzLnRhc2tDb25maWcudGFza0lkKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtT3duZXJWYWx1ZShvd25lcklkKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXREZWxpdmVyYWJsZVJldmlld0J5VGFza0lkKHRoaXMudGFza0NvbmZpZy50YXNrSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSkuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVWYWxpZGF0aW9uID0gcmVzcG9uc2VbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFRhc2tDb25zdGFudHMuVEFTS19UWVBFX0xJU1QuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZyAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlRBU0tfVFlQRSAmJiBlbGVtZW50Lk5BTUUgPT09IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5UQVNLX1RZUEUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmV4aXN0aW5nVGFza1R5cGVEZXRhaWxzID0gZWxlbWVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BTExPQ0FUSU9OX1RZUEUgJiYgZWxlbWVudC5OQU1FID09PSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQUxMT0NBVElPTl9UWVBFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5leGlzdGluZ1Rhc2tUeXBlRGV0YWlscyA9IGVsZW1lbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VUYXNrRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRhc2tzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByb2plY3QgRGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrRmllbGRzKHRhc2spIHtcclxuICAgICAgICBjb25zdCB0YXNrRmllbGQgPSB7XHJcbiAgICAgICAgICAgIGlzQXBwcm92YWxUYXNrU2VsZWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBzaG93T3RoZXJDb21tZW50czogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzT3RoZXJDb21tZW50c1NlbGVjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgc2hvd1ByZWRlY2Vzc29yOiBmYWxzZSxcclxuICAgICAgICAgICAgZW5hYmxlRGVsaXZlcmFibGVBcHByb3ZlcjogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dEZWxpdmVyeVBhY2thZ2U6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRhc2suUEFSRU5UX1RBU0tfSUQpKSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrRmllbGQuaXNPdGhlckNvbW1lbnRzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRhc2tGaWVsZC5zaG93T3RoZXJDb21tZW50cyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dQcmVkZWNlc3NvciA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dEZWxpdmVyeVBhY2thZ2UgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGFza0ZpZWxkLmVuYWJsZURlbGl2ZXJhYmxlQXBwcm92ZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRhc2tGaWVsZC5pc0FwcHJvdmFsVGFza1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRhc2tGaWVsZC5pc0FwcHJvdmFsVGFza1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRhc2tGaWVsZC5zaG93T3RoZXJDb21tZW50cyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0YXNrRmllbGQuaXNPdGhlckNvbW1lbnRzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dEZWxpdmVyeVBhY2thZ2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dQcmVkZWNlc3NvciA9IHRydWU7XHJcbiAgICAgICAgICAgIHRhc2tGaWVsZC5lbmFibGVEZWxpdmVyYWJsZUFwcHJvdmVyID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0YXNrRmllbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJlZGVjZXNzb3J0YXNrKHByZWRlY2Vzc29ySWQpIHtcclxuICAgICAgICBsZXQgcHJlZGVjZXNzb3JUYXNrID0gbnVsbDtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdC5tYXAodGFzayA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0YXNrWydUYXNrLWlkJ10uSWQgPT09IHByZWRlY2Vzc29ySWQpIHtcclxuICAgICAgICAgICAgICAgIHByZWRlY2Vzc29yVGFzayA9IHRhc2s7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gcHJlZGVjZXNzb3JUYXNrO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRpYWxpc2VUYXNrRm9ybSgpIHtcclxuICAgICAgICBjb25zdCBkaXNhYmxlRmllbGQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnRhc2tGb3JtID0gbnVsbDtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5taW5EYXRlID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlNUQVJUX0RBVEU7XHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWF4RGF0ZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5EVUVfREFURTtcclxuICAgICAgICBjb25zdCBwYXR0ZXJuID0gUHJvamVjdENvbnN0YW50Lk5BTUVfU1RSSU5HX1BBVFRFUk47XHJcbiAgICAgICAgaWYgKCF0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5pc05ld1Rhc2spIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1N0YXR1cyA9IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdDtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICB0YXNrTmFtZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbikgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4KDk5OSksIFZhbGlkYXRvcnMubWluKDEpXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb25UeXBlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5EdXJhdGlvblR5cGVbMF0udmFsdWUsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9KSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3QgJiYgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0WzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXSA/IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddLklkIDogJycsIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICByb2xlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0ICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3RbMF1bJ01QTV9Qcmlvcml0eS1pZCddID8gdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnREYXRlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlNUQVJUX0RBVEUsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGVuZERhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEUsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMuZW5hYmxlRHVyYXRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHJldmlzaW9uUmV2aWV3UmVxdWlyZWQ6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2sgPyB0aGlzLmRlZmF1bHRSZXZpc2lvblJlcXVpcmVkU3RhdHVzIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgaXNNaWxlc3RvbmU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBmYWxzZSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICB2aWV3T3RoZXJDb21tZW50czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHByZWRlY2Vzc29yOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU3RhdHVzID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuZmlsdGVyU3RhdHVzT3B0aW9ucyh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCwgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QsIE1QTV9MRVZFTFMuVEFTSyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmdldFN0YXR1c0J5U3RhdHVzSWQodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQsIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tTdGF0dXNMaXN0KTtcclxuICAgICAgICAgICAgICAgIC8vIGlmIChzZWxlY3RlZFN0YXR1c0luZm8gJiYgKHNlbGVjdGVkU3RhdHVzSW5mb1snU1RBVFVTX0xFVkVMJ10gPT09IFN0YXR1c0xldmVscy5GSU5BTCkpIHtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAvLyBkaXNhYmxlRmllbGRzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuZGlzYWJsZVN0YXR1c09wdGlvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFzayA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaztcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tGaWVsZCA9IHRoaXMuZ2V0VGFza0ZpZWxkcyh0YXNrKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tSb2xlSWQgPSB0YXNrLlJfUE9fVEVBTV9ST0xFICYmIHRhc2suUl9QT19URUFNX1JPTEVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddID8gdGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIC8vIGNvbnN0IHRhc2tPd25lcklkID0gdGFzay5SX1BPX09XTkVSX0lEICYmIHRhc2suUl9QT19PV05FUl9JRFsnSWRlbnRpdHktaWQnXSA/IHRhc2suUl9QT19PV05FUl9JRFsnSWRlbnRpdHktaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgdGFzay5wcmVkZWNlc3NvciA9ICh0YXNrLlBBUkVOVF9UQVNLX0lEICYmIHR5cGVvZiB0YXNrLlBBUkVOVF9UQVNLX0lEICE9PSAnb2JqZWN0JykgPyB0YXNrLlBBUkVOVF9UQVNLX0lEIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICB0YXNrTmFtZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRhc2suTkFNRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OID09PSAnc3RyaW5nJyA/IHRhc2suVEFTS19EVVJBVElPTiA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvblR5cGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgOiB0aGlzLkR1cmF0aW9uVHlwZVswXS52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgIXRoaXMuZW5hYmxlRHVyYXRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5SX1BPX1NUQVRVUyAmJiB0YXNrLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10gPyB0YXNrLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPyAnJyA6IHRoaXMuc2VsZWN0ZWRPd25lcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCAodGhpcy5zZWxlY3RlZFN0YXR1c0luZm8gJiYgdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8uU1RBVFVTX1RZUEUgIT09IHRoaXMudGFza0NvbnN0YW50cy5TVEFUVVNfVFlQRV9JTklUSUFMKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Sb2xlVmFsdWUodGFza1JvbGVJZCksIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAodGFzay5SX1BPX1BSSU9SSVRZICYmIHRhc2suUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10gPyB0YXNrLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGFzay5TVEFSVF9EQVRFKSwgMCwgJ2RheXMnLCB0aGlzLmVuYWJsZVdvcmtXZWVrKSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpKVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRhc2suRFVFX0RBVEUsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMuZW5hYmxlRHVyYXRpb24pIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGFzay5ERVNDUklQVElPTikgPyAnJyA6IHRhc2suREVTQ1JJUFRJT04sIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcmV2aXNpb25SZXZpZXdSZXF1aXJlZDogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRhc2suUkVWSVNJT05fUkVWSUVXX1JFUVVJUkVEKSwgZGlzYWJsZWQ6IHRydWUgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgaXNNaWxlc3RvbmU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0YXNrLklTX01JTEVTVE9ORSksIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgdmlld090aGVyQ29tbWVudHM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0YXNrRmllbGQuc2hvd090aGVyQ29tbWVudHMsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJlZGVjZXNzb3I6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmZvcm1QcmVkZWNlc3Nvcih0YXNrLnByZWRlY2Vzc29yKSksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZGlzYWJsZWQ6IHRhc2tGaWVsZC5pc0FwcHJvdmFsVGFza1NlbGVjdGVkIHx8ICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPyB0aGlzLnNlbGVjdGVkT3duZXIgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCAodGhpcy5zZWxlY3RlZFN0YXR1c0luZm8gJiYgdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8uU1RBVFVTX1RZUEUgIT09IHRoaXMudGFza0NvbnN0YW50cy5TVEFUVVNfVFlQRV9JTklUSUFMKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5zYXZlZFJldmlld2VycyB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5BU1NJR05NRU5UX1RZUEUsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3NvciA9IHRoaXMuZ2V0UHJlZGVjZXNzb3J0YXNrKHRhc2sucHJlZGVjZXNzb3IpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3Nvci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3Nvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1pbkRhdGVPYmplY3QgPSB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3Nvci5EVUVfREFURSksIDEsICdkYXlzJywgdGhpcy5lbmFibGVXb3JrV2Vlayk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWluRGF0ZSA9IG1pbkRhdGVPYmplY3Q7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFRhc2tPd25lckFzc2lnbm1lbnRDb3VudCgpO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5vblRhc2tTZWxlY3Rpb24oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrVHlwZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5UQVNLX1RZUEU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9sZFRhc2tPYmplY3QgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jcmVhdGVOZXdUYXNrVXBkYXRlT2JqZWN0XHJcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKSwgdGhpcy50YXNrQ29uZmlnLnRhc2tUeXBlLCB0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2ssIHRoaXMudGFza01vZGFsQ29uZmlnLnByb2plY3RJZCwgdGhpcy5zYXZlZFJldmlld2Vycyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5pc05ld1Rhc2spIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1N0YXR1cyA9IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdDtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICB0YXNrTmFtZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICAvLyAyMDVcclxuICAgICAgICAgICAgICAgICAgICAvKiAgIGR1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uVHlwZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuRHVyYXRpb25UeXBlWzBdLnZhbHVlLCBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbikgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyBkaXNhYmxlRmllbGQgOiAhdGhpcy5lbmFibGVEdXJhdGlvbikgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4KDk5OSksIFZhbGlkYXRvcnMubWluKDEpXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb25UeXBlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5EdXJhdGlvblR5cGVbMF0udmFsdWUsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyBkaXNhYmxlRmllbGQgOiAhdGhpcy5lbmFibGVEdXJhdGlvbikgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2wodGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0ICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdFswXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXSA/IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddLklkIDogJycpLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaW9yaXR5OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdCAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0WzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0WzBdWydNUE1fUHJpb3JpdHktaWQnXSA/IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3RbMF1bJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcmV2aXNpb25SZXZpZXdSZXF1aXJlZDogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayA/IHRoaXMuZGVmYXVsdFJldmlzaW9uUmVxdWlyZWRTdGF0dXMgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBpc01pbGVzdG9uZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IGZhbHNlLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZXdPdGhlckNvbW1lbnRzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJlZGVjZXNzb3I6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU3RhdHVzID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuZmlsdGVyU3RhdHVzT3B0aW9ucyh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCwgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QsIE1QTV9MRVZFTFMuVEFTSyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmdldFN0YXR1c0J5U3RhdHVzSWQodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQsIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tTdGF0dXNMaXN0KTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXNrID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFza0ZpZWxkID0gdGhpcy5nZXRUYXNrRmllbGRzKHRhc2spO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFza1JvbGVJZCA9IHRhc2suUl9QT19URUFNX1JPTEUgJiYgdGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10gPyB0YXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgdGFzay5wcmVkZWNlc3NvciA9ICh0YXNrLlBBUkVOVF9UQVNLX0lEICYmIHR5cGVvZiB0YXNrLlBBUkVOVF9UQVNLX0lEICE9PSAnb2JqZWN0JykgPyB0YXNrLlBBUkVOVF9UQVNLX0lEIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICB0YXNrTmFtZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRhc2suTkFNRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gMjA1XHJcbiAgICAgICAgICAgICAgICAgICAgLyogZHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OID09PSAnc3RyaW5nJyA/IHRhc2suVEFTS19EVVJBVElPTiA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvblR5cGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgOiB0aGlzLkR1cmF0aW9uVHlwZVswXS52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgIXRoaXMuZW5hYmxlRHVyYXRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgfSksICovXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OID09PSAnc3RyaW5nJyA/IHRhc2suVEFTS19EVVJBVElPTiA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCB0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1RlbXBsYXRlID8gZGlzYWJsZUZpZWxkIDogIXRoaXMuZW5hYmxlRHVyYXRpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4KDk5OSksIFZhbGlkYXRvcnMubWluKDEpXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb25UeXBlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdHlwZW9mIHRhc2suVEFTS19EVVJBVElPTl9UWVBFID09PSAnc3RyaW5nJyA/IHRhc2suVEFTS19EVVJBVElPTl9UWVBFIDogdGhpcy5EdXJhdGlvblR5cGVbMF0udmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyBkaXNhYmxlRmllbGQgOiAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLlJfUE9fU1RBVFVTICYmIHRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXSA/IHRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPyAnJyA6IHRoaXMuc2VsZWN0ZWRPd25lcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCAodGhpcy5zZWxlY3RlZFN0YXR1c0luZm8gJiYgdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8uU1RBVFVTX1RZUEUgIT09IHRoaXMudGFza0NvbnN0YW50cy5TVEFUVVNfVFlQRV9JTklUSUFMKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Sb2xlVmFsdWUodGFza1JvbGVJZCksIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAodGFzay5SX1BPX1BSSU9SSVRZICYmIHRhc2suUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10gPyB0YXNrLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0YXNrLkRFU0NSSVBUSU9OKSA/ICcnIDogdGFzay5ERVNDUklQVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICByZXZpc2lvblJldmlld1JlcXVpcmVkOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGFzay5SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQpLCBkaXNhYmxlZDogdHJ1ZSB9KSxcclxuICAgICAgICAgICAgICAgICAgICBpc01pbGVzdG9uZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRhc2suSVNfTUlMRVNUT05FKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICB2aWV3T3RoZXJDb21tZW50czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRhc2tGaWVsZC5zaG93T3RoZXJDb21tZW50cywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmVkZWNlc3NvcjogbmV3IEZvcm1Db250cm9sKHRoaXMuZm9ybVByZWRlY2Vzc29yKHRhc2sucHJlZGVjZXNzb3IpKSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPyB0aGlzLnNlbGVjdGVkT3duZXIgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCAodGhpcy5zZWxlY3RlZFN0YXR1c0luZm8gJiYgdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8uU1RBVFVTX1RZUEUgIT09IHRoaXMudGFza0NvbnN0YW50cy5TVEFUVVNfVFlQRV9JTklUSUFMKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5zYXZlZFJldmlld2VycyB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5BU1NJR05NRU5UX1RZUEUsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkcHJlZGVjZXNzb3IgPSB0aGlzLmdldFByZWRlY2Vzc29ydGFzayh0YXNrLnByZWRlY2Vzc29yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3IuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkcHJlZGVjZXNzb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtaW5EYXRlT2JqZWN0ID0gdGhpcy5jb21tZW50c1V0aWxTZXJ2aWNlLmFkZFVuaXRzVG9EYXRlKG5ldyBEYXRlKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkcHJlZGVjZXNzb3IuRFVFX0RBVEUpLCAxLCAnZGF5cycsIHRoaXMuZW5hYmxlV29ya1dlZWspO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1pbkRhdGUgPSBtaW5EYXRlT2JqZWN0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRUYXNrT3duZXJBc3NpZ25tZW50Q291bnQoKTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMub25UYXNrU2VsZWN0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza1R5cGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suVEFTS19UWVBFO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vbGRUYXNrT2JqZWN0ID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuY3JlYXRlTmV3VGFza1VwZGF0ZU9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgICh0aGlzLnRhc2tGb3JtLmdldFJhd1ZhbHVlKCksIHRoaXMudGFza0NvbmZpZy50YXNrVHlwZSwgdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrLCB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcm9qZWN0SWQsIHRoaXMuc2F2ZWRSZXZpZXdlcnMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmZpbHRlcmVkUmV2aWV3ZXJzID0gdGhpcy50YXNrRm9ybS5nZXQoJ2RlbGl2ZXJhYmxlUmV2aWV3ZXJzJykudmFsdWVDaGFuZ2VzLnBpcGUoXHJcbiAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgIG1hcCgocmV2aWV3ZXI6IHN0cmluZyB8IG51bGwpID0+IHJldmlld2VyID8gdGhpcy5maWx0ZXJSZXZpZXdlcnMocmV2aWV3ZXIpIDogdGhpcy5hbGxSZXZpZXdlcnMuc2xpY2UoKSkpO1xyXG5cclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFN0YXR1c0l0ZW1JZCA9IHRoaXMudGFza0Zvcm0udmFsdWUuc3RhdHVzO1xyXG4gICAgICAgIC8vIHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhdHVzLmRpc2FibGUoKTtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcmVkZWNlc3NvckNvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3I7XHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcjtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUm9sZUNvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZTtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZvcm1Db250cm9sID0gdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUmV2aWV3ZXIuZm9ybUNvbnRyb2wgPSB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXI7XHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcnlQYWNrYWdlQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3NvcjtcclxuICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5hbGxvY2F0aW9uLnZhbHVlID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyICYmICF0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2spIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiYgIXRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2VyICYmICF0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2spIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLyogaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS52YWx1ZSAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9ICovXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnByZWRlY2Vzc29yLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUocHJlZGVjZXNzb3IgPT4ge1xyXG4gICAgICAgICAgICAvLyBUT0RPIHByZWRlY2Vzc29yIGNoYW5nZSBtZXRob2RcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgJiYgdHlwZW9mIHByZWRlY2Vzc29yID09PSAnb2JqZWN0JyAmJiBwcmVkZWNlc3NvciAmJiBwcmVkZWNlc3Nvci52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0xpc3QubWFwKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0YXNrWydUYXNrLWlkJ10uSWQgPT09IHByZWRlY2Vzc29yLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qaWYgKHRhc2suUkVWSVNJT05fUkVWSUVXX1JFUVVJUkVEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJldmlzaW9uUmV2aWV3UmVxdWlyZWQucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG1pbkRhdGVPYmplY3QgPSB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGFzay5EVUVfREFURSksIDEsICdkYXlzJywgdGhpcy5lbmFibGVXb3JrV2Vlayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1pbkRhdGUgPSBtaW5EYXRlT2JqZWN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5tYXhEYXRlID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkRVRV9EQVRFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnNldFZhbHVlKG1pbkRhdGVPYmplY3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnNldFZhbHVlKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5EVUVfREFURSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1pbkRhdGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZHByZWRlY2Vzc29yID9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3Nvci5EVUVfREFURSksIDEsICdkYXlzJywgdGhpcy5lbmFibGVXb3JrV2VlaykgOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuU1RBUlRfREFURTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1heERhdGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEU7XHJcbiAgICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRQcmVkZWNlc3NvcjtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFByZWRlY2Vzc29yID0gdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcnlQYWNrYWdlQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChwcmVkZWNlc3NvclZhbHVlID0+IHByZWRlY2Vzc29yVmFsdWUuZGlzcGxheU5hbWUgPT09IHByZWRlY2Vzc29yKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRQcmVkZWNlc3NvciA9IHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChwcmVkZWNlc3NvclZhbHVlID0+IHByZWRlY2Vzc29yVmFsdWUuZGlzcGxheU5hbWUgPT09IHByZWRlY2Vzc29yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFByZWRlY2Vzc29yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3Nvci5zZXRWYWx1ZShzZWxlY3RlZFByZWRlY2Vzc29yKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGlmICghdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnNldFZhbHVlKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5TVEFSVF9EQVRFKTtcclxuICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmVuZERhdGUuc2V0VmFsdWUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkRVRV9EQVRFKTtcclxuICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUob3duZXJWYWx1ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAob3duZXJWYWx1ZSAmJiB0eXBlb2Ygb3duZXJWYWx1ZSA9PT0gJ29iamVjdCcgJiYgIXRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucy5tYXAodGFza093bmVyTmFtZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0YXNrT3duZXJOYW1lLnZhbHVlID09PSBvd25lclZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldFRhc2tPd25lckFzc2lnbm1lbnRDb3VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3duZXJQcm9qZWN0Q291bnRNZXNzYWdlID0gJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRPd25lciA9IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zLmZpbmQob3duZXIgPT4gb3duZXIuZGlzcGxheU5hbWUgPT09IG93bmVyVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZE93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuc2V0VmFsdWUoc2VsZWN0ZWRPd25lcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5hbGxvY2F0aW9uLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoYWxsb2NhdGlvblR5cGVWYWx1ZSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcnMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcnMuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZkFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWUsIG51bGwsIG51bGwsIHRydWUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvKiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoZGVsaXZlcmFibGVSZXZpZXdlcnMgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkZWxpdmVyYWJsZVJldmlld2Vycyk7XHJcbiAgICAgICAgfSk7ICovXHJcblxyXG4gICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICBpZiAocm9sZSAmJiB0eXBlb2Ygcm9sZSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZlJvbGUocm9sZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFJvbGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zLmZpbmQodGFza1JvbGUgPT4gdGFza1JvbGUuZGlzcGxheU5hbWUgPT09IHJvbGUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm9sZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5zZXRWYWx1ZShzZWxlY3RlZFJvbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLnZhbHVlLm93bmVyICYmIHRoaXMudGFza0Zvcm0udmFsdWUub3duZXIudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucy5tYXAodGFza093bmVyTmFtZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0YXNrT3duZXJOYW1lLnZhbHVlID09PSB0aGlzLnRhc2tGb3JtLnZhbHVlLm93bmVyLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldFRhc2tPd25lckFzc2lnbm1lbnRDb3VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3duZXJQcm9qZWN0Q291bnRNZXNzYWdlID0gJyc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRlVmFsaWRhdGlvbiAmJiB0aGlzLmRhdGVWYWxpZGF0aW9uLnN0YXJ0TWF4RGF0ZSAmJiBuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLmRhdGVWYWxpZGF0aW9uLnN0YXJ0TWF4RGF0ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuUFJPSkVDVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kdXJhdGlvbkVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5EVVJBVElPTl9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmVuYWJsZUR1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52YWxpZGF0ZUR1cmF0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZGF0ZVZhbGlkYXRpb24gJiYgdGhpcy5kYXRlVmFsaWRhdGlvbi5zdGFydE1heERhdGUgJiYgbmV3IERhdGUodGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUudmFsdWUpID4gbmV3IERhdGUodGhpcy5kYXRlVmFsaWRhdGlvbi5zdGFydE1heERhdGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLkRFTElWRVJBQkxFX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLlBST0pFQ1RfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcblxyXG5cclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0udmFsdWUub3duZXIgJiYgdGhpcy50YXNrRm9ybS52YWx1ZS5vd25lci52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zLm1hcCh0YXNrT3duZXJOYW1lID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2tPd25lck5hbWUudmFsdWUgPT09IHRoaXMudGFza0Zvcm0udmFsdWUub3duZXIudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGFza093bmVyQXNzaWdubWVudENvdW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vd25lclByb2plY3RDb3VudE1lc3NhZ2UgPSAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRhdGVWYWxpZGF0aW9uICYmIHRoaXMuZGF0ZVZhbGlkYXRpb24uZW5kTWluRGF0ZSAmJiBuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmVuZERhdGUudmFsdWUpIDwgbmV3IERhdGUodGhpcy5kYXRlVmFsaWRhdGlvbi5lbmRNaW5EYXRlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZUVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5ERUxJVkVSQUJMRV9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuUFJPSkVDVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5lbmFibGVEdXJhdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlQ2hhbmdlcy5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUgJiYgdGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUudmFsdWUgJiYgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbiAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmFsaWRhdGVEdXJhdGlvbigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvblR5cGUudmFsdWVDaGFuZ2VzLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZSAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52YWxpZGF0ZUR1cmF0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmhhc0FsbENvbmZpZyA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyogb25UYXNrU2VsZWN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IHJvbGVzID0gW107XHJcbiAgICAgICAgY29uc3Qgcm9sZUlkcyA9IFtdO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2spIHtcclxuICAgICAgICAgICAgdGhpcy50ZWFtUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudC5hcHBSb2xlcy5ST0xFX0ROLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfTUFOQUdFUikgIT09IC0xIHx8IGVsZW1lbnQuYXBwUm9sZXMuUk9MRV9ETi5zZWFyY2goUm9sZUNvbnN0YW50cy5QUk9KRUNUX0FQUFJPVkVSKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICByb2xlcy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVJZHMucHVzaChlbGVtZW50Lm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmFwcFJvbGVzLlJPTEVfRE4uc2VhcmNoKFJvbGVDb25zdGFudHMuUFJPSkVDVF9NQU5BR0VSKSAhPT0gLTEgfHwgZWxlbWVudC5hcHBSb2xlcy5ST0xFX0ROLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfTUVNQkVSKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICByb2xlcy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVJZHMucHVzaChlbGVtZW50Lm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1JvbGVDb25maWcuZmlsdGVyT3B0aW9ucyA9IHJvbGVzO1xyXG4gICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgdmFsaWRhdGVEdXJhdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbiAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlIDwgMSB8fCB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlID4gOTk5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmR1cmF0aW9uRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLkRVUkFUSU9OX01JTl9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IGVuZERhdGVPYmplY3QgPSB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUudmFsdWUpLCB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uVHlwZS52YWx1ZSA9PT0gJ3dlZWtzJyA/ICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlKSA6ICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnZhbHVlIC0gMSksIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb25UeXBlLnZhbHVlLCB0aGlzLmVuYWJsZVdvcmtXZWVrKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS5zZXRWYWx1ZShlbmREYXRlT2JqZWN0KTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24uc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzayAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suTUFYX0RFTElWRVJBQkxFX0RBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobmV3IERhdGUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLk1BWF9ERUxJVkVSQUJMRV9EQVRFKSA+PSBuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmVuZERhdGUudmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG1pbkR1cmF0aW9uID0gdGhpcy5jb21tZW50c1V0aWxTZXJ2aWNlLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLk1BWF9ERUxJVkVSQUJMRV9EQVRFKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSksIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb25UeXBlLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbi5zZXRWYWxpZGF0b3JzKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heCg5OTkpLCBWYWxpZGF0b3JzLm1pbihtaW5EdXJhdGlvbiArIDEpXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmR1cmF0aW9uRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLkRFTElWRVJBQkxFX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25DaGFuZ2VvZkFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWUsIG93bmVyVmFsdWUsIHJvbGVWYWx1ZSwgY2hhbmdlRGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG5cclxuICAgICAgICBpZiAoYWxsb2NhdGlvblR5cGVWYWx1ZSA9PT0gVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmNsZWFyVmFsaWRhdG9ycygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnBhdGNoVmFsdWUocm9sZVZhbHVlID8gcm9sZVZhbHVlIDogJycpO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnBhdGNoVmFsdWUob3duZXJWYWx1ZSA/IG93bmVyVmFsdWUgOiAnJyk7XHJcbiAgICAgICAgICAgIGlmIChjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChhbGxvY2F0aW9uVHlwZVZhbHVlID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9VU0VSKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnBhdGNoVmFsdWUob3duZXJWYWx1ZSA/IG93bmVyVmFsdWUgOiAnJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2hhbmdlRGVsaXZlcmFibGVBcHByb3ZlciAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLmNsZWFyVmFsaWRhdG9ycygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUgJiYgIXRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlICYmICF0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUuZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZSb2xlKHJvbGUpIHtcclxuICAgICAgICBpZiAocm9sZSAmJiByb2xlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlICYmICF0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUuZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUgJiYgIXRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5lbmFibGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIucGF0Y2hWYWx1ZSgocm9sZSA/IHJvbGUgOiAnJykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoKHJvbGUgPyByb2xlIDogJycpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25BcHByb3ZhbFN0YXRlQ2hhbmdlKCkge1xyXG4gICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3Iuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgIGZvcmtKb2luKFt0aGlzLmdldEFsbFJvbGVzKCksIHRoaXMuZ2V0RGVwZW5kZW50VGFza3ModGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrKV0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3IudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtUHJlZGVjZXNzb3JWYWx1ZSh0YXNrKSB7XHJcbiAgICAgICAgaWYgKHRhc2suTkFNRSAmJiB0YXNrWydUYXNrLWlkJ10gJiYgdGFza1snVGFzay1pZCddLklkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0YXNrWydUYXNrLWlkJ10uSWQgKyAnLScgKyB0YXNrLk5BTUU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvcm1QcmVkZWNlc3Nvcih0YXNrSWQpIHtcclxuICAgICAgICBpZiAodGFza0lkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUHJlZGVjZXNzb3IgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcmVkZWNlc3NvckNvbmZpZy5maWx0ZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50LnZhbHVlID09PSB0YXNrSWQpO1xyXG4gICAgICAgICAgICByZXR1cm4gc2VsZWN0ZWRQcmVkZWNlc3NvciA/IHNlbGVjdGVkUHJlZGVjZXNzb3IgOiAnJztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvcm1Pd25lclZhbHVlKHRhc2tVc2VySUQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRhc2tVc2VySUQpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJJZDogdGFza1VzZXJJRFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQZXJzb25CeUlkZW50aXR5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50LnZhbHVlID09PSByZXNwb25zZS51c2VyQ04pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE93bmVyID0gc2VsZWN0ZWRVc2VyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1Sb2xlVmFsdWUodGFza1JvbGVJRCkge1xyXG4gICAgICAgIGlmICh0YXNrUm9sZUlEKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZSA9IHRoaXMudGVhbVJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSB0YXNrUm9sZUlEKTtcclxuICAgICAgICAgICAgcmV0dXJuIChzZWxlY3RlZFJvbGUpID8gc2VsZWN0ZWRSb2xlIDogJyc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrT3duZXJBc3NpZ25tZW50Q291bnQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsVGFza0NyZWF0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLnByaXN0aW5lKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjbG9zZT8nLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVRhc2soc2F2ZU9wdGlvbikge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3ZlciAmJiB0aGlzLnRhc2tGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3Zlci52YWx1ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kKHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSB0aGlzLnRhc2tGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3Zlci52YWx1ZSk7XHJcbiAgICAgICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTZWxlY3RlZCByZXZpZXdlciBpcyBhbHJlYWR5IHRoZSBhcHByb3ZlcicsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0ucHJpc3RpbmUgJiYgISh0aGlzLmNoaXBDaGFuZ2VkKSkge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdLaW5kbHkgbWFrZSBjaGFuZ2VzIHRvIHVwZGF0ZSB0aGUgdGFzaycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLldBUk4gfTtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbmFtZVNwbGl0ID0gdGhpcy50YXNrRm9ybS52YWx1ZS50YXNrTmFtZS5zcGxpdCgnXycpO1xyXG4gICAgICAgIGlmIChuYW1lU3BsaXRbMF0ubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ05hbWUgc2hvdWxkIG5vdCBzdGFydCB3aXRoIFwiX1wiJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuV0FSTiB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCB0YXNrRW5kRGF0ZVdhcm5pbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKHRhc2tFbmREYXRlV2FybmluZykge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLnBhdGNoVmFsdWUoe1xyXG4gICAgICAgICAgICAgICAgZW5kRGF0ZTogdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkRVRV9EQVRFXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdUYXNrXFwncyBlbmQgZGF0ZSBzaG91bGQgbm90IGJlIGxlc3MgdGhhbiB0aGUgZGVsaXZlcmFibGVzXFwnIGR1ZSBkYXRlJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuV0FSTiB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLnN0YXR1cyA9PT0gJ1ZBTElEJykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy50YXNrTmFtZS52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIHRhc2sgbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy50YXNrTmFtZS5zZXRWYWx1ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnRhc2tOYW1lLnZhbHVlLnRyaW0oKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVRhc2tEZXRhaWxzKHNhdmVPcHRpb24pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ0tpbmRseSBmaWxsIGFsbCBtYW5kYXRvcnkgZmllbGRzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JtUmV2aWV3ZXJPYmooKSB7XHJcbiAgICAgICAgY29uc3QgYWRkZWRSZXZpZXdlcnMgPSBbXTtcclxuICAgICAgICBjb25zdCByZW1vdmVkUmV2aWV3ZXJzID0gW107XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm5ld1Jldmlld2VycyAmJiB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubmV3UmV2aWV3ZXJzLmZvckVhY2gobmV3UmV2aWV3ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYWRkZWRSZXZpZXdlckluZGV4ID0gYWRkZWRSZXZpZXdlcnMuZmluZEluZGV4KHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSBuZXdSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFkZGVkUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWRkZWRSZXZpZXdlcnMucHVzaChuZXdSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmZpbmRJbmRleChkZWxpdmVyYWJsZVJldmlld2VyID0+IGRlbGl2ZXJhYmxlUmV2aWV3ZXIudmFsdWUgPT09IG5ld1Jldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhZGRlZFJldmlld2VySW5kZXggPSBhZGRlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IG5ld1Jldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkZGVkUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZGVkUmV2aWV3ZXJzLnB1c2gobmV3UmV2aWV3ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5uZXdyZXZpZXdlcnNPYmogPSBhZGRlZFJldmlld2Vycy5tYXAoKGFkZGVkUmV2aWV3ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVJZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgUmV2aWV3ZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJdGVtSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IGFkZGVkUmV2aWV3ZXIudmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogJ0FERCdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIFRhc2tJZDogdGhpcy50YXNrQ29uZmlnLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgICAgICBJc1BNQXNzaWduZWRSZXZpZXdlcjogdGhpcy5pc1BNXHJcblxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnJlbW92ZWRSZXZpZXdlcnMgJiYgdGhpcy5zYXZlZFJldmlld2Vycykge1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZWRSZXZpZXdlcnMuZm9yRWFjaChyZW1vdmVkUmV2aWV3ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2F2ZWRSZXZpZXdlcnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID0gdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5maW5kSW5kZXgoZGVsaXZlcmFibGVSZXZpZXdlciA9PiBkZWxpdmVyYWJsZVJldmlld2VyLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXZpZXdlckluZGV4ID0gcmVtb3ZlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlZFJldmlld2Vycy5wdXNoKHJlbW92ZWRSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNhdmVkUmV2aWV3ZXJJbmRleCA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZEluZGV4KHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2F2ZWRSZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZW1vdmVkUmV2aWV3ZXJJbmRleCA9IHJlbW92ZWRSZXZpZXdlcnMuZmluZEluZGV4KHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmZpbmRJbmRleChkYXRhID0+IGRhdGEudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZW1vdmVkUmV2aWV3ZXJJbmRleCA9PT0gLTEgJiYgZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZWRSZXZpZXdlcnMucHVzaChyZW1vdmVkUmV2aWV3ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVkcmV2aWV3ZXJzT2JqID0gcmVtb3ZlZFJldmlld2Vycy5tYXAoKHJldmlld2VyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIFJldmlld2VyOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXRlbUlkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcklkOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnUkVNT1ZFJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgVGFza0lkOiB0aGlzLnRhc2tDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgIElzUE1Bc3NpZ25lZFJldmlld2VyOiB0aGlzLmlzUE1cclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNlbGVjdGVkUmV2aWV3ZXJzT2JqID0gdGhpcy5zYXZlZFJldmlld2Vycy5tYXAoKHJldmlld2VyKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBJZDogJycsXHJcbiAgICAgICAgICAgICAgICBJdGVtSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgdXNlcklEOiByZXZpZXdlci52YWx1ZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnJldmlld2Vyc09iaiA9IHRoaXMubmV3cmV2aWV3ZXJzT2JqID8gdGhpcy5uZXdyZXZpZXdlcnNPYmouY29uY2F0KHRoaXMucmVtb3ZlZHJldmlld2Vyc09iaikgOiB0aGlzLnJlbW92ZWRyZXZpZXdlcnNPYmouY29uY2F0KHRoaXMubmV3cmV2aWV3ZXJzT2JqKTtcclxuICAgIH1cclxuICAgIHVwZGF0ZVRhc2tEZXRhaWxzKHNhdmVPcHRpb24pIHtcclxuICAgICAgICBjb25zdCB0YXNrRm9ybVZhbHVlcyA9IHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKTtcclxuICAgICAgICB0aGlzLmZvcm1SZXZpZXdlck9iaigpO1xyXG4gICAgICAgIGNvbnN0IFRhc2tPYmplY3Q6IFRhc2tVcGRhdGVPYmogPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jcmVhdGVOZXdUYXNrVXBkYXRlT2JqZWN0KHRhc2tGb3JtVmFsdWVzLCB0aGlzLnRhc2tDb25maWcudGFza1R5cGUsIHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayxcclxuICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcucHJvamVjdElkLCB0aGlzLnNlbGVjdGVkUmV2aWV3ZXJzT2JqKTtcclxuICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFza1snVGFzay1pZCddXHJcbiAgICAgICAgICAgICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFza1snVGFzay1pZCddLklkKSB7XHJcbiAgICAgICAgICAgIFRhc2tPYmplY3QuVGFza0lkLklkID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrWydUYXNrLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgIFRhc2tPYmplY3QuUmV2aXNpb25SZXZpZXdSZXF1aXJlZCA9IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUkVWSVNJT05fUkVWSUVXX1JFUVVJUkVEKVxyXG4gICAgICAgICAgICAgICAgPyAnJyA6IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQ7XHJcbiAgICAgICAgICAgIFRhc2tPYmplY3QuSXNNaWxlc3RvbmUgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLklTX01JTEVTVE9ORSlcclxuICAgICAgICAgICAgICAgID8gJycgOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suSVNfTUlMRVNUT05FO1xyXG4gICAgICAgICAgICBUYXNrT2JqZWN0LklzTWlsZXN0b25lID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRhc2tGb3JtVmFsdWVzLmlzTWlsZXN0b25lKVxyXG4gICAgICAgICAgICAgICAgPyAnJyA6IHRhc2tGb3JtVmFsdWVzLmlzTWlsZXN0b25lO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zYXZlVGFzayhUYXNrT2JqZWN0LCBzYXZlT3B0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBzYXZlVGFzayhUYXNrT2JqZWN0LCBzYXZlT3B0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgICAgIFRhc2tPYmplY3QgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jb21wYXJlVHdvUHJvamVjdERldGFpbHMoVGFza09iamVjdCwgdGhpcy5vbGRUYXNrT2JqZWN0KTtcclxuICAgICAgICAgICAgaWYgKE9iamVjdC5rZXlzKFRhc2tPYmplY3QpLmxlbmd0aCA+IDEgfHwgKEpTT04uc3RyaW5naWZ5KHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMpICE9PSBKU09OLnN0cmluZ2lmeSh0aGlzLnNhdmVkUmV2aWV3ZXJzKSkpIHtcclxuICAgICAgICAgICAgICAgIFRhc2tPYmplY3QuQnVsa09wZXJhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVxdWVzdE9iaiA9IHtcclxuICAgICAgICAgICAgICAgICAgICBUYXNrT2JqZWN0OiBUYXNrT2JqZWN0LFxyXG4gICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXI6IHRoaXMucmV2aWV3ZXJzT2JqXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudXBkYXRlVGFzayhyZXF1ZXN0T2JqKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnVGFzayB1cGRhdGVkIHN1Y2Nlc3NmdWxseScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLlNVQ0NFU1MgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlQ2FsbEJhY2tIYW5kbGVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFza0lkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlRhc2tbJ1Rhc2staWQnXS5JZCwgcHJvamVjdElkOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1RlbXBsYXRlLCBzYXZlT3B0aW9uOiBzYXZlT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JDb2RlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSwgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSB1cGRhdGluZyB0YXNrJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGRhdGluZyB0YXNrJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RPYmogPSB7XHJcbiAgICAgICAgICAgICAgICBUYXNrT2JqZWN0OiBUYXNrT2JqZWN0XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuY3JlYXRlVGFzayhyZXF1ZXN0T2JqKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuVGFzayAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlRhc2tbJ1Rhc2staWQnXSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlRhc2tbJ1Rhc2staWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdUYXNrIHNhdmVkIHN1Y2Nlc3NmdWxseScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLlNVQ0NFU1MgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlQ2FsbEJhY2tIYW5kbGVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFza0lkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlRhc2tbJ1Rhc2staWQnXS5JZCwgcHJvamVjdElkOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1RlbXBsYXRlLCBzYXZlT3B0aW9uOiBzYXZlT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JDb2RlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSwgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSBjcmVhdGluZyB0YXNrJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIHRhc2snLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXNlVGFza0NvbmZpZygpIHtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZyA9IHtcclxuICAgICAgICAgICAgaGFzQWxsQ29uZmlnOiBmYWxzZSxcclxuICAgICAgICAgICAgdGFza1N0YXR1c0xpc3Q6IG51bGwsXHJcbiAgICAgICAgICAgIHRhc2tJbml0aWFsU3RhdHVzTGlzdDogbnVsbCxcclxuICAgICAgICAgICAgdGFza1ByaW9yaXR5TGlzdDogbnVsbCxcclxuICAgICAgICAgICAgcHJlZGVjZXNzb3I6IG51bGwsXHJcbiAgICAgICAgICAgIHRhc2tMaXN0OiB0aGlzLnRhc2tDb25maWcudGFza0xpc3QsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkUHJvamVjdDogdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCxcclxuICAgICAgICAgICAgZGVmYXVsdFN0YXR1czogbnVsbCxcclxuICAgICAgICAgICAgZGVmYXVsdFByaW9yaXR5OiBudWxsLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFN0YXR1c0l0ZW1JZDogbnVsbCxcclxuICAgICAgICAgICAgbWluRGF0ZTogJycsXHJcbiAgICAgICAgICAgIG1heERhdGU6ICcnLFxyXG4gICAgICAgICAgICBpc05ld1Rhc2s6ICh0aGlzLnRhc2tDb25maWcudGFza0lkKSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgICAgICAgICAgdGFza0lkOiB0aGlzLnRhc2tDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFRhc2s6IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgIHNlbGVjdGVkcHJlZGVjZXNzb3I6IG51bGwsXHJcbiAgICAgICAgICAgIGlzVGVtcGxhdGU6IHRoaXMudGFza0NvbmZpZy5pc1RlbXBsYXRlLFxyXG4gICAgICAgICAgICBwcm9qZWN0SWQ6IHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgIHRhc2tPd25lckNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUYXNrIE93bmVyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGwsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHRhc2tSb2xlQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JvbGUnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcHJlZGVjZXNzb3JDb25maWc6IHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnUHJlZGVjZXNzb3InLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJhYmxlIEFwcHJvdmVyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcjoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZWxpdmVyYWJsZSBSZXZpZXdlcicsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRlbGl2ZXJ5UGFja2FnZUNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZWxpdmVyeSBQYWNrYWdlJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGFza0Fzc2lnbm1lbnRUeXBlczogbnVsbCxcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGNvbnN0IHByb2plY3RDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFByb2plY3RDb25maWcoKTtcclxuICAgICAgICB0aGlzLmVuYWJsZUR1cmF0aW9uID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX0RVUkFUSU9OXSk7XHJcbiAgICAgICAgdGhpcy5lbmFibGVXb3JrV2VlayA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHByb2plY3RDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX1BST0pFQ1RfQ09ORklHLkVOQUJMRV9XT1JLX1dFRUtdKTtcclxuICAgICAgICB0aGlzLnNob3dSZXZpc2lvblJldmlld1JlcXVpcmVkID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuU0hPV19SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRURdKTtcclxuICAgICAgICB0aGlzLnByb2plY3RDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFByb2plY3RDb25maWcoKTtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0UmV2aXNpb25SZXF1aXJlZFN0YXR1cyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMucHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuREVGQVVMVF9SRVZJU0lPTl9SRVFVSVJFRF9TVEFUVVNdKTtcclxuICAgICAgICB0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dKSA/ICcuKicgOlxyXG4gICAgICAgICAgICB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5OQU1FX1NUUklOR19QQVRURVJOXTtcclxuICAgICAgICB0aGlzLmFsbG93QWN0aW9uVGFzayA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkFMTE9XX0FDVElPTl9UQVNLXSk7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXIgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyT2JqZWN0KCk7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXIuTWFuYWdlckZvci5UYXJnZXQuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgaWYgKHVzZXIuTmFtZSA9PT0gJ01QTSBQcm9qZWN0IE1hbmFnZXInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzUE0gPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gY29uc3Qgc3RhdHVzZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnQ29tbWVudHMnKTtcclxuICAgICAgICBpZiAobmF2aWdhdG9yLmxhbmd1YWdlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5hZGFwdGVyLnNldExvY2FsZShuYXZpZ2F0b3IubGFuZ3VhZ2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnICYmIHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgdGhpcy5pbml0aWFsaXNlVGFza0NvbmZpZygpO1xyXG4gICAgICAgICAgICB0aGlzLmdldFByZURldGFpbHMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZGF0ZUZpbHRlcjogKGRhdGU6IERhdGUgfCBudWxsKSA9PiBib29sZWFuID1cclxuICAgICAgICAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZW5hYmxlV29ya1dlZWspIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGF5ID0gZGF0ZS5nZXREYXkoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXkgIT09IDAgJiYgZGF5ICE9PSA2O1xyXG4gICAgICAgICAgICAgICAgLy8wIG1lYW5zIHN1bmRheVxyXG4gICAgICAgICAgICAgICAgLy82IG1lYW5zIHNhdHVyZGF5XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbn1cclxuIl19