import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { DeliverableConstants } from '../deliverable.constants';
import { AppService } from '../../../../mpm-utils/services/app.service';
import { ProjectConstant } from '../../../project-overview/project.constants';
import { EntityAppDefService } from '../../../../mpm-utils/services/entity.appdef.service';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
import { AllocationTypes } from '../../../../mpm-utils/objects/AllocationType';
import { LoaderService } from '../../../../../lib/loader/loader.service';
import { UtilService } from '../../../../../lib/mpm-utils/services/util.service';
import * as acronui from '../../../../../lib/mpm-utils/auth/utility';
import { DeliverableService } from '../deliverable.service';
import { ConfirmationModalComponent } from '../../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SaveOptionConstant } from '../../../../shared/constants/save-options.constants';
import { StatusService } from '../../../../shared/services/status.service';
import { StatusLevels } from '../../../../mpm-utils/objects/StatusType';
import { ProjectUtilService } from '../../../shared/services/project-util.service';
import { DeliverableTypes } from '../../../../../lib/mpm-utils/objects/DeliverableTypes';
import { CustomSearchFieldComponent } from '../../../../shared/components/custom-search-field/custom-search-field.component';
import { SharingService } from '../../../../../lib/mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../../../../lib/shared/constants/application.config.constants';
import { startWith, map } from 'rxjs/operators';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { newArray } from '@angular/compiler/src/util';
import { TaskTypes } from '../../../../mpm-utils/objects/TaskType';
import { NotificationService } from '../../../../notification/notification.service';
let DeliverableCreationComponent = class DeliverableCreationComponent {
    constructor(adapter, appService, utilService, entityAppdefService, deliverableService, loaderService, dialog, statusService, projectUtilService, sharingService, notificationService) {
        this.adapter = adapter;
        this.appService = appService;
        this.utilService = utilService;
        this.entityAppdefService = entityAppdefService;
        this.deliverableService = deliverableService;
        this.loaderService = loaderService;
        this.dialog = dialog;
        this.statusService = statusService;
        this.projectUtilService = projectUtilService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.selectedOwner = null;
        this.selectedApprover = null;
        this.deliverableConstants = DeliverableConstants;
        this.saveOptions = JSON.parse(JSON.stringify(SaveOptionConstant.deliverableSaveOptions));
        this.SELECTED_SAVE_OPTION = {
            name: 'Save',
            value: 'SAVE'
        };
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.isPM = false;
        this.newReviewers = [];
        this.removedReviewers = [];
        // reviewers: any[] = [];
        this.savedReviewers = [];
        this.allReviewers = [];
        // availableReviewers: any[];
        // reviewerDetails;
        this.reviewerStatusData = [];
        this.deliverableReviewers = [];
        this.PROJECT_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.TASK_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.DELIVERABLE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable/operations';
        this.DELIVERABLE_TYPE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable_Type/operations';
        this.USER_TEAM_METHOD_NS = 'http://schemas.acheron-tech.com/mpm/teams/bpm/1.0';
        this.ROLE_TEAM_METHOD_NS = 'http://schemas/AcheronMPMTeams/Team_Role_Mapping/operations';
        this.ASSIGNMENT_TYPE_METHOD_NS = 'http://schemas/AcheronMPMCore/Assignment_Type/operations';
        this.DELIVERABLE_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
        this.GET_TASK_DETAILS_WS_METHOD_NAME = 'ReadTask';
        this.GET_DELIVERABLE_DETAILS_WS_METHOD_NAME = 'ReadDeliverable';
        this.GET_USERS_BY_TEAM_WS_METHOD_NAME = 'GetTeamUserDetails';
        this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME = 'GetAllDeliverablesByProject';
        this.GET_ASSIGNMENT_TYPE_WS_METHOD_NAME = 'GetAllAssignmentType';
        this.CREATE_DELIVERABLE_WS_METHOD_NAME = 'CreateDeliverable';
        this.UPDATE_DELIVERABLE_WS_METHOD_NAME = 'EditDeliverable';
        this.GET_ALL_DELIVERABLES_BY_NAME_WS_METHOD_NAME = 'GetAllDeliverablesByDeliverableName';
        this.GET_USERS_BY_TEAM_ROLE_WS_METHOD_NAME = 'GetTeamRoleUsers ';
        this.GET_DELIVERABLE_TYPE_BY_NAME_WS_METHOD_NAME = 'GetDeliverableTypeByName';
        this.dateFilter = (date) => {
            if (this.enableWorkWeek) {
                return true;
            }
            else {
                const day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
    }
    filterReviewers(value) {
        if (value && typeof value === 'string' && value.trim() !== '') {
            const filterValue = value.toLowerCase();
            return this.allReviewers.filter(reviewer => reviewer.value.toLowerCase().indexOf(filterValue) === 0);
        }
    }
    selected(event) {
        const reviewerIndex = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === event.option.value);
        const reviewer = this.allReviewers.find(reviewerData => reviewerData.value === event.option.value);
        if (this.deliverableForm.value.deliverableApprover &&
            event.option.value === this.deliverableForm.value.deliverableApprover.name) {
            this.notificationService.error(reviewer.displayName + ' is already the Approver');
        }
        else if (reviewerIndex > -1) {
            this.notificationService.error(reviewer.displayName + ' is already selected');
        }
        else {
            this.savedReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            this.newReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            const removeReviewerIndex = this.removedReviewers.findIndex(removedReviewer => removedReviewer.value === event.option.value);
            if (removeReviewerIndex >= 0) {
                this.removedReviewers.splice(removeReviewerIndex, 1);
            }
            this.reviewerStatusData.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName,
                isCompleted: false
            });
            this.reviewerInput.nativeElement.value = '';
            this.deliverableForm.controls.deliverableReviewers.setValue(null);
        }
    }
    addReviewer(event) {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            if (input) {
                input.value = '';
            }
            this.deliverableForm.controls.deliverableReviewers.setValue(null);
        }
    }
    onRemoveReviewer(reviewerValue) {
        const index = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === reviewerValue);
        const newReviewerIndex = this.newReviewers.findIndex(newReviewer => newReviewer.value === reviewerValue);
        const reviewerIndex = this.reviewerStatusData.findIndex(reviewerData => reviewerData.value === reviewerValue);
        const reviewer = this.allReviewers.find(reviewerData => reviewerData.value === reviewerValue);
        this.removedReviewers.push({
            name: reviewer.displayName,
            value: reviewer.value,
            displayName: reviewer.displayName
        });
        if (index >= 0) {
            this.deliverableForm.markAsDirty();
            this.savedReviewers.splice(index, 1);
        }
        if (reviewerIndex >= 0) {
            this.reviewerStatusData.splice(reviewerIndex, 1);
        }
        if (newReviewerIndex >= 0) {
            this.newReviewers.splice(newReviewerIndex, 1);
        }
    }
    onChangeofSkipReviewValue(isNeedReviewValue) {
        if (!isNeedReviewValue) {
            this.deliverableForm.controls.approverAllocation.clearValidators();
            this.deliverableForm.controls.deliverableApprover.clearValidators();
            this.deliverableForm.controls.deliverableApproverRole.clearValidators();
            this.deliverableForm.controls.approverAllocation.patchValue('');
            this.deliverableForm.controls.deliverableApprover.patchValue('');
            this.deliverableForm.controls.deliverableApproverRole.patchValue('');
            this.deliverableForm.controls.approverAllocation.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
        }
        else {
            this.onChangeofApproverAllocationType(this.deliverableForm.getRawValue().approverAllocation, this.deliverableForm.getRawValue().deliverableApprover, false);
        }
    }
    onChangeofOwnerAllocationType(allocationTypeValue, ownerValue, changeDeliverableApprover) {
        const roles = [];
        const roleIds = [];
        this.teamOwnerRolesOptions.find(element => {
            roles.push(element);
            roleIds.push(element.name);
        });
        if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableOwner) {
                this.deliverableForm.controls.deliverableOwner.disable();
            }
            this.deliverableForm.controls.deliverableOwnerRole.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableOwner.clearValidators();
            this.deliverableForm.controls.deliverableOwner.updateValueAndValidity();
            this.deliverableForm.controls.deliverableOwnerRole.updateValueAndValidity();
            this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = roles;
            if (this.deliverableForm.getRawValue().deliverableOwnerRole) {
                this.getUsersByRole('OWNER').subscribe(response => {
                    this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
                });
            }
            else {
                this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
            }
            this.deliverableForm.controls.deliverableOwner.patchValue(ownerValue ? ownerValue : '');
        }
        else if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_USER) {
            this.deliverableForm.controls.deliverableOwnerRole.clearValidators();
            this.deliverableForm.controls.deliverableOwner.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableOwnerRole.updateValueAndValidity();
            this.deliverableForm.controls.deliverableOwner.updateValueAndValidity();
            this.getUsersByRole('OWNER').subscribe(response => {
                this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
                this.deliverableForm.controls.deliverableOwner.patchValue(ownerValue ? ownerValue : '');
            });
            if (this.deliverableForm.controls.deliverableOwner && !this.deliverableForm.controls.ownerAllocation.disabled) {
                this.deliverableForm.controls.deliverableOwner.enable();
            }
        }
    }
    onChangeofApproverAllocationType(allocationTypeValue, ownerValue, changeDeliverableApprover) {
        const roles = [];
        const roleIds = [];
        this.teamApproverRolesOptions.find(element => {
            roles.push(element);
            roleIds.push(element.name);
        });
        if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.disable();
            }
            this.deliverableForm.controls.deliverableApproverRole.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableApprover.clearValidators();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
            this.deliverableModalConfig.deliverableApproverRole.filterOptions = roles;
            if (this.deliverableForm.getRawValue().deliverableApproverRole) {
                this.getUsersByRole('APPROVER').subscribe(response => {
                    this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
                });
            }
            else {
                this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
            }
            if (changeDeliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.patchValue('');
            }
        }
        else if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_USER) {
            this.deliverableForm.controls.deliverableApprover.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableApproverRole.clearValidators();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.getUsersByRole('APPROVER').subscribe(response => {
                this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
                if (changeDeliverableApprover) {
                    this.deliverableForm.controls.deliverableApprover.patchValue('');
                }
            });
            if (this.deliverableForm.controls.deliverableApprover && !this.deliverableForm.controls.approverAllocation.disabled) {
                this.deliverableForm.controls.deliverableApprover.enable();
            }
        }
    }
    onChangeofOwnerRole(ownerRole) {
        if (ownerRole && ownerRole.name) {
            if (this.deliverableForm.controls.deliverableOwner && !this.deliverableForm.controls.deliverableOwnerRole.disabled) {
                this.deliverableForm.controls.deliverableOwner.enable();
            }
            this.getUsersByRole('OWNER').subscribe(response => {
                this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
                this.deliverableForm.controls.deliverableOwner.patchValue('');
            });
        }
        else {
            if (this.deliverableForm.controls.deliverableOwner) {
                this.deliverableForm.controls.deliverableOwner.patchValue('');
                this.deliverableForm.controls.deliverableOwner.disable();
            }
        }
    }
    onChangeofApproverRole(approverRole) {
        if (approverRole && approverRole.name) {
            if (this.deliverableForm.controls.deliverableApprover && !this.deliverableForm.controls.deliverableApproverRole.disabled) {
                this.deliverableForm.controls.deliverableApprover.enable();
            }
            this.getUsersByRole('APPROVER').subscribe(response => {
                this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
                this.deliverableForm.controls.deliverableApprover.patchValue('');
            });
        }
        else {
            if (this.deliverableForm.controls.deliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.patchValue('');
                this.deliverableForm.controls.deliverableApprover.disable();
            }
        }
    }
    /* onDeliverableSelection() {
        const ownerRoles = [];
        const ownerRoleIds = [];
        const approverRoles = [];
        const approverRoleIds = [];
        this.teamOwnerRolesOptions.find(element => {
            if (element.value.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.value.search(RoleConstants.PROJECT_MEMBER) !== -1) {
                ownerRoles.push(element);
                ownerRoleIds.push(element.name);
            }
        });
        this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = ownerRoles;
        this.getUsersByRole('OWNER').subscribe(response => {
            this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleUserOptions;
        });
        this.teamApproverRolesOptions.find(element => {
            if (element.value.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.value.search(RoleConstants.PROJECT_APPROVER) !== -1) {
                approverRoles.push(element);
                approverRoleIds.push(element.name);
            }
        });
        this.deliverableModalConfig.deliverableApproverRole.filterOptions = approverRoles;
        this.getUsersByRole('APPROVER').subscribe(response => {
            this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
        });
    } */
    updateDeliverable(saveOption) {
        if (this.deliverableForm.getRawValue().deliverableApprover && this.deliverableForm.getRawValue().deliverableApprover.name) {
            const data = this.savedReviewers.find(reviewer => reviewer.value === this.deliverableForm.getRawValue().deliverableApprover.name);
            if (data) {
                const eventData = { message: 'Selected reviewer is already the approver', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                return;
            }
        }
        if (this.deliverableForm.pristine) {
            const eventData = { message: 'Kindly make changes to update the deliverable', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        const nameSplit = this.deliverableForm.getRawValue().deliverableName.split('_');
        if (nameSplit[0].length === 0) {
            const eventData = { message: 'Name should not start with "_"', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        if (this.deliverableForm.status === 'VALID') {
            if (this.deliverableForm.controls.deliverableName.value.trim() === '') {
                const eventData = { message: 'Please provide a valid deliverable name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                this.notificationHandler.next(eventData);
            }
            else {
                this.updateDeliverableDetails(saveOption);
            }
        }
        else {
            const eventData = {
                message: 'Kindly fill all mandatory fields',
                type: ProjectConstant.NOTIFICATION_LABELS.INFO
            };
            this.notificationHandler.next(eventData);
        }
    }
    getAssignementTypeId(typeName) {
        let assignementId = null;
        this.deliverableModalConfig.deliverableOwnerAssignmentType.map(type => {
            if (type.NAME === typeName) {
                assignementId = type['Assignment_Type-id'].Id;
            }
        });
        return assignementId;
    }
    getUpdatedDeliverableDetails() {
        var _a, _b, _c, _d;
        let deliverableObject;
        const deliverableFormValues = this.deliverableForm.getRawValue();
        deliverableObject = {
            DeliverableId: {
                Id: this.deliverableModalConfig.isNewDeliverable ? null : this.deliverableModalConfig.deliverableId,
            },
            DeliverableName: deliverableFormValues.deliverableName.trim() ? deliverableFormValues.deliverableName.trim().replace(/\s+/g, ' ') : deliverableFormValues.deliverableName.trim(),
            Description: deliverableFormValues.description,
            StartDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(this.deliverableModalConfig.selectedTask.START_DATE) ? '' :
                this.deliverableModalConfig.selectedTask.START_DATE),
            EndDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(deliverableFormValues.dueDate) ? '' :
                deliverableFormValues.dueDate),
            DueDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(deliverableFormValues.dueDate) ? '' : deliverableFormValues.dueDate),
            IsActive: true,
            IsDeleted: false,
            DeliverableType: deliverableFormValues.needReview ? 'UPLOAD_REVIEW' : 'UPLOAD',
            OwnerAssignmentType: deliverableFormValues.ownerAllocation,
            ApproverAssignmentType: deliverableFormValues.approverAllocation,
            ExpectedDuration: deliverableFormValues.expectedDuration,
            RPOStatus: {
                MPMStatusID: {
                    Id: deliverableFormValues.status
                }
            },
            RPOPriority: {
                MPMPriorityID: {
                    Id: deliverableFormValues.priority
                }
            },
            RPOTask: {
                TaskID: {
                    Id: this.deliverableModalConfig.selectedTask['Task-id'].Id
                }
            },
            RPOProject: {
                ProjectID: {
                    Id: this.deliverableModalConfig.selectedProject['Project-id'].Id
                }
            },
            RPOOwnerRoleID: {
                MPMTeamRoleMappingID: {
                    Id: (_a = deliverableFormValues.deliverableOwnerRole) === null || _a === void 0 ? void 0 : _a.name
                }
            },
            RPOApproverRoleID: {
                MPMTeamRoleMappingID: {
                    Id: (_b = deliverableFormValues.deliverableApproverRole) === null || _b === void 0 ? void 0 : _b.name
                }
            },
            RPODeliverableOwnerID: {
                IdentityID: {
                    userID: (_c = deliverableFormValues.deliverableOwner) === null || _c === void 0 ? void 0 : _c.name
                }
            },
            RPODeliverableApproverID: {
                IdentityID: {
                    userID: (_d = deliverableFormValues.deliverableApprover) === null || _d === void 0 ? void 0 : _d.name
                }
            }
        };
        return deliverableObject;
    }
    formReviewerObj() {
        const addedReviewers = [];
        const removedReviewers = [];
        if (this.newReviewers && this.deliverableReviewers) {
            this.newReviewers.forEach(newReviewer => {
                if (this.deliverableReviewers.length === 0) {
                    const addedReviewerIndex = addedReviewers.findIndex(reviewer => reviewer.value === newReviewer.value);
                    if (addedReviewerIndex === -1) {
                        addedReviewers.push(newReviewer);
                    }
                }
                else {
                    const deliverableReviewerIndex = this.deliverableReviewers.findIndex(deliverableReviewer => deliverableReviewer.value === newReviewer.value);
                    if (deliverableReviewerIndex === -1) {
                        const addedReviewerIndex = addedReviewers.findIndex(reviewer => reviewer.value === newReviewer.value);
                        if (addedReviewerIndex === -1) {
                            addedReviewers.push(newReviewer);
                        }
                    }
                }
            });
            this.newreviewersObj = addedReviewers.map((addedReviewer) => {
                return {
                    DeliverableId: this.deliverableConfig.deliverableId,
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: addedReviewer.value,
                        action: 'ADD'
                    },
                    TaskId: this.deliverableModalConfig.taskId,
                    IsPMAssignedReviewer: this.isPM
                };
            });
        }
        if (this.removedReviewers && this.savedReviewers) {
            this.removedReviewers.forEach(removedReviewer => {
                if (this.savedReviewers.length === 0) {
                    const deliverableReviewerIndex = this.deliverableReviewers.findIndex(deliverableReviewer => deliverableReviewer.value === removedReviewer.value);
                    if (deliverableReviewerIndex >= 0) {
                        const reviewerIndex = removedReviewers.findIndex(reviewer => reviewer.value === removedReviewer.value);
                        if (reviewerIndex === -1) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
                else {
                    const savedReviewerIndex = this.savedReviewers.findIndex(savedReviewer => savedReviewer.value === removedReviewer.value);
                    if (savedReviewerIndex === -1) {
                        const removedReviewerIndex = removedReviewers.findIndex(reviewer => reviewer.value === removedReviewer.value);
                        const deliverableReviewerIndex = this.deliverableReviewers.findIndex(data => data.value === removedReviewer.value);
                        if (removedReviewerIndex === -1 && deliverableReviewerIndex >= 0) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
            });
            this.removedreviewersObj = removedReviewers.map((reviewer) => {
                return {
                    DeliverableId: this.deliverableConfig.deliverableId,
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: reviewer.value,
                        action: 'REMOVE'
                    },
                    TaskId: this.deliverableModalConfig.taskId,
                    IsPMAssignedReviewer: this.isPM
                };
            });
        }
        this.reviewersObj = this.newreviewersObj ? this.newreviewersObj.concat(this.removedreviewersObj) : this.removedreviewersObj.concat(this.newreviewersObj);
    }
    updateDeliverableDetails(saveOption) {
        const deliverableObject = this.getUpdatedDeliverableDetails();
        this.formReviewerObj();
        this.saveDeliverable(deliverableObject, saveOption);
    }
    saveDeliverable(deliverableObject, saveOption) {
        this.loaderService.show();
        if (this.deliverableModalConfig.selectedDeliverable) {
            deliverableObject = this.projectUtilService.compareTwoProjectDetails(deliverableObject, this.oldFormValue);
            if (Object.keys(deliverableObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
                deliverableObject.BulkOperation = false;
                const requestObj = {
                    DeliverableObject: deliverableObject,
                    DeliverableReviewers: {
                        DeliverableReviewer: this.reviewersObj
                    }
                };
                this.appService.invokeRequest(this.DELIVERABLE_METHOD_NS, this.UPDATE_DELIVERABLE_WS_METHOD_NAME, requestObj)
                    .subscribe(response => {
                    this.loaderService.hide();
                    if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                        && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                        const eventData = { message: ' Deliverable updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        this.notificationHandler.next(eventData);
                        this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: this.deliverableModalConfig.taskId,
                            projectId: this.deliverableModalConfig.projectId,
                            isTemplate: this.deliverableModalConfig.isTemplate ? true : false,
                            saveOption: saveOption
                        });
                    }
                    else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                        const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        this.notificationHandler.next(eventData);
                    }
                    else {
                        this.loaderService.hide();
                        const eventData = {
                            message: 'Something went wrong on while updating deliverable',
                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                        };
                        this.notificationHandler.next(eventData);
                    }
                }, error => {
                    this.loaderService.hide();
                    const eventData = { message: 'Something went wrong on while updating deliverable', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                });
            }
            else {
                this.loaderService.hide();
            }
        }
        else {
            const request = {
                DeliverableObject: deliverableObject
            };
            this.appService.invokeRequest(this.DELIVERABLE_METHOD_NS, this.CREATE_DELIVERABLE_WS_METHOD_NAME, request)
                .subscribe(response => {
                this.loaderService.hide();
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                    && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                    const eventData = { message: 'Deliverable saved successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    this.notificationHandler.next(eventData);
                    if (this.deliverableModalConfig.isTemplate) {
                        this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: this.deliverableModalConfig.taskId,
                            projectId: this.deliverableModalConfig.projectId,
                            isTemplate: true,
                            saveOption: saveOption
                        });
                    }
                    else {
                        this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: this.deliverableModalConfig.taskId,
                            projectId: this.deliverableModalConfig.projectId,
                            isTemplate: false,
                            saveOption: saveOption
                        });
                    }
                }
                else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                    && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                    const eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                }
                else {
                    this.loaderService.hide();
                    const eventData = {
                        message: 'Something went wrong on while creating deliverable',
                        type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                    };
                    this.notificationHandler.next(eventData);
                }
            }, error => {
                this.loaderService.hide();
                const eventData = {
                    message: 'Something went wrong on while creating deliverable',
                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                };
                this.notificationHandler.next(eventData);
            });
        }
    }
    /* formUserValue(user) {
        const parameters = {
            userId: user
        };
        this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
            const selectedUser = this.teamRoleUserOptions.find(element => element.name === response.userCN);
            if (selectedUser) {
                this.selectedOwner =  selectedUser;
            }
        });
    } */
    formRoleValue(role, isReviewDeliverable) {
        if (role) {
            if (isReviewDeliverable) {
                const selectedRole = this.teamApproverRolesOptions.find(element => element.name === role);
                return (selectedRole) ? selectedRole : '';
            }
            else {
                const selectedRole = this.teamOwnerRolesOptions.find(element => element.name === role);
                return (selectedRole) ? selectedRole : '';
            }
        }
        else {
            return '';
        }
    }
    formOwnerValue(userId) {
        return new Observable(observer => {
            if (userId) {
                const parameters = {
                    userId: userId
                };
                this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
                    const selectedUser = this.teamRoleOwnerOptions.find(element => element.name === response.userCN);
                    this.selectedOwner = selectedUser;
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                observer.next(true);
                observer.complete();
            }
        });
    }
    formApproverValue(userId) {
        return new Observable(observer => {
            if (userId) {
                const parameters = {
                    userId: userId
                };
                this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
                    const selectedUser = this.teamRoleApproverOptions.find(element => element.name === response.userCN);
                    this.selectedApprover = selectedUser;
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                observer.next(true);
                observer.complete();
            }
        });
    }
    formOwnerRoleValue(deliverable) {
        if (deliverable.R_PO_OWNER_ROLE_ID && deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'] && typeof deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id !== 'object') {
            const selectedRole = this.teamOwnerRolesOptions.find(element => element.name === deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id);
            return (selectedRole) ? selectedRole : '';
        }
        return '';
    }
    formApproverRoleValue(deliverable) {
        if (deliverable && deliverable.R_PO_APPROVER_ROLE_ID && typeof deliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id !== 'object') {
            const selectedRole = this.teamApproverRolesOptions.find(element => element.name === deliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id);
            return (selectedRole) ? selectedRole : '';
        }
        return '';
    }
    formOwnerAllocationValue(deliverable) {
        if (deliverable) {
            return (deliverable.OWNER_ASSIGNMENT_TYPE) ? deliverable.OWNER_ASSIGNMENT_TYPE : '';
        }
        return '';
    }
    formApproverAllocationValue(deliverable) {
        if (deliverable) {
            return (deliverable.APPROVER_ASSIGNMENT_TYPE) ? deliverable.APPROVER_ASSIGNMENT_TYPE : '';
        }
        return '';
    }
    initialiseDeliverableForm() {
        const isTemplate = this.deliverableModalConfig.isTemplate;
        const isNewDeliverable = this.deliverableModalConfig.isNewDeliverable;
        const numericeVlalue = this.getMaxMinValueForNumber(0);
        let disableField = false;
        const selectedDeliverable = this.deliverableModalConfig.selectedDeliverable;
        this.showReviewer = (this.deliverableModalConfig && this.deliverableModalConfig.selectedDeliverable &&
            this.deliverableModalConfig.selectedDeliverable.DELIVERABLE_TYPE !== DeliverableTypes.UPLOAD) ? true : false;
        this.deliverableModalConfig.minDate = this.deliverableModalConfig.selectedTask.START_DATE;
        this.deliverableModalConfig.maxDate = this.deliverableModalConfig.selectedTask.DUE_DATE;
        const pattern = '^[a-zA-Z0-9 _().-]*$';
        if (this.deliverableConfig.title) {
            this.modalLabel = this.deliverableConfig.title;
        }
        if (!isTemplate) {
            if (isNewDeliverable) {
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: '', disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    priority: new FormControl({
                        value: this.deliverableModalConfig.deliverablePriorityList && this.deliverableModalConfig.deliverablePriorityList[0] &&
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'] ?
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    status: new FormControl({
                        value: this.deliverableModalConfig.deliverableStatusList && this.deliverableModalConfig.deliverableStatusList[0] &&
                            this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'] ? this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    dueDate: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.DUE_DATE, disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({ value: false, disabled: !this.enableNeedReview }),
                    approverAllocation: new FormControl({
                        value: '', disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE, disabled: disableField
                    }),
                    deliverableOwner: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID ?
                            this.selectedOwner : '', disabled: disableField
                    }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableOwnerRole: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE === DeliverableConstants.ALLOCATION_TYPE_ROLE ?
                            this.formRoleValue(this.deliverableModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id, false) : '', disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    expectedDuration: new FormControl('', [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
            else {
                disableField = this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.FINAL || !this.isUploaded ||
                    (selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW) ? true : disableField;
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: selectedDeliverable.NAME,
                        disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    deliverableOwner: new FormControl({
                        value: this.selectedOwner,
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableOwnerRole: new FormControl({
                        value: this.formOwnerRoleValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableApprover: new FormControl({
                        value: this.selectedApprover,
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableApproverRole: new FormControl({
                        value: this.formApproverRoleValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    status: new FormControl({
                        value: selectedDeliverable.R_PO_STATUS && selectedDeliverable.R_PO_STATUS['MPM_Status-id'] ? selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    priority: new FormControl({
                        value: selectedDeliverable.R_PO_PRIORITY && selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'] ? selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'].Id : '',
                        disabled: disableField
                    }, [Validators.required]),
                    dueDate: new FormControl({
                        value: selectedDeliverable.DUE_DATE,
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({
                        value: typeof selectedDeliverable.DESCRIPTION === 'string' ?
                            selectedDeliverable.DESCRIPTION : '',
                        disabled: disableField
                    }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({
                        value: this.utilService.isNullOrEmpty(selectedDeliverable.APPROVER_ASSIGNMENT_TYPE) ? false : true,
                        disabled: true
                    }),
                    // !disableField ? selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW ? disableField : true : disableField
                    approverAllocation: new FormControl({
                        value: this.formApproverAllocationValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    ownerAllocation: new FormControl({
                        value: this.formOwnerAllocationValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl(this.utilService.isNullOrEmpty(selectedDeliverable.EXPECTED_DURATION) ? '' : selectedDeliverable.EXPECTED_DURATION, [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
                // this.deliverableForm.controls.needReview.disable();
                // this.onDeliverableSelection();
            }
        }
        else {
            if (isNewDeliverable) {
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: '', disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    priority: new FormControl({
                        value: this.deliverableModalConfig.deliverablePriorityList && this.deliverableModalConfig.deliverablePriorityList[0] &&
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'] ?
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    status: new FormControl({
                        value: this.deliverableModalConfig.deliverableStatusList && this.deliverableModalConfig.deliverableStatusList[0] &&
                            this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'] ? this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({ value: false, disabled: !this.enableNeedReview }),
                    approverAllocation: new FormControl({
                        value: '', disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE, disabled: disableField
                    }),
                    deliverableOwner: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID ?
                            this.selectedOwner : '', disabled: disableField
                    }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableOwnerRole: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE === DeliverableConstants.ALLOCATION_TYPE_ROLE ?
                            this.formRoleValue(this.deliverableModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id, false) : '', disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl('', [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
            else {
                disableField = this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE ||
                    (selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW) ? true : disableField;
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: selectedDeliverable.NAME,
                        disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    deliverableOwner: new FormControl({
                        value: this.selectedOwner,
                        disabled: disableField
                    }),
                    deliverableOwnerRole: new FormControl({
                        value: this.formOwnerRoleValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    deliverableApprover: new FormControl({
                        value: this.selectedApprover,
                        disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({
                        value: this.formApproverRoleValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    status: new FormControl({
                        value: selectedDeliverable.R_PO_STATUS && selectedDeliverable.R_PO_STATUS['MPM_Status-id'] ? selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: true
                    }),
                    priority: new FormControl({
                        value: selectedDeliverable.R_PO_PRIORITY && selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'] ? selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'].Id : '',
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({
                        value: typeof selectedDeliverable.DESCRIPTION === 'string' ?
                            selectedDeliverable.DESCRIPTION : '',
                        disabled: disableField
                    }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({
                        value: this.utilService.isNullOrEmpty(selectedDeliverable.APPROVER_ASSIGNMENT_TYPE) ? false : true,
                        disabled: true
                    }),
                    // !disableField ? selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW ? disableField : true : disableField
                    approverAllocation: new FormControl({
                        value: this.formApproverAllocationValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.formOwnerAllocationValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl(this.utilService.isNullOrEmpty(selectedDeliverable.EXPECTED_DURATION) ? '' : selectedDeliverable.EXPECTED_DURATION, [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
        }
        this.filteredReviewers = this.deliverableForm.get('deliverableReviewers').valueChanges.pipe(startWith(''), map((reviewer) => reviewer ? this.filterReviewers(reviewer) : this.allReviewers.slice()));
        this.deliverableModalConfig.deliverableOwnerConfig.formControl = this.deliverableForm.controls.deliverableOwner;
        this.deliverableModalConfig.deliverableApprover.formControl = this.deliverableForm.controls.deliverableApprover;
        this.deliverableModalConfig.deliverableOwnerRoleConfig.formControl = this.deliverableForm.controls.deliverableOwnerRole;
        this.deliverableModalConfig.deliverableApproverRole.formControl = this.deliverableForm.controls.deliverableApproverRole;
        if (this.deliverableForm.controls.ownerAllocation.value === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableOwner && this.deliverableForm.controls.deliverableOwnerRole.value === '') {
                this.deliverableForm.controls.deliverableOwner.disable();
            }
            if (this.deliverableForm.controls.deliverableOwnerRole.value !== '' && isNewDeliverable) {
                this.getUsersByRole('OWNER').subscribe(response => {
                    this.deliverableModalConfig.deliverableOwnerConfig.formControl.updateValueAndValidity();
                    this.deliverableForm.get('deliverableOwner').updateValueAndValidity();
                });
            }
        }
        if (this.deliverableForm.controls.approverAllocation.value === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableApprover && this.deliverableForm.controls.deliverableApproverRole.value === '') {
                this.deliverableForm.controls.deliverableApprover.disable();
            }
            if (this.deliverableForm.controls.deliverableApproverRole.value !== '' && isNewDeliverable) {
                this.getUsersByRole('APPROVER').subscribe(response => {
                    this.deliverableModalConfig.deliverableApprover.formControl.updateValueAndValidity();
                    this.deliverableForm.get('deliverableApprover').updateValueAndValidity();
                });
            }
        }
        this.deliverableForm.controls.needReview.valueChanges.subscribe(isNeedReviewValue => {
            this.onChangeofSkipReviewValue(isNeedReviewValue);
        });
        // this.onChangeofOwnerAllocationType(this.deliverableForm.value.ownerAllocation, this.deliverableForm.value.deliverableOwner, false);
        this.deliverableForm.controls.ownerAllocation.valueChanges.subscribe(allocationTypeValue => {
            if (this.deliverableForm.controls.deliverableOwnerRole) {
                this.deliverableForm.controls.deliverableOwnerRole.setValue('');
            }
            if (this.deliverableForm.controls.deliverableOwner) {
                this.deliverableForm.controls.deliverableOwner.setValue('');
            }
            this.onChangeofOwnerAllocationType(allocationTypeValue, null, true);
        });
        // this.onChangeofApproverAllocationType(this.deliverableForm.value.approverAllocation, this.deliverableForm.value.deliverableApprover, false);
        this.deliverableForm.controls.approverAllocation.valueChanges.subscribe(allocationTypeValue => {
            if (this.deliverableForm.controls.deliverableApproverRole) {
                this.deliverableForm.controls.deliverableApproverRole.setValue('');
            }
            if (this.deliverableForm.controls.deliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.setValue('');
            }
            if (this.deliverableForm.controls.deliverableReviewers) {
                this.deliverableForm.controls.deliverableReviewers.setValue('');
            }
            this.onChangeofApproverAllocationType(allocationTypeValue, null, true);
        });
        this.deliverableForm.controls.deliverableOwnerRole.valueChanges.subscribe(ownerRole => {
            if (ownerRole && typeof ownerRole === 'object') {
                if (!(selectedDeliverable && selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW)) {
                    this.onChangeofOwnerRole(ownerRole);
                }
            }
            else {
                const selectedOwnerRole = this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions.find(role => role.displayName === ownerRole);
                if (selectedOwnerRole) {
                    this.deliverableForm.controls.deliverableOwnerRole.setValue(selectedOwnerRole);
                }
            }
        });
        this.deliverableForm.controls.deliverableApproverRole.valueChanges.subscribe(approverRole => {
            if (approverRole && typeof approverRole === 'object') {
                if (!(selectedDeliverable && selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW)) {
                    this.onChangeofApproverRole(approverRole);
                }
            }
            else {
                const selectedApproverRole = this.deliverableModalConfig.deliverableApproverRole.filterOptions.find(role => role.displayName === approverRole);
                if (selectedApproverRole) {
                    this.deliverableForm.controls.deliverableApproverRole.setValue(selectedApproverRole);
                }
            }
        });
        if (this.deliverableForm.controls.deliverableOwner) {
            this.deliverableForm.controls.deliverableOwner.valueChanges.subscribe(ownerValue => {
                if (!(ownerValue && typeof ownerValue === 'object')) {
                    const selectedOwner = this.deliverableModalConfig.deliverableOwnerConfig.filterOptions.find(owner => owner.displayName === ownerValue);
                    if (selectedOwner) {
                        this.deliverableForm.controls.deliverableOwner.setValue(selectedOwner);
                    }
                }
            });
        }
        if (this.deliverableForm.controls.deliverableApprover) {
            this.deliverableForm.controls.deliverableApprover.valueChanges.subscribe(approverValue => {
                if (!(approverValue && typeof approverValue === 'object')) {
                    const selectedApprover = this.deliverableModalConfig.deliverableApprover.filterOptions.find(approver => approver.displayName === approverValue);
                    if (selectedApprover) {
                        this.deliverableForm.controls.deliverableApprover.setValue(selectedApprover);
                    }
                }
            });
        }
        this.oldFormValue = this.getUpdatedDeliverableDetails();
    }
    getProjectDetails() {
        return new Observable(observer => {
            const getRequestObject = {
                ProjectID: this.deliverableModalConfig.projectId
            };
            this.loaderService.show();
            this.appService.invokeRequest(this.PROJECT_DETAILS_METHOD_NS, this.GET_PROJECT_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response && response.Project) {
                    const projectObj = response.Project;
                    if (projectObj.R_PO_CATEGORY && projectObj.R_PO_CATEGORY['MPM_Category-id']) {
                        this.deliverableModalConfig.categoryId = projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
                    }
                    if (projectObj.R_PO_TEAM && projectObj.R_PO_TEAM['MPM_Teams-id']) {
                        this.deliverableModalConfig.teamId = projectObj.R_PO_TEAM['MPM_Teams-id'].Id;
                    }
                    this.deliverableModalConfig.selectedProject = response.Project;
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, error => {
                observer.error(error);
                const eventData = { message: 'Something went wrong while fetching project details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.loaderService.hide();
            });
        });
    }
    getTaskDetails() {
        return new Observable(observer => {
            const getRequestObject = {
                'Task-id': {
                    Id: this.deliverableModalConfig.taskId
                }
            };
            this.loaderService.show();
            this.appService.invokeRequest(this.TASK_DETAILS_METHOD_NS, this.GET_TASK_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response && response.Task) {
                    const taskObj = response.Task;
                    this.deliverableModalConfig.selectedTask = response.Task;
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, error => {
                observer.error(error);
                const eventData = { message: 'Something went wrong while fetching task details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.loaderService.hide();
            });
        });
    }
    getDeliverableDetails() {
        this.loaderService.show();
        return new Observable(observer => {
            this.deliverableService.readDeliverable(this.deliverableModalConfig.deliverableId)
                .subscribe(response => {
                if (response && response.Deliverable) {
                    this.deliverableModalConfig.selectedDeliverable = response.Deliverable;
                    this.deliverableModalConfig.currStatus = this.deliverableModalConfig.selectedDeliverable.R_PO_STATUS ?
                        this.statusService.getStatusByStatusId(this.deliverableModalConfig.selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id) : null;
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, error => {
                observer.error(error);
                const eventData = { message: 'Something went wrong while fetching deliverable details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.loaderService.hide();
            });
        });
    }
    getPriorities() {
        return new Observable(observer => {
            /* this.entityAppdefService.getPriorities(MPM_LEVELS.DELIVERABLE)
                .subscribe(priorityResponse => {
                    if (priorityResponse) {
                        if (!Array.isArray(priorityResponse)) {
                            priorityResponse = [priorityResponse];
                        }
                        this.deliverableModalConfig.deliverablePriorityList = priorityResponse;
                    } else {
                        this.deliverableModalConfig.deliverablePriorityList = [];
                    }
                    observer.next(true);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                }); */
            let priority = this.entityAppdefService.getPriorities(MPM_LEVELS.DELIVERABLE);
            // .subscribe(priorityResponse => {
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                this.deliverableModalConfig.deliverablePriorityList = priority;
            }
            else {
                this.deliverableModalConfig.deliverablePriorityList = [];
            }
            observer.next(true);
            observer.complete();
            /* }, error => {
                const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            }); */
        });
    }
    getStatus(statusLeveLList) {
        return new Observable(observer => {
            this.entityAppdefService.getStatusByCategoryLevel(MPM_LEVELS.DELIVERABLE)
                .subscribe(statusResponse => {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    this.deliverableModalConfig.deliverableStatusList = statusResponse.filter(data => {
                        return statusLeveLList.indexOf(data.STATUS_LEVEL) >= 0 ? true : false;
                    });
                }
                else {
                    this.deliverableModalConfig.deliverableStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, error => {
                const eventData = { message: 'Something went wrong while fetching Statuses', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getAllRoles() {
        const ownerRolesParams = {
            teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
            isApproverTask: false,
            isMemberTask: true
        };
        const approverRolesParams = {
            teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
            isApproverTask: true,
            isMemberTask: false
        };
        return new Observable(observer => {
            forkJoin([this.appService.getTeamRoles(ownerRolesParams), this.appService.getTeamRoles(approverRolesParams)])
                .subscribe(response => {
                if (response && response[0].Teams && response[0].Teams.MPM_Teams &&
                    response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping && response[1].Teams &&
                    response[1].Teams.MPM_Teams && response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    let ownerRoleList = [];
                    let approverRoleList = [];
                    const ownerRoles = [];
                    const approverRoles = [];
                    if (!Array.isArray(response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping)) {
                        ownerRoleList = [response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping];
                    }
                    if (!Array.isArray(response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping)) {
                        approverRoleList = [response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping];
                    }
                    ownerRoleList = response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping;
                    approverRoleList = response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping;
                    if (ownerRoleList && ownerRoleList.length && ownerRoleList.length > 0) {
                        ownerRoleList.map(role => {
                            ownerRoles.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    if (approverRoleList && approverRoleList.length && approverRoleList.length > 0) {
                        approverRoleList.map(role => {
                            approverRoles.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    this.teamOwnerRolesOptions = ownerRoles;
                    this.teamApproverRolesOptions = approverRoles;
                    this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = this.teamOwnerRolesOptions;
                    this.deliverableModalConfig.deliverableApproverRole.filterOptions = this.teamApproverRolesOptions;
                }
                else {
                    this.teamOwnerRolesOptions = [];
                    this.teamApproverRolesOptions = [];
                }
                observer.next(true);
                observer.complete();
            }, error => {
                const eventData = { message: 'Something went wrong while fetching Roles', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getReviewerById(userId) {
        return new Observable(observer => {
            if (!this.utilService.isNullOrEmpty(userId)) {
                const parameters = {
                    userId: userId
                };
                this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    const eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    }
    getDeliverableReviewByDeliverableId(deliverableId) {
        return new Observable(observer => {
            this.appService.getDeliverableReviewByDeliverableID(deliverableId)
                .subscribe(response => {
                if (response && response.DeliverableReview) {
                    const reviewersList = acronui.findObjectsByProp(response, 'DeliverableReview');
                    if (reviewersList && reviewersList.length && reviewersList.length > 0) {
                        reviewersList.forEach(reviewer => {
                            if (!this.utilService.isNullOrEmpty(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id)) {
                                this.getReviewerById(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id).subscribe(reviewerData => {
                                    if (reviewerData) {
                                        const hasReviewerData = this.savedReviewers.find(savedReviewer => savedReviewer.value === reviewerData.userCN);
                                        if (!hasReviewerData) {
                                            this.savedReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                            this.deliverableReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                        }
                                    }
                                    this.entityAppdefService.getStatusById(reviewer.R_PO_REVIEW_STATUS['MPM_Status-id'].Id).subscribe(statusResponse => {
                                        const isReviewerCompleted = statusResponse.STATUS_LEVEL === 'FINAL' ? true : false;
                                        const reviewerIndex = this.reviewerStatusData.findIndex(data => data.value === reviewerData.userCN);
                                        if (reviewerIndex >= 0) {
                                            this.reviewerStatusData[reviewerIndex].isCompleted = isReviewerCompleted;
                                        }
                                        else {
                                            this.reviewerStatusData.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName,
                                                isCompleted: isReviewerCompleted
                                            });
                                        }
                                    });
                                    observer.next(this.savedReviewers);
                                    observer.complete();
                                });
                            }
                            else {
                                observer.next('');
                                observer.complete();
                            }
                        });
                    }
                    else {
                        observer.next('');
                        observer.complete();
                    }
                }
                else {
                    observer.next('');
                    observer.complete();
                }
            }, () => {
                const eventData = { message: 'Something went wrong while fetching Deliverable Review Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getReviewers() {
        return new Observable(observer => {
            if (this.deliverableConfig.selectedTask.TASK_TYPE === TaskTypes.APPROVAL_TASK) {
                let getReviewerUserRequest = {};
                getReviewerUserRequest = {
                    allocationType: 'USER',
                    roleDN: '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: true,
                    isUploadTask: false,
                    isReview: true
                };
                this.appService.getUsersForTeam(getReviewerUserRequest)
                    .subscribe(response => {
                    if (response && response.users && response.users.user) {
                        const userList = acronui.findObjectsByProp(response, 'user');
                        const users = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(user => {
                                const fieldObj = users.find(e => e.value === user.dn);
                                if (!fieldObj) {
                                    users.push({
                                        name: user.dn,
                                        value: user.cn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        this.allReviewers = users;
                    }
                    else {
                        this.allReviewers = [];
                    }
                    this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, () => {
                    const eventData = { message: 'Something went wrong while fetching Reviewer Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    }
    getUsersByRole(field, currAllocationType) {
        this.loaderService.show();
        let getOwnerByRoleRequest = {};
        let getApproverByRoleRequest = {};
        let getUserByRoleRequest = null;
        let userType;
        if (this.deliverableForm) {
            const allocationType = (field === 'OWNER') ? this.deliverableForm.getRawValue().ownerAllocation : this.deliverableForm.getRawValue().approverAllocation;
            userType = field;
            getUserByRoleRequest = {
                allocationType: allocationType,
                roleDN: allocationType === AllocationTypes.ROLE.toUpperCase() ?
                    (field === 'OWNER' ? this.deliverableForm.getRawValue().deliverableOwnerRole.value : this.deliverableForm.getRawValue().deliverableApproverRole.value) : '',
                teamID: allocationType === AllocationTypes.USER.toUpperCase() ? this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id : '',
                isApproveTask: (field === 'OWNER') ? false : true,
                isUploadTask: (field === 'OWNER') ? true : false
            };
        }
        else if (this.deliverableModalConfig.selectedDeliverable) {
            const selectedField = this.utilService.isNullOrEmpty(this.deliverableModalConfig.selectedDeliverable.DELIVERABLE_PARENT_ID) ? 'OWNER' : 'APPROVER';
            if (selectedField === 'OWNER') {
                const allocationType = this.deliverableModalConfig.selectedDeliverable.OWNER_ASSIGNMENT_TYPE;
                const selectedRoleId = allocationType === AllocationTypes.ROLE.toUpperCase() ? this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                userType = selectedField;
                getUserByRoleRequest = {
                    allocationType: allocationType,
                    roleDN: allocationType === AllocationTypes.ROLE.toUpperCase() ? this.formRoleValue(selectedRoleId, false).value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            else {
                const ownerAllocationType = this.deliverableModalConfig.selectedDeliverable.OWNER_ASSIGNMENT_TYPE;
                const approverAllocationType = this.deliverableModalConfig.selectedDeliverable.APPROVER_ASSIGNMENT_TYPE;
                const selectedOwnerRoleId = ownerAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                    this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                const selectedApproverRoleId = approverAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                    this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                getOwnerByRoleRequest = {
                    allocationType: ownerAllocationType,
                    roleDN: ownerAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                        this.formRoleValue(selectedOwnerRoleId, false).value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: false,
                    isUploadTask: true
                };
                getApproverByRoleRequest = {
                    allocationType: approverAllocationType,
                    roleDN: approverAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                        this.formRoleValue(selectedApproverRoleId, selectedField === 'APPROVER').value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: true,
                    isUploadTask: false
                };
            }
        }
        else {
            getOwnerByRoleRequest = {
                allocationType: 'USER',
                roleDN: '',
                teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                isApproveTask: false,
                isUploadTask: true
            };
            getApproverByRoleRequest = {
                allocationType: 'USER',
                roleDN: '',
                teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                isApproveTask: true,
                isUploadTask: false
            };
        }
        return new Observable(observer => {
            if (getUserByRoleRequest) {
                this.appService.getUsersForTeam(getUserByRoleRequest)
                    .subscribe(response => {
                    if (response && response.users && response.users.user) {
                        const userList = acronui.findObjectsByProp(response, 'user');
                        const users = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(user => {
                                const fieldObj = users.find(e => e.value === user.dn);
                                if (!fieldObj) {
                                    users.push({
                                        name: user.cn,
                                        value: user.dn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        if (userType === 'APPROVER') {
                            this.teamRoleApproverOptions = users;
                            this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
                        }
                        else {
                            this.teamRoleOwnerOptions = users;
                            this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
                        }
                    }
                    else {
                        if (userType === 'APPROVER') {
                            this.teamRoleApproverOptions = [];
                        }
                        else {
                            this.teamRoleOwnerOptions = [];
                        }
                    }
                    this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    this.loaderService.hide();
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                forkJoin([this.appService.getUsersForTeam(getOwnerByRoleRequest), this.appService.getUsersForTeam(getApproverByRoleRequest)])
                    .subscribe(response => {
                    if (response && response[0].users && response[0].users.user &&
                        response[1].users && response[1].users.user) {
                        const ownerList = acronui.findObjectsByProp(response[0], 'user');
                        const approverList = acronui.findObjectsByProp(response[1], 'user');
                        const owners = [];
                        const approvers = [];
                        if (ownerList && ownerList.length && ownerList.length > 0) {
                            ownerList.forEach(owner => {
                                const fieldObj = owners.find(e => e.value === owner.dn);
                                if (!fieldObj) {
                                    owners.push({
                                        name: owner.cn,
                                        value: owner.dn,
                                        displayName: owner.name
                                    });
                                }
                            });
                        }
                        if (approverList && approverList.length && approverList.length > 0) {
                            approverList.forEach(approver => {
                                const fieldObj = approvers.find(e => e.value === approver.dn);
                                if (!fieldObj) {
                                    approvers.push({
                                        name: approver.cn,
                                        value: approver.dn,
                                        displayName: approver.name
                                    });
                                }
                            });
                        }
                        this.teamRoleOwnerOptions = owners;
                        this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
                        this.teamRoleApproverOptions = approvers;
                        this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
                    }
                    else {
                        this.teamRoleOwnerOptions = [];
                        this.teamRoleApproverOptions = [];
                    }
                    this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    this.loaderService.hide();
                    observer.next(false);
                    observer.complete();
                });
            }
        });
    }
    getAssignmentTypes() {
        return new Observable(observer => {
            this.appService.invokeRequest(this.ASSIGNMENT_TYPE_METHOD_NS, this.GET_ASSIGNMENT_TYPE_WS_METHOD_NAME, null)
                .subscribe(response => {
                if (response.Assignment_Type) {
                    if (!Array.isArray(response.Assignment_Type)) {
                        response.Assignment_Type = [response.Assignment_Type];
                    }
                    this.deliverableModalConfig.deliverableOwnerAssignmentType = response.Assignment_Type;
                    this.deliverableModalConfig.deliverableApproverAssignmentType = response.Assignment_Type;
                }
                else {
                    this.deliverableModalConfig.deliverableOwnerAssignmentType = [];
                    this.deliverableModalConfig.deliverableApproverAssignmentType = [];
                }
                // TODO: check for defaultPriority
                observer.next(true);
                observer.complete();
            }, error => {
                const eventData = { message: 'Something went wrong while fetching Assignment types', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    }
    getPreDetails() {
        return new Observable(observer => {
            this.loaderService.show();
            if (this.deliverableConfig.deliverableId) {
                forkJoin([this.getDeliverableDetails(), this.getAllRoles()]).subscribe(() => {
                    forkJoin([this.getPriorities(), this.getUsersByRole(), this.getReviewers(), this.getDeliverableReviewByDeliverableId(this.deliverableConfig.deliverableId)])
                        .subscribe(delResponse => {
                        if (this.deliverableModalConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id'].Id && this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id) {
                            const statusList = [StatusLevels.INTERMEDIATE, StatusLevels.INITIAL];
                            if (this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE) {
                                statusList.pop();
                            }
                            const approverValue = this.deliverableModalConfig.selectedDeliverable && this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID &&
                                this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID['Identity-id'] ?
                                this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID['Identity-id'].Id : '';
                            const ownerValue = this.deliverableModalConfig.selectedDeliverable && this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID &&
                                this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID['Identity-id'] ?
                                this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID['Identity-id'].Id : '';
                            forkJoin([this.getStatus(statusList), this.formApproverValue(approverValue),
                                this.formOwnerValue(ownerValue)])
                                .subscribe(responseList => {
                                this.loaderService.hide();
                                observer.next();
                                observer.complete();
                            }, err => {
                                this.loaderService.hide();
                            });
                        }
                    }, delError => {
                        this.loaderService.hide();
                        observer.error(delError);
                    });
                });
            }
            else {
                forkJoin([this.getPriorities(), this.getStatus([StatusLevels.INITIAL]), this.getAllRoles(), this.getUsersByRole()])
                    .subscribe(responseList => {
                    const ownerValue = this.deliverableModalConfig.selectedTask && this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID &&
                        this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'] ?
                        this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'].Id : '';
                    forkJoin([this.formOwnerValue(ownerValue)])
                        .subscribe(response => {
                        this.loaderService.hide();
                        observer.next();
                        observer.complete();
                    }, err => {
                        this.loaderService.hide();
                    });
                }, err => {
                    this.loaderService.hide();
                    observer.error(err);
                });
            }
        });
    }
    cancelDeliverableCreation() {
        if (this.deliverableForm.pristine) {
            this.closeCallbackHandler.next(true);
        }
        else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    this.closeCallbackHandler.next(true);
                }
            });
        }
    }
    getMaxMinValueForNumber(scale) {
        let numericeVlalue = '99999';
        if (scale) {
            numericeVlalue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericeVlalue) : parseInt(numericeVlalue);
    }
    initialiseDeliverableConfig() {
        this.deliverableModalConfig = {
            hasAllConfig: true,
            deliverablePriorityList: null,
            deliverableStatusList: null,
            defaultStatus: null,
            defaultPriority: null,
            deliverableOwnerConfig: {
                label: 'Deliverable Owner',
                filterOptions: [],
                formControl: null
            },
            deliverableOwnerRoleConfig: {
                label: 'Deliverable Owner Role',
                filterOptions: [],
                formControl: null
            },
            deliverableApprover: {
                label: 'Deliverable Approver',
                filterOptions: [],
                formControl: null
            },
            deliverableApproverRole: {
                label: 'Deliverable Approver Role',
                filterOptions: [],
                formControl: null
            },
            selectedDeliverable: this.deliverableConfig.selectedDeliverable,
            deliverableOwnerAssignmentType: DeliverableConstants.ALLOCATION_TYPE,
            deliverableApproverAssignmentType: DeliverableConstants.ALLOCATION_TYPE,
            isNewDeliverable: (this.deliverableConfig.deliverableId) ? false : true,
            isTemplate: this.deliverableConfig.isTemplate,
            projectId: this.deliverableConfig.projectId,
            taskId: this.deliverableConfig.taskId,
            deliverableId: this.deliverableConfig.deliverableId,
            selectedProject: this.deliverableConfig.selectedProject,
            selectedTask: this.deliverableConfig.selectedTask,
            minDate: '',
            maxDate: '',
            deliverableAllocationList: [],
            currStatus: null
        };
        if (this.deliverableConfig.deliverableId) {
            this.saveOptions = [this.saveOptions[0]];
        }
    }
    ngOnInit() {
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach(user => {
            if (user.Name === 'MPM Project Manager') {
                this.isPM = true;
            }
        });
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        this.isApprover = this.sharingService.getIsApprover();
        this.isUploaded = this.sharingService.getIsUploaded();
        this.projectConfig = this.sharingService.getProjectConfig();
        this.enableNeedReview = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_NEED_REVIEW]);
        this.enableWorkWeek = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.appConfig = this.sharingService.getAppConfig();
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        if (this.deliverableConfig && this.deliverableConfig.taskId) {
            this.initialiseDeliverableConfig();
            this.getPreDetails().subscribe(response => {
                this.initialiseDeliverableForm();
            });
        }
    }
};
DeliverableCreationComponent.ctorParameters = () => [
    { type: DateAdapter },
    { type: AppService },
    { type: UtilService },
    { type: EntityAppDefService },
    { type: DeliverableService },
    { type: LoaderService },
    { type: MatDialog },
    { type: StatusService },
    { type: ProjectUtilService },
    { type: SharingService },
    { type: NotificationService }
];
__decorate([
    Input()
], DeliverableCreationComponent.prototype, "deliverableConfig", void 0);
__decorate([
    Output()
], DeliverableCreationComponent.prototype, "closeCallbackHandler", void 0);
__decorate([
    Output()
], DeliverableCreationComponent.prototype, "saveCallBackHandler", void 0);
__decorate([
    Output()
], DeliverableCreationComponent.prototype, "notificationHandler", void 0);
__decorate([
    ViewChild(CustomSearchFieldComponent)
], DeliverableCreationComponent.prototype, "customSearchFieldComponent", void 0);
__decorate([
    ViewChild('reviewerInput', { static: false })
], DeliverableCreationComponent.prototype, "reviewerInput", void 0);
__decorate([
    ViewChild('auto', { static: false })
], DeliverableCreationComponent.prototype, "matAutocomplete", void 0);
DeliverableCreationComponent = __decorate([
    Component({
        selector: 'mpm-deliverable-creation',
        template: "<div class=\"overview-content\">\r\n    <form class=\"deliverable-creation-form\" *ngIf=\"deliverableForm\" [formGroup]=\"deliverableForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Name</mat-label>\r\n                    <input [appAutoFocus]=\"deliverableModalConfig && deliverableModalConfig.hasAllConfig\" matInput\r\n                        placeholder=\"Deliverable Name\" formControlName=\"deliverableName\" required>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item\" *ngIf=\"!deliverableConfig.isTemplate\">\r\n                <mat-form-field id=\"dueDate\" appearance=\"outline\">\r\n                    <mat-label>Due On</mat-label>\r\n                    <input matInput [min]=\"deliverableModalConfig.minDate\" [max]=\"deliverableModalConfig.maxDate\"\r\n                        [matDatepicker]=\"dueDate\" placeholder=\"Deliverable Due Date (MM/DD/YYYY)\"\r\n                        formControlName=\"dueDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"dueDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #dueDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"(deliverableForm.get('dueDate').hasError('matDatepickerMax') || deliverableForm.get('dueDate').hasError('matDatepickerMin'))\">\r\n                        {{deliverableConstants.DATE_ERROR_MESSAGE}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item skip-review-section-holder\">\r\n                <section class=\"skip-review-check\">\r\n                    <mat-checkbox color=\"primary\" labelPosition=\"before\" formControlName=\"needReview\">\r\n                        <span class=\"skipReview\"> Need Review? </span>\r\n                    </mat-checkbox>\r\n                </section>\r\n            </div>\r\n            <div class=\"flex-row-item flex\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Priority</mat-label>\r\n                    <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" required>\r\n                        <mat-option *ngFor=\"let priority of deliverableModalConfig.deliverablePriorityList\"\r\n                            value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                            {{priority.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Status</mat-label>\r\n                    <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"status\" required>\r\n                        <mat-option *ngFor=\"let status of deliverableModalConfig.deliverableStatusList\"\r\n                            value=\"{{status['MPM_Status-id'].Id}}\">\r\n                            {{status.NAME}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Owner Allocation</mat-label>\r\n                    <mat-select formControlName=\"ownerAllocation\" placeholder=\"Allocation\" required\r\n                        name=\"ownerAllocation\" disable>\r\n                        <mat-option *ngFor=\"let allocationType of deliverableModalConfig.deliverableOwnerAssignmentType\"\r\n                            [value]=\"allocationType.NAME\">\r\n                            {{allocationType.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableOwnerRoleConfig\"\r\n                    [required]=\"deliverableForm.getRawValue().ownerAllocation !== deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE || deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableOwnerConfig\"\r\n                    [required]=\"deliverableForm.getRawValue().ownerAllocation !== deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\" *ngIf=\"deliverableForm.getRawValue().needReview\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Approver Allocation</mat-label>\r\n                    <mat-select formControlName=\"approverAllocation\" placeholder=\"Allocation\"\r\n                        [required]=\"deliverableForm.getRawValue().needReview\" name=\"approverAllocation\" disable>\r\n                        <mat-option\r\n                            *ngFor=\"let allocationType of deliverableModalConfig.deliverableApproverAssignmentType\"\r\n                            [value]=\"allocationType.NAME\">\r\n                            {{allocationType.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().needReview  && deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableApproverRole\"\r\n                    [required]=\"deliverableForm.getRawValue().needReview  && !(deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_USER)\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().needReview && deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE || deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableApprover\"\r\n                    [required]=\"deliverableForm.getRawValue().needReview && !(deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE)\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"task-owner-task-count\" *ngIf=\"hasDeliverableCount\">\r\n            <mat-error *ngIf=\"!isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-error>\r\n            <mat-label *ngIf=\"isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-label>\r\n        </div>\r\n        <!--         <div class=\"flex-row\" *ngIf=\"deliverableForm.getRawValue().needReview && showViewOrEditReviewers\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Reviewers</mat-label>\r\n                    <mat-chip-list #reviewerChipList aria-label=\"Reviewer selection\">\r\n                        <mat-chip *ngFor=\"let reviewer of reviewers\" [selectable]=\"false\" [removable]=\"true\"\r\n                            [ngClass]=\"{'has-reviewed': reviewer.hasReviewed}\">\r\n                            {{reviewer.displayName}}\r\n                            <mat-icon matChipRemove>cancel</mat-icon>\r\n                        </mat-chip>\r\n                        <input #reviewerInput formControlName=\"deliverableReviewers\">\r\n                    </mat-chip-list>\r\n                    <mat-autocomplete #auto=\"matAutocomplete\">\r\n                        <mat-option *ngFor=\"let reviewer of filteredReviewers | async\" [value]=\"reviewer\">\r\n                            {{reviewer.displayName}}\r\n                        </mat-option>\r\n                    </mat-autocomplete>\r\n                </mat-form-field>\r\n            </div>\r\n        </div> -->\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Expected Duration (Days)</mat-label>\r\n                    <input matInput placeholder=\"Expected Duration (Days)\" formControlName=\"expectedDuration\"\r\n                        type=\"number\">\r\n                    <!-- <mat-hint align=\"end\" *ngIf=\"projectOverviewForm.get('expectedDuration').errors\">\r\n                        Please enter the number lesser than 99999\r\n                    </mat-hint> -->\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Description</mat-label>\r\n                    <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item\">\r\n                    <span class=\"form-actions\">\r\n                        <button type=\"button\" mat-stroked-button (click)=\"cancelDeliverableCreation()\">Cancel</button>\r\n                        <button id=\"deliverable-save-btn\" color='primary' mat-flat-button\r\n                            (click)=\"updateDeliverable(SELECTED_SAVE_OPTION)\" type=\"submit\" class=\"ang-btn\"\r\n                            matTooltip=\"{{SELECTED_SAVE_OPTION.name}}\"\r\n                            [disabled]=\"deliverableForm.pristine || deliverableForm.invalid\">{{SELECTED_SAVE_OPTION.name}}</button>\r\n                        <span *ngIf=\"!deliverableConfig.deliverableId\">\r\n                            <button id=\"deliverable-save-option\" mat-flat-button color=\"primary\"\r\n                                [matMenuTriggerFor]=\"saveOptionsMenu\"\r\n                                [disabled]=\"deliverableForm.pristine || deliverableForm.invalid\">\r\n                                <mat-icon>arrow_drop_down</mat-icon>\r\n                            </button>\r\n                            <mat-menu #saveOptionsMenu=\"matMenu\">\r\n                                <span *ngFor=\"let saveOption of saveOptions\">\r\n                                    <button *ngIf=\"saveOption.value != SELECTED_SAVE_OPTION.value\" mat-menu-item\r\n                                        (click)=\"updateDeliverable(saveOption)\">\r\n                                        <span>{{saveOption.name}}</span>\r\n                                    </button>\r\n                                </span>\r\n                            </mat-menu>\r\n                        </span>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>",
        styles: [".project-overview-form{display:flex;flex-direction:column;flex-wrap:wrap;justify-content:center;padding:10px 50px;width:50vw}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-actions button{margin:10px}.task-owner-task-count{margin-left:16px;margin-bottom:16px;font-size:12px;margin-top:-16px}.task-owner-task-count mat-label{color:green}#deliverable-save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}#deliverable-save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:15px}.form-actions{margin-left:auto}mat-form-field{width:100%}.flex-row-item{margin:.666667em 15px 0;font-size:12px}.skip-review-section-holder{flex-grow:3}.flex-row .mat-form-field mpm-custom-search-field{margin:0}.form-group-panel{background:0 0}.form-group-panel ::ng-deep mpm-custom-metadata-field{width:100%}.form-group-panel ::ng-deep .mat-expansion-panel-header{padding:0 4px}.form-group-panel ::ng-deep .mat-expansion-panel-body{padding:0 16px 16px 0}.flex-row-item.flex{width:10.5rem}"]
    })
], DeliverableCreationComponent);
export { DeliverableCreationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtY3JlYXRpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS1jcmVhdGlvbi9kZWxpdmVyYWJsZS1jcmVhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQWdCLFNBQVMsRUFBYyxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUU5RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDakYsT0FBTyxLQUFLLE9BQU8sTUFBTSwyQ0FBMkMsQ0FBQztBQUdyRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwrRUFBK0UsQ0FBQztBQUMzSCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDekYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQzNFLE9BQU8sRUFBZSxZQUFZLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNyRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUN6RixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUM3SCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdURBQXVELENBQUM7QUFDdkYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDOUcsT0FBTyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdoRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDbkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFRcEYsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUErRnJDLFlBQ1csT0FBeUIsRUFDekIsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGtCQUFzQyxFQUN0QyxhQUE0QixFQUM1QixNQUFpQixFQUNqQixhQUE0QixFQUM1QixrQkFBc0MsRUFDdEMsY0FBOEIsRUFDOUIsbUJBQXdDO1FBVnhDLFlBQU8sR0FBUCxPQUFPLENBQWtCO1FBQ3pCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQXZHekMseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMvQyx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFVeEQsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBVXhCLHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBUTVDLGdCQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNwRix5QkFBb0IsR0FBRztZQUNuQixJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxNQUFNO1NBQ2hCLENBQUM7UUFJRixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsdUJBQWtCLEdBQWEsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFHOUMsU0FBSSxHQUFHLEtBQUssQ0FBQztRQUNiLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBRWxCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQVN0Qix5QkFBeUI7UUFDekIsbUJBQWMsR0FBVSxFQUFFLENBQUM7UUFDM0IsaUJBQVksR0FBVSxFQUFFLENBQUM7UUFDekIsNkJBQTZCO1FBQzdCLG1CQUFtQjtRQUNuQix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFFeEIseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBSzFCLDhCQUF5QixHQUFHLGtEQUFrRCxDQUFDO1FBQy9FLDJCQUFzQixHQUFHLCtDQUErQyxDQUFDO1FBQ3pFLGtDQUE2QixHQUFHLHNEQUFzRCxDQUFDO1FBQ3ZGLHVDQUFrQyxHQUFHLDJEQUEyRCxDQUFDO1FBRWpHLHdCQUFtQixHQUFHLG1EQUFtRCxDQUFDO1FBQzFFLHdCQUFtQixHQUFHLDZEQUE2RCxDQUFDO1FBQ3BGLDhCQUF5QixHQUFHLDBEQUEwRCxDQUFDO1FBQ3ZGLDBCQUFxQixHQUFHLHNEQUFzRCxDQUFDO1FBRS9FLHVDQUFrQyxHQUFHLGdCQUFnQixDQUFDO1FBQ3RELG9DQUErQixHQUFHLFVBQVUsQ0FBQztRQUM3QywyQ0FBc0MsR0FBRyxpQkFBaUIsQ0FBQztRQUMzRCxxQ0FBZ0MsR0FBRyxvQkFBb0IsQ0FBQztRQUN4RCwrQ0FBMEMsR0FBRyw2QkFBNkIsQ0FBQztRQUMzRSx1Q0FBa0MsR0FBRyxzQkFBc0IsQ0FBQztRQUM1RCxzQ0FBaUMsR0FBRyxtQkFBbUIsQ0FBQztRQUN4RCxzQ0FBaUMsR0FBRyxpQkFBaUIsQ0FBQztRQUN0RCxnREFBMkMsR0FBRyxxQ0FBcUMsQ0FBQztRQUNwRiwwQ0FBcUMsR0FBRyxtQkFBbUIsQ0FBQztRQUM1RCxnREFBMkMsR0FBRywwQkFBMEIsQ0FBQztRQW1vRHpFLGVBQVUsR0FDVixDQUFDLElBQWlCLEVBQUUsRUFBRTtZQUNsQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0gsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFDOUIsZ0JBQWdCO2dCQUNoQixrQkFBa0I7YUFDckI7UUFDTCxDQUFDLENBQUE7SUEvbkRHLENBQUM7SUFFTCxlQUFlLENBQUMsS0FBVTtRQUN0QixJQUFJLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLElBQUksS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUMzRCxNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFeEMsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1NBQ3hHO0lBQ0wsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFtQztRQUN4QyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNqSCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVuRyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLG1CQUFtQjtZQUM5QyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUU7WUFDNUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLDBCQUEwQixDQUFDLENBQUM7U0FDckY7YUFBTSxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsc0JBQXNCLENBQUMsQ0FBQztTQUNqRjthQUFNO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO2dCQUNyQixXQUFXLEVBQUUsUUFBUSxDQUFDLFdBQVc7YUFDcEMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO2dCQUNyQixXQUFXLEVBQUUsUUFBUSxDQUFDLFdBQVc7YUFDcEMsQ0FBQyxDQUFDO1lBQ0gsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdILElBQUksbUJBQW1CLElBQUksQ0FBQyxFQUFFO2dCQUMxQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3hEO1lBQ0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztnQkFDekIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxXQUFXO2dCQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JCLFdBQVcsRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDakMsV0FBVyxFQUFFLEtBQUs7YUFDckIsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckU7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLEtBQXdCO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRTtZQUM5QixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBRTFCLElBQUksS0FBSyxFQUFFO2dCQUNQLEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO2FBQ3BCO1lBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3JFO0lBQ0wsQ0FBQztJQUVELGdCQUFnQixDQUFDLGFBQWtCO1FBQy9CLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsQ0FBQztRQUNwRyxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsQ0FBQztRQUN6RyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsQ0FBQztRQUM5RyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxLQUFLLEtBQUssYUFBYSxDQUFDLENBQUM7UUFDOUYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztZQUN2QixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7WUFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO1lBQ3JCLFdBQVcsRUFBRSxRQUFRLENBQUMsV0FBVztTQUNwQyxDQUFDLENBQUM7UUFFSCxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4QztRQUNELElBQUksYUFBYSxJQUFJLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNwRDtRQUNELElBQUksZ0JBQWdCLElBQUksQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELHlCQUF5QixDQUFDLGlCQUFpQjtRQUN2QyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDcEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDeEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUMxRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDbEY7YUFBTTtZQUNILElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDL0o7SUFDTCxDQUFDO0lBRUQsNkJBQTZCLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLHlCQUF5QjtRQUNwRixNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDakIsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDdEMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDbkUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDNUQ7WUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4RixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDNUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLDBCQUEwQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0UsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLG9CQUFvQixFQUFFO2dCQUN6RCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDOUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7Z0JBQ2pHLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7YUFDaEc7WUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzNGO2FBQU0sSUFBSSxtQkFBbUIsS0FBSyxvQkFBb0IsQ0FBQyxvQkFBb0IsRUFBRTtZQUMxRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNyRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNwRixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzVFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDeEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzlDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO2dCQUM3RixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzVGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7Z0JBQzNHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzNEO1NBQ0o7SUFDTCxDQUFDO0lBRUQsZ0NBQWdDLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLHlCQUF5QjtRQUN2RixNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDakIsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDekMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDbkUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUMzRixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNwRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDL0UsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDMUUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLHVCQUF1QixFQUFFO2dCQUM1RCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7Z0JBQ2pHLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7YUFDaEc7WUFDRCxJQUFJLHlCQUF5QixFQUFFO2dCQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDcEU7U0FDSjthQUFNLElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDMUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDeEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUMvRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzNFLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztnQkFDN0YsSUFBSSx5QkFBeUIsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNwRTtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRTtnQkFDakgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDOUQ7U0FDSjtJQUNMLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxTQUFTO1FBQ3pCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUU7WUFDN0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRTtnQkFDaEgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDM0Q7WUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDOUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUM7Z0JBQzdGLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNsRSxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFO2dCQUNoRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzVEO1NBQ0o7SUFDTCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsWUFBWTtRQUMvQixJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsSUFBSSxFQUFFO1lBQ25DLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3RILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzlEO1lBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2pELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO2dCQUM3RixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckUsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMvRDtTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBeUJJO0lBRUosaUJBQWlCLENBQUMsVUFBVTtRQUN4QixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUU7WUFDdkgsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEksSUFBSSxJQUFJLEVBQUU7Z0JBQ04sTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsT0FBTzthQUNWO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFO1lBQy9CLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLCtDQUErQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDL0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxPQUFPO1NBQ1Y7UUFDRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEYsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUMzQixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2hILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsT0FBTztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sS0FBSyxPQUFPLEVBQUU7WUFDekMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDbkUsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUseUNBQXlDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDekgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM1QztpQkFBTTtnQkFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDN0M7U0FDSjthQUFNO1lBQ0gsTUFBTSxTQUFTLEdBQUc7Z0JBQ2QsT0FBTyxFQUFFLGtDQUFrQztnQkFDM0MsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJO2FBQ2pELENBQUM7WUFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0wsQ0FBQztJQUVELG9CQUFvQixDQUFDLFFBQWdCO1FBQ2pDLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xFLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7Z0JBQ3hCLGFBQWEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDakQ7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7SUFDRCw0QkFBNEI7O1FBQ3hCLElBQUksaUJBQTBDLENBQUM7UUFDL0MsTUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2pFLGlCQUFpQixHQUFHO1lBQ2hCLGFBQWEsRUFBRTtnQkFDWCxFQUFFLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO2FBQ3RHO1lBQ0QsZUFBZSxFQUFFLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUU7WUFDaEwsV0FBVyxFQUFFLHFCQUFxQixDQUFDLFdBQVc7WUFDOUMsU0FBUyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEosSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7WUFDeEQsT0FBTyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hILHFCQUFxQixDQUFDLE9BQU8sQ0FBQztZQUNsQyxPQUFPLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQztZQUMzSixRQUFRLEVBQUUsSUFBSTtZQUNkLFNBQVMsRUFBRSxLQUFLO1lBQ2hCLGVBQWUsRUFBRSxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUTtZQUM5RSxtQkFBbUIsRUFBRSxxQkFBcUIsQ0FBQyxlQUFlO1lBQzFELHNCQUFzQixFQUFFLHFCQUFxQixDQUFDLGtCQUFrQjtZQUNoRSxnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxnQkFBZ0I7WUFDeEQsU0FBUyxFQUFFO2dCQUNQLFdBQVcsRUFBRTtvQkFDVCxFQUFFLEVBQUUscUJBQXFCLENBQUMsTUFBTTtpQkFDbkM7YUFDSjtZQUNELFdBQVcsRUFBRTtnQkFDVCxhQUFhLEVBQUU7b0JBQ1gsRUFBRSxFQUFFLHFCQUFxQixDQUFDLFFBQVE7aUJBQ3JDO2FBQ0o7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsTUFBTSxFQUFFO29CQUNKLEVBQUUsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7aUJBQzdEO2FBQ0o7WUFDRCxVQUFVLEVBQUU7Z0JBQ1IsU0FBUyxFQUFFO29CQUNQLEVBQUUsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUU7aUJBQ25FO2FBQ0o7WUFDRCxjQUFjLEVBQUU7Z0JBQ1osb0JBQW9CLEVBQUU7b0JBQ2xCLEVBQUUsUUFBRSxxQkFBcUIsQ0FBQyxvQkFBb0IsMENBQUUsSUFBSTtpQkFDdkQ7YUFDSjtZQUNELGlCQUFpQixFQUFFO2dCQUNmLG9CQUFvQixFQUFFO29CQUNsQixFQUFFLFFBQUUscUJBQXFCLENBQUMsdUJBQXVCLDBDQUFFLElBQUk7aUJBQzFEO2FBQ0o7WUFDRCxxQkFBcUIsRUFBRTtnQkFDbkIsVUFBVSxFQUFFO29CQUNSLE1BQU0sUUFBRSxxQkFBcUIsQ0FBQyxnQkFBZ0IsMENBQUUsSUFBSTtpQkFDdkQ7YUFDSjtZQUNELHdCQUF3QixFQUFFO2dCQUN0QixVQUFVLEVBQUU7b0JBQ1IsTUFBTSxRQUFFLHFCQUFxQixDQUFDLG1CQUFtQiwwQ0FBRSxJQUFJO2lCQUMxRDthQUNKO1NBQ0osQ0FBQztRQUNGLE9BQU8saUJBQWlCLENBQUM7SUFDN0IsQ0FBQztJQUVELGVBQWU7UUFDWCxNQUFNLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDMUIsTUFBTSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFNUIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUNoRCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRTtnQkFDcEMsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDeEMsTUFBTSxrQkFBa0IsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssS0FBSyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3RHLElBQUksa0JBQWtCLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQzNCLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQ3BDO2lCQUNKO3FCQUFNO29CQUNILE1BQU0sd0JBQXdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDN0ksSUFBSSx3QkFBd0IsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDakMsTUFBTSxrQkFBa0IsR0FBRyxjQUFjLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssS0FBSyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3RHLElBQUksa0JBQWtCLEtBQUssQ0FBQyxDQUFDLEVBQUU7NEJBQzNCLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7eUJBQ3BDO3FCQUNKO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFhLEVBQUUsRUFBRTtnQkFDeEQsT0FBTztvQkFDSCxhQUFhLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWE7b0JBQ25ELFFBQVEsRUFBRTt3QkFDTixFQUFFLEVBQUUsRUFBRTt3QkFDTixNQUFNLEVBQUUsRUFBRTt3QkFDVixNQUFNLEVBQUUsYUFBYSxDQUFDLEtBQUs7d0JBQzNCLE1BQU0sRUFBRSxLQUFLO3FCQUNoQjtvQkFDRCxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07b0JBQzFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxJQUFJO2lCQUVsQyxDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsRUFBRTtnQkFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ2xDLE1BQU0sd0JBQXdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDakosSUFBSSx3QkFBd0IsSUFBSSxDQUFDLEVBQUU7d0JBQy9CLE1BQU0sYUFBYSxHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUN2RyxJQUFJLGFBQWEsS0FBSyxDQUFDLENBQUMsRUFBRTs0QkFDdEIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3lCQUMxQztxQkFDSjtpQkFDSjtxQkFBTTtvQkFDSCxNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3pILElBQUksa0JBQWtCLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQzNCLE1BQU0sb0JBQW9CLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzlHLE1BQU0sd0JBQXdCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNuSCxJQUFJLG9CQUFvQixLQUFLLENBQUMsQ0FBQyxJQUFJLHdCQUF3QixJQUFJLENBQUMsRUFBRTs0QkFDOUQsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3lCQUMxQztxQkFDSjtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUN6RCxPQUFPO29CQUNILGFBQWEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYTtvQkFDbkQsUUFBUSxFQUFFO3dCQUNOLEVBQUUsRUFBRSxFQUFFO3dCQUNOLE1BQU0sRUFBRSxFQUFFO3dCQUNWLE1BQU0sRUFBRSxRQUFRLENBQUMsS0FBSzt3QkFDdEIsTUFBTSxFQUFFLFFBQVE7cUJBQ25CO29CQUNELE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTTtvQkFDMUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLElBQUk7aUJBRWxDLENBQUM7WUFDTixDQUFDLENBQUMsQ0FBQztTQUVOO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDN0osQ0FBQztJQUVELHdCQUF3QixDQUFDLFVBQVU7UUFDL0IsTUFBTSxpQkFBaUIsR0FBNEIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7UUFDdkYsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELGVBQWUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVO1FBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLEVBQUU7WUFDakQsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMzRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO2dCQUNsSSxpQkFBaUIsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUN4QyxNQUFNLFVBQVUsR0FBRztvQkFDZixpQkFBaUIsRUFBRSxpQkFBaUI7b0JBQ3BDLG9CQUFvQixFQUFFO3dCQUNsQixtQkFBbUIsRUFBRSxJQUFJLENBQUMsWUFBWTtxQkFDekM7aUJBQ0osQ0FBQztnQkFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLGlDQUFpQyxFQUFFLFVBQVUsQ0FBQztxQkFDeEcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUk7MkJBQ3ZHLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFLEVBQUU7d0JBQ25LLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ3RILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ3pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7NEJBQzFCLGFBQWEsRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFOzRCQUN6RSxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07NEJBQzFDLFNBQVMsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUzs0QkFDaEQsVUFBVSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSzs0QkFDakUsVUFBVSxFQUFFLFVBQVU7eUJBQ3pCLENBQUMsQ0FBQztxQkFDTjt5QkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7MkJBQy9HLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7d0JBQ3BGLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUN4SCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUM1Qzt5QkFBTTt3QkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixNQUFNLFNBQVMsR0FBRzs0QkFDZCxPQUFPLEVBQUUsb0RBQW9EOzRCQUM3RCxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7eUJBQ2xELENBQUM7d0JBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7Z0JBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLG9EQUFvRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3JJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzdDLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUM3QjtTQUNKO2FBQU07WUFDSCxNQUFNLE9BQU8sR0FBRztnQkFDWixpQkFBaUIsRUFBRSxpQkFBaUI7YUFDdkMsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsaUNBQWlDLEVBQUUsT0FBTyxDQUFDO2lCQUNyRyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSTt1QkFDdkcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDbkssTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDbkgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFO3dCQUN4QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDOzRCQUMxQixhQUFhLEVBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBRTs0QkFDekUsTUFBTSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNOzRCQUMxQyxTQUFTLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7NEJBQ2hELFVBQVUsRUFBRSxJQUFJOzRCQUNoQixVQUFVLEVBQUUsVUFBVTt5QkFDekIsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7NEJBQzFCLGFBQWEsRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFOzRCQUN6RSxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07NEJBQzFDLFNBQVMsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUzs0QkFDaEQsVUFBVSxFQUFFLEtBQUs7NEJBQ2pCLFVBQVUsRUFBRSxVQUFVO3lCQUN6QixDQUFDLENBQUM7cUJBQ047aUJBRUo7cUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLO3VCQUMvRyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO29CQUNwRixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDeEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDNUM7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsTUFBTSxTQUFTLEdBQUc7d0JBQ2QsT0FBTyxFQUFFLG9EQUFvRDt3QkFDN0QsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO3FCQUNsRCxDQUFDO29CQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQzVDO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLE1BQU0sU0FBUyxHQUFHO29CQUNkLE9BQU8sRUFBRSxvREFBb0Q7b0JBQzdELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSztpQkFDbEQsQ0FBQztnQkFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7UUFVSTtJQUVKLGFBQWEsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CO1FBQ25DLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxtQkFBbUIsRUFBRTtnQkFDckIsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7Z0JBQzFGLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDN0M7aUJBQU07Z0JBQ0gsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7Z0JBQ3ZGLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDN0M7U0FDSjthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFFRCxjQUFjLENBQUMsTUFBTTtRQUNqQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksTUFBTSxFQUFFO2dCQUNSLE1BQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxNQUFNO2lCQUNqQixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN2RSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2pHLElBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO29CQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsTUFBTTtRQUNwQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksTUFBTSxFQUFFO2dCQUNSLE1BQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxNQUFNO2lCQUNqQixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN2RSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxZQUFZLENBQUM7b0JBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxXQUFXO1FBQzFCLElBQUksV0FBVyxDQUFDLGtCQUFrQixJQUFJLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLE9BQU8sV0FBVyxDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBRTtZQUNuTCxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsa0JBQWtCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNoSixPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQzdDO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQscUJBQXFCLENBQUMsV0FBVztRQUM3QixJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMscUJBQXFCLElBQUksT0FBTyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFFO1lBQzFJLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3RKLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDN0M7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxXQUFXO1FBQ2hDLElBQUksV0FBVyxFQUFFO1lBQ2IsT0FBTyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUN2RjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELDJCQUEyQixDQUFDLFdBQVc7UUFDbkMsSUFBSSxXQUFXLEVBQUU7WUFDYixPQUFPLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQzdGO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUM7UUFDMUQsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZ0JBQWdCLENBQUM7UUFDdEUsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztRQUN6QixNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQztRQUM1RSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLHNCQUFzQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUI7WUFDL0YsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNqSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO1FBQzFGLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDeEYsTUFBTSxPQUFPLEdBQUcsc0JBQXNCLENBQUM7UUFDdkMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFO1lBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQztTQUNsRDtRQUNELElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDYixJQUFJLGdCQUFnQixFQUFFO2dCQUNsQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksU0FBUyxDQUFDO29CQUNqQyxlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3BDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNoRyxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQzs0QkFDaEgsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzs0QkFDM0UsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ2hILEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDOzRCQUM1RyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSTtxQkFDNUssQ0FBQztvQkFDRixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsWUFBWTtxQkFDbkYsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQy9FLGtCQUFrQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNoQyxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNwQyxDQUFDO29CQUNGLGVBQWUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUMxRixDQUFDO29CQUNGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQzs0QkFDM0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUN0RCxDQUFDO29CQUNGLG1CQUFtQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzNFLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNsQyxLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxlQUFlLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQzs0QkFDM0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNySixDQUFDO29CQUNGLHVCQUF1QixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQy9FLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzVFLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM3RyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxZQUFZLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVO29CQUN6RyxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFDN0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDakMsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsbUJBQW1CLENBQUMsSUFBSTt3QkFDL0IsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNoRyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhO3dCQUN6QixRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNsQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDO3dCQUNuRCxRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLG1CQUFtQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNqQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjt3QkFDNUIsUUFBUSxFQUFFLFlBQVksSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxZQUFZLENBQUMsWUFBWTtxQkFDOUcsQ0FBQztvQkFDRix1QkFBdUIsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDdEQsUUFBUSxFQUFFLFlBQVksSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxZQUFZLENBQUMsWUFBWTtxQkFDOUcsQ0FBQztvQkFDRixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxXQUFXLElBQUksbUJBQW1CLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUNySixRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLG1CQUFtQixDQUFDLGFBQWEsSUFBSSxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUMvSixRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNyQixLQUFLLEVBQUUsbUJBQW1CLENBQUMsUUFBUTt3QkFDbkMsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDekIsS0FBSyxFQUFFLE9BQU8sbUJBQW1CLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDOzRCQUN4RCxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3hDLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7d0JBQ2xHLFFBQVEsRUFBRSxJQUFJO3FCQUNqQixDQUFDO29CQUNGLHdIQUF3SDtvQkFDeEgsa0JBQWtCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsMkJBQTJCLENBQUMsbUJBQW1CLENBQUM7d0JBQzVELFFBQVEsRUFBRSxZQUFZLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7cUJBQzlHLENBQUM7b0JBQ0YsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG1CQUFtQixDQUFDO3dCQUN6RCxRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM3RixnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixFQUNoSixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMzRSxDQUFDLENBQUM7Z0JBQ0gsc0RBQXNEO2dCQUN0RCxpQ0FBaUM7YUFDcEM7U0FDSjthQUFNO1lBQ0gsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDakMsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNwQyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDaEcsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7NEJBQ2hILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7NEJBQzNFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNoSCxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQzs0QkFDNUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUk7cUJBQzVLLENBQUM7b0JBQ0YsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQy9FLGtCQUFrQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNoQyxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNwQyxDQUFDO29CQUNGLGVBQWUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUMxRixDQUFDO29CQUNGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQzs0QkFDM0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUN0RCxDQUFDO29CQUNGLG1CQUFtQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzNFLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNsQyxLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxlQUFlLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQzs0QkFDM0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNySixDQUFDO29CQUNGLHVCQUF1QixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQy9FLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM3RixnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDN0csQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsWUFBWSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO29CQUM1RixDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFDN0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLFNBQVMsQ0FBQztvQkFDakMsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsbUJBQW1CLENBQUMsSUFBSTt3QkFDL0IsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNoRyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhO3dCQUN6QixRQUFRLEVBQUUsWUFBWTtxQkFDekIsQ0FBQztvQkFDRixvQkFBb0IsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDbEMsS0FBSyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDbkQsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLENBQUM7b0JBQ0YsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2pDLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCO3dCQUM1QixRQUFRLEVBQUUsWUFBWTtxQkFDekIsQ0FBQztvQkFDRix1QkFBdUIsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDdEQsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLENBQUM7b0JBQ0YsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNwQixLQUFLLEVBQUUsbUJBQW1CLENBQUMsV0FBVyxJQUFJLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDckosUUFBUSxFQUFFLElBQUk7cUJBQ2pCLENBQUM7b0JBQ0YsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsbUJBQW1CLENBQUMsYUFBYSxJQUFJLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQy9KLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3pCLEtBQUssRUFBRSxPQUFPLG1CQUFtQixDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsQ0FBQzs0QkFDeEQsbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUN4QyxRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN4QixLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJO3dCQUNsRyxRQUFRLEVBQUUsSUFBSTtxQkFDakIsQ0FBQztvQkFDRix3SEFBd0g7b0JBQ3hILGtCQUFrQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNoQyxLQUFLLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixDQUFDLG1CQUFtQixDQUFDO3dCQUM1RCxRQUFRLEVBQUUsWUFBWTtxQkFDekIsQ0FBQztvQkFDRixlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUM7d0JBQ3pELFFBQVEsRUFBRSxZQUFZO3FCQUN6QixDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM3RixnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixFQUNoSixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMzRSxDQUFDLENBQUM7YUFDTjtTQUNKO1FBRUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDdkYsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLEdBQUcsQ0FBQyxDQUFDLFFBQXVCLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFN0csSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUNoSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDO1FBQ2hILElBQUksQ0FBQyxzQkFBc0IsQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUM7UUFDeEgsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQztRQUV4SCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDbkcsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUNuSCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM1RDtZQUNELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDckYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzlDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDeEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUMxRSxDQUFDLENBQUMsQ0FBQzthQUNOO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLEtBQUssS0FBSyxvQkFBb0IsQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUU7Z0JBQ3pILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQy9EO1lBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGdCQUFnQixFQUFFO2dCQUN4RixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUNyRixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzdFLENBQUMsQ0FBQyxDQUFDO2FBQ047U0FDSjtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDaEYsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxzSUFBc0k7UUFFdEksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsRUFBRTtZQUN2RixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFO2dCQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbkU7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFO2dCQUNoRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsNkJBQTZCLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hFLENBQUMsQ0FBQyxDQUFDO1FBRUgsK0lBQStJO1FBRS9JLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsRUFBRTtZQUMxRixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixFQUFFO2dCQUN2RCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDdEU7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUNuRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbEU7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFO2dCQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbkU7WUFDRCxJQUFJLENBQUMsZ0NBQWdDLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNFLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUNsRixJQUFJLFNBQVMsSUFBSSxPQUFPLFNBQVMsS0FBSyxRQUFRLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxDQUFDLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUM1RixJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0o7aUJBQU07Z0JBQ0gsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsMEJBQTBCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxDQUFDLENBQUM7Z0JBQzVJLElBQUksaUJBQWlCLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2lCQUNsRjthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3hGLElBQUksWUFBWSxJQUFJLE9BQU8sWUFBWSxLQUFLLFFBQVEsRUFBRTtnQkFDbEQsSUFBSSxDQUFDLENBQUMsbUJBQW1CLElBQUksbUJBQW1CLENBQUMsZ0JBQWdCLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzVGLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDN0M7YUFDSjtpQkFBTTtnQkFDSCxNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxZQUFZLENBQUMsQ0FBQztnQkFDL0ksSUFBSSxvQkFBb0IsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7aUJBQ3hGO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7WUFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDL0UsSUFBSSxDQUFDLENBQUMsVUFBVSxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsQ0FBQyxFQUFFO29CQUNqRCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLENBQUM7b0JBQ3ZJLElBQUksYUFBYSxFQUFFO3dCQUNmLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztxQkFDMUU7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtZQUNuRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFO2dCQUNyRixJQUFJLENBQUMsQ0FBQyxhQUFhLElBQUksT0FBTyxhQUFhLEtBQUssUUFBUSxDQUFDLEVBQUU7b0JBQ3ZELE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxLQUFLLGFBQWEsQ0FBQyxDQUFDO29CQUNoSixJQUFJLGdCQUFnQixFQUFFO3dCQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztxQkFDaEY7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztJQUM1RCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLGdCQUFnQixHQUFHO2dCQUNyQixTQUFTLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7YUFDbkQsQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDbkgsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFO29CQUM5QixNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUNwQyxJQUFJLFVBQVUsQ0FBQyxhQUFhLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO3dCQUN6RSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7cUJBQzNGO29CQUNELElBQUksVUFBVSxDQUFDLFNBQVMsSUFBSSxVQUFVLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dCQUM5RCxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDO3FCQUNoRjtvQkFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7aUJBQ2xFO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUscURBQXFELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDdEksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWM7UUFDVixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLFNBQVMsRUFBRTtvQkFDUCxFQUFFLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07aUJBQ3pDO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQywrQkFBK0IsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDN0csU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUMzQixNQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUM5QixJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQzVEO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsa0RBQWtELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDbkksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFCQUFxQjtRQUNqQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRTFCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDO2lCQUM3RSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7b0JBQ2xDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO29CQUN2RSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDbEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ3RJO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUseURBQXlELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDMUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCOzs7Ozs7Ozs7Ozs7Ozs7OztzQkFpQlU7WUFFTixJQUFJLFFBQVEsR0FBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQTtZQUMvRSxtQ0FBbUM7WUFDOUIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzFCLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN6QjtnQkFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLEdBQUcsUUFBUSxDQUFDO2FBQ2xFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7YUFDNUQ7WUFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4Qjs7Ozs7a0JBS007UUFDZCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxTQUFTLENBQUMsZUFBbUM7UUFDekMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztpQkFDcEUsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLGNBQWMsRUFBRTtvQkFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7d0JBQ2hDLGNBQWMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3FCQUNyQztvQkFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDN0UsT0FBTyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO29CQUMxRSxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO2lCQUMxRDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDhDQUE4QyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQy9ILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7UUFDUCxNQUFNLGdCQUFnQixHQUFHO1lBQ3JCLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO1lBQ2hGLGNBQWMsRUFBRSxLQUFLO1lBQ3JCLFlBQVksRUFBRSxJQUFJO1NBQ3JCLENBQUM7UUFFRixNQUFNLG1CQUFtQixHQUFHO1lBQ3hCLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO1lBQ2hGLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLFlBQVksRUFBRSxLQUFLO1NBQ3RCLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO2lCQUN4RyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTO29CQUM1RCxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDdEUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMscUJBQXFCLEVBQUU7b0JBRWxGLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7b0JBQzFCLE1BQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQztvQkFDdEIsTUFBTSxhQUFhLEdBQUcsRUFBRSxDQUFDO29CQUV6QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUNuRSxhQUFhLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3FCQUN2RTtvQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUNuRSxnQkFBZ0IsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQUM7cUJBQzFFO29CQUNELGFBQWEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDbEUsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUM7b0JBRXJFLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ25FLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ3JCLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0NBQ1osSUFBSSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTztnQ0FDbkIsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUN0QixRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWE7NkJBQy9CLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFFRCxJQUFJLGdCQUFnQixJQUFJLGdCQUFnQixDQUFDLE1BQU0sSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUM1RSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ3hCLGFBQWEsQ0FBQyxJQUFJLENBQUM7Z0NBQ2YsSUFBSSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTztnQ0FDbkIsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUN0QixRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWE7NkJBQy9CLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFDRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsVUFBVSxDQUFDO29CQUN4QyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsYUFBYSxDQUFDO29CQUM5QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsMEJBQTBCLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztvQkFDbEcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUM7aUJBQ3JHO3FCQUFNO29CQUNILElBQUksQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUM7b0JBQ2hDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUM7aUJBQ3RDO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLE1BQU07UUFDbEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3pDLE1BQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxNQUFNO2lCQUNqQixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN2RSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDbkYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFtQyxDQUFDLGFBQWE7UUFDN0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLG1DQUFtQyxDQUFDLGFBQWEsQ0FBQztpQkFDN0QsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3hDLE1BQU0sYUFBYSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDL0UsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDbkUsYUFBYSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTs0QkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRTtnQ0FDaEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO29DQUN6RixJQUFJLFlBQVksRUFBRTt3Q0FDZCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dDQUMvRyxJQUFJLENBQUMsZUFBZSxFQUFFOzRDQUNsQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnREFDckIsSUFBSSxFQUFFLFlBQVksQ0FBQyxRQUFRO2dEQUMzQixLQUFLLEVBQUUsWUFBWSxDQUFDLE1BQU07Z0RBQzFCLFdBQVcsRUFBRSxZQUFZLENBQUMsUUFBUTs2Q0FDckMsQ0FBQyxDQUFDOzRDQUNILElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7Z0RBQzNCLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDM0IsS0FBSyxFQUFFLFlBQVksQ0FBQyxNQUFNO2dEQUMxQixXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVE7NkNBQ3JDLENBQUMsQ0FBQzt5Q0FDTjtxQ0FDSjtvQ0FDRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7d0NBQy9HLE1BQU0sbUJBQW1CLEdBQUcsY0FBYyxDQUFDLFlBQVksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3dDQUNuRixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7d0NBQ3BHLElBQUksYUFBYSxJQUFJLENBQUMsRUFBRTs0Q0FDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQzt5Q0FDNUU7NkNBQU07NENBQ0gsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztnREFDekIsSUFBSSxFQUFFLFlBQVksQ0FBQyxRQUFRO2dEQUMzQixLQUFLLEVBQUUsWUFBWSxDQUFDLE1BQU07Z0RBQzFCLFdBQVcsRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDbEMsV0FBVyxFQUFFLG1CQUFtQjs2Q0FDbkMsQ0FBQyxDQUFDO3lDQUNOO29DQUNMLENBQUMsQ0FBQyxDQUFDO29DQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUNuQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0NBQ3hCLENBQUMsQ0FBQyxDQUFDOzZCQUNOO2lDQUFNO2dDQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0NBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs2QkFDdkI7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtpQkFDSjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUNHLEdBQUcsRUFBRTtnQkFDRCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnRUFBZ0UsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqSixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDZixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZO1FBQ1IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxhQUFhLEVBQUU7Z0JBQzNFLElBQUksc0JBQXNCLEdBQUcsRUFBRSxDQUFDO2dCQUNoQyxzQkFBc0IsR0FBRztvQkFDckIsY0FBYyxFQUFFLE1BQU07b0JBQ3RCLE1BQU0sRUFBRSxFQUFFO29CQUNWLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO29CQUNoRixhQUFhLEVBQUUsSUFBSTtvQkFDbkIsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFFBQVEsRUFBRSxJQUFJO2lCQUNqQixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDO3FCQUNsRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7d0JBQ25ELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQzdELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQzt3QkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDcEQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQ0FDcEIsTUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLEtBQUssQ0FBQyxJQUFJLENBQUM7d0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFO3dDQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTt3Q0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7cUNBQ3pCLENBQUMsQ0FBQztpQ0FDTjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztxQkFDN0I7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7cUJBQzFCO29CQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEdBQUcsRUFBRTtvQkFDSixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxvREFBb0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNySSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsY0FBYyxDQUFDLEtBQU0sRUFBRSxrQkFBbUI7UUFDdEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLG9CQUFvQixHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLFFBQVEsQ0FBQztRQUNiLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixNQUFNLGNBQWMsR0FBRyxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLENBQUM7WUFDeEosUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNqQixvQkFBb0IsR0FBRztnQkFDbkIsY0FBYyxFQUFFLGNBQWM7Z0JBQzlCLE1BQU0sRUFBRSxjQUFjLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUMzRCxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUMvSixNQUFNLEVBQUUsY0FBYyxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDN0ksYUFBYSxFQUFFLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7Z0JBQ2pELFlBQVksRUFBRSxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLO2FBQ25ELENBQUM7U0FDTDthQUFNLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixFQUFFO1lBQ3hELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztZQUNuSixJQUFJLGFBQWEsS0FBSyxPQUFPLEVBQUU7Z0JBQzNCLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDN0YsTUFBTSxjQUFjLEdBQUcsY0FBYyxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN0TCxRQUFRLEdBQUcsYUFBYSxDQUFDO2dCQUN6QixvQkFBb0IsR0FBRztvQkFDbkIsY0FBYyxFQUFFLGNBQWM7b0JBQzlCLE1BQU0sRUFBRSxjQUFjLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNwSCxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtvQkFDaEYsYUFBYSxFQUFFLEtBQUs7b0JBQ3BCLFlBQVksRUFBRSxJQUFJO2lCQUNyQixDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUM7Z0JBQ2xHLE1BQU0sc0JBQXNCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLHdCQUF3QixDQUFDO2dCQUN4RyxNQUFNLG1CQUFtQixHQUFHLG1CQUFtQixLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFDcEYsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzNHLE1BQU0sc0JBQXNCLEdBQUcsc0JBQXNCLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUMxRixJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDOUcscUJBQXFCLEdBQUc7b0JBQ3BCLGNBQWMsRUFBRSxtQkFBbUI7b0JBQ25DLE1BQU0sRUFBRSxtQkFBbUIsS0FBSyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7d0JBQ2hFLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUM3RCxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtvQkFDaEYsYUFBYSxFQUFFLEtBQUs7b0JBQ3BCLFlBQVksRUFBRSxJQUFJO2lCQUNyQixDQUFDO2dCQUNGLHdCQUF3QixHQUFHO29CQUN2QixjQUFjLEVBQUUsc0JBQXNCO29CQUN0QyxNQUFNLEVBQUUsc0JBQXNCLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixFQUFFLGFBQWEsS0FBSyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZGLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO29CQUNoRixhQUFhLEVBQUUsSUFBSTtvQkFDbkIsWUFBWSxFQUFFLEtBQUs7aUJBQ3RCLENBQUM7YUFDTDtTQUNKO2FBQU07WUFDSCxxQkFBcUIsR0FBRztnQkFDcEIsY0FBYyxFQUFFLE1BQU07Z0JBQ3RCLE1BQU0sRUFBRSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO2dCQUNoRixhQUFhLEVBQUUsS0FBSztnQkFDcEIsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQztZQUNGLHdCQUF3QixHQUFHO2dCQUN2QixjQUFjLEVBQUUsTUFBTTtnQkFDdEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hGLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixZQUFZLEVBQUUsS0FBSzthQUN0QixDQUFDO1NBQ0w7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksb0JBQW9CLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDO3FCQUNoRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7d0JBQ25ELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQzdELE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQzt3QkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDcEQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQ0FDcEIsTUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLEtBQUssQ0FBQyxJQUFJLENBQUM7d0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFO3dDQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTt3Q0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7cUNBQ3pCLENBQUMsQ0FBQztpQ0FDTjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFDRCxJQUFJLFFBQVEsS0FBSyxVQUFVLEVBQUU7NEJBQ3pCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxLQUFLLENBQUM7NEJBQ3JDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO3lCQUNoRzs2QkFBTTs0QkFDSCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDOzRCQUNsQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQzt5QkFDaEc7cUJBQ0o7eUJBQU07d0JBQ0gsSUFBSSxRQUFRLEtBQUssVUFBVSxFQUFFOzRCQUN6QixJQUFJLENBQUMsdUJBQXVCLEdBQUcsRUFBRSxDQUFDO3lCQUNyQzs2QkFBTTs0QkFDSCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxDQUFDO3lCQUNsQztxQkFDSjtvQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxzREFBc0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUN2SSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7cUJBQ3hILFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUk7d0JBQ3ZELFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7d0JBRTdDLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ2pFLE1BQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ3BFLE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQzt3QkFDbEIsTUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDO3dCQUNyQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUN2RCxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUN0QixNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7Z0NBQ3hELElBQUksQ0FBQyxRQUFRLEVBQUU7b0NBQ1gsTUFBTSxDQUFDLElBQUksQ0FBQzt3Q0FDUixJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUU7d0NBQ2QsS0FBSyxFQUFFLEtBQUssQ0FBQyxFQUFFO3dDQUNmLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSTtxQ0FDMUIsQ0FBQyxDQUFDO2lDQUNOOzRCQUNMLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUNELElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxNQUFNLElBQUksWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ2hFLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0NBQzVCLE1BQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQ0FDOUQsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQ0FDWCxTQUFTLENBQUMsSUFBSSxDQUFDO3dDQUNYLElBQUksRUFBRSxRQUFRLENBQUMsRUFBRTt3Q0FDakIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxFQUFFO3dDQUNsQixXQUFXLEVBQUUsUUFBUSxDQUFDLElBQUk7cUNBQzdCLENBQUMsQ0FBQztpQ0FDTjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFDRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDO3dCQUNuQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQzt3QkFDN0YsSUFBSSxDQUFDLHVCQUF1QixHQUFHLFNBQVMsQ0FBQzt3QkFDekMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7cUJBQ2hHO3lCQUFNO3dCQUNILElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7d0JBQy9CLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7cUJBQ3JDO29CQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3ZJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDVjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtCQUFrQjtRQUNkLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxJQUFJLENBQUM7aUJBQ3ZHLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLENBQUMsZUFBZSxFQUFFO29CQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEVBQUU7d0JBQzFDLFFBQVEsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7cUJBQ3pEO29CQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyw4QkFBOEIsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDO29CQUN0RixJQUFJLENBQUMsc0JBQXNCLENBQUMsaUNBQWlDLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQztpQkFDNUY7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixDQUFDLDhCQUE4QixHQUFHLEVBQUUsQ0FBQztvQkFDaEUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGlDQUFpQyxHQUFHLEVBQUUsQ0FBQztpQkFDdEU7Z0JBQ0Qsa0NBQWtDO2dCQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3ZJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFO2dCQUN0QyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7b0JBQ3hFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzt5QkFDdkosU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFO3dCQUNyQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsRUFBRTs0QkFDN0osTUFBTSxVQUFVLEdBQUcsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDckUsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxZQUFZLENBQUMsWUFBWSxFQUFFO2dDQUNuRixVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7NkJBQ3BCOzRCQUNELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCO2dDQUMxSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQ0FDdEYsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUNqRyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQjtnQ0FDcEksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0NBQ25GLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDOUYsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDO2dDQUMzRSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7aUNBQzVCLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTtnQ0FDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUIsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO2dDQUNoQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NEJBQ3hCLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtnQ0FDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDOzRCQUM5QixDQUFDLENBQUMsQ0FBQzt5QkFDVjtvQkFDTCxDQUFDLEVBQUUsUUFBUSxDQUFDLEVBQUU7d0JBQ1YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDN0IsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztxQkFDOUcsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUN0QixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsYUFBYTt3QkFDakgsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzt3QkFDdkUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xGLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt5QkFDdEMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNsQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFO3dCQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzlCLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtvQkFDTCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNWO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7WUFDL0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0gsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzNELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxFQUFFLGlDQUFpQztvQkFDMUMsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNyQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3ZDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxLQUFLO1FBQ3pCLElBQUksY0FBYyxHQUFHLE9BQU8sQ0FBQztRQUM3QixJQUFJLEtBQUssRUFBRTtZQUNQLGNBQWMsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDOUQ7UUFDRCxrQ0FBa0M7UUFDbEMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCwyQkFBMkI7UUFDdkIsSUFBSSxDQUFDLHNCQUFzQixHQUFHO1lBQzFCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLHVCQUF1QixFQUFFLElBQUk7WUFDN0IscUJBQXFCLEVBQUUsSUFBSTtZQUMzQixhQUFhLEVBQUUsSUFBSTtZQUNuQixlQUFlLEVBQUUsSUFBSTtZQUNyQixzQkFBc0IsRUFBRTtnQkFDcEIsS0FBSyxFQUFFLG1CQUFtQjtnQkFDMUIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsMEJBQTBCLEVBQUU7Z0JBQ3hCLEtBQUssRUFBRSx3QkFBd0I7Z0JBQy9CLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELG1CQUFtQixFQUFFO2dCQUNqQixLQUFLLEVBQUUsc0JBQXNCO2dCQUM3QixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCx1QkFBdUIsRUFBRTtnQkFDckIsS0FBSyxFQUFFLDJCQUEyQjtnQkFDbEMsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQjtZQUMvRCw4QkFBOEIsRUFBRSxvQkFBb0IsQ0FBQyxlQUFlO1lBQ3BFLGlDQUFpQyxFQUFFLG9CQUFvQixDQUFDLGVBQWU7WUFDdkUsZ0JBQWdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN2RSxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVU7WUFDN0MsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTO1lBQzNDLE1BQU0sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTTtZQUNyQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWE7WUFDbkQsZUFBZSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlO1lBQ3ZELFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWTtZQUNqRCxPQUFPLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gseUJBQXlCLEVBQUUsRUFBRTtZQUM3QixVQUFVLEVBQUUsSUFBSTtTQUNuQixDQUFDO1FBQ0YsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQy9ELElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDL0MsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLHFCQUFxQixFQUFFO2dCQUNyQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUNwQjtRQUNMLENBQUMsQ0FBQyxDQUFBO1FBRUYsSUFBSSxTQUFTLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUMvSSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQzNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzSSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2xGLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7WUFDekQsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Q0FhSixDQUFBOztZQTNvRHVCLFdBQVc7WUFDUixVQUFVO1lBQ1QsV0FBVztZQUNILG1CQUFtQjtZQUNwQixrQkFBa0I7WUFDdkIsYUFBYTtZQUNwQixTQUFTO1lBQ0YsYUFBYTtZQUNSLGtCQUFrQjtZQUN0QixjQUFjO1lBQ1QsbUJBQW1COztBQXhHMUM7SUFBUixLQUFLLEVBQUU7dUVBQW1CO0FBQ2pCO0lBQVQsTUFBTSxFQUFFOzBFQUFnRDtBQUMvQztJQUFULE1BQU0sRUFBRTt5RUFBK0M7QUFDOUM7SUFBVCxNQUFNLEVBQUU7eUVBQStDO0FBRWpCO0lBQXRDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQztnRkFBd0Q7QUErRC9DO0lBQTlDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7bUVBQTZDO0FBQ3JEO0lBQXJDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7cUVBQWtDO0FBdkU5RCw0QkFBNEI7SUFOeEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLDBCQUEwQjtRQUNwQyxncVlBQW9EOztLQUV2RCxDQUFDO0dBRVcsNEJBQTRCLENBMnVEeEM7U0EzdURZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZHJlbiwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlQ29uc3RhbnRzIH0gZnJvbSAnLi4vZGVsaXZlcmFibGUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uLy4uLy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVVwZGF0ZU9iamVjdCB9IGZyb20gJy4vZGVsaXZlcmFibGUudXBkYXRlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBBbGxvY2F0aW9uVHlwZXMgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9BbGxvY2F0aW9uVHlwZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5cclxuaW1wb3J0IHsgUm9sZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvcm9sZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNlcnZpY2UgfSBmcm9tICcuLi9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNhdmVPcHRpb25Db25zdGFudCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2F2ZS1vcHRpb25zLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFN0YXR1c1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvc3RhdHVzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNMZXZlbCwgU3RhdHVzTGV2ZWxzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IFByb2plY3RVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LXV0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlVHlwZXMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvbXBtLXV0aWxzL29iamVjdHMvRGVsaXZlcmFibGVUeXBlcyc7XHJcbmltcG9ydCB7IEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY3VzdG9tLXNlYXJjaC1maWVsZC9jdXN0b20tc2VhcmNoLWZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBzdGFydFdpdGgsIG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTWF0QXV0b2NvbXBsZXRlLCBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlJztcclxuaW1wb3J0IHsgTWF0Q2hpcElucHV0RXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jaGlwcyc7XHJcbmltcG9ydCB7IEVOVEVSLCBDT01NQSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7IG5ld0FycmF5IH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXIvc3JjL3V0aWwnO1xyXG5pbXBvcnQgeyBUYXNrVHlwZXMgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9UYXNrVHlwZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1kZWxpdmVyYWJsZS1jcmVhdGlvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGVsaXZlcmFibGUtY3JlYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGVsaXZlcmFibGUtY3JlYXRpb24uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIERlbGl2ZXJhYmxlQ3JlYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGRlbGl2ZXJhYmxlQ29uZmlnO1xyXG4gICAgQE91dHB1dCgpIGNsb3NlQ2FsbGJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2F2ZUNhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG5vdGlmaWNhdGlvbkhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAVmlld0NoaWxkKEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50KSBjdXN0b21TZWFyY2hGaWVsZENvbXBvbmVudDogQ3VzdG9tU2VhcmNoRmllbGRDb21wb25lbnQ7XHJcblxyXG4gICAgZGVsaXZlcmFibGVNb2RhbENvbmZpZztcclxuICAgIHRlYW1Pd25lclJvbGVzT3B0aW9ucztcclxuICAgIHRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucztcclxuICAgIHRlYW1Sb2xlT3duZXJPcHRpb25zO1xyXG4gICAgdGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcblxyXG4gICAgc2VsZWN0ZWRPd25lciA9IG51bGw7XHJcbiAgICBzZWxlY3RlZEFwcHJvdmVyID0gbnVsbDtcclxuXHJcbiAgICBkZWxpdmVyYWJsZVR5cGVJZDtcclxuXHJcbiAgICBkZWxpdmVyYWJsZUZvcm06IEZvcm1Hcm91cDtcclxuXHJcbiAgICBoYXNEZWxpdmVyYWJsZUNvdW50O1xyXG4gICAgb3duZXJQcm9qZWN0Q291bnRNZXNzYWdlO1xyXG4gICAgaXNaZXJvT3duZXJQcm9qZWN0Q291bnQ7XHJcbiAgICBtb2RhbExhYmVsO1xyXG4gICAgZGVsaXZlcmFibGVDb25zdGFudHMgPSBEZWxpdmVyYWJsZUNvbnN0YW50cztcclxuXHJcbiAgICBlbmFibGVOZWVkUmV2aWV3O1xyXG4gICAgZW5hYmxlV29ya1dlZWs7XHJcbiAgICBwcm9qZWN0Q29uZmlnO1xyXG4gICAgYXBwQ29uZmlnO1xyXG4gICAgbmFtZVN0cmluZ1BhdHRlcm47XHJcblxyXG4gICAgc2F2ZU9wdGlvbnMgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KFNhdmVPcHRpb25Db25zdGFudC5kZWxpdmVyYWJsZVNhdmVPcHRpb25zKSk7XHJcbiAgICBTRUxFQ1RFRF9TQVZFX09QVElPTiA9IHtcclxuICAgICAgICBuYW1lOiAnU2F2ZScsXHJcbiAgICAgICAgdmFsdWU6ICdTQVZFJ1xyXG4gICAgfTtcclxuXHJcbiAgICBvbGRGb3JtVmFsdWU6IERlbGl2ZXJhYmxlVXBkYXRlT2JqZWN0O1xyXG5cclxuICAgIHZpc2libGUgPSB0cnVlO1xyXG4gICAgc2VsZWN0YWJsZSA9IHRydWU7XHJcbiAgICByZW1vdmFibGUgPSB0cnVlO1xyXG4gICAgYWRkT25CbHVyID0gdHJ1ZTtcclxuICAgIHNlcGFyYXRvcktleXNDb2RlczogbnVtYmVyW10gPSBbRU5URVIsIENPTU1BXTtcclxuICAgIGlzQXBwcm92ZXI7XHJcbiAgICBsb2dnZWRJblVzZXI7XHJcbiAgICBpc1BNID0gZmFsc2U7XHJcbiAgICBuZXdSZXZpZXdlcnMgPSBbXTtcclxuICAgIG5ld3Jldmlld2Vyc09iajtcclxuICAgIHJlbW92ZWRSZXZpZXdlcnMgPSBbXTtcclxuICAgIHJlbW92ZWRyZXZpZXdlcnNPYmo7XHJcbiAgICByZXZpZXdlcnNPYmo7XHJcbiAgICBjaGlwQ2hhbmdlZDtcclxuICAgIGlzVXBsb2FkZWQ7XHJcbiAgICByZXZpZXdlckNvbXBsZXRlZDtcclxuICAgIHNob3dSZXZpZXdlcjtcclxuXHJcbiAgICBmaWx0ZXJlZFJldmlld2VyczogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgICAvLyByZXZpZXdlcnM6IGFueVtdID0gW107XHJcbiAgICBzYXZlZFJldmlld2VyczogYW55W10gPSBbXTtcclxuICAgIGFsbFJldmlld2VyczogYW55W10gPSBbXTtcclxuICAgIC8vIGF2YWlsYWJsZVJldmlld2VyczogYW55W107XHJcbiAgICAvLyByZXZpZXdlckRldGFpbHM7XHJcbiAgICByZXZpZXdlclN0YXR1c0RhdGEgPSBbXTtcclxuXHJcbiAgICBkZWxpdmVyYWJsZVJldmlld2VycyA9IFtdO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3Jldmlld2VySW5wdXQnLCB7IHN0YXRpYzogZmFsc2UgfSkgcmV2aWV3ZXJJbnB1dDogRWxlbWVudFJlZjxIVE1MSW5wdXRFbGVtZW50PjtcclxuICAgIEBWaWV3Q2hpbGQoJ2F1dG8nLCB7IHN0YXRpYzogZmFsc2UgfSkgbWF0QXV0b2NvbXBsZXRlOiBNYXRBdXRvY29tcGxldGU7XHJcblxyXG4gICAgUFJPSkVDVF9ERVRBSUxTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Qcm9qZWN0L29wZXJhdGlvbnMnO1xyXG4gICAgVEFTS19ERVRBSUxTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9UYXNrL29wZXJhdGlvbnMnO1xyXG4gICAgREVMSVZFUkFCTEVfREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRGVsaXZlcmFibGUvb3BlcmF0aW9ucyc7XHJcbiAgICBERUxJVkVSQUJMRV9UWVBFX0RFVEFJTFNfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0RlbGl2ZXJhYmxlX1R5cGUvb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgVVNFUl9URUFNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLXRlY2guY29tL21wbS90ZWFtcy9icG0vMS4wJztcclxuICAgIFJPTEVfVEVBTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTVRlYW1zL1RlYW1fUm9sZV9NYXBwaW5nL29wZXJhdGlvbnMnO1xyXG4gICAgQVNTSUdOTUVOVF9UWVBFX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Bc3NpZ25tZW50X1R5cGUvb3BlcmF0aW9ucyc7XHJcbiAgICBERUxJVkVSQUJMRV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcblxyXG4gICAgR0VUX1BST0pFQ1RfREVUQUlMU19XU19NRVRIT0RfTkFNRSA9ICdHZXRQcm9qZWN0QnlJRCc7XHJcbiAgICBHRVRfVEFTS19ERVRBSUxTX1dTX01FVEhPRF9OQU1FID0gJ1JlYWRUYXNrJztcclxuICAgIEdFVF9ERUxJVkVSQUJMRV9ERVRBSUxTX1dTX01FVEhPRF9OQU1FID0gJ1JlYWREZWxpdmVyYWJsZSc7XHJcbiAgICBHRVRfVVNFUlNfQllfVEVBTV9XU19NRVRIT0RfTkFNRSA9ICdHZXRUZWFtVXNlckRldGFpbHMnO1xyXG4gICAgR0VUX0RFTElWRVJBQkxFU19CWV9QUk9KRUNUX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbERlbGl2ZXJhYmxlc0J5UHJvamVjdCc7XHJcbiAgICBHRVRfQVNTSUdOTUVOVF9UWVBFX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbEFzc2lnbm1lbnRUeXBlJztcclxuICAgIENSRUFURV9ERUxJVkVSQUJMRV9XU19NRVRIT0RfTkFNRSA9ICdDcmVhdGVEZWxpdmVyYWJsZSc7XHJcbiAgICBVUERBVEVfREVMSVZFUkFCTEVfV1NfTUVUSE9EX05BTUUgPSAnRWRpdERlbGl2ZXJhYmxlJztcclxuICAgIEdFVF9BTExfREVMSVZFUkFCTEVTX0JZX05BTUVfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsRGVsaXZlcmFibGVzQnlEZWxpdmVyYWJsZU5hbWUnO1xyXG4gICAgR0VUX1VTRVJTX0JZX1RFQU1fUk9MRV9XU19NRVRIT0RfTkFNRSA9ICdHZXRUZWFtUm9sZVVzZXJzICc7XHJcbiAgICBHRVRfREVMSVZFUkFCTEVfVFlQRV9CWV9OQU1FX1dTX01FVEhPRF9OQU1FID0gJ0dldERlbGl2ZXJhYmxlVHlwZUJ5TmFtZSc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFkYXB0ZXI6IERhdGVBZGFwdGVyPGFueT4sXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGVsaXZlcmFibGVTZXJ2aWNlOiBEZWxpdmVyYWJsZVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0VXRpbFNlcnZpY2U6IFByb2plY3RVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGZpbHRlclJldmlld2Vycyh2YWx1ZTogYW55KTogYW55W10ge1xyXG4gICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnICYmIHZhbHVlLnRyaW0oKSAhPT0gJycpIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsdGVyVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYWxsUmV2aWV3ZXJzLmZpbHRlcihyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID09PSAwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2VsZWN0ZWQoZXZlbnQ6IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCByZXZpZXdlckluZGV4ID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kSW5kZXgoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSBldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IHJldmlld2VyID0gdGhpcy5hbGxSZXZpZXdlcnMuZmluZChyZXZpZXdlckRhdGEgPT4gcmV2aWV3ZXJEYXRhLnZhbHVlID09PSBldmVudC5vcHRpb24udmFsdWUpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0udmFsdWUuZGVsaXZlcmFibGVBcHByb3ZlciAmJlxyXG4gICAgICAgICAgICBldmVudC5vcHRpb24udmFsdWUgPT09IHRoaXMuZGVsaXZlcmFibGVGb3JtLnZhbHVlLmRlbGl2ZXJhYmxlQXBwcm92ZXIubmFtZSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IocmV2aWV3ZXIuZGlzcGxheU5hbWUgKyAnIGlzIGFscmVhZHkgdGhlIEFwcHJvdmVyJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChyZXZpZXdlckluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHJldmlld2VyLmRpc3BsYXlOYW1lICsgJyBpcyBhbHJlYWR5IHNlbGVjdGVkJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlZFJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLm5ld1Jldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCByZW1vdmVSZXZpZXdlckluZGV4ID0gdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZW1vdmVkUmV2aWV3ZXIgPT4gcmVtb3ZlZFJldmlld2VyLnZhbHVlID09PSBldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgICAgICAgICBpZiAocmVtb3ZlUmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZWRSZXZpZXdlcnMuc3BsaWNlKHJlbW92ZVJldmlld2VySW5kZXgsIDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXIudmFsdWUsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogZmFsc2VcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJJbnB1dC5uYXRpdmVFbGVtZW50LnZhbHVlID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLnNldFZhbHVlKG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRSZXZpZXdlcihldmVudDogTWF0Q2hpcElucHV0RXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMubWF0QXV0b2NvbXBsZXRlLmlzT3Blbikge1xyXG4gICAgICAgICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG5cclxuICAgICAgICAgICAgaWYgKGlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICBpbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25SZW1vdmVSZXZpZXdlcihyZXZpZXdlclZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZEluZGV4KHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgY29uc3QgbmV3UmV2aWV3ZXJJbmRleCA9IHRoaXMubmV3UmV2aWV3ZXJzLmZpbmRJbmRleChuZXdSZXZpZXdlciA9PiBuZXdSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgY29uc3QgcmV2aWV3ZXJJbmRleCA9IHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLmZpbmRJbmRleChyZXZpZXdlckRhdGEgPT4gcmV2aWV3ZXJEYXRhLnZhbHVlID09PSByZXZpZXdlclZhbHVlKTtcclxuICAgICAgICBjb25zdCByZXZpZXdlciA9IHRoaXMuYWxsUmV2aWV3ZXJzLmZpbmQocmV2aWV3ZXJEYXRhID0+IHJldmlld2VyRGF0YS52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0ubWFya0FzRGlydHkoKTtcclxuICAgICAgICAgICAgdGhpcy5zYXZlZFJldmlld2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAocmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLnNwbGljZShyZXZpZXdlckluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKG5ld1Jldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm5ld1Jldmlld2Vycy5zcGxpY2UobmV3UmV2aWV3ZXJJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZTa2lwUmV2aWV3VmFsdWUoaXNOZWVkUmV2aWV3VmFsdWUpIHtcclxuICAgICAgICBpZiAoIWlzTmVlZFJldmlld1ZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmFwcHJvdmVyQWxsb2NhdGlvbi5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmFwcHJvdmVyQWxsb2NhdGlvbi5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmFwcHJvdmVyQWxsb2NhdGlvbi51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mQXBwcm92ZXJBbGxvY2F0aW9uVHlwZSh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmFwcHJvdmVyQWxsb2NhdGlvbiwgdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyLCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZPd25lckFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWUsIG93bmVyVmFsdWUsIGNoYW5nZURlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICBjb25zdCByb2xlcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHJvbGVJZHMgPSBbXTtcclxuICAgICAgICB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICByb2xlcy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICByb2xlSWRzLnB1c2goZWxlbWVudC5uYW1lKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoYWxsb2NhdGlvblR5cGVWYWx1ZSA9PT0gRGVsaXZlcmFibGVDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS5zZXRWYWxpZGF0b3JzKFtWYWxpZGF0b3JzLnJlcXVpcmVkXSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zID0gcm9sZXM7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlT3duZXJSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCdPV05FUicpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIucGF0Y2hWYWx1ZShvd25lclZhbHVlID8gb3duZXJWYWx1ZSA6ICcnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGFsbG9jYXRpb25UeXBlVmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9VU0VSKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLmNsZWFyVmFsaWRhdG9ycygpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnT1dORVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnBhdGNoVmFsdWUob3duZXJWYWx1ZSA/IG93bmVyVmFsdWUgOiAnJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lciAmJiAhdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMub3duZXJBbGxvY2F0aW9uLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZBcHByb3ZlckFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWUsIG93bmVyVmFsdWUsIGNoYW5nZURlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICBjb25zdCByb2xlcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHJvbGVJZHMgPSBbXTtcclxuICAgICAgICB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICByb2xlcy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICByb2xlSWRzLnB1c2goZWxlbWVudC5uYW1lKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoYWxsb2NhdGlvblR5cGVWYWx1ZSA9PT0gRGVsaXZlcmFibGVDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmZpbHRlck9wdGlvbnMgPSByb2xlcztcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ0FQUFJPVkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZUFwcHJvdmVyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGNoYW5nZURlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKGFsbG9jYXRpb25UeXBlVmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9VU0VSKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnQVBQUk9WRVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2hhbmdlRGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlciAmJiAhdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuYXBwcm92ZXJBbGxvY2F0aW9uLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZPd25lclJvbGUob3duZXJSb2xlKSB7XHJcbiAgICAgICAgaWYgKG93bmVyUm9sZSAmJiBvd25lclJvbGUubmFtZSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lciAmJiAhdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUuZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnT1dORVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlb2ZBcHByb3ZlclJvbGUoYXBwcm92ZXJSb2xlKSB7XHJcbiAgICAgICAgaWYgKGFwcHJvdmVyUm9sZSAmJiBhcHByb3ZlclJvbGUubmFtZSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlciAmJiAhdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUuZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnQVBQUk9WRVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qIG9uRGVsaXZlcmFibGVTZWxlY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgb3duZXJSb2xlcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IG93bmVyUm9sZUlkcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGFwcHJvdmVyUm9sZXMgPSBbXTtcclxuICAgICAgICBjb25zdCBhcHByb3ZlclJvbGVJZHMgPSBbXTtcclxuICAgICAgICB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZWxlbWVudC52YWx1ZS5zZWFyY2goUm9sZUNvbnN0YW50cy5QUk9KRUNUX01BTkFHRVIpICE9PSAtMSB8fCBlbGVtZW50LnZhbHVlLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfTUVNQkVSKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgIG93bmVyUm9sZXMucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgICAgICAgIG93bmVyUm9sZUlkcy5wdXNoKGVsZW1lbnQubmFtZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lclJvbGVDb25maWcuZmlsdGVyT3B0aW9ucyA9IG93bmVyUm9sZXM7XHJcbiAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnT1dORVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudGVhbUFwcHJvdmVyUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlbGVtZW50LnZhbHVlLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfTUFOQUdFUikgIT09IC0xIHx8IGVsZW1lbnQudmFsdWUuc2VhcmNoKFJvbGVDb25zdGFudHMuUFJPSkVDVF9BUFBST1ZFUikgIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVJZHMucHVzaChlbGVtZW50Lm5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmZpbHRlck9wdGlvbnMgPSBhcHByb3ZlclJvbGVzO1xyXG4gICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ0FQUFJPVkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICB1cGRhdGVEZWxpdmVyYWJsZShzYXZlT3B0aW9uKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3ZlciAmJiB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlQXBwcm92ZXIubmFtZSkge1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kKHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlQXBwcm92ZXIubmFtZSk7XHJcbiAgICAgICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTZWxlY3RlZCByZXZpZXdlciBpcyBhbHJlYWR5IHRoZSBhcHByb3ZlcicsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5wcmlzdGluZSkge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdLaW5kbHkgbWFrZSBjaGFuZ2VzIHRvIHVwZGF0ZSB0aGUgZGVsaXZlcmFibGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5XQVJOIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbmFtZVNwbGl0ID0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZU5hbWUuc3BsaXQoJ18nKTtcclxuICAgICAgICBpZiAobmFtZVNwbGl0WzBdLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdOYW1lIHNob3VsZCBub3Qgc3RhcnQgd2l0aCBcIl9cIicsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLldBUk4gfTtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLnN0YXR1cyA9PT0gJ1ZBTElEJykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVOYW1lLnZhbHVlLnRyaW0oKSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgZGVsaXZlcmFibGUgbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZURlbGl2ZXJhYmxlRGV0YWlscyhzYXZlT3B0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdLaW5kbHkgZmlsbCBhbGwgbWFuZGF0b3J5IGZpZWxkcycsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2lnbmVtZW50VHlwZUlkKHR5cGVOYW1lOiBzdHJpbmcpIHtcclxuICAgICAgICBsZXQgYXNzaWduZW1lbnRJZCA9IG51bGw7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJBc3NpZ25tZW50VHlwZS5tYXAodHlwZSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlLk5BTUUgPT09IHR5cGVOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICBhc3NpZ25lbWVudElkID0gdHlwZVsnQXNzaWdubWVudF9UeXBlLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gYXNzaWduZW1lbnRJZDtcclxuICAgIH1cclxuICAgIGdldFVwZGF0ZWREZWxpdmVyYWJsZURldGFpbHMoKTogRGVsaXZlcmFibGVVcGRhdGVPYmplY3Qge1xyXG4gICAgICAgIGxldCBkZWxpdmVyYWJsZU9iamVjdDogRGVsaXZlcmFibGVVcGRhdGVPYmplY3Q7XHJcbiAgICAgICAgY29uc3QgZGVsaXZlcmFibGVGb3JtVmFsdWVzID0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKTtcclxuICAgICAgICBkZWxpdmVyYWJsZU9iamVjdCA9IHtcclxuICAgICAgICAgICAgRGVsaXZlcmFibGVJZDoge1xyXG4gICAgICAgICAgICAgICAgSWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc05ld0RlbGl2ZXJhYmxlID8gbnVsbCA6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBEZWxpdmVyYWJsZU5hbWU6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kZWxpdmVyYWJsZU5hbWUudHJpbSgpID8gZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlbGl2ZXJhYmxlTmFtZS50cmltKCkucmVwbGFjZSgvXFxzKy9nLCAnICcpIDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlbGl2ZXJhYmxlTmFtZS50cmltKCksXHJcbiAgICAgICAgICAgIERlc2NyaXB0aW9uOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICAgIFN0YXJ0RGF0ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyAnJyA6ICh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5TVEFSVF9EQVRFKSA/ICcnIDpcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suU1RBUlRfREFURSksXHJcbiAgICAgICAgICAgIEVuZERhdGU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc1RlbXBsYXRlID8gJycgOiAodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kdWVEYXRlKSA/ICcnIDpcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kdWVEYXRlKSxcclxuICAgICAgICAgICAgRHVlRGF0ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyAnJyA6ICh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoZGVsaXZlcmFibGVGb3JtVmFsdWVzLmR1ZURhdGUpID8gJycgOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZHVlRGF0ZSksXHJcbiAgICAgICAgICAgIElzQWN0aXZlOiB0cnVlLFxyXG4gICAgICAgICAgICBJc0RlbGV0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBEZWxpdmVyYWJsZVR5cGU6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5uZWVkUmV2aWV3ID8gJ1VQTE9BRF9SRVZJRVcnIDogJ1VQTE9BRCcsXHJcbiAgICAgICAgICAgIE93bmVyQXNzaWdubWVudFR5cGU6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5vd25lckFsbG9jYXRpb24sXHJcbiAgICAgICAgICAgIEFwcHJvdmVyQXNzaWdubWVudFR5cGU6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5hcHByb3ZlckFsbG9jYXRpb24sXHJcbiAgICAgICAgICAgIEV4cGVjdGVkRHVyYXRpb246IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5leHBlY3RlZER1cmF0aW9uLFxyXG4gICAgICAgICAgICBSUE9TdGF0dXM6IHtcclxuICAgICAgICAgICAgICAgIE1QTVN0YXR1c0lEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5zdGF0dXNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgUlBPUHJpb3JpdHk6IHtcclxuICAgICAgICAgICAgICAgIE1QTVByaW9yaXR5SUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLnByaW9yaXR5XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFJQT1Rhc2s6IHtcclxuICAgICAgICAgICAgICAgIFRhc2tJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrWydUYXNrLWlkJ10uSWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgUlBPUHJvamVjdDoge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3RbJ1Byb2plY3QtaWQnXS5JZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBSUE9Pd25lclJvbGVJRDoge1xyXG4gICAgICAgICAgICAgICAgTVBNVGVhbVJvbGVNYXBwaW5nSUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlbGl2ZXJhYmxlT3duZXJSb2xlPy5uYW1lXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFJQT0FwcHJvdmVyUm9sZUlEOiB7XHJcbiAgICAgICAgICAgICAgICBNUE1UZWFtUm9sZU1hcHBpbmdJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGU/Lm5hbWVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgUlBPRGVsaXZlcmFibGVPd25lcklEOiB7XHJcbiAgICAgICAgICAgICAgICBJZGVudGl0eUlEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklEOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZGVsaXZlcmFibGVPd25lcj8ubmFtZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBSUE9EZWxpdmVyYWJsZUFwcHJvdmVySUQ6IHtcclxuICAgICAgICAgICAgICAgIElkZW50aXR5SUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VySUQ6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kZWxpdmVyYWJsZUFwcHJvdmVyPy5uYW1lXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBkZWxpdmVyYWJsZU9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtUmV2aWV3ZXJPYmooKSB7XHJcbiAgICAgICAgY29uc3QgYWRkZWRSZXZpZXdlcnMgPSBbXTtcclxuICAgICAgICBjb25zdCByZW1vdmVkUmV2aWV3ZXJzID0gW107XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm5ld1Jldmlld2VycyAmJiB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzKSB7XHJcbiAgICAgICAgICAgIHRoaXMubmV3UmV2aWV3ZXJzLmZvckVhY2gobmV3UmV2aWV3ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYWRkZWRSZXZpZXdlckluZGV4ID0gYWRkZWRSZXZpZXdlcnMuZmluZEluZGV4KHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSBuZXdSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFkZGVkUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWRkZWRSZXZpZXdlcnMucHVzaChuZXdSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmZpbmRJbmRleChkZWxpdmVyYWJsZVJldmlld2VyID0+IGRlbGl2ZXJhYmxlUmV2aWV3ZXIudmFsdWUgPT09IG5ld1Jldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhZGRlZFJldmlld2VySW5kZXggPSBhZGRlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IG5ld1Jldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkZGVkUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZGVkUmV2aWV3ZXJzLnB1c2gobmV3UmV2aWV3ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5uZXdyZXZpZXdlcnNPYmogPSBhZGRlZFJldmlld2Vycy5tYXAoKGFkZGVkUmV2aWV3ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVJZDogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICAgICAgICAgIFJldmlld2VyOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXRlbUlkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcklkOiBhZGRlZFJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdBREQnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBUYXNrSWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgSXNQTUFzc2lnbmVkUmV2aWV3ZXI6IHRoaXMuaXNQTVxyXG5cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5yZW1vdmVkUmV2aWV3ZXJzICYmIHRoaXMuc2F2ZWRSZXZpZXdlcnMpIHtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLmZvckVhY2gocmVtb3ZlZFJldmlld2VyID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNhdmVkUmV2aWV3ZXJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA9IHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMuZmluZEluZGV4KGRlbGl2ZXJhYmxlUmV2aWV3ZXIgPT4gZGVsaXZlcmFibGVSZXZpZXdlci52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmV2aWV3ZXJJbmRleCA9IHJlbW92ZWRSZXZpZXdlcnMuZmluZEluZGV4KHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZWRSZXZpZXdlcnMucHVzaChyZW1vdmVkUmV2aWV3ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzYXZlZFJldmlld2VySW5kZXggPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChzYXZlZFJldmlld2VyID0+IHNhdmVkUmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNhdmVkUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVtb3ZlZFJldmlld2VySW5kZXggPSByZW1vdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID0gdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5maW5kSW5kZXgoZGF0YSA9PiBkYXRhLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVtb3ZlZFJldmlld2VySW5kZXggPT09IC0xICYmIGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1vdmVkUmV2aWV3ZXJzLnB1c2gocmVtb3ZlZFJldmlld2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlZHJldmlld2Vyc09iaiA9IHJlbW92ZWRSZXZpZXdlcnMubWFwKChyZXZpZXdlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICBEZWxpdmVyYWJsZUlkOiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgUmV2aWV3ZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJdGVtSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdSRU1PVkUnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBUYXNrSWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgSXNQTUFzc2lnbmVkUmV2aWV3ZXI6IHRoaXMuaXNQTVxyXG5cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZXZpZXdlcnNPYmogPSB0aGlzLm5ld3Jldmlld2Vyc09iaiA/IHRoaXMubmV3cmV2aWV3ZXJzT2JqLmNvbmNhdCh0aGlzLnJlbW92ZWRyZXZpZXdlcnNPYmopIDogdGhpcy5yZW1vdmVkcmV2aWV3ZXJzT2JqLmNvbmNhdCh0aGlzLm5ld3Jldmlld2Vyc09iaik7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlRGVsaXZlcmFibGVEZXRhaWxzKHNhdmVPcHRpb24pIHtcclxuICAgICAgICBjb25zdCBkZWxpdmVyYWJsZU9iamVjdDogRGVsaXZlcmFibGVVcGRhdGVPYmplY3QgPSB0aGlzLmdldFVwZGF0ZWREZWxpdmVyYWJsZURldGFpbHMoKTtcclxuICAgICAgICB0aGlzLmZvcm1SZXZpZXdlck9iaigpO1xyXG4gICAgICAgIHRoaXMuc2F2ZURlbGl2ZXJhYmxlKGRlbGl2ZXJhYmxlT2JqZWN0LCBzYXZlT3B0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBzYXZlRGVsaXZlcmFibGUoZGVsaXZlcmFibGVPYmplY3QsIHNhdmVPcHRpb24pIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZU9iamVjdCA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmNvbXBhcmVUd29Qcm9qZWN0RGV0YWlscyhkZWxpdmVyYWJsZU9iamVjdCwgdGhpcy5vbGRGb3JtVmFsdWUpO1xyXG4gICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMoZGVsaXZlcmFibGVPYmplY3QpLmxlbmd0aCA+IDEgfHwgKEpTT04uc3RyaW5naWZ5KHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMpICE9PSBKU09OLnN0cmluZ2lmeSh0aGlzLnNhdmVkUmV2aWV3ZXJzKSkpIHtcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT2JqZWN0LkJ1bGtPcGVyYXRpb24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RPYmogPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVPYmplY3Q6IGRlbGl2ZXJhYmxlT2JqZWN0LFxyXG4gICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlUmV2aWV3ZXI6IHRoaXMucmV2aWV3ZXJzT2JqXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuREVMSVZFUkFCTEVfTUVUSE9EX05TLCB0aGlzLlVQREFURV9ERUxJVkVSQUJMRV9XU19NRVRIT0RfTkFNRSwgcmVxdWVzdE9iailcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZVsnRGVsaXZlcmFibGUtaWQnXSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICcgRGVsaXZlcmFibGUgdXBkYXRlZCBzdWNjZXNzZnVsbHknLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlQ2FsbEJhY2tIYW5kbGVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuaXNUZW1wbGF0ZSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYXZlT3B0aW9uOiBzYXZlT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnNTAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JDb2RlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgZGVsaXZlcmFibGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBEZWxpdmVyYWJsZU9iamVjdDogZGVsaXZlcmFibGVPYmplY3RcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxJVkVSQUJMRV9NRVRIT0RfTlMsIHRoaXMuQ1JFQVRFX0RFTElWRVJBQkxFX1dTX01FVEhPRF9OQU1FLCByZXF1ZXN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10gJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZVsnRGVsaXZlcmFibGUtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdEZWxpdmVyYWJsZSBzYXZlZCBzdWNjZXNzZnVsbHknLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlQ2FsbEJhY2tIYW5kbGVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNhdmVPcHRpb246IHNhdmVPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlQ2FsbEJhY2tIYW5kbGVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYXZlT3B0aW9uOiBzYXZlT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICc1MDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yQ29kZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIGNyZWF0aW5nIGRlbGl2ZXJhYmxlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgY3JlYXRpbmcgZGVsaXZlcmFibGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUlxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKiBmb3JtVXNlclZhbHVlKHVzZXIpIHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICB1c2VySWQ6IHVzZXJcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQZXJzb25CeUlkZW50aXR5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlciA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSByZXNwb25zZS51c2VyQ04pO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRVc2VyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3duZXIgPSAgc2VsZWN0ZWRVc2VyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgZm9ybVJvbGVWYWx1ZShyb2xlLCBpc1Jldmlld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgaWYgKHJvbGUpIHtcclxuICAgICAgICAgICAgaWYgKGlzUmV2aWV3RGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZSA9IHRoaXMudGVhbUFwcHJvdmVyUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IHJvbGUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChzZWxlY3RlZFJvbGUpID8gc2VsZWN0ZWRSb2xlIDogJyc7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFJvbGUgPSB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSByb2xlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoc2VsZWN0ZWRSb2xlKSA/IHNlbGVjdGVkUm9sZSA6ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JtT3duZXJWYWx1ZSh1c2VySWQpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFVzZXIgPSB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IHJlc3BvbnNlLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE93bmVyID0gc2VsZWN0ZWRVc2VyO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtQXBwcm92ZXJWYWx1ZSh1c2VySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh1c2VySWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklkOiB1c2VySWRcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlciA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQubmFtZSA9PT0gcmVzcG9uc2UudXNlckNOKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQXBwcm92ZXIgPSBzZWxlY3RlZFVzZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1Pd25lclJvbGVWYWx1ZShkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIGlmIChkZWxpdmVyYWJsZS5SX1BPX09XTkVSX1JPTEVfSUQgJiYgZGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXSAmJiB0eXBlb2YgZGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCAhPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb2xlID0gdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQubmFtZSA9PT0gZGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCk7XHJcbiAgICAgICAgICAgIHJldHVybiAoc2VsZWN0ZWRSb2xlKSA/IHNlbGVjdGVkUm9sZSA6ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUFwcHJvdmVyUm9sZVZhbHVlKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgaWYgKGRlbGl2ZXJhYmxlICYmIGRlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfUk9MRV9JRCAmJiB0eXBlb2YgZGVsaXZlcmFibGUuUl9QT19BUFBST1ZFUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCAhPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb2xlID0gdGhpcy50ZWFtQXBwcm92ZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQubmFtZSA9PT0gZGVsaXZlcmFibGUuUl9QT19BUFBST1ZFUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCk7XHJcbiAgICAgICAgICAgIHJldHVybiAoc2VsZWN0ZWRSb2xlKSA/IHNlbGVjdGVkUm9sZSA6ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybU93bmVyQWxsb2NhdGlvblZhbHVlKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgaWYgKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoZGVsaXZlcmFibGUuT1dORVJfQVNTSUdOTUVOVF9UWVBFKSA/IGRlbGl2ZXJhYmxlLk9XTkVSX0FTU0lHTk1FTlRfVFlQRSA6ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUFwcHJvdmVyQWxsb2NhdGlvblZhbHVlKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgaWYgKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoZGVsaXZlcmFibGUuQVBQUk9WRVJfQVNTSUdOTUVOVF9UWVBFKSA/IGRlbGl2ZXJhYmxlLkFQUFJPVkVSX0FTU0lHTk1FTlRfVFlQRSA6ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGlzZURlbGl2ZXJhYmxlRm9ybSgpIHtcclxuICAgICAgICBjb25zdCBpc1RlbXBsYXRlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmlzVGVtcGxhdGU7XHJcbiAgICAgICAgY29uc3QgaXNOZXdEZWxpdmVyYWJsZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc05ld0RlbGl2ZXJhYmxlO1xyXG4gICAgICAgIGNvbnN0IG51bWVyaWNlVmxhbHVlID0gdGhpcy5nZXRNYXhNaW5WYWx1ZUZvck51bWJlcigwKTtcclxuICAgICAgICBsZXQgZGlzYWJsZUZpZWxkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWREZWxpdmVyYWJsZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlO1xyXG4gICAgICAgIHRoaXMuc2hvd1Jldmlld2VyID0gKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZyAmJiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZSAmJlxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFICE9PSBEZWxpdmVyYWJsZVR5cGVzLlVQTE9BRCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLm1pbkRhdGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlNUQVJUX0RBVEU7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLm1heERhdGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkRVRV9EQVRFO1xyXG4gICAgICAgIGNvbnN0IHBhdHRlcm4gPSAnXlthLXpBLVowLTkgXygpLi1dKiQnO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnRpdGxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubW9kYWxMYWJlbCA9IHRoaXMuZGVsaXZlcmFibGVDb25maWcudGl0bGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICBpZiAoaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU5hbWU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdCAmJiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0WzBdWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVTdGF0dXNMaXN0ICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXSA/IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnLCBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1ZURhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkRVRV9EQVRFLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG5lZWRSZXZpZXc6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBmYWxzZSwgZGlzYWJsZWQ6ICF0aGlzLmVuYWJsZU5lZWRSZXZpZXcgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBvd25lckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkFTU0lHTk1FTlRfVFlQRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fT1dORVJfSUQgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE93bmVyIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkFTU0lHTk1FTlRfVFlQRSA9PT0gRGVsaXZlcmFibGVDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtUm9sZVZhbHVlKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19URUFNX1JPTEVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkLCBmYWxzZSkgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBleHBlY3RlZER1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRpc2FibGVGaWVsZCA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLkZJTkFMIHx8ICF0aGlzLmlzVXBsb2FkZWQgfHxcclxuICAgICAgICAgICAgICAgICAgICAoc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlJFVklFVykgPyB0cnVlIDogZGlzYWJsZUZpZWxkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU5hbWU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZERlbGl2ZXJhYmxlLk5BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lcjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWRPd25lcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyUm9sZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZm9ybU93bmVyUm9sZVZhbHVlKHNlbGVjdGVkRGVsaXZlcmFibGUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkQXBwcm92ZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHwgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMuU1RBVFVTX0xFVkVMID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1BcHByb3ZlclJvbGVWYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fU1RBVFVTICYmIHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXSA/IHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaW9yaXR5OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1BSSU9SSVRZICYmIHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10gPyBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1ZURhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZERlbGl2ZXJhYmxlLkRVRV9EQVRFLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFU0NSSVBUSU9OID09PSAnc3RyaW5nJyA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFU0NSSVBUSU9OIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBuZWVkUmV2aWV3OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHNlbGVjdGVkRGVsaXZlcmFibGUuQVBQUk9WRVJfQVNTSUdOTUVOVF9UWVBFKSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICAvLyAhZGlzYWJsZUZpZWxkID8gc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlJFVklFVyA/IGRpc2FibGVGaWVsZCA6IHRydWUgOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICBhcHByb3ZlckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1BcHByb3ZlckFsbG9jYXRpb25WYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBvd25lckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Pd25lckFsbG9jYXRpb25WYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZVJldmlld2VyczogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuc2F2ZWRSZXZpZXdlcnMsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShzZWxlY3RlZERlbGl2ZXJhYmxlLkVYUEVDVEVEX0RVUkFUSU9OKSA/ICcnIDogc2VsZWN0ZWREZWxpdmVyYWJsZS5FWFBFQ1RFRF9EVVJBVElPTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMubWF4KG51bWVyaWNlVmxhbHVlKSwgVmFsaWRhdG9ycy5taW4oLShudW1lcmljZVZsYWx1ZSkpXSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMubmVlZFJldmlldy5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLm9uRGVsaXZlcmFibGVTZWxlY3Rpb24oKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmIChpc05ld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlTmFtZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaW9yaXR5OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0ICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdFswXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0WzBdWydNUE1fUHJpb3JpdHktaWQnXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3RbMF1bJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3QgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdFswXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddID8gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddLklkIDogJycsIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBuZWVkUmV2aWV3OiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogZmFsc2UsIGRpc2FibGVkOiAhdGhpcy5lbmFibGVOZWVkUmV2aWV3IH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwcHJvdmVyQWxsb2NhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BU1NJR05NRU5UX1RZUEUsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPd25lciA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJSb2xlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BU1NJR05NRU5UX1RZUEUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybVJvbGVWYWx1ZSh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCwgZmFsc2UpIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyUm9sZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5zYXZlZFJldmlld2VycywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBleHBlY3RlZER1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRpc2FibGVGaWVsZCA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURSB8fFxyXG4gICAgICAgICAgICAgICAgICAgIChzZWxlY3RlZERlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1RZUEUgPT09IERlbGl2ZXJhYmxlVHlwZXMuUkVWSUVXKSA/IHRydWUgOiBkaXNhYmxlRmllbGQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlTmFtZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkRGVsaXZlcmFibGUuTkFNRSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5zZWxlY3RlZE93bmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Pd25lclJvbGVWYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkQXBwcm92ZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyUm9sZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZm9ybUFwcHJvdmVyUm9sZVZhbHVlKHNlbGVjdGVkRGVsaXZlcmFibGUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVUyAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10gPyBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19QUklPUklUWSAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddID8gc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1BSSU9SSVRZWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFU0NSSVBUSU9OID09PSAnc3RyaW5nJyA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFU0NSSVBUSU9OIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBuZWVkUmV2aWV3OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHNlbGVjdGVkRGVsaXZlcmFibGUuQVBQUk9WRVJfQVNTSUdOTUVOVF9UWVBFKSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICAvLyAhZGlzYWJsZUZpZWxkID8gc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlJFVklFVyA/IGRpc2FibGVGaWVsZCA6IHRydWUgOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICBhcHByb3ZlckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1BcHByb3ZlckFsbG9jYXRpb25WYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyQWxsb2NhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZm9ybU93bmVyQWxsb2NhdGlvblZhbHVlKHNlbGVjdGVkRGVsaXZlcmFibGUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNhdmVkUmV2aWV3ZXJzLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGV4cGVjdGVkRHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoc2VsZWN0ZWREZWxpdmVyYWJsZS5FWFBFQ1RFRF9EVVJBVElPTikgPyAnJyA6IHNlbGVjdGVkRGVsaXZlcmFibGUuRVhQRUNURURfRFVSQVRJT04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5maWx0ZXJlZFJldmlld2VycyA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldCgnZGVsaXZlcmFibGVSZXZpZXdlcnMnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgICAgICAgbWFwKChyZXZpZXdlcjogc3RyaW5nIHwgbnVsbCkgPT4gcmV2aWV3ZXIgPyB0aGlzLmZpbHRlclJldmlld2VycyhyZXZpZXdlcikgOiB0aGlzLmFsbFJldmlld2Vycy5zbGljZSgpKSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lcjtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5mb3JtQ29udHJvbCA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXI7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJSb2xlQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGU7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmZvcm1Db250cm9sID0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5vd25lckFsbG9jYXRpb24udmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyICYmIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnZhbHVlICE9PSAnJyAmJiBpc05ld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCdPV05FUicpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZm9ybUNvbnRyb2wudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldCgnZGVsaXZlcmFibGVPd25lcicpLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5hcHByb3ZlckFsbG9jYXRpb24udmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyICYmIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnZhbHVlICE9PSAnJyAmJiBpc05ld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCdBUFBST1ZFUicpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZm9ybUNvbnRyb2wudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldCgnZGVsaXZlcmFibGVBcHByb3ZlcicpLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5uZWVkUmV2aWV3LnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoaXNOZWVkUmV2aWV3VmFsdWUgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uQ2hhbmdlb2ZTa2lwUmV2aWV3VmFsdWUoaXNOZWVkUmV2aWV3VmFsdWUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyB0aGlzLm9uQ2hhbmdlb2ZPd25lckFsbG9jYXRpb25UeXBlKHRoaXMuZGVsaXZlcmFibGVGb3JtLnZhbHVlLm93bmVyQWxsb2NhdGlvbiwgdGhpcy5kZWxpdmVyYWJsZUZvcm0udmFsdWUuZGVsaXZlcmFibGVPd25lciwgZmFsc2UpO1xyXG5cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5vd25lckFsbG9jYXRpb24udmFsdWVDaGFuZ2VzLnN1YnNjcmliZShhbGxvY2F0aW9uVHlwZVZhbHVlID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZk93bmVyQWxsb2NhdGlvblR5cGUoYWxsb2NhdGlvblR5cGVWYWx1ZSwgbnVsbCwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIHRoaXMub25DaGFuZ2VvZkFwcHJvdmVyQWxsb2NhdGlvblR5cGUodGhpcy5kZWxpdmVyYWJsZUZvcm0udmFsdWUuYXBwcm92ZXJBbGxvY2F0aW9uLCB0aGlzLmRlbGl2ZXJhYmxlRm9ybS52YWx1ZS5kZWxpdmVyYWJsZUFwcHJvdmVyLCBmYWxzZSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmFwcHJvdmVyQWxsb2NhdGlvbi52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGFsbG9jYXRpb25UeXBlVmFsdWUgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mQXBwcm92ZXJBbGxvY2F0aW9uVHlwZShhbGxvY2F0aW9uVHlwZVZhbHVlLCBudWxsLCB0cnVlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShvd25lclJvbGUgPT4ge1xyXG4gICAgICAgICAgICBpZiAob3duZXJSb2xlICYmIHR5cGVvZiBvd25lclJvbGUgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIShzZWxlY3RlZERlbGl2ZXJhYmxlICYmIHNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSA9PT0gRGVsaXZlcmFibGVUeXBlcy5SRVZJRVcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mT3duZXJSb2xlKG93bmVyUm9sZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZE93bmVyUm9sZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zLmZpbmQocm9sZSA9PiByb2xlLmRpc3BsYXlOYW1lID09PSBvd25lclJvbGUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkT3duZXJSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUuc2V0VmFsdWUoc2VsZWN0ZWRPd25lclJvbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoYXBwcm92ZXJSb2xlID0+IHtcclxuICAgICAgICAgICAgaWYgKGFwcHJvdmVyUm9sZSAmJiB0eXBlb2YgYXBwcm92ZXJSb2xlID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEoc2VsZWN0ZWREZWxpdmVyYWJsZSAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1RZUEUgPT09IERlbGl2ZXJhYmxlVHlwZXMuUkVWSUVXKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZkFwcHJvdmVyUm9sZShhcHByb3ZlclJvbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBcHByb3ZlclJvbGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUuZmlsdGVyT3B0aW9ucy5maW5kKHJvbGUgPT4gcm9sZS5kaXNwbGF5TmFtZSA9PT0gYXBwcm92ZXJSb2xlKTtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZEFwcHJvdmVyUm9sZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnNldFZhbHVlKHNlbGVjdGVkQXBwcm92ZXJSb2xlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lcikge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUob3duZXJWYWx1ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIShvd25lclZhbHVlICYmIHR5cGVvZiBvd25lclZhbHVlID09PSAnb2JqZWN0JykpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZE93bmVyID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucy5maW5kKG93bmVyID0+IG93bmVyLmRpc3BsYXlOYW1lID09PSBvd25lclZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRPd25lcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnNldFZhbHVlKHNlbGVjdGVkT3duZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShhcHByb3ZlclZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghKGFwcHJvdmVyVmFsdWUgJiYgdHlwZW9mIGFwcHJvdmVyVmFsdWUgPT09ICdvYmplY3QnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXBwcm92ZXIgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5maWx0ZXJPcHRpb25zLmZpbmQoYXBwcm92ZXIgPT4gYXBwcm92ZXIuZGlzcGxheU5hbWUgPT09IGFwcHJvdmVyVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZEFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuc2V0VmFsdWUoc2VsZWN0ZWRBcHByb3Zlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMub2xkRm9ybVZhbHVlID0gdGhpcy5nZXRVcGRhdGVkRGVsaXZlcmFibGVEZXRhaWxzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdERldGFpbHMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElEOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcucHJvamVjdElkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfUFJPSkVDVF9ERVRBSUxTX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLlByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvamVjdE9iaiA9IHJlc3BvbnNlLlByb2plY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcm9qZWN0T2JqLlJfUE9fQ0FURUdPUlkgJiYgcHJvamVjdE9iai5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmNhdGVnb3J5SWQgPSBwcm9qZWN0T2JqLlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcm9qZWN0T2JqLlJfUE9fVEVBTSAmJiBwcm9qZWN0T2JqLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy50ZWFtSWQgPSBwcm9qZWN0T2JqLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdCA9IHJlc3BvbnNlLlByb2plY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBwcm9qZWN0IGRldGFpbHMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tEZXRhaWxzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgICdUYXNrLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfVEFTS19ERVRBSUxTX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLlRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGFza09iaiA9IHJlc3BvbnNlLlRhc2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgPSByZXNwb25zZS5UYXNrO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgdGFzayBkZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWxpdmVyYWJsZURldGFpbHMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlU2VydmljZS5yZWFkRGVsaXZlcmFibGUodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlSWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuRGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUgPSByZXNwb25zZS5EZWxpdmVyYWJsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVUyA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlTdGF0dXNJZCh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkKSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBkZWxpdmVyYWJsZSBkZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmlvcml0aWVzKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIC8qIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHByaW9yaXR5UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmlvcml0eVJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShwcmlvcml0eVJlc3BvbnNlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHlSZXNwb25zZSA9IFtwcmlvcml0eVJlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3QgPSBwcmlvcml0eVJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUHJpb3JpdGllcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7ICovXHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHByaW9yaXR5ID0gIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpXHJcbiAgICAgICAgICAgICAgIC8vIC5zdWJzY3JpYmUocHJpb3JpdHlSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByaW9yaXR5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShwcmlvcml0eSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaW9yaXR5ID0gW3ByaW9yaXR5XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3QgPSBwcmlvcml0eTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgLyogfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFN0YXR1cyhzdGF0dXNMZXZlTExpc3Q6IEFycmF5PFN0YXR1c0xldmVsPikge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRTdGF0dXNCeUNhdGVnb3J5TGV2ZWwoTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3RhdHVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXNSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoc3RhdHVzUmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNSZXNwb25zZSA9IFtzdGF0dXNSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdCA9IHN0YXR1c1Jlc3BvbnNlLmZpbHRlcihkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzdGF0dXNMZXZlTExpc3QuaW5kZXhPZihkYXRhLlNUQVRVU19MRVZFTCkgPj0gMCA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgU3RhdHVzZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFJvbGVzKCkge1xyXG4gICAgICAgIGNvbnN0IG93bmVyUm9sZXNQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICBpc0FwcHJvdmVyVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzTWVtYmVyVGFzazogdHJ1ZVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGFwcHJvdmVyUm9sZXNQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICBpc0FwcHJvdmVyVGFzazogdHJ1ZSxcclxuICAgICAgICAgICAgaXNNZW1iZXJUYXNrOiBmYWxzZVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmFwcFNlcnZpY2UuZ2V0VGVhbVJvbGVzKG93bmVyUm9sZXNQYXJhbXMpLCB0aGlzLmFwcFNlcnZpY2UuZ2V0VGVhbVJvbGVzKGFwcHJvdmVyUm9sZXNQYXJhbXMpXSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVswXS5UZWFtcyAmJiByZXNwb25zZVswXS5UZWFtcy5NUE1fVGVhbXMgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbMF0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZyAmJiByZXNwb25zZVsxXS5UZWFtcyAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsxXS5UZWFtcy5NUE1fVGVhbXMgJiYgcmVzcG9uc2VbMV0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZykge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG93bmVyUm9sZUxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGFwcHJvdmVyUm9sZUxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3duZXJSb2xlcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHByb3ZlclJvbGVzID0gW107XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2VbMF0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG93bmVyUm9sZUxpc3QgPSBbcmVzcG9uc2VbMF0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlWzFdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVMaXN0ID0gW3Jlc3BvbnNlWzFdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG93bmVyUm9sZUxpc3QgPSByZXNwb25zZVswXS5UZWFtcy5NUE1fVGVhbXMuTVBNX1RlYW1fUm9sZV9NYXBwaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVMaXN0ID0gcmVzcG9uc2VbMV0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvd25lclJvbGVMaXN0ICYmIG93bmVyUm9sZUxpc3QubGVuZ3RoICYmIG93bmVyUm9sZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3duZXJSb2xlTGlzdC5tYXAocm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3duZXJSb2xlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcm9sZVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByb2xlLlJPTEVfRE4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByb2xlLk5BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcFJvbGVzOiByb2xlLk1QTV9BUFBfUm9sZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXBwcm92ZXJSb2xlTGlzdCAmJiBhcHByb3ZlclJvbGVMaXN0Lmxlbmd0aCAmJiBhcHByb3ZlclJvbGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcHJvdmVyUm9sZUxpc3QubWFwKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcHJvdmVyUm9sZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJvbGVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogcm9sZS5ST0xFX0ROLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcm9sZS5OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBSb2xlczogcm9sZS5NUE1fQVBQX1JvbGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucyA9IG93bmVyUm9sZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbUFwcHJvdmVyUm9sZXNPcHRpb25zID0gYXBwcm92ZXJSb2xlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJSb2xlQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Pd25lclJvbGVzT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUm9sZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJldmlld2VyQnlJZCh1c2VySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHVzZXJJZCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklkOiB1c2VySWRcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlbGl2ZXJhYmxlUmV2aWV3QnlEZWxpdmVyYWJsZUlkKGRlbGl2ZXJhYmxlSWQpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0RGVsaXZlcmFibGVSZXZpZXdCeURlbGl2ZXJhYmxlSUQoZGVsaXZlcmFibGVJZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5EZWxpdmVyYWJsZVJldmlldykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXZpZXdlcnNMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ0RlbGl2ZXJhYmxlUmV2aWV3Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXZpZXdlcnNMaXN0ICYmIHJldmlld2Vyc0xpc3QubGVuZ3RoICYmIHJldmlld2Vyc0xpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV2aWV3ZXJzTGlzdC5mb3JFYWNoKHJldmlld2VyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShyZXZpZXdlci5SX1BPX1JFVklFV0VSX1VTRVJbJ0lkZW50aXR5LWlkJ10uSWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UmV2aWV3ZXJCeUlkKHJldmlld2VyLlJfUE9fUkVWSUVXRVJfVVNFUlsnSWRlbnRpdHktaWQnXS5JZCkuc3Vic2NyaWJlKHJldmlld2VyRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaGFzUmV2aWV3ZXJEYXRhID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kKHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gcmV2aWV3ZXJEYXRhLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNSZXZpZXdlckRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZChyZXZpZXdlci5SX1BPX1JFVklFV19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCkuc3Vic2NyaWJlKHN0YXR1c1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpc1Jldmlld2VyQ29tcGxldGVkID0gc3RhdHVzUmVzcG9uc2UuU1RBVFVTX0xFVkVMID09PSAnRklOQUwnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5maW5kSW5kZXgoZGF0YSA9PiBkYXRhLnZhbHVlID09PSByZXZpZXdlckRhdGEudXNlckNOKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhW3Jldmlld2VySW5kZXhdLmlzQ29tcGxldGVkID0gaXNSZXZpZXdlckNvbXBsZXRlZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlckRhdGEudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyRGF0YS5mdWxsTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiBpc1Jldmlld2VyQ29tcGxldGVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnNhdmVkUmV2aWV3ZXJzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIERlbGl2ZXJhYmxlIFJldmlldyBEZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmV2aWV3ZXJzKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkVGFzay5UQVNLX1RZUEUgPT09IFRhc2tUeXBlcy5BUFBST1ZBTF9UQVNLKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdCA9IHt9O1xyXG4gICAgICAgICAgICAgICAgZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogJ1VTRVInLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzUmV2aWV3OiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRSZXZpZXdlclVzZXJSZXF1ZXN0KVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcnMgJiYgcmVzcG9uc2UudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh1c2VyTGlzdCAmJiB1c2VyTGlzdC5sZW5ndGggJiYgdXNlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gdXNlcnMuZmluZChlID0+IGUudmFsdWUgPT09IHVzZXIuZG4pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWZpZWxkT2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB1c2VyLmRuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbFJldmlld2VycyA9IHVzZXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxSZXZpZXdlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUmV2aWV3ZXIgVXNlcnMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZ2V0VXNlcnNCeVJvbGUoZmllbGQ/LCBjdXJyQWxsb2NhdGlvblR5cGU/KSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBsZXQgZ2V0T3duZXJCeVJvbGVSZXF1ZXN0ID0ge307XHJcbiAgICAgICAgbGV0IGdldEFwcHJvdmVyQnlSb2xlUmVxdWVzdCA9IHt9O1xyXG4gICAgICAgIGxldCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IG51bGw7XHJcbiAgICAgICAgbGV0IHVzZXJUeXBlO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybSkge1xyXG4gICAgICAgICAgICBjb25zdCBhbGxvY2F0aW9uVHlwZSA9IChmaWVsZCA9PT0gJ09XTkVSJykgPyB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLm93bmVyQWxsb2NhdGlvbiA6IHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuYXBwcm92ZXJBbGxvY2F0aW9uO1xyXG4gICAgICAgICAgICB1c2VyVHlwZSA9IGZpZWxkO1xyXG4gICAgICAgICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiBhbGxvY2F0aW9uVHlwZSxcclxuICAgICAgICAgICAgICAgIHJvbGVETjogYWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgP1xyXG4gICAgICAgICAgICAgICAgICAgIChmaWVsZCA9PT0gJ09XTkVSJyA/IHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVPd25lclJvbGUudmFsdWUgOiB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnZhbHVlKSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgdGVhbUlEOiBhbGxvY2F0aW9uVHlwZSA9PT0gQWxsb2NhdGlvblR5cGVzLlVTRVIudG9VcHBlckNhc2UoKSA/IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogKGZpZWxkID09PSAnT1dORVInKSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogKGZpZWxkID09PSAnT1dORVInKSA/IHRydWUgOiBmYWxzZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRGaWVsZCA9IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9QQVJFTlRfSUQpID8gJ09XTkVSJyA6ICdBUFBST1ZFUic7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEZpZWxkID09PSAnT1dORVInKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhbGxvY2F0aW9uVHlwZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLk9XTkVSX0FTU0lHTk1FTlRfVFlQRTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZUlkID0gYWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgPyB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX09XTkVSX1JPTEVfSURbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICB1c2VyVHlwZSA9IHNlbGVjdGVkRmllbGQ7XHJcbiAgICAgICAgICAgICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogYWxsb2NhdGlvblR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUROOiBhbGxvY2F0aW9uVHlwZSA9PT0gQWxsb2NhdGlvblR5cGVzLlJPTEUudG9VcHBlckNhc2UoKSA/IHRoaXMuZm9ybVJvbGVWYWx1ZShzZWxlY3RlZFJvbGVJZCwgZmFsc2UpLnZhbHVlIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3duZXJBbGxvY2F0aW9uVHlwZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLk9XTkVSX0FTU0lHTk1FTlRfVFlQRTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGFwcHJvdmVyQWxsb2NhdGlvblR5cGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5BUFBST1ZFUl9BU1NJR05NRU5UX1RZUEU7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZE93bmVyUm9sZUlkID0gb3duZXJBbGxvY2F0aW9uVHlwZSA9PT0gQWxsb2NhdGlvblR5cGVzLlJPTEUudG9VcHBlckNhc2UoKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBcHByb3ZlclJvbGVJZCA9IGFwcHJvdmVyQWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIGdldE93bmVyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogb3duZXJBbGxvY2F0aW9uVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46IG93bmVyQWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Sb2xlVmFsdWUoc2VsZWN0ZWRPd25lclJvbGVJZCwgZmFsc2UpLnZhbHVlIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgZ2V0QXBwcm92ZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiBhcHByb3ZlckFsbG9jYXRpb25UeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogYXBwcm92ZXJBbGxvY2F0aW9uVHlwZSA9PT0gQWxsb2NhdGlvblR5cGVzLlJPTEUudG9VcHBlckNhc2UoKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybVJvbGVWYWx1ZShzZWxlY3RlZEFwcHJvdmVyUm9sZUlkLCBzZWxlY3RlZEZpZWxkID09PSAnQVBQUk9WRVInKS52YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGdldE93bmVyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiAnVVNFUicsXHJcbiAgICAgICAgICAgICAgICByb2xlRE46ICcnLFxyXG4gICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBnZXRBcHByb3ZlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogJ1VTRVInLFxyXG4gICAgICAgICAgICAgICAgcm9sZUROOiAnJyxcclxuICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogZmFsc2VcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKGdldFVzZXJCeVJvbGVSZXF1ZXN0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldFVzZXJCeVJvbGVSZXF1ZXN0KVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcnMgJiYgcmVzcG9uc2UudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh1c2VyTGlzdCAmJiB1c2VyTGlzdC5sZW5ndGggJiYgdXNlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gdXNlcnMuZmluZChlID0+IGUudmFsdWUgPT09IHVzZXIuZG4pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWZpZWxkT2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmRuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlclR5cGUgPT09ICdBUFBST1ZFUicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zID0gdXNlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnMgPSB1c2VycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZU93bmVyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh1c2VyVHlwZSA9PT0gJ0FQUFJPVkVSJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZU93bmVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBJbmRpdmlkdWFsIFVzZXJzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmb3JrSm9pbihbdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRPd25lckJ5Um9sZVJlcXVlc3QpLCB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldEFwcHJvdmVyQnlSb2xlUmVxdWVzdCldKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2VbMF0udXNlcnMgJiYgcmVzcG9uc2VbMF0udXNlcnMudXNlciAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbMV0udXNlcnMgJiYgcmVzcG9uc2VbMV0udXNlcnMudXNlcikge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVyTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2VbMF0sICd1c2VyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHByb3Zlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlWzFdLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3duZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHByb3ZlcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvd25lckxpc3QgJiYgb3duZXJMaXN0Lmxlbmd0aCAmJiBvd25lckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG93bmVyTGlzdC5mb3JFYWNoKG93bmVyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSBvd25lcnMuZmluZChlID0+IGUudmFsdWUgPT09IG93bmVyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3duZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IG93bmVyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBvd25lci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogb3duZXIubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHByb3Zlckxpc3QgJiYgYXBwcm92ZXJMaXN0Lmxlbmd0aCAmJiBhcHByb3Zlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcHJvdmVyTGlzdC5mb3JFYWNoKGFwcHJvdmVyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSBhcHByb3ZlcnMuZmluZChlID0+IGUudmFsdWUgPT09IGFwcHJvdmVyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGFwcHJvdmVyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBhcHByb3Zlci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogYXBwcm92ZXIubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnMgPSBvd25lcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZU93bmVyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnMgPSBhcHByb3ZlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZUFwcHJvdmVyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgSW5kaXZpZHVhbCBVc2VycycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXNzaWdubWVudFR5cGVzKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuQVNTSUdOTUVOVF9UWVBFX01FVEhPRF9OUywgdGhpcy5HRVRfQVNTSUdOTUVOVF9UWVBFX1dTX01FVEhPRF9OQU1FLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuQXNzaWdubWVudF9UeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuQXNzaWdubWVudF9UeXBlID0gW3Jlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJBc3NpZ25tZW50VHlwZSA9IHJlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJBc3NpZ25tZW50VHlwZSA9IHJlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckFzc2lnbm1lbnRUeXBlID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyQXNzaWdubWVudFR5cGUgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVE9ETzogY2hlY2sgZm9yIGRlZmF1bHRQcmlvcml0eVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBBc3NpZ25tZW50IHR5cGVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmVEZXRhaWxzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVJZCkge1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuZ2V0RGVsaXZlcmFibGVEZXRhaWxzKCksIHRoaXMuZ2V0QWxsUm9sZXMoKV0pLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuZ2V0UHJpb3JpdGllcygpLCB0aGlzLmdldFVzZXJzQnlSb2xlKCksIHRoaXMuZ2V0UmV2aWV3ZXJzKCksIHRoaXMuZ2V0RGVsaXZlcmFibGVSZXZpZXdCeURlbGl2ZXJhYmxlSWQodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkKV0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGVsUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzTGlzdCA9IFtTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFLCBTdGF0dXNMZXZlbHMuSU5JVElBTF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNMaXN0LnBvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHByb3ZlclZhbHVlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19BUFBST1ZFUl9VU0VSX0lEICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfVVNFUl9JRFsnSWRlbnRpdHktaWQnXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfVVNFUl9JRFsnSWRlbnRpdHktaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVyVmFsdWUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZSAmJiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX09XTkVSX1VTRVJfSUQgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19PV05FUl9VU0VSX0lEWydJZGVudGl0eS1pZCddID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19PV05FUl9VU0VSX0lEWydJZGVudGl0eS1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuZ2V0U3RhdHVzKHN0YXR1c0xpc3QpLCB0aGlzLmZvcm1BcHByb3ZlclZhbHVlKGFwcHJvdmVyVmFsdWUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybU93bmVyVmFsdWUob3duZXJWYWx1ZSldKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlTGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBkZWxFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZGVsRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuZ2V0UHJpb3JpdGllcygpLCB0aGlzLmdldFN0YXR1cyhbU3RhdHVzTGV2ZWxzLklOSVRJQUxdKSwgdGhpcy5nZXRBbGxSb2xlcygpLCB0aGlzLmdldFVzZXJzQnlSb2xlKCldKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2VMaXN0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3duZXJWYWx1ZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fT1dORVJfSURbJ0lkZW50aXR5LWlkJ10gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmZvcm1Pd25lclZhbHVlKG93bmVyVmFsdWUpXSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsRGVsaXZlcmFibGVDcmVhdGlvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNsb3NlPycsXHJcbiAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWF4TWluVmFsdWVGb3JOdW1iZXIoc2NhbGUpOiBhbnkge1xyXG4gICAgICAgIGxldCBudW1lcmljZVZsYWx1ZSA9ICc5OTk5OSc7XHJcbiAgICAgICAgaWYgKHNjYWxlKSB7XHJcbiAgICAgICAgICAgIG51bWVyaWNlVmxhbHVlICs9ICcuJyArIG5ld0FycmF5KHNjYWxlKS5maWxsKCc5Jykuam9pbignJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogcmFkaXhcclxuICAgICAgICByZXR1cm4gc2NhbGUgPyBwYXJzZUZsb2F0KG51bWVyaWNlVmxhbHVlKSA6IHBhcnNlSW50KG51bWVyaWNlVmxhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXNlRGVsaXZlcmFibGVDb25maWcoKSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnID0ge1xyXG4gICAgICAgICAgICBoYXNBbGxDb25maWc6IHRydWUsXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0OiBudWxsLFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZVN0YXR1c0xpc3Q6IG51bGwsXHJcbiAgICAgICAgICAgIGRlZmF1bHRTdGF0dXM6IG51bGwsXHJcbiAgICAgICAgICAgIGRlZmF1bHRQcmlvcml0eTogbnVsbCxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lckNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZWxpdmVyYWJsZSBPd25lcicsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJSb2xlQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJhYmxlIE93bmVyIFJvbGUnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJhYmxlIEFwcHJvdmVyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlclJvbGU6IHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnRGVsaXZlcmFibGUgQXBwcm92ZXIgUm9sZScsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHNlbGVjdGVkRGVsaXZlcmFibGU6IHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZSxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lckFzc2lnbm1lbnRUeXBlOiBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEUsXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXJBc3NpZ25tZW50VHlwZTogRGVsaXZlcmFibGVDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFLFxyXG4gICAgICAgICAgICBpc05ld0RlbGl2ZXJhYmxlOiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkKSA/IGZhbHNlIDogdHJ1ZSxcclxuICAgICAgICAgICAgaXNUZW1wbGF0ZTogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5pc1RlbXBsYXRlLFxyXG4gICAgICAgICAgICBwcm9qZWN0SWQ6IHRoaXMuZGVsaXZlcmFibGVDb25maWcucHJvamVjdElkLFxyXG4gICAgICAgICAgICB0YXNrSWQ6IHRoaXMuZGVsaXZlcmFibGVDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZUlkOiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlSWQsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkUHJvamVjdDogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFByb2plY3QsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkVGFzazogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgIG1pbkRhdGU6ICcnLFxyXG4gICAgICAgICAgICBtYXhEYXRlOiAnJyxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVBbGxvY2F0aW9uTGlzdDogW10sXHJcbiAgICAgICAgICAgIGN1cnJTdGF0dXM6IG51bGxcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlT3B0aW9ucyA9IFt0aGlzLnNhdmVPcHRpb25zWzBdXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXIgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyT2JqZWN0KCk7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXIuTWFuYWdlckZvci5UYXJnZXQuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgaWYgKHVzZXIuTmFtZSA9PT0gJ01QTSBQcm9qZWN0IE1hbmFnZXInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzUE0gPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgaWYgKG5hdmlnYXRvci5sYW5ndWFnZSAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWRhcHRlci5zZXRMb2NhbGUobmF2aWdhdG9yLmxhbmd1YWdlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5pc0FwcHJvdmVyID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRJc0FwcHJvdmVyKCk7XHJcbiAgICAgICAgdGhpcy5pc1VwbG9hZGVkID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRJc1VwbG9hZGVkKCk7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0Q29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRQcm9qZWN0Q29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5lbmFibGVOZWVkUmV2aWV3ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5wcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5FTkFCTEVfTkVFRF9SRVZJRVddKTtcclxuICAgICAgICB0aGlzLmVuYWJsZVdvcmtXZWVrID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5wcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5FTkFCTEVfV09SS19XRUVLXSk7XHJcbiAgICAgICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMubmFtZVN0cmluZ1BhdHRlcm4gPSB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuTkFNRV9TVFJJTkdfUEFUVEVSTl0pID8gJy4qJyA6XHJcbiAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnICYmIHRoaXMuZGVsaXZlcmFibGVDb25maWcudGFza0lkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbGlzZURlbGl2ZXJhYmxlQ29uZmlnKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJlRGV0YWlscygpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VEZWxpdmVyYWJsZUZvcm0oKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRhdGVGaWx0ZXI6IChkYXRlOiBEYXRlIHwgbnVsbCkgPT4gYm9vbGVhbiA9XHJcbiAgICAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVXb3JrV2Vlaykge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBkYXkgPSBkYXRlLmdldERheSgpO1xyXG4gICAgICAgICAgICByZXR1cm4gZGF5ICE9PSAwICYmIGRheSAhPT0gNjtcclxuICAgICAgICAgICAgLy8wIG1lYW5zIHN1bmRheVxyXG4gICAgICAgICAgICAvLzYgbWVhbnMgc2F0dXJkYXlcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19