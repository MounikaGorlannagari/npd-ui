import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { TaskService } from '../../task.service';
import { FieldConfigService } from '../../../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../../mpm-utils/objects/MPMField';
import { DeliverableBulkCountService } from '../../../../deliverable-task/service/deliverable-bulk-count.service';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
let DeliverableSwinLaneComponent = class DeliverableSwinLaneComponent {
    constructor(media, changeDetectorRef, taskService, fieldConfigService, deliverableBulkCountService) {
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.taskService = taskService;
        this.fieldConfigService = fieldConfigService;
        this.deliverableBulkCountService = deliverableBulkCountService;
        this.refreshSwimLine = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.editDeliverableHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.deliverableDetailsHandler = new EventEmitter();
        this.versionsHandler = new EventEmitter();
        this.shareAssetHandler = new EventEmitter();
        this.deliverableBulkEditCount = new EventEmitter();
        this.selectedDeliverableCount = new EventEmitter();
        this.selectedDeliverableDatas = new EventEmitter();
        this.selectedDeliverableData = new EventEmitter();
        this.swimListIds = [];
        this.swimList = [];
        this.deliverableId = [];
        this.masterSelected = false;
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    editDeliverable(eventData) {
        this.editDeliverableHandler.emit(eventData);
    }
    masterToogle(event) {
        this.masterToogleDeliverable.emit(event);
    }
    openCR(event) {
        this.crHandler.emit(event);
    }
    deliverableDetails(event) {
        this.deliverableDetailsHandler.emit(event);
    }
    deliverableBulkCheckCount(deliverableData) {
        let id;
        let deliverableid;
        if (this.masterSelectedDeliverable === true) {
            this.masterSelected = false;
        }
        if (this.masterSelectedDeliverable === false && !this.masterSelected) {
            id = [];
            deliverableid = true;
            this.masterSelected = true;
        }
        id = this.deliverableBulkCountService.getBulkDeliverableId(deliverableData, deliverableid);
        /*  if (this.deliverableId && !this.deliverableId.includes(deliverableData)) {
             this.deliverableId.push(deliverableData);
         } else {
             const index = this.deliverableId.indexOf(deliverableData);
             if (index > -1) {
                 this.deliverableId.splice(index, 1);
             }
         }
  */
        if (id.length > 1) {
            this.deliverableBulkEditCount.emit(true);
        }
        else {
            this.deliverableBulkEditCount.emit(false);
        }
        this.selectedDeliverableCount.emit(id.length);
        this.selectedDeliverableDatas.next(id);
    }
    selectedBulkDeliverableData(data) {
        this.selectedDeliverableData.next(data);
    }
    viewVersions(event) {
        this.versionsHandler.emit(event);
    }
    shareAsset(event) {
        this.shareAssetHandler.emit(event);
    }
    getConnectedId(id) {
        return this.swimListIds.filter(ids => ids !== id);
    }
    refresh() {
        this.refreshSwimLine.next(true);
    }
    refreshDeliverable(event) {
        if (event) {
            this.refresh();
        }
    }
    initialiseSwimList() {
        this.swimList = [];
        this.swimListIds = [];
        if (this.deliverableConfig) {
            if (this.deliverableConfig.deliverableStatus) {
                const mpmStatusField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID, MPM_LEVELS.DELIVERABLE);
                this.deliverableConfig.deliverableStatus.map(status => {
                    const listData = [];
                    if (this.deliverableConfig.deliverableTotalCount > 0) {
                        this.deliverableConfig.deliverableList.map(deliverable => {
                            if (this.fieldConfigService.getFieldValueByDisplayColumn(mpmStatusField, deliverable) === status['MPM_Status-id'].Id) {
                                if (deliverable.DELIVERABLE_TYPE !== 'COMPLETE' && deliverable.DELIVERABLE_STATUS_VALUE !== 'Approved' && deliverable.DELIVERABLE_STATUS_VALUE !== 'Cancelled'
                                    && deliverable.DELIVERABLE_STATUS_VALUE !== 'Rejected' && deliverable.DELIVERABLE_TYPE === 'REVIEW') {
                                    deliverable.bulkEdit = true;
                                }
                                else {
                                    deliverable.bulkEdit = false;
                                }
                                if (deliverable.IS_BULK_UPDATE_DELIVERABLE !== 'true' && deliverable.BULK_ACTION !== 'true') {
                                    listData.push(deliverable);
                                }
                            }
                        });
                    }
                    const swimList = {
                        id: status['MPM_Status-id'].Id,
                        listDetail: status,
                        data: listData,
                        dataCount: listData.length
                    };
                    this.swimListIds.push(status['MPM_Status-id'].Id);
                    this.swimList.push(swimList);
                });
            }
        }
        if (this.deliverableConfig && this.deliverableConfig.deliverableTotalCount !== 0) {
            this.deliverableSwimLaneMinHeight = '31rem';
        }
    }
    ngOnInit() {
        this.initialiseSwimList();
    }
};
DeliverableSwinLaneComponent.ctorParameters = () => [
    { type: MediaMatcher },
    { type: ChangeDetectorRef },
    { type: TaskService },
    { type: FieldConfigService },
    { type: DeliverableBulkCountService }
];
__decorate([
    Input()
], DeliverableSwinLaneComponent.prototype, "deliverableConfig", void 0);
__decorate([
    Input()
], DeliverableSwinLaneComponent.prototype, "isViewByResourceFilterEnabled", void 0);
__decorate([
    Input()
], DeliverableSwinLaneComponent.prototype, "deliverableBulkResponse", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "refreshSwimLine", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "loadingHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "notificationHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "editDeliverableHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "crHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "deliverableDetailsHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "versionsHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "shareAssetHandler", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "deliverableBulkEditCount", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "selectedDeliverableCount", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "selectedDeliverableDatas", void 0);
__decorate([
    Output()
], DeliverableSwinLaneComponent.prototype, "selectedDeliverableData", void 0);
__decorate([
    Input()
], DeliverableSwinLaneComponent.prototype, "masterSelectedDeliverable", void 0);
DeliverableSwinLaneComponent = __decorate([
    Component({
        selector: 'mpm-deliverable-swin-lane',
        template: "<div class=\"flex-col \"\r\n    [ngClass]=\"{'swim-layout':isViewByResourceFilterEnabled,'swim-layout-one':!isViewByResourceFilterEnabled}\">\r\n    <div class=\"flex-col-item\">\r\n        <div class=\"swim-nonDrag-columns\">\r\n            <div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\">\r\n                <div class=\"grid-list\">\r\n                    <div class=\"card-wrap\">\r\n                        <div class=\"mpm-card\">\r\n                            <section class=\"card-summary\">{{list.listDetail.NAME}}\r\n                                ({{list.dataCount}})</section>\r\n                        </div>\r\n                    </div>\r\n                    <span>\r\n                        <div class=\"swim-data-card\" *ngFor=\"let item of list.data\">\r\n                            <mpm-deliverable-card [isPMView]=\"true\" [deliverableData]=\"item\"\r\n                                [deliverableBulkResponse]=\"deliverableBulkResponse\"\r\n                                (deliverableBulkCheckCount)=\"deliverableBulkCheckCount($event)\"\r\n                                (selectedDeliverableData)=\"selectedBulkDeliverableData($event)\"\r\n                                [isTemplate]=\"deliverableConfig.isTemplate\"\r\n                                (refreshDeliverable)=\"refreshDeliverable($event)\"\r\n                                (editDeliverableHandler)=\"editDeliverable($event)\" (crHandler)=\"openCR($event)\"\r\n                                (deliverableDetailsHandler)=\"deliverableDetails($event)\"\r\n                                [masterSelectedDeliverable]=\"masterSelectedDeliverable\"\r\n                                (versionsHandler)=\"viewVersions($event)\" (shareAssetHandler)=\"shareAsset($event)\">\r\n                            </mpm-deliverable-card><!-- [projectId]=\"deliverableConfig.projectId\" -->\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>",
        styles: [".swim-layout{height:calc(100vh - 154px);overflow:auto}.swim-layout-one{height:calc(100vh - 200px);overflow:auto}.swim-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0}.swim-column{box-sizing:border-box;list-style:none;margin:0;position:relative;vertical-align:top;justify-content:center;flex:1 1 0}.swim-data-card{margin-bottom:10px}.swim-nonDrag-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0;display:flex;flex-direction:row}.swim-nonDrag-column{box-sizing:border-box;display:table-cell;list-style:none;margin:0 10px 0 0;position:relative;vertical-align:top;table-layout:fixed;padding-left:5px;padding-right:5px;max-width:15rem;border:1px solid #ddd}::ng-deep .swim-data-card .task-content{margin:5px}::ng-deep app-task-card-view .task-card{min-width:180px}.cdk-drag-preview{box-sizing:border-box;border-radius:4px;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{opacity:0}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.card-box:last-child{border:none}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder){transition:transform 250ms cubic-bezier(0,0,.2,1)}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder) .grid-list{border:1px solid red}.mpm-card,.mpm-deliverable{font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.card-summary{display:block;box-sizing:content-box;line-height:1.42857143;max-height:4.28571429em;overflow:hidden}.deliverable-swim{width:240px}.deliverable-data-card{margin:0 0 15px}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}"]
    })
], DeliverableSwinLaneComponent);
export { DeliverableSwinLaneComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtc3dpbi1sYW5lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUtc3dpbi1sYW5lL2RlbGl2ZXJhYmxlLXN3aW4tbGFuZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFakQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDdEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDckYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFFQUFxRSxDQUFBO0FBQ2pILE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQVNqRSxJQUFhLDRCQUE0QixHQUF6QyxNQUFhLDRCQUE0QjtJQStCckMsWUFDVyxLQUFtQixFQUNuQixpQkFBb0MsRUFDcEMsV0FBd0IsRUFDeEIsa0JBQXNDLEVBQ3RDLDJCQUF3RDtRQUp4RCxVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQ25CLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxnQ0FBMkIsR0FBM0IsMkJBQTJCLENBQTZCO1FBaEN6RCxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3pDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsMkJBQXNCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNqRCxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyw4QkFBeUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BELG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxQyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRTVDLDZCQUF3QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkQsNkJBQXdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUVuRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFNNUQsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFDakIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUVkLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBSW5CLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBUW5CLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3hFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDMUIsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQzFEO0lBQ0wsQ0FBQztJQUVELGVBQWUsQ0FBQyxTQUFTO1FBQ3JCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtJQUM1QyxDQUFDO0lBRUQsTUFBTSxDQUFDLEtBQUs7UUFDUixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBSztRQUNwQixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyxlQUFlO1FBQ3JDLElBQUksRUFBRSxDQUFDO1FBQ1AsSUFBSSxhQUFhLENBQUM7UUFDbEIsSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssSUFBSSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNsRSxFQUFFLEdBQUcsRUFBRSxDQUFDO1lBQ1IsYUFBYSxHQUFHLElBQUksQ0FBQztZQUNyQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUVELEVBQUUsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsb0JBQW9CLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzNGOzs7Ozs7OztJQVFKO1FBQ0ksSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNmLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0M7UUFDRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLENBQUM7SUFFRCwyQkFBMkIsQ0FBQyxJQUFJO1FBQzVCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsY0FBYyxDQUFDLEVBQUU7UUFDYixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQUs7UUFDcEIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEI7SUFDTCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzFDLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDcEwsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDbEQsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNwQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxDQUFDLEVBQUU7d0JBQ2xELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUNyRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxjQUFjLEVBQUUsV0FBVyxDQUFDLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQ0FDbEgsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEtBQUssVUFBVSxJQUFJLFdBQVcsQ0FBQyx3QkFBd0IsS0FBSyxVQUFVLElBQUksV0FBVyxDQUFDLHdCQUF3QixLQUFLLFdBQVc7dUNBQ3ZKLFdBQVcsQ0FBQyx3QkFBd0IsS0FBSyxVQUFVLElBQUksV0FBVyxDQUFDLGdCQUFnQixLQUFLLFFBQVEsRUFBRTtvQ0FDckcsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7aUNBQy9CO3FDQUFNO29DQUNILFdBQVcsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2lDQUNoQztnQ0FDRCxJQUFJLFdBQVcsQ0FBQywwQkFBMEIsS0FBSyxNQUFNLElBQUksV0FBVyxDQUFDLFdBQVcsS0FBSyxNQUFNLEVBQUU7b0NBQ3pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7aUNBQzlCOzZCQUNKO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELE1BQU0sUUFBUSxHQUFHO3dCQUNiLEVBQUUsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRTt3QkFDOUIsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLElBQUksRUFBRSxRQUFRO3dCQUNkLFNBQVMsRUFBRSxRQUFRLENBQUMsTUFBTTtxQkFDN0IsQ0FBQztvQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNqQyxDQUFDLENBQUMsQ0FBQzthQUNOO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLEtBQUssQ0FBQyxFQUFFO1lBQzlFLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxPQUFPLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7Q0FFSixDQUFBOztZQW5JcUIsWUFBWTtZQUNBLGlCQUFpQjtZQUN2QixXQUFXO1lBQ0osa0JBQWtCO1lBQ1QsMkJBQTJCOztBQW5DMUQ7SUFBUixLQUFLLEVBQUU7dUVBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFO21GQUErQjtBQUM5QjtJQUFSLEtBQUssRUFBRTs2RUFBeUI7QUFDdkI7SUFBVCxNQUFNLEVBQUU7cUVBQTJDO0FBQzFDO0lBQVQsTUFBTSxFQUFFO29FQUEwQztBQUN6QztJQUFULE1BQU0sRUFBRTt5RUFBK0M7QUFDOUM7SUFBVCxNQUFNLEVBQUU7NEVBQWtEO0FBQ2pEO0lBQVQsTUFBTSxFQUFFOytEQUFxQztBQUNwQztJQUFULE1BQU0sRUFBRTsrRUFBcUQ7QUFDcEQ7SUFBVCxNQUFNLEVBQUU7cUVBQTJDO0FBQzFDO0lBQVQsTUFBTSxFQUFFO3VFQUE2QztBQUU1QztJQUFULE1BQU0sRUFBRTs4RUFBb0Q7QUFDbkQ7SUFBVCxNQUFNLEVBQUU7OEVBQW9EO0FBRW5EO0lBQVQsTUFBTSxFQUFFOzhFQUFvRDtBQUNuRDtJQUFULE1BQU0sRUFBRTs2RUFBbUQ7QUFDbkQ7SUFBUixLQUFLLEVBQUU7K0VBQTJCO0FBbEIxQiw0QkFBNEI7SUFOeEMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLDJCQUEyQjtRQUNyQyxzaUVBQXFEOztLQUV4RCxDQUFDO0dBRVcsNEJBQTRCLENBbUt4QztTQW5LWSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZWRpYU1hdGNoZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0JztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi8uLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUJ1bGtDb3VudFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9kZWxpdmVyYWJsZS10YXNrL3NlcnZpY2UvZGVsaXZlcmFibGUtYnVsay1jb3VudC5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tZGVsaXZlcmFibGUtc3dpbi1sYW5lJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kZWxpdmVyYWJsZS1zd2luLWxhbmUuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGVsaXZlcmFibGUtc3dpbi1sYW5lLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEZWxpdmVyYWJsZVN3aW5MYW5lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIGRlbGl2ZXJhYmxlQ29uZmlnO1xyXG4gICAgQElucHV0KCkgaXNWaWV3QnlSZXNvdXJjZUZpbHRlckVuYWJsZWQ7XHJcbiAgICBASW5wdXQoKSBkZWxpdmVyYWJsZUJ1bGtSZXNwb25zZTtcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoU3dpbUxpbmUgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBsb2FkaW5nSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG5vdGlmaWNhdGlvbkhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0RGVsaXZlcmFibGVIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3JIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZGVsaXZlcmFibGVEZXRhaWxzSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHZlcnNpb25zSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNoYXJlQXNzZXRIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQE91dHB1dCgpIGRlbGl2ZXJhYmxlQnVsa0VkaXRDb3VudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdGVkRGVsaXZlcmFibGVDb3VudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlRGF0YXMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlRGF0YSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQElucHV0KCkgbWFzdGVyU2VsZWN0ZWREZWxpdmVyYWJsZTtcclxuICAgIG1hc3RlclRvb2dsZURlbGl2ZXJhYmxlO1xyXG4gICAgbW9iaWxlUXVlcnk6IE1lZGlhUXVlcnlMaXN0O1xyXG4gICAgcHVibGljIG1vYmlsZVF1ZXJ5TGlzdGVuZXI6ICgpID0+IHZvaWQ7XHJcblxyXG4gICAgc3dpbUxpc3RJZHMgPSBbXTtcclxuICAgIHN3aW1MaXN0ID0gW107XHJcbiAgICBkZWxpdmVyYWJsZVN3aW1MYW5lTWluSGVpZ2h0O1xyXG4gICAgZGVsaXZlcmFibGVJZCA9IFtdO1xyXG4gICAgLy9zdGF0dXMgPSBbJ0NPTVBMRVRFJywgJ0EnLCAnQXJjaGl2ZWQnXTtcclxuXHJcbiAgICB1bnNlbGVjdGVkRGVsaXZlcmFibGU7XHJcbiAgICBtYXN0ZXJTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIG1lZGlhOiBNZWRpYU1hdGNoZXIsXHJcbiAgICAgICAgcHVibGljIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgICAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlOiBEZWxpdmVyYWJsZUJ1bGtDb3VudFNlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMubW9iaWxlUXVlcnkgPSB0aGlzLm1lZGlhLm1hdGNoTWVkaWEoJyhtYXgtd2lkdGg6IDYwMHB4KScpO1xyXG4gICAgICAgIHRoaXMubW9iaWxlUXVlcnlMaXN0ZW5lciA9ICgpID0+IHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgIGlmICh0aGlzLm1vYmlsZVF1ZXJ5Lm1hdGNoZXMpIHtcclxuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkZXByZWNhdGlvblxyXG4gICAgICAgICAgICB0aGlzLm1vYmlsZVF1ZXJ5LmFkZExpc3RlbmVyKHRoaXMubW9iaWxlUXVlcnlMaXN0ZW5lcik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXREZWxpdmVyYWJsZShldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLmVkaXREZWxpdmVyYWJsZUhhbmRsZXIuZW1pdChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG1hc3RlclRvb2dsZShldmVudCkge1xyXG4gICAgICAgIHRoaXMubWFzdGVyVG9vZ2xlRGVsaXZlcmFibGUuZW1pdChldmVudClcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ1IoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmNySGFuZGxlci5lbWl0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxpdmVyYWJsZURldGFpbHMoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRGV0YWlsc0hhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVsaXZlcmFibGVCdWxrQ2hlY2tDb3VudChkZWxpdmVyYWJsZURhdGEpIHtcclxuICAgICAgICBsZXQgaWQ7XHJcbiAgICAgICAgbGV0IGRlbGl2ZXJhYmxlaWQ7XHJcbiAgICAgICAgaWYgKHRoaXMubWFzdGVyU2VsZWN0ZWREZWxpdmVyYWJsZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLm1hc3RlclNlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLm1hc3RlclNlbGVjdGVkRGVsaXZlcmFibGUgPT09IGZhbHNlICYmICF0aGlzLm1hc3RlclNlbGVjdGVkKSB7XHJcbiAgICAgICAgICAgIGlkID0gW107XHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLm1hc3RlclNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlkID0gdGhpcy5kZWxpdmVyYWJsZUJ1bGtDb3VudFNlcnZpY2UuZ2V0QnVsa0RlbGl2ZXJhYmxlSWQoZGVsaXZlcmFibGVEYXRhLCBkZWxpdmVyYWJsZWlkKTtcclxuICAgICAgICAvKiAgaWYgKHRoaXMuZGVsaXZlcmFibGVJZCAmJiAhdGhpcy5kZWxpdmVyYWJsZUlkLmluY2x1ZGVzKGRlbGl2ZXJhYmxlRGF0YSkpIHtcclxuICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZC5wdXNoKGRlbGl2ZXJhYmxlRGF0YSk7XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuZGVsaXZlcmFibGVJZC5pbmRleE9mKGRlbGl2ZXJhYmxlRGF0YSk7XHJcbiAgICAgICAgICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgKi9cclxuICAgICAgICBpZiAoaWQubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQnVsa0VkaXRDb3VudC5lbWl0KHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVCdWxrRWRpdENvdW50LmVtaXQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVDb3VudC5lbWl0KGlkLmxlbmd0aCk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlRGF0YXMubmV4dChpZCk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdGVkQnVsa0RlbGl2ZXJhYmxlRGF0YShkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlRGF0YS5uZXh0KGRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIHZpZXdWZXJzaW9ucyhldmVudCkge1xyXG4gICAgICAgIHRoaXMudmVyc2lvbnNIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNoYXJlQXNzZXQoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbm5lY3RlZElkKGlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3dpbUxpc3RJZHMuZmlsdGVyKGlkcyA9PiBpZHMgIT09IGlkKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgICAgIHRoaXMucmVmcmVzaFN3aW1MaW5lLm5leHQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaERlbGl2ZXJhYmxlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXNlU3dpbUxpc3QoKSB7XHJcbiAgICAgICAgdGhpcy5zd2ltTGlzdCA9IFtdO1xyXG4gICAgICAgIHRoaXMuc3dpbUxpc3RJZHMgPSBbXTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZVN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbXBtU3RhdHVzRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBNUE1GaWVsZENvbnN0YW50cy5ERUxJVkVSQUJMRV9NUE1fRklFTERTLkRFTElWRVJBQkxFX1NUQVRVU19JRCwgTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzLm1hcChzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxpc3REYXRhID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVUb3RhbENvdW50ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdC5tYXAoZGVsaXZlcmFibGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4obXBtU3RhdHVzRmllbGQsIGRlbGl2ZXJhYmxlKSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSAhPT0gJ0NPTVBMRVRFJyAmJiBkZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9TVEFUVVNfVkFMVUUgIT09ICdBcHByb3ZlZCcgJiYgZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfU1RBVFVTX1ZBTFVFICE9PSAnQ2FuY2VsbGVkJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiBkZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9TVEFUVVNfVkFMVUUgIT09ICdSZWplY3RlZCcgJiYgZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSA9PT0gJ1JFVklFVycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGUuYnVsa0VkaXQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLmJ1bGtFZGl0ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZS5JU19CVUxLX1VQREFURV9ERUxJVkVSQUJMRSAhPT0gJ3RydWUnICYmIGRlbGl2ZXJhYmxlLkJVTEtfQUNUSU9OICE9PSAndHJ1ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdERhdGEucHVzaChkZWxpdmVyYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3dpbUxpc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGlzdERldGFpbDogc3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBsaXN0RGF0YSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YUNvdW50OiBsaXN0RGF0YS5sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3dpbUxpc3RJZHMucHVzaChzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zd2ltTGlzdC5wdXNoKHN3aW1MaXN0KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnICYmIHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVUb3RhbENvdW50ICE9PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVTd2ltTGFuZU1pbkhlaWdodCA9ICczMXJlbSc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGlzZVN3aW1MaXN0KCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==