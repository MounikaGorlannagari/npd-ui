import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BulkCommentsComponent } from '../../../../../lib/comments/bulk-comments/bulk-comments.component';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../../../../notification/notification.service';
import { LoaderService } from '../../../../loader/loader.service';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../../../mpm-utils/services/entity.appdef.service';
import { Observable } from 'rxjs';
import { AppService } from '../../../../mpm-utils/services/app.service';
import { StatusTypes } from '../../../../mpm-utils/objects/StatusType';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
import { StatusService } from '../../../../shared/services/status.service';
import { ProjectConstant } from '../../../../project/project-overview/project.constants';
import { TaskConstants } from '../../task.constants';
import * as acronui from '../../../../mpm-utils/auth/utility';
import { MPM_ROLES } from '../../../../mpm-utils/objects/Role';
let BulkActionsComponent = class BulkActionsComponent {
    constructor(dialog, notificationService, loaderService, sharingService, entityAppdefService, appService, statusService) {
        this.dialog = dialog;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.sharingService = sharingService;
        this.entityAppdefService = entityAppdefService;
        this.appService = appService;
        this.statusService = statusService;
        this.unselectDeliverable = new EventEmitter();
        this.bulkResponse = new EventEmitter();
        this.bulkResponseData = new EventEmitter();
        this.bulkActions = true;
        this.projectStatus = {
            REQUIRE_REASON: 'true',
            REQUIRE_COMMENTS: 'true',
            MPM_Status_Reason: ''
        };
    }
    bulkAction(data, bulkAction) {
        this.bulkStatus = data;
        this.bulkStatusAction = bulkAction;
        const dialogRef = this.dialog.open(BulkCommentsComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: 'Bulk Approval',
                hasConfirmationComment: true,
                status: this.bulkStatus,
                deliverableData: this.deliverableData,
                projectData: this.projectData,
                statusMessage: 'Approval State',
                commentText: 'Comments',
                isCommentRequired: true,
                errorMessage: 'Kindly fill all the required fields',
                submitButton: 'Yes',
                cancelButton: 'No',
                statusData: this.projectStatus,
            }
        });
        dialogRef.afterClosed().subscribe(bulkComment => {
            if (bulkComment) {
                const appId = this.entityAppdefService.getApplicationID();
                const crActions = this.sharingService.getCRActions();
                const teamItemId = this.projectData.R_PO_TEAM['MPM_Teams-id'].Id;
                const crAction = crActions.find(el => el['MPM_Teams-id'].Id === teamItemId);
                const selectedReasonsData = bulkComment.SelectedReasons;
                const assetId = '';
                const versioning = true;
                const isBulkApprove = true;
                const taskName = '';
                let teamRole;
                const userRoles = acronui.findObjectsByProp(crAction, 'MPM_Team_Role_Mapping');
                userRoles.forEach(role => {
                    const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                    const roleObj = appRoles.find(appRole => {
                        return appRole.ROLE_NAME === MPM_ROLES.MANAGER;
                    });
                    if (roleObj) {
                        teamRole = role;
                    }
                });
                if (teamRole && teamRole.Approver_CR_Role && teamRole.Approver_CR_Role.ROLE_ID) {
                    this.crRole = teamRole.Approver_CR_Role;
                }
                this.performBulkAction(this.bulkStatusAction, appId, this.deliverableData, assetId, this.crRole.ROLE_ID, versioning, taskName, bulkComment.comment, isBulkApprove, selectedReasonsData);
            }
        });
    }
    getBulkActionData(appId, deliverableId, assetId, crRoleId, versioning, taskName, bulkComment, isBulkApprove, statusId, selectedReasonsData) {
        return new Observable(observer => {
            this.appService.handleCRApproval(appId, deliverableId, assetId, taskName, crRoleId, versioning, bulkComment, isBulkApprove, statusId, selectedReasonsData, '')
                .subscribe(response => {
                if (response) {
                    this.notificationService.info('Bulk Approval process initiated');
                    // this.reload(true)
                    this.bulkResponse.next(true);
                    this.bulkResponseData.next(deliverableId);
                    this.loaderService.hide();
                }
                else {
                    observer.error('Something went wrong in Bulk Approval process');
                }
            }, error => {
                observer.error(error);
            });
        });
    }
    performBulkAction(action, appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, selectedReasonsData) {
        let statusType;
        this.loaderService.show();
        if (action === 'Approve') {
            statusType = StatusTypes.FINAL_APPROVED;
        }
        else if (action === 'Reject') {
            statusType = StatusTypes.FINAL_REJECTED;
        }
        else if (action === 'Complete') {
            statusType = StatusTypes.FINAL_COMPLETED;
        }
        this.statusService.getStatusByCategoryLevelAndStatusType(MPM_LEVELS.DELIVERABLE, statusType)
            .subscribe(response => {
            const status = response.find(el => el.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || el.STATUS_TYPE === StatusTypes.FINAL_APPROVED || el.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
            if (status) {
                this.statusId = status['MPM_Status-id'].Id;
                this.getBulkActionData(appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, this.statusId, selectedReasonsData)
                    .subscribe(crData => {
                    this.loaderService.hide();
                    //this.crHandler.next(crData);
                    console.log(crData);
                }, error => {
                    this.loaderService.hide();
                    this.notificationService.error('Something went wrong in Bulk Approval process');
                });
            }
            else {
                this.loaderService.hide();
                this.notificationService.error('Something went wrong while performing the deliverable action');
            }
        }, error => {
            this.loaderService.hide();
            this.notificationService.error('Something went wrong while performing the deliverable action');
        });
    }
    masterToogle(event) {
        this.bulkActions = event.checked;
        this.unselectDeliverable.next(event.checked);
    }
    getPriorities() {
        return new Observable(observer => {
            let priority = this.entityAppdefService.getPriorities(this.categoryLevel);
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                this.priorityList = priority;
            }
            else {
                this.priorityList = [];
            }
            /*  this.entityAppdefService.getPriorities(this.categoryLevel)
               .subscribe(priorityResponse => {
                 if (priorityResponse) {
                   if (!Array.isArray(priorityResponse)) {
                     priorityResponse = [priorityResponse];
                   }
                   this.priorityList = priorityResponse;
                 } else {
                   this.priorityList = [];
                 }
                 observer.next(true);
                 observer.complete();
               }, () => {
                 observer.next(false);
                 observer.complete();
               }); */
        });
    }
    getAllRoles() {
        const getRequestObject = {
            teamID: this.teamId,
            isApproverTask: this.isApprovalTask,
            isMemberTask: !this.isApprovalTask
        };
        return new Observable(observer => {
            this.loaderService.show();
            this.appService.getTeamRoles(getRequestObject)
                .subscribe(response => {
                if (response && response.Teams && response.Teams.MPM_Teams && response.Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    response = response.Teams.MPM_Teams;
                    if (!Array.isArray(response.MPM_Team_Role_Mapping)) {
                        response.MPM_Team_Role_Mapping = [response.MPM_Team_Role_Mapping];
                    }
                    let roleList;
                    roleList = response.MPM_Team_Role_Mapping;
                    const roles = [];
                    if (roleList && roleList.length && roleList.length > 0) {
                        roleList.map(role => {
                            roles.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    this.teamRolesOptions = roles;
                }
                else {
                    this.teamRolesOptions = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                observer.next(false);
                observer.complete();
            });
        });
    }
    getUsersByRole() {
        return new Observable(observer => {
            this.loaderService.show();
            let getUserByRoleRequest = {};
            if (this.selectedRole) {
                getUserByRoleRequest = {
                    allocationType: this.selectedAllocation,
                    roleDN: this.selectedAllocation === TaskConstants.ALLOCATION_TYPE_ROLE ? this.selectedRole.value : '',
                    teamID: this.selectedAllocation === TaskConstants.ALLOCATION_TYPE_USER ? this.teamId : '',
                    isApproveTask: this.isApprovalTask,
                    isUploadTask: !this.isApprovalTask
                };
            }
            else {
                getUserByRoleRequest = {
                    allocationType: this.selectedAllocation,
                    roleDN: '',
                    teamID: this.teamId,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(response => {
                if (response && response.users && response.users.user) {
                    const userList = acronui.findObjectsByProp(response, 'user');
                    const users = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(user => {
                            const fieldObj = users.find(e => e.value === user.dn);
                            if (!fieldObj) {
                                users.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    this.teamRoleUserOptions = users;
                    // TODO: getuser entities by userId
                }
                else {
                    this.teamRoleUserOptions = [];
                }
                this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, () => {
                observer.next(false);
                observer.complete();
            });
        });
    }
    ngOnInit() {
        this.statusService.getStatusByCategoryLevelAndStatusType(ProjectConstant.CATEGORY_LEVEL_PROJECT, StatusTypes.INITIAL).subscribe(response => {
            console.log(response);
        });
        this.statusService.getAllCRStatusReasons().subscribe(response => {
            this.projectStatus.MPM_Status_Reason = response;
        });
    }
};
BulkActionsComponent.ctorParameters = () => [
    { type: MatDialog },
    { type: NotificationService },
    { type: LoaderService },
    { type: SharingService },
    { type: EntityAppDefService },
    { type: AppService },
    { type: StatusService }
];
__decorate([
    Input()
], BulkActionsComponent.prototype, "enableAction", void 0);
__decorate([
    Input()
], BulkActionsComponent.prototype, "delCount", void 0);
__decorate([
    Input()
], BulkActionsComponent.prototype, "deliverableData", void 0);
__decorate([
    Input()
], BulkActionsComponent.prototype, "projectData", void 0);
__decorate([
    Output()
], BulkActionsComponent.prototype, "unselectDeliverable", void 0);
__decorate([
    Output()
], BulkActionsComponent.prototype, "bulkResponse", void 0);
__decorate([
    Output()
], BulkActionsComponent.prototype, "bulkResponseData", void 0);
__decorate([
    Input()
], BulkActionsComponent.prototype, "selectedProjectData", void 0);
__decorate([
    Input()
], BulkActionsComponent.prototype, "categoryLevel", void 0);
BulkActionsComponent = __decorate([
    Component({
        selector: 'mpm-bulk-actions',
        template: "<div class=\"flex-container\" *ngIf=\"bulkActions && delCount > 0\">\r\n    <div class=\"flex-container-item\">\r\n        <mat-checkbox class=\"mat-checkbox-color\" [checked]='true' (change)=\"$event ? masterToogle($event) : null\">\r\n        </mat-checkbox>\r\n        <span>{{delCount}} Items Selected </span>\r\n    </div>\r\n    <div>\r\n        <span>\r\n            <!-- *ngIf=\"!enableAction\" -->\r\n            <button class=\"new-project-btn\" [disabled]=\"!enableAction\" mat-raised-button matTooltip=\"Actions\"\r\n                [matMenuTriggerFor]=\"actionTypeMenu\">\r\n                <span> Actions</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n        </span>\r\n        <mat-menu #actionTypeMenu=\"matMenu\">\r\n            <button mat-menu-item matTooltip=\"Approved\" (click)=\"bulkAction('Approved','Approve')\">\r\n                <span>Approved</span>\r\n            </button>\r\n            <button mat-menu-item matTooltip=\"Requested Changes\" (click)=\"bulkAction('Requested Changes','Reject')\">\r\n                <span>Requested Changes</span>\r\n            </button>\r\n        </mat-menu>\r\n\r\n        <!--  <mat-form-field appearance=\"outline\">\r\n            <mat-label>Status</mat-label>\r\n            <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"projectStatus\"\r\n                (selectionChange)=\"validateTaskStatusUpdate(selectedStatus)\" [required]=\"true\"\r\n                [disabled]=\"projectOverviewForm.get('status').status === 'DISABLED' ? true : false\">\r\n                <span *ngFor=\"let status of overViewConfig.statusOptions\">\r\n                    <mat-option value=\"{{status['MPM_Status-id'].Id}}\">\r\n                        {{status.NAME}}\r\n                    </mat-option>\r\n                </span>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Priority</mat-label>\r\n            <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" [required]=\"true\"\r\n                [disabled]=\"projectOverviewForm.get('priority').status === 'DISABLED' ? true : false\">\r\n                <mat-option *ngFor=\"let priority of overViewConfig.priorityOptions\"\r\n                    value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                    {{priority.DESCRIPTION}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"overViewConfig.isProject\">\r\n            <mat-label>Start Date</mat-label>\r\n            <input formControlName=\"startDate\" matInput [matDatepicker]=\"startDate\"\r\n                placeholder=\"Start Date (MM/DD/YYYY)\" required\r\n                [min]=\"this.overViewConfig && this.overViewConfig.selectedProject && this.overViewConfig.selectedProject.START_DATE ? this.overViewConfig.selectedProject.START_DATE : minStartDate\"\r\n                [(value)]=\"selectedStartDate\" (dateChange)=\"validateProjectStartDate($event)\"\r\n                [max]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.startMaxDate ? overViewConfig.dateValidation.startMaxDate : null\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDate [disabled]=\"disableStartDate\"></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"overViewConfig.isProject\">\r\n            <mat-label>End Date</mat-label>\r\n            <input formControlName=\"endDate\"\r\n                [min]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.endMinDate ? overViewConfig.dateValidation.endMinDate : (projectOverviewForm.value.startDate ? projectOverviewForm.value.startDate : selectedStartDate)\"\r\n                matInput [matDatepicker]=\"endDate\" placeholder=\"End Date (MM/DD/YYYY)\" required\r\n                [(value)]=\"selectedEndDate\" (dateChange)=\"validateProjectEndDate($event)\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #endDate [disabled]=\"disableEndDate\"></mat-datepicker>\r\n        </mat-form-field>-->\r\n    </div>\r\n\r\n</div>",
        styles: [".flex-container{display:flex}.flex-container>div{margin:10px;padding:10px}.flex-container-item{padding:18px!important}.mat-checkbox-color{background-color:#fff}"]
    })
], BulkActionsComponent);
export { BulkActionsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVsay1hY3Rpb25zLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvYnVsay1hY3Rpb25zL2J1bGstYWN0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sbUVBQW1FLENBQUM7QUFDMUcsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDaEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDM0YsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRCxPQUFPLEtBQUssT0FBTyxNQUFNLG9DQUFvQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQU0vRCxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQStCL0IsWUFDUyxNQUFpQixFQUNqQixtQkFBd0MsRUFDeEMsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsbUJBQXdDLEVBQ3hDLFVBQXNCLEVBQ3RCLGFBQTRCO1FBTjVCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBaEMzQix3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2QyxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBV3JELGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBT25CLGtCQUFhLEdBQUc7WUFDZCxjQUFjLEVBQUUsTUFBTTtZQUN0QixnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLGlCQUFpQixFQUFFLEVBQUU7U0FDdEIsQ0FBQztJQVNFLENBQUM7SUFFTCxVQUFVLENBQUMsSUFBSSxFQUFFLFVBQVU7UUFFekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQztRQUNuQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUN4RCxLQUFLLEVBQUUsS0FBSztZQUNaLFlBQVksRUFBRSxJQUFJO1lBQ2xCLElBQUksRUFDSjtnQkFDRSxPQUFPLEVBQUUsZUFBZTtnQkFDeEIsc0JBQXNCLEVBQUUsSUFBSTtnQkFDNUIsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUN2QixlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7Z0JBQ3JDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztnQkFDN0IsYUFBYSxFQUFFLGdCQUFnQjtnQkFDL0IsV0FBVyxFQUFFLFVBQVU7Z0JBQ3ZCLGlCQUFpQixFQUFFLElBQUk7Z0JBQ3ZCLFlBQVksRUFBRSxxQ0FBcUM7Z0JBQ25ELFlBQVksRUFBRSxLQUFLO2dCQUNuQixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsVUFBVSxFQUFFLElBQUksQ0FBQyxhQUFhO2FBQy9CO1NBQ0YsQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUM5QyxJQUFJLFdBQVcsRUFBRTtnQkFDZixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUQsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDckQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNqRSxNQUFNLFFBQVEsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxVQUFVLENBQUMsQ0FBQztnQkFDNUUsTUFBTSxtQkFBbUIsR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDO2dCQUN4RCxNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7Z0JBQ25CLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDeEIsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7Z0JBQ3BCLElBQUksUUFBUSxDQUFDO2dCQUNiLE1BQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFDL0UsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDdkIsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztvQkFDbEUsTUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTt3QkFDdEMsT0FBTyxPQUFPLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxPQUFPLENBQUM7b0JBQ2pELENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksT0FBTyxFQUFFO3dCQUNYLFFBQVEsR0FBRyxJQUFJLENBQUM7cUJBQ2pCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO29CQUM5RSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDekM7Z0JBRUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO2FBQ3pMO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsbUJBQW1CO1FBQ3hJLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxtQkFBbUIsRUFBRSxFQUFFLENBQUM7aUJBQzNKLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLEVBQUU7b0JBQ1osSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO29CQUNqRSxvQkFBb0I7b0JBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2lCQUUzQjtxQkFBTTtvQkFDTCxRQUFRLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7aUJBQ2pFO1lBQ0gsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNULFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxNQUF5QyxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsbUJBQW1CO1FBQ3pLLElBQUksVUFBVSxDQUFDO1FBQ2YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUUxQixJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7WUFDeEIsVUFBVSxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUM7U0FDekM7YUFBTSxJQUFJLE1BQU0sS0FBSyxRQUFRLEVBQUU7WUFDOUIsVUFBVSxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUM7U0FDekM7YUFBTSxJQUFJLE1BQU0sS0FBSyxVQUFVLEVBQUU7WUFDaEMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7U0FDMUM7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLHFDQUFxQyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDO2FBQ3pGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixNQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxFQUFFLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNyTCxJQUFJLE1BQU0sRUFBRTtnQkFDVixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUM7cUJBQ2xKLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsOEJBQThCO29CQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7b0JBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO2dCQUNsRixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsOERBQThELENBQUMsQ0FBQzthQUNoRztRQUNILENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNULElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyw4REFBOEQsQ0FBQyxDQUFDO1FBQ2pHLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLO1FBQ2hCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUQsYUFBYTtRQUNYLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFFL0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7WUFFekUsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzVCLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QjtnQkFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzthQUN4QjtZQUdEOzs7Ozs7Ozs7Ozs7Ozs7cUJBZVM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxXQUFXO1FBQ1QsTUFBTSxnQkFBZ0IsR0FBRztZQUN2QixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDbkIsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO1lBQ25DLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjO1NBQ25DLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRTtvQkFDNUcsUUFBUSxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO29CQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsRUFBRTt3QkFDbEQsUUFBUSxDQUFDLHFCQUFxQixHQUFHLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUM7cUJBQ25FO29CQUNELElBQUksUUFBUSxDQUFDO29CQUNiLFFBQVEsR0FBRyxRQUFRLENBQUMscUJBQXFCLENBQUM7b0JBQzFDLE1BQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDdEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDbEIsS0FBSyxDQUFDLElBQUksQ0FBQztnQ0FDVCxJQUFJLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRTtnQ0FDekMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPO2dDQUNuQixXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0NBQ3RCLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYTs2QkFDN0IsQ0FBQyxDQUFDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNKO29CQUNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7aUJBQy9CO3FCQUFNO29CQUNMLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7aUJBQzVCO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsR0FBRyxFQUFFO2dCQUNOLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGNBQWM7UUFDWixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxvQkFBb0IsR0FBRyxFQUFFLENBQUM7WUFDOUIsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNyQixvQkFBb0IsR0FBRztvQkFDckIsY0FBYyxFQUFFLElBQUksQ0FBQyxrQkFBa0I7b0JBQ3ZDLE1BQU0sRUFBRSxJQUFJLENBQUMsa0JBQWtCLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDckcsTUFBTSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3pGLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYztvQkFDbEMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWM7aUJBQ25DLENBQUM7YUFDSDtpQkFBTTtnQkFDTCxvQkFBb0IsR0FBRztvQkFDckIsY0FBYyxFQUFFLElBQUksQ0FBQyxrQkFBa0I7b0JBQ3ZDLE1BQU0sRUFBRSxFQUFFO29CQUNWLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtvQkFDbkIsYUFBYSxFQUFFLEtBQUs7b0JBQ3BCLFlBQVksRUFBRSxJQUFJO2lCQUNuQixDQUFDO2FBQ0g7WUFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDbEQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNwQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO29CQUNyRCxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUM3RCxNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7b0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3RELFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ3RCLE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDdEQsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQ0FDYixLQUFLLENBQUMsSUFBSSxDQUFDO29DQUNULElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2QsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2lDQUN2QixDQUFDLENBQUM7NkJBQ0o7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztvQkFDakMsbUNBQW1DO2lCQUNwQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO2lCQUMvQjtnQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLEdBQUcsRUFBRTtnQkFDTixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQ0FBcUMsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUN6SSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM5RCxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7Q0FDRixDQUFBOztZQTFRa0IsU0FBUztZQUNJLG1CQUFtQjtZQUN6QixhQUFhO1lBQ1osY0FBYztZQUNULG1CQUFtQjtZQUM1QixVQUFVO1lBQ1AsYUFBYTs7QUFwQzVCO0lBQVIsS0FBSyxFQUFFOzBEQUFjO0FBQ2I7SUFBUixLQUFLLEVBQUU7c0RBQVU7QUFDVDtJQUFSLEtBQUssRUFBRTs2REFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7eURBQWE7QUFDWDtJQUFULE1BQU0sRUFBRTtpRUFBK0M7QUFDOUM7SUFBVCxNQUFNLEVBQUU7MERBQXdDO0FBQ3ZDO0lBQVQsTUFBTSxFQUFFOzhEQUE0QztBQUM1QztJQUFSLEtBQUssRUFBRTtpRUFBcUI7QUFDcEI7SUFBUixLQUFLLEVBQUU7MkRBQWU7QUFWWixvQkFBb0I7SUFMaEMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGtCQUFrQjtRQUM1Qix3eElBQTRDOztLQUU3QyxDQUFDO0dBQ1csb0JBQW9CLENBMFNoQztTQTFTWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJ1bGtDb21tZW50c0NvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9jb21tZW50cy9idWxrLWNvbW1lbnRzL2J1bGstY29tbWVudHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IFN0YXR1c1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvc3RhdHVzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi8uLi8uLi9wcm9qZWN0L3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1fUk9MRVMgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9Sb2xlJztcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tYnVsay1hY3Rpb25zJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vYnVsay1hY3Rpb25zLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9idWxrLWFjdGlvbnMuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCdWxrQWN0aW9uc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIGVuYWJsZUFjdGlvbjtcclxuICBASW5wdXQoKSBkZWxDb3VudDtcclxuICBASW5wdXQoKSBkZWxpdmVyYWJsZURhdGE7XHJcbiAgQElucHV0KCkgcHJvamVjdERhdGE7XHJcbiAgQE91dHB1dCgpIHVuc2VsZWN0RGVsaXZlcmFibGUgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgYnVsa1Jlc3BvbnNlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGJ1bGtSZXNwb25zZURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBASW5wdXQoKSBzZWxlY3RlZFByb2plY3REYXRhO1xyXG4gIEBJbnB1dCgpIGNhdGVnb3J5TGV2ZWw7XHJcblxyXG4gIHByaW9yaXR5TGlzdDtcclxuICBpc0FwcHJvdmFsVGFzaztcclxuICB0ZWFtSWQ7XHJcbiAgdGVhbVJvbGVzT3B0aW9ucztcclxuICB0ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gIHNlbGVjdGVkUm9sZTtcclxuICBzZWxlY3RlZEFsbG9jYXRpb247XHJcbiAgYnVsa0FjdGlvbnMgPSB0cnVlO1xyXG4gIGNyUm9sZTtcclxuICBidWxrU3RhdHVzO1xyXG4gIHN0YXR1c0lkO1xyXG4gIHN0YXR1c09wdGlvbnM7XHJcbiAgYnVsa1N0YXR1c0FjdGlvbjtcclxuXHJcbiAgcHJvamVjdFN0YXR1cyA9IHtcclxuICAgIFJFUVVJUkVfUkVBU09OOiAndHJ1ZScsXHJcbiAgICBSRVFVSVJFX0NPTU1FTlRTOiAndHJ1ZScsXHJcbiAgICBNUE1fU3RhdHVzX1JlYXNvbjogJydcclxuICB9O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIHN0YXR1c1NlcnZpY2U6IFN0YXR1c1NlcnZpY2UsXHJcbiAgKSB7IH1cclxuXHJcbiAgYnVsa0FjdGlvbihkYXRhLCBidWxrQWN0aW9uKSB7XHJcblxyXG4gICAgdGhpcy5idWxrU3RhdHVzID0gZGF0YTtcclxuICAgIHRoaXMuYnVsa1N0YXR1c0FjdGlvbiA9IGJ1bGtBY3Rpb247XHJcbiAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEJ1bGtDb21tZW50c0NvbXBvbmVudCwge1xyXG4gICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgZGF0YTpcclxuICAgICAge1xyXG4gICAgICAgIG1lc3NhZ2U6ICdCdWxrIEFwcHJvdmFsJyxcclxuICAgICAgICBoYXNDb25maXJtYXRpb25Db21tZW50OiB0cnVlLFxyXG4gICAgICAgIHN0YXR1czogdGhpcy5idWxrU3RhdHVzLFxyXG4gICAgICAgIGRlbGl2ZXJhYmxlRGF0YTogdGhpcy5kZWxpdmVyYWJsZURhdGEsXHJcbiAgICAgICAgcHJvamVjdERhdGE6IHRoaXMucHJvamVjdERhdGEsXHJcbiAgICAgICAgc3RhdHVzTWVzc2FnZTogJ0FwcHJvdmFsIFN0YXRlJyxcclxuICAgICAgICBjb21tZW50VGV4dDogJ0NvbW1lbnRzJyxcclxuICAgICAgICBpc0NvbW1lbnRSZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICBlcnJvck1lc3NhZ2U6ICdLaW5kbHkgZmlsbCBhbGwgdGhlIHJlcXVpcmVkIGZpZWxkcycsXHJcbiAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICBjYW5jZWxCdXR0b246ICdObycsXHJcbiAgICAgICAgc3RhdHVzRGF0YTogdGhpcy5wcm9qZWN0U3RhdHVzLFxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUoYnVsa0NvbW1lbnQgPT4ge1xyXG4gICAgICBpZiAoYnVsa0NvbW1lbnQpIHtcclxuICAgICAgICBjb25zdCBhcHBJZCA9IHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRBcHBsaWNhdGlvbklEKCk7XHJcbiAgICAgICAgY29uc3QgY3JBY3Rpb25zID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDUkFjdGlvbnMoKTtcclxuICAgICAgICBjb25zdCB0ZWFtSXRlbUlkID0gdGhpcy5wcm9qZWN0RGF0YS5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkO1xyXG4gICAgICAgIGNvbnN0IGNyQWN0aW9uID0gY3JBY3Rpb25zLmZpbmQoZWwgPT4gZWxbJ01QTV9UZWFtcy1pZCddLklkID09PSB0ZWFtSXRlbUlkKTtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZFJlYXNvbnNEYXRhID0gYnVsa0NvbW1lbnQuU2VsZWN0ZWRSZWFzb25zO1xyXG4gICAgICAgIGNvbnN0IGFzc2V0SWQgPSAnJztcclxuICAgICAgICBjb25zdCB2ZXJzaW9uaW5nID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCBpc0J1bGtBcHByb3ZlID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCB0YXNrTmFtZSA9ICcnO1xyXG4gICAgICAgIGxldCB0ZWFtUm9sZTtcclxuICAgICAgICBjb25zdCB1c2VyUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKGNyQWN0aW9uLCAnTVBNX1RlYW1fUm9sZV9NYXBwaW5nJyk7XHJcbiAgICAgICAgdXNlclJvbGVzLmZvckVhY2gocm9sZSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBhcHBSb2xlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aocm9sZSwgJ01QTV9BUFBfUm9sZXMnKTtcclxuICAgICAgICAgIGNvbnN0IHJvbGVPYmogPSBhcHBSb2xlcy5maW5kKGFwcFJvbGUgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gYXBwUm9sZS5ST0xFX05BTUUgPT09IE1QTV9ST0xFUy5NQU5BR0VSO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpZiAocm9sZU9iaikge1xyXG4gICAgICAgICAgICB0ZWFtUm9sZSA9IHJvbGU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh0ZWFtUm9sZSAmJiB0ZWFtUm9sZS5BcHByb3Zlcl9DUl9Sb2xlICYmIHRlYW1Sb2xlLkFwcHJvdmVyX0NSX1JvbGUuUk9MRV9JRCkge1xyXG4gICAgICAgICAgdGhpcy5jclJvbGUgPSB0ZWFtUm9sZS5BcHByb3Zlcl9DUl9Sb2xlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5wZXJmb3JtQnVsa0FjdGlvbih0aGlzLmJ1bGtTdGF0dXNBY3Rpb24sIGFwcElkLCB0aGlzLmRlbGl2ZXJhYmxlRGF0YSwgYXNzZXRJZCwgdGhpcy5jclJvbGUuUk9MRV9JRCwgdmVyc2lvbmluZywgdGFza05hbWUsIGJ1bGtDb21tZW50LmNvbW1lbnQsIGlzQnVsa0FwcHJvdmUsIHNlbGVjdGVkUmVhc29uc0RhdGEpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEJ1bGtBY3Rpb25EYXRhKGFwcElkLCBkZWxpdmVyYWJsZUlkLCBhc3NldElkLCBjclJvbGVJZCwgdmVyc2lvbmluZywgdGFza05hbWUsIGJ1bGtDb21tZW50LCBpc0J1bGtBcHByb3ZlLCBzdGF0dXNJZCwgc2VsZWN0ZWRSZWFzb25zRGF0YSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuaGFuZGxlQ1JBcHByb3ZhbChhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgdGFza05hbWUsIGNyUm9sZUlkLCB2ZXJzaW9uaW5nLCBidWxrQ29tbWVudCwgaXNCdWxrQXBwcm92ZSwgc3RhdHVzSWQsIHNlbGVjdGVkUmVhc29uc0RhdGEsICcnKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdCdWxrIEFwcHJvdmFsIHByb2Nlc3MgaW5pdGlhdGVkJyk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMucmVsb2FkKHRydWUpXHJcbiAgICAgICAgICAgIHRoaXMuYnVsa1Jlc3BvbnNlLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuYnVsa1Jlc3BvbnNlRGF0YS5uZXh0KGRlbGl2ZXJhYmxlSWQpO1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG5cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBpbiBCdWxrIEFwcHJvdmFsIHByb2Nlc3MnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHBlcmZvcm1CdWxrQWN0aW9uKGFjdGlvbjogJ0FwcHJvdmUnIHwgJ1JlamVjdCcgfCAnQ29tcGxldGUnLCBhcHBJZCwgZGVsaXZlcmFibGVEYXRhLCBhc3NldElkLCBjclJvbGUsIHZlcnNpb25pbmcsIHRhc2tOYW1lLCBidWxrQ29tbWVudCwgaXNCdWxrQXBwcm92ZSwgc2VsZWN0ZWRSZWFzb25zRGF0YSkge1xyXG4gICAgbGV0IHN0YXR1c1R5cGU7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG5cclxuICAgIGlmIChhY3Rpb24gPT09ICdBcHByb3ZlJykge1xyXG4gICAgICBzdGF0dXNUeXBlID0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQ7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PT0gJ1JlamVjdCcpIHtcclxuICAgICAgc3RhdHVzVHlwZSA9IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT09ICdDb21wbGV0ZScpIHtcclxuICAgICAgc3RhdHVzVHlwZSA9IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRDtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsQW5kU3RhdHVzVHlwZShNUE1fTEVWRUxTLkRFTElWRVJBQkxFLCBzdGF0dXNUeXBlKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBjb25zdCBzdGF0dXMgPSByZXNwb25zZS5maW5kKGVsID0+IGVsLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQgfHwgZWwuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEIHx8IGVsLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCk7XHJcbiAgICAgICAgaWYgKHN0YXR1cykge1xyXG4gICAgICAgICAgdGhpcy5zdGF0dXNJZCA9IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkO1xyXG4gICAgICAgICAgdGhpcy5nZXRCdWxrQWN0aW9uRGF0YShhcHBJZCwgZGVsaXZlcmFibGVEYXRhLCBhc3NldElkLCBjclJvbGUsIHZlcnNpb25pbmcsIHRhc2tOYW1lLCBidWxrQ29tbWVudCwgaXNCdWxrQXBwcm92ZSwgdGhpcy5zdGF0dXNJZCwgc2VsZWN0ZWRSZWFzb25zRGF0YSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShjckRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgLy90aGlzLmNySGFuZGxlci5uZXh0KGNyRGF0YSk7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coY3JEYXRhKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyBpbiBCdWxrIEFwcHJvdmFsIHByb2Nlc3MnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHBlcmZvcm1pbmcgdGhlIGRlbGl2ZXJhYmxlIGFjdGlvbicpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBwZXJmb3JtaW5nIHRoZSBkZWxpdmVyYWJsZSBhY3Rpb24nKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBtYXN0ZXJUb29nbGUoZXZlbnQpIHtcclxuICAgIHRoaXMuYnVsa0FjdGlvbnMgPSBldmVudC5jaGVja2VkO1xyXG4gICAgdGhpcy51bnNlbGVjdERlbGl2ZXJhYmxlLm5leHQoZXZlbnQuY2hlY2tlZCk7XHJcbiAgfVxyXG5cclxuICBnZXRQcmlvcml0aWVzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG5cclxuICAgICAgbGV0IHByaW9yaXR5ID0gdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFByaW9yaXRpZXModGhpcy5jYXRlZ29yeUxldmVsKVxyXG5cclxuICAgICAgaWYgKHByaW9yaXR5KSB7XHJcbiAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHByaW9yaXR5KSkge1xyXG4gICAgICAgICAgcHJpb3JpdHkgPSBbcHJpb3JpdHldO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnByaW9yaXR5TGlzdCA9IHByaW9yaXR5O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucHJpb3JpdHlMaXN0ID0gW107XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICAvKiAgdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFByaW9yaXRpZXModGhpcy5jYXRlZ29yeUxldmVsKVxyXG4gICAgICAgICAuc3Vic2NyaWJlKHByaW9yaXR5UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgIGlmIChwcmlvcml0eVJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocHJpb3JpdHlSZXNwb25zZSkpIHtcclxuICAgICAgICAgICAgICAgcHJpb3JpdHlSZXNwb25zZSA9IFtwcmlvcml0eVJlc3BvbnNlXTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIHRoaXMucHJpb3JpdHlMaXN0ID0gcHJpb3JpdHlSZXNwb25zZTtcclxuICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgdGhpcy5wcmlvcml0eUxpc3QgPSBbXTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgfSk7ICovXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEFsbFJvbGVzKCkge1xyXG4gICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgdGVhbUlEOiB0aGlzLnRlYW1JZCxcclxuICAgICAgaXNBcHByb3ZlclRhc2s6IHRoaXMuaXNBcHByb3ZhbFRhc2ssXHJcbiAgICAgIGlzTWVtYmVyVGFzazogIXRoaXMuaXNBcHByb3ZhbFRhc2tcclxuICAgIH07XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VGVhbVJvbGVzKGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuVGVhbXMgJiYgcmVzcG9uc2UuVGVhbXMuTVBNX1RlYW1zICYmIHJlc3BvbnNlLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpIHtcclxuICAgICAgICAgICAgcmVzcG9uc2UgPSByZXNwb25zZS5UZWFtcy5NUE1fVGVhbXM7XHJcbiAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpKSB7XHJcbiAgICAgICAgICAgICAgcmVzcG9uc2UuTVBNX1RlYW1fUm9sZV9NYXBwaW5nID0gW3Jlc3BvbnNlLk1QTV9UZWFtX1JvbGVfTWFwcGluZ107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGV0IHJvbGVMaXN0O1xyXG4gICAgICAgICAgICByb2xlTGlzdCA9IHJlc3BvbnNlLk1QTV9UZWFtX1JvbGVfTWFwcGluZztcclxuICAgICAgICAgICAgY29uc3Qgcm9sZXMgPSBbXTtcclxuICAgICAgICAgICAgaWYgKHJvbGVMaXN0ICYmIHJvbGVMaXN0Lmxlbmd0aCAmJiByb2xlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgcm9sZUxpc3QubWFwKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgcm9sZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgIG5hbWU6IHJvbGVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICB2YWx1ZTogcm9sZS5ST0xFX0ROLFxyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcm9sZS5OQU1FLFxyXG4gICAgICAgICAgICAgICAgICBhcHBSb2xlczogcm9sZS5NUE1fQVBQX1JvbGVzXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlc09wdGlvbnMgPSByb2xlcztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVhbVJvbGVzT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0VXNlcnNCeVJvbGUoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgIGxldCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHt9O1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZFJvbGUpIHtcclxuICAgICAgICBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiB0aGlzLnNlbGVjdGVkQWxsb2NhdGlvbixcclxuICAgICAgICAgIHJvbGVETjogdGhpcy5zZWxlY3RlZEFsbG9jYXRpb24gPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUgPyB0aGlzLnNlbGVjdGVkUm9sZS52YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgdGVhbUlEOiB0aGlzLnNlbGVjdGVkQWxsb2NhdGlvbiA9PT0gVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUiA/IHRoaXMudGVhbUlkIDogJycsXHJcbiAgICAgICAgICBpc0FwcHJvdmVUYXNrOiB0aGlzLmlzQXBwcm92YWxUYXNrLFxyXG4gICAgICAgICAgaXNVcGxvYWRUYXNrOiAhdGhpcy5pc0FwcHJvdmFsVGFza1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogdGhpcy5zZWxlY3RlZEFsbG9jYXRpb24sXHJcbiAgICAgICAgICByb2xlRE46ICcnLFxyXG4gICAgICAgICAgdGVhbUlEOiB0aGlzLnRlYW1JZCxcclxuICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS51c2VycyAmJiByZXNwb25zZS51c2Vycy51c2VyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBbXTtcclxuICAgICAgICAgICAgaWYgKHVzZXJMaXN0ICYmIHVzZXJMaXN0Lmxlbmd0aCAmJiB1c2VyTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgdXNlckxpc3QuZm9yRWFjaCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gdXNlcnMuZmluZChlID0+IGUudmFsdWUgPT09IHVzZXIuZG4pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiB1c2VyLmRuLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gdXNlcnM7XHJcbiAgICAgICAgICAgIC8vIFRPRE86IGdldHVzZXIgZW50aXRpZXMgYnkgdXNlcklkXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbEFuZFN0YXR1c1R5cGUoUHJvamVjdENvbnN0YW50LkNBVEVHT1JZX0xFVkVMX1BST0pFQ1QsIFN0YXR1c1R5cGVzLklOSVRJQUwpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxDUlN0YXR1c1JlYXNvbnMoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLnByb2plY3RTdGF0dXMuTVBNX1N0YXR1c19SZWFzb24gPSByZXNwb25zZTtcclxuICAgIH0pO1xyXG5cclxuICB9XHJcbn1cclxuIl19