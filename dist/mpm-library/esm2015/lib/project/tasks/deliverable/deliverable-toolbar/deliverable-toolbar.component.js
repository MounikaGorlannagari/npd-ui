import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { DeliverableSortComponent } from '../deliverable-sort/deliverable-sort.component';
import { TaskService } from '../../task.service';
import { TaskConstants } from '../../task.constants';
import { AssetStatusConstants } from '../../../asset-card/asset_status_constants';
let DeliverableToolbarComponent = class DeliverableToolbarComponent {
    constructor(sharingService, taskService) {
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.resouceFilterChange = new EventEmitter();
        this.deliverableConfigChange = new EventEmitter();
        this.createNewDeliverableHandler = new EventEmitter();
        this.showTaskViewButton = true;
        this.viewByResources = new FormControl();
        this.taskConstants = TaskConstants;
        this.assetStatusConstants = AssetStatusConstants;
        this.disableAction = true;
    }
    createNewDeliverable() {
        this.createNewDeliverableHandler.next(this.deliverableConfig);
    }
    selectResource(resourceName) {
    }
    refresh() {
        this.deliverableConfigChange.next(this.deliverableConfig);
    }
    changeView() {
        if (this.deliverableConfig.isTaskListView) {
            this.deliverableConfig.isTaskListView = false;
            localStorage.setItem('isTaskListView', 'false');
        }
        else {
            this.deliverableConfig.isTaskListView = true;
            localStorage.setItem('isTaskListView', 'true');
        }
        this.refresh();
    }
    onSortChange(eventData) {
        if (eventData && eventData.option) {
            this.deliverableConfig.sortFieldName = eventData.option.name;
            this.deliverableConfig.sortOrder = eventData.sortType;
        }
        this.deliverableConfigChange.next(this.deliverableConfig);
    }
    refreshChild(deliverableConfig) {
        this.deliverableConfig = deliverableConfig;
        if (this.deliverableSortComponent) {
            this.deliverableSortComponent.refreshData(this.deliverableConfig);
        }
    }
    onResourceSelect() {
        this.deliverableConfig.selectedResources = this.viewByResources.value;
        this.deliverableConfigChange.next(this.deliverableConfig);
    }
    onResourceRemoved(resource) {
        const index = this.deliverableConfig.selectedResources.indexOf(resource);
        this.deliverableConfig.selectedResources.splice(index, 1);
        this.isViewByResourceFilterEnabled = false;
        this.viewByResources.patchValue(this.deliverableConfig.selectedResources);
        this.deliverableConfigChange.next(this.deliverableConfig);
        this.resouceFilterChange.next(this.isViewByResourceFilterEnabled);
    }
    ngOnInit() {
        const currentUSerID = this.sharingService.getCurrentUserItemID();
        if (currentUSerID && this.deliverableConfig.selectedProject &&
            currentUSerID === this.deliverableConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.deliverableConfig.isProjectOwner = true;
        }
        this.isProjectCompleted = false;
        this.isOnHoldProject = (this.deliverableConfig.selectedProject && this.deliverableConfig.selectedProject.IS_ON_HOLD === 'true') ? true : false;
        if (this.deliverableConfig.isNewDeliverable) {
            this.createNewDeliverable();
        }
    }
};
DeliverableToolbarComponent.ctorParameters = () => [
    { type: SharingService },
    { type: TaskService }
];
__decorate([
    Input()
], DeliverableToolbarComponent.prototype, "deliverableConfig", void 0);
__decorate([
    Input()
], DeliverableToolbarComponent.prototype, "isViewByResourceFilterEnabled", void 0);
__decorate([
    Output()
], DeliverableToolbarComponent.prototype, "resouceFilterChange", void 0);
__decorate([
    Output()
], DeliverableToolbarComponent.prototype, "deliverableConfigChange", void 0);
__decorate([
    Output()
], DeliverableToolbarComponent.prototype, "createNewDeliverableHandler", void 0);
__decorate([
    Input()
], DeliverableToolbarComponent.prototype, "enableAction", void 0);
__decorate([
    Input()
], DeliverableToolbarComponent.prototype, "delCount", void 0);
__decorate([
    Input()
], DeliverableToolbarComponent.prototype, "crBulkResponse", void 0);
__decorate([
    ViewChild(DeliverableSortComponent)
], DeliverableToolbarComponent.prototype, "deliverableSortComponent", void 0);
DeliverableToolbarComponent = __decorate([
    Component({
        selector: 'mpm-deliverable-toolbar',
        template: "<mat-toolbar class=\"app-task-toolbar\">\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-row-item task-info-holder\">\r\n            <span class=\"flex-row-item deliverable-count\">Deliverables\r\n                ({{deliverableConfig.deliverableTotalCount ? deliverableConfig.deliverableTotalCount : '0'}})</span>\r\n            <span class=\"flex-row-item task-details\"\r\n                *ngIf=\"deliverableConfig.selectedTask && deliverableConfig.selectedTask.NAME\">\r\n                <mat-icon>assignment_turned_in</mat-icon>\r\n                <div class=\"task-name\" matTooltip=\"Task Name: {{deliverableConfig.selectedTask.NAME}}\">\r\n                    {{deliverableConfig.selectedTask.NAME}}</div>\r\n            </span>\r\n            <button mat-icon-button (click)=\"refresh()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <!--   <span *ngIf=\"delCount\" matTooltip=\"Selected Deliverable\"> ({{delCount}})</span> -->\r\n        <!--   <span>\r\n            <button [disabled]=\"!enableAction\" class=\"new-project-btn\" mat-raised-button  matTooltip=\"Actions\"\r\n                [matMenuTriggerFor]=\"actionTypeMenu\">\r\n                <span> Actions</span>\r\n                <mat-icon>arrow_drop_down</mat-icon>\r\n            </button>\r\n        </span>\r\n        <mat-menu #actionTypeMenu=\"matMenu\">\r\n            <button mat-menu-item matTooltip=\"Approved\">\r\n                <span>Approved</span>\r\n            </button>\r\n            <button mat-menu-item matTooltip=\"Rejected\">\r\n                <span>Rejected</span>\r\n            </button>\r\n        </mat-menu> -->\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mpm-deliverable-sort [sortOptions]=\"deliverableConfig.deliverableSortOptions\"\r\n                [selectedOption]=\"deliverableConfig.selectedSortOption\" (onSortChange)=\"onSortChange($event)\">\r\n            </mpm-deliverable-sort>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mat-form-field appearance=\"outline\" class=\"resource-select\">\r\n                <mat-label>View by resource</mat-label>\r\n                <mat-select #resourceSelect [formControl]=\"viewByResources\" multiple\r\n                    [disabled]=\"!deliverableConfig.deliverableResources.length\">\r\n                    <mat-select-trigger>\r\n                        {{viewByResources.value ? viewByResources.value[0] : ''}}\r\n                        <span *ngIf=\"viewByResources.value?.length > 1\" class=\"example-additional-selection\">\r\n                            (+{{viewByResources.value.length - 1}}\r\n                            {{viewByResources.value?.length === 2 ? 'other' : 'others'}})\r\n                        </span>\r\n                    </mat-select-trigger>\r\n                    <mat-option *ngFor=\"let resource of deliverableConfig.deliverableResources\" [value]=\"resource\">\r\n                        {{resource}}\r\n                    </mat-option>\r\n\r\n                    <button class=\"resource-apply-btn\" (click)=\"onResourceSelect(); resourceSelect.close()\"\r\n                        mat-button>Apply</button>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-stroked-button (click)=\"createNewDeliverable()\"\r\n                [disabled]=\"!deliverableConfig.isProjectOwner || isProjectCompleted || (deliverableConfig.selectedTask && deliverableConfig.selectedTask.TASK_TYPE === taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE) || deliverableConfig.isReadOnly ||\r\n            (deliverableConfig.selectedTask && deliverableConfig.selectedTask.statusDetails && deliverableConfig.selectedTask.statusDetails.NAME === assetStatusConstants.APPROVED) || (deliverableConfig.selectedTask && deliverableConfig.selectedTask.IS_APPROVAL_TASK === 'true')\">\r\n                <mat-icon>add</mat-icon>\r\n                <span> New Deliverable</span>\r\n            </button>\r\n        </div>\r\n\r\n        <span class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-icon-button>\r\n                <mat-icon class=\"dashboard-action-btn\">settings\r\n                </mat-icon>\r\n            </button>\r\n        </span>\r\n    </mat-toolbar-row>\r\n\r\n    <mat-toolbar-row *ngIf=\"deliverableConfig.selectedResources && deliverableConfig.selectedResources.length > 0\">\r\n        <span class=\"flex-row\">\r\n            <span class=\"resource-heading\">Resources: </span>\r\n            <mat-chip-list aria-label=\"resource selection\">\r\n                <mat-chip [removable]=\"true\" (removed)=\"onResourceRemoved(resource)\"\r\n                    *ngFor=\"let resource of deliverableConfig.selectedResources\">{{resource}}\r\n                    <mat-icon matChipRemove>cancel</mat-icon>\r\n                </mat-chip>\r\n            </mat-chip-list>\r\n        </span>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>",
        styles: ["mat-toolbar.app-task-toolbar{background-color:transparent}.app-task-toolbar .spacer{flex:1 1 auto}.flex-row-item button{margin:5px}.resources-row,.task-resource-icon{font-size:14px}.resourceCheckbox{margin-right:5px;margin-left:5px}.task-icon-btn{margin-top:18px}.task-info-holder{flex-grow:0;align-items:center}.task-info-holder .deliverable-count{padding:8px}.task-info-holder .task-details{align-items:center;max-width:150px}.task-info-holder .task-details .task-name{text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap}.align-items-center{align-items:center}.right{justify-content:flex-end}mat-form-field.resource-select{font-size:11px}::ng-deep .resource-select .mat-form-field-wrapper{padding:0}.flex-grow-0{flex-grow:0}.resources-heading{font-size:16px;margin-right:8px}.resource-apply-btn{width:100%}"]
    })
], DeliverableToolbarComponent);
export { DeliverableToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLXRvb2xiYXIvZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUEyQyxNQUFNLGVBQWUsQ0FBQztBQUNuSSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ2hGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDckQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFRbEYsSUFBYSwyQkFBMkIsR0FBeEMsTUFBYSwyQkFBMkI7SUEyQnBDLFlBQ1csY0FBOEIsRUFDOUIsV0FBd0I7UUFEeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBeEJ6Qix3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbEQsZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQVFoRSx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFFMUIsb0JBQWUsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBRXBDLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBQzlCLHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBQzVDLGtCQUFhLEdBQUcsSUFBSSxDQUFDO0lBU2pCLENBQUM7SUFFTCxvQkFBb0I7UUFDaEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUNsRSxDQUFDO0lBRUQsY0FBYyxDQUFDLFlBQVk7SUFFM0IsQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxVQUFVO1FBQ04sSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzlDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsT0FBTyxDQUFDLENBQUM7U0FDbkQ7YUFBTTtZQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzdDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDbEQ7UUFDRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELFlBQVksQ0FBQyxTQUFTO1FBQ2xCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUM3RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUM7U0FDekQ7UUFDRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxZQUFZLENBQUMsaUJBQWlCO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUMzQyxJQUFJLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUMvQixJQUFJLENBQUMsd0JBQXdCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ3JFO0lBQ0wsQ0FBQztJQUVELGdCQUFnQjtRQUNaLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQztRQUN0RSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxRQUFRO1FBQ3RCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLDZCQUE2QixHQUFDLEtBQUssQ0FBQztRQUV6QyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELFFBQVE7UUFDSixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDakUsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWU7WUFDdkQsYUFBYSxLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQy9GLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDL0ksSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUU7WUFDekMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FDL0I7SUFDTCxDQUFDO0NBRUosQ0FBQTs7WUF0RThCLGNBQWM7WUFDakIsV0FBVzs7QUEzQjFCO0lBQVIsS0FBSyxFQUFFO3NFQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTtrRkFBK0I7QUFFN0I7SUFBVCxNQUFNLEVBQUU7d0VBQStDO0FBQzlDO0lBQVQsTUFBTSxFQUFFOzRFQUFtRDtBQUNsRDtJQUFULE1BQU0sRUFBRTtnRkFBdUQ7QUFFdkQ7SUFBUixLQUFLLEVBQUU7aUVBQWM7QUFDYjtJQUFSLEtBQUssRUFBRTs2REFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO21FQUFnQjtBQWNhO0lBQXBDLFNBQVMsQ0FBQyx3QkFBd0IsQ0FBQzs2RUFBb0Q7QUF6Qi9FLDJCQUEyQjtJQU52QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUseUJBQXlCO1FBQ25DLHVvS0FBbUQ7O0tBRXRELENBQUM7R0FFVywyQkFBMkIsQ0FrR3ZDO1NBbEdZLDJCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzLCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQgfSBmcm9tICcuLi9kZWxpdmVyYWJsZS1zb3J0L2RlbGl2ZXJhYmxlLXNvcnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi8uLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBc3NldFN0YXR1c0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL2Fzc2V0LWNhcmQvYXNzZXRfc3RhdHVzX2NvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWRlbGl2ZXJhYmxlLXRvb2xiYXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RlbGl2ZXJhYmxlLXRvb2xiYXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVUb29sYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBkZWxpdmVyYWJsZUNvbmZpZztcclxuICAgIEBJbnB1dCgpIGlzVmlld0J5UmVzb3VyY2VGaWx0ZXJFbmFibGVkO1xyXG5cclxuICAgIEBPdXRwdXQoKSByZXNvdWNlRmlsdGVyQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZGVsaXZlcmFibGVDb25maWdDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjcmVhdGVOZXdEZWxpdmVyYWJsZUhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBASW5wdXQoKSBlbmFibGVBY3Rpb247XHJcbiAgICBASW5wdXQoKSBkZWxDb3VudDtcclxuICAgIEBJbnB1dCgpIGNyQnVsa1Jlc3BvbnNlO1xyXG5cclxuXHJcbiAgICBpc1Byb2plY3RDb21wbGV0ZWQ6IGJvb2xlYW47XHJcbiAgICBzaG93VGFza1ZpZXdCdXR0b24gPSB0cnVlO1xyXG4gICAgc2VsZWN0ZWRPcHRpb247XHJcbiAgICB2aWV3QnlSZXNvdXJjZXMgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuXHJcbiAgICB0YXNrQ29uc3RhbnRzID0gVGFza0NvbnN0YW50cztcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzID0gQXNzZXRTdGF0dXNDb25zdGFudHM7XHJcbiAgICBkaXNhYmxlQWN0aW9uID0gdHJ1ZTtcclxuXHJcbiAgICBpc09uSG9sZFByb2plY3Q6IGJvb2xlYW47XHJcblxyXG4gICAgQFZpZXdDaGlsZChEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQpIGRlbGl2ZXJhYmxlU29ydENvbXBvbmVudDogRGVsaXZlcmFibGVTb3J0Q29tcG9uZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjcmVhdGVOZXdEZWxpdmVyYWJsZSgpIHtcclxuICAgICAgICB0aGlzLmNyZWF0ZU5ld0RlbGl2ZXJhYmxlSGFuZGxlci5uZXh0KHRoaXMuZGVsaXZlcmFibGVDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdFJlc291cmNlKHJlc291cmNlTmFtZSkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWdDaGFuZ2UubmV4dCh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VWaWV3KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmlzVGFza0xpc3RWaWV3KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWcuaXNUYXNrTGlzdFZpZXcgPSBmYWxzZTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2lzVGFza0xpc3RWaWV3JywgJ2ZhbHNlJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5pc1Rhc2tMaXN0VmlldyA9IHRydWU7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdpc1Rhc2tMaXN0VmlldycsICd0cnVlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU29ydENoYW5nZShldmVudERhdGEpIHtcclxuICAgICAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5vcHRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zb3J0RmllbGROYW1lID0gZXZlbnREYXRhLm9wdGlvbi5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNvcnRPcmRlciA9IGV2ZW50RGF0YS5zb3J0VHlwZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZ0NoYW5nZS5uZXh0KHRoaXMuZGVsaXZlcmFibGVDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hDaGlsZChkZWxpdmVyYWJsZUNvbmZpZykge1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWcgPSBkZWxpdmVyYWJsZUNvbmZpZztcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZVNvcnRDb21wb25lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVNvcnRDb21wb25lbnQucmVmcmVzaERhdGEodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmVzb3VyY2VTZWxlY3QoKSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFJlc291cmNlcyA9IHRoaXMudmlld0J5UmVzb3VyY2VzLnZhbHVlO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWdDaGFuZ2UubmV4dCh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJlc291cmNlUmVtb3ZlZChyZXNvdXJjZSkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFJlc291cmNlcy5pbmRleE9mKHJlc291cmNlKTtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgdGhpcy5pc1ZpZXdCeVJlc291cmNlRmlsdGVyRW5hYmxlZD1mYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy52aWV3QnlSZXNvdXJjZXMucGF0Y2hWYWx1ZSh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzKTtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnQ2hhbmdlLm5leHQodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyk7XHJcbiAgICAgICAgdGhpcy5yZXNvdWNlRmlsdGVyQ2hhbmdlLm5leHQodGhpcy5pc1ZpZXdCeVJlc291cmNlRmlsdGVyRW5hYmxlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFVTZXJJRCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTtcclxuICAgICAgICBpZiAoY3VycmVudFVTZXJJRCAmJiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICBjdXJyZW50VVNlcklEID09PSB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNQcm9qZWN0Q29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5pc09uSG9sZFByb2plY3QgPSAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFByb2plY3QuSVNfT05fSE9MRCA9PT0gJ3RydWUnKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5pc05ld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlTmV3RGVsaXZlcmFibGUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==