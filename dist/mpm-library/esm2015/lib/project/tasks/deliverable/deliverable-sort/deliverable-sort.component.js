import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../../../../lib/mpm-utils/services/sharing.service';
import { SortConstants } from '../../../../../lib/shared/constants/sort.constants';
import { ApplicationConfigConstants } from '../../../../../lib/shared/constants/application.config.constants';
let DeliverableSortComponent = class DeliverableSortComponent {
    constructor(sharingService) {
        this.sharingService = sharingService;
        this.onSortChange = new EventEmitter();
    }
    selectSorting() {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.onSortChange.next(this.selectedOption);
    }
    onSelectionChange(event, fields) {
        if (event && event.currentTarget.value) {
            fields.map(option => {
                if (option.name === event.currentTarget.value) {
                    this.selectedOption.option = option;
                }
            });
        }
        this.onSortChange.next(this.selectedOption);
    }
    refreshData(taskConfig) {
        if (!this.selectedOption) {
            if (this.sortOptions) {
                // this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    }
    ngOnInit() {
        this.appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                // this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    }
};
DeliverableSortComponent.ctorParameters = () => [
    { type: SharingService }
];
__decorate([
    Input()
], DeliverableSortComponent.prototype, "sortOptions", void 0);
__decorate([
    Input()
], DeliverableSortComponent.prototype, "selectedOption", void 0);
__decorate([
    Output()
], DeliverableSortComponent.prototype, "onSortChange", void 0);
DeliverableSortComponent = __decorate([
    Component({
        selector: 'mpm-deliverable-sort',
        template: "<div class=\"flex-row-item justify-flex-end\" *ngIf=\"sortOptions && sortOptions.length > 0\">\r\n    <button class=\"sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption.option.displayName}}\"\r\n        [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n        <span>Sort by: {{selectedOption.option.displayName}}</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n    </button>\r\n    <button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n        mat-icon-button>\r\n        <mat-icon *ngIf=\"selectedOption.sortType === 'desc'\">arrow_downward</mat-icon>\r\n        <mat-icon *ngIf=\"selectedOption.sortType === 'asc'\">arrow_upward</mat-icon>\r\n    </button>\r\n    <mat-menu #sortOrderListMenu=\"matMenu\">\r\n        <span *ngFor=\"let fieldOption of sortOptions\">\r\n            <button *ngIf=\"selectedOption.option.name !== fieldOption.name\" mat-menu-item\r\n                matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n                [value]=\"fieldOption.name\">\r\n                <span>{{fieldOption.displayName}}</span>\r\n            </button>\r\n        </span>\r\n    </mat-menu>\r\n</div>",
        styles: [".flex-row-item button{margin:5px}"]
    })
], DeliverableSortComponent);
export { DeliverableSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLXNvcnQvZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNuRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQVM5RyxJQUFhLHdCQUF3QixHQUFyQyxNQUFhLHdCQUF3QjtJQU9qQyxZQUNXLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUwvQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFNN0MsQ0FBQztJQUNMLGFBQWE7UUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDckcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsTUFBTTtRQUMzQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtZQUNwQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoQixJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztpQkFDdkM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxXQUFXLENBQUMsVUFBVTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ25CLDBFQUEwRTtnQkFDekUsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMzSztpQkFBTTtnQkFDSCx3REFBd0Q7Z0JBQ3hELElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMxSjtTQUNKO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNuQiwwRUFBMEU7Z0JBQzFFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUs7aUJBQU07Z0JBQ0gsd0RBQXdEO2dCQUN4RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUo7U0FDSjtJQUNMLENBQUM7Q0FDSixDQUFBOztZQTNDOEIsY0FBYzs7QUFQaEM7SUFBUixLQUFLLEVBQUU7NkRBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTtnRUFBZ0I7QUFDZDtJQUFULE1BQU0sRUFBRTs4REFBd0M7QUFIeEMsd0JBQXdCO0lBTnBDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxzQkFBc0I7UUFDaEMsNnVDQUFnRDs7S0FFbkQsQ0FBQztHQUVXLHdCQUF3QixDQW1EcEM7U0FuRFksd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU29ydENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9zaGFyZWQvY29uc3RhbnRzL3NvcnQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWRlbGl2ZXJhYmxlLXNvcnQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RlbGl2ZXJhYmxlLXNvcnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVTb3J0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIHNvcnRPcHRpb25zO1xyXG4gICAgQElucHV0KCkgc2VsZWN0ZWRPcHRpb247XHJcbiAgICBAT3V0cHV0KCkgb25Tb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgYXBwQ29uZmlnOiBhbnk7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcbiAgICBzZWxlY3RTb3J0aW5nKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPSAodGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9PT0gU29ydENvbnN0YW50cy5BU0MpID8gJ2Rlc2MnIDogJ2FzYyc7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9IHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGU7XHJcbiAgICAgICAgdGhpcy5vblNvcnRDaGFuZ2UubmV4dCh0aGlzLnNlbGVjdGVkT3B0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBvblNlbGVjdGlvbkNoYW5nZShldmVudCwgZmllbGRzKSB7XHJcbiAgICAgICAgaWYgKGV2ZW50ICYmIGV2ZW50LmN1cnJlbnRUYXJnZXQudmFsdWUpIHtcclxuICAgICAgICAgICAgZmllbGRzLm1hcChvcHRpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKG9wdGlvbi5uYW1lID09PSBldmVudC5jdXJyZW50VGFyZ2V0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5vcHRpb24gPSBvcHRpb247XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9uU29ydENoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hEYXRhKHRhc2tDb25maWcpIHtcclxuICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc29ydE9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgLy8gdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc29ydE9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgLy8gdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSA9PT0gJ0FTQycgPyAnYXNjJyA6ICdkZXNjJ307XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiAnYXNjJyB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB7fSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==