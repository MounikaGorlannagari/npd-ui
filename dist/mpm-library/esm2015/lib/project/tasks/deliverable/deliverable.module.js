import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliverableToolbarComponent } from './deliverable-toolbar/deliverable-toolbar.component';
import { MaterialModule } from '../../../material.module';
import { DeliverableSwinLaneComponent } from './deliverable-swin-lane/deliverable-swin-lane.component';
import { DeliverableCardComponent } from './deliverable-card/deliverable-card.component';
import { DeliverableSortComponent } from './deliverable-sort/deliverable-sort.component';
import { MpmLibraryModule } from '../../../mpm-library.module';
import { BulkActionsComponent } from './bulk-actions/bulk-actions.component';
let DeliverableModule = class DeliverableModule {
};
DeliverableModule = __decorate([
    NgModule({
        declarations: [
            DeliverableToolbarComponent,
            DeliverableSwinLaneComponent,
            DeliverableCardComponent,
            DeliverableSortComponent,
            BulkActionsComponent
        ],
        imports: [
            CommonModule,
            MaterialModule,
            MpmLibraryModule
        ],
        exports: [
            DeliverableToolbarComponent,
            DeliverableSwinLaneComponent,
            DeliverableCardComponent,
            DeliverableSortComponent,
            BulkActionsComponent
        ]
    })
], DeliverableModule);
export { DeliverableModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUN2RyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQXdCN0UsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7Q0FBSSxDQUFBO0FBQXJCLGlCQUFpQjtJQXRCN0IsUUFBUSxDQUFDO1FBQ04sWUFBWSxFQUFFO1lBQ1YsMkJBQTJCO1lBQzNCLDRCQUE0QjtZQUM1Qix3QkFBd0I7WUFDeEIsd0JBQXdCO1lBQ3hCLG9CQUFvQjtTQUN2QjtRQUNELE9BQU8sRUFBRTtZQUNMLFlBQVk7WUFDWixjQUFjO1lBQ2QsZ0JBQWdCO1NBQ25CO1FBQ0QsT0FBTyxFQUFFO1lBQ0wsMkJBQTJCO1lBQzNCLDRCQUE0QjtZQUM1Qix3QkFBd0I7WUFDeEIsd0JBQXdCO1lBQ3hCLG9CQUFvQjtTQUN2QjtLQUNKLENBQUM7R0FFVyxpQkFBaUIsQ0FBSTtTQUFyQixpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVRvb2xiYXJDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLXRvb2xiYXIvZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU3dpbkxhbmVDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLXN3aW4tbGFuZS9kZWxpdmVyYWJsZS1zd2luLWxhbmUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVDYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9kZWxpdmVyYWJsZS1jYXJkL2RlbGl2ZXJhYmxlLWNhcmQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTb3J0Q29tcG9uZW50IH0gZnJvbSAnLi9kZWxpdmVyYWJsZS1zb3J0L2RlbGl2ZXJhYmxlLXNvcnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTXBtTGlicmFyeU1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL21wbS1saWJyYXJ5Lm1vZHVsZSc7XHJcbmltcG9ydCB7IEJ1bGtBY3Rpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9idWxrLWFjdGlvbnMvYnVsay1hY3Rpb25zLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgRGVsaXZlcmFibGVUb29sYmFyQ29tcG9uZW50LFxyXG4gICAgICAgIERlbGl2ZXJhYmxlU3dpbkxhbmVDb21wb25lbnQsXHJcbiAgICAgICAgRGVsaXZlcmFibGVDYXJkQ29tcG9uZW50LFxyXG4gICAgICAgIERlbGl2ZXJhYmxlU29ydENvbXBvbmVudCxcclxuICAgICAgICBCdWxrQWN0aW9uc0NvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgTXBtTGlicmFyeU1vZHVsZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBEZWxpdmVyYWJsZVRvb2xiYXJDb21wb25lbnQsXHJcbiAgICAgICAgRGVsaXZlcmFibGVTd2luTGFuZUNvbXBvbmVudCxcclxuICAgICAgICBEZWxpdmVyYWJsZUNhcmRDb21wb25lbnQsXHJcbiAgICAgICAgRGVsaXZlcmFibGVTb3J0Q29tcG9uZW50LFxyXG4gICAgICAgIEJ1bGtBY3Rpb25zQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVNb2R1bGUgeyB9XHJcbiJdfQ==