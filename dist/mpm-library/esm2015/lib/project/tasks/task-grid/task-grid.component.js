import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { SwimLaneComponent } from '../swim-lane/swim-lane.component';
import { TaskListViewComponent } from '../task-list-view/task-list-view.component';
let TaskGridComponent = class TaskGridComponent {
    constructor() {
        this.refreshTaskGrid = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
    }
    refreshChild(taskConfig) {
        this.taskConfig = taskConfig;
        if (this.taskConfig.isTaskListView) {
            this.taskListViewComponent.refreshData(this.taskConfig);
        }
        else {
            this.swimLaneComponent.initialiseSwimList();
        }
    }
    refreshSwimLine(event) {
        if (event) {
            this.refreshTaskGrid.next(true);
        }
    }
    refreshTaskList(event) {
        this.refreshTaskGrid.next(event);
    }
    editTask(eventData) {
        this.editTaskHandler.next(eventData);
    }
    openTaskDetails(eventData) {
        this.taskDetailsHandler.next(eventData);
    }
    onSortChange(eventData) {
        this.sortChange.emit(eventData);
    }
    ngOnInit() {
        if (this.taskConfig) {
        }
    }
};
__decorate([
    Input()
], TaskGridComponent.prototype, "taskConfig", void 0);
__decorate([
    Output()
], TaskGridComponent.prototype, "refreshTaskGrid", void 0);
__decorate([
    Output()
], TaskGridComponent.prototype, "editTaskHandler", void 0);
__decorate([
    Output()
], TaskGridComponent.prototype, "taskDetailsHandler", void 0);
__decorate([
    Output()
], TaskGridComponent.prototype, "sortChange", void 0);
__decorate([
    ViewChild(SwimLaneComponent)
], TaskGridComponent.prototype, "swimLaneComponent", void 0);
__decorate([
    ViewChild(TaskListViewComponent)
], TaskGridComponent.prototype, "taskListViewComponent", void 0);
TaskGridComponent = __decorate([
    Component({
        selector: 'mpm-task-grid',
        template: "<mpm-swim-lane *ngIf=\"!taskConfig.isTaskListView\" [taskConfig]=\"taskConfig\" (refreshSwimLine)=\"refreshSwimLine($event)\"\r\n    (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n</mpm-swim-lane>\r\n<mpm-task-list-view *ngIf=\"taskConfig.isTaskListView\" [taskConfig]=\"taskConfig\"\r\n    (editTaskHandler)=\"editTask($event)\"  (taskDetailsHandler)=\"openTaskDetails($event)\" (sortChange)=\"onSortChange($event)\"\r\n    (refreshTaskListData)=\"refreshTaskList($event)\">\r\n</mpm-task-list-view>",
        styles: ["app-swim-lane{width:100%}"]
    })
], TaskGridComponent);
export { TaskGridComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1ncmlkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvdGFzay1ncmlkL3Rhc2stZ3JpZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUW5GLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBVzFCO1FBUlUsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxQyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzdDLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBSy9CLENBQUM7SUFFakIsWUFBWSxDQUFDLFVBQVU7UUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDN0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtZQUNoQyxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUMzRDthQUFNO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQsZUFBZSxDQUFDLEtBQUs7UUFDakIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7SUFFRCxlQUFlLENBQUMsS0FBSztRQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUgsUUFBUSxDQUFDLFNBQVM7UUFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsZUFBZSxDQUFDLFNBQVM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsWUFBWSxDQUFDLFNBQVM7UUFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7U0FFcEI7SUFDTCxDQUFDO0NBRUosQ0FBQTtBQWhEWTtJQUFSLEtBQUssRUFBRTtxREFBWTtBQUNWO0lBQVQsTUFBTSxFQUFFOzBEQUEyQztBQUMxQztJQUFULE1BQU0sRUFBRTswREFBMkM7QUFDMUM7SUFBVCxNQUFNLEVBQUU7NkRBQThDO0FBQzdDO0lBQVQsTUFBTSxFQUFFO3FEQUFzQztBQUVqQjtJQUE3QixTQUFTLENBQUMsaUJBQWlCLENBQUM7NERBQXNDO0FBQ2pDO0lBQWpDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQztnRUFBOEM7QUFUdEUsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLDZpQkFBeUM7O0tBRTVDLENBQUM7R0FFVyxpQkFBaUIsQ0FrRDdCO1NBbERZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTd2ltTGFuZUNvbXBvbmVudCB9IGZyb20gJy4uL3N3aW0tbGFuZS9zd2ltLWxhbmUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFza0xpc3RWaWV3Q29tcG9uZW50IH0gZnJvbSAnLi4vdGFzay1saXN0LXZpZXcvdGFzay1saXN0LXZpZXcuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tdGFzay1ncmlkJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90YXNrLWdyaWQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdGFzay1ncmlkLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBUYXNrR3JpZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgdGFza0NvbmZpZztcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoVGFza0dyaWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0VGFza0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB0YXNrRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQFZpZXdDaGlsZChTd2ltTGFuZUNvbXBvbmVudCkgc3dpbUxhbmVDb21wb25lbnQ6IFN3aW1MYW5lQ29tcG9uZW50O1xyXG4gICAgQFZpZXdDaGlsZChUYXNrTGlzdFZpZXdDb21wb25lbnQpIHRhc2tMaXN0Vmlld0NvbXBvbmVudDogVGFza0xpc3RWaWV3Q29tcG9uZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgcmVmcmVzaENoaWxkKHRhc2tDb25maWcpIHtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcgPSB0YXNrQ29uZmlnO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrTGlzdFZpZXdDb21wb25lbnQucmVmcmVzaERhdGEodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnN3aW1MYW5lQ29tcG9uZW50LmluaXRpYWxpc2VTd2ltTGlzdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoU3dpbUxpbmUoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoVGFza0dyaWQubmV4dCh0cnVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaFRhc2tMaXN0KGV2ZW50KXtcclxuICAgICAgICB0aGlzLnJlZnJlc2hUYXNrR3JpZC5uZXh0KGV2ZW50KTtcclxuICAgICAgfVxyXG5cclxuICAgIGVkaXRUYXNrKGV2ZW50RGF0YSkge1xyXG4gICAgICAgIHRoaXMuZWRpdFRhc2tIYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuVGFza0RldGFpbHMoZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy50YXNrRGV0YWlsc0hhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU29ydENoYW5nZShldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLnNvcnRDaGFuZ2UuZW1pdChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcpIHtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=