import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TaskService } from '../task.service';
import { TaskConstants } from '../task.constants';
import { SharingService } from '../../../../lib/mpm-utils/services/sharing.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { AssetStatusConstants } from '../../asset-card/asset_status_constants';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { ViewConfigService } from '../../../shared/services/view-config.service';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { SearchConfigConstants } from '../../../mpm-utils/objects/SearchConfigConstants';
import { ProjectConstant } from '../../project-overview/project.constants';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
let TaskCardViewComponent = class TaskCardViewComponent {
    constructor(taskService, sharingService, otmmMetadataService, formatToLocalePipe, utilService, viewConfigService, fieldConfigService, loaderService, notificationService, dialog) {
        this.taskService = taskService;
        this.sharingService = sharingService;
        this.otmmMetadataService = otmmMetadataService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.utilService = utilService;
        this.viewConfigService = viewConfigService;
        this.fieldConfigService = fieldConfigService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.dialog = dialog;
        this.refreshTask = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.taskConstants = TaskConstants;
        this.assetStatusConstants = AssetStatusConstants;
        this.isProjectOwner = false;
        this.isFinalStatus = false;
        this.isCancelledTask = false;
        this.isFinalTask = false;
        this.copyToolTip = 'Click to copy task name';
        this.view = 'ProjectManagement';
        this.displayableMPMFields = [];
        this.MPMFeildConstants = MPMFieldConstants.MPM_TASK_FIELDS;
        this.priorityObj = {
            color: '',
            icon: '',
            tooltip: ''
        };
        this.isNormalTask = false;
        this.forceDeletion = false;
        this.affectedTask = [];
    }
    openTaskDetails(isNewDeliverable) {
        this.taskConfig.isNewDeliverable = isNewDeliverable;
        const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.taskDetailsHandler.emit(this.taskConfig);
            // this.refresh();
        }
    }
    openCustomWorkflow() {
        const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId) {
            const selectedTask = this.taskConfig.taskList.find(task => task.ITEM_ID === taskItemId);
            this.customWorkflowHandler.next(selectedTask);
        }
    }
    editTask() {
        const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        const isApprovalTask = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_IS_APPROVAL);
        this.taskConfig.isApprovalTask = isApprovalTask === 'true' ? true : false;
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        }
    }
    deleteTask(isForceDelete) {
        const taskId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ID);
        let msg;
        const isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
        if (isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        }
        else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }
        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(approvalTask => {
            if (approvalTask.tuple != undefined) {
                this.forceDeletion = true;
                this.affectedTask = [];
                let tasks;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                }
                else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(task => {
                    if (task.old.TaskView.isDeleted === 'false') {
                        this.affectedTask.push(task.old.TaskView.name);
                    }
                });
            }
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    //name: this.isTemplate ? [] : this.affectedTask,
                    name: this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    if (isForceDelete) {
                        this.taskService.forceDeleteTask(taskId, isCustomWorkflow).subscribe(response => {
                            this.notificationService.success('Task has been deleted successfully');
                            this.refresh();
                        }, error => {
                            this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                    else {
                        this.taskService.softDeleteTask(taskId, isCustomWorkflow).subscribe(response => {
                            this.notificationService.success('Task has been deleted successfully');
                            this.refresh();
                        }, error => {
                            this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                }
            });
        });
    }
    openComment() {
    }
    openRequestDetails() {
    }
    showReminder() {
    }
    refresh() {
        this.refreshTask.next(true);
    }
    triggerActionRule(actionId) {
        const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            const taskId = taskItemId.split('.')[1];
            this.loaderService.show();
            this.taskService.triggerRuleOnAction(taskId, actionId).subscribe(response => {
                this.loaderService.hide();
                this.notificationService.success('Triggering action has been initiated');
            });
        }
    }
    refreshCurrentTask() {
        this.refreshTask.next(this.taskConfig);
    }
    getProperty(taskData, mapperName) {
        const displayColum = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.TASK);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, taskData);
    }
    converToLocalDate(deliverableData, propertyId) {
        return this.formatToLocalePipe.transform(this.taskService.converToLocalDate(deliverableData, propertyId));
    }
    getPriority(taskData, mapperName) {
        const priorityObj = this.utilService.getPriorityIcon(this.getProperty(taskData, mapperName), MPM_LEVELS.TASK);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    }
    /**
     * @since MPMV3-946
     * @param taskType
     */
    getTaskTypeIcon(taskType) {
        return this.taskService.getTaskIconByTaskType(taskType);
    }
    /**
     * @since MPMV3-1141
     * @param taskType
     */
    handleActionsOnTaskView() {
        let currTaskValue = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_STATUS);
        const currTaskStatusType = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_STATUS_TYPE);
        if (this.assetStatusConstants.REJECTED === currTaskValue || this.assetStatusConstants.APPROVED === currTaskValue ||
            this.assetStatusConstants.CANCELLED === currTaskValue || this.assetStatusConstants.COMPLETED === currTaskValue) {
            this.isCancelledTask = true;
        }
        if (this.projectOwner === this.sharingService.getCurrentUserID() || this.projectOwner === this.sharingService.getCurrentUserItemID()) {
            this.isProjectOwner = true;
        }
        currTaskValue = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_TYPE);
        if (currTaskValue === this.taskConstants.TASK_TYPE.NORMAL_TASK) {
            this.isNormalTask = true;
        }
        if (currTaskStatusType === StatusTypes.FINAL_APPROVED || currTaskStatusType === StatusTypes.FINAL_REJECTED || currTaskStatusType === StatusTypes.FINAL_COMPLETED) {
            this.isFinalTask = true;
        }
    }
    getMetadataTypeValue(metadata) {
        if (metadata.value && metadata.value.value) {
            return metadata.value.value.type ? metadata.value.value.type : null;
        }
        return null;
    }
    hasCurrentWorkflowActions() {
        return this.currentWorkflowActions && this.currentWorkflowActions.length > 0 ? true : false;
    }
    ngOnInit() {
        let viewConfig;
        const currentUSerID = this.sharingService.getCurrentUserItemID();
        const customMetadataFields = this.otmmMetadataService.getCustomTaskMetadataFields();
        this.projectCancelledStatus = this.projectStatusType === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
        if (this.taskData.STATUS && this.taskData.STATUS.TYPE === TaskConstants.StatusConstant.TASK_STATUS.CANCELLED) {
            this.isCancelledTask = true;
        }
        if (currentUSerID && this.taskConfig.selectedProject && this.taskConfig.selectedProject.R_PO_PROJECT_OWNER &&
            this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'] && currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.isProjectOwner = true;
        }
        this.taskStatusType = StatusTypes.FINAL_REJECTED;
        this.handleActionsOnTaskView();
        if (this.taskData && this.taskData.STATUS_STATE === 'FINAL') {
            this.isFinalStatus = true;
        }
        //this.enableDeleteForTask = this.taskData.TASK_STATUS_TYPE === StatusTypes.INITIAL ? true : this.taskData.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ? true : false;
        if (this.taskConfig.taskViewName) {
            viewConfig = this.taskConfig.taskViewName;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetailsParent) => {
            const viewDetails = viewDetailsParent ? JSON.parse(JSON.stringify(viewDetailsParent)) : null;
            if (viewDetails && viewDetails.R_PM_CARD_VIEW_MPM_FIELDS) {
                viewDetails.R_PM_CARD_VIEW_MPM_FIELDS.forEach((mpmField) => {
                    if (this.taskConstants.DEFAULT_CARD_FIELDS.indexOf(mpmField.MAPPER_NAME) < 0) {
                        mpmField.VALUE = this.fieldConfigService.getFieldValueByDisplayColumn(mpmField, this.taskData);
                        this.displayableMPMFields.push(mpmField);
                    }
                });
            }
        });
        if (this.taskConfig.currentProjectId && this.taskConfig.projectData && this.taskConfig.projectData.length > 0) {
            this.taskConfig.selectedProject = this.taskConfig.projectData.find(project => project.ID === this.taskConfig.currentProjectId);
        }
        if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW === 'true' && this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            const taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
            if (taskItemId && taskItemId.split('.')[1]) {
                const taskId = taskItemId.split('.')[1];
                const currentWorkflowActionRules = this.taskConfig.workflowActionRules.filter(workflowActionRule => workflowActionRule.TaskId === taskId);
                this.currentWorkflowActions = [];
                currentWorkflowActionRules.forEach(rule => {
                    if (rule && rule.Action) {
                        if (rule.Action.length > 0) {
                            rule.Action.forEach(action => {
                                const selectedAction = this.currentWorkflowActions.find(currentAction => currentAction.Id === action.Id);
                                if (!selectedAction) {
                                    this.currentWorkflowActions.push(action);
                                }
                            });
                        }
                        else {
                            this.currentWorkflowActions.push(rule.Action);
                        }
                    }
                });
            }
        }
    }
};
TaskCardViewComponent.ctorParameters = () => [
    { type: TaskService },
    { type: SharingService },
    { type: OtmmMetadataService },
    { type: FormatToLocalePipe },
    { type: UtilService },
    { type: ViewConfigService },
    { type: FieldConfigService },
    { type: LoaderService },
    { type: NotificationService },
    { type: MatDialog }
];
__decorate([
    Input()
], TaskCardViewComponent.prototype, "taskConfig", void 0);
__decorate([
    Input()
], TaskCardViewComponent.prototype, "taskData", void 0);
__decorate([
    Input()
], TaskCardViewComponent.prototype, "isTemplate", void 0);
__decorate([
    Input()
], TaskCardViewComponent.prototype, "projectOwner", void 0);
__decorate([
    Input()
], TaskCardViewComponent.prototype, "projectStatusType", void 0);
__decorate([
    Input()
], TaskCardViewComponent.prototype, "isOnHoldProject", void 0);
__decorate([
    Output()
], TaskCardViewComponent.prototype, "refreshTask", void 0);
__decorate([
    Output()
], TaskCardViewComponent.prototype, "editTaskHandler", void 0);
__decorate([
    Output()
], TaskCardViewComponent.prototype, "taskDetailsHandler", void 0);
__decorate([
    Output()
], TaskCardViewComponent.prototype, "customWorkflowHandler", void 0);
TaskCardViewComponent = __decorate([
    Component({
        selector: 'mpm-task-card-view',
        template: "<mat-card class=\"task-card\" *ngIf=\"taskData\">\r\n    <div class=\"flex-col\">\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item copy-icon card-title-wrapper\">\r\n                    <mat-icon class=\"task-card-type-icon \" *ngIf=\"!isTemplate\"\r\n                        [ngClass]=\"[ ((getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)==='true') && (!(projectCancelledStatus) && !(isOnHoldProject))) ? 'active-task-icon' : 'inactive-task-icon']\"\r\n                        matTooltip=\"{{getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)==='true' ? 'Active Task' : 'Inactive Task'}}\">\r\n                        <!-- [style.color]=\"getActiveColor(getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)) === 'true' ? active-task : inactive-task\" -->\r\n                        {{getTaskTypeIcon(getProperty(taskData, MPMFeildConstants.TASK_TYPE))}}</mat-icon>\r\n                    <span class=\"card-title\" (click)=\"openTaskDetails(false)\"\r\n                        matTooltip=\"{{getProperty(taskData, MPMFeildConstants.TASK_NAME) || 'NA'}}\">\r\n                        {{getProperty(taskData, MPMFeildConstants.TASK_NAME) || 'NA'}}\r\n                    </span>\r\n                </div>\r\n                <div class=\"flex-row-item action-button\">\r\n                    <mat-icon color=\"primary\" matTooltip=\"Actions\" matSuffix [matMenuTriggerFor]=\"taskMenu\"\r\n                        *ngIf=\"isProjectOwner\">\r\n                        more_vert\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <span>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item card-item\">\r\n                        <div class=\"task-percentage\"\r\n                            matTooltip=\"Task Completion : {{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS) || 0}}%\">\r\n                            <span class=\"item-title\">\r\n                                {{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS) || 0}}%</span>\r\n                            <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                                value=\"{{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS)}}\">\r\n                            </mat-progress-bar>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </span>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-col-item task-info-wrapper\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row\">\r\n                        <div class=\"flex-row-item card-item task-info\" *ngIf=\"!isTemplate\"\r\n                            matTooltip=\"Due Date: {{getProperty(taskData, MPMFeildConstants.TASK_DUE_DATE)}}\">\r\n                            <mat-icon>event</mat-icon>\r\n                            <span class=\"item-value\">{{getProperty(taskData, MPMFeildConstants.TASK_DUE_DATE)}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <span *ngIf=\"displayableMPMFields && displayableMPMFields.length > 0\">\r\n                    <div class=\"flex-col-item\" *ngFor=\"let column of displayableMPMFields;let i = index;\">\r\n                        <div class=\"flex-row\">\r\n                            <div class=\"flex-row-item card-item task-info\"\r\n                                matTooltip=\"{{column.DISPLAY_NAME}} : {{column.VALUE|| 'NA'}}\">\r\n                                <span class=\"item-title\">{{column.DISPLAY_NAME}}:</span>\r\n                                <span class=\"item-value\">{{column.VALUE || 'NA'}}</span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </span>\r\n            </div>\r\n            <div class=\"flex-row-item priority-info\">\r\n                <mat-icon matTooltip=\"Priority: {{getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).tooltip}}\"\r\n                    [style.color]=\"getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).color\">\r\n                    {{getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).icon}}</mat-icon>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</mat-card>\r\n<mat-menu #taskMenu=\"matMenu\">\r\n    <button mat-menu-item matTooltip=\"Add New Deliverable\" (click)=\"openTaskDetails(true)\"\r\n        [disabled]=\"!(!isFinalTask && isProjectOwner && isNormalTask)\">\r\n        <mat-icon>add</mat-icon>\r\n        <span>Add New Deliverable</span>\r\n    </button>\r\n    <button mat-menu-item matTooltip=\"Edit Task\" (click)=\"editTask()\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>edit</mat-icon>\r\n        <span>Edit Task</span>\r\n    </button>\r\n    <!-- *ngIf=\"isTemplate\" -->\r\n    <button mat-menu-item matTooltip=\"Delete Task\" (click)=\"deleteTask(false)\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>delete</mat-icon>\r\n        <span>Delete Task</span>\r\n    </button>\r\n    <!--*ngIf=\"isTemplate\"-->\r\n    <button mat-menu-item matTooltip=\"Permanent Delete Task\" (click)=\"deleteTask(true)\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>delete</mat-icon>\r\n        <span>Permanent Delete Task</span>\r\n    </button>\r\n    \r\n\r\n</mat-menu>",
        styles: [".circle{height:10px;width:10px;background-color:#e0e0e0;border-radius:10px;margin-top:10px}.circle.active{background-color:#cbc32d}.task-header{display:flex;flex-direction:row}.task-card-image{flex-grow:1;margin-bottom:5px;margin-left:5px}.task-percentage{margin-bottom:10px;text-align:right;width:100%}mat-card{cursor:pointer}.card-title{display:inline-block;text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap;width:150px;margin:5px;font-size:15px}.flex-row-item p{font-size:13px;margin:2px}.task-card{padding:12px}.task-card .action-button{justify-content:flex-end}.task-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.card-heading{font-weight:590}.card-item{margin:4px;font-size:14px;flex-grow:1!important;align-items:center}mat-icon.task-card-type-icon{visibility:visible;margin-top:5px;margin-right:0}.priority-info{justify-content:flex-end}.task-card .task-info-wrapper{width:85%}.task-card .task-info{max-width:180px}.task-card span.item-title{margin-right:4px;font-size:14px;font-weight:600;line-height:16px;white-space:nowrap}.task-card span.item-value{text-overflow:ellipsis;overflow:hidden;display:inline-block;white-space:nowrap}"]
    })
], TaskCardViewComponent);
export { TaskCardViewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jYXJkLXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLWNhcmQtdmlldy90YXNrLWNhcmQtdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDcEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQztBQUN4SCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFckQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFZLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRTdFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN6RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUWpGLElBQWEscUJBQXFCLEdBQWxDLE1BQWEscUJBQXFCO0lBa0M5QixZQUNXLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLG1CQUF3QyxFQUN4QyxrQkFBc0MsRUFDdEMsV0FBd0IsRUFDeEIsaUJBQW9DLEVBQ3BDLGtCQUFzQyxFQUN0QyxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsTUFBaUI7UUFUakIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBckNsQixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUUxRCxrQkFBYSxHQUFHLGFBQWEsQ0FBQztRQUM5Qix5QkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztRQUM1QyxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixnQkFBVyxHQUFHLHlCQUF5QixDQUFDO1FBQ3hDLFNBQUksR0FBRyxtQkFBbUIsQ0FBQztRQUMzQix5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUN0RCxnQkFBVyxHQUFHO1lBQ1YsS0FBSyxFQUFFLEVBQUU7WUFDVCxJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxFQUFFO1NBQ2QsQ0FBQztRQUNGLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSXJCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO0lBYWQsQ0FBQztJQUVMLGVBQWUsQ0FBQyxnQkFBZ0I7UUFDNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUNwRCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hGLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxrQkFBa0I7U0FDckI7SUFDTCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN4RixJQUFJLFVBQVUsRUFBRTtZQUNaLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssVUFBVSxDQUFDLENBQUM7WUFDeEYsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNqRDtJQUNMLENBQUM7SUFFRCxRQUFRO1FBQ0osTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN4RixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsY0FBYyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDMUUsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3BFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNDLGtCQUFrQjtTQUNyQjthQUFNLElBQUksVUFBVSxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQyxrQkFBa0I7U0FDckI7SUFDTCxDQUFDO0lBSUQsVUFBVSxDQUFDLGFBQXNCO1FBQzdCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDL0UsSUFBSSxHQUFHLENBQUM7UUFDUixNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDOUcsSUFBSSxnQkFBZ0IsRUFBRTtZQUNsQixHQUFHLEdBQUcscUZBQXFGLENBQUM7U0FDL0Y7YUFBTTtZQUNILEdBQUcsR0FBRyxxREFBcUQsQ0FBQztTQUMvRDtRQUVELGNBQWM7UUFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUNsRSxJQUFJLFlBQVksQ0FBQyxLQUFLLElBQUksU0FBUyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksS0FBSyxDQUFDO2dCQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDcEMsS0FBSyxHQUFHLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNoQztxQkFBTTtvQkFDSCxLQUFLLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztpQkFDOUI7Z0JBQ0QsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDakIsSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssT0FBTyxFQUFFO3dCQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDbEQ7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUNELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO2dCQUMzRCxLQUFLLEVBQUUsS0FBSztnQkFDWixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsSUFBSSxFQUFFO29CQUNGLE9BQU8sRUFBRSxHQUFHO29CQUNaLGlEQUFpRDtvQkFDakQsSUFBSSxFQUFFLElBQUksQ0FBQyxZQUFZO29CQUN2QixZQUFZLEVBQUUsS0FBSztvQkFDbkIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxNQUFNLEVBQUU7b0JBQ1IsSUFBRyxhQUFhLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUNoRSxRQUFRLENBQUMsRUFBRTs0QkFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7NEJBQ3ZFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDbkIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFOzRCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQzt3QkFDcEYsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUMvRCxRQUFRLENBQUMsRUFBRTs0QkFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7NEJBQ3ZFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDbkIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFOzRCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQzt3QkFDcEYsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7SUFDWCxDQUFDO0lBRUQsa0JBQWtCO0lBQ2xCLENBQUM7SUFFRCxZQUFZO0lBQ1osQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsaUJBQWlCLENBQUMsUUFBUTtRQUN0QixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hGLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNwRSxNQUFNLE1BQU0sR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN4RSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELFdBQVcsQ0FBQyxRQUFhLEVBQUUsVUFBa0I7UUFDekMsTUFBTSxZQUFZLEdBQWEsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqSSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUVELGlCQUFpQixDQUFDLGVBQWUsRUFBRSxVQUFVO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQzlHLENBQUM7SUFFRCxXQUFXLENBQUMsUUFBYSxFQUFFLFVBQWtCO1FBQ3pDLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxFQUN2RixVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFckIsT0FBTztZQUNILEtBQUssRUFBRSxXQUFXLENBQUMsS0FBSztZQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUk7WUFDdEIsT0FBTyxFQUFFLFdBQVcsQ0FBQyxPQUFPO1NBQy9CLENBQUM7SUFDTixDQUFDO0lBQ0Q7OztPQUdHO0lBQ0gsZUFBZSxDQUFDLFFBQVE7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRDs7O09BR0c7SUFDSCx1QkFBdUI7UUFDbkIsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RixNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNwRyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEtBQUssYUFBYSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEtBQUssYUFBYTtZQUM1RyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxLQUFLLGFBQWEsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxLQUFLLGFBQWEsRUFBRTtZQUNoSCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztTQUMvQjtRQUNELElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLEVBQUU7WUFDbEksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDOUI7UUFDRCxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsRixJQUFJLGFBQWEsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUU7WUFDNUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDNUI7UUFDRCxJQUFJLGtCQUFrQixLQUFLLFdBQVcsQ0FBQyxjQUFjLElBQUksa0JBQWtCLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxrQkFBa0IsS0FBSyxXQUFXLENBQUMsZUFBZSxFQUFFO1lBQzlKLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1NBQzNCO0lBRUwsQ0FBQztJQUNELG9CQUFvQixDQUFDLFFBQVE7UUFDekIsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ3hDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUN2RTtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDRCx5QkFBeUI7UUFDckIsT0FBTyxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ2hHLENBQUM7SUFDRCxRQUFRO1FBQ0osSUFBSSxVQUFVLENBQUM7UUFDZixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDakUsTUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztRQUVwRixJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixLQUFLLGVBQWUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFFcEgsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssYUFBYSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFO1lBQzFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCO1lBQ3RHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxJQUFJLGFBQWEsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDN0osSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDakQsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDL0IsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxLQUFLLE9BQU8sRUFBRTtZQUN6RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUM3QjtRQUNELHdLQUF3SztRQUV4SyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO1lBQzlCLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztTQUM3QzthQUFNO1lBQ0gsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7U0FDdkQ7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsK0JBQStCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsaUJBQTZCLEVBQUUsRUFBRTtZQUMzRyxNQUFNLFdBQVcsR0FBZSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3pHLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyx5QkFBeUIsRUFBRTtnQkFDdEQsV0FBVyxDQUFDLHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQWtCLEVBQUUsRUFBRTtvQkFDakUsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUMxRSxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUMvRixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUM1QztnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNsSTtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEtBQUssTUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNMLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDeEYsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDeEMsTUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsTUFBTSwwQkFBMEIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUMsa0JBQWtCLENBQUMsTUFBTSxLQUFLLE1BQU0sQ0FBQyxDQUFDO2dCQUMxSSxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO2dCQUNqQywwQkFBMEIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3RDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7d0JBQ3JCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtnQ0FDekIsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUN6RyxJQUFJLENBQUMsY0FBYyxFQUFFO29DQUNqQixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lDQUM1Qzs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjs2QkFBTTs0QkFDSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt5QkFDakQ7cUJBQ0o7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNKO0lBQ0wsQ0FBQztDQUVKLENBQUE7O1lBcFEyQixXQUFXO1lBQ1IsY0FBYztZQUNULG1CQUFtQjtZQUNwQixrQkFBa0I7WUFDekIsV0FBVztZQUNMLGlCQUFpQjtZQUNoQixrQkFBa0I7WUFDdkIsYUFBYTtZQUNQLG1CQUFtQjtZQUNoQyxTQUFTOztBQTNDbkI7SUFBUixLQUFLLEVBQUU7eURBQVk7QUFDWDtJQUFSLEtBQUssRUFBRTt1REFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO3lEQUFZO0FBQ1g7SUFBUixLQUFLLEVBQUU7MkRBQXNCO0FBQ3JCO0lBQVIsS0FBSyxFQUFFO2dFQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTs4REFBaUI7QUFDZjtJQUFULE1BQU0sRUFBRTswREFBdUM7QUFDdEM7SUFBVCxNQUFNLEVBQUU7OERBQTJDO0FBQzFDO0lBQVQsTUFBTSxFQUFFO2lFQUE4QztBQUM3QztJQUFULE1BQU0sRUFBRTtvRUFBaUQ7QUFWakQscUJBQXFCO0lBTmpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxvQkFBb0I7UUFDOUIsZzVLQUE4Qzs7S0FFakQsQ0FBQztHQUVXLHFCQUFxQixDQXVTakM7U0F2U1kscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbGliL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEFzc2V0U3RhdHVzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vYXNzZXQtY2FyZC9hc3NldF9zdGF0dXNfY29uc3RhbnRzJztcclxuXHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5cclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCwgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TZWFyY2hDb25maWdDb25zdGFudHMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS10YXNrLWNhcmQtdmlldycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdGFzay1jYXJkLXZpZXcuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdGFzay1jYXJkLXZpZXcuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRhc2tDYXJkVmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBASW5wdXQoKSB0YXNrQ29uZmlnO1xyXG4gICAgQElucHV0KCkgdGFza0RhdGE7XHJcbiAgICBASW5wdXQoKSBpc1RlbXBsYXRlO1xyXG4gICAgQElucHV0KCkgcHJvamVjdE93bmVyOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBwcm9qZWN0U3RhdHVzVHlwZTtcclxuICAgIEBJbnB1dCgpIGlzT25Ib2xkUHJvamVjdDtcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoVGFzayA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGVkaXRUYXNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHRhc2tEZXRhaWxzSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGN1c3RvbVdvcmtmbG93SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIHRhc2tDb25zdGFudHMgPSBUYXNrQ29uc3RhbnRzO1xyXG4gICAgYXNzZXRTdGF0dXNDb25zdGFudHMgPSBBc3NldFN0YXR1c0NvbnN0YW50cztcclxuICAgIGlzUHJvamVjdE93bmVyID0gZmFsc2U7XHJcbiAgICBpc0ZpbmFsU3RhdHVzID0gZmFsc2U7XHJcbiAgICBpc0NhbmNlbGxlZFRhc2sgPSBmYWxzZTtcclxuICAgIGlzRmluYWxUYXNrID0gZmFsc2U7XHJcbiAgICBjb3B5VG9vbFRpcCA9ICdDbGljayB0byBjb3B5IHRhc2sgbmFtZSc7XHJcbiAgICB2aWV3ID0gJ1Byb2plY3RNYW5hZ2VtZW50JztcclxuICAgIGRpc3BsYXlhYmxlTVBNRmllbGRzOiBBcnJheTxNUE1GaWVsZD4gPSBbXTtcclxuICAgIE1QTUZlaWxkQ29uc3RhbnRzID0gTVBNRmllbGRDb25zdGFudHMuTVBNX1RBU0tfRklFTERTO1xyXG4gICAgcHJpb3JpdHlPYmogPSB7XHJcbiAgICAgICAgY29sb3I6ICcnLFxyXG4gICAgICAgIGljb246ICcnLFxyXG4gICAgICAgIHRvb2x0aXA6ICcnXHJcbiAgICB9O1xyXG4gICAgaXNOb3JtYWxUYXNrID0gZmFsc2U7XHJcbiAgICBwcm9qZWN0Q2FuY2VsbGVkU3RhdHVzO1xyXG4gICAgY3VycmVudFdvcmtmbG93QWN0aW9ucztcclxuICAgIHRhc2tTdGF0dXNUeXBlO1xyXG4gICAgZm9yY2VEZWxldGlvbiA9IGZhbHNlO1xyXG4gICAgYWZmZWN0ZWRUYXNrID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmb3JtYXRUb0xvY2FsZVBpcGU6IEZvcm1hdFRvTG9jYWxlUGlwZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZ1xyXG4gICAgKSB7IH1cclxuXHJcbiAgICBvcGVuVGFza0RldGFpbHMoaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZy5pc05ld0RlbGl2ZXJhYmxlID0gaXNOZXdEZWxpdmVyYWJsZTtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMudGFza0RldGFpbHNIYW5kbGVyLmVtaXQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgLy8gdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5DdXN0b21Xb3JrZmxvdygpIHtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgaWYgKHRhc2tJdGVtSWQpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRUYXNrID0gdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0LmZpbmQodGFzayA9PiB0YXNrLklURU1fSUQgPT09IHRhc2tJdGVtSWQpO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93SGFuZGxlci5uZXh0KHNlbGVjdGVkVGFzayk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXRUYXNrKCkge1xyXG4gICAgICAgIGNvbnN0IHRhc2tJdGVtSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICBjb25zdCBpc0FwcHJvdmFsVGFzayA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX0lTX0FQUFJPVkFMKTtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2sgPSBpc0FwcHJvdmFsVGFzayA9PT0gJ3RydWUnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgdGhpcy5lZGl0VGFza0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRhc2tJdGVtSWQgJiYgISh0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykpKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkO1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRUYXNrSGFuZGxlci5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG5cclxuICAgIGRlbGV0ZVRhc2soaXNGb3JjZURlbGV0ZTogYm9vbGVhbikge1xyXG4gICAgICAgIGNvbnN0IHRhc2tJZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX0lEKTtcclxuICAgICAgICBsZXQgbXNnO1xyXG4gICAgICAgIGNvbnN0IGlzQ3VzdG9tV29ya2Zsb3cgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0LklTX0NVU1RPTV9XT1JLRkxPVyk7XHJcbiAgICAgICAgaWYgKGlzQ3VzdG9tV29ya2Zsb3cpIHtcclxuICAgICAgICAgICAgbXNnID0gJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIHRhc2tzIGFuZCByZWNvbmZpZ3VyZSB0aGUgd29ya2Zsb3cgcnVsZSBlbmdpbmU/JztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtc2cgPSAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGUgdGFza3MgYW5kIEV4aXQ/JztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vICBNUE1WMy0yMDk0XHJcbiAgICAgICAgdGhpcy50YXNrU2VydmljZS5nZXRBbGxBcHByb3ZhbFRhc2tzKHRhc2tJZCkuc3Vic2NyaWJlKGFwcHJvdmFsVGFzayA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcHByb3ZhbFRhc2sudHVwbGUgIT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcmNlRGVsZXRpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZmZlY3RlZFRhc2sgPSBbXTtcclxuICAgICAgICAgICAgICAgIGxldCB0YXNrcztcclxuICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShhcHByb3ZhbFRhc2sudHVwbGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFza3MgPSBbYXBwcm92YWxUYXNrLnR1cGxlXTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFza3MgPSBhcHByb3ZhbFRhc2sudHVwbGU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0YXNrcy5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHRhc2sub2xkLlRhc2tWaWV3LmlzRGVsZXRlZCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFmZmVjdGVkVGFzay5wdXNoKHRhc2sub2xkLlRhc2tWaWV3Lm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IG1zZyxcclxuICAgICAgICAgICAgICAgICAgICAvL25hbWU6IHRoaXMuaXNUZW1wbGF0ZSA/IFtdIDogdGhpcy5hZmZlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdGhpcy5hZmZlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGlzRm9yY2VEZWxldGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5mb3JjZURlbGV0ZVRhc2sodGFza0lkLCBpc0N1c3RvbVdvcmtmbG93KS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1Rhc2sgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0RlbGV0ZSBUYXNrIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5zb2Z0RGVsZXRlVGFzayh0YXNrSWQsIGlzQ3VzdG9tV29ya2Zsb3cpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVGFzayBoYXMgYmVlbiBkZWxldGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRGVsZXRlIFRhc2sgb3BlcmF0aW9uIGZhaWxlZCwgdHJ5IGFnYWluIGxhdGVyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNvbW1lbnQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblJlcXVlc3REZXRhaWxzKCkge1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dSZW1pbmRlcigpIHtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgICAgIHRoaXMucmVmcmVzaFRhc2submV4dCh0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICB0cmlnZ2VyQWN0aW9uUnVsZShhY3Rpb25JZCkge1xyXG4gICAgICAgIGNvbnN0IHRhc2tJdGVtSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50cmlnZ2VyUnVsZU9uQWN0aW9uKHRhc2tJZCwgYWN0aW9uSWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1RyaWdnZXJpbmcgYWN0aW9uIGhhcyBiZWVuIGluaXRpYXRlZCcpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaEN1cnJlbnRUYXNrKCkge1xyXG4gICAgICAgIHRoaXMucmVmcmVzaFRhc2submV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KHRhc2tEYXRhOiBhbnksIG1hcHBlck5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgZGlzcGxheUNvbHVtOiBNUE1GaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIG1hcHBlck5hbWUsIE1QTV9MRVZFTFMuVEFTSyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtLCB0YXNrRGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUoZGVsaXZlcmFibGVEYXRhLCBwcm9wZXJ0eUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZm9ybWF0VG9Mb2NhbGVQaXBlLnRyYW5zZm9ybSh0aGlzLnRhc2tTZXJ2aWNlLmNvbnZlclRvTG9jYWxEYXRlKGRlbGl2ZXJhYmxlRGF0YSwgcHJvcGVydHlJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByaW9yaXR5KHRhc2tEYXRhOiBhbnksIG1hcHBlck5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IHByaW9yaXR5T2JqID0gdGhpcy51dGlsU2VydmljZS5nZXRQcmlvcml0eUljb24odGhpcy5nZXRQcm9wZXJ0eSh0YXNrRGF0YSwgbWFwcGVyTmFtZSksXHJcbiAgICAgICAgICAgIE1QTV9MRVZFTFMuVEFTSyk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBwcmlvcml0eU9iai5jb2xvcixcclxuICAgICAgICAgICAgaWNvbjogcHJpb3JpdHlPYmouaWNvbixcclxuICAgICAgICAgICAgdG9vbHRpcDogcHJpb3JpdHlPYmoudG9vbHRpcFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICAvKipcclxuICAgICAqIEBzaW5jZSBNUE1WMy05NDZcclxuICAgICAqIEBwYXJhbSB0YXNrVHlwZVxyXG4gICAgICovXHJcbiAgICBnZXRUYXNrVHlwZUljb24odGFza1R5cGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNrU2VydmljZS5nZXRUYXNrSWNvbkJ5VGFza1R5cGUodGFza1R5cGUpO1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBAc2luY2UgTVBNVjMtMTE0MVxyXG4gICAgICogQHBhcmFtIHRhc2tUeXBlXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZUFjdGlvbnNPblRhc2tWaWV3KCkge1xyXG4gICAgICAgIGxldCBjdXJyVGFza1ZhbHVlID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTKTtcclxuICAgICAgICBjb25zdCBjdXJyVGFza1N0YXR1c1R5cGUgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19TVEFUVVNfVFlQRSk7XHJcbiAgICAgICAgaWYgKHRoaXMuYXNzZXRTdGF0dXNDb25zdGFudHMuUkVKRUNURUQgPT09IGN1cnJUYXNrVmFsdWUgfHwgdGhpcy5hc3NldFN0YXR1c0NvbnN0YW50cy5BUFBST1ZFRCA9PT0gY3VyclRhc2tWYWx1ZSB8fFxyXG4gICAgICAgICAgICB0aGlzLmFzc2V0U3RhdHVzQ29uc3RhbnRzLkNBTkNFTExFRCA9PT0gY3VyclRhc2tWYWx1ZSB8fCB0aGlzLmFzc2V0U3RhdHVzQ29uc3RhbnRzLkNPTVBMRVRFRCA9PT0gY3VyclRhc2tWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmlzQ2FuY2VsbGVkVGFzayA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RPd25lciA9PT0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKCkgfHwgdGhpcy5wcm9qZWN0T3duZXIgPT09IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmlzUHJvamVjdE93bmVyID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY3VyclRhc2tWYWx1ZSA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX1RZUEUpO1xyXG4gICAgICAgIGlmIChjdXJyVGFza1ZhbHVlID09PSB0aGlzLnRhc2tDb25zdGFudHMuVEFTS19UWVBFLk5PUk1BTF9UQVNLKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNOb3JtYWxUYXNrID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGN1cnJUYXNrU3RhdHVzVHlwZSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQgfHwgY3VyclRhc2tTdGF0dXNUeXBlID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCB8fCBjdXJyVGFza1N0YXR1c1R5cGUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkge1xyXG4gICAgICAgICAgICB0aGlzLmlzRmluYWxUYXNrID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG4gICAgZ2V0TWV0YWRhdGFUeXBlVmFsdWUobWV0YWRhdGEpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChtZXRhZGF0YS52YWx1ZSAmJiBtZXRhZGF0YS52YWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbWV0YWRhdGEudmFsdWUudmFsdWUudHlwZSA/IG1ldGFkYXRhLnZhbHVlLnZhbHVlLnR5cGUgOiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIGhhc0N1cnJlbnRXb3JrZmxvd0FjdGlvbnMoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFdvcmtmbG93QWN0aW9ucyAmJiB0aGlzLmN1cnJlbnRXb3JrZmxvd0FjdGlvbnMubGVuZ3RoID4gMCA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRVU2VySUQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgY29uc3QgY3VzdG9tTWV0YWRhdGFGaWVsZHMgPSB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2UuZ2V0Q3VzdG9tVGFza01ldGFkYXRhRmllbGRzKCk7XHJcblxyXG4gICAgICAgIHRoaXMucHJvamVjdENhbmNlbGxlZFN0YXR1cyA9IHRoaXMucHJvamVjdFN0YXR1c1R5cGUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQgPyB0cnVlIDogZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tEYXRhLlNUQVRVUyAmJiB0aGlzLnRhc2tEYXRhLlNUQVRVUy5UWVBFID09PSBUYXNrQ29uc3RhbnRzLlN0YXR1c0NvbnN0YW50LlRBU0tfU1RBVFVTLkNBTkNFTExFRCkge1xyXG4gICAgICAgICAgICB0aGlzLmlzQ2FuY2VsbGVkVGFzayA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjdXJyZW50VVNlcklEICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVIgJiZcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10gJiYgY3VycmVudFVTZXJJRCA9PT0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudGFza1N0YXR1c1R5cGUgPSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRDtcclxuICAgICAgICB0aGlzLmhhbmRsZUFjdGlvbnNPblRhc2tWaWV3KCk7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0RhdGEgJiYgdGhpcy50YXNrRGF0YS5TVEFUVVNfU1RBVEUgPT09ICdGSU5BTCcpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0ZpbmFsU3RhdHVzID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy90aGlzLmVuYWJsZURlbGV0ZUZvclRhc2sgPSB0aGlzLnRhc2tEYXRhLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwgPyB0cnVlIDogdGhpcy50YXNrRGF0YS5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTlRFUk1FRElBVEUgPyB0cnVlIDogZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcudGFza1ZpZXdOYW1lKSB7XHJcbiAgICAgICAgICAgIHZpZXdDb25maWcgPSB0aGlzLnRhc2tDb25maWcudGFza1ZpZXdOYW1lO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuVEFTSztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHNQYXJlbnQ6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgICAgICAgY29uc3Qgdmlld0RldGFpbHM6IFZpZXdDb25maWcgPSB2aWV3RGV0YWlsc1BhcmVudCA/IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodmlld0RldGFpbHNQYXJlbnQpKSA6IG51bGw7XHJcbiAgICAgICAgICAgIGlmICh2aWV3RGV0YWlscyAmJiB2aWV3RGV0YWlscy5SX1BNX0NBUkRfVklFV19NUE1fRklFTERTKSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3RGV0YWlscy5SX1BNX0NBUkRfVklFV19NUE1fRklFTERTLmZvckVhY2goKG1wbUZpZWxkOiBNUE1GaWVsZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25zdGFudHMuREVGQVVMVF9DQVJEX0ZJRUxEUy5pbmRleE9mKG1wbUZpZWxkLk1BUFBFUl9OQU1FKSA8IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbXBtRmllbGQuVkFMVUUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKG1wbUZpZWxkLCB0aGlzLnRhc2tEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5YWJsZU1QTUZpZWxkcy5wdXNoKG1wbUZpZWxkKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuY3VycmVudFByb2plY3RJZCAmJiB0aGlzLnRhc2tDb25maWcucHJvamVjdERhdGEgJiYgdGhpcy50YXNrQ29uZmlnLnByb2plY3REYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCA9IHRoaXMudGFza0NvbmZpZy5wcm9qZWN0RGF0YS5maW5kKHByb2plY3QgPT4gcHJvamVjdC5JRCA9PT0gdGhpcy50YXNrQ29uZmlnLmN1cnJlbnRQcm9qZWN0SWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0LklTX0NVU1RPTV9XT1JLRkxPVyA9PT0gJ3RydWUnICYmIHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzICYmIHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgdGFza0l0ZW1JZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRXb3JrZmxvd0FjdGlvblJ1bGVzID0gdGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMuZmlsdGVyKHdvcmtmbG93QWN0aW9uUnVsZSA9PiB3b3JrZmxvd0FjdGlvblJ1bGUuVGFza0lkID09PSB0YXNrSWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50V29ya2Zsb3dBY3Rpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICBjdXJyZW50V29ya2Zsb3dBY3Rpb25SdWxlcy5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChydWxlICYmIHJ1bGUuQWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChydWxlLkFjdGlvbi5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBydWxlLkFjdGlvbi5mb3JFYWNoKGFjdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBY3Rpb24gPSB0aGlzLmN1cnJlbnRXb3JrZmxvd0FjdGlvbnMuZmluZChjdXJyZW50QWN0aW9uID0+IGN1cnJlbnRBY3Rpb24uSWQgPT09IGFjdGlvbi5JZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzZWxlY3RlZEFjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRXb3JrZmxvd0FjdGlvbnMucHVzaChhY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50V29ya2Zsb3dBY3Rpb25zLnB1c2gocnVsZS5BY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==