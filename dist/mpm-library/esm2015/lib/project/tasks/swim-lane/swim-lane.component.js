import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { TaskService } from '../task.service';
import { MatDialog } from '@angular/material/dialog';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ProjectConstant } from '../../project-overview/project.constants';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { EntityAppDefService } from '../../../mpm-utils/services/entity.appdef.service';
import { AssetService } from '../../shared/services/asset.service';
let SwimLaneComponent = class SwimLaneComponent {
    constructor(media, changeDetectorRef, taskService, dialog, fieldConfigService, entityAppDefService, assetService) {
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.taskService = taskService;
        this.dialog = dialog;
        this.fieldConfigService = fieldConfigService;
        this.entityAppDefService = entityAppDefService;
        this.assetService = assetService;
        this.refreshSwimLine = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.swimListIds = [];
        this.swimList = [];
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => this.changeDetectorRef.detectChanges();
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    editTask(eventData) {
        this.editTaskHandler.emit(eventData);
    }
    openTaskDetails(eventData) {
        this.taskDetailsHandler.emit(eventData);
    }
    openCustomWorkflow(eventData) {
        this.customWorkflowHandler.emit(eventData);
    }
    getConnectedId(id) {
        return this.swimListIds.filter(ids => ids !== id);
    }
    changeSwimLine(task, status) {
    }
    drop(event, list) {
        if (this.taskConfig.isProjectOwner) {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            }
            else {
                if (this.taskConfig.isTemplate === true) {
                    const eventData = { message: 'Status cannot be update for a template\'s task', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                    this.notificationHandler.next(eventData);
                    return;
                }
                transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
                this.changeSwimLine(event.container.data[event.currentIndex], list.listDetail);
            }
        }
    }
    dropColumn(event) {
        if (this.taskConfig.isProjectOwner) {
            if (this.swimList.length > 1) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
                this.updateSwimLine(event.container.data, event.previousIndex, event.currentIndex);
            }
        }
    }
    updateSwimLine(containerData, previousIndex, currentIndex) {
        const draggedLane = containerData[currentIndex];
        const objects = [];
        const tempObject = {
            ItemId: Number,
            updateData: {
                Properties: {}
            }
        };
        const draggedIndex = previousIndex;
        const droppedIndex = currentIndex;
        if (droppedIndex > draggedIndex) {
            tempObject.ItemId =
                containerData[currentIndex].listDetail.Identity.ItemId;
            tempObject.updateData = {
                Properties: {
                    SEQUENCE: droppedIndex + 1
                }
            };
            objects.push(tempObject);
            for (let i = droppedIndex; i > draggedIndex; i--) {
                const tempObj = {
                    ItemId: Number,
                    updateData: {
                        Properties: {}
                    }
                };
                tempObj.ItemId = this.swimList[i - 1].listDetail.Identity.ItemId;
                tempObj.updateData = {
                    Properties: {
                        SEQUENCE: i
                    }
                };
                objects.push(tempObj);
            }
        }
        else if (draggedIndex > droppedIndex) {
            tempObject.ItemId =
                draggedLane.listDetail.Identity.ItemId;
            tempObject.updateData = {
                Properties: {
                    SEQUENCE: droppedIndex + 1
                }
            };
            objects.push(tempObject);
            for (let i = droppedIndex; i < draggedIndex; i++) {
                const tempObj = {
                    ItemId: Number,
                    updateData: {
                        Properties: {}
                    }
                };
                tempObj.ItemId = this.swimList[i + 1].listDetail.Identity.ItemId;
                tempObj.updateData = {
                    Properties: {
                        SEQUENCE: i + 2
                    }
                };
                objects.push(tempObj);
            }
        }
        this.swimLineUpdate(objects);
    }
    swimLineUpdate(objects) {
        this.taskService.updateDragandDropLanes(objects)
            .subscribe(response => {
            if (response) {
                this.refresh();
            }
            else {
                this.refresh();
                const eventData = { message: 'Something went wrong while updating swimlane position', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
            }
        });
    }
    refresh() {
        this.refreshSwimLine.next(true);
    }
    refreshTask(event) {
        if (event) {
            this.refresh();
        }
    }
    initialiseSwimList() {
        this.swimList = [];
        this.swimListIds = [];
        if (this.taskConfig) {
            // this.entityAppDefService.getStatusById(this.taskConfig.projectStatusId).subscribe(response => {
            //   console.log(response);
            //  this.projectStatusType = response.STATUS_TYPE;
            // });
            const displayColum = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS_ID, MPM_LEVELS.TASK);
            if (this.taskConfig.taskStatus) {
                this.taskConfig.taskStatus.map(status => {
                    const listData = [];
                    if (this.taskConfig.taskTotalCount > 0) {
                        this.taskConfig.taskList.map(task => {
                            if (this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, task) === status['MPM_Status-id'].Id) {
                                listData.push(task);
                            }
                        });
                    }
                    const swimList = {
                        id: status['MPM_Status-id'].Id,
                        listDetail: status,
                        data: listData,
                        dataCount: listData.length
                    };
                    this.swimListIds.push(status['MPM_Status-id'].Id);
                    this.swimList.push(swimList);
                });
            }
            //  });
        }
        if (this.taskConfig && this.taskConfig.taskTotalCount !== 0) {
            this.taskSwimLaneMinHeight = '31rem';
        }
    }
    ngOnInit() {
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(projectResponse => {
                this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD === 'true' ? true : false;
                this.entityAppDefService.getStatusById(this.taskConfig.projectStatusId).subscribe(response => {
                    this.projectStatusType = response.STATUS_TYPE;
                    this.initialiseSwimList();
                });
                // this.initialiseSwimList();
            });
        }
    }
};
SwimLaneComponent.ctorParameters = () => [
    { type: MediaMatcher },
    { type: ChangeDetectorRef },
    { type: TaskService },
    { type: MatDialog },
    { type: FieldConfigService },
    { type: EntityAppDefService },
    { type: AssetService }
];
__decorate([
    Input()
], SwimLaneComponent.prototype, "taskConfig", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "refreshSwimLine", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "loadingHandler", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "notificationHandler", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "editTaskHandler", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "taskDetailsHandler", void 0);
__decorate([
    Output()
], SwimLaneComponent.prototype, "customWorkflowHandler", void 0);
SwimLaneComponent = __decorate([
    Component({
        selector: 'mpm-swim-lane',
        template: "<div class=\"flex-col swim-layout\">\r\n    <!-- uncomment this once drag and drop position is maintained in backend -->\r\n    <!--<div cdkDropList cdkDropListOrientation=\"horizontal\" class=\"flex-col-item\" [cdkDropListData]=\"swimList\"\r\n        (cdkDropListDropped)=\"dropColumn($event)\">-->\r\n    <div class=\"flex-col-item\">\r\n        <!--remove the above tag once above tag is uncommented-->\r\n        <div class=\"swim-nonDrag-columns\">\r\n            <!-- uncomment this once drag and drop position is maintained in backend -->\r\n            <!--<div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\" cdkDrag>                \r\n                <div cdkDropList class=\"grid-list\" [cdkDropListData]=\"list.data\"\r\n                    (cdkDropListDropped)=\"drop($event, list)\" [cdkDropListConnectedTo]=\"getConnectedId(list.id)\"\r\n                    id=\"{{list.id}}\" [ngStyle]=\"{'min-height': taskSwimLaneMinHeight}\">-->\r\n            <div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\">\r\n                <!--remove the above tag once above tag is uncommented-->\r\n                <div class=\"grid-list\" id=\"{{list.id}}\" [ngStyle]=\"{'min-height': taskSwimLaneMinHeight}\">\r\n                    <!--remove the above tag once above tag is uncommented-->\r\n                    <div class=\"card-wrap\">\r\n                        <div class=\"mpm-card\">\r\n                            <section class=\"card-summary\">{{list.listDetail.NAME}}\r\n                                ({{list.dataCount}})</section>\r\n                        </div>\r\n                    </div>\r\n                    <span>\r\n                        <!-- uncomment this once drag and drop task update is maintained in backend -->\r\n                        <!-- <div class=\"swim-data-card\" cdkDrag [cdkDragData]=\"item\" *ngFor=\"let item of list.data\">\r\n                            <mpm-task-card-view [isTemplate]=\"taskConfig.isTemplate\" [taskData]=\"item\"\r\n                                [taskConfig]=\"taskConfig\" (refreshTask)=\"refreshTask($event)\"\r\n                                (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n                            </mpm-task-card-view>\r\n                        </div> -->\r\n\r\n                        <!--remove the following block once above block is uncommented-->\r\n                        <div class=\"swim-data-card\" *ngFor=\"let item of list.data\">\r\n                            <mpm-task-card-view [isTemplate]=\"taskConfig.isTemplate\" [taskData]=\"item\"\r\n                                [taskConfig]=\"taskConfig\" (refreshTask)=\"refreshTask($event)\" [projectStatusType]=\"projectStatusType\"\r\n                                (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n                            </mpm-task-card-view>\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"taskConfig && taskConfig.taskTotalCount==0\" class=\"central-info\">\r\n        <button color=\"primary\" disabled mat-flat-button>No Task is available</button>\r\n    </div>\r\n</div>",
        styles: ["::ng-deep .swim-layout .flex-row-item{flex-grow:.5}.swim-layout{overflow:auto}.swim-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0}.swim-column{box-sizing:border-box;list-style:none;margin:0;position:relative;vertical-align:top;justify-content:center;flex:1 1 0}.swim-data-card{margin-bottom:10px}.swim-nonDrag-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0;display:flex;flex-direction:row}.swim-nonDrag-column{box-sizing:border-box;display:table-cell;list-style:none;margin:0 0 0 12px;position:relative;vertical-align:top;table-layout:fixed;padding-left:5px;padding-right:5px;max-width:15rem;border:1px solid #ddd}::ng-deep .swim-data-card .task-content{margin:5px;cursor:move}::ng-deep app-task-card-view .task-card{min-width:180px}.cdk-drag-preview{box-sizing:border-box;border-radius:4px;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{opacity:0}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.card-box:last-child{border:none}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder){transition:transform 250ms cubic-bezier(0,0,.2,1)}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder) .grid-list{border:1px solid red}.mpm-card{cursor:move;font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.mpm-deliverable{font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.card-summary{display:block;box-sizing:content-box;line-height:1.42857143;max-height:4.28571429em;overflow:hidden}.deliverable-swim{width:240px}.deliverable-data-card{margin:0 0 15px}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}"]
    })
], SwimLaneComponent);
export { SwimLaneComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpbS1sYW5lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3Mvc3dpbS1sYW5lL3N3aW0tbGFuZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBZSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUV6RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUN4RixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFRbkUsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFtQjFCLFlBQ1csS0FBbUIsRUFDbkIsaUJBQW9DLEVBQ3BDLFdBQXdCLEVBQ3hCLE1BQWlCLEVBQ2pCLGtCQUFzQyxFQUN0QyxtQkFBd0MsRUFDeEMsWUFBMEI7UUFOMUIsVUFBSyxHQUFMLEtBQUssQ0FBYztRQUNuQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBdkIzQixvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3pDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUsxRCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBY1YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDeEUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtZQUMxQix3Q0FBd0M7WUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDMUQ7SUFDTCxDQUFDO0lBRUQsUUFBUSxDQUFDLFNBQVM7UUFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsZUFBZSxDQUFDLFNBQVM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsa0JBQWtCLENBQUMsU0FBUztRQUN4QixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCxjQUFjLENBQUMsRUFBRTtRQUNiLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJLEVBQUUsTUFBTTtJQUMzQixDQUFDO0lBRUQsSUFBSSxDQUFDLEtBQTRCLEVBQUUsSUFBSTtRQUNuQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO1lBQ2hDLElBQUksS0FBSyxDQUFDLGlCQUFpQixLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQzdDLGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNsRjtpQkFBTTtnQkFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxLQUFLLElBQUksRUFBRTtvQkFDckMsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0RBQWdELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDaEksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsT0FBTztpQkFDVjtnQkFDRCxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUMxQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksRUFDcEIsS0FBSyxDQUFDLGFBQWEsRUFDbkIsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDbEY7U0FDSjtJQUNMLENBQUM7SUFFRCxVQUFVLENBQUMsS0FBNEI7UUFDbkMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtZQUNoQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDMUIsZUFBZSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvRSxJQUFJLENBQUMsY0FBYyxDQUNmLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNyQixDQUFDO2FBQ0w7U0FDSjtJQUNMLENBQUM7SUFFRCxjQUFjLENBQUMsYUFBYSxFQUFFLGFBQWEsRUFBRSxZQUFZO1FBQ3JELE1BQU0sV0FBVyxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNoRCxNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbkIsTUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsTUFBTTtZQUNkLFVBQVUsRUFBRTtnQkFDUixVQUFVLEVBQUUsRUFBRTthQUNqQjtTQUNKLENBQUM7UUFDRixNQUFNLFlBQVksR0FBRyxhQUFhLENBQUM7UUFDbkMsTUFBTSxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ2xDLElBQUksWUFBWSxHQUFHLFlBQVksRUFBRTtZQUM3QixVQUFVLENBQUMsTUFBTTtnQkFDYixhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDM0QsVUFBVSxDQUFDLFVBQVUsR0FBRztnQkFDcEIsVUFBVSxFQUFFO29CQUNSLFFBQVEsRUFBRSxZQUFZLEdBQUcsQ0FBQztpQkFDN0I7YUFDSixDQUFDO1lBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM5QyxNQUFNLE9BQU8sR0FBRztvQkFDWixNQUFNLEVBQUUsTUFBTTtvQkFDZCxVQUFVLEVBQUU7d0JBQ1IsVUFBVSxFQUFFLEVBQUU7cUJBQ2pCO2lCQUNKLENBQUM7Z0JBQ0YsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDakUsT0FBTyxDQUFDLFVBQVUsR0FBRztvQkFDakIsVUFBVSxFQUFFO3dCQUNSLFFBQVEsRUFBRSxDQUFDO3FCQUNkO2lCQUNKLENBQUM7Z0JBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUN6QjtTQUNKO2FBQU0sSUFBSSxZQUFZLEdBQUcsWUFBWSxFQUFFO1lBQ3BDLFVBQVUsQ0FBQyxNQUFNO2dCQUNiLFdBQVcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUMzQyxVQUFVLENBQUMsVUFBVSxHQUFHO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1IsUUFBUSxFQUFFLFlBQVksR0FBRyxDQUFDO2lCQUM3QjthQUNKLENBQUM7WUFDRixPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsR0FBRyxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzlDLE1BQU0sT0FBTyxHQUFHO29CQUNaLE1BQU0sRUFBRSxNQUFNO29CQUNkLFVBQVUsRUFBRTt3QkFDUixVQUFVLEVBQUUsRUFBRTtxQkFDakI7aUJBQ0osQ0FBQztnQkFDRixPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO2dCQUNqRSxPQUFPLENBQUMsVUFBVSxHQUFHO29CQUNqQixVQUFVLEVBQUU7d0JBQ1IsUUFBUSxFQUFFLENBQUMsR0FBRyxDQUFDO3FCQUNsQjtpQkFDSixDQUFDO2dCQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDekI7U0FDSjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELGNBQWMsQ0FBQyxPQUFPO1FBQ2xCLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDO2FBQzNDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNsQixJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDbEI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNmLE1BQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHVEQUF1RCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3hJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDNUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2IsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEI7SUFDTCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLGtHQUFrRztZQUNsRywyQkFBMkI7WUFDM0Isa0RBQWtEO1lBQ2xELE1BQU07WUFDTixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3SixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFO2dCQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ3BDLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUU7d0JBQ3BDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDaEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0NBQ3pHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBQ3ZCO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELE1BQU0sUUFBUSxHQUFHO3dCQUNiLEVBQUUsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRTt3QkFDOUIsVUFBVSxFQUFFLE1BQU07d0JBQ2xCLElBQUksRUFBRSxRQUFRO3dCQUNkLFNBQVMsRUFBRSxRQUFRLENBQUMsTUFBTTtxQkFDN0IsQ0FBQztvQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNqQyxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsT0FBTztTQUVWO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxLQUFLLENBQUMsRUFBRTtZQUN6RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsT0FBTyxDQUFDO1NBQ3hDO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQ3ZGLElBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDcEYsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDekYsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7b0JBQzlDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQztnQkFDSCw2QkFBNkI7WUFDakMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Q0FFSixDQUFBOztZQTNNcUIsWUFBWTtZQUNBLGlCQUFpQjtZQUN2QixXQUFXO1lBQ2hCLFNBQVM7WUFDRyxrQkFBa0I7WUFDakIsbUJBQW1CO1lBQzFCLFlBQVk7O0FBeEI1QjtJQUFSLEtBQUssRUFBRTtxREFBbUI7QUFDakI7SUFBVCxNQUFNLEVBQUU7MERBQTJDO0FBQzFDO0lBQVQsTUFBTSxFQUFFO3lEQUEwQztBQUN6QztJQUFULE1BQU0sRUFBRTs4REFBK0M7QUFDOUM7SUFBVCxNQUFNLEVBQUU7MERBQTJDO0FBQzFDO0lBQVQsTUFBTSxFQUFFOzZEQUE4QztBQUM3QztJQUFULE1BQU0sRUFBRTtnRUFBaUQ7QUFSakQsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLDJ5R0FBeUM7O0tBRTVDLENBQUM7R0FFVyxpQkFBaUIsQ0ErTjdCO1NBL05ZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lZGlhTWF0Y2hlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9sYXlvdXQnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wLCBtb3ZlSXRlbUluQXJyYXksIHRyYW5zZmVyQXJyYXlJdGVtIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uLy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXN3aW0tbGFuZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc3dpbS1sYW5lLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3N3aW0tbGFuZS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU3dpbUxhbmVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHB1YmxpYyB0YXNrQ29uZmlnO1xyXG4gICAgQE91dHB1dCgpIHJlZnJlc2hTd2ltTGluZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGxvYWRpbmdIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgbm90aWZpY2F0aW9uSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGVkaXRUYXNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHRhc2tEZXRhaWxzSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGN1c3RvbVdvcmtmbG93SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIG1vYmlsZVF1ZXJ5OiBNZWRpYVF1ZXJ5TGlzdDtcclxuICAgIHB1YmxpYyBtb2JpbGVRdWVyeUxpc3RlbmVyOiAoKSA9PiB2b2lkO1xyXG5cclxuICAgIHN3aW1MaXN0SWRzID0gW107XHJcbiAgICBzd2ltTGlzdCA9IFtdO1xyXG4gICAgdGFza1N3aW1MYW5lTWluSGVpZ2h0O1xyXG4gICAgcHJvamVjdFN0YXR1c1R5cGU7XHJcbiAgICBpc09uSG9sZFByb2plY3Q6IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIG1lZGlhOiBNZWRpYU1hdGNoZXIsXHJcbiAgICAgICAgcHVibGljIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgICAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlXHJcbiAgICApIHtcclxuICAgICAgICB0aGlzLm1vYmlsZVF1ZXJ5ID0gdGhpcy5tZWRpYS5tYXRjaE1lZGlhKCcobWF4LXdpZHRoOiA2MDBweCknKTtcclxuICAgICAgICB0aGlzLm1vYmlsZVF1ZXJ5TGlzdGVuZXIgPSAoKSA9PiB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICBpZiAodGhpcy5tb2JpbGVRdWVyeS5tYXRjaGVzKSB7XHJcbiAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogZGVwcmVjYXRpb25cclxuICAgICAgICAgICAgdGhpcy5tb2JpbGVRdWVyeS5hZGRMaXN0ZW5lcih0aGlzLm1vYmlsZVF1ZXJ5TGlzdGVuZXIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0VGFzayhldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLmVkaXRUYXNrSGFuZGxlci5lbWl0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblRhc2tEZXRhaWxzKGV2ZW50RGF0YSkge1xyXG4gICAgICAgIHRoaXMudGFza0RldGFpbHNIYW5kbGVyLmVtaXQoZXZlbnREYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ3VzdG9tV29ya2Zsb3coZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0hhbmRsZXIuZW1pdChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbm5lY3RlZElkKGlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3dpbUxpc3RJZHMuZmlsdGVyKGlkcyA9PiBpZHMgIT09IGlkKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VTd2ltTGluZSh0YXNrLCBzdGF0dXMpIHtcclxuICAgIH1cclxuXHJcbiAgICBkcm9wKGV2ZW50OiBDZGtEcmFnRHJvcDxzdHJpbmdbXT4sIGxpc3QpIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzUHJvamVjdE93bmVyKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudC5wcmV2aW91c0NvbnRhaW5lciA9PT0gZXZlbnQuY29udGFpbmVyKSB7XHJcbiAgICAgICAgICAgICAgICBtb3ZlSXRlbUluQXJyYXkoZXZlbnQuY29udGFpbmVyLmRhdGEsIGV2ZW50LnByZXZpb3VzSW5kZXgsIGV2ZW50LmN1cnJlbnRJbmRleCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzVGVtcGxhdGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTdGF0dXMgY2Fubm90IGJlIHVwZGF0ZSBmb3IgYSB0ZW1wbGF0ZVxcJ3MgdGFzaycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRyYW5zZmVyQXJyYXlJdGVtKGV2ZW50LnByZXZpb3VzQ29udGFpbmVyLmRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuY29udGFpbmVyLmRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmlvdXNJbmRleCxcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5jdXJyZW50SW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGFuZ2VTd2ltTGluZShldmVudC5jb250YWluZXIuZGF0YVtldmVudC5jdXJyZW50SW5kZXhdLCBsaXN0Lmxpc3REZXRhaWwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRyb3BDb2x1bW4oZXZlbnQ6IENka0RyYWdEcm9wPHN0cmluZ1tdPikge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNQcm9qZWN0T3duZXIpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3dpbUxpc3QubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgbW92ZUl0ZW1JbkFycmF5KGV2ZW50LmNvbnRhaW5lci5kYXRhLCBldmVudC5wcmV2aW91c0luZGV4LCBldmVudC5jdXJyZW50SW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVTd2ltTGluZShcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5jb250YWluZXIuZGF0YSxcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2aW91c0luZGV4LFxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LmN1cnJlbnRJbmRleFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVTd2ltTGluZShjb250YWluZXJEYXRhLCBwcmV2aW91c0luZGV4LCBjdXJyZW50SW5kZXgpIHtcclxuICAgICAgICBjb25zdCBkcmFnZ2VkTGFuZSA9IGNvbnRhaW5lckRhdGFbY3VycmVudEluZGV4XTtcclxuICAgICAgICBjb25zdCBvYmplY3RzID0gW107XHJcbiAgICAgICAgY29uc3QgdGVtcE9iamVjdCA9IHtcclxuICAgICAgICAgICAgSXRlbUlkOiBOdW1iZXIsXHJcbiAgICAgICAgICAgIHVwZGF0ZURhdGE6IHtcclxuICAgICAgICAgICAgICAgIFByb3BlcnRpZXM6IHt9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IGRyYWdnZWRJbmRleCA9IHByZXZpb3VzSW5kZXg7XHJcbiAgICAgICAgY29uc3QgZHJvcHBlZEluZGV4ID0gY3VycmVudEluZGV4O1xyXG4gICAgICAgIGlmIChkcm9wcGVkSW5kZXggPiBkcmFnZ2VkSW5kZXgpIHtcclxuICAgICAgICAgICAgdGVtcE9iamVjdC5JdGVtSWQgPVxyXG4gICAgICAgICAgICAgICAgY29udGFpbmVyRGF0YVtjdXJyZW50SW5kZXhdLmxpc3REZXRhaWwuSWRlbnRpdHkuSXRlbUlkO1xyXG4gICAgICAgICAgICB0ZW1wT2JqZWN0LnVwZGF0ZURhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgU0VRVUVOQ0U6IGRyb3BwZWRJbmRleCArIDFcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgb2JqZWN0cy5wdXNoKHRlbXBPYmplY3QpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gZHJvcHBlZEluZGV4OyBpID4gZHJhZ2dlZEluZGV4OyBpLS0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBPYmogPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgSXRlbUlkOiBOdW1iZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgdXBkYXRlRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7fVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqLkl0ZW1JZCA9IHRoaXMuc3dpbUxpc3RbaSAtIDFdLmxpc3REZXRhaWwuSWRlbnRpdHkuSXRlbUlkO1xyXG4gICAgICAgICAgICAgICAgdGVtcE9iai51cGRhdGVEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIFByb3BlcnRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgU0VRVUVOQ0U6IGlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgb2JqZWN0cy5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChkcmFnZ2VkSW5kZXggPiBkcm9wcGVkSW5kZXgpIHtcclxuICAgICAgICAgICAgdGVtcE9iamVjdC5JdGVtSWQgPVxyXG4gICAgICAgICAgICAgICAgZHJhZ2dlZExhbmUubGlzdERldGFpbC5JZGVudGl0eS5JdGVtSWQ7XHJcbiAgICAgICAgICAgIHRlbXBPYmplY3QudXBkYXRlRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIFByb3BlcnRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBTRVFVRU5DRTogZHJvcHBlZEluZGV4ICsgMVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBvYmplY3RzLnB1c2godGVtcE9iamVjdCk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSBkcm9wcGVkSW5kZXg7IGkgPCBkcmFnZ2VkSW5kZXg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGVtcE9iaiA9IHtcclxuICAgICAgICAgICAgICAgICAgICBJdGVtSWQ6IE51bWJlcixcclxuICAgICAgICAgICAgICAgICAgICB1cGRhdGVEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb3BlcnRpZXM6IHt9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmouSXRlbUlkID0gdGhpcy5zd2ltTGlzdFtpICsgMV0ubGlzdERldGFpbC5JZGVudGl0eS5JdGVtSWQ7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqLnVwZGF0ZURhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgUHJvcGVydGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBTRVFVRU5DRTogaSArIDJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgb2JqZWN0cy5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc3dpbUxpbmVVcGRhdGUob2JqZWN0cyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3dpbUxpbmVVcGRhdGUob2JqZWN0cykge1xyXG4gICAgICAgIHRoaXMudGFza1NlcnZpY2UudXBkYXRlRHJhZ2FuZERyb3BMYW5lcyhvYmplY3RzKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGRhdGluZyBzd2ltbGFuZSBwb3NpdGlvbicsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hTd2ltTGluZS5uZXh0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hUYXNrKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXNlU3dpbUxpc3QoKSB7XHJcbiAgICAgICAgdGhpcy5zd2ltTGlzdCA9IFtdO1xyXG4gICAgICAgIHRoaXMuc3dpbUxpc3RJZHMgPSBbXTtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnKSB7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRTdGF0dXNCeUlkKHRoaXMudGFza0NvbmZpZy5wcm9qZWN0U3RhdHVzSWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIC8vICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAvLyAgdGhpcy5wcm9qZWN0U3RhdHVzVHlwZSA9IHJlc3BvbnNlLlNUQVRVU19UWVBFO1xyXG4gICAgICAgICAgICAvLyB9KTtcclxuICAgICAgICAgICAgY29uc3QgZGlzcGxheUNvbHVtID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgTVBNRmllbGRDb25zdGFudHMuTVBNX1RBU0tfRklFTERTLlRBU0tfU1RBVFVTX0lELCBNUE1fTEVWRUxTLlRBU0spO1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnRhc2tTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrU3RhdHVzLm1hcChzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxpc3REYXRhID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy50YXNrVG90YWxDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0Lm1hcCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bSwgdGFzaykgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdERhdGEucHVzaCh0YXNrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHN3aW1MaXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxpc3REZXRhaWw6IHN0YXR1cyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogbGlzdERhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFDb3VudDogbGlzdERhdGEubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN3aW1MaXN0SWRzLnB1c2goc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3dpbUxpc3QucHVzaChzd2ltTGlzdCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyAgfSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnICYmIHRoaXMudGFza0NvbmZpZy50YXNrVG90YWxDb3VudCAhPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTd2ltTGFuZU1pbkhlaWdodCA9ICczMXJlbSc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcucHJvamVjdElkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXNzZXRTZXJ2aWNlLmdldFByb2plY3REZXRhaWxzKHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQpLnN1YnNjcmliZShwcm9qZWN0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc09uSG9sZFByb2plY3QgPSBwcm9qZWN0UmVzcG9uc2UuUHJvamVjdC5JU19PTl9IT0xEID09PSAndHJ1ZScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZCh0aGlzLnRhc2tDb25maWcucHJvamVjdFN0YXR1c0lkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFN0YXR1c1R5cGUgPSByZXNwb25zZS5TVEFUVVNfVFlQRTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VTd2ltTGlzdCgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmluaXRpYWxpc2VTd2ltTGlzdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==