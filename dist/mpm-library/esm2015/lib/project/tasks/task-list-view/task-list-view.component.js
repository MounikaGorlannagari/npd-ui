import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Renderer2, SimpleChanges, OnChanges } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { TaskConstants } from '../task.constants';
import { AssetStatusConstants } from '../../asset-card/asset_status_constants';
import { TaskService } from '../task.service';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ProjectService } from '../../../project/shared/services/project.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { DataTypeConstants } from '../../shared/constants/data-type.constants';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { AssetService } from '../../shared/services/asset.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
let TaskListViewComponent = class TaskListViewComponent {
    constructor(sharingService, taskService, formatToLocalePipe, utilService, fieldConfigService, assetService, loaderService, notificationService, projectService, dialog, renderer) {
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.utilService = utilService;
        this.fieldConfigService = fieldConfigService;
        this.assetService = assetService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.dialog = dialog;
        this.renderer = renderer;
        this.refreshTaskListData = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.taskListDataSource = new MatTableDataSource([]);
        this.metaDataDataTypes = DataTypeConstants;
        this.displayableMPMFields = [];
        this.isProjectOwner = false;
        this.displayColumns = [];
        this.assetStatusConstants = AssetStatusConstants;
        this.MPMFieldConstants = MPMFieldConstants.MPM_TASK_FIELDS;
        this.taskTypeConstants = TaskConstants.TASK_TYPE;
        this.page = 1;
        this.skip = 0;
        this.forceDeletion = false;
        this.affectedTask = [];
    }
    editTask(selectedTask) {
        const taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        const isApprovalTask = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_IS_APPROVAL);
        this.taskConfig.isApprovalTask = isApprovalTask === 'true' ? true : false;
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.editTaskHandler.next(this.taskConfig);
            this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.editTaskHandler.next(this.taskConfig);
            this.refresh();
        }
    }
    openCustomWorkflow(selectedTask) {
        this.customWorkflowHandler.next(selectedTask);
    }
    tableDrop(event) {
        let count = 0;
        if (this.displayColumns.find(column => column === 'Active')) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        const orderedFields = [];
        this.displayColumns.forEach(column => {
            if (!(column === 'Active' || column === 'Actions')) {
                orderedFields.push(this.displayableFields.find(displayableField => displayableField.INDEXER_FIELD_ID === column));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    }
    onResizeMouseDown(event, column) {
        event.stopPropagation();
        event.preventDefault();
        const start = event.target;
        const pressed = true;
        const startX = event.x;
        const startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    }
    initResizableColumns(start, pressed, startX, startWidth, column) {
        this.renderer.listen('body', 'mousemove', (event) => {
            if (pressed) {
                const width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', (event) => {
            if (pressed) {
                pressed = false;
                this.resizeDisplayableFields.next();
            }
        });
    }
    openComment(selectedTask) {
    }
    openTaskDetails(selectedTask, isNewDeliverable) {
        this.taskConfig.isNewDeliverable = isNewDeliverable;
        const taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.taskDetailsHandler.next(this.taskConfig);
            this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.taskDetailsHandler.next(this.taskConfig);
            this.refresh();
        }
    }
    isCancelledTask(selectedTask) {
    }
    deleteTask(selectedTask, isForceDelete) {
        const taskId = selectedTask.ID;
        let msg;
        if (this.isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        }
        else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }
        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(approvalTask => {
            console.log(approvalTask);
            if (approvalTask.tuple != undefined) {
                this.forceDeletion = true;
                this.affectedTask = [];
                let tasks;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                }
                else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(task => {
                    if (task.old.TaskView.isDeleted === 'false') {
                        this.affectedTask.push(task.old.TaskView.name);
                    }
                });
            }
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    if (isForceDelete) {
                        this.taskService.forceDeleteTask(taskId, this.isCustomWorkflow).subscribe(response => {
                            this.notificationService.success('Task has been deleted successfully');
                            this.refresh();
                        }, error => {
                            this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                    else {
                        this.taskService.softDeleteTask(taskId, this.isCustomWorkflow).subscribe(response => {
                            this.notificationService.success('Task has been deleted successfully');
                            this.refresh();
                        }, error => {
                            this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                }
            });
        });
    }
    openRequestDetails(selectedTask) {
    }
    showReminder(selectedTask) {
    }
    changeTaskStatus(selectedTask, status) {
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }
    refreshData(taskConfig) {
        this.displayableMPMFields = this.taskConfig.viewConfig ? this.utilService.getDisplayOrderData(this.displayableFields, this.taskConfig.viewConfig.listFieldOrder) : [];
        this.displayColumns = this.displayableMPMFields.map((column) => column.INDEXER_FIELD_ID);
        this.displayColumns = ['Active'].concat(this.displayColumns);
        if (this.isProjectOwner || (this.currentUserId && this.taskConfig.selectedProject &&
            this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id)) {
            this.displayColumns.push('Actions');
        }
        else if (!this.taskConfig.selectedProject) {
            this.displayColumns.push('Actions');
        }
        if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW) {
            this.isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
        }
        // this.taskConfig = taskConfig;
        this.taskListDataSource.data = [];
        this.taskListDataSource.data = this.taskConfig.taskList;
        if (this.sort && this.taskConfig.taskListSortFieldName) {
            this.sort.active = this.taskConfig.taskListSortFieldName;
            this.sort.direction = this.taskConfig.taskListSortOrder.toLowerCase();
        }
        this.taskListDataSource.sort = this.sort;
    }
    refresh() {
        this.refreshTaskListData.next(true);
    }
    getProperty(displayColum, taskData) {
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, taskData);
    }
    getPropertyByMapper(taskData, mapperName) {
        const displayColum = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.TASK);
        return this.getProperty(displayColum, taskData);
    }
    getPriority(taskData, displayColum) {
        const priorityObj = this.utilService.getPriorityIcon(this.getProperty(displayColum, taskData), MPM_LEVELS.TASK);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    }
    pagination(pagination) {
        this.pageSize = pagination.pageSize;
        this.top = pagination.pageSize;
        this.skip = (pagination.pageIndex * pagination.pageSize);
        this.page = 1 + pagination.pageIndex;
        this.pageNumber = this.page;
        this.loadingHandler(true);
        /*if (this.isCampaignAssetView) {
            this.routeService.gotoCampaignAssets(this.urlParams.isTemplate, this.campaignId, this.pageNumber, this.pageSize);
        } else {
            this.routeService.gotoProjectAssets(this.urlParams.isTemplate, this.projectId, this.urlParams.campaignId, this.pageNumber, this.pageSize);
        }
        this.getAssets();*/
    }
    converToLocalDate(obj, path) {
        return this.formatToLocalePipe.transform(this.taskService.converToLocalDate(obj, path));
    }
    /**
     * @since MPMV3-946
     * @param taskType
     */
    getTaskTypeIcon(taskType) {
        return this.taskService.getTaskIconByTaskType(taskType);
    }
    getStatusCheck(selectedTask) {
        return this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_REJECTED;
    }
    getFinalStatusCheck(selectedTask) {
        return this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_REJECTED;
    }
    hasCurrentWorkflowActions(selectedTask) {
        if (this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            const taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
            let taskId;
            if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
                taskId = taskItemId.split('.')[1];
            }
            return this.taskConfig.workflowActionRules.find(workflowActionRule => workflowActionRule.TaskId === taskId) ? true : false;
        }
        else {
            return false;
        }
    }
    getCurrentWorkflowActions(selectedTask) {
        if (this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            const taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
            let taskId;
            if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
                taskId = taskItemId.split('.')[1];
            }
            const currentWorkflowActionRules = this.taskConfig.workflowActionRules.filter(workflowActionRule => workflowActionRule.TaskId === taskId);
            const currentActions = [];
            currentWorkflowActionRules.forEach(rule => {
                if (rule && rule.Action) {
                    if (rule.Action.length > 0) {
                        rule.Action.forEach(action => {
                            const selectedAction = currentActions.find(currentAction => currentAction.Id === action.Id);
                            if (!selectedAction) {
                                currentActions.push(action);
                            }
                        });
                    }
                    else {
                        currentActions.push(rule.Action);
                    }
                }
            });
            return currentActions;
        }
        else {
            return [];
        }
    }
    triggerActionRule(selectedTask, actionId) {
        const taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            const taskId = taskItemId.split('.')[1];
            this.loaderService.show();
            this.taskService.triggerRuleOnAction(taskId, actionId).subscribe(response => {
                this.loaderService.hide();
                this.notificationService.success('Triggering action has been initiated');
            });
        }
    }
    ngOnChanges(changes) {
        if (changes.displayableFields && !changes.displayableFields.firstChange &&
            (JSON.stringify(changes.displayableFields.currentValue) !== JSON.stringify(changes.displayableFields.previousValue))) {
            this.refreshData(this.taskConfig);
        }
    }
    ngOnInit() {
        const appConfig = this.sharingService.getAppConfig();
        this.top = this.utilService.convertStringToNumber(appConfig["DEFAULT_PAGINATION"]);
        this.pageSize = this.utilService.convertStringToNumber(appConfig["DEFAULT_PAGINATION"]);
        this.pageNumber = this.page;
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(projectResponse => {
                this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD === 'true' ? true : false;
                this.isTemplate = this.taskConfig.isTemplate;
                this.currentUserId = this.sharingService.getCurrentUserItemID();
                if (this.currentUserId && this.taskConfig.selectedProject &&
                    this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    this.isProjectOwner = true;
                }
                if (this.taskConfig.metaDataFields && this.taskConfig.taskList) {
                    this.refreshData(this.taskConfig);
                }
                if (!this.taskConfig.taskList) {
                    this.taskConfig.taskList = [];
                }
                if (!this.taskConfig.metaDataFields) {
                    this.taskConfig.metaDataFields = [];
                }
                if (this.sort) {
                    this.sort.disableClear = true;
                    this.sort.sortChange.subscribe(() => {
                        if (this.sort.active && this.sort.direction) {
                            const selectedSortList = {
                                sortType: this.sort.direction.toUpperCase(),
                                selectedSortItem: this.sort.active
                            };
                            this.sortChange.emit(selectedSortList);
                        }
                    });
                }
            });
        }
        /* this.currentUserId = this.sharingService.getCurrentUserItemID();
        if (this.currentUserId && this.taskConfig.selectedProject &&
            this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.isProjectOwner = true;
        }
        if (this.taskConfig.metaDataFields && this.taskConfig.taskList) {
            this.refreshData(this.taskConfig);
        }

        if (!this.taskConfig.taskList) {
            this.taskConfig.taskList = [];
        }

        if (!this.taskConfig.metaDataFields) {
            this.taskConfig.metaDataFields = [];
        }

        if (this.sort) {
            this.sort.disableClear = true;
            this.sort.sortChange.subscribe(() => {
                if (this.sort.active && this.sort.direction) {
                    const selectedSortList = {
                        sortType: this.sort.direction.toUpperCase(),
                        selectedSortItem: this.sort.active
                    };

                    this.sortChange.emit(selectedSortList);
                }
            });
        } */
    }
};
TaskListViewComponent.ctorParameters = () => [
    { type: SharingService },
    { type: TaskService },
    { type: FormatToLocalePipe },
    { type: UtilService },
    { type: FieldConfigService },
    { type: AssetService },
    { type: LoaderService },
    { type: NotificationService },
    { type: ProjectService },
    { type: MatDialog },
    { type: Renderer2 }
];
__decorate([
    Input()
], TaskListViewComponent.prototype, "taskConfig", void 0);
__decorate([
    Input()
], TaskListViewComponent.prototype, "displayableFields", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "refreshTaskListData", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "editTaskHandler", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "taskDetailsHandler", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "sortChange", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "customWorkflowHandler", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "orderedDisplayableFields", void 0);
__decorate([
    Output()
], TaskListViewComponent.prototype, "resizeDisplayableFields", void 0);
__decorate([
    ViewChild(MatSort, { static: true })
], TaskListViewComponent.prototype, "sort", void 0);
TaskListViewComponent = __decorate([
    Component({
        selector: 'mpm-task-list-view',
        template: "<table class=\"task-list\" mat-table [dataSource]=\"taskListDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Active\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-icon [ngClass]=\"[ getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE)==='true' ? 'active-task-icon' : 'inactive-task-icon']\"\r\n                \r\n                matTooltip=\"{{getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE) === 'true' ? 'Active Task' : 'Inactive Task'}}\"><!-- [style.color]=\"getActiveColor(getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE))\" -->\r\n                {{getTaskTypeIcon(getPropertyByMapper(row, MPMFieldConstants.TASK_TYPE))}}</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMPMFields\">y\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':column.DATA_TYPE === 'string'}\">\r\n            <span>\r\n                <div *ngIf=\"(column.MAPPER_NAME !== MPMFieldConstants.TASK_PRIORITY)\">\r\n                    <a class=\"pointer\" matTooltip=\"{{getProperty(column, row)}}\"\r\n                        (click)=\"(column.MAPPER_NAME === MPMFieldConstants.TASK_ID || column.MAPPER_NAME === MPMFieldConstants.TASK_ITEM_ID || column.MAPPER_NAME === MPMFieldConstants.TASK_NAME) && openTaskDetails(row, false)\">{{getProperty(column, row) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === MPMFieldConstants.TASK_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">\r\n                        {{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div>\r\n                <button mat-icon-button matTooltip=\"More\" [matMenuTriggerFor]=\"TaskActions\"\r\n                    [matMenuTriggerData]=\"{selectedTask: row}\"\r\n                    [disabled]=\"!isProjectOwner && (!((row.REFERENCE_LINK !== null) && (row.REFERENCE_LINK_ID !== null)))\">\r\n                    <mat-icon>more_vert</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row.asset_id}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"project-element-row\"></tr>\r\n</table>\r\n<mat-menu #TaskActions=\"matMenu\">\r\n    <ng-template matMenuContent let-selectedTask=\"selectedTask\">\r\n        <button mat-menu-item matTooltip=\"Add New Deliverable\" (click)=\"openTaskDetails(selectedTask, true)\"\r\n            [disabled]=\"getStatusCheck(selectedTask) || !taskConfig.isProjectOwner || taskConfig.isReadOnly || (getPropertyByMapper(selectedTask, MPMFieldConstants.TASK_TYPE) !== taskTypeConstants.NORMAL_TASK)\">\r\n            <mat-icon>add</mat-icon>\r\n            <span>Add New Deliverable</span>\r\n        </button>\r\n        <button mat-menu-item matTooltip=\"Edit Task\" (click)=\"editTask(selectedTask)\"\r\n            [disabled]=\"getFinalStatusCheck(selectedTask) || !taskConfig.isProjectOwner || taskConfig.isReadOnly\">\r\n            <mat-icon>edit</mat-icon>\r\n            <span>Edit Task</span>\r\n        </button>\r\n    </ng-template>\r\n</mat-menu>\r\n<mat-menu #taskStatusMenu=\"matMenu\">\r\n    <ng-template matMenuContent let-selectedTask=\"selectedTask\">\r\n        <span *ngFor=\"let status of taskConfig.taskStatus\">\r\n            <button mat-menu-item matTooltip=\"Task Status\" (click)=\"changeTaskStatus(selectedTask, status)\"\r\n                *ngIf=\"status.DISPLAY_NAME != selectedTask.STATUS_DISPLAY_NAME\">\r\n                <span>{{status.DISPLAY_NAME}}</span>\r\n            </button>\r\n        </span>\r\n    </ng-template>\r\n</mat-menu>\r\n\r\n<!--<div class=\"flex-row\" *ngIf=\"totalAssetsCount > 0\">\r\n    <div class=\"flex-row-item right\">\r\n        <mpm-pagination [length]=\"totalAssetsCount\" [pageSize]=\"pageSize\" [pageIndex]=\"page\"\r\n            (paginator)=\"pagination($event)\"></mpm-pagination>\r\n    </div>\r\n</div>--->\r\n\r\n<div *ngIf=\"taskConfig.taskTotalCount === 0\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No task is available.</button>\r\n</div>",
        styles: ["table.task-list{width:100%}table.task-list tr{height:40px}table.task-list tr th:last-child{padding-left:12px}table.task-list tr td a.mat-button{padding:0;text-align:left}table.task-list tr td mat-icon{font-size:16px}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.active-refrenced-task{background-color:#73736f}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.task-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.task-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.wrap-text-custom span>div{max-width:14em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
    })
], TaskListViewComponent);
export { TaskListViewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1saXN0LXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLWxpc3Qtdmlldy90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvSCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFLaEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNsRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFHL0UsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFZLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBRW5GLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDbkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDRFQUE0RSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQWUsZUFBZSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFPNUIsSUFBYSxxQkFBcUIsR0FBbEMsTUFBYSxxQkFBcUI7SUFtQzlCLFlBQ1csY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsa0JBQXNDLEVBQ3RDLFdBQXdCLEVBQ3hCLGtCQUFzQyxFQUN0QyxZQUEwQixFQUMxQixhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsTUFBaUIsRUFDakIsUUFBbUI7UUFWbkIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLGFBQVEsR0FBUixRQUFRLENBQVc7UUExQ3BCLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNoRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJNUQsdUJBQWtCLEdBQUcsSUFBSSxrQkFBa0IsQ0FBTSxFQUFFLENBQUMsQ0FBQztRQUNyRCxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0Qyx5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBQzVDLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUN0RCxzQkFBaUIsR0FBRyxhQUFhLENBQUMsU0FBUyxDQUFDO1FBQzVDLFNBQUksR0FBRyxDQUFDLENBQUM7UUFDVCxTQUFJLEdBQUcsQ0FBQyxDQUFDO1FBU1Qsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsaUJBQVksR0FBRyxFQUFFLENBQUM7SUFjZCxDQUFDO0lBRUwsUUFBUSxDQUFDLFlBQVk7UUFDakIsTUFBTSxVQUFVLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkcsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2RyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxjQUFjLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMxRSxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNsRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7WUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxZQUFZO1FBQzNCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUE0QjtRQUNsQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxLQUFLLFFBQVEsQ0FBQyxFQUFFO1lBQ3pELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1NBQ3JCO1FBQ0QsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLGFBQWEsR0FBRyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsQ0FBQztRQUM5RixNQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDakMsSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxNQUFNLEtBQUssU0FBUyxDQUFDLEVBQUU7Z0JBQ2hELGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQzthQUNySDtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLE1BQU07UUFDM0IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzNCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQztRQUNyQixNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLE1BQU0sVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTTtRQUMzRCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDaEQsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsTUFBTSxLQUFLLEdBQUcsVUFBVSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDeEI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUM5QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNoQixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDdkM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxXQUFXLENBQUMsWUFBWTtJQUN4QixDQUFDO0lBRUQsZUFBZSxDQUFDLFlBQVksRUFBRSxnQkFBZ0I7UUFDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUNwRCxNQUFNLFVBQVUsR0FBVyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN2RyxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEI7YUFBTSxJQUFJLFVBQVUsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztZQUNwQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEI7SUFDTCxDQUFDO0lBRUQsZUFBZSxDQUFDLFlBQVk7SUFDNUIsQ0FBQztJQUVELFVBQVUsQ0FBQyxZQUFZLEVBQUMsYUFBcUI7UUFDekMsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztRQUMvQixJQUFJLEdBQUcsQ0FBQztRQUNSLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZCLEdBQUcsR0FBRyxxRkFBcUYsQ0FBQztTQUMvRjthQUFNO1lBQ0gsR0FBRyxHQUFHLHFEQUFxRCxDQUFDO1NBQy9EO1FBQ0QsY0FBYztRQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDekIsSUFBSSxZQUFZLENBQUMsS0FBSyxJQUFJLFNBQVMsRUFBRTtnQkFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixJQUFJLEtBQUssQ0FBQztnQkFDVixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3BDLEtBQUssR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDaEM7cUJBQU07b0JBQ0gsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7aUJBQzlCO2dCQUNELEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2pCLElBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLE9BQU8sRUFBRTt3QkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2xEO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFDRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsR0FBRztvQkFDWixJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVk7b0JBQ3ZCLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDckI7YUFDSixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUN2QyxJQUFJLE1BQU0sRUFBRTtvQkFDUixJQUFHLGFBQWEsRUFBRTt3QkFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUNyRSxRQUFRLENBQUMsRUFBRTs0QkFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7NEJBQ3ZFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDbkIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFOzRCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQzt3QkFDcEYsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7eUJBQU07d0JBQ0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsQ0FDcEUsUUFBUSxDQUFDLEVBQUU7NEJBQ1AsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDOzRCQUN2RSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ25CLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTs0QkFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7d0JBQ3BGLENBQUMsQ0FBQyxDQUFDO3FCQUNkO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxZQUFZO0lBQy9CLENBQUM7SUFFRCxZQUFZLENBQUMsWUFBWTtJQUN6QixDQUFDO0lBRUQsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLE1BQU07SUFFckMsQ0FBQztJQUVELGNBQWMsQ0FBQyxLQUFLO1FBQ2hCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDcEUsQ0FBQztJQUVELFdBQVcsQ0FBQyxVQUFVO1FBQ2xCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN0SyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFnQixFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNuRyxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUU3RCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZTtZQUM3RSxJQUFJLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQzlGLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3ZDO2FBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRTtZQUN2RixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUNoSDtRQUNELGdDQUFnQztRQUNoQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDO1FBQ3hELElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFFO1lBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLENBQUM7WUFDekQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6RTtRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztJQUM3QyxDQUFDO0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELFdBQVcsQ0FBQyxZQUFzQixFQUFFLFFBQWE7UUFDN0MsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFDRCxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsVUFBVTtRQUNwQyxNQUFNLFlBQVksR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNELFdBQVcsQ0FBQyxRQUFRLEVBQUUsWUFBWTtRQUM5QixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsRUFDekYsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXJCLE9BQU87WUFDSCxLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUs7WUFDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxXQUFXLENBQUMsT0FBTztTQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVELFVBQVUsQ0FBQyxVQUFlO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztRQUNwQyxJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7UUFDL0IsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBRTVCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUI7Ozs7OzJCQUttQjtJQUN2QixDQUFDO0lBRUQsaUJBQWlCLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDdkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUNEOzs7T0FHRztJQUNILGVBQWUsQ0FBQyxRQUFRO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBQ0QsY0FBYyxDQUFDLFlBQVk7UUFDdkIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxjQUFjO1lBQ2pILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEtBQUssV0FBVyxDQUFDLGVBQWU7WUFDL0csSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDO0lBQ3ZILENBQUM7SUFDRCxtQkFBbUIsQ0FBQyxZQUFZO1FBQzVCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxXQUFXLENBQUMsY0FBYztZQUNqSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxlQUFlO1lBQy9HLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQztJQUN2SCxDQUFDO0lBRUQseUJBQXlCLENBQUMsWUFBWTtRQUNsQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZGLE1BQU0sVUFBVSxHQUFXLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZHLElBQUksTUFBTSxDQUFDO1lBQ1gsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNwRSxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNyQztZQUNELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsRUFBRSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDOUg7YUFBTTtZQUNILE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQztJQUVELHlCQUF5QixDQUFDLFlBQVk7UUFDbEMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN2RixNQUFNLFVBQVUsR0FBVyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2RyxJQUFJLE1BQU0sQ0FBQztZQUNYLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDcEUsTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckM7WUFDRCxNQUFNLDBCQUEwQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLENBQUM7WUFDMUksTUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQzFCLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUN6QixNQUFNLGNBQWMsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQzVGLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0NBQ2pCLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NkJBQy9CO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUNwQztpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxjQUFjLENBQUM7U0FDekI7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsWUFBWSxFQUFFLFFBQVE7UUFDcEMsTUFBTSxVQUFVLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkcsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3BFLE1BQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3hFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FBQztZQUM3RSxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLE9BQU8sQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXO1lBQ25FLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTtZQUN0SCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNyQztJQUNMLENBQUM7SUFFRCxRQUFRO1FBQ0osTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNyRCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztRQUN4RixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDNUIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUN2RixJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBRXBGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBRTdDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNoRSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlO29CQUNyRCxJQUFJLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0YsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7aUJBQzlCO2dCQUNELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7b0JBQzVELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNyQztnQkFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztpQkFDakM7Z0JBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO29CQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7aUJBQ3ZDO2dCQUVELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7d0JBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7NEJBQ3pDLE1BQU0sZ0JBQWdCLEdBQUc7Z0NBQ3JCLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUU7Z0NBQzNDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTs2QkFDckMsQ0FBQzs0QkFFRixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3lCQUMxQztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUVMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUE2Qkk7SUFDUixDQUFDO0NBQ0osQ0FBQTs7WUFwWThCLGNBQWM7WUFDakIsV0FBVztZQUNKLGtCQUFrQjtZQUN6QixXQUFXO1lBQ0osa0JBQWtCO1lBQ3hCLFlBQVk7WUFDWCxhQUFhO1lBQ1AsbUJBQW1CO1lBQ3hCLGNBQWM7WUFDdEIsU0FBUztZQUNQLFNBQVM7O0FBNUNyQjtJQUFSLEtBQUssRUFBRTt5REFBaUM7QUFDaEM7SUFBUixLQUFLLEVBQUU7Z0VBQW1CO0FBQ2pCO0lBQVQsTUFBTSxFQUFFO2tFQUErQztBQUM5QztJQUFULE1BQU0sRUFBRTs4REFBMkM7QUFDMUM7SUFBVCxNQUFNLEVBQUU7aUVBQThDO0FBQzdDO0lBQVQsTUFBTSxFQUFFO3lEQUFzQztBQUNyQztJQUFULE1BQU0sRUFBRTtvRUFBaUQ7QUFDaEQ7SUFBVCxNQUFNLEVBQUU7dUVBQW9EO0FBQ25EO0lBQVQsTUFBTSxFQUFFO3NFQUFtRDtBQUV0QjtJQUFyQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO21EQUFlO0FBWjNDLHFCQUFxQjtJQUxqQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLDZ5SkFBOEM7O0tBRWpELENBQUM7R0FDVyxxQkFBcUIsQ0F3YWpDO1NBeGFZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCwgUmVuZGVyZXIyLCBTaW1wbGVDaGFuZ2VzLCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NvcnQnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFzc2V0U3RhdHVzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vYXNzZXQtY2FyZC9hc3NldF9zdGF0dXNfY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZVBpcGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBQYWdpbmF0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vcGFnaW5hdGlvbi9wYWdpbmF0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcic7XHJcblxyXG5cclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBEYXRhVHlwZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvZGF0YS10eXBlLmNvbnN0YW50cyc7XHJcblxyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNRmllbGQsIE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza0NvbmZpZ0ludGVyZmFjZSB9IGZyb20gJy4uL3Rhc2suY29uZmlnLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IEFzc2V0U2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQ2RrRHJhZ0Ryb3AsIG1vdmVJdGVtSW5BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXRhc2stbGlzdC12aWV3JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYXNrTGlzdFZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KCkgdGFza0NvbmZpZzogVGFza0NvbmZpZ0ludGVyZmFjZTtcclxuICAgIEBJbnB1dCgpIGRpc3BsYXlhYmxlRmllbGRzO1xyXG4gICAgQE91dHB1dCgpIHJlZnJlc2hUYXNrTGlzdERhdGEgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0VGFza0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB0YXNrRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3VzdG9tV29ya2Zsb3dIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3JkZXJlZERpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVzaXplRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAVmlld0NoaWxkKE1hdFNvcnQsIHsgc3RhdGljOiB0cnVlIH0pIHNvcnQ6IE1hdFNvcnQ7XHJcblxyXG4gICAgdGFza0xpc3REYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+KFtdKTtcclxuICAgIG1ldGFEYXRhRGF0YVR5cGVzID0gRGF0YVR5cGVDb25zdGFudHM7XHJcbiAgICBkaXNwbGF5YWJsZU1QTUZpZWxkczogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcbiAgICBpc1Byb2plY3RPd25lciA9IGZhbHNlO1xyXG4gICAgZGlzcGxheUNvbHVtbnMgPSBbXTtcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzID0gQXNzZXRTdGF0dXNDb25zdGFudHM7XHJcbiAgICBNUE1GaWVsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9UQVNLX0ZJRUxEUztcclxuICAgIHRhc2tUeXBlQ29uc3RhbnRzID0gVGFza0NvbnN0YW50cy5UQVNLX1RZUEU7XHJcbiAgICBwYWdlID0gMTtcclxuICAgIHNraXAgPSAwO1xyXG4gICAgcGFnZVNpemU6IG51bWJlcjtcclxuICAgIHRvcDogbnVtYmVyO1xyXG4gICAgcGFnZU51bWJlcjogbnVtYmVyO1xyXG5cclxuICAgIGN1cnJlbnRVc2VySWQ7XHJcbiAgICBpc09uSG9sZFByb2plY3Q6IGJvb2xlYW47XHJcbiAgICBpc0N1c3RvbVdvcmtmbG93O1xyXG4gICAgaXNUZW1wbGF0ZTogYm9vbGVhbjtcclxuICAgIGZvcmNlRGVsZXRpb24gPSBmYWxzZTtcclxuICAgIGFmZmVjdGVkVGFzayA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZm9ybWF0VG9Mb2NhbGVQaXBlOiBGb3JtYXRUb0xvY2FsZVBpcGUsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGVkaXRUYXNrKHNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgIGNvbnN0IHRhc2tJdGVtSWQ6IHN0cmluZyA9IHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICBjb25zdCBpc0FwcHJvdmFsVGFzayA9IHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19JU19BUFBST1ZBTCk7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrID0gaXNBcHByb3ZhbFRhc2sgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMuZWRpdFRhc2tIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXNrSXRlbUlkICYmICEodGFza0l0ZW1JZC5pbmNsdWRlcygnLicpKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0lkID0gdGFza0l0ZW1JZDtcclxuICAgICAgICAgICAgdGhpcy5lZGl0VGFza0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkN1c3RvbVdvcmtmbG93KHNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dIYW5kbGVyLm5leHQoc2VsZWN0ZWRUYXNrKTtcclxuICAgIH1cclxuXHJcbiAgICB0YWJsZURyb3AoZXZlbnQ6IENka0RyYWdEcm9wPHN0cmluZ1tdPikge1xyXG4gICAgICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICAgICAgaWYgKHRoaXMuZGlzcGxheUNvbHVtbnMuZmluZChjb2x1bW4gPT4gY29sdW1uID09PSAnQWN0aXZlJykpIHtcclxuICAgICAgICAgICAgY291bnQgPSBjb3VudCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG1vdmVJdGVtSW5BcnJheSh0aGlzLmRpc3BsYXlDb2x1bW5zLCBldmVudC5wcmV2aW91c0luZGV4ICsgY291bnQsIGV2ZW50LmN1cnJlbnRJbmRleCArIGNvdW50KTtcclxuICAgICAgICBjb25zdCBvcmRlcmVkRmllbGRzID0gW107XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XHJcbiAgICAgICAgICAgIGlmICghKGNvbHVtbiA9PT0gJ0FjdGl2ZScgfHwgY29sdW1uID09PSAnQWN0aW9ucycpKSB7XHJcbiAgICAgICAgICAgICAgICBvcmRlcmVkRmllbGRzLnB1c2godGhpcy5kaXNwbGF5YWJsZUZpZWxkcy5maW5kKGRpc3BsYXlhYmxlRmllbGQgPT4gZGlzcGxheWFibGVGaWVsZC5JTkRFWEVSX0ZJRUxEX0lEID09PSBjb2x1bW4pKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3JkZXJlZERpc3BsYXlhYmxlRmllbGRzLm5leHQob3JkZXJlZEZpZWxkcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNpemVNb3VzZURvd24oZXZlbnQsIGNvbHVtbikge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3Qgc3RhcnQgPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgY29uc3QgcHJlc3NlZCA9IHRydWU7XHJcbiAgICAgICAgY29uc3Qgc3RhcnRYID0gZXZlbnQueDtcclxuICAgICAgICBjb25zdCBzdGFydFdpZHRoID0gJChzdGFydCkucGFyZW50KCkud2lkdGgoKTtcclxuICAgICAgICB0aGlzLmluaXRSZXNpemFibGVDb2x1bW5zKHN0YXJ0LCBwcmVzc2VkLCBzdGFydFgsIHN0YXJ0V2lkdGgsIGNvbHVtbik7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKSB7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2Vtb3ZlJywgKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB3aWR0aCA9IHN0YXJ0V2lkdGggKyAoZXZlbnQueCAtIHN0YXJ0WCk7XHJcbiAgICAgICAgICAgICAgICBjb2x1bW4ud2lkdGggPSB3aWR0aDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIubGlzdGVuKCdib2R5JywgJ21vdXNldXAnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgIHByZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzaXplRGlzcGxheWFibGVGaWVsZHMubmV4dCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIG9wZW5Db21tZW50KHNlbGVjdGVkVGFzaykge1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5UYXNrRGV0YWlscyhzZWxlY3RlZFRhc2ssIGlzTmV3RGVsaXZlcmFibGUpIHtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNOZXdEZWxpdmVyYWJsZSA9IGlzTmV3RGVsaXZlcmFibGU7XHJcbiAgICAgICAgY29uc3QgdGFza0l0ZW1JZDogc3RyaW5nID0gdGhpcy5nZXRQcm9wZXJ0eUJ5TWFwcGVyKHNlbGVjdGVkVGFzaywgdGhpcy5NUE1GaWVsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgdGhpcy50YXNrRGV0YWlsc0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRhc2tJdGVtSWQgJiYgISh0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykpKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tEZXRhaWxzSGFuZGxlci5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpc0NhbmNlbGxlZFRhc2soc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlVGFzayhzZWxlY3RlZFRhc2ssaXNGb3JjZURlbGV0ZTpib29sZWFuKSB7XHJcbiAgICAgICAgY29uc3QgdGFza0lkID0gc2VsZWN0ZWRUYXNrLklEO1xyXG4gICAgICAgIGxldCBtc2c7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDdXN0b21Xb3JrZmxvdykge1xyXG4gICAgICAgICAgICBtc2cgPSAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGUgdGFza3MgYW5kIHJlY29uZmlndXJlIHRoZSB3b3JrZmxvdyBydWxlIGVuZ2luZT8nO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG1zZyA9ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoZSB0YXNrcyBhbmQgRXhpdD8nO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0QWxsQXBwcm92YWxUYXNrcyh0YXNrSWQpLnN1YnNjcmliZShhcHByb3ZhbFRhc2sgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhhcHByb3ZhbFRhc2spXHJcbiAgICAgICAgICAgIGlmIChhcHByb3ZhbFRhc2sudHVwbGUgIT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcmNlRGVsZXRpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZmZlY3RlZFRhc2sgPSBbXTtcclxuICAgICAgICAgICAgICAgIGxldCB0YXNrcztcclxuICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShhcHByb3ZhbFRhc2sudHVwbGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFza3MgPSBbYXBwcm92YWxUYXNrLnR1cGxlXTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFza3MgPSBhcHByb3ZhbFRhc2sudHVwbGU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0YXNrcy5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHRhc2sub2xkLlRhc2tWaWV3LmlzRGVsZXRlZCA9PT0gJ2ZhbHNlJykgeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZmZlY3RlZFRhc2sucHVzaCh0YXNrLm9sZC5UYXNrVmlldy5uYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBtc2csXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdGhpcy5hZmZlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGlzRm9yY2VEZWxldGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5mb3JjZURlbGV0ZVRhc2sodGFza0lkLCB0aGlzLmlzQ3VzdG9tV29ya2Zsb3cpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVGFzayBoYXMgYmVlbiBkZWxldGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRGVsZXRlIFRhc2sgb3BlcmF0aW9uIGZhaWxlZCwgdHJ5IGFnYWluIGxhdGVyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5zb2Z0RGVsZXRlVGFzayh0YXNrSWQsIHRoaXMuaXNDdXN0b21Xb3JrZmxvdykuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1Rhc2sgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0RlbGV0ZSBUYXNrIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuUmVxdWVzdERldGFpbHMoc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1JlbWluZGVyKHNlbGVjdGVkVGFzaykge1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVRhc2tTdGF0dXMoc2VsZWN0ZWRUYXNrLCBzdGF0dXMpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgbG9hZGluZ0hhbmRsZXIoZXZlbnQpIHtcclxuICAgICAgICAoZXZlbnQpID8gdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKSA6IHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaERhdGEodGFza0NvbmZpZykge1xyXG4gICAgICAgIHRoaXMuZGlzcGxheWFibGVNUE1GaWVsZHMgPSB0aGlzLnRhc2tDb25maWcudmlld0NvbmZpZyA/IHRoaXMudXRpbFNlcnZpY2UuZ2V0RGlzcGxheU9yZGVyRGF0YSh0aGlzLmRpc3BsYXlhYmxlRmllbGRzLCB0aGlzLnRhc2tDb25maWcudmlld0NvbmZpZy5saXN0RmllbGRPcmRlcikgOiBbXTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gdGhpcy5kaXNwbGF5YWJsZU1QTUZpZWxkcy5tYXAoKGNvbHVtbjogTVBNRmllbGQpID0+IGNvbHVtbi5JTkRFWEVSX0ZJRUxEX0lEKTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gWydBY3RpdmUnXS5jb25jYXQodGhpcy5kaXNwbGF5Q29sdW1ucyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzUHJvamVjdE93bmVyIHx8ICh0aGlzLmN1cnJlbnRVc2VySWQgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VySWQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zLnB1c2goJ0FjdGlvbnMnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMucHVzaCgnQWN0aW9ucycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0N1c3RvbVdvcmtmbG93ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0aGlzLnRhc2tDb25maWcgPSB0YXNrQ29uZmlnO1xyXG4gICAgICAgIHRoaXMudGFza0xpc3REYXRhU291cmNlLmRhdGEgPSBbXTtcclxuICAgICAgICB0aGlzLnRhc2tMaXN0RGF0YVNvdXJjZS5kYXRhID0gdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0O1xyXG4gICAgICAgIGlmICh0aGlzLnNvcnQgJiYgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0U29ydEZpZWxkTmFtZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNvcnQuYWN0aXZlID0gdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0U29ydEZpZWxkTmFtZTtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LmRpcmVjdGlvbiA9IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRPcmRlci50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy50YXNrTGlzdERhdGFTb3VyY2Uuc29ydCA9IHRoaXMuc29ydDtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgICAgIHRoaXMucmVmcmVzaFRhc2tMaXN0RGF0YS5uZXh0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KGRpc3BsYXlDb2x1bTogTVBNRmllbGQsIHRhc2tEYXRhOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bSwgdGFza0RhdGEpO1xyXG4gICAgfVxyXG4gICAgZ2V0UHJvcGVydHlCeU1hcHBlcih0YXNrRGF0YSwgbWFwcGVyTmFtZSk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgZGlzcGxheUNvbHVtOiBNUE1GaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIG1hcHBlck5hbWUsIE1QTV9MRVZFTFMuVEFTSyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UHJvcGVydHkoZGlzcGxheUNvbHVtLCB0YXNrRGF0YSk7XHJcbiAgICB9XHJcbiAgICBnZXRQcmlvcml0eSh0YXNrRGF0YSwgZGlzcGxheUNvbHVtKSB7XHJcbiAgICAgICAgY29uc3QgcHJpb3JpdHlPYmogPSB0aGlzLnV0aWxTZXJ2aWNlLmdldFByaW9yaXR5SWNvbih0aGlzLmdldFByb3BlcnR5KGRpc3BsYXlDb2x1bSwgdGFza0RhdGEpLFxyXG4gICAgICAgICAgICBNUE1fTEVWRUxTLlRBU0spO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBjb2xvcjogcHJpb3JpdHlPYmouY29sb3IsXHJcbiAgICAgICAgICAgIGljb246IHByaW9yaXR5T2JqLmljb24sXHJcbiAgICAgICAgICAgIHRvb2x0aXA6IHByaW9yaXR5T2JqLnRvb2x0aXBcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHBhZ2luYXRpb24ocGFnaW5hdGlvbjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5wYWdlU2l6ZSA9IHBhZ2luYXRpb24ucGFnZVNpemU7XHJcbiAgICAgICAgdGhpcy50b3AgPSBwYWdpbmF0aW9uLnBhZ2VTaXplO1xyXG4gICAgICAgIHRoaXMuc2tpcCA9IChwYWdpbmF0aW9uLnBhZ2VJbmRleCAqIHBhZ2luYXRpb24ucGFnZVNpemUpO1xyXG4gICAgICAgIHRoaXMucGFnZSA9IDEgKyBwYWdpbmF0aW9uLnBhZ2VJbmRleDtcclxuICAgICAgICB0aGlzLnBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcblxyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIodHJ1ZSk7XHJcbiAgICAgICAgLyppZiAodGhpcy5pc0NhbXBhaWduQXNzZXRWaWV3KSB7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVTZXJ2aWNlLmdvdG9DYW1wYWlnbkFzc2V0cyh0aGlzLnVybFBhcmFtcy5pc1RlbXBsYXRlLCB0aGlzLmNhbXBhaWduSWQsIHRoaXMucGFnZU51bWJlciwgdGhpcy5wYWdlU2l6ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZVNlcnZpY2UuZ290b1Byb2plY3RBc3NldHModGhpcy51cmxQYXJhbXMuaXNUZW1wbGF0ZSwgdGhpcy5wcm9qZWN0SWQsIHRoaXMudXJsUGFyYW1zLmNhbXBhaWduSWQsIHRoaXMucGFnZU51bWJlciwgdGhpcy5wYWdlU2l6ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ2V0QXNzZXRzKCk7Ki9cclxuICAgIH1cclxuXHJcbiAgICBjb252ZXJUb0xvY2FsRGF0ZShvYmosIHBhdGgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKHRoaXMudGFza1NlcnZpY2UuY29udmVyVG9Mb2NhbERhdGUob2JqLCBwYXRoKSk7XHJcbiAgICB9XHJcbiAgICAvKipcclxuICAgICAqIEBzaW5jZSBNUE1WMy05NDZcclxuICAgICAqIEBwYXJhbSB0YXNrVHlwZVxyXG4gICAgICovXHJcbiAgICBnZXRUYXNrVHlwZUljb24odGFza1R5cGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNrU2VydmljZS5nZXRUYXNrSWNvbkJ5VGFza1R5cGUodGFza1R5cGUpO1xyXG4gICAgfVxyXG4gICAgZ2V0U3RhdHVzQ2hlY2soc2VsZWN0ZWRUYXNrKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19TVEFUVVNfVFlQRSkgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEIHx8XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19TVEFUVVNfVFlQRSkgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCB8fFxyXG4gICAgICAgICAgICB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRDtcclxuICAgIH1cclxuICAgIGdldEZpbmFsU3RhdHVzQ2hlY2soc2VsZWN0ZWRUYXNrKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19TVEFUVVNfVFlQRSkgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEIHx8XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19TVEFUVVNfVFlQRSkgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCB8fFxyXG4gICAgICAgICAgICB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRDtcclxuICAgIH1cclxuXHJcbiAgICBoYXNDdXJyZW50V29ya2Zsb3dBY3Rpb25zKHNlbGVjdGVkVGFzayk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcyAmJiB0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhc2tJdGVtSWQ6IHN0cmluZyA9IHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICAgICAgbGV0IHRhc2tJZDtcclxuICAgICAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5pbmNsdWRlcygnLicpICYmIHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICAgICAgdGFza0lkID0gdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcy5maW5kKHdvcmtmbG93QWN0aW9uUnVsZSA9PiB3b3JrZmxvd0FjdGlvblJ1bGUuVGFza0lkID09PSB0YXNrSWQpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFdvcmtmbG93QWN0aW9ucyhzZWxlY3RlZFRhc2spIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMgJiYgdGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXNrSXRlbUlkOiBzdHJpbmcgPSB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgICAgIGxldCB0YXNrSWQ7XHJcbiAgICAgICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgICAgIHRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBjdXJyZW50V29ya2Zsb3dBY3Rpb25SdWxlcyA9IHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzLmZpbHRlcih3b3JrZmxvd0FjdGlvblJ1bGUgPT4gd29ya2Zsb3dBY3Rpb25SdWxlLlRhc2tJZCA9PT0gdGFza0lkKTtcclxuICAgICAgICAgICAgY29uc3QgY3VycmVudEFjdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgY3VycmVudFdvcmtmbG93QWN0aW9uUnVsZXMuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChydWxlICYmIHJ1bGUuQWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJ1bGUuQWN0aW9uLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZS5BY3Rpb24uZm9yRWFjaChhY3Rpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBY3Rpb24gPSBjdXJyZW50QWN0aW9ucy5maW5kKGN1cnJlbnRBY3Rpb24gPT4gY3VycmVudEFjdGlvbi5JZCA9PT0gYWN0aW9uLklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2VsZWN0ZWRBY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50QWN0aW9ucy5wdXNoKGFjdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnRBY3Rpb25zLnB1c2gocnVsZS5BY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiBjdXJyZW50QWN0aW9ucztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gW107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRyaWdnZXJBY3Rpb25SdWxlKHNlbGVjdGVkVGFzaywgYWN0aW9uSWQpIHtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkOiBzdHJpbmcgPSB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5pbmNsdWRlcygnLicpICYmIHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudHJpZ2dlclJ1bGVPbkFjdGlvbih0YXNrSWQsIGFjdGlvbklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdUcmlnZ2VyaW5nIGFjdGlvbiBoYXMgYmVlbiBpbml0aWF0ZWQnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoY2hhbmdlcy5kaXNwbGF5YWJsZUZpZWxkcyAmJiAhY2hhbmdlcy5kaXNwbGF5YWJsZUZpZWxkcy5maXJzdENoYW5nZSAmJlxyXG4gICAgICAgICAgICAoSlNPTi5zdHJpbmdpZnkoY2hhbmdlcy5kaXNwbGF5YWJsZUZpZWxkcy5jdXJyZW50VmFsdWUpICE9PSBKU09OLnN0cmluZ2lmeShjaGFuZ2VzLmRpc3BsYXlhYmxlRmllbGRzLnByZXZpb3VzVmFsdWUpKSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hEYXRhKHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGNvbnN0IGFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy50b3AgPSB0aGlzLnV0aWxTZXJ2aWNlLmNvbnZlcnRTdHJpbmdUb051bWJlcihhcHBDb25maWdbXCJERUZBVUxUX1BBR0lOQVRJT05cIl0pO1xyXG4gICAgICAgIHRoaXMucGFnZVNpemUgPSB0aGlzLnV0aWxTZXJ2aWNlLmNvbnZlcnRTdHJpbmdUb051bWJlcihhcHBDb25maWdbXCJERUZBVUxUX1BBR0lOQVRJT05cIl0pO1xyXG4gICAgICAgIHRoaXMucGFnZU51bWJlciA9IHRoaXMucGFnZTtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0U2VydmljZS5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLnRhc2tDb25maWcucHJvamVjdElkKS5zdWJzY3JpYmUocHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNPbkhvbGRQcm9qZWN0ID0gcHJvamVjdFJlc3BvbnNlLlByb2plY3QuSVNfT05fSE9MRCA9PT0gJ3RydWUnID8gdHJ1ZSA6IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuaXNUZW1wbGF0ZSA9IHRoaXMudGFza0NvbmZpZy5pc1RlbXBsYXRlO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJJZCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRVc2VySWQgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJJZCA9PT0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzUHJvamVjdE93bmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcubWV0YURhdGFGaWVsZHMgJiYgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoRGF0YSh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnRhc2tDb25maWcubWV0YURhdGFGaWVsZHMpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcubWV0YURhdGFGaWVsZHMgPSBbXTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zb3J0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zb3J0LmRpc2FibGVDbGVhciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc29ydC5hY3RpdmUgJiYgdGhpcy5zb3J0LmRpcmVjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRTb3J0TGlzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzb3J0VHlwZTogdGhpcy5zb3J0LmRpcmVjdGlvbi50b1VwcGVyQ2FzZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkU29ydEl0ZW06IHRoaXMuc29ydC5hY3RpdmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLmVtaXQoc2VsZWN0ZWRTb3J0TGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvKiB0aGlzLmN1cnJlbnRVc2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudFVzZXJJZCAmJiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmXHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJJZCA9PT0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcubWV0YURhdGFGaWVsZHMgJiYgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaERhdGEodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdCA9IFtdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnRhc2tDb25maWcubWV0YURhdGFGaWVsZHMpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zb3J0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydC5kaXNhYmxlQ2xlYXIgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNvcnQuc29ydENoYW5nZS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc29ydC5hY3RpdmUgJiYgdGhpcy5zb3J0LmRpcmVjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkU29ydExpc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRUeXBlOiB0aGlzLnNvcnQuZGlyZWN0aW9uLnRvVXBwZXJDYXNlKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkU29ydEl0ZW06IHRoaXMuc29ydC5hY3RpdmVcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNvcnRDaGFuZ2UuZW1pdChzZWxlY3RlZFNvcnRMaXN0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSAqL1xyXG4gICAgfVxyXG59XHJcbiJdfQ==