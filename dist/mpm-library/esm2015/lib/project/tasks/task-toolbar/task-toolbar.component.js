import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormArray } from '@angular/forms';
import { ItemSortComponent } from '../item-sort/item-sort.component';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { TaskConstants } from '../task.constants';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { TaskTypeIconForTask } from '../../shared/constants/task-type-icon.constants';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
import { AssetService } from '../../shared/services/asset.service';
let TaskToolbarComponent = class TaskToolbarComponent {
    constructor(sharingService, commentUtilService, utilService, assetService) {
        this.sharingService = sharingService;
        this.commentUtilService = commentUtilService;
        this.utilService = utilService;
        this.assetService = assetService;
        this.taskConfigChange = new EventEmitter();
        this.createNewTaskHandler = new EventEmitter();
        this.createNewTaskByTypeHandler = new EventEmitter();
        this.viewChange = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.columnChooserHandler = new EventEmitter();
        this.saveColumnChooserHandler = new EventEmitter();
        this.resetColumnChooserHandler = new EventEmitter();
        this.isColumnsFreezable = false;
        this.showTaskViewButton = true;
        this.resources = new FormArray([]);
        this.viewByResources = new FormControl();
        this.taskConstants = TaskConstants;
        this.disableNewTask = false;
        this.taskTypeIcon = TaskTypeIconForTask;
    }
    createNewTask() {
        this.createNewTaskHandler.next(this.taskConfig);
    }
    createNewTaskByType(type) {
        this.taskConfig.taskCreationType = type;
        this.createNewTaskByTypeHandler.next(this.taskConfig);
    }
    refresh() {
        this.taskConfigChange.next(this.taskConfig);
    }
    openCustomWorkflow() {
        this.customWorkflowHandler.next();
    }
    updateDisplayColumns(displayColumns) {
        this.columnChooserHandler.next(displayColumns);
    }
    saveDisplayColumns(displayColumns) {
        this.saveColumnChooserHandler.next(displayColumns);
    }
    resetDisplayColumns() {
        this.resetColumnChooserHandler.next();
    }
    changeView() {
        if (this.taskConfig.isTaskListView) {
            this.taskConfig.isTaskListView = false;
        }
        else {
            this.taskConfig.isTaskListView = true;
        }
        this.viewChange.next(this.taskConfig.isTaskListView);
        this.refresh();
    }
    onSortChange(eventData) {
        if (eventData && eventData.option) {
            if (this.taskConfig.isTaskListView) {
                this.taskConfig.taskListSortFieldName = eventData.option.name;
                this.taskConfig.taskListSortOrder = eventData.sortType;
                this.taskConfig.selectedTaskListSortOption = {
                    option: this.taskConfig.taskListSortOptions.find(data => data.name === this.taskConfig.taskListSortFieldName),
                    sortType: this.taskConfig.taskListSortOrder
                };
            }
            else {
                this.taskConfig.taskCardSortFieldName = eventData.option.name;
                this.taskConfig.taskCardSortOrder = eventData.sortType;
                this.taskConfig.selectedTaskCardSortOption = {
                    option: this.taskConfig.taskCardSortOptions.find(data => data.name === this.taskConfig.taskCardSortFieldName),
                    sortType: this.taskConfig.taskCardSortOrder
                };
            }
        }
        this.taskConfig.isSortChange = true;
        this.taskConfigChange.next(this.taskConfig);
    }
    refreshChild(taskConfig) {
        this.taskConfig = taskConfig;
        if (this.itemSortComponent) {
            this.itemSortComponent.refreshData(this.taskConfig);
        }
    }
    onResourceSelect() {
        this.taskConfig.selectedResources = this.viewByResources.value;
        this.taskConfigChange.next(this.taskConfig);
    }
    onResourceRemoved(resource) {
        const index = this.taskConfig.selectedResources.indexOf(resource);
        this.taskConfig.selectedResources.splice(index, 1);
        this.viewByResources.patchValue(this.taskConfig.selectedResources);
        this.taskConfigChange.next(this.taskConfig);
    }
    ngOnInit() {
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(projectResponse => {
                this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD;
                const currentUSerID = this.sharingService.getCurrentUserItemID();
                if (currentUSerID && this.taskConfig.selectedProject &&
                    currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    this.taskConfig.isProjectOwner = true;
                }
                this.isProjectCompleted = this.taskConfig.isReadOnly;
                console.log(this.taskConfig.IS_ON_HOLD);
                this.appConfig = this.sharingService.getAppConfig();
                this.disableGridView = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]);
            });
        }
        /* const currentUSerID = this.sharingService.getCurrentUserItemID();
        if (currentUSerID && this.taskConfig.selectedProject &&
            currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.taskConfig.isProjectOwner = true;
        }
        this.isProjectCompleted = this.taskConfig.isReadOnly;
        console.log(this.taskConfig.IS_ON_HOLD);
        this.appConfig = this.sharingService.getAppConfig();
        this.disableGridView = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]);
    */ 
    }
};
TaskToolbarComponent.ctorParameters = () => [
    { type: SharingService },
    { type: CommentsUtilService },
    { type: UtilService },
    { type: AssetService }
];
__decorate([
    Input()
], TaskToolbarComponent.prototype, "taskConfig", void 0);
__decorate([
    Input()
], TaskToolbarComponent.prototype, "columnChooserFields", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "taskConfigChange", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "createNewTaskHandler", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "createNewTaskByTypeHandler", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "viewChange", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "customWorkflowHandler", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "columnChooserHandler", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "saveColumnChooserHandler", void 0);
__decorate([
    Output()
], TaskToolbarComponent.prototype, "resetColumnChooserHandler", void 0);
__decorate([
    ViewChild(ItemSortComponent)
], TaskToolbarComponent.prototype, "itemSortComponent", void 0);
TaskToolbarComponent = __decorate([
    Component({
        selector: 'mpm-task-toolbar',
        template: "<mat-toolbar class=\"app-task-toolbar\">\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-row-item task-info-holder\">\r\n            <mpm-item-count *ngIf=\"taskConfig\" [name]=\"'Tasks'\" [count]=\"taskConfig.taskTotalCount\"></mpm-item-count>\r\n            <button mat-icon-button (click)=\"refresh()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\" *ngIf=\"!taskConfig.isTaskListView\">\r\n            <mpm-item-sort [sortOptions]=\"taskConfig.taskSortOptions\" [selectedOption]=\"taskConfig.selectedSortOption\"\r\n                (onSortChange)=\"onSortChange($event)\"></mpm-item-sort>\r\n        </div>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mat-form-field appearance=\"outline\" class=\"resource-select\">\r\n                <mat-label>View by resource</mat-label>\r\n                <mat-select #resourceSelect [formControl]=\"viewByResources\" multiple\r\n                    [disabled]=\"!taskConfig.taskOwnerResources.length\">\r\n                    <mat-select-trigger>\r\n                        {{viewByResources.value ? viewByResources.value[0] : ''}}\r\n                        <span *ngIf=\"viewByResources.value?.length > 1\" class=\"example-additional-selection\">\r\n                            (+{{viewByResources.value.length - 1}}\r\n                            {{viewByResources.value?.length === 2 ? 'other' : 'others'}})\r\n                        </span>\r\n                    </mat-select-trigger>\r\n                    <mat-option *ngFor=\"let resource of taskConfig.taskOwnerResources\" [value]=\"resource\">{{resource}}\r\n                    </mat-option>\r\n                    <button class=\"resource-apply-btn\" (click)=\"onResourceSelect(); resourceSelect.close()\"\r\n                        mat-button>Apply</button>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <button [matMenuTriggerFor]=\"addnewMenu\" mat-raised-button color=\"accent\"\r\n                matTooltip=\"Click to view more options\" [disabled]=\"taskConfig.isReadOnly\">\r\n                <mat-icon>add</mat-icon>\r\n                <span> New</span>\r\n                <mat-icon>arrow_drop_down</mat-icon>\r\n            </button>\r\n            <mat-menu #addnewMenu=\"matMenu\">\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.TASK_WITHOUT_DELIVERABLE}}</mat-icon>\r\n                    <span>Action Task</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.NORMAL_TASK)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.NORMAL_TASK}}</mat-icon>\r\n                    <span>Upload Task</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.APPROVAL_TASK)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.APPROVAL_TASK}}</mat-icon>\r\n                    <span>Approval Task</span>\r\n                </button>\r\n            </mat-menu>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\" *ngIf=\"!disableGridView\">\r\n            <button matTooltip=\"Switch to {{taskConfig.isTaskListView ? 'Card' : 'List'}} View\" mat-button\r\n                (click)=\"changeView()\" *ngIf=\"showTaskViewButton\">\r\n                <mat-icon *ngIf=\"taskConfig.isTaskListView\" class=\"task-action-btn\">view_module\r\n                </mat-icon>\r\n                <mat-icon *ngIf=\"!taskConfig.isTaskListView\" class=\"task-action-btn\">\r\n                    format_list_bulleted\r\n                </mat-icon>\r\n            </button>\r\n        </div>\r\n        <span class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-icon-button>\r\n                <mat-icon class=\"dashboard-action-btn\">settings\r\n                </mat-icon>\r\n            </button>\r\n        </span>\r\n    </mat-toolbar-row>\r\n    <mat-toolbar-row *ngIf=\"taskConfig.selectedResources && taskConfig.selectedResources.length > 0\">\r\n        <span class=\"flex-row\">\r\n            <span class=\"resources-heading\">Resources: </span>\r\n            <mat-chip-list aria-label=\"resource selection\">\r\n                <mat-chip [removable]=\"true\" (removed)=\"onResourceRemoved(resource)\"\r\n                    *ngFor=\"let resource of taskConfig.selectedResources\">{{resource}}\r\n                    <mat-icon matChipRemove>cancel</mat-icon>\r\n                </mat-chip>\r\n            </mat-chip-list>\r\n        </span>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>",
        styles: ["mat-toolbar.app-task-toolbar{background-color:transparent}mat-toolbar .spacer{flex:1 1 auto}.flex-row-item button{margin:5px}.resources-row,.task-resource-icon{font-size:14px}.resourceCheckbox{margin-right:5px;margin-left:5px}.task-icon-btn{margin-top:18px}.task-info-holder{flex-grow:0;align-items:center}.align-items-center{align-items:center}.right{justify-content:flex-end}mat-form-field.resource-select{font-size:11px}::ng-deep .resource-select .mat-form-field-wrapper{padding:0}.flex-grow-0{flex-grow:0}.resources-heading{font-size:16px;margin-right:8px}.resource-apply-btn{width:100%}"]
    })
], TaskToolbarComponent);
export { TaskToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay10b29sYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvdGFzay10b29sYmFyL3Rhc2stdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUN0RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDcEcsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBT25FLElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBMkI3QixZQUNXLGNBQThCLEVBQzlCLGtCQUF1QyxFQUN2QyxXQUF3QixFQUN4QixZQUEwQjtRQUgxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUN2QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQTNCM0IscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMzQyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLCtCQUEwQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckQsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNoRCx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLDZCQUF3QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkQsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5RCx1QkFBa0IsR0FBUyxLQUFLLENBQUE7UUFFaEMsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBRTFCLGNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5QixvQkFBZSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDcEMsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFDOUIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsaUJBQVksR0FBRyxtQkFBbUIsQ0FBQztJQVkvQixDQUFDO0lBRUwsYUFBYTtRQUNULElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxJQUFJO1FBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELGtCQUFrQjtRQUNkLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0lBRUQsb0JBQW9CLENBQUMsY0FBYztRQUMvQixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxjQUFjO1FBQzdCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELG1CQUFtQjtRQUNmLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQsVUFBVTtRQUNOLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1NBQzFDO2FBQU07WUFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDekM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsWUFBWSxDQUFDLFNBQVM7UUFDbEIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUMvQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUM5RCxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxVQUFVLENBQUMsMEJBQTBCLEdBQUc7b0JBQ3pDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDN0csUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCO2lCQUM5QyxDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDOUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDO2dCQUN2RCxJQUFJLENBQUMsVUFBVSxDQUFDLDBCQUEwQixHQUFHO29CQUN6QyxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLENBQUM7b0JBQzdHLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtpQkFDOUMsQ0FBQzthQUNMO1NBQ0o7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELFlBQVksQ0FBQyxVQUFVO1FBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQzdCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3hCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3ZEO0lBQ0wsQ0FBQztJQUVELGdCQUFnQjtRQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUM7UUFDL0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELGlCQUFpQixDQUFDLFFBQVE7UUFDdEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRW5ELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7WUFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsRUFBRTtnQkFDdkYsSUFBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQztnQkFDMUQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNqRSxJQUFJLGFBQWEsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWU7b0JBQ2hELGFBQWEsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hGLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztpQkFDekM7Z0JBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDO2dCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7WUFFekksQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNEOzs7Ozs7Ozs7TUFTRjtJQUFDLENBQUM7Q0FDUCxDQUFBOztZQWpIOEIsY0FBYztZQUNWLG1CQUFtQjtZQUMxQixXQUFXO1lBQ1YsWUFBWTs7QUE3QjVCO0lBQVIsS0FBSyxFQUFFO3dEQUFZO0FBQ1g7SUFBUixLQUFLLEVBQUU7aUVBQXFCO0FBQ25CO0lBQVQsTUFBTSxFQUFFOzhEQUE0QztBQUMzQztJQUFULE1BQU0sRUFBRTtrRUFBZ0Q7QUFDL0M7SUFBVCxNQUFNLEVBQUU7d0VBQXNEO0FBQ3JEO0lBQVQsTUFBTSxFQUFFO3dEQUFzQztBQUNyQztJQUFULE1BQU0sRUFBRTttRUFBaUQ7QUFDaEQ7SUFBVCxNQUFNLEVBQUU7a0VBQWdEO0FBQy9DO0lBQVQsTUFBTSxFQUFFO3NFQUFvRDtBQUNuRDtJQUFULE1BQU0sRUFBRTt1RUFBcUQ7QUFjaEM7SUFBN0IsU0FBUyxDQUFDLGlCQUFpQixDQUFDOytEQUFzQztBQXpCMUQsb0JBQW9CO0lBTmhDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsNnBMQUE0Qzs7S0FFL0MsQ0FBQztHQUVXLG9CQUFvQixDQTZJaEM7U0E3SVksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sLCBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEl0ZW1Tb3J0Q29tcG9uZW50IH0gZnJvbSAnLi4vaXRlbS1zb3J0L2l0ZW0tc29ydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29tbWVudHMvc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1R5cGVJY29uRm9yVGFzayB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvdGFzay10eXBlLWljb24uY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tdGFzay10b29sYmFyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90YXNrLXRvb2xiYXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdGFzay10b29sYmFyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBUYXNrVG9vbGJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgdGFza0NvbmZpZztcclxuICAgIEBJbnB1dCgpIGNvbHVtbkNob29zZXJGaWVsZHM7XHJcbiAgICBAT3V0cHV0KCkgdGFza0NvbmZpZ0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNyZWF0ZU5ld1Rhc2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3JlYXRlTmV3VGFza0J5VHlwZUhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB2aWV3Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3VzdG9tV29ya2Zsb3dIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY29sdW1uQ2hvb3NlckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSByZXNldENvbHVtbkNob29zZXJIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBpc0NvbHVtbnNGcmVlemFibGU6Ym9vbGVhbj1mYWxzZVxyXG4gICAgaXNQcm9qZWN0Q29tcGxldGVkOiBib29sZWFuO1xyXG4gICAgc2hvd1Rhc2tWaWV3QnV0dG9uID0gdHJ1ZTtcclxuICAgIHNlbGVjdGVkT3B0aW9uO1xyXG4gICAgcmVzb3VyY2VzID0gbmV3IEZvcm1BcnJheShbXSk7XHJcbiAgICB2aWV3QnlSZXNvdXJjZXMgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICAgIHRhc2tDb25zdGFudHMgPSBUYXNrQ29uc3RhbnRzO1xyXG4gICAgZGlzYWJsZU5ld1Rhc2sgPSBmYWxzZTtcclxuICAgIHRhc2tUeXBlSWNvbiA9IFRhc2tUeXBlSWNvbkZvclRhc2s7XHJcbiAgICBkaXNhYmxlR3JpZFZpZXc6IGJvb2xlYW47XHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIGlzT25Ib2xkUHJvamVjdDogYm9vbGVhbjtcclxuICAgIGVuYWJsZUFwcHJvdmFsVGFzazogYm9vbGVhbjtcclxuICAgIEBWaWV3Q2hpbGQoSXRlbVNvcnRDb21wb25lbnQpIGl0ZW1Tb3J0Q29tcG9uZW50OiBJdGVtU29ydENvbXBvbmVudDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXNzZXRTZXJ2aWNlOiBBc3NldFNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgY3JlYXRlTmV3VGFzaygpIHtcclxuICAgICAgICB0aGlzLmNyZWF0ZU5ld1Rhc2tIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVOZXdUYXNrQnlUeXBlKHR5cGUpIHtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0NyZWF0aW9uVHlwZSA9IHR5cGU7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVOZXdUYXNrQnlUeXBlSGFuZGxlci5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWdDaGFuZ2UubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5DdXN0b21Xb3JrZmxvdygpIHtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93SGFuZGxlci5uZXh0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlRGlzcGxheUNvbHVtbnMoZGlzcGxheUNvbHVtbnMpIHtcclxuICAgICAgICB0aGlzLmNvbHVtbkNob29zZXJIYW5kbGVyLm5leHQoZGlzcGxheUNvbHVtbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNhdmVEaXNwbGF5Q29sdW1ucyhkaXNwbGF5Q29sdW1ucykge1xyXG4gICAgICAgIHRoaXMuc2F2ZUNvbHVtbkNob29zZXJIYW5kbGVyLm5leHQoZGlzcGxheUNvbHVtbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0RGlzcGxheUNvbHVtbnMoKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldENvbHVtbkNob29zZXJIYW5kbGVyLm5leHQoKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VWaWV3KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzVGFza0xpc3RWaWV3ID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzVGFza0xpc3RWaWV3ID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy52aWV3Q2hhbmdlLm5leHQodGhpcy50YXNrQ29uZmlnLmlzVGFza0xpc3RWaWV3KTtcclxuICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgIH1cclxuXHJcbiAgICBvblNvcnRDaGFuZ2UoZXZlbnREYXRhKSB7XHJcbiAgICAgICAgaWYgKGV2ZW50RGF0YSAmJiBldmVudERhdGEub3B0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRGaWVsZE5hbWUgPSBldmVudERhdGEub3B0aW9uLm5hbWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0xpc3RTb3J0T3JkZXIgPSBldmVudERhdGEuc29ydFR5cGU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRUYXNrTGlzdFNvcnRPcHRpb24gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3B0aW9uOiB0aGlzLnRhc2tDb25maWcudGFza0xpc3RTb3J0T3B0aW9ucy5maW5kKGRhdGEgPT4gZGF0YS5uYW1lID09PSB0aGlzLnRhc2tDb25maWcudGFza0xpc3RTb3J0RmllbGROYW1lKSxcclxuICAgICAgICAgICAgICAgICAgICBzb3J0VHlwZTogdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0U29ydE9yZGVyXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tDYXJkU29ydEZpZWxkTmFtZSA9IGV2ZW50RGF0YS5vcHRpb24ubmFtZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrQ2FyZFNvcnRPcmRlciA9IGV2ZW50RGF0YS5zb3J0VHlwZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFRhc2tDYXJkU29ydE9wdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgICAgICBvcHRpb246IHRoaXMudGFza0NvbmZpZy50YXNrQ2FyZFNvcnRPcHRpb25zLmZpbmQoZGF0YSA9PiBkYXRhLm5hbWUgPT09IHRoaXMudGFza0NvbmZpZy50YXNrQ2FyZFNvcnRGaWVsZE5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICAgIHNvcnRUeXBlOiB0aGlzLnRhc2tDb25maWcudGFza0NhcmRTb3J0T3JkZXJcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzU29ydENoYW5nZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnQ2hhbmdlLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoQ2hpbGQodGFza0NvbmZpZykge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZyA9IHRhc2tDb25maWc7XHJcbiAgICAgICAgaWYgKHRoaXMuaXRlbVNvcnRDb21wb25lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5pdGVtU29ydENvbXBvbmVudC5yZWZyZXNoRGF0YSh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblJlc291cmNlU2VsZWN0KCkge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFJlc291cmNlcyA9IHRoaXMudmlld0J5UmVzb3VyY2VzLnZhbHVlO1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZ0NoYW5nZS5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNvdXJjZVJlbW92ZWQocmVzb3VyY2UpIHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFJlc291cmNlcy5pbmRleE9mKHJlc291cmNlKTtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRSZXNvdXJjZXMuc3BsaWNlKGluZGV4LCAxKTtcclxuXHJcbiAgICAgICAgdGhpcy52aWV3QnlSZXNvdXJjZXMucGF0Y2hWYWx1ZSh0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRSZXNvdXJjZXMpO1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZ0NoYW5nZS5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgdGhpcy5hc3NldFNlcnZpY2UuZ2V0UHJvamVjdERldGFpbHModGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkuc3Vic2NyaWJlKHByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzT25Ib2xkUHJvamVjdCA9IHByb2plY3RSZXNwb25zZS5Qcm9qZWN0LklTX09OX0hPTEQ7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50VVNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRVU2VySUQgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRVU2VySUQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzUHJvamVjdE93bmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuaXNQcm9qZWN0Q29tcGxldGVkID0gdGhpcy50YXNrQ29uZmlnLmlzUmVhZE9ubHk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRhc2tDb25maWcuSVNfT05fSE9MRCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVHcmlkVmlldyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRJU0FCTEVfR1JJRF9WSUVXXSk7XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyogY29uc3QgY3VycmVudFVTZXJJRCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTtcclxuICAgICAgICBpZiAoY3VycmVudFVTZXJJRCAmJiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmXHJcbiAgICAgICAgICAgIGN1cnJlbnRVU2VySUQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNQcm9qZWN0Q29tcGxldGVkID0gdGhpcy50YXNrQ29uZmlnLmlzUmVhZE9ubHk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy50YXNrQ29uZmlnLklTX09OX0hPTEQpO1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICB0aGlzLmRpc2FibGVHcmlkVmlldyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRJU0FCTEVfR1JJRF9WSUVXXSk7XHJcbiAgICAqLyB9XHJcbn1cclxuIl19