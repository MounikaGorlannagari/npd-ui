import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { SortConstants } from '../../../shared/constants/sort.constants';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
let ItemSortComponent = class ItemSortComponent {
    constructor(sharingService) {
        this.sharingService = sharingService;
        this.onSortChange = new EventEmitter();
    }
    selectSorting() {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.onSortChange.next(this.selectedOption);
    }
    onSelectionChange(event, fields) {
        if (event && event.currentTarget.value) {
            fields.map(option => {
                if (option.name === event.currentTarget.value) {
                    this.selectedOption.option = option;
                }
            });
        }
        this.onSortChange.next(this.selectedOption);
    }
    refreshData(taskConfig) {
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    }
    ngOnInit() {
        this.appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    }
};
ItemSortComponent.ctorParameters = () => [
    { type: SharingService }
];
__decorate([
    Input()
], ItemSortComponent.prototype, "sortOptions", void 0);
__decorate([
    Input()
], ItemSortComponent.prototype, "selectedOption", void 0);
__decorate([
    Output()
], ItemSortComponent.prototype, "onSortChange", void 0);
ItemSortComponent = __decorate([
    Component({
        selector: 'mpm-item-sort',
        template: "<div class=\"flex-row-item justify-flex-end\" *ngIf=\"sortOptions && sortOptions.length > 0\">\r\n    <button class=\" sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption.option.displayName}}\"\r\n        [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n        <span>Sort by: {{selectedOption.option.displayName}}</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n    </button>\r\n    <button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n        mat-icon-button>\r\n        <mat-icon *ngIf=\"selectedOption.sortType=='desc'\">arrow_downward</mat-icon>\r\n        <mat-icon *ngIf=\"selectedOption.sortType=='asc'\">arrow_upward</mat-icon>\r\n    </button>\r\n    <mat-menu #sortOrderListMenu=\"matMenu\">\r\n        <span *ngFor=\"let fieldOption of sortOptions\">\r\n            <button *ngIf=\"selectedOption.option.name != fieldOption.name\" mat-menu-item\r\n                matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n                [value]=\"fieldOption.name\">\r\n                <span>{{fieldOption.displayName}}</span>\r\n            </button>\r\n        </span>\r\n    </mat-menu>\r\n</div>",
        styles: [".flex-row-item button{margin:5px}"]
    })
], ItemSortComponent);
export { ItemSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1zb3J0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvaXRlbS1zb3J0L2l0ZW0tc29ydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLGNBQWMsRUFBQyxNQUFNLDZDQUE2QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQVNwRyxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQU8xQixZQUNXLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUwvQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFNN0MsQ0FBQztJQUNMLGFBQWE7UUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDckcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsTUFBTTtRQUMzQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtZQUNwQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNoQixJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztpQkFDdkM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxXQUFXLENBQUMsVUFBVTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLHlFQUF5RTtnQkFDekUsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMzSztpQkFBTTtnQkFDSCx3REFBd0Q7Z0JBQ3hELElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMxSjtTQUNKO0lBQ0wsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQix5RUFBeUU7Z0JBQ3pFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDM0s7aUJBQU07Z0JBQ0gsd0RBQXdEO2dCQUN4RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUo7U0FDSjtJQUNMLENBQUM7Q0FDSixDQUFBOztZQTNDOEIsY0FBYzs7QUFQaEM7SUFBUixLQUFLLEVBQUU7c0RBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTt5REFBZ0I7QUFDZDtJQUFULE1BQU0sRUFBRTt1REFBd0M7QUFIeEMsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxlQUFlO1FBQ3pCLHV1Q0FBeUM7O0tBRTVDLENBQUM7R0FFVyxpQkFBaUIsQ0FtRDdCO1NBbkRZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2V9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTb3J0Q29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zb3J0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWl0ZW0tc29ydCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaXRlbS1zb3J0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2l0ZW0tc29ydC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgSXRlbVNvcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KCkgc29ydE9wdGlvbnM7XHJcbiAgICBASW5wdXQoKSBzZWxlY3RlZE9wdGlvbjtcclxuICAgIEBPdXRwdXQoKSBvblNvcnRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIFxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZVxyXG4gICAgKSB7IH1cclxuICAgIHNlbGVjdFNvcnRpbmcoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9ICh0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlID09PSBTb3J0Q29uc3RhbnRzLkFTQykgPyAnZGVzYycgOiAnYXNjJztcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlID0gdGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZTtcclxuICAgICAgICB0aGlzLm9uU29ydENoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2VsZWN0aW9uQ2hhbmdlKGV2ZW50LCBmaWVsZHMpIHtcclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZSkge1xyXG4gICAgICAgICAgICBmaWVsZHMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9uLm5hbWUgPT09IGV2ZW50LmN1cnJlbnRUYXJnZXQudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLm9wdGlvbiA9IG9wdGlvbjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub25Tb3J0Q2hhbmdlLm5leHQodGhpcy5zZWxlY3RlZE9wdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaERhdGEodGFza0NvbmZpZykge1xyXG4gICAgICAgIGlmICghdGhpcy5zZWxlY3RlZE9wdGlvbikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zb3J0T3B0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHRoaXMuc29ydE9wdGlvbnNbMF0sIHNvcnRUeXBlOiAnYXNjJyB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSA9PT0gJ0FTQycgPyAnYXNjJyA6ICdkZXNjJ307XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiAnYXNjJyB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB7fSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc29ydE9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=