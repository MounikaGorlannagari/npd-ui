import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { NotificationService } from '../../../notification/notification.service';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { StatusService } from '../../../shared/services/status.service';
import { ProjectUtilService } from '../../shared/services/project-util.service';
import { TaskService } from '../task.service';
let CustomWorkflowFieldComponent = class CustomWorkflowFieldComponent {
    constructor(statusService, sharingService, taskService, notificationService, projectUtilService, dialog) {
        this.statusService = statusService;
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.notificationService = notificationService;
        this.projectUtilService = projectUtilService;
        this.dialog = dialog;
        this.closeCallbackHandler = new EventEmitter();
        this.removeWorkflowRuleHandler = new EventEmitter();
        this.enableInitiateWorkflowHandler = new EventEmitter();
        //@Output() isTaskLevelRuleEdit = new EventEmitter<any>();
        this.istaskRuleEdit = false;
        this.ruleGroupHandler = new EventEmitter();
        this.ruleDependencyErrorHandler = new EventEmitter();
        //@Input() allRuleListGroup; MVSS-322
        this.isChildDependecies = false;
        this.triggerTypes = [{
                name: 'Status',
                value: 'STATUS'
            }, {
                name: 'Action',
                value: 'ACTION'
            }];
        this.targetTypes = [{
                name: 'Task',
                value: 'TASK'
            }, {
                name: 'Event',
                value: 'EVENT'
            }];
        this.triggerData = [];
        this.targetData = [];
        this.taskList = [];
        this.allRuleList = [];
        this.currentSourceTasks = [];
        this.noPredecessorRuleList = [];
        this.httpRequestArray = [];
    }
    //for Editing workflow Rules...
    updateWorkflowRule() {
        if (this.ruleData.currentTask.length > this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            //Removing a rule from the source tasks list
            const newTasks = [];
            this.ruleData.currentTask.forEach(task => {
                const index = this.customWorkflowFieldForm.getRawValue().currentTask.indexOf(task);
                if (index < 0) {
                    newTasks.push(task);
                }
            });
            let deletedRules = [];
            newTasks.forEach(task => {
                const rule = {
                    currentTask: task,
                    isInitialRule: this.ruleData.isInitialRule,
                    targetData: this.ruleData.targetData,
                    targetType: this.ruleData.targetType,
                    triggerData: this.ruleData.triggerData,
                    triggerType: this.ruleData.triggerType
                };
                deletedRules.push(rule);
            });
            let originalRules = [];
            for (let i = 0; i < this.ruleData.currentTask.length; i++) {
                const rule = {
                    currentTask: this.ruleData.currentTask[i],
                    isInitialRule: this.ruleData.isInitialRule,
                    targetData: this.ruleData.targetData,
                    targetType: this.ruleData.targetType,
                    triggerData: this.ruleData.triggerData,
                    triggerType: this.ruleData.triggerType,
                    workflowRuleId: this.ruleData.workflowRuleId[i]
                };
                originalRules.push(rule);
            }
            let deletedRuleIds = [];
            deletedRules.forEach(rule => {
                originalRules.forEach(eachRule => {
                    if (rule.currentTask == eachRule.currentTask) {
                        deletedRuleIds.push(eachRule.workflowRuleId);
                    }
                });
            });
            const otherParamsChanged = ((this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType)
                || (this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData)
                || (this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType)
                || (this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData)
                || (this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule));
            if (otherParamsChanged) {
                for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                    const rule = this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                    this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i]));
                }
            }
            let approvalTasks = this.taskData.filter(task => task.IS_APPROVAL_TASK === 'true');
            let removedParentTasks = [];
            approvalTasks.forEach(approvalTask => {
                const index = newTasks.indexOf(String(approvalTask.TASK_PARENT_ID));
                if (index >= 0) {
                    removedParentTasks.push(newTasks[index]);
                }
            });
            //checking if the removed task is a parent task hence checking dependencies...
            /************************************************************************************************* */
            let approvalTasksIdList = [];
            removedParentTasks.forEach(eachTask => {
                const approvalTaskIds = this.taskData.filter(task => String(task.TASK_PARENT_ID) == eachTask);
                approvalTasksIdList.push(...approvalTaskIds);
            });
            let allTasksinRules = [];
            this.ruleListGroup.forEach(rule => {
                allTasksinRules.push(...rule.currentTask);
                allTasksinRules.push(rule.targetData);
            });
            let affectedApprovalTasks = [];
            approvalTasksIdList.forEach(eachTask => {
                const index = allTasksinRules.indexOf(eachTask.ID);
                if (index >= 0) {
                    affectedApprovalTasks.push(eachTask);
                }
            });
            this.ruleListGroup.forEach(rule => rule.hasPredecessor = true);
            this.ruleListGroup.forEach(rule => {
                let ruleTasks = [];
                ruleTasks.push(...rule.currentTask);
                ruleTasks.push(rule.targetData);
                affectedApprovalTasks.forEach(eachTask => {
                    const index = ruleTasks.indexOf(eachTask.ID);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                    }
                });
            });
            /**************************************************************************************************************** */
            if (!(affectedApprovalTasks && affectedApprovalTasks.length > 0)) {
                let ruleIds = [];
                deletedRuleIds.forEach(workflowId => {
                    const Rule = {
                        Id: workflowId
                    };
                    ruleIds.push(Rule);
                });
                this.taskService.removeWorkflowRule(ruleIds, {}).subscribe(response => {
                    if (response) {
                        // this.removeWorkflowRuleHandler.next(this.ruleData);
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach(task => this.currentSourceTasks.push(task));
                        this.ruleData.currentTask = this.customWorkflowFieldForm.controls.currentTask.value;
                        this.isRuleSaved = true;
                        this.enableInitiateWorkflowHandler.next();
                        this.notificationService.info('Workflow Rule has been removed');
                    }
                    else {
                        this.notificationService.error('Something went wrong while removing workflow rule');
                    }
                });
            }
            else {
                this.notificationService.error('Edit Operation could not be Performed because It is a Parent Task and it Will affect the highlighted rules');
            }
        }
        else if (this.ruleData.currentTask.length < this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            //Adding a rule to the source tasks list
            const newTasks = [];
            this.customWorkflowFieldForm.getRawValue().currentTask.forEach(task => {
                const index = this.ruleData.currentTask.indexOf(task);
                if (index < 0) {
                    newTasks.push(task);
                }
            });
            this.httpRequestArray = [];
            if ((this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType)
                || (this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData)
                || (this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType)
                || (this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData)
                || (this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule)) {
                for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                    const rule = this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                    this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i]));
                }
            }
            else {
                newTasks.forEach(task => {
                    const rule = this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = task;
                    this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, ''));
                });
            }
        }
    }
    updateValidation() {
        let allSourceTasks = [];
        if (!this.isTaskLevelcalled) {
            this.ruleList.forEach(rule => {
                if (!rule.isInitialRule) {
                    allSourceTasks.push(...rule.currentTask);
                }
            });
        }
        else {
            this.taskruleListGroup.forEach(rule => {
                if (!rule.isInitialRule) {
                    allSourceTasks.push(...rule.currentTask);
                }
            });
        }
        const index = allSourceTasks.indexOf(this.ruleData.targetData);
        if (index >= 0) {
            this.customWorkflowFieldForm.controls.currentTask.setValue(this.ruleData.currentTask);
            this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            this.isRuleSaved = true;
            this.notificationService.error("Cannot edit targetTask as it has dependencies");
        }
        else if (JSON.stringify(this.ruleData) !== JSON.stringify(this.customWorkflowFieldForm.getRawValue() && !this.isTaskLevelcalled)
            && this.ruleData.isExist
            && this.ruleData.currentTask.length != this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            this.updateWorkflowRule();
        }
        else if (!this.isTaskLevelcalled) {
            this.httpRequestArray = [];
            for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                const workflowRule = this.customWorkflowFieldForm.getRawValue();
                workflowRule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                this.httpRequestArray.push(this.taskService.createWorkflowRule(workflowRule, this.projectId, this.ruleData.workflowRuleId[i]));
            }
        }
    }
    checkTaskDependencies() {
        let allSourceTasks = [];
        this.taskruleListGroup.forEach(rule => {
            if (!rule.isInitialRule) {
                allSourceTasks.push(...rule.currentTask);
            }
        });
        const index = allSourceTasks.indexOf(this.ruleData.targetData);
        if (index >= 0) {
            this.customWorkflowFieldForm.controls.currentTask.setValue(this.ruleData.currentTask);
            this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            this.isRuleSaved = true;
            this.notificationService.error("Cannot edit targetTask as it has dependencies");
            this.isChildDependecies = true;
        }
        return this.isChildDependecies;
    }
    addWorkflowRule() {
        if (this.isTaskLevelcalled && !this.checkTaskDependencies()) {
            this.httpRequestArray = [];
            for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                var workflowFlowObject = [];
                workflowFlowObject.push({
                    currentTask: this.customWorkflowFieldForm.controls.currentTask.value[i],
                    triggerType: this.customWorkflowFieldForm.controls.triggerType.value,
                    targetType: this.customWorkflowFieldForm.controls.targetType.value,
                    triggerData: this.customWorkflowFieldForm.controls.triggerData.value,
                    targetData: this.customWorkflowFieldForm.controls.targetData.value,
                    isInitialRule: this.customWorkflowFieldForm.controls.isInitialRule.value,
                });
                this.httpRequestArray.push(this.taskService.createWorkflowRule(JSON.parse(JSON.stringify(workflowFlowObject[0])), this.projectId, this.ruleData.workflowRuleId[i]));
                // this.isRuleSaved= true
                // this.istaskRuleEdit=true
                // this.enableInitiateWorkflowHandler.next(this.istaskRuleEdit);
            }
        }
        else if (this.ruleData.targetData !== this.customWorkflowFieldForm.getRawValue().targetData && this.ruleData.isExist && !this.isTaskLevelcalled) {
            this.updateValidation();
        }
        else if (JSON.stringify(this.ruleData) !== JSON.stringify(this.customWorkflowFieldForm.getRawValue() && !this.isTaskLevelcalled)
            && this.ruleData.isExist
            && this.ruleData.currentTask.length != this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            this.updateWorkflowRule();
        }
        else if (!this.isTaskLevelcalled) {
            this.httpRequestArray = [];
            for (let i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                const workflowRule = this.customWorkflowFieldForm.getRawValue();
                workflowRule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                this.httpRequestArray.push(this.taskService.createWorkflowRule(workflowRule, this.projectId, this.ruleData.workflowRuleId[i]));
            }
        }
        forkJoin(this.httpRequestArray).subscribe(response => {
            if (response) {
                this.ruleData.currentTask = [this.customWorkflowFieldForm.getRawValue().currentTask];
                this.ruleData.triggerType = this.customWorkflowFieldForm.getRawValue().triggerType;
                this.ruleData.triggerData = this.customWorkflowFieldForm.getRawValue().triggerData;
                this.ruleData.targetType = this.customWorkflowFieldForm.getRawValue().targetType;
                this.ruleData.targetData = this.customWorkflowFieldForm.getRawValue().targetData;
                this.ruleData.isInitialRule = this.customWorkflowFieldForm.getRawValue().isInitialRule;
                this.ruleData.isRuleAdded = true;
                this.ruleData.workflowRuleId = [];
                for (let i = 0; i < response.length; i++) {
                    this.ruleData.workflowRuleId.push(response[i]['Workflow_Rules-id'] && response[i]['Workflow_Rules-id'].Id ? response[i]['Workflow_Rules-id'].Id : null);
                }
                this.ruleGroupHandler.next();
                this.isRuleSaved = true;
                if (!this.isTaskLevelcalled) {
                    this.enableInitiateWorkflowHandler.next(this.istaskRuleEdit);
                }
                else {
                    this.istaskRuleEdit = true;
                    this.enableInitiateWorkflowHandler.next(this.istaskRuleEdit);
                }
                //   else if(this.isTaskLevelcalled){
                //     this.istaskRuleEdit= true
                //     this.isTaskLevelRuleEdit.emit(this.istaskRuleEdit)
                //   }
                this.notificationService.info('Workflow Rule has been saved');
            }
            else {
                this.notificationService.error('Something went wrong while creating workflow rule');
            }
        });
    }
    checkRuleDependencies1() {
        let removedRuleTargetTask = this.ruleData.targetData;
        if (this.ruleData.targetType === 'TASK' && !this.isTaskLevelcalled) {
            let sourceTasks = new Set();
            let sourceTasksArray = [];
            //this.allRuleListGroup.forEach(rule => {
            this.ruleListGroup.forEach(rule => {
                if (!rule.isInitialRule) {
                    rule.currentTask.forEach(task => sourceTasks.add(task));
                }
            });
            sourceTasksArray = Array.from(sourceTasks);
            let noPredecessorRuleListArray = [];
            let count = 0;
            // this.allRuleListGroup.forEach(rule => rule.hasPredecessor = true);
            this.ruleListGroup.forEach(rule => rule.hasPredecessor = true);
            const targetIndex = sourceTasksArray.indexOf(removedRuleTargetTask);
            if (targetIndex >= 0) {
                // this.allRuleListGroup.forEach(rule => {
                this.ruleListGroup.forEach(rule => {
                    const index = rule.currentTask.indexOf(removedRuleTargetTask);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                        noPredecessorRuleListArray.push(rule);
                        count += 1;
                    }
                });
            }
            this.noPredecessorRuleList = count <= 0 ? [] : noPredecessorRuleListArray;
            if (this.ruleData.targetTask !== "") {
                // this.ruleDependencyErrorHandler.next(this.allRuleListGroup);
                this.ruleDependencyErrorHandler.next(this.ruleListGroup);
            }
        }
        else if (this.ruleData.targetType === 'TASK' && this.isTaskLevelcalled) {
            let sourceTasks = new Set();
            let sourceTasksArray = [];
            this.taskruleListGroup.forEach(rule => {
                if (!rule.isInitialRule) {
                    rule.currentTask.forEach(task => sourceTasks.add(task));
                }
            });
            sourceTasksArray = Array.from(sourceTasks);
            let noPredecessorRuleListArray = [];
            let count = 0;
            this.taskruleListGroup.forEach(rule => rule.hasPredecessor = true);
            const targetIndex = sourceTasksArray.indexOf(removedRuleTargetTask);
            if (targetIndex >= 0) {
                this.taskruleListGroup.forEach(rule => {
                    const index = rule.currentTask.indexOf(removedRuleTargetTask);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                        noPredecessorRuleListArray.push(rule);
                        count += 1;
                    }
                });
            }
            this.noPredecessorRuleList = count <= 0 ? [] : noPredecessorRuleListArray;
            if (this.ruleData.targetTask !== "") {
                //  this.ruleDependencyErrorHandler.next(this.allRuleListGroup);
                this.ruleDependencyErrorHandler.next(this.ruleListGroup);
            }
        }
    }
    validateRemoveWorkflowRule() {
        // let ruleCountWithSameTargetTask = 0;
        // this.ruleListGroup.forEach(rule => {
        //   if(rule.targetData == this.ruleData.targetData){
        //     ruleCountWithSameTargetTask ++;
        //   }
        // })
        this.checkRuleDependencies1();
        if ((this.noPredecessorRuleList.length <= 0)) {
            if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
                this.ruleListGroup.forEach(rule => rule.hasPredecessor = true);
                this.removeWorkflowRule();
            }
            else {
                const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'Are you sure you want to remove the Rule?',
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result && result.isTrue) {
                        this.removeWorkflowRule();
                    }
                });
            }
        }
        else {
            if (!this.isTaskLevelcalled) {
                // this.notificationService.error("Please Delete Highlighted Rule First!! If Rule not visible change the page size and try again!!");
                this.notificationService.error("Please Delete Highlighted Child Rules First");
            }
            else {
                this.notificationService.error("Please open global configure rule and then edit and remove this task");
            }
        }
    }
    removeWorkflowRule() {
        const isInitialRule1 = this.ruleData.isInitialRule ? 'true' : 'false';
        if (this.ruleData.workflowRuleId) {
            this.ruleData.workflowRuleId = Array.isArray(this.ruleData.workflowRuleId) ? this.ruleData.workflowRuleId : [this.ruleData.workflowRuleId];
            //let httpRequestArray = []
            let ruleIds = [];
            this.ruleData.workflowRuleId.forEach(workflowId => {
                const Rule = {
                    Id: workflowId
                };
                ruleIds.push(Rule);
                //httpRequestArray.push(this.taskService.removeWorkflowRule(workflowId));
            });
            let currentTask = [];
            this.ruleData.currentTask.forEach(eachTask => {
                const sourceTask = {
                    Id: eachTask
                };
                currentTask.push(sourceTask);
            });
            let rData = {
                currentTask,
                isInitialRule: isInitialRule1
            };
            this.taskService.removeWorkflowRule(ruleIds, rData).subscribe(response => {
                if (response) {
                    this.removeWorkflowRuleHandler.next(this.ruleData);
                    this.enableInitiateWorkflowHandler.next();
                    this.notificationService.info('Workflow Rule has been removed');
                }
                else {
                    this.notificationService.error('Something went wrong while removing workflow rule');
                }
            });
        }
        else {
            this.removeWorkflowRuleHandler.next(null);
        }
    }
    closeDialog() {
        if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
            this.closeCallbackHandler.next();
        }
        else {
            const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result && result.isTrue) {
                    this.closeCallbackHandler.next();
                }
            });
        }
    }
    initializeForm() {
        var _a, _b, _c, _d, _e, _f;
        // this.allRuleList = [...new Set([...this.ruleList, ...this.projectRuleList])]; --- Update Standard code for all rule list - Menaka
        this.ruleList.forEach(rule => {
            const existingRule = this.allRuleList.find(selectedRule => JSON.stringify(selectedRule) === JSON.stringify(rule));
            if (!existingRule) {
                this.allRuleList.push(rule);
            }
        });
        this.projectRuleList.forEach(rule => {
            const existingRule = this.allRuleList.find(selectedRule => JSON.stringify(selectedRule) === JSON.stringify(rule));
            if (!existingRule) {
                this.allRuleList.push(rule);
            }
        });
        this.currentRuleList = [];
        this.allRuleList.forEach(rule => {
            if (JSON.stringify(this.ruleData) !== JSON.stringify(rule)) {
                this.currentRuleList.push(rule);
            }
            if (this.isTaskLevelcalled) {
                this.currentRuleList.push(rule);
            }
        });
        if (this.taskData[0].PROJECT_ITEM_ID) {
            const projectItemId = this.taskData[0].PROJECT_ITEM_ID;
            this.projectId = projectItemId.split('.')[1];
        }
        this.taskData.forEach(task => {
            if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL || task.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE) || (this.ruleData.isExist && this.isRulesInitiated)) {
                if (task.IS_APPROVAL_TASK === 'true') {
                    /*if (this.currentRuleList) {
                      const hasParentTaskRule = this.currentRuleList.find(rule => rule.currentTask === String(task.TASK_PARENT_ID) || this.currentRuleList.find(rule => rule.targetData === task.ID));
                      if (hasParentTaskRule) {
                        this.taskList.push({
                          name: task.TASK_NAME,
                          value: task.ID
                        });
                      }
                    }
                    */
                    if (this.currentRuleList && this.currentRuleList.length > 0 && !this.isTaskLevelcalled) {
                        const hasRule = this.currentRuleList.find(rule => rule.targetData === task.ID);
                        if (hasRule) {
                            this.taskList.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                            });
                        }
                    }
                    else if (this.isTaskLevelcalled) {
                        this.taskList.push({
                            name: task.TASK_NAME,
                            value: task.ID,
                            isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                        });
                    }
                }
                else {
                    this.taskList.push({
                        name: task.TASK_NAME,
                        value: task.ID,
                        isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                    });
                }
            }
        });
        const taskStatus = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.TASK);
        // this.taskStatuses = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.TASK);
        this.taskStatuses = taskStatus && taskStatus.length > 0 && Array.isArray(taskStatus) ? taskStatus[0] : taskStatus;
        let currentRuleTriggerData = this.taskStatuses.find(status => status['MPM_Status-id'].Id == this.ruleData.targetData);
        this.targetTask = this.taskData.find(task => task.ID === this.ruleData.targetData);
        this.customWorkflowFieldForm = new FormGroup({
            currentTask: new FormControl({
                value: this.ruleData.currentTask,
                disabled: (this.isCurrentTaskRule ? true : (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_a = this.targetTask) === null || _a === void 0 ? void 0 : _a.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false))
            }, [Validators.required]),
            triggerType: new FormControl({ value: this.ruleData.triggerType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_b = this.targetTask) === null || _b === void 0 ? void 0 : _b.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
            triggerData: new FormControl({ value: '', disabled: this.ruleData.triggerType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_c = this.targetTask) === null || _c === void 0 ? void 0 : _c.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
            targetType: new FormControl({ value: this.ruleData.targetType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_d = this.targetTask) === null || _d === void 0 ? void 0 : _d.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
            targetData: new FormControl({ value: '', disabled: this.ruleData.targetType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_e = this.targetTask) === null || _e === void 0 ? void 0 : _e.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
            isInitialRule: new FormControl({ value: this.ruleData.isInitialRule, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_f = this.targetTask) === null || _f === void 0 ? void 0 : _f.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }),
        });
        if (this.customWorkflowFieldForm.controls.currentTask.value) {
            const currentTaskList = this.customWorkflowFieldForm.controls.currentTask.value;
            for (let i = 0; i < currentTaskList.length; i++) {
                let currentTask = this.taskData.find(task => task.ID === currentTaskList[i]);
                if (currentTask && currentTask.IS_APPROVAL_TASK === 'true') {
                    this.approvalTask = true;
                    break;
                }
                else {
                    this.approvalTask = false;
                }
            }
            let isTargetTask = false;
            this.allRuleList.forEach(rule => {
                // if (rule.targetData === this.customWorkflowFieldForm.getRawValue().currentTask) {
                //   const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                //   if (!(rule.triggerType === 'STATUS' && status['MPM_Status-id'].Id === rule.triggerData)) {
                //     isTargetTask = true;
                //   }
                // }
                this.customWorkflowFieldForm.getRawValue().currentTask.forEach(eachTask => {
                    if (rule.targetData === eachTask) {
                        const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                        if (!(rule.triggerType === 'STATUS' && status['MPM_Status-id'].Id === rule.triggerData)) {
                            isTargetTask = true;
                        }
                    }
                });
            });
            if (isTargetTask || this.approvalTask) {
                this.customWorkflowFieldForm.controls.isInitialRule.disable();
            }
        }
        if (this.customWorkflowFieldForm.getRawValue().triggerType) {
            if (this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS') {
                this.taskStatuses.forEach(status => {
                    if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                        !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !this.approvalTask)) {
                        this.triggerData.push({
                            name: status.NAME,
                            value: status['MPM_Status-id'].Id
                        });
                    }
                });
                this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            }
            else {
                const taskWorkflowActions = this.sharingService.getTaskWorkflowActions();
                taskWorkflowActions.forEach(action => {
                    this.triggerData.push({
                        name: action.NAME,
                        value: action['MPM_Workflow_Actions-id'].Id
                    });
                });
                this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            }
        }
        let isSourceTask = false;
        if (this.customWorkflowFieldForm.getRawValue().targetType) {
            if (this.customWorkflowFieldForm.getRawValue().targetType === 'TASK') {
                this.taskData.forEach(task => {
                    let isSelectedTask = false;
                    let isParentTaskCompleted = true;
                    let isCycleTask = false;
                    const currentTaskRuleList = [];
                    if (this.currentRuleList && this.currentRuleList.length > 0) {
                        this.currentRuleList.forEach(rule => {
                            this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                                if (eachTask === rule.currentTask[0]) {
                                    currentTaskRuleList.push(rule);
                                }
                            });
                        });
                        // currentTaskRuleList.forEach(rule => {
                        //   if (rule.targetData === task.ID && rule.triggerData === this.customWorkflowFieldForm.controls.triggerData.value) {
                        //     isSelectedTask = true;
                        //   }
                        // });
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value) {
                            const targetTaskRuleList = [];
                            this.currentRuleList.forEach(rule => {
                                this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList.forEach(rule => {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === this.customWorkflowFieldForm.controls.triggerData.value) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
                    const statusAsRequestedChanges = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                    if (task.IS_APPROVAL_TASK === 'true') {
                        let isParentTaskInCurrentTask = false;
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask && ((this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                            (this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            const parentTaskRuleList = this.allRuleList.filter(rule => rule.currentTask.includes(String(task.TASK_PARENT_ID)) || rule.targetData === String(task.TASK_PARENT_ID));
                            if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                parentTaskRuleList.forEach(parentTaskRule => {
                                    if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                        isParentTaskCompleted = false;
                                    }
                                });
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                        if (task.ID === eachTask) {
                            isSourceTask = true;
                        }
                    });
                    if ((!isSelectedTask && !isCycleTask && isParentTaskCompleted /*&& !this.isTaskLevelcalled*/) /*|| (this.ruleData.isExist && this.isRulesInitiated)*/) {
                        if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL) || (this.ruleData.isExist && this.isRulesInitiated)) {
                            this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                    // else if(this.isTaskLevelcalled){
                    //   if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL) || (this.ruleData.isExist && this.isRulesInitiated)) {
                    //     this.targetData.push({
                    //       name: task.TASK_NAME,
                    //       value: task.ID,
                    //       isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                    //       isApprovalTask: task.IS_APPROVAL_TASK==='true' ?true:false
                    //     });
                    //   }
                    // }
                });
                this.groupTargetTask();
                this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            }
            else {
                const taskEvents = this.sharingService.getTaskEvents();
                taskEvents.forEach(event => {
                    this.targetData.push({
                        name: event.DISPLAY_NAME,
                        value: event['MPM_Events-id'].Id
                    });
                });
                this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            }
        }
        this.customWorkflowFieldForm.controls.currentTask.valueChanges.subscribe(currentTask => {
            var _a;
            /***********************Checking For Approval Task in Current Source task List*********************/
            /*************************We shouldn't allow Approval task to be grouped with other rules*************/
            let sourceTasks = [];
            // this.customWorkflowFieldForm.controls.isInitialRule.setValue(false);
            this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                let currentTask = this.taskData.find(task => task.ID === eachTask);
                sourceTasks.push(currentTask);
            });
            sourceTasks.forEach(eachTask => {
                var _a;
                if (((_a = eachTask) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true' && sourceTasks.length > 1) {
                    this.customWorkflowFieldForm.controls.currentTask.setValue([]);
                    this.notificationService.error('Approval Task cant be Grouped with Other tasks');
                }
            });
            //Checking if there only completed tasks in source tasks of a Rule.
            //This comes handy while updating tasks
            let onlyCompletedTasksInSourceTaskList = true;
            sourceTasks.forEach(eachTask => {
                var _a, _b, _c;
                if (!(((_a = eachTask) === null || _a === void 0 ? void 0 : _a.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED || ((_b = eachTask) === null || _b === void 0 ? void 0 : _b.TASK_STATUS_TYPE) === StatusTypes.FINAL_CANCELLED || ((_c = eachTask) === null || _c === void 0 ? void 0 : _c.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED)) {
                    onlyCompletedTasksInSourceTaskList = false;
                }
            });
            if (onlyCompletedTasksInSourceTaskList && this.customWorkflowFieldForm.controls.currentTask.value.length > 0) {
                this.customWorkflowFieldForm.controls.currentTask.setValue(this.ruleData.currentTask);
                this.notificationService.error('There should be atleast one Task that is not Completed');
            }
            //checking if target type is "TASK/EVENT"
            if (currentTask && this.customWorkflowFieldForm.controls.targetType && this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                this.targetData = [];
                this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
                this.taskData.forEach(task => {
                    let isSelectedTask = false;
                    let isParentTaskCompleted = true;
                    let isCycleTask = false;
                    if (this.currentRuleList && this.currentRuleList.length > 0) {
                        const currentTaskRuleList = [];
                        this.currentRuleList.forEach(rule => {
                            currentTask.forEach(eachTask => {
                                if (rule.currentTask[0] === eachTask) {
                                    currentTaskRuleList.push(rule);
                                }
                            });
                        });
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value) {
                            const targetTaskRuleList = [];
                            this.currentRuleList.forEach(rule => {
                                this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList.forEach(rule => {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === this.customWorkflowFieldForm.controls.triggerData.value) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
                    const statusAsRequestedChanges = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                    if (task.IS_APPROVAL_TASK === 'true') {
                        let isParentTaskInCurrentTask = false;
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask && ((this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                            (this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            if (this.currentRuleList && this.currentRuleList.length > 0) {
                                const parentTaskRuleList = this.currentRuleList.filter(rule => rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID));
                                if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                    parentTaskRuleList.forEach(parentTaskRule => {
                                        if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                            isParentTaskCompleted = false;
                                        }
                                    });
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    if (!isSelectedTask && !isCycleTask && isParentTaskCompleted && !this.isTaskLevelcalled) {
                        if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                            this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                });
                this.groupTargetTask();
            }
            //checking with Trigger type 
            if (currentTask && this.customWorkflowFieldForm.controls.triggerType) {
                let approvalTaskInSource = false;
                for (let i = 0; i < sourceTasks.length; i++) {
                    if (((_a = sourceTasks[i]) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true') {
                        approvalTaskInSource = true;
                        break;
                    }
                }
                if (this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS') {
                    this.triggerData = [];
                    this.taskStatuses.forEach(status => {
                        if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                            !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !approvalTaskInSource)) {
                            this.triggerData.push({
                                name: status.NAME,
                                value: status['MPM_Status-id'].Id
                            });
                        }
                    });
                    this.selectedTriggerData = this.triggerData[0];
                }
            }
            let isTargetTask = false;
            if (this.currentRuleList && this.currentRuleList.length > 0) {
                this.currentRuleList.forEach(rule => {
                    // if (rule.targetData === currentTask) {
                    this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                        if (rule.targetData === eachTask) {
                            const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                            if (!(rule.triggerType === 'STATUS' && status['MPM_Status-id'].Id === rule.triggerData)) {
                                isTargetTask = true;
                            }
                        }
                    });
                });
            }
            if (this.customWorkflowFieldForm.controls.currentTask.value) {
                const currentTaskList = this.customWorkflowFieldForm.controls.currentTask.value;
                for (let i = 0; i < currentTaskList.length; i++) {
                    let currentTask = this.taskData.find(task => task.ID === currentTaskList[i]);
                    if (currentTask && currentTask.IS_APPROVAL_TASK === 'true') {
                        this.approvalTask = true;
                        break;
                    }
                    else {
                        this.approvalTask = false;
                    }
                }
            }
            if (isTargetTask || this.approvalTask) {
                this.customWorkflowFieldForm.controls.isInitialRule.disable();
            }
            else {
                this.customWorkflowFieldForm.controls.isInitialRule.enable();
            }
            this.customWorkflowFieldForm.controls.targetData.setValue('');
            this.groupTargetTask();
        });
        this.customWorkflowFieldForm.controls.triggerType.valueChanges.subscribe(triggerType => {
            if (triggerType) {
                if (this.customWorkflowFieldForm.controls.currentTask.value) {
                    const currentTaskList = this.customWorkflowFieldForm.controls.currentTask.value;
                    for (let i = 0; i < currentTaskList.length; i++) {
                        let currentTask = this.taskData.find(task => task.ID === currentTaskList[i]);
                        if (currentTask && currentTask.IS_APPROVAL_TASK === 'true') {
                            this.approvalTask = true;
                            break;
                        }
                        else {
                            this.approvalTask = false;
                        }
                    }
                }
                this.triggerData = [];
                this.customWorkflowFieldForm.controls.triggerData.setValue('');
                if (triggerType === 'STATUS') {
                    this.taskStatuses.forEach(status => {
                        if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                            !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !this.approvalTask)) {
                            this.triggerData.push({
                                name: status.NAME,
                                value: status['MPM_Status-id'].Id
                            });
                        }
                    });
                }
                else if (triggerType === 'ACTION') {
                    const taskWorkflowActions = this.sharingService.getTaskWorkflowActions();
                    taskWorkflowActions.forEach(action => {
                        this.triggerData.push({
                            name: action.NAME,
                            value: action['MPM_Workflow_Actions-id'].Id
                        });
                    });
                }
                if (this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                    this.targetData = [];
                    this.customWorkflowFieldForm.controls.targetData.setValue('');
                    this.taskData.forEach(task => {
                        let isParentTaskCompleted = true;
                        const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
                        const statusAsRequestedChanges = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                        if (task.IS_APPROVAL_TASK === 'true') {
                            let isParentTaskInCurrentTask = false;
                            this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                if (eachTask === String(task.TASK_PARENT_ID)) {
                                    isParentTaskInCurrentTask = true;
                                }
                            });
                            if (!(isParentTaskInCurrentTask && ((triggerType === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) || (triggerType === 'ACTION')))) {
                                if (this.currentRuleList && this.currentRuleList.length > 0) {
                                    const parentTaskRuleList = this.currentRuleList.filter(rule => rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID));
                                    if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                        parentTaskRuleList.forEach(parentTaskRule => {
                                            if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id))) || (parentTaskRule.triggerType === 'ACTION')) {
                                                isParentTaskCompleted = false;
                                            }
                                        });
                                    }
                                    else {
                                        isParentTaskCompleted = false;
                                    }
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                        }
                        if (isParentTaskCompleted) {
                            if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL && task.ID !== this.customWorkflowFieldForm.controls.currentTask.value) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                                this.targetData.push({
                                    name: task.TASK_NAME,
                                    value: task.ID,
                                    isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                    isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                                });
                            }
                        }
                    });
                }
                this.groupTargetTask();
                this.customWorkflowFieldForm.controls.triggerData.enable();
            }
        });
        this.customWorkflowFieldForm.controls.triggerData.valueChanges.subscribe(triggerData => {
            if (triggerData && this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                let removedTasks = [];
                this.ruleData.currentTask.forEach(eachTask => {
                    const index = this.customWorkflowFieldForm.controls.currentTask.value.indexOf(eachTask);
                    if (index < 0) {
                        removedTasks.push(eachTask);
                    }
                });
                this.targetData = [];
                this.customWorkflowFieldForm.controls.targetData.setValue('');
                this.taskData.forEach(task => {
                    let isSelectedTask = false;
                    let isCycleTask = false;
                    let isParentTaskCompleted = true;
                    if (this.currentRuleList && this.currentRuleList.length > 0) {
                        const currentTaskRuleList = [];
                        this.currentRuleList.forEach(rule => {
                            this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                if (rule.currentTask[0] === eachTask) {
                                    currentTaskRuleList.push(rule);
                                }
                            });
                        });
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS') {
                            const targetTaskRuleList = [];
                            this.currentRuleList.forEach(rule => {
                                this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList.forEach(rule => {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === triggerData) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
                    const statusAsRequestedChanges = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                    let isParentTaskInCurrentTask = false;
                    if (task.IS_APPROVAL_TASK === 'true') {
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask
                            && ((this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && triggerData === status['MPM_Status-id'].Id) ||
                                (this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            if (this.currentRuleList && this.currentRuleList.length > 0) {
                                const parentTaskRuleList = this.currentRuleList.filter(rule => rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID));
                                //while editing if a remove a parent task from source task list then we are removing that rule from parentTaskRuleList
                                for (let i = 0; i < parentTaskRuleList.length; i++) {
                                    removedTasks.forEach(eachTask => {
                                        if (eachTask === parentTaskRuleList[i].currentTask[0]) {
                                            parentTaskRuleList.splice(i, 1);
                                        }
                                    });
                                }
                                if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                    parentTaskRuleList.forEach(parentTaskRule => {
                                        if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                            isParentTaskCompleted = false;
                                        }
                                    });
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
                        if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL && task.ID !== this.customWorkflowFieldForm.controls.currentTask.value) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                            this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                });
            }
            this.groupTargetTask();
        });
        this.customWorkflowFieldForm.controls.targetType.valueChanges.subscribe(targetType => {
            if (targetType) {
                this.targetData = [];
                this.customWorkflowFieldForm.controls.targetData.setValue('');
                if (targetType === 'EVENT') {
                    const taskEvents = this.sharingService.getTaskEvents();
                    taskEvents.forEach(event => {
                        this.targetData.push({
                            name: event.DISPLAY_NAME,
                            value: event['MPM_Events-id'].Id
                        });
                    });
                }
                else if (targetType === 'TASK') {
                    this.taskData.forEach(task => {
                        let isSelectedTask = false;
                        let isParentTaskCompleted = true;
                        let isCycleTask = false;
                        this.customWorkflowFieldForm.controls.currentTask.value.forEach((eachTask) => {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (this.currentRuleList && this.currentRuleList.length > 0) {
                            if (this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value) {
                                const targetTaskRuleList = [];
                                this.currentRuleList.forEach(rule => {
                                    this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                        if (rule.targetData === eachTask) {
                                            targetTaskRuleList.push(rule);
                                        }
                                    });
                                });
                                targetTaskRuleList.forEach(rule => {
                                    if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === this.customWorkflowFieldForm.controls.triggerData.value) {
                                        isCycleTask = true;
                                    }
                                });
                            }
                        }
                        const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
                        const statusAsRequestedChanges = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                        if (task.IS_APPROVAL_TASK === 'true') {
                            let isParentTaskInCurrentTask = false;
                            this.customWorkflowFieldForm.controls.currentTask.value.forEach(eachTask => {
                                if (eachTask === String(task.TASK_PARENT_ID)) {
                                    isParentTaskInCurrentTask = true;
                                }
                            });
                            if (!(isParentTaskInCurrentTask
                                && ((this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                                    (this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                                if (this.currentRuleList && this.currentRuleList.length > 0) {
                                    const parentTaskRuleList = this.currentRuleList.filter(rule => rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID));
                                    if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                        parentTaskRuleList.forEach(parentTaskRule => {
                                            if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                                isParentTaskCompleted = false;
                                            }
                                        });
                                    }
                                    else {
                                        isParentTaskCompleted = false;
                                    }
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                        }
                        if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
                            if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                                this.targetData.push({
                                    name: task.TASK_NAME,
                                    value: task.ID,
                                    isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                    isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                                });
                            }
                        }
                    });
                }
                this.groupTargetTask();
                this.customWorkflowFieldForm.controls.targetData.enable();
            }
        });
        this.customWorkflowFieldForm.controls.targetData.valueChanges.subscribe(response => {
            this.isRuleSaved = false;
        });
        this.customWorkflowFieldForm.valueChanges.subscribe(response => {
            this.isRuleChanged = true;
        });
    }
    ngOnInit() {
        const sourceTasksSet = new Set();
        this.ruleListGroup.forEach(rule => {
            rule.hasPredecessor = true;
        });
        this.taskData.forEach((task) => {
            this.ruleData.currentTask.forEach(eachTask => {
                if (task.ID === eachTask) {
                    sourceTasksSet.add(task.TASK_NAME);
                }
            });
        });
        sourceTasksSet.forEach(task => this.currentSourceTasks.push(task));
        this.ruleData.currentTask = this.ruleData.currentTask.length > 0 && Array.isArray(this.ruleData.currentTask) ? this.ruleData.currentTask : [this.ruleData.currentTask];
        this.ruleList.forEach((rule) => {
            rule.currentTask = rule.currentTask.length > 0 && Array.isArray(rule.currentTask) ? rule.currentTask : [rule.currentTask];
        });
        this.initializeForm();
    }
    groupTargetTask() {
        var _a, _b, _c;
        const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED);
        // if(this.isTaskLevelcalled){
        if (((_a = this.taskWorkFlowRuleToDisplay) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true' && !(this.customWorkflowFieldForm.controls.triggerData.value == status['MPM_Status-id'].Id)) {
            const newtargetData = this.targetData.filter(targetTask => !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value)); /*.filter(t1=> t1.isApprovalTask==false)*/
            this.targetData = newtargetData;
        }
        else if (((_b = this.taskWorkFlowRuleToDisplay) === null || _b === void 0 ? void 0 : _b.IS_APPROVAL_TASK) === 'true' && (this.customWorkflowFieldForm.controls.triggerData.value == status['MPM_Status-id'].Id)) {
            const parentId = ((_c = this.taskWorkFlowRuleToDisplay) === null || _c === void 0 ? void 0 : _c.TASK_PARENT_ID) + '';
            const removeTargetTask = new Set();
            this.taskruleListGroup.forEach(task => {
                if (task.currentTask.includes(parentId)) {
                    task.currentTask.forEach(taskId => removeTargetTask.add(taskId));
                }
                // const newtargetData= this.targetData.filter(targetTask=> targetTask.value!=this.taskWorkFlowRuleToDisplay?.TASK_PARENT_ID && !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value) && !removeTargetTask.has(targetTask.value))
                const newtargetData = this.targetData.filter(targetTask => !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value));
                this.targetData = newtargetData;
            });
        }
        else if (this.customWorkflowFieldForm.controls.triggerData.value != status['MPM_Status-id'].Id) {
            var newtargetData = this.targetData.filter(targetTask => !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value)); /*.filter(t1=> t1.isApprovalTask==false)*/
            this.targetData = newtargetData;
        }
        else {
            const newtargetData = this.targetData.filter(targetTask => /*targetTask.value!=this.taskWorkFlowRuleToDisplay?.TASK_PARENT_ID &&*/ !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value));
            this.targetData = newtargetData;
        }
    }
};
CustomWorkflowFieldComponent.ctorParameters = () => [
    { type: StatusService },
    { type: SharingService },
    { type: TaskService },
    { type: NotificationService },
    { type: ProjectUtilService },
    { type: MatDialog }
];
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "ruleData", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "ruleList", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "projectRuleList", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "taskData", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "isRulesInitiated", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "isCurrentTaskRule", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "ruleListGroup", void 0);
__decorate([
    Output()
], CustomWorkflowFieldComponent.prototype, "closeCallbackHandler", void 0);
__decorate([
    Output()
], CustomWorkflowFieldComponent.prototype, "removeWorkflowRuleHandler", void 0);
__decorate([
    Output()
], CustomWorkflowFieldComponent.prototype, "enableInitiateWorkflowHandler", void 0);
__decorate([
    Output()
], CustomWorkflowFieldComponent.prototype, "ruleGroupHandler", void 0);
__decorate([
    Output()
], CustomWorkflowFieldComponent.prototype, "ruleDependencyErrorHandler", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "isTaskLevelcalled", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "taskWorkFlowRuleToDisplay", void 0);
__decorate([
    Input()
], CustomWorkflowFieldComponent.prototype, "taskruleListGroup", void 0);
CustomWorkflowFieldComponent = __decorate([
    Component({
        selector: 'mpm-custom-workflow-field',
        template: "<div [ngClass]=\"{'dependency-error': !ruleData.hasPredecessor}\">\r\n    <form [formGroup]=\"customWorkflowFieldForm\">\r\n        <mat-form-field appearance=\"outline\" matTooltip=\"{{currentSourceTasks}}\">\r\n            <mat-label>Task Name</mat-label>\r\n            <mat-select formControlName=\"currentTask\" multiple>\r\n                <mat-option *ngFor=\"let task of taskList\" value=\"{{task.value}}\" [disabled]=\"task?.isCompleted\">\r\n                    <span matTooltip=\"{{task.name}}\" [style.color]=\"task?.isCompleted ? 'grey':''\">{{task.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Trigger Type</mat-label>\r\n            <mat-select formControlName=\"triggerType\">\r\n                <mat-option *ngFor=\"let triggerType of triggerTypes\" value=\"{{triggerType.value}}\">\r\n                    <span matTooltip=\"{{triggerType.name}}\">{{triggerType.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Status/Action</mat-label>\r\n            <mat-select formControlName=\"triggerData\">\r\n                <mat-option *ngFor=\"let data of triggerData\" value=\"{{data.value}}\">\r\n                    <span matTooltip=\"{{data.name}}\">{{data.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Target Type</mat-label>\r\n            <mat-select formControlName=\"targetType\">\r\n                <mat-option *ngFor=\"let targetType of targetTypes\" value=\"{{targetType.value}}\">\r\n                    <span matTooltip=\"{{targetType.name}}\">{{targetType.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Task/Event</mat-label>\r\n            <mat-select formControlName=\"targetData\">\r\n                <mat-option *ngFor=\"let data of targetData\" value=\"{{data.value}}\" [disabled]=\"data?.isCompleted\">\r\n                    <span matTooltip=\"{{data.name}}\" [style.color]=\"data?.isCompleted ? 'grey':''\">{{data.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-checkbox color=\"primary\" formControlName=\"isInitialRule\">\r\n            <span>Is Initial</span>\r\n        </mat-checkbox>\r\n\r\n        <button class=\"field-row-action\" *ngIf=\"isRuleChanged && (!ruleData.isExist || customWorkflowFieldForm.dirty) && customWorkflowFieldForm.valid && !isRuleSaved\" mat-icon-button color=\"primary\" type=\"button\" matTooltip=\"Save this Rule\" (click)=\"addWorkflowRule()\">\r\n            <mat-icon>done</mat-icon>\r\n        </button>\r\n        <button class=\"field-row-action\" type=\"button\" *ngIf=\"(!ruleData.isExist || (targetTask?.IS_ACTIVE_TASK==='false' && targetTask?.TASK_STATUS_TYPE !=='FINAL-ACCEPTED')) || !ruleData.hasPredecessor || ruleData.targetType=='EVENT'\" (click)=\"validateRemoveWorkflowRule()\"\r\n            mat-icon-button color=\"primary\" matTooltip=\"Remove this Rule\">\r\n            <mat-icon>close</mat-icon>\r\n        </button>\r\n    </form>\r\n</div>",
        styles: ["mat-form-field{width:17%;margin-left:8px;font-size:12px}"]
    })
], CustomWorkflowFieldComponent);
export { CustomWorkflowFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXdvcmtmbG93LWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvY3VzdG9tLXdvcmtmbG93LWZpZWxkL2N1c3RvbS13b3JrZmxvdy1maWVsZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDRFQUE0RSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFPOUMsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUFvRHZDLFlBQ1MsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGtCQUFzQyxFQUN0QyxNQUFpQjtRQUxqQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLFdBQU0sR0FBTixNQUFNLENBQVc7UUFqRGhCLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDL0MsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRCxrQ0FBNkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ2xFLDBEQUEwRDtRQUMxRCxtQkFBYyxHQUFDLEtBQUssQ0FBQztRQUNYLHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDM0MsK0JBQTBCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUkvRCxxQ0FBcUM7UUFDcEMsdUJBQWtCLEdBQUUsS0FBSyxDQUFDO1FBRTNCLGlCQUFZLEdBQUcsQ0FBQztnQkFDZCxJQUFJLEVBQUUsUUFBUTtnQkFDZCxLQUFLLEVBQUUsUUFBUTthQUNoQixFQUFFO2dCQUNELElBQUksRUFBRSxRQUFRO2dCQUNkLEtBQUssRUFBRSxRQUFRO2FBQ2hCLENBQUMsQ0FBQztRQUNILGdCQUFXLEdBQUcsQ0FBQztnQkFDYixJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsS0FBSyxFQUFFLE9BQU87YUFDZixDQUFDLENBQUM7UUFDSCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixlQUFVLEdBQU8sRUFBRSxDQUFDO1FBQ3BCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFLZCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUdqQix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFFeEIsMEJBQXFCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztJQVVsQixDQUFDO0lBRUwsK0JBQStCO0lBQy9CLGtCQUFrQjtRQUNoQixJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBQztZQUNsRyw0Q0FBNEM7WUFDNUMsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdkMsTUFBTSxLQUFLLEdBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xGLElBQUcsS0FBSyxHQUFHLENBQUMsRUFBQztvQkFDWCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNyQjtZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0YsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFBO1lBQ3JCLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3RCLE1BQU0sSUFBSSxHQUFFO29CQUNaLFdBQVcsRUFBRSxJQUFJO29CQUNqQixhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhO29CQUMxQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXO29CQUN0QyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXO2lCQUNyQyxDQUFBO2dCQUNELFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUE7WUFDRixJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDdkIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQztnQkFDckQsTUFBTSxJQUFJLEdBQUU7b0JBQ1YsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQkFDekMsYUFBYSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYTtvQkFDMUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVTtvQkFDcEMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVTtvQkFDcEMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVztvQkFDdEMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVztvQkFDdEMsY0FBYyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztpQkFDOUMsQ0FBQTtnQkFDSCxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFCO1lBQ0QsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFCLGFBQWEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQy9CLElBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFDO3dCQUMxQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDOUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDLENBQUMsQ0FBQTtZQUNGLE1BQU0sa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7bUJBQy9HLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQzttQkFDdEYsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO21CQUNwRixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7bUJBQ3BGLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDL0YsSUFBRyxrQkFBa0IsRUFDcEI7Z0JBQ0csS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNoRixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEg7YUFDSDtZQUNGLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxDQUFDO1lBQ25GLElBQUksa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1lBQzVCLGFBQWEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ25DLE1BQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNwRSxJQUFHLEtBQUssSUFBSSxDQUFDLEVBQUM7b0JBQ1osa0JBQWtCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2lCQUMxQztZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0YsOEVBQThFO1lBQzlFLHFHQUFxRztZQUNyRyxJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztZQUM3QixrQkFBa0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3BDLE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxRQUFRLENBQUUsQ0FBQztnQkFDL0YsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsZUFBZSxDQUFDLENBQUM7WUFDL0MsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hDLGVBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hDLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxxQkFBcUIsR0FBRyxFQUFFLENBQUM7WUFDL0IsbUJBQW1CLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNyQyxNQUFNLEtBQUssR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbkQsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO29CQUNaLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDdEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUM3RCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUNuQixTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDaEMscUJBQXFCLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN2QyxNQUFNLEtBQUssR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDN0MsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO3dCQUNaLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO3FCQUM3QjtnQkFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUMsQ0FBQyxDQUFBO1lBQ0gsb0hBQW9IO1lBQ25ILElBQUcsQ0FBQyxDQUFDLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBQztnQkFDaEUsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFBO2dCQUNoQixjQUFjLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFO29CQUNwQyxNQUFNLElBQUksR0FBRzt3QkFDWCxFQUFFLEVBQUUsVUFBVTtxQkFDZixDQUFBO29CQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25CLENBQUMsQ0FBQyxDQUFBO2dCQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDbkUsSUFBSSxRQUFRLEVBQUU7d0JBQ1osc0RBQXNEO3dCQUN0RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUM1RyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7d0JBQ3BGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO3dCQUN4QixJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztxQkFDakU7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3FCQUNyRjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFJO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsNEdBQTRHLENBQUMsQ0FBQzthQUM5STtTQUVGO2FBQ0ksSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUM7WUFDdkcsd0NBQXdDO1lBQ3hDLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDcEUsTUFBTSxLQUFLLEdBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNyRCxJQUFHLEtBQUssR0FBRyxDQUFDLEVBQUM7b0JBQ1gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckI7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7bUJBQ3JGLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQzttQkFDdEYsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO21CQUNwRixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7bUJBQ3BGLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxFQUM3RjtnQkFDRyxLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0JBQ2hGLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN0SDthQUNIO2lCQUFJO2dCQUNKLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3RCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDeEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1RixDQUFDLENBQUMsQ0FBQTthQUNGO1NBQ0g7SUFDSCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ2QsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7WUFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzNCLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDO29CQUNyQixjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO2lCQUN6QztZQUNILENBQUMsQ0FBQyxDQUFBO1NBQ0g7YUFDRztZQUNGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BDLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDO29CQUNyQixjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFBO2lCQUN6QztZQUNILENBQUMsQ0FBQyxDQUFBO1NBQ0g7UUFDQSxNQUFNLEtBQUssR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0QsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO1lBQ1gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1NBQ2pGO2FBQUssSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztlQUMzSCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87ZUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFDO1lBQ25HLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBRTdCO2FBRUksSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztZQUMvQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkYsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNoRSxZQUFZLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEk7U0FDRjtJQUNILENBQUM7SUFFRCxxQkFBcUI7UUFFbkIsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDcEMsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQ3JCLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7YUFDekM7UUFDSCxDQUFDLENBQUMsQ0FBQTtRQUNILE1BQU0sS0FBSyxHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvRCxJQUFHLEtBQUssSUFBSSxDQUFDLEVBQUM7WUFDWCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNwRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLGtCQUFrQixHQUFDLElBQUksQ0FBQztTQUM5QjtRQUNELE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFBO0lBQ2xDLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsRUFBQztZQUN6RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUUsRUFBRSxDQUFBO1lBQzFCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDcEYsSUFBSSxrQkFBa0IsR0FBRSxFQUFFLENBQUM7Z0JBQzNCLGtCQUFrQixDQUFDLElBQUksQ0FBQztvQkFDdEIsV0FBVyxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7b0JBQ3ZFLFdBQVcsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLO29CQUNwRSxVQUFVLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSztvQkFDbEUsV0FBVyxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7b0JBQ3BFLFVBQVUsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLO29CQUNsRSxhQUFhLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSztpQkFDMUUsQ0FBQyxDQUFBO2dCQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwSyx5QkFBeUI7Z0JBQ3pCLDJCQUEyQjtnQkFDM0IsZ0VBQWdFO2FBQ2pFO1NBQ0E7YUFDRyxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7WUFDM0ksSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDekI7YUFBSyxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2VBQzdILElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTztlQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUM7WUFDbkcsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0I7YUFBSyxJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO1lBQy9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDM0IsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuRixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2hFLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoSTtTQUNGO1FBRUQsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNuRCxJQUFHLFFBQVEsRUFBQztnQkFDVixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDbkYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDbkYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsQ0FBQztnQkFDdkYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7Z0JBQ2xDLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN6SjtnQkFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixJQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMzQixJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDNUQ7cUJBQ0c7b0JBQ0YsSUFBSSxDQUFDLGNBQWMsR0FBQyxJQUFJLENBQUM7b0JBQ3pCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lCQUM5RDtnQkFDSCxxQ0FBcUM7Z0JBQ3JDLGdDQUFnQztnQkFDaEMseURBQXlEO2dCQUN6RCxNQUFNO2dCQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUMvRDtpQkFBSTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7YUFDckY7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFQSxzQkFBc0I7UUFDckIsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztRQUNwRCxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztZQUNqRSxJQUFJLFdBQVcsR0FBRyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQzVCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFBO1lBQ3pCLHlDQUF5QztZQUN2QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDbEMsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUN6RDtZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0osZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMzQyxJQUFJLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDZixxRUFBcUU7WUFDckUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBRTlELE1BQU0sV0FBVyxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3BFLElBQUcsV0FBVyxJQUFJLENBQUMsRUFBQztnQkFDbkIsMENBQTBDO2dCQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDaEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDOUQsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO3dCQUNaLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO3dCQUM1QiwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3RDLEtBQUssSUFBSSxDQUFDLENBQUM7cUJBQ1o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtZQUNELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLElBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLDBCQUEwQixDQUFDO1lBQ3pFLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUssRUFBRSxFQUFDO2dCQUNsQywrREFBK0Q7Z0JBQy9ELElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBRXhEO1NBQ0Q7YUFDRyxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUM7WUFDbkUsSUFBSSxXQUFXLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUM1QixJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQTtZQUN6QixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNwQyxJQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBQztvQkFDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3pEO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixnQkFBZ0IsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzNDLElBQUksMEJBQTBCLEdBQUcsRUFBRSxDQUFDO1lBQ3BDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQ25FLE1BQU0sV0FBVyxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3BFLElBQUcsV0FBVyxJQUFJLENBQUMsRUFBQztnQkFDbEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDcEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDOUQsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO3dCQUNaLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO3dCQUM1QiwwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3RDLEtBQUssSUFBSSxDQUFDLENBQUM7cUJBQ1o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtZQUNELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLElBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLDBCQUEwQixDQUFDO1lBQ3pFLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUssRUFBRSxFQUFDO2dCQUNuQyxnRUFBZ0U7Z0JBQ2hFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBRXZEO1NBQ0Q7SUFDRixDQUFDO0lBRUYsMEJBQTBCO1FBQ3hCLHVDQUF1QztRQUN2Qyx1Q0FBdUM7UUFDdkMscURBQXFEO1FBQ3JELHNDQUFzQztRQUN0QyxNQUFNO1FBQ04sS0FBSztRQUNMLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxFQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7b0JBQzdELEtBQUssRUFBRSxLQUFLO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixJQUFJLEVBQUU7d0JBQ0osT0FBTyxFQUFFLDJDQUEyQzt3QkFDcEQsWUFBWSxFQUFFLEtBQUs7d0JBQ25CLFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRixDQUFDLENBQUM7Z0JBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDekMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTt3QkFDM0IsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7cUJBQzNCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjthQUFJO1lBQ0gsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDMUIscUlBQXFJO2dCQUNySSxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7YUFDOUU7aUJBQ0c7Z0JBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxzRUFBc0UsQ0FBQyxDQUFDO2FBQ3hHO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2hCLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztRQUN0RSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMzSSwyQkFBMkI7WUFDM0IsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFBO1lBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDaEQsTUFBTSxJQUFJLEdBQUc7b0JBQ1gsRUFBRSxFQUFFLFVBQVU7aUJBQ2YsQ0FBQTtnQkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNuQix5RUFBeUU7WUFDM0UsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUMzQyxNQUFNLFVBQVUsR0FBRztvQkFDakIsRUFBRSxFQUFFLFFBQVE7aUJBQ2IsQ0FBQTtnQkFDRCxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQy9CLENBQUMsQ0FBQyxDQUFBO1lBQ0YsSUFBSSxLQUFLLEdBQUc7Z0JBQ1YsV0FBVztnQkFDWCxhQUFhLEVBQUUsY0FBYzthQUM5QixDQUFBO1lBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN0RSxJQUFJLFFBQVEsRUFBRTtvQkFDWixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7aUJBQ2pFO3FCQUFNO29CQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQztpQkFDckY7WUFDSCxDQUFDLENBQUMsQ0FBQTtTQUNIO2FBQU07WUFDTCxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNDO0lBQ0gsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxFQUFFO1lBQ3pFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNsQzthQUFNO1lBQ0wsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzdELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0osT0FBTyxFQUFFLGlDQUFpQztvQkFDMUMsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNuQjthQUNGLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3pDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztpQkFDbEM7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELGNBQWM7O1FBRVosb0lBQW9JO1FBQ3BJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzNCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEgsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xDLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEgsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzdCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakM7WUFDRCxJQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBQztnQkFDeEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7YUFDaEM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLEVBQUU7WUFDcEMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUM7WUFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzlDO1FBR0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtnQkFDN0osSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFFO29CQUNwQzs7Ozs7Ozs7O3NCQVNFO29CQUNGLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQ3RGLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQzlFLElBQUksT0FBTyxFQUFFOzRCQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dDQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtnQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDOzZCQUMxTCxDQUFDLENBQUM7eUJBQ0w7cUJBRUY7eUJBQ0ksSUFBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7d0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDOzRCQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7NEJBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTs0QkFDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDO3lCQUMzTCxDQUFDLENBQUM7cUJBQ0o7aUJBQ0E7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7d0JBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO3dCQUNkLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFLLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUM7cUJBQzNMLENBQUMsQ0FBQztpQkFDSjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsRixzRkFBc0Y7UUFDdEYsSUFBSSxDQUFDLFlBQVksR0FBRyxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7UUFDbEgsSUFBSSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUd0SCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUMzQyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQzNCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVc7Z0JBQ2hDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzFaLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL2MsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQUEsSUFBSSxDQUFDLFVBQVUsMENBQUUsY0FBYyxNQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsaUJBQWlCLElBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdkLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsS0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBQSxJQUFJLENBQUMsVUFBVSwwQ0FBRSxjQUFjLE1BQUksTUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxpQkFBaUIsSUFBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9jLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxZCxhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQUEsSUFBSSxDQUFDLFVBQVUsMENBQUUsY0FBYyxNQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsaUJBQWlCLElBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztTQUM1YixDQUFDLENBQUM7UUFJTCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtZQUMzRCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDaEYsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQzNDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0UsSUFBRyxXQUFXLElBQUksV0FBVyxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQztvQkFDeEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ3pCLE1BQU07aUJBQ1A7cUJBQUk7b0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7aUJBQzNCO2FBQ0Y7WUFDRCxJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLG9GQUFvRjtnQkFDcEYsZ0hBQWdIO2dCQUNoSCwrRkFBK0Y7Z0JBQy9GLDJCQUEyQjtnQkFDM0IsTUFBTTtnQkFDTixJQUFJO2dCQUNKLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN4RSxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFDO3dCQUM5QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUMzRyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTs0QkFDbkYsWUFBWSxHQUFHLElBQUksQ0FBQzt5QkFDckI7cUJBQ047Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQy9EO1NBQ0Y7UUFHRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEVBQUU7WUFDMUQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxLQUFLLFFBQVEsRUFBRTtnQkFDdkUsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2pDLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGVBQWU7d0JBQ2xHLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFFLEVBQUU7d0JBQzdFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDOzRCQUNwQixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7NEJBQ2pCLEtBQUssRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRTt5QkFDbEMsQ0FBQyxDQUFDO3FCQUNKO2dCQUNELENBQUMsQ0FBQyxDQUFDO2dCQUNMLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3ZGO2lCQUFNO2dCQUNMLE1BQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUN6RSxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUNwQixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7d0JBQ2pCLEtBQUssRUFBRSxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFO3FCQUM1QyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkY7U0FDRjtRQUdELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEVBQUU7WUFDekQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLE1BQU0sRUFBRTtnQkFDcEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQzNCLElBQUksY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDM0IsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUM7b0JBQ2pDLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztvQkFDeEIsTUFBTSxtQkFBbUIsR0FBRyxFQUFFLENBQUM7b0JBQy9CLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzNELElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUNsQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0NBQzNFLElBQUcsUUFBUSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUM7b0NBQ2xDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDaEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osQ0FBQyxDQUFDLENBQUE7d0JBQ0Ysd0NBQXdDO3dCQUN4Qyx1SEFBdUg7d0JBQ3ZILDZCQUE2Qjt3QkFDN0IsTUFBTTt3QkFDTixNQUFNO3dCQUNOLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTs0QkFDM0UsSUFBRyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBQztnQ0FDdEIsY0FBYyxHQUFHLElBQUksQ0FBQzs2QkFDdkI7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTs0QkFDbkksTUFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7NEJBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dDQUNsQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29DQUN6RSxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFDO3dDQUM5QixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUNBQy9CO2dDQUNILENBQUMsQ0FBQyxDQUFBOzRCQUNKLENBQUMsQ0FBQyxDQUFDOzRCQUNMLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQ0FDaEMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0NBQ3BKLFdBQVcsR0FBRyxJQUFJLENBQUM7aUNBQ3BCOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKO3FCQUNGO29CQUNELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQzNHLE1BQU0sd0JBQXdCLEdBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDNUgsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDO3dCQUNsQyxJQUFJLHlCQUF5QixHQUFHLEtBQUssQ0FBQzt3QkFDdEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTs0QkFDekUsSUFBRyxRQUFRLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBQztnQ0FDMUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDOzZCQUNsQzt3QkFDSCxDQUFDLENBQUMsQ0FBQTt3QkFDSixJQUFJLENBQUMsQ0FBQyx5QkFBeUIsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDaE0sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUM1RSxNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDOzRCQUN0SyxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQ3ZELGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtvQ0FDMUMsSUFBSSxDQUFDLENBQUMsY0FBYyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksY0FBYyxDQUFDLFdBQVcsS0FBSSx3QkFBd0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsQ0FBQyxFQUFFO3dDQUM1UCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7cUNBQy9CO2dDQUNILENBQUMsQ0FBQyxDQUFDOzZCQUNKO2lDQUFNO2dDQUNMLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2QkFDL0I7eUJBQ0Y7cUJBRUY7b0JBRUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO3dCQUMzRSxJQUFHLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFDOzRCQUN0QixZQUFZLEdBQUcsSUFBSSxDQUFDO3lCQUNyQjtvQkFDSCxDQUFDLENBQUMsQ0FBQTtvQkFDRixJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxXQUFXLElBQUkscUJBQXFCLENBQUMsOEJBQThCLENBQUMsQ0FBQyx1REFBdUQsRUFBRTt3QkFDckosSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTs0QkFDdkcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0NBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUztnQ0FDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO2dDQUNkLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLHFCQUFxQixDQUFDO2dDQUNwVCxjQUFjLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUEsSUFBSSxDQUFBLENBQUMsQ0FBQSxLQUFLOzZCQUMzRCxDQUFDLENBQUM7eUJBQ0o7cUJBQ0Y7b0JBQ0QsbUNBQW1DO29CQUNuQywrR0FBK0c7b0JBQy9HLDZCQUE2QjtvQkFDN0IsOEJBQThCO29CQUM5Qix3QkFBd0I7b0JBQ3hCLDhUQUE4VDtvQkFDOVQsbUVBQW1FO29CQUNuRSxVQUFVO29CQUNWLE1BQU07b0JBQ04sSUFBSTtnQkFDTixDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7Z0JBQ3RCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3JGO2lCQUFNO2dCQUNMLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7Z0JBQ3ZELFVBQVUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO3dCQUNuQixJQUFJLEVBQUUsS0FBSyxDQUFDLFlBQVk7d0JBQ3hCLEtBQUssRUFBRSxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRTtxQkFDakMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3JGO1NBQ0Y7UUFHRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFOztZQUNyRixvR0FBb0c7WUFDcEcsdUdBQXVHO1lBQ3ZHLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0Qix1RUFBdUU7WUFDdEUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUMzRSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLENBQUM7Z0JBQ25FLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUM7WUFDTCxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFOztnQkFDN0IsSUFBRyxPQUFBLFFBQVEsMENBQUUsZ0JBQWdCLE1BQUssTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO29CQUNqRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQy9ELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztpQkFDbEY7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLG1FQUFtRTtZQUNuRSx1Q0FBdUM7WUFDdkMsSUFBSSxrQ0FBa0MsR0FBRyxJQUFJLENBQUM7WUFDOUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTs7Z0JBQzdCLElBQUcsQ0FBQyxDQUFDLE9BQUEsUUFBUSwwQ0FBRSxnQkFBZ0IsTUFBSyxXQUFXLENBQUMsY0FBYyxJQUFJLE9BQUEsUUFBUSwwQ0FBRSxnQkFBZ0IsTUFBSyxXQUFXLENBQUMsZUFBZSxJQUFJLE9BQUEsUUFBUSwwQ0FBRSxnQkFBZ0IsTUFBSyxXQUFXLENBQUMsZUFBZSxDQUFDLEVBQUM7b0JBQzFMLGtDQUFrQyxHQUFHLEtBQUssQ0FBQztpQkFDNUM7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUcsa0NBQWtDLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7Z0JBQzFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN0RixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLHdEQUF3RCxDQUFDLENBQUM7YUFDMUY7WUFFQyx5Q0FBeUM7WUFDekMsSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLE1BQU0sRUFBRTtnQkFDeEksSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0IsSUFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDO29CQUMzQixJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQztvQkFDakMsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUM1RCxNQUFNLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ2xDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0NBQzdCLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLEVBQUM7b0NBQ2xDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDaEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFOzRCQUMzRSxJQUFHLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFDO2dDQUN0QixjQUFjLEdBQUcsSUFBSSxDQUFDOzZCQUN2Qjt3QkFDSCxDQUFDLENBQUMsQ0FBQTt3QkFDRixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFOzRCQUNuSSxNQUFNLGtCQUFrQixHQUFHLEVBQUUsQ0FBQzs0QkFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0NBQ2xDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0NBQ3pFLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUM7d0NBQzlCLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQ0FDL0I7Z0NBQ0gsQ0FBQyxDQUFDLENBQUE7NEJBQ0osQ0FBQyxDQUFDLENBQUM7NEJBQ0wsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dDQUNoQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtvQ0FDcEosV0FBVyxHQUFHLElBQUksQ0FBQztpQ0FDcEI7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7eUJBQ0o7cUJBQ0Y7b0JBRUQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDM0csTUFBTSx3QkFBd0IsR0FBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUEsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFBO29CQUN6SCxJQUFHLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQUM7d0JBQ2xDLElBQUkseUJBQXlCLEdBQUcsS0FBSyxDQUFDO3dCQUN0QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFOzRCQUN6RSxJQUFHLFFBQVEsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFDO2dDQUMxQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7NkJBQ2xDO3dCQUNILENBQUMsQ0FBQyxDQUFBO3dCQUNKLElBQUksQ0FBQyxDQUFDLHlCQUF5QixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUNoTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQzVFLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQzNELE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0NBQ3ZLLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQ0FDdkQsa0JBQWtCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dDQUMxQyxJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxjQUFjLENBQUMsV0FBVyxLQUFJLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLEVBQUU7NENBQzVQLHFCQUFxQixHQUFHLEtBQUssQ0FBQzt5Q0FDL0I7b0NBQ0gsQ0FBQyxDQUFDLENBQUM7aUNBQ0o7cUNBQU07b0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDO2lDQUMvQjs2QkFDRjtpQ0FBTTtnQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7NkJBQy9CO3lCQUNGO3FCQUNGO29CQUNELElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxXQUFXLElBQUkscUJBQXFCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQ3JGLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRSx1REFBdUQ7NEJBQzFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dDQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtnQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztnQ0FDcFQsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsS0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSzs2QkFDM0QsQ0FBQyxDQUFDO3lCQUNKO3FCQUNGO2dCQUVILENBQUMsQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUN2QjtZQUdELDZCQUE2QjtZQUM3QixJQUFJLFdBQVcsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRTtnQkFDcEUsSUFBSSxvQkFBb0IsR0FBRyxLQUFLLENBQUM7Z0JBQ2pDLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUN2QyxJQUFHLE9BQUEsV0FBVyxDQUFDLENBQUMsQ0FBQywwQ0FBRSxnQkFBZ0IsTUFBSyxNQUFNLEVBQUM7d0JBQzdDLG9CQUFvQixHQUFHLElBQUksQ0FBQzt3QkFDNUIsTUFBTTtxQkFDUDtpQkFDRjtnQkFDRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEtBQUssUUFBUSxFQUFFO29CQUN2RSxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztvQkFFdEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2pDLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGVBQWU7NEJBQ2xHLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFOzRCQUUvRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQ0FDcEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO2dDQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUU7NkJBQ2xDLENBQUMsQ0FBQzt5QkFFSjtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDL0M7YUFDRjtZQUdELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMzRCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbEMseUNBQXlDO29CQUN6QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUN6RSxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFDOzRCQUM5QixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDOzRCQUMzRyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTtnQ0FDdkYsWUFBWSxHQUFHLElBQUksQ0FBQzs2QkFDckI7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUE7Z0JBQ0osQ0FBQyxDQUFDLENBQUE7YUFDSDtZQUNELElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFO2dCQUMzRCxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQ2hGLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUMzQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzdFLElBQUcsV0FBVyxJQUFJLFdBQVcsQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQUM7d0JBQ3hELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO3dCQUN6QixNQUFNO3FCQUNQO3lCQUFJO3dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO3FCQUMzQjtpQkFDRjthQUNGO1lBQ0QsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDckMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDL0Q7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDOUQ7WUFDRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBU0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNyRixJQUFJLFdBQVcsRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtvQkFDM0QsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29CQUNoRixLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQzt3QkFDM0MsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM3RSxJQUFHLFdBQVcsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDOzRCQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzs0QkFDekIsTUFBTTt5QkFDUDs2QkFBSTs0QkFDSCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzt5QkFDM0I7cUJBQ0Y7aUJBQ0Y7Z0JBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxXQUFXLEtBQUssUUFBUSxFQUFFO29CQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDakMsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsZUFBZTs0QkFDbEcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTs0QkFDMUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7Z0NBQ3RCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtnQ0FDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFOzZCQUNsQyxDQUFDLENBQUM7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU0sSUFBSSxXQUFXLEtBQUssUUFBUSxFQUFFO29CQUNyQyxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDekUsbUJBQW1CLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQzs0QkFDcEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJOzRCQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRTt5QkFDNUMsQ0FBQyxDQUFDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLE1BQU0sRUFBRTtvQkFDckUsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7b0JBQ3JCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQzNCLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDO3dCQUNqQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUMzRyxNQUFNLHdCQUF3QixHQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQSxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSSxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUE7d0JBQ3pILElBQUcsSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQzs0QkFDbEMsSUFBSSx5QkFBeUIsR0FBRyxLQUFLLENBQUM7NEJBQ3RDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0NBQ3pFLElBQUcsUUFBUSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUM7b0NBQzFDLHlCQUF5QixHQUFHLElBQUksQ0FBQztpQ0FDbEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7NEJBQ0osSUFBSSxDQUFDLENBQUMseUJBQXlCLElBQUksQ0FBQyxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3hMLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQzNELE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0NBQ3ZLLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDdkQsa0JBQWtCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFOzRDQUMxQyxJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxjQUFjLENBQUMsV0FBVyxLQUFJLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLEVBQUU7Z0RBQzVQLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2Q0FDL0I7d0NBQ0gsQ0FBQyxDQUFDLENBQUM7cUNBQ0o7eUNBQU07d0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3FDQUMvQjtpQ0FDRjtxQ0FBTTtvQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7aUNBQy9COzZCQUNGO3lCQUNGO3dCQUNDLElBQUkscUJBQXFCLEVBQUU7NEJBQ3pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsRUFBRSx1REFBdUQ7Z0NBQ2pMLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO29DQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztvQ0FDblQsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsS0FBRyxNQUFNLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSztpQ0FDM0QsQ0FBQyxDQUFDOzZCQUNKO3lCQUNGO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7YUFFNUQ7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDckYsSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLE1BQU0sRUFBRTtnQkFDcEYsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFBO2dCQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzNDLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hGLElBQUcsS0FBSyxHQUFHLENBQUMsRUFBQzt3QkFDWCxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUM3QjtnQkFDSCxDQUFDLENBQUMsQ0FBQTtnQkFDRixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDckIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0IsSUFBSSxjQUFjLEdBQUcsS0FBSyxDQUFDO29CQUMzQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMzRCxNQUFNLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzt3QkFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ2xDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0NBQ3pFLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLEVBQUM7b0NBQ2xDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDaEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFOzRCQUMzRSxJQUFHLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFDO2dDQUN0QixjQUFjLEdBQUcsSUFBSSxDQUFDOzZCQUN2Qjt3QkFDSCxDQUFDLENBQUMsQ0FBQTt3QkFDRixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7NEJBQ3hFLE1BQU0sa0JBQWtCLEdBQUcsRUFBRSxDQUFDOzRCQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQ0FDbEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtvQ0FDekUsSUFBRyxJQUFJLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBQzt3Q0FDOUIsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FDQUMvQjtnQ0FDSCxDQUFDLENBQUMsQ0FBQTs0QkFDSixDQUFDLENBQUMsQ0FBQzs0QkFDTCxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0NBQ2hDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssV0FBVyxFQUFFO29DQUN4RyxXQUFXLEdBQUcsSUFBSSxDQUFDO2lDQUNwQjs0QkFDSCxDQUFDLENBQUMsQ0FBQzt5QkFDSjtxQkFDRjtvQkFDRCxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUMzRyxNQUFNLHdCQUF3QixHQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQSxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSSxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUE7b0JBQ3pILElBQUkseUJBQXlCLEdBQUcsS0FBSyxDQUFDO29CQUN0QyxJQUFHLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQUM7d0JBQ2xDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7NEJBQ3pFLElBQUcsUUFBUSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUM7Z0NBQzFDLHlCQUF5QixHQUFHLElBQUksQ0FBQzs2QkFDbEM7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osSUFBSSxDQUFDLENBQUMseUJBQXlCOytCQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQ0FDdEgsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUM1RSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUMzRCxNQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dDQUV2SyxzSEFBc0g7Z0NBQ3RILEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0NBQzVDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7d0NBQzlCLElBQUcsUUFBUSxLQUFLLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBQzs0Q0FDbkQsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQzt5Q0FDaEM7b0NBQ0gsQ0FBQyxDQUFDLENBQUE7aUNBQ0g7Z0NBQ0QsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29DQUN2RCxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7d0NBQzFDLElBQUksQ0FBQyxDQUFDLGNBQWMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxJQUFJLGNBQWMsQ0FBQyxXQUFXLEtBQUksd0JBQXdCLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUMsRUFBRTs0Q0FDNVAscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3lDQUMvQjtvQ0FDSCxDQUFDLENBQUMsQ0FBQztpQ0FDSjtxQ0FBTTtvQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7aUNBQy9COzZCQUNGO2lDQUFNO2dDQUNMLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2QkFDL0I7eUJBQ0Y7cUJBQ0Y7b0JBQ0MsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLFdBQVcsSUFBSSxxQkFBcUIsRUFBRTt3QkFDNUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxFQUFFLHVEQUF1RDs0QkFDakwsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0NBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUztnQ0FDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO2dDQUNkLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLHFCQUFxQixDQUFDO2dDQUNwVCxjQUFjLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixLQUFJLE1BQU0sQ0FBRSxDQUFDLENBQUEsSUFBSSxDQUFBLENBQUMsQ0FBQSxLQUFLOzZCQUM5RCxDQUFDLENBQUM7eUJBQ0g7cUJBQ0Y7Z0JBQ0QsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUNELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDbkYsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxVQUFVLEtBQUssT0FBTyxFQUFFO29CQUMxQixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUN2RCxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQzs0QkFDbkIsSUFBSSxFQUFFLEtBQUssQ0FBQyxZQUFZOzRCQUN4QixLQUFLLEVBQUUsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUU7eUJBQ2pDLENBQUMsQ0FBQztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDSjtxQkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7b0JBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUMzQixJQUFJLGNBQWMsR0FBRyxLQUFLLENBQUM7d0JBQzNCLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDO3dCQUNqQyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7d0JBQ3hCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTs0QkFDM0UsSUFBRyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBQztnQ0FDdEIsY0FBYyxHQUFHLElBQUksQ0FBQzs2QkFDdkI7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDNUQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtnQ0FDbEksTUFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7Z0NBQzlCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO29DQUNsQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dDQUN6RSxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFDOzRDQUM5QixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7eUNBQy9CO29DQUNILENBQUMsQ0FBQyxDQUFBO2dDQUNKLENBQUMsQ0FBQyxDQUFDO2dDQUNILGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQ0FDaEMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7d0NBQ3BKLFdBQVcsR0FBRyxJQUFJLENBQUM7cUNBQ3BCO2dDQUNILENBQUMsQ0FBQyxDQUFDOzZCQUNKO3lCQUNGO3dCQUNELE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBQzNHLE1BQU0sd0JBQXdCLEdBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFBLEVBQUUsQ0FBQyxVQUFVLENBQUMsV0FBVyxLQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQTt3QkFDekgsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDOzRCQUNsQyxJQUFJLHlCQUF5QixHQUFHLEtBQUssQ0FBQzs0QkFDdEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtnQ0FDekUsSUFBRyxRQUFRLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBQztvQ0FDMUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO2lDQUNsQzs0QkFDSCxDQUFDLENBQUMsQ0FBQTs0QkFDRixJQUFJLENBQUMsQ0FBQyx5QkFBeUI7bUNBQzFCLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDO29DQUNsSyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQzVFLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQzNELE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0NBQ3ZLLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDdkQsa0JBQWtCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFOzRDQUMxQyxJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxjQUFjLENBQUMsV0FBVyxLQUFJLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLEVBQUU7Z0RBQzVQLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2Q0FDL0I7d0NBQ0gsQ0FBQyxDQUFDLENBQUM7cUNBQ0o7eUNBQU07d0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3FDQUMvQjtpQ0FDRjtxQ0FBTTtvQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7aUNBQy9COzZCQUNGO3lCQUNKO3dCQUNDLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxXQUFXLElBQUkscUJBQXFCLEVBQUU7NEJBQzVELElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxPQUFPLEVBQUcsRUFBRSx1REFBdUQ7Z0NBQzNHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO29DQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztvQ0FDcFQsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsS0FBSSxNQUFNLENBQUMsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSztpQ0FDNUQsQ0FBQyxDQUFDOzZCQUNKO3lCQUNGO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDM0Q7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDakYsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFHSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQztJQUNILENBQUM7SUFFSCxRQUFRO1FBQ04sTUFBTSxjQUFjLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUMzQyxJQUFHLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFDO29CQUN0QixjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDcEM7WUFDSCxDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO1FBQ0YsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2SyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1SCxDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsZUFBZTs7UUFDYixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzdHLDhCQUE4QjtRQUMzQixJQUFHLE9BQUEsSUFBSSxDQUFDLHlCQUF5QiwwQ0FBRSxnQkFBZ0IsTUFBSSxNQUFNLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUM7WUFDdkosTUFBTSxhQUFhLEdBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFBLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUEsQ0FBQSwwQ0FBMEM7WUFDdkwsSUFBSSxDQUFDLFVBQVUsR0FBRSxhQUFhLENBQUE7U0FDOUI7YUFFRyxJQUFHLE9BQUEsSUFBSSxDQUFDLHlCQUF5QiwwQ0FBRSxnQkFBZ0IsTUFBSSxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQzNKLE1BQU0sUUFBUSxHQUFFLE9BQUEsSUFBSSxDQUFDLHlCQUF5QiwwQ0FBRSxjQUFjLElBQUMsRUFBRSxDQUFDO1lBQ2xFLE1BQU0sZ0JBQWdCLEdBQUUsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNqQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQSxFQUFFO2dCQUNuQyxJQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFDO29CQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUEsRUFBRSxDQUFBLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBO2lCQUMvRDtnQkFDRiwrUEFBK1A7Z0JBQ2hRLE1BQU0sYUFBYSxHQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQSxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO2dCQUMzSSxJQUFJLENBQUMsVUFBVSxHQUFFLGFBQWEsQ0FBQTtZQUMvQixDQUFDLENBQUMsQ0FBQTtTQUNIO2FBQ0ssSUFBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBQztZQUMzRixJQUFJLGFBQWEsR0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUEsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQSxDQUFBLDBDQUEwQztZQUVyTCxJQUFJLENBQUMsVUFBVSxHQUFFLGFBQWEsQ0FBQTtTQUM5QjthQUNHO1lBQ0wsTUFBTSxhQUFhLEdBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFBLEVBQUUsQ0FBQyx1RUFBdUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7WUFDck4sSUFBSSxDQUFDLFVBQVUsR0FBRSxhQUFhLENBQUE7U0FDN0I7SUFDTCxDQUFDO0NBQ0EsQ0FBQTs7WUE5c0N5QixhQUFhO1lBQ1osY0FBYztZQUNqQixXQUFXO1lBQ0gsbUJBQW1CO1lBQ3BCLGtCQUFrQjtZQUM5QixTQUFTOztBQXhEakI7SUFBUixLQUFLLEVBQUU7OERBQVU7QUFDVDtJQUFSLEtBQUssRUFBRTs4REFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO3FFQUFpQjtBQUNoQjtJQUFSLEtBQUssRUFBRTs4REFBaUI7QUFDaEI7SUFBUixLQUFLLEVBQUU7c0VBQWtCO0FBQ2pCO0lBQVIsS0FBSyxFQUFFO3VFQUFtQjtBQUNsQjtJQUFSLEtBQUssRUFBRTttRUFBZTtBQUNiO0lBQVQsTUFBTSxFQUFFOzBFQUFnRDtBQUMvQztJQUFULE1BQU0sRUFBRTsrRUFBcUQ7QUFDcEQ7SUFBVCxNQUFNLEVBQUU7bUZBQXlEO0FBR3hEO0lBQVQsTUFBTSxFQUFFO3NFQUE0QztBQUMzQztJQUFULE1BQU0sRUFBRTtnRkFBc0Q7QUFDdEQ7SUFBUixLQUFLLEVBQUU7dUVBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFOytFQUEwQjtBQUN6QjtJQUFSLEtBQUssRUFBRTt1RUFBbUI7QUFsQmhCLDRCQUE0QjtJQUx4QyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsMkJBQTJCO1FBQ3JDLG80R0FBcUQ7O0tBRXRELENBQUM7R0FDVyw0QkFBNEIsQ0Ftd0N4QztTQW53Q1ksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBmb3JrSm9pbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN0YXR1c1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvc3RhdHVzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0VXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC11dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uL3Rhc2suc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jdXN0b20td29ya2Zsb3ctZmllbGQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jdXN0b20td29ya2Zsb3ctZmllbGQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2N1c3RvbS13b3JrZmxvdy1maWVsZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDdXN0b21Xb3JrZmxvd0ZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgcnVsZURhdGE7XHJcbiAgQElucHV0KCkgcnVsZUxpc3Q7XHJcbiAgQElucHV0KCkgcHJvamVjdFJ1bGVMaXN0O1xyXG4gIEBJbnB1dCgpIHRhc2tEYXRhOiBhbnlbXTtcclxuICBASW5wdXQoKSBpc1J1bGVzSW5pdGlhdGVkO1xyXG4gIEBJbnB1dCgpIGlzQ3VycmVudFRhc2tSdWxlO1xyXG4gIEBJbnB1dCgpIHJ1bGVMaXN0R3JvdXA7XHJcbiAgQE91dHB1dCgpIGNsb3NlQ2FsbGJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHJlbW92ZVdvcmtmbG93UnVsZUhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgZW5hYmxlSW5pdGlhdGVXb3JrZmxvd0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAvL0BPdXRwdXQoKSBpc1Rhc2tMZXZlbFJ1bGVFZGl0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgaXN0YXNrUnVsZUVkaXQ9ZmFsc2U7XHJcbiAgQE91dHB1dCgpIHJ1bGVHcm91cEhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgcnVsZURlcGVuZGVuY3lFcnJvckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBASW5wdXQoKSBpc1Rhc2tMZXZlbGNhbGxlZDtcclxuICBASW5wdXQoKSB0YXNrV29ya0Zsb3dSdWxlVG9EaXNwbGF5XHJcbiAgQElucHV0KCkgdGFza3J1bGVMaXN0R3JvdXA7XHJcbiAgLy9ASW5wdXQoKSBhbGxSdWxlTGlzdEdyb3VwOyBNVlNTLTMyMlxyXG4gICBpc0NoaWxkRGVwZW5kZWNpZXM9IGZhbHNlO1xyXG4gIGN1c3RvbVdvcmtmbG93RmllbGRGb3JtOiBGb3JtR3JvdXA7XHJcbiAgdHJpZ2dlclR5cGVzID0gW3tcclxuICAgIG5hbWU6ICdTdGF0dXMnLFxyXG4gICAgdmFsdWU6ICdTVEFUVVMnXHJcbiAgfSwge1xyXG4gICAgbmFtZTogJ0FjdGlvbicsXHJcbiAgICB2YWx1ZTogJ0FDVElPTidcclxuICB9XTtcclxuICB0YXJnZXRUeXBlcyA9IFt7XHJcbiAgICBuYW1lOiAnVGFzaycsXHJcbiAgICB2YWx1ZTogJ1RBU0snXHJcbiAgfSwge1xyXG4gICAgbmFtZTogJ0V2ZW50JyxcclxuICAgIHZhbHVlOiAnRVZFTlQnXHJcbiAgfV07XHJcbiAgdHJpZ2dlckRhdGEgPSBbXTtcclxuICB0YXJnZXREYXRhOmFueSA9IFtdO1xyXG4gIHRhc2tMaXN0ID0gW107XHJcbiAgcHJvamVjdElkO1xyXG4gIGlzUnVsZUNoYW5nZWQ7XHJcbiAgdGFza1N0YXR1c2VzO1xyXG4gIGN1cnJlbnRSdWxlTGlzdDtcclxuICBhbGxSdWxlTGlzdCA9IFtdO1xyXG4gIGlzUnVsZVNhdmVkO1xyXG4gIGFwcHJvdmFsVGFzaztcclxuICBjdXJyZW50U291cmNlVGFza3MgPSBbXTtcclxuICB0YXJnZXRUYXNrOiBhbnk7XHJcbiAgbm9QcmVkZWNlc3NvclJ1bGVMaXN0ID0gW107XHJcbiAgaHR0cFJlcXVlc3RBcnJheSA9IFtdO1xyXG4gIHNlbGVjdGVkVHJpZ2dlckRhdGE7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIHN0YXR1c1NlcnZpY2U6IFN0YXR1c1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIHByb2plY3RVdGlsU2VydmljZTogUHJvamVjdFV0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nXHJcbiAgKSB7IH1cclxuICBcclxuICAvL2ZvciBFZGl0aW5nIHdvcmtmbG93IFJ1bGVzLi4uXHJcbiAgdXBkYXRlV29ya2Zsb3dSdWxlKCl7XHJcbiAgICBpZih0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmxlbmd0aCA+IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGgpe1xyXG4gICAgICAvL1JlbW92aW5nIGEgcnVsZSBmcm9tIHRoZSBzb3VyY2UgdGFza3MgbGlzdFxyXG4gICAgICBjb25zdCBuZXdUYXNrcyA9IFtdO1xyXG4gICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPXRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5pbmRleE9mKHRhc2spO1xyXG4gICAgICAgIGlmKGluZGV4IDwgMCl7XHJcbiAgICAgICAgICBuZXdUYXNrcy5wdXNoKHRhc2spO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgbGV0IGRlbGV0ZWRSdWxlcyA9IFtdXHJcbiAgICAgIG5ld1Rhc2tzLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgY29uc3QgcnVsZSA9e1xyXG4gICAgICAgIGN1cnJlbnRUYXNrOiB0YXNrLFxyXG4gICAgICAgIGlzSW5pdGlhbFJ1bGU6IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSxcclxuICAgICAgICB0YXJnZXREYXRhOiB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEsXHJcbiAgICAgICAgdGFyZ2V0VHlwZTogdGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlLFxyXG4gICAgICAgIHRyaWdnZXJEYXRhOiB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhLFxyXG4gICAgICAgIHRyaWdnZXJUeXBlOiB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJUeXBlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRlbGV0ZWRSdWxlcy5wdXNoKHJ1bGUpO1xyXG4gICAgICB9KVxyXG4gICAgICBsZXQgb3JpZ2luYWxSdWxlcyA9IFtdO1xyXG4gICAgICBmb3IobGV0IGk9MDsgaSA8IHRoaXMucnVsZURhdGEuY3VycmVudFRhc2subGVuZ3RoOyBpKyspe1xyXG4gICAgICAgIGNvbnN0IHJ1bGUgPXtcclxuICAgICAgICAgIGN1cnJlbnRUYXNrOiB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrW2ldLFxyXG4gICAgICAgICAgaXNJbml0aWFsUnVsZTogdGhpcy5ydWxlRGF0YS5pc0luaXRpYWxSdWxlLFxyXG4gICAgICAgICAgdGFyZ2V0RGF0YTogdGhpcy5ydWxlRGF0YS50YXJnZXREYXRhLFxyXG4gICAgICAgICAgdGFyZ2V0VHlwZTogdGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlLFxyXG4gICAgICAgICAgdHJpZ2dlckRhdGE6IHRoaXMucnVsZURhdGEudHJpZ2dlckRhdGEsXHJcbiAgICAgICAgICB0cmlnZ2VyVHlwZTogdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZSxcclxuICAgICAgICAgIHdvcmtmbG93UnVsZUlkOiB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkW2ldXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgb3JpZ2luYWxSdWxlcy5wdXNoKHJ1bGUpO1xyXG4gICAgICB9XHJcbiAgICAgIGxldCBkZWxldGVkUnVsZUlkcyA9IFtdO1xyXG4gICAgICBkZWxldGVkUnVsZXMuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBvcmlnaW5hbFJ1bGVzLmZvckVhY2goZWFjaFJ1bGUgPT4ge1xyXG4gICAgICAgICAgaWYocnVsZS5jdXJyZW50VGFzayA9PSBlYWNoUnVsZS5jdXJyZW50VGFzayl7XHJcbiAgICAgICAgICAgIGRlbGV0ZWRSdWxlSWRzLnB1c2goZWFjaFJ1bGUud29ya2Zsb3dSdWxlSWQpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pXHJcbiAgICAgIGNvbnN0IG90aGVyUGFyYW1zQ2hhbmdlZCA9ICgodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJUeXBlICE9PSB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJUeXBlKVxyXG4gICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJEYXRhICE9PSB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhKVxyXG4gICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldFR5cGUgIT09IHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSlcclxuICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXREYXRhICE9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpXHJcbiAgICAgIHx8ICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuaXNJbml0aWFsUnVsZSAhPT0gdGhpcy5ydWxlRGF0YS5pc0luaXRpYWxSdWxlKSk7XHJcbiAgICAgIGlmKG90aGVyUGFyYW1zQ2hhbmdlZClcclxuICAgICAgIHtcclxuICAgICAgICAgIGZvcihsZXQgaT0wOyBpPHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgICAgIHJ1bGUuY3VycmVudFRhc2sgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2tbaV07XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheS5wdXNoKHRoaXMudGFza1NlcnZpY2UuY3JlYXRlV29ya2Zsb3dSdWxlKHJ1bGUsdGhpcy5wcm9qZWN0SWQsdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZFtpXSkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgfVxyXG4gICAgICBsZXQgYXBwcm92YWxUYXNrcyA9IHRoaXMudGFza0RhdGEuZmlsdGVyKHRhc2sgPT4gdGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpO1xyXG4gICAgICBsZXQgcmVtb3ZlZFBhcmVudFRhc2tzID0gW107XHJcbiAgICAgIGFwcHJvdmFsVGFza3MuZm9yRWFjaChhcHByb3ZhbFRhc2sgPT4ge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gbmV3VGFza3MuaW5kZXhPZihTdHJpbmcoYXBwcm92YWxUYXNrLlRBU0tfUEFSRU5UX0lEKSk7XHJcbiAgICAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgICByZW1vdmVkUGFyZW50VGFza3MucHVzaChuZXdUYXNrc1tpbmRleF0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgLy9jaGVja2luZyBpZiB0aGUgcmVtb3ZlZCB0YXNrIGlzIGEgcGFyZW50IHRhc2sgaGVuY2UgY2hlY2tpbmcgZGVwZW5kZW5jaWVzLi4uXHJcbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbiAgICAgIGxldCBhcHByb3ZhbFRhc2tzSWRMaXN0ID0gW107XHJcbiAgICAgIHJlbW92ZWRQYXJlbnRUYXNrcy5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICBjb25zdCBhcHByb3ZhbFRhc2tJZHMgPSB0aGlzLnRhc2tEYXRhLmZpbHRlcih0YXNrID0+IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSA9PSBlYWNoVGFzayApO1xyXG4gICAgICAgIGFwcHJvdmFsVGFza3NJZExpc3QucHVzaCguLi5hcHByb3ZhbFRhc2tJZHMpO1xyXG4gICAgICB9KTtcclxuICAgICAgbGV0IGFsbFRhc2tzaW5SdWxlcyA9IFtdO1xyXG4gICAgICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBhbGxUYXNrc2luUnVsZXMucHVzaCguLi5ydWxlLmN1cnJlbnRUYXNrKTtcclxuICAgICAgICBhbGxUYXNrc2luUnVsZXMucHVzaChydWxlLnRhcmdldERhdGEpO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGxldCBhZmZlY3RlZEFwcHJvdmFsVGFza3MgPSBbXTtcclxuICAgICAgYXBwcm92YWxUYXNrc0lkTGlzdC5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IGFsbFRhc2tzaW5SdWxlcy5pbmRleE9mKGVhY2hUYXNrLklEKTtcclxuICAgICAgICBpZihpbmRleCA+PSAwKXtcclxuICAgICAgICAgIGFmZmVjdGVkQXBwcm92YWxUYXNrcy5wdXNoKGVhY2hUYXNrKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBcclxuICAgICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiBydWxlLmhhc1ByZWRlY2Vzc29yID0gdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PntcclxuICAgICAgICAgIGxldCBydWxlVGFza3MgPSBbXTtcclxuICAgICAgICAgIHJ1bGVUYXNrcy5wdXNoKC4uLnJ1bGUuY3VycmVudFRhc2spO1xyXG4gICAgICAgICAgcnVsZVRhc2tzLnB1c2gocnVsZS50YXJnZXREYXRhKTtcclxuICAgICAgICAgIGFmZmVjdGVkQXBwcm92YWxUYXNrcy5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaW5kZXggPSBydWxlVGFza3MuaW5kZXhPZihlYWNoVGFzay5JRCk7XHJcbiAgICAgICAgICAgIGlmKGluZGV4ID49IDApe1xyXG4gICAgICAgICAgICAgIHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9KVxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuICAgICAgICBpZighKGFmZmVjdGVkQXBwcm92YWxUYXNrcyAmJiBhZmZlY3RlZEFwcHJvdmFsVGFza3MubGVuZ3RoID4gMCkpe1xyXG4gICAgICAgIGxldCBydWxlSWRzID0gW11cclxuICAgICAgICBkZWxldGVkUnVsZUlkcy5mb3JFYWNoKHdvcmtmbG93SWQgPT4ge1xyXG4gICAgICAgIGNvbnN0IFJ1bGUgPSB7XHJcbiAgICAgICAgICBJZDogd29ya2Zsb3dJZFxyXG4gICAgICAgIH1cclxuICAgICAgICBydWxlSWRzLnB1c2goUnVsZSk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnJlbW92ZVdvcmtmbG93UnVsZShydWxlSWRzLHt9KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4geyBcclxuICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAvLyB0aGlzLnJlbW92ZVdvcmtmbG93UnVsZUhhbmRsZXIubmV4dCh0aGlzLnJ1bGVEYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKHRhc2sgPT4gdGhpcy5jdXJyZW50U291cmNlVGFza3MucHVzaCh0YXNrKSk7XHJcbiAgICAgICAgICAgIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2sgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLmlzUnVsZVNhdmVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVJbml0aWF0ZVdvcmtmbG93SGFuZGxlci5uZXh0KCk7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdXb3JrZmxvdyBSdWxlIGhhcyBiZWVuIHJlbW92ZWQnKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgcmVtb3Zpbmcgd29ya2Zsb3cgcnVsZScpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0VkaXQgT3BlcmF0aW9uIGNvdWxkIG5vdCBiZSBQZXJmb3JtZWQgYmVjYXVzZSBJdCBpcyBhIFBhcmVudCBUYXNrIGFuZCBpdCBXaWxsIGFmZmVjdCB0aGUgaGlnaGxpZ2h0ZWQgcnVsZXMnKTtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgIH1cclxuICAgIGVsc2UgaWYodGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5sZW5ndGggPCB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoKXtcclxuICAgICAgLy9BZGRpbmcgYSBydWxlIHRvIHRoZSBzb3VyY2UgdGFza3MgbGlzdFxyXG4gICAgICBjb25zdCBuZXdUYXNrcyA9IFtdO1xyXG4gICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2suZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICBjb25zdCBpbmRleCA9dGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5pbmRleE9mKHRhc2spO1xyXG4gICAgICAgIGlmKGluZGV4IDwgMCl7XHJcbiAgICAgICAgICBuZXdUYXNrcy5wdXNoKHRhc2spO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgdGhpcy5odHRwUmVxdWVzdEFycmF5ID0gW107XHJcbiAgICAgIGlmKCh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudHJpZ2dlclR5cGUgIT09IHRoaXMucnVsZURhdGEudHJpZ2dlclR5cGUpXHJcbiAgICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJEYXRhICE9PSB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhKVxyXG4gICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXRUeXBlICE9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGUpXHJcbiAgICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldERhdGEgIT09IHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSlcclxuICAgICAgIHx8ICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuaXNJbml0aWFsUnVsZSAhPT0gdGhpcy5ydWxlRGF0YS5pc0luaXRpYWxSdWxlKSlcclxuICAgICAgIHtcclxuICAgICAgICAgIGZvcihsZXQgaT0wOyBpPHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgICAgIHJ1bGUuY3VycmVudFRhc2sgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2tbaV07XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheS5wdXNoKHRoaXMudGFza1NlcnZpY2UuY3JlYXRlV29ya2Zsb3dSdWxlKHJ1bGUsdGhpcy5wcm9qZWN0SWQsdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZFtpXSkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgfWVsc2V7XHJcbiAgICAgICAgbmV3VGFza3MuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgIGNvbnN0IHJ1bGUgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgICBydWxlLmN1cnJlbnRUYXNrID0gdGFzaztcclxuICAgICAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheS5wdXNoKHRoaXMudGFza1NlcnZpY2UuY3JlYXRlV29ya2Zsb3dSdWxlKHJ1bGUsIHRoaXMucHJvamVjdElkLCAnJykpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gIHVwZGF0ZVZhbGlkYXRpb24oKXtcclxuICAgIGxldCBhbGxTb3VyY2VUYXNrcyA9IFtdO1xyXG4gICAgaWYoIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgdGhpcy5ydWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICBpZighcnVsZS5pc0luaXRpYWxSdWxlKXtcclxuICAgICAgICBhbGxTb3VyY2VUYXNrcy5wdXNoKC4uLnJ1bGUuY3VycmVudFRhc2spXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfVxyXG4gIGVsc2V7XHJcbiAgICB0aGlzLnRhc2tydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgIGlmKCFydWxlLmlzSW5pdGlhbFJ1bGUpe1xyXG4gICAgICAgIGFsbFNvdXJjZVRhc2tzLnB1c2goLi4ucnVsZS5jdXJyZW50VGFzaylcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9XHJcbiAgIGNvbnN0IGluZGV4ID0gYWxsU291cmNlVGFza3MuaW5kZXhPZih0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICBpZihpbmRleCA+PSAwKXtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrKTtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhKTtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcbiAgICAgIHRoaXMuaXNSdWxlU2F2ZWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoXCJDYW5ub3QgZWRpdCB0YXJnZXRUYXNrIGFzIGl0IGhhcyBkZXBlbmRlbmNpZXNcIik7XHJcbiAgICB9ZWxzZSBpZihKU09OLnN0cmluZ2lmeSh0aGlzLnJ1bGVEYXRhKSAhPT0gSlNPTi5zdHJpbmdpZnkodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpICYmICF0aGlzLmlzVGFza0xldmVsY2FsbGVkKSBcclxuICAgICAgJiYgdGhpcy5ydWxlRGF0YS5pc0V4aXN0XHJcbiAgICAgICYmIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2subGVuZ3RoICE9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGgpe1xyXG4gICAgICAgIHRoaXMudXBkYXRlV29ya2Zsb3dSdWxlKCk7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIGVsc2UgaWYgKCF0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgICAgdGhpcy5odHRwUmVxdWVzdEFycmF5ID0gW107XHJcbiAgICAgIGZvcihsZXQgaT0wOyBpIDwgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrLmxlbmd0aDsgaSsrICl7XHJcbiAgICAgICAgY29uc3Qgd29ya2Zsb3dSdWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgIHdvcmtmbG93UnVsZS5jdXJyZW50VGFzayA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFza1tpXTtcclxuICAgICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZSh3b3JrZmxvd1J1bGUsIHRoaXMucHJvamVjdElkLCB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkW2ldKSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNoZWNrVGFza0RlcGVuZGVuY2llcygpOmFueXtcclxuICAgIFxyXG4gICAgbGV0IGFsbFNvdXJjZVRhc2tzID0gW107XHJcbiAgICAgIHRoaXMudGFza3J1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBpZighcnVsZS5pc0luaXRpYWxSdWxlKXtcclxuICAgICAgICAgIGFsbFNvdXJjZVRhc2tzLnB1c2goLi4ucnVsZS5jdXJyZW50VGFzaylcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgY29uc3QgaW5kZXggPSBhbGxTb3VyY2VUYXNrcy5pbmRleE9mKHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcbiAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrKTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudHJpZ2dlckRhdGEpO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgICAgIHRoaXMuaXNSdWxlU2F2ZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihcIkNhbm5vdCBlZGl0IHRhcmdldFRhc2sgYXMgaXQgaGFzIGRlcGVuZGVuY2llc1wiKTtcclxuICAgICAgICB0aGlzLmlzQ2hpbGREZXBlbmRlY2llcz10cnVlO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLmlzQ2hpbGREZXBlbmRlY2llc1xyXG4gIH1cclxuXHJcbiAgYWRkV29ya2Zsb3dSdWxlKCkge1xyXG4gICAgaWYodGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCAmJiAhdGhpcy5jaGVja1Rhc2tEZXBlbmRlbmNpZXMoKSl7XHJcbiAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheT0gW11cclxuICAgICBmb3IobGV0IGk9MDsgaSA8IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGg7IGkrKyApe1xyXG4gICAgICB2YXIgd29ya2Zsb3dGbG93T2JqZWN0PSBbXTtcclxuICAgICAgd29ya2Zsb3dGbG93T2JqZWN0LnB1c2goe1xyXG4gICAgICAgIGN1cnJlbnRUYXNrOiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlW2ldLFxyXG4gICAgICAgIHRyaWdnZXJUeXBlOiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlLFxyXG4gICAgICAgIHRhcmdldFR5cGU6IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0VHlwZS52YWx1ZSxcclxuICAgICAgICB0cmlnZ2VyRGF0YTogdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSxcclxuICAgICAgICB0YXJnZXREYXRhOiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEudmFsdWUsXHJcbiAgICAgICAgaXNJbml0aWFsUnVsZTogdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5pc0luaXRpYWxSdWxlLnZhbHVlLFxyXG4gICAgIH0pIFxyXG4gICAgIFxyXG4gICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZShKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHdvcmtmbG93Rmxvd09iamVjdFswXSkpLCB0aGlzLnByb2plY3RJZCwgdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZFtpXSkpO1xyXG4gICAgICAvLyB0aGlzLmlzUnVsZVNhdmVkPSB0cnVlXHJcbiAgICAgIC8vIHRoaXMuaXN0YXNrUnVsZUVkaXQ9dHJ1ZVxyXG4gICAgICAvLyB0aGlzLmVuYWJsZUluaXRpYXRlV29ya2Zsb3dIYW5kbGVyLm5leHQodGhpcy5pc3Rhc2tSdWxlRWRpdCk7XHJcbiAgICB9XHJcbiAgICB9XHJcbiAgIGVsc2UgaWYodGhpcy5ydWxlRGF0YS50YXJnZXREYXRhICE9PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudGFyZ2V0RGF0YSAmJiB0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgJiYhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgIHRoaXMudXBkYXRlVmFsaWRhdGlvbigpO1xyXG4gICAgfWVsc2UgaWYoSlNPTi5zdHJpbmdpZnkodGhpcy5ydWxlRGF0YSkgIT09IEpTT04uc3RyaW5naWZ5KHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKSAmJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCkgXHJcbiAgICAmJiB0aGlzLnJ1bGVEYXRhLmlzRXhpc3RcclxuICAgICYmIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2subGVuZ3RoICE9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGgpe1xyXG4gICAgICB0aGlzLnVwZGF0ZVdvcmtmbG93UnVsZSgpO1xyXG4gICAgfWVsc2UgaWYoIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkgPSBbXTtcclxuICAgICAgZm9yKGxldCBpPTA7IGkgPCB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoOyBpKysgKXtcclxuICAgICAgICBjb25zdCB3b3JrZmxvd1J1bGUgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgd29ya2Zsb3dSdWxlLmN1cnJlbnRUYXNrID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrW2ldO1xyXG4gICAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheS5wdXNoKHRoaXMudGFza1NlcnZpY2UuY3JlYXRlV29ya2Zsb3dSdWxlKHdvcmtmbG93UnVsZSwgdGhpcy5wcm9qZWN0SWQsIHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWRbaV0pKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvcmtKb2luKHRoaXMuaHR0cFJlcXVlc3RBcnJheSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgaWYocmVzcG9uc2Upe1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2sgPSBbdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrXTtcclxuICAgICAgICB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJUeXBlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJUeXBlO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEudHJpZ2dlckRhdGEgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudHJpZ2dlckRhdGE7XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldFR5cGU7XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS50YXJnZXREYXRhID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldERhdGE7XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS5pc0luaXRpYWxSdWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmlzSW5pdGlhbFJ1bGU7XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS5pc1J1bGVBZGRlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZCA9IFtdO1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpIDwgcmVzcG9uc2UubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZC5wdXNoKHJlc3BvbnNlW2ldWydXb3JrZmxvd19SdWxlcy1pZCddICYmIHJlc3BvbnNlW2ldWydXb3JrZmxvd19SdWxlcy1pZCddLklkID8gcmVzcG9uc2VbaV1bJ1dvcmtmbG93X1J1bGVzLWlkJ10uSWQgOiBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5ydWxlR3JvdXBIYW5kbGVyLm5leHQoKTtcclxuICAgICAgICB0aGlzLmlzUnVsZVNhdmVkID0gdHJ1ZTtcclxuICAgICAgICBpZighdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgICAgdGhpcy5lbmFibGVJbml0aWF0ZVdvcmtmbG93SGFuZGxlci5uZXh0KHRoaXMuaXN0YXNrUnVsZUVkaXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgdGhpcy5pc3Rhc2tSdWxlRWRpdD10cnVlO1xyXG4gICAgICAgICAgdGhpcy5lbmFibGVJbml0aWF0ZVdvcmtmbG93SGFuZGxlci5uZXh0KHRoaXMuaXN0YXNrUnVsZUVkaXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgLy8gICBlbHNlIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICAvLyAgICAgdGhpcy5pc3Rhc2tSdWxlRWRpdD0gdHJ1ZVxyXG4gICAgICAvLyAgICAgdGhpcy5pc1Rhc2tMZXZlbFJ1bGVFZGl0LmVtaXQodGhpcy5pc3Rhc2tSdWxlRWRpdClcclxuICAgICAgLy8gICB9XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1dvcmtmbG93IFJ1bGUgaGFzIGJlZW4gc2F2ZWQnKTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBjcmVhdGluZyB3b3JrZmxvdyBydWxlJyk7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICAgY2hlY2tSdWxlRGVwZW5kZW5jaWVzMSgpe1xyXG4gICAgbGV0IHJlbW92ZWRSdWxlVGFyZ2V0VGFzayA9IHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YTtcclxuICAgICBpZih0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGUgPT09ICdUQVNLJyAmJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgIGxldCBzb3VyY2VUYXNrcyA9IG5ldyBTZXQoKTtcclxuICAgICAgbGV0IHNvdXJjZVRhc2tzQXJyYXkgPSBbXVxyXG4gICAgICAvL3RoaXMuYWxsUnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIHRoaXMucnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIGlmKCFydWxlLmlzSW5pdGlhbFJ1bGUpe1xyXG4gICAgICAgICAgcnVsZS5jdXJyZW50VGFzay5mb3JFYWNoKHRhc2sgPT4gc291cmNlVGFza3MuYWRkKHRhc2spKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICBzb3VyY2VUYXNrc0FycmF5ID0gQXJyYXkuZnJvbShzb3VyY2VUYXNrcyk7XHJcbiAgICBsZXQgbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXkgPSBbXTtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgIC8vIHRoaXMuYWxsUnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4gcnVsZS5oYXNQcmVkZWNlc3NvciA9IHRydWUpO1xyXG4gICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSB0cnVlKTtcclxuXHJcbiAgICBjb25zdCB0YXJnZXRJbmRleCA9IHNvdXJjZVRhc2tzQXJyYXkuaW5kZXhPZihyZW1vdmVkUnVsZVRhcmdldFRhc2spO1xyXG4gICAgaWYodGFyZ2V0SW5kZXggPj0gMCl7XHJcbiAgICAgLy8gdGhpcy5hbGxSdWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgIHRoaXMucnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gcnVsZS5jdXJyZW50VGFzay5pbmRleE9mKHJlbW92ZWRSdWxlVGFyZ2V0VGFzayk7XHJcbiAgICAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgICBydWxlLmhhc1ByZWRlY2Vzc29yID0gZmFsc2U7XHJcbiAgICAgICAgICBub1ByZWRlY2Vzc29yUnVsZUxpc3RBcnJheS5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgY291bnQgKz0gMTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgICB0aGlzLm5vUHJlZGVjZXNzb3JSdWxlTGlzdCA9IGNvdW50IDw9MCA/IFtdIDogbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXk7XHJcbiAgICBpZih0aGlzLnJ1bGVEYXRhLnRhcmdldFRhc2sgIT09IFwiXCIpe1xyXG4gICAgIC8vIHRoaXMucnVsZURlcGVuZGVuY3lFcnJvckhhbmRsZXIubmV4dCh0aGlzLmFsbFJ1bGVMaXN0R3JvdXApO1xyXG4gICAgIHRoaXMucnVsZURlcGVuZGVuY3lFcnJvckhhbmRsZXIubmV4dCh0aGlzLnJ1bGVMaXN0R3JvdXApO1xyXG5cclxuICAgICB9XHJcbiAgICB9XHJcbiAgIGVsc2UgaWYodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlID09PSAnVEFTSycgJiYgdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgIGxldCBzb3VyY2VUYXNrcyA9IG5ldyBTZXQoKTtcclxuICAgICAgbGV0IHNvdXJjZVRhc2tzQXJyYXkgPSBbXVxyXG4gICAgICB0aGlzLnRhc2tydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgaWYoIXJ1bGUuaXNJbml0aWFsUnVsZSl7XHJcbiAgICAgICAgICBydWxlLmN1cnJlbnRUYXNrLmZvckVhY2godGFzayA9PiBzb3VyY2VUYXNrcy5hZGQodGFzaykpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgIHNvdXJjZVRhc2tzQXJyYXkgPSBBcnJheS5mcm9tKHNvdXJjZVRhc2tzKTtcclxuICAgIGxldCBub1ByZWRlY2Vzc29yUnVsZUxpc3RBcnJheSA9IFtdO1xyXG4gICAgbGV0IGNvdW50ID0gMDtcclxuICAgIHRoaXMudGFza3J1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSB0cnVlKTtcclxuICAgIGNvbnN0IHRhcmdldEluZGV4ID0gc291cmNlVGFza3NBcnJheS5pbmRleE9mKHJlbW92ZWRSdWxlVGFyZ2V0VGFzayk7XHJcbiAgICBpZih0YXJnZXRJbmRleCA+PSAwKXtcclxuICAgICAgdGhpcy50YXNrcnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gcnVsZS5jdXJyZW50VGFzay5pbmRleE9mKHJlbW92ZWRSdWxlVGFyZ2V0VGFzayk7XHJcbiAgICAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgICBydWxlLmhhc1ByZWRlY2Vzc29yID0gZmFsc2U7XHJcbiAgICAgICAgICBub1ByZWRlY2Vzc29yUnVsZUxpc3RBcnJheS5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgY291bnQgKz0gMTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgICB0aGlzLm5vUHJlZGVjZXNzb3JSdWxlTGlzdCA9IGNvdW50IDw9MCA/IFtdIDogbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXk7XHJcbiAgICBpZih0aGlzLnJ1bGVEYXRhLnRhcmdldFRhc2sgIT09IFwiXCIpe1xyXG4gICAgLy8gIHRoaXMucnVsZURlcGVuZGVuY3lFcnJvckhhbmRsZXIubmV4dCh0aGlzLmFsbFJ1bGVMaXN0R3JvdXApO1xyXG4gICAgdGhpcy5ydWxlRGVwZW5kZW5jeUVycm9ySGFuZGxlci5uZXh0KHRoaXMucnVsZUxpc3RHcm91cCk7XHJcblxyXG4gICAgIH1cclxuICAgIH1cclxuICAgfVxyXG5cclxuICB2YWxpZGF0ZVJlbW92ZVdvcmtmbG93UnVsZSgpIHtcclxuICAgIC8vIGxldCBydWxlQ291bnRXaXRoU2FtZVRhcmdldFRhc2sgPSAwO1xyXG4gICAgLy8gdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAvLyAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpe1xyXG4gICAgLy8gICAgIHJ1bGVDb3VudFdpdGhTYW1lVGFyZ2V0VGFzayArKztcclxuICAgIC8vICAgfVxyXG4gICAgLy8gfSlcclxuICAgIHRoaXMuY2hlY2tSdWxlRGVwZW5kZW5jaWVzMSgpO1xyXG4gICAgaWYoKHRoaXMubm9QcmVkZWNlc3NvclJ1bGVMaXN0Lmxlbmd0aCA8PSAwKSl7XHJcbiAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSB0cnVlKTtcclxuICAgICAgICB0aGlzLnJlbW92ZVdvcmtmbG93UnVsZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgbWVzc2FnZTogJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byByZW1vdmUgdGhlIFJ1bGU/JyxcclxuICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVXb3JrZmxvd1J1bGUoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfWVsc2V7XHJcbiAgICAgIGlmKCF0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgICAgIC8vIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihcIlBsZWFzZSBEZWxldGUgSGlnaGxpZ2h0ZWQgUnVsZSBGaXJzdCEhIElmIFJ1bGUgbm90IHZpc2libGUgY2hhbmdlIHRoZSBwYWdlIHNpemUgYW5kIHRyeSBhZ2FpbiEhXCIpO1xyXG4gICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKFwiUGxlYXNlIERlbGV0ZSBIaWdobGlnaHRlZCBDaGlsZCBSdWxlcyBGaXJzdFwiKTtcclxuICAgICAgfVxyXG4gICAgICBlbHNle1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihcIlBsZWFzZSBvcGVuIGdsb2JhbCBjb25maWd1cmUgcnVsZSBhbmQgdGhlbiBlZGl0IGFuZCByZW1vdmUgdGhpcyB0YXNrXCIpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW1vdmVXb3JrZmxvd1J1bGUoKSB7XHJcbiAgICBjb25zdCBpc0luaXRpYWxSdWxlMSA9IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSA/ICd0cnVlJyA6ICdmYWxzZSc7XHJcbiAgICBpZiAodGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZCkge1xyXG4gICAgICB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkID0gQXJyYXkuaXNBcnJheSh0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkKSA/IHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWQgOiBbdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZF07XHJcbiAgICAgIC8vbGV0IGh0dHBSZXF1ZXN0QXJyYXkgPSBbXVxyXG4gICAgICBsZXQgcnVsZUlkcyA9IFtdXHJcbiAgICAgIHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWQuZm9yRWFjaCh3b3JrZmxvd0lkID0+IHtcclxuICAgICAgICBjb25zdCBSdWxlID0ge1xyXG4gICAgICAgICAgSWQ6IHdvcmtmbG93SWRcclxuICAgICAgICB9XHJcbiAgICAgICAgcnVsZUlkcy5wdXNoKFJ1bGUpO1xyXG4gICAgICAgIC8vaHR0cFJlcXVlc3RBcnJheS5wdXNoKHRoaXMudGFza1NlcnZpY2UucmVtb3ZlV29ya2Zsb3dSdWxlKHdvcmtmbG93SWQpKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGxldCBjdXJyZW50VGFzayA9IFtdO1xyXG4gICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgIGNvbnN0IHNvdXJjZVRhc2sgPSB7XHJcbiAgICAgICAgICBJZDogZWFjaFRhc2tcclxuICAgICAgICB9XHJcbiAgICAgICAgY3VycmVudFRhc2sucHVzaChzb3VyY2VUYXNrKTtcclxuICAgICAgfSlcclxuICAgICAgbGV0IHJEYXRhID0ge1xyXG4gICAgICAgIGN1cnJlbnRUYXNrLFxyXG4gICAgICAgIGlzSW5pdGlhbFJ1bGU6IGlzSW5pdGlhbFJ1bGUxXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy50YXNrU2VydmljZS5yZW1vdmVXb3JrZmxvd1J1bGUocnVsZUlkcyxyRGF0YSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHsgXHJcbiAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICB0aGlzLnJlbW92ZVdvcmtmbG93UnVsZUhhbmRsZXIubmV4dCh0aGlzLnJ1bGVEYXRhKTtcclxuICAgICAgICAgIHRoaXMuZW5hYmxlSW5pdGlhdGVXb3JrZmxvd0hhbmRsZXIubmV4dCgpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1dvcmtmbG93IFJ1bGUgaGFzIGJlZW4gcmVtb3ZlZCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHJlbW92aW5nIHdvcmtmbG93IHJ1bGUnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnJlbW92ZVdvcmtmbG93UnVsZUhhbmRsZXIubmV4dChudWxsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNsb3NlRGlhbG9nKCkge1xyXG4gICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0gJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5wcmlzdGluZSkge1xyXG4gICAgICB0aGlzLmNsb3NlQ2FsbGJhY2tIYW5kbGVyLm5leHQoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gY2xvc2U/JyxcclxuICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpbml0aWFsaXplRm9ybSgpIHtcclxuXHJcbiAgICAvLyB0aGlzLmFsbFJ1bGVMaXN0ID0gWy4uLm5ldyBTZXQoWy4uLnRoaXMucnVsZUxpc3QsIC4uLnRoaXMucHJvamVjdFJ1bGVMaXN0XSldOyAtLS0gVXBkYXRlIFN0YW5kYXJkIGNvZGUgZm9yIGFsbCBydWxlIGxpc3QgLSBNZW5ha2FcclxuICAgIHRoaXMucnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgY29uc3QgZXhpc3RpbmdSdWxlID0gdGhpcy5hbGxSdWxlTGlzdC5maW5kKHNlbGVjdGVkUnVsZSA9PiBKU09OLnN0cmluZ2lmeShzZWxlY3RlZFJ1bGUpID09PSBKU09OLnN0cmluZ2lmeShydWxlKSk7XHJcbiAgICAgIGlmICghZXhpc3RpbmdSdWxlKSB7XHJcbiAgICAgICAgdGhpcy5hbGxSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnByb2plY3RSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICBjb25zdCBleGlzdGluZ1J1bGUgPSB0aGlzLmFsbFJ1bGVMaXN0LmZpbmQoc2VsZWN0ZWRSdWxlID0+IEpTT04uc3RyaW5naWZ5KHNlbGVjdGVkUnVsZSkgPT09IEpTT04uc3RyaW5naWZ5KHJ1bGUpKTtcclxuICAgICAgaWYgKCFleGlzdGluZ1J1bGUpIHtcclxuICAgICAgICB0aGlzLmFsbFJ1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdCA9IFtdO1xyXG4gICAgdGhpcy5hbGxSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgaWYgKEpTT04uc3RyaW5naWZ5KHRoaXMucnVsZURhdGEpICE9PSBKU09OLnN0cmluZ2lmeShydWxlKSkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFJ1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgIH1cclxuICAgICAgaWYodGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QucHVzaChydWxlKVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgaWYgKHRoaXMudGFza0RhdGFbMF0uUFJPSkVDVF9JVEVNX0lEKSB7XHJcbiAgICAgIGNvbnN0IHByb2plY3RJdGVtSWQgPSB0aGlzLnRhc2tEYXRhWzBdLlBST0pFQ1RfSVRFTV9JRDtcclxuICAgICAgdGhpcy5wcm9qZWN0SWQgPSBwcm9qZWN0SXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHRoaXMudGFza0RhdGEuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgaWYgKCh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwgfHwgdGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTlRFUk1FRElBVEUpIHx8ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgJiYgdGhpcy5pc1J1bGVzSW5pdGlhdGVkKSkge1xyXG4gICAgICAgIGlmICh0YXNrLklTX0FQUFJPVkFMX1RBU0sgPT09ICd0cnVlJykge1xyXG4gICAgICAgICAgLyppZiAodGhpcy5jdXJyZW50UnVsZUxpc3QpIHtcclxuICAgICAgICAgICAgY29uc3QgaGFzUGFyZW50VGFza1J1bGUgPSB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maW5kKHJ1bGUgPT4gcnVsZS5jdXJyZW50VGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpIHx8IHRoaXMuY3VycmVudFJ1bGVMaXN0LmZpbmQocnVsZSA9PiBydWxlLnRhcmdldERhdGEgPT09IHRhc2suSUQpKTtcclxuICAgICAgICAgICAgaWYgKGhhc1BhcmVudFRhc2tSdWxlKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy50YXNrTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHRhc2suVEFTS19OQU1FLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suSURcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBcclxuICAgICAgICAgICovXHJcbiAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UnVsZUxpc3QgJiYgdGhpcy5jdXJyZW50UnVsZUxpc3QubGVuZ3RoID4gMCAmJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCkge1xyXG4gICAgICAgICAgICBjb25zdCBoYXNSdWxlID0gdGhpcy5jdXJyZW50UnVsZUxpc3QuZmluZChydWxlID0+IHJ1bGUudGFyZ2V0RGF0YSA9PT0gdGFzay5JRCk7XHJcbiAgICAgICAgICAgICBpZiAoaGFzUnVsZSkge1xyXG4gICAgICAgICAgICAgIHRoaXMudGFza0xpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRClcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICB9XHJcbiAgICAgICAgIGVsc2UgaWYodGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgICAgICB0aGlzLnRhc2tMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnRhc2tMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBjb25zdCB0YXNrU3RhdHVzID0gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0J5Y2F0ZWdvcnlOYW1lKE1QTV9MRVZFTFMuVEFTSyk7XHJcbiAgICAvLyB0aGlzLnRhc2tTdGF0dXNlcyA9IHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxTdGF0dXNCeWNhdGVnb3J5TmFtZShNUE1fTEVWRUxTLlRBU0spO1xyXG4gICAgdGhpcy50YXNrU3RhdHVzZXMgPSB0YXNrU3RhdHVzICYmIHRhc2tTdGF0dXMubGVuZ3RoID4gMCAmJiBBcnJheS5pc0FycmF5KHRhc2tTdGF0dXMpID8gdGFza1N0YXR1c1swXSA6IHRhc2tTdGF0dXM7XHJcbiAgICBsZXQgY3VycmVudFJ1bGVUcmlnZ2VyRGF0YSA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQoc3RhdHVzID0+IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkID09IHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcblxyXG5cclxuICAgIHRoaXMudGFyZ2V0VGFzayA9IHRoaXMudGFza0RhdGEuZmluZCh0YXNrID0+IHRhc2suSUQgPT09IHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgIGN1cnJlbnRUYXNrOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgIHZhbHVlOiB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLCBcclxuICAgICAgICBkaXNhYmxlZDogKHRoaXMuaXNDdXJyZW50VGFza1J1bGUgPyB0cnVlIDogKHRoaXMucnVsZURhdGEuaXNFeGlzdCA/ICh0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGU9PT0nVEFTSycgPyAodGhpcy50YXJnZXRUYXNrPy5JU19BQ1RJVkVfVEFTSyA9PT0ndHJ1ZScgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQUNDRVBURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUNBTkNFTExFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVKRUNURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFVklFVy1SRUpFQ1RFRCcgPyB0cnVlIDogZmFsc2UpOiBmYWxzZSkgOiBmYWxzZSkpXHJcbiAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgIHRyaWdnZXJUeXBlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZSwgZGlzYWJsZWQ6ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyh0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGU9PT0nVEFTSycgPyAodGhpcy50YXJnZXRUYXNrPy5JU19BQ1RJVkVfVEFTSyA9PT0ndHJ1ZScgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQUNDRVBURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUNBTkNFTExFRCd8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRUpFQ1RFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVWSUVXLVJFSkVDVEVEJyA/IHRydWUgOiBmYWxzZSk6ZmFsc2UpIDogZmFsc2UpIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgIHRyaWdnZXJEYXRhOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJUeXBlID8gKHRoaXMucnVsZURhdGEuaXNFeGlzdCA/KHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZT09PSdUQVNLJyA/ICh0aGlzLnRhcmdldFRhc2s/LklTX0FDVElWRV9UQVNLID09PSd0cnVlJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1BQ0NFUFRFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQ0FOQ0VMTEVEJ3x8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFSkVDVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRVZJRVctUkVKRUNURUQnID8gdHJ1ZSA6IGZhbHNlKSA6IGZhbHNlKSA6IGZhbHNlKSA6IHRydWUgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgdGFyZ2V0VHlwZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSwgZGlzYWJsZWQ6ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyAodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlPT09J1RBU0snID8gKHRoaXMudGFyZ2V0VGFzaz8uSVNfQUNUSVZFX1RBU0sgPT09J3RydWUnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUFDQ0VQVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1DQU5DRUxMRUQnfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVKRUNURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFVklFVy1SRUpFQ1RFRCcgPyB0cnVlIDogZmFsc2UpOiBmYWxzZSkgOiBmYWxzZSkgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgdGFyZ2V0RGF0YTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogdGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlID8gKHRoaXMucnVsZURhdGEuaXNFeGlzdCA/ICh0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGU9PT0nVEFTSycgPyAodGhpcy50YXJnZXRUYXNrPy5JU19BQ1RJVkVfVEFTSyA9PT0ndHJ1ZScgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQUNDRVBURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUNBTkNFTExFRCd8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRUpFQ1RFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVWSUVXLVJFSkVDVEVEJyA/IHRydWUgOiBmYWxzZSk6ZmFsc2UpIDogZmFsc2UpIDogdHJ1ZSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICBpc0luaXRpYWxSdWxlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5ydWxlRGF0YS5pc0luaXRpYWxSdWxlLCBkaXNhYmxlZDogKHRoaXMucnVsZURhdGEuaXNFeGlzdCA/ICh0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGU9PT0nVEFTSycgPyAodGhpcy50YXJnZXRUYXNrPy5JU19BQ1RJVkVfVEFTSyA9PT0ndHJ1ZScgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQUNDRVBURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUNBTkNFTExFRCd8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRUpFQ1RFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVWSUVXLVJFSkVDVEVEJyA/IHRydWUgOiBmYWxzZSk6ZmFsc2UpIDogZmFsc2UpIH0pLFxyXG4gICAgICB9KTtcclxuXHJcblxyXG4gICBcclxuICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IGN1cnJlbnRUYXNrTGlzdCA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWU7XHJcbiAgICAgIGZvcihsZXQgaT0wOyBpIDwgY3VycmVudFRhc2tMaXN0Lmxlbmd0aDsgaSsrKXtcclxuICAgICAgICBsZXQgY3VycmVudFRhc2sgPSB0aGlzLnRhc2tEYXRhLmZpbmQodGFzayA9PiB0YXNrLklEID09PSBjdXJyZW50VGFza0xpc3RbaV0pO1xyXG4gICAgICAgIGlmKGN1cnJlbnRUYXNrICYmIGN1cnJlbnRUYXNrLklTX0FQUFJPVkFMX1RBU0sgPT09ICd0cnVlJyl7XHJcbiAgICAgICAgICB0aGlzLmFwcHJvdmFsVGFzayA9IHRydWU7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIHRoaXMuYXBwcm92YWxUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGxldCBpc1RhcmdldFRhc2sgPSBmYWxzZTtcclxuICAgICAgdGhpcy5hbGxSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIC8vIGlmIChydWxlLnRhcmdldERhdGEgPT09IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzaykge1xyXG4gICAgICAgIC8vICAgY29uc3Qgc3RhdHVzID0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKTtcclxuICAgICAgICAvLyAgIGlmICghKHJ1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkID09PSBydWxlLnRyaWdnZXJEYXRhKSkge1xyXG4gICAgICAgIC8vICAgICBpc1RhcmdldFRhc2sgPSB0cnVlO1xyXG4gICAgICAgIC8vICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2suZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICBpZihydWxlLnRhcmdldERhdGEgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgY29uc3Qgc3RhdHVzID0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKTtcclxuICAgICAgICAgICAgaWYgKCEocnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQgPT09IHJ1bGUudHJpZ2dlckRhdGEpKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzVGFyZ2V0VGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgfSk7XHJcbiAgICAgIGlmIChpc1RhcmdldFRhc2sgfHwgdGhpcy5hcHByb3ZhbFRhc2spIHtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmlzSW5pdGlhbFJ1bGUuZGlzYWJsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudHJpZ2dlclR5cGUpIHtcclxuICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycpIHtcclxuICAgICAgICB0aGlzLnRhc2tTdGF0dXNlcy5mb3JFYWNoKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICBpZiAoc3RhdHVzLlNUQVRVU19UWVBFICE9PSBTdGF0dXNUeXBlcy5JTklUSUFMICYmIHN0YXR1cy5TVEFUVVNfVFlQRSAhPT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEICYmXHJcbiAgICAgICAgICAgICEoc3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCAmJiAhdGhpcy5hcHByb3ZhbFRhc2sgKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRyaWdnZXJEYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgIG5hbWU6IHN0YXR1cy5OQU1FLFxyXG4gICAgICAgICAgICAgIHZhbHVlOiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgdGFza1dvcmtmbG93QWN0aW9ucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0VGFza1dvcmtmbG93QWN0aW9ucygpO1xyXG4gICAgICAgIHRhc2tXb3JrZmxvd0FjdGlvbnMuZm9yRWFjaChhY3Rpb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy50cmlnZ2VyRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgbmFtZTogYWN0aW9uLk5BTUUsXHJcbiAgICAgICAgICAgIHZhbHVlOiBhY3Rpb25bJ01QTV9Xb3JrZmxvd19BY3Rpb25zLWlkJ10uSWRcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgbGV0IGlzU291cmNlVGFzayA9IGZhbHNlO1xyXG4gICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXRUeXBlKSB7XHJcbiAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudGFyZ2V0VHlwZSA9PT0gJ1RBU0snKSB7XHJcbiAgICAgICAgdGhpcy50YXNrRGF0YS5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgbGV0IGlzU2VsZWN0ZWRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICBsZXQgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gdHJ1ZTtcclxuICAgICAgICAgIGxldCBpc0N5Y2xlVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgY29uc3QgY3VycmVudFRhc2tSdWxlTGlzdCA9IFtdO1xyXG4gICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goKGVhY2hUYXNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gcnVsZS5jdXJyZW50VGFza1swXSl7XHJcbiAgICAgICAgICAgICAgICAgIGN1cnJlbnRUYXNrUnVsZUxpc3QucHVzaChydWxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAvLyBjdXJyZW50VGFza1J1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgIC8vICAgaWYgKHJ1bGUudGFyZ2V0RGF0YSA9PT0gdGFzay5JRCAmJiBydWxlLnRyaWdnZXJEYXRhID09PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIC8vICAgICBpc1NlbGVjdGVkVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgIC8vICAgfVxyXG4gICAgICAgICAgICAvLyB9KTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmKHRhc2suSUQgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0VGFza1J1bGVMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZS5jdXJyZW50VGFza1swXSA9PT0gdGFzay5JRCAmJiBydWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiBydWxlLnRyaWdnZXJEYXRhID09PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzQ3ljbGVUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgY29uc3Qgc3RhdHVzID0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKTtcclxuICAgICAgICAgIGNvbnN0IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlcz0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKTtcclxuICAgICAgICAgIGlmKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgbGV0IGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKXtcclxuICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIGlmICghKGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgJiYgKCh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgfHxcclxuICAgICAgICAgICAgICAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ0FDVElPTicpKSkpIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyZW50VGFza1J1bGVMaXN0ID0gdGhpcy5hbGxSdWxlTGlzdC5maWx0ZXIocnVsZSA9PiBydWxlLmN1cnJlbnRUYXNrLmluY2x1ZGVzKFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSkgfHwgcnVsZS50YXJnZXREYXRhID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG4gICAgICAgICAgICBpZiAocGFyZW50VGFza1J1bGVMaXN0ICYmIHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgcGFyZW50VGFza1J1bGVMaXN0LmZvckVhY2gocGFyZW50VGFza1J1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEocGFyZW50VGFza1J1bGUgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGEgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkIHx8IHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhPT09IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlc1snTVBNX1N0YXR1cy1pZCddLklkKSkgfHwgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykpKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICBpZih0YXNrLklEID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgICAgaXNTb3VyY2VUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIGlmICgoIWlzU2VsZWN0ZWRUYXNrICYmICFpc0N5Y2xlVGFzayAmJiBpc1BhcmVudFRhc2tDb21wbGV0ZWQgLyomJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCovKSAvKnx8ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgJiYgdGhpcy5pc1J1bGVzSW5pdGlhdGVkKSovKSB7XHJcbiAgICAgICAgICAgIGlmICgodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMKSB8fCAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ICYmIHRoaXMuaXNSdWxlc0luaXRpYXRlZCkpIHtcclxuICAgICAgICAgICAgICB0aGlzLnRhcmdldERhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFVklFV19SRUpFQ1RFRCksXHJcbiAgICAgICAgICAgICAgICBpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLPT09J3RydWUnID90cnVlOmZhbHNlXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC8vIGVsc2UgaWYodGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgICAgICAvLyAgIGlmICgodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMKSB8fCAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ICYmIHRoaXMuaXNSdWxlc0luaXRpYXRlZCkpIHtcclxuICAgICAgICAgIC8vICAgICB0aGlzLnRhcmdldERhdGEucHVzaCh7XHJcbiAgICAgICAgICAvLyAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgIC8vICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgLy8gICAgICAgaXNDb21wbGV0ZWQ6ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFVklFV19SRUpFQ1RFRCksXHJcbiAgICAgICAgICAvLyAgICAgICBpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLPT09J3RydWUnID90cnVlOmZhbHNlXHJcbiAgICAgICAgICAvLyAgICAgfSk7XHJcbiAgICAgICAgICAvLyAgIH1cclxuICAgICAgICAgIC8vIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmdyb3VwVGFyZ2V0VGFzaygpXHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc3QgdGFza0V2ZW50cyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0VGFza0V2ZW50cygpO1xyXG4gICAgICAgIHRhc2tFdmVudHMuZm9yRWFjaChldmVudCA9PiB7XHJcbiAgICAgICAgICB0aGlzLnRhcmdldERhdGEucHVzaCh7XHJcbiAgICAgICAgICAgIG5hbWU6IGV2ZW50LkRJU1BMQVlfTkFNRSxcclxuICAgICAgICAgICAgdmFsdWU6IGV2ZW50WydNUE1fRXZlbnRzLWlkJ10uSWRcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgICB9IFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoY3VycmVudFRhc2sgPT4ge1xyXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKipDaGVja2luZyBGb3IgQXBwcm92YWwgVGFzayBpbiBDdXJyZW50IFNvdXJjZSB0YXNrIExpc3QqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqV2Ugc2hvdWxkbid0IGFsbG93IEFwcHJvdmFsIHRhc2sgdG8gYmUgZ3JvdXBlZCB3aXRoIG90aGVyIHJ1bGVzKioqKioqKioqKioqKi9cclxuICAgICAgbGV0IHNvdXJjZVRhc2tzID0gW107XHJcbiAgICAgLy8gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5pc0luaXRpYWxSdWxlLnNldFZhbHVlKGZhbHNlKTtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgIGxldCBjdXJyZW50VGFzayA9IHRoaXMudGFza0RhdGEuZmluZCh0YXNrID0+IHRhc2suSUQgPT09IGVhY2hUYXNrKTtcclxuICAgICAgICBzb3VyY2VUYXNrcy5wdXNoKGN1cnJlbnRUYXNrKTtcclxuICAgICAgfSk7XHJcbiAgICBzb3VyY2VUYXNrcy5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgaWYoZWFjaFRhc2s/LklTX0FQUFJPVkFMX1RBU0sgPT09ICd0cnVlJyAmJiBzb3VyY2VUYXNrcy5sZW5ndGggPiAxKXtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnNldFZhbHVlKFtdKTtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0FwcHJvdmFsIFRhc2sgY2FudCBiZSBHcm91cGVkIHdpdGggT3RoZXIgdGFza3MnKTtcclxuICAgICAgfVxyXG4gICAgfSlcclxuICAgIC8vQ2hlY2tpbmcgaWYgdGhlcmUgb25seSBjb21wbGV0ZWQgdGFza3MgaW4gc291cmNlIHRhc2tzIG9mIGEgUnVsZS5cclxuICAgIC8vVGhpcyBjb21lcyBoYW5keSB3aGlsZSB1cGRhdGluZyB0YXNrc1xyXG4gICAgbGV0IG9ubHlDb21wbGV0ZWRUYXNrc0luU291cmNlVGFza0xpc3QgPSB0cnVlO1xyXG4gICAgc291cmNlVGFza3MuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgIGlmKCEoZWFjaFRhc2s/LlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEIHx8IGVhY2hUYXNrPy5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQgfHwgZWFjaFRhc2s/LlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkpe1xyXG4gICAgICAgIG9ubHlDb21wbGV0ZWRUYXNrc0luU291cmNlVGFza0xpc3QgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSlcclxuICAgIGlmKG9ubHlDb21wbGV0ZWRUYXNrc0luU291cmNlVGFza0xpc3QgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5sZW5ndGggPiAwKXtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrKTtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdUaGVyZSBzaG91bGQgYmUgYXRsZWFzdCBvbmUgVGFzayB0aGF0IGlzIG5vdCBDb21wbGV0ZWQnKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgICAvL2NoZWNraW5nIGlmIHRhcmdldCB0eXBlIGlzIFwiVEFTSy9FVkVOVFwiXHJcbiAgICAgIGlmIChjdXJyZW50VGFzayAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldFR5cGUgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXRUeXBlLnZhbHVlID09PSAnVEFTSycpIHtcclxuICAgICAgICB0aGlzLnRhcmdldERhdGEgPSBbXTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKTtcclxuICAgICAgICB0aGlzLnRhc2tEYXRhLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgICBsZXQgaXNTZWxlY3RlZFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgbGV0IGlzQ3ljbGVUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UnVsZUxpc3QgJiYgdGhpcy5jdXJyZW50UnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgIGNvbnN0IGN1cnJlbnRUYXNrUnVsZUxpc3QgPSBbXTsgXHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFJ1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgY3VycmVudFRhc2suZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihydWxlLmN1cnJlbnRUYXNrWzBdID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgICAgICAgIGN1cnJlbnRUYXNrUnVsZUxpc3QucHVzaChydWxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goKGVhY2hUYXNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYodGFzay5JRCA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgaXNTZWxlY3RlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdTVEFUVVMnICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUpIHtcclxuICAgICAgICAgICAgICBjb25zdCB0YXJnZXRUYXNrUnVsZUxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFJ1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocnVsZS50YXJnZXREYXRhID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0YXJnZXRUYXNrUnVsZUxpc3QucHVzaChydWxlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB0YXJnZXRUYXNrUnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChydWxlLmN1cnJlbnRUYXNrWzBdID09PSB0YXNrLklEICYmIHJ1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHJ1bGUudHJpZ2dlckRhdGEgPT09IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgaXNDeWNsZVRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgY29uc3Qgc3RhdHVzID0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKTtcclxuICAgICAgICAgIGNvbnN0IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlcz0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRT09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRClcclxuICAgICAgICAgIGlmKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgbGV0IGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKXtcclxuICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIGlmICghKGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgJiYgKCh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgfHxcclxuICAgICAgICAgICAgICAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ0FDVElPTicpKSkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICBjb25zdCBwYXJlbnRUYXNrUnVsZUxpc3QgPSB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maWx0ZXIocnVsZSA9PiBydWxlLmN1cnJlbnRUYXNrWzBdID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkgfHwgcnVsZS50YXJnZXREYXRhID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG4gICAgICAgICAgICAgIGlmIChwYXJlbnRUYXNrUnVsZUxpc3QgJiYgcGFyZW50VGFza1J1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHBhcmVudFRhc2tSdWxlTGlzdC5mb3JFYWNoKHBhcmVudFRhc2tSdWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgaWYgKCEocGFyZW50VGFza1J1bGUgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGEgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkIHx8IHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhPT09IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlc1snTVBNX1N0YXR1cy1pZCddLklkKSkgfHwgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFpc1NlbGVjdGVkVGFzayAmJiAhaXNDeWNsZVRhc2sgJiYgaXNQYXJlbnRUYXNrQ29tcGxldGVkICYmICF0aGlzLmlzVGFza0xldmVsY2FsbGVkKSB7XHJcbiAgICAgICAgICAgIGlmICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwpIHsgLy8gICYmIHRhc2suVEFTS19TVEFSVF9EQVRFID4gY3VycmVudFRhc2suVEFTS19EVUVfREFURVxyXG4gICAgICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHRhc2suVEFTS19OQU1FLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVWSUVXX1JFSkVDVEVEKSxcclxuICAgICAgICAgICAgICAgIGlzQXBwcm92YWxUYXNrOiB0YXNrLklTX0FQUFJPVkFMX1RBU0s9PT0ndHJ1ZScgP3RydWU6ZmFsc2VcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICB0aGlzLmdyb3VwVGFyZ2V0VGFzaygpO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG5cclxuICAgICAgLy9jaGVja2luZyB3aXRoIFRyaWdnZXIgdHlwZSBcclxuICAgICAgaWYgKGN1cnJlbnRUYXNrICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUpIHtcclxuICAgICAgICBsZXQgYXBwcm92YWxUYXNrSW5Tb3VyY2UgPSBmYWxzZTtcclxuICAgICAgICBmb3IobGV0IGk9MDsgaSA8IHNvdXJjZVRhc2tzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgIGlmKHNvdXJjZVRhc2tzW2ldPy5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICBhcHByb3ZhbFRhc2tJblNvdXJjZSA9IHRydWU7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJykge1xyXG4gICAgICAgICAgdGhpcy50cmlnZ2VyRGF0YSA9IFtdO1xyXG4gICAgICAgICBcclxuICAgICAgICAgIHRoaXMudGFza1N0YXR1c2VzLmZvckVhY2goc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgaWYgKHN0YXR1cy5TVEFUVVNfVFlQRSAhPT0gU3RhdHVzVHlwZXMuSU5JVElBTCAmJiBzdGF0dXMuU1RBVFVTX1RZUEUgIT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCAmJlxyXG4gICAgICAgICAgICAgICEoc3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCAmJiAhYXBwcm92YWxUYXNrSW5Tb3VyY2UpKSB7XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgdGhpcy50cmlnZ2VyRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHN0YXR1cy5OQU1FLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZFRyaWdnZXJEYXRhPSB0aGlzLnRyaWdnZXJEYXRhWzBdO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgXHJcbiAgICAgIGxldCBpc1RhcmdldFRhc2sgPSBmYWxzZTtcclxuICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgLy8gaWYgKHJ1bGUudGFyZ2V0RGF0YSA9PT0gY3VycmVudFRhc2spIHtcclxuICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCk7XHJcbiAgICAgICAgICAgICAgaWYgKCEocnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQgPT09IHJ1bGUudHJpZ2dlckRhdGEpKSB7XHJcbiAgICAgICAgICAgICAgICBpc1RhcmdldFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSkgIFxyXG4gICAgICAgIH0pXHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50VGFza0xpc3QgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlO1xyXG4gICAgICAgIGZvcihsZXQgaT0wOyBpIDwgY3VycmVudFRhc2tMaXN0Lmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgIGxldCBjdXJyZW50VGFzayA9IHRoaXMudGFza0RhdGEuZmluZCh0YXNrID0+IHRhc2suSUQgPT09IGN1cnJlbnRUYXNrTGlzdFtpXSk7XHJcbiAgICAgICAgICBpZihjdXJyZW50VGFzayAmJiBjdXJyZW50VGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICB0aGlzLmFwcHJvdmFsVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwcm92YWxUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmIChpc1RhcmdldFRhc2sgfHwgdGhpcy5hcHByb3ZhbFRhc2spIHtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmlzSW5pdGlhbFJ1bGUuZGlzYWJsZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuaXNJbml0aWFsUnVsZS5lbmFibGUoKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuc2V0VmFsdWUoJycpO1xyXG4gICAgICB0aGlzLmdyb3VwVGFyZ2V0VGFzaygpO1xyXG4gICAgfSk7XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh0cmlnZ2VyVHlwZSA9PiB7XHJcbiAgICAgIGlmICh0cmlnZ2VyVHlwZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlKSB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyZW50VGFza0xpc3QgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlO1xyXG4gICAgICAgICAgZm9yKGxldCBpPTA7IGkgPCBjdXJyZW50VGFza0xpc3QubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICBsZXQgY3VycmVudFRhc2sgPSB0aGlzLnRhc2tEYXRhLmZpbmQodGFzayA9PiB0YXNrLklEID09PSBjdXJyZW50VGFza0xpc3RbaV0pO1xyXG4gICAgICAgICAgICBpZihjdXJyZW50VGFzayAmJiBjdXJyZW50VGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICAgIHRoaXMuYXBwcm92YWxUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgdGhpcy5hcHByb3ZhbFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRyaWdnZXJEYXRhID0gW107XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgaWYgKHRyaWdnZXJUeXBlID09PSAnU1RBVFVTJykge1xyXG4gICAgICAgICAgdGhpcy50YXNrU3RhdHVzZXMuZm9yRWFjaChzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc3RhdHVzLlNUQVRVU19UWVBFICE9PSBTdGF0dXNUeXBlcy5JTklUSUFMICYmIHN0YXR1cy5TVEFUVVNfVFlQRSAhPT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEICYmXHJcbiAgICAgICAgICAgICAgIShzdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEICYmICF0aGlzLmFwcHJvdmFsVGFzaykpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudHJpZ2dlckRhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZFxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykge1xyXG4gICAgICAgICAgY29uc3QgdGFza1dvcmtmbG93QWN0aW9ucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0VGFza1dvcmtmbG93QWN0aW9ucygpO1xyXG4gICAgICAgICAgdGFza1dvcmtmbG93QWN0aW9ucy5mb3JFYWNoKGFjdGlvbiA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudHJpZ2dlckRhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgbmFtZTogYWN0aW9uLk5BTUUsXHJcbiAgICAgICAgICAgICAgdmFsdWU6IGFjdGlvblsnTVBNX1dvcmtmbG93X0FjdGlvbnMtaWQnXS5JZFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXRUeXBlLnZhbHVlID09PSAnVEFTSycpIHtcclxuICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgIHRoaXMudGFza0RhdGEuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgbGV0IGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlcz0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRT09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRClcclxuICAgICAgICAgICAgaWYodGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgIGlmKGVhY2hUYXNrID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpe1xyXG4gICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBpZiAoIShpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrICYmICgodHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKSB8fCAodHJpZ2dlclR5cGUgPT09ICdBQ1RJT04nKSkpKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmVudFRhc2tSdWxlTGlzdCA9IHRoaXMuY3VycmVudFJ1bGVMaXN0LmZpbHRlcihydWxlID0+IHJ1bGUuY3VycmVudFRhc2tbMF0gPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSB8fCBydWxlLnRhcmdldERhdGEgPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSk7XHJcbiAgICAgICAgICAgICAgICBpZiAocGFyZW50VGFza1J1bGVMaXN0ICYmIHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgIHBhcmVudFRhc2tSdWxlTGlzdC5mb3JFYWNoKHBhcmVudFRhc2tSdWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIShwYXJlbnRUYXNrUnVsZSAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQgfHwgcGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGE9PT0gc3RhdHVzQXNSZXF1ZXN0ZWRDaGFuZ2VzWydNUE1fU3RhdHVzLWlkJ10uSWQpKSkgfHwgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChpc1BhcmVudFRhc2tDb21wbGV0ZWQpIHtcclxuICAgICAgICAgICAgICBpZiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMICYmIHRhc2suSUQgIT09IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUpIHsgLy8gICYmIHRhc2suVEFTS19TVEFSVF9EQVRFID4gY3VycmVudFRhc2suVEFTS19EVUVfREFURVxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRVZJRVdfUkVKRUNURUQpXHJcbiAgICAgICAgICAgICAgICAgICxpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLPT09J3RydWUnP3RydWU6ZmFsc2VcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ3JvdXBUYXJnZXRUYXNrKCk7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS5lbmFibGUoKTtcclxuICAgICAgICBcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh0cmlnZ2VyRGF0YSA9PiB7XHJcbiAgICAgIGlmICh0cmlnZ2VyRGF0YSAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldFR5cGUudmFsdWUgPT09ICdUQVNLJykgeyBcclxuICAgICAgICBsZXQgcmVtb3ZlZFRhc2tzID0gW11cclxuICAgICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmluZGV4T2YoZWFjaFRhc2spO1xyXG4gICAgICAgICAgaWYoaW5kZXggPCAwKXtcclxuICAgICAgICAgICAgcmVtb3ZlZFRhc2tzLnB1c2goZWFjaFRhc2spO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgdGhpcy50YXJnZXREYXRhID0gW107XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKCcnKTtcclxuICAgICAgICB0aGlzLnRhc2tEYXRhLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgICBsZXQgaXNTZWxlY3RlZFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgIGxldCBpc0N5Y2xlVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgbGV0IGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IHRydWU7XHJcbiAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UnVsZUxpc3QgJiYgdGhpcy5jdXJyZW50UnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBjdXJyZW50VGFza1J1bGVMaXN0ID0gW107IFxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihydWxlLmN1cnJlbnRUYXNrWzBdID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgICAgICAgIGN1cnJlbnRUYXNrUnVsZUxpc3QucHVzaChydWxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goKGVhY2hUYXNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYodGFzay5JRCA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgaXNTZWxlY3RlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdTVEFUVVMnKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0VGFza1J1bGVMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZS5jdXJyZW50VGFza1swXSA9PT0gdGFzay5JRCAmJiBydWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiBydWxlLnRyaWdnZXJEYXRhID09PSB0cmlnZ2VyRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICBpc0N5Y2xlVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCk7XHJcbiAgICAgICAgICBjb25zdCBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXM9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cz0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEU9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpXHJcbiAgICAgICAgICBsZXQgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgaWYodGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgIGlmKGVhY2hUYXNrID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpe1xyXG4gICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgaWYgKCEoaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFza1xyXG4gICAgICAgICAgICAmJiAoKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdTVEFUVVMnICYmIHRyaWdnZXJEYXRhID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgfHxcclxuICAgICAgICAgICAgICAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ0FDVElPTicpKSkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICBjb25zdCBwYXJlbnRUYXNrUnVsZUxpc3QgPSB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maWx0ZXIocnVsZSA9PiBydWxlLmN1cnJlbnRUYXNrWzBdID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkgfHwgcnVsZS50YXJnZXREYXRhID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG5cclxuICAgICAgICAgICAgICAvL3doaWxlIGVkaXRpbmcgaWYgYSByZW1vdmUgYSBwYXJlbnQgdGFzayBmcm9tIHNvdXJjZSB0YXNrIGxpc3QgdGhlbiB3ZSBhcmUgcmVtb3ZpbmcgdGhhdCBydWxlIGZyb20gcGFyZW50VGFza1J1bGVMaXN0XHJcbiAgICAgICAgICAgICAgZm9yKGxldCBpPTA7IGk8cGFyZW50VGFza1J1bGVMaXN0Lmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgICAgIHJlbW92ZWRUYXNrcy5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgaWYoZWFjaFRhc2sgPT09IHBhcmVudFRhc2tSdWxlTGlzdFtpXS5jdXJyZW50VGFza1swXSl7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50VGFza1J1bGVMaXN0LnNwbGljZShpLDEpO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAocGFyZW50VGFza1J1bGVMaXN0ICYmIHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJlbnRUYXNrUnVsZUxpc3QuZm9yRWFjaChwYXJlbnRUYXNrUnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICghKHBhcmVudFRhc2tSdWxlICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCB8fCBwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YT09PSBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkpIHx8IChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ0FDVElPTicpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKCFpc1NlbGVjdGVkVGFzayAmJiAhaXNDeWNsZVRhc2sgJiYgaXNQYXJlbnRUYXNrQ29tcGxldGVkKSB7XHJcbiAgICAgICAgICAgIGlmICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwgJiYgdGFzay5JRCAhPT0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZSkgeyAvLyAgJiYgdGFzay5UQVNLX1NUQVJUX0RBVEUgPiBjdXJyZW50VGFzay5UQVNLX0RVRV9EQVRFXHJcbiAgICAgICAgICAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5JRCxcclxuICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRVZJRVdfUkVKRUNURUQpXHJcbiAgICAgICAgICAgICAgICxpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLPT09ICd0cnVlJyAgP3RydWU6ZmFsc2VcclxuICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmdyb3VwVGFyZ2V0VGFzaygpO1xyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0VHlwZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHRhcmdldFR5cGUgPT4ge1xyXG4gICAgICBpZiAodGFyZ2V0VHlwZSkge1xyXG4gICAgICAgIHRoaXMudGFyZ2V0RGF0YSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgaWYgKHRhcmdldFR5cGUgPT09ICdFVkVOVCcpIHtcclxuICAgICAgICAgIGNvbnN0IHRhc2tFdmVudHMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFRhc2tFdmVudHMoKTtcclxuICAgICAgICAgIHRhc2tFdmVudHMuZm9yRWFjaChldmVudCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICBuYW1lOiBldmVudC5ESVNQTEFZX05BTUUsXHJcbiAgICAgICAgICAgICAgdmFsdWU6IGV2ZW50WydNUE1fRXZlbnRzLWlkJ10uSWRcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRhcmdldFR5cGUgPT09ICdUQVNLJykge1xyXG4gICAgICAgICAgdGhpcy50YXNrRGF0YS5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICBsZXQgaXNTZWxlY3RlZFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgbGV0IGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIGxldCBpc0N5Y2xlVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goKGVhY2hUYXNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYodGFzay5JRCA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgaXNTZWxlY3RlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXJnZXRUYXNrUnVsZUxpc3QgPSBbXTsgXHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXRUYXNrUnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHJ1bGUuY3VycmVudFRhc2tbMF0gPT09IHRhc2suSUQgJiYgcnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgcnVsZS50cmlnZ2VyRGF0YSA9PT0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzQ3ljbGVUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlcz0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRT09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRClcclxuICAgICAgICAgICAgaWYodGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgIGlmKGVhY2hUYXNrID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpe1xyXG4gICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KSBcclxuICAgICAgICAgICAgICBpZiAoIShpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrXHJcbiAgICAgICAgICAgICAgICAmJiAoKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdTVEFUVVMnICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKSB8fFxyXG4gICAgICAgICAgICAgICAgICAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ0FDVElPTicpKSkpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmVudFRhc2tSdWxlTGlzdCA9IHRoaXMuY3VycmVudFJ1bGVMaXN0LmZpbHRlcihydWxlID0+IHJ1bGUuY3VycmVudFRhc2tbMF0gPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSB8fCBydWxlLnRhcmdldERhdGEgPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSk7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnRUYXNrUnVsZUxpc3QgJiYgcGFyZW50VGFza1J1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBwYXJlbnRUYXNrUnVsZUxpc3QuZm9yRWFjaChwYXJlbnRUYXNrUnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoIShwYXJlbnRUYXNrUnVsZSAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQgfHwgcGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGE9PT0gc3RhdHVzQXNSZXF1ZXN0ZWRDaGFuZ2VzWydNUE1fU3RhdHVzLWlkJ10uSWQpKSB8fCAocGFyZW50VGFza1J1bGUudHJpZ2dlclR5cGUgPT09ICdBQ1RJT04nKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCFpc1NlbGVjdGVkVGFzayAmJiAhaXNDeWNsZVRhc2sgJiYgaXNQYXJlbnRUYXNrQ29tcGxldGVkKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5JVElBTCApIHsgLy8gICYmIHRhc2suVEFTS19TVEFSVF9EQVRFID4gY3VycmVudFRhc2suVEFTS19EVUVfREFURVxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRVZJRVdfUkVKRUNURUQpLFxyXG4gICAgICAgICAgICAgICAgICBpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLID09PSd0cnVlJyA/dHJ1ZTpmYWxzZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5ncm91cFRhcmdldFRhc2soKTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuZW5hYmxlKCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMuaXNSdWxlU2F2ZWQgPSBmYWxzZTtcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLmlzUnVsZUNoYW5nZWQgPSB0cnVlO1xyXG4gICAgfSk7XHJcbiAgICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgY29uc3Qgc291cmNlVGFza3NTZXQgPSBuZXcgU2V0KCk7XHJcbiAgICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgcnVsZS5oYXNQcmVkZWNlc3NvciA9IHRydWU7XHJcbiAgICB9KVxyXG4gICAgdGhpcy50YXNrRGF0YS5mb3JFYWNoKCh0YXNrKSA9PiB7XHJcbiAgICAgIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2suZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgaWYodGFzay5JRCA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgc291cmNlVGFza3NTZXQuYWRkKHRhc2suVEFTS19OQU1FKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9KVxyXG4gICAgc291cmNlVGFza3NTZXQuZm9yRWFjaCh0YXNrID0+IHRoaXMuY3VycmVudFNvdXJjZVRhc2tzLnB1c2godGFzaykpO1xyXG4gICAgdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzayA9IHRoaXMucnVsZURhdGEuY3VycmVudFRhc2subGVuZ3RoID4gMCAmJiBBcnJheS5pc0FycmF5KHRoaXMucnVsZURhdGEuY3VycmVudFRhc2spID8gdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzayA6IFt0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrXTtcclxuICAgIHRoaXMucnVsZUxpc3QuZm9yRWFjaCgocnVsZSkgPT4ge1xyXG4gICAgICBydWxlLmN1cnJlbnRUYXNrID0gcnVsZS5jdXJyZW50VGFzay5sZW5ndGggPiAwICYmIEFycmF5LmlzQXJyYXkocnVsZS5jdXJyZW50VGFzaykgPyBydWxlLmN1cnJlbnRUYXNrIDogW3J1bGUuY3VycmVudFRhc2tdO1xyXG4gICAgfSlcclxuICAgIHRoaXMuaW5pdGlhbGl6ZUZvcm0oKTtcclxuICB9XHJcbiAgXHJcbiAgZ3JvdXBUYXJnZXRUYXNrKCl7XHJcbiAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXMgPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpO1xyXG4gIC8vIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgIGlmKHRoaXMudGFza1dvcmtGbG93UnVsZVRvRGlzcGxheT8uSVNfQVBQUk9WQUxfVEFTSz09PSAndHJ1ZScgJiYgISh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlPT1zdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkpe1xyXG4gICAgICBjb25zdCBuZXd0YXJnZXREYXRhPSB0aGlzLnRhcmdldERhdGEuZmlsdGVyKHRhcmdldFRhc2s9PiAhdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5pbmNsdWRlcyh0YXJnZXRUYXNrLnZhbHVlKSkvKi5maWx0ZXIodDE9PiB0MS5pc0FwcHJvdmFsVGFzaz09ZmFsc2UpKi9cclxuICAgICAgdGhpcy50YXJnZXREYXRhPSBuZXd0YXJnZXREYXRhXHJcbiAgICAgfVxyXG4gICAgIFxyXG4gICAgZWxzZSBpZih0aGlzLnRhc2tXb3JrRmxvd1J1bGVUb0Rpc3BsYXk/LklTX0FQUFJPVkFMX1RBU0s9PT0gJ3RydWUnICYmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlPT1zdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkpe1xyXG4gICAgIGNvbnN0IHBhcmVudElkPSB0aGlzLnRhc2tXb3JrRmxvd1J1bGVUb0Rpc3BsYXk/LlRBU0tfUEFSRU5UX0lEKycnO1xyXG4gICAgIGNvbnN0IHJlbW92ZVRhcmdldFRhc2s9IG5ldyBTZXQoKTtcclxuICAgICAgdGhpcy50YXNrcnVsZUxpc3RHcm91cC5mb3JFYWNoKHRhc2s9PntcclxuICAgICAgICBpZih0YXNrLmN1cnJlbnRUYXNrLmluY2x1ZGVzKHBhcmVudElkKSl7XHJcbiAgICAgICAgICB0YXNrLmN1cnJlbnRUYXNrLmZvckVhY2godGFza0lkPT5yZW1vdmVUYXJnZXRUYXNrLmFkZCh0YXNrSWQpKVxyXG4gICAgICAgIH1cclxuICAgICAgIC8vIGNvbnN0IG5ld3RhcmdldERhdGE9IHRoaXMudGFyZ2V0RGF0YS5maWx0ZXIodGFyZ2V0VGFzaz0+IHRhcmdldFRhc2sudmFsdWUhPXRoaXMudGFza1dvcmtGbG93UnVsZVRvRGlzcGxheT8uVEFTS19QQVJFTlRfSUQgJiYgIXRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuaW5jbHVkZXModGFyZ2V0VGFzay52YWx1ZSkgJiYgIXJlbW92ZVRhcmdldFRhc2suaGFzKHRhcmdldFRhc2sudmFsdWUpKVxyXG4gICAgICBjb25zdCBuZXd0YXJnZXREYXRhPSB0aGlzLnRhcmdldERhdGEuZmlsdGVyKHRhcmdldFRhc2s9PiF0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmluY2x1ZGVzKHRhcmdldFRhc2sudmFsdWUpKVxyXG4gICAgICAgdGhpcy50YXJnZXREYXRhPSBuZXd0YXJnZXREYXRhXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgICAgZWxzZSBpZih0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlIT1zdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCl7XHJcbiAgICAgIHZhciBuZXd0YXJnZXREYXRhPSB0aGlzLnRhcmdldERhdGEuZmlsdGVyKHRhcmdldFRhc2s9PiAhdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5pbmNsdWRlcyh0YXJnZXRUYXNrLnZhbHVlKSkvKi5maWx0ZXIodDE9PiB0MS5pc0FwcHJvdmFsVGFzaz09ZmFsc2UpKi9cclxuICAgICAgXHJcbiAgICAgIHRoaXMudGFyZ2V0RGF0YT0gbmV3dGFyZ2V0RGF0YVxyXG4gICAgIH1cclxuICAgICBlbHNle1xyXG4gICAgY29uc3QgbmV3dGFyZ2V0RGF0YT0gdGhpcy50YXJnZXREYXRhLmZpbHRlcih0YXJnZXRUYXNrPT4gLyp0YXJnZXRUYXNrLnZhbHVlIT10aGlzLnRhc2tXb3JrRmxvd1J1bGVUb0Rpc3BsYXk/LlRBU0tfUEFSRU5UX0lEICYmKi8gIXRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuaW5jbHVkZXModGFyZ2V0VGFzay52YWx1ZSkpXHJcbiAgICB0aGlzLnRhcmdldERhdGE9IG5ld3RhcmdldERhdGFcclxuICAgIH1cclxufVxyXG59XHJcbiJdfQ==