import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { TaskConstants } from './task.constants';
import { TaskTypeIconForTask } from '../shared/constants/task-type-icon.constants';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../shared/services/otmm-metadata.service";
import * as i4 from "../../shared/services/indexer/indexer.service";
import * as i5 from "../../shared/services/field-config.service";
import * as i6 from "../../shared/services/view-config.service";
import * as i7 from "../../mpm-utils/services/sharing.service";
let TaskService = class TaskService {
    constructor(appService, otmmService, otmmMetadataService, indexerService, fieldConfigService, viewConfigService, sharingService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.otmmMetadataService = otmmMetadataService;
        this.indexerService = indexerService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.sharingService = sharingService;
        this.STATUS_METHOD_NS = 'http://schemas/AcheronMPMSupportingEntity/Status/operations';
        this.TASK_TYPE_METHOD_NS = 'http://schemas/AcheronMPMAppConfig/MPM_Task_Type/operations';
        this.TASK_BPM_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.TASK_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.WORKFLOW_RULE_NS = 'http://schemas/AcheronMPMCore/Workflow_Rules/operations';
        this.TASK_WSAPP_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.DELETE_WORKFLOW_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_MPM_USER_PREFERENCE_BY_MENU_NS = "http://schemas/AcheronMPMCore/MPM_User_Preference/operations";
        this.GET_MPM_USER_PREFERENCE_BY_MENU = "GetDefaultUserPreferenceByMenu";
        this.DELETE_DELIVERABLE_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_ALL_APPROVAL_TASKS_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.DELETE_TASK_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.ASSIGN_DELIVERABLE_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.DELETE_WORKFLOW = 'DeleteWorkflow';
        this.FORCE_DELETE_TASK = 'ForceDeleteTask';
        this.SOFT_DELETE_TASK = 'SoftDeleteTask';
        this.GET_STATUS_FOR_CATEGORY_WS_METHOD_NAME = 'GetStatusForCategoryLevelName';
        this.GET_TASK_TYPE_WS_METHOD_NAME = 'ReadTask_Type';
        this.GET_STATUS_WS_METHOD_NAME = 'ReadStatus';
        this.GET_DEPENDENT_TASK = 'GetDependentTask';
        this.GET_TASK_BY_ID = 'ReadTask';
        this.UPDATE_TASK = 'UpdateTask';
        this.CREATE_TASK = 'CreateTask';
        this.EDIT_TASK = 'EditTask';
        this.CREATE_WORKFLOW_RULE_WS_METHOD_NAME = 'CreateWorkflowRules';
        this.GET_WORKFLOW_RULES_BY_PROJECT_WS_METHOD_NAME = 'GetWorkflowRulesByProject';
        this.GET_WORKFLOW_RULES_BY_TASK_WS_METHOD_NAME = 'GetWorkflowRulesByTask';
        this.DELETE_WORKFLOW_RULES_BY_ID_WS_METHOD_NAME = 'DeleteWorkflow_Rules';
        this.TRIGGER_TASK_BY_RULE_WS_METHOD_NAME = 'TriggerTaskByRule';
        this.DELETE_WORKFLOW_RULES_WS_METHOD_NAME = 'DeleteWorkflowRules';
        this.UPDATE_DATE_ON_RULE_INITIATE_WS_METHOD_NAME = 'UpdateDateOnRuleInitiate';
        this.GET_ACTION_RULES_BY_PROJECT_ID_WS_METHOD_NAME = 'GetActionRulesByProjectId';
        this.TRIGGER_RULE_ON_ACTION_WS_METHOD_NAME = 'TriggerRuleOnAction';
        this.DELETE_DELIVERABLE = 'DeleteDeliverable';
        this.GET_ALL_APPROVAL_TASKS = 'GetAllApprovalTasks';
        this.DELETE_TASK = 'DeleteTask';
        this.ASSIGN_DELIVERABLE = 'AssignDeliverable';
        this.REMOVE_WORKFLOW_RULES = 'RemoveWorkflowRules';
        this.defaultTaskMetadataFields = [];
        this.customTaskMetadataFields = [];
        this.refreshDataSubject = new BehaviorSubject(100);
        this.refreshData = this.refreshDataSubject.asObservable();
        this.deliverableRefreshDataSubject = new BehaviorSubject(100);
        this.deliverableRefreshData = this.deliverableRefreshDataSubject.asObservable();
    }
    setRefreshData(data) {
        if (data !== 100) {
            this.refreshDataSubject.next(data);
        }
    }
    setDeliverableRefreshData(data) {
        if (data !== 100) {
            this.deliverableRefreshDataSubject.next(data);
        }
    }
    setDefaultTaskMetadataFields(metadataFields) {
        this.defaultTaskMetadataFields = metadataFields;
    }
    getDefaultTaskMetadataFields() {
        return this.defaultTaskMetadataFields;
    }
    setCustomTaskMetadataFields(metadataFields) {
        this.customTaskMetadataFields = metadataFields;
    }
    getCustomTaskMetadataFields() {
        return this.customTaskMetadataFields;
    }
    getStatus(categoryLevelName) {
        const getRequestObject = {
            CategoryLevelName: categoryLevelName
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.STATUS_METHOD_NS, this.GET_STATUS_FOR_CATEGORY_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (!Array.isArray(response.Status)) {
                    response.Status = [response.Status];
                }
                observer.next(response.Status);
                observer.complete();
            }, error => {
                observer.next(error);
                observer.complete();
            });
        });
    }
    getTaskType(taskTypeId) {
        return new Observable(observer => {
            const params = {
                'Task_Type-id': {
                    Id: taskTypeId
                }
            };
            this.appService.invokeRequest(this.TASK_TYPE_METHOD_NS, this.GET_TASK_TYPE_WS_METHOD_NAME, params)
                .subscribe(taskTypeDetailResponse => {
                if (taskTypeDetailResponse && taskTypeDetailResponse.Task_Type) {
                    observer.next(taskTypeDetailResponse.Task_Type);
                    observer.complete();
                }
            }, taskTypeDetailError => {
                observer.error(taskTypeDetailError);
            });
        });
    }
    getStatusById(statusId) {
        return new Observable(observer => {
            const params = {
                'Status-id': {
                    Id: statusId
                }
            };
            this.appService.invokeRequest(this.STATUS_METHOD_NS, this.GET_STATUS_WS_METHOD_NAME, params)
                .subscribe(statusResponse => {
                if (statusResponse && statusResponse.Status) {
                    observer.next(statusResponse.Status);
                    observer.complete();
                }
            }, statusError => {
                observer.error(statusError);
            });
        });
    }
    updateDragandDropLanes(swimlane) {
        return new Observable(observer => {
            observer.next(true);
            observer.complete();
        });
    }
    getProperty(obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    }
    converToLocalDate(obj, path) {
        const dateObj = this.otmmMetadataService.getFieldValueById(obj.metadata, path);
        if (dateObj != null) {
            return dateObj;
        }
        return '';
    }
    getAllActiveTasks(parameters, projectViewName) {
        return new Observable(observer => {
            if (!parameters.projectId) {
                observer.error('Project id is missing.');
            }
            let viewConfig;
            let searchConditionList = this.fieldConfigService.formIndexerIdFromMapperId(TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST.search_condition_list.search_condition);
            searchConditionList = searchConditionList.concat(this.fieldConfigService.formIndexerIdFromMapperId([TaskConstants.ACTIVE_TASK_SEARCH_CONDITION]));
            const projectSearch = this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, parameters.projectId);
            if (projectSearch) {
                searchConditionList.push(projectSearch);
            }
            if (projectViewName) {
                viewConfig = projectViewName; //this.sharingService.getViewConfigById(projectViewId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
                const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                const parametersReq = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: '',
                    search_condition_list: {
                        search_condition: searchConditionList
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: [{
                                field_id: this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME),
                                order: 'ASC'
                            }]
                    },
                    cursor: {
                        page_index: parameters.skip || 0,
                        page_size: parameters.top || 100
                    }
                };
                this.indexerService.search(parametersReq).subscribe((response) => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    }
    getTaskDataValues(taskId) {
        const parameter = {
            TaskID: taskId
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.TASK_BPM_NS, 'EditTaskDateValidation', parameter)
                .subscribe(response => {
                observer.next({
                    startMaxDate: response && response.Date.StartDate && typeof response.Date.StartDate === 'string' ? new Date(response.Date.StartDate) : null,
                    endMinDate: response && response.Date.EndDate && typeof response.Date.EndDate === 'string' ? new Date(response.Date.EndDate) : null,
                });
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getDependentTask(parameters) {
        return new Observable(observer => {
            this.appService.invokeRequest(this.TASK_BPM_NS, this.GET_DEPENDENT_TASK, parameters)
                .subscribe(dependentTaskResponse => {
                if (dependentTaskResponse && dependentTaskResponse.Task) {
                    observer.next(dependentTaskResponse.Task);
                    observer.complete();
                }
                else {
                    observer.next([]);
                    observer.complete();
                }
            }, error => {
                observer.error(error);
            });
        });
    }
    getTaskById(taskId) {
        const parameter = {
            'Task-id': {
                Id: taskId
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.TASK_NS, this.GET_TASK_BY_ID, parameter)
                .subscribe(response => {
                observer.next(response.Task);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    createTask(parameters) {
        return this.appService.invokeRequest(this.TASK_BPM_NS, this.CREATE_TASK, parameters);
    }
    updateTask(parameters) {
        return new Observable(observer => {
            this.appService.invokeRequest(this.TASK_BPM_NS, this.EDIT_TASK, parameters)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getTaskIconByTaskType(taskType) {
        let taskIcon = 'fiber_manual_record';
        TaskConstants.TASK_TYPE_LIST.forEach(eachTaskType => {
            if (eachTaskType.NAME === taskType) {
                taskIcon = TaskTypeIconForTask[eachTaskType.NAME];
            }
        });
        return taskIcon;
    }
    createWorkflowRule(ruleFormValues, projectId, workflowRuleId) {
        return new Observable(observer => {
            const parameter = {
                WorkflowRules: {
                    TriggerType: ruleFormValues.triggerType,
                    TargetType: ruleFormValues.targetType,
                    RPOProject: {
                        ProjectID: {
                            Id: projectId
                        }
                    },
                    RPOTask: {
                        TaskID: {
                            Id: ruleFormValues.currentTask
                        }
                    },
                    RPOStatus: {
                        StatusID: {
                            Id: ruleFormValues.triggerType === 'STATUS' ? ruleFormValues.triggerData : ''
                        }
                    },
                    RPOActions: {
                        ActionID: {
                            Id: ruleFormValues.triggerType === 'ACTION' ? ruleFormValues.triggerData : ''
                        }
                    },
                    RPOEvent: {
                        EventID: {
                            Id: ruleFormValues.targetType === 'EVENT' ? ruleFormValues.targetData : ''
                        }
                    },
                    RPOTargetTask: {
                        TargetTaskID: {
                            Id: ruleFormValues.targetType === 'TASK' ? ruleFormValues.targetData : ''
                        }
                    },
                    WorkflowRuleId: {
                        Id: workflowRuleId
                    },
                    IsInitialRule: ruleFormValues.isInitialRule
                }
            };
            this.appService.invokeRequest(this.TASK_BPM_NS, this.CREATE_WORKFLOW_RULE_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                    if (response.APIResponse.data && response.APIResponse.data.Workflow_Rules) {
                        observer.next(response.APIResponse.data.Workflow_Rules);
                        observer.complete();
                    }
                    else {
                        observer.next(false);
                        observer.complete();
                    }
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    removeWorkflowRule(WorkflowRuleId, RuleData) {
        return new Observable(observer => {
            const parameter = {
                WorkflowRules: {
                    Rule: WorkflowRuleId
                },
                RuleData
            };
            this.appService.invokeRequest(this.DELETE_WORKFLOW_NS, this.REMOVE_WORKFLOW_RULES, parameter).subscribe(response => {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    deleteWorkflowRules(ruleIds) {
        return new Observable(observer => {
            const parameter = {
                WorkflowRules: {
                    Rule: ruleIds
                }
            };
            this.appService.invokeRequest(this.TASK_BPM_NS, this.DELETE_WORKFLOW_RULES_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    getWorkflowRulesByProject(projectId) {
        return new Observable(observer => {
            const parameter = {
                projectId: projectId
            };
            this.appService.invokeRequest(this.WORKFLOW_RULE_NS, this.GET_WORKFLOW_RULES_BY_PROJECT_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response && response.Workflow_Rules) {
                    observer.next(response.Workflow_Rules);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    getWorkflowRulesByTask(taskId) {
        return new Observable(observer => {
            const parameter = {
                taskId: taskId
            };
            this.appService.invokeRequest(this.WORKFLOW_RULE_NS, this.GET_WORKFLOW_RULES_BY_TASK_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response && response.Workflow_Rules) {
                    observer.next(response.Workflow_Rules);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    UpdateDateOnRuleInitiate(projectId) {
        return new Observable(observer => {
            const parameter = {
                projectId: projectId
            };
            this.appService.invokeRequest(this.TASK_BPM_NS, this.UPDATE_DATE_ON_RULE_INITIATE_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    triggerWorkflow(projectId, isStatusRules) {
        return new Observable(observer => {
            const parameter = {
                ProjectId: projectId,
                IsStatusRules: isStatusRules
            };
            this.appService.invokeRequest(this.TASK_BPM_NS, this.TRIGGER_TASK_BY_RULE_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response && response.APIResponse && response.APIResponse.statusCode && response.APIResponse.statusCode === '200') {
                    observer.next(response);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    getActionRulesByProjectId(projectId) {
        return new Observable(observer => {
            const parameter = {
                ProjectId: projectId
            };
            this.appService.invokeRequest(this.TASK_WSAPP_NS, this.GET_ACTION_RULES_BY_PROJECT_ID_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response && response.tuple && response.tuple.old && response.tuple.old.ActionRules && response.tuple.old.ActionRules.ActionRule) {
                    let actionRuleResponse;
                    if (response.tuple.old.ActionRules.ActionRule.length > 0) {
                        actionRuleResponse = response.tuple.old.ActionRules.ActionRule;
                    }
                    else {
                        actionRuleResponse = [response.tuple.old.ActionRules.ActionRule];
                    }
                    observer.next(actionRuleResponse);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    triggerRuleOnAction(taskId, actionId) {
        return new Observable(observer => {
            const parameter = {
                TaskId: taskId,
                ActionId: actionId
            };
            this.appService.invokeRequest(this.TASK_BPM_NS, this.TRIGGER_RULE_ON_ACTION_WS_METHOD_NAME, parameter).subscribe(response => {
                if (response) {
                    observer.next(response);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    }
    /*deleteTaskDeliverable(type: string, Id: string, isCustomWorkflow: boolean): Observable<any> {
        const parameter = {
            type: type,
            id: Id,
            isCustomWorkflow: isCustomWorkflow
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_WORKFLOW_NS, this.DELETE_WORKFLOW, parameter)
                .subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
        });
    }*/
    forceDeleteTask(Id, isCustomWorkflow) {
        const parameter = {
            id: Id,
            isCustomWorkflow: isCustomWorkflow,
            isForceUploadDeliverableDelete: false
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_WORKFLOW_NS, this.FORCE_DELETE_TASK, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    softDeleteTask(Id, isCustomWorkflow) {
        const parameter = {
            id: Id,
            isCustomWorkflow: isCustomWorkflow
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_WORKFLOW_NS, this.SOFT_DELETE_TASK, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    //  MPMV3-2094
    deleteDeliverable(deliverableID, forceDelete) {
        const parameter = {
            deliverableID: deliverableID,
            forceDelete: forceDelete
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_DELIVERABLE_NS, this.DELETE_DELIVERABLE, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    //  MPMV3-2094
    getAllApprovalTasks(taskID) {
        const parameter = {
            taskIds: {
                TaskDetails: {
                    task: {
                        TaskId: taskID
                    }
                }
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.GET_ALL_APPROVAL_TASKS_NS, this.GET_ALL_APPROVAL_TASKS, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    //  MPMV3-2094
    deleteTask(taskID, forceDelete) {
        const parameter = {
            taskID: taskID,
            forceDelete: forceDelete
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_TASK_NS, this.DELETE_TASK, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    // MPM_V3-2217
    assignDeliverable(deliverableId, userId) {
        const parameter = {
            DeliverableId: deliverableId,
            UserId: userId
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.ASSIGN_DELIVERABLE_NS, this.ASSIGN_DELIVERABLE, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
};
TaskService.ctorParameters = () => [
    { type: AppService },
    { type: OTMMService },
    { type: OtmmMetadataService },
    { type: IndexerService },
    { type: FieldConfigService },
    { type: ViewConfigService },
    { type: SharingService }
];
TaskService.ɵprov = i0.ɵɵdefineInjectable({ factory: function TaskService_Factory() { return new TaskService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.OtmmMetadataService), i0.ɵɵinject(i4.IndexerService), i0.ɵɵinject(i5.FieldConfigService), i0.ɵɵinject(i6.ViewConfigService), i0.ɵɵinject(i7.SharingService)); }, token: TaskService, providedIn: "root" });
TaskService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], TaskService);
export { TaskService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUVwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFJakQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFHbkYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRTlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7Ozs7Ozs7O0FBTzFFLElBQWEsV0FBVyxHQUF4QixNQUFhLFdBQVc7SUFnRHBCLFlBQ1csVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGNBQThCLEVBQzlCLGtCQUFzQyxFQUN0QyxpQkFBb0MsRUFDcEMsY0FBOEI7UUFOOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBckR6QyxxQkFBZ0IsR0FBRyw2REFBNkQsQ0FBQztRQUNqRix3QkFBbUIsR0FBRyw2REFBNkQsQ0FBQztRQUNwRixnQkFBVyxHQUFHLHNEQUFzRCxDQUFDO1FBQ3JFLFlBQU8sR0FBRywrQ0FBK0MsQ0FBQztRQUMxRCxxQkFBZ0IsR0FBRyx5REFBeUQsQ0FBQztRQUM3RSxrQkFBYSxHQUFHLCtDQUErQyxDQUFDO1FBQ2hFLHVCQUFrQixHQUFHLHNEQUFzRCxDQUFDO1FBRTVFLHVDQUFrQyxHQUFHLDhEQUE4RCxDQUFDO1FBQ3BHLG9DQUErQixHQUFHLGdDQUFnQyxDQUFDO1FBRW5FLDBCQUFxQixHQUFHLHNEQUFzRCxDQUFDO1FBQy9FLDhCQUF5QixHQUFHLCtDQUErQyxDQUFDO1FBQzVFLG1CQUFjLEdBQUcsc0RBQXNELENBQUM7UUFFeEUsMEJBQXFCLEdBQUcsc0RBQXNELENBQUM7UUFFL0Usb0JBQWUsR0FBRyxnQkFBZ0IsQ0FBQztRQUNuQyxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0QyxxQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUNwQywyQ0FBc0MsR0FBRywrQkFBK0IsQ0FBQztRQUN6RSxpQ0FBNEIsR0FBRyxlQUFlLENBQUM7UUFDL0MsOEJBQXlCLEdBQUcsWUFBWSxDQUFDO1FBQ3pDLHVCQUFrQixHQUFHLGtCQUFrQixDQUFDO1FBQ3hDLG1CQUFjLEdBQUcsVUFBVSxDQUFDO1FBQzVCLGdCQUFXLEdBQUcsWUFBWSxDQUFDO1FBQzNCLGdCQUFXLEdBQUcsWUFBWSxDQUFDO1FBQzNCLGNBQVMsR0FBRyxVQUFVLENBQUM7UUFDdkIsd0NBQW1DLEdBQUcscUJBQXFCLENBQUM7UUFDNUQsaURBQTRDLEdBQUcsMkJBQTJCLENBQUM7UUFDM0UsOENBQXlDLEdBQUcsd0JBQXdCLENBQUM7UUFDckUsK0NBQTBDLEdBQUcsc0JBQXNCLENBQUM7UUFDcEUsd0NBQW1DLEdBQUcsbUJBQW1CLENBQUM7UUFDMUQseUNBQW9DLEdBQUcscUJBQXFCLENBQUM7UUFDN0QsZ0RBQTJDLEdBQUcsMEJBQTBCLENBQUM7UUFDekUsa0RBQTZDLEdBQUcsMkJBQTJCLENBQUM7UUFDNUUsMENBQXFDLEdBQUcscUJBQXFCLENBQUM7UUFDOUQsdUJBQWtCLEdBQUcsbUJBQW1CLENBQUM7UUFDekMsMkJBQXNCLEdBQUcscUJBQXFCLENBQUM7UUFDL0MsZ0JBQVcsR0FBRyxZQUFZLENBQUM7UUFDM0IsdUJBQWtCLEdBQUcsbUJBQW1CLENBQUM7UUFDekMsMEJBQXFCLEdBQUcscUJBQXFCLENBQUM7UUFFOUMsOEJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQy9CLDZCQUF3QixHQUFHLEVBQUUsQ0FBQztRQVl2Qix1QkFBa0IsR0FBRyxJQUFJLGVBQWUsQ0FBTSxHQUFHLENBQUMsQ0FBQztRQUMxRCxnQkFBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUU5QyxrQ0FBNkIsR0FBRyxJQUFJLGVBQWUsQ0FBTSxHQUFHLENBQUMsQ0FBQztRQUNyRSwyQkFBc0IsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFOdkUsQ0FBQztJQVFMLGNBQWMsQ0FBQyxJQUFTO1FBQ3BCLElBQUksSUFBSSxLQUFLLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdEM7SUFDTCxDQUFDO0lBRUQseUJBQXlCLENBQUMsSUFBUztRQUMvQixJQUFJLElBQUksS0FBSyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELDRCQUE0QixDQUFDLGNBQWM7UUFDdkMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLGNBQWMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsNEJBQTRCO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDO0lBQzFDLENBQUM7SUFFRCwyQkFBMkIsQ0FBQyxjQUFjO1FBQ3RDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxjQUFjLENBQUM7SUFDbkQsQ0FBQztJQUVELDJCQUEyQjtRQUN2QixPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN6QyxDQUFDO0lBRUQsU0FBUyxDQUFDLGlCQUFpQjtRQUN2QixNQUFNLGdCQUFnQixHQUFHO1lBQ3JCLGlCQUFpQixFQUFFLGlCQUFpQjtTQUN2QyxDQUFDO1FBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHNDQUFzQyxFQUFFLGdCQUFnQixDQUFDO2lCQUM5RyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUFFO2dCQUM3RSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXLENBQUMsVUFBVTtRQUNsQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sTUFBTSxHQUFHO2dCQUNYLGNBQWMsRUFBRTtvQkFDWixFQUFFLEVBQUUsVUFBVTtpQkFDakI7YUFDSixDQUFDO1lBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxNQUFNLENBQUM7aUJBQzdGLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO2dCQUNoQyxJQUFJLHNCQUFzQixJQUFJLHNCQUFzQixDQUFDLFNBQVMsRUFBRTtvQkFDNUQsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDaEQsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxtQkFBbUIsQ0FBQyxFQUFFO2dCQUNyQixRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxhQUFhLENBQUMsUUFBUTtRQUNsQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sTUFBTSxHQUFHO2dCQUNYLFdBQVcsRUFBRTtvQkFDVCxFQUFFLEVBQUUsUUFBUTtpQkFDZjthQUNKLENBQUM7WUFFRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHlCQUF5QixFQUFFLE1BQU0sQ0FBQztpQkFDdkYsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUN4QixJQUFJLGNBQWMsSUFBSSxjQUFjLENBQUMsTUFBTSxFQUFFO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDckMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxXQUFXLENBQUMsRUFBRTtnQkFDYixRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxDQUFDO1FBRVgsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsUUFBUTtRQUMzQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVcsQ0FBQyxHQUFHLEVBQUUsSUFBSTtRQUNqQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRTtZQUNoQyxPQUFPO1NBQ1Y7UUFDRCxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQsaUJBQWlCLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDdkIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0UsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO1lBQ2pCLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsVUFBVSxFQUFFLGVBQWdCO1FBQzFDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3ZCLFFBQVEsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQzthQUM1QztZQUNELElBQUksVUFBVSxDQUFDO1lBQ2YsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsYUFBYSxDQUFDLG1DQUFtQyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFFdEssbUJBQW1CLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVsSixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFDakgsa0JBQWtCLENBQUMsRUFBRSxFQUFFLHNCQUFzQixDQUFDLEVBQUUsRUFBRSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXBHLElBQUksYUFBYSxFQUFFO2dCQUNmLG1CQUFtQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUMzQztZQUNELElBQUksZUFBZSxFQUFFO2dCQUNqQixVQUFVLEdBQUcsZUFBZSxDQUFDLENBQUEsNERBQTREO2FBQzVGO2lCQUFNO2dCQUNILFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO2FBQ3ZEO1lBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFdBQXVCLEVBQUUsRUFBRTtnQkFDckcsTUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixJQUFJLE9BQU8sV0FBVyxDQUFDLDJCQUEyQixLQUFLLFFBQVE7b0JBQ3ZILENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25FLE1BQU0sYUFBYSxHQUFrQjtvQkFDakMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQ3BELE9BQU8sRUFBRSxFQUFFO29CQUNYLHFCQUFxQixFQUFFO3dCQUNuQixnQkFBZ0IsRUFBRSxtQkFBbUI7cUJBQ3hDO29CQUNELG9CQUFvQixFQUFFO3dCQUNsQixlQUFlLEVBQUUsRUFBRTtxQkFDdEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLElBQUksRUFBRSxDQUFDO2dDQUNILFFBQVEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQztnQ0FDeEcsS0FBSyxFQUFFLEtBQUs7NkJBQ2YsQ0FBQztxQkFDTDtvQkFDRCxNQUFNLEVBQUU7d0JBQ0osVUFBVSxFQUFFLFVBQVUsQ0FBQyxJQUFJLElBQUksQ0FBQzt3QkFDaEMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxHQUFHLElBQUksR0FBRztxQkFDbkM7aUJBQ0osQ0FBQztnQkFDRixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUF3QixFQUFFLEVBQUU7b0JBQzdFLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FBQztnQkFDbkUsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlCQUFpQixDQUFDLE1BQWM7UUFDNUIsTUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsTUFBTTtTQUNqQixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLHdCQUF3QixFQUFFLFNBQVMsQ0FBQztpQkFDL0UsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDO29CQUNWLFlBQVksRUFBRSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQzNJLFVBQVUsRUFBRSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7aUJBQ3RJLENBQUMsQ0FBQztnQkFDSCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxVQUFVO1FBQ3ZCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDO2lCQUMvRSxTQUFTLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxxQkFBcUIsSUFBSSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUU7b0JBQ3JELFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsV0FBVyxDQUFDLE1BQWM7UUFDdEIsTUFBTSxTQUFTLEdBQUc7WUFDZCxTQUFTLEVBQUU7Z0JBQ1AsRUFBRSxFQUFFLE1BQU07YUFDYjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxTQUFTLENBQUM7aUJBQ3RFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzdCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFVBQVUsQ0FBQyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFFRCxVQUFVLENBQUMsVUFBVTtRQUNqQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7aUJBQ3RFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUJBQXFCLENBQUMsUUFBZ0I7UUFDbEMsSUFBSSxRQUFRLEdBQUcscUJBQXFCLENBQUM7UUFDckMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDaEQsSUFBSSxZQUFZLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDaEMsUUFBUSxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNyRDtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELGtCQUFrQixDQUFDLGNBQWMsRUFBRSxTQUFTLEVBQUUsY0FBYztRQUN4RCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLGFBQWEsRUFBRTtvQkFDWCxXQUFXLEVBQUUsY0FBYyxDQUFDLFdBQVc7b0JBQ3ZDLFVBQVUsRUFBRSxjQUFjLENBQUMsVUFBVTtvQkFDckMsVUFBVSxFQUFFO3dCQUNSLFNBQVMsRUFBRTs0QkFDUCxFQUFFLEVBQUUsU0FBUzt5QkFDaEI7cUJBQ0o7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLE1BQU0sRUFBRTs0QkFDSixFQUFFLEVBQUUsY0FBYyxDQUFDLFdBQVc7eUJBQ2pDO3FCQUNKO29CQUNELFNBQVMsRUFBRTt3QkFDUCxRQUFRLEVBQUU7NEJBQ04sRUFBRSxFQUFFLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFO3lCQUNoRjtxQkFDSjtvQkFDRCxVQUFVLEVBQUU7d0JBQ1IsUUFBUSxFQUFFOzRCQUNOLEVBQUUsRUFBRSxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTt5QkFDaEY7cUJBQ0o7b0JBQ0QsUUFBUSxFQUFFO3dCQUNOLE9BQU8sRUFBRTs0QkFDTCxFQUFFLEVBQUUsY0FBYyxDQUFDLFVBQVUsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUU7eUJBQzdFO3FCQUNKO29CQUNELGFBQWEsRUFBRTt3QkFDWCxZQUFZLEVBQUU7NEJBQ1YsRUFBRSxFQUFFLGNBQWMsQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFO3lCQUM1RTtxQkFDSjtvQkFDRCxjQUFjLEVBQUU7d0JBQ1osRUFBRSxFQUFFLGNBQWM7cUJBQ3JCO29CQUNELGFBQWEsRUFBRSxjQUFjLENBQUMsYUFBYTtpQkFDOUM7YUFDSixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsbUNBQW1DLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN0SCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTtvQkFDL0UsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7d0JBQ3ZFLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBQ3hELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtpQkFDSjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxjQUFjLEVBQUMsUUFBUTtRQUN0QyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLGFBQWEsRUFBRTtvQkFDWCxJQUFJLEVBQUUsY0FBYztpQkFDdkI7Z0JBQ0QsUUFBUTthQUNYLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDL0csSUFBSSxRQUFRLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxPQUFPO1FBQ3ZCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxTQUFTLEdBQUc7Z0JBQ2QsYUFBYSxFQUFFO29CQUNYLElBQUksRUFBRSxPQUFPO2lCQUNoQjthQUNKLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxvQ0FBb0MsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3ZILElBQUksUUFBUSxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUJBQXlCLENBQUMsU0FBUztRQUMvQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLDRDQUE0QyxFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDcEksSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsRUFBRTtvQkFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0JBQXNCLENBQUMsTUFBTTtRQUN6QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLE1BQU0sRUFBRSxNQUFNO2FBQ2pCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHlDQUF5QyxFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakksSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsRUFBRTtvQkFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0JBQXdCLENBQUMsU0FBUztRQUM5QixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQywyQ0FBMkMsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQzlILElBQUksUUFBUSxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLFNBQVMsRUFBRSxhQUFhO1FBQ3BDLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxTQUFTLEdBQUc7Z0JBQ2QsU0FBUyxFQUFFLFNBQVM7Z0JBQ3BCLGFBQWEsRUFBRSxhQUFhO2FBQy9CLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxtQ0FBbUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3RILElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUNsSCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlCQUF5QixDQUFDLFNBQVM7UUFDL0IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLFNBQVMsR0FBRztnQkFDZCxTQUFTLEVBQUUsU0FBUzthQUN2QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsNkNBQTZDLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsSSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFO29CQUNqSSxJQUFJLGtCQUFrQixDQUFDO29CQUN2QixJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDdEQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztxQkFDbEU7eUJBQU07d0JBQ0gsa0JBQWtCLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUE7cUJBQ25FO29CQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztvQkFDbEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsUUFBUTtRQUNoQyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sU0FBUyxHQUFHO2dCQUNkLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFFBQVEsRUFBRSxRQUFRO2FBQ3JCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxxQ0FBcUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3hILElBQUksUUFBUSxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7OztPQWVHO0lBRUgsZUFBZSxDQUFDLEVBQVUsRUFBRSxnQkFBeUI7UUFDakQsTUFBTSxTQUFTLEdBQUc7WUFDZCxFQUFFLEVBQUUsRUFBRTtZQUNOLGdCQUFnQixFQUFFLGdCQUFnQjtZQUNsQyw4QkFBOEIsRUFBRSxLQUFLO1NBQ3hDLENBQUE7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNwRixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWMsQ0FBQyxFQUFVLEVBQUUsZ0JBQXlCO1FBQ2hELE1BQU0sU0FBUyxHQUFHO1lBQ2QsRUFBRSxFQUFFLEVBQUU7WUFDTixnQkFBZ0IsRUFBRSxnQkFBZ0I7U0FDckMsQ0FBQTtRQUNELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxTQUFTLENBQUM7aUJBQ25GLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYztJQUNkLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxXQUFXO1FBQ3hDLE1BQU0sU0FBUyxHQUFHO1lBQ2QsYUFBYSxFQUFFLGFBQWE7WUFDNUIsV0FBVyxFQUFFLFdBQVc7U0FDM0IsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLENBQUM7aUJBQ3hGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYztJQUNkLG1CQUFtQixDQUFDLE1BQU07UUFDdEIsTUFBTSxTQUFTLEdBQUc7WUFDZCxPQUFPLEVBQUU7Z0JBQ0wsV0FBVyxFQUFFO29CQUNULElBQUksRUFBRTt3QkFDRixNQUFNLEVBQUUsTUFBTTtxQkFDakI7aUJBQ0o7YUFDSjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsU0FBUyxDQUFDO2lCQUNoRyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWM7SUFDZCxVQUFVLENBQUMsTUFBTSxFQUFFLFdBQVc7UUFDMUIsTUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsTUFBTTtZQUNkLFdBQVcsRUFBRSxXQUFXO1NBQzNCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUM7aUJBQzFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBSUQsY0FBYztJQUNkLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxNQUFNO1FBQ25DLE1BQU0sU0FBUyxHQUFHO1lBQ2QsYUFBYSxFQUFFLGFBQWE7WUFDNUIsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLENBQUM7aUJBQ3hGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUE3a0IwQixVQUFVO1lBQ1QsV0FBVztZQUNILG1CQUFtQjtZQUN4QixjQUFjO1lBQ1Ysa0JBQWtCO1lBQ25CLGlCQUFpQjtZQUNwQixjQUFjOzs7QUF2RGhDLFdBQVc7SUFKdkIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLFdBQVcsQ0E4bkJ2QjtTQTluQlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFRhc2sgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9UYXNrJztcclxuaW1wb3J0IHsgT2JzZXJ2ZXJzTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL29ic2VydmVycyc7XHJcbmltcG9ydCB7IFRhc2tUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1Rhc2tUeXBlJztcclxuaW1wb3J0IHsgVGFza1R5cGVJY29uRm9yVGFzayB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvdGFzay10eXBlLWljb24uY29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2VhcmNoUmVxdWVzdCB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVxdWVzdCc7XHJcbmltcG9ydCB7IFNlYXJjaFJlc3BvbnNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9TZWFyY2hSZXNwb25zZSc7XHJcbmltcG9ydCB7IEluZGV4ZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvaW5kZXhlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0U29ydERpcmVjdGlvbnMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NYXRTb3J0RGlyZWN0aW9uJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvdmlldy1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFZpZXdDb25maWcgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9WaWV3Q29uZmlnJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBUYXNrU2VydmljZSB7XHJcblxyXG4gICAgU1RBVFVTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNU3VwcG9ydGluZ0VudGl0eS9TdGF0dXMvb3BlcmF0aW9ucyc7XHJcbiAgICBUQVNLX1RZUEVfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1BcHBDb25maWcvTVBNX1Rhc2tfVHlwZS9vcGVyYXRpb25zJztcclxuICAgIFRBU0tfQlBNX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgVEFTS19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9UYXNrL29wZXJhdGlvbnMnO1xyXG4gICAgV09SS0ZMT1dfUlVMRV9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Xb3JrZmxvd19SdWxlcy9vcGVyYXRpb25zJztcclxuICAgIFRBU0tfV1NBUFBfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICAgIERFTEVURV9XT1JLRkxPV19OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuXHJcbiAgICBHRVRfTVBNX1VTRVJfUFJFRkVSRU5DRV9CWV9NRU5VX05TID0gXCJodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fVXNlcl9QcmVmZXJlbmNlL29wZXJhdGlvbnNcIjtcclxuICAgIEdFVF9NUE1fVVNFUl9QUkVGRVJFTkNFX0JZX01FTlUgPSBcIkdldERlZmF1bHRVc2VyUHJlZmVyZW5jZUJ5TWVudVwiO1xyXG5cclxuICAgIERFTEVURV9ERUxJVkVSQUJMRV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIEdFVF9BTExfQVBQUk9WQUxfVEFTS1NfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICAgIERFTEVURV9UQVNLX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG5cclxuICAgIEFTU0lHTl9ERUxJVkVSQUJMRV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuXHJcbiAgICBERUxFVEVfV09SS0ZMT1cgPSAnRGVsZXRlV29ya2Zsb3cnO1xyXG4gICAgRk9SQ0VfREVMRVRFX1RBU0sgPSAnRm9yY2VEZWxldGVUYXNrJztcclxuICAgIFNPRlRfREVMRVRFX1RBU0sgPSAnU29mdERlbGV0ZVRhc2snO1xyXG4gICAgR0VUX1NUQVRVU19GT1JfQ0FURUdPUllfV1NfTUVUSE9EX05BTUUgPSAnR2V0U3RhdHVzRm9yQ2F0ZWdvcnlMZXZlbE5hbWUnO1xyXG4gICAgR0VUX1RBU0tfVFlQRV9XU19NRVRIT0RfTkFNRSA9ICdSZWFkVGFza19UeXBlJztcclxuICAgIEdFVF9TVEFUVVNfV1NfTUVUSE9EX05BTUUgPSAnUmVhZFN0YXR1cyc7XHJcbiAgICBHRVRfREVQRU5ERU5UX1RBU0sgPSAnR2V0RGVwZW5kZW50VGFzayc7XHJcbiAgICBHRVRfVEFTS19CWV9JRCA9ICdSZWFkVGFzayc7XHJcbiAgICBVUERBVEVfVEFTSyA9ICdVcGRhdGVUYXNrJztcclxuICAgIENSRUFURV9UQVNLID0gJ0NyZWF0ZVRhc2snO1xyXG4gICAgRURJVF9UQVNLID0gJ0VkaXRUYXNrJztcclxuICAgIENSRUFURV9XT1JLRkxPV19SVUxFX1dTX01FVEhPRF9OQU1FID0gJ0NyZWF0ZVdvcmtmbG93UnVsZXMnO1xyXG4gICAgR0VUX1dPUktGTE9XX1JVTEVTX0JZX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUgPSAnR2V0V29ya2Zsb3dSdWxlc0J5UHJvamVjdCc7XHJcbiAgICBHRVRfV09SS0ZMT1dfUlVMRVNfQllfVEFTS19XU19NRVRIT0RfTkFNRSA9ICdHZXRXb3JrZmxvd1J1bGVzQnlUYXNrJztcclxuICAgIERFTEVURV9XT1JLRkxPV19SVUxFU19CWV9JRF9XU19NRVRIT0RfTkFNRSA9ICdEZWxldGVXb3JrZmxvd19SdWxlcyc7XHJcbiAgICBUUklHR0VSX1RBU0tfQllfUlVMRV9XU19NRVRIT0RfTkFNRSA9ICdUcmlnZ2VyVGFza0J5UnVsZSc7XHJcbiAgICBERUxFVEVfV09SS0ZMT1dfUlVMRVNfV1NfTUVUSE9EX05BTUUgPSAnRGVsZXRlV29ya2Zsb3dSdWxlcyc7XHJcbiAgICBVUERBVEVfREFURV9PTl9SVUxFX0lOSVRJQVRFX1dTX01FVEhPRF9OQU1FID0gJ1VwZGF0ZURhdGVPblJ1bGVJbml0aWF0ZSc7XHJcbiAgICBHRVRfQUNUSU9OX1JVTEVTX0JZX1BST0pFQ1RfSURfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWN0aW9uUnVsZXNCeVByb2plY3RJZCc7XHJcbiAgICBUUklHR0VSX1JVTEVfT05fQUNUSU9OX1dTX01FVEhPRF9OQU1FID0gJ1RyaWdnZXJSdWxlT25BY3Rpb24nO1xyXG4gICAgREVMRVRFX0RFTElWRVJBQkxFID0gJ0RlbGV0ZURlbGl2ZXJhYmxlJztcclxuICAgIEdFVF9BTExfQVBQUk9WQUxfVEFTS1MgPSAnR2V0QWxsQXBwcm92YWxUYXNrcyc7XHJcbiAgICBERUxFVEVfVEFTSyA9ICdEZWxldGVUYXNrJztcclxuICAgIEFTU0lHTl9ERUxJVkVSQUJMRSA9ICdBc3NpZ25EZWxpdmVyYWJsZSc7XHJcbiAgICBSRU1PVkVfV09SS0ZMT1dfUlVMRVMgPSAnUmVtb3ZlV29ya2Zsb3dSdWxlcyc7IFxyXG5cclxuICAgIGRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuICAgIGN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1NZXRhZGF0YVNlcnZpY2U6IE90bW1NZXRhZGF0YVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHB1YmxpYyByZWZyZXNoRGF0YVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGFueT4oMTAwKTtcclxuICAgIHJlZnJlc2hEYXRhID0gdGhpcy5yZWZyZXNoRGF0YVN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcblxyXG4gICAgcHVibGljIGRlbGl2ZXJhYmxlUmVmcmVzaERhdGFTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KDEwMCk7XHJcbiAgICBkZWxpdmVyYWJsZVJlZnJlc2hEYXRhID0gdGhpcy5kZWxpdmVyYWJsZVJlZnJlc2hEYXRhU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgICBzZXRSZWZyZXNoRGF0YShkYXRhOiBhbnkpIHtcclxuICAgICAgICBpZiAoZGF0YSAhPT0gMTAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaERhdGFTdWJqZWN0Lm5leHQoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldERlbGl2ZXJhYmxlUmVmcmVzaERhdGEoZGF0YTogYW55KSB7XHJcbiAgICAgICAgaWYgKGRhdGEgIT09IDEwMCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlUmVmcmVzaERhdGFTdWJqZWN0Lm5leHQoZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldERlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZHMpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0VGFza01ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VzdG9tVGFza01ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VzdG9tVGFza01ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFN0YXR1cyhjYXRlZ29yeUxldmVsTmFtZSkge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIENhdGVnb3J5TGV2ZWxOYW1lOiBjYXRlZ29yeUxldmVsTmFtZVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU1RBVFVTX01FVEhPRF9OUywgdGhpcy5HRVRfU1RBVFVTX0ZPUl9DQVRFR09SWV9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5TdGF0dXMpKSB7IHJlc3BvbnNlLlN0YXR1cyA9IFtyZXNwb25zZS5TdGF0dXNdOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5TdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tUeXBlKHRhc2tUeXBlSWQpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICAnVGFza19UeXBlLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiB0YXNrVHlwZUlkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfVFlQRV9NRVRIT0RfTlMsIHRoaXMuR0VUX1RBU0tfVFlQRV9XU19NRVRIT0RfTkFNRSwgcGFyYW1zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSh0YXNrVHlwZURldGFpbFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGFza1R5cGVEZXRhaWxSZXNwb25zZSAmJiB0YXNrVHlwZURldGFpbFJlc3BvbnNlLlRhc2tfVHlwZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRhc2tUeXBlRGV0YWlsUmVzcG9uc2UuVGFza19UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCB0YXNrVHlwZURldGFpbEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcih0YXNrVHlwZURldGFpbEVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFN0YXR1c0J5SWQoc3RhdHVzSWQpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICAnU3RhdHVzLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBzdGF0dXNJZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5TVEFUVVNfTUVUSE9EX05TLCB0aGlzLkdFVF9TVEFUVVNfV1NfTUVUSE9EX05BTUUsIHBhcmFtcylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3RhdHVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXNSZXNwb25zZSAmJiBzdGF0dXNSZXNwb25zZS5TdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNSZXNwb25zZS5TdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIHN0YXR1c0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihzdGF0dXNFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlRHJhZ2FuZERyb3BMYW5lcyhzd2ltbGFuZSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHkob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgaWYgKCFvYmogfHwgIXBhdGggfHwgIW9iai5tZXRhZGF0YSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5SWQob2JqLm1ldGFkYXRhLCBwYXRoLCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb252ZXJUb0xvY2FsRGF0ZShvYmosIHBhdGgpIHtcclxuICAgICAgICBjb25zdCBkYXRlT2JqID0gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKG9iai5tZXRhZGF0YSwgcGF0aCk7XHJcbiAgICAgICAgaWYgKGRhdGVPYmogIT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZU9iajtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbEFjdGl2ZVRhc2tzKHBhcmFtZXRlcnMsIHByb2plY3RWaWV3TmFtZT8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghcGFyYW1ldGVycy5wcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdQcm9qZWN0IGlkIGlzIG1pc3NpbmcuJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICAgICAgICAgIGxldCBzZWFyY2hDb25kaXRpb25MaXN0ID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZm9ybUluZGV4ZXJJZEZyb21NYXBwZXJJZChUYXNrQ29uc3RhbnRzLkdFVF9BTExfVEFTS1NfU0VBUkNIX0NPTkRJVElPTl9MSVNULnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbkxpc3QgPSBzZWFyY2hDb25kaXRpb25MaXN0LmNvbmNhdCh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5mb3JtSW5kZXhlcklkRnJvbU1hcHBlcklkKFtUYXNrQ29uc3RhbnRzLkFDVElWRV9UQVNLX1NFQVJDSF9DT05ESVRJT05dKSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwcm9qZWN0U2VhcmNoID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuY3JlYXRlQ29uZGl0aW9uT2JqZWN0KE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9UQVNLX0ZJRUxEUy5UQVNLX1BST0pFQ1RfSUQsXHJcbiAgICAgICAgICAgICAgICBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsIE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsIE1QTVNlYXJjaE9wZXJhdG9ycy5BTkQsIHBhcmFtZXRlcnMucHJvamVjdElkKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChwcm9qZWN0U2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hDb25kaXRpb25MaXN0LnB1c2gocHJvamVjdFNlYXJjaCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHByb2plY3RWaWV3TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IHByb2plY3RWaWV3TmFtZTsvL3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Vmlld0NvbmZpZ0J5SWQocHJvamVjdFZpZXdJZCkuVklFVztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuVEFTSztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld0NvbmZpZykuc3Vic2NyaWJlKCh2aWV3RGV0YWlsczogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoQ29uZmlnID0gdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHICYmIHR5cGVvZiB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgPT09ICdzdHJpbmcnXHJcbiAgICAgICAgICAgICAgICAgICAgPyBwYXJzZUludCh2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcsIDEwKSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzUmVxOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IHNlYXJjaENvbmRpdGlvbkxpc3RcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0OiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEluZGV4ZXJJZEJ5TWFwcGVyVmFsdWUoTVBNRmllbGRDb25zdGFudHMuTVBNX1RBU0tfRklFTERTLlRBU0tfTkFNRSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcmRlcjogJ0FTQydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlX2luZGV4OiBwYXJhbWV0ZXJzLnNraXAgfHwgMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZV9zaXplOiBwYXJhbWV0ZXJzLnRvcCB8fCAxMDBcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbmRleGVyU2VydmljZS5zZWFyY2gocGFyYW1ldGVyc1JlcSkuc3Vic2NyaWJlKChyZXNwb25zZTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIHByb2plY3RzLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tEYXRhVmFsdWVzKHRhc2tJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIFRhc2tJRDogdGFza0lkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfQlBNX05TLCAnRWRpdFRhc2tEYXRlVmFsaWRhdGlvbicsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydE1heERhdGU6IHJlc3BvbnNlICYmIHJlc3BvbnNlLkRhdGUuU3RhcnREYXRlICYmIHR5cGVvZiByZXNwb25zZS5EYXRlLlN0YXJ0RGF0ZSA9PT0gJ3N0cmluZycgPyBuZXcgRGF0ZShyZXNwb25zZS5EYXRlLlN0YXJ0RGF0ZSkgOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmRNaW5EYXRlOiByZXNwb25zZSAmJiByZXNwb25zZS5EYXRlLkVuZERhdGUgJiYgdHlwZW9mIHJlc3BvbnNlLkRhdGUuRW5kRGF0ZSA9PT0gJ3N0cmluZycgPyBuZXcgRGF0ZShyZXNwb25zZS5EYXRlLkVuZERhdGUpIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlcGVuZGVudFRhc2socGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5HRVRfREVQRU5ERU5UX1RBU0ssIHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRlcGVuZGVudFRhc2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlcGVuZGVudFRhc2tSZXNwb25zZSAmJiBkZXBlbmRlbnRUYXNrUmVzcG9uc2UuVGFzaykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGRlcGVuZGVudFRhc2tSZXNwb25zZS5UYXNrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KFtdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza0J5SWQodGFza0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFRhc2s+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICdUYXNrLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgSWQ6IHRhc2tJZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfTlMsIHRoaXMuR0VUX1RBU0tfQllfSUQsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuVGFzayk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVUYXNrKHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfQlBNX05TLCB0aGlzLkNSRUFURV9UQVNLLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVUYXNrKHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19CUE1fTlMsIHRoaXMuRURJVF9UQVNLLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrSWNvbkJ5VGFza1R5cGUodGFza1R5cGU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHRhc2tJY29uID0gJ2ZpYmVyX21hbnVhbF9yZWNvcmQnO1xyXG4gICAgICAgIFRhc2tDb25zdGFudHMuVEFTS19UWVBFX0xJU1QuZm9yRWFjaChlYWNoVGFza1R5cGUgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZWFjaFRhc2tUeXBlLk5BTUUgPT09IHRhc2tUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrSWNvbiA9IFRhc2tUeXBlSWNvbkZvclRhc2tbZWFjaFRhc2tUeXBlLk5BTUVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRhc2tJY29uO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVdvcmtmbG93UnVsZShydWxlRm9ybVZhbHVlcywgcHJvamVjdElkLCB3b3JrZmxvd1J1bGVJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgV29ya2Zsb3dSdWxlczoge1xyXG4gICAgICAgICAgICAgICAgICAgIFRyaWdnZXJUeXBlOiBydWxlRm9ybVZhbHVlcy50cmlnZ2VyVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBUYXJnZXRUeXBlOiBydWxlRm9ybVZhbHVlcy50YXJnZXRUeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIFJQT1Byb2plY3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdElEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogcHJvamVjdElkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIFJQT1Rhc2s6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgVGFza0lEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogcnVsZUZvcm1WYWx1ZXMuY3VycmVudFRhc2tcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgUlBPU3RhdHVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFN0YXR1c0lEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogcnVsZUZvcm1WYWx1ZXMudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnID8gcnVsZUZvcm1WYWx1ZXMudHJpZ2dlckRhdGEgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBSUE9BY3Rpb25zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFjdGlvbklEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogcnVsZUZvcm1WYWx1ZXMudHJpZ2dlclR5cGUgPT09ICdBQ1RJT04nID8gcnVsZUZvcm1WYWx1ZXMudHJpZ2dlckRhdGEgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBSUE9FdmVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBFdmVudElEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogcnVsZUZvcm1WYWx1ZXMudGFyZ2V0VHlwZSA9PT0gJ0VWRU5UJyA/IHJ1bGVGb3JtVmFsdWVzLnRhcmdldERhdGEgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBSUE9UYXJnZXRUYXNrOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFRhcmdldFRhc2tJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHJ1bGVGb3JtVmFsdWVzLnRhcmdldFR5cGUgPT09ICdUQVNLJyA/IHJ1bGVGb3JtVmFsdWVzLnRhcmdldERhdGEgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBXb3JrZmxvd1J1bGVJZDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogd29ya2Zsb3dSdWxlSWRcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIElzSW5pdGlhbFJ1bGU6IHJ1bGVGb3JtVmFsdWVzLmlzSW5pdGlhbFJ1bGVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5DUkVBVEVfV09SS0ZMT1dfUlVMRV9XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICcyMDAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5Xb3JrZmxvd19SdWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuV29ya2Zsb3dfUnVsZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlV29ya2Zsb3dSdWxlKFdvcmtmbG93UnVsZUlkLFJ1bGVEYXRhKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBXb3JrZmxvd1J1bGVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgUnVsZTogV29ya2Zsb3dSdWxlSWRcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBSdWxlRGF0YVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9XT1JLRkxPV19OUywgdGhpcy5SRU1PVkVfV09SS0ZMT1dfUlVMRVMsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IFxyXG5cclxuICAgIGRlbGV0ZVdvcmtmbG93UnVsZXMocnVsZUlkcyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgV29ya2Zsb3dSdWxlczoge1xyXG4gICAgICAgICAgICAgICAgICAgIFJ1bGU6IHJ1bGVJZHNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5ERUxFVEVfV09SS0ZMT1dfUlVMRVNfV1NfTUVUSE9EX05BTUUsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0V29ya2Zsb3dSdWxlc0J5UHJvamVjdChwcm9qZWN0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIHByb2plY3RJZDogcHJvamVjdElkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuV09SS0ZMT1dfUlVMRV9OUywgdGhpcy5HRVRfV09SS0ZMT1dfUlVMRVNfQllfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLldvcmtmbG93X1J1bGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5Xb3JrZmxvd19SdWxlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0V29ya2Zsb3dSdWxlc0J5VGFzayh0YXNrSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIHRhc2tJZDogdGFza0lkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuV09SS0ZMT1dfUlVMRV9OUywgdGhpcy5HRVRfV09SS0ZMT1dfUlVMRVNfQllfVEFTS19XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLldvcmtmbG93X1J1bGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5Xb3JrZmxvd19SdWxlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgVXBkYXRlRGF0ZU9uUnVsZUluaXRpYXRlKHByb2plY3RJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgcHJvamVjdElkOiBwcm9qZWN0SWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5VUERBVEVfREFURV9PTl9SVUxFX0lOSVRJQVRFX1dTX01FVEhPRF9OQU1FLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRyaWdnZXJXb3JrZmxvdyhwcm9qZWN0SWQsIGlzU3RhdHVzUnVsZXMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIFByb2plY3RJZDogcHJvamVjdElkLFxyXG4gICAgICAgICAgICAgICAgSXNTdGF0dXNSdWxlczogaXNTdGF0dXNSdWxlc1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfQlBNX05TLCB0aGlzLlRSSUdHRVJfVEFTS19CWV9SVUxFX1dTX01FVEhPRF9OQU1FLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFjdGlvblJ1bGVzQnlQcm9qZWN0SWQocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9qZWN0SWQ6IHByb2plY3RJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfV1NBUFBfTlMsIHRoaXMuR0VUX0FDVElPTl9SVUxFU19CWV9QUk9KRUNUX0lEX1dTX01FVEhPRF9OQU1FLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudHVwbGUgJiYgcmVzcG9uc2UudHVwbGUub2xkICYmIHJlc3BvbnNlLnR1cGxlLm9sZC5BY3Rpb25SdWxlcyAmJiByZXNwb25zZS50dXBsZS5vbGQuQWN0aW9uUnVsZXMuQWN0aW9uUnVsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBhY3Rpb25SdWxlUmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnR1cGxlLm9sZC5BY3Rpb25SdWxlcy5BY3Rpb25SdWxlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uUnVsZVJlc3BvbnNlID0gcmVzcG9uc2UudHVwbGUub2xkLkFjdGlvblJ1bGVzLkFjdGlvblJ1bGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uUnVsZVJlc3BvbnNlID0gW3Jlc3BvbnNlLnR1cGxlLm9sZC5BY3Rpb25SdWxlcy5BY3Rpb25SdWxlXVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGFjdGlvblJ1bGVSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdHJpZ2dlclJ1bGVPbkFjdGlvbih0YXNrSWQsIGFjdGlvbklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBUYXNrSWQ6IHRhc2tJZCxcclxuICAgICAgICAgICAgICAgIEFjdGlvbklkOiBhY3Rpb25JZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfQlBNX05TLCB0aGlzLlRSSUdHRVJfUlVMRV9PTl9BQ1RJT05fV1NfTUVUSE9EX05BTUUsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qZGVsZXRlVGFza0RlbGl2ZXJhYmxlKHR5cGU6IHN0cmluZywgSWQ6IHN0cmluZywgaXNDdXN0b21Xb3JrZmxvdzogYm9vbGVhbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICB0eXBlOiB0eXBlLFxyXG4gICAgICAgICAgICBpZDogSWQsXHJcbiAgICAgICAgICAgIGlzQ3VzdG9tV29ya2Zsb3c6IGlzQ3VzdG9tV29ya2Zsb3dcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuREVMRVRFX1dPUktGTE9XX05TLCB0aGlzLkRFTEVURV9XT1JLRkxPVywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0qL1xyXG5cclxuICAgIGZvcmNlRGVsZXRlVGFzayhJZDogc3RyaW5nLCBpc0N1c3RvbVdvcmtmbG93OiBib29sZWFuKSA6T2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIGlkOiBJZCxcclxuICAgICAgICAgICAgaXNDdXN0b21Xb3JrZmxvdzogaXNDdXN0b21Xb3JrZmxvdyxcclxuICAgICAgICAgICAgaXNGb3JjZVVwbG9hZERlbGl2ZXJhYmxlRGVsZXRlOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9XT1JLRkxPV19OUywgdGhpcy5GT1JDRV9ERUxFVEVfVEFTSywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzb2Z0RGVsZXRlVGFzayhJZDogc3RyaW5nLCBpc0N1c3RvbVdvcmtmbG93OiBib29sZWFuKSA6T2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIGlkOiBJZCxcclxuICAgICAgICAgICAgaXNDdXN0b21Xb3JrZmxvdzogaXNDdXN0b21Xb3JrZmxvd1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9XT1JLRkxPV19OUywgdGhpcy5TT0ZUX0RFTEVURV9UQVNLLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vICBNUE1WMy0yMDk0XHJcbiAgICBkZWxldGVEZWxpdmVyYWJsZShkZWxpdmVyYWJsZUlELCBmb3JjZURlbGV0ZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZUlEOiBkZWxpdmVyYWJsZUlELFxyXG4gICAgICAgICAgICBmb3JjZURlbGV0ZTogZm9yY2VEZWxldGVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuREVMRVRFX0RFTElWRVJBQkxFX05TLCB0aGlzLkRFTEVURV9ERUxJVkVSQUJMRSwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgZ2V0QWxsQXBwcm92YWxUYXNrcyh0YXNrSUQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgdGFza0lkczoge1xyXG4gICAgICAgICAgICAgICAgVGFza0RldGFpbHM6IHtcclxuICAgICAgICAgICAgICAgICAgICB0YXNrOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFRhc2tJZDogdGFza0lEXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfQVBQUk9WQUxfVEFTS1NfTlMsIHRoaXMuR0VUX0FMTF9BUFBST1ZBTF9UQVNLUywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgZGVsZXRlVGFzayh0YXNrSUQsIGZvcmNlRGVsZXRlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIHRhc2tJRDogdGFza0lELFxyXG4gICAgICAgICAgICBmb3JjZURlbGV0ZTogZm9yY2VEZWxldGVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuREVMRVRFX1RBU0tfTlMsIHRoaXMuREVMRVRFX1RBU0ssIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcblxyXG4gICAgLy8gTVBNX1YzLTIyMTdcclxuICAgIGFzc2lnbkRlbGl2ZXJhYmxlKGRlbGl2ZXJhYmxlSWQsIHVzZXJJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBEZWxpdmVyYWJsZUlkOiBkZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICBVc2VySWQ6IHVzZXJJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5BU1NJR05fREVMSVZFUkFCTEVfTlMsIHRoaXMuQVNTSUdOX0RFTElWRVJBQkxFLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=