import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { CategoryService } from './category.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { ProjectConstant } from '../../project-overview/project.constants';
import { FormControl, Validators, FormArray } from '@angular/forms';
import { DateValidators } from '../../../shared/validators/date.validator';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ProjectService } from './project.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import { DeliverableConstants } from '../../tasks/deliverable/deliverable.constants';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { MPM_ROLES } from '../../../mpm-utils/objects/Role';
import * as acronui from '../../../mpm-utils/auth/utility';
import { LoaderService } from '../../../loader/loader.service';
import { IndexerService } from '../../../shared/services/indexer/indexer.service';
import { SearchConfigConstants } from '../../../mpm-utils/objects/SearchConfigConstants';
import { ViewConfigService } from '../../../shared/services/view-config.service';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { MPMSearchOperators } from '../../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { newArray } from '@angular/compiler/src/util';
import { MPMSearchOperatorNames } from '../../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "./category.service";
import * as i3 from "../../../mpm-utils/services/otmm.service";
import * as i4 from "../../../mpm-utils/services/util.service";
import * as i5 from "./project.service";
import * as i6 from "../../../shared/services/otmm-metadata.service";
import * as i7 from "../../../mpm-utils/services/sharing.service";
import * as i8 from "../../../loader/loader.service";
import * as i9 from "../../../shared/services/indexer/indexer.service";
import * as i10 from "../../../shared/services/view-config.service";
import * as i11 from "../../../shared/services/field-config.service";
let ProjectFromTemplateService = class ProjectFromTemplateService {
    constructor(appService, categoryService, otmmService, utilService, projectService, otmmMetadataService, sharingService, loaderService, indexerService, viewConfigService, fieldConfigService) {
        this.appService = appService;
        this.categoryService = categoryService;
        this.otmmService = otmmService;
        this.utilService = utilService;
        this.projectService = projectService;
        this.otmmMetadataService = otmmMetadataService;
        this.sharingService = sharingService;
        this.loaderService = loaderService;
        this.indexerService = indexerService;
        this.viewConfigService = viewConfigService;
        this.fieldConfigService = fieldConfigService;
        this.PROJECT_NAME_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.GET_PROJECT_BY_NAME_WS_METHOD_NAME = 'GetAllProjectsByNameType';
        this.PROJECT_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_PROJECT_FROM_TEMPLATE_METHOD_NAME = 'CreateProjectFromTemplate';
        //TEMPLATE_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.TEMPLATE_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_TEMPLATE_FROM_TEMPLATE_METHOD_NAME = 'CreateTemplateFromSource';
        this.TEAM_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
        this.GET_TEAM_WS_METHOD_NAME = 'GetTeamsForUser';
        this.TEAM_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Teams/operations';
        this.GET_ALL_TEAMS_WS_METHOD_NAME = 'GetAllTeams';
        this.DELIVERABLE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable/operations';
        this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME = 'GetAllDeliverablesByProject';
        this.GET_TASK_BY_PROJECT_WS_METHOD_NAME = 'GetTaskByProjectID';
        this.TASK_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.metadataConstants = MPMFieldConstants;
    }
    getProperty(obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    }
    getUserDetailsByUserCn(userCn) {
        return new Observable(observer => {
            let userIdentyID = '';
            const parameters = {
                userCN: userCn
            };
            this.appService.getPersonDetailsByUserCN(parameters)
                .subscribe(response => {
                if (response && response.Person && response.Person.PersonToUser['Identity-id'] &&
                    response.Person.PersonToUser['Identity-id'].Id) {
                    userIdentyID = response.Person.PersonToUser['Identity-id'].Id;
                }
                observer.next(userIdentyID);
                observer.complete();
            });
        });
    }
    getProjectOwners(teamId) {
        const roleDetailObj = this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
        return new Observable(observer => {
            const getUserByRoleRequest = {
                allocationType: 'ROLE',
                roleDN: roleDetailObj.ROLE_DN,
                teamID: teamId,
                isApproveTask: false,
                isUploadTask: true
            };
            this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            });
        });
    }
    getTeams(isCampaignProject) {
        return new Observable(observer => {
            if (isCampaignProject) {
                this.appService.invokeRequest(this.TEAM_METHOD_NS, this.GET_ALL_TEAMS_WS_METHOD_NAME, null)
                    .subscribe(response => {
                    const teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    observer.error(eventData);
                });
            }
            else {
                const roleDetailObj = this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
                const param = {
                    roleDN: roleDetailObj.ROLE_DN
                };
                this.appService.invokeRequest(this.TEAM_BPM_METHOD_NS, this.GET_TEAM_WS_METHOD_NAME, param)
                    .subscribe(response => {
                    const teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    observer.error(eventData);
                });
            }
        });
    }
    getTeam(teamId) {
        return new Observable(observer => {
            const getRequestObject = {
                TeamId: teamId
            };
            this.appService.invokeRequest(this.TEAM_BPM_METHOD_NS, this.GET_TEAM_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response && response.Team) {
                    observer.next(response.Team);
                    observer.complete();
                }
            }, error => {
                observer.error(error);
                const eventData = {
                    message: 'Something went wrong while fetching Team',
                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                };
            });
        });
    }
    getTasks(projectId) {
        return new Observable(observer => {
            const getRequestObject = {
                projectID: projectId
            };
            this.appService.invokeRequest(this.TASK_DETAILS_METHOD_NS, this.GET_TASK_BY_PROJECT_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response && response.Task) {
                    if (!Array.isArray(response.Task)) {
                        response.Task = [response.Task];
                    }
                    observer.next(response.Task);
                    observer.complete();
                }
                observer.next(false);
                observer.complete();
            }, error => {
                observer.error(error);
                const eventData = { message: 'Something went wrong while fetching Tasks', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            });
        });
    }
    getDeliverables(projectId) {
        return new Observable(observer => {
            const getRequestObject = {
                ProjectID: projectId
            };
            this.appService.invokeRequest(this.DELIVERABLE_DETAILS_METHOD_NS, this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                if (response && response.Deliverable) {
                    if (!Array.isArray(response.Deliverable)) {
                        response.Deliverable = [response.Deliverable];
                    }
                    observer.next(response.Deliverable);
                    observer.complete();
                }
                observer.next(false);
                observer.complete();
            }, error => {
                observer.error(error);
                const eventData = { message: 'Something went wrong while fetching Deliverables', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            });
        });
    }
    getDeliverableByProjectFolderId(projectFolderId) {
        const searchConditionList = JSON.parse(JSON.stringify(DeliverableConstants.DELIVERABLE_SEARCH_CONDITION));
        const skip = 0;
        const top = 100;
        const otmmAllActiveTasksSearchConfig = {
            search_condition_list: searchConditionList,
            load_type: 'metadata',
            child_count_load_type: 'folders',
            level_of_detail: 'full',
            folder_filter: projectFolderId
        };
        return this.otmmService.searchCustomText(otmmAllActiveTasksSearchConfig, skip, top, otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString());
    }
    checkProjectName(projectSearchCondition, searchKeyword, viewConfigName) {
        return new Observable(observer => {
            let viewConfig;
            if (viewConfigName) {
                viewConfig = viewConfigName; //this.sharingService.getViewConfigById(viewConfigId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.PROJECT;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
                const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                const parameters = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: searchKeyword,
                    search_condition_list: {
                        search_condition: projectSearchCondition
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: []
                    },
                    cursor: {
                        page_index: 0,
                        page_size: 999
                    }
                };
                this.indexerService.search(parameters).subscribe((response) => {
                    if (response && response.data && response.data.length) {
                        let hasSameProjectName = false;
                        const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                        response.data.forEach(project => {
                            if (project[indexerField] === searchKeyword) {
                                hasSameProjectName = true;
                            }
                        });
                        if (hasSameProjectName) {
                            observer.next(true);
                        }
                        else {
                            observer.next(false);
                        }
                    }
                    else {
                        observer.next(false);
                    }
                    observer.complete();
                }, error => {
                    this.loaderService.hide();
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    }
    createProjectFromTemplate(CreateProjectFromSourceObject) {
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_BPM_METHOD_NS, this.CREATE_PROJECT_FROM_TEMPLATE_METHOD_NAME, CreateProjectFromSourceObject)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    createTemplateFromTemplate(CreateTemplateFromSourceObject) {
        return new Observable(observer => {
            this.appService.invokeRequest(this.TEMPLATE_BPM_METHOD_NS, this.CREATE_TEMPLATE_FROM_TEMPLATE_METHOD_NAME, CreateTemplateFromSourceObject)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getOwnerListByTeam(teamName, isCampaignProject) {
        return new Observable(observer => {
            let projectOwnerList = [];
            this.getTeams(isCampaignProject)
                .subscribe(teamList => {
                let hasTeam = false;
                teamList.map(team => {
                    if (team.NAME === teamName) {
                        hasTeam = true;
                        const teamId = team['MPM_Teams-id'].Id;
                        this.getProjectOwners(teamId)
                            .subscribe(response => {
                            if (response && response['users'] && response['users'].user) {
                                response['users'].user.map(user => {
                                    user.displayName = user.name;
                                    user.value = user.cn;
                                    user.id = user.cn;
                                });
                                projectOwnerList = response['users'].user;
                            }
                            observer.next(projectOwnerList);
                            observer.complete();
                        }, error => {
                            observer.next(projectOwnerList);
                            observer.complete();
                        });
                    }
                });
                if (!hasTeam) {
                    observer.next(projectOwnerList);
                    observer.complete();
                }
            }, error => {
                observer.next(projectOwnerList);
                observer.complete();
            });
        });
    }
    validateProjectTaskandDeliverable(templateObj, templateform, projectDependency, deliverableViewName) {
        if (templateObj) {
            let viewConfig;
            const templateProjectItemId = this.fieldConfigService.getFeildValueByMapperName(templateObj, this.metadataConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
            if (templateProjectItemId) {
                const templateProjectId = templateProjectItemId.split('.')[1];
                this.getTasks(templateProjectId)
                    .subscribe(response => {
                    const tasks = response;
                    if (!tasks || !tasks.length || tasks.length === 0) {
                        templateform.controls.copyTask.patchValue(false);
                        templateform.controls.copyDeliverable.patchValue(false);
                        templateform.controls.copyTask.disable();
                        templateform.controls.copyDeliverable.disable();
                        projectDependency.isNoTasks = true;
                        projectDependency.isNoDeliverables = true;
                        projectDependency.isTaskWithTaskWithDeliverableDependency = false;
                        return;
                    }
                    else {
                        templateform.controls.copyTask.patchValue(true);
                        templateform.controls.copyDeliverable.patchValue(true);
                        templateform.controls.copyTask.enable();
                        projectDependency.isNoTasks = false;
                        projectDependency.isNoDeliverables = false;
                        projectDependency.isTaskWithTaskWithDeliverableDependency = false;
                    }
                    if (deliverableViewName) {
                        viewConfig = deliverableViewName;
                    }
                    else {
                        viewConfig = SearchConfigConstants.SEARCH_NAME.DELIVERABLE;
                    }
                    this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
                        const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                            ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                        const searchConditionList = [];
                        const displayField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID, MPM_LEVELS.DELIVERABLE);
                        searchConditionList.push({
                            type: displayField ? displayField.DATA_TYPE : 'string',
                            field_id: displayField.INDEXER_FIELD_ID,
                            relational_operator_id: MPMSearchOperators.IS,
                            relational_operator_name: MPMSearchOperatorNames.IS,
                            value: templateProjectItemId
                        });
                        const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                        const parametersReq = {
                            search_config_id: searchConfig ? searchConfig : null,
                            keyword: '',
                            search_condition_list: {
                                search_condition: searchConditionList
                            },
                            facet_condition_list: {
                                facet_condition: []
                            },
                            sorting_list: {
                                sort: [{
                                        field_id: indexerField,
                                        order: 'ASC'
                                    }]
                            },
                            cursor: {
                                page_index: 0,
                                page_size: 2
                            }
                        };
                        this.indexerService.search(parametersReq).subscribe((responseData) => {
                            if (responseData.data.length > 0) {
                                templateform.controls.copyDeliverable.enable();
                            }
                            else {
                                templateform.controls.copyDeliverable.patchValue(false);
                                templateform.controls.copyDeliverable.disable();
                                projectDependency.isNoDeliverables = true;
                            }
                        });
                    });
                }, error => {
                });
            }
        }
        else {
            const eventData = { message: 'Invalid project information supplied to validate task and deliverables', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        }
    }
    copyTemplate(sourceId, templateform, templateSearchCondition, customMetadata, projectOwner, viewConfigName, enableDuplicateNames) {
        return new Observable(observer => {
            if (sourceId) {
                let ProjectObject;
                if (projectOwner) {
                    const userId = projectOwner.value;
                    ProjectObject = {
                        SourceID: sourceId.split('.')[1],
                        ProjectName: templateform.value.projectName,
                        ProjectOwner: userId,
                        ProjectDescription: templateform.value.description,
                        IsCopyTask: templateform.value.copyTask ? templateform.value.copyTask : false,
                        IsCopyDeliverable: templateform.value.copyDeliverable ? templateform.value.copyDeliverable : false,
                        CustomMetadata: customMetadata,
                        CampaignID: templateform.getRawValue().campaign.value,
                        CategoryMetadataID: templateform.getRawValue().categoryMetadata.value,
                        isCopyWorkflowRulesNeeded: templateform.controls.copyWorflowRules.value
                    };
                    this.checkProjectName(templateSearchCondition, ProjectObject.ProjectName, viewConfigName)
                        .subscribe(response => {
                        if (response && enableDuplicateNames === true) {
                            const eventData = {
                                message: 'Template already exists',
                                type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                            };
                            observer.next(eventData);
                            observer.complete();
                        }
                        else {
                            const requestObject = {
                                CreateProjectFromSourceObject: ProjectObject
                            };
                            let eventData = {
                                message: 'Create Template has been initiated sucessfully',
                                type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS
                            };
                            this.createTemplateFromTemplate(ProjectObject)
                                .subscribe(createProjectResponse => {
                                if (createProjectResponse && createProjectResponse['APIResponse']) {
                                    if (createProjectResponse['APIResponse'].statusCode === '500') {
                                        eventData = {
                                            message: createProjectResponse['APIResponse'].error.errorMessage,
                                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                        };
                                        observer.next(eventData);
                                        observer.complete();
                                    }
                                    else if (createProjectResponse['APIResponse'].statusCode === '200') {
                                        observer.next(eventData);
                                        observer.complete();
                                    }
                                }
                            }, error => {
                                const eventData = {
                                    message: 'Something went wrong while copying template',
                                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                };
                                observer.next(eventData);
                                observer.complete();
                            });
                        }
                    });
                    // });
                }
            }
        });
    }
    createProject(sourceId, templateform, projectSearchCondition, customMetadata, projectOwner, viewConfigName, enableDuplicateNames) {
        return new Observable(observer => {
            if (sourceId) {
                let ProjectObject;
                if (projectOwner) {
                    const userId = projectOwner.value;
                    // templateform.value.projectOwner ? templateform.value.projectOwner.cn : 
                    // this.getUserDetailsByUserCn(ownerCn)
                    //     .subscribe(userId => {
                    ProjectObject = {
                        TemplateID: sourceId.split('.')[1],
                        ProjectName: templateform.value.projectName,
                        ProjectOwner: userId,
                        ProjectDescription: templateform.value.description,
                        ExpectedProjectDuration: templateform.value.expectedDuration,
                        ProjectStartDate: templateform.value.projectStartDate,
                        ProjectDueDate: templateform.value.projectEndDate,
                        TaskStartDate: templateform.value.taskStartDate,
                        TaskDueDate: templateform.controls.taskEndDate.value,
                        IsCopyTask: templateform.value.copyTask || false,
                        IsCopyDeliverable: templateform.value.copyDeliverable || false,
                        CustomMetadata: customMetadata,
                        CampaignID: templateform.getRawValue().campaign ?
                            (templateform.getRawValue().campaign.value ? templateform.getRawValue().campaign.value : templateform.getRawValue().campaign.split(/-(.+)/)[0]) : '',
                        CategoryMetadataID: templateform.getRawValue().categoryMetadata ?
                            (templateform.getRawValue().categoryMetadata.value ? templateform.getRawValue().categoryMetadata.value : templateform.getRawValue().categoryMetadata) : ''
                    };
                    this.checkProjectName(projectSearchCondition, ProjectObject.ProjectName, viewConfigName)
                        .subscribe(response => {
                        if (response && enableDuplicateNames == 'false') {
                            const eventData = {
                                message: 'Project already exists',
                                type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                            };
                            observer.next(eventData);
                            observer.complete();
                        }
                        else {
                            const requestObject = {
                                CreateProjectFromSourceObject: ProjectObject
                            };
                            let eventData = {
                                message: 'Create Project has been initiated sucessfully',
                                type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS
                            };
                            this.createProjectFromTemplate(ProjectObject)
                                .subscribe(createProjectResponse => {
                                if (createProjectResponse && createProjectResponse['APIResponse']) {
                                    if (createProjectResponse['APIResponse'].statusCode === '500') {
                                        eventData = {
                                            message: createProjectResponse['APIResponse'].error.errorMessage,
                                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                        };
                                        observer.next(eventData);
                                        observer.complete();
                                    }
                                    else if (createProjectResponse['APIResponse'].statusCode === '200') {
                                        observer.next(eventData);
                                        observer.complete();
                                    }
                                }
                            }, error => {
                                const eventData = {
                                    message: 'Something went wrong while copying project',
                                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                };
                                observer.next(eventData);
                                observer.complete();
                            });
                        }
                    });
                    // });
                }
            }
        });
    }
    putCustomMetadata(customFieldsGroup, templateObj) {
        const costomMetadataValues = [];
        if (customFieldsGroup && customFieldsGroup['controls'] && customFieldsGroup['controls']['fieldset']
            && customFieldsGroup['controls']['fieldset'].controls
            && customFieldsGroup['controls']['fieldset'].controls.length > 0) {
            for (const control of customFieldsGroup['controls']['fieldset'].controls) {
                if (this.getProperty(templateObj, control.fieldset.id)) {
                    control.value = this.getProperty(templateObj, control.fieldset.id);
                }
            }
        }
        return customFieldsGroup;
    }
    getCustomMetadata(customFieldsGroup) {
        var _a, _b;
        const costomMetadataValues = [];
        if (customFieldsGroup && customFieldsGroup['controls'] && customFieldsGroup['controls']['fieldset']
            && customFieldsGroup['controls']['fieldset'].controls
            && customFieldsGroup['controls']['fieldset'].controls.length > 0) {
            const controls = customFieldsGroup['controls']['fieldset'].controls;
            for (const control of controls) {
                if ((((_a = control) === null || _a === void 0 ? void 0 : _a.value) !== null && ((_b = control) === null || _b === void 0 ? void 0 : _b.value) !== '')) {
                    const metadataFieldObj = {};
                    metadataFieldObj['dataType'] = this.otmmMetadataService.getMetadataFieldType(control.fieldset.data_type);
                    /*metadataFieldObj['defaultValue'] = control.fieldset.data_type === ProjectConstant.METADATA_EDIT_TYPES.DATE ?
                        this.utilService.convertToOTMMDateFormat(control.value) : control.value;*/
                    metadataFieldObj['defaultValue'] = control.value;
                    metadataFieldObj['fieldId'] = control.fieldset.id;
                    metadataFieldObj['fieldType'] = control.fieldset.type;
                    metadataFieldObj['referenceValue'] = 'defaultReference';
                    metadataFieldObj['domainFieldValue'] = null;
                    if (control.fieldset && control.fieldset.edit_type && control.fieldset.values
                        && Array.isArray(control.fieldset.values) && control.fieldset.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
                        control.fieldset.values.forEach(data => {
                            if (data.field_value && data.field_value.value && data.field_value.value === metadataFieldObj['defaultValue']) {
                                metadataFieldObj['domainFieldValue'] = data.display_value;
                            }
                        });
                    }
                    costomMetadataValues.push(metadataFieldObj);
                }
            }
        }
        return { Metadata: { metadataFields: costomMetadataValues } };
    }
    getMaxMinValueForCustomMetadata(dataLength, scale) {
        let data = Math.abs(dataLength - scale);
        let numericValue = newArray(data).fill('9').join('');
        if (scale) {
            numericValue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericValue) : parseInt(numericValue);
    }
    mapCustomMetadataForm(metadataField, templateObj) {
        const validators = [];
        const value = (templateObj) ?
            this.otmmMetadataService.getFieldValueById(templateObj.metadata, metadataField.id) : '';
        const formControlObj = new FormControl({
            value: value ? value : '',
            disabled: !metadataField.editable,
        });
        if (metadataField.required) {
            validators.push(Validators.required);
        }
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
            validators.push(DateValidators.dateFormat);
        }
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
            metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
            validators.push(Validators.maxLength(metadataField.data_length));
        }
        formControlObj.setValidators(validators);
        formControlObj.updateValueAndValidity();
        formControlObj['fieldset'] = metadataField;
        formControlObj['name'] = metadataField.name;
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
            metadataField.values = this.utilService.getLookupDomainValuesById(metadataField.domain_id);
        }
        return formControlObj;
    }
    getOTMMCustomMetaData(customMetadataObj, formGroup, templateObj, categoryLevelDetail) {
        return new Observable(observer => {
            let categoryLevelDetails = this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.PROJECT);
            if (categoryLevelDetail) {
                categoryLevelDetails = categoryLevelDetail;
            }
            this.loaderService.show();
            this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
                .subscribe(metaDataModelDetails => {
                customMetadataObj.fieldsetGroup = [];
                customMetadataObj.fieldGroups = [];
                customMetadataObj.customMetadataFields = [];
                if (metaDataModelDetails && metaDataModelDetails.metadata_element_list &&
                    Array.isArray(metaDataModelDetails.metadata_element_list)) {
                    for (const metadataFieldGroup of metaDataModelDetails.metadata_element_list) {
                        const formControlArray = [];
                        if (metadataFieldGroup.id === ProjectConstant.MPM_PROJECT_METADATA_GROUP) {
                            continue;
                        }
                        if (metadataFieldGroup.metadata_element_list &&
                            Array.isArray(metadataFieldGroup.metadata_element_list)) {
                            for (const metadataField of metadataFieldGroup.metadata_element_list) {
                                customMetadataObj.customMetadataFields.push(metadataField);
                                const validators = [];
                                let value = (templateObj) ?
                                    this.fieldConfigService.getIndexerIdByOTMMId(metadataField.id) : '';
                                value = templateObj[value] ? templateObj[value] : '';
                                const formControlObj = new FormControl({
                                    value: typeof value !== 'undefined' ? value : '',
                                    disabled: !metadataField.editable,
                                });
                                if (metadataField.required) {
                                    validators.push(Validators.required);
                                }
                                if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
                                    validators.push(DateValidators.dateFormat);
                                }
                                if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                                    metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
                                    validators.push(Validators.maxLength(metadataField.data_length));
                                }
                                if (metadataField.data_type === ProjectConstant.METADATA_EDIT_TYPES.NUMBER) {
                                    const numericValue = this.getMaxMinValueForCustomMetadata(metadataField.data_length, metadataField.scale);
                                    validators.push(Validators.max(numericValue), Validators.min(-(numericValue)));
                                }
                                formControlObj.setValidators(validators);
                                formControlObj.updateValueAndValidity();
                                formControlObj['fieldset'] = metadataField;
                                formControlObj['name'] = metadataField.name;
                                if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
                                    metadataField.values = this.utilService.getLookupDomainValuesById(metadataField.domain_id);
                                }
                                customMetadataObj.fieldsetGroup.push(formControlObj);
                                // new code
                                formControlArray.push(formControlObj);
                            }
                        }
                        const fieldGroup = formGroup.group({ fieldset: new FormArray(formControlArray) });
                        fieldGroup['name'] = metadataFieldGroup.name;
                        customMetadataObj.fieldGroups.push(fieldGroup);
                    }
                }
                this.loaderService.hide();
                observer.next(customMetadataObj);
                observer.complete();
            }, error => {
                this.loaderService.hide();
                observer.error(customMetadataObj);
            });
        });
    }
    checkTaskAccess(projectDependency, templateform) {
        if (!projectDependency.selectedObject && templateform.controls.copyTask.value) {
            templateform.controls.copyTask.patchValue(false);
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy task option. Please select a template.';
        }
        if (projectDependency.isNoTasks && templateform.controls.copyTask.value) {
            templateform.controls.copyTask.patchValue(false);
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy task option. No tasks are present.';
        }
        if (templateform.controls.copyTask.value && projectDependency.isTaskWithTaskWithDeliverableDependency) {
            templateform.controls.copyDeliverable.patchValue(true);
            return '';
        }
        if (!templateform.controls.copyTask.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return '';
        }
    }
    checkDeliverableAccess(projectDependency, templateform) {
        if (!projectDependency.selectedObject && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. Please select a template.';
        }
        if (projectDependency.isNoDeliverables && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. No Deliverables are present.';
        }
        if (!templateform.controls.copyTask.value && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. Enable copy task option to enable copy deliverble.';
        }
        if (!templateform.controls.copyDeliverable.value && projectDependency.isTaskWithTaskWithDeliverableDependency) {
            templateform.controls.copyDeliverable.patchValue(true);
            return 'Cannot disable copy deliverable option. Selected project contains Approval Tasks, which has deliverable dependencies.';
        }
        if (templateform.controls.copyDeliverable.value && !projectDependency.isNoDeliverables) {
            templateform.controls.copyTask.patchValue(true);
            return '';
        }
    }
    getAllTemplates(templateSearchCondition, viewConfigName) {
        return new Observable(observer => {
            let viewConfig;
            if (viewConfigName) {
                viewConfig = viewConfigName; //this.sharingService.getViewConfigById(viewConfigId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.PROJECT;
            }
            this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe((viewDetails) => {
                const searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                const indexerField = this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                const parametersReq = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: '',
                    search_condition_list: {
                        search_condition: templateSearchCondition
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: [{
                                field_id: indexerField,
                                order: 'ASC'
                            }]
                    },
                    cursor: {
                        page_index: 0,
                        page_size: 100
                    }
                };
                this.indexerService.search(parametersReq).subscribe((response) => {
                    const templateObj = {
                        allTemplateCount: response.cursor.total_records,
                        allTemplateList: response.data
                    };
                    observer.next(templateObj);
                    observer.complete();
                }, error => {
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    }
};
ProjectFromTemplateService.ctorParameters = () => [
    { type: AppService },
    { type: CategoryService },
    { type: OTMMService },
    { type: UtilService },
    { type: ProjectService },
    { type: OtmmMetadataService },
    { type: SharingService },
    { type: LoaderService },
    { type: IndexerService },
    { type: ViewConfigService },
    { type: FieldConfigService }
];
ProjectFromTemplateService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectFromTemplateService_Factory() { return new ProjectFromTemplateService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.CategoryService), i0.ɵɵinject(i3.OTMMService), i0.ɵɵinject(i4.UtilService), i0.ɵɵinject(i5.ProjectService), i0.ɵɵinject(i6.OtmmMetadataService), i0.ɵɵinject(i7.SharingService), i0.ɵɵinject(i8.LoaderService), i0.ɵɵinject(i9.IndexerService), i0.ɵɵinject(i10.ViewConfigService), i0.ɵɵinject(i11.FieldConfigService)); }, token: ProjectFromTemplateService, providedIn: "root" });
ProjectFromTemplateService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ProjectFromTemplateService);
export { ProjectFromTemplateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1mcm9tLXRlbXBsYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LWZyb20tdGVtcGxhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUV2RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDckYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRzdFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUM1RCxPQUFPLEtBQUssT0FBTyxNQUFNLGlDQUFpQyxDQUFDO0FBQzNELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUcvRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDbEYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFFekYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDakYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdFQUF3RSxDQUFDO0FBQzVHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQzs7Ozs7Ozs7Ozs7OztBQU9wSCxJQUFhLDBCQUEwQixHQUF2QyxNQUFhLDBCQUEwQjtJQXlCbkMsWUFDVyxVQUFzQixFQUN0QixlQUFnQyxFQUNoQyxXQUF3QixFQUN4QixXQUF3QixFQUN4QixjQUE4QixFQUM5QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsaUJBQW9DLEVBQ3BDLGtCQUFzQztRQVZ0QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWxDakQsMkJBQXNCLEdBQUcsa0RBQWtELENBQUM7UUFDNUUsdUNBQWtDLEdBQUcsMEJBQTBCLENBQUM7UUFFaEUsMEJBQXFCLEdBQUcsc0RBQXNELENBQUM7UUFDL0UsNkNBQXdDLEdBQUcsMkJBQTJCLENBQUM7UUFFdkUsa0ZBQWtGO1FBQ2xGLDJCQUFzQixHQUFHLHNEQUFzRCxDQUFDO1FBQ2hGLDhDQUF5QyxHQUFHLDBCQUEwQixDQUFDO1FBRXZFLHVCQUFrQixHQUFHLDhDQUE4QyxDQUFDO1FBQ3BFLDRCQUF1QixHQUFHLGlCQUFpQixDQUFDO1FBQzVDLG1CQUFjLEdBQUcsb0RBQW9ELENBQUM7UUFDdEUsaUNBQTRCLEdBQUcsYUFBYSxDQUFDO1FBRTdDLGtDQUE2QixHQUFHLHNEQUFzRCxDQUFDO1FBQ3ZGLCtDQUEwQyxHQUFHLDZCQUE2QixDQUFDO1FBRTNFLHVDQUFrQyxHQUFHLG9CQUFvQixDQUFDO1FBQzFELDJCQUFzQixHQUFHLCtDQUErQyxDQUFDO1FBRXpFLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDO0lBY2xDLENBQUM7SUFFTCxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUk7UUFDakIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7WUFDaEMsT0FBTztTQUNWO1FBQ0QsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELHNCQUFzQixDQUFDLE1BQU07UUFDekIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdEIsTUFBTSxVQUFVLEdBQUc7Z0JBQ2YsTUFBTSxFQUFFLE1BQU07YUFDakIsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDO2lCQUMvQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDO29CQUMxRSxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ2hELFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ2pFO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzVCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdCQUFnQixDQUFDLE1BQU07UUFDbkIsTUFBTSxhQUFhLEdBQVUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxvQkFBb0IsR0FBRztnQkFDekIsY0FBYyxFQUFFLE1BQU07Z0JBQ3RCLE1BQU0sRUFBRSxhQUFhLENBQUMsT0FBTztnQkFDN0IsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLFlBQVksRUFBRSxJQUFJO2FBQ3JCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxRQUFRLENBQUMsaUJBQWlCO1FBQ3RCLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxpQkFBaUIsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDO3FCQUN0RixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLE1BQU0sS0FBSyxHQUFnQixPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUM1SCxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQzthQUNWO2lCQUFNO2dCQUNILE1BQU0sYUFBYSxHQUFVLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEYsTUFBTSxLQUFLLEdBQUc7b0JBQ1YsTUFBTSxFQUFFLGFBQWEsQ0FBQyxPQUFPO2lCQUNoQyxDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsS0FBSyxDQUFDO3FCQUN0RixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLE1BQU0sS0FBSyxHQUFnQixPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUM1SCxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQzthQUNWO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsT0FBTyxDQUFDLE1BQU07UUFDVixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLE1BQU0sRUFBRSxNQUFNO2FBQ2pCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLGdCQUFnQixDQUFDO2lCQUNqRyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7b0JBQzNCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM3QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLE1BQU0sU0FBUyxHQUFHO29CQUNkLE9BQU8sRUFBRSwwQ0FBMEM7b0JBQ25ELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSztpQkFDbEQsQ0FBQztZQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsUUFBUSxDQUFDLFNBQVM7UUFDZCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLGdCQUFnQixDQUFDO2lCQUNoSCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDL0IsUUFBUSxDQUFDLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDbkM7b0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzdCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixNQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hJLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsZUFBZSxDQUFDLFNBQVM7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLGdCQUFnQixHQUFHO2dCQUNyQixTQUFTLEVBQUUsU0FBUzthQUN2QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFLElBQUksQ0FBQywwQ0FBMEMsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDL0gsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO29CQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7d0JBQ3RDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQ2pEO29CQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsa0RBQWtELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN2SSxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtCQUErQixDQUFDLGVBQWU7UUFDM0MsTUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO1FBRTFHLE1BQU0sSUFBSSxHQUFHLENBQUMsQ0FBQztRQUNmLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUVoQixNQUFNLDhCQUE4QixHQUFHO1lBQ25DLHFCQUFxQixFQUFFLG1CQUFtQjtZQUMxQyxTQUFTLEVBQUUsVUFBVTtZQUNyQixxQkFBcUIsRUFBRSxTQUFTO1lBQ2hDLGVBQWUsRUFBRSxNQUFNO1lBQ3ZCLGFBQWEsRUFBRSxlQUFlO1NBQ2pDLENBQUM7UUFHRixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsOEJBQThCLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFDOUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVELGdCQUFnQixDQUFDLHNCQUFzQixFQUFFLGFBQWEsRUFBRSxjQUFjO1FBQ2xFLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxVQUFVLENBQUM7WUFDZixJQUFJLGNBQWMsRUFBRTtnQkFDaEIsVUFBVSxHQUFHLGNBQWMsQ0FBQyxDQUFBLDJEQUEyRDthQUMxRjtpQkFBTTtnQkFDSCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQzthQUMxRDtZQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7Z0JBQ3JHLE1BQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQywyQkFBMkIsSUFBSSxPQUFPLFdBQVcsQ0FBQywyQkFBMkIsS0FBSyxRQUFRO29CQUN2SCxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNuRSxNQUFNLFVBQVUsR0FBa0I7b0JBQzlCLGdCQUFnQixFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUNwRCxPQUFPLEVBQUUsYUFBYTtvQkFDdEIscUJBQXFCLEVBQUU7d0JBQ25CLGdCQUFnQixFQUFFLHNCQUFzQjtxQkFDM0M7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ2xCLGVBQWUsRUFBRSxFQUFFO3FCQUN0QjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsSUFBSSxFQUFFLEVBQUU7cUJBQ1g7b0JBQ0QsTUFBTSxFQUFFO3dCQUNKLFVBQVUsRUFBRSxDQUFDO3dCQUNiLFNBQVMsRUFBRSxHQUFHO3FCQUNqQjtpQkFDSixDQUFDO2dCQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFFBQXdCLEVBQUUsRUFBRTtvQkFDMUUsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTt3QkFDbkQsSUFBSSxrQkFBa0IsR0FBRyxLQUFLLENBQUM7d0JBQy9CLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDMUgsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7NEJBQzVCLElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxLQUFLLGFBQWEsRUFBRTtnQ0FDekMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDOzZCQUM3Qjt3QkFDTCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLGtCQUFrQixFQUFFOzRCQUNwQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUN2Qjs2QkFBTTs0QkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUN4QjtxQkFDSjt5QkFBTTt3QkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN4QjtvQkFDRCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixRQUFRLENBQUMsS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7Z0JBQ25FLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5QkFBeUIsQ0FBQyw2QkFBNkI7UUFDbkQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLHdDQUF3QyxFQUFFLDZCQUE2QixDQUFDO2lCQUNsSSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDBCQUEwQixDQUFDLDhCQUE4QjtRQUNyRCxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMseUNBQXlDLEVBQUUsOEJBQThCLENBQUM7aUJBQ3JJLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsUUFBUSxFQUFFLGlCQUFpQjtRQUMxQyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUM7aUJBQzNCLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQixJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO3dCQUN4QixPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ3ZDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7NkJBQ3hCLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTs0QkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUU7Z0NBQ3pELFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO29DQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7b0NBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztvQ0FDckIsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO2dDQUN0QixDQUFDLENBQUMsQ0FBQztnQ0FDSCxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDOzZCQUM3Qzs0QkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7NEJBQ2hDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFOzRCQUNQLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs0QkFDaEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUN4QixDQUFDLENBQUMsQ0FBQztxQkFDVjtnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDaEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2hDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlDQUFpQyxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQUUsbUJBQW1CO1FBQy9GLElBQUksV0FBVyxFQUFFO1lBQ2IsSUFBSSxVQUFVLENBQUM7WUFDZixNQUFNLHFCQUFxQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3hKLElBQUkscUJBQXFCLEVBQUU7Z0JBQ3ZCLE1BQU0saUJBQWlCLEdBQUcscUJBQXFCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDO3FCQUMzQixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2xCLE1BQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7d0JBQy9DLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDakQsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUN4RCxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDekMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ2hELGlCQUFpQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7d0JBQ25DLGlCQUFpQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzt3QkFDMUMsaUJBQWlCLENBQUMsdUNBQXVDLEdBQUcsS0FBSyxDQUFDO3dCQUNsRSxPQUFPO3FCQUNWO3lCQUFNO3dCQUNILFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDaEQsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN2RCxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDeEMsaUJBQWlCLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFDcEMsaUJBQWlCLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO3dCQUMzQyxpQkFBaUIsQ0FBQyx1Q0FBdUMsR0FBRyxLQUFLLENBQUM7cUJBQ3JFO29CQUNELElBQUksbUJBQW1CLEVBQUU7d0JBQ3JCLFVBQVUsR0FBRyxtQkFBbUIsQ0FBQztxQkFDcEM7eUJBQU07d0JBQ0gsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7cUJBQzlEO29CQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7d0JBQ3JHLE1BQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQywyQkFBMkIsSUFBSSxPQUFPLFdBQVcsQ0FBQywyQkFBMkIsS0FBSyxRQUFROzRCQUN2SCxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUNuRSxNQUFNLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzt3QkFDL0IsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQ3BGLGlCQUFpQixDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixFQUFFLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDN0YsbUJBQW1CLENBQUMsSUFBSSxDQUFDOzRCQUNyQixJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFROzRCQUN0RCxRQUFRLEVBQUUsWUFBWSxDQUFDLGdCQUFnQjs0QkFDdkMsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTs0QkFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTs0QkFDbkQsS0FBSyxFQUFFLHFCQUFxQjt5QkFDL0IsQ0FBQyxDQUFDO3dCQUNILE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDMUgsTUFBTSxhQUFhLEdBQWtCOzRCQUNqQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTs0QkFDcEQsT0FBTyxFQUFFLEVBQUU7NEJBQ1gscUJBQXFCLEVBQUU7Z0NBQ25CLGdCQUFnQixFQUFFLG1CQUFtQjs2QkFDeEM7NEJBQ0Qsb0JBQW9CLEVBQUU7Z0NBQ2xCLGVBQWUsRUFBRSxFQUFFOzZCQUN0Qjs0QkFDRCxZQUFZLEVBQUU7Z0NBQ1YsSUFBSSxFQUFFLENBQUM7d0NBQ0gsUUFBUSxFQUFFLFlBQVk7d0NBQ3RCLEtBQUssRUFBRSxLQUFLO3FDQUNmLENBQUM7NkJBQ0w7NEJBQ0QsTUFBTSxFQUFFO2dDQUNKLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxDQUFDOzZCQUNmO3lCQUNKLENBQUM7d0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBNEIsRUFBRSxFQUFFOzRCQUNqRixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQ0FDOUIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUM7NkJBQ2xEO2lDQUFNO2dDQUNILFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDeEQsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7Z0NBQ2hELGlCQUFpQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs2QkFDN0M7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNYLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7U0FDSjthQUFNO1lBQ0gsTUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsd0VBQXdFLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUM1SjtJQUNMLENBQUM7SUFFRCxZQUFZLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxvQkFBb0I7UUFDNUgsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLGFBQWEsQ0FBQztnQkFDbEIsSUFBSSxZQUFZLEVBQUU7b0JBQ2QsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztvQkFDbEMsYUFBYSxHQUFHO3dCQUNaLFFBQVEsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDM0MsWUFBWSxFQUFFLE1BQU07d0JBQ3BCLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDbEQsVUFBVSxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFBLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSzt3QkFDNUUsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLO3dCQUNqRyxjQUFjLEVBQUUsY0FBYzt3QkFDOUIsVUFBVSxFQUFFLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSzt3QkFDckQsa0JBQWtCLEVBQUUsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEtBQUs7d0JBQ3JFLHlCQUF5QixFQUFFLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSztxQkFDMUUsQ0FBQztvQkFDRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQUUsYUFBYSxDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7eUJBQ3BGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbEIsSUFBSSxRQUFRLElBQUksb0JBQW9CLEtBQUssSUFBSSxFQUFFOzRCQUMzQyxNQUFNLFNBQVMsR0FBRztnQ0FDZCxPQUFPLEVBQUUseUJBQXlCO2dDQUNsQyxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7NkJBQ2xELENBQUM7NEJBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUN2Qjs2QkFBTTs0QkFDSCxNQUFNLGFBQWEsR0FBRztnQ0FDbEIsNkJBQTZCLEVBQUUsYUFBYTs2QkFDL0MsQ0FBQzs0QkFDRixJQUFJLFNBQVMsR0FBRztnQ0FDWixPQUFPLEVBQUUsZ0RBQWdEO2dDQUN6RCxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU87NkJBQ3BELENBQUM7NEJBQ0YsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGFBQWEsQ0FBQztpQ0FDekMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7Z0NBQy9CLElBQUkscUJBQXFCLElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLEVBQUU7b0NBQy9ELElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3Q0FDM0QsU0FBUyxHQUFHOzRDQUNSLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWTs0Q0FDaEUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO3lDQUNsRCxDQUFDO3dDQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0NBQ3pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQ0FDdkI7eUNBQU0sSUFBSSxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO3dDQUNsRSxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dDQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUNBQ3ZCO2lDQUNKOzRCQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQ0FDUCxNQUFNLFNBQVMsR0FBRztvQ0FDZCxPQUFPLEVBQUUsNkNBQTZDO29DQUN0RCxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7aUNBQ2xELENBQUM7Z0NBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQ0FDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUN4QixDQUFDLENBQUMsQ0FBQzt5QkFDVjtvQkFDTCxDQUFDLENBQUMsQ0FBQztvQkFDUCxNQUFNO2lCQUNUO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxhQUFhLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxzQkFBc0IsRUFBRSxjQUFjLEVBQUUsWUFBWSxFQUFFLGNBQWMsRUFBRSxvQkFBb0I7UUFDNUgsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLGFBQWEsQ0FBQztnQkFDbEIsSUFBSSxZQUFZLEVBQUU7b0JBQ2QsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztvQkFDbEMsMEVBQTBFO29CQUMxRSx1Q0FBdUM7b0JBQ3ZDLDZCQUE2QjtvQkFDN0IsYUFBYSxHQUFHO3dCQUNaLFVBQVUsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbEMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDM0MsWUFBWSxFQUFFLE1BQU07d0JBQ3BCLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDbEQsdUJBQXVCLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0I7d0JBQzVELGdCQUFnQixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCO3dCQUNyRCxjQUFjLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxjQUFjO3dCQUNqRCxhQUFhLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxhQUFhO3dCQUMvQyxXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSzt3QkFDcEQsVUFBVSxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUs7d0JBQ2hELGlCQUFpQixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLEtBQUs7d0JBQzlELGNBQWMsRUFBRSxjQUFjO3dCQUM5QixVQUFVLEVBQUUsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzRCQUM3QyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDeEosa0JBQWtCLEVBQUUsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLENBQUM7NEJBQzdELENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7cUJBQ2pLLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHNCQUFzQixFQUFFLGFBQWEsQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDO3lCQUNuRixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ2xCLElBQUksUUFBUSxJQUFJLG9CQUFvQixJQUFJLE9BQU8sRUFBRTs0QkFDN0MsTUFBTSxTQUFTLEdBQUc7Z0NBQ2QsT0FBTyxFQUFFLHdCQUF3QjtnQ0FDakMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLOzZCQUNsRCxDQUFDOzRCQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ3pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDdkI7NkJBQU07NEJBQ0gsTUFBTSxhQUFhLEdBQUc7Z0NBQ2xCLDZCQUE2QixFQUFFLGFBQWE7NkJBQy9DLENBQUM7NEJBQ0YsSUFBSSxTQUFTLEdBQUc7Z0NBQ1osT0FBTyxFQUFFLCtDQUErQztnQ0FDeEQsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPOzZCQUNwRCxDQUFDOzRCQUNGLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUM7aUNBQ3hDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO2dDQUMvQixJQUFJLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxFQUFFO29DQUMvRCxJQUFJLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7d0NBQzNELFNBQVMsR0FBRzs0Q0FDUixPQUFPLEVBQUUscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVk7NENBQ2hFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSzt5Q0FDbEQsQ0FBQzt3Q0FDRixRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dDQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUNBQ3ZCO3lDQUFNLElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3Q0FDbEUsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3Q0FDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FDQUN2QjtpQ0FDSjs0QkFDTCxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0NBQ1AsTUFBTSxTQUFTLEdBQUc7b0NBQ2QsT0FBTyxFQUFFLDRDQUE0QztvQ0FDckQsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO2lDQUNsRCxDQUFDO2dDQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0NBQ3pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs0QkFDeEIsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsTUFBTTtpQkFDVDthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsaUJBQWlCLEVBQUUsV0FBVztRQUM1QyxNQUFNLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxJQUFJLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQztlQUM1RixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRO2VBQ2xELGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2xFLEtBQUssTUFBTSxPQUFPLElBQUksaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxFQUFFO2dCQUN0RSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUU7b0JBQ3BELE9BQU8sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDdEU7YUFDSjtTQUNKO1FBQ0QsT0FBTyxpQkFBaUIsQ0FBQztJQUM3QixDQUFDO0lBQ0QsaUJBQWlCLENBQUMsaUJBQWlCOztRQUMvQixNQUFNLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxJQUFJLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQztlQUM1RixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRO2VBQ2xELGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2xFLE1BQU0sUUFBUSxHQUFlLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUNoRixLQUFLLE1BQU0sT0FBTyxJQUFJLFFBQVEsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLE9BQUEsT0FBTywwQ0FBRSxLQUFLLE1BQUssSUFBSSxJQUFJLE9BQUEsT0FBTywwQ0FBRSxLQUFLLE1BQUssRUFBRSxDQUFDLEVBQUU7b0JBQ3BELE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO29CQUM1QixnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekc7a0dBQzhFO29CQUM5RSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO29CQUNqRCxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztvQkFDbEQsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ3RELGdCQUFnQixDQUFDLGdCQUFnQixDQUFDLEdBQUcsa0JBQWtCLENBQUM7b0JBQ3hELGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUM1QyxJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNOzJCQUN0RSxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRTt3QkFDdkgsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUNuQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssZ0JBQWdCLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0NBQzNHLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQzs2QkFDN0Q7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0Qsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7aUJBQy9DO2FBQ0o7U0FDSjtRQUNELE9BQU8sRUFBRSxRQUFRLEVBQUUsRUFBRSxjQUFjLEVBQUUsb0JBQW9CLEVBQUUsRUFBRSxDQUFDO0lBQ2xFLENBQUM7SUFFRCwrQkFBK0IsQ0FBQyxVQUFVLEVBQUUsS0FBSztRQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsQ0FBQztRQUN4QyxJQUFJLFlBQVksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNyRCxJQUFJLEtBQUssRUFBRTtZQUNQLFlBQVksSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUQ7UUFDRCxrQ0FBa0M7UUFDbEMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRCxxQkFBcUIsQ0FBQyxhQUFhLEVBQUUsV0FBVztRQUM1QyxNQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdEIsTUFBTSxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRTVGLE1BQU0sY0FBYyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQ25DLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN6QixRQUFRLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUTtTQUNwQyxDQUFDLENBQUM7UUFFSCxJQUFJLGFBQWEsQ0FBQyxRQUFRLEVBQUU7WUFDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDeEM7UUFFRCxJQUFJLGFBQWEsQ0FBQyxTQUFTLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRTtZQUN0RSxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM5QztRQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsTUFBTTtZQUN0RSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUU7WUFDMUUsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQ3BFO1FBRUQsY0FBYyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN6QyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUV4QyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsYUFBYSxDQUFDO1FBQzNDLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO1FBRTVDLElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFO1lBQ3ZFLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUY7UUFDRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBQ0QscUJBQXFCLENBQUMsaUJBQW9DLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxtQkFBbUI7UUFDbkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLG9CQUFvQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsNkJBQTZCLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xHLElBQUksbUJBQW1CLEVBQUU7Z0JBQ3JCLG9CQUFvQixHQUFHLG1CQUFtQixDQUFDO2FBQzlDO1lBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixDQUFDO2lCQUN2RSxTQUFTLENBQUMsb0JBQW9CLENBQUMsRUFBRTtnQkFDOUIsaUJBQWlCLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztnQkFDckMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDbkMsaUJBQWlCLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxDQUFDO2dCQUM1QyxJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLHFCQUFxQjtvQkFDbEUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO29CQUMzRCxLQUFLLE1BQU0sa0JBQWtCLElBQUksb0JBQW9CLENBQUMscUJBQXFCLEVBQUU7d0JBQ3pFLE1BQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO3dCQUM1QixJQUFJLGtCQUFrQixDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUMsMEJBQTBCLEVBQUU7NEJBQ3RFLFNBQVM7eUJBQ1o7d0JBQ0QsSUFBSSxrQkFBa0IsQ0FBQyxxQkFBcUI7NEJBQ3hDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsRUFBRTs0QkFDekQsS0FBSyxNQUFNLGFBQWEsSUFBSSxrQkFBa0IsQ0FBQyxxQkFBcUIsRUFBRTtnQ0FDbEUsaUJBQWlCLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dDQUUzRCxNQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0NBQ3RCLElBQUksS0FBSyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQ0FDdkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dDQUN4RSxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQ0FDckQsTUFBTSxjQUFjLEdBQUcsSUFBSSxXQUFXLENBQUM7b0NBQ25DLEtBQUssRUFBRSxPQUFPLEtBQUssS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtvQ0FDaEQsUUFBUSxFQUFFLENBQUMsYUFBYSxDQUFDLFFBQVE7aUNBQ3BDLENBQUMsQ0FBQztnQ0FFSCxJQUFJLGFBQWEsQ0FBQyxRQUFRLEVBQUU7b0NBQ3hCLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lDQUN4QztnQ0FFRCxJQUFJLGFBQWEsQ0FBQyxTQUFTLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRTtvQ0FDdEUsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7aUNBQzlDO2dDQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsTUFBTTtvQ0FDdEUsYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFO29DQUMxRSxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7aUNBQ3BFO2dDQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFO29DQUN4RSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsK0JBQStCLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7b0NBQzFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7aUNBQ2xGO2dDQUVELGNBQWMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0NBQ3pDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dDQUV4QyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsYUFBYSxDQUFDO2dDQUMzQyxjQUFjLENBQUMsTUFBTSxDQUFDLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQztnQ0FFNUMsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUU7b0NBQ3ZFLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7aUNBQzlGO2dDQUNELGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0NBQ3JELFdBQVc7Z0NBQ1gsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzZCQUN6Qzt5QkFDSjt3QkFDRCxNQUFNLFVBQVUsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksU0FBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNsRixVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDO3dCQUM3QyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUNsRDtpQkFDSjtnQkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ2pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLGlCQUFpQixFQUFFLFlBQVk7UUFDM0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7WUFDM0UsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pELFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RCxPQUFPLDJEQUEyRCxDQUFDO1NBQ3RFO1FBQ0QsSUFBSSxpQkFBaUIsQ0FBQyxTQUFTLElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQ3JFLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNqRCxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEQsT0FBTyx1REFBdUQsQ0FBQztTQUNsRTtRQUNELElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLGlCQUFpQixDQUFDLHVDQUF1QyxFQUFFO1lBQ25HLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2RCxPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUN2QyxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEQsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFDRCxzQkFBc0IsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZO1FBQ2xELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFO1lBQ2xGLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RCxPQUFPLGtFQUFrRSxDQUFDO1NBQzdFO1FBQ0QsSUFBSSxpQkFBaUIsQ0FBQyxnQkFBZ0IsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUU7WUFDbkYsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hELE9BQU8scUVBQXFFLENBQUM7U0FDaEY7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRTtZQUN0RixZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEQsT0FBTywyRkFBMkYsQ0FBQztTQUN0RztRQUVELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLElBQUksaUJBQWlCLENBQUMsdUNBQXVDLEVBQUU7WUFDM0csWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sdUhBQXVILENBQUM7U0FDbEk7UUFDRCxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFO1lBQ3BGLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoRCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0wsQ0FBQztJQUVELGVBQWUsQ0FBQyx1QkFBdUIsRUFBRSxjQUFjO1FBQ25ELE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxVQUFVLENBQUM7WUFDZixJQUFJLGNBQWMsRUFBRTtnQkFDaEIsVUFBVSxHQUFHLGNBQWMsQ0FBQyxDQUFBLDJEQUEyRDthQUMxRjtpQkFBTTtnQkFDSCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQTthQUN6RDtZQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUF1QixFQUFFLEVBQUU7Z0JBQ3JHLE1BQU0sWUFBWSxHQUFHLFdBQVcsQ0FBQywyQkFBMkIsSUFBSSxPQUFPLFdBQVcsQ0FBQywyQkFBMkIsS0FBSyxRQUFRO29CQUN2SCxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNuRSxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQzFILE1BQU0sYUFBYSxHQUFrQjtvQkFDakMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQ3BELE9BQU8sRUFBRSxFQUFFO29CQUNYLHFCQUFxQixFQUFFO3dCQUNuQixnQkFBZ0IsRUFBRSx1QkFBdUI7cUJBQzVDO29CQUNELG9CQUFvQixFQUFFO3dCQUNsQixlQUFlLEVBQUUsRUFBRTtxQkFDdEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLElBQUksRUFBRSxDQUFDO2dDQUNILFFBQVEsRUFBRSxZQUFZO2dDQUN0QixLQUFLLEVBQUUsS0FBSzs2QkFDZixDQUFDO3FCQUNMO29CQUNELE1BQU0sRUFBRTt3QkFDSixVQUFVLEVBQUUsQ0FBQzt3QkFDYixTQUFTLEVBQUUsR0FBRztxQkFDakI7aUJBQ0osQ0FBQztnQkFDRixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUF3QixFQUFFLEVBQUU7b0JBQzdFLE1BQU0sV0FBVyxHQUFHO3dCQUNoQixnQkFBZ0IsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWE7d0JBQy9DLGVBQWUsRUFBRSxRQUFRLENBQUMsSUFBSTtxQkFDakMsQ0FBQztvQkFDRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBRXhCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7Z0JBQ25FLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSixDQUFBOztZQXB3QjBCLFVBQVU7WUFDTCxlQUFlO1lBQ25CLFdBQVc7WUFDWCxXQUFXO1lBQ1IsY0FBYztZQUNULG1CQUFtQjtZQUN4QixjQUFjO1lBQ2YsYUFBYTtZQUNaLGNBQWM7WUFDWCxpQkFBaUI7WUFDaEIsa0JBQWtCOzs7QUFwQ3hDLDBCQUEwQjtJQUp0QyxVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDO0dBRVcsMEJBQTBCLENBOHhCdEM7U0E5eEJZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi9jYXRlZ29yeS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sLCBWYWxpZGF0b3JzLCBGb3JtQXJyYXkgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERhdGVWYWxpZGF0b3JzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3ZhbGlkYXRvcnMvZGF0ZS52YWxpZGF0b3InO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDdXN0b21NZXRhZGF0YU9iaiB9IGZyb20gJy4uLy4uL3Byb2plY3QtZnJvbS10ZW1wbGF0ZXMvcHJvamVjdC5mcm9tLnRlbXBsYXRlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVDb25zdGFudHMgfSBmcm9tICcuLi8uLi90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUZWFtIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVGVhbSc7XHJcbmltcG9ydCB7IFJvbGVzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUm9sZXMnO1xyXG5pbXBvcnQgeyBNUE1fUk9MRVMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9Sb2xlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaFJlcXVlc3QgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlcXVlc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBJbmRleGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2luZGV4ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1NlYXJjaENvbmZpZ0NvbnN0YW50cyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWcgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9WaWV3Q29uZmlnJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvdmlldy1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IG5ld0FycmF5IH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXIvc3JjL3V0aWwnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvck5hbWVzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yTmFtZS5jb25zdGFudHMnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlIHtcclxuXHJcbiAgICBQUk9KRUNUX05BTUVfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL1Byb2plY3Qvb3BlcmF0aW9ucyc7XHJcbiAgICBHRVRfUFJPSkVDVF9CWV9OQU1FX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbFByb2plY3RzQnlOYW1lVHlwZSc7XHJcblxyXG4gICAgUFJPSkVDVF9CUE1fTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgQ1JFQVRFX1BST0pFQ1RfRlJPTV9URU1QTEFURV9NRVRIT0RfTkFNRSA9ICdDcmVhdGVQcm9qZWN0RnJvbVRlbXBsYXRlJztcclxuXHJcbiAgICAvL1RFTVBMQVRFX0JQTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcbiAgICBURU1QTEFURV9CUE1fTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgQ1JFQVRFX1RFTVBMQVRFX0ZST01fVEVNUExBVEVfTUVUSE9EX05BTUUgPSAnQ3JlYXRlVGVtcGxhdGVGcm9tU291cmNlJztcclxuXHJcbiAgICBURUFNX0JQTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3RlYW1zL2JwbS8xLjAnO1xyXG4gICAgR0VUX1RFQU1fV1NfTUVUSE9EX05BTUUgPSAnR2V0VGVhbXNGb3JVc2VyJztcclxuICAgIFRFQU1fTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9UZWFtcy9vcGVyYXRpb25zJztcclxuICAgIEdFVF9BTExfVEVBTVNfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsVGVhbXMnO1xyXG5cclxuICAgIERFTElWRVJBQkxFX0RFVEFJTFNfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0RlbGl2ZXJhYmxlL29wZXJhdGlvbnMnO1xyXG4gICAgR0VUX0RFTElWRVJBQkxFU19CWV9QUk9KRUNUX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbERlbGl2ZXJhYmxlc0J5UHJvamVjdCc7XHJcblxyXG4gICAgR0VUX1RBU0tfQllfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSA9ICdHZXRUYXNrQnlQcm9qZWN0SUQnO1xyXG4gICAgVEFTS19ERVRBSUxTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9UYXNrL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIG1ldGFkYXRhQ29uc3RhbnRzID0gTVBNRmllbGRDb25zdGFudHM7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNhdGVnb3J5U2VydmljZTogQ2F0ZWdvcnlTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGluZGV4ZXJTZXJ2aWNlOiBJbmRleGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICkgeyB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHkob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgaWYgKCFvYmogfHwgIXBhdGggfHwgIW9iai5tZXRhZGF0YSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5SWQob2JqLm1ldGFkYXRhLCBwYXRoLCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VyRGV0YWlsc0J5VXNlckNuKHVzZXJDbikge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGxldCB1c2VySWRlbnR5SUQgPSAnJztcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdXNlckNuXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQZXJzb25EZXRhaWxzQnlVc2VyQ04ocGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5QZXJzb24gJiYgcmVzcG9uc2UuUGVyc29uLlBlcnNvblRvVXNlclsnSWRlbnRpdHktaWQnXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5QZXJzb24uUGVyc29uVG9Vc2VyWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZGVudHlJRCA9IHJlc3BvbnNlLlBlcnNvbi5QZXJzb25Ub1VzZXJbJ0lkZW50aXR5LWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlcklkZW50eUlEKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdE93bmVycyh0ZWFtSWQpIHtcclxuICAgICAgICBjb25zdCByb2xlRGV0YWlsT2JqOiBSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Um9sZUJ5TmFtZShNUE1fUk9MRVMuTUFOQUdFUik7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogJ1JPTEUnLFxyXG4gICAgICAgICAgICAgICAgcm9sZUROOiByb2xlRGV0YWlsT2JqLlJPTEVfRE4sXHJcbiAgICAgICAgICAgICAgICB0ZWFtSUQ6IHRlYW1JZCxcclxuICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0VXNlckJ5Um9sZVJlcXVlc3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGVhbXMoaXNDYW1wYWlnblByb2plY3QpOiBPYnNlcnZhYmxlPEFycmF5PFRlYW0+PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKGlzQ2FtcGFpZ25Qcm9qZWN0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1fTUVUSE9EX05TLCB0aGlzLkdFVF9BTExfVEVBTVNfV1NfTUVUSE9EX05BTUUsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRlYW1zOiBBcnJheTxUZWFtPiA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fVGVhbXMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0ZWFtcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUZWFtcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb2xlRGV0YWlsT2JqOiBSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Um9sZUJ5TmFtZShNUE1fUk9MRVMuTUFOQUdFUik7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46IHJvbGVEZXRhaWxPYmouUk9MRV9ETlxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTV9CUE1fTUVUSE9EX05TLCB0aGlzLkdFVF9URUFNX1dTX01FVEhPRF9OQU1FLCBwYXJhbSlcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVhbXM6IEFycmF5PFRlYW0+ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9UZWFtcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRlYW1zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRlYW1zJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZ2V0VGVhbSh0ZWFtSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgICAgICBUZWFtSWQ6IHRlYW1JZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1fQlBNX01FVEhPRF9OUywgdGhpcy5HRVRfVEVBTV9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5UZWFtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuVGVhbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUZWFtJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza3MocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgcHJvamVjdElEOiBwcm9qZWN0SWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0RFVEFJTFNfTUVUSE9EX05TLCB0aGlzLkdFVF9UQVNLX0JZX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuVGFzaykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuVGFzaykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLlRhc2sgPSBbcmVzcG9uc2UuVGFza107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5UYXNrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgVGFza3MnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBnZXREZWxpdmVyYWJsZXMocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElEOiBwcm9qZWN0SWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxJVkVSQUJMRV9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfREVMSVZFUkFCTEVTX0JZX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuRGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLkRlbGl2ZXJhYmxlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuRGVsaXZlcmFibGUgPSBbcmVzcG9uc2UuRGVsaXZlcmFibGVdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuRGVsaXZlcmFibGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBEZWxpdmVyYWJsZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVsaXZlcmFibGVCeVByb2plY3RGb2xkZXJJZChwcm9qZWN0Rm9sZGVySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaENvbmRpdGlvbkxpc3QgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KERlbGl2ZXJhYmxlQ29uc3RhbnRzLkRFTElWRVJBQkxFX1NFQVJDSF9DT05ESVRJT04pKTtcclxuXHJcbiAgICAgICAgY29uc3Qgc2tpcCA9IDA7XHJcbiAgICAgICAgY29uc3QgdG9wID0gMTAwO1xyXG5cclxuICAgICAgICBjb25zdCBvdG1tQWxsQWN0aXZlVGFza3NTZWFyY2hDb25maWcgPSB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDogc2VhcmNoQ29uZGl0aW9uTGlzdCxcclxuICAgICAgICAgICAgbG9hZF90eXBlOiAnbWV0YWRhdGEnLFxyXG4gICAgICAgICAgICBjaGlsZF9jb3VudF9sb2FkX3R5cGU6ICdmb2xkZXJzJyxcclxuICAgICAgICAgICAgbGV2ZWxfb2ZfZGV0YWlsOiAnZnVsbCcsXHJcbiAgICAgICAgICAgIGZvbGRlcl9maWx0ZXI6IHByb2plY3RGb2xkZXJJZFxyXG4gICAgICAgIH07XHJcblxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5vdG1tU2VydmljZS5zZWFyY2hDdXN0b21UZXh0KG90bW1BbGxBY3RpdmVUYXNrc1NlYXJjaENvbmZpZywgc2tpcCwgdG9wLFxyXG4gICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy51c2VyU2Vzc2lvbklkLnRvU3RyaW5nKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrUHJvamVjdE5hbWUocHJvamVjdFNlYXJjaENvbmRpdGlvbiwgc2VhcmNoS2V5d29yZCwgdmlld0NvbmZpZ05hbWUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgICAgICAgICBpZiAodmlld0NvbmZpZ05hbWUpIHtcclxuICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSB2aWV3Q29uZmlnTmFtZTsvL3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Vmlld0NvbmZpZ0J5SWQodmlld0NvbmZpZ0lkKS5WSUVXO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5QUk9KRUNUO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgJiYgdHlwZW9mIHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyA9PT0gJ3N0cmluZydcclxuICAgICAgICAgICAgICAgICAgICA/IHBhcnNlSW50KHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnM6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogc2VhcmNoQ29uZmlnID8gc2VhcmNoQ29uZmlnIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBrZXl3b3JkOiBzZWFyY2hLZXl3b3JkLFxyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBwcm9qZWN0U2VhcmNoQ29uZGl0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydDogW11cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlX2luZGV4OiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlX3NpemU6IDk5OVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5kYXRhICYmIHJlc3BvbnNlLmRhdGEubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBoYXNTYW1lUHJvamVjdE5hbWUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaW5kZXhlckZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlcklkQnlNYXBwZXJWYWx1ZShNUE1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9OQU1FKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuZGF0YS5mb3JFYWNoKHByb2plY3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHByb2plY3RbaW5kZXhlckZpZWxkXSA9PT0gc2VhcmNoS2V5d29yZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc1NhbWVQcm9qZWN0TmFtZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaGFzU2FtZVByb2plY3ROYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIHByb2plY3RzLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVQcm9qZWN0RnJvbVRlbXBsYXRlKENyZWF0ZVByb2plY3RGcm9tU291cmNlT2JqZWN0KSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0JQTV9NRVRIT0RfTlMsIHRoaXMuQ1JFQVRFX1BST0pFQ1RfRlJPTV9URU1QTEFURV9NRVRIT0RfTkFNRSwgQ3JlYXRlUHJvamVjdEZyb21Tb3VyY2VPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVRlbXBsYXRlRnJvbVRlbXBsYXRlKENyZWF0ZVRlbXBsYXRlRnJvbVNvdXJjZU9iamVjdCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEVNUExBVEVfQlBNX01FVEhPRF9OUywgdGhpcy5DUkVBVEVfVEVNUExBVEVfRlJPTV9URU1QTEFURV9NRVRIT0RfTkFNRSwgQ3JlYXRlVGVtcGxhdGVGcm9tU291cmNlT2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRPd25lckxpc3RCeVRlYW0odGVhbU5hbWUsIGlzQ2FtcGFpZ25Qcm9qZWN0KSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IHByb2plY3RPd25lckxpc3QgPSBbXTtcclxuICAgICAgICAgICAgdGhpcy5nZXRUZWFtcyhpc0NhbXBhaWduUHJvamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodGVhbUxpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBoYXNUZWFtID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUxpc3QubWFwKHRlYW0gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGVhbS5OQU1FID09PSB0ZWFtTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzVGVhbSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZWFtSWQgPSB0ZWFtWydNUE1fVGVhbXMtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdE93bmVycyh0ZWFtSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVsndXNlcnMnXSAmJiByZXNwb25zZVsndXNlcnMnXS51c2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsndXNlcnMnXS51c2VyLm1hcCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyLmRpc3BsYXlOYW1lID0gdXNlci5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXIudmFsdWUgPSB1c2VyLmNuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXIuaWQgPSB1c2VyLmNuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0T3duZXJMaXN0ID0gcmVzcG9uc2VbJ3VzZXJzJ10udXNlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHByb2plY3RPd25lckxpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChwcm9qZWN0T3duZXJMaXN0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghaGFzVGVhbSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHByb2plY3RPd25lckxpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHByb2plY3RPd25lckxpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVByb2plY3RUYXNrYW5kRGVsaXZlcmFibGUodGVtcGxhdGVPYmosIHRlbXBsYXRlZm9ybSwgcHJvamVjdERlcGVuZGVuY3ksIGRlbGl2ZXJhYmxlVmlld05hbWUpIHtcclxuICAgICAgICBpZiAodGVtcGxhdGVPYmopIHtcclxuICAgICAgICAgICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICAgICAgICAgIGNvbnN0IHRlbXBsYXRlUHJvamVjdEl0ZW1JZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZlaWxkVmFsdWVCeU1hcHBlck5hbWUodGVtcGxhdGVPYmosIHRoaXMubWV0YWRhdGFDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSVRFTV9JRCk7XHJcbiAgICAgICAgICAgIGlmICh0ZW1wbGF0ZVByb2plY3RJdGVtSWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBsYXRlUHJvamVjdElkID0gdGVtcGxhdGVQcm9qZWN0SXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFRhc2tzKHRlbXBsYXRlUHJvamVjdElkKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0YXNrcyA9IHJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXRhc2tzIHx8ICF0YXNrcy5sZW5ndGggfHwgdGFza3MubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNOb1Rhc2tzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3REZXBlbmRlbmN5LmlzTm9EZWxpdmVyYWJsZXMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNUYXNrV2l0aFRhc2tXaXRoRGVsaXZlcmFibGVEZXBlbmRlbmN5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay5lbmFibGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3REZXBlbmRlbmN5LmlzTm9UYXNrcyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNOb0RlbGl2ZXJhYmxlcyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNUYXNrV2l0aFRhc2tXaXRoRGVsaXZlcmFibGVEZXBlbmRlbmN5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlVmlld05hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSBkZWxpdmVyYWJsZVZpZXdOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5ERUxJVkVSQUJMRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld0NvbmZpZykuc3Vic2NyaWJlKCh2aWV3RGV0YWlsczogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoQ29uZmlnID0gdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHICYmIHR5cGVvZiB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgPT09ICdzdHJpbmcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBwYXJzZUludCh2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcsIDEwKSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hDb25kaXRpb25MaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkaXNwbGF5RmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE1QTUZpZWxkQ29uc3RhbnRzLkRFTElWRVJBQkxFX01QTV9GSUVMRFMuREVMSVZFUkFCTEVfUFJPSkVDVF9JRCwgTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hDb25kaXRpb25MaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IGRpc3BsYXlGaWVsZCA/IGRpc3BsYXlGaWVsZC5EQVRBX1RZUEUgOiAnc3RyaW5nJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogZGlzcGxheUZpZWxkLklOREVYRVJfRklFTERfSUQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogTVBNU2VhcmNoT3BlcmF0b3JzLklTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGVtcGxhdGVQcm9qZWN0SXRlbUlkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGluZGV4ZXJGaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEluZGV4ZXJJZEJ5TWFwcGVyVmFsdWUoTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfTkFNRSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzUmVxOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IHNlYXJjaENvbmRpdGlvbkxpc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzb3J0OiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IGluZGV4ZXJGaWVsZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yZGVyOiAnQVNDJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2Vfc2l6ZTogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzUmVxKS5zdWJzY3JpYmUoKHJlc3BvbnNlRGF0YTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VEYXRhLmRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0RGVwZW5kZW5jeS5pc05vRGVsaXZlcmFibGVzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnSW52YWxpZCBwcm9qZWN0IGluZm9ybWF0aW9uIHN1cHBsaWVkIHRvIHZhbGlkYXRlIHRhc2sgYW5kIGRlbGl2ZXJhYmxlcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvcHlUZW1wbGF0ZShzb3VyY2VJZCwgdGVtcGxhdGVmb3JtLCB0ZW1wbGF0ZVNlYXJjaENvbmRpdGlvbiwgY3VzdG9tTWV0YWRhdGEsIHByb2plY3RPd25lciwgdmlld0NvbmZpZ05hbWUsIGVuYWJsZUR1cGxpY2F0ZU5hbWVzKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNvdXJjZUlkKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgUHJvamVjdE9iamVjdDtcclxuICAgICAgICAgICAgICAgIGlmIChwcm9qZWN0T3duZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSBwcm9qZWN0T3duZXIudmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgUHJvamVjdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgU291cmNlSUQ6IHNvdXJjZUlkLnNwbGl0KCcuJylbMV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3ROYW1lOiB0ZW1wbGF0ZWZvcm0udmFsdWUucHJvamVjdE5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3RPd25lcjogdXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9qZWN0RGVzY3JpcHRpb246IHRlbXBsYXRlZm9ybS52YWx1ZS5kZXNjcmlwdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXNDb3B5VGFzazogdGVtcGxhdGVmb3JtLnZhbHVlLmNvcHlUYXNrPyB0ZW1wbGF0ZWZvcm0udmFsdWUuY29weVRhc2sgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXNDb3B5RGVsaXZlcmFibGU6IHRlbXBsYXRlZm9ybS52YWx1ZS5jb3B5RGVsaXZlcmFibGU/IHRlbXBsYXRlZm9ybS52YWx1ZS5jb3B5RGVsaXZlcmFibGUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgQ3VzdG9tTWV0YWRhdGE6IGN1c3RvbU1ldGFkYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDYW1wYWlnbklEOiB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYW1wYWlnbi52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgQ2F0ZWdvcnlNZXRhZGF0YUlEOiB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYXRlZ29yeU1ldGFkYXRhLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0NvcHlXb3JrZmxvd1J1bGVzTmVlZGVkOiB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVdvcmZsb3dSdWxlcy52YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja1Byb2plY3ROYW1lKHRlbXBsYXRlU2VhcmNoQ29uZGl0aW9uLCBQcm9qZWN0T2JqZWN0LlByb2plY3ROYW1lLCB2aWV3Q29uZmlnTmFtZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgZW5hYmxlRHVwbGljYXRlTmFtZXMgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdUZW1wbGF0ZSBhbHJlYWR5IGV4aXN0cycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ3JlYXRlUHJvamVjdEZyb21Tb3VyY2VPYmplY3Q6IFByb2plY3RPYmplY3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdDcmVhdGUgVGVtcGxhdGUgaGFzIGJlZW4gaW5pdGlhdGVkIHN1Y2Vzc2Z1bGx5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTU1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVUZW1wbGF0ZUZyb21UZW1wbGF0ZShQcm9qZWN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZVByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY3JlYXRlUHJvamVjdFJlc3BvbnNlICYmIGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ0FQSVJlc3BvbnNlJ10uc3RhdHVzQ29kZSA9PT0gJzUwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogY3JlYXRlUHJvamVjdFJlc3BvbnNlWydBUElSZXNwb25zZSddLmVycm9yLmVycm9yTWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGNvcHlpbmcgdGVtcGxhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlUHJvamVjdChzb3VyY2VJZCwgdGVtcGxhdGVmb3JtLCBwcm9qZWN0U2VhcmNoQ29uZGl0aW9uLCBjdXN0b21NZXRhZGF0YSwgcHJvamVjdE93bmVyLCB2aWV3Q29uZmlnTmFtZSwgZW5hYmxlRHVwbGljYXRlTmFtZXMpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc291cmNlSWQpIHtcclxuICAgICAgICAgICAgICAgIGxldCBQcm9qZWN0T2JqZWN0O1xyXG4gICAgICAgICAgICAgICAgaWYgKHByb2plY3RPd25lcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IHByb2plY3RPd25lci52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyB0ZW1wbGF0ZWZvcm0udmFsdWUucHJvamVjdE93bmVyID8gdGVtcGxhdGVmb3JtLnZhbHVlLnByb2plY3RPd25lci5jbiA6IFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuZ2V0VXNlckRldGFpbHNCeVVzZXJDbihvd25lckNuKVxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAuc3Vic2NyaWJlKHVzZXJJZCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgUHJvamVjdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgVGVtcGxhdGVJRDogc291cmNlSWQuc3BsaXQoJy4nKVsxXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdE5hbWU6IHRlbXBsYXRlZm9ybS52YWx1ZS5wcm9qZWN0TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdE93bmVyOiB1c2VySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3REZXNjcmlwdGlvbjogdGVtcGxhdGVmb3JtLnZhbHVlLmRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBFeHBlY3RlZFByb2plY3REdXJhdGlvbjogdGVtcGxhdGVmb3JtLnZhbHVlLmV4cGVjdGVkRHVyYXRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3RTdGFydERhdGU6IHRlbXBsYXRlZm9ybS52YWx1ZS5wcm9qZWN0U3RhcnREYXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9qZWN0RHVlRGF0ZTogdGVtcGxhdGVmb3JtLnZhbHVlLnByb2plY3RFbmREYXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBUYXNrU3RhcnREYXRlOiB0ZW1wbGF0ZWZvcm0udmFsdWUudGFza1N0YXJ0RGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgVGFza0R1ZURhdGU6IHRlbXBsYXRlZm9ybS5jb250cm9scy50YXNrRW5kRGF0ZS52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXNDb3B5VGFzazogdGVtcGxhdGVmb3JtLnZhbHVlLmNvcHlUYXNrIHx8IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJc0NvcHlEZWxpdmVyYWJsZTogdGVtcGxhdGVmb3JtLnZhbHVlLmNvcHlEZWxpdmVyYWJsZSB8fCBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgQ3VzdG9tTWV0YWRhdGE6IGN1c3RvbU1ldGFkYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDYW1wYWlnbklEOiB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYW1wYWlnbiA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAodGVtcGxhdGVmb3JtLmdldFJhd1ZhbHVlKCkuY2FtcGFpZ24udmFsdWUgPyB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYW1wYWlnbi52YWx1ZSA6IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhbXBhaWduLnNwbGl0KC8tKC4rKS8pWzBdKSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDYXRlZ29yeU1ldGFkYXRhSUQ6IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhdGVnb3J5TWV0YWRhdGEgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhdGVnb3J5TWV0YWRhdGEudmFsdWUgPyB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYXRlZ29yeU1ldGFkYXRhLnZhbHVlIDogdGVtcGxhdGVmb3JtLmdldFJhd1ZhbHVlKCkuY2F0ZWdvcnlNZXRhZGF0YSkgOiAnJ1xyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja1Byb2plY3ROYW1lKHByb2plY3RTZWFyY2hDb25kaXRpb24sIFByb2plY3RPYmplY3QuUHJvamVjdE5hbWUsIHZpZXdDb25maWdOYW1lKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiBlbmFibGVEdXBsaWNhdGVOYW1lcyA9PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnUHJvamVjdCBhbHJlYWR5IGV4aXN0cycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQ3JlYXRlUHJvamVjdEZyb21Tb3VyY2VPYmplY3Q6IFByb2plY3RPYmplY3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdDcmVhdGUgUHJvamVjdCBoYXMgYmVlbiBpbml0aWF0ZWQgc3VjZXNzZnVsbHknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVByb2plY3RGcm9tVGVtcGxhdGUoUHJvamVjdE9iamVjdClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjcmVhdGVQcm9qZWN0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0ZVByb2plY3RSZXNwb25zZSAmJiBjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ0FQSVJlc3BvbnNlJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY3JlYXRlUHJvamVjdFJlc3BvbnNlWydBUElSZXNwb25zZSddLnN0YXR1c0NvZGUgPT09ICc1MDAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXS5lcnJvci5lcnJvck1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ0FQSVJlc3BvbnNlJ10uc3RhdHVzQ29kZSA9PT0gJzIwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBjb3B5aW5nIHByb2plY3QnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHV0Q3VzdG9tTWV0YWRhdGEoY3VzdG9tRmllbGRzR3JvdXAsIHRlbXBsYXRlT2JqKSB7XHJcbiAgICAgICAgY29uc3QgY29zdG9tTWV0YWRhdGFWYWx1ZXMgPSBbXTtcclxuICAgICAgICBpZiAoY3VzdG9tRmllbGRzR3JvdXAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ10gJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J11cclxuICAgICAgICAgICAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10uY29udHJvbHNcclxuICAgICAgICAgICAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10uY29udHJvbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNvbnRyb2wgb2YgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10uY29udHJvbHMpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmdldFByb3BlcnR5KHRlbXBsYXRlT2JqLCBjb250cm9sLmZpZWxkc2V0LmlkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRyb2wudmFsdWUgPSB0aGlzLmdldFByb3BlcnR5KHRlbXBsYXRlT2JqLCBjb250cm9sLmZpZWxkc2V0LmlkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY3VzdG9tRmllbGRzR3JvdXA7XHJcbiAgICB9XHJcbiAgICBnZXRDdXN0b21NZXRhZGF0YShjdXN0b21GaWVsZHNHcm91cCkge1xyXG4gICAgICAgIGNvbnN0IGNvc3RvbU1ldGFkYXRhVmFsdWVzID0gW107XHJcbiAgICAgICAgaWYgKGN1c3RvbUZpZWxkc0dyb3VwICYmIGN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddICYmIGN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddWydmaWVsZHNldCddXHJcbiAgICAgICAgICAgICYmIGN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddWydmaWVsZHNldCddLmNvbnRyb2xzXHJcbiAgICAgICAgICAgICYmIGN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddWydmaWVsZHNldCddLmNvbnRyb2xzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgY29udHJvbHM6IEFycmF5PGFueT4gPSBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXVsnZmllbGRzZXQnXS5jb250cm9scztcclxuICAgICAgICAgICAgZm9yIChjb25zdCBjb250cm9sIG9mIGNvbnRyb2xzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoKGNvbnRyb2w/LnZhbHVlICE9PSBudWxsICYmIGNvbnRyb2w/LnZhbHVlICE9PSAnJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtZXRhZGF0YUZpZWxkT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZE9ialsnZGF0YVR5cGUnXSA9IHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXRNZXRhZGF0YUZpZWxkVHlwZShjb250cm9sLmZpZWxkc2V0LmRhdGFfdHlwZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLyptZXRhZGF0YUZpZWxkT2JqWydkZWZhdWx0VmFsdWUnXSA9IGNvbnRyb2wuZmllbGRzZXQuZGF0YV90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5EQVRFID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZS5jb252ZXJ0VG9PVE1NRGF0ZUZvcm1hdChjb250cm9sLnZhbHVlKSA6IGNvbnRyb2wudmFsdWU7Ki9cclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydkZWZhdWx0VmFsdWUnXSA9IGNvbnRyb2wudmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZE9ialsnZmllbGRJZCddID0gY29udHJvbC5maWVsZHNldC5pZDtcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydmaWVsZFR5cGUnXSA9IGNvbnRyb2wuZmllbGRzZXQudHlwZTtcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydyZWZlcmVuY2VWYWx1ZSddID0gJ2RlZmF1bHRSZWZlcmVuY2UnO1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRPYmpbJ2RvbWFpbkZpZWxkVmFsdWUnXSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvbnRyb2wuZmllbGRzZXQgJiYgY29udHJvbC5maWVsZHNldC5lZGl0X3R5cGUgJiYgY29udHJvbC5maWVsZHNldC52YWx1ZXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgQXJyYXkuaXNBcnJheShjb250cm9sLmZpZWxkc2V0LnZhbHVlcykgJiYgY29udHJvbC5maWVsZHNldC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkNPTUJPKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRyb2wuZmllbGRzZXQudmFsdWVzLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5maWVsZF92YWx1ZSAmJiBkYXRhLmZpZWxkX3ZhbHVlLnZhbHVlICYmIGRhdGEuZmllbGRfdmFsdWUudmFsdWUgPT09IG1ldGFkYXRhRmllbGRPYmpbJ2RlZmF1bHRWYWx1ZSddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZE9ialsnZG9tYWluRmllbGRWYWx1ZSddID0gZGF0YS5kaXNwbGF5X3ZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgY29zdG9tTWV0YWRhdGFWYWx1ZXMucHVzaChtZXRhZGF0YUZpZWxkT2JqKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4geyBNZXRhZGF0YTogeyBtZXRhZGF0YUZpZWxkczogY29zdG9tTWV0YWRhdGFWYWx1ZXMgfSB9O1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1heE1pblZhbHVlRm9yQ3VzdG9tTWV0YWRhdGEoZGF0YUxlbmd0aCwgc2NhbGUpOiBhbnkge1xyXG4gICAgICAgIGxldCBkYXRhID0gTWF0aC5hYnMoZGF0YUxlbmd0aCAtIHNjYWxlKTtcclxuICAgICAgICBsZXQgbnVtZXJpY1ZhbHVlID0gbmV3QXJyYXkoZGF0YSkuZmlsbCgnOScpLmpvaW4oJycpO1xyXG4gICAgICAgIGlmIChzY2FsZSkge1xyXG4gICAgICAgICAgICBudW1lcmljVmFsdWUgKz0gJy4nICsgbmV3QXJyYXkoc2NhbGUpLmZpbGwoJzknKS5qb2luKCcnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiByYWRpeFxyXG4gICAgICAgIHJldHVybiBzY2FsZSA/IHBhcnNlRmxvYXQobnVtZXJpY1ZhbHVlKSA6IHBhcnNlSW50KG51bWVyaWNWYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbWFwQ3VzdG9tTWV0YWRhdGFGb3JtKG1ldGFkYXRhRmllbGQsIHRlbXBsYXRlT2JqKSB7XHJcbiAgICAgICAgY29uc3QgdmFsaWRhdG9ycyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gKHRlbXBsYXRlT2JqKSA/XHJcbiAgICAgICAgICAgIHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXRGaWVsZFZhbHVlQnlJZCh0ZW1wbGF0ZU9iai5tZXRhZGF0YSwgbWV0YWRhdGFGaWVsZC5pZCkgOiAnJztcclxuXHJcbiAgICAgICAgY29uc3QgZm9ybUNvbnRyb2xPYmogPSBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICB2YWx1ZTogdmFsdWUgPyB2YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICBkaXNhYmxlZDogIW1ldGFkYXRhRmllbGQuZWRpdGFibGUsXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLnJlcXVpcmVkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuREFURSkge1xyXG4gICAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goRGF0ZVZhbGlkYXRvcnMuZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLlNJTVBMRSB8fFxyXG4gICAgICAgICAgICBtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuVEVYVEFSRUEpIHtcclxuICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMubWF4TGVuZ3RoKG1ldGFkYXRhRmllbGQuZGF0YV9sZW5ndGgpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvcm1Db250cm9sT2JqLnNldFZhbGlkYXRvcnModmFsaWRhdG9ycyk7XHJcbiAgICAgICAgZm9ybUNvbnRyb2xPYmoudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG5cclxuICAgICAgICBmb3JtQ29udHJvbE9ialsnZmllbGRzZXQnXSA9IG1ldGFkYXRhRmllbGQ7XHJcbiAgICAgICAgZm9ybUNvbnRyb2xPYmpbJ25hbWUnXSA9IG1ldGFkYXRhRmllbGQubmFtZTtcclxuXHJcbiAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5DT01CTykge1xyXG4gICAgICAgICAgICBtZXRhZGF0YUZpZWxkLnZhbHVlcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChtZXRhZGF0YUZpZWxkLmRvbWFpbl9pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmb3JtQ29udHJvbE9iajtcclxuICAgIH1cclxuICAgIGdldE9UTU1DdXN0b21NZXRhRGF0YShjdXN0b21NZXRhZGF0YU9iajogQ3VzdG9tTWV0YWRhdGFPYmosIGZvcm1Hcm91cCwgdGVtcGxhdGVPYmosIGNhdGVnb3J5TGV2ZWxEZXRhaWwpOiBPYnNlcnZhYmxlPEN1c3RvbU1ldGFkYXRhT2JqPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IGNhdGVnb3J5TGV2ZWxEZXRhaWxzID0gdGhpcy5jYXRlZ29yeVNlcnZpY2UuZ2V0Q2F0ZWdvcnlMZXZlbERldGFpbHNCeVR5cGUoTVBNX0xFVkVMUy5QUk9KRUNUKTtcclxuICAgICAgICAgICAgaWYgKGNhdGVnb3J5TGV2ZWxEZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxEZXRhaWxzID0gY2F0ZWdvcnlMZXZlbERldGFpbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldE1ldGFkYXRNb2RlbEJ5SWQoY2F0ZWdvcnlMZXZlbERldGFpbHMuTUVUQURBVEFfTU9ERUxfSUQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKG1ldGFEYXRhTW9kZWxEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5maWVsZHNldEdyb3VwID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgY3VzdG9tTWV0YWRhdGFPYmouZmllbGRHcm91cHMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5jdXN0b21NZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChtZXRhRGF0YU1vZGVsRGV0YWlscyAmJiBtZXRhRGF0YU1vZGVsRGV0YWlscy5tZXRhZGF0YV9lbGVtZW50X2xpc3QgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgQXJyYXkuaXNBcnJheShtZXRhRGF0YU1vZGVsRGV0YWlscy5tZXRhZGF0YV9lbGVtZW50X2xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgbWV0YWRhdGFGaWVsZEdyb3VwIG9mIG1ldGFEYXRhTW9kZWxEZXRhaWxzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZm9ybUNvbnRyb2xBcnJheSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRmllbGRHcm91cC5pZCA9PT0gUHJvamVjdENvbnN0YW50Lk1QTV9QUk9KRUNUX01FVEFEQVRBX0dST1VQKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZEdyb3VwLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEFycmF5LmlzQXJyYXkobWV0YWRhdGFGaWVsZEdyb3VwLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IG1ldGFkYXRhRmllbGQgb2YgbWV0YWRhdGFGaWVsZEdyb3VwLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5jdXN0b21NZXRhZGF0YUZpZWxkcy5wdXNoKG1ldGFkYXRhRmllbGQpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9ycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgdmFsdWUgPSAodGVtcGxhdGVPYmopID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEluZGV4ZXJJZEJ5T1RNTUlkKG1ldGFkYXRhRmllbGQuaWQpIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdGVtcGxhdGVPYmpbdmFsdWVdID8gdGVtcGxhdGVPYmpbdmFsdWVdIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZvcm1Db250cm9sT2JqID0gbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdmFsdWUgIT09ICd1bmRlZmluZWQnID8gdmFsdWUgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAhbWV0YWRhdGFGaWVsZC5lZGl0YWJsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5yZXF1aXJlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkRBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChEYXRlVmFsaWRhdG9ycy5kYXRlRm9ybWF0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5TSU1QTEUgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5URVhUQVJFQSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMubWF4TGVuZ3RoKG1ldGFkYXRhRmllbGQuZGF0YV9sZW5ndGgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZGF0YV90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5OVU1CRVIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG51bWVyaWNWYWx1ZSA9IHRoaXMuZ2V0TWF4TWluVmFsdWVGb3JDdXN0b21NZXRhZGF0YShtZXRhZGF0YUZpZWxkLmRhdGFfbGVuZ3RoLCBtZXRhZGF0YUZpZWxkLnNjYWxlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLm1heChudW1lcmljVmFsdWUpLCBWYWxpZGF0b3JzLm1pbigtKG51bWVyaWNWYWx1ZSkpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xPYmouc2V0VmFsaWRhdG9ycyh2YWxpZGF0b3JzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xPYmoudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xPYmpbJ2ZpZWxkc2V0J10gPSBtZXRhZGF0YUZpZWxkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE9ialsnbmFtZSddID0gbWV0YWRhdGFGaWVsZC5uYW1lO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5DT01CTykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZC52YWx1ZXMgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldExvb2t1cERvbWFpblZhbHVlc0J5SWQobWV0YWRhdGFGaWVsZC5kb21haW5faWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmZpZWxkc2V0R3JvdXAucHVzaChmb3JtQ29udHJvbE9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG5ldyBjb2RlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1Db250cm9sQXJyYXkucHVzaChmb3JtQ29udHJvbE9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRHcm91cCA9IGZvcm1Hcm91cC5ncm91cCh7IGZpZWxkc2V0OiBuZXcgRm9ybUFycmF5KGZvcm1Db250cm9sQXJyYXkpIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRHcm91cFsnbmFtZSddID0gbWV0YWRhdGFGaWVsZEdyb3VwLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5maWVsZEdyb3Vwcy5wdXNoKGZpZWxkR3JvdXApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChjdXN0b21NZXRhZGF0YU9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGN1c3RvbU1ldGFkYXRhT2JqKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrVGFza0FjY2Vzcyhwcm9qZWN0RGVwZW5kZW5jeSwgdGVtcGxhdGVmb3JtKSB7XHJcbiAgICAgICAgaWYgKCFwcm9qZWN0RGVwZW5kZW5jeS5zZWxlY3RlZE9iamVjdCAmJiB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sudmFsdWUpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gJ0Nhbm5vdCBlbmFibGUgY29weSB0YXNrIG9wdGlvbi4gUGxlYXNlIHNlbGVjdCBhIHRlbXBsYXRlLic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChwcm9qZWN0RGVwZW5kZW5jeS5pc05vVGFza3MgJiYgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgcmV0dXJuICdDYW5ub3QgZW5hYmxlIGNvcHkgdGFzayBvcHRpb24uIE5vIHRhc2tzIGFyZSBwcmVzZW50Lic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sudmFsdWUgJiYgcHJvamVjdERlcGVuZGVuY3kuaXNUYXNrV2l0aFRhc2tXaXRoRGVsaXZlcmFibGVEZXBlbmRlbmN5KSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay52YWx1ZSkge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgY2hlY2tEZWxpdmVyYWJsZUFjY2Vzcyhwcm9qZWN0RGVwZW5kZW5jeSwgdGVtcGxhdGVmb3JtKSB7XHJcbiAgICAgICAgaWYgKCFwcm9qZWN0RGVwZW5kZW5jeS5zZWxlY3RlZE9iamVjdCAmJiB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiAnQ2Fubm90IGVuYWJsZSBjb3B5IGRlbGl2ZXJhYmxlIG9wdGlvbi4gUGxlYXNlIHNlbGVjdCBhIHRlbXBsYXRlLic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChwcm9qZWN0RGVwZW5kZW5jeS5pc05vRGVsaXZlcmFibGVzICYmIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUudmFsdWUpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgcmV0dXJuICdDYW5ub3QgZW5hYmxlIGNvcHkgZGVsaXZlcmFibGUgb3B0aW9uLiBObyBEZWxpdmVyYWJsZXMgYXJlIHByZXNlbnQuJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLnZhbHVlICYmIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUudmFsdWUpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgcmV0dXJuICdDYW5ub3QgZW5hYmxlIGNvcHkgZGVsaXZlcmFibGUgb3B0aW9uLiBFbmFibGUgY29weSB0YXNrIG9wdGlvbiB0byBlbmFibGUgY29weSBkZWxpdmVyYmxlLic7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUudmFsdWUgJiYgcHJvamVjdERlcGVuZGVuY3kuaXNUYXNrV2l0aFRhc2tXaXRoRGVsaXZlcmFibGVEZXBlbmRlbmN5KSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgcmV0dXJuICdDYW5ub3QgZGlzYWJsZSBjb3B5IGRlbGl2ZXJhYmxlIG9wdGlvbi4gU2VsZWN0ZWQgcHJvamVjdCBjb250YWlucyBBcHByb3ZhbCBUYXNrcywgd2hpY2ggaGFzIGRlbGl2ZXJhYmxlIGRlcGVuZGVuY2llcy4nO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS52YWx1ZSAmJiAhcHJvamVjdERlcGVuZGVuY3kuaXNOb0RlbGl2ZXJhYmxlcykge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sucGF0Y2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxUZW1wbGF0ZXModGVtcGxhdGVTZWFyY2hDb25kaXRpb24sIHZpZXdDb25maWdOYW1lKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBsZXQgdmlld0NvbmZpZztcclxuICAgICAgICAgICAgaWYgKHZpZXdDb25maWdOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3Q29uZmlnID0gdmlld0NvbmZpZ05hbWU7Ly90aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFZpZXdDb25maWdCeUlkKHZpZXdDb25maWdJZCkuVklFVztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuUFJPSkVDVFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgJiYgdHlwZW9mIHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyA9PT0gJ3N0cmluZydcclxuICAgICAgICAgICAgICAgICAgICA/IHBhcnNlSW50KHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApIDogbnVsbDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGluZGV4ZXJGaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEluZGV4ZXJJZEJ5TWFwcGVyVmFsdWUoTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfTkFNRSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzUmVxOiBTZWFyY2hSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHNlYXJjaENvbmZpZyA/IHNlYXJjaENvbmZpZyA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAga2V5d29yZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IHRlbXBsYXRlU2VhcmNoQ29uZGl0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydDogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkX2lkOiBpbmRleGVyRmllbGQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcmRlcjogJ0FTQydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnNvcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlX2luZGV4OiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlX3NpemU6IDEwMFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzUmVxKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBsYXRlT2JqID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxUZW1wbGF0ZUNvdW50OiByZXNwb25zZS5jdXJzb3IudG90YWxfcmVjb3JkcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxsVGVtcGxhdGVMaXN0OiByZXNwb25zZS5kYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRlbXBsYXRlT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBwcm9qZWN0cy4nKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=