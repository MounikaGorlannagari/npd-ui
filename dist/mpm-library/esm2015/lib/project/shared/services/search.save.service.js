import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/sharing.service";
let SearchSaveService = class SearchSaveService {
    constructor(appService, sharingService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.CreateMPMSavedSearchesOperationNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.CreateMPMSavedSearchesWS = 'CreateMPM_Saved_Searches';
        this.DeleteMPMSavedSearchesWS = 'DeleteMPM_Saved_Searches';
        this.DeleteMPMSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.UpdateMPMSavedSearchesWS = 'UpdateMPM_Saved_Searches';
        this.UpdateMPMSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
    }
    saveSearch(name, desc, searchConditionList, isPublic, viewName) {
        return new Observable(observer => {
            const param = {
                'MPM_Saved_Searches-create': {
                    NAME: name,
                    DESCRIPTION: desc,
                    IS_PUBLIC: isPublic,
                    IS_ROLE_SPECIFIC: false,
                    ROLE_ID: '',
                    TYPE: '',
                    IS_SYSTEM_DEFAULT_SEARCH: false,
                    DEFAULT_OPERATOR: '',
                    SEARCH_DATA: searchConditionList ? JSON.stringify(searchConditionList) : '',
                    VIEW_NAME: viewName,
                    R_PO_CREATED_USER: {
                        'Identity-id': {
                            Id: this.sharingService.getcurrentUserItemID()
                        }
                    }
                }
            };
            this.appService.invokeRequest(this.CreateMPMSavedSearchesOperationNS, this.CreateMPMSavedSearchesWS, param).subscribe(savedSearch => {
                observer.next(savedSearch);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
    deleteSavedSearch(savedSearchId) {
        return new Observable(observer => {
            const param = {
                MPM_Saved_Searches: {
                    'MPM_Saved_Searches-id': {
                        Id: savedSearchId
                    }
                }
            };
            this.appService.invokeRequest(this.DeleteMPMSavedSearchesNS, this.DeleteMPMSavedSearchesWS, param).subscribe(success => {
                observer.next(true);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
    updateSavedSearch(seachId, name, desc, searchConditionList, isPublic, viewName) {
        return new Observable(observer => {
            const param = {
                'MPM_Saved_Searches-id': {
                    Id: seachId
                },
                'MPM_Saved_Searches-update': {
                    NAME: name,
                    DESCRIPTION: desc,
                    IS_PUBLIC: isPublic,
                    IS_ROLE_SPECIFIC: false,
                    ROLE_ID: '',
                    TYPE: '',
                    IS_SYSTEM_DEFAULT_SEARCH: false,
                    DEFAULT_OPERATOR: '',
                    SEARCH_DATA: searchConditionList ? JSON.stringify(searchConditionList) : '',
                    VIEW_NAME: viewName,
                    R_PO_CREATED_USER: {
                        'Identity-id': {
                            Id: this.sharingService.getcurrentUserItemID()
                        }
                    }
                }
            };
            this.appService.invokeRequest(this.UpdateMPMSavedSearchesNS, this.UpdateMPMSavedSearchesWS, param).subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error();
                observer.complete();
            });
        });
    }
};
SearchSaveService.ctorParameters = () => [
    { type: AppService },
    { type: SharingService }
];
SearchSaveService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchSaveService_Factory() { return new SearchSaveService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService)); }, token: SearchSaveService, providedIn: "root" });
SearchSaveService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SearchSaveService);
export { SearchSaveService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNhdmUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC5zYXZlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUVsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDOzs7O0FBVTdFLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRTFCLFlBQ1csVUFBc0IsRUFDdEIsY0FBOEI7UUFEOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFHekMsc0NBQWlDLEdBQUcsNkRBQTZELENBQUM7UUFDbEcsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFFdEQsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFDdEQsNkJBQXdCLEdBQUcsNkRBQTZELENBQUM7UUFFekYsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFDdEQsNkJBQXdCLEdBQUcsNkRBQTZELENBQUM7SUFUckYsQ0FBQztJQVdMLFVBQVUsQ0FBQyxJQUFZLEVBQUUsSUFBWSxFQUFFLG1CQUF3QyxFQUFFLFFBQWlCLEVBQUUsUUFBUTtRQUN4RyxPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLE1BQU0sS0FBSyxHQUF5QjtnQkFDaEMsMkJBQTJCLEVBQUU7b0JBQ3pCLElBQUksRUFBRSxJQUFJO29CQUNWLFdBQVcsRUFBRSxJQUFJO29CQUNqQixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsZ0JBQWdCLEVBQUUsS0FBSztvQkFDdkIsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLEVBQUU7b0JBQ1Isd0JBQXdCLEVBQUUsS0FBSztvQkFDL0IsZ0JBQWdCLEVBQUUsRUFBRTtvQkFDcEIsV0FBVyxFQUFFLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQzNFLFNBQVMsRUFBRSxRQUFRO29CQUNuQixpQkFBaUIsRUFBRTt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7eUJBQ2pEO3FCQUNKO2lCQUNKO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUNoSSxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsYUFBcUI7UUFDbkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixNQUFNLEtBQUssR0FBNEI7Z0JBQ25DLGtCQUFrQixFQUFFO29CQUNoQix1QkFBdUIsRUFBRTt3QkFDckIsRUFBRSxFQUFFLGFBQWE7cUJBQ3BCO2lCQUNKO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNuSCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsT0FBZSxFQUFFLElBQVksRUFBRSxJQUFZLEVBQUUsbUJBQXdDLEVBQUUsUUFBaUIsRUFBRSxRQUFRO1FBQ2hJLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsTUFBTSxLQUFLLEdBQTZCO2dCQUNwQyx1QkFBdUIsRUFBRTtvQkFDckIsRUFBRSxFQUFFLE9BQU87aUJBQ2Q7Z0JBQ0QsMkJBQTJCLEVBQUU7b0JBQ3pCLElBQUksRUFBRSxJQUFJO29CQUNWLFdBQVcsRUFBRSxJQUFJO29CQUNqQixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsZ0JBQWdCLEVBQUUsS0FBSztvQkFDdkIsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLEVBQUU7b0JBQ1Isd0JBQXdCLEVBQUUsS0FBSztvQkFDL0IsZ0JBQWdCLEVBQUUsRUFBRTtvQkFDcEIsV0FBVyxFQUFFLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQzNFLFNBQVMsRUFBRSxRQUFRO29CQUNuQixpQkFBaUIsRUFBRTt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7eUJBQ2pEO3FCQUNKO2lCQUNKO2FBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNwSCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUFqRzBCLFVBQVU7WUFDTixjQUFjOzs7QUFKaEMsaUJBQWlCO0lBSjdCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7R0FFVyxpQkFBaUIsQ0FvRzdCO1NBcEdZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNUE1TYXZlZFNlYXJjaENyZWF0ZSwgU2VhcmNoQ29uZGl0aW9uTGlzdCB9IGZyb20gJy4uLy4uLy4uL3NlYXJjaC9vYmplY3RzL01QTVNhdmVkU2VhcmNoQ3JlYXRlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmRpdGlvbkxpc3QgfSBmcm9tICcuLi8uLi8uLi9zZWFyY2gvb2JqZWN0cy9Db25kaXRpb25MaXN0JztcclxuaW1wb3J0IHsgTVBNRGVsZXRlU2F2ZWRTZWFyY2hSZXEgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvb2JqZWN0cy9NUE1EZWxldGVTYXZlZFNlYXJjaFJlcSc7XHJcbmltcG9ydCB7IE1QTVNhdmVkU2VhcmNoIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL29iamVjdHMvTVBNU2F2ZWRTZWFyY2gnO1xyXG5pbXBvcnQgeyBVcGRhdGVNUE1TYXZlZFNlYXJjaGVSZXEgfSBmcm9tICcuLi8uLi8uLi9zZWFyY2gvb2JqZWN0cy9VcGRhdGVNUE1TYXZlZFNlYXJjaGVSZXEnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2VhcmNoU2F2ZVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgQ3JlYXRlTVBNU2F2ZWRTZWFyY2hlc09wZXJhdGlvbk5TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9TYXZlZF9TZWFyY2hlcy9vcGVyYXRpb25zJztcclxuICAgIENyZWF0ZU1QTVNhdmVkU2VhcmNoZXNXUyA9ICdDcmVhdGVNUE1fU2F2ZWRfU2VhcmNoZXMnO1xyXG5cclxuICAgIERlbGV0ZU1QTVNhdmVkU2VhcmNoZXNXUyA9ICdEZWxldGVNUE1fU2F2ZWRfU2VhcmNoZXMnO1xyXG4gICAgRGVsZXRlTVBNU2F2ZWRTZWFyY2hlc05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9TYXZlZF9TZWFyY2hlcy9vcGVyYXRpb25zJztcclxuXHJcbiAgICBVcGRhdGVNUE1TYXZlZFNlYXJjaGVzV1MgPSAnVXBkYXRlTVBNX1NhdmVkX1NlYXJjaGVzJztcclxuICAgIFVwZGF0ZU1QTVNhdmVkU2VhcmNoZXNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU2F2ZWRfU2VhcmNoZXMvb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgc2F2ZVNlYXJjaChuYW1lOiBzdHJpbmcsIGRlc2M6IHN0cmluZywgc2VhcmNoQ29uZGl0aW9uTGlzdDogU2VhcmNoQ29uZGl0aW9uTGlzdCwgaXNQdWJsaWM6IGJvb2xlYW4sIHZpZXdOYW1lKTogT2JzZXJ2YWJsZTxNUE1TYXZlZFNlYXJjaENyZWF0ZT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtOiBNUE1TYXZlZFNlYXJjaENyZWF0ZSA9IHtcclxuICAgICAgICAgICAgICAgICdNUE1fU2F2ZWRfU2VhcmNoZXMtY3JlYXRlJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIE5BTUU6IG5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgREVTQ1JJUFRJT046IGRlc2MsXHJcbiAgICAgICAgICAgICAgICAgICAgSVNfUFVCTElDOiBpc1B1YmxpYyxcclxuICAgICAgICAgICAgICAgICAgICBJU19ST0xFX1NQRUNJRklDOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBST0xFX0lEOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBUWVBFOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBJU19TWVNURU1fREVGQVVMVF9TRUFSQ0g6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIERFRkFVTFRfT1BFUkFUT1I6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIFNFQVJDSF9EQVRBOiBzZWFyY2hDb25kaXRpb25MaXN0ID8gSlNPTi5zdHJpbmdpZnkoc2VhcmNoQ29uZGl0aW9uTGlzdCkgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBWSUVXX05BTUU6IHZpZXdOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIFJfUE9fQ1JFQVRFRF9VU0VSOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdJZGVudGl0eS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIElkOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldGN1cnJlbnRVc2VySXRlbUlEKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DcmVhdGVNUE1TYXZlZFNlYXJjaGVzT3BlcmF0aW9uTlMsIHRoaXMuQ3JlYXRlTVBNU2F2ZWRTZWFyY2hlc1dTLCBwYXJhbSkuc3Vic2NyaWJlKHNhdmVkU2VhcmNoID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc2F2ZWRTZWFyY2gpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVNhdmVkU2VhcmNoKHNhdmVkU2VhcmNoSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtOiBNUE1EZWxldGVTYXZlZFNlYXJjaFJlcSA9IHtcclxuICAgICAgICAgICAgICAgIE1QTV9TYXZlZF9TZWFyY2hlczoge1xyXG4gICAgICAgICAgICAgICAgICAgICdNUE1fU2F2ZWRfU2VhcmNoZXMtaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiBzYXZlZFNlYXJjaElkXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRlbGV0ZU1QTVNhdmVkU2VhcmNoZXNOUywgdGhpcy5EZWxldGVNUE1TYXZlZFNlYXJjaGVzV1MsIHBhcmFtKS5zdWJzY3JpYmUoc3VjY2VzcyA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVNhdmVkU2VhcmNoKHNlYWNoSWQ6IHN0cmluZywgbmFtZTogc3RyaW5nLCBkZXNjOiBzdHJpbmcsIHNlYXJjaENvbmRpdGlvbkxpc3Q6IFNlYXJjaENvbmRpdGlvbkxpc3QsIGlzUHVibGljOiBib29sZWFuLCB2aWV3TmFtZSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtOiBVcGRhdGVNUE1TYXZlZFNlYXJjaGVSZXEgPSB7XHJcbiAgICAgICAgICAgICAgICAnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBzZWFjaElkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgJ01QTV9TYXZlZF9TZWFyY2hlcy11cGRhdGUnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgTkFNRTogbmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBERVNDUklQVElPTjogZGVzYyxcclxuICAgICAgICAgICAgICAgICAgICBJU19QVUJMSUM6IGlzUHVibGljLFxyXG4gICAgICAgICAgICAgICAgICAgIElTX1JPTEVfU1BFQ0lGSUM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIFJPTEVfSUQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIFRZUEU6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIElTX1NZU1RFTV9ERUZBVUxUX1NFQVJDSDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgREVGQVVMVF9PUEVSQVRPUjogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgU0VBUkNIX0RBVEE6IHNlYXJjaENvbmRpdGlvbkxpc3QgPyBKU09OLnN0cmluZ2lmeShzZWFyY2hDb25kaXRpb25MaXN0KSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIFZJRVdfTkFNRTogdmlld05hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgUl9QT19DUkVBVEVEX1VTRVI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0lkZW50aXR5LWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlVwZGF0ZU1QTVNhdmVkU2VhcmNoZXNOUywgdGhpcy5VcGRhdGVNUE1TYXZlZFNlYXJjaGVzV1MsIHBhcmFtKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==