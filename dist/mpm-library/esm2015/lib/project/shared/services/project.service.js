import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import * as acronui from '../../../mpm-utils/auth/utility';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/otmm.service";
import * as i3 from "../../../mpm-utils/services/util.service";
import * as i4 from "../../../shared/services/otmm-metadata.service";
import * as i5 from "../../../mpm-utils/services/sharing.service";
import * as i6 from "../../../comments/services/comments.util.service";
let ProjectService = class ProjectService {
    constructor(appService, otmmService, utilService, otmmMetadataService, sharingService, commentUtilService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.utilService = utilService;
        this.otmmMetadataService = otmmMetadataService;
        this.sharingService = sharingService;
        this.commentUtilService = commentUtilService;
        this.PROJECT_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.PROJECT_FILTER_NS = 'http://schemas/AcheronMPMCore/MPM_Project_Filter/operations';
        this.PROJECT_BPM = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_ALL_CATEGORY_METADATA_NS = 'http://schemas/AcheronMPMCore/MPM_Category_Metadata/operations';
        this.DOWNLOAD_EXCEL_NS = 'http://schemas.acheron.com/mpm/wsapp/excel/1.0';
        this.DELETE_PROJECT_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_TASK_BY_PROJECT_ID_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.DOWNLOAD_EXCEL_TEMPLATE = 'DownloadExcelTemplate ';
        // CREATE_BULK_PROJECT_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_BULK_PROJECT = 'BulkProjectCreation';
        this.CREATE_DOCUMENT_NS = 'http://schemas.cordys.com/documentstore/default/1.0';
        this.CREATE_DOCUMENT = 'CreateDocument';
        this.GET_ALL_PROJECTS_WS = 'GetAllProjects';
        this.GET_ALL_PROJECT_FILTERS = 'GetAllProjectFilters';
        this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_WS = 'GetMilestoneTasksByProject';
        this.GET_PROJECTS_BY_ID = 'ReadProject';
        this.RESTART_PROJECT_WS = 'HandleRestartProject';
        this.GET_ALL_CATEGORY_METADATA = 'GetAllCategoryMetadata';
        this.DELETE_PROJECT = 'DeleteProject';
        this.GET_TASK_BY_PROJECT_ID = 'GetTaskByProjectID ';
        this.updateProjectInfo = new BehaviorSubject(null);
        this.onUpdateProjectInfo = this.updateProjectInfo.asObservable();
    }
    setSelectedProjectDetails(projectObj) {
        this.selectedProject = projectObj;
    }
    getSelectedProjectDetails() {
        return this.selectedProject;
    }
    setUpdatedProjectInfo(currMetadata) {
        this.updateProjectInfo.next(currMetadata);
    }
    getProperty(obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    }
    converToLocalDate(obj, path) {
        const dateObj = this.otmmMetadataService.getFieldValueById(obj.metadata, path);
        if (dateObj != null) {
            return dateObj;
        }
        return;
    }
    getProjects(parameters) {
        const searchConditionList = JSON.parse(parameters.defaultSearchConditionList);
        if (parameters.searchConditionList && parameters.searchConditionList.length > 0) {
            searchConditionList.search_condition_list.search_condition = searchConditionList.search_condition_list.search_condition.concat(parameters.searchConditionList);
        }
        const skip = parameters.skip || 0;
        const top = parameters.top || 10;
        const folderFilter = searchConditionList.search_condition_list.search_condition.some(sc => sc.metadata_field_id === 'MPM.PROJECT.PROJECT_TYPE' && sc.value === 'PROJECT') ?
            this.sharingService.getSelectedCategory().PROJECT_FOLDER_ID : this.sharingService.getSelectedCategory().TEMPLATE_FOLDER_ID;
        const facetRestrictionList = parameters.facetRestrictionList || '';
        const otmmProjectsSearchConfig = {
            load_type: 'metadata',
            level_of_detail: 'slim',
            child_count_load_type: 'folders',
            search_condition_list: searchConditionList,
            folder_filter_type: 'direct',
            facet_restriction_list: facetRestrictionList,
            folder_filter: folderFilter,
            sortString: parameters.sortString,
            search_config_id: parameters.search_config_id,
        };
        return new Observable(observer => {
            this.otmmService.searchCustomText(otmmProjectsSearchConfig, skip, top, otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()).
                subscribe(response => {
                if (response.search_result_resource && response.search_result_resource.search_result
                    && response.search_result_resource.search_result.total_hit_count > 0
                    && response.search_result_resource.asset_list) {
                    response.search_result_resource.asset_list = this.otmmMetadataService.setReferenceValueForMetadataFeilds(response.search_result_resource.asset_list, this.otmmMetadataService.getDefaultProjectMetadataFields());
                    response.search_result_resource.asset_list = this.otmmMetadataService.setReferenceValueForMetadataFeilds(response.search_result_resource.asset_list, this.otmmMetadataService.getCustomProjectMetadataFields());
                }
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getProjectById(projectId) {
        const parameter = {
            'Project-id': {
                Id: projectId
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_NS, this.GET_PROJECTS_BY_ID, parameter)
                .subscribe(response => {
                observer.next(response.Project);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getCompletedMilestonesByProjectId(projectId) {
        const parameter = {
            'projectId': projectId
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_NS, this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_WS, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getProjectDataValues(projectId) {
        const parameter = {
            ProjectID: projectId
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_BPM, 'EditProjectDateValidation', parameter)
                .subscribe(response => {
                observer.next({
                    startMaxDate: response && response['Date']['StartDate'] && typeof response['Date']['StartDate'] === 'string' ? new Date(response['Date']['StartDate']) : null,
                    endMinDate: response && response['Date']['EndDate'] && typeof response['Date']['EndDate'] === 'string' ? new Date(response['Date']['EndDate']) : null,
                });
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    updateProjectDetails(updatedProject, cancelComment, reasons) {
        const parameter = {
            ProjectObject: updatedProject,
            CancelComment: cancelComment,
            Reasons: {
                'MPM_Status_Reason-id': reasons
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_BPM, 'EditProject', parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getCurrProjectStatus(statusId) {
        if (statusId) {
            return this.commentUtilService.getStatusLevelByStatusId(statusId);
        }
        return null;
    }
    bulkProjectCreation(documentURL, templateID) {
        const parameter = {
            documentURL: documentURL,
            templateID: templateID,
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_BPM, 'BulkProjectCreation', parameter).
                subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    restartProject(projectMetadata, customMetadata, comments) {
        const parameter = {
            ProjectMetadata: projectMetadata,
            CustomMetadata: customMetadata,
            Comments: comments
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.PROJECT_BPM, this.RESTART_PROJECT_WS, parameter).
                subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    createDocument(documentName, documentContent) {
        const parameter = {
            DocumentName: documentName,
            Desctiption: "Test From Angular",
            DocumentContent: documentContent,
            Folder: "/opt/opentext/AppWorksPlatform/devbpm/content/uploadcontent",
            'Properties': {
                MimeType: "application/octet-stream"
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.CREATE_DOCUMENT_NS, this.CREATE_DOCUMENT, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    downloadEncodedExcelFile(userID, categoryID) {
        const parameter = {
            userId: userID,
            categoryId: categoryID
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DOWNLOAD_EXCEL_NS, this.DOWNLOAD_EXCEL_TEMPLATE, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    getAllCategoryMetadata() {
        return new Observable(observer => {
            if (this.sharingService.getCategoryMetadata().length > 0) {
                observer.next(this.sharingService.getCategoryMetadata());
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.GET_ALL_CATEGORY_METADATA_NS, this.GET_ALL_CATEGORY_METADATA, {})
                    .subscribe(response => {
                    this.sharingService.setCategoryMetadata(acronui.findObjectsByProp(response, 'MPM_Category_Metadata'));
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Category_Metadata'));
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
            }
        });
    }
    //  MPMV3-2094
    deleteProject(id, itemId, statusId) {
        const parameter = {
            ProjectObject: {
                ProjectId: {
                    Id: id,
                    ItemId: itemId
                },
                RPOStatus: {
                    StatusID: {
                        Id: statusId
                    }
                }
            }
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_PROJECT_NS, this.DELETE_PROJECT, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
    //  MPMV3-2094
    getTaskByProjectID(projectId) {
        const parameter = {
            projectID: projectId
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.GET_TASK_BY_PROJECT_ID_NS, this.GET_TASK_BY_PROJECT_ID, parameter)
                .subscribe(response => {
                observer.next(response);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        });
    }
};
ProjectService.ctorParameters = () => [
    { type: AppService },
    { type: OTMMService },
    { type: UtilService },
    { type: OtmmMetadataService },
    { type: SharingService },
    { type: CommentsUtilService }
];
ProjectService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectService_Factory() { return new ProjectService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.UtilService), i0.ɵɵinject(i4.OtmmMetadataService), i0.ɵɵinject(i5.SharingService), i0.ɵɵinject(i6.CommentsUtilService)); }, token: ProjectService, providedIn: "root" });
ProjectService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ProjectService);
export { ProjectService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxLQUFLLE9BQU8sTUFBTSxpQ0FBaUMsQ0FBQztBQUMzRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRTdFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOzs7Ozs7OztBQVN2RixJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBaUN2QixZQUNXLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3hCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxjQUE4QixFQUM5QixrQkFBdUM7UUFMdkMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQXFCO1FBckNsRCxlQUFVLEdBQUcsa0RBQWtELENBQUM7UUFDaEUsOENBQXlDLEdBQUcsK0NBQStDLENBQUM7UUFDNUYsc0JBQWlCLEdBQUcsNkRBQTZELENBQUM7UUFDbEYsZ0JBQVcsR0FBRyxzREFBc0QsQ0FBQztRQUNyRSxpQ0FBNEIsR0FBRyxnRUFBZ0UsQ0FBQztRQUNoRyxzQkFBaUIsR0FBRyxnREFBZ0QsQ0FBQztRQUNyRSxzQkFBaUIsR0FBRyxzREFBc0QsQ0FBQztRQUMzRSw4QkFBeUIsR0FBRywrQ0FBK0MsQ0FBQztRQUU1RSw0QkFBdUIsR0FBRyx3QkFBd0IsQ0FBQztRQUVuRCxtRkFBbUY7UUFDbkYsd0JBQW1CLEdBQUcscUJBQXFCLENBQUM7UUFFNUMsdUJBQWtCLEdBQUcscURBQXFELENBQUM7UUFDM0Usb0JBQWUsR0FBRyxnQkFBZ0IsQ0FBQztRQUVuQyx3QkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQztRQUN2Qyw0QkFBdUIsR0FBRyxzQkFBc0IsQ0FBQztRQUNqRCw4Q0FBeUMsR0FBRyw0QkFBNEIsQ0FBQztRQUN6RSx1QkFBa0IsR0FBRyxhQUFhLENBQUM7UUFDbkMsdUJBQWtCLEdBQUcsc0JBQXNCLENBQUM7UUFDNUMsOEJBQXlCLEdBQUcsd0JBQXdCLENBQUM7UUFDckQsbUJBQWMsR0FBRyxlQUFlLENBQUM7UUFDakMsMkJBQXNCLEdBQUcscUJBQXFCLENBQUM7UUFJeEMsc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsd0JBQW1CLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBU3hELENBQUM7SUFFTCx5QkFBeUIsQ0FBQyxVQUFtQjtRQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztJQUN0QyxDQUFDO0lBRUQseUJBQXlCO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoQyxDQUFDO0lBRUQscUJBQXFCLENBQUMsWUFBcUI7UUFDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJO1FBQ2pCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFO1lBQ2hDLE9BQU87U0FDVjtRQUNELE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxHQUFHLEVBQUUsSUFBSTtRQUN2QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvRSxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7WUFDakIsT0FBTyxPQUFPLENBQUM7U0FDbEI7UUFDRCxPQUFPO0lBQ1gsQ0FBQztJQUVELFdBQVcsQ0FBQyxVQUFlO1FBQ3ZCLE1BQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUU5RSxJQUFJLFVBQVUsQ0FBQyxtQkFBbUIsSUFBSSxVQUFVLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3RSxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsR0FBRyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDbEs7UUFFRCxNQUFNLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztRQUNsQyxNQUFNLEdBQUcsR0FBRyxVQUFVLENBQUMsR0FBRyxJQUFJLEVBQUUsQ0FBQztRQUVqQyxNQUFNLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEtBQUssMEJBQTBCLElBQUksRUFBRSxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3ZLLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLGtCQUFrQixDQUFDO1FBRS9ILE1BQU0sb0JBQW9CLEdBQUcsVUFBVSxDQUFDLG9CQUFvQixJQUFJLEVBQUUsQ0FBQztRQUVuRSxNQUFNLHdCQUF3QixHQUFHO1lBQzdCLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLGVBQWUsRUFBRSxNQUFNO1lBQ3ZCLHFCQUFxQixFQUFFLFNBQVM7WUFDaEMscUJBQXFCLEVBQUUsbUJBQW1CO1lBQzFDLGtCQUFrQixFQUFFLFFBQVE7WUFDNUIsc0JBQXNCLEVBQUUsb0JBQW9CO1lBQzVDLGFBQWEsRUFBRSxZQUFZO1lBQzNCLFVBQVUsRUFBRSxVQUFVLENBQUMsVUFBVTtZQUNqQyxnQkFBZ0IsRUFBRSxVQUFVLENBQUMsZ0JBQWdCO1NBQ2hELENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3pJLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakIsSUFBSSxRQUFRLENBQUMsc0JBQXNCLElBQUksUUFBUSxDQUFDLHNCQUFzQixDQUFDLGFBQWE7dUJBQzdFLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsZUFBZSxHQUFHLENBQUM7dUJBQ2pFLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUU7b0JBQy9DLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGtDQUFrQyxDQUNwRyxRQUFRLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQywrQkFBK0IsRUFBRSxDQUFDLENBQUM7b0JBQzVHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGtDQUFrQyxDQUNwRyxRQUFRLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyw4QkFBOEIsRUFBRSxDQUFDLENBQUM7aUJBQzlHO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWMsQ0FBQyxTQUFpQjtRQUM1QixNQUFNLFNBQVMsR0FBRztZQUNkLFlBQVksRUFBRTtnQkFDVixFQUFFLEVBQUUsU0FBUzthQUNoQjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLFNBQVMsQ0FBQztpQkFDN0UsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDaEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUNBQWlDLENBQUMsU0FBUztRQUN2QyxNQUFNLFNBQVMsR0FBRztZQUNkLFdBQVcsRUFBRSxTQUFTO1NBQ3pCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyx5Q0FBeUMsRUFBRSxJQUFJLENBQUMseUNBQXlDLEVBQUUsU0FBUyxDQUFDO2lCQUNuSSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFvQixDQUFDLFNBQWlCO1FBQ2xDLE1BQU0sU0FBUyxHQUFHO1lBQ2QsU0FBUyxFQUFFLFNBQVM7U0FDdkIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSwyQkFBMkIsRUFBRSxTQUFTLENBQUM7aUJBQ2xGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDVixZQUFZLEVBQUUsUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUM3SixVQUFVLEVBQUUsUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO2lCQUN4SixDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsY0FBZ0MsRUFBRSxhQUFhLEVBQUUsT0FBYTtRQUMvRSxNQUFNLFNBQVMsR0FBRztZQUNkLGFBQWEsRUFBRSxjQUFjO1lBQzdCLGFBQWEsRUFBRSxhQUFhO1lBQzVCLE9BQU8sRUFBRTtnQkFDTCxzQkFBc0IsRUFBRSxPQUFPO2FBQ2xDO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxhQUFhLEVBQUUsU0FBUyxDQUFDO2lCQUNwRSxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELG9CQUFvQixDQUFDLFFBQVE7UUFDekIsSUFBSSxRQUFRLEVBQUU7WUFDVixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNyRTtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxXQUFtQixFQUFFLFVBQWtCO1FBQ3ZELE1BQU0sU0FBUyxHQUFHO1lBQ2QsV0FBVyxFQUFFLFdBQVc7WUFDeEIsVUFBVSxFQUFFLFVBQVU7U0FDekIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsRUFBRSxTQUFTLENBQUM7Z0JBQzdFLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYyxDQUFDLGVBQWUsRUFBRSxjQUFjLEVBQUUsUUFBUTtRQUNwRCxNQUFNLFNBQVMsR0FBRztZQUNkLGVBQWUsRUFBRSxlQUFlO1lBQ2hDLGNBQWMsRUFBRSxjQUFjO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLFNBQVMsQ0FBQztnQkFDL0UsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjLENBQUMsWUFBb0IsRUFBRSxlQUF1QjtRQUN4RCxNQUFNLFNBQVMsR0FBRztZQUNkLFlBQVksRUFBRSxZQUFZO1lBQzFCLFdBQVcsRUFBRSxtQkFBbUI7WUFDaEMsZUFBZSxFQUFFLGVBQWU7WUFDaEMsTUFBTSxFQUFFLDZEQUE2RDtZQUNyRSxZQUFZLEVBQUU7Z0JBQ1YsUUFBUSxFQUFFLDBCQUEwQjthQUN2QztTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLFNBQVMsQ0FBQztpQkFDbEYsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxNQUFjLEVBQUUsVUFBa0I7UUFDdkQsTUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsTUFBTTtZQUNkLFVBQVUsRUFBRSxVQUFVO1NBRXpCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsU0FBUyxDQUFDO2lCQUN6RixTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNCQUFzQjtRQUNsQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3RELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUM7Z0JBQ3pELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLHlCQUF5QixFQUFFLEVBQUUsQ0FBQztxQkFDL0YsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNsQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO29CQUN0RyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO29CQUM1RSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQzthQUNWO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYztJQUNkLGFBQWEsQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLFFBQVE7UUFDOUIsTUFBTSxTQUFTLEdBQUc7WUFDZCxhQUFhLEVBQUU7Z0JBQ1gsU0FBUyxFQUFFO29CQUNQLEVBQUUsRUFBRSxFQUFFO29CQUNOLE1BQU0sRUFBRSxNQUFNO2lCQUNqQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1AsUUFBUSxFQUFFO3dCQUNOLEVBQUUsRUFBRSxRQUFRO3FCQUNmO2lCQUNKO2FBQ0o7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxTQUFTLENBQUM7aUJBQ2hGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYztJQUNkLGtCQUFrQixDQUFDLFNBQVM7UUFDeEIsTUFBTSxTQUFTLEdBQUc7WUFDZCxTQUFTLEVBQUUsU0FBUztTQUN2QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLFNBQVMsQ0FBQztpQkFDaEcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FFSixDQUFBOztZQS9SMEIsVUFBVTtZQUNULFdBQVc7WUFDWCxXQUFXO1lBQ0gsbUJBQW1CO1lBQ3hCLGNBQWM7WUFDVixtQkFBbUI7OztBQXZDekMsY0FBYztJQUoxQixVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDO0dBRVcsY0FBYyxDQWlVMUI7U0FqVVksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT3RtbU1ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vdG1tLW1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0IH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUHJvamVjdCc7XHJcbmltcG9ydCB7IENvbW1lbnRzVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb21tZW50cy9zZXJ2aWNlcy9jb21tZW50cy51dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0VXBkYXRlT2JqIH0gZnJvbSAnLi4vLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LnVwZGF0ZSc7XHJcbmltcG9ydCB7IFN0YXR1c0xldmVsIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IGlkZW50aWZpZXJNb2R1bGVVcmwgfSBmcm9tICdAYW5ndWxhci9jb21waWxlcic7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9qZWN0U2VydmljZSB7XHJcblxyXG4gICAgUFJPSkVDVF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Qcm9qZWN0L29wZXJhdGlvbnMnO1xyXG4gICAgR0VUX0NPTVBMRVRFRF9NSUxFU1RPTkVTX0JZX1BST0pFQ1RfSURfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICAgIFBST0pFQ1RfRklMVEVSX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9Qcm9qZWN0X0ZpbHRlci9vcGVyYXRpb25zJztcclxuICAgIFBST0pFQ1RfQlBNID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgR0VUX0FMTF9DQVRFR09SWV9NRVRBREFUQV9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQ2F0ZWdvcnlfTWV0YWRhdGEvb3BlcmF0aW9ucyc7XHJcbiAgICBET1dOTE9BRF9FWENFTF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vd3NhcHAvZXhjZWwvMS4wJztcclxuICAgIERFTEVURV9QUk9KRUNUX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgR0VUX1RBU0tfQllfUFJPSkVDVF9JRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9UYXNrL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIERPV05MT0FEX0VYQ0VMX1RFTVBMQVRFID0gJ0Rvd25sb2FkRXhjZWxUZW1wbGF0ZSAnO1xyXG4gICAgXHJcbiAgICAvLyBDUkVBVEVfQlVMS19QUk9KRUNUX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgQ1JFQVRFX0JVTEtfUFJPSkVDVCA9ICdCdWxrUHJvamVjdENyZWF0aW9uJztcclxuXHJcbiAgICBDUkVBVEVfRE9DVU1FTlRfTlMgPSAnaHR0cDovL3NjaGVtYXMuY29yZHlzLmNvbS9kb2N1bWVudHN0b3JlL2RlZmF1bHQvMS4wJztcclxuICAgIENSRUFURV9ET0NVTUVOVCA9ICdDcmVhdGVEb2N1bWVudCc7XHJcblxyXG4gICAgR0VUX0FMTF9QUk9KRUNUU19XUyA9ICdHZXRBbGxQcm9qZWN0cyc7XHJcbiAgICBHRVRfQUxMX1BST0pFQ1RfRklMVEVSUyA9ICdHZXRBbGxQcm9qZWN0RmlsdGVycyc7XHJcbiAgICBHRVRfQ09NUExFVEVEX01JTEVTVE9ORVNfQllfUFJPSkVDVF9JRF9XUyA9ICdHZXRNaWxlc3RvbmVUYXNrc0J5UHJvamVjdCc7XHJcbiAgICBHRVRfUFJPSkVDVFNfQllfSUQgPSAnUmVhZFByb2plY3QnO1xyXG4gICAgUkVTVEFSVF9QUk9KRUNUX1dTID0gJ0hhbmRsZVJlc3RhcnRQcm9qZWN0JztcclxuICAgIEdFVF9BTExfQ0FURUdPUllfTUVUQURBVEEgPSAnR2V0QWxsQ2F0ZWdvcnlNZXRhZGF0YSc7XHJcbiAgICBERUxFVEVfUFJPSkVDVCA9ICdEZWxldGVQcm9qZWN0JztcclxuICAgIEdFVF9UQVNLX0JZX1BST0pFQ1RfSUQgPSAnR2V0VGFza0J5UHJvamVjdElEICc7XHJcblxyXG4gICAgc2VsZWN0ZWRQcm9qZWN0OiBQcm9qZWN0O1xyXG5cclxuICAgIHB1YmxpYyB1cGRhdGVQcm9qZWN0SW5mbyA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgICBvblVwZGF0ZVByb2plY3RJbmZvID0gdGhpcy51cGRhdGVQcm9qZWN0SW5mby5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1NZXRhZGF0YVNlcnZpY2U6IE90bW1NZXRhZGF0YVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgY29tbWVudFV0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHNldFNlbGVjdGVkUHJvamVjdERldGFpbHMocHJvamVjdE9iajogUHJvamVjdCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0ID0gcHJvamVjdE9iajtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTZWxlY3RlZFByb2plY3REZXRhaWxzKCk6IFByb2plY3Qge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGVkUHJvamVjdDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRVcGRhdGVkUHJvamVjdEluZm8oY3Vyck1ldGFkYXRhOiBQcm9qZWN0KSB7XHJcbiAgICAgICAgdGhpcy51cGRhdGVQcm9qZWN0SW5mby5uZXh0KGN1cnJNZXRhZGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHkob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgaWYgKCFvYmogfHwgIXBhdGggfHwgIW9iai5tZXRhZGF0YSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5SWQob2JqLm1ldGFkYXRhLCBwYXRoLCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb252ZXJUb0xvY2FsRGF0ZShvYmosIHBhdGgpIHtcclxuICAgICAgICBjb25zdCBkYXRlT2JqID0gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKG9iai5tZXRhZGF0YSwgcGF0aCk7XHJcbiAgICAgICAgaWYgKGRhdGVPYmogIT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZU9iajtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RzKHBhcmFtZXRlcnM6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgc2VhcmNoQ29uZGl0aW9uTGlzdCA9IEpTT04ucGFyc2UocGFyYW1ldGVycy5kZWZhdWx0U2VhcmNoQ29uZGl0aW9uTGlzdCk7XHJcblxyXG4gICAgICAgIGlmIChwYXJhbWV0ZXJzLnNlYXJjaENvbmRpdGlvbkxpc3QgJiYgcGFyYW1ldGVycy5zZWFyY2hDb25kaXRpb25MaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9uTGlzdC5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbiA9IHNlYXJjaENvbmRpdGlvbkxpc3Quc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24uY29uY2F0KHBhcmFtZXRlcnMuc2VhcmNoQ29uZGl0aW9uTGlzdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBza2lwID0gcGFyYW1ldGVycy5za2lwIHx8IDA7XHJcbiAgICAgICAgY29uc3QgdG9wID0gcGFyYW1ldGVycy50b3AgfHwgMTA7XHJcblxyXG4gICAgICAgIGNvbnN0IGZvbGRlckZpbHRlciA9IHNlYXJjaENvbmRpdGlvbkxpc3Quc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24uc29tZShzYyA9PiBzYy5tZXRhZGF0YV9maWVsZF9pZCA9PT0gJ01QTS5QUk9KRUNULlBST0pFQ1RfVFlQRScgJiYgc2MudmFsdWUgPT09ICdQUk9KRUNUJykgP1xyXG4gICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFNlbGVjdGVkQ2F0ZWdvcnkoKS5QUk9KRUNUX0ZPTERFUl9JRCA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0U2VsZWN0ZWRDYXRlZ29yeSgpLlRFTVBMQVRFX0ZPTERFUl9JRDtcclxuXHJcbiAgICAgICAgY29uc3QgZmFjZXRSZXN0cmljdGlvbkxpc3QgPSBwYXJhbWV0ZXJzLmZhY2V0UmVzdHJpY3Rpb25MaXN0IHx8ICcnO1xyXG5cclxuICAgICAgICBjb25zdCBvdG1tUHJvamVjdHNTZWFyY2hDb25maWcgPSB7XHJcbiAgICAgICAgICAgIGxvYWRfdHlwZTogJ21ldGFkYXRhJyxcclxuICAgICAgICAgICAgbGV2ZWxfb2ZfZGV0YWlsOiAnc2xpbScsXHJcbiAgICAgICAgICAgIGNoaWxkX2NvdW50X2xvYWRfdHlwZTogJ2ZvbGRlcnMnLFxyXG4gICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHNlYXJjaENvbmRpdGlvbkxpc3QsXHJcbiAgICAgICAgICAgIGZvbGRlcl9maWx0ZXJfdHlwZTogJ2RpcmVjdCcsXHJcbiAgICAgICAgICAgIGZhY2V0X3Jlc3RyaWN0aW9uX2xpc3Q6IGZhY2V0UmVzdHJpY3Rpb25MaXN0LFxyXG4gICAgICAgICAgICBmb2xkZXJfZmlsdGVyOiBmb2xkZXJGaWx0ZXIsXHJcbiAgICAgICAgICAgIHNvcnRTdHJpbmc6IHBhcmFtZXRlcnMuc29ydFN0cmluZyxcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogcGFyYW1ldGVycy5zZWFyY2hfY29uZmlnX2lkLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2Uuc2VhcmNoQ3VzdG9tVGV4dChvdG1tUHJvamVjdHNTZWFyY2hDb25maWcsIHNraXAsIHRvcCwgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpKS5cclxuICAgICAgICAgICAgICAgIHN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZS5zZWFyY2hfcmVzdWx0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2Uuc2VhcmNoX3Jlc3VsdC50b3RhbF9oaXRfY291bnQgPiAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UuYXNzZXRfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLmFzc2V0X2xpc3QgPSB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2Uuc2V0UmVmZXJlbmNlVmFsdWVGb3JNZXRhZGF0YUZlaWxkcyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UuYXNzZXRfbGlzdCwgdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldERlZmF1bHRQcm9qZWN0TWV0YWRhdGFGaWVsZHMoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UuYXNzZXRfbGlzdCA9IHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5zZXRSZWZlcmVuY2VWYWx1ZUZvck1ldGFkYXRhRmVpbGRzKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZS5hc3NldF9saXN0LCB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2UuZ2V0Q3VzdG9tUHJvamVjdE1ldGFkYXRhRmllbGRzKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RCeUlkKHByb2plY3RJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxQcm9qZWN0PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAnUHJvamVjdC1pZCc6IHtcclxuICAgICAgICAgICAgICAgIElkOiBwcm9qZWN0SWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX05TLCB0aGlzLkdFVF9QUk9KRUNUU19CWV9JRCwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5Qcm9qZWN0KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbXBsZXRlZE1pbGVzdG9uZXNCeVByb2plY3RJZChwcm9qZWN0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgJ3Byb2plY3RJZCc6IHByb2plY3RJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQ09NUExFVEVEX01JTEVTVE9ORVNfQllfUFJPSkVDVF9JRF9OUywgdGhpcy5HRVRfQ09NUExFVEVEX01JTEVTVE9ORVNfQllfUFJPSkVDVF9JRF9XUywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0RGF0YVZhbHVlcyhwcm9qZWN0SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBQcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0JQTSwgJ0VkaXRQcm9qZWN0RGF0ZVZhbGlkYXRpb24nLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRNYXhEYXRlOiByZXNwb25zZSAmJiByZXNwb25zZVsnRGF0ZSddWydTdGFydERhdGUnXSAmJiB0eXBlb2YgcmVzcG9uc2VbJ0RhdGUnXVsnU3RhcnREYXRlJ10gPT09ICdzdHJpbmcnID8gbmV3IERhdGUocmVzcG9uc2VbJ0RhdGUnXVsnU3RhcnREYXRlJ10pIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kTWluRGF0ZTogcmVzcG9uc2UgJiYgcmVzcG9uc2VbJ0RhdGUnXVsnRW5kRGF0ZSddICYmIHR5cGVvZiByZXNwb25zZVsnRGF0ZSddWydFbmREYXRlJ10gPT09ICdzdHJpbmcnID8gbmV3IERhdGUocmVzcG9uc2VbJ0RhdGUnXVsnRW5kRGF0ZSddKSA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVQcm9qZWN0RGV0YWlscyh1cGRhdGVkUHJvamVjdDogUHJvamVjdFVwZGF0ZU9iaiwgY2FuY2VsQ29tbWVudCwgcmVhc29ucz86IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBQcm9qZWN0T2JqZWN0OiB1cGRhdGVkUHJvamVjdCxcclxuICAgICAgICAgICAgQ2FuY2VsQ29tbWVudDogY2FuY2VsQ29tbWVudCxcclxuICAgICAgICAgICAgUmVhc29uczoge1xyXG4gICAgICAgICAgICAgICAgJ01QTV9TdGF0dXNfUmVhc29uLWlkJzogcmVhc29uc1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQlBNLCAnRWRpdFByb2plY3QnLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZ2V0Q3VyclByb2plY3RTdGF0dXMoc3RhdHVzSWQpOiBTdGF0dXNMZXZlbCB7XHJcbiAgICAgICAgaWYgKHN0YXR1c0lkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRTdGF0dXNMZXZlbEJ5U3RhdHVzSWQoc3RhdHVzSWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBidWxrUHJvamVjdENyZWF0aW9uKGRvY3VtZW50VVJMOiBzdHJpbmcsIHRlbXBsYXRlSUQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBkb2N1bWVudFVSTDogZG9jdW1lbnRVUkwsXHJcbiAgICAgICAgICAgIHRlbXBsYXRlSUQ6IHRlbXBsYXRlSUQsXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQlBNLCAnQnVsa1Byb2plY3RDcmVhdGlvbicsIHBhcmFtZXRlcikuXHJcbiAgICAgICAgICAgICAgICBzdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzdGFydFByb2plY3QocHJvamVjdE1ldGFkYXRhLCBjdXN0b21NZXRhZGF0YSwgY29tbWVudHMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgUHJvamVjdE1ldGFkYXRhOiBwcm9qZWN0TWV0YWRhdGEsXHJcbiAgICAgICAgICAgIEN1c3RvbU1ldGFkYXRhOiBjdXN0b21NZXRhZGF0YSxcclxuICAgICAgICAgICAgQ29tbWVudHM6IGNvbW1lbnRzXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQlBNLCB0aGlzLlJFU1RBUlRfUFJPSkVDVF9XUywgcGFyYW1ldGVyKS5cclxuICAgICAgICAgICAgICAgIHN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVEb2N1bWVudChkb2N1bWVudE5hbWU6IHN0cmluZywgZG9jdW1lbnRDb250ZW50OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgRG9jdW1lbnROYW1lOiBkb2N1bWVudE5hbWUsXHJcbiAgICAgICAgICAgIERlc2N0aXB0aW9uOiBcIlRlc3QgRnJvbSBBbmd1bGFyXCIsXHJcbiAgICAgICAgICAgIERvY3VtZW50Q29udGVudDogZG9jdW1lbnRDb250ZW50LFxyXG4gICAgICAgICAgICBGb2xkZXI6IFwiL29wdC9vcGVudGV4dC9BcHBXb3Jrc1BsYXRmb3JtL2RldmJwbS9jb250ZW50L3VwbG9hZGNvbnRlbnRcIixcclxuICAgICAgICAgICAgJ1Byb3BlcnRpZXMnOiB7XHJcbiAgICAgICAgICAgICAgICBNaW1lVHlwZTogXCJhcHBsaWNhdGlvbi9vY3RldC1zdHJlYW1cIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNSRUFURV9ET0NVTUVOVF9OUywgdGhpcy5DUkVBVEVfRE9DVU1FTlQsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZG93bmxvYWRFbmNvZGVkRXhjZWxGaWxlKHVzZXJJRDogc3RyaW5nLCBjYXRlZ29yeUlEOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgdXNlcklkOiB1c2VySUQsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5SWQ6IGNhdGVnb3J5SURcclxuXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRPV05MT0FEX0VYQ0VMX05TLCB0aGlzLkRPV05MT0FEX0VYQ0VMX1RFTVBMQVRFLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbENhdGVnb3J5TWV0YWRhdGEoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zaGFyaW5nU2VydmljZS5nZXRDYXRlZ29yeU1ldGFkYXRhKCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhdGVnb3J5TWV0YWRhdGEoKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX0NBVEVHT1JZX01FVEFEQVRBX05TLCB0aGlzLkdFVF9BTExfQ0FURUdPUllfTUVUQURBVEEsIHt9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldENhdGVnb3J5TWV0YWRhdGEoYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9DYXRlZ29yeV9NZXRhZGF0YScpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX0NhdGVnb3J5X01ldGFkYXRhJykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gIE1QTVYzLTIwOTRcclxuICAgIGRlbGV0ZVByb2plY3QoaWQsIGl0ZW1JZCwgc3RhdHVzSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgUHJvamVjdE9iamVjdDoge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElkOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IGlkLFxyXG4gICAgICAgICAgICAgICAgICAgIEl0ZW1JZDogaXRlbUlkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgUlBPU3RhdHVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgU3RhdHVzSUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHN0YXR1c0lkXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9QUk9KRUNUX05TLCB0aGlzLkRFTEVURV9QUk9KRUNULCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vICBNUE1WMy0yMDk0XHJcbiAgICBnZXRUYXNrQnlQcm9qZWN0SUQocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIHByb2plY3RJRDogcHJvamVjdElkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9UQVNLX0JZX1BST0pFQ1RfSURfTlMsIHRoaXMuR0VUX1RBU0tfQllfUFJPSkVDVF9JRCwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19