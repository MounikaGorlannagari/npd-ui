import { __decorate } from "tslib";
import { Component, EventEmitter, Input, isDevMode, OnInit, Output, Renderer2, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from '../../mpm-utils/services/util.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import * as $ from 'jquery';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SelectionModel } from '@angular/cdk/collections';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../notification/notification.service';
import { LoaderService } from '../../loader/loader.service';
import { AssetService } from '../shared/services/asset.service';
import { QdsService } from '../../upload/services/qds.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { Observable } from 'rxjs';
import { DeliverableService } from '../tasks/deliverable/deliverable.service';
import { AppService } from '../../mpm-utils/services/app.service';
let AssetListComponent = class AssetListComponent {
    constructor(utilService, renderer, fieldConfigService, sharingService, otmmService, notificationService, loaderService, qdsService, entityAppdefService, taskService, assetService, deliverableService, appService) {
        this.utilService = utilService;
        this.renderer = renderer;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.qdsService = qdsService;
        this.entityAppdefService = entityAppdefService;
        this.taskService = taskService;
        this.assetService = assetService;
        this.deliverableService = deliverableService;
        this.appService = appService;
        this.resizeDisplayableFields = new EventEmitter();
        this.selectedAssetsCallBackHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.openCreativeReview = new EventEmitter();
        this.downloadHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.unSelectedAssetsCallBackHandler = new EventEmitter();
        this.assetListMasterCheckboxHandler = new EventEmitter();
        // selection = new SelectionModel<any>(true, []);
        this.selectedAssets = new SelectionModel(true, []);
        this.displayableMetadataFields = [];
        this.mpmFieldConstants = MPMFieldConstants;
        this.assetDataDataSource = new MatTableDataSource([]);
        this.assetsData = [];
        this.selectAllChecked = false;
        this.assetDescription = "";
        this.onClickEdit = (rowData) => {
            this.currentAsset = rowData;
            this.assetDescription = "";
        };
    }
    onResizeMouseDown(event, column) {
        event.stopPropagation();
        event.preventDefault();
        const start = event.target;
        const pressed = true;
        const startX = event.x;
        const startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    }
    initResizableColumns(start, pressed, startX, startWidth, column) {
        this.renderer.listen('body', 'mousemove', (event) => {
            if (pressed) {
                const width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', (event) => {
            if (pressed) {
                pressed = false;
                this.resizeDisplayableFields.next();
            }
        });
    }
    isSelectedAll() {
        const selectedAssets = [];
        this.assetList.forEach(asset => {
            const selectedAsset = this.selectedAssets.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length === this.totalAssetsCount) {
            this.selectAllChecked = true;
        }
        return selectedAssets.length === this.totalAssetsCount ? true : false;
    }
    isSelectedPartial() {
        const selectedAssets = [];
        this.assetList.forEach(asset => {
            const selectedAsset = this.selectedAssets.selected.find(selectedAsset => selectedAsset.asset_id === asset.asset_id);
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length !== this.totalAssetsCount) {
            this.selectAllChecked = false;
        }
        return selectedAssets.length > 0 && selectedAssets.length !== this.totalAssetsCount ? true : false;
    }
    masterToggle() {
        this.selectAllAssetChecked ? this.checkAllProjects() : this.uncheckAllProjects();
    }
    checkAllProjects() {
        this.assetDataDataSource.data.forEach(row => {
            row.selected = true;
            this.selectedAssets.select(row);
            this.selectedAssetsCallBackHandler.emit(row);
        });
    }
    uncheckAllProjects() {
        this.assetDataDataSource.data.forEach(row => {
            row.selected = false;
            this.selectedAssets.deselect(row);
            this.unSelectedAssetsCallBackHandler.emit(row);
        });
    }
    checkDeliverable(AssetData, event) {
        if (event.checked) {
            AssetData.selected = true;
            this.selectedAssets.select(AssetData);
            this.selectedAssetsCallBackHandler.emit(AssetData);
        }
        else {
            this.selectedAssets.deselect(AssetData);
            this.unSelectedAssetsCallBackHandler.emit(AssetData);
        }
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }
    notificationHandler(event) {
        if (event.message) {
            this.notificationService.error(event.message);
        }
    }
    onClickMoreActions(asset) {
        this.selectedAsset = asset;
    }
    updateDescription() {
        this.loadingHandler(true);
        let assetId = this.currentAsset.asset_id;
        let body = {
            "edited_asset": {
                "data": {
                    "asset_identifier": "string",
                    "metadata": [
                        {
                            "id": "ARTESIA.FIELD.ASSET DESCRIPTION",
                            "name": "Description",
                            "type": "com.artesia.metadata.MetadataField",
                            "value": {
                                "value": {
                                    "type": "string",
                                    "value": this.assetDescription
                                }
                            }
                        }
                    ]
                }
            }
        };
        this.otmmService.updateMetadata(assetId, 'Asset', body).subscribe((res) => {
            this.notificationService.success('metadata Successfully updated');
        }, (error) => {
            this.notificationService.error('Something Went Wrong While Updating the Metadata');
        });
        this.loadingHandler(false);
    }
    ngOnChanges(changes) {
        if (changes.assetDisplayableFields && !changes.assetDisplayableFields.firstChange &&
            (JSON.stringify(changes.assetDisplayableFields.currentValue) !== JSON.stringify(changes.assetDisplayableFields.previousValue))) {
            this.refreshData();
        }
        if (changes && changes.assets && (changes.assets.previousValue !== changes.assets.currentValue)) {
            this.assetList = changes.assets.currentValue;
            this.totalAssetsCount = this.assetList.length;
        }
        if (changes.assets) {
            this.assetDataDataSource.data = [];
            this.assets.forEach(eachAsset => {
                if (eachAsset.inherited_metadata_collections && eachAsset.inherited_metadata_collections[0]) {
                    eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list.concat(eachAsset.inherited_metadata_collections[0].inherited_metadata_values);
                }
                else {
                    eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list;
                }
            });
            this.assetDataDataSource.data = this.assets;
        }
    }
    findDocType(fileName) {
        return this.assetService.findIconByName(fileName);
    }
    refreshData() {
        this.assetDataDataSource.data = [];
        this.assets.forEach(eachAsset => {
            var _a, _b;
            if (eachAsset.inherited_metadata_collections && eachAsset.inherited_metadata_collections[0]) {
                eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list.concat((_b = (_a = eachAsset) === null || _a === void 0 ? void 0 : _a.inherited_metadata_collections[0]) === null || _b === void 0 ? void 0 : _b.inherited_metadata_values);
            }
            else {
                eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list;
            }
        });
        this.assetDataDataSource.data = this.assets;
        this.displayableMetadataFields = this.assetConfig.assetViewConfig ?
            this.utilService.getDisplayOrderData(this.assetDisplayableFields, this.assetConfig.assetViewConfig.listFieldOrder) : [];
        this.displayColumns = this.displayableMetadataFields.map((column) => column.INDEXER_FIELD_ID);
        this.displayColumns.unshift('Image');
        this.displayColumns.unshift('Select');
        this.displayColumns.push('Actions');
        const orderedFields = [];
        this.displayColumns.forEach(column => {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(this.assetDisplayableFields.find(displayableField => displayableField.INDEXER_FIELD_ID === column));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    }
    download(asset) {
        this.downloadHandler.next(asset);
    }
    getDeliverableDetails(deliverableId) {
        return new Observable(observer => {
            this.deliverableService.readDeliverable(deliverableId)
                .subscribe(response => {
                observer.next(response.Deliverable);
                observer.complete();
            }, error => {
                observer.error();
            });
        });
    }
    getAssetVersions() {
        this.otmmService.getAssetVersionsByAssetId(this.selectedAsset.asset_id).subscribe(response => {
            this.assetVersionsCallBackHandler.next(response[0]);
        });
    }
    getAssetDetails() {
        this.assetDetailsCallBackHandler.next(this.selectedAsset);
    }
    getAssetPreview() {
        this.assetPreviewCallBackHandler.next(this.selectedAsset);
    }
    getProperty(asset, property) {
        return this.taskService.getProperty(asset, property);
    }
    getFileProofingSessionData(asset, appId, deliverableId, assetId, crRoleId, versioning, taskName) {
        return new Observable(observer => {
            this.appService.getFileProofingSession(appId, deliverableId, assetId, taskName, crRoleId, versioning)
                .subscribe(response => {
                if (response && response.sessionDetails && response.sessionDetails.sessionUrl) {
                    this.loaderService.hide();
                    const crData = {
                        url: response.sessionDetails.sessionUrl,
                        headerInfos: [{
                                title: 'Name',
                                value: this.getProperty(asset, DeliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID),
                                type: 'string'
                            }]
                    };
                    observer.next(crData);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while opening Creative Review.');
                }
            }, error => {
                observer.error(error);
            });
        });
    }
    openCR(asset) {
        this.openCreativeReview.next(asset);
    }
    getColumnProperty(column, data) {
        for (let row = 0; row < data.length; row++) {
            if (column.OTMM_FIELD_ID === data[row].id) {
                if (data[row].value && data[row].value.value && data[row].value.value.value) {
                    return data[row].value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value) {
                    return data[row].metadata_element.value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.field_value) {
                    return data[row].metadata_element.value.value.field_value.value;
                }
            }
        }
        return 'NA';
    }
    shareAsset(asset) {
        this.shareAssetsCallbackHandler.next([asset]);
    }
    ngOnInit() {
        this.assetList = this.assets;
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.displayColumns = this.assetDisplayableFields.map((column) => column.INDEXER_FIELD_ID);
        this.crActionData = this.sharingService.getCRActions();
        this.refreshData();
    }
};
AssetListComponent.ctorParameters = () => [
    { type: UtilService },
    { type: Renderer2 },
    { type: FieldConfigService },
    { type: SharingService },
    { type: OTMMService },
    { type: NotificationService },
    { type: LoaderService },
    { type: QdsService },
    { type: EntityAppDefService },
    { type: TaskService },
    { type: AssetService },
    { type: DeliverableService },
    { type: AppService }
];
__decorate([
    Input()
], AssetListComponent.prototype, "selectedAssetData", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "isVersion", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "selectAllDeliverables", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "assets", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "assetDisplayableFields", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "assetConfig", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "projectData", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "taskData", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "isAllSelected", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "isAssetListView", void 0);
__decorate([
    Input()
], AssetListComponent.prototype, "isPartialSelected", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "resizeDisplayableFields", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "selectedAssetsCallBackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "crHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "shareAssetsCallbackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "orderedDisplayableFields", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "openCreativeReview", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "downloadHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "assetPreviewCallBackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "assetDetailsCallBackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "assetVersionsCallBackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "unSelectedAssetsCallBackHandler", void 0);
__decorate([
    Output()
], AssetListComponent.prototype, "assetListMasterCheckboxHandler", void 0);
AssetListComponent = __decorate([
    Component({
        selector: 'mpm-asset-list',
        template: "<table *ngIf=\"displayableMetadataFields.length > 0 && assetDataDataSource.data.length > 0\" class=\"project-list\" mat-table [dataSource]=\"assetDataDataSource.data\" matSort multiTemplateDataRows cdkDropList cdkDropListOrientation=\"horizontal\">\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllAssetChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"isSelectedAll()\" [indeterminate]=\"isSelectedPartial()\">\r\n            </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"row.selected\" (click)=\"$event.stopPropagation()\" (change)=\"checkDeliverable(row,$event)\" [checked]=\"selectedAssets.isSelected(row)\">\r\n            </mat-checkbox>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Image\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <span>Image</span>\r\n            <span class=\"ui-column-resizer\" (click)=\"$event.stopPropagation()\">\r\n                <mat-icon cdkDragHandle>more_vert</mat-icon>\r\n            </span>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div *ngIf=\"row.rendition_content && row.rendition_content.thumbnail_content && row.rendition_content.thumbnail_content.url\">\r\n                <div class=\"asset-holder\">\r\n                    <img src=\"{{row.rendition_content.thumbnail_content.url}}\" alt=\"asset\" style=\"height: 50px;\">\r\n                </div>\r\n            </div>\r\n            <div *ngIf=\"!row.rendition_content || !row.rendition_content.thumbnail_content\">\r\n                <div class=\"asset-holder\">\r\n                    <mat-icon style=\"height: 50px;\" matTooltip=\"{{row.name}}\">\r\n                        {{findDocType(row.name)}}\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\" cdkDrag [style.min-width.px]=\"column.width\">\r\n            <span>{{column.DISPLAY_NAME}}</span>\r\n            <span class=\"ui-column-resizer\" (click)=\"$event.stopPropagation()\" (mousedown)=\"onResizeMouseDown($event, column)\">\r\n                <mat-icon cdkDragHandle>more_vert</mat-icon>\r\n            </span>\r\n        </th>\r\n\r\n        <td mat-cell *matCellDef=\"let row\" [style.max-width.px]=\"column.width\">\r\n            <span>\r\n            <div >\r\n                <span *ngIf=\"column.DATA_TYPE ==='dateTime' && getColumnProperty(column,row.allMetadata) != 'NA'\">{{getColumnProperty(column,row.allMetadata) | date:'yyyy/MM/dd'}}</span>\r\n            <span *ngIf=\"column.DATA_TYPE ==='dateTime' && getColumnProperty(column,row.allMetadata) === 'NA'\">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <span *ngIf=\"column.MAPPER_NAME !='DESCRIPTION' && column.DATA_TYPE !='dateTime' \">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <span *ngIf=\"column.MAPPER_NAME==='DESCRIPTION'\" class=\"asset-description\">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <button *ngIf=\"column.MAPPER_NAME==='DESCRIPTION'\" mat-icon-button (click)=\"onClickEdit(row)\" [matMenuTriggerFor]=\"DescriptionEditor\">\r\n                    <mat-icon matTooltip=\"Edit Asset Description\">edit</mat-icon>\r\n                </button>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div class=\"min-width-action flex-card\">\r\n                <button mat-icon-button matTooltip=\"open creative review\" (click)=\"openCR(row)\">\r\n                    <mat-icon>rate_review</mat-icon>\r\n                </button>\r\n                <button mat-icon-button matTooltip=\"download asset\" (click)=\"download(row)\">\r\n                    <mat-icon>get_app</mat-icon>\r\n                </button>\r\n\r\n                <button mat-icon-button matTooltip=\"share asset\" (click)=\"shareAsset(row)\">\r\n                    <mat-icon>share</mat-icon>\r\n                </button>\r\n                <button mat-icon-button color=\"primary\" matTooltip=\"More Actions\" (click)=\"onClickMoreActions(row)\" [matMenuTriggerFor]=\"AssetMenu\">\r\n                    <mat-icon matSuffix>more_vert</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns sticky: true\"></tr>\r\n    <tr mat-row *matRowDef=\"let row; columns:displayColumns;\"></tr>\r\n\r\n</table>\r\n\r\n<div class=\"pagination\">\r\n    <button color=\"primary\" *ngIf=\"displayableMetadataFields.length === 0\" disabled mat-flat-button>No Columns Configured</button>\r\n</div>\r\n\r\n<mat-menu #DescriptionEditor=\"matMenu\">\r\n    <div>\r\n        <mat-form-field class=\"description-edit-field\" appearance=\"fill\">\r\n            <mat-label>Asset Description</mat-label>\r\n            <input matInput type=\"text\" placeholder=\"Enter Asset Description\" style=\"border: 2px;\" (click)=\"$event.stopPropagation()\" [(ngModel)]=\"assetDescription\">\r\n            <button matSuffix mat-icon-button aria-label=\"Clear\">\r\n              <mat-icon>close</mat-icon>\r\n            </button>\r\n            <button matSuffix mat-icon-button aria-label=\"Clear\" (click)=\"updateDescription()\">\r\n                <mat-icon>save</mat-icon>\r\n            </button>\r\n        </mat-form-field>\r\n    </div>\r\n</mat-menu>\r\n\r\n<mat-menu #AssetMenu=\"matMenu\">\r\n    <button mat-menu-item (click)=\"getAssetDetails()\">\r\n        <mat-icon>info</mat-icon>\r\n        <span>View Details</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetVersions()\">\r\n        <mat-icon>collections</mat-icon>\r\n        <span>View Versions</span>\r\n    </button>\r\n    <button mat-menu-item *ngIf=\"((selectedAsset && selectedAsset.rendition_content && selectedAsset.rendition_content.preview_content !==undefined && selectedAsset.rendition_content.preview_content.mime_type !='audio/mpeg')||\r\n       (selectedAsset && selectedAsset.rendition_content && selectedAsset.rendition_content.pdf_preview_content !==undefined && !selectedAsset.rendition_content.pdf_preview_content))\" (click)=\"getAssetPreview()\">\r\n        <mat-icon>remove_red_eye</mat-icon>\r\n        <span>Preview</span>\r\n    </button>\r\n</mat-menu>",
        styles: ["table.project-list{width:100%}table.project-list tr{cursor:pointer}table.project-list tr .mat-checkbox{padding:0 8px}table.project-list tr th{padding-left:10px;padding-right:10px}table.project-list tr td:last-child{padding-left:10px}table.project-list tr td span div{display:flex;align-items:center}table.project-list tr td{padding-left:15px;padding-right:10px}table.project-list tr td a.mat-button{padding:0;text-align:left}table.project-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.project-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}table.project-list tr td mat-progress-bar{max-width:7em}.pagination{width:85vw;display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;align-items:center;margin-bottom:20px;margin-top:20px}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.project-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.project-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.project-detail{padding:0!important}tr.project-detail-row{height:0}.project-element-row td{border-bottom-width:0}.project-element-detail{overflow:hidden;display:flex;flex-wrap:wrap;cursor:auto!important}.project-element-diagram{min-width:80px;padding:8px;font-weight:lighter;margin:8px 0}.task-info{min-width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex}.task-info.data{justify-content:left}.task-info.no-data{justify-content:center}td.wrap-text-custom span>div{max-width:12em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:9em}.min-width-action ::ng-deep .mat-icon-button{max-width:30px!important}.mattooltipclass{background-color:red;margin-left:70px}.campaignLink:hover{cursor:pointer;-webkit-text-decoration-line:underline;text-decoration-line:underline}::ng-deep .mat-sort-header-button{position:unset!important}th.mat-header-cell:hover .ui-column-resizer{display:block}.ui-column-resizer{display:none;position:absolute;right:0;padding:0;cursor:col-resize}.cdk-drag-preview{box-sizing:border-box;position:relative;padding-top:20px;white-space:nowrap}.cdk-drag-preview::after{content:\"\";position:absolute;top:0;bottom:0;left:0;right:0;border-radius:4px;border:1px solid rgba(0,0,0,.4);box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{color:transparent;position:relative;transition:transform 250ms cubic-bezier(0,0,.2,1)}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.cdk-drag-placeholder::after{content:\"\";position:absolute;top:0;bottom:0;left:0;right:0;border-radius:4px;background:rgba(0,0,0,.1);border:1px dashed rgba(0,0,0,.4)}table.project-table-list{width:100rem}.asset-description{width:60px;text-overflow:ellipsis;overflow:hidden}"]
    })
], AssetListComponent);
export { AssetListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L2Fzc2V0LWxpc3QvYXNzZXQtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTdELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUMvRSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUVoRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFNUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFbEYsT0FBTyxFQUFZLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUc5RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFRbEUsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFFN0IsWUFDVSxXQUF3QixFQUN4QixRQUFtQixFQUNuQixrQkFBc0MsRUFDdEMsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixZQUEwQixFQUMxQixrQkFBc0MsRUFDdEMsVUFBc0I7UUFadEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUNuQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFjdEIsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUNwRCxrQ0FBNkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3hELGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BDLCtCQUEwQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckQsNkJBQXdCLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUNyRCx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzdDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxQyxnQ0FBMkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3RELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsaUNBQTRCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2RCxvQ0FBK0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFELG1DQUE4QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFFbkUsaURBQWlEO1FBQ2pELG1CQUFjLEdBQUcsSUFBSSxjQUFjLENBQU0sSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ25ELDhCQUF5QixHQUFvQixFQUFFLENBQUM7UUFFaEQsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFHdEMsd0JBQW1CLEdBQUcsSUFBSSxrQkFBa0IsQ0FBTSxFQUFFLENBQUMsQ0FBQztRQUN0RCxlQUFVLEdBQUcsRUFBRSxDQUFDO1FBa0JoQixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFDbEMscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBV3RCLGdCQUFXLEdBQUcsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUN4QixJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQztZQUM1QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FBQTtJQW5FRyxDQUFDO0lBc0RMLGlCQUFpQixDQUFDLEtBQUssRUFBRSxNQUFNO1FBQzdCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUMzQixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDckIsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUN2QixNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBT0Qsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFdBQVcsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ2hELElBQUksT0FBTyxFQUFFO2dCQUNULE1BQU0sS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDOUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYTtRQUNYLE1BQU0sY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMzQixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwSCxJQUFJLGFBQWEsRUFBRTtnQkFDZixjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3RDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7UUFDRCxPQUFPLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUM5RSxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsTUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzNCLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3BILElBQUksYUFBYSxFQUFFO2dCQUNmLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDdEM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDakQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELE9BQU8sY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzNHLENBQUM7SUFFQyxZQUFZO1FBQ1YsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDbkYsQ0FBQztJQUVELGdCQUFnQjtRQUNkLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3hDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakQsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDeEMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuRCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsS0FBSztRQUMvQixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDZixTQUFTLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3hEO0lBRUgsQ0FBQztJQUVELGNBQWMsQ0FBQyxLQUFLO1FBQ2xCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbEUsQ0FBQztJQUVELG1CQUFtQixDQUFDLEtBQUs7UUFDdkIsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDakQ7SUFDSCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBSztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUN6QyxJQUFJLElBQUksR0FBRztZQUNULGNBQWMsRUFBQztnQkFDWixNQUFNLEVBQUM7b0JBQ0osa0JBQWtCLEVBQUMsUUFBUTtvQkFDM0IsVUFBVSxFQUFDO3dCQUNSOzRCQUNHLElBQUksRUFBQyxpQ0FBaUM7NEJBQ3RDLE1BQU0sRUFBQyxhQUFhOzRCQUNwQixNQUFNLEVBQUMsb0NBQW9DOzRCQUMzQyxPQUFPLEVBQUM7Z0NBQ0wsT0FBTyxFQUFDO29DQUNMLE1BQU0sRUFBQyxRQUFRO29DQUNmLE9BQU8sRUFBRSxJQUFJLENBQUMsZ0JBQWdCO2lDQUNoQzs2QkFDSDt5QkFDSDtxQkFDSDtpQkFDSDthQUNIO1NBQ0gsQ0FBQTtRQUNBLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDdEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ3BFLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ1YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1FBQ3RGLENBQUMsQ0FBQyxDQUFDO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLHNCQUFzQixJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLFdBQVc7WUFDN0UsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO1lBQ2hJLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQy9GLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDN0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1NBQy9DO1FBQ0QsSUFBRyxPQUFPLENBQUMsTUFBTSxFQUFDO1lBQ2hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUM5QixJQUFHLFNBQVMsQ0FBQyw4QkFBOEIsSUFBSSxTQUFTLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDLEVBQUM7b0JBQ3pGLFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQ3pLO3FCQUFJO29CQUNILFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQTtpQkFDMUY7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztTQUM3QztJQUNILENBQUM7SUFFRCxXQUFXLENBQUMsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7O1lBQzlCLElBQUcsU0FBUyxDQUFDLDhCQUE4QixJQUFJLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMsRUFBQztnQkFDekYsU0FBUyxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sYUFBQyxTQUFTLDBDQUFFLDhCQUE4QixDQUFDLENBQUMsMkNBQUcseUJBQXlCLENBQUMsQ0FBQzthQUMzSztpQkFBSTtnQkFDSCxTQUFTLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUE7YUFDMUY7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM1QyxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3hILElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQWdCLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLE1BQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxLQUFLLFNBQVMsQ0FBQyxFQUFFO2dCQUN2RSxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDMUg7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELHFCQUFxQixDQUFDLGFBQWE7UUFDakMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQztpQkFDakQsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDcEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3pGLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLLEVBQUUsUUFBUTtRQUN6QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsMEJBQTBCLENBQUMsS0FBSyxFQUFDLEtBQUssRUFBRSxhQUFhLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsUUFBUTtRQUM1RixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7aUJBQ2hHLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDbEIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtvQkFDM0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsTUFBTSxNQUFNLEdBQUc7d0JBQ1gsR0FBRyxFQUFFLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVTt3QkFDdkMsV0FBVyxFQUFFLENBQUM7Z0NBQ1YsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLG9CQUFvQixDQUFDLGlDQUFpQyxDQUFDO2dDQUN0RixJQUFJLEVBQUUsUUFBUTs2QkFDakIsQ0FBQztxQkFDTCxDQUFDO29CQUVGLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO2lCQUN6RTtZQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtnQkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsTUFBTSxDQUFDLEtBQUs7UUFDVixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFHRCxpQkFBaUIsQ0FBQyxNQUFNLEVBQUMsSUFBSTtRQUMzQixLQUFJLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBQztZQUN4QyxJQUFHLE1BQU0sQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBQztnQkFDdkMsSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBQztvQkFDekUsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7aUJBQ3BDO3FCQUNJLElBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFDO29CQUMvSixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDckQ7cUJBQ0ksSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUM7b0JBQ3JLLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztpQkFDakU7YUFDRjtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUMsUUFBUTtRQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFDeEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBZ0IsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDckcsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0NBRUYsQ0FBQTs7WUE3VndCLFdBQVc7WUFDZCxTQUFTO1lBQ0Msa0JBQWtCO1lBQ3RCLGNBQWM7WUFDakIsV0FBVztZQUNILG1CQUFtQjtZQUN6QixhQUFhO1lBQ2hCLFVBQVU7WUFDRCxtQkFBbUI7WUFDM0IsV0FBVztZQUNWLFlBQVk7WUFDTixrQkFBa0I7WUFDMUIsVUFBVTs7QUFFdkI7SUFBUixLQUFLLEVBQUU7NkRBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFO3FEQUFXO0FBQ1Y7SUFBUixLQUFLLEVBQUU7aUVBQXVCO0FBQ3RCO0lBQVIsS0FBSyxFQUFFO2tEQUFRO0FBQ1A7SUFBUixLQUFLLEVBQUU7a0VBQXdCO0FBQ3ZCO0lBQVIsS0FBSyxFQUFFO3VEQUFhO0FBQ1o7SUFBUixLQUFLLEVBQUU7dURBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTtvREFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO3lEQUFlO0FBQ2Q7SUFBUixLQUFLLEVBQUU7MkRBQWlCO0FBQ2hCO0lBQVIsS0FBSyxFQUFFOzZEQUFtQjtBQUVqQjtJQUFULE1BQU0sRUFBRTttRUFBcUQ7QUFDcEQ7SUFBVCxNQUFNLEVBQUU7eUVBQXlEO0FBQ3hEO0lBQVQsTUFBTSxFQUFFO3FEQUFxQztBQUNwQztJQUFULE1BQU0sRUFBRTtzRUFBc0Q7QUFDckQ7SUFBVCxNQUFNLEVBQUU7b0VBQXNEO0FBQ3JEO0lBQVQsTUFBTSxFQUFFOzhEQUE4QztBQUM3QztJQUFULE1BQU0sRUFBRTsyREFBMkM7QUFDMUM7SUFBVCxNQUFNLEVBQUU7dUVBQXVEO0FBQ3REO0lBQVQsTUFBTSxFQUFFO3VFQUF1RDtBQUN0RDtJQUFULE1BQU0sRUFBRTt3RUFBd0Q7QUFDdkQ7SUFBVCxNQUFNLEVBQUU7MkVBQTJEO0FBQzFEO0lBQVQsTUFBTSxFQUFFOzBFQUEwRDtBQXhDeEQsa0JBQWtCO0lBTDlCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxnQkFBZ0I7UUFDMUIsZ3ZOQUEwQzs7S0FFM0MsQ0FBQztHQUNXLGtCQUFrQixDQWdXOUI7U0FoV1ksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBpc0Rldk1vZGUsIE9uSW5pdCwgT3V0cHV0LCBSZW5kZXJlcjIsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vLi4vbGliL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IEFzc2V0U2VydmljZSB9IGZyb20gJy4uL3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrcy90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrcy90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IGZvcmtKb2luLCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU2VydmljZSB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNX1JPTEVTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUm9sZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBmb3JtYXREYXRlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWFzc2V0LWxpc3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9hc3NldC1saXN0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hc3NldC1saXN0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0TGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICBwcml2YXRlIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgIHByaXZhdGUgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgcWRzU2VydmljZTogUWRzU2VydmljZSxcclxuICAgIHByaXZhdGUgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgIHByaXZhdGUgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZSxcclxuICAgIHByaXZhdGUgZGVsaXZlcmFibGVTZXJ2aWNlOiBEZWxpdmVyYWJsZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2VcclxuICApIHsgfVxyXG4gIEBJbnB1dCgpIHNlbGVjdGVkQXNzZXREYXRhO1xyXG4gIEBJbnB1dCgpIGlzVmVyc2lvbjtcclxuICBASW5wdXQoKSBzZWxlY3RBbGxEZWxpdmVyYWJsZXM7XHJcbiAgQElucHV0KCkgYXNzZXRzO1xyXG4gIEBJbnB1dCgpIGFzc2V0RGlzcGxheWFibGVGaWVsZHM7XHJcbiAgQElucHV0KCkgYXNzZXRDb25maWc7XHJcbiAgQElucHV0KCkgcHJvamVjdERhdGE7XHJcbiAgQElucHV0KCkgdGFza0RhdGE7XHJcbiAgQElucHV0KCkgaXNBbGxTZWxlY3RlZDtcclxuICBASW5wdXQoKSBpc0Fzc2V0TGlzdFZpZXc7XHJcbiAgQElucHV0KCkgaXNQYXJ0aWFsU2VsZWN0ZWQ7XHJcblxyXG4gIEBPdXRwdXQoKSByZXNpemVEaXNwbGF5YWJsZUZpZWxkcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGNySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBzaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBvcmRlcmVkRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gIEBPdXRwdXQoKSBvcGVuQ3JlYXRpdmVSZXZpZXcgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgZG93bmxvYWRIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGFzc2V0UHJldmlld0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBhc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgYXNzZXRWZXJzaW9uc0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSB1blNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGFzc2V0TGlzdE1hc3RlckNoZWNrYm94SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAvLyBzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8YW55Pih0cnVlLCBbXSk7XHJcbiAgc2VsZWN0ZWRBc3NldHMgPSBuZXcgU2VsZWN0aW9uTW9kZWw8YW55Pih0cnVlLCBbXSk7XHJcbiAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkczogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcblxyXG4gIG1wbUZpZWxkQ29uc3RhbnRzID0gTVBNRmllbGRDb25zdGFudHM7XHJcbiAgZGlzcGxheUNvbHVtbnM7XHJcbiAgc2VsZWN0QWxsQXNzZXRDaGVja2VkOiBib29sZWFuO1xyXG4gIGFzc2V0RGF0YURhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlPGFueT4oW10pO1xyXG4gIGFzc2V0c0RhdGEgPSBbXTtcclxuICBjdXJyZW50QXNzZXQ7XHJcbiAgZGVsaXZlcmFibGVJZDtcclxuICBzZXNzaW9uSWQ7XHJcbiAgb3RtbUJhc2VVcmw7XHJcbiAgaXNPbkhvbGRQcm9qZWN0O1xyXG4gIHByb2plY3RDb21wbGV0ZWRTdGF0dXM7XHJcbiAgY3VycmVudFVzZXJDUkRhdGE7XHJcbiAgY3JBY3Rpb25EYXRhO1xyXG4gIGN1cnJlbnRVc2VyVGVhbVJvbGVNYXBwaW5nO1xyXG4gIGRlbGl2ZXJhYmxlU3RhdHVzXHJcbiAgdGFza05hbWVcclxuICBjclJvbGVcclxuICB0YXNrSWQ7XHJcbiAgc2VsZWN0ZWRBc3NldDtcclxuICBjdXJyUHJvamVjdElkO1xyXG4gIGFzc2V0TGlzdDtcclxuICB0b3RhbEFzc2V0c0NvdW50O1xyXG4gIHNlbGVjdEFsbENoZWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBhc3NldERlc2NyaXB0aW9uID0gXCJcIjtcclxuICBvblJlc2l6ZU1vdXNlRG93bihldmVudCwgY29sdW1uKSB7XHJcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBjb25zdCBzdGFydCA9IGV2ZW50LnRhcmdldDtcclxuICAgIGNvbnN0IHByZXNzZWQgPSB0cnVlO1xyXG4gICAgY29uc3Qgc3RhcnRYID0gZXZlbnQueDtcclxuICAgIGNvbnN0IHN0YXJ0V2lkdGggPSAkKHN0YXJ0KS5wYXJlbnQoKS53aWR0aCgpO1xyXG4gICAgdGhpcy5pbml0UmVzaXphYmxlQ29sdW1ucyhzdGFydCwgcHJlc3NlZCwgc3RhcnRYLCBzdGFydFdpZHRoLCBjb2x1bW4pO1xyXG4gIH1cclxuXHJcbiAgb25DbGlja0VkaXQgPSAocm93RGF0YSkgPT4ge1xyXG4gICAgdGhpcy5jdXJyZW50QXNzZXQgPSByb3dEYXRhO1xyXG4gICAgdGhpcy5hc3NldERlc2NyaXB0aW9uID0gXCJcIjtcclxuICB9XHJcblxyXG4gIGluaXRSZXNpemFibGVDb2x1bW5zKHN0YXJ0LCBwcmVzc2VkLCBzdGFydFgsIHN0YXJ0V2lkdGgsIGNvbHVtbikge1xyXG4gICAgICB0aGlzLnJlbmRlcmVyLmxpc3RlbignYm9keScsICdtb3VzZW1vdmUnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgY29uc3Qgd2lkdGggPSBzdGFydFdpZHRoICsgKGV2ZW50LnggLSBzdGFydFgpO1xyXG4gICAgICAgICAgICAgIGNvbHVtbi53aWR0aCA9IHdpZHRoO1xyXG4gICAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2V1cCcsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICBwcmVzc2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgdGhpcy5yZXNpemVEaXNwbGF5YWJsZUZpZWxkcy5uZXh0KCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaXNTZWxlY3RlZEFsbCgpIHtcclxuICAgIGNvbnN0IHNlbGVjdGVkQXNzZXRzID0gW107XHJcbiAgICAgICAgdGhpcy5hc3NldExpc3QuZm9yRWFjaChhc3NldCA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLnNlbGVjdGVkQXNzZXRzLnNlbGVjdGVkLmZpbmQoc2VsZWN0ZWRBc3NldCA9PiBzZWxlY3RlZEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEFzc2V0KSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZEFzc2V0cy5wdXNoKHNlbGVjdGVkQXNzZXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXRzLmxlbmd0aCA9PT0gdGhpcy50b3RhbEFzc2V0c0NvdW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0QWxsQ2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZEFzc2V0cy5sZW5ndGggPT09IHRoaXMudG90YWxBc3NldHNDb3VudCA/IHRydWUgOiBmYWxzZTtcclxufVxyXG5cclxuaXNTZWxlY3RlZFBhcnRpYWwoKSB7XHJcbiAgY29uc3Qgc2VsZWN0ZWRBc3NldHMgPSBbXTtcclxuICAgICAgICB0aGlzLmFzc2V0TGlzdC5mb3JFYWNoKGFzc2V0ID0+IHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBc3NldCA9IHRoaXMuc2VsZWN0ZWRBc3NldHMuc2VsZWN0ZWQuZmluZChzZWxlY3RlZEFzc2V0ID0+IHNlbGVjdGVkQXNzZXQuYXNzZXRfaWQgPT09IGFzc2V0LmFzc2V0X2lkKTtcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXQpIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkQXNzZXRzLnB1c2goc2VsZWN0ZWRBc3NldCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldHMubGVuZ3RoICE9PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RBbGxDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZEFzc2V0cy5sZW5ndGggPiAwICYmIHNlbGVjdGVkQXNzZXRzLmxlbmd0aCAhPT0gdGhpcy50b3RhbEFzc2V0c0NvdW50ID8gdHJ1ZSA6IGZhbHNlO1xyXG59XHJcblxyXG4gIG1hc3RlclRvZ2dsZSgpIHtcclxuICAgIHRoaXMuc2VsZWN0QWxsQXNzZXRDaGVja2VkID8gdGhpcy5jaGVja0FsbFByb2plY3RzKCkgOiB0aGlzLnVuY2hlY2tBbGxQcm9qZWN0cygpO1xyXG4gIH1cclxuXHJcbiAgY2hlY2tBbGxQcm9qZWN0cygpIHtcclxuICAgIHRoaXMuYXNzZXREYXRhRGF0YVNvdXJjZS5kYXRhLmZvckVhY2gocm93ID0+IHtcclxuICAgICAgICByb3cuc2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHMuc2VsZWN0KHJvdyk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHJvdyk7XHJcbiAgICB9KTtcclxuICAgIFxyXG59XHJcblxyXG51bmNoZWNrQWxsUHJvamVjdHMoKSB7XHJcbiAgICB0aGlzLmFzc2V0RGF0YURhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiAgICAgICAgcm93LnNlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0cy5kZXNlbGVjdChyb3cpO1xyXG4gICAgICAgIHRoaXMudW5TZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHJvdyk7XHJcbiAgICB9KTtcclxuICAgIFxyXG59XHJcblxyXG5jaGVja0RlbGl2ZXJhYmxlKEFzc2V0RGF0YSwgZXZlbnQpIHtcclxuICBpZiAoZXZlbnQuY2hlY2tlZCkge1xyXG4gICAgICBBc3NldERhdGEuc2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzLnNlbGVjdChBc3NldERhdGEpO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyLmVtaXQoQXNzZXREYXRhKTtcclxuICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzLmRlc2VsZWN0KEFzc2V0RGF0YSk7XHJcbiAgICAgIHRoaXMudW5TZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KEFzc2V0RGF0YSk7XHJcbiAgfVxyXG5cclxufVxyXG5cclxubG9hZGluZ0hhbmRsZXIoZXZlbnQpIHtcclxuICAoZXZlbnQpID8gdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKSA6IHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbn1cclxuXHJcbm5vdGlmaWNhdGlvbkhhbmRsZXIoZXZlbnQpIHtcclxuICBpZiAoZXZlbnQubWVzc2FnZSkge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoZXZlbnQubWVzc2FnZSk7XHJcbiAgfSBcclxufVxyXG5cclxub25DbGlja01vcmVBY3Rpb25zKGFzc2V0KXtcclxuICB0aGlzLnNlbGVjdGVkQXNzZXQgPSBhc3NldDtcclxufVxyXG5cclxudXBkYXRlRGVzY3JpcHRpb24oKSB7XHJcbiAgdGhpcy5sb2FkaW5nSGFuZGxlcih0cnVlKTtcclxuICBsZXQgYXNzZXRJZCA9IHRoaXMuY3VycmVudEFzc2V0LmFzc2V0X2lkO1xyXG4gIGxldCBib2R5ID0ge1xyXG4gICAgXCJlZGl0ZWRfYXNzZXRcIjp7XHJcbiAgICAgICBcImRhdGFcIjp7XHJcbiAgICAgICAgICBcImFzc2V0X2lkZW50aWZpZXJcIjpcInN0cmluZ1wiLFxyXG4gICAgICAgICAgXCJtZXRhZGF0YVwiOltcclxuICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIFwiaWRcIjpcIkFSVEVTSUEuRklFTEQuQVNTRVQgREVTQ1JJUFRJT05cIixcclxuICAgICAgICAgICAgICAgIFwibmFtZVwiOlwiRGVzY3JpcHRpb25cIixcclxuICAgICAgICAgICAgICAgIFwidHlwZVwiOlwiY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFGaWVsZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJ2YWx1ZVwiOntcclxuICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjp7XHJcbiAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjpcInN0cmluZ1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgXCJ2YWx1ZVwiOiB0aGlzLmFzc2V0RGVzY3JpcHRpb25cclxuICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgIF1cclxuICAgICAgIH1cclxuICAgIH1cclxuIH1cclxuICB0aGlzLm90bW1TZXJ2aWNlLnVwZGF0ZU1ldGFkYXRhKGFzc2V0SWQsJ0Fzc2V0Jyxib2R5KS5zdWJzY3JpYmUoKHJlcykgPT4ge1xyXG4gICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ21ldGFkYXRhIFN1Y2Nlc3NmdWxseSB1cGRhdGVkJyk7XHJcbiAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgV2VudCBXcm9uZyBXaGlsZSBVcGRhdGluZyB0aGUgTWV0YWRhdGEnKTtcclxuICB9KTtcclxuICAgIHRoaXMubG9hZGluZ0hhbmRsZXIoZmFsc2UpO1xyXG59XHJcblxyXG5uZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgaWYgKGNoYW5nZXMuYXNzZXREaXNwbGF5YWJsZUZpZWxkcyAmJiAhY2hhbmdlcy5hc3NldERpc3BsYXlhYmxlRmllbGRzLmZpcnN0Q2hhbmdlICYmXHJcbiAgICAgIChKU09OLnN0cmluZ2lmeShjaGFuZ2VzLmFzc2V0RGlzcGxheWFibGVGaWVsZHMuY3VycmVudFZhbHVlKSAhPT0gSlNPTi5zdHJpbmdpZnkoY2hhbmdlcy5hc3NldERpc3BsYXlhYmxlRmllbGRzLnByZXZpb3VzVmFsdWUpKSkge1xyXG4gICAgICB0aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgfVxyXG4gIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuYXNzZXRzICYmIChjaGFuZ2VzLmFzc2V0cy5wcmV2aW91c1ZhbHVlICE9PSBjaGFuZ2VzLmFzc2V0cy5jdXJyZW50VmFsdWUpKSB7XHJcbiAgICB0aGlzLmFzc2V0TGlzdCA9IGNoYW5nZXMuYXNzZXRzLmN1cnJlbnRWYWx1ZTtcclxuICAgIHRoaXMudG90YWxBc3NldHNDb3VudCA9IHRoaXMuYXNzZXRMaXN0Lmxlbmd0aDtcclxuICB9XHJcbiAgaWYoY2hhbmdlcy5hc3NldHMpe1xyXG4gICAgdGhpcy5hc3NldERhdGFEYXRhU291cmNlLmRhdGEgPSBbXTtcclxuICAgIHRoaXMuYXNzZXRzLmZvckVhY2goZWFjaEFzc2V0ID0+IHtcclxuICAgICAgaWYoZWFjaEFzc2V0LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9ucyAmJiBlYWNoQXNzZXQuaW5oZXJpdGVkX21ldGFkYXRhX2NvbGxlY3Rpb25zWzBdKXtcclxuICAgICAgICBlYWNoQXNzZXQuYWxsTWV0YWRhdGEgPSBlYWNoQXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0WzBdLm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5jb25jYXQoZWFjaEFzc2V0LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9uc1swXS5pbmhlcml0ZWRfbWV0YWRhdGFfdmFsdWVzKTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgZWFjaEFzc2V0LmFsbE1ldGFkYXRhID0gZWFjaEFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3RcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmFzc2V0RGF0YURhdGFTb3VyY2UuZGF0YSA9IHRoaXMuYXNzZXRzO1xyXG4gIH1cclxufVxyXG5cclxuZmluZERvY1R5cGUoZmlsZU5hbWUpIHtcclxuICByZXR1cm4gdGhpcy5hc3NldFNlcnZpY2UuZmluZEljb25CeU5hbWUoZmlsZU5hbWUpO1xyXG59XHJcblxyXG5yZWZyZXNoRGF0YSgpIHtcclxuICB0aGlzLmFzc2V0RGF0YURhdGFTb3VyY2UuZGF0YSA9IFtdO1xyXG4gIHRoaXMuYXNzZXRzLmZvckVhY2goZWFjaEFzc2V0ID0+IHtcclxuICAgIGlmKGVhY2hBc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnMgJiYgZWFjaEFzc2V0LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9uc1swXSl7XHJcbiAgICAgIGVhY2hBc3NldC5hbGxNZXRhZGF0YSA9IGVhY2hBc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0LmNvbmNhdChlYWNoQXNzZXQ/LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9uc1swXT8uaW5oZXJpdGVkX21ldGFkYXRhX3ZhbHVlcyk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgZWFjaEFzc2V0LmFsbE1ldGFkYXRhID0gZWFjaEFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3RcclxuICAgIH1cclxuICB9KTtcclxuICB0aGlzLmFzc2V0RGF0YURhdGFTb3VyY2UuZGF0YSA9IHRoaXMuYXNzZXRzO1xyXG4gIHRoaXMuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IHRoaXMuYXNzZXRDb25maWcuYXNzZXRWaWV3Q29uZmlnID9cclxuICAgICAgdGhpcy51dGlsU2VydmljZS5nZXREaXNwbGF5T3JkZXJEYXRhKHRoaXMuYXNzZXREaXNwbGF5YWJsZUZpZWxkcywgdGhpcy5hc3NldENvbmZpZy5hc3NldFZpZXdDb25maWcubGlzdEZpZWxkT3JkZXIpIDogW107XHJcbiAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMgPSB0aGlzLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMubWFwKChjb2x1bW46IE1QTUZpZWxkKSA9PiBjb2x1bW4uSU5ERVhFUl9GSUVMRF9JRCk7XHJcbiAgdGhpcy5kaXNwbGF5Q29sdW1ucy51bnNoaWZ0KCdJbWFnZScpO1xyXG4gIHRoaXMuZGlzcGxheUNvbHVtbnMudW5zaGlmdCgnU2VsZWN0Jyk7XHJcbiAgdGhpcy5kaXNwbGF5Q29sdW1ucy5wdXNoKCdBY3Rpb25zJyk7XHJcbiAgY29uc3Qgb3JkZXJlZEZpZWxkcyA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICAgICAgICBpZiAoIShjb2x1bW4gPT09ICdTZWxlY3QnIHx8IGNvbHVtbiA9PT0gJ0V4cGFuZCcgfHwgY29sdW1uID09PSAnQWN0aW9ucycpKSB7XHJcbiAgICAgICAgICAgICAgICBvcmRlcmVkRmllbGRzLnB1c2godGhpcy5hc3NldERpc3BsYXlhYmxlRmllbGRzLmZpbmQoZGlzcGxheWFibGVGaWVsZCA9PiBkaXNwbGF5YWJsZUZpZWxkLklOREVYRVJfRklFTERfSUQgPT09IGNvbHVtbikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcmRlcmVkRGlzcGxheWFibGVGaWVsZHMubmV4dChvcmRlcmVkRmllbGRzKTtcclxufVxyXG5cclxuZG93bmxvYWQoYXNzZXQpIHtcclxuICB0aGlzLmRvd25sb2FkSGFuZGxlci5uZXh0KGFzc2V0KTtcclxufVxyXG5cclxuZ2V0RGVsaXZlcmFibGVEZXRhaWxzKGRlbGl2ZXJhYmxlSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHRoaXMuZGVsaXZlcmFibGVTZXJ2aWNlLnJlYWREZWxpdmVyYWJsZShkZWxpdmVyYWJsZUlkKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5EZWxpdmVyYWJsZSk7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmdldEFzc2V0VmVyc2lvbnMoKSB7XHJcbiAgdGhpcy5vdG1tU2VydmljZS5nZXRBc3NldFZlcnNpb25zQnlBc3NldElkKHRoaXMuc2VsZWN0ZWRBc3NldC5hc3NldF9pZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5hc3NldFZlcnNpb25zQ2FsbEJhY2tIYW5kbGVyLm5leHQocmVzcG9uc2VbMF0pO1xyXG4gIH0pO1xyXG59XHJcblxyXG5nZXRBc3NldERldGFpbHMoKSB7XHJcbiAgdGhpcy5hc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIubmV4dCh0aGlzLnNlbGVjdGVkQXNzZXQpO1xyXG59XHJcblxyXG5nZXRBc3NldFByZXZpZXcoKSB7XHJcbiAgdGhpcy5hc3NldFByZXZpZXdDYWxsQmFja0hhbmRsZXIubmV4dCh0aGlzLnNlbGVjdGVkQXNzZXQpO1xyXG59XHJcblxyXG5nZXRQcm9wZXJ0eShhc3NldCwgcHJvcGVydHkpIHtcclxuICByZXR1cm4gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eShhc3NldCwgcHJvcGVydHkpO1xyXG59XHJcblxyXG5nZXRGaWxlUHJvb2ZpbmdTZXNzaW9uRGF0YShhc3NldCxhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgY3JSb2xlSWQsIHZlcnNpb25pbmcsIHRhc2tOYW1lKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0RmlsZVByb29maW5nU2Vzc2lvbihhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgdGFza05hbWUsIGNyUm9sZUlkLCB2ZXJzaW9uaW5nKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnNlc3Npb25EZXRhaWxzICYmIHJlc3BvbnNlLnNlc3Npb25EZXRhaWxzLnNlc3Npb25VcmwpIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgY29uc3QgY3JEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdXJsOiByZXNwb25zZS5zZXNzaW9uRGV0YWlscy5zZXNzaW9uVXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgaGVhZGVySW5mb3M6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdOYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRQcm9wZXJ0eShhc3NldCwgRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfTkFNRV9NRVRBREFUQUZJRUxEX0lEKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoY3JEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgb3BlbmluZyBDcmVhdGl2ZSBSZXZpZXcuJyk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgIH0pO1xyXG4gIH0pO1xyXG59XHJcblxyXG5vcGVuQ1IoYXNzZXQpe1xyXG4gIHRoaXMub3BlbkNyZWF0aXZlUmV2aWV3Lm5leHQoYXNzZXQpO1xyXG59XHJcblxyXG5cclxuZ2V0Q29sdW1uUHJvcGVydHkoY29sdW1uLGRhdGEpe1xyXG4gIGZvcihsZXQgcm93ID0gMDsgcm93IDwgZGF0YS5sZW5ndGg7IHJvdysrKXtcclxuICAgIGlmKGNvbHVtbi5PVE1NX0ZJRUxEX0lEID09PSBkYXRhW3Jvd10uaWQpe1xyXG4gICAgICBpZihkYXRhW3Jvd10udmFsdWUgJiYgZGF0YVtyb3ddLnZhbHVlLnZhbHVlICYmIGRhdGFbcm93XS52YWx1ZS52YWx1ZS52YWx1ZSl7XHJcbiAgICAgICAgcmV0dXJuIGRhdGFbcm93XS52YWx1ZS52YWx1ZS52YWx1ZTtcclxuICAgICAgfVxyXG4gICAgICBlbHNlIGlmKGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50ICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlLnZhbHVlKXtcclxuICAgICAgICByZXR1cm4gZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUudmFsdWUudmFsdWU7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZSBpZihkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudCAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZS5maWVsZF92YWx1ZSl7XHJcbiAgICAgICAgcmV0dXJuIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlLmZpZWxkX3ZhbHVlLnZhbHVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJldHVybiAnTkEnO1xyXG59XHJcblxyXG5zaGFyZUFzc2V0KGFzc2V0KSB7XHJcbiAgdGhpcy5zaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KFthc3NldF0pO1xyXG59XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5hc3NldExpc3QgPSB0aGlzLmFzc2V0cztcclxuICAgIHRoaXMub3RtbUJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgIHRoaXMuZGlzcGxheUNvbHVtbnMgPSB0aGlzLmFzc2V0RGlzcGxheWFibGVGaWVsZHMubWFwKChjb2x1bW46IE1QTUZpZWxkKSA9PiBjb2x1bW4uSU5ERVhFUl9GSUVMRF9JRCk7XHJcbiAgICB0aGlzLmNyQWN0aW9uRGF0YSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q1JBY3Rpb25zKCk7XHJcbiAgICB0aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=