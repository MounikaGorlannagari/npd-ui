import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AssetMetadataHelper } from '../shared/services/asset.metadata.helper';
import { SharingService } from '../../mpm-utils/services/sharing.service';
let AssetDetailsComponent = class AssetDetailsComponent {
    constructor(otmmService, assetMetdataHelper, sharingService) {
        this.otmmService = otmmService;
        this.assetMetdataHelper = assetMetdataHelper;
        this.sharingService = sharingService;
        this.data = {
            displayableMetadataFields: [],
            displayableInheritedMetadataFields: []
        };
    }
    ngOnInit() {
        this.data = this.assetMetdataHelper.getAssetDetailsMetadata(this.asset.data, this.asset.metadata_fields.R_PM_LIST_VIEW_MPM_FIELDS, this.asset.isTemplate);
        //this.asset.metadata_fields.R_PM_LIST_VIEW_MPM_FIELDS
        //this.asset.data
        /*this.otmmService.getOTMMAssetById(this.asset.asset_id, 'custom')
          .subscribe(response => {
            if (response.inherited_metadata_collections) {
              this.assetInheritedData = response.inherited_metadata_collections;
              this.data = this.assetMetdataHelper.getListDisplayColumnConfig(this.asset.metadataFields, response.metadata, this.assetInheritedData, false);
            }
          });*/
    }
};
AssetDetailsComponent.ctorParameters = () => [
    { type: OTMMService },
    { type: AssetMetadataHelper },
    { type: SharingService }
];
__decorate([
    Input()
], AssetDetailsComponent.prototype, "asset", void 0);
AssetDetailsComponent = __decorate([
    Component({
        selector: 'mpm-asset-details',
        template: "<div class=\"metadataList\" *ngFor=\"let metadata of data.displayableMetadataFields\">\r\n    <span class=\"metadata-element\">{{metadata.MetadataFieldGroup}}</span>\r\n    <mat-list role=\"list\">\r\n        <mat-list-item class=\"asset-detail\" role=\"listitem\" *ngFor=\"let element of metadata.MetadataFields\">\r\n            <mat-label class=\"property-name asset-detail-title card-text-title\">\r\n                {{element.name}}:\r\n            </mat-label>\r\n            <mat-label class=\"card-text-value\">\r\n                {{(element.value) ? element.value : 'NA'}}\r\n            </mat-label>\r\n        </mat-list-item>\r\n    </mat-list>\r\n</div>\r\n<div class=\"metadataList\" *ngFor=\"let inheritedMetadata of data.displayableInheritedMetadataFields\">\r\n    <span class=\"metadata-element\">{{inheritedMetadata.MetadataFieldGroup}}</span>\r\n    <mat-list role=\"list\">\r\n        <mat-list-item class=\"asset-detail\" role=\"listitem\" *ngFor=\"let element of inheritedMetadata.MetadataFields\">\r\n            <mat-label class=\"property-name asset-detail-title card-text-title\">\r\n                {{element.metadata_element.name}}:\r\n            </mat-label>&nbsp;\r\n            <mat-label class=\"card-text-value\">\r\n                {{(element.metadata_element.value) && (element.metadata_element.value.value) ? element.metadata_element.value.value.value : 'NA'}}\r\n            </mat-label>\r\n        </mat-list-item>\r\n    </mat-list>\r\n</div>",
        styles: [".metadataList .metadata-element{font-weight:700}.metadataList mat-list{display:flex;flex-wrap:wrap}.metadataList mat-list mat-list-item{flex:0 50%}.asset-detail{height:auto!important}"]
    })
], AssetDetailsComponent);
export { AssetDetailsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L2Fzc2V0LWRldGFpbHMvYXNzZXQtZGV0YWlscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUdwRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFPMUUsSUFBYSxxQkFBcUIsR0FBbEMsTUFBYSxxQkFBcUI7SUFVaEMsWUFDUyxXQUF3QixFQUN4QixrQkFBdUMsRUFDdkMsY0FBOEI7UUFGOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUN2QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFUdkMsU0FBSSxHQUFHO1lBQ0wseUJBQXlCLEVBQUUsRUFBRTtZQUM3QixrQ0FBa0MsRUFBRSxFQUFFO1NBQ3ZDLENBQUM7SUFPRSxDQUFDO0lBRUwsUUFBUTtRQUNOLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUosc0RBQXNEO1FBQ3RELGlCQUFpQjtRQUNqQjs7Ozs7O2VBTU87SUFDVCxDQUFDO0NBRUYsQ0FBQTs7WUFsQnVCLFdBQVc7WUFDSixtQkFBbUI7WUFDdkIsY0FBYzs7QUFYOUI7SUFBUixLQUFLLEVBQUU7b0RBQWM7QUFGWCxxQkFBcUI7SUFMakMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLG1CQUFtQjtRQUM3Qix1OUNBQTZDOztLQUU5QyxDQUFDO0dBQ1cscUJBQXFCLENBNkJqQztTQTdCWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL2Fzc2V0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NTVBNRGF0YVR5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvT1RNTU1QTURhdGFUeXBlcyc7XHJcbmltcG9ydCB7IEFzc2V0TWV0YWRhdGFIZWxwZXIgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvYXNzZXQubWV0YWRhdGEuaGVscGVyJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWFzc2V0LWRldGFpbHMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9hc3NldC1kZXRhaWxzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hc3NldC1kZXRhaWxzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0RGV0YWlsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBhc3NldDtcclxuXHJcbiAgZGF0YSA9IHtcclxuICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM6IFtdLFxyXG4gICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkczogW11cclxuICB9O1xyXG4gIGFzc2V0SW5oZXJpdGVkRGF0YTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIGFzc2V0TWV0ZGF0YUhlbHBlcjogQXNzZXRNZXRhZGF0YUhlbHBlcixcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuZGF0YSA9IHRoaXMuYXNzZXRNZXRkYXRhSGVscGVyLmdldEFzc2V0RGV0YWlsc01ldGFkYXRhKHRoaXMuYXNzZXQuZGF0YSwgdGhpcy5hc3NldC5tZXRhZGF0YV9maWVsZHMuUl9QTV9MSVNUX1ZJRVdfTVBNX0ZJRUxEUywgdGhpcy5hc3NldC5pc1RlbXBsYXRlKTtcclxuICAgIC8vdGhpcy5hc3NldC5tZXRhZGF0YV9maWVsZHMuUl9QTV9MSVNUX1ZJRVdfTVBNX0ZJRUxEU1xyXG4gICAgLy90aGlzLmFzc2V0LmRhdGFcclxuICAgIC8qdGhpcy5vdG1tU2VydmljZS5nZXRPVE1NQXNzZXRCeUlkKHRoaXMuYXNzZXQuYXNzZXRfaWQsICdjdXN0b20nKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBpZiAocmVzcG9uc2UuaW5oZXJpdGVkX21ldGFkYXRhX2NvbGxlY3Rpb25zKSB7XHJcbiAgICAgICAgICB0aGlzLmFzc2V0SW5oZXJpdGVkRGF0YSA9IHJlc3BvbnNlLmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9ucztcclxuICAgICAgICAgIHRoaXMuZGF0YSA9IHRoaXMuYXNzZXRNZXRkYXRhSGVscGVyLmdldExpc3REaXNwbGF5Q29sdW1uQ29uZmlnKHRoaXMuYXNzZXQubWV0YWRhdGFGaWVsZHMsIHJlc3BvbnNlLm1ldGFkYXRhLCB0aGlzLmFzc2V0SW5oZXJpdGVkRGF0YSwgZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7Ki9cclxuICB9XHJcblxyXG59XHJcbiJdfQ==