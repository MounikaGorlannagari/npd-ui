import { __decorate } from "tslib";
import { Component, Input, isDevMode, EventEmitter, Output } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AssetStatusConstants } from './asset_status_constants';
import { AssetService } from '../shared/services/asset.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { QdsService } from '../../upload/services/qds.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import * as acronui from '../../../lib/mpm-utils/auth/utility';
import { MPM_ROLES } from '../../../lib/mpm-utils/objects/Role';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { forkJoin, Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { Router } from '@angular/router';
import { TaskConstants } from '../tasks/task.constants';
import { DeliverableService } from '../tasks/deliverable/deliverable.service';
import { ProjectConstant } from '../project-overview/project.constants';
let AssetCardComponent = class AssetCardComponent {
    constructor(otmmService, assetService, sharingService, taskService, utilService, qdsService, entityAppdefService, loaderService, notificationService, appService, router, deliverableService) {
        this.otmmService = otmmService;
        this.assetService = assetService;
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.utilService = utilService;
        this.qdsService = qdsService;
        this.entityAppdefService = entityAppdefService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.appService = appService;
        this.router = router;
        this.deliverableService = deliverableService;
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.selectedAssetsCallBackHandler = new EventEmitter();
        this.unSelectedAssetsCallBackHandler = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.downloadCallBackHandler = new EventEmitter();
        // @Output() crHandler = new EventEmitter<any>();
        this.openCreativeReview = new EventEmitter();
        this.downloadHandler = new EventEmitter();
        this.copyToolTip = 'Click to copy deliverable name';
        this.assetDescription = "";
        this.assetStatusConstants = AssetStatusConstants;
        this.deliverableConstants = DeliverableConstants;
        this.skip = 0;
        this.top = 10;
    }
    findDocType(fileName) {
        return this.assetService.findIconByName(fileName);
    }
    fileFormat(fileName) {
        return this.assetService.getfileFormatObject(fileName);
    }
    checkDeliverable(asset) {
        if (this.checked) {
            this.selectedAssetsCallBackHandler.emit(asset);
            // this.crHandler.emit(asset);
        }
        else {
            this.unSelectedAssetsCallBackHandler.emit(asset);
        }
    }
    download(asset) {
        this.downloadHandler.next(asset);
    }
    getAssetVersions(asset) {
        this.otmmService.getAssetVersionsByAssetId(asset.asset_id).subscribe(response => {
            this.assetVersionsCallBackHandler.next(response[0]);
        });
    }
    getAssetDetails(asset) {
        this.assetDetailsCallBackHandler.next(asset);
    }
    getAssetPreview(asset) {
        this.assetPreviewCallBackHandler.next(asset);
    }
    updateDescription() {
        this.loadingHandler(true);
        let assetId = this.asset.asset_id;
        let body = {
            "edited_asset": {
                "data": {
                    "asset_identifier": "string",
                    "metadata": [
                        {
                            "id": "ARTESIA.FIELD.ASSET DESCRIPTION",
                            "name": "Description",
                            "type": "com.artesia.metadata.MetadataField",
                            "value": {
                                "value": {
                                    "type": "string",
                                    "value": this.assetDescription
                                }
                            }
                        }
                    ]
                }
            }
        };
        this.otmmService.updateMetadata(assetId, 'Asset', body).subscribe((res) => {
            this.notificationService.success('metadata Successfully updated');
        });
        this.loadingHandler(false);
    }
    notificationHandler(event) {
        if (event.message) {
            this.notificationService.error(event.message);
        }
    }
    loadingHandler(event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    }
    getColumnValue(column) {
        const data = this.asset.allMetadata;
        for (let row = 0; row < data.length; row++) {
            if (column.OTMM_FIELD_ID === data[row].id) {
                if (data[row].value && data[row].value.value && data[row].value.value.value) {
                    return data[row].value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value) {
                    return data[row].metadata_element.value.value.value;
                }
            }
        }
        return " :NA";
    }
    getProperty(asset, property) {
        return this.taskService.getProperty(asset, property);
    }
    shareAsset() {
        this.shareAssetsCallbackHandler.next([this.asset]);
    }
    getDeliverableDetails(deliverableId) {
        return new Observable(observer => {
            this.deliverableService.readDeliverable(deliverableId)
                .subscribe(response => {
                observer.next(response.Deliverable);
                observer.complete();
            }, error => {
                observer.error();
            });
        });
    }
    onClickEdit() {
        this.assetDescription = "";
    }
    openCR() {
        this.openCreativeReview.next(this.asset);
    }
    ngOnChanges(changes) {
        if (changes && changes.selectAllDeliverables && typeof changes.selectAllDeliverables.currentValue === 'boolean' && !changes.isAssetListView) {
            if (changes.selectAllDeliverables.currentValue) {
                this.checked = true;
                this.selectedAssetsCallBackHandler.emit(this.asset);
            }
            else {
                this.checked = false;
            }
        }
        if (changes.isAssetListView) {
            this.checked = false;
            this.checkDeliverable(this.asset);
        }
    }
    ngOnInit() {
        var _a;
        this.assetCardFields = ((_a = this.assetCardFields) === null || _a === void 0 ? void 0 : _a.length) > 10 ? this.assetCardFields.splice(0, 10) : this.assetCardFields;
        if (this.asset.inherited_metadata_collections && this.asset.inherited_metadata_collections[0]) {
            this.asset.allMetadata = this.asset.metadata.metadata_element_list[0].metadata_element_list.concat(this.asset.inherited_metadata_collections[0].inherited_metadata_values);
        }
        else {
            this.asset.allMetadata = this.asset.metadata.metadata_element_list[0].metadata_element_list;
        }
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.appConfig = this.sharingService.getAppConfig();
        this.canDownload = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_ASSET_DOWNLOAD]);
        this.crActionData = this.sharingService.getCRActions();
        this.checked = this.selectedAssetData === undefined ? false : this.selectedAssetData.find(assetId => assetId === this.asset.asset_id) ? true : false;
        if (this.selectAllDeliverables) {
            this.selectAllDeliverables.subscribe((data) => {
                if (data) {
                    this.checked = true;
                }
                else {
                    this.checked = false;
                }
                this.checkDeliverable(this.asset);
            });
        }
        if (this.asset && this.asset.metadata && this.asset.metadata.metadata_element_list) {
            this.asset.metadata.metadata_element_list.forEach(metadataElement => {
                if (metadataElement.id === 'MPM.DELIVERABLE.GROUP') {
                    metadataElement.metadata_element_list.forEach(metadataElementList => {
                        if (metadataElementList.id === 'MPM.DELIVERABLE.STATUS') {
                            this.assetStatus = metadataElementList.value.value.value;
                        }
                    });
                }
                else if (metadataElement.id === 'MPM.ASSET.GROUP') {
                    metadataElement.metadata_element_list.forEach(metadataElementList => {
                        if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                            this.isReferenceAsset = (metadataElementList && metadataElementList.value && metadataElementList.value.value && metadataElementList.value.value.value === 'true') ? true : false;
                        }
                    });
                }
            });
        }
        /*  console.log(this.router.url.split('/apps/mpm/project/')[1].split('/')[0]);
         this.currProjectId = this.router.url.split('/apps/mpm/project/')[1].split('/')[0];
 
         this.assetService.getProjectDetails(this.currProjectId).subscribe(projectResponse => {
             this.projectData = projectResponse.Project;
             this.currentUserCRData = this.crActionData.find(data => data['MPM_Teams-id'].Id === this.projectData.R_PO_TEAM['MPM_Teams-id'].Id);
             if (this.currentUserCRData) {
                 const userRoles = acronui.findObjectsByProp(this.currentUserCRData, 'MPM_Team_Role_Mapping');
                 let currentRole;
                 currentRole = MPM_ROLES.MANAGER;
                 userRoles.find(role => {
                     const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                     const appRoleObj = appRoles.find(appRole => {
                         return appRole.ROLE_NAME === currentRole;
                     });
                     if (appRoleObj) {
                         this.currentUserTeamRoleMapping = role;
                     }
                 });
             }
         }); */
        this.taskId = this.taskService.getProperty(this.asset, TaskConstants.TASK_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        /*
                this.taskService.getTaskById(this.taskId).subscribe(taskData => {
                    this.taskName = taskData.NAME;
                    console.log(taskData);
                }); */
        this.deliverableId = this.taskService.getProperty(this.asset, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        this.getDeliverableDetails(this.deliverableId)
            .subscribe((deliverableResponse) => {
            this.currProjectId = deliverableResponse.R_PO_PROJECT['Project-id'].Id;
            forkJoin([this.assetService.getProjectDetails(this.currProjectId), this.taskService.getTaskById(this.taskId)])
                .subscribe(responseList => {
                let projectResponse = null;
                this.taskData = responseList[1];
                projectResponse = responseList[0] ? responseList[0] : null;
                this.projectData = projectResponse.Project;
                this.isOnHoldProject = (this.projectData && this.projectData.IS_ON_HOLD === 'true') ? true : false;
                this.entityAppdefService.getStatusById(this.projectData.R_PO_STATUS['MPM_Status-id'].Id).subscribe(status => {
                    this.projectCompletedStatus = status.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
                    this.currentUserCRData = this.crActionData.find(data => data['MPM_Teams-id'].Id === this.projectData.R_PO_TEAM['MPM_Teams-id'].Id);
                    if (this.currentUserCRData) {
                        const userRoles = acronui.findObjectsByProp(this.currentUserCRData, 'MPM_Team_Role_Mapping');
                        let currentRole;
                        currentRole = MPM_ROLES.MANAGER;
                        userRoles.forEach(role => {
                            const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                            const appRoleObj = appRoles.find(appRole => {
                                return appRole.ROLE_NAME === currentRole;
                            });
                            if (appRoleObj) {
                                this.currentUserTeamRoleMapping = role;
                            }
                        });
                    }
                });
                // deliverable status
                this.entityAppdefService.getStatusById(deliverableResponse.R_PO_STATUS['MPM_Status-id'].Id).subscribe(status => {
                    this.deliverableStatus = status.NAME;
                });
            });
        });
    }
};
AssetCardComponent.ctorParameters = () => [
    { type: OTMMService },
    { type: AssetService },
    { type: SharingService },
    { type: TaskService },
    { type: UtilService },
    { type: QdsService },
    { type: EntityAppDefService },
    { type: LoaderService },
    { type: NotificationService },
    { type: AppService },
    { type: Router },
    { type: DeliverableService }
];
__decorate([
    Input()
], AssetCardComponent.prototype, "asset", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "selectedAssetData", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "isVersion", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "selectAllDeliverables", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "filterType", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "IsNotPMView", void 0);
__decorate([
    Input()
], AssetCardComponent.prototype, "assetCardFields", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "assetVersionsCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "assetDetailsCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "selectedAssetsCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "unSelectedAssetsCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "shareAssetsCallbackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "assetPreviewCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "downloadCallBackHandler", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "openCreativeReview", void 0);
__decorate([
    Output()
], AssetCardComponent.prototype, "downloadHandler", void 0);
AssetCardComponent = __decorate([
    Component({
        selector: 'mpm-asset-card',
        template: "<mat-card class=\"asset-card\" *ngIf=\"asset && (asset.latest_version || isVersion)\">\r\n    <mat-checkbox *ngIf=\"!isVersion\" color=\"primary\" class=\"flex-row-item select-asset\" (click)=\"$event.stopPropagation()\" (change)=\"checkDeliverable(asset)\" [(ngModel)]=\"checked\">\r\n    </mat-checkbox>\r\n    <div class=\"asset-content-area\" *ngIf=\"asset.rendition_content && asset.rendition_content.thumbnail_content && asset.rendition_content.thumbnail_content.url\">\r\n        <div class=\"asset-holder\">\r\n            <img src=\"{{otmmBaseUrl+asset.rendition_content.thumbnail_content.url.slice(1)}}\" alt=\"asset\" matTooltip=\"{{asset.name}}\">\r\n        </div>\r\n    </div>\r\n    <div class=\"asset-content-area\" *ngIf=\"!asset.rendition_content\">\r\n        <div class=\"asset-holder\">\r\n            <mat-icon class=\"preview-file-icon\" matTooltip=\"{{asset.name}}\">\r\n                {{findDocType(asset.name)}}\r\n            </mat-icon>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"flex-row asset-actions\">\r\n            <button class=\"green status-icon\" *ngIf=\"assetStatus === assetStatusConstants.APPROVED && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>check_circle</mat-icon>\r\n            </button>\r\n            <button class=\"red status-icon\" *ngIf=\"assetStatus === assetStatusConstants.REQUESTED_CHANGES || !asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>highlight_off</mat-icon>\r\n            </button>\r\n            <button class=\"orange status-icon\" *ngIf=\"assetStatus === assetStatusConstants.IN_PROGRESS && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>fiber_manual_record</mat-icon>\r\n            </button>\r\n            <!-- Need to remove asset status - defined, if not needed-->\r\n            <button class=\"gray status-icon\" *ngIf=\"assetStatus === assetStatusConstants.DEFINED && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>fiber_manual_record</mat-icon>\r\n            </button>\r\n            <!-- open creative review -->\r\n            <button mat-icon-button matTooltip=\"Open Creative Review\" (click)=\"openCR()\" *ngIf=\"!isReferenceAsset && !isVersion\">\r\n                <mat-icon>rate_review</mat-icon>\r\n                <!--  *ngIf=\"!isTemplate && deliverableData.deliverableType !== 'COMPLETE'\"\r\n                 [disabled]=\"(isPMView && !isProjectOwner) || (!deliverableData.asset) || \r\n                (!isPMView && getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_TYPE) === 'REVIEW' && (deliverableData.deliverableReviewId ? deliverableData.deliverableReviewId !== loggedInUserId : (getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_APPROVER_ID) !== loggedInUserId && getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_ACTIVE_USER_NAME) === 'NA'))) || projectCompletedStatus || isOnHoldProject\" -->\r\n            </button>\r\n            <button mat-icon-button matTooltip=\"Download Asset\" [disabled]=\"isOnHoldProject || projectCompletedStatus\" (click)=\"download(asset)\" *ngIf=\"canDownload\">\r\n                <mat-icon>get_app</mat-icon>\r\n            </button>\r\n            <button mat-icon-button matTooltip=\"Share Asset\" [disabled]=\"isOnHoldProject || projectCompletedStatus\" (click)=\"shareAsset()\">\r\n                <mat-icon>share</mat-icon>\r\n            </button>\r\n            <span class=\"action-icon flex-row-item\">\r\n                <button mat-icon-button color=\"primary\" matTooltip=\"More Actions\" [matMenuTriggerFor]=\"AssetMenu\"\r\n                    *ngIf=\"!isVersion\">\r\n                    <mat-icon matSuffix>more_vert</mat-icon>\r\n                </button>\r\n            </span>\r\n        </div>\r\n        <div class=\"flex-row asset-details\">\r\n            <div class=\"flex-row-item\" *ngIf=\"asset.name\">\r\n                <mat-icon class=\"asset-type-icon\" matTooltip=\"{{fileFormat(asset.name)}}\">\r\n                    {{findDocType(asset.name)}}\r\n                </mat-icon>\r\n            </div>\r\n            <!-- <div class=\"flex-row-item\">\r\n                <span class=\"asset-name\"\r\n                    matTooltip=\"Deliverable: {{getProperty(asset, deliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID) || 'NA'}}\"\r\n                    matTooltipPosition=\"below\">{{getProperty(asset, deliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID) || 'NA'}}</span>\r\n            </div> -->\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"asset-name\" matTooltip=\"Name: {{asset.name || 'NA' }}\" matTooltipPosition=\"below\">{{asset.name || 'NA'}}</span>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-icon>call_merge</mat-icon>\r\n                <span matTooltip=\"Version: {{asset.version}}\">\r\n                    V{{asset.version}}\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</mat-card>\r\n<mat-menu #AssetMenu=\"matMenu\">\r\n    <button mat-menu-item (click)=\"getAssetDetails(asset)\">\r\n        <mat-icon>info</mat-icon>\r\n        <span>View Details</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetVersions(asset)\">\r\n        <mat-icon>collections</mat-icon>\r\n        <span>View Versions</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetPreview(asset)\">\r\n        <mat-icon>remove_red_eye</mat-icon>\r\n        <span>Preview</span>\r\n    </button>\r\n</mat-menu>",
        styles: [".asset-menu{color:#000}.version-chip{min-height:18px!important;padding:4px!important;margin-top:14px!important}.mat-checkbox-checked.select-asset{display:inline-block}.asset-card{padding:0}.asset-card .select-asset{position:absolute;top:10px;left:12px;display:inline-block;z-index:100}.asset-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.asset-card div.asset-content-area{height:14rem;width:100%;position:relative;display:flex;align-items:flex-end;justify-content:center;outline:0}.asset-card div.asset-content-area .asset-holder{display:inline-block;width:100%;height:100%;position:relative;border:1px solid #ddd}.asset-card div.asset-content-area .asset-holder img{display:block;position:absolute;left:0;right:0;top:0;bottom:0;margin:auto;width:auto;height:auto;max-width:100%;max-height:100%;min-width:190px;min-height:1px}.asset-card div.asset-content-area .preview-file-icon{font-size:70px;margin-left:80px;margin-top:80px}.asset-card div.card-content .asset-actions .action-icon{justify-content:flex-end}.asset-card div.card-content .asset-actions .red{color:red}.asset-card div.card-content .asset-actions .orange{color:orange}.asset-card div.card-content .asset-actions .green{color:green}.asset-card div.card-content .asset-actions .gray{color:gray}.asset-card div.card-content .asset-actions .status-icon{cursor:default}.asset-card div.card-content .asset-details{padding:8px}.asset-card div.card-content .asset-details .asset-name{text-overflow:ellipsis;overflow:hidden;width:140px;white-space:nowrap}"]
    })
], AssetCardComponent);
export { AssetCardComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtY2FyZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L2Fzc2V0LWNhcmQvYXNzZXQtY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFbEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBRWpHLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEtBQUssT0FBTyxNQUFNLHFDQUFxQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBT3hFLElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBK0MzQixZQUNXLFdBQXdCLEVBQ3hCLFlBQTBCLEVBQzFCLGNBQThCLEVBQzlCLFdBQXdCLEVBQ3hCLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsVUFBc0IsRUFDdEIsTUFBYyxFQUNkLGtCQUFzQztRQVh0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWpEdkMsaUNBQTRCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2RCxnQ0FBMkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3RELGtDQUE2QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDeEQsb0NBQStCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxRCwrQkFBMEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3JELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM1RCxpREFBaUQ7UUFDdkMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3QyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJcEQsZ0JBQVcsR0FBRyxnQ0FBZ0MsQ0FBQztRQUUvQyxxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMsU0FBSSxHQUFHLENBQUMsQ0FBQztRQUNULFFBQUcsR0FBRyxFQUFFLENBQUM7SUErQkwsQ0FBQztJQUVMLFdBQVcsQ0FBQyxRQUFRO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELFVBQVUsQ0FBQyxRQUFRO1FBQ2YsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxLQUFLO1FBQ2xCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0MsOEJBQThCO1NBQ2pDO2FBQU07WUFDSCxJQUFJLENBQUMsK0JBQStCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3BEO0lBQ0wsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFLO1FBQ1YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELGdCQUFnQixDQUFDLEtBQUs7UUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzVFLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZUFBZSxDQUFDLEtBQUs7UUFDakIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsZUFBZSxDQUFDLEtBQUs7UUFDakIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsaUJBQWlCO1FBQ2IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztRQUNsQyxJQUFJLElBQUksR0FBRztZQUNULGNBQWMsRUFBQztnQkFDWixNQUFNLEVBQUM7b0JBQ0osa0JBQWtCLEVBQUMsUUFBUTtvQkFDM0IsVUFBVSxFQUFDO3dCQUNSOzRCQUNHLElBQUksRUFBQyxpQ0FBaUM7NEJBQ3RDLE1BQU0sRUFBQyxhQUFhOzRCQUNwQixNQUFNLEVBQUMsb0NBQW9DOzRCQUMzQyxPQUFPLEVBQUM7Z0NBQ0wsT0FBTyxFQUFDO29DQUNMLE1BQU0sRUFBQyxRQUFRO29DQUNmLE9BQU8sRUFBRSxJQUFJLENBQUMsZ0JBQWdCO2lDQUNoQzs2QkFDSDt5QkFDSDtxQkFDSDtpQkFDSDthQUNIO1NBQ0gsQ0FBQTtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBQyxPQUFPLEVBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDdkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQyxDQUFDO1FBQ0EsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsbUJBQW1CLENBQUMsS0FBSztRQUN2QixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDZixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7SUFFUCxjQUFjLENBQUMsS0FBSztRQUNoQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2xFLENBQUM7SUFFQyxjQUFjLENBQUMsTUFBTTtRQUNqQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztRQUNwQyxLQUFJLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBQztZQUN0QyxJQUFHLE1BQU0sQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBQztnQkFDdkMsSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBQztvQkFDekUsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7aUJBQ3BDO3FCQUNJLElBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFDO29CQUMvSixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDckQ7YUFDRjtTQUNGO1FBQ0gsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLLEVBQUUsUUFBUTtRQUN2QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQsVUFBVTtRQUNOLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBR0QscUJBQXFCLENBQUMsYUFBYTtRQUMvQixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDO2lCQUNqRCxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsZ0JBQWdCLEdBQUMsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxNQUFNO1FBQ0YsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMscUJBQXFCLElBQUksT0FBTyxPQUFPLENBQUMscUJBQXFCLENBQUMsWUFBWSxLQUFLLFNBQVMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7WUFDekksSUFBSSxPQUFPLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFO2dCQUM1QyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDcEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkQ7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDeEI7U0FDSjtRQUNELElBQUcsT0FBTyxDQUFDLGVBQWUsRUFBQztZQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQztJQUVELFFBQVE7O1FBQ0osSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFBLElBQUksQ0FBQyxlQUFlLDBDQUFFLE1BQU0sSUFBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNwSCxJQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsOEJBQThCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMsRUFBQztZQUN6RixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1NBQzlLO2FBQUk7WUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQztTQUMvRjtRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUN4RixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7UUFDckksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3JKLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFhLEVBQUUsRUFBRTtnQkFDbkQsSUFBSSxJQUFJLEVBQUU7b0JBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMscUJBQXFCLEVBQUU7WUFDaEYsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUNoRSxJQUFJLGVBQWUsQ0FBQyxFQUFFLEtBQUssdUJBQXVCLEVBQUU7b0JBQ2hELGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsRUFBRTt3QkFDaEUsSUFBSSxtQkFBbUIsQ0FBQyxFQUFFLEtBQUssd0JBQXdCLEVBQUU7NEJBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7eUJBQzVEO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNLElBQUksZUFBZSxDQUFDLEVBQUUsS0FBSyxpQkFBaUIsRUFBRTtvQkFDakQsZUFBZSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO3dCQUNoRSxJQUFJLG1CQUFtQixDQUFDLEVBQUUsS0FBSyw4QkFBOEIsRUFBRTs0QkFDM0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsbUJBQW1CLElBQUksbUJBQW1CLENBQUMsS0FBSyxJQUFJLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3lCQUNwTDtvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFvQk87UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLDZCQUE2QixDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxIOzs7O3NCQUljO1FBRWQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLG9CQUFvQixDQUFDLG9DQUFvQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO2FBQ3pDLFNBQVMsQ0FBQyxDQUFDLG1CQUFtQixFQUFFLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3ZFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUN6RyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksZUFBZSxHQUFRLElBQUksQ0FBQztnQkFDaEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLGVBQWUsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRCxJQUFJLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUM7Z0JBQzNDLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDbkcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ3hHLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxNQUFNLENBQUMsV0FBVyxLQUFLLGVBQWUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ2hILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ25JLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO3dCQUN4QixNQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLHVCQUF1QixDQUFDLENBQUM7d0JBQzdGLElBQUksV0FBVyxDQUFDO3dCQUNoQixXQUFXLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQzt3QkFDaEMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDckIsTUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQzs0QkFDbEUsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtnQ0FDdkMsT0FBTyxPQUFPLENBQUMsU0FBUyxLQUFLLFdBQVcsQ0FBQzs0QkFDN0MsQ0FBQyxDQUFDLENBQUM7NEJBQ0gsSUFBSSxVQUFVLEVBQUU7Z0NBQ1osSUFBSSxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQzs2QkFDMUM7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gscUJBQXFCO2dCQUNyQixJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzNHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUN6QyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0NBRUosQ0FBQTs7WUE3UDJCLFdBQVc7WUFDVixZQUFZO1lBQ1YsY0FBYztZQUNqQixXQUFXO1lBQ1gsV0FBVztZQUNaLFVBQVU7WUFDRCxtQkFBbUI7WUFDekIsYUFBYTtZQUNQLG1CQUFtQjtZQUM1QixVQUFVO1lBQ2QsTUFBTTtZQUNNLGtCQUFrQjs7QUF6RHhDO0lBQVIsS0FBSyxFQUFFO2lEQUFPO0FBQ047SUFBUixLQUFLLEVBQUU7NkRBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFO3FEQUFXO0FBQ1Y7SUFBUixLQUFLLEVBQUU7aUVBQXVCO0FBQ3RCO0lBQVIsS0FBSyxFQUFFO3NEQUFZO0FBQ1g7SUFBUixLQUFLLEVBQUU7dURBQWE7QUFDWjtJQUFSLEtBQUssRUFBRTsyREFBaUI7QUFFZjtJQUFULE1BQU0sRUFBRTt3RUFBd0Q7QUFDdkQ7SUFBVCxNQUFNLEVBQUU7dUVBQXVEO0FBQ3REO0lBQVQsTUFBTSxFQUFFO3lFQUF5RDtBQUN4RDtJQUFULE1BQU0sRUFBRTsyRUFBMkQ7QUFDMUQ7SUFBVCxNQUFNLEVBQUU7c0VBQXNEO0FBQ3JEO0lBQVQsTUFBTSxFQUFFO3VFQUF1RDtBQUN0RDtJQUFULE1BQU0sRUFBRTttRUFBbUQ7QUFFbEQ7SUFBVCxNQUFNLEVBQUU7OERBQThDO0FBQzdDO0lBQVQsTUFBTSxFQUFFOzJEQUEyQztBQW5CM0Msa0JBQWtCO0lBTDlCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxnQkFBZ0I7UUFDMUIsaW5MQUEwQzs7S0FFN0MsQ0FBQztHQUNXLGtCQUFrQixDQTZTOUI7U0E3U1ksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBpc0Rldk1vZGUsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRTdGF0dXNDb25zdGFudHMgfSBmcm9tICcuL2Fzc2V0X3N0YXR1c19jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vdGFza3MvdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVDb25zdGFudHMgfSBmcm9tICcuLi90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBPVE1NTVBNRGF0YVR5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvT1RNTU1QTURhdGFUeXBlcyc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuLi8uLi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vLi4vbGliL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1fUk9MRVMgfSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL29iamVjdHMvUm9sZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgZm9ya0pvaW4sIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrcy90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU2VydmljZSB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tYXNzZXQtY2FyZCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vYXNzZXQtY2FyZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hc3NldC1jYXJkLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0Q2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKSBhc3NldDtcclxuICAgIEBJbnB1dCgpIHNlbGVjdGVkQXNzZXREYXRhO1xyXG4gICAgQElucHV0KCkgaXNWZXJzaW9uO1xyXG4gICAgQElucHV0KCkgc2VsZWN0QWxsRGVsaXZlcmFibGVzO1xyXG4gICAgQElucHV0KCkgZmlsdGVyVHlwZTtcclxuICAgIEBJbnB1dCgpIElzTm90UE1WaWV3O1xyXG4gICAgQElucHV0KCkgYXNzZXRDYXJkRmllbGRzO1xyXG5cclxuICAgIEBPdXRwdXQoKSBhc3NldFZlcnNpb25zQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB1blNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldFByZXZpZXdDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBkb3dubG9hZENhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgLy8gQE91dHB1dCgpIGNySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG9wZW5DcmVhdGl2ZVJldmlldyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRvd25sb2FkSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIG90bW1CYXNlVXJsO1xyXG4gICAgY2hlY2tlZDogYm9vbGVhbjtcclxuICAgIGNvcHlUb29sVGlwID0gJ0NsaWNrIHRvIGNvcHkgZGVsaXZlcmFibGUgbmFtZSc7XHJcbiAgICBhc3NldFN0YXR1cztcclxuICAgIGFzc2V0RGVzY3JpcHRpb24gPSBcIlwiO1xyXG4gICAgYXNzZXRTdGF0dXNDb25zdGFudHMgPSBBc3NldFN0YXR1c0NvbnN0YW50cztcclxuICAgIGRlbGl2ZXJhYmxlQ29uc3RhbnRzID0gRGVsaXZlcmFibGVDb25zdGFudHM7XHJcbiAgICBza2lwID0gMDtcclxuICAgIHRvcCA9IDEwO1xyXG4gICAgYXBwQ29uZmlnOiBhbnk7XHJcbiAgICBjYW5Eb3dubG9hZDogYm9vbGVhbjtcclxuICAgIGlzUmVmZXJlbmNlQXNzZXQ7XHJcbiAgICBjckFjdGlvbkRhdGE6IGFueTtcclxuICAgIHByb2plY3REYXRhOiBhbnk7XHJcbiAgICBjdXJyZW50VXNlckNSRGF0YTogYW55O1xyXG4gICAgY3VycmVudFVzZXJUZWFtUm9sZU1hcHBpbmc6IGFueTtcclxuICAgIGNyUm9sZTogYW55O1xyXG4gICAgY3VyclByb2plY3RJZDogYW55O1xyXG4gICAgdGFza0lkOiBhbnk7XHJcbiAgICB0YXNrTmFtZTogYW55O1xyXG4gICAgZGVsaXZlcmFibGVJZDogYW55O1xyXG4gICAgdGFza0RhdGE6IGFueTtcclxuICAgIHByb2plY3RDb21wbGV0ZWRTdGF0dXM6IGFueTtcclxuICAgIGRlbGl2ZXJhYmxlU3RhdHVzOiBhbnk7XHJcbiAgICBpc09uSG9sZFByb2plY3Q6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcWRzU2VydmljZTogUWRzU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHVibGljIGRlbGl2ZXJhYmxlU2VydmljZTogRGVsaXZlcmFibGVTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGZpbmREb2NUeXBlKGZpbGVOYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXNzZXRTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGVOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICBmaWxlRm9ybWF0KGZpbGVOYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXNzZXRTZXJ2aWNlLmdldGZpbGVGb3JtYXRPYmplY3QoZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrRGVsaXZlcmFibGUoYXNzZXQpIHtcclxuICAgICAgICBpZiAodGhpcy5jaGVja2VkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIuZW1pdChhc3NldCk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuY3JIYW5kbGVyLmVtaXQoYXNzZXQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudW5TZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KGFzc2V0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZG93bmxvYWQoYXNzZXQpe1xyXG4gICAgICAgIHRoaXMuZG93bmxvYWRIYW5kbGVyLm5leHQoYXNzZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0VmVyc2lvbnMoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldEFzc2V0VmVyc2lvbnNCeUFzc2V0SWQoYXNzZXQuYXNzZXRfaWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXNzZXRWZXJzaW9uc0NhbGxCYWNrSGFuZGxlci5uZXh0KHJlc3BvbnNlWzBdKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldERldGFpbHMoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0RGV0YWlsc0NhbGxCYWNrSGFuZGxlci5uZXh0KGFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldFByZXZpZXcoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0UHJldmlld0NhbGxCYWNrSGFuZGxlci5uZXh0KGFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVEZXNjcmlwdGlvbigpIHtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyKHRydWUpO1xyXG4gICAgICAgIGxldCBhc3NldElkID0gdGhpcy5hc3NldC5hc3NldF9pZDtcclxuICAgICAgICBsZXQgYm9keSA9IHtcclxuICAgICAgICAgIFwiZWRpdGVkX2Fzc2V0XCI6e1xyXG4gICAgICAgICAgICAgXCJkYXRhXCI6e1xyXG4gICAgICAgICAgICAgICAgXCJhc3NldF9pZGVudGlmaWVyXCI6XCJzdHJpbmdcIixcclxuICAgICAgICAgICAgICAgIFwibWV0YWRhdGFcIjpbXHJcbiAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBcImlkXCI6XCJBUlRFU0lBLkZJRUxELkFTU0VUIERFU0NSSVBUSU9OXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICBcIm5hbWVcIjpcIkRlc2NyaXB0aW9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjpcImNvbS5hcnRlc2lhLm1ldGFkYXRhLk1ldGFkYXRhRmllbGRcIixcclxuICAgICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjp7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBcInZhbHVlXCI6e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6XCJzdHJpbmdcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjogdGhpcy5hc3NldERlc2NyaXB0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICB9XHJcbiAgICAgICB0aGlzLm90bW1TZXJ2aWNlLnVwZGF0ZU1ldGFkYXRhKGFzc2V0SWQsJ0Fzc2V0Jyxib2R5KS5zdWJzY3JpYmUoKHJlcykgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdtZXRhZGF0YSBTdWNjZXNzZnVsbHkgdXBkYXRlZCcpO1xyXG4gICAgICAgfSk7XHJcbiAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyKGZhbHNlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbm90aWZpY2F0aW9uSGFuZGxlcihldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihldmVudC5tZXNzYWdlKTtcclxuICAgICAgICB9IFxyXG4gICAgICB9XHJcbiAgICAgIFxyXG5sb2FkaW5nSGFuZGxlcihldmVudCkge1xyXG4gICAgKGV2ZW50KSA/IHRoaXMubG9hZGVyU2VydmljZS5zaG93KCkgOiB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gIH1cclxuXHJcbiAgICBnZXRDb2x1bW5WYWx1ZShjb2x1bW4pe1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmFzc2V0LmFsbE1ldGFkYXRhO1xyXG4gICAgICAgIGZvcihsZXQgcm93ID0gMDsgcm93IDwgZGF0YS5sZW5ndGg7IHJvdysrKXtcclxuICAgICAgICAgICAgaWYoY29sdW1uLk9UTU1fRklFTERfSUQgPT09IGRhdGFbcm93XS5pZCl7XHJcbiAgICAgICAgICAgICAgaWYoZGF0YVtyb3ddLnZhbHVlICYmIGRhdGFbcm93XS52YWx1ZS52YWx1ZSAmJiBkYXRhW3Jvd10udmFsdWUudmFsdWUudmFsdWUpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGFbcm93XS52YWx1ZS52YWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgZWxzZSBpZihkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudCAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZS52YWx1ZSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUudmFsdWUudmFsdWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIFwiIDpOQVwiO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBnZXRQcm9wZXJ0eShhc3NldCwgcHJvcGVydHkpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eShhc3NldCwgcHJvcGVydHkpO1xyXG4gICAgfVxyXG5cclxuICAgIHNoYXJlQXNzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KFt0aGlzLmFzc2V0XSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldERlbGl2ZXJhYmxlRGV0YWlscyhkZWxpdmVyYWJsZUlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlU2VydmljZS5yZWFkRGVsaXZlcmFibGUoZGVsaXZlcmFibGVJZClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuRGVsaXZlcmFibGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2tFZGl0KCl7XHJcbiAgICAgICAgdGhpcy5hc3NldERlc2NyaXB0aW9uPVwiXCI7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNSKCl7XHJcbiAgICAgICAgdGhpcy5vcGVuQ3JlYXRpdmVSZXZpZXcubmV4dCh0aGlzLmFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMgJiYgdHlwZW9mIGNoYW5nZXMuc2VsZWN0QWxsRGVsaXZlcmFibGVzLmN1cnJlbnRWYWx1ZSA9PT0gJ2Jvb2xlYW4nICYmICFjaGFuZ2VzLmlzQXNzZXRMaXN0Vmlldykge1xyXG4gICAgICAgICAgICBpZiAoY2hhbmdlcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHRoaXMuYXNzZXQpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoY2hhbmdlcy5pc0Fzc2V0TGlzdFZpZXcpe1xyXG4gICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jaGVja0RlbGl2ZXJhYmxlKHRoaXMuYXNzZXQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmFzc2V0Q2FyZEZpZWxkcyA9IHRoaXMuYXNzZXRDYXJkRmllbGRzPy5sZW5ndGggPiAxMCA/IHRoaXMuYXNzZXRDYXJkRmllbGRzLnNwbGljZSgwLDEwKSA6IHRoaXMuYXNzZXRDYXJkRmllbGRzO1xyXG4gICAgICAgIGlmKHRoaXMuYXNzZXQuaW5oZXJpdGVkX21ldGFkYXRhX2NvbGxlY3Rpb25zICYmIHRoaXMuYXNzZXQuaW5oZXJpdGVkX21ldGFkYXRhX2NvbGxlY3Rpb25zWzBdKXtcclxuICAgICAgICAgICAgdGhpcy5hc3NldC5hbGxNZXRhZGF0YSA9IHRoaXMuYXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0WzBdLm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5jb25jYXQodGhpcy5hc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnNbMF0uaW5oZXJpdGVkX21ldGFkYXRhX3ZhbHVlcyk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMuYXNzZXQuYWxsTWV0YWRhdGEgPSB0aGlzLmFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3Q7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub3RtbUJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5jYW5Eb3dubG9hZCA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVOQUJMRV9BU1NFVF9ET1dOTE9BRF0pO1xyXG4gICAgICAgIHRoaXMuY3JBY3Rpb25EYXRhID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDUkFjdGlvbnMoKTtcclxuICAgICAgICB0aGlzLmNoZWNrZWQgPSB0aGlzLnNlbGVjdGVkQXNzZXREYXRhID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IHRoaXMuc2VsZWN0ZWRBc3NldERhdGEuZmluZChhc3NldElkID0+IGFzc2V0SWQgPT09IHRoaXMuYXNzZXQuYXNzZXRfaWQpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcykge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcy5zdWJzY3JpYmUoKGRhdGE6IGJvb2xlYW4pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNrRGVsaXZlcmFibGUodGhpcy5hc3NldCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXNzZXQgJiYgdGhpcy5hc3NldC5tZXRhZGF0YSAmJiB0aGlzLmFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5mb3JFYWNoKG1ldGFkYXRhRWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50LmlkID09PSAnTVBNLkRFTElWRVJBQkxFLkdST1VQJykge1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRWxlbWVudC5tZXRhZGF0YV9lbGVtZW50X2xpc3QuZm9yRWFjaChtZXRhZGF0YUVsZW1lbnRMaXN0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRWxlbWVudExpc3QuaWQgPT09ICdNUE0uREVMSVZFUkFCTEUuU1RBVFVTJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldFN0YXR1cyA9IG1ldGFkYXRhRWxlbWVudExpc3QudmFsdWUudmFsdWUudmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobWV0YWRhdGFFbGVtZW50LmlkID09PSAnTVBNLkFTU0VULkdST1VQJykge1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRWxlbWVudC5tZXRhZGF0YV9lbGVtZW50X2xpc3QuZm9yRWFjaChtZXRhZGF0YUVsZW1lbnRMaXN0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRWxlbWVudExpc3QuaWQgPT09ICdNUE0uQVNTRVQuSVNfUkVGRVJFTkNFX0FTU0VUJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1JlZmVyZW5jZUFzc2V0ID0gKG1ldGFkYXRhRWxlbWVudExpc3QgJiYgbWV0YWRhdGFFbGVtZW50TGlzdC52YWx1ZSAmJiBtZXRhZGF0YUVsZW1lbnRMaXN0LnZhbHVlLnZhbHVlICYmIG1ldGFkYXRhRWxlbWVudExpc3QudmFsdWUudmFsdWUudmFsdWUgPT09ICd0cnVlJykgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8qICBjb25zb2xlLmxvZyh0aGlzLnJvdXRlci51cmwuc3BsaXQoJy9hcHBzL21wbS9wcm9qZWN0LycpWzFdLnNwbGl0KCcvJylbMF0pO1xyXG4gICAgICAgICB0aGlzLmN1cnJQcm9qZWN0SWQgPSB0aGlzLnJvdXRlci51cmwuc3BsaXQoJy9hcHBzL21wbS9wcm9qZWN0LycpWzFdLnNwbGl0KCcvJylbMF07XHJcbiBcclxuICAgICAgICAgdGhpcy5hc3NldFNlcnZpY2UuZ2V0UHJvamVjdERldGFpbHModGhpcy5jdXJyUHJvamVjdElkKS5zdWJzY3JpYmUocHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgIHRoaXMucHJvamVjdERhdGEgPSBwcm9qZWN0UmVzcG9uc2UuUHJvamVjdDtcclxuICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJDUkRhdGEgPSB0aGlzLmNyQWN0aW9uRGF0YS5maW5kKGRhdGEgPT4gZGF0YVsnTVBNX1RlYW1zLWlkJ10uSWQgPT09IHRoaXMucHJvamVjdERhdGEuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCk7XHJcbiAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50VXNlckNSRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJSb2xlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AodGhpcy5jdXJyZW50VXNlckNSRGF0YSwgJ01QTV9UZWFtX1JvbGVfTWFwcGluZycpO1xyXG4gICAgICAgICAgICAgICAgIGxldCBjdXJyZW50Um9sZTtcclxuICAgICAgICAgICAgICAgICBjdXJyZW50Um9sZSA9IE1QTV9ST0xFUy5NQU5BR0VSO1xyXG4gICAgICAgICAgICAgICAgIHVzZXJSb2xlcy5maW5kKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBSb2xlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aocm9sZSwgJ01QTV9BUFBfUm9sZXMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwUm9sZU9iaiA9IGFwcFJvbGVzLmZpbmQoYXBwUm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYXBwUm9sZS5ST0xFX05BTUUgPT09IGN1cnJlbnRSb2xlO1xyXG4gICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcFJvbGVPYmopIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJUZWFtUm9sZU1hcHBpbmcgPSByb2xlO1xyXG4gICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgIHRoaXMudGFza0lkID0gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eSh0aGlzLmFzc2V0LCBUYXNrQ29uc3RhbnRzLlRBU0tfSVRFTV9JRF9NRVRBREFUQUZJRUxEX0lEKS5zcGxpdCgnLicpWzFdO1xyXG5cclxuICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0VGFza0J5SWQodGhpcy50YXNrSWQpLnN1YnNjcmliZSh0YXNrRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTmFtZSA9IHRhc2tEYXRhLk5BTUU7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGFza0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgfSk7ICovXHJcblxyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZCA9IHRoaXMudGFza1NlcnZpY2UuZ2V0UHJvcGVydHkodGhpcy5hc3NldCwgRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfSVRFTV9JRF9NRVRBREFUQUZJRUxEX0lEKS5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgIHRoaXMuZ2V0RGVsaXZlcmFibGVEZXRhaWxzKHRoaXMuZGVsaXZlcmFibGVJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGVsaXZlcmFibGVSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyUHJvamVjdElkID0gZGVsaXZlcmFibGVSZXNwb25zZS5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmFzc2V0U2VydmljZS5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLmN1cnJQcm9qZWN0SWQpLCB0aGlzLnRhc2tTZXJ2aWNlLmdldFRhc2tCeUlkKHRoaXMudGFza0lkKV0pXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZUxpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcHJvamVjdFJlc3BvbnNlOiBhbnkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tEYXRhID0gcmVzcG9uc2VMaXN0WzFdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0UmVzcG9uc2UgPSByZXNwb25zZUxpc3RbMF0gPyByZXNwb25zZUxpc3RbMF0gOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3REYXRhID0gcHJvamVjdFJlc3BvbnNlLlByb2plY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNPbkhvbGRQcm9qZWN0ID0gKHRoaXMucHJvamVjdERhdGEgJiYgdGhpcy5wcm9qZWN0RGF0YS5JU19PTl9IT0xEID09PSAndHJ1ZScpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZCh0aGlzLnByb2plY3REYXRhLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQpLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0Q29tcGxldGVkU3RhdHVzID0gc3RhdHVzLlNUQVRVU19UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlckNSRGF0YSA9IHRoaXMuY3JBY3Rpb25EYXRhLmZpbmQoZGF0YSA9PiBkYXRhWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGhpcy5wcm9qZWN0RGF0YS5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRVc2VyQ1JEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlclJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcCh0aGlzLmN1cnJlbnRVc2VyQ1JEYXRhLCAnTVBNX1RlYW1fUm9sZV9NYXBwaW5nJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGN1cnJlbnRSb2xlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnRSb2xlID0gTVBNX1JPTEVTLk1BTkFHRVI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlclJvbGVzLmZvckVhY2gocm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFwcFJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyb2xlLCAnTVBNX0FQUF9Sb2xlcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBSb2xlT2JqID0gYXBwUm9sZXMuZmluZChhcHBSb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcHBSb2xlLlJPTEVfTkFNRSA9PT0gY3VycmVudFJvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXBwUm9sZU9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclRlYW1Sb2xlTWFwcGluZyA9IHJvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGRlbGl2ZXJhYmxlIHN0YXR1c1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZChkZWxpdmVyYWJsZVJlc3BvbnNlLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQpLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVN0YXR1cyA9IHN0YXR1cy5OQU1FO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==