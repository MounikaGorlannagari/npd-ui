import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacetComponent } from './facet.component';
import { MaterialModule } from '../material.module';
import { FacetButtonComponent } from './facet-button/facet-button.component';
import { ChickletComponent } from './chicklet/chicklet.component';
let FacetModule = class FacetModule {
};
FacetModule = __decorate([
    NgModule({
        declarations: [
            FacetComponent,
            FacetButtonComponent,
            ChickletComponent
        ],
        imports: [
            CommonModule,
            MaterialModule
        ],
        exports: [
            FacetComponent,
            FacetButtonComponent,
            ChickletComponent
        ]
    })
], FacetModule);
export { FacetModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvZmFjZXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBa0JsRSxJQUFhLFdBQVcsR0FBeEIsTUFBYSxXQUFXO0NBQUksQ0FBQTtBQUFmLFdBQVc7SUFoQnZCLFFBQVEsQ0FBQztRQUNSLFlBQVksRUFBRTtZQUNaLGNBQWM7WUFDZCxvQkFBb0I7WUFDcEIsaUJBQWlCO1NBQ2xCO1FBQ0QsT0FBTyxFQUFFO1lBQ1AsWUFBWTtZQUNaLGNBQWM7U0FDZjtRQUNELE9BQU8sRUFBRTtZQUNQLGNBQWM7WUFDZCxvQkFBb0I7WUFDcEIsaUJBQWlCO1NBQ2xCO0tBQ0YsQ0FBQztHQUNXLFdBQVcsQ0FBSTtTQUFmLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGYWNldENvbXBvbmVudCB9IGZyb20gJy4vZmFjZXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBGYWNldEJ1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vZmFjZXQtYnV0dG9uL2ZhY2V0LWJ1dHRvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDaGlja2xldENvbXBvbmVudCB9IGZyb20gJy4vY2hpY2tsZXQvY2hpY2tsZXQuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBGYWNldENvbXBvbmVudCxcclxuICAgIEZhY2V0QnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2hpY2tsZXRDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBGYWNldENvbXBvbmVudCxcclxuICAgIEZhY2V0QnV0dG9uQ29tcG9uZW50LFxyXG4gICAgQ2hpY2tsZXRDb21wb25lbnRcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGYWNldE1vZHVsZSB7IH1cclxuIl19