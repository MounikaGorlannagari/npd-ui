import { __decorate } from "tslib";
import { Component, ViewChild, Input } from '@angular/core';
import { FacetChangeEventService } from './services/facet-change-event.service';
import { FacetChickletEventService } from './services/facet-chicklet-event.service';
import { SearchChangeEventService } from '../search/services/search-change-event.service';
import { FormatToLocalePipe } from '../shared/pipe/format-to-locale.pipe';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { FacetService } from './services/facet.service';
import { LoaderService } from '../loader/loader.service';
let FacetComponent = class FacetComponent {
    constructor(adapter, formatToLocalePipe, sharingService, facetService, searchChangeEventService, facetChangeEventService, facetChickletEventService, loaderService) {
        this.adapter = adapter;
        this.formatToLocalePipe = formatToLocalePipe;
        this.sharingService = sharingService;
        this.facetService = facetService;
        this.searchChangeEventService = searchChangeEventService;
        this.facetChangeEventService = facetChangeEventService;
        this.facetChickletEventService = facetChickletEventService;
        this.loaderService = loaderService;
        this.chickletData = [];
        this.facetFilters = [];
        this.allMPMFields = [];
        this.selectedFacetFilters = [];
        this.allExpandState = true;
        this.facetGenBehaviour = 'EXCLUDE';
        this.isShowAll = false;
    }
    toggleExpandState() {
        this.allExpandState = !this.allExpandState;
        if (this.allExpandState) {
            this.facetAccordion.openAll();
        }
        else {
            this.facetAccordion.closeAll();
        }
    }
    toggleShowMore(facet) {
        facet.isShowAll = !facet.isShowAll;
        if (!facet.isShowAll) {
            facet.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px'; //23
        }
        else {
            facet.height = 'auto';
        }
    }
    toggleShowAll() {
        this.isShowAll = !this.isShowAll;
    }
    getFieldDetails(fieldId) {
        return this.allMPMFields.find(field => {
            return field.INDEXER_FIELD_ID === fieldId;
        });
    }
    publishFacetFieldRestriction() {
        const facetRestrictionList = {
            facet_condition: this.selectedFacetFilters
        };
        this.facetChangeEventService.updateFacetSelectFacetCondition(facetRestrictionList);
    }
    getValue(value) {
        if (!value) {
            return;
        }
        if (typeof value === 'string') {
            return value;
        }
        else if (typeof value === 'object' && value !== null) {
            if (value.value) {
                return value.value;
            }
            else if (value.range_label) {
                return value.range_label;
            }
            else if (value.interval_label) {
                return value.interval_label;
            }
        }
        return;
    }
    updateChicklet() {
        const chicklets = [];
        this.selectedFacetFilters.forEach(item => {
            item.values.forEach(value => {
                const data = {
                    fieldId: item.field_id,
                    value: this.getValue(value),
                    originalValue: value
                };
                chicklets.push(data);
            });
        });
        this.facetChickletEventService.updateChickletFilters(chicklets);
    }
    clearAllFacets() {
        this.loaderService.show();
        this.selectedFacetFilters = Object.assign([]);
        this.updateChicklet();
        this.publishFacetFieldRestriction();
    }
    getFactFromSelectedFacets(fieldId) {
        return this.selectedFacetFilters.find(facetFilterItem => {
            return facetFilterItem.field_id === fieldId;
        });
    }
    isValueSelected(fieldId, facetValue) {
        if (!fieldId || !facetValue) {
            return;
        }
        const fieldRestrictionItem = this.getFactFromSelectedFacets(fieldId);
        const facetObj = this.facetFilters.find(filterItrItem => {
            return filterItrItem.facet_field_request.field_id === fieldId;
        });
        if (!fieldRestrictionItem || !facetObj) {
            return;
        }
        if (typeof facetValue === 'string') {
            if (fieldRestrictionItem.values.indexOf(facetValue) !== -1) {
                return true;
            }
        }
        else if (typeof facetValue === 'object' && facetValue !== null) {
            let value = null;
            if (facetObj.isDate && facetObj.isRange) {
                value = facetValue.range_label || facetValue.date_range.range_label;
            }
            else if (facetObj.isDate && facetObj.isInterval) {
                value = facetValue.interval_label || facetValue.date_interval.interval_label;
            }
            if (facetObj.isNumeric && facetObj.isRange) {
                value = facetValue.range_label || facetValue.numeric_range.range_label;
            }
            else if (facetObj.isNumeric && facetObj.isInterval) {
                value = facetValue.interval_label || facetValue.numeric_interval.interval_label;
            }
            else if (facetValue.value) {
                value = facetValue.value;
            }
            if (!value) {
                return;
            }
            return fieldRestrictionItem.values.find(restrictionItemValue => {
                if (typeof restrictionItemValue === 'string') {
                    if (fieldRestrictionItem.values.indexOf(value) !== -1) {
                        return true;
                    }
                }
                else if (typeof facetValue === 'object' && facetValue !== null) {
                    return (restrictionItemValue.range_label === value ||
                        restrictionItemValue.interval_label === value ||
                        restrictionItemValue.value === value);
                }
            });
        }
    }
    removeFilterSelection(fieldId, facetValue) {
        if (!fieldId || !facetValue) {
            return;
        }
        const fieldSelected = this.getFactFromSelectedFacets(fieldId);
        if (!fieldSelected) {
            return;
        }
        const selectedFacetFiltersCopy = Object.assign([], this.selectedFacetFilters);
        if (fieldSelected.values.length > 1) {
            const filteredValues = fieldSelected.values.filter(valueItem => {
                if (!facetValue) {
                    return true;
                }
                if (facetValue === valueItem) {
                    return false;
                }
                if (facetValue && facetValue.range_label && facetValue.range_label === valueItem.range_label) {
                    return false;
                }
                else if (facetValue && facetValue.interval_label && facetValue.interval_label === valueItem.interval_label) {
                    return false;
                }
                if (facetValue.value && valueItem.value && (facetValue.value === valueItem.value)) {
                    return false;
                }
                else {
                    return true;
                }
            });
            fieldSelected.values = filteredValues;
        }
        else {
            this.selectedFacetFilters = selectedFacetFiltersCopy.filter(selectionItem => {
                return selectionItem.field_id !== fieldId;
            });
        }
    }
    handleFacetSelection(fieldId, facetValue) {
        const facetObj = this.facetFilters.find(filterItrItem => {
            return filterItrItem.facet_field_request.field_id === fieldId;
        });
        if (this.selectedFacetFilters.length === 0) {
            this.selectedFacetFilters.push({
                type: facetObj.type,
                behavior: this.facetGenBehaviour,
                field_id: fieldId,
                values: [facetValue]
            });
        }
        else {
            const fieldSelected = this.getFactFromSelectedFacets(fieldId);
            if (fieldSelected) {
                const isValueSelected = this.isValueSelected(fieldId, facetValue);
                if (isValueSelected) {
                    this.removeFilterSelection(fieldId, facetValue);
                }
                else {
                    if (facetObj) {
                        if (facetObj.facet_field_request.multi_select) {
                            fieldSelected.values.push(facetValue);
                        }
                        else {
                            fieldSelected.values = [facetValue];
                        }
                    }
                    else {
                        this.removeFilterSelection(fieldId, facetValue);
                    }
                }
            }
            else {
                this.selectedFacetFilters.push({
                    type: facetObj.type,
                    behavior: this.facetGenBehaviour,
                    field_id: fieldId,
                    values: [facetValue]
                });
            }
        }
        this.updateChicklet();
        this.publishFacetFieldRestriction();
    }
    filterByCustomFacet(facet) {
        let customValue;
        if (facet.isDate) {
            if (facet.isInterval) {
                customValue = {
                    custom_range: true,
                    fixed_end_date: this.toDateString(facet.formGroup.get('end_date').value),
                    fixed_start_date: this.toDateString(facet.formGroup.get('start_date').value),
                    interval_label: this.converToLocalDate(facet.formGroup.get('start_date').value)
                        + ' to ' + this.converToLocalDate(facet.formGroup.get('end_date').value)
                };
            }
            else if (facet.isRange) {
                customValue = {
                    custom_range: true,
                    end_date: this.toDateString(facet.formGroup.get('end_date').value),
                    start_date: this.toDateString(facet.formGroup.get('start_date').value),
                    range_label: this.converToLocalDate(facet.formGroup.get('start_date').value)
                        + ' to ' + this.converToLocalDate(facet.formGroup.get('end_date').value)
                };
            }
        }
        else if (facet.isNumeric) {
            if (facet.isInterval) {
                customValue = {
                    custom_range: true,
                    range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
                    end_value: facet.formGroup.get('end_value').value,
                    start_value: facet.formGroup.get('start_value').value
                };
            }
            else if (facet.isRange) {
                customValue = {
                    custom_range: true,
                    range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
                    end_value: facet.formGroup.get('end_value').value,
                    start_value: facet.formGroup.get('start_value').value
                };
            }
        }
        if (customValue) {
            this.handleFacetSelection(facet.facet_field_request.field_id, customValue);
        }
    }
    filterByFacets(filterId, filterValue) {
        this.loaderService.show();
        if (!filterId || !filterValue) {
            return;
        }
        this.handleFacetSelection(filterId, filterValue);
    }
    chickletRemoved(removedFilters) {
        this.loaderService.show();
        const removedFilter = removedFilters[0];
        this.handleFacetSelection(removedFilter.fieldId, removedFilter.originalValue);
    }
    setFilterSelectinAndViewType(filter) {
        let isFilterSelected = false;
        const fieldRestrictionItem = this.selectedFacetFilters.find(facetFilterItem => {
            return facetFilterItem.field_id === filter.facet_field_request.field_id;
        });
        if (fieldRestrictionItem && Array.isArray(fieldRestrictionItem.values) && fieldRestrictionItem.values.length > 0) {
            if (filter.facet_field_request.multi_select) {
                filter.facet_value_list.forEach(valueObj => {
                    if (this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
                        valueObj.isSelected = true;
                        isFilterSelected = true;
                    }
                });
            }
            else {
                filter.facet_value_list.forEach(valueObj => {
                    if (this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
                        if (typeof valueObj === 'string') {
                            filter.selectedItem = valueObj;
                        }
                        else if (typeof valueObj === 'object' && valueObj !== null) {
                            let valueToAssign;
                            let dataType;
                            if (filter.isDate) {
                                dataType = 'date';
                            }
                            else if (filter.isNumeric) {
                                dataType = 'numeric';
                            }
                            else {
                                valueToAssign = this.getValue(valueObj);
                            }
                            if (dataType) {
                                if (filter.isInterval) {
                                    valueToAssign = this.getValue(valueObj[dataType + '_' + 'interval']);
                                }
                                else if (filter.isRange) {
                                    valueToAssign = this.getValue(valueObj[dataType + '_' + 'range']);
                                }
                            }
                            if (valueToAssign) {
                                filter.selectedItem = valueToAssign;
                            }
                        }
                        isFilterSelected = true;
                    }
                });
            }
            if (this.activeFacetConfiguration.valueOrder === 'COUNT') {
                filter.facet_value_list.sort((a, b) => (a.asset_count < b.asset_count) ? 1 : -1);
            }
        }
        if (this.activeFacetConfiguration.defaultFacetValueDisplayed &&
            Array.isArray(filter.facet_value_list) && filter.facet_value_list.length >
            this.activeFacetConfiguration.defaultFacetValueDisplayed) {
            if (!isFilterSelected) {
                filter.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px';
                filter.isShowAll = false;
            }
            else {
                filter.height = 'auto';
                filter.isShowAll = true;
            }
        }
        else {
            filter.height = 'auto';
            filter.isShowAll = true;
        }
    }
    converToLocalDate(date) {
        return this.formatToLocalePipe.transform(date);
    }
    toDateString(date) {
        return date.toISOString();
    }
    formRangeLabel(filter) {
        const isDateType = filter.isDate;
        const isNumeric = filter.isNumeric;
        filter.facet_value_list.map(value => {
            if (isDateType) {
                value.date_range.range_label = ((value.date_range.start_date ? this.converToLocalDate(value.date_range.start_date) : 'before')
                    + ' - ' +
                    (value.date_range.end_date ? this.converToLocalDate(value.date_range.end_date) : 'after'));
            }
            else if (isNumeric) {
                if (value.numeric_range.start_value && value.numeric_range.end_value) {
                    value.numeric_range.range_label = value.numeric_range.start_value + ' - ' + value.numeric_range.end_value;
                }
                else if (value.numeric_range.start_value) {
                    value.numeric_range.range_label = ' > ' + value.numeric_range.start_value;
                }
                else if (value.numeric_range.end_value) {
                    value.numeric_range.range_label = ' < ' + value.numeric_range.end_value;
                }
            }
        });
    }
    initalizeFacets() {
        const activeFacetId = this.searchChangeEventService.getFacetConfigIdForActiveSearch();
        this.activeFacetConfiguration = this.facetConfigurations.find(config => {
            return config.id === activeFacetId;
        });
        if (this.activeFacetConfiguration) {
            this.facetFilters.forEach(filter => {
                if (filter && filter.type) {
                    const typeSplitted = filter.type.split('Response');
                    if (typeSplitted.length > 1) {
                        filter.type = typeSplitted[0] + 'Restriction';
                    }
                }
                if (filter.type.indexOf('Date') !== -1) {
                    filter.isDate = true;
                    filter.formGroup = new FormGroup({
                        start_date: new FormControl('', Validators.required),
                        end_date: new FormControl('', Validators.required)
                    });
                }
                if (filter.type.indexOf('Numeric') !== -1) {
                    filter.isNumeric = true;
                    filter.formGroup = new FormGroup({
                        start_value: new FormControl('', Validators.required),
                        end_value: new FormControl('', Validators.required)
                    });
                }
                if (filter.type.indexOf('Range') !== -1) {
                    filter.isRange = true;
                    this.formRangeLabel(filter);
                }
                if (filter.type.indexOf('Interval') !== -1) {
                    this.facetService.facetIntervalDisplayValues(filter);
                    filter.isInterval = true;
                }
                if (!filter.facet_field_request.name) {
                    const fieldDetails = this.getFieldDetails(filter.facet_field_request.field_id);
                    filter.facet_field_request.name = fieldDetails ? fieldDetails.DISPLAY_NAME : '';
                }
                this.setFilterSelectinAndViewType(filter);
            });
        }
        else {
            console.error('Facet Configration not found');
        }
    }
    resetFacet() {
        this.facetFilters = Object.assign([]);
        this.selectedFacetFilters = Object.assign([]);
        this.updateChicklet();
    }
    ngOnInit() {
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        this.allMPMFields = this.sharingService.getAllApplicationFields();
        this.tmpFacetChangeEventSubscription$ = this.facetChangeEventService.onFacetFiltersChange.subscribe(facet => {
            this.facetFilters = facet || Object.assign([]);
            if (Array.isArray(this.facetConfigurations) && this.facetConfigurations.length > 0) {
                this.initalizeFacets();
            }
            else {
                this.facetService.setFacetConfigurations().subscribe(facetConfigurations => {
                    this.facetConfigurations = facetConfigurations;
                    this.initalizeFacets();
                }, error => {
                    this.facetFilters = Object.assign([]);
                    this.selectedFacetFilters = [];
                    this.initalizeFacets();
                });
            }
        });
        this.tmpFacetResetEventSubscription$ = this.facetChangeEventService.resetFacet$.subscribe(data => {
            this.resetFacet();
        });
    }
    ngOnDestroy() {
        this.resetFacet();
        // this.facetChangeEventService.updateFacetSelectFacetCondition(null); 
        if (this.tmpFacetChangeEventSubscription$) {
            this.tmpFacetChangeEventSubscription$.unsubscribe();
        }
        if (this.tmpFacetResetEventSubscription$) {
            this.tmpFacetResetEventSubscription$.unsubscribe();
        }
    }
};
FacetComponent.ctorParameters = () => [
    { type: DateAdapter },
    { type: FormatToLocalePipe },
    { type: SharingService },
    { type: FacetService },
    { type: SearchChangeEventService },
    { type: FacetChangeEventService },
    { type: FacetChickletEventService },
    { type: LoaderService }
];
__decorate([
    ViewChild('facetAccordion')
], FacetComponent.prototype, "facetAccordion", void 0);
__decorate([
    Input()
], FacetComponent.prototype, "dataCount", void 0);
FacetComponent = __decorate([
    Component({
        selector: 'mpm-facet',
        template: "<div class=\"facet-wrapper\">\r\n  <div class=\"facet\">\r\n    <div class=\"facet-header\">\r\n      <span class=\"facet-title\">Refine Your Search</span>\r\n      <span class=\"facet-actions\" *ngIf=\"facetFilters.length\">\r\n        <button mat-stroked-button class=\"filter-action-btn\"\r\n          matTooltip=\"{{ allExpandState ? 'Collapse All' : 'Expand All' }}\"\r\n          (click)=\"toggleExpandState()\">{{ allExpandState ? \"Collapse All\" : \"Expand All\" }}</button>\r\n        <button mat-stroked-button *ngIf=\"selectedFacetFilters.length\" class=\"filter-action-btn\"\r\n          matTooltip=\"Clear all applied facets\" (click)=\"clearAllFacets()\">Clear All</button>\r\n      </span>\r\n      <div class=\"facet-chicklet-area\">\r\n        <mpm-chicklet (removed)=\"chickletRemoved($event)\"></mpm-chicklet>\r\n      </div>\r\n    </div>\r\n    <mat-accordion *ngIf=\"dataCount > 0\" class=\"facet-panel-accordion\" #facetAccordion=\"matAccordion\" [multi]=\"true\">\r\n      <ng-container *ngFor=\"let facet of facetFilters; let i = index\">\r\n        <mat-expansion-panel class=\"facet-panel\" [expanded]=\"true\"\r\n          *ngIf=\"((activeFacetConfiguration && (activeFacetConfiguration.defaultFacetDisplayed > i)) || isShowAll ) && facet.facet_value_list.length > 0 \">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title>{{facet.facet_field_request.name}}</mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <div class=\"panel-content\">\r\n            <ul class=\"facet-list\" [style.height]=\"facet.height\">\r\n\r\n              <ng-container *ngIf=\"facet.facet_field_request.multi_select\">\r\n\r\n                <ng-container *ngIf=\"facet.isDate\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.date_range && !facetItem.date_range.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_range)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.date_range.range_label}}\">{{facetItem.date_range.range_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet\">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.date_interval && !facetItem.date_interval.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_interval)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.displayName}}\">{{facetItem.displayName}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                  <div>\r\n                    <br />\r\n                    <form [formGroup]=\"facet.formGroup\">\r\n                      <h4>Date Range</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <input formControlName=\"start_date\" matInput [matDatepicker]=\"startDate\"\r\n                          placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                        <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                        <mat-datepicker #startDate></mat-datepicker>\r\n                      </mat-form-field>\r\n                      <h4>To</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <input formControlName=\"end_date\" matInput [matDatepicker]=\"endDate\"\r\n                          placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                        <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                        <mat-datepicker #endDate></mat-datepicker>\r\n                      </mat-form-field>\r\n                      <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                        (click)=\"filterByCustomFacet(facet)\"\r\n                        [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                    </form>\r\n                  </div>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"facet.isNumeric\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.numeric_range && !facetItem.numeric_range.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_range)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.numeric_range.range_label}}\">{{facetItem.numeric_range.range_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                    <li class=\"filter-item\"\r\n                      *ngIf=\"facetItem.numeric_interval && !facetItem.numeric_interval.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_interval)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.numeric_interval.interval_label}}\">{{facetItem.numeric_interval.interval_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                  <div>\r\n                    <br />\r\n                    <form [formGroup]=\"facet.formGroup\">\r\n                      <h4>From</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <mat-label>From</mat-label>\r\n                        <input formControlName=\"start_value\" matInput type=\"number\" placeholder=\"From\">\r\n                      </mat-form-field>\r\n                      <h4>To</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <mat-label>To</mat-label>\r\n                        <input formControlName=\"end_value\" matInput type=\"number\" placeholder=\"From\">\r\n                      </mat-form-field>\r\n                      <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                        (click)=\"filterByCustomFacet(facet)\"\r\n                        [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                    </form>\r\n                  </div>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"(!facet.isDate && !facet.isNumeric)\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.value\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.value)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\" matTooltip=\"{{facetItem.value}}\">{{facetItem.value}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                </ng-container>\r\n\r\n              </ng-container>\r\n\r\n              <ng-container *ngIf=\"!facet.facet_field_request.multi_select\">\r\n\r\n                <mat-radio-group aria-labelledby=\"facet-radio-input\" class=\"facet-radio\"\r\n                  [(ngModel)]=\"facet.selectedItem\">\r\n                  <ng-container *ngIf=\"facet.isDate\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.date_range && !facetItem.date_range.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.date_range.range_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_range)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.date_range.range_label}}\">{{facetItem.date_range.range_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.date_interval && !facetItem.date_interval.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.displayName\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_interval)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.displayName}}\">{{facetItem.displayName}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                    <div>\r\n                      <br />\r\n                      <form [formGroup]=\"facet.formGroup\">\r\n                        <h4>Date Range</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <input formControlName=\"start_date\" matInput [matDatepicker]=\"startDate\"\r\n                            placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                          <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                          <mat-datepicker #startDate></mat-datepicker>\r\n                        </mat-form-field>\r\n                        <h4>To</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <input formControlName=\"end_date\" matInput [matDatepicker]=\"endDate\"\r\n                            placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                          <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                          <mat-datepicker #endDate></mat-datepicker>\r\n                        </mat-form-field>\r\n                        <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                          (click)=\"filterByCustomFacet(facet)\"\r\n                          [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                      </form>\r\n                    </div>\r\n                  </ng-container>\r\n\r\n                  <ng-container *ngIf=\"facet.isNumeric\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.numeric_range && !facetItem.numeric_range.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.numeric_range.range_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_range)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.numeric_range.range_label}}\">{{facetItem.numeric_range.range_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                      <li class=\"filter-item\"\r\n                        *ngIf=\"facetItem.numeric_interval && !facetItem.numeric_interval.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.numeric_interval.interval_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_interval)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.numeric_interval.interval_label}}\">{{facetItem.numeric_interval.interval_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                    <div>\r\n                      <br />\r\n                      <form [formGroup]=\"facet.formGroup\">\r\n                        <h4>From</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <mat-label>From</mat-label>\r\n                          <input formControlName=\"start_value\" matInput type=\"number\" placeholder=\"From\">\r\n                        </mat-form-field>\r\n                        <h4>To</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <mat-label>To</mat-label>\r\n                          <input formControlName=\"end_value\" matInput type=\"number\" placeholder=\"From\">\r\n                        </mat-form-field>\r\n                        <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                          (click)=\"filterByCustomFacet(facet)\"\r\n                          [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                      </form>\r\n                    </div>\r\n                  </ng-container>\r\n\r\n                  <ng-container *ngIf=\"(!facet.isDate && !facet.isNumeric)\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.value\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\" [value]=\"facetItem.value\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.value)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\" matTooltip=\"{{facetItem.value}}\">{{facetItem.value}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                  </ng-container>\r\n\r\n                </mat-radio-group>\r\n              </ng-container>\r\n            </ul>\r\n            <button class=\"more-btn\" mat-button *ngIf=\"activeFacetConfiguration && \r\n            (facet.facet_value_list.length > activeFacetConfiguration.defaultFacetValueDisplayed)\"\r\n              matTooltip=\"{{facet.isShowAll ? 'Show less filters' : 'Show more filters'}}\"\r\n              (click)=\"toggleShowMore(facet)\">{{facet.isShowAll ? 'Show less' : 'Show more...'}}</button>\r\n          </div>\r\n        </mat-expansion-panel>\r\n      </ng-container>\r\n      <button class=\"more-btn full-width\" mat-stroked-button *ngIf=\"(activeFacetConfiguration && \r\n            (facetFilters.length > activeFacetConfiguration.defaultFacetDisplayed) && !isShowAll)\"\r\n        matTooltip=\"{{isShowAll ? 'Show less facets' : 'Show more facets'}}\"\r\n        (click)=\"toggleShowAll()\">{{isShowAll ? 'Show less facets' : 'Show more facets'}}</button>\r\n    </mat-accordion>\r\n    <p *ngIf=\"(!facetFilters || !facetFilters.length || dataCount ==0)\">No Facets available for the current view !</p>\r\n  </div>\r\n</div>",
        styles: [".facet{width:300px;overflow:auto}.facet-header{padding:10px}.facet-title{font-size:14px}.facet-actions{display:inline-block}.facet-actions button{padding:0 5px;margin:5px;font-size:12px;height:25px;line-height:1px}.facet-chicklet-area{margin-top:10px;max-height:30vh;overflow-y:auto;overflow-x:hidden}.facet-list{overflow:hidden}.facet-list li{list-style-type:none;margin:10px 0;height:22px}.selection-align{margin:0 10px}.facet-panel{border-radius:0!important;box-shadow:none}.facet-wrapper{margin-bottom:80px}.facet-wrapper p{padding:30px}.more-btn{float:right;font-weight:600}.mat-badge-content{top:-3px!important}.mat-badge{padding-right:10px}.facet-count{min-height:15px;float:right;background-color:transparent!important;padding:8px;color:#737373!important}.filter-item .label{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;width:65%;margin-bottom:-5px;display:inline-block}.full-width{width:100%}mat-expansion-panel.facet-panel{border:1px solid #e4e4e4;margin:0!important}mat-expansion-panel.facet-panel mat-expansion-panel-header{height:50px}mat-form-field{width:212px}"]
    })
], FacetComponent);
export { FacetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvZmFjZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBR2hGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRXBGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFHdkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU96RCxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBb0J6QixZQUNTLE9BQXlCLEVBQ3pCLGtCQUFzQyxFQUN0QyxjQUE4QixFQUM5QixZQUEwQixFQUMxQix3QkFBa0QsRUFDbEQsdUJBQWdELEVBQ2hELHlCQUFvRCxFQUNwRCxhQUE0QjtRQVA1QixZQUFPLEdBQVAsT0FBTyxDQUFrQjtRQUN6Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQiw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQTBCO1FBQ2xELDRCQUF1QixHQUF2Qix1QkFBdUIsQ0FBeUI7UUFDaEQsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtRQUNwRCxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQTFCckMsaUJBQVksR0FBVSxFQUFFLENBQUM7UUFDekIsaUJBQVksR0FBWSxFQUFFLENBQUM7UUFDM0IsaUJBQVksR0FBZSxFQUFFLENBQUM7UUFDOUIseUJBQW9CLEdBQTBCLEVBQUUsQ0FBQztRQUdqRCxtQkFBYyxHQUFHLElBQUksQ0FBQztRQUV0QixzQkFBaUIsR0FBRyxTQUFTLENBQUM7UUFDOUIsY0FBUyxHQUFHLEtBQUssQ0FBQztJQWtCZCxDQUFDO0lBRUwsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDM0MsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDL0I7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDaEM7SUFDSCxDQUFDO0lBRUQsY0FBYyxDQUFDLEtBQUs7UUFDbEIsS0FBSyxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDcEIsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxJQUFJO1NBQzVGO2FBQU07WUFDTCxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztTQUN2QjtJQUNILENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDbkMsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFlO1FBQzdCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDcEMsT0FBTyxLQUFLLENBQUMsZ0JBQWdCLEtBQUssT0FBTyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUE0QjtRQUMxQixNQUFNLG9CQUFvQixHQUF1QjtZQUMvQyxlQUFlLEVBQUUsSUFBSSxDQUFDLG9CQUFvQjtTQUMzQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLHVCQUF1QixDQUFDLCtCQUErQixDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVELFFBQVEsQ0FBQyxLQUFLO1FBQ1osSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNWLE9BQU87U0FDUjtRQUNELElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFO1lBQ3RELElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtnQkFDZixPQUFPLEtBQUssQ0FBQyxLQUFLLENBQUM7YUFDcEI7aUJBQU0sSUFBSSxLQUFLLENBQUMsV0FBVyxFQUFFO2dCQUM1QixPQUFPLEtBQUssQ0FBQyxXQUFXLENBQUM7YUFDMUI7aUJBQU0sSUFBSSxLQUFLLENBQUMsY0FBYyxFQUFFO2dCQUMvQixPQUFPLEtBQUssQ0FBQyxjQUFjLENBQUM7YUFDN0I7U0FDRjtRQUNELE9BQU87SUFDVCxDQUFDO0lBRUQsY0FBYztRQUNaLE1BQU0sU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMxQixNQUFNLElBQUksR0FBRztvQkFDWCxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDM0IsYUFBYSxFQUFFLEtBQUs7aUJBQ3JCLENBQUM7Z0JBQ0YsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHlCQUF5QixDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxjQUFjO1FBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELHlCQUF5QixDQUFDLE9BQU87UUFDL0IsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQ3RELE9BQU8sZUFBZSxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZUFBZSxDQUFDLE9BQU8sRUFBRSxVQUFVO1FBQ2pDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDM0IsT0FBTztTQUNSO1FBQ0QsTUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckUsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDdEQsT0FBTyxhQUFhLENBQUMsbUJBQW1CLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQztRQUNoRSxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxvQkFBb0IsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUN0QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsRUFBRTtZQUNsQyxJQUFJLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzFELE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjthQUFNLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7WUFDaEUsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFO2dCQUN2QyxLQUFLLEdBQUcsVUFBVSxDQUFDLFdBQVcsSUFBSSxVQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQzthQUNyRTtpQkFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLFVBQVUsRUFBRTtnQkFDakQsS0FBSyxHQUFHLFVBQVUsQ0FBQyxjQUFjLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUM7YUFDOUU7WUFBQyxJQUFJLFFBQVEsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDNUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7YUFDeEU7aUJBQU0sSUFBSSxRQUFRLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BELEtBQUssR0FBRyxVQUFVLENBQUMsY0FBYyxJQUFJLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7YUFDakY7aUJBQU0sSUFBSSxVQUFVLENBQUMsS0FBSyxFQUFFO2dCQUMzQixLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQzthQUMxQjtZQUNELElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ1YsT0FBTzthQUNSO1lBQ0QsT0FBTyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEVBQUU7Z0JBQzdELElBQUksT0FBTyxvQkFBb0IsS0FBSyxRQUFRLEVBQUU7b0JBQzVDLElBQUksb0JBQW9CLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDckQsT0FBTyxJQUFJLENBQUM7cUJBQ2I7aUJBQ0Y7cUJBQU0sSUFBSSxPQUFPLFVBQVUsS0FBSyxRQUFRLElBQUksVUFBVSxLQUFLLElBQUksRUFBRTtvQkFDaEUsT0FBTyxDQUNMLG9CQUFvQixDQUFDLFdBQVcsS0FBSyxLQUFLO3dCQUMxQyxvQkFBb0IsQ0FBQyxjQUFjLEtBQUssS0FBSzt3QkFDN0Msb0JBQW9CLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FDckMsQ0FBQztpQkFDSDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBTyxFQUFFLFVBQVU7UUFDdkMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMzQixPQUFPO1NBQ1I7UUFDRCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNsQixPQUFPO1NBQ1I7UUFDRCxNQUFNLHdCQUF3QixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzlFLElBQUksYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLE1BQU0sY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUM3RCxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNmLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2dCQUNELElBQUksVUFBVSxLQUFLLFNBQVMsRUFBRTtvQkFDNUIsT0FBTyxLQUFLLENBQUM7aUJBQ2Q7Z0JBQUMsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFdBQVcsSUFBSSxVQUFVLENBQUMsV0FBVyxLQUFLLFNBQVMsQ0FBQyxXQUFXLEVBQUU7b0JBQzlGLE9BQU8sS0FBSyxDQUFDO2lCQUNkO3FCQUFNLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxjQUFjLElBQUksVUFBVSxDQUFDLGNBQWMsS0FBSyxTQUFTLENBQUMsY0FBYyxFQUFFO29CQUM1RyxPQUFPLEtBQUssQ0FBQztpQkFDZDtnQkFBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNuRixPQUFPLEtBQUssQ0FBQztpQkFDZDtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQztpQkFDYjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsYUFBYSxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQUM7U0FDdkM7YUFBTTtZQUNMLElBQUksQ0FBQyxvQkFBb0IsR0FBRyx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0JBQzFFLE9BQU8sYUFBYSxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsVUFBVTtRQUN0QyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUN0RCxPQUFPLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEtBQUssT0FBTyxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUMxQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO2dCQUM3QixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7Z0JBQ25CLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2dCQUNoQyxRQUFRLEVBQUUsT0FBTztnQkFDakIsTUFBTSxFQUFFLENBQUMsVUFBVSxDQUFDO2FBQ3JCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUQsSUFBSSxhQUFhLEVBQUU7Z0JBQ2pCLE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUNsRSxJQUFJLGVBQWUsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztpQkFDakQ7cUJBQU07b0JBQ0wsSUFBSSxRQUFRLEVBQUU7d0JBQ1osSUFBSSxRQUFRLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFOzRCQUM3QyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDdkM7NkJBQU07NEJBQ0wsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNyQztxQkFDRjt5QkFBTTt3QkFDTCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO3FCQUNqRDtpQkFDRjthQUNGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSTtvQkFDbkIsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7b0JBQ2hDLFFBQVEsRUFBRSxPQUFPO29CQUNqQixNQUFNLEVBQUUsQ0FBQyxVQUFVLENBQUM7aUJBQ3JCLENBQUMsQ0FBQzthQUNKO1NBQ0Y7UUFDRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELG1CQUFtQixDQUFDLEtBQVk7UUFDOUIsSUFBSSxXQUFXLENBQUM7UUFDaEIsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksS0FBSyxDQUFDLFVBQVUsRUFBRTtnQkFDcEIsV0FBVyxHQUFHO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixjQUFjLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ3hFLGdCQUFnQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDO29CQUM1RSxjQUFjLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQzswQkFDM0UsTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQzNFLENBQUM7YUFDSDtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ3hCLFdBQVcsR0FBRztvQkFDWixZQUFZLEVBQUUsSUFBSTtvQkFDbEIsUUFBUSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDO29CQUNsRSxVQUFVLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ3RFLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDOzBCQUN4RSxNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQkFDM0UsQ0FBQzthQUNIO1NBQ0Y7YUFBTSxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDMUIsSUFBSSxLQUFLLENBQUMsVUFBVSxFQUFFO2dCQUNwQixXQUFXLEdBQUc7b0JBQ1osWUFBWSxFQUFFLElBQUk7b0JBQ2xCLFdBQVcsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUs7b0JBQ3RHLFNBQVMsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLO29CQUNqRCxXQUFXLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSztpQkFDdEQsQ0FBQzthQUNIO2lCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDeEIsV0FBVyxHQUFHO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixXQUFXLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLO29CQUN0RyxTQUFTLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSztvQkFDakQsV0FBVyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUs7aUJBQ3RELENBQUM7YUFDSDtTQUNGO1FBQ0QsSUFBSSxXQUFXLEVBQUU7WUFDZixJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztTQUM1RTtJQUNILENBQUM7SUFFRCxjQUFjLENBQUMsUUFBUSxFQUFFLFdBQVc7UUFDbEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQzdCLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELGVBQWUsQ0FBQyxjQUFjO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsTUFBTSxhQUFhLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQsNEJBQTRCLENBQUMsTUFBTTtRQUNqQyxJQUFJLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM3QixNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDNUUsT0FBTyxlQUFlLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLG9CQUFvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLElBQUksb0JBQW9CLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDaEgsSUFBSSxNQUFNLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFO2dCQUMzQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxFQUFFO3dCQUNqRSxRQUFRLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzt3QkFDM0IsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3FCQUN6QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ3pDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUUsUUFBUSxDQUFDLEVBQUU7d0JBQ2pFLElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFOzRCQUNoQyxNQUFNLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQzt5QkFDaEM7NkJBQU0sSUFBSSxPQUFPLFFBQVEsS0FBSyxRQUFRLElBQUksUUFBUSxLQUFLLElBQUksRUFBRTs0QkFDNUQsSUFBSSxhQUFhLENBQUM7NEJBQ2xCLElBQUksUUFBUSxDQUFDOzRCQUNiLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtnQ0FDakIsUUFBUSxHQUFHLE1BQU0sQ0FBQzs2QkFDbkI7aUNBQU0sSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO2dDQUMzQixRQUFRLEdBQUcsU0FBUyxDQUFDOzZCQUN0QjtpQ0FBTTtnQ0FDTCxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzs2QkFDekM7NEJBQ0QsSUFBSSxRQUFRLEVBQUU7Z0NBQ1osSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFO29DQUNyQixhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO2lDQUN0RTtxQ0FBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0NBQ3pCLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7aUNBQ25FOzZCQUNGOzRCQUNELElBQUksYUFBYSxFQUFFO2dDQUNqQixNQUFNLENBQUMsWUFBWSxHQUFHLGFBQWEsQ0FBQzs2QkFDckM7eUJBQ0Y7d0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3FCQUN6QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtnQkFDeEQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNsRjtTQUNGO1FBQ0QsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsMEJBQTBCO1lBQzFELEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksTUFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU07WUFDeEUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDBCQUEwQixFQUFFO1lBQzFELElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDckIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQywwQkFBMEIsR0FBRyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZGLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2FBQzFCO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUN2QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzthQUN6QjtTQUNGO2FBQU07WUFDTCxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUN2QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN6QjtJQUNILENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsWUFBWSxDQUFDLElBQUk7UUFDZixPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQsY0FBYyxDQUFDLE1BQU07UUFDbkIsTUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDbEMsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsS0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsQ0FDN0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztzQkFDNUYsS0FBSztvQkFDUCxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQzFGLENBQUM7YUFDSDtpQkFBTSxJQUFJLFNBQVMsRUFBRTtnQkFDcEIsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRTtvQkFDcEUsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO2lCQUMzRztxQkFBTSxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO29CQUMxQyxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7aUJBQzNFO3FCQUFNLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUU7b0JBQ3hDLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztpQkFDekU7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGVBQWU7UUFDYixNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsK0JBQStCLEVBQUUsQ0FBQztRQUN0RixJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNyRSxPQUFPLE1BQU0sQ0FBQyxFQUFFLEtBQUssYUFBYSxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2pDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEVBQUU7b0JBQ3pCLE1BQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNuRCxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMzQixNQUFNLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRyxhQUFhLENBQUM7cUJBQy9DO2lCQUNGO2dCQUNELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3RDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO29CQUNyQixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDO3dCQUMvQixVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7d0JBQ3BELFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztxQkFDbkQsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3pDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUN4QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksU0FBUyxDQUFDO3dCQUMvQixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7d0JBQ3JELFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztxQkFDcEQsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUN0QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUM3QjtnQkFDRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUMxQyxJQUFJLENBQUMsWUFBWSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNyRCxNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztpQkFDMUI7Z0JBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUU7b0JBQ3BDLE1BQU0sWUFBWSxHQUFhLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6RixNQUFNLENBQUMsbUJBQW1CLENBQUMsSUFBSSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNqRjtnQkFDRCxJQUFJLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1NBQy9DO0lBQ0gsQ0FBQztJQUVELFVBQVU7UUFDUixJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxTQUFTLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDNUM7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUVsRSxJQUFJLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMxRyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbEYsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsRUFBRTtvQkFDekUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLG1CQUFtQixDQUFDO29CQUMvQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3pCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDVCxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7b0JBQy9CLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLCtCQUErQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9GLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ2xCLHVFQUF1RTtRQUN2RSxJQUFJLElBQUksQ0FBQyxnQ0FBZ0MsRUFBRTtZQUN6QyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDckQ7UUFDRCxJQUFJLElBQUksQ0FBQywrQkFBK0IsRUFBRTtZQUN4QyxJQUFJLENBQUMsK0JBQStCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEQ7SUFDSCxDQUFDO0NBRUYsQ0FBQTs7WUFyY21CLFdBQVc7WUFDQSxrQkFBa0I7WUFDdEIsY0FBYztZQUNoQixZQUFZO1lBQ0Esd0JBQXdCO1lBQ3pCLHVCQUF1QjtZQUNyQix5QkFBeUI7WUFDckMsYUFBYTs7QUFaUjtJQUE1QixTQUFTLENBQUMsZ0JBQWdCLENBQUM7c0RBQThCO0FBRWpEO0lBQVIsS0FBSyxFQUFFO2lEQUFXO0FBbEJSLGNBQWM7SUFMMUIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLFdBQVc7UUFDckIsbXprQkFBcUM7O0tBRXRDLENBQUM7R0FDVyxjQUFjLENBMGQxQjtTQTFkWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgVmlld0NoaWxkLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGYWNldENoYW5nZUV2ZW50U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZmFjZXQtY2hhbmdlLWV2ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGYWNldCB9IGZyb20gJy4vb2JqZWN0cy9NUE1GYWNldCc7XHJcbmltcG9ydCB7IE1hdEFjY29yZGlvbiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2V4cGFuc2lvbic7XHJcbmltcG9ydCB7IEZhY2V0Q2hpY2tsZXRFdmVudFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2ZhY2V0LWNoaWNrbGV0LWV2ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGYWNldENvbmRpdGlvbkxpc3QsIEZhY2V0RmllbGRDb25kaXRpb24gfSBmcm9tICcuL29iamVjdHMvTVBNRmFjZXRDb25kaXRpb25MaXN0JztcclxuaW1wb3J0IHsgU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VhcmNoL3NlcnZpY2VzL3NlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IFZhbGlkYXRvcnMsIEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBNUE1GYWNldENvbmZpZyB9IGZyb20gJy4vb2JqZWN0cy9NUE1GYWNldENvbmZpZyc7XHJcbmltcG9ydCB7IEZhY2V0U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZmFjZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tZmFjZXQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mYWNldC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmFjZXQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmFjZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gIGNoaWNrbGV0RGF0YTogYW55W10gPSBbXTtcclxuICBmYWNldEZpbHRlcnM6IEZhY2V0W10gPSBbXTtcclxuICBhbGxNUE1GaWVsZHM6IE1QTUZpZWxkW10gPSBbXTtcclxuICBzZWxlY3RlZEZhY2V0RmlsdGVyczogRmFjZXRGaWVsZENvbmRpdGlvbltdID0gW107XHJcbiAgZmFjZXRDb25maWd1cmF0aW9uczogTVBNRmFjZXRDb25maWdbXTtcclxuICBhY3RpdmVGYWNldENvbmZpZ3VyYXRpb246IE1QTUZhY2V0Q29uZmlnO1xyXG4gIGFsbEV4cGFuZFN0YXRlID0gdHJ1ZTtcclxuXHJcbiAgZmFjZXRHZW5CZWhhdmlvdXIgPSAnRVhDTFVERSc7XHJcbiAgaXNTaG93QWxsID0gZmFsc2U7XHJcblxyXG4gIHRtcEZhY2V0UmVzZXRFdmVudFN1YnNjcmlwdGlvbiQ7XHJcbiAgdG1wRmFjZXRDaGFuZ2VFdmVudFN1YnNjcmlwdGlvbiQ7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ2ZhY2V0QWNjb3JkaW9uJykgZmFjZXRBY2NvcmRpb246IE1hdEFjY29yZGlvbjtcclxuXHJcbiAgQElucHV0KCkgZGF0YUNvdW50O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+LFxyXG4gICAgcHVibGljIGZvcm1hdFRvTG9jYWxlUGlwZTogRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBmYWNldFNlcnZpY2U6IEZhY2V0U2VydmljZSxcclxuICAgIHB1YmxpYyBzZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2U6IFNlYXJjaENoYW5nZUV2ZW50U2VydmljZSxcclxuICAgIHB1YmxpYyBmYWNldENoYW5nZUV2ZW50U2VydmljZTogRmFjZXRDaGFuZ2VFdmVudFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZmFjZXRDaGlja2xldEV2ZW50U2VydmljZTogRmFjZXRDaGlja2xldEV2ZW50U2VydmljZSxcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgdG9nZ2xlRXhwYW5kU3RhdGUoKSB7XHJcbiAgICB0aGlzLmFsbEV4cGFuZFN0YXRlID0gIXRoaXMuYWxsRXhwYW5kU3RhdGU7XHJcbiAgICBpZiAodGhpcy5hbGxFeHBhbmRTdGF0ZSkge1xyXG4gICAgICB0aGlzLmZhY2V0QWNjb3JkaW9uLm9wZW5BbGwoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZmFjZXRBY2NvcmRpb24uY2xvc2VBbGwoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHRvZ2dsZVNob3dNb3JlKGZhY2V0KSB7XHJcbiAgICBmYWNldC5pc1Nob3dBbGwgPSAhZmFjZXQuaXNTaG93QWxsO1xyXG4gICAgaWYgKCFmYWNldC5pc1Nob3dBbGwpIHtcclxuICAgICAgZmFjZXQuaGVpZ2h0ID0gKHRoaXMuYWN0aXZlRmFjZXRDb25maWd1cmF0aW9uLmRlZmF1bHRGYWNldFZhbHVlRGlzcGxheWVkICogMzMpICsgJ3B4JzsgLy8yM1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZmFjZXQuaGVpZ2h0ID0gJ2F1dG8nO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdG9nZ2xlU2hvd0FsbCgpIHtcclxuICAgIHRoaXMuaXNTaG93QWxsID0gIXRoaXMuaXNTaG93QWxsO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmllbGREZXRhaWxzKGZpZWxkSWQ6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMuYWxsTVBNRmllbGRzLmZpbmQoZmllbGQgPT4ge1xyXG4gICAgICByZXR1cm4gZmllbGQuSU5ERVhFUl9GSUVMRF9JRCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGlzaEZhY2V0RmllbGRSZXN0cmljdGlvbigpIHtcclxuICAgIGNvbnN0IGZhY2V0UmVzdHJpY3Rpb25MaXN0OiBGYWNldENvbmRpdGlvbkxpc3QgPSB7XHJcbiAgICAgIGZhY2V0X2NvbmRpdGlvbjogdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVyc1xyXG4gICAgfTtcclxuICAgIHRoaXMuZmFjZXRDaGFuZ2VFdmVudFNlcnZpY2UudXBkYXRlRmFjZXRTZWxlY3RGYWNldENvbmRpdGlvbihmYWNldFJlc3RyaWN0aW9uTGlzdCk7XHJcbiAgfVxyXG5cclxuICBnZXRWYWx1ZSh2YWx1ZSkge1xyXG4gICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgIT09IG51bGwpIHtcclxuICAgICAgaWYgKHZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLnZhbHVlO1xyXG4gICAgICB9IGVsc2UgaWYgKHZhbHVlLnJhbmdlX2xhYmVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLnJhbmdlX2xhYmVsO1xyXG4gICAgICB9IGVsc2UgaWYgKHZhbHVlLmludGVydmFsX2xhYmVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmludGVydmFsX2xhYmVsO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICB1cGRhdGVDaGlja2xldCgpIHtcclxuICAgIGNvbnN0IGNoaWNrbGV0cyA9IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBpdGVtLnZhbHVlcy5mb3JFYWNoKHZhbHVlID0+IHtcclxuICAgICAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICAgICAgZmllbGRJZDogaXRlbS5maWVsZF9pZCxcclxuICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlKHZhbHVlKSxcclxuICAgICAgICAgIG9yaWdpbmFsVmFsdWU6IHZhbHVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjaGlja2xldHMucHVzaChkYXRhKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICAgIHRoaXMuZmFjZXRDaGlja2xldEV2ZW50U2VydmljZS51cGRhdGVDaGlja2xldEZpbHRlcnMoY2hpY2tsZXRzKTtcclxuICB9XHJcblxyXG4gIGNsZWFyQWxsRmFjZXRzKCkge1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMgPSBPYmplY3QuYXNzaWduKFtdKTtcclxuICAgIHRoaXMudXBkYXRlQ2hpY2tsZXQoKTtcclxuICAgIHRoaXMucHVibGlzaEZhY2V0RmllbGRSZXN0cmljdGlvbigpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmFjdEZyb21TZWxlY3RlZEZhY2V0cyhmaWVsZElkKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycy5maW5kKGZhY2V0RmlsdGVySXRlbSA9PiB7XHJcbiAgICAgIHJldHVybiBmYWNldEZpbHRlckl0ZW0uZmllbGRfaWQgPT09IGZpZWxkSWQ7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGlzVmFsdWVTZWxlY3RlZChmaWVsZElkLCBmYWNldFZhbHVlKSB7XHJcbiAgICBpZiAoIWZpZWxkSWQgfHwgIWZhY2V0VmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3QgZmllbGRSZXN0cmljdGlvbkl0ZW0gPSB0aGlzLmdldEZhY3RGcm9tU2VsZWN0ZWRGYWNldHMoZmllbGRJZCk7XHJcbiAgICBjb25zdCBmYWNldE9iaiA9IHRoaXMuZmFjZXRGaWx0ZXJzLmZpbmQoZmlsdGVySXRySXRlbSA9PiB7XHJcbiAgICAgIHJldHVybiBmaWx0ZXJJdHJJdGVtLmZhY2V0X2ZpZWxkX3JlcXVlc3QuZmllbGRfaWQgPT09IGZpZWxkSWQ7XHJcbiAgICB9KTtcclxuICAgIGlmICghZmllbGRSZXN0cmljdGlvbkl0ZW0gfHwgIWZhY2V0T2JqKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICh0eXBlb2YgZmFjZXRWYWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgaWYgKGZpZWxkUmVzdHJpY3Rpb25JdGVtLnZhbHVlcy5pbmRleE9mKGZhY2V0VmFsdWUpICE9PSAtMSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBmYWNldFZhbHVlID09PSAnb2JqZWN0JyAmJiBmYWNldFZhbHVlICE9PSBudWxsKSB7XHJcbiAgICAgIGxldCB2YWx1ZSA9IG51bGw7XHJcbiAgICAgIGlmIChmYWNldE9iai5pc0RhdGUgJiYgZmFjZXRPYmouaXNSYW5nZSkge1xyXG4gICAgICAgIHZhbHVlID0gZmFjZXRWYWx1ZS5yYW5nZV9sYWJlbCB8fCBmYWNldFZhbHVlLmRhdGVfcmFuZ2UucmFuZ2VfbGFiZWw7XHJcbiAgICAgIH0gZWxzZSBpZiAoZmFjZXRPYmouaXNEYXRlICYmIGZhY2V0T2JqLmlzSW50ZXJ2YWwpIHtcclxuICAgICAgICB2YWx1ZSA9IGZhY2V0VmFsdWUuaW50ZXJ2YWxfbGFiZWwgfHwgZmFjZXRWYWx1ZS5kYXRlX2ludGVydmFsLmludGVydmFsX2xhYmVsO1xyXG4gICAgICB9IGlmIChmYWNldE9iai5pc051bWVyaWMgJiYgZmFjZXRPYmouaXNSYW5nZSkge1xyXG4gICAgICAgIHZhbHVlID0gZmFjZXRWYWx1ZS5yYW5nZV9sYWJlbCB8fCBmYWNldFZhbHVlLm51bWVyaWNfcmFuZ2UucmFuZ2VfbGFiZWw7XHJcbiAgICAgIH0gZWxzZSBpZiAoZmFjZXRPYmouaXNOdW1lcmljICYmIGZhY2V0T2JqLmlzSW50ZXJ2YWwpIHtcclxuICAgICAgICB2YWx1ZSA9IGZhY2V0VmFsdWUuaW50ZXJ2YWxfbGFiZWwgfHwgZmFjZXRWYWx1ZS5udW1lcmljX2ludGVydmFsLmludGVydmFsX2xhYmVsO1xyXG4gICAgICB9IGVsc2UgaWYgKGZhY2V0VmFsdWUudmFsdWUpIHtcclxuICAgICAgICB2YWx1ZSA9IGZhY2V0VmFsdWUudmFsdWU7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gZmllbGRSZXN0cmljdGlvbkl0ZW0udmFsdWVzLmZpbmQocmVzdHJpY3Rpb25JdGVtVmFsdWUgPT4ge1xyXG4gICAgICAgIGlmICh0eXBlb2YgcmVzdHJpY3Rpb25JdGVtVmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICBpZiAoZmllbGRSZXN0cmljdGlvbkl0ZW0udmFsdWVzLmluZGV4T2YodmFsdWUpICE9PSAtMSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBmYWNldFZhbHVlID09PSAnb2JqZWN0JyAmJiBmYWNldFZhbHVlICE9PSBudWxsKSB7XHJcbiAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICByZXN0cmljdGlvbkl0ZW1WYWx1ZS5yYW5nZV9sYWJlbCA9PT0gdmFsdWUgfHxcclxuICAgICAgICAgICAgcmVzdHJpY3Rpb25JdGVtVmFsdWUuaW50ZXJ2YWxfbGFiZWwgPT09IHZhbHVlIHx8XHJcbiAgICAgICAgICAgIHJlc3RyaWN0aW9uSXRlbVZhbHVlLnZhbHVlID09PSB2YWx1ZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlRmlsdGVyU2VsZWN0aW9uKGZpZWxkSWQsIGZhY2V0VmFsdWUpIHtcclxuICAgIGlmICghZmllbGRJZCB8fCAhZmFjZXRWYWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBmaWVsZFNlbGVjdGVkID0gdGhpcy5nZXRGYWN0RnJvbVNlbGVjdGVkRmFjZXRzKGZpZWxkSWQpO1xyXG4gICAgaWYgKCFmaWVsZFNlbGVjdGVkKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNvbnN0IHNlbGVjdGVkRmFjZXRGaWx0ZXJzQ29weSA9IE9iamVjdC5hc3NpZ24oW10sIHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMpO1xyXG4gICAgaWYgKGZpZWxkU2VsZWN0ZWQudmFsdWVzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgY29uc3QgZmlsdGVyZWRWYWx1ZXMgPSBmaWVsZFNlbGVjdGVkLnZhbHVlcy5maWx0ZXIodmFsdWVJdGVtID0+IHtcclxuICAgICAgICBpZiAoIWZhY2V0VmFsdWUpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmFjZXRWYWx1ZSA9PT0gdmFsdWVJdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBpZiAoZmFjZXRWYWx1ZSAmJiBmYWNldFZhbHVlLnJhbmdlX2xhYmVsICYmIGZhY2V0VmFsdWUucmFuZ2VfbGFiZWwgPT09IHZhbHVlSXRlbS5yYW5nZV9sYWJlbCkge1xyXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZmFjZXRWYWx1ZSAmJiBmYWNldFZhbHVlLmludGVydmFsX2xhYmVsICYmIGZhY2V0VmFsdWUuaW50ZXJ2YWxfbGFiZWwgPT09IHZhbHVlSXRlbS5pbnRlcnZhbF9sYWJlbCkge1xyXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0gaWYgKGZhY2V0VmFsdWUudmFsdWUgJiYgdmFsdWVJdGVtLnZhbHVlICYmIChmYWNldFZhbHVlLnZhbHVlID09PSB2YWx1ZUl0ZW0udmFsdWUpKSB7XHJcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGZpZWxkU2VsZWN0ZWQudmFsdWVzID0gZmlsdGVyZWRWYWx1ZXM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzID0gc2VsZWN0ZWRGYWNldEZpbHRlcnNDb3B5LmZpbHRlcihzZWxlY3Rpb25JdGVtID0+IHtcclxuICAgICAgICByZXR1cm4gc2VsZWN0aW9uSXRlbS5maWVsZF9pZCAhPT0gZmllbGRJZDtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBoYW5kbGVGYWNldFNlbGVjdGlvbihmaWVsZElkLCBmYWNldFZhbHVlKSB7XHJcbiAgICBjb25zdCBmYWNldE9iaiA9IHRoaXMuZmFjZXRGaWx0ZXJzLmZpbmQoZmlsdGVySXRySXRlbSA9PiB7XHJcbiAgICAgIHJldHVybiBmaWx0ZXJJdHJJdGVtLmZhY2V0X2ZpZWxkX3JlcXVlc3QuZmllbGRfaWQgPT09IGZpZWxkSWQ7XHJcbiAgICB9KTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzLnB1c2goe1xyXG4gICAgICAgIHR5cGU6IGZhY2V0T2JqLnR5cGUsXHJcbiAgICAgICAgYmVoYXZpb3I6IHRoaXMuZmFjZXRHZW5CZWhhdmlvdXIsXHJcbiAgICAgICAgZmllbGRfaWQ6IGZpZWxkSWQsXHJcbiAgICAgICAgdmFsdWVzOiBbZmFjZXRWYWx1ZV1cclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zdCBmaWVsZFNlbGVjdGVkID0gdGhpcy5nZXRGYWN0RnJvbVNlbGVjdGVkRmFjZXRzKGZpZWxkSWQpO1xyXG4gICAgICBpZiAoZmllbGRTZWxlY3RlZCkge1xyXG4gICAgICAgIGNvbnN0IGlzVmFsdWVTZWxlY3RlZCA9IHRoaXMuaXNWYWx1ZVNlbGVjdGVkKGZpZWxkSWQsIGZhY2V0VmFsdWUpO1xyXG4gICAgICAgIGlmIChpc1ZhbHVlU2VsZWN0ZWQpIHtcclxuICAgICAgICAgIHRoaXMucmVtb3ZlRmlsdGVyU2VsZWN0aW9uKGZpZWxkSWQsIGZhY2V0VmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoZmFjZXRPYmopIHtcclxuICAgICAgICAgICAgaWYgKGZhY2V0T2JqLmZhY2V0X2ZpZWxkX3JlcXVlc3QubXVsdGlfc2VsZWN0KSB7XHJcbiAgICAgICAgICAgICAgZmllbGRTZWxlY3RlZC52YWx1ZXMucHVzaChmYWNldFZhbHVlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBmaWVsZFNlbGVjdGVkLnZhbHVlcyA9IFtmYWNldFZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5yZW1vdmVGaWx0ZXJTZWxlY3Rpb24oZmllbGRJZCwgZmFjZXRWYWx1ZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMucHVzaCh7XHJcbiAgICAgICAgICB0eXBlOiBmYWNldE9iai50eXBlLFxyXG4gICAgICAgICAgYmVoYXZpb3I6IHRoaXMuZmFjZXRHZW5CZWhhdmlvdXIsXHJcbiAgICAgICAgICBmaWVsZF9pZDogZmllbGRJZCxcclxuICAgICAgICAgIHZhbHVlczogW2ZhY2V0VmFsdWVdXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMudXBkYXRlQ2hpY2tsZXQoKTtcclxuICAgIHRoaXMucHVibGlzaEZhY2V0RmllbGRSZXN0cmljdGlvbigpO1xyXG4gIH1cclxuXHJcbiAgZmlsdGVyQnlDdXN0b21GYWNldChmYWNldDogRmFjZXQpIHtcclxuICAgIGxldCBjdXN0b21WYWx1ZTtcclxuICAgIGlmIChmYWNldC5pc0RhdGUpIHtcclxuICAgICAgaWYgKGZhY2V0LmlzSW50ZXJ2YWwpIHtcclxuICAgICAgICBjdXN0b21WYWx1ZSA9IHtcclxuICAgICAgICAgIGN1c3RvbV9yYW5nZTogdHJ1ZSxcclxuICAgICAgICAgIGZpeGVkX2VuZF9kYXRlOiB0aGlzLnRvRGF0ZVN0cmluZyhmYWNldC5mb3JtR3JvdXAuZ2V0KCdlbmRfZGF0ZScpLnZhbHVlKSxcclxuICAgICAgICAgIGZpeGVkX3N0YXJ0X2RhdGU6IHRoaXMudG9EYXRlU3RyaW5nKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X2RhdGUnKS52YWx1ZSksXHJcbiAgICAgICAgICBpbnRlcnZhbF9sYWJlbDogdGhpcy5jb252ZXJUb0xvY2FsRGF0ZShmYWNldC5mb3JtR3JvdXAuZ2V0KCdzdGFydF9kYXRlJykudmFsdWUpXHJcbiAgICAgICAgICAgICsgJyB0byAnICsgdGhpcy5jb252ZXJUb0xvY2FsRGF0ZShmYWNldC5mb3JtR3JvdXAuZ2V0KCdlbmRfZGF0ZScpLnZhbHVlKVxyXG4gICAgICAgIH07XHJcbiAgICAgIH0gZWxzZSBpZiAoZmFjZXQuaXNSYW5nZSkge1xyXG4gICAgICAgIGN1c3RvbVZhbHVlID0ge1xyXG4gICAgICAgICAgY3VzdG9tX3JhbmdlOiB0cnVlLFxyXG4gICAgICAgICAgZW5kX2RhdGU6IHRoaXMudG9EYXRlU3RyaW5nKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF9kYXRlJykudmFsdWUpLFxyXG4gICAgICAgICAgc3RhcnRfZGF0ZTogdGhpcy50b0RhdGVTdHJpbmcoZmFjZXQuZm9ybUdyb3VwLmdldCgnc3RhcnRfZGF0ZScpLnZhbHVlKSxcclxuICAgICAgICAgIHJhbmdlX2xhYmVsOiB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X2RhdGUnKS52YWx1ZSlcclxuICAgICAgICAgICAgKyAnIHRvICcgKyB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF9kYXRlJykudmFsdWUpXHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmIChmYWNldC5pc051bWVyaWMpIHtcclxuICAgICAgaWYgKGZhY2V0LmlzSW50ZXJ2YWwpIHtcclxuICAgICAgICBjdXN0b21WYWx1ZSA9IHtcclxuICAgICAgICAgIGN1c3RvbV9yYW5nZTogdHJ1ZSxcclxuICAgICAgICAgIHJhbmdlX2xhYmVsOiBmYWNldC5mb3JtR3JvdXAuZ2V0KCdzdGFydF92YWx1ZScpLnZhbHVlICsgJyAtICcgKyBmYWNldC5mb3JtR3JvdXAuZ2V0KCdlbmRfdmFsdWUnKS52YWx1ZSxcclxuICAgICAgICAgIGVuZF92YWx1ZTogZmFjZXQuZm9ybUdyb3VwLmdldCgnZW5kX3ZhbHVlJykudmFsdWUsXHJcbiAgICAgICAgICBzdGFydF92YWx1ZTogZmFjZXQuZm9ybUdyb3VwLmdldCgnc3RhcnRfdmFsdWUnKS52YWx1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgIH0gZWxzZSBpZiAoZmFjZXQuaXNSYW5nZSkge1xyXG4gICAgICAgIGN1c3RvbVZhbHVlID0ge1xyXG4gICAgICAgICAgY3VzdG9tX3JhbmdlOiB0cnVlLFxyXG4gICAgICAgICAgcmFuZ2VfbGFiZWw6IGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X3ZhbHVlJykudmFsdWUgKyAnIC0gJyArIGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF92YWx1ZScpLnZhbHVlLFxyXG4gICAgICAgICAgZW5kX3ZhbHVlOiBmYWNldC5mb3JtR3JvdXAuZ2V0KCdlbmRfdmFsdWUnKS52YWx1ZSxcclxuICAgICAgICAgIHN0YXJ0X3ZhbHVlOiBmYWNldC5mb3JtR3JvdXAuZ2V0KCdzdGFydF92YWx1ZScpLnZhbHVlXHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKGN1c3RvbVZhbHVlKSB7XHJcbiAgICAgIHRoaXMuaGFuZGxlRmFjZXRTZWxlY3Rpb24oZmFjZXQuZmFjZXRfZmllbGRfcmVxdWVzdC5maWVsZF9pZCwgY3VzdG9tVmFsdWUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZmlsdGVyQnlGYWNldHMoZmlsdGVySWQsIGZpbHRlclZhbHVlKSB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgaWYgKCFmaWx0ZXJJZCB8fCAhZmlsdGVyVmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5oYW5kbGVGYWNldFNlbGVjdGlvbihmaWx0ZXJJZCwgZmlsdGVyVmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgY2hpY2tsZXRSZW1vdmVkKHJlbW92ZWRGaWx0ZXJzKSB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgY29uc3QgcmVtb3ZlZEZpbHRlciA9IHJlbW92ZWRGaWx0ZXJzWzBdO1xyXG4gICAgdGhpcy5oYW5kbGVGYWNldFNlbGVjdGlvbihyZW1vdmVkRmlsdGVyLmZpZWxkSWQsIHJlbW92ZWRGaWx0ZXIub3JpZ2luYWxWYWx1ZSk7XHJcbiAgfVxyXG5cclxuICBzZXRGaWx0ZXJTZWxlY3RpbkFuZFZpZXdUeXBlKGZpbHRlcikge1xyXG4gICAgbGV0IGlzRmlsdGVyU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgIGNvbnN0IGZpZWxkUmVzdHJpY3Rpb25JdGVtID0gdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycy5maW5kKGZhY2V0RmlsdGVySXRlbSA9PiB7XHJcbiAgICAgIHJldHVybiBmYWNldEZpbHRlckl0ZW0uZmllbGRfaWQgPT09IGZpbHRlci5mYWNldF9maWVsZF9yZXF1ZXN0LmZpZWxkX2lkO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoZmllbGRSZXN0cmljdGlvbkl0ZW0gJiYgQXJyYXkuaXNBcnJheShmaWVsZFJlc3RyaWN0aW9uSXRlbS52YWx1ZXMpICYmIGZpZWxkUmVzdHJpY3Rpb25JdGVtLnZhbHVlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIGlmIChmaWx0ZXIuZmFjZXRfZmllbGRfcmVxdWVzdC5tdWx0aV9zZWxlY3QpIHtcclxuICAgICAgICBmaWx0ZXIuZmFjZXRfdmFsdWVfbGlzdC5mb3JFYWNoKHZhbHVlT2JqID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLmlzVmFsdWVTZWxlY3RlZChmaWVsZFJlc3RyaWN0aW9uSXRlbS5maWVsZF9pZCwgdmFsdWVPYmopKSB7XHJcbiAgICAgICAgICAgIHZhbHVlT2JqLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBpc0ZpbHRlclNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBmaWx0ZXIuZmFjZXRfdmFsdWVfbGlzdC5mb3JFYWNoKHZhbHVlT2JqID0+IHtcclxuICAgICAgICAgIGlmICh0aGlzLmlzVmFsdWVTZWxlY3RlZChmaWVsZFJlc3RyaWN0aW9uSXRlbS5maWVsZF9pZCwgdmFsdWVPYmopKSB7XHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgdmFsdWVPYmogPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgZmlsdGVyLnNlbGVjdGVkSXRlbSA9IHZhbHVlT2JqO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiB2YWx1ZU9iaiA9PT0gJ29iamVjdCcgJiYgdmFsdWVPYmogIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICBsZXQgdmFsdWVUb0Fzc2lnbjtcclxuICAgICAgICAgICAgICBsZXQgZGF0YVR5cGU7XHJcbiAgICAgICAgICAgICAgaWYgKGZpbHRlci5pc0RhdGUpIHtcclxuICAgICAgICAgICAgICAgIGRhdGFUeXBlID0gJ2RhdGUnO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsdGVyLmlzTnVtZXJpYykge1xyXG4gICAgICAgICAgICAgICAgZGF0YVR5cGUgPSAnbnVtZXJpYyc7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlVG9Bc3NpZ24gPSB0aGlzLmdldFZhbHVlKHZhbHVlT2JqKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKGRhdGFUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZmlsdGVyLmlzSW50ZXJ2YWwpIHtcclxuICAgICAgICAgICAgICAgICAgdmFsdWVUb0Fzc2lnbiA9IHRoaXMuZ2V0VmFsdWUodmFsdWVPYmpbZGF0YVR5cGUgKyAnXycgKyAnaW50ZXJ2YWwnXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGZpbHRlci5pc1JhbmdlKSB7XHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlVG9Bc3NpZ24gPSB0aGlzLmdldFZhbHVlKHZhbHVlT2JqW2RhdGFUeXBlICsgJ18nICsgJ3JhbmdlJ10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAodmFsdWVUb0Fzc2lnbikge1xyXG4gICAgICAgICAgICAgICAgZmlsdGVyLnNlbGVjdGVkSXRlbSA9IHZhbHVlVG9Bc3NpZ247XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlzRmlsdGVyU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbi52YWx1ZU9yZGVyID09PSAnQ09VTlQnKSB7XHJcbiAgICAgICAgZmlsdGVyLmZhY2V0X3ZhbHVlX2xpc3Quc29ydCgoYSwgYikgPT4gKGEuYXNzZXRfY291bnQgPCBiLmFzc2V0X2NvdW50KSA/IDEgOiAtMSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmICh0aGlzLmFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbi5kZWZhdWx0RmFjZXRWYWx1ZURpc3BsYXllZCAmJlxyXG4gICAgICBBcnJheS5pc0FycmF5KGZpbHRlci5mYWNldF92YWx1ZV9saXN0KSAmJiBmaWx0ZXIuZmFjZXRfdmFsdWVfbGlzdC5sZW5ndGggPlxyXG4gICAgICB0aGlzLmFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbi5kZWZhdWx0RmFjZXRWYWx1ZURpc3BsYXllZCkge1xyXG4gICAgICBpZiAoIWlzRmlsdGVyU2VsZWN0ZWQpIHtcclxuICAgICAgICBmaWx0ZXIuaGVpZ2h0ID0gKHRoaXMuYWN0aXZlRmFjZXRDb25maWd1cmF0aW9uLmRlZmF1bHRGYWNldFZhbHVlRGlzcGxheWVkICogMzMpICsgJ3B4JztcclxuICAgICAgICBmaWx0ZXIuaXNTaG93QWxsID0gZmFsc2U7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZmlsdGVyLmhlaWdodCA9ICdhdXRvJztcclxuICAgICAgICBmaWx0ZXIuaXNTaG93QWxsID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZmlsdGVyLmhlaWdodCA9ICdhdXRvJztcclxuICAgICAgZmlsdGVyLmlzU2hvd0FsbCA9IHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjb252ZXJUb0xvY2FsRGF0ZShkYXRlKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKGRhdGUpO1xyXG4gIH1cclxuXHJcbiAgdG9EYXRlU3RyaW5nKGRhdGUpIHtcclxuICAgIHJldHVybiBkYXRlLnRvSVNPU3RyaW5nKCk7XHJcbiAgfVxyXG5cclxuICBmb3JtUmFuZ2VMYWJlbChmaWx0ZXIpIHtcclxuICAgIGNvbnN0IGlzRGF0ZVR5cGUgPSBmaWx0ZXIuaXNEYXRlO1xyXG4gICAgY29uc3QgaXNOdW1lcmljID0gZmlsdGVyLmlzTnVtZXJpYztcclxuICAgIGZpbHRlci5mYWNldF92YWx1ZV9saXN0Lm1hcCh2YWx1ZSA9PiB7XHJcbiAgICAgIGlmIChpc0RhdGVUeXBlKSB7XHJcbiAgICAgICAgdmFsdWUuZGF0ZV9yYW5nZS5yYW5nZV9sYWJlbCA9IChcclxuICAgICAgICAgICh2YWx1ZS5kYXRlX3JhbmdlLnN0YXJ0X2RhdGUgPyB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKHZhbHVlLmRhdGVfcmFuZ2Uuc3RhcnRfZGF0ZSkgOiAnYmVmb3JlJylcclxuICAgICAgICAgICsgJyAtICcgK1xyXG4gICAgICAgICAgKHZhbHVlLmRhdGVfcmFuZ2UuZW5kX2RhdGUgPyB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKHZhbHVlLmRhdGVfcmFuZ2UuZW5kX2RhdGUpIDogJ2FmdGVyJylcclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2UgaWYgKGlzTnVtZXJpYykge1xyXG4gICAgICAgIGlmICh2YWx1ZS5udW1lcmljX3JhbmdlLnN0YXJ0X3ZhbHVlICYmIHZhbHVlLm51bWVyaWNfcmFuZ2UuZW5kX3ZhbHVlKSB7XHJcbiAgICAgICAgICB2YWx1ZS5udW1lcmljX3JhbmdlLnJhbmdlX2xhYmVsID0gdmFsdWUubnVtZXJpY19yYW5nZS5zdGFydF92YWx1ZSArICcgLSAnICsgdmFsdWUubnVtZXJpY19yYW5nZS5lbmRfdmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5udW1lcmljX3JhbmdlLnN0YXJ0X3ZhbHVlKSB7XHJcbiAgICAgICAgICB2YWx1ZS5udW1lcmljX3JhbmdlLnJhbmdlX2xhYmVsID0gJyA+ICcgKyB2YWx1ZS5udW1lcmljX3JhbmdlLnN0YXJ0X3ZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUubnVtZXJpY19yYW5nZS5lbmRfdmFsdWUpIHtcclxuICAgICAgICAgIHZhbHVlLm51bWVyaWNfcmFuZ2UucmFuZ2VfbGFiZWwgPSAnIDwgJyArIHZhbHVlLm51bWVyaWNfcmFuZ2UuZW5kX3ZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBpbml0YWxpemVGYWNldHMoKSB7XHJcbiAgICBjb25zdCBhY3RpdmVGYWNldElkID0gdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UuZ2V0RmFjZXRDb25maWdJZEZvckFjdGl2ZVNlYXJjaCgpO1xyXG4gICAgdGhpcy5hY3RpdmVGYWNldENvbmZpZ3VyYXRpb24gPSB0aGlzLmZhY2V0Q29uZmlndXJhdGlvbnMuZmluZChjb25maWcgPT4ge1xyXG4gICAgICByZXR1cm4gY29uZmlnLmlkID09PSBhY3RpdmVGYWNldElkO1xyXG4gICAgfSk7XHJcbiAgICBpZiAodGhpcy5hY3RpdmVGYWNldENvbmZpZ3VyYXRpb24pIHtcclxuICAgICAgdGhpcy5mYWNldEZpbHRlcnMuZm9yRWFjaChmaWx0ZXIgPT4ge1xyXG4gICAgICAgIGlmIChmaWx0ZXIgJiYgZmlsdGVyLnR5cGUpIHtcclxuICAgICAgICAgIGNvbnN0IHR5cGVTcGxpdHRlZCA9IGZpbHRlci50eXBlLnNwbGl0KCdSZXNwb25zZScpO1xyXG4gICAgICAgICAgaWYgKHR5cGVTcGxpdHRlZC5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgIGZpbHRlci50eXBlID0gdHlwZVNwbGl0dGVkWzBdICsgJ1Jlc3RyaWN0aW9uJztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpbHRlci50eXBlLmluZGV4T2YoJ0RhdGUnKSAhPT0gLTEpIHtcclxuICAgICAgICAgIGZpbHRlci5pc0RhdGUgPSB0cnVlO1xyXG4gICAgICAgICAgZmlsdGVyLmZvcm1Hcm91cCA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICBzdGFydF9kYXRlOiBuZXcgRm9ybUNvbnRyb2woJycsIFZhbGlkYXRvcnMucmVxdWlyZWQpLFxyXG4gICAgICAgICAgICBlbmRfZGF0ZTogbmV3IEZvcm1Db250cm9sKCcnLCBWYWxpZGF0b3JzLnJlcXVpcmVkKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmaWx0ZXIudHlwZS5pbmRleE9mKCdOdW1lcmljJykgIT09IC0xKSB7XHJcbiAgICAgICAgICBmaWx0ZXIuaXNOdW1lcmljID0gdHJ1ZTtcclxuICAgICAgICAgIGZpbHRlci5mb3JtR3JvdXAgPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgc3RhcnRfdmFsdWU6IG5ldyBGb3JtQ29udHJvbCgnJywgVmFsaWRhdG9ycy5yZXF1aXJlZCksXHJcbiAgICAgICAgICAgIGVuZF92YWx1ZTogbmV3IEZvcm1Db250cm9sKCcnLCBWYWxpZGF0b3JzLnJlcXVpcmVkKVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmaWx0ZXIudHlwZS5pbmRleE9mKCdSYW5nZScpICE9PSAtMSkge1xyXG4gICAgICAgICAgZmlsdGVyLmlzUmFuZ2UgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5mb3JtUmFuZ2VMYWJlbChmaWx0ZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmlsdGVyLnR5cGUuaW5kZXhPZignSW50ZXJ2YWwnKSAhPT0gLTEpIHtcclxuICAgICAgICAgIHRoaXMuZmFjZXRTZXJ2aWNlLmZhY2V0SW50ZXJ2YWxEaXNwbGF5VmFsdWVzKGZpbHRlcik7XHJcbiAgICAgICAgICBmaWx0ZXIuaXNJbnRlcnZhbCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghZmlsdGVyLmZhY2V0X2ZpZWxkX3JlcXVlc3QubmFtZSkge1xyXG4gICAgICAgICAgY29uc3QgZmllbGREZXRhaWxzOiBNUE1GaWVsZCA9IHRoaXMuZ2V0RmllbGREZXRhaWxzKGZpbHRlci5mYWNldF9maWVsZF9yZXF1ZXN0LmZpZWxkX2lkKTtcclxuICAgICAgICAgIGZpbHRlci5mYWNldF9maWVsZF9yZXF1ZXN0Lm5hbWUgPSBmaWVsZERldGFpbHMgPyBmaWVsZERldGFpbHMuRElTUExBWV9OQU1FIDogJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0RmlsdGVyU2VsZWN0aW5BbmRWaWV3VHlwZShmaWx0ZXIpO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ0ZhY2V0IENvbmZpZ3JhdGlvbiBub3QgZm91bmQnKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlc2V0RmFjZXQoKSB7XHJcbiAgICB0aGlzLmZhY2V0RmlsdGVycyA9IE9iamVjdC5hc3NpZ24oW10pO1xyXG4gICAgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycyA9IE9iamVjdC5hc3NpZ24oW10pO1xyXG4gICAgdGhpcy51cGRhdGVDaGlja2xldCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBpZiAobmF2aWdhdG9yLmxhbmd1YWdlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy5hZGFwdGVyLnNldExvY2FsZShuYXZpZ2F0b3IubGFuZ3VhZ2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuYWxsTVBNRmllbGRzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxBcHBsaWNhdGlvbkZpZWxkcygpO1xyXG5cclxuICAgIHRoaXMudG1wRmFjZXRDaGFuZ2VFdmVudFN1YnNjcmlwdGlvbiQgPSB0aGlzLmZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlLm9uRmFjZXRGaWx0ZXJzQ2hhbmdlLnN1YnNjcmliZShmYWNldCA9PiB7XHJcbiAgICAgIHRoaXMuZmFjZXRGaWx0ZXJzID0gZmFjZXQgfHwgT2JqZWN0LmFzc2lnbihbXSk7XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZmFjZXRDb25maWd1cmF0aW9ucykgJiYgdGhpcy5mYWNldENvbmZpZ3VyYXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICB0aGlzLmluaXRhbGl6ZUZhY2V0cygpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZmFjZXRTZXJ2aWNlLnNldEZhY2V0Q29uZmlndXJhdGlvbnMoKS5zdWJzY3JpYmUoZmFjZXRDb25maWd1cmF0aW9ucyA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZhY2V0Q29uZmlndXJhdGlvbnMgPSBmYWNldENvbmZpZ3VyYXRpb25zO1xyXG4gICAgICAgICAgdGhpcy5pbml0YWxpemVGYWNldHMoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZhY2V0RmlsdGVycyA9IE9iamVjdC5hc3NpZ24oW10pO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycyA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbml0YWxpemVGYWNldHMoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy50bXBGYWNldFJlc2V0RXZlbnRTdWJzY3JpcHRpb24kID0gdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS5yZXNldEZhY2V0JC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgIHRoaXMucmVzZXRGYWNldCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRGYWNldCgpO1xyXG4gICAgLy8gdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS51cGRhdGVGYWNldFNlbGVjdEZhY2V0Q29uZGl0aW9uKG51bGwpOyBcclxuICAgIGlmICh0aGlzLnRtcEZhY2V0Q2hhbmdlRXZlbnRTdWJzY3JpcHRpb24kKSB7XHJcbiAgICAgIHRoaXMudG1wRmFjZXRDaGFuZ2VFdmVudFN1YnNjcmlwdGlvbiQudW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnRtcEZhY2V0UmVzZXRFdmVudFN1YnNjcmlwdGlvbiQpIHtcclxuICAgICAgdGhpcy50bXBGYWNldFJlc2V0RXZlbnRTdWJzY3JpcHRpb24kLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=