import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { FacetChickletEventService } from '../services/facet-chicklet-event.service';
let ChickletComponent = class ChickletComponent {
    constructor(facetChickletEventService) {
        this.facetChickletEventService = facetChickletEventService;
        this.filters = [];
        this.removed = new EventEmitter();
    }
    removeFilter(filter) {
        this.removed.next([filter]);
    }
    ngOnInit() {
        this.facetChickletEventService.chickletFacetFilterChanged$.subscribe(items => {
            this.filters = items;
        });
    }
};
ChickletComponent.ctorParameters = () => [
    { type: FacetChickletEventService }
];
__decorate([
    Output()
], ChickletComponent.prototype, "removed", void 0);
ChickletComponent = __decorate([
    Component({
        selector: 'mpm-chicklet',
        template: "<div class=\"facet-chicklet-wrapper\">\r\n    <mat-chip-list class=\"chicklet-list\">\r\n        <mat-chip class=\"chicklet-chip\" *ngFor=\"let filter of filters\">\r\n            <span class=\"chicklet-value\" matTooltip=\"{{filter.value}}\">{{filter.value}}</span>\r\n            <mat-icon matChipRemove (click)=\"removeFilter(filter)\" matTooltip=\"Remove this filter\" mat-icon-button\r\n                aria-label=\"Remove Filter\">cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n</div>",
        styles: [".title{padding:0 5px}.chicklet-chip{padding:0 10px;font-size:12px;min-height:25px!important}.chicklet-chip .chicklet-value{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;max-width:200px;display:inline-block}.chicklet-chip:hover .chicklet-value{text-decoration:line-through}"]
    })
], ChickletComponent);
export { ChickletComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpY2tsZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvY2hpY2tsZXQvY2hpY2tsZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFPckYsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7SUFNNUIsWUFDUyx5QkFBb0Q7UUFBcEQsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtRQUw3RCxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBRUgsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7SUFJMUMsQ0FBQztJQUVMLFlBQVksQ0FBQyxNQUFNO1FBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyx5QkFBeUIsQ0FBQywyQkFBMkIsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDM0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBRUYsQ0FBQTs7WUFicUMseUJBQXlCOztBQUhuRDtJQUFULE1BQU0sRUFBRTtrREFBcUM7QUFKbkMsaUJBQWlCO0lBTDdCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxjQUFjO1FBQ3hCLDJnQkFBd0M7O0tBRXpDLENBQUM7R0FDVyxpQkFBaUIsQ0FvQjdCO1NBcEJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZmFjZXQtY2hpY2tsZXQtZXZlbnQuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jaGlja2xldCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NoaWNrbGV0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jaGlja2xldC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDaGlja2xldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGZpbHRlcnMgPSBbXTtcclxuXHJcbiAgQE91dHB1dCgpIHJlbW92ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBmYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlOiBGYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgcmVtb3ZlRmlsdGVyKGZpbHRlcikge1xyXG4gICAgdGhpcy5yZW1vdmVkLm5leHQoW2ZpbHRlcl0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmZhY2V0Q2hpY2tsZXRFdmVudFNlcnZpY2UuY2hpY2tsZXRGYWNldEZpbHRlckNoYW5nZWQkLnN1YnNjcmliZShpdGVtcyA9PiB7XHJcbiAgICAgIHRoaXMuZmlsdGVycyA9IGl0ZW1zO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=