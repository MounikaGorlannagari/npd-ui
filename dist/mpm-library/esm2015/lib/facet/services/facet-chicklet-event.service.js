import { __decorate } from "tslib";
import { Injectable, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
let FacetChickletEventService = class FacetChickletEventService {
    constructor() {
        this.chickletFacetFilterChanged$ = new EventEmitter();
        this.chickletRemoved$ = new EventEmitter();
        this.chickletFacetFilterChanged$ = new EventEmitter();
        this.chickletRemoved$ = new EventEmitter();
    }
    updateChickletFilters(filters) {
        this.chickletFacetFilterChanged$.emit(filters);
    }
    removeChicklet(chicklet) {
        this.chickletRemoved$.emit(chicklet);
    }
};
FacetChickletEventService.ɵprov = i0.ɵɵdefineInjectable({ factory: function FacetChickletEventService_Factory() { return new FacetChickletEventService(); }, token: FacetChickletEventService, providedIn: "root" });
FacetChickletEventService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], FacetChickletEventService);
export { FacetChickletEventService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQtY2hpY2tsZXQtZXZlbnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L3NlcnZpY2VzL2ZhY2V0LWNoaWNrbGV0LWV2ZW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUt6RCxJQUFhLHlCQUF5QixHQUF0QyxNQUFhLHlCQUF5QjtJQUlwQztRQUhPLGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakQscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUczQyxJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN0RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUM3QyxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBTztRQUMzQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxjQUFjLENBQUMsUUFBUTtRQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Q0FFRixDQUFBOztBQWpCWSx5QkFBeUI7SUFIckMsVUFBVSxDQUFDO1FBQ1YsVUFBVSxFQUFFLE1BQU07S0FDbkIsQ0FBQztHQUNXLHlCQUF5QixDQWlCckM7U0FqQlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlIHtcclxuICBwdWJsaWMgY2hpY2tsZXRGYWNldEZpbHRlckNoYW5nZWQkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIHB1YmxpYyBjaGlja2xldFJlbW92ZWQkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHRoaXMuY2hpY2tsZXRGYWNldEZpbHRlckNoYW5nZWQkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgdGhpcy5jaGlja2xldFJlbW92ZWQkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlQ2hpY2tsZXRGaWx0ZXJzKGZpbHRlcnMpIHtcclxuICAgIHRoaXMuY2hpY2tsZXRGYWNldEZpbHRlckNoYW5nZWQkLmVtaXQoZmlsdGVycyk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVDaGlja2xldChjaGlja2xldCkge1xyXG4gICAgdGhpcy5jaGlja2xldFJlbW92ZWQkLmVtaXQoY2hpY2tsZXQpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19