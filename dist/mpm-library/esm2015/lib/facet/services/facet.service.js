import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../shared/services/field-config.service";
import * as i3 from "../../notification/notification.service";
let FacetService = class FacetService {
    constructor(appService, fieldConfigService, notificationService) {
        this.appService = appService;
        this.fieldConfigService = fieldConfigService;
        this.notificationService = notificationService;
        this.allFacetConfigs = [];
        this.GetFacetConfigurationWS = 'GetFacetConfiguration';
        this.GetFacetConfigurationNS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
    }
    getCastedFacetConfigResponse(facetConfigRes) {
        const facetConfigs = [];
        if (Array.isArray(facetConfigRes) && facetConfigRes.length) {
            facetConfigRes.forEach(config => {
                const facetConfig = {
                    id: config.id,
                    name: config.name,
                    multiSelect: (config.multiSelect === 'true') ? true : false,
                    description: config.description,
                    defaultFacetDisplayed: parseInt(config.defaultFacetDisplayed, 0),
                    defaultFacetValueDisplayed: parseInt(config.defaultFacetValueDisplayed, 0),
                    valueOrder: config.valueOrder,
                    valueLimit: parseInt(config.valueLimit, 0),
                    fields: {
                        facetField: []
                    }
                };
                const faceFields = acronui.findObjectsByProp(config, 'facetField');
                if (Array.isArray(faceFields) && faceFields.length) {
                    faceFields.forEach(field => {
                        if (field && field.id) {
                            const matchField = this.fieldConfigService.getFieldByKeyValue('Id', field.fieldId);
                            if (matchField) {
                                facetConfig.fields.facetField.push(matchField);
                            }
                        }
                    });
                }
                facetConfigs.push(facetConfig);
            });
        }
        return facetConfigs;
    }
    /* setFacetConfigurations(): Observable<any[]> {
      return new Observable(observer => {
        if (Array.isArray(this.allFacetConfigs) && this.allFacetConfigs.length) {
          observer.next(this.allFacetConfigs);
          observer.complete();
          return;
        }
        this.appService.invokeRequest(this.GetFacetConfigurationNS, this.GetFacetConfigurationWS, null).subscribe(response => {
          const facetConfig: any[] = acronui.findObjectsByProp(response, 'FacetConfig');
          this.allFacetConfigs = this.getCastedFacetConfigResponse(facetConfig);
          observer.next(this.allFacetConfigs);
          observer.complete();
        }, err => {
          this.notificationService.error('Something went wrong while getting Facet config');
          observer.error();
          observer.complete();
        });
      });
    } */
    setFacetConfigurations() {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.FACET_CONFIGURATIONS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.FACET_CONFIGURATIONS)));
                observer.complete();
            }
            else {
                this.appService.invokeRequest(this.GetFacetConfigurationNS, this.GetFacetConfigurationWS, null)
                    .subscribe(response => {
                    const facetConfig = acronui.findObjectsByProp(response, 'FacetConfig');
                    this.allFacetConfigs = this.getCastedFacetConfigResponse(facetConfig);
                    sessionStorage.setItem(SessionStorageConstants.FACET_CONFIGURATIONS, JSON.stringify(this.allFacetConfigs));
                    observer.next(this.allFacetConfigs);
                    observer.complete();
                }, error => {
                    this.notificationService.error('Something went wrong while getting Facet config');
                    observer.error();
                    observer.complete();
                });
            }
        });
    }
    facetIntervalDisplayValues(facet) {
        facet.facet_value_list.forEach(element => {
            let displayName = '';
            let displayYear;
            const intervalLabel = (element.date_interval.interval_label.split(' '));
            intervalLabel.forEach(elementName => {
                if (elementName === '%%YEAR%%') {
                    displayName = displayName + new Date().getFullYear();
                }
                else {
                    if (elementName.includes('%%YEAR')) {
                        const yearCreation = elementName.split('%%YEAR');
                        yearCreation.forEach(data => {
                            const yearValue = data.split('%%');
                            yearValue.forEach(value => {
                                if (this.isNumber(value)) {
                                    const adjustmentOffset = +value;
                                    displayYear = new Date().getFullYear() + (adjustmentOffset);
                                    displayName = displayName + ' ' + displayYear;
                                }
                                else {
                                    displayName = displayName + ' ';
                                }
                            });
                        });
                    }
                    else {
                        displayName = displayName + elementName + ' ';
                    }
                }
            });
            if (element.date_interval) {
                element.date_interval["displayName"] = displayName;
            }
            element.displayName = displayName;
        });
    }
    isNumber(data) {
        return ((data <= 0 || data > 0) && (data.length > 0)) ? true : false;
    }
};
FacetService.ctorParameters = () => [
    { type: AppService },
    { type: FieldConfigService },
    { type: NotificationService }
];
FacetService.ɵprov = i0.ɵɵdefineInjectable({ factory: function FacetService_Factory() { return new FacetService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.FieldConfigService), i0.ɵɵinject(i3.NotificationService)); }, token: FacetService, providedIn: "root" });
FacetService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], FacetService);
export { FacetService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L3NlcnZpY2VzL2ZhY2V0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUVsRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQzs7Ozs7QUFLM0YsSUFBYSxZQUFZLEdBQXpCLE1BQWEsWUFBWTtJQUV2QixZQUNTLFVBQXNCLEVBQ3RCLGtCQUFzQyxFQUN0QyxtQkFBd0M7UUFGeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFHakQsb0JBQWUsR0FBcUIsRUFBRSxDQUFDO1FBRXZDLDRCQUF1QixHQUFHLHVCQUF1QixDQUFDO1FBQ2xELDRCQUF1QixHQUFHLCtDQUErQyxDQUFDO0lBTHRFLENBQUM7SUFPTCw0QkFBNEIsQ0FBQyxjQUFxQjtRQUNoRCxNQUFNLFlBQVksR0FBcUIsRUFBRSxDQUFDO1FBQzFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxFQUFFO1lBQzFELGNBQWMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzlCLE1BQU0sV0FBVyxHQUFtQjtvQkFDbEMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFO29CQUNiLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtvQkFDakIsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLFdBQVcsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLO29CQUMzRCxXQUFXLEVBQUUsTUFBTSxDQUFDLFdBQVc7b0JBQy9CLHFCQUFxQixFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxDQUFDO29CQUNoRSwwQkFBMEIsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLDBCQUEwQixFQUFFLENBQUMsQ0FBQztvQkFDMUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxVQUFVO29CQUM3QixVQUFVLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO29CQUMxQyxNQUFNLEVBQUU7d0JBQ04sVUFBVSxFQUFFLEVBQUU7cUJBQ2Y7aUJBQ0YsQ0FBQztnQkFDRixNQUFNLFVBQVUsR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUMxRSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtvQkFDbEQsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDekIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLEVBQUUsRUFBRTs0QkFDckIsTUFBTSxVQUFVLEdBQWEsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQzdGLElBQUksVUFBVSxFQUFFO2dDQUNkLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs2QkFDaEQ7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7Z0JBQ0QsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxZQUFZLENBQUM7SUFDdEIsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7UUFrQkk7SUFFSixzQkFBc0I7UUFDcEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMvQixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ2pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUM7cUJBQzVGLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDcEIsTUFBTSxXQUFXLEdBQVUsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxhQUFhLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsNEJBQTRCLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3RFLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztvQkFDM0csUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQ3BDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO29CQUNULElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztvQkFDbEYsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxLQUFLO1FBQzlCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDdkMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO1lBQ3JCLElBQUksV0FBVyxDQUFDO1lBQ2hCLE1BQU0sYUFBYSxHQUFHLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDeEUsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRTtnQkFDbEMsSUFBSSxXQUFXLEtBQUssVUFBVSxFQUFFO29CQUM5QixXQUFXLEdBQUcsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3REO3FCQUFNO29CQUNMLElBQUksV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbEMsTUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDakQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDMUIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDbkMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTtnQ0FDeEIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO29DQUN4QixNQUFNLGdCQUFnQixHQUFXLENBQUMsS0FBSyxDQUFDO29DQUN4QyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0NBQzVELFdBQVcsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQztpQ0FDL0M7cUNBQU07b0NBQ0wsV0FBVyxHQUFHLFdBQVcsR0FBRyxHQUFHLENBQUM7aUNBQ2pDOzRCQUNILENBQUMsQ0FBQyxDQUFDO3dCQUVMLENBQUMsQ0FBQyxDQUFDO3FCQUNKO3lCQUFNO3dCQUNMLFdBQVcsR0FBRyxXQUFXLEdBQUcsV0FBVyxHQUFHLEdBQUcsQ0FBQztxQkFDL0M7aUJBQ0Y7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUcsT0FBTyxDQUFDLGFBQWEsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBQyxXQUFXLENBQUM7YUFDbEQ7WUFDRCxPQUFPLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRLENBQUMsSUFBSTtRQUNYLE9BQU8sQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN2RSxDQUFDO0NBRUYsQ0FBQTs7WUE5SHNCLFVBQVU7WUFDRixrQkFBa0I7WUFDakIsbUJBQW1COzs7QUFMdEMsWUFBWTtJQUh4QixVQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csWUFBWSxDQWlJeEI7U0FqSVksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1GYWNldENvbmZpZyB9IGZyb20gJy4uL29iamVjdHMvTVBNRmFjZXRDb25maWcnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGYWNldFNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBhbGxGYWNldENvbmZpZ3M6IE1QTUZhY2V0Q29uZmlnW10gPSBbXTtcclxuXHJcbiAgR2V0RmFjZXRDb25maWd1cmF0aW9uV1MgPSAnR2V0RmFjZXRDb25maWd1cmF0aW9uJztcclxuICBHZXRGYWNldENvbmZpZ3VyYXRpb25OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vd3NhcHAvY29yZS8xLjAnO1xyXG5cclxuICBnZXRDYXN0ZWRGYWNldENvbmZpZ1Jlc3BvbnNlKGZhY2V0Q29uZmlnUmVzOiBhbnlbXSk6IE1QTUZhY2V0Q29uZmlnW10ge1xyXG4gICAgY29uc3QgZmFjZXRDb25maWdzOiBNUE1GYWNldENvbmZpZ1tdID0gW107XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheShmYWNldENvbmZpZ1JlcykgJiYgZmFjZXRDb25maWdSZXMubGVuZ3RoKSB7XHJcbiAgICAgIGZhY2V0Q29uZmlnUmVzLmZvckVhY2goY29uZmlnID0+IHtcclxuICAgICAgICBjb25zdCBmYWNldENvbmZpZzogTVBNRmFjZXRDb25maWcgPSB7XHJcbiAgICAgICAgICBpZDogY29uZmlnLmlkLFxyXG4gICAgICAgICAgbmFtZTogY29uZmlnLm5hbWUsXHJcbiAgICAgICAgICBtdWx0aVNlbGVjdDogKGNvbmZpZy5tdWx0aVNlbGVjdCA9PT0gJ3RydWUnKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOiBjb25maWcuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICBkZWZhdWx0RmFjZXREaXNwbGF5ZWQ6IHBhcnNlSW50KGNvbmZpZy5kZWZhdWx0RmFjZXREaXNwbGF5ZWQsIDApLFxyXG4gICAgICAgICAgZGVmYXVsdEZhY2V0VmFsdWVEaXNwbGF5ZWQ6IHBhcnNlSW50KGNvbmZpZy5kZWZhdWx0RmFjZXRWYWx1ZURpc3BsYXllZCwgMCksXHJcbiAgICAgICAgICB2YWx1ZU9yZGVyOiBjb25maWcudmFsdWVPcmRlcixcclxuICAgICAgICAgIHZhbHVlTGltaXQ6IHBhcnNlSW50KGNvbmZpZy52YWx1ZUxpbWl0LCAwKSxcclxuICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICBmYWNldEZpZWxkOiBbXVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgZmFjZUZpZWxkczogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKGNvbmZpZywgJ2ZhY2V0RmllbGQnKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShmYWNlRmllbGRzKSAmJiBmYWNlRmllbGRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgZmFjZUZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLmlkKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgbWF0Y2hGaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoJ0lkJywgZmllbGQuZmllbGRJZCk7XHJcbiAgICAgICAgICAgICAgaWYgKG1hdGNoRmllbGQpIHtcclxuICAgICAgICAgICAgICAgIGZhY2V0Q29uZmlnLmZpZWxkcy5mYWNldEZpZWxkLnB1c2gobWF0Y2hGaWVsZCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmFjZXRDb25maWdzLnB1c2goZmFjZXRDb25maWcpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWNldENvbmZpZ3M7XHJcbiAgfVxyXG5cclxuICAvKiBzZXRGYWNldENvbmZpZ3VyYXRpb25zKCk6IE9ic2VydmFibGU8YW55W10+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuYWxsRmFjZXRDb25maWdzKSAmJiB0aGlzLmFsbEZhY2V0Q29uZmlncy5sZW5ndGgpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsRmFjZXRDb25maWdzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdldEZhY2V0Q29uZmlndXJhdGlvbk5TLCB0aGlzLkdldEZhY2V0Q29uZmlndXJhdGlvbldTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGNvbnN0IGZhY2V0Q29uZmlnOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdGYWNldENvbmZpZycpO1xyXG4gICAgICAgIHRoaXMuYWxsRmFjZXRDb25maWdzID0gdGhpcy5nZXRDYXN0ZWRGYWNldENvbmZpZ1Jlc3BvbnNlKGZhY2V0Q29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsRmFjZXRDb25maWdzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBGYWNldCBjb25maWcnKTtcclxuICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfSAqL1xyXG5cclxuICBzZXRGYWNldENvbmZpZ3VyYXRpb25zKCk6IE9ic2VydmFibGU8YW55W10+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkZBQ0VUX0NPTkZJR1VSQVRJT05TKSAhPT0gbnVsbCkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkZBQ0VUX0NPTkZJR1VSQVRJT05TKSkpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HZXRGYWNldENvbmZpZ3VyYXRpb25OUywgdGhpcy5HZXRGYWNldENvbmZpZ3VyYXRpb25XUywgbnVsbClcclxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBmYWNldENvbmZpZzogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnRmFjZXRDb25maWcnKTtcclxuICAgICAgICAgICAgdGhpcy5hbGxGYWNldENvbmZpZ3MgPSB0aGlzLmdldENhc3RlZEZhY2V0Q29uZmlnUmVzcG9uc2UoZmFjZXRDb25maWcpO1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkZBQ0VUX0NPTkZJR1VSQVRJT05TLCBKU09OLnN0cmluZ2lmeSh0aGlzLmFsbEZhY2V0Q29uZmlncykpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsRmFjZXRDb25maWdzKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIEZhY2V0IGNvbmZpZycpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZmFjZXRJbnRlcnZhbERpc3BsYXlWYWx1ZXMoZmFjZXQpIHtcclxuICAgIGZhY2V0LmZhY2V0X3ZhbHVlX2xpc3QuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgbGV0IGRpc3BsYXlOYW1lID0gJyc7XHJcbiAgICAgIGxldCBkaXNwbGF5WWVhcjtcclxuICAgICAgY29uc3QgaW50ZXJ2YWxMYWJlbCA9IChlbGVtZW50LmRhdGVfaW50ZXJ2YWwuaW50ZXJ2YWxfbGFiZWwuc3BsaXQoJyAnKSk7XHJcbiAgICAgIGludGVydmFsTGFiZWwuZm9yRWFjaChlbGVtZW50TmFtZSA9PiB7XHJcbiAgICAgICAgaWYgKGVsZW1lbnROYW1lID09PSAnJSVZRUFSJSUnKSB7XHJcbiAgICAgICAgICBkaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lICsgbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpZiAoZWxlbWVudE5hbWUuaW5jbHVkZXMoJyUlWUVBUicpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHllYXJDcmVhdGlvbiA9IGVsZW1lbnROYW1lLnNwbGl0KCclJVlFQVInKTtcclxuICAgICAgICAgICAgeWVhckNyZWF0aW9uLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc3QgeWVhclZhbHVlID0gZGF0YS5zcGxpdCgnJSUnKTtcclxuICAgICAgICAgICAgICB5ZWFyVmFsdWUuZm9yRWFjaCh2YWx1ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pc051bWJlcih2YWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgY29uc3QgYWRqdXN0bWVudE9mZnNldDogbnVtYmVyID0gK3ZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKSArIChhZGp1c3RtZW50T2Zmc2V0KTtcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWUgPSBkaXNwbGF5TmFtZSArICcgJyArIGRpc3BsYXlZZWFyO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWUgPSBkaXNwbGF5TmFtZSArICcgJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGlzcGxheU5hbWUgPSBkaXNwbGF5TmFtZSArIGVsZW1lbnROYW1lICsgJyAnO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGlmKGVsZW1lbnQuZGF0ZV9pbnRlcnZhbCkge1xyXG4gICAgICAgIGVsZW1lbnQuZGF0ZV9pbnRlcnZhbFtcImRpc3BsYXlOYW1lXCJdPWRpc3BsYXlOYW1lO1xyXG4gICAgICB9XHJcbiAgICAgIGVsZW1lbnQuZGlzcGxheU5hbWUgPSBkaXNwbGF5TmFtZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaXNOdW1iZXIoZGF0YSkge1xyXG4gICAgcmV0dXJuICgoZGF0YSA8PSAwIHx8IGRhdGEgPiAwKSAmJiAoZGF0YS5sZW5ndGggPiAwKSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=