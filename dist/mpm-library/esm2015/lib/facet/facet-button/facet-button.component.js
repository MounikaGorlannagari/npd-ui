import { __decorate } from "tslib";
import { Component, Output, Input, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
let FacetButtonComponent = class FacetButtonComponent {
    constructor() {
        this.facetToggled = new EventEmitter();
        this.expansionState = false;
        this.leftState = 'default';
        this.isLeftNavExpanded = true;
    }
    toggleFacet() {
        if (this.isRMView === true) {
            this.leftState === 'default' ? this.isLeftNavExpanded = false : this.isLeftNavExpanded = true;
            this.leftState = (this.leftState === 'default' ? 'rotated' : 'default');
        }
        this.expansionState = !this.expansionState;
        this.facetToggled.next(this.expansionState);
    }
    ngOnInit() {
        this.disabled = this.disabled ? true : false;
        this.expansionState = this.initalState ? true : false;
    }
};
__decorate([
    Input()
], FacetButtonComponent.prototype, "disabled", void 0);
__decorate([
    Input()
], FacetButtonComponent.prototype, "initalState", void 0);
__decorate([
    Input()
], FacetButtonComponent.prototype, "isRMView", void 0);
__decorate([
    Output()
], FacetButtonComponent.prototype, "facetToggled", void 0);
FacetButtonComponent = __decorate([
    Component({
        selector: 'mpm-facet-button',
        template: "<button class=\"mpm-facet-btn\" mat-icon-button (click)=\"toggleFacet()\"\r\n    [disabled]=\"disabled\" matTooltip=\"Click to toggle Facet View\">\r\n    <mat-icon *ngIf=\"isRMView === true\" color=\"primary\" [@rotatedState]='leftState'>chevron_left\r\n    </mat-icon>\r\n    <mat-icon *ngIf=\"isRMView === false\" color=\"primary\">filter_list\r\n    </mat-icon>\r\n</button>",
        animations: [
            trigger('rotatedState', [
                state('default', style({ transform: 'rotate(0deg)' })),
                state('rotated', style({ transform: 'rotate(180deg)' })),
                transition('rotated => default', animate('250ms ease-out')),
                transition('default => rotated', animate('250ms ease-in'))
            ])
        ],
        styles: [""]
    })
], FacetButtonComponent);
export { FacetButtonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQtYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L2ZhY2V0LWJ1dHRvbi9mYWNldC1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFlakYsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFhL0I7UUFQVSxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFNUMsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUN0QixzQkFBaUIsR0FBRyxJQUFJLENBQUM7SUFFVCxDQUFDO0lBRWpCLFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxFQUFFO1lBQzFCLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlGLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN6RTtRQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUU5QyxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RCxDQUFDO0NBRUYsQ0FBQTtBQTVCVTtJQUFSLEtBQUssRUFBRTtzREFBVTtBQUNUO0lBQVIsS0FBSyxFQUFFO3lEQUFhO0FBQ1o7SUFBUixLQUFLLEVBQUU7c0RBQVU7QUFFUjtJQUFULE1BQU0sRUFBRTswREFBbUM7QUFOakMsb0JBQW9CO0lBYmhDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsc1lBQTRDO1FBRTVDLFVBQVUsRUFBRTtZQUNWLE9BQU8sQ0FBQyxjQUFjLEVBQUU7Z0JBQ3BCLEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLGNBQWMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RELEtBQUssQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQztnQkFDeEQsVUFBVSxDQUFDLG9CQUFvQixFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUMzRCxVQUFVLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQzdELENBQUM7U0FDTDs7S0FDQSxDQUFDO0dBQ1csb0JBQW9CLENBOEJoQztTQTlCWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT3V0cHV0LCBJbnB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyB0cmlnZ2VyLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIGFuaW1hdGUgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWZhY2V0LWJ1dHRvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZhY2V0LWJ1dHRvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmFjZXQtYnV0dG9uLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcigncm90YXRlZFN0YXRlJywgW1xyXG4gICAgICAgIHN0YXRlKCdkZWZhdWx0Jywgc3R5bGUoeyB0cmFuc2Zvcm06ICdyb3RhdGUoMGRlZyknIH0pKSxcclxuICAgICAgICBzdGF0ZSgncm90YXRlZCcsIHN0eWxlKHsgdHJhbnNmb3JtOiAncm90YXRlKDE4MGRlZyknIH0pKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCdyb3RhdGVkID0+IGRlZmF1bHQnLCBhbmltYXRlKCcyNTBtcyBlYXNlLW91dCcpKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCdkZWZhdWx0ID0+IHJvdGF0ZWQnLCBhbmltYXRlKCcyNTBtcyBlYXNlLWluJykpXHJcbiAgICBdKVxyXG5dLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmFjZXRCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBkaXNhYmxlZDtcclxuICBASW5wdXQoKSBpbml0YWxTdGF0ZTtcclxuICBASW5wdXQoKSBpc1JNVmlldztcclxuXHJcbiAgQE91dHB1dCgpIGZhY2V0VG9nZ2xlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgZXhwYW5zaW9uU3RhdGUgPSBmYWxzZTtcclxuXHJcbiAgbGVmdFN0YXRlID0gJ2RlZmF1bHQnO1xyXG4gIGlzTGVmdE5hdkV4cGFuZGVkID0gdHJ1ZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgdG9nZ2xlRmFjZXQoKSB7XHJcbiAgICBpZiAodGhpcy5pc1JNVmlldyA9PT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLmxlZnRTdGF0ZSA9PT0gJ2RlZmF1bHQnID8gdGhpcy5pc0xlZnROYXZFeHBhbmRlZCA9IGZhbHNlIDogdGhpcy5pc0xlZnROYXZFeHBhbmRlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMubGVmdFN0YXRlID0gKHRoaXMubGVmdFN0YXRlID09PSAnZGVmYXVsdCcgPyAncm90YXRlZCcgOiAnZGVmYXVsdCcpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5leHBhbnNpb25TdGF0ZSA9ICF0aGlzLmV4cGFuc2lvblN0YXRlO1xyXG4gICAgdGhpcy5mYWNldFRvZ2dsZWQubmV4dCh0aGlzLmV4cGFuc2lvblN0YXRlKTtcclxuXHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZGlzYWJsZWQgPSB0aGlzLmRpc2FibGVkID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgdGhpcy5leHBhbnNpb25TdGF0ZSA9IHRoaXMuaW5pdGFsU3RhdGUgPyB0cnVlIDogZmFsc2U7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=