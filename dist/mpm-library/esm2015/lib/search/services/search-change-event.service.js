import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
let SearchChangeEventService = class SearchChangeEventService {
    constructor() {
        this.currentSearchViewConfig = new BehaviorSubject(null);
        this.onSearchViewChange = this.currentSearchViewConfig.asObservable();
        this.viewToggleSearch = new BehaviorSubject(null);
        this.onViewToggleSearch = this.viewToggleSearch.asObservable();
    }
    update(viewInfo) {
        this.activeSearchId = viewInfo ? viewInfo.R_PO_ADVANCED_SEARCH_CONFIG : null;
        this.currentSearchViewConfig.next(viewInfo);
    }
    setAdvancedSearchConfig(advancedSearchConfig) {
        this.advancedSearchConfig = advancedSearchConfig;
    }
    getAdvancedSearchConfigs() {
        return this.advancedSearchConfig ? this.advancedSearchConfig : [];
    }
    getAdvancedSearchConfigByName(searchIdentifier) {
        if (!searchIdentifier) {
            return;
        }
        // check this
        const currConfig = this.getAdvancedSearchConfigs().find(serachConfig => {
            return serachConfig['MPM_Advanced_Search_Config-id'].Id === searchIdentifier;
        });
        if (currConfig) {
            let configId = 0;
            try {
                configId = parseInt(currConfig['MPM_Advanced_Search_Config-id'].Id, 0);
            }
            catch (e) {
                configId = -1;
            }
            return configId;
        }
        return -1;
    }
    getFacetConfigIdForActiveSearch() {
        if (!this.activeSearchId) {
            return;
        }
        // Change it for facet
        const advConfig = this.getAdvancedSearchConfigs().find(serachConfig => {
            return serachConfig['MPM_Advanced_Search_Config-id'].Id === this.activeSearchId;
        });
        if (!advConfig) {
            return;
        }
        if (advConfig.facetConfigId) {
            return advConfig.facetConfigId;
        }
        return;
    }
    updateOnViewChange(updateAction) {
        this.viewToggleSearch.next(updateAction);
    }
};
SearchChangeEventService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchChangeEventService_Factory() { return new SearchChangeEventService(); }, token: SearchChangeEventService, providedIn: "root" });
SearchChangeEventService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SearchChangeEventService);
export { SearchChangeEventService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNoYW5nZS1ldmVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2VhcmNoL3NlcnZpY2VzL3NlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBd0IsTUFBTSxlQUFlLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFPdkMsSUFBYSx3QkFBd0IsR0FBckMsTUFBYSx3QkFBd0I7SUFVbkM7UUFQTyw0QkFBdUIsR0FBRyxJQUFJLGVBQWUsQ0FBYSxJQUFJLENBQUMsQ0FBQztRQUN2RSx1QkFBa0IsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFMUQscUJBQWdCLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsdUJBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBRzFDLENBQUM7SUFFakIsTUFBTSxDQUFDLFFBQW9CO1FBQ3pCLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUM3RSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxvQkFBb0I7UUFDMUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLG9CQUFvQixDQUFDO0lBQ25ELENBQUM7SUFFRCx3QkFBd0I7UUFDdEIsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3BFLENBQUM7SUFFRCw2QkFBNkIsQ0FBQyxnQkFBZ0I7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3JCLE9BQU87U0FDUjtRQUNELGFBQWE7UUFDYixNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDckUsT0FBTyxZQUFZLENBQUMsK0JBQStCLENBQUMsQ0FBQyxFQUFFLEtBQUssZ0JBQWdCLENBQUM7UUFDL0UsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQztZQUNqQixJQUFJO2dCQUNGLFFBQVEsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDLCtCQUErQixDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3hFO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1YsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2Y7WUFDRCxPQUFPLFFBQVEsQ0FBQztTQUNqQjtRQUNELE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDWixDQUFDO0lBRUQsK0JBQStCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3hCLE9BQU87U0FDUjtRQUNELHNCQUFzQjtRQUN0QixNQUFNLFNBQVMsR0FBNEIsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQzdGLE9BQU8sWUFBWSxDQUFDLCtCQUErQixDQUFDLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDbEYsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsT0FBTztTQUNSO1FBQ0QsSUFBSSxTQUFTLENBQUMsYUFBYSxFQUFFO1lBQzNCLE9BQU8sU0FBUyxDQUFDLGFBQWEsQ0FBQztTQUNoQztRQUNELE9BQU87SUFDVCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsWUFBOEI7UUFDL0MsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMzQyxDQUFDO0NBQ0YsQ0FBQTs7QUFqRVksd0JBQXdCO0lBSHBDLFVBQVUsQ0FBQztRQUNWLFVBQVUsRUFBRSxNQUFNO0tBQ25CLENBQUM7R0FDVyx3QkFBd0IsQ0FpRXBDO1NBakVZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTZWFyY2hFdmVudE1vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9zZWFyY2gtZXZlbnQtbW9kYWwnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNQWR2YW5jZWRTZWFyY2hDb25maWcnO1xyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2Uge1xyXG5cclxuICBwdWJsaWMgYWN0aXZlU2VhcmNoSWQ7XHJcbiAgcHVibGljIGN1cnJlbnRTZWFyY2hWaWV3Q29uZmlnID0gbmV3IEJlaGF2aW9yU3ViamVjdDxWaWV3Q29uZmlnPihudWxsKTtcclxuICBvblNlYXJjaFZpZXdDaGFuZ2UgPSB0aGlzLmN1cnJlbnRTZWFyY2hWaWV3Q29uZmlnLmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICBwdWJsaWMgdmlld1RvZ2dsZVNlYXJjaCA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgb25WaWV3VG9nZ2xlU2VhcmNoID0gdGhpcy52aWV3VG9nZ2xlU2VhcmNoLmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICBwdWJsaWMgYWR2YW5jZWRTZWFyY2hDb25maWc6IEFycmF5PGFueT47XHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgdXBkYXRlKHZpZXdJbmZvOiBWaWV3Q29uZmlnKSB7XHJcbiAgICB0aGlzLmFjdGl2ZVNlYXJjaElkID0gdmlld0luZm8gPyB2aWV3SW5mby5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgOiBudWxsO1xyXG4gICAgdGhpcy5jdXJyZW50U2VhcmNoVmlld0NvbmZpZy5uZXh0KHZpZXdJbmZvKTtcclxuICB9XHJcblxyXG4gIHNldEFkdmFuY2VkU2VhcmNoQ29uZmlnKGFkdmFuY2VkU2VhcmNoQ29uZmlnKSB7XHJcbiAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gYWR2YW5jZWRTZWFyY2hDb25maWc7XHJcbiAgfVxyXG5cclxuICBnZXRBZHZhbmNlZFNlYXJjaENvbmZpZ3MoKTogQXJyYXk8TVBNQWR2YW5jZWRTZWFyY2hDb25maWc+IHtcclxuICAgIHJldHVybiB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID8gdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA6IFtdO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeU5hbWUoc2VhcmNoSWRlbnRpZmllcik6IG51bWJlciB7XHJcbiAgICBpZiAoIXNlYXJjaElkZW50aWZpZXIpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gY2hlY2sgdGhpc1xyXG4gICAgY29uc3QgY3VyckNvbmZpZyA9IHRoaXMuZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdzKCkuZmluZChzZXJhY2hDb25maWcgPT4ge1xyXG4gICAgICByZXR1cm4gc2VyYWNoQ29uZmlnWydNUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy1pZCddLklkID09PSBzZWFyY2hJZGVudGlmaWVyO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoY3VyckNvbmZpZykge1xyXG4gICAgICBsZXQgY29uZmlnSWQgPSAwO1xyXG4gICAgICB0cnkge1xyXG4gICAgICAgIGNvbmZpZ0lkID0gcGFyc2VJbnQoY3VyckNvbmZpZ1snTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZCwgMCk7XHJcbiAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICBjb25maWdJZCA9IC0xO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBjb25maWdJZDtcclxuICAgIH1cclxuICAgIHJldHVybiAtMTtcclxuICB9XHJcblxyXG4gIGdldEZhY2V0Q29uZmlnSWRGb3JBY3RpdmVTZWFyY2goKTogc3RyaW5nIHtcclxuICAgIGlmICghdGhpcy5hY3RpdmVTZWFyY2hJZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyBDaGFuZ2UgaXQgZm9yIGZhY2V0XHJcbiAgICBjb25zdCBhZHZDb25maWc6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnID0gdGhpcy5nZXRBZHZhbmNlZFNlYXJjaENvbmZpZ3MoKS5maW5kKHNlcmFjaENvbmZpZyA9PiB7XHJcbiAgICAgIHJldHVybiBzZXJhY2hDb25maWdbJ01QTV9BZHZhbmNlZF9TZWFyY2hfQ29uZmlnLWlkJ10uSWQgPT09IHRoaXMuYWN0aXZlU2VhcmNoSWQ7XHJcbiAgICB9KTtcclxuICAgIGlmICghYWR2Q29uZmlnKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChhZHZDb25maWcuZmFjZXRDb25maWdJZCkge1xyXG4gICAgICByZXR1cm4gYWR2Q29uZmlnLmZhY2V0Q29uZmlnSWQ7XHJcbiAgICB9XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICB1cGRhdGVPblZpZXdDaGFuZ2UodXBkYXRlQWN0aW9uOiBTZWFyY2hFdmVudE1vZGFsKSB7XHJcbiAgICB0aGlzLnZpZXdUb2dnbGVTZWFyY2gubmV4dCh1cGRhdGVBY3Rpb24pO1xyXG4gIH1cclxufVxyXG4iXX0=