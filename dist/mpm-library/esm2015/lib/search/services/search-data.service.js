import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
let SearchDataService = class SearchDataService {
    constructor() {
        this.searchDataSubject = new BehaviorSubject([]);
        this.searchData = this.searchDataSubject.asObservable();
        this.KEYWORD_MAPPER = 'MPM.FIELD.ALL_KEYWORD';
    }
    setSearchData(data) {
        this.searchDataSubject.next(data);
    }
};
SearchDataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchDataService_Factory() { return new SearchDataService(); }, token: SearchDataService, providedIn: "root" });
SearchDataService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SearchDataService);
export { SearchDataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWRhdGEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7O0FBTXZDLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBSzFCO1FBSE8sc0JBQWlCLEdBQUcsSUFBSSxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkQsZUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM1QyxtQkFBYyxHQUFHLHVCQUF1QixDQUFDO0lBQ2hDLENBQUM7SUFFakIsYUFBYSxDQUFDLElBQVc7UUFDckIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0NBRUosQ0FBQTs7QUFYWSxpQkFBaUI7SUFKN0IsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztHQUVXLGlCQUFpQixDQVc3QjtTQVhZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hEYXRhU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHNlYXJjaERhdGFTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdChbXSk7XHJcbiAgICBzZWFyY2hEYXRhID0gdGhpcy5zZWFyY2hEYXRhU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAgIHB1YmxpYyBLRVlXT1JEX01BUFBFUiA9ICdNUE0uRklFTEQuQUxMX0tFWVdPUkQnO1xyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICBzZXRTZWFyY2hEYXRhKGRhdGE6IGFueVtdKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hEYXRhU3ViamVjdC5uZXh0KGRhdGEpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=