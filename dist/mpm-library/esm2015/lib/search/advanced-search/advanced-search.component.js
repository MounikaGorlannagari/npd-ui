import { __decorate, __param } from "tslib";
import { Component, Inject, HostListener } from '@angular/core';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SearchSaveService } from '../../project/shared/services/search.save.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { DatePipe } from '@angular/common';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMSearchOperators } from '../../../lib/shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../../lib/shared/services/indexer/constants/MPMSearchOperatorName.constants';
let AdvancedSearchComponent = class AdvancedSearchComponent {
    constructor(dialogRef, data, loaderService, notificationService, searchSaveService, formatToLocalePipe, sharingService, searchConfigService, fieldConfigService, datePipe) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.searchSaveService = searchSaveService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.sharingService = sharingService;
        this.searchConfigService = searchConfigService;
        this.fieldConfigService = fieldConfigService;
        this.datePipe = datePipe;
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.filterRowList = [];
        this.searchTobeSavedName = '';
        this.keywords = [];
        this.allSearchFields = [];
        this.availableSearchFields = [];
    }
    keyEvent(event) {
        if (event.key === 'Enter') {
            this.close(false);
        }
    }
    add(event) {
        const input = event.input;
        let value = event.value;
        // Add the keyword
        value = (value || '').trim();
        if (value) {
            this.keywords.push({ name: value });
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
    }
    remove(keyword) {
        const index = this.keywords.indexOf(keyword);
        if (index >= 0) {
            this.keywords.splice(index, 1);
        }
    }
    close(data) {
        if (data && data !== true) {
            this.sharingService.setRecentSearch(data);
        }
        if (data === true && this.data && this.data.recentSearch) {
            this.data.recentSearch.map(rowData => {
                if (rowData.isEdit) {
                    rowData.isEdit = false;
                }
            });
        }
        this.dialogRef.close(data);
    }
    filterSelectionChange(event) {
        this.setAvialbleSearchFiels();
    }
    getFieldDetails(fieldId) {
        return this.data.advancedSearchConfigData.metadataFields.filter(field => {
            return field['MPM_Fields_Config-id'].Id === fieldId;
        });
    }
    // to remove all fields
    clearFilter() {
        this.filterRowList = [];
        // this.addFilter();
        this.setAvialbleSearchFiels();
    }
    // to remove selected field
    removeFilter(event) {
        if (event && event.rowId) {
            this.filterRowList = this.filterRowList.filter(rowData => {
                return rowData.rowId !== event.rowId;
            });
        }
        this.setAvialbleSearchFiels();
    }
    // to add new field
    addFilter() {
        let canAddNewFilter = true;
        this.filterRowList.forEach(rowData => {
            if (rowData && !rowData.isFilterSelected) {
                canAddNewFilter = false;
            }
        });
        if (canAddNewFilter) {
            if (this.availableSearchFields.length > 0) {
                const count = this.filterRowList.length + 1;
                this.filterRowList.push({
                    rowId: count,
                    searchField: '',
                    searchOperatorId: '',
                    searchOperatorName: '',
                    searchValue: '',
                    searchSecondValue: '',
                    issearchValueRequired: true,
                    isFilterSelected: false,
                    hasTwoValues: '',
                    availableSearchFields: this.availableSearchFields
                });
            }
            else {
                this.notificationService.info('No more filters to add');
                return;
            }
        }
        else {
            this.notificationService.info('Kindly select the previous filter first');
        }
    }
    validateForSearch() {
        let allFilterFieldSelected = true;
        let isValid = true;
        let emptyValue = false;
        this.filterRowList.forEach(filter => {
            const searchField = filter.availableSearchFields.find(data => (filter.searchField === data.MAPPER_NAME || filter.searchField === data.MPM_FIELD_CONFIG_ID));
            if (!searchField.IS_DROP_DOWN) {
                if (filter.searchField) {
                    if (filter.searchOperatorId) {
                        if (filter.issearchValueRequired) {
                            if (filter.searchValue) {
                                if (filter.hasTwoValues && !filter.searchSecondValue) {
                                    isValid = false;
                                }
                            }
                            else {
                                isValid = false;
                            }
                        }
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                }
            }
            if (typeof (filter.searchValue) === 'string' && filter.searchValue.trim() === '' && filter.issearchValueRequired) {
                isValid = false;
                emptyValue = true;
            }
            if (!isValid || !filter.isFilterSelected) {
                allFilterFieldSelected = false;
            }
        });
        if (emptyValue) {
            this.notificationService.info('Kindly enter the value to be searched');
            return false;
        }
        else if (!allFilterFieldSelected) {
            this.notificationService.info('Kindly select all field');
            return false;
        }
        else {
            return true;
        }
    }
    converToLocalDate(date) {
        const isoStringToLocale = acronui.getDateWithLocaleTimeZone(date);
        return this.formatToLocalePipe.transform(isoStringToLocale, 'MM/dd/yyyy');
    }
    saveSearch(searchConditions, viewName) {
        if (!this.searchTobeSavedName) {
            this.notificationService.info('Kindly add search name');
            return;
        }
        if (this.searchTobeSavedName.length > 64) {
            this.notificationService.info('Search name cannot be more than 64 characters');
            return;
        }
        let hasDuplicateSearchName = false;
        this.data.availableSavedSearch.forEach(savedSearch => {
            if ((savedSearch.NAME).toLowerCase() === (this.searchTobeSavedName).toLowerCase()) {
                hasDuplicateSearchName = true;
            }
        });
        if (hasDuplicateSearchName && (!this.data.savedSearch || (typeof (this.data.savedSearch) === 'object') && this.data.savedSearch.NAME !== this.searchTobeSavedName)) { // || typeof (this.data.savedSearch) === 'object')
            this.notificationService.error('Search Name already exists, Kindly change the name');
            return;
        }
        const searchConditionList = {
            search_condition_list: {
                search_condition: searchConditions
            }
        };
        const savedSearchId = (this.data.savedSearch && this.data.savedSearch['MPM_Saved_Searches-id'] &&
            this.data.savedSearch['MPM_Saved_Searches-id'].Id) ? this.data.savedSearch['MPM_Saved_Searches-id'].Id : '';
        this.loaderService.show();
        if (savedSearchId) {
            this.searchSaveService.updateSavedSearch(savedSearchId, this.searchTobeSavedName, this.searchTobeSavedName, searchConditionList, false, viewName).subscribe(response => {
                this.loaderService.hide();
                const data = searchConditions;
                data[0].NAME = response['MPM_Saved_Searches'].NAME;
                this.close(data);
            }, error => {
                this.loaderService.hide();
                this.notificationService.error('Error while saving search');
            });
        }
        else {
            this.searchSaveService.saveSearch(this.searchTobeSavedName, this.searchTobeSavedName, searchConditionList, false, viewName).subscribe(response => {
                this.loaderService.hide();
                const data = searchConditions;
                data[0].NAME = response['MPM_Saved_Searches'].NAME;
                this.close(data);
            }, error => {
                this.loaderService.hide();
                this.notificationService.error('Error while saving search');
            });
        }
    }
    search(isSavedSearch) {
        if (this.validateForSearch()) {
            const searchConditions = [];
            this.filterRowList.forEach(rowData => {
                if (rowData.searchValue && rowData.searchValue instanceof Date) {
                    rowData.searchValue = this.datePipe.transform(new Date(rowData.searchValue), 'yyyy-MM-dd').concat('T00:00:00Z');
                    if (rowData.searchSecondValue && rowData.searchSecondValue instanceof Date) {
                        rowData.searchSecondValue = this.datePipe.transform(new Date(rowData.searchSecondValue), 'yyyy-MM-dd').concat('T00:00:00Z');
                    }
                }
                // rowData.availableSearchFields = [];
                const currMPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MPM_FIELD_CONFIG_ID, rowData.searchField);
                const fieldId = currMPMField ? this.fieldConfigService.formIndexerIdFromField(currMPMField) : rowData.searchField;
                if (rowData.isComboType) {
                    const values = rowData.searchValue;
                    const condidtion = {
                        isAdvancedSearch: true,
                        searchIdentifier: this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG,
                        field_id: fieldId,
                        relational_operator_id: rowData.searchOperatorId ? rowData.searchOperatorId : MPMSearchOperators.IS,
                        relational_operator_name: rowData.searchOperatorName ? rowData.searchOperatorName : MPMSearchOperatorNames.IS,
                        type: currMPMField ? currMPMField.DATA_TYPE : 'string',
                        mapper_field_id: rowData.searchField,
                        mapper_name: currMPMField.MAPPER_NAME,
                        view: this.data.advancedSearchConfigData.searchIdentifier.VIEW
                    };
                    if (values.length === 1) {
                        const conditionCopy = Object.assign({}, condidtion);
                        conditionCopy.filterRowData = rowData;
                        conditionCopy.left_paren = '(';
                        conditionCopy.right_paren = ')';
                        conditionCopy.display_value = values[0].display_value;
                        conditionCopy.value = values[0].field_value.value;
                        conditionCopy.relational_operator = 'and';
                        searchConditions.push(conditionCopy);
                    }
                    else {
                        const lastValueIndex = values.length - 1;
                        for (let i = 0; i < values.length; i++) {
                            const conditionCopy = Object.assign({}, condidtion);
                            conditionCopy.display_value = values[i].display_value;
                            conditionCopy.value = values[i].field_value.value;
                            if (i === 0) {
                                conditionCopy.left_paren = '(';
                                conditionCopy.filterRowData = rowData;
                            }
                            else if (i === lastValueIndex) {
                                conditionCopy.right_paren = ')';
                                conditionCopy.relational_operator = 'or';
                            }
                            else {
                                conditionCopy.relational_operator = 'or';
                            }
                            searchConditions.push(conditionCopy);
                        }
                    }
                }
                else {
                    let value = rowData.searchValue;
                    if (rowData.searchSecondValue) {
                        // if (rowData.searchSecondValue instanceof Date && rowData.issearchValueRequired) {
                        //   rowData.searchSecondValue = this.converToLocalDate(rowData.searchSecondValue);
                        // }
                        /* if (rowData.searchSecondValue instanceof Date) {
                          // value = value.toISOString() + ' and ' + rowData.searchSecondValue.toISOString();
                          value = this.datePipe.transform(new Date(rowData.searchValue), 'yyyy-MM-dd').concat('T00:00:00Z') + ' and ' +
                                  this.datePipe.transform(new Date(rowData.searchSecondValue), 'yyyy-MM-dd').concat('T00:00:00Z')
                        } else { */
                        value = value + ' and ' + rowData.searchSecondValue;
                        // }
                    }
                    const condition = {
                        isAdvancedSearch: true,
                        searchIdentifier: this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG,
                        filterRowData: rowData,
                        field_id: fieldId,
                        type: currMPMField ? currMPMField.DATA_TYPE : 'string',
                        value: value,
                        relational_operator_id: rowData.searchOperatorId ? rowData.searchOperatorId : MPMSearchOperators.IS,
                        relational_operator_name: rowData.searchOperatorName ? rowData.searchOperatorName : MPMSearchOperatorNames.IS,
                        relational_operator: 'and',
                        mapper_field_id: rowData.searchField,
                        mapper_name: currMPMField.MAPPER_NAME,
                        view: this.data.advancedSearchConfigData.searchIdentifier.VIEW
                    };
                    searchConditions.push(condition);
                }
            });
            if (isSavedSearch) {
                if (this.data && this.data.recentSearch) {
                    this.data.recentSearch.map(rowData => {
                        if (rowData.isEdit) {
                            rowData.isEdit = false;
                        }
                    });
                }
                this.saveSearch(searchConditions, this.data.advancedSearchConfigData.searchIdentifier.VIEW);
            }
            else {
                this.close(searchConditions);
            }
        }
    }
    isFieldAlreadySelected(fieldId) {
        if (!fieldId) {
            return true;
        }
        return this.filterRowList.find(row => {
            return row.searchField === fieldId;
        });
    }
    setAvialbleSearchFiels() {
        this.availableSearchFields = [];
        if (!Array.isArray(this.allSearchFields)) {
            return;
        }
        for (const field of this.allSearchFields) {
            if (field && field.MPM_FIELD_CONFIG_ID &&
                !this.isFieldAlreadySelected(field.MPM_FIELD_CONFIG_ID)) {
                this.availableSearchFields.push(field);
            }
        }
        // this.advancedSearchConfigData.availableSearchFields = this.availableSearchFields;
    }
    mapSeachConfig() {
        if (this.data.savedSearch) {
            this.searchTobeSavedName = this.data.savedSearch.NAME;
            let conditions = [];
            if (this.data.savedSearch.SEARCH_DATA &&
                this.data.savedSearch.SEARCH_DATA.search_condition_list &&
                this.data.savedSearch.SEARCH_DATA.search_condition_list.search_condition) {
                conditions = this.data.savedSearch.SEARCH_DATA.search_condition_list.search_condition;
            }
            if (conditions && conditions[0].searchIdentifier) {
                const advConfig = this.searchConfigService.getAdvancedSearchConfigById(conditions[0].searchIdentifier);
                this.allSearchFields = advConfig.R_PM_Search_Fields;
                conditions.forEach(condition => {
                    const count = this.filterRowList.length + 1;
                    this.setAvialbleSearchFiels();
                    const value = condition.value != null ? condition.value : '';
                    /* this.filterRowList.push({
                      rowId: count,
                      searchField: condition.mapper_field_id,
                      searchOperatorId: condition.relational_operator_id,
                      searchOperatorName: condition.relational_operator_name,
                      searchValue: condition.value,
                      searchSecondValue: condition.value.split('and ').length > 0 ? condition.value.split('and ')[1] : '',
                      issearchValueRequired: condition.value ? true : false,
                      isFilterSelected: true,
                      hasTwoValues: '',
                      availableSearchFields: this.allSearchFields.filter(searchFieldItem => {
                        return searchFieldItem.MPM_FIELD_CONFIG_ID === condition.mapper_field_id;
                      })
                    }); */
                    this.filterRowList.push({
                        rowId: count,
                        searchField: condition.mapper_field_id,
                        searchOperatorId: condition.relational_operator_id,
                        searchOperatorName: condition.relational_operator_name,
                        searchValue: value,
                        searchSecondValue: value.split('and ').length > 0 ? value.split('and ')[1] !== undefined ? value.split('and ')[1] : '' : '',
                        issearchValueRequired: value ? true : false,
                        isFilterSelected: true,
                        hasTwoValues: '',
                        availableSearchFields: this.allSearchFields.filter(searchFieldItem => {
                            return searchFieldItem.MPM_FIELD_CONFIG_ID === condition.mapper_field_id;
                        })
                    });
                    this.setAvialbleSearchFiels();
                });
                this.advancedSearchConfigData.availableSearchFields = this.allSearchFields;
                this.advancedSearchConfigData.searchOperatorList = this.data.advancedSearchConfigData.searchOperatorList;
            }
        }
        else {
            if (this.data.advancedSearchConfigData && this.data.advancedSearchConfigData.searchIdentifier) {
                const searchConfig = this.data.advancedSearchConfigData.advancedSearchConfiguration.filter(serachConfig => {
                    return serachConfig['MPM_Advanced_Search_Config-id'].Id === this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG;
                });
                // this.data.advancedSearchConfigData.searchIdentifier = searchConfig[0]['MPM_Advanced_Search_Config-id'].Id;
                this.allSearchFields = searchConfig[0].R_PM_Search_Fields;
                this.advancedSearchConfigData.availableSearchFields = searchConfig[0].R_PM_Search_Fields;
                this.advancedSearchConfigData.searchOperatorList = this.data.advancedSearchConfigData.searchOperatorList;
                this.setAvialbleSearchFiels();
                // this.addFilter();
            }
        }
    }
    ngOnInit() {
        this.advancedSearchConfigData = {
            availableSearchFields: [],
            searchOperatorList: []
        };
        this.mapSeachConfig();
        if (this.data.recentSearch) {
            this.filterRowList = [];
            this.data.recentSearch.map(rowData => {
                if (rowData.filterRowData) {
                    rowData.filterRowData.availableSearchFields = this.allSearchFields.filter(searchFieldItem => {
                        return searchFieldItem.MPM_FIELD_CONFIG_ID === rowData.filterRowData.searchField;
                    });
                    this.filterRowList.push(rowData.filterRowData);
                    this.setAvialbleSearchFiels();
                }
            });
        }
    }
};
AdvancedSearchComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: LoaderService },
    { type: NotificationService },
    { type: SearchSaveService },
    { type: DatePipe },
    { type: SharingService },
    { type: SearchConfigService },
    { type: FieldConfigService },
    { type: DatePipe }
];
__decorate([
    HostListener('window:keyup', ['$event'])
], AdvancedSearchComponent.prototype, "keyEvent", null);
AdvancedSearchComponent = __decorate([
    Component({
        selector: 'mpm-advanced-search',
        template: "<div class=\"advanced-search-modal\">\r\n\t<div class=\"search-header\">\r\n\t\t<button (click)=\"close(true)\" mat-icon-button color=\"primary\" matTooltip=\"Navigate back to simple search\">\r\n\t\t\t<mat-icon>keyboard_backspace</mat-icon>\r\n\t\t</button>\r\n\t\t<span>Back To Normal Search</span>\r\n\t\t<button (click)=\"close(true)\" class=\"right-align\" mat-icon-button color=\"primary\" matTooltip=\"Exit Search\">\r\n\t\t\t<mat-icon>close</mat-icon>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"field-action\">\r\n\t\t<button (click)=\"addFilter()\" mat-flat-button class=\"trySpace\" matTooltip=\"Add Filter\"\r\n\t\t\t[disabled]=\"!availableSearchFields.length\">\r\n\t\t\t<mat-icon>add</mat-icon>\r\n\t\t\tAdd Filter\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"field-options\">\r\n\t\t<span *ngFor=\"let row of filterRowList\">\r\n\t\t\t<mpm-advanced-search-field [filterRowData]=\"row\" [advancedSearchConfigData]=\"advancedSearchConfigData\"\r\n\t\t\t\t(removeFilterField)=\"removeFilter($event)\" (filterSelected)=\"filterSelectionChange($event)\">\r\n\t\t\t</mpm-advanced-search-field>\r\n\t\t</span>\r\n\t\t<div class=\"info\" *ngIf=\"!filterRowList.length\">\r\n\t\t\t<p>Click on Add Filter to start adding filters</p>\r\n\t\t</div>\r\n\t</div>\r\n\t<div>\r\n\t\t<mat-form-field appearance=\"outline\" class=\"search-name\">\r\n\t\t\t<mat-label>Search Name</mat-label>\r\n\t\t\t<input [(ngModel)]=\"searchTobeSavedName\" matInput placeholder=\"Provide a name to the Search\">\r\n\t\t</mat-form-field>\r\n\t\t<button (click)=\"search(true)\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Save and Perform Search\">Save and Search</button>\r\n\t\t<button (click)=\"search(false)\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Search\">Search</button>\r\n\t\t<button (click)=\"clearFilter()\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Clear All\">Clear All</button>\r\n\t</div>\r\n</div>",
        styles: [".advanced-search-modal{font-size:12px!important;min-width:900px}.example-chip-list{width:100%}.trySpace{margin-right:10px}.filter-row{margin:10px;width:95%!important}.filter-field{max-height:55vh;min-height:21vh}button,mat-form-field{font-size:12px!important;margin:5px}.search-header{margin-bottom:20px}.field-action{margin-left:-20px}.search-name{width:435px}.field-options{max-height:40vh;overflow-y:auto;min-height:310px}.info{width:100%;text-align:center}.searchName{display:inline-block}.right-align{float:right}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], AdvancedSearchComponent);
export { AdvancedSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9hZHZhbmNlZC1zZWFyY2gvYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXhFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUczQyxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFFbEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNEVBQTRFLENBQUM7QUFDaEgsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZ0ZBQWdGLENBQUM7QUFXeEgsSUFBYSx1QkFBdUIsR0FBcEMsTUFBYSx1QkFBdUI7SUFhbEMsWUFDUyxTQUFnRCxFQUN2QixJQUF3QixFQUNqRCxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGtCQUE0QixFQUM1QixjQUE4QixFQUM5QixtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLFFBQWtCO1FBVGxCLGNBQVMsR0FBVCxTQUFTLENBQXVDO1FBQ3ZCLFNBQUksR0FBSixJQUFJLENBQW9CO1FBQ2pELGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQVU7UUFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBckIzQixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIsd0JBQW1CLEdBQUcsRUFBRSxDQUFDO1FBRXpCLGFBQVEsR0FBYyxFQUFFLENBQUM7UUFDekIsb0JBQWUsR0FBZSxFQUFFLENBQUM7UUFDakMsMEJBQXFCLEdBQWUsRUFBRSxDQUFDO0lBYW5DLENBQUM7SUFHTCxRQUFRLENBQUMsS0FBb0I7UUFDM0IsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLE9BQU8sRUFBRTtZQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELEdBQUcsQ0FBQyxLQUF3QjtRQUMxQixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFFeEIsa0JBQWtCO1FBQ2xCLEtBQUssR0FBRyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM3QixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7U0FDckM7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxLQUFLLEVBQUU7WUFDVCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUNsQjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsT0FBZ0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFN0MsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUVELEtBQUssQ0FBQyxJQUFVO1FBQ2QsSUFBSSxJQUFJLElBQUksSUFBSSxLQUFLLElBQUksRUFBRTtZQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDbkMsSUFBSSxPQUFPLENBQUMsTUFBTSxFQUFFO29CQUNsQixPQUFPLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztpQkFDeEI7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELHFCQUFxQixDQUFDLEtBQUs7UUFDekIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFPO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3RFLE9BQU8sS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx1QkFBdUI7SUFDdkIsV0FBVztRQUNULElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsMkJBQTJCO0lBQzNCLFlBQVksQ0FBQyxLQUFLO1FBQ2hCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDdkQsT0FBTyxPQUFPLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDdkMsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxtQkFBbUI7SUFDbkIsU0FBUztRQUNQLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNuQyxJQUFJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDeEMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxlQUFlLEVBQUU7WUFDbkIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDekMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztvQkFDdEIsS0FBSyxFQUFFLEtBQUs7b0JBQ1osV0FBVyxFQUFFLEVBQUU7b0JBQ2YsZ0JBQWdCLEVBQUUsRUFBRTtvQkFDcEIsa0JBQWtCLEVBQUUsRUFBRTtvQkFDdEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsaUJBQWlCLEVBQUUsRUFBRTtvQkFDckIscUJBQXFCLEVBQUUsSUFBSTtvQkFDM0IsZ0JBQWdCLEVBQUUsS0FBSztvQkFDdkIsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLHFCQUFxQixFQUFFLElBQUksQ0FBQyxxQkFBcUI7aUJBQ2xELENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEQsT0FBTzthQUNSO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUMxRTtJQUNILENBQUM7SUFFRCxpQkFBaUI7UUFDZixJQUFJLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2xDLE1BQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7WUFDNUosSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUU7Z0JBQzdCLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRTtvQkFDdEIsSUFBSSxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7d0JBQzNCLElBQUksTUFBTSxDQUFDLHFCQUFxQixFQUFFOzRCQUNoQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7Z0NBQ3RCLElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRTtvQ0FDcEQsT0FBTyxHQUFHLEtBQUssQ0FBQztpQ0FDakI7NkJBQ0Y7aUNBQU07Z0NBQ0wsT0FBTyxHQUFHLEtBQUssQ0FBQzs2QkFDakI7eUJBQ0Y7cUJBQ0Y7eUJBQU07d0JBQ0wsT0FBTyxHQUFHLEtBQUssQ0FBQztxQkFDakI7aUJBQ0Y7cUJBQU07b0JBQ0wsT0FBTyxHQUFHLEtBQUssQ0FBQztpQkFDakI7YUFDRjtZQUVELElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxRQUFRLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksTUFBTSxDQUFDLHFCQUFxQixFQUFFO2dCQUNoSCxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNoQixVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQ25CO1lBR0QsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDeEMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztZQUN2RSxPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU0sSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQ2xDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUN6RCxPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDcEIsTUFBTSxpQkFBaUIsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFRCxVQUFVLENBQUMsZ0JBQWlDLEVBQUUsUUFBZ0I7UUFDNUQsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDeEQsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLCtDQUErQyxDQUFDLENBQUM7WUFDL0UsT0FBTztTQUNSO1FBQ0QsSUFBSSxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDbkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO2dCQUNqRixzQkFBc0IsR0FBRyxJQUFJLENBQUM7YUFDL0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksc0JBQXNCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUUsa0RBQWtEO1lBQ3ROLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQztZQUNyRixPQUFPO1NBQ1I7UUFDRCxNQUFNLG1CQUFtQixHQUFHO1lBQzFCLHFCQUFxQixFQUFFO2dCQUNyQixnQkFBZ0IsRUFBRSxnQkFBZ0I7YUFDbkM7U0FDRixDQUFDO1FBQ0YsTUFBTSxhQUFhLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQztZQUM1RixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzlHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxhQUFhLEVBQUU7WUFDakIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUN4RyxtQkFBbUIsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN6RCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixNQUFNLElBQUksR0FBUSxnQkFBZ0IsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNULElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUM5RCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLEVBQ2xGLG1CQUFtQixFQUFFLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLE1BQU0sSUFBSSxHQUFRLGdCQUFnQixDQUFDO2dCQUNuQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBQzlELENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGFBQWE7UUFDbEIsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRTtZQUM1QixNQUFNLGdCQUFnQixHQUFvQixFQUFFLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksT0FBTyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVyxZQUFZLElBQUksRUFBRTtvQkFDOUQsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNoSCxJQUFJLE9BQU8sQ0FBQyxpQkFBaUIsSUFBSSxPQUFPLENBQUMsaUJBQWlCLFlBQVksSUFBSSxFQUFFO3dCQUMxRSxPQUFPLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUM3SDtpQkFDRjtnQkFDRCxzQ0FBc0M7Z0JBQ3RDLE1BQU0sWUFBWSxHQUFhLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNqSSxNQUFNLE9BQU8sR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztnQkFDbEgsSUFBSSxPQUFPLENBQUMsV0FBVyxFQUFFO29CQUN2QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO29CQUNuQyxNQUFNLFVBQVUsR0FBa0I7d0JBQ2hDLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsMkJBQTJCO3dCQUNqRyxRQUFRLEVBQUUsT0FBTzt3QkFDakIsc0JBQXNCLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7d0JBQ25HLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO3dCQUM3RyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFRO3dCQUN0RCxlQUFlLEVBQUUsT0FBTyxDQUFDLFdBQVc7d0JBQ3BDLFdBQVcsRUFBRSxZQUFZLENBQUMsV0FBVzt3QkFDckMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsSUFBSTtxQkFDL0QsQ0FBQztvQkFDRixJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO3dCQUN2QixNQUFNLGFBQWEsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDcEQsYUFBYSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7d0JBQ3RDLGFBQWEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO3dCQUMvQixhQUFhLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQzt3QkFDaEMsYUFBYSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDO3dCQUN0RCxhQUFhLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO3dCQUNsRCxhQUFhLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO3dCQUMxQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7cUJBQ3RDO3lCQUFNO3dCQUNMLE1BQU0sY0FBYyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO3dCQUN6QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDdEMsTUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUM7NEJBQ3BELGFBQWEsQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQzs0QkFDdEQsYUFBYSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDbEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dDQUNYLGFBQWEsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDO2dDQUMvQixhQUFhLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQzs2QkFDdkM7aUNBQU0sSUFBSSxDQUFDLEtBQUssY0FBYyxFQUFFO2dDQUMvQixhQUFhLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztnQ0FDaEMsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzs2QkFDMUM7aUNBQU07Z0NBQ0wsYUFBYSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzs2QkFDMUM7NEJBQ0QsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3lCQUN0QztxQkFDRjtpQkFDRjtxQkFBTTtvQkFDTCxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDO29CQUNoQyxJQUFJLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRTt3QkFDN0Isb0ZBQW9GO3dCQUNwRixtRkFBbUY7d0JBQ25GLElBQUk7d0JBQ0o7Ozs7bUNBSVc7d0JBQ1gsS0FBSyxHQUFHLEtBQUssR0FBRyxPQUFPLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO3dCQUNwRCxJQUFJO3FCQUNMO29CQUNELE1BQU0sU0FBUyxHQUFrQjt3QkFDL0IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQkFBZ0IsQ0FBQywyQkFBMkI7d0JBQ2pHLGFBQWEsRUFBRSxPQUFPO3dCQUN0QixRQUFRLEVBQUUsT0FBTzt3QkFDakIsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUTt3QkFDdEQsS0FBSyxFQUFFLEtBQUs7d0JBQ1osc0JBQXNCLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7d0JBQ25HLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO3dCQUM3RyxtQkFBbUIsRUFBRSxLQUFLO3dCQUMxQixlQUFlLEVBQUUsT0FBTyxDQUFDLFdBQVc7d0JBQ3BDLFdBQVcsRUFBRSxZQUFZLENBQUMsV0FBVzt3QkFDckMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsSUFBSTtxQkFDL0QsQ0FBQztvQkFDRixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ2xDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGFBQWEsRUFBRTtnQkFDakIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO29CQUN2QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ25DLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTs0QkFDbEIsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7eUJBQ3hCO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM3RjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDOUI7U0FDRjtJQUNILENBQUM7SUFFRCxzQkFBc0IsQ0FBQyxPQUFlO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNuQyxPQUFPLEdBQUcsQ0FBQyxXQUFXLEtBQUssT0FBTyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNCQUFzQjtRQUNwQixJQUFJLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUN4QyxPQUFPO1NBQ1I7UUFDRCxLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLG1CQUFtQjtnQkFDcEMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDeEM7U0FDRjtRQUNELG9GQUFvRjtJQUN0RixDQUFDO0lBRUQsY0FBYztRQUNaLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDekIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztZQUN0RCxJQUFJLFVBQVUsR0FBb0IsRUFBRSxDQUFDO1lBQ3JDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVztnQkFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtnQkFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixFQUFFO2dCQUMxRSxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDO2FBQ3ZGO1lBQ0QsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO2dCQUNoRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsMkJBQTJCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3ZHLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLGtCQUFrQixDQUFDO2dCQUNwRCxVQUFVLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFO29CQUM3QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBQzVDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUM5QixNQUFNLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUM3RDs7Ozs7Ozs7Ozs7OzswQkFhTTtvQkFDTixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLEtBQUs7d0JBQ1osV0FBVyxFQUFFLFNBQVMsQ0FBQyxlQUFlO3dCQUN0QyxnQkFBZ0IsRUFBRSxTQUFTLENBQUMsc0JBQXNCO3dCQUNsRCxrQkFBa0IsRUFBRSxTQUFTLENBQUMsd0JBQXdCO3dCQUN0RCxXQUFXLEVBQUUsS0FBSzt3QkFDbEIsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUMzSCxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSzt3QkFDM0MsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLHFCQUFxQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxFQUFFOzRCQUNuRSxPQUFPLGVBQWUsQ0FBQyxtQkFBbUIsS0FBSyxTQUFTLENBQUMsZUFBZSxDQUFDO3dCQUMzRSxDQUFDLENBQUM7cUJBQ0gsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUNoQyxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztnQkFDM0UsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUM7YUFDMUc7U0FDRjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQzdGLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsMkJBQTJCLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUN4RyxPQUFPLFlBQVksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLDJCQUEyQixDQUFDO2dCQUM5SSxDQUFDLENBQUMsQ0FBQztnQkFFSCw2R0FBNkc7Z0JBRTdHLElBQUksQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDO2dCQUMxRCxJQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDO2dCQUN6RixJQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDekcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzlCLG9CQUFvQjthQUNyQjtTQUNGO0lBQ0gsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsd0JBQXdCLEdBQUc7WUFDOUIscUJBQXFCLEVBQUUsRUFBRTtZQUN6QixrQkFBa0IsRUFBRSxFQUFFO1NBQ3ZCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksT0FBTyxDQUFDLGFBQWEsRUFBRTtvQkFDekIsT0FBTyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFBRTt3QkFDMUYsT0FBTyxlQUFlLENBQUMsbUJBQW1CLEtBQUssT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7b0JBQ25GLENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQy9CO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Q0FDRixDQUFBOztZQTVhcUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7WUFDRCxhQUFhO1lBQ1AsbUJBQW1CO1lBQ3JCLGlCQUFpQjtZQUNoQixRQUFRO1lBQ1osY0FBYztZQUNULG1CQUFtQjtZQUNwQixrQkFBa0I7WUFDNUIsUUFBUTs7QUFJM0I7SUFEQyxZQUFZLENBQUMsY0FBYyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7dURBS3hDO0FBL0JVLHVCQUF1QjtJQUxuQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUscUJBQXFCO1FBQy9CLDJrRUFBK0M7O0tBRWhELENBQUM7SUFnQkcsV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FmZix1QkFBdUIsQ0EwYm5DO1NBMWJZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRDaGlwSW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NoaXBzJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNlYXJjaFNhdmVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvc2VhcmNoLnNhdmUuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hEYXRhIH0gZnJvbSAnLi4vb2JqZWN0cy9BZHZhbmNlZFNlYXJjaERhdGEnO1xyXG5pbXBvcnQgeyBBZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEgfSBmcm9tICcuLi9vYmplY3RzL0FkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkLCBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvc2VhcmNoLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29uZGl0aW9uTGlzdCB9IGZyb20gJy4uL29iamVjdHMvQ29uZGl0aW9uTGlzdCc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9ycyB9IGZyb20gJy4uLy4uLy4uL2xpYi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3IuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB9IGZyb20gJy4uLy4uLy4uL2xpYi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEtleXdvcmQge1xyXG4gIG5hbWU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tYWR2YW5jZWQtc2VhcmNoJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hZHZhbmNlZC1zZWFyY2guY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWR2YW5jZWRTZWFyY2hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICB2aXNpYmxlID0gdHJ1ZTtcclxuICBzZWxlY3RhYmxlID0gdHJ1ZTtcclxuICByZW1vdmFibGUgPSB0cnVlO1xyXG4gIGFkZE9uQmx1ciA9IHRydWU7XHJcbiAgZmlsdGVyUm93TGlzdCA9IFtdO1xyXG4gIHNlYXJjaFRvYmVTYXZlZE5hbWUgPSAnJztcclxuICBhZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGE6IEFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YTtcclxuICBrZXl3b3JkczogS2V5d29yZFtdID0gW107XHJcbiAgYWxsU2VhcmNoRmllbGRzOiBNUE1GaWVsZFtdID0gW107XHJcbiAgYXZhaWxhYmxlU2VhcmNoRmllbGRzOiBNUE1GaWVsZFtdID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50PixcclxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogQWR2YW5jZWRTZWFyY2hEYXRhLFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHB1YmxpYyBzZWFyY2hTYXZlU2VydmljZTogU2VhcmNoU2F2ZVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZm9ybWF0VG9Mb2NhbGVQaXBlOiBEYXRlUGlwZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2VhcmNoQ29uZmlnU2VydmljZTogU2VhcmNoQ29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBkYXRlUGlwZTogRGF0ZVBpcGVcclxuICApIHsgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6a2V5dXAnLCBbJyRldmVudCddKVxyXG4gIGtleUV2ZW50KGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQua2V5ID09PSAnRW50ZXInKSB7XHJcbiAgICAgIHRoaXMuY2xvc2UoZmFsc2UpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYWRkKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQge1xyXG4gICAgY29uc3QgaW5wdXQgPSBldmVudC5pbnB1dDtcclxuICAgIGxldCB2YWx1ZSA9IGV2ZW50LnZhbHVlO1xyXG5cclxuICAgIC8vIEFkZCB0aGUga2V5d29yZFxyXG4gICAgdmFsdWUgPSAodmFsdWUgfHwgJycpLnRyaW0oKTtcclxuICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICB0aGlzLmtleXdvcmRzLnB1c2goeyBuYW1lOiB2YWx1ZSB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXNldCB0aGUgaW5wdXQgdmFsdWVcclxuICAgIGlmIChpbnB1dCkge1xyXG4gICAgICBpbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlKGtleXdvcmQ6IEtleXdvcmQpOiB2b2lkIHtcclxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5rZXl3b3Jkcy5pbmRleE9mKGtleXdvcmQpO1xyXG5cclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMua2V5d29yZHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNsb3NlKGRhdGE/OiBhbnkpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEgIT09IHRydWUpIHtcclxuICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2goZGF0YSk7XHJcbiAgICB9XHJcbiAgICBpZiAoZGF0YSA9PT0gdHJ1ZSAmJiB0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLnJlY2VudFNlYXJjaCkge1xyXG4gICAgICB0aGlzLmRhdGEucmVjZW50U2VhcmNoLm1hcChyb3dEYXRhID0+IHtcclxuICAgICAgICBpZiAocm93RGF0YS5pc0VkaXQpIHtcclxuICAgICAgICAgIHJvd0RhdGEuaXNFZGl0ID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGRhdGEpO1xyXG4gIH1cclxuXHJcbiAgZmlsdGVyU2VsZWN0aW9uQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICB0aGlzLnNldEF2aWFsYmxlU2VhcmNoRmllbHMoKTtcclxuICB9XHJcblxyXG4gIGdldEZpZWxkRGV0YWlscyhmaWVsZElkKSB7XHJcbiAgICByZXR1cm4gdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5tZXRhZGF0YUZpZWxkcy5maWx0ZXIoZmllbGQgPT4ge1xyXG4gICAgICByZXR1cm4gZmllbGRbJ01QTV9GaWVsZHNfQ29uZmlnLWlkJ10uSWQgPT09IGZpZWxkSWQ7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIHRvIHJlbW92ZSBhbGwgZmllbGRzXHJcbiAgY2xlYXJGaWx0ZXIoKSB7XHJcbiAgICB0aGlzLmZpbHRlclJvd0xpc3QgPSBbXTtcclxuICAgIC8vIHRoaXMuYWRkRmlsdGVyKCk7XHJcbiAgICB0aGlzLnNldEF2aWFsYmxlU2VhcmNoRmllbHMoKTtcclxuICB9XHJcblxyXG4gIC8vIHRvIHJlbW92ZSBzZWxlY3RlZCBmaWVsZFxyXG4gIHJlbW92ZUZpbHRlcihldmVudCkge1xyXG4gICAgaWYgKGV2ZW50ICYmIGV2ZW50LnJvd0lkKSB7XHJcbiAgICAgIHRoaXMuZmlsdGVyUm93TGlzdCA9IHRoaXMuZmlsdGVyUm93TGlzdC5maWx0ZXIocm93RGF0YSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHJvd0RhdGEucm93SWQgIT09IGV2ZW50LnJvd0lkO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuc2V0QXZpYWxibGVTZWFyY2hGaWVscygpO1xyXG4gIH1cclxuXHJcbiAgLy8gdG8gYWRkIG5ldyBmaWVsZFxyXG4gIGFkZEZpbHRlcigpIHtcclxuICAgIGxldCBjYW5BZGROZXdGaWx0ZXIgPSB0cnVlO1xyXG4gICAgdGhpcy5maWx0ZXJSb3dMaXN0LmZvckVhY2gocm93RGF0YSA9PiB7XHJcbiAgICAgIGlmIChyb3dEYXRhICYmICFyb3dEYXRhLmlzRmlsdGVyU2VsZWN0ZWQpIHtcclxuICAgICAgICBjYW5BZGROZXdGaWx0ZXIgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBpZiAoY2FuQWRkTmV3RmlsdGVyKSB7XHJcbiAgICAgIGlmICh0aGlzLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgY29uc3QgY291bnQgPSB0aGlzLmZpbHRlclJvd0xpc3QubGVuZ3RoICsgMTtcclxuICAgICAgICB0aGlzLmZpbHRlclJvd0xpc3QucHVzaCh7XHJcbiAgICAgICAgICByb3dJZDogY291bnQsXHJcbiAgICAgICAgICBzZWFyY2hGaWVsZDogJycsXHJcbiAgICAgICAgICBzZWFyY2hPcGVyYXRvcklkOiAnJyxcclxuICAgICAgICAgIHNlYXJjaE9wZXJhdG9yTmFtZTogJycsXHJcbiAgICAgICAgICBzZWFyY2hWYWx1ZTogJycsXHJcbiAgICAgICAgICBzZWFyY2hTZWNvbmRWYWx1ZTogJycsXHJcbiAgICAgICAgICBpc3NlYXJjaFZhbHVlUmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICBpc0ZpbHRlclNlbGVjdGVkOiBmYWxzZSxcclxuICAgICAgICAgIGhhc1R3b1ZhbHVlczogJycsXHJcbiAgICAgICAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IHRoaXMuYXZhaWxhYmxlU2VhcmNoRmllbGRzXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ05vIG1vcmUgZmlsdGVycyB0byBhZGQnKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdLaW5kbHkgc2VsZWN0IHRoZSBwcmV2aW91cyBmaWx0ZXIgZmlyc3QnKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHZhbGlkYXRlRm9yU2VhcmNoKCkge1xyXG4gICAgbGV0IGFsbEZpbHRlckZpZWxkU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgbGV0IGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgbGV0IGVtcHR5VmFsdWUgPSBmYWxzZTtcclxuICAgIHRoaXMuZmlsdGVyUm93TGlzdC5mb3JFYWNoKGZpbHRlciA9PiB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaEZpZWxkID0gZmlsdGVyLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5maW5kKGRhdGEgPT4gKGZpbHRlci5zZWFyY2hGaWVsZCA9PT0gZGF0YS5NQVBQRVJfTkFNRSB8fCBmaWx0ZXIuc2VhcmNoRmllbGQgPT09IGRhdGEuTVBNX0ZJRUxEX0NPTkZJR19JRCkpO1xyXG4gICAgICBpZiAoIXNlYXJjaEZpZWxkLklTX0RST1BfRE9XTikge1xyXG4gICAgICAgIGlmIChmaWx0ZXIuc2VhcmNoRmllbGQpIHtcclxuICAgICAgICAgIGlmIChmaWx0ZXIuc2VhcmNoT3BlcmF0b3JJZCkge1xyXG4gICAgICAgICAgICBpZiAoZmlsdGVyLmlzc2VhcmNoVmFsdWVSZXF1aXJlZCkge1xyXG4gICAgICAgICAgICAgIGlmIChmaWx0ZXIuc2VhcmNoVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChmaWx0ZXIuaGFzVHdvVmFsdWVzICYmICFmaWx0ZXIuc2VhcmNoU2Vjb25kVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgKGZpbHRlci5zZWFyY2hWYWx1ZSkgPT09ICdzdHJpbmcnICYmIGZpbHRlci5zZWFyY2hWYWx1ZS50cmltKCkgPT09ICcnICYmIGZpbHRlci5pc3NlYXJjaFZhbHVlUmVxdWlyZWQpIHtcclxuICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgZW1wdHlWYWx1ZSA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgICBpZiAoIWlzVmFsaWQgfHwgIWZpbHRlci5pc0ZpbHRlclNlbGVjdGVkKSB7XHJcbiAgICAgICAgYWxsRmlsdGVyRmllbGRTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAoZW1wdHlWYWx1ZSkge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IGVudGVyIHRoZSB2YWx1ZSB0byBiZSBzZWFyY2hlZCcpO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9IGVsc2UgaWYgKCFhbGxGaWx0ZXJGaWVsZFNlbGVjdGVkKSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdLaW5kbHkgc2VsZWN0IGFsbCBmaWVsZCcpO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbnZlclRvTG9jYWxEYXRlKGRhdGUpIHtcclxuICAgIGNvbnN0IGlzb1N0cmluZ1RvTG9jYWxlID0gYWNyb251aS5nZXREYXRlV2l0aExvY2FsZVRpbWVab25lKGRhdGUpO1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybWF0VG9Mb2NhbGVQaXBlLnRyYW5zZm9ybShpc29TdHJpbmdUb0xvY2FsZSwgJ01NL2RkL3l5eXknKTtcclxuICB9XHJcblxyXG4gIHNhdmVTZWFyY2goc2VhcmNoQ29uZGl0aW9uczogQ29uZGl0aW9uTGlzdFtdLCB2aWV3TmFtZTogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSkge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IGFkZCBzZWFyY2ggbmFtZScpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lLmxlbmd0aCA+IDY0KSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdTZWFyY2ggbmFtZSBjYW5ub3QgYmUgbW9yZSB0aGFuIDY0IGNoYXJhY3RlcnMnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgbGV0IGhhc0R1cGxpY2F0ZVNlYXJjaE5hbWUgPSBmYWxzZTtcclxuICAgIHRoaXMuZGF0YS5hdmFpbGFibGVTYXZlZFNlYXJjaC5mb3JFYWNoKHNhdmVkU2VhcmNoID0+IHtcclxuICAgICAgaWYgKChzYXZlZFNlYXJjaC5OQU1FKS50b0xvd2VyQ2FzZSgpID09PSAodGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lKS50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgaGFzRHVwbGljYXRlU2VhcmNoTmFtZSA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKGhhc0R1cGxpY2F0ZVNlYXJjaE5hbWUgJiYgKCF0aGlzLmRhdGEuc2F2ZWRTZWFyY2ggfHwgKHR5cGVvZiAodGhpcy5kYXRhLnNhdmVkU2VhcmNoKSA9PT0gJ29iamVjdCcpICYmIHRoaXMuZGF0YS5zYXZlZFNlYXJjaC5OQU1FICE9PSB0aGlzLnNlYXJjaFRvYmVTYXZlZE5hbWUpKSB7IC8vIHx8IHR5cGVvZiAodGhpcy5kYXRhLnNhdmVkU2VhcmNoKSA9PT0gJ29iamVjdCcpXHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU2VhcmNoIE5hbWUgYWxyZWFkeSBleGlzdHMsIEtpbmRseSBjaGFuZ2UgdGhlIG5hbWUnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VhcmNoQ29uZGl0aW9uTGlzdCA9IHtcclxuICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogc2VhcmNoQ29uZGl0aW9uc1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgY29uc3Qgc2F2ZWRTZWFyY2hJZCA9ICh0aGlzLmRhdGEuc2F2ZWRTZWFyY2ggJiYgdGhpcy5kYXRhLnNhdmVkU2VhcmNoWydNUE1fU2F2ZWRfU2VhcmNoZXMtaWQnXSAmJlxyXG4gICAgICB0aGlzLmRhdGEuc2F2ZWRTZWFyY2hbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddLklkKSA/IHRoaXMuZGF0YS5zYXZlZFNlYXJjaFsnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10uSWQgOiAnJztcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICBpZiAoc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICB0aGlzLnNlYXJjaFNhdmVTZXJ2aWNlLnVwZGF0ZVNhdmVkU2VhcmNoKHNhdmVkU2VhcmNoSWQsIHRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSwgdGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lLFxyXG4gICAgICAgIHNlYXJjaENvbmRpdGlvbkxpc3QsIGZhbHNlLCB2aWV3TmFtZSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBjb25zdCBkYXRhOiBhbnkgPSBzZWFyY2hDb25kaXRpb25zO1xyXG4gICAgICAgICAgZGF0YVswXS5OQU1FID0gcmVzcG9uc2VbJ01QTV9TYXZlZF9TZWFyY2hlcyddLk5BTUU7XHJcbiAgICAgICAgICB0aGlzLmNsb3NlKGRhdGEpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0Vycm9yIHdoaWxlIHNhdmluZyBzZWFyY2gnKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VhcmNoU2F2ZVNlcnZpY2Uuc2F2ZVNlYXJjaCh0aGlzLnNlYXJjaFRvYmVTYXZlZE5hbWUsIHRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSxcclxuICAgICAgICBzZWFyY2hDb25kaXRpb25MaXN0LCBmYWxzZSwgdmlld05hbWUpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgY29uc3QgZGF0YTogYW55ID0gc2VhcmNoQ29uZGl0aW9ucztcclxuICAgICAgICAgIGRhdGFbMF0uTkFNRSA9IHJlc3BvbnNlWydNUE1fU2F2ZWRfU2VhcmNoZXMnXS5OQU1FO1xyXG4gICAgICAgICAgdGhpcy5jbG9zZShkYXRhKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdFcnJvciB3aGlsZSBzYXZpbmcgc2VhcmNoJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZWFyY2goaXNTYXZlZFNlYXJjaCkge1xyXG4gICAgaWYgKHRoaXMudmFsaWRhdGVGb3JTZWFyY2goKSkge1xyXG4gICAgICBjb25zdCBzZWFyY2hDb25kaXRpb25zOiBDb25kaXRpb25MaXN0W10gPSBbXTtcclxuICAgICAgdGhpcy5maWx0ZXJSb3dMaXN0LmZvckVhY2gocm93RGF0YSA9PiB7XHJcbiAgICAgICAgaWYgKHJvd0RhdGEuc2VhcmNoVmFsdWUgJiYgcm93RGF0YS5zZWFyY2hWYWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgICAgICAgIHJvd0RhdGEuc2VhcmNoVmFsdWUgPSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZShyb3dEYXRhLnNlYXJjaFZhbHVlKSwgJ3l5eXktTU0tZGQnKS5jb25jYXQoJ1QwMDowMDowMFonKTtcclxuICAgICAgICAgIGlmIChyb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlICYmIHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgaW5zdGFuY2VvZiBEYXRlKSB7XHJcbiAgICAgICAgICAgIHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgPSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZShyb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlKSwgJ3l5eXktTU0tZGQnKS5jb25jYXQoJ1QwMDowMDowMFonKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gcm93RGF0YS5hdmFpbGFibGVTZWFyY2hGaWVsZHMgPSBbXTtcclxuICAgICAgICBjb25zdCBjdXJyTVBNRmllbGQ6IE1QTUZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NUE1fRklFTERfQ09ORklHX0lELCByb3dEYXRhLnNlYXJjaEZpZWxkKTtcclxuICAgICAgICBjb25zdCBmaWVsZElkID0gY3Vyck1QTUZpZWxkID8gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZm9ybUluZGV4ZXJJZEZyb21GaWVsZChjdXJyTVBNRmllbGQpIDogcm93RGF0YS5zZWFyY2hGaWVsZDtcclxuICAgICAgICBpZiAocm93RGF0YS5pc0NvbWJvVHlwZSkge1xyXG4gICAgICAgICAgY29uc3QgdmFsdWVzID0gcm93RGF0YS5zZWFyY2hWYWx1ZTtcclxuICAgICAgICAgIGNvbnN0IGNvbmRpZHRpb246IENvbmRpdGlvbkxpc3QgPSB7XHJcbiAgICAgICAgICAgIGlzQWR2YW5jZWRTZWFyY2g6IHRydWUsXHJcbiAgICAgICAgICAgIHNlYXJjaElkZW50aWZpZXI6IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllci5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcsXHJcbiAgICAgICAgICAgIGZpZWxkX2lkOiBmaWVsZElkLFxyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiByb3dEYXRhLnNlYXJjaE9wZXJhdG9ySWQgPyByb3dEYXRhLnNlYXJjaE9wZXJhdG9ySWQgOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfbmFtZTogcm93RGF0YS5zZWFyY2hPcGVyYXRvck5hbWUgPyByb3dEYXRhLnNlYXJjaE9wZXJhdG9yTmFtZSA6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgICAgIHR5cGU6IGN1cnJNUE1GaWVsZCA/IGN1cnJNUE1GaWVsZC5EQVRBX1RZUEUgOiAnc3RyaW5nJyxcclxuICAgICAgICAgICAgbWFwcGVyX2ZpZWxkX2lkOiByb3dEYXRhLnNlYXJjaEZpZWxkLFxyXG4gICAgICAgICAgICBtYXBwZXJfbmFtZTogY3Vyck1QTUZpZWxkLk1BUFBFUl9OQU1FLFxyXG4gICAgICAgICAgICB2aWV3OiB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaElkZW50aWZpZXIuVklFV1xyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIGlmICh2YWx1ZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbmRpdGlvbkNvcHkgPSBPYmplY3QuYXNzaWduKHt9LCBjb25kaWR0aW9uKTtcclxuICAgICAgICAgICAgY29uZGl0aW9uQ29weS5maWx0ZXJSb3dEYXRhID0gcm93RGF0YTtcclxuICAgICAgICAgICAgY29uZGl0aW9uQ29weS5sZWZ0X3BhcmVuID0gJygnO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LnJpZ2h0X3BhcmVuID0gJyknO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LmRpc3BsYXlfdmFsdWUgPSB2YWx1ZXNbMF0uZGlzcGxheV92YWx1ZTtcclxuICAgICAgICAgICAgY29uZGl0aW9uQ29weS52YWx1ZSA9IHZhbHVlc1swXS5maWVsZF92YWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgY29uZGl0aW9uQ29weS5yZWxhdGlvbmFsX29wZXJhdG9yID0gJ2FuZCc7XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnMucHVzaChjb25kaXRpb25Db3B5KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxhc3RWYWx1ZUluZGV4ID0gdmFsdWVzLmxlbmd0aCAtIDE7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgY29uZGl0aW9uQ29weSA9IE9iamVjdC5hc3NpZ24oe30sIGNvbmRpZHRpb24pO1xyXG4gICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkuZGlzcGxheV92YWx1ZSA9IHZhbHVlc1tpXS5kaXNwbGF5X3ZhbHVlO1xyXG4gICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkudmFsdWUgPSB2YWx1ZXNbaV0uZmllbGRfdmFsdWUudmFsdWU7XHJcbiAgICAgICAgICAgICAgaWYgKGkgPT09IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkubGVmdF9wYXJlbiA9ICcoJztcclxuICAgICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkuZmlsdGVyUm93RGF0YSA9IHJvd0RhdGE7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChpID09PSBsYXN0VmFsdWVJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5yaWdodF9wYXJlbiA9ICcpJztcclxuICAgICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkucmVsYXRpb25hbF9vcGVyYXRvciA9ICdvcic7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkucmVsYXRpb25hbF9vcGVyYXRvciA9ICdvcic7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnMucHVzaChjb25kaXRpb25Db3B5KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsZXQgdmFsdWUgPSByb3dEYXRhLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgaWYgKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUpIHtcclxuICAgICAgICAgICAgLy8gaWYgKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgaW5zdGFuY2VvZiBEYXRlICYmIHJvd0RhdGEuaXNzZWFyY2hWYWx1ZVJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgIC8vICAgcm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSA9IHRoaXMuY29udmVyVG9Mb2NhbERhdGUocm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSk7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLyogaWYgKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgaW5zdGFuY2VvZiBEYXRlKSB7XHJcbiAgICAgICAgICAgICAgLy8gdmFsdWUgPSB2YWx1ZS50b0lTT1N0cmluZygpICsgJyBhbmQgJyArIHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUudG9JU09TdHJpbmcoKTtcclxuICAgICAgICAgICAgICB2YWx1ZSA9IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKHJvd0RhdGEuc2VhcmNoVmFsdWUpLCAneXl5eS1NTS1kZCcpLmNvbmNhdCgnVDAwOjAwOjAwWicpICsgJyBhbmQgJyArXHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZShyb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlKSwgJ3l5eXktTU0tZGQnKS5jb25jYXQoJ1QwMDowMDowMFonKSAgXHJcbiAgICAgICAgICAgIH0gZWxzZSB7ICovXHJcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUgKyAnIGFuZCAnICsgcm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgY29uc3QgY29uZGl0aW9uOiBDb25kaXRpb25MaXN0ID0ge1xyXG4gICAgICAgICAgICBpc0FkdmFuY2VkU2VhcmNoOiB0cnVlLFxyXG4gICAgICAgICAgICBzZWFyY2hJZGVudGlmaWVyOiB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaElkZW50aWZpZXIuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLFxyXG4gICAgICAgICAgICBmaWx0ZXJSb3dEYXRhOiByb3dEYXRhLFxyXG4gICAgICAgICAgICBmaWVsZF9pZDogZmllbGRJZCxcclxuICAgICAgICAgICAgdHlwZTogY3Vyck1QTUZpZWxkID8gY3Vyck1QTUZpZWxkLkRBVEFfVFlQRSA6ICdzdHJpbmcnLFxyXG4gICAgICAgICAgICB2YWx1ZTogdmFsdWUsXHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCA/IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCA6IE1QTVNlYXJjaE9wZXJhdG9ycy5JUyxcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiByb3dEYXRhLnNlYXJjaE9wZXJhdG9yTmFtZSA/IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JOYW1lIDogTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUyxcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogJ2FuZCcsXHJcbiAgICAgICAgICAgIG1hcHBlcl9maWVsZF9pZDogcm93RGF0YS5zZWFyY2hGaWVsZCxcclxuICAgICAgICAgICAgbWFwcGVyX25hbWU6IGN1cnJNUE1GaWVsZC5NQVBQRVJfTkFNRSxcclxuICAgICAgICAgICAgdmlldzogdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hJZGVudGlmaWVyLlZJRVdcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICBzZWFyY2hDb25kaXRpb25zLnB1c2goY29uZGl0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBpZiAoaXNTYXZlZFNlYXJjaCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLnJlY2VudFNlYXJjaCkge1xyXG4gICAgICAgICAgdGhpcy5kYXRhLnJlY2VudFNlYXJjaC5tYXAocm93RGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyb3dEYXRhLmlzRWRpdCkge1xyXG4gICAgICAgICAgICAgIHJvd0RhdGEuaXNFZGl0ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNhdmVTZWFyY2goc2VhcmNoQ29uZGl0aW9ucywgdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hJZGVudGlmaWVyLlZJRVcpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY2xvc2Uoc2VhcmNoQ29uZGl0aW9ucyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGlzRmllbGRBbHJlYWR5U2VsZWN0ZWQoZmllbGRJZDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIWZpZWxkSWQpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5maWx0ZXJSb3dMaXN0LmZpbmQocm93ID0+IHtcclxuICAgICAgcmV0dXJuIHJvdy5zZWFyY2hGaWVsZCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2V0QXZpYWxibGVTZWFyY2hGaWVscygpIHtcclxuICAgIHRoaXMuYXZhaWxhYmxlU2VhcmNoRmllbGRzID0gW107XHJcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkodGhpcy5hbGxTZWFyY2hGaWVsZHMpKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgZmllbGQgb2YgdGhpcy5hbGxTZWFyY2hGaWVsZHMpIHtcclxuICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLk1QTV9GSUVMRF9DT05GSUdfSUQgJiZcclxuICAgICAgICAhdGhpcy5pc0ZpZWxkQWxyZWFkeVNlbGVjdGVkKGZpZWxkLk1QTV9GSUVMRF9DT05GSUdfSUQpKSB7XHJcbiAgICAgICAgdGhpcy5hdmFpbGFibGVTZWFyY2hGaWVsZHMucHVzaChmaWVsZCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcyA9IHRoaXMuYXZhaWxhYmxlU2VhcmNoRmllbGRzO1xyXG4gIH1cclxuXHJcbiAgbWFwU2VhY2hDb25maWcoKSB7XHJcbiAgICBpZiAodGhpcy5kYXRhLnNhdmVkU2VhcmNoKSB7XHJcbiAgICAgIHRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSA9IHRoaXMuZGF0YS5zYXZlZFNlYXJjaC5OQU1FO1xyXG4gICAgICBsZXQgY29uZGl0aW9uczogQ29uZGl0aW9uTGlzdFtdID0gW107XHJcbiAgICAgIGlmICh0aGlzLmRhdGEuc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEgJiZcclxuICAgICAgICB0aGlzLmRhdGEuc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0ICYmXHJcbiAgICAgICAgdGhpcy5kYXRhLnNhdmVkU2VhcmNoLlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uKSB7XHJcbiAgICAgICAgY29uZGl0aW9ucyA9IHRoaXMuZGF0YS5zYXZlZFNlYXJjaC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbjtcclxuICAgICAgfVxyXG4gICAgICBpZiAoY29uZGl0aW9ucyAmJiBjb25kaXRpb25zWzBdLnNlYXJjaElkZW50aWZpZXIpIHtcclxuICAgICAgICBjb25zdCBhZHZDb25maWcgPSB0aGlzLnNlYXJjaENvbmZpZ1NlcnZpY2UuZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeUlkKGNvbmRpdGlvbnNbMF0uc2VhcmNoSWRlbnRpZmllcik7XHJcbiAgICAgICAgdGhpcy5hbGxTZWFyY2hGaWVsZHMgPSBhZHZDb25maWcuUl9QTV9TZWFyY2hfRmllbGRzO1xyXG4gICAgICAgIGNvbmRpdGlvbnMuZm9yRWFjaChjb25kaXRpb24gPT4ge1xyXG4gICAgICAgICAgY29uc3QgY291bnQgPSB0aGlzLmZpbHRlclJvd0xpc3QubGVuZ3RoICsgMTtcclxuICAgICAgICAgIHRoaXMuc2V0QXZpYWxibGVTZWFyY2hGaWVscygpO1xyXG4gICAgICAgICAgY29uc3QgdmFsdWUgPSBjb25kaXRpb24udmFsdWUgIT0gbnVsbCA/IGNvbmRpdGlvbi52YWx1ZSA6ICcnO1xyXG4gICAgICAgICAgLyogdGhpcy5maWx0ZXJSb3dMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICByb3dJZDogY291bnQsXHJcbiAgICAgICAgICAgIHNlYXJjaEZpZWxkOiBjb25kaXRpb24ubWFwcGVyX2ZpZWxkX2lkLFxyXG4gICAgICAgICAgICBzZWFyY2hPcGVyYXRvcklkOiBjb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvcl9pZCxcclxuICAgICAgICAgICAgc2VhcmNoT3BlcmF0b3JOYW1lOiBjb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lLFxyXG4gICAgICAgICAgICBzZWFyY2hWYWx1ZTogY29uZGl0aW9uLnZhbHVlLFxyXG4gICAgICAgICAgICBzZWFyY2hTZWNvbmRWYWx1ZTogY29uZGl0aW9uLnZhbHVlLnNwbGl0KCdhbmQgJykubGVuZ3RoID4gMCA/IGNvbmRpdGlvbi52YWx1ZS5zcGxpdCgnYW5kICcpWzFdIDogJycsXHJcbiAgICAgICAgICAgIGlzc2VhcmNoVmFsdWVSZXF1aXJlZDogY29uZGl0aW9uLnZhbHVlID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICBpc0ZpbHRlclNlbGVjdGVkOiB0cnVlLFxyXG4gICAgICAgICAgICBoYXNUd29WYWx1ZXM6ICcnLFxyXG4gICAgICAgICAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IHRoaXMuYWxsU2VhcmNoRmllbGRzLmZpbHRlcihzZWFyY2hGaWVsZEl0ZW0gPT4ge1xyXG4gICAgICAgICAgICAgIHJldHVybiBzZWFyY2hGaWVsZEl0ZW0uTVBNX0ZJRUxEX0NPTkZJR19JRCA9PT0gY29uZGl0aW9uLm1hcHBlcl9maWVsZF9pZDtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgICAgdGhpcy5maWx0ZXJSb3dMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICByb3dJZDogY291bnQsXHJcbiAgICAgICAgICAgIHNlYXJjaEZpZWxkOiBjb25kaXRpb24ubWFwcGVyX2ZpZWxkX2lkLFxyXG4gICAgICAgICAgICBzZWFyY2hPcGVyYXRvcklkOiBjb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvcl9pZCxcclxuICAgICAgICAgICAgc2VhcmNoT3BlcmF0b3JOYW1lOiBjb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lLFxyXG4gICAgICAgICAgICBzZWFyY2hWYWx1ZTogdmFsdWUsXHJcbiAgICAgICAgICAgIHNlYXJjaFNlY29uZFZhbHVlOiB2YWx1ZS5zcGxpdCgnYW5kICcpLmxlbmd0aCA+IDAgPyB2YWx1ZS5zcGxpdCgnYW5kICcpWzFdICE9PSB1bmRlZmluZWQgPyB2YWx1ZS5zcGxpdCgnYW5kICcpWzFdIDogJycgOiAnJyxcclxuICAgICAgICAgICAgaXNzZWFyY2hWYWx1ZVJlcXVpcmVkOiB2YWx1ZSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgaXNGaWx0ZXJTZWxlY3RlZDogdHJ1ZSxcclxuICAgICAgICAgICAgaGFzVHdvVmFsdWVzOiAnJyxcclxuICAgICAgICAgICAgYXZhaWxhYmxlU2VhcmNoRmllbGRzOiB0aGlzLmFsbFNlYXJjaEZpZWxkcy5maWx0ZXIoc2VhcmNoRmllbGRJdGVtID0+IHtcclxuICAgICAgICAgICAgICByZXR1cm4gc2VhcmNoRmllbGRJdGVtLk1QTV9GSUVMRF9DT05GSUdfSUQgPT09IGNvbmRpdGlvbi5tYXBwZXJfZmllbGRfaWQ7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuc2V0QXZpYWxibGVTZWFyY2hGaWVscygpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcyA9IHRoaXMuYWxsU2VhcmNoRmllbGRzO1xyXG4gICAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaE9wZXJhdG9yTGlzdCA9IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoT3BlcmF0b3JMaXN0O1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSAmJiB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaElkZW50aWZpZXIpIHtcclxuICAgICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbi5maWx0ZXIoc2VyYWNoQ29uZmlnID0+IHtcclxuICAgICAgICAgIHJldHVybiBzZXJhY2hDb25maWdbJ01QTV9BZHZhbmNlZF9TZWFyY2hfQ29uZmlnLWlkJ10uSWQgPT09IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllci5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUc7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllciA9IHNlYXJjaENvbmZpZ1swXVsnTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZDtcclxuXHJcbiAgICAgICAgdGhpcy5hbGxTZWFyY2hGaWVsZHMgPSBzZWFyY2hDb25maWdbMF0uUl9QTV9TZWFyY2hfRmllbGRzO1xyXG4gICAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcyA9IHNlYXJjaENvbmZpZ1swXS5SX1BNX1NlYXJjaF9GaWVsZHM7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoT3BlcmF0b3JMaXN0ID0gdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hPcGVyYXRvckxpc3Q7XHJcbiAgICAgICAgdGhpcy5zZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk7XHJcbiAgICAgICAgLy8gdGhpcy5hZGRGaWx0ZXIoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSA9IHtcclxuICAgICAgYXZhaWxhYmxlU2VhcmNoRmllbGRzOiBbXSxcclxuICAgICAgc2VhcmNoT3BlcmF0b3JMaXN0OiBbXVxyXG4gICAgfTtcclxuICAgIHRoaXMubWFwU2VhY2hDb25maWcoKTtcclxuICAgIGlmICh0aGlzLmRhdGEucmVjZW50U2VhcmNoKSB7XHJcbiAgICAgIHRoaXMuZmlsdGVyUm93TGlzdCA9IFtdO1xyXG4gICAgICB0aGlzLmRhdGEucmVjZW50U2VhcmNoLm1hcChyb3dEYXRhID0+IHtcclxuICAgICAgICBpZiAocm93RGF0YS5maWx0ZXJSb3dEYXRhKSB7XHJcbiAgICAgICAgICByb3dEYXRhLmZpbHRlclJvd0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzID0gdGhpcy5hbGxTZWFyY2hGaWVsZHMuZmlsdGVyKHNlYXJjaEZpZWxkSXRlbSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBzZWFyY2hGaWVsZEl0ZW0uTVBNX0ZJRUxEX0NPTkZJR19JRCA9PT0gcm93RGF0YS5maWx0ZXJSb3dEYXRhLnNlYXJjaEZpZWxkO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0aGlzLmZpbHRlclJvd0xpc3QucHVzaChyb3dEYXRhLmZpbHRlclJvd0RhdGEpO1xyXG4gICAgICAgICAgdGhpcy5zZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19