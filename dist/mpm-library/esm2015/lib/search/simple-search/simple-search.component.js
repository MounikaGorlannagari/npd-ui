import { __decorate, __param } from "tslib";
import { Component, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { AdvancedSearchComponent } from '../advanced-search/advanced-search.component';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { SearchDataService } from '../services/search-data.service';
import { SearchSaveService } from '../../project/shared/services/search.save.service';
import { Observable } from 'rxjs';
let SimpleSearchComponent = class SimpleSearchComponent {
    constructor(dialogRef, data, sharingService, dialog, loaderService, notificationService, searchConfigService, searchDataService, searchSaveService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.sharingService = sharingService;
        this.dialog = dialog;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.searchConfigService = searchConfigService;
        this.searchDataService = searchDataService;
        this.searchSaveService = searchSaveService;
        this.savedSearches = [];
        this.recentSearches = [];
    }
    search(searchKeyword) {
        if (searchKeyword == null || searchKeyword === '' || (typeof (searchKeyword) === 'string' && searchKeyword.trim() === '')) {
            this.notificationService.info('Kindly enter the value to be searched');
            return;
        }
        if (searchKeyword && searchKeyword[0] && searchKeyword[0].isAdvancedSearch) {
            this.closeSimpleSearchPopup(searchKeyword);
        }
        else {
            const searchConditions = [{
                    metadata_field_id: this.searchDataService.KEYWORD_MAPPER,
                    keyword: searchKeyword,
                }];
            this.sharingService.setRecentSearch(searchKeyword);
            this.closeSimpleSearchPopup(searchConditions);
        }
    }
    closeSimpleSearchPopup(data) {
        this.dialogRef.close(data);
    }
    getSavedSearchDetail(savedSearch) {
        if (savedSearch.SEARCH_DATA) {
            const searchConditions = savedSearch.SEARCH_DATA.search_condition_list.search_condition;
            searchConditions[0].isAdvancedSearch = true;
            searchConditions[0].savedSearch = true;
            searchConditions[0].NAME = savedSearch.NAME;
            searchConditions[0].savedSearchId = savedSearch['MPM_Saved_Searches-id'].Id;
            this.sharingService.setRecentSearch(searchConditions);
            this.closeSimpleSearchPopup(savedSearch);
        }
    }
    deleteSavedSearch(savedSearch) {
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: 'Are you sure you want to delete saved search?',
                submitButton: 'Yes',
                cancelButton: 'No'
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result && result.isTrue) {
                this.loaderService.show();
                this.searchSaveService.deleteSavedSearch(savedSearch['MPM_Saved_Searches-id'].Id).subscribe(response => {
                    this.getAllSavedSearches().subscribe(searchResponse => {
                    });
                    this.loaderService.hide();
                }, error => {
                    this.loaderService.hide();
                    this.notificationService.error('Error while deleting saved search');
                });
            }
        });
    }
    editSavedSearch(savedSearch) {
        if (this.data && this.data.searchData && this.data.searchData.searchConfig
            && this.data.searchData.searchConfig.advancedSearchConfiguration) {
            this.openAdvancedSearch(savedSearch, false);
        }
    }
    editRecentAdvancedSearch(resentSearch) {
        resentSearch.forEach(data => {
            data.isEdit = true;
        });
        this.openAdvancedSearch(false, resentSearch);
    }
    getAllSavedSearches() {
        return new Observable(observer => {
            this.searchConfigService.getSavedSearchesForViewAndCurrentUser(this.data.searchData.searchConfig.searchIdentifier.VIEW).subscribe(response => {
                if (response && response.length > 0) {
                    this.savedSearches = response;
                }
                else {
                    this.savedSearches = [];
                }
                observer.next(true);
                observer.complete();
            }, error => {
                console.error('Something went wrong while getting the saved searches.');
            });
        });
    }
    getAllRecentSearches() {
        // const allRecentSearches = this.sharingService.getRecentSearches();
        const allRecentSearches = this.sharingService.getRecentSearchesByView(this.data.searchData.searchConfig.searchIdentifier.VIEW);
        const allowedRecentSearches = [];
        if (Array.isArray(allRecentSearches)) {
            allRecentSearches.forEach(item => {
                if (item) {
                    const advItem = item[0];
                    if (advItem && advItem.isAdvancedSearch) {
                        if (!advItem.isEdit) {
                            allowedRecentSearches.push(item);
                        }
                    }
                    else {
                        allowedRecentSearches.push(item);
                    }
                }
            });
        }
        this.recentSearches = [...new Set(allowedRecentSearches)];
    }
    openAdvancedSearchModal(advData) {
        if (this.data.searchData && this.data.searchData.advancedSearchConfig && (this.data.searchData.advancedSearchConfig.NAME !== '' || this.data.searchData.advancedSearchConfig[0].NAME !== '')) {
            this.savedSearches.forEach(data => {
                if (data.NAME === (this.data.searchData.advancedSearchConfig.NAME ? this.data.searchData.advancedSearchConfig.NAME : this.data.searchData.advancedSearchConfig[0].NAME)) {
                    advData.savedSearch = data;
                }
            });
        }
        else if (advData.recentSearch && advData.recentSearch[0] && advData.recentSearch[0].NAME !== '') {
            this.savedSearches.forEach(data => {
                if (data.NAME === advData.recentSearch[0].NAME) {
                    advData.savedSearch = data;
                }
            });
        }
        this.advDialogRef = this.dialog.open(AdvancedSearchComponent, {
            position: { top: '0' },
            width: '1100px',
            // height: '65%',
            disableClose: true,
            data: advData
        });
        this.advDialogRef.afterClosed().subscribe(result => {
            if (result === true) {
                //  this.dialogRef.close(result);
            }
            else {
                this.dialogRef.close(result);
            }
        });
    }
    openAdvancedSearch(savedSearchId, recentSearch) {
        if (this.data && this.data.searchData && this.data.searchData.searchConfig
            && this.data.searchData.searchConfig.advancedSearchConfiguration) {
            const advData = {
                advancedSearchConfigData: this.data.searchData.searchConfig,
                availableSavedSearch: this.savedSearches,
                savedSearch: savedSearchId,
                recentSearch: recentSearch
            };
            if (savedSearchId) {
                if (typeof savedSearchId === 'string') {
                    this.searchConfigService.getSavedSearcheById(savedSearchId).subscribe(savedSearchDetails => {
                        if (savedSearchDetails) {
                            advData.savedSearch = savedSearchDetails;
                            this.openAdvancedSearchModal(advData);
                        }
                    });
                }
                else {
                    this.openAdvancedSearchModal(advData);
                }
            }
            else {
                this.openAdvancedSearchModal(advData);
            }
        }
    }
    onKeyUp() {
        if (this.advDialogRef) {
            this.advDialogRef.close();
        }
    }
    ngOnInit() {
        this.searchKeyword = this.data.searchData.searchValue;
        this.getAllSavedSearches().subscribe(response => {
            if (this.data.searchData && this.data.searchData.advancedSearchConfig) {
                if (this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'] &&
                    this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'].Id) {
                    this.editSavedSearch(this.data.searchData.advancedSearchConfig);
                }
                else {
                    this.openAdvancedSearch(false, this.data.searchData.advancedSearchConfig);
                }
            }
        });
        this.getAllRecentSearches();
    }
};
SimpleSearchComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: SharingService },
    { type: MatDialog },
    { type: LoaderService },
    { type: NotificationService },
    { type: SearchConfigService },
    { type: SearchDataService },
    { type: SearchSaveService }
];
__decorate([
    HostListener('window:keyup.esc')
], SimpleSearchComponent.prototype, "onKeyUp", null);
SimpleSearchComponent = __decorate([
    Component({
        selector: 'mpm-simple-search',
        template: "<mat-dialog-content>\r\n    <div class=\"search-input\">\r\n        <form class=\"example-form\">\r\n            <mat-form-field class=\"example-full-width\">\r\n                <mat-icon matPrefix class=\"cursor-pointer\" (click)=\"search(searchKeyword)\">search</mat-icon>\r\n                <input type=\"text\" matInput [(ngModel)]=\"searchKeyword\" name=\"searchKeyword\"\r\n                    (keyup.enter)=\"search(searchKeyword)\">\r\n            </mat-form-field>\r\n        </form>\r\n    </div>\r\n\r\n    <div class=\"saved-searches\">\r\n        <span class=\"heading\">Saved Searches</span>\r\n        <mat-chip-list aria-label=\"Saved Search Selection\" class=\"saved-search-items\" *ngIf=\"savedSearches.length > 0\">\r\n            <mat-chip class=\"cursor-pointer\" *ngFor=\"let savedSearch of savedSearches\"\r\n                (click)=\"getSavedSearchDetail(savedSearch)\">\r\n                <span class=\"searchName\">{{savedSearch.NAME}}</span>\r\n                <mat-icon matTooltip=\"Delete this Search\" (click)=\"deleteSavedSearch(savedSearch)\" matChipRemove\r\n                    *ngIf=\"!savedSearch.public\">delete\r\n                </mat-icon>\r\n                <mat-icon matTooltip=\"Edit this Search\" (click)=\"editSavedSearch(savedSearch)\" matChipRemove\r\n                    *ngIf=\"!savedSearch.public\">edit\r\n                </mat-icon>\r\n            </mat-chip>\r\n        </mat-chip-list>\r\n        <p class=\"no-items\" *ngIf=\"savedSearches.length === 0\">No saved searches.</p>\r\n    </div>\r\n\r\n    <div class=\"recent-searches\">\r\n        <span class=\"heading\">Recent Searches</span>\r\n        <mat-list role=\"list\" class=\"recent-search-items\" *ngIf=\"recentSearches.length > 0\">\r\n            <mat-list-item role=\"listitem\" *ngFor=\"let recentSearch of recentSearches\">\r\n                <mat-icon mat-list-icon>replay</mat-icon>\r\n                <p mat-line *ngIf=\"!recentSearch[0].isAdvancedSearch\"> <span class=\"cursor-pointer\"\r\n                        (click)=\"search(recentSearch)\" matTooltip=\"{{recentSearch}}\">\r\n                        Keyword: {{recentSearch}}\r\n                    </span>\r\n                </p>\r\n                <p mat-line *ngIf=\"recentSearch[0] && recentSearch[0].isAdvancedSearch\">\r\n                    <span class=\"cursor-pointer\" (click)=\"search(recentSearch)\"\r\n                        matTooltip=\"{{recentSearch[0].NAME ? recentSearch[0].NAME : 'Advanced Search Criteria'}}\">\r\n                        {{recentSearch[0].NAME ? recentSearch[0].NAME : 'Advanced Search Criteria'}} </span>\r\n                    <!-- <mat-icon (click)=\"editRecentAdvancedSearch(recentSearch)\">edit\r\n                    </mat-icon> -->\r\n                </p>\r\n                <mat-icon class=\"cursor-pointer\" *ngIf=\"recentSearch[0] && recentSearch[0].isAdvancedSearch\"\r\n                    (click)=\"editRecentAdvancedSearch(recentSearch)\">edit\r\n                </mat-icon>\r\n            </mat-list-item>\r\n        </mat-list>\r\n        <p class=\"no-items\" *ngIf=\"recentSearches.length === 0\">No recent searches.</p>\r\n    </div>\r\n\r\n    <div class=\"advanced-search-link\">\r\n        <button mat-stroked-button (click)=\"openAdvancedSearch(false, false)\" mat-button>Advanced Search</button>\r\n    </div>\r\n</mat-dialog-content>",
        styles: ["::ng-deep .mat-dialog-container{padding:12px 24px 0}div.search-input .example-form,div.search-input .example-full-width{width:100%}div.saved-searches{margin:16px 0}div.saved-searches .saved-search-items ::ng-deep .mat-chip-list-wrapper{margin:4px 0;max-height:80px;overflow:auto}div.recent-searches{margin:16px 0}div.recent-searches .recent-search-items{max-height:204px;overflow:auto}div.recent-searches .recent-search-items mat-list-item mat-icon{font-size:18px;height:18px}div.recent-searches .heading,div.saved-searches .heading{font-size:large;margin:4px 0}div.recent-searches p.no-items,div.saved-searches p.no-items{margin:4px 0;text-align:center}div.advanced-search-link button{width:100%;margin-bottom:10px}.cursor-pointer{cursor:pointer;max-width:98%}.searchName{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], SimpleSearchComponent);
export { SimpleSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2ltcGxlLXNlYXJjaC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zZWFyY2gvc2ltcGxlLXNlYXJjaC9zaW1wbGUtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3BGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUN2RixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx5RUFBeUUsQ0FBQztBQUNySCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFJbEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFFcEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDdEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQVFsQyxJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQU85QixZQUNXLFNBQThDLEVBQ3JCLElBQXNCLEVBQy9DLGNBQThCLEVBQzlCLE1BQWlCLEVBQ2pCLGFBQTRCLEVBQzVCLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGlCQUFvQztRQVJwQyxjQUFTLEdBQVQsU0FBUyxDQUFxQztRQUNyQixTQUFJLEdBQUosSUFBSSxDQUFrQjtRQUMvQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBZC9DLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLG1CQUFjLEdBQUcsRUFBRSxDQUFDO0lBY2hCLENBQUM7SUFFTCxNQUFNLENBQUMsYUFBYTtRQUNoQixJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxRQUFRLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztZQUN2RSxPQUFPO1NBQ1Y7UUFDRCxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO1lBQ3hFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUM5QzthQUFNO1lBQ0gsTUFBTSxnQkFBZ0IsR0FBRyxDQUFDO29CQUN0QixpQkFBaUIsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYztvQkFDeEQsT0FBTyxFQUFFLGFBQWE7aUJBRXpCLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELHNCQUFzQixDQUFDLElBQUk7UUFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELG9CQUFvQixDQUFDLFdBQTJCO1FBQzVDLElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUN6QixNQUFNLGdCQUFnQixHQUFvQixXQUFXLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDO1lBQ3pHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM1QyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ3ZDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQzVDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDNUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsaUJBQWlCLENBQUMsV0FBMkI7UUFDekMsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDM0QsS0FBSyxFQUFFLEtBQUs7WUFDWixZQUFZLEVBQUUsSUFBSTtZQUNsQixJQUFJLEVBQUU7Z0JBQ0YsT0FBTyxFQUFFLCtDQUErQztnQkFDeEQsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLFlBQVksRUFBRSxJQUFJO2FBQ3JCO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN2QyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO2dCQUN6QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUNuRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7b0JBQ3RELENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtvQkFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7Z0JBQ3hFLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxlQUFlLENBQUMsV0FBVztRQUN2QixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWTtlQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsMkJBQTJCLEVBQUU7WUFDbEUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMvQztJQUNMLENBQUM7SUFFRCx3QkFBd0IsQ0FBQyxZQUFZO1FBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxtQkFBbUI7UUFDZixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxxQ0FBcUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUN6SSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDakMsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUM7aUJBQ2pDO3FCQUFNO29CQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUMzQjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUNQLE9BQU8sQ0FBQyxLQUFLLENBQUMsd0RBQXdELENBQUMsQ0FBQztZQUM1RSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9CQUFvQjtRQUNoQixxRUFBcUU7UUFDckUsTUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvSCxNQUFNLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNqQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNsQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzdCLElBQUksSUFBSSxFQUFFO29CQUNOLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLGdCQUFnQixFQUFFO3dCQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTs0QkFDakIscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNwQztxQkFDSjt5QkFBTTt3QkFDSCxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3BDO2lCQUVKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxHQUFHLElBQUksR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsdUJBQXVCLENBQUMsT0FBTztRQUMzQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsSUFBSSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDLEVBQUU7WUFDMUwsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDckssT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQzlCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNLElBQUksT0FBTyxDQUFDLFlBQVksSUFBSSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLEVBQUUsRUFBRTtZQUMvRixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDOUIsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFO29CQUM1QyxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztpQkFDOUI7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUMxRCxRQUFRLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFO1lBQ3RCLEtBQUssRUFBRSxRQUFRO1lBQ2YsaUJBQWlCO1lBQ2pCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLElBQUksRUFBRSxPQUFPO1NBQ2hCLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQy9DLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtnQkFDakIsaUNBQWlDO2FBQ3BDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ2hDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsYUFBYSxFQUFFLFlBQVk7UUFDMUMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVk7ZUFDbkUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLDJCQUEyQixFQUFFO1lBQ2xFLE1BQU0sT0FBTyxHQUF1QjtnQkFDaEMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWTtnQkFDM0Qsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLGFBQWE7Z0JBQ3hDLFdBQVcsRUFBRSxhQUFhO2dCQUMxQixZQUFZLEVBQUUsWUFBWTthQUM3QixDQUFDO1lBQ0YsSUFBSSxhQUFhLEVBQUU7Z0JBQ2YsSUFBSSxPQUFPLGFBQWEsS0FBSyxRQUFRLEVBQUU7b0JBQ25DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsRUFBRTt3QkFDdkYsSUFBSSxrQkFBa0IsRUFBRTs0QkFDcEIsT0FBTyxDQUFDLFdBQVcsR0FBRyxrQkFBa0IsQ0FBQzs0QkFDekMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUN6QztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3pDO1NBQ0o7SUFDTCxDQUFDO0lBRWlDLE9BQU87UUFDckMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDN0I7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1FBQ3RELElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM1QyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixFQUFFO2dCQUNuRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDO29CQUNsRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDdkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2lCQUVuRTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUM7aUJBQzdFO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7Q0FDSixDQUFBOztZQXZNeUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7WUFDQSxjQUFjO1lBQ3RCLFNBQVM7WUFDRixhQUFhO1lBQ1AsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNyQixpQkFBaUI7WUFDakIsaUJBQWlCOztBQTBLYjtJQUFqQyxZQUFZLENBQUMsa0JBQWtCLENBQUM7b0RBSWhDO0FBOUxRLHFCQUFxQjtJQU5qQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLCt6R0FBNkM7O0tBRWhELENBQUM7SUFXTyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtHQVRuQixxQkFBcUIsQ0ErTWpDO1NBL01ZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSwgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hDb21wb25lbnQgfSBmcm9tICcuLi9hZHZhbmNlZC1zZWFyY2gvYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvc2VhcmNoLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNU2F2ZWRTZWFyY2ggfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvb2JqZWN0cy9NUE1TYXZlZFNlYXJjaCc7XHJcbmltcG9ydCB7IFNpbXBsZVNlYXJjaERhdGEgfSBmcm9tICcuLi9vYmplY3RzL1NpbXBsZVNlYXJjaERhdGEnO1xyXG5pbXBvcnQgeyBBZHZhbmNlZFNlYXJjaERhdGEgfSBmcm9tICcuLi9vYmplY3RzL0FkdmFuY2VkU2VhcmNoRGF0YSc7XHJcbmltcG9ydCB7IFNlYXJjaERhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvc2VhcmNoLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmRpdGlvbkxpc3QgfSBmcm9tICcuLi9vYmplY3RzL0NvbmRpdGlvbkxpc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hTYXZlU2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC5zYXZlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXNpbXBsZS1zZWFyY2gnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NpbXBsZS1zZWFyY2guY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vc2ltcGxlLXNlYXJjaC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2ltcGxlU2VhcmNoQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBzYXZlZFNlYXJjaGVzID0gW107XHJcbiAgICByZWNlbnRTZWFyY2hlcyA9IFtdO1xyXG4gICAgc2VhcmNoS2V5d29yZDogc3RyaW5nO1xyXG4gICAgYWR2RGlhbG9nUmVmO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxTaW1wbGVTZWFyY2hDb21wb25lbnQ+LFxyXG4gICAgICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogU2ltcGxlU2VhcmNoRGF0YSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2VhcmNoQ29uZmlnU2VydmljZTogU2VhcmNoQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2VhcmNoRGF0YVNlcnZpY2U6IFNlYXJjaERhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzZWFyY2hTYXZlU2VydmljZTogU2VhcmNoU2F2ZVNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgc2VhcmNoKHNlYXJjaEtleXdvcmQpIHtcclxuICAgICAgICBpZiAoc2VhcmNoS2V5d29yZCA9PSBudWxsIHx8IHNlYXJjaEtleXdvcmQgPT09ICcnIHx8ICh0eXBlb2YgKHNlYXJjaEtleXdvcmQpID09PSAnc3RyaW5nJyAmJiBzZWFyY2hLZXl3b3JkLnRyaW0oKSA9PT0gJycpKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdLaW5kbHkgZW50ZXIgdGhlIHZhbHVlIHRvIGJlIHNlYXJjaGVkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaEtleXdvcmQgJiYgc2VhcmNoS2V5d29yZFswXSAmJiBzZWFyY2hLZXl3b3JkWzBdLmlzQWR2YW5jZWRTZWFyY2gpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZVNpbXBsZVNlYXJjaFBvcHVwKHNlYXJjaEtleXdvcmQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlYXJjaENvbmRpdGlvbnMgPSBbe1xyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFfZmllbGRfaWQ6IHRoaXMuc2VhcmNoRGF0YVNlcnZpY2UuS0VZV09SRF9NQVBQRVIsXHJcbiAgICAgICAgICAgICAgICBrZXl3b3JkOiBzZWFyY2hLZXl3b3JkLFxyXG5cclxuICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0UmVjZW50U2VhcmNoKHNlYXJjaEtleXdvcmQpO1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlU2ltcGxlU2VhcmNoUG9wdXAoc2VhcmNoQ29uZGl0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlU2ltcGxlU2VhcmNoUG9wdXAoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFNhdmVkU2VhcmNoRGV0YWlsKHNhdmVkU2VhcmNoOiBNUE1TYXZlZFNlYXJjaCkge1xyXG4gICAgICAgIGlmIChzYXZlZFNlYXJjaC5TRUFSQ0hfREFUQSkge1xyXG4gICAgICAgICAgICBjb25zdCBzZWFyY2hDb25kaXRpb25zOiBDb25kaXRpb25MaXN0W10gPSBzYXZlZFNlYXJjaC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbjtcclxuICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9uc1swXS5pc0FkdmFuY2VkU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9uc1swXS5zYXZlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnNbMF0uTkFNRSA9IHNhdmVkU2VhcmNoLk5BTUU7XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnNbMF0uc2F2ZWRTZWFyY2hJZCA9IHNhdmVkU2VhcmNoWydNUE1fU2F2ZWRfU2VhcmNoZXMtaWQnXS5JZDtcclxuICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2goc2VhcmNoQ29uZGl0aW9ucyk7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VTaW1wbGVTZWFyY2hQb3B1cChzYXZlZFNlYXJjaCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVNhdmVkU2VhcmNoKHNhdmVkU2VhcmNoOiBNUE1TYXZlZFNlYXJjaCkge1xyXG4gICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHNhdmVkIHNlYXJjaD8nLFxyXG4gICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoU2F2ZVNlcnZpY2UuZGVsZXRlU2F2ZWRTZWFyY2goc2F2ZWRTZWFyY2hbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddLklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWxsU2F2ZWRTZWFyY2hlcygpLnN1YnNjcmliZShzZWFyY2hSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgZGVsZXRpbmcgc2F2ZWQgc2VhcmNoJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGVkaXRTYXZlZFNlYXJjaChzYXZlZFNlYXJjaCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEuc2VhcmNoQ29uZmlnXHJcbiAgICAgICAgICAgICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZy5hZHZhbmNlZFNlYXJjaENvbmZpZ3VyYXRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5vcGVuQWR2YW5jZWRTZWFyY2goc2F2ZWRTZWFyY2gsIGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdFJlY2VudEFkdmFuY2VkU2VhcmNoKHJlc2VudFNlYXJjaCkge1xyXG4gICAgICAgIHJlc2VudFNlYXJjaC5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBkYXRhLmlzRWRpdCA9IHRydWU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcGVuQWR2YW5jZWRTZWFyY2goZmFsc2UsIHJlc2VudFNlYXJjaCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsU2F2ZWRTZWFyY2hlcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5nZXRTYXZlZFNlYXJjaGVzRm9yVmlld0FuZEN1cnJlbnRVc2VyKHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZy5zZWFyY2hJZGVudGlmaWVyLlZJRVcpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hlcyA9IHJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIHRoZSBzYXZlZCBzZWFyY2hlcy4nKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsUmVjZW50U2VhcmNoZXMoKSB7XHJcbiAgICAgICAgLy8gY29uc3QgYWxsUmVjZW50U2VhcmNoZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFJlY2VudFNlYXJjaGVzKCk7XHJcbiAgICAgICAgY29uc3QgYWxsUmVjZW50U2VhcmNoZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFJlY2VudFNlYXJjaGVzQnlWaWV3KHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZy5zZWFyY2hJZGVudGlmaWVyLlZJRVcpO1xyXG4gICAgICAgIGNvbnN0IGFsbG93ZWRSZWNlbnRTZWFyY2hlcyA9IFtdO1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGFsbFJlY2VudFNlYXJjaGVzKSkge1xyXG4gICAgICAgICAgICBhbGxSZWNlbnRTZWFyY2hlcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhZHZJdGVtID0gaXRlbVswXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoYWR2SXRlbSAmJiBhZHZJdGVtLmlzQWR2YW5jZWRTZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFhZHZJdGVtLmlzRWRpdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsb3dlZFJlY2VudFNlYXJjaGVzLnB1c2goaXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd2VkUmVjZW50U2VhcmNoZXMucHVzaChpdGVtKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZWNlbnRTZWFyY2hlcyA9IFsuLi5uZXcgU2V0KGFsbG93ZWRSZWNlbnRTZWFyY2hlcyldO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5BZHZhbmNlZFNlYXJjaE1vZGFsKGFkdkRhdGEpIHtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLnNlYXJjaERhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcgJiYgKHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnLk5BTUUgIT09ICcnIHx8IHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnWzBdLk5BTUUgIT09ICcnKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoZXMuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLk5BTUUgPT09ICh0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZy5OQU1FID8gdGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcuTkFNRSA6IHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnWzBdLk5BTUUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWR2RGF0YS5zYXZlZFNlYXJjaCA9IGRhdGE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoYWR2RGF0YS5yZWNlbnRTZWFyY2ggJiYgYWR2RGF0YS5yZWNlbnRTZWFyY2hbMF0gJiYgYWR2RGF0YS5yZWNlbnRTZWFyY2hbMF0uTkFNRSAhPT0gJycpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaGVzLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5OQU1FID09PSBhZHZEYXRhLnJlY2VudFNlYXJjaFswXS5OQU1FKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWR2RGF0YS5zYXZlZFNlYXJjaCA9IGRhdGE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmFkdkRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQWR2YW5jZWRTZWFyY2hDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHsgdG9wOiAnMCcgfSxcclxuICAgICAgICAgICAgd2lkdGg6ICcxMTAwcHgnLFxyXG4gICAgICAgICAgICAvLyBoZWlnaHQ6ICc2NSUnLFxyXG4gICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgIGRhdGE6IGFkdkRhdGFcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmFkdkRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0ID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgdGhpcy5kaWFsb2dSZWYuY2xvc2UocmVzdWx0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQWR2YW5jZWRTZWFyY2goc2F2ZWRTZWFyY2hJZCwgcmVjZW50U2VhcmNoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSAmJiB0aGlzLmRhdGEuc2VhcmNoRGF0YSAmJiB0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hDb25maWdcclxuICAgICAgICAgICAgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEuc2VhcmNoQ29uZmlnLmFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbikge1xyXG4gICAgICAgICAgICBjb25zdCBhZHZEYXRhOiBBZHZhbmNlZFNlYXJjaERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBhZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGE6IHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZyxcclxuICAgICAgICAgICAgICAgIGF2YWlsYWJsZVNhdmVkU2VhcmNoOiB0aGlzLnNhdmVkU2VhcmNoZXMsXHJcbiAgICAgICAgICAgICAgICBzYXZlZFNlYXJjaDogc2F2ZWRTZWFyY2hJZCxcclxuICAgICAgICAgICAgICAgIHJlY2VudFNlYXJjaDogcmVjZW50U2VhcmNoXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGlmIChzYXZlZFNlYXJjaElkKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHNhdmVkU2VhcmNoSWQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hDb25maWdTZXJ2aWNlLmdldFNhdmVkU2VhcmNoZUJ5SWQoc2F2ZWRTZWFyY2hJZCkuc3Vic2NyaWJlKHNhdmVkU2VhcmNoRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzYXZlZFNlYXJjaERldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkdkRhdGEuc2F2ZWRTZWFyY2ggPSBzYXZlZFNlYXJjaERldGFpbHM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5BZHZhbmNlZFNlYXJjaE1vZGFsKGFkdkRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3BlbkFkdmFuY2VkU2VhcmNoTW9kYWwoYWR2RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wZW5BZHZhbmNlZFNlYXJjaE1vZGFsKGFkdkRhdGEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzprZXl1cC5lc2MnKSBvbktleVVwKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmFkdkRpYWxvZ1JlZikge1xyXG4gICAgICAgICAgICB0aGlzLmFkdkRpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnNlYXJjaEtleXdvcmQgPSB0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hWYWx1ZTtcclxuICAgICAgICB0aGlzLmdldEFsbFNhdmVkU2VhcmNoZXMoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kYXRhLnNlYXJjaERhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ1snTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10gJiZcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ1snTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVkaXRTYXZlZFNlYXJjaCh0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZyk7XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5BZHZhbmNlZFNlYXJjaChmYWxzZSwgdGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5nZXRBbGxSZWNlbnRTZWFyY2hlcygpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==