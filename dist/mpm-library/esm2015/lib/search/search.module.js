import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SimpleSearchComponent } from './simple-search/simple-search.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';
import { SearchIconComponent } from './search-icon/search-icon.component';
import { MaterialModule } from '../material.module';
import { AdvancedSearchFieldComponent } from './advanced-search-field/advanced-search-field.component';
let SearchModule = class SearchModule {
};
SearchModule = __decorate([
    NgModule({
        declarations: [
            SimpleSearchComponent,
            AdvancedSearchComponent,
            SearchIconComponent,
            AdvancedSearchFieldComponent,
        ],
        imports: [
            CommonModule,
            MaterialModule
        ],
        exports: [
            SimpleSearchComponent,
            SearchIconComponent,
            AdvancedSearchComponent
        ],
        entryComponents: [],
        providers: [DatePipe]
    })
], SearchModule);
export { SearchModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDaEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDdEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBc0J2RyxJQUFhLFlBQVksR0FBekIsTUFBYSxZQUFZO0NBQUksQ0FBQTtBQUFoQixZQUFZO0lBcEJ4QixRQUFRLENBQUM7UUFDTixZQUFZLEVBQUU7WUFDVixxQkFBcUI7WUFDckIsdUJBQXVCO1lBQ3ZCLG1CQUFtQjtZQUNuQiw0QkFBNEI7U0FDL0I7UUFDRCxPQUFPLEVBQUU7WUFDTCxZQUFZO1lBQ1osY0FBYztTQUNqQjtRQUNELE9BQU8sRUFBRTtZQUNMLHFCQUFxQjtZQUNyQixtQkFBbUI7WUFDbkIsdUJBQXVCO1NBQzFCO1FBQ0QsZUFBZSxFQUFFLEVBQUU7UUFDbkIsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDO0tBQ3hCLENBQUM7R0FFVyxZQUFZLENBQUk7U0FBaEIsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSwgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBTaW1wbGVTZWFyY2hDb21wb25lbnQgfSBmcm9tICcuL3NpbXBsZS1zZWFyY2gvc2ltcGxlLXNlYXJjaC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBZHZhbmNlZFNlYXJjaENvbXBvbmVudCB9IGZyb20gJy4vYWR2YW5jZWQtc2VhcmNoL2FkdmFuY2VkLXNlYXJjaC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWFyY2hJY29uQ29tcG9uZW50IH0gZnJvbSAnLi9zZWFyY2gtaWNvbi9zZWFyY2gtaWNvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IEFkdmFuY2VkU2VhcmNoRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2FkdmFuY2VkLXNlYXJjaC1maWVsZC9hZHZhbmNlZC1zZWFyY2gtZmllbGQuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBTaW1wbGVTZWFyY2hDb21wb25lbnQsXHJcbiAgICAgICAgQWR2YW5jZWRTZWFyY2hDb21wb25lbnQsXHJcbiAgICAgICAgU2VhcmNoSWNvbkNvbXBvbmVudCxcclxuICAgICAgICBBZHZhbmNlZFNlYXJjaEZpZWxkQ29tcG9uZW50LFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgU2ltcGxlU2VhcmNoQ29tcG9uZW50LFxyXG4gICAgICAgIFNlYXJjaEljb25Db21wb25lbnQsXHJcbiAgICAgICAgQWR2YW5jZWRTZWFyY2hDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtdLFxyXG4gICAgcHJvdmlkZXJzOiBbRGF0ZVBpcGVdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2VhcmNoTW9kdWxlIHsgfVxyXG4iXX0=