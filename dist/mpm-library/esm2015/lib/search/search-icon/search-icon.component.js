import { __decorate } from "tslib";
import { Component, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SimpleSearchComponent } from '../simple-search/simple-search.component';
import { SearchChangeEventService } from '../services/search-change-event.service';
import { SearchEventTypes } from '../objects/search-event-types';
import { SearchDataService } from '../services/search-data.service';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { forkJoin, BehaviorSubject } from 'rxjs';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ActivatedRoute } from '@angular/router';
import { StateService } from '../../shared/services/state.service';
let SearchIconComponent = class SearchIconComponent {
    constructor(dialog, searchChangeEventService, searchDataService, searchConfigService, fieldConfigService, activatedRoute, sharingService, stateService) {
        this.dialog = dialog;
        this.searchChangeEventService = searchChangeEventService;
        this.searchDataService = searchDataService;
        this.searchConfigService = searchConfigService;
        this.fieldConfigService = fieldConfigService;
        this.activatedRoute = activatedRoute;
        this.sharingService = sharingService;
        this.stateService = stateService;
        this.search = new EventEmitter();
        this.isAdvancedSearch = false;
        this.searchName = null;
        this.searchData = [];
        this.savedSearchName = null;
        this.isSavedSearchData = new BehaviorSubject(false);
        this.isAdvancedSearchData = new BehaviorSubject(false);
        this.advSearchData = null;
    }
    clearSearchValue(isEmitEvent) {
        this.searchValue = '';
        this.clearSearch = false;
        this.isAdvancedSearch = false;
        this.advancedSearchConfig = null;
        if (isEmitEvent) {
            this.search.emit([]);
        }
    }
    openSimpleSearchPopup(advancedSearchConfig) {
        if (advancedSearchConfig && advancedSearchConfig[0]) {
            advancedSearchConfig[0].isEdit = true;
        }
        const dialogRef = this.dialog.open(SimpleSearchComponent, {
            position: {
                top: '0'
            },
            width: '575px',
            data: {
                searchData: {
                    searchValue: this.searchValue,
                    searchConfig: this.searchConfig,
                    advancedSearchConfig: advancedSearchConfig
                }
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result && result.length > 0) {
                if (result[0].isAdvancedSearch) {
                    this.savedSearchName = result[0].NAME ? result[0].NAME : null;
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                }
                else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                    this.searchName = this.searchValue;
                }
                this.clearSearch = true;
                this.search.emit(result);
            }
            else if (result && result.SEARCH_DATA && result.SEARCH_DATA.search_condition_list &&
                result.SEARCH_DATA.search_condition_list.search_condition &&
                result.SEARCH_DATA.search_condition_list.search_condition.length) {
                this.savedSearchName = result.NAME;
                if (result.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                    this.advancedSearchConfig = result;
                    this.isAdvancedSearch = true;
                }
                else {
                    this.isAdvancedSearch = false;
                    this.searchValue = result[0].keyword || result[0].value;
                }
                this.clearSearch = true;
                this.search.emit(result.SEARCH_DATA.search_condition_list.search_condition);
            }
        });
    }
    getAdvancedSearchConfig() {
        forkJoin([this.fieldConfigService.getAllMPMFieldConfig(), this.searchConfigService.setAllAdvancedSearchConfig(), this.searchConfigService.setAllSearchOperators()]).subscribe(response => {
            if (response && response[0] && response[1] && response[2]) {
                this.searchConfig.metadataFields = response[0];
                this.searchConfig.advancedSearchConfiguration = response[1];
                this.searchChangeEventService.setAdvancedSearchConfig(this.searchConfig.advancedSearchConfiguration);
                this.searchConfig.searchOperatorList = response[2];
                this.isAdvancedSearchData.next(true);
            }
        });
    }
    initConfigs() {
        this.searchConfig = {
            advancedSearchConfiguration: [],
            metadataFields: [],
            searchOperatorList: [],
            // searchConfigId: '',
            searchIdentifier: null
        };
        this.getAdvancedSearchConfig();
        this.searchConfig.searchIdentifier = this.searchIdentifier;
    }
    formAdvSearchData(data) {
        const searchDataList = JSON.parse(data);
        const advSearchConfigFields = this.searchConfigService.getAdvancedSearchConfigById(searchDataList[0].searchIdentifier);
        const advSearchOperators = this.searchConfigService.getAllSearchOperators();
        const advancedSearchConditionList = [];
        searchDataList.forEach((searchData, index) => {
            const selectedField = advSearchConfigFields.R_PM_Search_Fields.find(searchConfig => searchConfig.MAPPER_NAME === searchData.mapperName);
            const fieldOperators = advSearchOperators.filter(operator => operator.DATA_TYPE === selectedField.DATA_TYPE);
            const selectedOperator = searchData.relationalOperatorName ? fieldOperators.find(operator => operator.NAME === searchData.relationalOperatorName) : '';
            const values = searchData.hasTwoValues ? searchData.value.split(' and ') : searchData.value;
            const filterRowData = {
                availableSearchFields: [],
                hasTwoValues: searchData.hasTwoValues,
                isFilterSelected: true,
                issearchValueRequired: searchData.value ? true : false,
                rowId: index + 1,
                searchField: selectedField.MPM_FIELD_CONFIG_ID,
                searchOperatorId: selectedOperator ? selectedOperator.OPERATOR_ID : '',
                searchOperatorName: selectedOperator ? selectedOperator.NAME : '',
                searchSecondValue: searchData.hasTwoValues ? (values[1] ? values[1] : '') : '',
                searchValue: searchData.hasTwoValues ? (values[0] ? values[0] : '') : searchData.value
            };
            const advancedSearchCondition = {
                field_id: selectedField.INDEXER_FIELD_ID,
                filterRowData: filterRowData,
                isAdvancedSearch: true,
                mapper_field_id: selectedField.MPM_FIELD_CONFIG_ID,
                relational_operator: MPMSearchOperators.AND,
                relational_operator_id: selectedOperator ? selectedOperator.OPERATOR_ID : MPMSearchOperators.IS,
                relational_operator_name: selectedOperator ? selectedOperator.NAME : MPMSearchOperatorNames.IS,
                searchIdentifier: searchData.searchIdentifier,
                type: selectedField.DATA_TYPE,
                value: searchData.value,
                mapper_name: selectedField.MAPPER_NAME,
                view: searchData.view
            };
            advancedSearchConditionList.push(advancedSearchCondition);
        });
        return advancedSearchConditionList;
    }
    ngOnInit() {
        this.tmpViewToggleEventServiceSubscription$ = this.searchChangeEventService.onViewToggleSearch.subscribe((data) => {
            if (data && data.action === SearchEventTypes.CLEAR) {
                this.clearSearchValue(false);
            }
        });
        this.tmpSearchChangeEventServiceSubscription$ = this.searchChangeEventService.onSearchViewChange.subscribe((viewConfigDetails) => {
            this.searchIdentifier = viewConfigDetails ? viewConfigDetails : null;
            if (this.searchIdentifier) {
                this.searchDataService.setSearchData([]);
            }
            this.initConfigs();
            this.searchChangeEventService.updateOnViewChange({ action: SearchEventTypes.CLEAR });
            this.activatedRoute.queryParams.subscribe(params => {
                if ((params.viewName !== this.selectedView || params.groupByFilter !== this.groupByFilter ||
                    params.deliverableTaskFilter !== this.deliverableTaskFilter || params.menuName !== this.menuName) ||
                    !((params.searchName === this.searchName) || (params.savedSearchName === this.savedSearchName) || (params.advSearchData === this.advSearchData))) {
                    if (params.viewName && this.selectedView && this.selectedView !== params.viewName) {
                        this.selectedView = params.viewName;
                        this.clearSearchValue(true);
                    }
                    else if ((params.groupByFilter && this.groupByFilter && this.groupByFilter !== params.groupByFilter) ||
                        params.deliverableTaskFilter && this.deliverableTaskFilter && this.deliverableTaskFilter !== params.deliverableTaskFilter) {
                        this.groupByFilter = params.groupByFilter;
                        this.deliverableTaskFilter = params.deliverableTaskFilter;
                        this.clearSearchValue(true);
                    }
                    else {
                        this.selectedView = params.viewName;
                        this.groupByFilter = params.groupByFilter;
                        this.deliverableTaskFilter = params.deliverableTaskFilter;
                        if (params.searchName && this.searchIdentifier) {
                            if (this.searchName !== params.searchName || this.menuName !== params.menuName) {
                                this.menuName = params.menuName;
                                this.searchName = params.searchName;
                                this.searchData = [];
                                this.searchData.push({
                                    keyword: this.searchName,
                                    metadata_field_id: 'MPM.FIELD.ALL_KEYWORD'
                                });
                                if (this.searchData && this.searchData[0] && this.searchData[0].keyword) {
                                    const searches = this.sharingService.getRecentSearches();
                                    if (searches) {
                                        const isSearched = searches.find(searchData => searchData === this.searchName);
                                        if (!isSearched) {
                                            this.sharingService.setRecentSearch(this.searchName);
                                        }
                                    }
                                    else {
                                        this.sharingService.setRecentSearch(this.searchName);
                                    }
                                    if (this.searchData && this.searchData.length > 0) {
                                        if (this.searchData[0].isAdvancedSearch) {
                                            this.advancedSearchConfig = this.searchData;
                                            this.isAdvancedSearch = true;
                                        }
                                        else {
                                            this.isAdvancedSearch = false;
                                            this.searchValue = this.searchData[0].keyword || this.searchData[0].value;
                                        }
                                        this.clearSearch = true;
                                        this.search.emit(this.searchData);
                                    }
                                }
                            }
                        }
                        else if (!params.searchName && this.searchName !== null) {
                            this.searchName = null;
                        }
                        if (params.savedSearchName && this.searchIdentifier) {
                            if (this.savedSearchName !== params.savedSearchName || this.menuName !== params.menuName) {
                                this.menuName = params.menuName;
                                this.savedSearchName = params.savedSearchName;
                                this.isSavedSearchTrigger = false;
                                this.isSavedSearchData.next(true);
                            }
                        }
                        else if (!params.savedSearchName && this.savedSearchName !== null) {
                            this.savedSearchName = null;
                        }
                        if (params.advSearchData && this.searchIdentifier) {
                            this.advSearchData = params.advSearchData;
                            const advancedSearchData = this.stateService.getAdvancedSearch(params.menuName);
                            if (advancedSearchData && advancedSearchData.length > 0) {
                                if (advancedSearchData !== this.advancedSearchConfig || this.menuName !== params.menuName) {
                                    this.menuName = params.menuName;
                                    if (advancedSearchData[0].isAdvancedSearch) {
                                        this.advancedSearchConfig = advancedSearchData;
                                        this.isAdvancedSearch = true;
                                    }
                                    this.clearSearch = true;
                                    this.search.emit(advancedSearchData);
                                }
                            }
                            else {
                                this.isAdvancedSearchTrigger = false;
                                this.isAdvancedSearchData.subscribe(response => {
                                    if (response && !this.isAdvancedSearchTrigger && this.searchConfig.advancedSearchConfiguration) {
                                        this.isAdvancedSearchTrigger = true;
                                        const searchCondition = this.formAdvSearchData(this.advSearchData);
                                        this.sharingService.setRecentSearch(searchCondition);
                                        if (searchCondition && searchCondition.length > 0) {
                                            if (searchCondition[0].isAdvancedSearch) {
                                                this.advancedSearchConfig = searchCondition;
                                                this.isAdvancedSearch = true;
                                            }
                                            this.clearSearch = true;
                                            this.search.emit(searchCondition);
                                        }
                                    }
                                });
                            }
                        }
                        else if (!params.advSearchData && this.advSearchData !== null) {
                            this.advSearchData = null;
                        }
                        if (!params.searchName && !params.advSearchData && !params.savedSearchName) {
                            this.menuName = params.menuName;
                        }
                    }
                }
            });
        });
        this.isSavedSearchData.subscribe(res => {
            if (res) {
                if (this.savedSearchName && !this.isSavedSearchTrigger && this.searchConfig && this.searchConfig.searchIdentifier && this.searchConfig.searchIdentifier.VIEW) {
                    this.searchConfigService.getSavedSearchesForViewAndCurrentUser(this.searchConfig.searchIdentifier.VIEW).subscribe(responseData => {
                        if (responseData && responseData.length > 0) {
                            this.savedSearches = responseData;
                        }
                        else {
                            this.savedSearches = [];
                        }
                        if (this.savedSearches && !this.isSavedSearchTrigger) {
                            const result = this.savedSearches.find(data => data.NAME === this.savedSearchName);
                            if (result && result.SEARCH_DATA && result.SEARCH_DATA.search_condition_list &&
                                result.SEARCH_DATA.search_condition_list.search_condition &&
                                result.SEARCH_DATA.search_condition_list.search_condition.length) {
                                result.SEARCH_DATA.search_condition_list.search_condition[0].savedSearchId = result['MPM_Saved_Searches-id'].Id;
                                result.SEARCH_DATA.search_condition_list.search_condition[0].NAME = result.NAME;
                                const searches = this.sharingService.getRecentSearches();
                                if (searches) {
                                    const isSearched = searches.find(searchData => searchData[0].savedSearchId === result.SEARCH_DATA.search_condition_list.search_condition[0].savedSearchId);
                                    if (!isSearched) {
                                        this.sharingService.setRecentSearch(result.SEARCH_DATA.search_condition_list.search_condition);
                                    }
                                }
                                else {
                                    this.sharingService.setRecentSearch(result.SEARCH_DATA.search_condition_list.search_condition);
                                }
                                this.isSavedSearchTrigger = true;
                                if (result.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                                    this.advancedSearchConfig = result;
                                    this.isAdvancedSearch = true;
                                }
                                else {
                                    this.isAdvancedSearch = false;
                                    this.searchValue = result[0].keyword || result[0].value;
                                }
                                this.clearSearch = true;
                                this.search.emit(result.SEARCH_DATA.search_condition_list.search_condition);
                            }
                        }
                    });
                }
            }
        });
    }
    ngOnDestroy() {
        if (this.tmpViewToggleEventServiceSubscription$) {
            this.tmpViewToggleEventServiceSubscription$.unsubscribe();
        }
        if (this.tmpSearchChangeEventServiceSubscription$) {
            this.tmpSearchChangeEventServiceSubscription$.unsubscribe();
        }
    }
};
SearchIconComponent.ctorParameters = () => [
    { type: MatDialog },
    { type: SearchChangeEventService },
    { type: SearchDataService },
    { type: SearchConfigService },
    { type: FieldConfigService },
    { type: ActivatedRoute },
    { type: SharingService },
    { type: StateService }
];
__decorate([
    Input()
], SearchIconComponent.prototype, "backgroundColor", void 0);
__decorate([
    Input()
], SearchIconComponent.prototype, "searchIdentifier", void 0);
__decorate([
    Output()
], SearchIconComponent.prototype, "search", void 0);
SearchIconComponent = __decorate([
    Component({
        selector: 'mpm-search-icon',
        template: "<div class=\"search-box\" [style.background-color]=\"backgroundColor\" (click)=\"openSimpleSearchPopup(null)\"\r\n    *ngIf=\"searchIdentifier\">\r\n    <button mat-icon-button color=\"primary\" matTooltip=\"Search\">\r\n        <mat-icon>search</mat-icon>\r\n    </button>\r\n\r\n    <span class=\"placeholder\" *ngIf=\"!searchValue && !isAdvancedSearch\">Search for content..</span>\r\n\r\n    <mat-chip-list #chipList aria-label=\"Current Search\" *ngIf=\"searchValue && !isAdvancedSearch\" class=\"\">\r\n        <mat-chip *ngIf=\"!isAdvancedSearch\" [removable]=\"true\" (removed)=\"clearSearchValue(true)\">\r\n            Keyword: <span class=\"wrap-text-custom\" matTooltip=\"{{searchValue}}\"> {{searchValue}} </span>\r\n            <mat-icon matTooltip=\"Clear\" matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n\r\n    <mat-chip-list #chipList aria-label=\"Current Search\" *ngIf=\"isAdvancedSearch && advancedSearchConfig\">\r\n        <mat-chip class=\"cursor-pointer\" (click)=\"openSimpleSearchPopup(advancedSearchConfig)\" [removable]=\"true\"\r\n            (removed)=\"clearSearchValue(true)\">\r\n            <span class=\"wrap-text-custom\"\r\n                matTooltip=\"{{advancedSearchConfig.NAME ? advancedSearchConfig.NAME : (advancedSearchConfig[0] && advancedSearchConfig[0].NAME ? advancedSearchConfig[0].NAME : 'Advanced Search Criteria')}}\">\r\n                {{advancedSearchConfig.NAME ? advancedSearchConfig.NAME : (advancedSearchConfig[0] && advancedSearchConfig[0].NAME ? advancedSearchConfig[0].NAME : 'Advanced Search Criteria')}}\r\n            </span>\r\n            <mat-icon matTooltip=\"Clear\" matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n</div>",
        styles: [".search-box{display:flex;flex-direction:row;width:450px;padding:12px;justify-content:flex-start;align-items:center;cursor:pointer}.search-box .placeholder{font-size:14px;font-weight:300}.search-box mat-chip-list{overflow:hidden}.search-box mat-chip-list mat-chip{max-width:98%}.cursor-pointer{cursor:pointer}.wrap-text-custom{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
    })
], SearchIconComponent);
export { SearchIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWljb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2VhcmNoL3NlYXJjaC1pY29uL3NlYXJjaC1pY29uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDakYsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFFbkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDakUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDakQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBSWhGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFTbkUsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUE0QjVCLFlBQ1csTUFBaUIsRUFDakIsd0JBQWtELEVBQ2xELGlCQUFvQyxFQUNwQyxtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLFlBQTBCO1FBUDFCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUNsRCxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBakMzQixXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUs5QyxxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFNekIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ3hELHlCQUFvQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBUTNELGtCQUFhLEdBQUcsSUFBSSxDQUFDO0lBV2pCLENBQUM7SUFFTCxnQkFBZ0IsQ0FBQyxXQUFXO1FBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLFdBQVcsRUFBRTtZQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELHFCQUFxQixDQUFDLG9CQUE2QztRQUMvRCxJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2pELG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDekM7UUFDRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUN0RCxRQUFRLEVBQUU7Z0JBQ04sR0FBRyxFQUFFLEdBQUc7YUFDWDtZQUNELEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFO2dCQUNGLFVBQVUsRUFBRTtvQkFDUixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7b0JBQzdCLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtvQkFDL0Isb0JBQW9CLEVBQUUsb0JBQW9CO2lCQUM3QzthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN2QyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDN0IsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUM5RCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDO29CQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2lCQUNoQztxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO29CQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDeEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2lCQUN0QztnQkFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDNUI7aUJBQU0sSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtnQkFDL0UsTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0I7Z0JBQ3pELE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dCQUNsRSxJQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ25DLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDL0UsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQztvQkFDbkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztpQkFDaEM7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQzNEO2dCQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDL0U7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1QkFBdUI7UUFDbkIsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixFQUFFLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixFQUFFLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNyTCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDdkQsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLDJCQUEyQixHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsMkJBQTJCLENBQUMsQ0FBQztnQkFDckcsSUFBSSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFlBQVksR0FBRztZQUNoQiwyQkFBMkIsRUFBRSxFQUFFO1lBQy9CLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLGtCQUFrQixFQUFFLEVBQUU7WUFDdEIsc0JBQXNCO1lBQ3RCLGdCQUFnQixFQUFFLElBQUk7U0FDekIsQ0FBQztRQUNGLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQy9ELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxJQUFJO1FBQ2xCLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsTUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsMkJBQTJCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDdkgsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUM1RSxNQUFNLDJCQUEyQixHQUFHLEVBQUUsQ0FBQztRQUN2QyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQ3pDLE1BQU0sYUFBYSxHQUFHLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hJLE1BQU0sY0FBYyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEtBQUssYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdHLE1BQU0sZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3ZKLE1BQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO1lBQzVGLE1BQU0sYUFBYSxHQUFHO2dCQUNsQixxQkFBcUIsRUFBRSxFQUFFO2dCQUN6QixZQUFZLEVBQUUsVUFBVSxDQUFDLFlBQVk7Z0JBQ3JDLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLHFCQUFxQixFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDdEQsS0FBSyxFQUFFLEtBQUssR0FBRyxDQUFDO2dCQUNoQixXQUFXLEVBQUUsYUFBYSxDQUFDLG1CQUFtQjtnQkFDOUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDdEUsa0JBQWtCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDakUsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzlFLFdBQVcsRUFBRSxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUs7YUFDekYsQ0FBQztZQUNGLE1BQU0sdUJBQXVCLEdBQUc7Z0JBQzVCLFFBQVEsRUFBRSxhQUFhLENBQUMsZ0JBQWdCO2dCQUN4QyxhQUFhLEVBQUUsYUFBYTtnQkFDNUIsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsZUFBZSxFQUFFLGFBQWEsQ0FBQyxtQkFBbUI7Z0JBQ2xELG1CQUFtQixFQUFFLGtCQUFrQixDQUFDLEdBQUc7Z0JBQzNDLHNCQUFzQixFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7Z0JBQy9GLHdCQUF3QixFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEVBQUU7Z0JBQzlGLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxnQkFBZ0I7Z0JBQzdDLElBQUksRUFBRSxhQUFhLENBQUMsU0FBUztnQkFDN0IsS0FBSyxFQUFFLFVBQVUsQ0FBQyxLQUFLO2dCQUN2QixXQUFXLEVBQUUsYUFBYSxDQUFDLFdBQVc7Z0JBQ3RDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSTthQUN4QixDQUFDO1lBQ0YsMkJBQTJCLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLDJCQUEyQixDQUFDO0lBQ3ZDLENBQUM7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLHNDQUFzQyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFzQixFQUFFLEVBQUU7WUFDaEksSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNoQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHdDQUF3QyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxpQkFBNkIsRUFBRSxFQUFFO1lBQ3pJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNyRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM1QztZQUNELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUNyRixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQy9DLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxZQUFZLElBQUksTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsYUFBYTtvQkFDckYsTUFBTSxDQUFDLHFCQUFxQixLQUFLLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ2pHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO29CQUNsSixJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7d0JBQy9FLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUMvQjt5QkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLEtBQUssTUFBTSxDQUFDLGFBQWEsQ0FBQzt3QkFDbEcsTUFBTSxDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEtBQUssTUFBTSxDQUFDLHFCQUFxQixFQUFFO3dCQUMzSCxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7d0JBQzFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUM7d0JBQzFELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDL0I7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO3dCQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7d0JBQzFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUM7d0JBQzFELElBQUksTUFBTSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQzVDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssTUFBTSxDQUFDLFFBQVEsRUFBRTtnQ0FDNUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO2dDQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0NBQ3BDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dDQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQ0FDakIsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVO29DQUN4QixpQkFBaUIsRUFBRSx1QkFBdUI7aUNBQzdDLENBQUMsQ0FBQztnQ0FDSCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRTtvQ0FDckUsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29DQUN6RCxJQUFJLFFBQVEsRUFBRTt3Q0FDVixNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzt3Q0FDL0UsSUFBSSxDQUFDLFVBQVUsRUFBRTs0Q0FDYixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7eUNBQ3hEO3FDQUNKO3lDQUFNO3dDQUNILElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDeEQ7b0NBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDL0MsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFOzRDQUNyQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzs0Q0FDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzt5Q0FDaEM7NkNBQU07NENBQ0gsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQzs0Q0FDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt5Q0FDN0U7d0NBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0NBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDckM7aUNBQ0o7NkJBQ0o7eUJBQ0o7NkJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7NEJBQ3ZELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3lCQUMxQjt3QkFDRCxJQUFJLE1BQU0sQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUNqRCxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssTUFBTSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0NBQ3RGLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztnQ0FDaEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDO2dDQUM5QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO2dDQUNsQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNyQzt5QkFDSjs2QkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLElBQUksRUFBRTs0QkFDakUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7eUJBQy9CO3dCQUNELElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQy9DLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQzs0QkFDMUMsTUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDaEYsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUNyRCxJQUFJLGtCQUFrQixLQUFLLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7b0NBQ3ZGLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztvQ0FDaEMsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRTt3Q0FDeEMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGtCQUFrQixDQUFDO3dDQUMvQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3FDQUNoQztvQ0FDRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztvQ0FDeEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztpQ0FDeEM7NkJBQ0o7aUNBQU07Z0NBQ0gsSUFBSSxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztnQ0FDckMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQ0FDM0MsSUFBSSxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQywyQkFBMkIsRUFBRTt3Q0FDNUYsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQzt3Q0FDcEMsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzt3Q0FDbkUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLENBQUM7d0NBQ3JELElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRDQUMvQyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRTtnREFDckMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGVBQWUsQ0FBQztnREFDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs2Q0FDaEM7NENBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7NENBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3lDQUNyQztxQ0FDSjtnQ0FDTCxDQUFDLENBQUMsQ0FBQzs2QkFDTjt5QkFDSjs2QkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLElBQUksRUFBRTs0QkFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7eUJBQzdCO3dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUU7NEJBQ3hFLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzt5QkFDbkM7cUJBQ0o7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNuQyxJQUFJLEdBQUcsRUFBRTtnQkFDTCxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFO29CQUMxSixJQUFJLENBQUMsbUJBQW1CLENBQUMscUNBQXFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEVBQUU7d0JBQzdILElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUN6QyxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQzt5QkFDckM7NkJBQU07NEJBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7eUJBQzNCO3dCQUNELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTs0QkFDbEQsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzs0QkFDbkYsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtnQ0FDeEUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0I7Z0NBQ3pELE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dDQUNsRSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0NBQ2hILE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0NBQ2hGLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQ0FDekQsSUFBSSxRQUFRLEVBQUU7b0NBQ1YsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEtBQUssTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQztvQ0FDM0osSUFBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDYixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUM7cUNBQ2xHO2lDQUNKO3FDQUFNO29DQUNILElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztpQ0FDbEc7Z0NBQ0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztnQ0FDakMsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO29DQUMvRSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDO29DQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2lDQUNoQztxQ0FBTTtvQ0FDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO29DQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQ0FDM0Q7Z0NBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0NBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs2QkFDL0U7eUJBQ0o7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLElBQUksQ0FBQyxzQ0FBc0MsRUFBRTtZQUM3QyxJQUFJLENBQUMsc0NBQXNDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0Q7UUFDRCxJQUFJLElBQUksQ0FBQyx3Q0FBd0MsRUFBRTtZQUMvQyxJQUFJLENBQUMsd0NBQXdDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDL0Q7SUFDTCxDQUFDO0NBQ0osQ0FBQTs7WUE1U3NCLFNBQVM7WUFDUyx3QkFBd0I7WUFDL0IsaUJBQWlCO1lBQ2YsbUJBQW1CO1lBQ3BCLGtCQUFrQjtZQUN0QixjQUFjO1lBQ2QsY0FBYztZQUNoQixZQUFZOztBQW5DNUI7SUFBUixLQUFLLEVBQUU7NERBQWlCO0FBQ2hCO0lBQVIsS0FBSyxFQUFFOzZEQUFrQjtBQUNoQjtJQUFULE1BQU0sRUFBRTttREFBcUM7QUFIckMsbUJBQW1CO0lBTi9CLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxpQkFBaUI7UUFDM0IsNnVEQUEyQzs7S0FFOUMsQ0FBQztHQUVXLG1CQUFtQixDQXlVL0I7U0F6VVksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNpbXBsZVNlYXJjaENvbXBvbmVudCB9IGZyb20gJy4uL3NpbXBsZS1zZWFyY2gvc2ltcGxlLXNlYXJjaC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zZWFyY2gtY2hhbmdlLWV2ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hFdmVudE1vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9zZWFyY2gtZXZlbnQtbW9kYWwnO1xyXG5pbXBvcnQgeyBTZWFyY2hFdmVudFR5cGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9zZWFyY2gtZXZlbnQtdHlwZXMnO1xyXG5pbXBvcnQgeyBTZWFyY2hEYXRhU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3NlYXJjaC1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IGZvcmtKb2luLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ0RhdGEgfSBmcm9tICcuLi9vYmplY3RzL1NlYXJjaENvbmZpZ0RhdGEnO1xyXG5pbXBvcnQgeyBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUFkdmFuY2VkU2VhcmNoQ29uZmlnJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFN0YXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zdGF0ZS5zZXJ2aWNlJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXNlYXJjaC1pY29uJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zZWFyY2gtaWNvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9zZWFyY2gtaWNvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2VhcmNoSWNvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIEBJbnB1dCgpIGJhY2tncm91bmRDb2xvcjtcclxuICAgIEBJbnB1dCgpIHNlYXJjaElkZW50aWZpZXI7XHJcbiAgICBAT3V0cHV0KCkgc2VhcmNoID0gbmV3IEV2ZW50RW1pdHRlcjxvYmplY3Q+KCk7XHJcblxyXG4gICAgc2VhcmNoVmFsdWU6IHN0cmluZztcclxuICAgIGNsZWFyU2VhcmNoOiBib29sZWFuO1xyXG4gICAgc2VhcmNoQ29uZmlnOiBTZWFyY2hDb25maWdEYXRhO1xyXG4gICAgaXNBZHZhbmNlZFNlYXJjaCA9IGZhbHNlO1xyXG4gICAgYWR2YW5jZWRTZWFyY2hDb25maWc7IC8vIE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnXHJcblxyXG4gICAgdG1wU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlU3Vic2NyaXB0aW9uJDtcclxuICAgIHRtcFZpZXdUb2dnbGVFdmVudFNlcnZpY2VTdWJzY3JpcHRpb24kO1xyXG5cclxuICAgIHNlYXJjaE5hbWUgPSBudWxsO1xyXG4gICAgc2VhcmNoRGF0YSA9IFtdO1xyXG4gICAgc2F2ZWRTZWFyY2hOYW1lID0gbnVsbDtcclxuICAgIGlzU2F2ZWRTZWFyY2hEYXRhID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XHJcbiAgICBpc0FkdmFuY2VkU2VhcmNoRGF0YSA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xyXG4gICAgaXNBZHZhbmNlZFNlYXJjaFRyaWdnZXI7XHJcbiAgICBzYXZlZFNlYXJjaGVzO1xyXG4gICAgaXNTYXZlZFNlYXJjaFRyaWdnZXI7XHJcbiAgICBzZWxlY3RlZFZpZXc7XHJcbiAgICBncm91cEJ5RmlsdGVyO1xyXG4gICAgZGVsaXZlcmFibGVUYXNrRmlsdGVyO1xyXG4gICAgbWVudU5hbWU7XHJcbiAgICBhZHZTZWFyY2hEYXRhID0gbnVsbDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIHNlYXJjaENoYW5nZUV2ZW50U2VydmljZTogU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzZWFyY2hEYXRhU2VydmljZTogU2VhcmNoRGF0YVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNlYXJjaENvbmZpZ1NlcnZpY2U6IFNlYXJjaENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc3RhdGVTZXJ2aWNlOiBTdGF0ZVNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgY2xlYXJTZWFyY2hWYWx1ZShpc0VtaXRFdmVudCkge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoVmFsdWUgPSAnJztcclxuICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IG51bGw7XHJcbiAgICAgICAgaWYgKGlzRW1pdEV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoLmVtaXQoW10pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcGVuU2ltcGxlU2VhcmNoUG9wdXAoYWR2YW5jZWRTZWFyY2hDb25maWc6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnKSB7XHJcbiAgICAgICAgaWYgKGFkdmFuY2VkU2VhcmNoQ29uZmlnICYmIGFkdmFuY2VkU2VhcmNoQ29uZmlnWzBdKSB7XHJcbiAgICAgICAgICAgIGFkdmFuY2VkU2VhcmNoQ29uZmlnWzBdLmlzRWRpdCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oU2ltcGxlU2VhcmNoQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICB0b3A6ICcwJ1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB3aWR0aDogJzU3NXB4JyxcclxuICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaFZhbHVlOiB0aGlzLnNlYXJjaFZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaENvbmZpZzogdGhpcy5zZWFyY2hDb25maWcsXHJcbiAgICAgICAgICAgICAgICAgICAgYWR2YW5jZWRTZWFyY2hDb25maWc6IGFkdmFuY2VkU2VhcmNoQ29uZmlnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHRbMF0uaXNBZHZhbmNlZFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hOYW1lID0gcmVzdWx0WzBdLk5BTUUgPyByZXN1bHRbMF0uTkFNRSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IHJlc3VsdDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gcmVzdWx0WzBdLmtleXdvcmQgfHwgcmVzdWx0WzBdLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoTmFtZSA9IHRoaXMuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoLmVtaXQocmVzdWx0KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChyZXN1bHQgJiYgcmVzdWx0LlNFQVJDSF9EQVRBICYmIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3QgJiZcclxuICAgICAgICAgICAgICAgIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbiAmJlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSByZXN1bHQuTkFNRTtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb25bMF0uaXNBZHZhbmNlZFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWcgPSByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hWYWx1ZSA9IHJlc3VsdFswXS5rZXl3b3JkIHx8IHJlc3VsdFswXS52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdChyZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWcoKSB7XHJcbiAgICAgICAgZm9ya0pvaW4oW3RoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEFsbE1QTUZpZWxkQ29uZmlnKCksIHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5zZXRBbGxBZHZhbmNlZFNlYXJjaENvbmZpZygpLCB0aGlzLnNlYXJjaENvbmZpZ1NlcnZpY2Uuc2V0QWxsU2VhcmNoT3BlcmF0b3JzKCldKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2VbMF0gJiYgcmVzcG9uc2VbMV0gJiYgcmVzcG9uc2VbMl0pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnLm1ldGFkYXRhRmllbGRzID0gcmVzcG9uc2VbMF07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaENvbmZpZy5hZHZhbmNlZFNlYXJjaENvbmZpZ3VyYXRpb24gPSByZXNwb25zZVsxXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLnNldEFkdmFuY2VkU2VhcmNoQ29uZmlnKHRoaXMuc2VhcmNoQ29uZmlnLmFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaENvbmZpZy5zZWFyY2hPcGVyYXRvckxpc3QgPSByZXNwb25zZVsyXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaERhdGEubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRDb25maWdzKCkge1xyXG4gICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnID0ge1xyXG4gICAgICAgICAgICBhZHZhbmNlZFNlYXJjaENvbmZpZ3VyYXRpb246IFtdLFxyXG4gICAgICAgICAgICBtZXRhZGF0YUZpZWxkczogW10sXHJcbiAgICAgICAgICAgIHNlYXJjaE9wZXJhdG9yTGlzdDogW10sXHJcbiAgICAgICAgICAgIC8vIHNlYXJjaENvbmZpZ0lkOiAnJyxcclxuICAgICAgICAgICAgc2VhcmNoSWRlbnRpZmllcjogbnVsbFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5nZXRBZHZhbmNlZFNlYXJjaENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnLnNlYXJjaElkZW50aWZpZXIgPSB0aGlzLnNlYXJjaElkZW50aWZpZXI7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUFkdlNlYXJjaERhdGEoZGF0YSkge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaERhdGFMaXN0ID0gSlNPTi5wYXJzZShkYXRhKTtcclxuICAgICAgICBjb25zdCBhZHZTZWFyY2hDb25maWdGaWVsZHMgPSB0aGlzLnNlYXJjaENvbmZpZ1NlcnZpY2UuZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeUlkKHNlYXJjaERhdGFMaXN0WzBdLnNlYXJjaElkZW50aWZpZXIpO1xyXG4gICAgICAgIGNvbnN0IGFkdlNlYXJjaE9wZXJhdG9ycyA9IHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5nZXRBbGxTZWFyY2hPcGVyYXRvcnMoKTtcclxuICAgICAgICBjb25zdCBhZHZhbmNlZFNlYXJjaENvbmRpdGlvbkxpc3QgPSBbXTtcclxuICAgICAgICBzZWFyY2hEYXRhTGlzdC5mb3JFYWNoKChzZWFyY2hEYXRhLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZEZpZWxkID0gYWR2U2VhcmNoQ29uZmlnRmllbGRzLlJfUE1fU2VhcmNoX0ZpZWxkcy5maW5kKHNlYXJjaENvbmZpZyA9PiBzZWFyY2hDb25maWcuTUFQUEVSX05BTUUgPT09IHNlYXJjaERhdGEubWFwcGVyTmFtZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpZWxkT3BlcmF0b3JzID0gYWR2U2VhcmNoT3BlcmF0b3JzLmZpbHRlcihvcGVyYXRvciA9PiBvcGVyYXRvci5EQVRBX1RZUEUgPT09IHNlbGVjdGVkRmllbGQuREFUQV9UWVBFKTtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRPcGVyYXRvciA9IHNlYXJjaERhdGEucmVsYXRpb25hbE9wZXJhdG9yTmFtZSA/IGZpZWxkT3BlcmF0b3JzLmZpbmQob3BlcmF0b3IgPT4gb3BlcmF0b3IuTkFNRSA9PT0gc2VhcmNoRGF0YS5yZWxhdGlvbmFsT3BlcmF0b3JOYW1lKSA6ICcnO1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZXMgPSBzZWFyY2hEYXRhLmhhc1R3b1ZhbHVlcyA/IHNlYXJjaERhdGEudmFsdWUuc3BsaXQoJyBhbmQgJykgOiBzZWFyY2hEYXRhLnZhbHVlO1xyXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJSb3dEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgYXZhaWxhYmxlU2VhcmNoRmllbGRzOiBbXSxcclxuICAgICAgICAgICAgICAgIGhhc1R3b1ZhbHVlczogc2VhcmNoRGF0YS5oYXNUd29WYWx1ZXMsXHJcbiAgICAgICAgICAgICAgICBpc0ZpbHRlclNlbGVjdGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgaXNzZWFyY2hWYWx1ZVJlcXVpcmVkOiBzZWFyY2hEYXRhLnZhbHVlID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcm93SWQ6IGluZGV4ICsgMSxcclxuICAgICAgICAgICAgICAgIHNlYXJjaEZpZWxkOiBzZWxlY3RlZEZpZWxkLk1QTV9GSUVMRF9DT05GSUdfSUQsXHJcbiAgICAgICAgICAgICAgICBzZWFyY2hPcGVyYXRvcklkOiBzZWxlY3RlZE9wZXJhdG9yID8gc2VsZWN0ZWRPcGVyYXRvci5PUEVSQVRPUl9JRCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgc2VhcmNoT3BlcmF0b3JOYW1lOiBzZWxlY3RlZE9wZXJhdG9yID8gc2VsZWN0ZWRPcGVyYXRvci5OQU1FIDogJycsXHJcbiAgICAgICAgICAgICAgICBzZWFyY2hTZWNvbmRWYWx1ZTogc2VhcmNoRGF0YS5oYXNUd29WYWx1ZXMgPyAodmFsdWVzWzFdID8gdmFsdWVzWzFdIDogJycpIDogJycsXHJcbiAgICAgICAgICAgICAgICBzZWFyY2hWYWx1ZTogc2VhcmNoRGF0YS5oYXNUd29WYWx1ZXMgPyAodmFsdWVzWzBdID8gdmFsdWVzWzBdIDogJycpIDogc2VhcmNoRGF0YS52YWx1ZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBjb25zdCBhZHZhbmNlZFNlYXJjaENvbmRpdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgIGZpZWxkX2lkOiBzZWxlY3RlZEZpZWxkLklOREVYRVJfRklFTERfSUQsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJSb3dEYXRhOiBmaWx0ZXJSb3dEYXRhLFxyXG4gICAgICAgICAgICAgICAgaXNBZHZhbmNlZFNlYXJjaDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIG1hcHBlcl9maWVsZF9pZDogc2VsZWN0ZWRGaWVsZC5NUE1fRklFTERfQ09ORklHX0lELFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcjogTVBNU2VhcmNoT3BlcmF0b3JzLkFORCxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IHNlbGVjdGVkT3BlcmF0b3IgPyBzZWxlY3RlZE9wZXJhdG9yLk9QRVJBVE9SX0lEIDogTVBNU2VhcmNoT3BlcmF0b3JzLklTLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBzZWxlY3RlZE9wZXJhdG9yID8gc2VsZWN0ZWRPcGVyYXRvci5OQU1FIDogTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUyxcclxuICAgICAgICAgICAgICAgIHNlYXJjaElkZW50aWZpZXI6IHNlYXJjaERhdGEuc2VhcmNoSWRlbnRpZmllcixcclxuICAgICAgICAgICAgICAgIHR5cGU6IHNlbGVjdGVkRmllbGQuREFUQV9UWVBFLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHNlYXJjaERhdGEudmFsdWUsXHJcbiAgICAgICAgICAgICAgICBtYXBwZXJfbmFtZTogc2VsZWN0ZWRGaWVsZC5NQVBQRVJfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZpZXc6IHNlYXJjaERhdGEudmlld1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBhZHZhbmNlZFNlYXJjaENvbmRpdGlvbkxpc3QucHVzaChhZHZhbmNlZFNlYXJjaENvbmRpdGlvbik7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGFkdmFuY2VkU2VhcmNoQ29uZGl0aW9uTGlzdDtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnRtcFZpZXdUb2dnbGVFdmVudFNlcnZpY2VTdWJzY3JpcHRpb24kID0gdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2Uub25WaWV3VG9nZ2xlU2VhcmNoLnN1YnNjcmliZSgoZGF0YTogU2VhcmNoRXZlbnRNb2RhbCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmFjdGlvbiA9PT0gU2VhcmNoRXZlbnRUeXBlcy5DTEVBUikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnRtcFNlYXJjaENoYW5nZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQgPSB0aGlzLnNlYXJjaENoYW5nZUV2ZW50U2VydmljZS5vblNlYXJjaFZpZXdDaGFuZ2Uuc3Vic2NyaWJlKCh2aWV3Q29uZmlnRGV0YWlsczogVmlld0NvbmZpZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaElkZW50aWZpZXIgPSB2aWV3Q29uZmlnRGV0YWlscyA/IHZpZXdDb25maWdEZXRhaWxzIDogbnVsbDtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hEYXRhU2VydmljZS5zZXRTZWFyY2hEYXRhKFtdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmluaXRDb25maWdzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLnVwZGF0ZU9uVmlld0NoYW5nZSh7IGFjdGlvbjogU2VhcmNoRXZlbnRUeXBlcy5DTEVBUiB9KTtcclxuICAgICAgICAgICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5xdWVyeVBhcmFtcy5zdWJzY3JpYmUocGFyYW1zID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICgocGFyYW1zLnZpZXdOYW1lICE9PSB0aGlzLnNlbGVjdGVkVmlldyB8fCBwYXJhbXMuZ3JvdXBCeUZpbHRlciAhPT0gdGhpcy5ncm91cEJ5RmlsdGVyIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlciAhPT0gdGhpcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIgfHwgcGFyYW1zLm1lbnVOYW1lICE9PSB0aGlzLm1lbnVOYW1lKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICEoKHBhcmFtcy5zZWFyY2hOYW1lID09PSB0aGlzLnNlYXJjaE5hbWUpIHx8IChwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lID09PSB0aGlzLnNhdmVkU2VhcmNoTmFtZSkgfHwgKHBhcmFtcy5hZHZTZWFyY2hEYXRhID09PSB0aGlzLmFkdlNlYXJjaERhdGEpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbXMudmlld05hbWUgJiYgdGhpcy5zZWxlY3RlZFZpZXcgJiYgdGhpcy5zZWxlY3RlZFZpZXcgIT09IHBhcmFtcy52aWV3TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmlldyA9IHBhcmFtcy52aWV3TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoKHBhcmFtcy5ncm91cEJ5RmlsdGVyICYmIHRoaXMuZ3JvdXBCeUZpbHRlciAmJiB0aGlzLmdyb3VwQnlGaWx0ZXIgIT09IHBhcmFtcy5ncm91cEJ5RmlsdGVyKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyICYmIHRoaXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyICYmIHRoaXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyICE9PSBwYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ3JvdXBCeUZpbHRlciA9IHBhcmFtcy5ncm91cEJ5RmlsdGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlVGFza0ZpbHRlciA9IHBhcmFtcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJTZWFyY2hWYWx1ZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmlldyA9IHBhcmFtcy52aWV3TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ncm91cEJ5RmlsdGVyID0gcGFyYW1zLmdyb3VwQnlGaWx0ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyID0gcGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmFtcy5zZWFyY2hOYW1lICYmIHRoaXMuc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoTmFtZSAhPT0gcGFyYW1zLnNlYXJjaE5hbWUgfHwgdGhpcy5tZW51TmFtZSAhPT0gcGFyYW1zLm1lbnVOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51TmFtZSA9IHBhcmFtcy5tZW51TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaE5hbWUgPSBwYXJhbXMuc2VhcmNoTmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaERhdGEgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaERhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleXdvcmQ6IHRoaXMuc2VhcmNoTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFfZmllbGRfaWQ6ICdNUE0uRklFTEQuQUxMX0tFWVdPUkQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoRGF0YSAmJiB0aGlzLnNlYXJjaERhdGFbMF0gJiYgdGhpcy5zZWFyY2hEYXRhWzBdLmtleXdvcmQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFJlY2VudFNlYXJjaGVzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWFyY2hlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaXNTZWFyY2hlZCA9IHNlYXJjaGVzLmZpbmQoc2VhcmNoRGF0YSA9PiBzZWFyY2hEYXRhID09PSB0aGlzLnNlYXJjaE5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1NlYXJjaGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2godGhpcy5zZWFyY2hOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0UmVjZW50U2VhcmNoKHRoaXMuc2VhcmNoTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoRGF0YSAmJiB0aGlzLnNlYXJjaERhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VhcmNoRGF0YVswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IHRoaXMuc2VhcmNoRGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gdGhpcy5zZWFyY2hEYXRhWzBdLmtleXdvcmQgfHwgdGhpcy5zZWFyY2hEYXRhWzBdLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaC5lbWl0KHRoaXMuc2VhcmNoRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXBhcmFtcy5zZWFyY2hOYW1lICYmIHRoaXMuc2VhcmNoTmFtZSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hOYW1lID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1zLnNhdmVkU2VhcmNoTmFtZSAmJiB0aGlzLnNlYXJjaElkZW50aWZpZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnNhdmVkU2VhcmNoTmFtZSAhPT0gcGFyYW1zLnNhdmVkU2VhcmNoTmFtZSB8fCB0aGlzLm1lbnVOYW1lICE9PSBwYXJhbXMubWVudU5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVOYW1lID0gcGFyYW1zLm1lbnVOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hOYW1lID0gcGFyYW1zLnNhdmVkU2VhcmNoTmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzU2F2ZWRTZWFyY2hUcmlnZ2VyID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1NhdmVkU2VhcmNoRGF0YS5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKCFwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lICYmIHRoaXMuc2F2ZWRTZWFyY2hOYW1lICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoTmFtZSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmFtcy5hZHZTZWFyY2hEYXRhICYmIHRoaXMuc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZTZWFyY2hEYXRhID0gcGFyYW1zLmFkdlNlYXJjaERhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhZHZhbmNlZFNlYXJjaERhdGEgPSB0aGlzLnN0YXRlU2VydmljZS5nZXRBZHZhbmNlZFNlYXJjaChwYXJhbXMubWVudU5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkdmFuY2VkU2VhcmNoRGF0YSAmJiBhZHZhbmNlZFNlYXJjaERhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhZHZhbmNlZFNlYXJjaERhdGEgIT09IHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWcgfHwgdGhpcy5tZW51TmFtZSAhPT0gcGFyYW1zLm1lbnVOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWVudU5hbWUgPSBwYXJhbXMubWVudU5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhZHZhbmNlZFNlYXJjaERhdGFbMF0uaXNBZHZhbmNlZFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IGFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoLmVtaXQoYWR2YW5jZWRTZWFyY2hEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaFRyaWdnZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2hEYXRhLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiAhdGhpcy5pc0FkdmFuY2VkU2VhcmNoVHJpZ2dlciAmJiB0aGlzLnNlYXJjaENvbmZpZy5hZHZhbmNlZFNlYXJjaENvbmZpZ3VyYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaFRyaWdnZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoQ29uZGl0aW9uID0gdGhpcy5mb3JtQWR2U2VhcmNoRGF0YSh0aGlzLmFkdlNlYXJjaERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2goc2VhcmNoQ29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWFyY2hDb25kaXRpb24gJiYgc2VhcmNoQ29uZGl0aW9uLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VhcmNoQ29uZGl0aW9uWzBdLmlzQWR2YW5jZWRTZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IHNlYXJjaENvbmRpdGlvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdChzZWFyY2hDb25kaXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXBhcmFtcy5hZHZTZWFyY2hEYXRhICYmIHRoaXMuYWR2U2VhcmNoRGF0YSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZTZWFyY2hEYXRhID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXBhcmFtcy5zZWFyY2hOYW1lICYmICFwYXJhbXMuYWR2U2VhcmNoRGF0YSAmJiAhcGFyYW1zLnNhdmVkU2VhcmNoTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51TmFtZSA9IHBhcmFtcy5tZW51TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5pc1NhdmVkU2VhcmNoRGF0YS5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2F2ZWRTZWFyY2hOYW1lICYmICF0aGlzLmlzU2F2ZWRTZWFyY2hUcmlnZ2VyICYmIHRoaXMuc2VhcmNoQ29uZmlnICYmIHRoaXMuc2VhcmNoQ29uZmlnLnNlYXJjaElkZW50aWZpZXIgJiYgdGhpcy5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllci5WSUVXKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hDb25maWdTZXJ2aWNlLmdldFNhdmVkU2VhcmNoZXNGb3JWaWV3QW5kQ3VycmVudFVzZXIodGhpcy5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllci5WSUVXKS5zdWJzY3JpYmUocmVzcG9uc2VEYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlRGF0YSAmJiByZXNwb25zZURhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaGVzID0gcmVzcG9uc2VEYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaGVzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2F2ZWRTZWFyY2hlcyAmJiAhdGhpcy5pc1NhdmVkU2VhcmNoVHJpZ2dlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5zYXZlZFNlYXJjaGVzLmZpbmQoZGF0YSA9PiBkYXRhLk5BTUUgPT09IHRoaXMuc2F2ZWRTZWFyY2hOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LlNFQVJDSF9EQVRBICYmIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3QgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24ubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uWzBdLnNhdmVkU2VhcmNoSWQgPSByZXN1bHRbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvblswXS5OQU1FID0gcmVzdWx0Lk5BTUU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFJlY2VudFNlYXJjaGVzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGlzU2VhcmNoZWQgPSBzZWFyY2hlcy5maW5kKHNlYXJjaERhdGEgPT4gc2VhcmNoRGF0YVswXS5zYXZlZFNlYXJjaElkID09PSByZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb25bMF0uc2F2ZWRTZWFyY2hJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghaXNTZWFyY2hlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2gocmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0UmVjZW50U2VhcmNoKHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNTYXZlZFNlYXJjaFRyaWdnZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb25bMF0uaXNBZHZhbmNlZFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gcmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gcmVzdWx0WzBdLmtleXdvcmQgfHwgcmVzdWx0WzBdLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaC5lbWl0KHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRtcFZpZXdUb2dnbGVFdmVudFNlcnZpY2VTdWJzY3JpcHRpb24kKSB7XHJcbiAgICAgICAgICAgIHRoaXMudG1wVmlld1RvZ2dsZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudG1wU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlU3Vic2NyaXB0aW9uJCkge1xyXG4gICAgICAgICAgICB0aGlzLnRtcFNlYXJjaENoYW5nZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19