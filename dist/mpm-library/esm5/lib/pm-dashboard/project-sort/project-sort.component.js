import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { SortConstants } from '../../shared/constants/sort.constants';
var ProjectSortComponent = /** @class */ (function () {
    function ProjectSortComponent(sharingService) {
        this.sharingService = sharingService;
        this.sortChange = new EventEmitter();
    }
    ProjectSortComponent.prototype.selectSorting = function () {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.sortChange.next(this.selectedOption);
    };
    ProjectSortComponent.prototype.onSelectionChange = function (event, fields) {
        var _this = this;
        if (event && event.currentTarget.value) {
            fields.map(function (option) {
                if (option.name === event.currentTarget.value) {
                    _this.selectedOption.option = option;
                }
            });
        }
        this.sortChange.next(this.selectedOption);
    };
    ProjectSortComponent.prototype.ngOnInit = function () {
        var appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] };
            }
            else {
                // this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] };
            }
        }
    };
    ProjectSortComponent.ctorParameters = function () { return [
        { type: SharingService }
    ]; };
    __decorate([
        Input()
    ], ProjectSortComponent.prototype, "sortOptions", void 0);
    __decorate([
        Input()
    ], ProjectSortComponent.prototype, "selectedOption", void 0);
    __decorate([
        Output()
    ], ProjectSortComponent.prototype, "sortChange", void 0);
    ProjectSortComponent = __decorate([
        Component({
            selector: 'mpm-project-sort',
            template: "<button class=\"sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption && selectedOption.option && selectedOption.option.displayName ? selectedOption.option.displayName :''}}\"\r\n    [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n    <span>Sort by: {{selectedOption && selectedOption.option && selectedOption.option.displayName ? selectedOption.option.displayName :''}}</span>\r\n    <mat-icon>arrow_drop_down</mat-icon>\r\n</button>\r\n<button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n    mat-icon-button>\r\n    <mat-icon *ngIf=\"selectedOption.sortType === 'desc'\">arrow_downward</mat-icon>\r\n    <mat-icon *ngIf=\"selectedOption.sortType === 'asc'\">arrow_upward</mat-icon>\r\n</button>\r\n<mat-menu #sortOrderListMenu=\"matMenu\">\r\n    <span *ngFor=\"let fieldOption of sortOptions\">\r\n        <button *ngIf=\"selectedOption.option.name !== fieldOption.name\" mat-menu-item\r\n            matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n            [value]=\"fieldOption.name\">\r\n            <span>{{fieldOption.displayName}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
            styles: [""]
        })
    ], ProjectSortComponent);
    return ProjectSortComponent;
}());
export { ProjectSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1zb3J0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LXNvcnQvcHJvamVjdC1zb3J0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBUXRFO0lBTUksOEJBQ1csY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBSC9CLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBSTNDLENBQUM7SUFFTCw0Q0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsS0FBSyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3JHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1FBQzVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsZ0RBQWlCLEdBQWpCLFVBQWtCLEtBQUssRUFBRSxNQUFNO1FBQS9CLGlCQVNDO1FBUkcsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7WUFDcEMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU07Z0JBQ2IsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFO29CQUMzQyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7aUJBQ3ZDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUNJLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQix5RUFBeUU7Z0JBQ3pFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7YUFDNUk7aUJBQU07Z0JBQ0gseURBQXlEO2dCQUN6RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7YUFDM0g7U0FDSjtJQUNMLENBQUM7O2dCQS9CMEIsY0FBYzs7SUFMaEM7UUFBUixLQUFLLEVBQUU7NkRBQWE7SUFDWjtRQUFSLEtBQUssRUFBRTtnRUFBZ0I7SUFDZDtRQUFULE1BQU0sRUFBRTs0REFBc0M7SUFKdEMsb0JBQW9CO1FBTmhDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsMnRDQUE0Qzs7U0FFL0MsQ0FBQztPQUVXLG9CQUFvQixDQXVDaEM7SUFBRCwyQkFBQztDQUFBLEFBdkNELElBdUNDO1NBdkNZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTb3J0Q29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zb3J0LmNvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXByb2plY3Qtc29ydCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcHJvamVjdC1zb3J0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Byb2plY3Qtc29ydC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvamVjdFNvcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIHNvcnRPcHRpb25zO1xyXG4gICAgQElucHV0KCkgc2VsZWN0ZWRPcHRpb247XHJcbiAgICBAT3V0cHV0KCkgc29ydENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgc2VsZWN0U29ydGluZygpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlID0gKHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPT09IFNvcnRDb25zdGFudHMuQVNDKSA/ICdkZXNjJyA6ICdhc2MnO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPSB0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlO1xyXG4gICAgICAgIHRoaXMuc29ydENoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2VsZWN0aW9uQ2hhbmdlKGV2ZW50LCBmaWVsZHMpIHtcclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZSkge1xyXG4gICAgICAgICAgICBmaWVsZHMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9uLm5hbWUgPT09IGV2ZW50LmN1cnJlbnRUYXJnZXQudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLm9wdGlvbiA9IG9wdGlvbjtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc29ydENoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGNvbnN0IGFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLnNlbGVjdGVkT3B0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNvcnRPcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHRoaXMuc29ydE9wdGlvbnNbMF0sIHNvcnRUeXBlOiBhcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB7fSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=