import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProjectService } from '../../project/shared/services/project.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { ProjectBulkCountService } from '../../shared/services/project-bulk-count.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../notification/notification.service';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
var ProjectCardComponent = /** @class */ (function () {
    function ProjectCardComponent(dialog, utilService, otmmMetadataService, fieldConfigService, viewConfigService, sharingService, projectBulkCountService, router, projectService, notificationService) {
        this.dialog = dialog;
        this.utilService = utilService;
        this.otmmMetadataService = otmmMetadataService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.sharingService = sharingService;
        this.projectBulkCountService = projectBulkCountService;
        this.router = router;
        this.projectService = projectService;
        this.notificationService = notificationService;
        this.viewProjectDetails = new EventEmitter();
        this.copyProject = new EventEmitter();
        this.openCommentsModule = new EventEmitter();
        this.selectedProjectCardCount = new EventEmitter();
        this.refreshHandler = new EventEmitter();
        this.customMetadataValues = [];
        this.priorityObj = {
            color: '',
            icon: '',
            tooltip: ''
        };
        this.mpmFieldConstants = MPMFieldConstants;
        this.displayFieldList = [];
        this.isCampaignDashboard = false;
        this.categoryLevel = MPM_LEVELS.PROJECT;
        this.affectedTask = [];
        this.copyToolTip = 'Click to copy project name';
        this.status = ['Defined', 'In Progress'];
        this.enableBulkEdit = false;
    }
    ProjectCardComponent.prototype.copiedText = function (element) {
        var _this = this;
        this.copyToolTip = 'Copied!';
        setTimeout(function () {
            if (_this.project.PROJECT_TYPE === 'PROJECT') {
                _this.copyToolTip = 'Click to copy project name';
            }
            else {
                _this.copyToolTip = 'Click to copy template name';
            }
        }, 2000);
    };
    ProjectCardComponent.prototype.openProjectDetails = function (project) {
        if (this.isCampaignDashboard) {
            var campaignId = this.getProperty(project, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID);
            if (campaignId) {
                campaignId = campaignId.split('.')[1];
            }
            this.viewProjectDetails.emit(campaignId);
        }
        else {
            var projectId = this.getProperty(project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
            if (projectId) {
                projectId = projectId.split('.')[1];
            }
            this.viewProjectDetails.emit(projectId);
        }
    };
    ProjectCardComponent.prototype.openComments = function () {
        var projectId = this.getProperty(this.project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
        if (projectId) {
            projectId = projectId.split('.')[1];
        }
        this.openCommentsModule.emit(projectId);
    };
    ProjectCardComponent.prototype.openCopyProject = function (project, isCopyTemplate) {
        if (!this.isTemplate) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        project.isCopyTemplate = isCopyTemplate ? true : false;
        this.copyProject.next(project);
    };
    ProjectCardComponent.prototype.converToLocalDate = function (data, propertyId) {
        return this.getProperty(data, propertyId);
    };
    ProjectCardComponent.prototype.getProperty = function (projectData, propertyId) {
        var _this = this;
        var currMPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, propertyId, this.categoryLevel);
        this.status.forEach(function (statusFilter) {
            if (_this.fieldConfigService.getFieldValueByDisplayColumn(currMPMField, projectData) === statusFilter) {
                _this.enableBulkEdit = true;
            }
        });
        return this.fieldConfigService.getFieldValueByDisplayColumn(currMPMField, projectData);
    };
    ProjectCardComponent.prototype.getPriority = function (projectData, propertyId) {
        var priorityObj = this.utilService.getPriorityIcon(this.getProperty(projectData, propertyId), MPM_LEVELS.PROJECT);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    };
    ProjectCardComponent.prototype.getMetadataTypeValue = function (metadata) {
        if (metadata.value && metadata.value.value) {
            return metadata.value.value.type ? metadata.value.value.type : null;
        }
        return null;
    };
    ProjectCardComponent.prototype.isSelected = function (projectDetails, event) {
        //console.log(this.projectBulkCountService.getProjectBulkCount(projectDetails));
        this.selectedProjectCardCount.next(this.projectBulkCountService.getProjectBulkCount(projectDetails));
    };
    ProjectCardComponent.prototype.deleteProject = function (project) {
        var _this = this;
        console.log("Project card deleteing Project");
        console.log(project);
        this.affectedTask = [];
        var tasks;
        var msg;
        var isCustomWorkflow = this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW);
        var name = this.isTemplate ? 'Template' : 'Project';
        if (isCustomWorkflow) {
            msg = "Are you sure you want to delete the " + name + " and reconfigure the workflow rule engine?";
        }
        else {
            msg = "Are you sure you want to delete the " + name + " and Exit?";
        }
        this.projectService.getTaskByProjectID(project.ID).subscribe(function (projectTaskResponse) {
            console.log(projectTaskResponse.GetTaskByProjectIDResponse.Task);
            if (projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task && !Array.isArray(projectTaskResponse.GetTaskByProjectIDResponse.Task)) {
                tasks = [projectTaskResponse.GetTaskByProjectIDResponse.Task];
            }
            else {
                tasks = projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task ? projectTaskResponse.GetTaskByProjectIDResponse.Task : '';
            }
            if (tasks && tasks.length > 0) {
                tasks.forEach(function (task) {
                    _this.affectedTask.push(task.NAME);
                });
            }
            // });
            var dialogRef = _this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: !_this.isTemplate ? _this.affectedTask : [],
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (response) {
                if (response) {
                    _this.projectService.deleteProject(project.ID, project.ITEM_ID, project.PROJECT_STATUS_ID).subscribe(function (deleteProjectResponse) {
                        _this.notificationService.success(name + ' has been deleted successfully');
                        //   this.refresh();
                        _this.refreshHandler.emit();
                    }, function (error) {
                        _this.notificationService.error('Delete ' + name + ' operation failed, try again later');
                    });
                }
            });
        });
    };
    ProjectCardComponent.prototype.ngOnInit = function () {
        var _this = this;
        var viewConfig;
        var userId = this.sharingService.getCurrentUserItemID();
        this.loggedInUserId = +userId;
        this.finalCompleted = StatusTypes.FINAL_COMPLETED;
        var projectConfig = this.sharingService.getProjectConfig();
        this.showCopyProject = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_COPY_PROJECT]);
        this.isTemplate = this.getProperty(this.project, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_TYPE) === 'TEMPLATE' ? true : false;
        if (this.router.url.includes('Campaigns') || this.dashboardMenuConfig.IS_CAMPAIGN_TYPE) {
            this.isCampaignDashboard = true;
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
        }
        if (this.dashboardMenuConfig.projectViewName) {
            viewConfig = this.dashboardMenuConfig.projectViewName; //this.sharingService.getViewConfigById(this.dashboardMenuConfig.projectViewId).VIEW;
        }
        else {
            //MPM_V3 -1799
            /*  viewConfig = this.isTemplate ?
                 SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE : SearchConfigConstants.SEARCH_NAME.PROJECT;
          */
            viewConfig = this.isCampaignDashboard ? SearchConfigConstants.SEARCH_NAME.CAMPAIGN : this.isTemplate ?
                SearchConfigConstants.SEARCH_NAME.PROJECT_TEMPLATE : SearchConfigConstants.SEARCH_NAME.PROJECT;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetailsParent) {
            var viewDetails = viewDetailsParent ? JSON.parse(JSON.stringify(viewDetailsParent)) : null;
            if (viewDetails && viewDetails.R_PM_CARD_VIEW_MPM_FIELDS) {
                viewDetails.R_PM_CARD_VIEW_MPM_FIELDS.forEach(function (mpmField) {
                    if (ProjectConstant.DEFAULT_CARD_FIELDS.indexOf(mpmField.MAPPER_NAME) < 0) {
                        mpmField.VALUE = _this.fieldConfigService.getFieldValueByDisplayColumn(mpmField, _this.project);
                        _this.displayFieldList.push(mpmField);
                    }
                });
            }
        });
    };
    ProjectCardComponent.ctorParameters = function () { return [
        { type: MatDialog },
        { type: UtilService },
        { type: OtmmMetadataService },
        { type: FieldConfigService },
        { type: ViewConfigService },
        { type: SharingService },
        { type: ProjectBulkCountService },
        { type: Router },
        { type: ProjectService },
        { type: NotificationService }
    ]; };
    __decorate([
        Input()
    ], ProjectCardComponent.prototype, "project", void 0);
    __decorate([
        Input()
    ], ProjectCardComponent.prototype, "dashboardMenuConfig", void 0);
    __decorate([
        Output()
    ], ProjectCardComponent.prototype, "viewProjectDetails", void 0);
    __decorate([
        Output()
    ], ProjectCardComponent.prototype, "copyProject", void 0);
    __decorate([
        Output()
    ], ProjectCardComponent.prototype, "openCommentsModule", void 0);
    __decorate([
        Output()
    ], ProjectCardComponent.prototype, "selectedProjectCardCount", void 0);
    __decorate([
        Output()
    ], ProjectCardComponent.prototype, "refreshHandler", void 0);
    ProjectCardComponent = __decorate([
        Component({
            selector: 'mpm-project-card',
            template: "<mat-card class=\"project-card\">\r\n    <div class=\"project-header\">\r\n        <div class=\"project-card-image\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-title-wrapper\">\r\n                    <!-- <mat-checkbox [disabled]=\"!enableBulkEdit\" color=\"primary\" class=\"select-project\" (click)=\"$event.stopPropagation()\"\r\n                    (change)=\"isSelected(project,$event)\"\r\n                    >\r\n                    </mat-checkbox> -->\r\n                    <mpm-name-icon\r\n                        [userName]=\"getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'\"\r\n                        [charCount]=\"2\" [color]=\"'#c0ca33'\"></mpm-name-icon>\r\n                    <span class=\"card-title\" (click)=\"openProjectDetails(project)\"\r\n                        matTooltip=\"{{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'}}\">\r\n                        {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME) || 'NA'}}\r\n                    </span>\r\n                </div>\r\n                <div class=\"flex-row-item action-button\">\r\n                    <mat-icon *ngIf=\"showCopyProject || !isTemplate\" color=\"primary\" matTooltip=\"Actions\" matSuffix\r\n                        [matMenuTriggerFor]=\"projectMenu\">\r\n                        more_vert\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- <div>\r\n            <mat-icon matTooltip=\"Priority: {{priorityObj.tooltip}}\" [style.color]=\"priorityObj.color\">\r\n                {{priorityObj.icon}}</mat-icon>\r\n\r\n            <mat-icon color=\"primary\" matTooltip=\"Actions\" matSuffix [matMenuTriggerFor]=\"projectMenu\">\r\n                more_vert\r\n            </mat-icon>\r\n        </div> -->\r\n    </div>\r\n    <div class=\"flex-col-item\">\r\n        <!-- <span> -->\r\n        <div class=\"flex-col-item\" *ngIf=\"!isTemplate\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-item\">\r\n                    <div class=\"project-percentage\"\r\n                        matTooltip=\"Project Completion - {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || 0}}%\">\r\n                        <span class=\"item-title\">\r\n                            {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || 0}}%</span>\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                            value=\"{{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS)}}\">\r\n                        </mat-progress-bar>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item card-item\" *ngIf=\"!isTemplate\"\r\n                    matTooltip=\"Completion Date: {{converToLocalDate(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DUE_DATE) || 'NA'}}\">\r\n                    <mat-icon>event</mat-icon>\r\n                    <span class=\"item-value\">{{getProperty(project,\r\n                        mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_DUE_DATE) || 'NA'}}</span>\r\n                </div>\r\n                <div class=\"flex-row-item card-item\">\r\n                    <mat-chip\r\n                        matTooltip=\"Status: {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || 'NA'}}\"\r\n                        class=\"status-chip\">\r\n                        {{getProperty(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || 'NA'}}\r\n                    </mat-chip>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                        matTooltip=\"Priority: {{getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).tooltip}}\"\r\n                        [style.color]=\"getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).color\">\r\n                        {{getPriority(project, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <span *ngIf=\"displayFieldList && displayFieldList.length > 0\">\r\n            <div class=\"flex-col-item\" *ngFor=\"let metadata of displayFieldList\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item card-item\"\r\n                        matTooltip=\"{{metadata.DISPLAY_NAME}} : {{metadata.VALUE || 'NA'}}\">\r\n                        <span class=\"item-title\">{{metadata.DISPLAY_NAME}}:</span>\r\n                        <span class=\"item-value\"\r\n                            [ngClass]=\"{'text-warp': metadata.DATA_TYPE === 'string'}\">{{metadata.VALUE || 'NA'}}</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </span>\r\n        <!-- </span> -->\r\n    </div>\r\n</mat-card>\r\n\r\n<mat-menu #projectMenu=\"matMenu\">\r\n    <button *ngIf=\"showCopyProject\" mat-menu-item\r\n        matTooltip=\"{{!isTemplate ? 'Copy Project' : 'Project from template'}}\" (click)=\"openCopyProject(project,false)\">\r\n        <mat-icon>insert_drive_file</mat-icon>\r\n        <span>{{!isTemplate ? 'Copy Project' : 'Project from template'}}</span>\r\n    </button>\r\n    <!--showCopyProject && !isCampaignDashboard && -->\r\n    <button *ngIf=\"isTemplate\" mat-menu-item\r\n        matTooltip=\"Copy Template\" (click)=\"openCopyProject(project,true)\">\r\n        <mat-icon>insert_drive_file</mat-icon>\r\n        <span>Copy Template</span>\r\n    </button>\r\n    <button mat-menu-item matTooltip=\"Comment\" (click)=\"openComments()\" *ngIf=\"!isTemplate\">\r\n        <mat-icon>\r\n            comment\r\n        </mat-icon>\r\n        <span>Comments</span>\r\n    </button>\r\n    <!--*ngIf=\"!isTemplate\"-->\r\n    <button mat-menu-item matTooltip=\"{{isTemplate ? 'Delete Template' : 'Delete Project'}}\" (click)=\"deleteProject(project)\" >\r\n        <mat-icon>delete</mat-icon>\r\n        <span>{{isTemplate ? 'Delete Template' : 'Delete Project'}}</span>\r\n    </button>\r\n</mat-menu>",
            styles: [".project-header{display:flex;flex-direction:row}.project-card-image{flex-grow:1;margin-bottom:5px;margin-left:5px}.project-card-image .action-button{justify-content:flex-end;align-items:center}.project-percentage{margin-bottom:10px;text-align:right;width:100%}mat-card{min-width:270px;min-height:200px;cursor:pointer}.card-title-wrapper{align-items:center}.card-title{display:inline-block;text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap;margin:5px;font-size:15px;min-width:20px;max-width:190px}.flex-row-item p{font-size:13px;margin:2px}.project-card{padding:12px}.project-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.card-heading{font-weight:590}span.item-title{margin-right:4px;font-size:14px;font-weight:600;line-height:16px}.card-item{margin:4px;font-size:14px;align-items:center;max-width:260px}.status-chip{padding:4px 8px;border-radius:4px;font-weight:400}.text-warp{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;max-width:56%}.select-project{padding:10px}"]
        })
    ], ProjectCardComponent);
    return ProjectCardComponent;
}());
export { ProjectCardComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1jYXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LWNhcmQvcHJvamVjdC1jYXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFFL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUUzRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUUvRSxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ25GLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3JILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVFyRDtJQTZCSSw4QkFDVyxNQUFpQixFQUNqQixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGlCQUFvQyxFQUNwQyxjQUE4QixFQUM5Qix1QkFBZ0QsRUFDaEQsTUFBYyxFQUNkLGNBQThCLEVBQzlCLG1CQUF3QztRQVR4QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQW5DekMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3QyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3Qyw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUtuRCx5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBRztZQUNWLEtBQUssRUFBRSxFQUFFO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixPQUFPLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRixzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0QyxxQkFBZ0IsR0FBb0IsRUFBRSxDQUFDO1FBQ3ZDLHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUU1QixrQkFBYSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFJbkMsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFpQmxCLGdCQUFXLEdBQUcsNEJBQTRCLENBQUM7UUFDM0MsV0FBTSxHQUFHLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3BDLG1CQUFjLEdBQUcsS0FBSyxDQUFDO0lBTG5CLENBQUM7SUFNTCx5Q0FBVSxHQUFWLFVBQVcsT0FBTztRQUFsQixpQkFTQztRQVJHLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1FBQzdCLFVBQVUsQ0FBQztZQUNQLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEtBQUssU0FBUyxFQUFFO2dCQUN6QyxLQUFJLENBQUMsV0FBVyxHQUFHLDRCQUE0QixDQUFDO2FBQ25EO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxXQUFXLEdBQUcsNkJBQTZCLENBQUM7YUFDcEQ7UUFDTCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsaURBQWtCLEdBQWxCLFVBQW1CLE9BQU87UUFDdEIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDMUIsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDeEcsSUFBSSxVQUFVLEVBQUU7Z0JBQ1osVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekM7WUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzVDO2FBQU07WUFDSCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDckcsSUFBSSxTQUFTLEVBQUU7Z0JBQ1gsU0FBUyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQzNDO0lBQ0wsQ0FBQztJQUVELDJDQUFZLEdBQVo7UUFDSSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzFHLElBQUksU0FBUyxFQUFFO1lBQ1gsU0FBUyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBRTVDLENBQUM7SUFFRCw4Q0FBZSxHQUFmLFVBQWdCLE9BQU8sRUFBQyxjQUFzQjtRQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQixPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUNoQzthQUFNO1lBQ0gsT0FBTyxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7U0FDakM7UUFDRCxPQUFPLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELGdEQUFpQixHQUFqQixVQUFrQixJQUFJLEVBQUUsVUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksV0FBVyxFQUFFLFVBQVU7UUFBbkMsaUJBU0M7UUFSRyxJQUFNLFlBQVksR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRXBJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsWUFBWTtZQUM1QixJQUFJLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLEtBQUssWUFBWSxFQUFFO2dCQUNsRyxLQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQzthQUM5QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQzNGLENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksV0FBVyxFQUFFLFVBQVU7UUFDL0IsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEVBQzFGLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV4QixPQUFPO1lBQ0gsS0FBSyxFQUFFLFdBQVcsQ0FBQyxLQUFLO1lBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtZQUN0QixPQUFPLEVBQUUsV0FBVyxDQUFDLE9BQU87U0FDL0IsQ0FBQztJQUNOLENBQUM7SUFDRCxtREFBb0IsR0FBcEIsVUFBcUIsUUFBUTtRQUN6QixJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDeEMsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ3ZFO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHlDQUFVLEdBQVYsVUFBVyxjQUFjLEVBQUUsS0FBSztRQUM1QixnRkFBZ0Y7UUFDaEYsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUN6RyxDQUFDO0lBRUQsNENBQWEsR0FBYixVQUFjLE9BQU87UUFBckIsaUJBZ0RDO1FBL0NHLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksS0FBSyxDQUFDO1FBQ1YsSUFBSSxHQUFHLENBQUM7UUFDUixJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3BGLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQ3JELElBQUksZ0JBQWdCLEVBQUU7WUFDbEIsR0FBRyxHQUFHLHNDQUFzQyxHQUFDLElBQUksR0FBQyw0Q0FBNEMsQ0FBQztTQUNsRzthQUFNO1lBQ0gsR0FBRyxHQUFHLHNDQUFzQyxHQUFDLElBQUksR0FBQyxZQUFZLENBQUM7U0FDbEU7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUI7WUFDNUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqRSxJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3JNLEtBQUssR0FBRyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNO2dCQUNILEtBQUssR0FBRyxtQkFBbUIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsSUFBSSxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ25NO1lBQ0QsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzNCLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUNkLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUNELE1BQU07WUFDTixJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsR0FBRztvQkFDWixJQUFJLEVBQUUsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUMvQyxZQUFZLEVBQUUsS0FBSztvQkFDbkIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ3RDLElBQUksUUFBUSxFQUFFO29CQUNWLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxxQkFBcUI7d0JBQ3JILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFDLGdDQUFnQyxDQUFDLENBQUM7d0JBQ3hFLG9CQUFvQjt3QkFDcEIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDL0IsQ0FBQyxFQUFFLFVBQUEsS0FBSzt3QkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBQyxJQUFJLEdBQUMsb0NBQW9DLENBQUMsQ0FBQztvQkFDeEYsQ0FBQyxDQUFDLENBQUM7aUJBQ047WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFRLEdBQVI7UUFBQSxpQkFtQ0M7UUFsQ0csSUFBSSxVQUFVLENBQUM7UUFDZixJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztRQUMxSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2SSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUU7WUFDcEYsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDNUM7UUFDRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLEVBQUU7WUFDMUMsVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsQ0FBQSxxRkFBcUY7U0FFOUk7YUFBTTtZQUNILGNBQWM7WUFDZDs7WUFFQTtZQUNBLFVBQVUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbEcscUJBQXFCLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1NBRXRHO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLGlCQUE2QjtZQUN2RyxJQUFNLFdBQVcsR0FBZSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3pHLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyx5QkFBeUIsRUFBRTtnQkFDdEQsV0FBVyxDQUFDLHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQWtCO29CQUM3RCxJQUFJLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDdkUsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDOUYsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDeEM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBekxrQixTQUFTO2dCQUNKLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUNwQixrQkFBa0I7Z0JBQ25CLGlCQUFpQjtnQkFDcEIsY0FBYztnQkFDTCx1QkFBdUI7Z0JBQ3hDLE1BQU07Z0JBQ0UsY0FBYztnQkFDVCxtQkFBbUI7O0lBckMxQztRQUFSLEtBQUssRUFBRTt5REFBZ0I7SUFDZjtRQUFSLEtBQUssRUFBRTtxRUFBcUI7SUFDbkI7UUFBVCxNQUFNLEVBQUU7b0VBQThDO0lBQzdDO1FBQVQsTUFBTSxFQUFFOzZEQUF1QztJQUN0QztRQUFULE1BQU0sRUFBRTtvRUFBOEM7SUFDN0M7UUFBVCxNQUFNLEVBQUU7MEVBQW9EO0lBQ25EO1FBQVQsTUFBTSxFQUFFO2dFQUEwQztJQVIxQyxvQkFBb0I7UUFOaEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGtCQUFrQjtZQUM1Qixnek1BQTRDOztTQUUvQyxDQUFDO09BRVcsb0JBQW9CLENBeU5oQztJQUFELDJCQUFDO0NBQUEsQUF6TkQsSUF5TkM7U0F6Tlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuXHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgT1RNTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1EYXRhVHlwZXMnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCwgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TZWFyY2hDb25maWdDb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFByb2plY3RCdWxrQ291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtYnVsay1jb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tcHJvamVjdC1jYXJkJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wcm9qZWN0LWNhcmQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcHJvamVjdC1jYXJkLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9qZWN0Q2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgcHVibGljIHByb2plY3Q7XHJcbiAgICBASW5wdXQoKSBkYXNoYm9hcmRNZW51Q29uZmlnO1xyXG4gICAgQE91dHB1dCgpIHZpZXdQcm9qZWN0RGV0YWlscyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNvcHlQcm9qZWN0ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3BlbkNvbW1lbnRzTW9kdWxlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWRQcm9qZWN0Q2FyZENvdW50ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVmcmVzaEhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBzaG93Q29weVByb2plY3Q6IGJvb2xlYW47XHJcbiAgICBjb2xvcjogJyNjMGNhMzMnO1xyXG4gICAgaXNUZW1wbGF0ZTogYm9vbGVhbjtcclxuICAgIGN1c3RvbU1ldGFkYXRhVmFsdWVzID0gW107XHJcbiAgICBwcmlvcml0eU9iaiA9IHtcclxuICAgICAgICBjb2xvcjogJycsXHJcbiAgICAgICAgaWNvbjogJycsXHJcbiAgICAgICAgdG9vbHRpcDogJydcclxuICAgIH07XHJcbiAgICBtcG1GaWVsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzO1xyXG4gICAgZGlzcGxheUZpZWxkTGlzdDogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcbiAgICBpc0NhbXBhaWduRGFzaGJvYXJkID0gZmFsc2U7XHJcblxyXG4gICAgY2F0ZWdvcnlMZXZlbCA9IE1QTV9MRVZFTFMuUFJPSkVDVDtcclxuXHJcbiAgICBsb2dnZWRJblVzZXJJZDtcclxuICAgIGZpbmFsQ29tcGxldGVkO1xyXG4gICAgYWZmZWN0ZWRUYXNrID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1NZXRhZGF0YVNlcnZpY2U6IE90bW1NZXRhZGF0YVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdEJ1bGtDb3VudFNlcnZpY2U6IFByb2plY3RCdWxrQ291bnRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcblxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBpc1Byb2plY3RPd25lcjtcclxuICAgIGNvcHlUb29sVGlwID0gJ0NsaWNrIHRvIGNvcHkgcHJvamVjdCBuYW1lJztcclxuICAgIHN0YXR1cyA9IFsnRGVmaW5lZCcsICdJbiBQcm9ncmVzcyddO1xyXG4gICAgZW5hYmxlQnVsa0VkaXQgPSBmYWxzZTtcclxuICAgIGNvcGllZFRleHQoZWxlbWVudCkge1xyXG4gICAgICAgIHRoaXMuY29weVRvb2xUaXAgPSAnQ29waWVkISc7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3QuUFJPSkVDVF9UWVBFID09PSAnUFJPSkVDVCcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29weVRvb2xUaXAgPSAnQ2xpY2sgdG8gY29weSBwcm9qZWN0IG5hbWUnO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb3B5VG9vbFRpcCA9ICdDbGljayB0byBjb3B5IHRlbXBsYXRlIG5hbWUnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMjAwMCk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblByb2plY3REZXRhaWxzKHByb2plY3QpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkKSB7XHJcbiAgICAgICAgICAgIGxldCBjYW1wYWlnbklkID0gdGhpcy5nZXRQcm9wZXJ0eShwcm9qZWN0LCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9DQU1QQUlHTl9GRUlMRFMuQ0FNUEFJR05fSVRFTV9JRCk7XHJcbiAgICAgICAgICAgIGlmIChjYW1wYWlnbklkKSB7XHJcbiAgICAgICAgICAgICAgICBjYW1wYWlnbklkID0gY2FtcGFpZ25JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQoY2FtcGFpZ25JZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IHByb2plY3RJZCA9IHRoaXMuZ2V0UHJvcGVydHkocHJvamVjdCwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lEKTtcclxuICAgICAgICAgICAgaWYgKHByb2plY3RJZCkge1xyXG4gICAgICAgICAgICAgICAgcHJvamVjdElkID0gcHJvamVjdElkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy52aWV3UHJvamVjdERldGFpbHMuZW1pdChwcm9qZWN0SWQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ29tbWVudHMoKSB7XHJcbiAgICAgICAgbGV0IHByb2plY3RJZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5wcm9qZWN0LCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0lURU1fSUQpO1xyXG4gICAgICAgIGlmIChwcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgcHJvamVjdElkID0gcHJvamVjdElkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub3BlbkNvbW1lbnRzTW9kdWxlLmVtaXQocHJvamVjdElkKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNvcHlQcm9qZWN0KHByb2plY3QsaXNDb3B5VGVtcGxhdGU6Ym9vbGVhbikge1xyXG4gICAgICAgIGlmICghdGhpcy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgIHByb2plY3QuaXNQcm9qZWN0VHlwZSA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJvamVjdC5pc1Byb2plY3RUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHByb2plY3QuaXNDb3B5VGVtcGxhdGUgPSBpc0NvcHlUZW1wbGF0ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB0aGlzLmNvcHlQcm9qZWN0Lm5leHQocHJvamVjdCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUoZGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5KGRhdGEsIHByb3BlcnR5SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KHByb2plY3REYXRhLCBwcm9wZXJ0eUlkKSB7XHJcbiAgICAgICAgY29uc3QgY3Vyck1QTUZpZWxkOiBNUE1GaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHByb3BlcnR5SWQsIHRoaXMuY2F0ZWdvcnlMZXZlbCk7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhdHVzLmZvckVhY2goc3RhdHVzRmlsdGVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oY3Vyck1QTUZpZWxkLCBwcm9qZWN0RGF0YSkgPT09IHN0YXR1c0ZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbmFibGVCdWxrRWRpdCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihjdXJyTVBNRmllbGQsIHByb2plY3REYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmlvcml0eShwcm9qZWN0RGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIGNvbnN0IHByaW9yaXR5T2JqID0gdGhpcy51dGlsU2VydmljZS5nZXRQcmlvcml0eUljb24odGhpcy5nZXRQcm9wZXJ0eShwcm9qZWN0RGF0YSwgcHJvcGVydHlJZCksXHJcbiAgICAgICAgICAgIE1QTV9MRVZFTFMuUFJPSkVDVCk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGNvbG9yOiBwcmlvcml0eU9iai5jb2xvcixcclxuICAgICAgICAgICAgaWNvbjogcHJpb3JpdHlPYmouaWNvbixcclxuICAgICAgICAgICAgdG9vbHRpcDogcHJpb3JpdHlPYmoudG9vbHRpcFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICBnZXRNZXRhZGF0YVR5cGVWYWx1ZShtZXRhZGF0YSk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKG1ldGFkYXRhLnZhbHVlICYmIG1ldGFkYXRhLnZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtZXRhZGF0YS52YWx1ZS52YWx1ZS50eXBlID8gbWV0YWRhdGEudmFsdWUudmFsdWUudHlwZSA6IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGlzU2VsZWN0ZWQocHJvamVjdERldGFpbHMsIGV2ZW50KSB7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyh0aGlzLnByb2plY3RCdWxrQ291bnRTZXJ2aWNlLmdldFByb2plY3RCdWxrQ291bnQocHJvamVjdERldGFpbHMpKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdENhcmRDb3VudC5uZXh0KHRoaXMucHJvamVjdEJ1bGtDb3VudFNlcnZpY2UuZ2V0UHJvamVjdEJ1bGtDb3VudChwcm9qZWN0RGV0YWlscykpO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVByb2plY3QocHJvamVjdCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiUHJvamVjdCBjYXJkIGRlbGV0ZWluZyBQcm9qZWN0XCIpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHByb2plY3QpO1xyXG4gICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrID0gW107XHJcbiAgICAgICAgbGV0IHRhc2tzO1xyXG4gICAgICAgIGxldCBtc2c7XHJcbiAgICAgICAgbGV0IGlzQ3VzdG9tV29ya2Zsb3cgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0LklTX0NVU1RPTV9XT1JLRkxPVyk7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9IHRoaXMuaXNUZW1wbGF0ZT8gJ1RlbXBsYXRlJyA6ICdQcm9qZWN0JztcclxuICAgICAgICBpZiAoaXNDdXN0b21Xb3JrZmxvdykge1xyXG4gICAgICAgICAgICBtc2cgPSBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIFwiK25hbWUrXCIgYW5kIHJlY29uZmlndXJlIHRoZSB3b3JrZmxvdyBydWxlIGVuZ2luZT9cIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtc2cgPSBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIFwiK25hbWUrXCIgYW5kIEV4aXQ/XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0VGFza0J5UHJvamVjdElEKHByb2plY3QuSUQpLnN1YnNjcmliZShwcm9qZWN0VGFza1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrKTtcclxuICAgICAgICAgICAgaWYgKHByb2plY3RUYXNrUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZSAmJiBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2sgJiYgIUFycmF5LmlzQXJyYXkocHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrKSkge1xyXG4gICAgICAgICAgICAgICAgdGFza3MgPSBbcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRhc2tzID0gcHJvamVjdFRhc2tSZXNwb25zZSAmJiBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlICYmIHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzayA/IHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzayA6ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0YXNrcyAmJiB0YXNrcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrcy5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrLnB1c2godGFzay5OQU1FKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogbXNnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICF0aGlzLmlzVGVtcGxhdGUgPyB0aGlzLmFmZmVjdGVkVGFzayA6IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5kZWxldGVQcm9qZWN0KHByb2plY3QuSUQsIHByb2plY3QuSVRFTV9JRCwgcHJvamVjdC5QUk9KRUNUX1NUQVRVU19JRCkuc3Vic2NyaWJlKGRlbGV0ZVByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKG5hbWUrJyBoYXMgYmVlbiBkZWxldGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2hIYW5kbGVyLmVtaXQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRGVsZXRlICcrbmFtZSsnIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBsZXQgdmlld0NvbmZpZztcclxuICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXJJZCA9ICt1c2VySWQ7XHJcbiAgICAgICAgdGhpcy5maW5hbENvbXBsZXRlZCA9IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRDtcclxuICAgICAgICBjb25zdCBwcm9qZWN0Q29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRQcm9qZWN0Q29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5zaG93Q29weVByb2plY3QgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5FTkFCTEVfQ09QWV9QUk9KRUNUXSk7XHJcbiAgICAgICAgdGhpcy5pc1RlbXBsYXRlID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnByb2plY3QsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfVFlQRSkgPT09ICdURU1QTEFURScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMucm91dGVyLnVybC5pbmNsdWRlcygnQ2FtcGFpZ25zJykgfHwgdGhpcy5kYXNoYm9hcmRNZW51Q29uZmlnLklTX0NBTVBBSUdOX1RZUEUpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0NhbXBhaWduRGFzaGJvYXJkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5DQU1QQUlHTjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuZGFzaGJvYXJkTWVudUNvbmZpZy5wcm9qZWN0Vmlld05hbWUpIHtcclxuICAgICAgICAgICAgdmlld0NvbmZpZyA9IHRoaXMuZGFzaGJvYXJkTWVudUNvbmZpZy5wcm9qZWN0Vmlld05hbWU7Ly90aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFZpZXdDb25maWdCeUlkKHRoaXMuZGFzaGJvYXJkTWVudUNvbmZpZy5wcm9qZWN0Vmlld0lkKS5WSUVXO1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvL01QTV9WMyAtMTc5OVxyXG4gICAgICAgICAgICAvKiAgdmlld0NvbmZpZyA9IHRoaXMuaXNUZW1wbGF0ZSA/XHJcbiAgICAgICAgICAgICAgICAgU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlBST0pFQ1RfVEVNUExBVEUgOiBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuUFJPSkVDVDtcclxuICAgICAgICAgICovXHJcbiAgICAgICAgICAgIHZpZXdDb25maWcgPSB0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgPyBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuQ0FNUEFJR04gOiB0aGlzLmlzVGVtcGxhdGUgP1xyXG4gICAgICAgICAgICAgICAgU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlBST0pFQ1RfVEVNUExBVEUgOiBTZWFyY2hDb25maWdDb25zdGFudHMuU0VBUkNIX05BTUUuUFJPSkVDVDtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzUGFyZW50OiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnID0gdmlld0RldGFpbHNQYXJlbnQgPyBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHZpZXdEZXRhaWxzUGFyZW50KSkgOiBudWxsO1xyXG4gICAgICAgICAgICBpZiAodmlld0RldGFpbHMgJiYgdmlld0RldGFpbHMuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUykge1xyXG4gICAgICAgICAgICAgICAgdmlld0RldGFpbHMuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUy5mb3JFYWNoKChtcG1GaWVsZDogTVBNRmllbGQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoUHJvamVjdENvbnN0YW50LkRFRkFVTFRfQ0FSRF9GSUVMRFMuaW5kZXhPZihtcG1GaWVsZC5NQVBQRVJfTkFNRSkgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1wbUZpZWxkLlZBTFVFID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihtcG1GaWVsZCwgdGhpcy5wcm9qZWN0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RmllbGRMaXN0LnB1c2gobXBtRmllbGQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==