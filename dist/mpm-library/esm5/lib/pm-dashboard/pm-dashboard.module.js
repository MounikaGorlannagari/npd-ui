import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { NameIconComponent } from '../shared/components/name-icon/name-icon.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectSortComponent } from './project-sort/project-sort.component';
import { MpmLibraryModule } from '../mpm-library.module';
var PMDashboardModule = /** @class */ (function () {
    function PMDashboardModule() {
    }
    PMDashboardModule = __decorate([
        NgModule({
            declarations: [
                NameIconComponent,
                ProjectCardComponent,
                ProjectListComponent,
                ProjectSortComponent
            ],
            imports: [
                CommonModule,
                MaterialModule,
                MpmLibraryModule
            ],
            exports: [
                ProjectCardComponent,
                ProjectListComponent,
                ProjectSortComponent,
                NameIconComponent
            ]
        })
    ], PMDashboardModule);
    return PMDashboardModule;
}());
export { PMDashboardModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG0tZGFzaGJvYXJkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wbS1kYXNoYm9hcmQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDdkYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDN0UsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDN0UsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDN0UsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFzQnpEO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixpQkFBaUI7UUFwQjdCLFFBQVEsQ0FBQztZQUNOLFlBQVksRUFBRTtnQkFDVixpQkFBaUI7Z0JBQ2pCLG9CQUFvQjtnQkFDcEIsb0JBQW9CO2dCQUNwQixvQkFBb0I7YUFDdkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsWUFBWTtnQkFDWixjQUFjO2dCQUNkLGdCQUFnQjthQUNuQjtZQUNELE9BQU8sRUFBRTtnQkFDTCxvQkFBb0I7Z0JBQ3BCLG9CQUFvQjtnQkFDcEIsb0JBQW9CO2dCQUNwQixpQkFBaUI7YUFDcEI7U0FDSixDQUFDO09BRVcsaUJBQWlCLENBQUk7SUFBRCx3QkFBQztDQUFBLEFBQWxDLElBQWtDO1NBQXJCLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgTmFtZUljb25Db21wb25lbnQgfSBmcm9tICcuLi9zaGFyZWQvY29tcG9uZW50cy9uYW1lLWljb24vbmFtZS1pY29uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFByb2plY3RDYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0LWNhcmQvcHJvamVjdC1jYXJkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFByb2plY3RMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0LWxpc3QvcHJvamVjdC1saXN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFByb2plY3RTb3J0Q29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0LXNvcnQvcHJvamVjdC1zb3J0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1wbUxpYnJhcnlNb2R1bGUgfSBmcm9tICcuLi9tcG0tbGlicmFyeS5tb2R1bGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIE5hbWVJY29uQ29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RDYXJkQ29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RMaXN0Q29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RTb3J0Q29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgICAgICBNcG1MaWJyYXJ5TW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIFByb2plY3RDYXJkQ29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RMaXN0Q29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RTb3J0Q29tcG9uZW50LFxyXG4gICAgICAgIE5hbWVJY29uQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUE1EYXNoYm9hcmRNb2R1bGUgeyB9XHJcbiJdfQ==