import { __decorate } from "tslib";
import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges, Renderer2 } from '@angular/core';
import { ProjectService } from '../../project/shared/services/project.service';
//import { RouteService } from '../../login/services/app.route.service
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DataTypeConstants } from '../../project/shared/constants/data-type.constants';
import { FormatToLocalePipe } from '../../shared/pipe/format-to-locale.pipe';
import { SelectionModel } from '@angular/cdk/collections';
import { NotificationService } from '../../notification/notification.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { TaskService } from '../../project/tasks/task.service';
import { LoaderService } from '../../loader/loader.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ProjectBulkCountService } from '../../shared/services/project-bulk-count.service';
import { Router } from '@angular/router';
import { ProjectTypes } from '../../mpm-utils/objects/ProjectTypes';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { GetPropertyPipe } from '../../shared/pipe/get-property.pipe';
var ProjectListComponent = /** @class */ (function () {
    function ProjectListComponent(dialog, projectService, taskService, utilService, notificationService, loaderService, formatToLocalePipe, otmmMetadataService, entityAppDefService, fieldConfigService, sharingService, projectBulkCountService, router, renderer, getPropertyPipe) {
        this.dialog = dialog;
        this.projectService = projectService;
        this.taskService = taskService;
        this.utilService = utilService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.otmmMetadataService = otmmMetadataService;
        this.entityAppDefService = entityAppDefService;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.projectBulkCountService = projectBulkCountService;
        this.router = router;
        this.renderer = renderer;
        this.getPropertyPipe = getPropertyPipe;
        this.enableGhostResize = false;
        this.viewProjectDetails = new EventEmitter();
        this.viewTaskDetails = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.copyProject = new EventEmitter();
        this.openMilestoneDialog = new EventEmitter();
        this.openCommentsModule = new EventEmitter();
        /*  @Output() selectedProjectCount = new EventEmitter<any>();
         @Output() selectedProjectData = new EventEmitter<any>();
         @Output() selectedProjectDatas = new EventEmitter<any>(); */
        this.selectedProjectsCallBackHandler = new EventEmitter();
        // @Output() selectAllProjectsCount = new EventEmitter<boolean>();
        this.openCampaignInProjectList = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.refreshEmitter = new EventEmitter();
        this.selection = new SelectionModel(true, []);
        this.projectDataDataSource = new MatTableDataSource([]);
        this.metaDataDataTypes = DataTypeConstants;
        this.displayableMetadataFields = [];
        this.displayableCustomMetadataFields = [];
        this.displayColumns = [];
        this.mpmFieldConstants = MPMFieldConstants;
        this.position = {
            'value': ''
        };
        this.status = ['Defined', 'In Progress'];
        this.enableBulkEdit = false;
        this.selectedProjects = new SelectionModel(true, []);
        this.isCampaignDashboard = false;
        this.milestoneTasks = '';
        this.categoryLevel = MPM_LEVELS.PROJECT;
        this.isRefresh = true;
        this.affectedTask = [];
    }
    /** Whether the number of selected elements matches the total number of rows. */
    ProjectListComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.projectDataDataSource.data.length;
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    /*  masterToggle(data, value?: any) {
         
         let projectId;
         if (this.isAllSelected()) {
             this.projectDataDataSource.data.forEach(row => {
 
                 projectId = this.projectBulkCountService.getProjectBulkCount(row);
             });
             this.selection.clear();
         } else {
             this.projectDataDataSource.data.forEach(row => {
                 this.selection.select(row);
                 // projectId.push(row.ID);
                 projectId = this.projectBulkCountService.getProjectBulkCount(row);
             });
             this.selectedProjectData.next(this.projectDataDataSource.data);
         }
         console.log(projectId.length);
         this.selectedProjectCount.next(projectId.length)
         
     } */
    ProjectListComponent.prototype.getCompletedMilestonesByProjectId = function (projectId) {
        var _this = this;
        this.projectService.getCompletedMilestonesByProjectId(projectId).subscribe(function (response) {
            _this.loaderService.show();
            if (response && response.tuple && response.tuple) {
                if (!Array.isArray(response.tuple)) {
                    response.tuple = [response.tuple];
                }
                var milestoneList = void 0;
                milestoneList = response.tuple;
                if (milestoneList && milestoneList.length && milestoneList.length > 0) {
                    milestoneList.map(function (milestone) {
                        if (milestone && milestone.old && milestone.old.Task) {
                            _this.milestoneTasks = _this.milestoneTasks.concat(milestone.old.Task.TaskName);
                            _this.milestoneTasks = _this.milestoneTasks.concat(",");
                        }
                    });
                }
                /*if (this.milestoneTasks.length <= 0) {
                    this.milestoneTasks = '';
                }
                else {
                    this.milestoneTasks = '';
                }*/
                _this.milestoneTasks = '';
            }
            else {
                _this.milestoneTasks = '';
            }
        }, function () {
        });
    };
    ProjectListComponent.prototype.masterToggle = function () {
        this.selectAllProjectChecked ? this.checkAllProjects() : this.uncheckAllProjects();
    };
    ProjectListComponent.prototype.checkAllProjects = function () {
        var _this = this;
        this.projectDataDataSource.data.forEach(function (row) {
            row.selected = true;
            _this.selectedProjects.select(row);
        });
        this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    };
    ProjectListComponent.prototype.uncheckAllProjects = function () {
        var _this = this;
        this.projectDataDataSource.data.forEach(function (row) {
            row.selected = false;
            _this.selectedProjects.deselect(row);
        });
        this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
    };
    ProjectListComponent.prototype.checkProject = function (projectData, event) {
        if (event.checked) {
            projectData.selected = true;
            this.selectedProjects.select(projectData);
            this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
        }
        else {
            this.selectedProjects.deselect(projectData);
            this.selectedProjectsCallBackHandler.emit(this.selectedProjects.selected);
        }
    };
    ProjectListComponent.prototype.getProperty = function (displayColumn, projectData) {
        var _this = this;
        this.status.forEach(function (statusFilter) {
            if (_this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData) === statusFilter) {
                _this.enableBulkEdit = true;
            }
        });
        // if(displayColumn.MAPPER_NAME === this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS){
        //     this.getCompletedMilestonesByProjectId(projectData.ID);
        //     return (this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData).concat(this.milestoneTasks));
        // }
        // else{
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData);
        // }
    };
    ProjectListComponent.prototype.getPropertyByMapper = function (projectData, propertyId) {
        var currMPMField = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, propertyId, this.categoryLevel);
        return this.getProperty(currMPMField, projectData);
    };
    ProjectListComponent.prototype.converToLocalDate = function (data, propertyId) {
        return this.formatToLocalePipe.transform(this.projectService.converToLocalDate(data, propertyId));
    };
    ProjectListComponent.prototype.getPriority = function (projectData, displayColum) {
        var priorityObj = this.utilService.getPriorityIcon(this.getProperty(displayColum, projectData), MPM_LEVELS.PROJECT);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    };
    ProjectListComponent.prototype.editProject = function (event, project, isCopyTemplate) {
        event.stopPropagation();
        if (this.projectConfig.IS_PROJECT_TYPE) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        project.isCopyTemplate = isCopyTemplate ? true : false;
        this.copyProject.next(project);
    };
    ProjectListComponent.prototype.openMilestone = function (event, project) {
        event.stopPropagation();
        if (this.projectConfig.IS_PROJECT_TYPE) {
            project.isProjectType = true;
        }
        else {
            project.isProjectType = false;
        }
        this.openMilestoneDialog.next(project);
    };
    ProjectListComponent.prototype.editTask = function (eventData) {
        this.editTaskHandler.emit(eventData);
    };
    ProjectListComponent.prototype.openComment = function (event, projectDetails) {
        event.stopPropagation();
        var projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ID, this.categoryLevel), projectDetails);
        this.openCommentsModule.emit(projectId);
    };
    ProjectListComponent.prototype.getAllActiveTasksForProject = function (project, isUpdate) {
        var _this = this;
        if (!project.taskList || isUpdate) {
            project.taskList = [];
            project.taskTotalCount = 0;
            this.projectStatusType = project.PROJECT_STATUS_TYPE;
            this.projectCancelledStatus = this.projectStatusType === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
            this.isOnHoldProject = project.IS_ON_HOLD === 'true' ? true : false;
            this.projectConfig.currentProjectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ID, this.categoryLevel), project);
            this.projectConfig.currentProjectItemId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel), project);
            var parameters = {
                projectId: this.projectConfig.currentProjectItemId,
                skip: 0,
                top: 100
            };
            this.loaderService.show();
            this.taskService.getAllActiveTasks(parameters, this.projectConfig.projectViewName)
                .subscribe(function (response) {
                _this.loaderService.hide();
                project.taskList = response.data;
                project.taskTotalCount = response.cursor.total_records;
            }, function (error) {
                _this.loaderService.hide();
                _this.notificationService.error('Something went wrong while getting active tasks for project');
            });
        }
    };
    ProjectListComponent.prototype.refreshTask = function (project) {
        this.getAllActiveTasksForProject(project, true);
    };
    ProjectListComponent.prototype.openTaskDetails = function (eventData) {
        this.viewTaskDetails.next(eventData);
    };
    ProjectListComponent.prototype.expandRow = function (event, row) {
        event.stopPropagation();
        if (row.isExpanded) {
            row.isExpanded = false;
        }
        else {
            row.isExpanded = true;
            this.getAllActiveTasksForProject(row);
        }
    };
    ProjectListComponent.prototype.openProjectDetails = function (project, columnName, enableCampaign, isCampaignManager, isPmView) {
        if (columnName === MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID && enableCampaign && isCampaignManager && isPmView) { /* isCampaignManager */
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
            var campaignId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID, this.categoryLevel), project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                if (!this.isCampaignDashboard) {
                    this.openCampaignInProjectList.emit(true);
                }
                this.viewProjectDetails.emit(campaignId.split('.').length > 1 ? campaignId.split('.')[1] : campaignId.split('.')[0]);
            } /* else {
                this.notificationService.error('Invalid campaign Id');
            } */
        }
        else if (this.isCampaignDashboard) {
            // this.categoryLevel = MPM_LEVELS.CAMPAIGN;
            var campaignId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID, this.categoryLevel), project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                /* if (!this.isCampaignDashboard) {
                    this.openCampaignInProjectList.emit(true);
                } */
                this.viewProjectDetails.emit(campaignId.split('.')[1]);
            }
            else {
                this.notificationService.error('Invalid campaign Id');
            }
        }
        else {
            this.categoryLevel = MPM_LEVELS.PROJECT;
            var projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel), project);
            if (projectId && projectId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(projectId.split('.')[1]);
            }
            else {
                this.notificationService.error('Invalid project Id');
            }
        }
        /* if (this.isCampaignDashboard ) {
            const campaignId =  this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ITEM_ID, this.categoryLevel)
                , project);
            if (campaignId && campaignId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(campaignId.split('.')[1]);
            } else {
                this.notificationService.error('Invalid campaign Id');
            }
        } else {
            this.categoryLevel = MPM_LEVELS.PROJECT;
            const projectId = this.getProperty(this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, this.mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID, this.categoryLevel)
                , project);
            if (projectId && projectId !== 'NA') {
                this.loaderService.show();
                this.viewProjectDetails.emit(projectId.split('.')[1]);
            } else {
                this.notificationService.error('Invalid project Id');
            }
        } */
    };
    ProjectListComponent.prototype.refreshData = function () {
        this.displayableMetadataFields = this.projectConfig.projectViewConfig ?
            this.utilService.getDisplayOrderData(this.displayableFields, this.projectConfig.projectViewConfig.listFieldOrder) : [];
        // this.displayableMetadataFields = this.projectConfig.projectViewConfig ?
        //     this.utilService.getDisplayOrderData(this.projectConfig.projectViewConfig.R_PM_LIST_VIEW_MPM_FIELDS, this.projectConfig.projectViewConfig.LIST_FIELD_ORDER) : [];
        this.displayColumns = this.displayableMetadataFields.map(function (column) { return column.INDEXER_FIELD_ID; });
        this.isSelect = this.isCampaign ? !this.isCampaign : (!this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE);
        /* if ((this.isCampaign && !this.isCampaign) || (this.isCampaignDashboard && !this.isCampaignDashboard || !this.projectConfig.IS_CAMPAIGN_TYPE)) {
         */
        if (this.isSelect) {
            this.displayColumns.unshift('Select');
        }
        if (this.projectConfig.IS_PROJECT_TYPE) {
            this.displayColumns.unshift('Expand');
        }
        //Copy template ticket-shree tharani
        /*if (this.showCopyProject || this.projectConfig.IS_PROJECT_TYPE) {
            this.displayColumns.push('Actions');
        }*/
        this.displayColumns.push('Actions');
        this.projectDataDataSource.data = [];
        this.projectDataDataSource.data = this.projectConfig.projectData;
        this.sort.active = this.projectConfig.selectedSortOption.option.name;
        this.sort.direction = this.projectConfig.selectedSortOption.sortType.toLowerCase();
        this.projectDataDataSource.sort = this.sort;
    };
    ProjectListComponent.prototype.tableDrop = function (event) {
        var _this = this;
        var count = 0;
        if (this.displayColumns.find(function (column) { return column === 'Select'; })) {
            count = count + 1;
        }
        if (this.displayColumns.find(function (column) { return column === 'Expand'; })) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        var orderedFields = [];
        this.displayColumns.forEach(function (column) {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(_this.displayableFields.find(function (displayableField) { return displayableField.INDEXER_FIELD_ID === column; }));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    };
    ProjectListComponent.prototype.onResizeMouseDown = function (event, column) {
        event.stopPropagation();
        event.preventDefault();
        var start = event.target;
        var pressed = true;
        var startX = event.x;
        var startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    };
    ProjectListComponent.prototype.initResizableColumns = function (start, pressed, startX, startWidth, column) {
        var _this = this;
        this.renderer.listen('body', 'mousemove', function (event) {
            if (pressed) {
                var width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', function (event) {
            if (pressed) {
                pressed = false;
                _this.resizeDisplayableFields.next();
            }
        });
    };
    ProjectListComponent.prototype.deleteProject = function (event, row) {
        var _this = this;
        console.log(event);
        console.log(row);
        var msg;
        this.affectedTask = [];
        var tasks;
        var isCustomWorkflow = this.utilService.getBooleanValue(row.IS_CUSTOM_WORKFLOW);
        var name = this.isTemplate ? 'Template' : 'Project';
        if (isCustomWorkflow) {
            msg = "Are you sure you want to delete the " + name + " and reconfigure the workflow rule engine?";
        }
        else {
            msg = "Are you sure you want to delete the " + name + " and Exit?";
        }
        this.projectService.getTaskByProjectID(row.ID).subscribe(function (projectTaskResponse) {
            console.log(projectTaskResponse.GetTaskByProjectIDResponse.Task);
            if (projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task && !Array.isArray(projectTaskResponse.GetTaskByProjectIDResponse.Task)) {
                tasks = [projectTaskResponse.GetTaskByProjectIDResponse.Task];
            }
            else {
                tasks = projectTaskResponse && projectTaskResponse.GetTaskByProjectIDResponse && projectTaskResponse.GetTaskByProjectIDResponse.Task ? projectTaskResponse.GetTaskByProjectIDResponse.Task : '';
            }
            if (tasks && tasks.length > 0) {
                tasks.forEach(function (task) {
                    _this.affectedTask.push(task.NAME);
                });
            }
            // });
            var dialogRef = _this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: _this.projectConfig.IS_PROJECT_TYPE ? _this.affectedTask : [],
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (response) {
                if (response) {
                    _this.projectService.deleteProject(row.ID, row.ITEM_ID, row.PROJECT_STATUS_ID).subscribe(function (deleteProjectResponse) {
                        _this.notificationService.success(name + ' has been deleted successfully');
                        //this.refreshData();
                        _this.refreshEmitter.next();
                    }, function (error) {
                        _this.notificationService.error('Delete ' + name + ' operation failed, try again later');
                    });
                }
            });
        });
    };
    ProjectListComponent.prototype.ngOnChanges = function (changes) {
        if (changes.displayableFields && !changes.displayableFields.firstChange &&
            (JSON.stringify(changes.displayableFields.currentValue) !== JSON.stringify(changes.displayableFields.previousValue))) {
            this.refreshData();
        }
        this.refreshData();
    };
    ProjectListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.position.value = 'below';
        var userId = this.sharingService.getCurrentUserItemID();
        this.loggedInUserId = +userId;
        this.finalCompleted = StatusTypes.FINAL_COMPLETED;
        var projectConfig = this.sharingService.getProjectConfig();
        var appConfig = this.sharingService.getAppConfig();
        this.showCopyProject = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_COPY_PROJECT]);
        this.enableCampaign = this.utilService.getBooleanValue(appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.isCampaignManager = (this.sharingService.getCampaignRole() && this.sharingService.getCampaignRole().length > 0) ? true : false;
        this.IsPMView = (this.projectType === ProjectTypes.CAMPAIGN || this.router.url.includes('Campaigns') || this.router.url.includes('campaign')) ? true : false;
        this.isTemplate = this.projectType === ProjectTypes.TEMPLATE || this.router.url.includes('Templates') ? true : false;
        if (this.router.url.includes('Campaigns') || this.projectConfig.IS_CAMPAIGN_TYPE) {
            this.isCampaignDashboard = true;
            this.categoryLevel = MPM_LEVELS.CAMPAIGN;
        }
        if (this.projectConfig.metaDataFields && this.projectConfig.projectData) {
            this.refreshData();
        }
        if (!this.projectConfig.projectData) {
            this.projectConfig.projectData = [];
        }
        if (!this.projectConfig.metaDataFields) {
            this.projectConfig.metaDataFields = [];
        }
        if (this.sort) {
            this.sort.disableClear = true;
            this.sort.sortChange.subscribe(function () {
                if (_this.sort.active && _this.sort.direction) {
                    var selectedSortList = {
                        sortType: _this.sort.direction.toUpperCase(),
                        option: {
                            name: _this.sort.active,
                            displayName: ''
                        }
                    };
                    _this.sortChange.emit(selectedSortList);
                }
            });
        }
        /*  if (this.selectAllDeliverables) {
             this.selectAllDeliverables.subscribe((data: boolean) => {
                 if (data) {
                     this.checked = true;
                 } else {
                     this.checked = false;
                 }
                 this.checkDeliverable(this.asset);
             });
         } */
    };
    ProjectListComponent.ctorParameters = function () { return [
        { type: MatDialog },
        { type: ProjectService },
        { type: TaskService },
        { type: UtilService },
        { type: NotificationService },
        { type: LoaderService },
        { type: FormatToLocalePipe },
        { type: OtmmMetadataService },
        { type: EntityAppDefService },
        { type: FieldConfigService },
        { type: SharingService },
        { type: ProjectBulkCountService },
        { type: Router },
        { type: Renderer2 },
        { type: GetPropertyPipe }
    ]; };
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "projectConfig", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "isCampaign", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "projectType", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "displayableFields", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "facetExpansionState", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "enableGhostResize", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "viewProjectDetails", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "viewTaskDetails", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "editTaskHandler", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "sortChange", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "copyProject", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "openMilestoneDialog", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "openCommentsModule", void 0);
    __decorate([
        Input()
    ], ProjectListComponent.prototype, "userPreferenceFreezeCount", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "selectedProjectsCallBackHandler", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "openCampaignInProjectList", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "orderedDisplayableFields", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "resizeDisplayableFields", void 0);
    __decorate([
        Output()
    ], ProjectListComponent.prototype, "refreshEmitter", void 0);
    __decorate([
        ViewChild(MatSort, { static: true })
    ], ProjectListComponent.prototype, "sort", void 0);
    ProjectListComponent = __decorate([
        Component({
            selector: 'mpm-project-list',
            template: "<table class=\"project-list\" mat-table [dataSource]=\"projectDataDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Expand\" *ngIf=\"projectConfig.IS_PROJECT_TYPE\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\" (click)=\"expandRow($event, row)\">\r\n            <mat-icon *ngIf=\"!row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_right</mat-icon>\r\n            <mat-icon *ngIf=\"row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_down</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllProjectChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"selection.hasValue() && isAllSelected()\" [indeterminate]=\"selection.hasValue() && !isAllSelected()\">\r\n                <!-- [(ngModel)]=\"selectAllProjectChecked\" -->\r\n            </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"row.selected\" [disabled]=\"!enableBulkEdit\" (click)=\"$event.stopPropagation()\" (change)=\"$event ? checkProject(row,$event) : null\" [checked]=\"selection.isSelected(row)\">\r\n            </mat-checkbox>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields; index as i\" [sticky]=\"i < userPreferenceFreezeCount\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':column.DATA_TYPE === 'string'}\">\r\n            <span>\r\n                <div *ngIf=\"column.MAPPER_NAME!== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS && column.MAPPER_NAME !== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY && column.MAPPER_NAME !== mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS\"\r\n                    class=\"flex-card\" (click)=\"(openProjectDetails(row,column.MAPPER_NAME,enableCampaign , isCampaignManager , !IsPMView))\">\r\n                    <a matTooltip=\"{{getProperty(column, row)}}\" [matTooltipPosition]=\"position.value\"\r\n                        matTooltipClass=\"mattooltipclass\" [ngClass]=\"{'campaignLink': getProperty(column, row) != 'NA' && (column.MAPPER_NAME === mpmFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_ID) && enableCampaign && isCampaignManager && !IsPMView}\">{{getProperty(column, row) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">\r\n                        {{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS\">\r\n                    <span matTooltip=\"{{getProperty(column, row) || 'NA'}}% {{milestoneTasks}}\">{{getProperty(column, row) || 'NA'}}%\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                            value=\"{{getProperty(column, row) || 'NA'}}\" class=\"\">\r\n                        </mat-progress-bar>\r\n                    </span>\r\n            </div>\r\n            <div *ngIf=\"column.MAPPER_NAME === mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS\">\r\n                <mat-chip-list>\r\n                    <mat-chip class=\"status-chip\">{{getProperty(column, row) || 'NA'}}</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div class=\"min-width-action flex-card\">\r\n                <button *ngIf=\"showCopyProject\" mat-icon-button matTooltip=\"{{projectConfig.IS_PROJECT_TYPE ? 'Copy Project': 'Project from template'}}\" (click)=\"editProject($event, row, false)\">\r\n                    <mat-icon>insert_drive_file</mat-icon>\r\n                </button>\r\n                <!--Copy Template\r\n                (!isCampaignDashboard || !projectConfig.IS_CAMPAIGN_TYPE) && !this.projectConfig.IS_PROJECT_TYPE\r\n                isTemplate-change-->\r\n                <button *ngIf=\"isTemplate\" mat-icon-button matTooltip=\"Copy Template\" (click)=\"editProject($event, row, true)\">\r\n                    <mat-icon>insert_drive_file</mat-icon>\r\n                </button>\r\n\r\n                <button mat-icon-button matTooltip=\"Comments\" *ngIf=\"projectConfig.IS_PROJECT_TYPE\" (click)=\"openComment($event, row)\">\r\n                    <mat-icon>comment</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"expandedDetail\">\r\n        <td class=\"project-detail\" mat-cell *matCellDef=\"let row\" [attr.colspan]=\"displayColumns.length\">\r\n            <div class=\"project-element-detail\" [@detailExpand]=\"row.isExpanded ? 'expanded' : 'collapsed'\">\r\n                <div class=\"task-info data\" *ngIf=\"row.taskTotalCount !== 0\">\r\n                    <div class=\"task-heading\">Current Active Tasks:</div>\r\n                </div>\r\n                <div class=\"project-element-diagram\" *ngFor=\"let task of row.taskList\">\r\n                    <mpm-task-card-view [isTemplate]=\"!projectConfig.IS_PROJECT_TYPE\" [taskData]=\"task\" [projectStatusType]=\"projectStatusType\" [taskConfig]=\"projectConfig\" (refreshTask)=\"refreshTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\" (editTaskHandler)=\"editTask($event)\"\r\n                        [projectOwner]=\"getPropertyByMapper(row, mpmFieldConstants.MPM_PROJECT_FEILDS.PROJECT_OWNER_ID)\">\r\n                    </mpm-task-card-view>\r\n                </div>\r\n                <div class=\"task-info no-data\" *ngIf=\"row.taskTotalCount === 0\">\r\n                    <div class=\"task-heading\">No Active Task</div>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"project-element-row\">\r\n        <!-- (click)=\"openProjectDetails(row,isCampaignManager)\" -->\r\n    </tr>\r\n\r\n    <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\" class=\"project-detail-row\"></tr>\r\n</table>\r\n\r\n<div *ngIf=\"projectConfig.totalProjectCount == 0\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No project is available.</button>\r\n</div>",
            animations: [trigger('detailExpand', [
                    state('collapsed', style({ height: '0px' })),
                    state('expanded', style({ height: '*' })),
                    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.42, 0.0, 0.58, 1.0)')),
                ])] /* ,
            encapsulation: ViewEncapsulation.None */,
            styles: ["table.project-list{width:100%}table.project-list tr{cursor:pointer}table.project-list tr .mat-checkbox{padding:0 8px}table.project-list tr th:last-child{padding-left:12px}table.project-list tr td{min-width:8em}table.project-list tr td a.mat-button{padding:0;text-align:left}table.project-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.project-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}table.project-list tr td mat-progress-bar{max-width:7em}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.project-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.project-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.project-detail{padding:0!important}tr.project-detail-row{height:0}.project-element-row td{border-bottom-width:0}.project-element-detail{overflow:hidden;display:flex;flex-wrap:wrap;cursor:auto!important}.project-element-diagram{min-width:80px;padding:8px;font-weight:lighter;margin:8px 0}.task-info{min-width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex}.task-info.data{justify-content:left}.task-info.no-data{justify-content:center}td.wrap-text-custom span>div{max-width:12em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:6em}.mattooltipclass{background-color:red;margin-left:70px}"]
        })
    ], ProjectListComponent);
    return ProjectListComponent;
}());
export { ProjectListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3BtLWRhc2hib2FyZC9wcm9qZWN0LWxpc3QvcHJvamVjdC1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9ILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMvRSxzRUFBc0U7QUFDdEUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRXBFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFaEYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQzNGLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ25GLE9BQU8sRUFBZSxlQUFlLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN0RSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDakUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFFckgsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQWF0RTtJQTRESSw4QkFDVyxNQUFpQixFQUNqQixjQUE4QixFQUM5QixXQUF3QixFQUN4QixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsYUFBNEIsRUFDNUIsa0JBQXNDLEVBQ3RDLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLHVCQUFnRCxFQUNoRCxNQUFjLEVBQ2QsUUFBbUIsRUFDbkIsZUFBZ0M7UUFkaEMsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQXJFbEMsc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ2xDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0Msb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQzVDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQVMsQ0FBQztRQUM1QyxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNyQyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5Qyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRXZEOztxRUFFNkQ7UUFDbkQsb0NBQStCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRSxrRUFBa0U7UUFDeEQsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQztRQUN4RCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3JELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7UUFDcEQsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBSXJELGNBQVMsR0FBRyxJQUFJLGNBQWMsQ0FBTSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDOUMsMEJBQXFCLEdBQUcsSUFBSSxrQkFBa0IsQ0FBTSxFQUFFLENBQUMsQ0FBQztRQUN4RCxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0Qyw4QkFBeUIsR0FBb0IsRUFBRSxDQUFDO1FBQ2hELG9DQUErQixHQUFHLEVBQUUsQ0FBQztRQUVyQyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUV0QyxhQUFRLEdBQUc7WUFDUCxPQUFPLEVBQUUsRUFBRTtTQUNkLENBQUM7UUFDRixXQUFNLEdBQUcsQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDcEMsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIscUJBQWdCLEdBQUcsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBR2hELHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUc1QixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQixrQkFBYSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7UUFPbkMsY0FBUyxHQUFHLElBQUksQ0FBQztRQUdqQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQWlCZCxDQUFDO0lBR0wsZ0ZBQWdGO0lBQ2hGLDRDQUFhLEdBQWI7UUFDSSxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDbkQsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdkQsT0FBTyxXQUFXLEtBQUssT0FBTyxDQUFDO0lBQ25DLENBQUM7SUFFRCxnRkFBZ0Y7SUFDaEY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1NBb0JLO0lBRUwsZ0VBQWlDLEdBQWpDLFVBQWtDLFNBQVM7UUFBM0MsaUJBZ0NDO1FBOUJHLElBQUksQ0FBQyxjQUFjLENBQUMsaUNBQWlDLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUMvRSxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNoQyxRQUFRLENBQUMsS0FBSyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyQztnQkFDRCxJQUFJLGFBQWEsU0FBQSxDQUFDO2dCQUNsQixhQUFhLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztnQkFDL0IsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDbkUsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLFNBQVM7d0JBQ3ZCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxHQUFHLElBQUksU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUU7NEJBQ2xELEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQzlFLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7eUJBQ3pEO29CQUVMLENBQUMsQ0FBQyxDQUFDO2lCQUVOO2dCQUNEOzs7OzttQkFLRztnQkFDSCxLQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzthQUM1QjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzthQUM1QjtRQUNMLENBQUMsRUFBRTtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUN2RixDQUFDO0lBRUQsK0NBQWdCLEdBQWhCO1FBQUEsaUJBTUM7UUFMRyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7WUFDdkMsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDcEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCxpREFBa0IsR0FBbEI7UUFBQSxpQkFNQztRQUxHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztZQUN2QyxHQUFHLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNyQixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDOUUsQ0FBQztJQUVELDJDQUFZLEdBQVosVUFBYSxXQUFXLEVBQUUsS0FBSztRQUMzQixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDZixXQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUM1QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzdFO2FBQU07WUFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzdFO0lBRUwsQ0FBQztJQUNELDBDQUFXLEdBQVgsVUFBWSxhQUF1QixFQUFFLFdBQWdCO1FBQXJELGlCQWNDO1FBYkcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxZQUFZO1lBQzVCLElBQUksS0FBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsS0FBSyxZQUFZLEVBQUU7Z0JBQ25HLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzlCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxnR0FBZ0c7UUFDaEcsOERBQThEO1FBQzlELDZIQUE2SDtRQUM3SCxJQUFJO1FBQ0osUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN4RixJQUFJO0lBRVIsQ0FBQztJQUVELGtEQUFtQixHQUFuQixVQUFvQixXQUFXLEVBQUUsVUFBVTtRQUN2QyxJQUFNLFlBQVksR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUNELGdEQUFpQixHQUFqQixVQUFrQixJQUFJLEVBQUUsVUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN0RyxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLFdBQWdCLEVBQUUsWUFBc0I7UUFDaEQsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLEVBQzVGLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV4QixPQUFPO1lBQ0gsS0FBSyxFQUFFLFdBQVcsQ0FBQyxLQUFLO1lBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtZQUN0QixPQUFPLEVBQUUsV0FBVyxDQUFDLE9BQU87U0FDL0IsQ0FBQztJQUNOLENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksS0FBSyxFQUFFLE9BQU8sRUFBQyxjQUFzQjtRQUM3QyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRTtZQUNwQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUNoQzthQUFNO1lBQ0gsT0FBTyxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7U0FDakM7UUFDRCxPQUFPLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELDRDQUFhLEdBQWIsVUFBYyxLQUFLLEVBQUUsT0FBTztRQUN4QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsRUFBRTtZQUNwQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUNoQzthQUFNO1lBQ0gsT0FBTyxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCx1Q0FBUSxHQUFSLFVBQVMsU0FBUztRQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksS0FBSyxFQUFFLGNBQWM7UUFDN0IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQzNLLGNBQWMsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELDBEQUEyQixHQUEzQixVQUE0QixPQUFZLEVBQUUsUUFBa0I7UUFBNUQsaUJBOEJDO1FBN0JHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxJQUFJLFFBQVEsRUFBRTtZQUMvQixPQUFPLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1lBQ3JELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEtBQUssZUFBZSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwSCxJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwRSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQ2xELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUM1SSxPQUFPLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FDdEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQ2pKLE9BQU8sQ0FBQyxDQUFDO1lBQ2YsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CO2dCQUNsRCxJQUFJLEVBQUUsQ0FBQztnQkFDUCxHQUFHLEVBQUUsR0FBRzthQUNYLENBQUM7WUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBRTFCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDO2lCQUM3RSxTQUFTLENBQUMsVUFBQyxRQUF3QjtnQkFDaEMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsT0FBTyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUNqQyxPQUFPLENBQUMsY0FBYyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDO1lBQzNELENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO1lBQ2xHLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLE9BQVk7UUFDcEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsOENBQWUsR0FBZixVQUFnQixTQUFTO1FBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCx3Q0FBUyxHQUFULFVBQVUsS0FBSyxFQUFFLEdBQUc7UUFDaEIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLElBQUksR0FBRyxDQUFDLFVBQVUsRUFBRTtZQUNoQixHQUFHLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztTQUMxQjthQUFNO1lBQ0gsR0FBRyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3pDO0lBQ0wsQ0FBQztJQUVELGlEQUFrQixHQUFsQixVQUFtQixPQUFPLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxRQUFRO1FBQy9FLElBQUksVUFBVSxLQUFLLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLFdBQVcsSUFBSSxjQUFjLElBQUksaUJBQWlCLElBQUksUUFBUSxFQUFFLEVBQUMsdUJBQXVCO1lBQzdJLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUN6QyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUM5SyxPQUFPLENBQUMsQ0FBQztZQUNmLElBQUksVUFBVSxJQUFJLFVBQVUsS0FBSyxJQUFJLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7b0JBQzNCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzdDO2dCQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEgsQ0FBQzs7Z0JBRUU7U0FDUDthQUNJLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQy9CLDRDQUE0QztZQUM1QyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQ25MLE9BQU8sQ0FBQyxDQUFDO1lBQ2YsSUFBSSxVQUFVLElBQUksVUFBVSxLQUFLLElBQUksRUFBRTtnQkFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUI7O29CQUVJO2dCQUNKLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzFEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQzthQUN6RDtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDeEMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsRUFDaEwsT0FBTyxDQUFDLENBQUM7WUFDZixJQUFJLFNBQVMsSUFBSSxTQUFTLEtBQUssSUFBSSxFQUFFO2dCQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN6RDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7YUFDeEQ7U0FDSjtRQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBbUJJO0lBQ1IsQ0FBQztJQUVELDBDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMzSCwwRUFBMEU7UUFDMUUsd0tBQXdLO1FBQ3hLLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLEdBQUcsQ0FBQyxVQUFDLE1BQWdCLElBQUssT0FBQSxNQUFNLENBQUMsZ0JBQWdCLEVBQXZCLENBQXVCLENBQUMsQ0FBQztRQUN4RyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN6SDtXQUNHO1FBQ0gsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3pDO1FBQ0Qsb0NBQW9DO1FBQ3BDOztXQUVHO1FBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUNqRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDckUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkYsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ2hELENBQUM7SUFFRCx3Q0FBUyxHQUFULFVBQVUsS0FBNEI7UUFBdEMsaUJBZ0JDO1FBZkcsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sS0FBSyxRQUFRLEVBQW5CLENBQW1CLENBQUMsRUFBRTtZQUN6RCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUNyQjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLEtBQUssUUFBUSxFQUFuQixDQUFtQixDQUFDLEVBQUU7WUFDekQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUM7U0FDckI7UUFDRCxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzlGLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07WUFDOUIsSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxTQUFTLENBQUMsRUFBRTtnQkFDdkUsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQUEsZ0JBQWdCLElBQUksT0FBQSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQTVDLENBQTRDLENBQUMsQ0FBQyxDQUFDO2FBQ3JIO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxnREFBaUIsR0FBakIsVUFBa0IsS0FBSyxFQUFFLE1BQU07UUFDM0IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzNCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLElBQU0sVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFRCxtREFBb0IsR0FBcEIsVUFBcUIsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFBL0QsaUJBYUM7UUFaRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLFVBQUMsS0FBSztZQUM1QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFNLEtBQUssR0FBRyxVQUFVLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxVQUFDLEtBQUs7WUFDMUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQWEsR0FBYixVQUFjLEtBQUssRUFBRSxHQUFHO1FBQXhCLGlCQWlEQztRQWhERyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxHQUFHLENBQUM7UUFDUixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLEtBQUssQ0FBQztRQUNWLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDaEYsSUFBTSxJQUFJLEdBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDdkQsSUFBSSxnQkFBZ0IsRUFBRTtZQUNsQixHQUFHLEdBQUcsc0NBQXNDLEdBQUMsSUFBSSxHQUFDLDRDQUE0QyxDQUFDO1NBQ2xHO2FBQU07WUFDSCxHQUFHLEdBQUcsc0NBQXNDLEdBQUMsSUFBSSxHQUFDLFlBQVksQ0FBQztTQUNsRTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLG1CQUFtQjtZQUN4RSxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pFLElBQUksbUJBQW1CLElBQUksbUJBQW1CLENBQUMsMEJBQTBCLElBQUksbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDck0sS0FBSyxHQUFHLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakU7aUJBQU07Z0JBQ0gsS0FBSyxHQUFHLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixJQUFJLG1CQUFtQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDbk07WUFDRCxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDM0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ2QsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QyxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsTUFBTTtZQUNOLElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO2dCQUMzRCxLQUFLLEVBQUUsS0FBSztnQkFDWixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsSUFBSSxFQUFFO29CQUNGLE9BQU8sRUFBRSxHQUFHO29CQUNaLElBQUksRUFBRSxLQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDakUsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNyQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUN0QyxJQUFJLFFBQVEsRUFBRTtvQkFDVixLQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEscUJBQXFCO3dCQUN6RyxLQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLElBQUksR0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO3dCQUN4RSxxQkFBcUI7d0JBQ3JCLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQy9CLENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUMsSUFBSSxHQUFDLG9DQUFvQyxDQUFDLENBQUM7b0JBQ3hGLENBQUMsQ0FBQyxDQUFDO2lCQUVOO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsaUJBQWlCLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsV0FBVztZQUNuRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7WUFDdEgsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFBO0lBQ3RCLENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQUEsaUJBbURDO1FBbERHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUM5QixJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFBO1FBQ3pJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBQzdILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3BJLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxLQUFLLFlBQVksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM3SixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLEtBQUssWUFBWSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3RILElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUU7WUFDOUUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDNUM7UUFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQ3JFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN0QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRTtZQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUU7WUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1gsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztnQkFDM0IsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDekMsSUFBTSxnQkFBZ0IsR0FBRzt3QkFDckIsUUFBUSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRTt3QkFDM0MsTUFBTSxFQUFFOzRCQUNKLElBQUksRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07NEJBQ3RCLFdBQVcsRUFBRSxFQUFFO3lCQUNsQjtxQkFDSixDQUFDO29CQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7aUJBQzFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUVEOzs7Ozs7Ozs7YUFTSztJQUNULENBQUM7O2dCQXhka0IsU0FBUztnQkFDRCxjQUFjO2dCQUNqQixXQUFXO2dCQUNYLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUN6QixhQUFhO2dCQUNSLGtCQUFrQjtnQkFDakIsbUJBQW1CO2dCQUNuQixtQkFBbUI7Z0JBQ3BCLGtCQUFrQjtnQkFDdEIsY0FBYztnQkFDTCx1QkFBdUI7Z0JBQ3hDLE1BQU07Z0JBQ0osU0FBUztnQkFDRixlQUFlOztJQTFFbEM7UUFBUixLQUFLLEVBQUU7K0RBQWU7SUFDZDtRQUFSLEtBQUssRUFBRTs0REFBWTtJQUNYO1FBQVIsS0FBSyxFQUFFOzZEQUFhO0lBQ1o7UUFBUixLQUFLLEVBQUU7bUVBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO3FFQUFxQjtJQUNwQjtRQUFSLEtBQUssRUFBRTttRUFBb0M7SUFDbEM7UUFBVCxNQUFNLEVBQUU7b0VBQThDO0lBQzdDO1FBQVQsTUFBTSxFQUFFO2lFQUE2QztJQUM1QztRQUFULE1BQU0sRUFBRTtpRUFBNkM7SUFDNUM7UUFBVCxNQUFNLEVBQUU7NERBQXNDO0lBQ3JDO1FBQVQsTUFBTSxFQUFFOzZEQUF1QztJQUN0QztRQUFULE1BQU0sRUFBRTtxRUFBK0M7SUFDOUM7UUFBVCxNQUFNLEVBQUU7b0VBQThDO0lBQzlDO1FBQVIsS0FBSyxFQUFFOzJFQUEyQjtJQUl6QjtRQUFULE1BQU0sRUFBRTtpRkFBMkQ7SUFFMUQ7UUFBVCxNQUFNLEVBQUU7MkVBQXlEO0lBQ3hEO1FBQVQsTUFBTSxFQUFFOzBFQUFzRDtJQUNyRDtRQUFULE1BQU0sRUFBRTt5RUFBcUQ7SUFDcEQ7UUFBVCxNQUFNLEVBQUU7Z0VBQTRDO0lBRWY7UUFBckMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztzREFBZTtJQXpCM0Msb0JBQW9CO1FBWmhDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsMmhPQUE0QztZQUU1QyxVQUFVLEVBQUUsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFO29CQUNqQyxLQUFLLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO29CQUM1QyxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO29CQUN6QyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7aUJBQzVGLENBQUMsQ0FBQyxDQUFBO29EQUNxQzs7U0FDM0MsQ0FBQztPQUVXLG9CQUFvQixDQXNoQmhDO0lBQUQsMkJBQUM7Q0FBQSxBQXRoQkQsSUFzaEJDO1NBdGhCWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIFZpZXdDaGlsZCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcywgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcclxuLy9pbXBvcnQgeyBSb3V0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2dpbi9zZXJ2aWNlcy9hcHAucm91dGUuc2VydmljZVxyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCc7XHJcbmltcG9ydCB7IE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYmxlJztcclxuaW1wb3J0IHsgRGF0YVR5cGVDb25zdGFudHMgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9jb25zdGFudHMvZGF0YS10eXBlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IHRyaWdnZXIsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgYW5pbWF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3QvdGFza3MvdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCwgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RCdWxrQ291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtYnVsay1jb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgUHJvamVjdFR5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUHJvamVjdFR5cGVzJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQ2RrRHJhZ0Ryb3AsIG1vdmVJdGVtSW5BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzVHlwZSc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgR2V0UHJvcGVydHlQaXBlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3BpcGUvZ2V0LXByb3BlcnR5LnBpcGUnO1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXByb2plY3QtbGlzdCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcHJvamVjdC1saXN0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Byb2plY3QtbGlzdC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgYW5pbWF0aW9uczogW3RyaWdnZXIoJ2RldGFpbEV4cGFuZCcsIFtcclxuICAgICAgICBzdGF0ZSgnY29sbGFwc2VkJywgc3R5bGUoeyBoZWlnaHQ6ICcwcHgnIH0pKSxcclxuICAgICAgICBzdGF0ZSgnZXhwYW5kZWQnLCBzdHlsZSh7IGhlaWdodDogJyonIH0pKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCdleHBhbmRlZCA8PT4gY29sbGFwc2VkJywgYW5pbWF0ZSgnMjI1bXMgY3ViaWMtYmV6aWVyKDAuNDIsIDAuMCwgMC41OCwgMS4wKScpKSxcclxuICAgIF0pXS8qICxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUgKi9cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQcm9qZWN0TGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuICAgIEBJbnB1dCgpIHByb2plY3RDb25maWc7XHJcbiAgICBASW5wdXQoKSBpc0NhbXBhaWduO1xyXG4gICAgQElucHV0KCkgcHJvamVjdFR5cGU7XHJcbiAgICBASW5wdXQoKSBkaXNwbGF5YWJsZUZpZWxkcztcclxuICAgIEBJbnB1dCgpIGZhY2V0RXhwYW5zaW9uU3RhdGU7XHJcbiAgICBASW5wdXQoKSBlbmFibGVHaG9zdFJlc2l6ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgQE91dHB1dCgpIHZpZXdQcm9qZWN0RGV0YWlscyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHZpZXdUYXNrRGV0YWlscyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgICBAT3V0cHV0KCkgZWRpdFRhc2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICAgIEBPdXRwdXQoKSBzb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY29weVByb2plY3QgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBvcGVuTWlsZXN0b25lRGlhbG9nID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3BlbkNvbW1lbnRzTW9kdWxlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBASW5wdXQoKSB1c2VyUHJlZmVyZW5jZUZyZWV6ZUNvdW50O1xyXG4gICAgLyogIEBPdXRwdXQoKSBzZWxlY3RlZFByb2plY3RDb3VudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgIEBPdXRwdXQoKSBzZWxlY3RlZFByb2plY3REYXRhID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICAgQE91dHB1dCgpIHNlbGVjdGVkUHJvamVjdERhdGFzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7ICovXHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWRQcm9qZWN0c0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgLy8gQE91dHB1dCgpIHNlbGVjdEFsbFByb2plY3RzQ291bnQgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3BlbkNhbXBhaWduSW5Qcm9qZWN0TGlzdCA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcclxuICAgIEBPdXRwdXQoKSBvcmRlcmVkRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gICAgQE91dHB1dCgpIHJlc2l6ZURpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoRW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcblxyXG4gICAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBzb3J0OiBNYXRTb3J0O1xyXG5cclxuICAgIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxhbnk+KHRydWUsIFtdKTtcclxuICAgIHByb2plY3REYXRhRGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2U8YW55PihbXSk7XHJcbiAgICBtZXRhRGF0YURhdGFUeXBlcyA9IERhdGFUeXBlQ29uc3RhbnRzO1xyXG4gICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkczogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcbiAgICBkaXNwbGF5YWJsZUN1c3RvbU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICBpc1Byb2plY3RPd25lcjtcclxuICAgIGRpc3BsYXlDb2x1bW5zID0gW107XHJcbiAgICBtcG1GaWVsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzO1xyXG4gICAgc2hvd0NvcHlQcm9qZWN0OiBib29sZWFuO1xyXG4gICAgcG9zaXRpb24gPSB7XHJcbiAgICAgICAgJ3ZhbHVlJzogJydcclxuICAgIH07XHJcbiAgICBzdGF0dXMgPSBbJ0RlZmluZWQnLCAnSW4gUHJvZ3Jlc3MnXTtcclxuICAgIGVuYWJsZUJ1bGtFZGl0ID0gZmFsc2U7XHJcbiAgICBzZWxlY3RlZFByb2plY3RzID0gbmV3IFNlbGVjdGlvbk1vZGVsKHRydWUsIFtdKTtcclxuICAgIHNlbGVjdEFsbFByb2plY3RDaGVja2VkOiBib29sZWFuO1xyXG4gICAgY2hlY2tlZDogYm9vbGVhbjtcclxuICAgIGlzQ2FtcGFpZ25EYXNoYm9hcmQgPSBmYWxzZTtcclxuICAgIGlzU2VsZWN0O1xyXG4gICAgaXNDYW1wYWlnbk1hbmFnZXI7XHJcbiAgICBtaWxlc3RvbmVUYXNrcyA9ICcnO1xyXG5cclxuICAgIGNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLlBST0pFQ1Q7XHJcbiAgICBlbmFibGVDYW1wYWlnbjtcclxuICAgIElzUE1WaWV3O1xyXG4gICAgaXNUZW1wbGF0ZTtcclxuICAgIHByb2plY3RTdGF0dXNUeXBlO1xyXG4gICAgcHJvamVjdENhbmNlbGxlZFN0YXR1cztcclxuICAgIGlzT25Ib2xkUHJvamVjdDtcclxuICAgIGlzUmVmcmVzaCA9IHRydWU7XHJcbiAgICBsb2dnZWRJblVzZXJJZDtcclxuICAgIGZpbmFsQ29tcGxldGVkO1xyXG4gICAgYWZmZWN0ZWRUYXNrID0gW107XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZvcm1hdFRvTG9jYWxlUGlwZTogRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0QnVsa0NvdW50U2VydmljZTogUHJvamVjdEJ1bGtDb3VudFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgICAgIHB1YmxpYyBnZXRQcm9wZXJ0eVBpcGU6IEdldFByb3BlcnR5UGlwZVxyXG4gICAgKSB7IH1cclxuXHJcblxyXG4gICAgLyoqIFdoZXRoZXIgdGhlIG51bWJlciBvZiBzZWxlY3RlZCBlbGVtZW50cyBtYXRjaGVzIHRoZSB0b3RhbCBudW1iZXIgb2Ygcm93cy4gKi9cclxuICAgIGlzQWxsU2VsZWN0ZWQoKSB7XHJcbiAgICAgICAgY29uc3QgbnVtU2VsZWN0ZWQgPSB0aGlzLnNlbGVjdGlvbi5zZWxlY3RlZC5sZW5ndGg7XHJcbiAgICAgICAgY29uc3QgbnVtUm93cyA9IHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEubGVuZ3RoO1xyXG4gICAgICAgIHJldHVybiBudW1TZWxlY3RlZCA9PT0gbnVtUm93cztcclxuICAgIH1cclxuXHJcbiAgICAvKiogU2VsZWN0cyBhbGwgcm93cyBpZiB0aGV5IGFyZSBub3QgYWxsIHNlbGVjdGVkOyBvdGhlcndpc2UgY2xlYXIgc2VsZWN0aW9uLiAqL1xyXG4gICAgLyogIG1hc3RlclRvZ2dsZShkYXRhLCB2YWx1ZT86IGFueSkge1xyXG4gICAgICAgICBcclxuICAgICAgICAgbGV0IHByb2plY3RJZDtcclxuICAgICAgICAgaWYgKHRoaXMuaXNBbGxTZWxlY3RlZCgpKSB7XHJcbiAgICAgICAgICAgICB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5kYXRhLmZvckVhY2gocm93ID0+IHtcclxuIFxyXG4gICAgICAgICAgICAgICAgIHByb2plY3RJZCA9IHRoaXMucHJvamVjdEJ1bGtDb3VudFNlcnZpY2UuZ2V0UHJvamVjdEJ1bGtDb3VudChyb3cpO1xyXG4gICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICB0aGlzLnNlbGVjdGlvbi5jbGVhcigpO1xyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgdGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3Rpb24uc2VsZWN0KHJvdyk7XHJcbiAgICAgICAgICAgICAgICAgLy8gcHJvamVjdElkLnB1c2gocm93LklEKTtcclxuICAgICAgICAgICAgICAgICBwcm9qZWN0SWQgPSB0aGlzLnByb2plY3RCdWxrQ291bnRTZXJ2aWNlLmdldFByb2plY3RCdWxrQ291bnQocm93KTtcclxuICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3REYXRhLm5leHQodGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2UuZGF0YSk7XHJcbiAgICAgICAgIH0gXHJcbiAgICAgICAgIGNvbnNvbGUubG9nKHByb2plY3RJZC5sZW5ndGgpO1xyXG4gICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdENvdW50Lm5leHQocHJvamVjdElkLmxlbmd0aClcclxuICAgICAgICAgXHJcbiAgICAgfSAqL1xyXG5cclxuICAgIGdldENvbXBsZXRlZE1pbGVzdG9uZXNCeVByb2plY3RJZChwcm9qZWN0SWQpIHtcclxuXHJcbiAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRDb21wbGV0ZWRNaWxlc3RvbmVzQnlQcm9qZWN0SWQocHJvamVjdElkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudHVwbGUgJiYgcmVzcG9uc2UudHVwbGUpIHtcclxuICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS50dXBsZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS50dXBsZSA9IFtyZXNwb25zZS50dXBsZV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBsZXQgbWlsZXN0b25lTGlzdDtcclxuICAgICAgICAgICAgICAgIG1pbGVzdG9uZUxpc3QgPSByZXNwb25zZS50dXBsZTtcclxuICAgICAgICAgICAgICAgIGlmIChtaWxlc3RvbmVMaXN0ICYmIG1pbGVzdG9uZUxpc3QubGVuZ3RoICYmIG1pbGVzdG9uZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1pbGVzdG9uZUxpc3QubWFwKG1pbGVzdG9uZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtaWxlc3RvbmUgJiYgbWlsZXN0b25lLm9sZCAmJiBtaWxlc3RvbmUub2xkLlRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWlsZXN0b25lVGFza3MgPSB0aGlzLm1pbGVzdG9uZVRhc2tzLmNvbmNhdChtaWxlc3RvbmUub2xkLlRhc2suVGFza05hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5taWxlc3RvbmVUYXNrcyA9IHRoaXMubWlsZXN0b25lVGFza3MuY29uY2F0KFwiLFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvKmlmICh0aGlzLm1pbGVzdG9uZVRhc2tzLmxlbmd0aCA8PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5taWxlc3RvbmVUYXNrcyA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5taWxlc3RvbmVUYXNrcyA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfSovXHJcbiAgICAgICAgICAgICAgICB0aGlzLm1pbGVzdG9uZVRhc2tzID0gJyc7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1pbGVzdG9uZVRhc2tzID0gJyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbWFzdGVyVG9nZ2xlKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0QWxsUHJvamVjdENoZWNrZWQgPyB0aGlzLmNoZWNrQWxsUHJvamVjdHMoKSA6IHRoaXMudW5jaGVja0FsbFByb2plY3RzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tBbGxQcm9qZWN0cygpIHtcclxuICAgICAgICB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5kYXRhLmZvckVhY2gocm93ID0+IHtcclxuICAgICAgICAgICAgcm93LnNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdChyb3cpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0c0NhbGxCYWNrSGFuZGxlci5lbWl0KHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5zZWxlY3RlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgdW5jaGVja0FsbFByb2plY3RzKCkge1xyXG4gICAgICAgIHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEuZm9yRWFjaChyb3cgPT4ge1xyXG4gICAgICAgICAgICByb3cuc2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzLmRlc2VsZWN0KHJvdyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzQ2FsbEJhY2tIYW5kbGVyLmVtaXQodGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdGVkKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja1Byb2plY3QocHJvamVjdERhdGEsIGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuICAgICAgICAgICAgcHJvamVjdERhdGEuc2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdHMuc2VsZWN0KHByb2plY3REYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByb2plY3RzQ2FsbEJhY2tIYW5kbGVyLmVtaXQodGhpcy5zZWxlY3RlZFByb2plY3RzLnNlbGVjdGVkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdHMuZGVzZWxlY3QocHJvamVjdERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdHNDYWxsQmFja0hhbmRsZXIuZW1pdCh0aGlzLnNlbGVjdGVkUHJvamVjdHMuc2VsZWN0ZWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgICBnZXRQcm9wZXJ0eShkaXNwbGF5Q29sdW1uOiBNUE1GaWVsZCwgcHJvamVjdERhdGE6IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgdGhpcy5zdGF0dXMuZm9yRWFjaChzdGF0dXNGaWx0ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihkaXNwbGF5Q29sdW1uLCBwcm9qZWN0RGF0YSkgPT09IHN0YXR1c0ZpbHRlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbmFibGVCdWxrRWRpdCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBpZihkaXNwbGF5Q29sdW1uLk1BUFBFUl9OQU1FID09PSB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX1BST0dSRVNTKXtcclxuICAgICAgICAvLyAgICAgdGhpcy5nZXRDb21wbGV0ZWRNaWxlc3RvbmVzQnlQcm9qZWN0SWQocHJvamVjdERhdGEuSUQpO1xyXG4gICAgICAgIC8vICAgICByZXR1cm4gKHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtbiwgcHJvamVjdERhdGEpLmNvbmNhdCh0aGlzLm1pbGVzdG9uZVRhc2tzKSk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIC8vIGVsc2V7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oZGlzcGxheUNvbHVtbiwgcHJvamVjdERhdGEpO1xyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHlCeU1hcHBlcihwcm9qZWN0RGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIGNvbnN0IGN1cnJNUE1GaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBwcm9wZXJ0eUlkLCB0aGlzLmNhdGVnb3J5TGV2ZWwpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5KGN1cnJNUE1GaWVsZCwgcHJvamVjdERhdGEpO1xyXG4gICAgfVxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUoZGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdFRvTG9jYWxlUGlwZS50cmFuc2Zvcm0odGhpcy5wcm9qZWN0U2VydmljZS5jb252ZXJUb0xvY2FsRGF0ZShkYXRhLCBwcm9wZXJ0eUlkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJpb3JpdHkocHJvamVjdERhdGE6IGFueSwgZGlzcGxheUNvbHVtOiBNUE1GaWVsZCkge1xyXG4gICAgICAgIGNvbnN0IHByaW9yaXR5T2JqID0gdGhpcy51dGlsU2VydmljZS5nZXRQcmlvcml0eUljb24odGhpcy5nZXRQcm9wZXJ0eShkaXNwbGF5Q29sdW0sIHByb2plY3REYXRhKSxcclxuICAgICAgICAgICAgTVBNX0xFVkVMUy5QUk9KRUNUKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY29sb3I6IHByaW9yaXR5T2JqLmNvbG9yLFxyXG4gICAgICAgICAgICBpY29uOiBwcmlvcml0eU9iai5pY29uLFxyXG4gICAgICAgICAgICB0b29sdGlwOiBwcmlvcml0eU9iai50b29sdGlwXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0UHJvamVjdChldmVudCwgcHJvamVjdCxpc0NvcHlUZW1wbGF0ZTpib29sZWFuKSB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdENvbmZpZy5JU19QUk9KRUNUX1RZUEUpIHtcclxuICAgICAgICAgICAgcHJvamVjdC5pc1Byb2plY3RUeXBlID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBwcm9qZWN0LmlzUHJvamVjdFR5cGUgPSBmYWxzZTtcclxuICAgICAgICB9IFxyXG4gICAgICAgIHByb2plY3QuaXNDb3B5VGVtcGxhdGUgPSBpc0NvcHlUZW1wbGF0ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB0aGlzLmNvcHlQcm9qZWN0Lm5leHQocHJvamVjdCk7XHJcbiAgICB9XHJcblxyXG4gICAgb3Blbk1pbGVzdG9uZShldmVudCwgcHJvamVjdCkge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RDb25maWcuSVNfUFJPSkVDVF9UWVBFKSB7XHJcbiAgICAgICAgICAgIHByb2plY3QuaXNQcm9qZWN0VHlwZSA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcHJvamVjdC5pc1Byb2plY3RUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub3Blbk1pbGVzdG9uZURpYWxvZy5uZXh0KHByb2plY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIGVkaXRUYXNrKGV2ZW50RGF0YSkge1xyXG4gICAgICAgIHRoaXMuZWRpdFRhc2tIYW5kbGVyLmVtaXQoZXZlbnREYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ29tbWVudChldmVudCwgcHJvamVjdERldGFpbHMpIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBjb25zdCBwcm9qZWN0SWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfSUQsIHRoaXMuY2F0ZWdvcnlMZXZlbClcclxuICAgICAgICAgICAgLCBwcm9qZWN0RGV0YWlscyk7XHJcbiAgICAgICAgdGhpcy5vcGVuQ29tbWVudHNNb2R1bGUuZW1pdChwcm9qZWN0SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbEFjdGl2ZVRhc2tzRm9yUHJvamVjdChwcm9qZWN0OiBhbnksIGlzVXBkYXRlPzogYm9vbGVhbikge1xyXG4gICAgICAgIGlmICghcHJvamVjdC50YXNrTGlzdCB8fCBpc1VwZGF0ZSkge1xyXG4gICAgICAgICAgICBwcm9qZWN0LnRhc2tMaXN0ID0gW107XHJcbiAgICAgICAgICAgIHByb2plY3QudGFza1RvdGFsQ291bnQgPSAwO1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RTdGF0dXNUeXBlID0gcHJvamVjdC5QUk9KRUNUX1NUQVRVU19UWVBFO1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RDYW5jZWxsZWRTdGF0dXMgPSB0aGlzLnByb2plY3RTdGF0dXNUeXBlID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmlzT25Ib2xkUHJvamVjdCA9IHByb2plY3QuSVNfT05fSE9MRCA9PT0gJ3RydWUnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RDb25maWcuY3VycmVudFByb2plY3RJZCA9IHRoaXMuZ2V0UHJvcGVydHkoXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RDb25maWcuY3VycmVudFByb2plY3RJdGVtSWQgPSB0aGlzLmdldFByb3BlcnR5KFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgcHJvamVjdElkOiB0aGlzLnByb2plY3RDb25maWcuY3VycmVudFByb2plY3RJdGVtSWQsXHJcbiAgICAgICAgICAgICAgICBza2lwOiAwLFxyXG4gICAgICAgICAgICAgICAgdG9wOiAxMDBcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0QWxsQWN0aXZlVGFza3MocGFyYW1ldGVycywgdGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3RWaWV3TmFtZSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvamVjdC50YXNrTGlzdCA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvamVjdC50YXNrVG90YWxDb3VudCA9IHJlc3BvbnNlLmN1cnNvci50b3RhbF9yZWNvcmRzO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIGFjdGl2ZSB0YXNrcyBmb3IgcHJvamVjdCcpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hUYXNrKHByb2plY3Q6IGFueSkge1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsQWN0aXZlVGFza3NGb3JQcm9qZWN0KHByb2plY3QsIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5UYXNrRGV0YWlscyhldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLnZpZXdUYXNrRGV0YWlscy5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgZXhwYW5kUm93KGV2ZW50LCByb3cpIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBpZiAocm93LmlzRXhwYW5kZWQpIHtcclxuICAgICAgICAgICAgcm93LmlzRXhwYW5kZWQgPSBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByb3cuaXNFeHBhbmRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0QWxsQWN0aXZlVGFza3NGb3JQcm9qZWN0KHJvdyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5Qcm9qZWN0RGV0YWlscyhwcm9qZWN0LCBjb2x1bW5OYW1lLCBlbmFibGVDYW1wYWlnbiwgaXNDYW1wYWlnbk1hbmFnZXIsIGlzUG1WaWV3KSB7XHJcbiAgICAgICAgaWYgKGNvbHVtbk5hbWUgPT09IE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9DQU1QQUlHTl9GRUlMRFMuQ0FNUEFJR05fSUQgJiYgZW5hYmxlQ2FtcGFpZ24gJiYgaXNDYW1wYWlnbk1hbmFnZXIgJiYgaXNQbVZpZXcpIHsvKiBpc0NhbXBhaWduTWFuYWdlciAqL1xyXG4gICAgICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLkNBTVBBSUdOO1xyXG4gICAgICAgICAgICBjb25zdCBjYW1wYWlnbklkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCB0aGlzLm1wbUZpZWxkQ29uc3RhbnRzLk1QTV9DQU1QQUlHTl9GRUlMRFMuQ0FNUEFJR05fSUQsIHRoaXMuY2F0ZWdvcnlMZXZlbClcclxuICAgICAgICAgICAgICAgICwgcHJvamVjdCk7XHJcbiAgICAgICAgICAgIGlmIChjYW1wYWlnbklkICYmIGNhbXBhaWduSWQgIT09ICdOQScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3BlbkNhbXBhaWduSW5Qcm9qZWN0TGlzdC5lbWl0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy52aWV3UHJvamVjdERldGFpbHMuZW1pdChjYW1wYWlnbklkLnNwbGl0KCcuJykubGVuZ3RoID4gMSA/IGNhbXBhaWduSWQuc3BsaXQoJy4nKVsxXSA6IGNhbXBhaWduSWQuc3BsaXQoJy4nKVswXSk7XHJcbiAgICAgICAgICAgIH0gLyogZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0ludmFsaWQgY2FtcGFpZ24gSWQnKTtcclxuICAgICAgICAgICAgfSAqL1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmICh0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQpIHtcclxuICAgICAgICAgICAgLy8gdGhpcy5jYXRlZ29yeUxldmVsID0gTVBNX0xFVkVMUy5DQU1QQUlHTjtcclxuICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25JZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fQ0FNUEFJR05fRkVJTERTLkNBTVBBSUdOX0lURU1fSUQsIHRoaXMuY2F0ZWdvcnlMZXZlbClcclxuICAgICAgICAgICAgICAgICwgcHJvamVjdCk7XHJcbiAgICAgICAgICAgIGlmIChjYW1wYWlnbklkICYmIGNhbXBhaWduSWQgIT09ICdOQScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAvKiBpZiAoIXRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3BlbkNhbXBhaWduSW5Qcm9qZWN0TGlzdC5lbWl0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSAqL1xyXG4gICAgICAgICAgICAgICAgdGhpcy52aWV3UHJvamVjdERldGFpbHMuZW1pdChjYW1wYWlnbklkLnNwbGl0KCcuJylbMV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdJbnZhbGlkIGNhbXBhaWduIElkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLlBST0pFQ1Q7XHJcbiAgICAgICAgICAgIGNvbnN0IHByb2plY3RJZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBpZiAocHJvamVjdElkICYmIHByb2plY3RJZCAhPT0gJ05BJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQocHJvamVjdElkLnNwbGl0KCcuJylbMV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdJbnZhbGlkIHByb2plY3QgSWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyogaWYgKHRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCApIHtcclxuICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25JZCA9ICB0aGlzLmdldFByb3BlcnR5KHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIHRoaXMubXBtRmllbGRDb25zdGFudHMuTVBNX0NBTVBBSUdOX0ZFSUxEUy5DQU1QQUlHTl9JVEVNX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBpZiAoY2FtcGFpZ25JZCAmJiBjYW1wYWlnbklkICE9PSAnTkEnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy52aWV3UHJvamVjdERldGFpbHMuZW1pdChjYW1wYWlnbklkLnNwbGl0KCcuJylbMV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdJbnZhbGlkIGNhbXBhaWduIElkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLlBST0pFQ1Q7XHJcbiAgICAgICAgICAgIGNvbnN0IHByb2plY3RJZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgdGhpcy5tcG1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lELCB0aGlzLmNhdGVnb3J5TGV2ZWwpXHJcbiAgICAgICAgICAgICAgICAsIHByb2plY3QpO1xyXG4gICAgICAgICAgICBpZiAocHJvamVjdElkICYmIHByb2plY3RJZCAhPT0gJ05BJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmlld1Byb2plY3REZXRhaWxzLmVtaXQocHJvamVjdElkLnNwbGl0KCcuJylbMV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdJbnZhbGlkIHByb2plY3QgSWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gKi9cclxuICAgIH1cclxuICAgIFxyXG4gICAgcmVmcmVzaERhdGEoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gdGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3RWaWV3Q29uZmlnID9cclxuICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZS5nZXREaXNwbGF5T3JkZXJEYXRhKHRoaXMuZGlzcGxheWFibGVGaWVsZHMsIHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0Vmlld0NvbmZpZy5saXN0RmllbGRPcmRlcikgOiBbXTtcclxuICAgICAgICAvLyB0aGlzLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSB0aGlzLnByb2plY3RDb25maWcucHJvamVjdFZpZXdDb25maWcgP1xyXG4gICAgICAgIC8vICAgICB0aGlzLnV0aWxTZXJ2aWNlLmdldERpc3BsYXlPcmRlckRhdGEodGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3RWaWV3Q29uZmlnLlJfUE1fTElTVF9WSUVXX01QTV9GSUVMRFMsIHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0Vmlld0NvbmZpZy5MSVNUX0ZJRUxEX09SREVSKSA6IFtdO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMgPSB0aGlzLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMubWFwKChjb2x1bW46IE1QTUZpZWxkKSA9PiBjb2x1bW4uSU5ERVhFUl9GSUVMRF9JRCk7XHJcbiAgICAgICAgdGhpcy5pc1NlbGVjdCA9IHRoaXMuaXNDYW1wYWlnbiA/ICF0aGlzLmlzQ2FtcGFpZ24gOiAoIXRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCB8fCAhdGhpcy5wcm9qZWN0Q29uZmlnLklTX0NBTVBBSUdOX1RZUEUpO1xyXG4gICAgICAgIC8qIGlmICgodGhpcy5pc0NhbXBhaWduICYmICF0aGlzLmlzQ2FtcGFpZ24pIHx8ICh0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgJiYgIXRoaXMuaXNDYW1wYWlnbkRhc2hib2FyZCB8fCAhdGhpcy5wcm9qZWN0Q29uZmlnLklTX0NBTVBBSUdOX1RZUEUpKSB7XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgaWYgKHRoaXMuaXNTZWxlY3QpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy51bnNoaWZ0KCdTZWxlY3QnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdENvbmZpZy5JU19QUk9KRUNUX1RZUEUpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy51bnNoaWZ0KCdFeHBhbmQnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy9Db3B5IHRlbXBsYXRlIHRpY2tldC1zaHJlZSB0aGFyYW5pXHJcbiAgICAgICAgLyppZiAodGhpcy5zaG93Q29weVByb2plY3QgfHwgdGhpcy5wcm9qZWN0Q29uZmlnLklTX1BST0pFQ1RfVFlQRSkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zLnB1c2goJ0FjdGlvbnMnKTtcclxuICAgICAgICB9Ki8gXHJcbiAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy5wdXNoKCdBY3Rpb25zJyk7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0RGF0YURhdGFTb3VyY2UuZGF0YSA9IFtdO1xyXG4gICAgICAgIHRoaXMucHJvamVjdERhdGFEYXRhU291cmNlLmRhdGEgPSB0aGlzLnByb2plY3RDb25maWcucHJvamVjdERhdGE7XHJcbiAgICAgICAgdGhpcy5zb3J0LmFjdGl2ZSA9IHRoaXMucHJvamVjdENvbmZpZy5zZWxlY3RlZFNvcnRPcHRpb24ub3B0aW9uLm5hbWU7XHJcbiAgICAgICAgdGhpcy5zb3J0LmRpcmVjdGlvbiA9IHRoaXMucHJvamVjdENvbmZpZy5zZWxlY3RlZFNvcnRPcHRpb24uc29ydFR5cGUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB0aGlzLnByb2plY3REYXRhRGF0YVNvdXJjZS5zb3J0ID0gdGhpcy5zb3J0O1xyXG4gICAgfVxyXG5cclxuICAgIHRhYmxlRHJvcChldmVudDogQ2RrRHJhZ0Ryb3A8c3RyaW5nW10+KSB7XHJcbiAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuICAgICAgICBpZiAodGhpcy5kaXNwbGF5Q29sdW1ucy5maW5kKGNvbHVtbiA9PiBjb2x1bW4gPT09ICdTZWxlY3QnKSkge1xyXG4gICAgICAgICAgICBjb3VudCA9IGNvdW50ICsgMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuZGlzcGxheUNvbHVtbnMuZmluZChjb2x1bW4gPT4gY29sdW1uID09PSAnRXhwYW5kJykpIHtcclxuICAgICAgICAgICAgY291bnQgPSBjb3VudCArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG1vdmVJdGVtSW5BcnJheSh0aGlzLmRpc3BsYXlDb2x1bW5zLCBldmVudC5wcmV2aW91c0luZGV4ICsgY291bnQsIGV2ZW50LmN1cnJlbnRJbmRleCArIGNvdW50KTtcclxuICAgICAgICBjb25zdCBvcmRlcmVkRmllbGRzID0gW107XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XHJcbiAgICAgICAgICAgIGlmICghKGNvbHVtbiA9PT0gJ1NlbGVjdCcgfHwgY29sdW1uID09PSAnRXhwYW5kJyB8fCBjb2x1bW4gPT09ICdBY3Rpb25zJykpIHtcclxuICAgICAgICAgICAgICAgIG9yZGVyZWRGaWVsZHMucHVzaCh0aGlzLmRpc3BsYXlhYmxlRmllbGRzLmZpbmQoZGlzcGxheWFibGVGaWVsZCA9PiBkaXNwbGF5YWJsZUZpZWxkLklOREVYRVJfRklFTERfSUQgPT09IGNvbHVtbikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcmRlcmVkRGlzcGxheWFibGVGaWVsZHMubmV4dChvcmRlcmVkRmllbGRzKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJlc2l6ZU1vdXNlRG93bihldmVudCwgY29sdW1uKSB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBzdGFydCA9IGV2ZW50LnRhcmdldDtcclxuICAgICAgICBjb25zdCBwcmVzc2VkID0gdHJ1ZTtcclxuICAgICAgICBjb25zdCBzdGFydFggPSBldmVudC54O1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0V2lkdGggPSAkKHN0YXJ0KS5wYXJlbnQoKS53aWR0aCgpO1xyXG4gICAgICAgIHRoaXMuaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0UmVzaXphYmxlQ29sdW1ucyhzdGFydCwgcHJlc3NlZCwgc3RhcnRYLCBzdGFydFdpZHRoLCBjb2x1bW4pIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLmxpc3RlbignYm9keScsICdtb3VzZW1vdmUnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gc3RhcnRXaWR0aCArIChldmVudC54IC0gc3RhcnRYKTtcclxuICAgICAgICAgICAgICAgIGNvbHVtbi53aWR0aCA9IHdpZHRoO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2V1cCcsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocHJlc3NlZCkge1xyXG4gICAgICAgICAgICAgICAgcHJlc3NlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNpemVEaXNwbGF5YWJsZUZpZWxkcy5uZXh0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVQcm9qZWN0KGV2ZW50LCByb3cpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhldmVudCk7XHJcbiAgICAgICAgY29uc29sZS5sb2cocm93KTtcclxuICAgICAgICBsZXQgbXNnO1xyXG4gICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrID0gW107XHJcbiAgICAgICAgbGV0IHRhc2tzO1xyXG4gICAgICAgIGxldCBpc0N1c3RvbVdvcmtmbG93ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocm93LklTX0NVU1RPTV9XT1JLRkxPVyk7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9ICB0aGlzLmlzVGVtcGxhdGUgPyAnVGVtcGxhdGUnIDogJ1Byb2plY3QnO1xyXG4gICAgICAgIGlmIChpc0N1c3RvbVdvcmtmbG93KSB7XHJcbiAgICAgICAgICAgIG1zZyA9IFwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGUgXCIrbmFtZStcIiBhbmQgcmVjb25maWd1cmUgdGhlIHdvcmtmbG93IHJ1bGUgZW5naW5lP1wiO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG1zZyA9IFwiQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGUgXCIrbmFtZStcIiBhbmQgRXhpdD9cIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRUYXNrQnlQcm9qZWN0SUQocm93LklEKS5zdWJzY3JpYmUocHJvamVjdFRhc2tSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzayk7XHJcbiAgICAgICAgICAgIGlmIChwcm9qZWN0VGFza1Jlc3BvbnNlICYmIHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZS5UYXNrICYmICFBcnJheS5pc0FycmF5KHByb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFzaykpIHtcclxuICAgICAgICAgICAgICAgIHRhc2tzID0gW3Byb2plY3RUYXNrUmVzcG9uc2UuR2V0VGFza0J5UHJvamVjdElEUmVzcG9uc2UuVGFza107XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrcyA9IHByb2plY3RUYXNrUmVzcG9uc2UgJiYgcHJvamVjdFRhc2tSZXNwb25zZS5HZXRUYXNrQnlQcm9qZWN0SURSZXNwb25zZSAmJiBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2sgPyBwcm9qZWN0VGFza1Jlc3BvbnNlLkdldFRhc2tCeVByb2plY3RJRFJlc3BvbnNlLlRhc2sgOiAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGFza3MgJiYgdGFza3MubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGFza3MuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFmZmVjdGVkVGFzay5wdXNoKHRhc2suTkFNRSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyB9KTtcclxuICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IG1zZyxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLnByb2plY3RDb25maWcuSVNfUFJPSkVDVF9UWVBFID8gdGhpcy5hZmZlY3RlZFRhc2sgOiBbXSxcclxuICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZGVsZXRlUHJvamVjdChyb3cuSUQsIHJvdy5JVEVNX0lELCByb3cuUFJPSkVDVF9TVEFUVVNfSUQpLnN1YnNjcmliZShkZWxldGVQcm9qZWN0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcyhuYW1lKycgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy90aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaEVtaXR0ZXIubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdEZWxldGUgJytuYW1lKycgb3BlcmF0aW9uIGZhaWxlZCwgdHJ5IGFnYWluIGxhdGVyJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMgJiYgIWNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMuZmlyc3RDaGFuZ2UgJiZcclxuICAgICAgICAgICAgKEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMuY3VycmVudFZhbHVlKSAhPT0gSlNPTi5zdHJpbmdpZnkoY2hhbmdlcy5kaXNwbGF5YWJsZUZpZWxkcy5wcmV2aW91c1ZhbHVlKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnJlZnJlc2hEYXRhKClcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uLnZhbHVlID0gJ2JlbG93JztcclxuICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgdGhpcy5sb2dnZWRJblVzZXJJZCA9ICt1c2VySWQ7XHJcbiAgICAgICAgdGhpcy5maW5hbENvbXBsZXRlZCA9IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRDtcclxuICAgICAgICBjb25zdCBwcm9qZWN0Q29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRQcm9qZWN0Q29uZmlnKCk7XHJcbiAgICAgICAgY29uc3QgYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICB0aGlzLnNob3dDb3B5UHJvamVjdCA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHByb2plY3RDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX1BST0pFQ1RfQ09ORklHLkVOQUJMRV9DT1BZX1BST0pFQ1RdKVxyXG4gICAgICAgIHRoaXMuZW5hYmxlQ2FtcGFpZ24gPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShhcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX0NBTVBBSUdOXSk7XHJcbiAgICAgICAgdGhpcy5pc0NhbXBhaWduTWFuYWdlciA9ICh0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhbXBhaWduUm9sZSgpICYmIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q2FtcGFpZ25Sb2xlKCkubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5Jc1BNVmlldyA9ICh0aGlzLnByb2plY3RUeXBlID09PSBQcm9qZWN0VHlwZXMuQ0FNUEFJR04gfHwgdGhpcy5yb3V0ZXIudXJsLmluY2x1ZGVzKCdDYW1wYWlnbnMnKSB8fCB0aGlzLnJvdXRlci51cmwuaW5jbHVkZXMoJ2NhbXBhaWduJykpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaXNUZW1wbGF0ZSA9IHRoaXMucHJvamVjdFR5cGUgPT09IFByb2plY3RUeXBlcy5URU1QTEFURSB8fCB0aGlzLnJvdXRlci51cmwuaW5jbHVkZXMoJ1RlbXBsYXRlcycpICA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5yb3V0ZXIudXJsLmluY2x1ZGVzKCdDYW1wYWlnbnMnKSB8fCB0aGlzLnByb2plY3RDb25maWcuSVNfQ0FNUEFJR05fVFlQRSkge1xyXG4gICAgICAgICAgICB0aGlzLmlzQ2FtcGFpZ25EYXNoYm9hcmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWwgPSBNUE1fTEVWRUxTLkNBTVBBSUdOO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5wcm9qZWN0Q29uZmlnLm1ldGFEYXRhRmllbGRzICYmIHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0RGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGhpcy5wcm9qZWN0Q29uZmlnLnByb2plY3REYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvamVjdENvbmZpZy5wcm9qZWN0RGF0YSA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMucHJvamVjdENvbmZpZy5tZXRhRGF0YUZpZWxkcykge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RDb25maWcubWV0YURhdGFGaWVsZHMgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuc29ydCkge1xyXG4gICAgICAgICAgICB0aGlzLnNvcnQuZGlzYWJsZUNsZWFyID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNvcnQuYWN0aXZlICYmIHRoaXMuc29ydC5kaXJlY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFNvcnRMaXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0VHlwZTogdGhpcy5zb3J0LmRpcmVjdGlvbi50b1VwcGVyQ2FzZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb246IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHRoaXMuc29ydC5hY3RpdmUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLmVtaXQoc2VsZWN0ZWRTb3J0TGlzdCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLyogIGlmICh0aGlzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcykge1xyXG4gICAgICAgICAgICAgdGhpcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMuc3Vic2NyaWJlKChkYXRhOiBib29sZWFuKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5jaGVja0RlbGl2ZXJhYmxlKHRoaXMuYXNzZXQpO1xyXG4gICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgIH0gKi9cclxuICAgIH1cclxufSJdfQ==