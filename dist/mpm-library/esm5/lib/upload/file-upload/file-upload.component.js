import { __decorate, __read, __spread, __values } from "tslib";
import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef, HostListener, isDevMode } from '@angular/core';
import { NotificationService } from '../../notification/notification.service';
import { AssetFileConfigService } from '../services/asset.file.config.service';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SharingService } from '../../mpm-utils/services/sharing.service';
var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent(element, notificationService, assetFileConfigService, dialog, sharingService) {
        this.element = element;
        this.notificationService = notificationService;
        this.assetFileConfigService = assetFileConfigService;
        this.dialog = dialog;
        this.sharingService = sharingService;
        this.filesAdded = new EventEmitter();
        this.queuedFiles = new EventEmitter();
        this.thumbnailFormats = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];
        this.selectedFiles = [];
        this.isQDSUpload = otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload;
        this.htmlElement = this.element.nativeElement;
    }
    FileUploadComponent.prototype.getTypeOf = function (obj) {
        return typeof obj;
    };
    FileUploadComponent.prototype.openQDSFileChooser = function () {
        var that = this;
        qds_otmm.connector.openFileChooser(function (fileInfoList) {
            var e_1, _a;
            var fileObj;
            var transferFileObjectList = [];
            if (!fileInfoList) {
                return;
            }
            if (!Array.isArray(fileInfoList)) {
                fileInfoList = [fileInfoList];
            }
            try {
                for (var fileInfoList_1 = __values(fileInfoList), fileInfoList_1_1 = fileInfoList_1.next(); !fileInfoList_1_1.done; fileInfoList_1_1 = fileInfoList_1.next()) {
                    var fileInfo = fileInfoList_1_1.value;
                    fileObj = {};
                    fileObj.name = fileInfo.name;
                    fileObj.path = fileInfo.path;
                    fileObj.size = fileInfo.size;
                    fileObj.type = fileInfo.type;
                    fileObj.url = fileInfo.thumbnail;
                    fileObj.assetUrl = fileInfo.thumbnail;
                    fileObj.width = fileInfo.imageWidth;
                    fileObj.height = fileInfo.imageHeight;
                    transferFileObjectList.push(fileObj);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (fileInfoList_1_1 && !fileInfoList_1_1.done && (_a = fileInfoList_1.return)) _a.call(fileInfoList_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            that.processFileSelection(transferFileObjectList);
        });
    };
    FileUploadComponent.prototype.calculateSelectedFileSize = function () {
        var e_2, _a;
        var fileSize = 0;
        if (Array.isArray(this.selectedFiles) && this.selectedFiles.length > 0) {
            try {
                for (var _b = __values(this.selectedFiles), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var selectedFile = _c.value;
                    if (selectedFile && selectedFile.size
                        && selectedFile.size != null && selectedFile.size > 0) {
                        fileSize += selectedFile.size;
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        return fileSize;
    };
    FileUploadComponent.prototype.formatBytes = function (bytes) {
        if (bytes === 0) {
            return '0 Bytes';
        }
        var k = 1024;
        var dm = 2;
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        var i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    };
    FileUploadComponent.prototype.checkFilesSizeCosntraint = function (files) {
        var e_3, _a;
        if (!this.maxFileSize) {
            return true;
        }
        var validity = true;
        var currentSelectedFileSize = 0;
        if (files && files.length) {
            try {
                for (var files_1 = __values(files), files_1_1 = files_1.next(); !files_1_1.done; files_1_1 = files_1.next()) {
                    var file = files_1_1.value;
                    if (file.size && file.size != null && file.size > 0) {
                        currentSelectedFileSize += file.size;
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (files_1_1 && !files_1_1.done && (_a = files_1.return)) _a.call(files_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
            if (currentSelectedFileSize <= this.maxFileSize) {
                var existingFileSize = this.calculateSelectedFileSize();
                if (currentSelectedFileSize + existingFileSize <= this.maxFileSize) {
                    validity = true;
                }
                else {
                    validity = false;
                }
            }
            else {
                validity = false;
            }
        }
        if (!validity) {
            this.notificationService.info('Cannot upload file(s) of size more than ' + (this.maxFileSize / 1000) + 'KB');
        }
        return validity;
    };
    FileUploadComponent.prototype.filterFiles = function (currentSelectedFiles, existingFiles) {
        var e_4, _a, e_5, _b;
        var validFiles = [];
        var duplicates = [];
        if (currentSelectedFiles && currentSelectedFiles != null && currentSelectedFiles.length && currentSelectedFiles.length > 0) {
            try {
                for (var currentSelectedFiles_1 = __values(currentSelectedFiles), currentSelectedFiles_1_1 = currentSelectedFiles_1.next(); !currentSelectedFiles_1_1.done; currentSelectedFiles_1_1 = currentSelectedFiles_1.next()) {
                    var currentSelectedFile = currentSelectedFiles_1_1.value;
                    var alreadyExist = false;
                    var currentFile = currentSelectedFile;
                    try {
                        for (var existingFiles_1 = (e_5 = void 0, __values(existingFiles)), existingFiles_1_1 = existingFiles_1.next(); !existingFiles_1_1.done; existingFiles_1_1 = existingFiles_1.next()) {
                            var existingFile = existingFiles_1_1.value;
                            if (currentFile.name === existingFile.name) {
                                alreadyExist = true;
                                duplicates.push(currentSelectedFile);
                                break;
                            }
                        }
                    }
                    catch (e_5_1) { e_5 = { error: e_5_1 }; }
                    finally {
                        try {
                            if (existingFiles_1_1 && !existingFiles_1_1.done && (_b = existingFiles_1.return)) _b.call(existingFiles_1);
                        }
                        finally { if (e_5) throw e_5.error; }
                    }
                    if (!alreadyExist) {
                        validFiles.push(currentSelectedFile);
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (currentSelectedFiles_1_1 && !currentSelectedFiles_1_1.done && (_a = currentSelectedFiles_1.return)) _a.call(currentSelectedFiles_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
            if (duplicates.length > 0) {
                this.notificationService.info('File was not added because its name matches an existing file');
            }
            return validFiles;
        }
        else {
            return validFiles;
        }
    };
    FileUploadComponent.prototype.getFilesWebkitDataTransferItems = function (dataTransferItems) {
        var files = [];
        function traverseFileTreePromise(item, path) {
            if (path === void 0) { path = ''; }
            return new Promise(function (resolve) {
                if (item.isFile) {
                    item.file(function (file) {
                        file.filepath = path + file.name;
                        files.push(file);
                        resolve(file);
                    });
                }
                else if (item.isDirectory) {
                    var dirReader = item.createReader();
                    dirReader.readEntries(function (entries) {
                        var e_6, _a;
                        var entriesPromises = [];
                        try {
                            for (var entries_1 = __values(entries), entries_1_1 = entries_1.next(); !entries_1_1.done; entries_1_1 = entries_1.next()) {
                                var entr = entries_1_1.value;
                                entriesPromises.push(traverseFileTreePromise(entr, path + item.name + '/'));
                            }
                        }
                        catch (e_6_1) { e_6 = { error: e_6_1 }; }
                        finally {
                            try {
                                if (entries_1_1 && !entries_1_1.done && (_a = entries_1.return)) _a.call(entries_1);
                            }
                            finally { if (e_6) throw e_6.error; }
                        }
                        resolve(Promise.all(entriesPromises));
                    });
                }
            });
        }
        return new Promise(function (resolve, reject) {
            var e_7, _a;
            var entriesPromises = [];
            try {
                for (var dataTransferItems_1 = __values(dataTransferItems), dataTransferItems_1_1 = dataTransferItems_1.next(); !dataTransferItems_1_1.done; dataTransferItems_1_1 = dataTransferItems_1.next()) {
                    var it = dataTransferItems_1_1.value;
                    entriesPromises.push(traverseFileTreePromise(it.webkitGetAsEntry()));
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (dataTransferItems_1_1 && !dataTransferItems_1_1.done && (_a = dataTransferItems_1.return)) _a.call(dataTransferItems_1);
                }
                finally { if (e_7) throw e_7.error; }
            }
            Promise.all(entriesPromises).then(function (entries) {
                resolve(files);
            });
        });
    };
    FileUploadComponent.prototype.createFileObj = function (file, data) {
        if (file && file !== null) {
            if (data && data !== null) {
                file.data = data.split('base64,')[1];
                file.url = data;
            }
            return file;
        }
        else {
            return null;
        }
    };
    FileUploadComponent.prototype.checkNumberOfFileConstraint = function (files) {
        if (!this.maxFiles || files.length === 0) {
            return true;
        }
        var isValid = true;
        var currentSelectedFilesCount = files.length;
        var alreadySelectedFilesCount = this.selectedFiles.length;
        if (this.maxFiles === alreadySelectedFilesCount) {
            isValid = false;
        }
        else if (this.maxFiles < (currentSelectedFilesCount + alreadySelectedFilesCount)) {
            isValid = false;
        }
        if (!isValid) {
            if (this.isRevisionUpload) {
                this.notificationService.info('You can select only maximum of '
                    + this.maxFiles + ' file in Revision Upload');
            }
            else if (alreadySelectedFilesCount === 0) {
                this.notificationService.info('You can select only a maximum '
                    + this.maxFiles + ' files, But you are trying to select ' + currentSelectedFilesCount + ' file(s)');
            }
            else if (alreadySelectedFilesCount > 0) {
                this.notificationService.info('You can select maximum of ' + this.maxFiles
                    + ' file(s), You have already selected ' + alreadySelectedFilesCount + ' files');
            }
            else {
                this.notificationService.info('You you can select only a maximum ' + this.maxFiles + ' files');
            }
        }
        return isValid;
    };
    FileUploadComponent.prototype.checkForSpecialCharacter = function (files) {
        var e_8, _a;
        var _this = this;
        if (files) {
            var filesWithoutSpecialCharacter_1 = [];
            var filesWithSpecialCharacter_1 = [];
            try {
                for (var files_2 = __values(files), files_2_1 = files_2.next(); !files_2_1.done; files_2_1 = files_2.next()) {
                    var fileInfo = files_2_1.value;
                    (fileInfo && fileInfo.name && (fileInfo.name.indexOf('\'') >= 0)) ?
                        filesWithSpecialCharacter_1.push(fileInfo) : filesWithoutSpecialCharacter_1.push(fileInfo);
                }
            }
            catch (e_8_1) { e_8 = { error: e_8_1 }; }
            finally {
                try {
                    if (files_2_1 && !files_2_1.done && (_a = files_2.return)) _a.call(files_2);
                }
                finally { if (e_8) throw e_8.error; }
            }
            if (filesWithSpecialCharacter_1.length > 0) {
                var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'The special character $ and \' will be replaced with _.Do you want to continue?',
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(function (result) {
                    if (result && result.isTrue) {
                        _this.processFileSelection(__spread(filesWithoutSpecialCharacter_1, filesWithSpecialCharacter_1));
                    }
                });
            }
            else {
                this.processFileSelection(files);
            }
        }
    };
    FileUploadComponent.prototype.processFileSelection = function (files) {
        if ((this.isQDSUpload && this.checkNumberOfFileConstraint(files)) ||
            (this.checkNumberOfFileConstraint(files) && this.checkFilesSizeCosntraint(files))) {
            var validFiles = this.filterFiles(files, this.selectedFiles);
            this.handleFileProcessing(validFiles);
        }
    };
    FileUploadComponent.prototype.handleFileProcessing = function (validFiles) {
        var _this = this;
        this.filesAdded.next(validFiles);
        var _loop_1 = function (i) {
            var fileObj = validFiles[i];
            var objectUrl = fileObj.assetUrl;
            if (objectUrl instanceof Promise) {
                objectUrl
                    .then(function (obj) {
                    fileObj.width = obj.imageWidth,
                        fileObj.height = obj.imageHeight,
                        fileObj.url = obj.thumbnail,
                        fileObj.assetUrl = obj.thumbnail;
                })
                    .catch(function (err) {
                    console.log('Unable to generate thumbnail :' + err);
                });
            }
            if (fileObj && fileObj != null) {
                if (this_1.thumbnailFormats.indexOf(fileObj.type) !== -1 && i < 10) {
                    if (!this_1.isQDSUpload) {
                        var reader_1 = new FileReader();
                        reader_1.onload = function (e) {
                            var data = reader_1.result;
                            var obj = _this.createFileObj(fileObj, data);
                            if (obj && obj !== null) {
                                _this.selectedFiles.push(obj);
                            }
                        };
                        reader_1.readAsDataURL(fileObj);
                    }
                    else {
                        if (!fileObj.url || typeof fileObj.url === 'object') {
                            fileObj.icon = this_1.assetFileConfigService.findIconByName(fileObj.name);
                        }
                        if (this_1.isQDSUpload && fileObj.name && (fileObj.name.indexOf('$') >= 0 || fileObj.name.indexOf('\'') >= 0)) {
                            this_1.notificationService.error("Selected file(s) name has special characters dollar($) or single quote(') which is not allowed. Please change the file name and upload again.");
                        }
                        else {
                            this_1.selectedFiles.push(fileObj);
                        }
                    }
                }
                else {
                    fileObj.icon = this_1.assetFileConfigService.findIconByName(fileObj.name);
                    this_1.selectedFiles.push(fileObj);
                }
            }
        };
        var this_1 = this;
        for (var i = 0; i < validFiles.length; i++) {
            _loop_1(i);
        }
        this.queuedFiles.next(this.selectedFiles);
    };
    FileUploadComponent.prototype.onDrop = function (event) {
        var _this = this;
        var items = event.dataTransfer.items;
        this.getFilesWebkitDataTransferItems(items).then(function (files) {
            _this.checkForSpecialCharacter(files);
        });
        event.preventDefault();
        event.stopPropagation();
        this.element.nativeElement.value = '';
    };
    FileUploadComponent.prototype.onDropOver = function (event) {
        event.preventDefault();
    };
    FileUploadComponent.prototype.onFileSelectionChange = function (event) {
        var files = event.target.files;
        this.checkForSpecialCharacter(files);
        event.target.value = '';
    };
    FileUploadComponent.prototype.removeAll = function () {
        this.selectedFiles = [];
        this.queuedFiles.next(this.selectedFiles);
    };
    FileUploadComponent.prototype.removeFile = function (fileObj, index, event) {
        if (event) {
            event.stopPropagation();
        }
        this.selectedFiles.splice(index, 1);
        this.queuedFiles.next(this.selectedFiles);
    };
    FileUploadComponent.prototype.createQDSDropArea = function () {
        var that = this;
        var dropzoneEl = this.element.nativeElement.querySelector('div.acron-file-upload-wrapper');
        dropzoneEl.addEventListener('qds_dragdrop', function (event) {
            event = event || {};
            var eventType = (event.detail || {}).qdsEventType;
            if (eventType === 'dragleave' || eventType === 'addfile') {
                //   $(dropzoneEl).trigger('dragDropHideWindow');
            }
        });
        qds_otmm.connector.createDropArea(dropzoneEl, function (fileInfoList) {
            var e_9, _a;
            var fileObj;
            var transferFileObjectList = [];
            if (!fileInfoList) {
                return;
            }
            if (!Array.isArray(fileInfoList)) {
                fileInfoList = [fileInfoList];
            }
            try {
                for (var fileInfoList_2 = __values(fileInfoList), fileInfoList_2_1 = fileInfoList_2.next(); !fileInfoList_2_1.done; fileInfoList_2_1 = fileInfoList_2.next()) {
                    var fileInfo = fileInfoList_2_1.value;
                    fileObj = {};
                    fileObj.name = fileInfo.name;
                    fileObj.path = fileInfo.path;
                    fileObj.size = fileInfo.size;
                    fileObj.type = fileInfo.type;
                    fileObj.url = fileInfo.thumbnail;
                    fileObj.assetUrl = fileInfo.thumbnail;
                    fileObj.width = fileInfo.imageWidth;
                    fileObj.height = fileInfo.imageHeight;
                    transferFileObjectList.push(fileObj);
                }
            }
            catch (e_9_1) { e_9 = { error: e_9_1 }; }
            finally {
                try {
                    if (fileInfoList_2_1 && !fileInfoList_2_1.done && (_a = fileInfoList_2.return)) _a.call(fileInfoList_2);
                }
                finally { if (e_9) throw e_9.error; }
            }
            that.processFileSelection(transferFileObjectList);
        });
    };
    FileUploadComponent.prototype.renameFileName = function (fileName) {
        return fileName.replace(new RegExp('\''), '_').split('$').join('_');
    };
    FileUploadComponent.prototype.ngOnInit = function () {
        this.selectedFiles = [];
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        // if (!this.maxFileSize) {
        //     this.maxFileSize = 15000000;
        // }
    };
    FileUploadComponent.prototype.ngAfterViewInit = function () {
        if (this.isQDSUpload) {
            this.createQDSDropArea();
        }
    };
    FileUploadComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NotificationService },
        { type: AssetFileConfigService },
        { type: MatDialog },
        { type: SharingService }
    ]; };
    __decorate([
        Input()
    ], FileUploadComponent.prototype, "fileFormats", void 0);
    __decorate([
        Input()
    ], FileUploadComponent.prototype, "maxFiles", void 0);
    __decorate([
        Input()
    ], FileUploadComponent.prototype, "maxFileSize", void 0);
    __decorate([
        Input()
    ], FileUploadComponent.prototype, "isRevisionUpload", void 0);
    __decorate([
        Input()
    ], FileUploadComponent.prototype, "fileToRevision", void 0);
    __decorate([
        Output()
    ], FileUploadComponent.prototype, "filesAdded", void 0);
    __decorate([
        Output()
    ], FileUploadComponent.prototype, "queuedFiles", void 0);
    __decorate([
        HostListener('drop', ['$event'])
    ], FileUploadComponent.prototype, "onDrop", null);
    __decorate([
        HostListener('dragover', ['$event'])
    ], FileUploadComponent.prototype, "onDropOver", null);
    FileUploadComponent = __decorate([
        Component({
            selector: 'mpm-file-upload',
            template: "<div class=\"acron-file-upload-container\">\r\n    <div class=\"acron-file-upload-action\">\r\n        <div class=\"flex-row actions-align\">\r\n            <div class=\"flex-item\">\r\n                <mat-chip-list matTooltip=\"No. of Files Selected\">\r\n                    <mat-chip *ngIf=\"selectedFiles.length\">Selected ({{selectedFiles.length}}<span\r\n                            *ngIf=\"this.maxFiles > 0\">/{{this.maxFiles}}</span>)</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            <div class=\"actions-holder flex-item\">\r\n                <button mat-raised-button class=\"actions\" *ngIf=\"selectedFiles.length > 1\" (click)=\"removeAll()\"\r\n                    matTooltip=\"Click here to remove all selected files\">Remove All</button>\r\n                <button mat-raised-button class=\"actions\" (click)=\"isQDSUpload ? openQDSFileChooser() : fileBtn.click()\"\r\n                    matTooltip=\"Click here to select Files\">Select\r\n                    Files</button>\r\n                <input mat-raised-button class=\"actions\" #fileBtn type=\"file\" multiple\r\n                    (change)=\"onFileSelectionChange($event)\" style=\"display: none;\" />\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"qds-thumbnail-note\" *ngIf=\"isQDSUpload\">\r\n        <span>Note: Thumbnail preview will be shown only for first 10 image files for each selection.</span>\r\n    </div>\r\n    <div class=\"existing-file-holder\" *ngIf=\"isRevisionUpload && fileToRevision\">\r\n        <div class=\"file-items-holder\">\r\n            <mat-card>\r\n                <div class=\"card-file-contents\">\r\n                    <div class=\"file-icon\" matTooltip=\"Preview\">\r\n                        <img src=\"{{otmmBaseUrl + fileToRevision.url}}\" *ngIf=\"fileToRevision.url\" />\r\n                        <mat-icon *ngIf=\"fileToRevision.icon\">{{fileToRevision.icon}}</mat-icon>\r\n                    </div>\r\n                    <div class=\"file-details name\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"{{renameFileName(fileToRevision.name)}}\">{{renameFileName(fileToRevision.name)}}</span>\r\n                    </div>\r\n                </div>\r\n            </mat-card>\r\n        </div>\r\n    </div>\r\n    <div class=\"acron-file-upload-wrapper\" (click)=\"isQDSUpload ? openQDSFileChooser() : fileBtn.click()\">\r\n        <div class=\"file-items-holder\" *ngIf=\"selectedFiles.length > 0\">\r\n            <mat-card *ngFor=\"let file of selectedFiles\">\r\n                <div class=\"card-file-contents\">\r\n                    <div class=\"file-icon\" matTooltip=\"Preview\">\r\n                        <img *ngIf=\"file.url && getTypeOf(file.url) !== 'object'\" src=\"{{file.url}}\" />\r\n                        <mat-icon class=\"mat-preview-size\"\r\n                            *ngIf=\"file.icon && !(file.url || getTypeOf(file.url) === 'object')\">{{file.icon}}\r\n                        </mat-icon>\r\n                    </div>\r\n                    <div class=\"file-details name\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"File Name: {{renameFileName(file.name)}}\">{{renameFileName(file.name)}}</span>\r\n                    </div>\r\n                    <div class=\"file-details size\">\r\n                        <span class=\"file-info\"\r\n                            matTooltip=\"File Size: {{formatBytes(file.size)}}\">{{formatBytes(file.size)}}</span>\r\n                    </div>\r\n                    <div class=\"file-details type\">\r\n                        <span class=\"file-info\" matTooltip=\"File Type: {{file.type}}\">{{file.type}}</span>\r\n                    </div>\r\n                    <div class=\"file-actions\">\r\n                        <a class=\"action\" mat-icon-button matTooltip=\"Click here to remove this file\">\r\n                            <mat-icon class=\"action\" (click)=\"removeFile(file, selectedFiles.indexOf(file), $event)\">\r\n                                cancel</mat-icon>\r\n                        </a>\r\n                    </div>\r\n                </div>\r\n            </mat-card>\r\n        </div>\r\n        <div *ngIf=\"!selectedFiles.length\">\r\n            <p>Click or Drag and Drop Files here to upload</p>\r\n        </div>\r\n    </div>\r\n</div>",
            styles: [".acron-file-upload-container{display:flex;flex-direction:column;height:100%;width:100%}.acron-file-upload-container .qds-thumbnail-note{font-size:small;color:#f44336}.acron-file-upload-container .existing-file-holder{margin-right:-4px}.acron-file-upload-action,.acron-file-upload-wrapper{display:flex;flex-direction:row}.actions-align{width:100%;align-items:baseline}.acron-file-upload-wrapper{padding:0;margin:0 0 16px;align-items:center;justify-content:center;width:100%;height:100%;overflow:auto;border:2px dashed}.acron-file-upload-wrapper p{display:flex;align-items:center;justify-content:center}.file-items-holder{width:100%;height:100%}.mat-preview-size{font-size:40px;padding-right:15px}.file-icon img{height:40px;width:40px}.card-file-contents,.file-actions,.file-details{display:flex;flex-direction:row}.file-actions,.file-details{flex-grow:1}.file-details{margin:0 15px;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;display:inline-block}.file-details.name{width:25em}.file-details.size{width:5em}.file-details.type{width:10em}.acron-file-upload-action .actions-holder,.file-actions .action{margin-left:auto}.acron-file-upload-action .actions-holder .actions{margin:10px}.card-file-contents{align-items:center}"]
        })
    ], FileUploadComponent);
    return FileUploadComponent;
}());
export { FileUploadComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11cGxvYWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvdXBsb2FkL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25JLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3JILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFVMUU7SUFrQkksNkJBQ1csT0FBbUIsRUFDbkIsbUJBQXdDLEVBQ3hDLHNCQUE4QyxFQUM5QyxNQUFpQixFQUNqQixjQUE4QjtRQUo5QixZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQ25CLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQWZ4QixlQUFVLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7UUFDOUQsZ0JBQVcsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUdoRixxQkFBZ0IsR0FBRyxDQUFDLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBR3pFLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDO1FBU25FLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7SUFDbEQsQ0FBQztJQUVELHVDQUFTLEdBQVQsVUFBVSxHQUFHO1FBQ1QsT0FBTyxPQUFPLEdBQUcsQ0FBQztJQUN0QixDQUFDO0lBRUQsZ0RBQWtCLEdBQWxCO1FBQ0ksSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLFVBQUMsWUFBWTs7WUFFNUMsSUFBSSxPQUFPLENBQUM7WUFDWixJQUFNLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztZQUVsQyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNmLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUM5QixZQUFZLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNqQzs7Z0JBRUQsS0FBdUIsSUFBQSxpQkFBQSxTQUFBLFlBQVksQ0FBQSwwQ0FBQSxvRUFBRTtvQkFBaEMsSUFBTSxRQUFRLHlCQUFBO29CQUNmLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUM3QixPQUFPLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDN0IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUM3QixPQUFPLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQ2pDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztvQkFDdEMsT0FBTyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDO29CQUNwQyxPQUFPLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7b0JBQ3RDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDeEM7Ozs7Ozs7OztZQUVELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRXRELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVEQUF5QixHQUF6Qjs7UUFDSSxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O2dCQUNwRSxLQUEyQixJQUFBLEtBQUEsU0FBQSxJQUFJLENBQUMsYUFBYSxDQUFBLGdCQUFBLDRCQUFFO29CQUExQyxJQUFNLFlBQVksV0FBQTtvQkFDbkIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLElBQUk7MkJBQzlCLFlBQVksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLFlBQVksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO3dCQUN2RCxRQUFRLElBQUksWUFBWSxDQUFDLElBQUksQ0FBQztxQkFDakM7aUJBQ0o7Ozs7Ozs7OztTQUNKO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELHlDQUFXLEdBQVgsVUFBWSxLQUFhO1FBQ3JCLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTtZQUFFLE9BQU8sU0FBUyxDQUFDO1NBQUU7UUFDdEMsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ2YsSUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2IsSUFBTSxLQUFLLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3hFLElBQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEQsT0FBTyxVQUFVLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFRCxzREFBd0IsR0FBeEIsVUFBeUIsS0FBYTs7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbkIsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLHVCQUF1QixHQUFHLENBQUMsQ0FBQztRQUNoQyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFOztnQkFDdkIsS0FBbUIsSUFBQSxVQUFBLFNBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO29CQUFyQixJQUFNLElBQUksa0JBQUE7b0JBQ1gsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO3dCQUNqRCx1QkFBdUIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDO3FCQUN4QztpQkFDSjs7Ozs7Ozs7O1lBQ0QsSUFBSSx1QkFBdUIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUM3QyxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO2dCQUMxRCxJQUFJLHVCQUF1QixHQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hFLFFBQVEsR0FBRyxJQUFJLENBQUM7aUJBQ25CO3FCQUFNO29CQUNILFFBQVEsR0FBRyxLQUFLLENBQUM7aUJBQ3BCO2FBQ0o7aUJBQU07Z0JBQ0gsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUNwQjtTQUNKO1FBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsMENBQTBDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1NBQ2hIO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELHlDQUFXLEdBQVgsVUFBWSxvQkFBb0IsRUFBRSxhQUFhOztRQUMzQyxJQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksb0JBQW9CLElBQUksb0JBQW9CLElBQUksSUFBSSxJQUFJLG9CQUFvQixDQUFDLE1BQU0sSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDeEgsS0FBa0MsSUFBQSx5QkFBQSxTQUFBLG9CQUFvQixDQUFBLDBEQUFBLDRGQUFFO29CQUFuRCxJQUFNLG1CQUFtQixpQ0FBQTtvQkFDMUIsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUN6QixJQUFNLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQzs7d0JBQ3hDLEtBQTJCLElBQUEsaUNBQUEsU0FBQSxhQUFhLENBQUEsQ0FBQSw0Q0FBQSx1RUFBRTs0QkFBckMsSUFBTSxZQUFZLDBCQUFBOzRCQUNuQixJQUFJLFdBQVcsQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLElBQUksRUFBRTtnQ0FDeEMsWUFBWSxHQUFHLElBQUksQ0FBQztnQ0FDcEIsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUNyQyxNQUFNOzZCQUNUO3lCQUNKOzs7Ozs7Ozs7b0JBQ0QsSUFBSSxDQUFDLFlBQVksRUFBRTt3QkFDZixVQUFVLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7cUJBQ3hDO2lCQUNKOzs7Ozs7Ozs7WUFDRCxJQUFJLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLDhEQUE4RCxDQUFDLENBQUM7YUFDakc7WUFDRCxPQUFPLFVBQVUsQ0FBQztTQUNyQjthQUFNO1lBQ0gsT0FBTyxVQUFVLENBQUM7U0FDckI7SUFDTCxDQUFDO0lBRUQsNkRBQStCLEdBQS9CLFVBQWdDLGlCQUFpQjtRQUM3QyxJQUFNLEtBQUssR0FBVyxFQUFFLENBQUM7UUFDekIsU0FBUyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsSUFBUztZQUFULHFCQUFBLEVBQUEsU0FBUztZQUM1QyxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUEsT0FBTztnQkFDdEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO29CQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO3dCQUNWLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ2pDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEIsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUN6QixJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ3RDLFNBQVMsQ0FBQyxXQUFXLENBQUMsVUFBQSxPQUFPOzt3QkFDekIsSUFBTSxlQUFlLEdBQUcsRUFBRSxDQUFDOzs0QkFDM0IsS0FBbUIsSUFBQSxZQUFBLFNBQUEsT0FBTyxDQUFBLGdDQUFBLHFEQUFFO2dDQUF2QixJQUFNLElBQUksb0JBQUE7Z0NBQ1gsZUFBZSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQzs2QkFDL0U7Ozs7Ozs7Ozt3QkFDRCxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxDQUFDLENBQUMsQ0FBQztpQkFDTjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQztRQUNELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7WUFDL0IsSUFBTSxlQUFlLEdBQUcsRUFBRSxDQUFDOztnQkFDM0IsS0FBaUIsSUFBQSxzQkFBQSxTQUFBLGlCQUFpQixDQUFBLG9EQUFBLG1GQUFFO29CQUEvQixJQUFNLEVBQUUsOEJBQUE7b0JBQ1QsZUFBZSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3hFOzs7Ozs7Ozs7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87Z0JBQ3JDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFhLEdBQWIsVUFBYyxJQUFJLEVBQUUsSUFBSTtRQUNwQixJQUFJLElBQUksSUFBSSxJQUFJLEtBQUssSUFBSSxFQUFFO1lBQ3ZCLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7YUFDbkI7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNmO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELHlEQUEyQixHQUEzQixVQUE0QixLQUFhO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3RDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBTSx5QkFBeUIsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQU0seUJBQXlCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFDNUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLHlCQUF5QixFQUFFO1lBQzdDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDbkI7YUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyx5QkFBeUIsR0FBRyx5QkFBeUIsQ0FBQyxFQUFFO1lBQ2hGLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDbkI7UUFDRCxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ1YsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsaUNBQWlDO3NCQUN6RCxJQUFJLENBQUMsUUFBUSxHQUFHLDBCQUEwQixDQUFDLENBQUM7YUFDckQ7aUJBQU0sSUFBSSx5QkFBeUIsS0FBSyxDQUFDLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDO3NCQUN4RCxJQUFJLENBQUMsUUFBUSxHQUFHLHVDQUF1QyxHQUFHLHlCQUF5QixHQUFHLFVBQVUsQ0FBQyxDQUFDO2FBQzNHO2lCQUFNLElBQUkseUJBQXlCLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQyxRQUFRO3NCQUNwRSxzQ0FBc0MsR0FBRyx5QkFBeUIsR0FBRyxRQUFRLENBQUMsQ0FBQzthQUN4RjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLG9DQUFvQyxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUM7YUFDbEc7U0FDSjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFDRCxzREFBd0IsR0FBeEIsVUFBeUIsS0FBYTs7UUFBdEMsaUJBMkJDO1FBMUJHLElBQUksS0FBSyxFQUFFO1lBQ1AsSUFBTSw4QkFBNEIsR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBTSwyQkFBeUIsR0FBRyxFQUFFLENBQUM7O2dCQUNyQyxLQUF1QixJQUFBLFVBQUEsU0FBQSxLQUFLLENBQUEsNEJBQUEsK0NBQUU7b0JBQXpCLElBQU0sUUFBUSxrQkFBQTtvQkFDZixDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMvRCwyQkFBeUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLDhCQUE0QixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDOUY7Ozs7Ozs7OztZQUNELElBQUksMkJBQXlCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdEMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7b0JBQzNELEtBQUssRUFBRSxLQUFLO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixJQUFJLEVBQUU7d0JBQ0YsT0FBTyxFQUFFLGlGQUFpRjt3QkFDMUYsWUFBWSxFQUFFLEtBQUs7d0JBQ25CLFlBQVksRUFBRSxJQUFJO3FCQUNyQjtpQkFDSixDQUFDLENBQUM7Z0JBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07b0JBQ3BDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7d0JBQ3pCLEtBQUksQ0FBQyxvQkFBb0IsVUFBSyw4QkFBNEIsRUFBSywyQkFBeUIsRUFBRSxDQUFDO3FCQUM5RjtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNwQztTQUNKO0lBQ0wsQ0FBQztJQUNELGtEQUFvQixHQUFwQixVQUFxQixLQUFLO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3RCxDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNuRixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3pDO0lBQ0wsQ0FBQztJQUVELGtEQUFvQixHQUFwQixVQUFxQixVQUFVO1FBQS9CLGlCQWdEQztRQS9DRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDeEIsQ0FBQztZQUNOLElBQU0sT0FBTyxHQUFRLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyxJQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBRW5DLElBQUksU0FBUyxZQUFZLE9BQU8sRUFBRTtnQkFDOUIsU0FBUztxQkFDSixJQUFJLENBQUMsVUFBQSxHQUFHO29CQUNMLE9BQU8sQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLFVBQVU7d0JBQzFCLE9BQU8sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLFdBQVc7d0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLFNBQVM7d0JBQzNCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQztnQkFDekMsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxVQUFBLEdBQUc7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsR0FBRyxHQUFHLENBQUMsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLENBQUM7YUFDVjtZQUVELElBQUksT0FBTyxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7Z0JBQzVCLElBQUksT0FBSyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUU7b0JBQzlELElBQUksQ0FBQyxPQUFLLFdBQVcsRUFBRTt3QkFDbkIsSUFBTSxRQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQzt3QkFDaEMsUUFBTSxDQUFDLE1BQU0sR0FBRyxVQUFDLENBQUM7NEJBQ2QsSUFBTSxJQUFJLEdBQUcsUUFBTSxDQUFDLE1BQU0sQ0FBQzs0QkFDM0IsSUFBTSxHQUFHLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQzlDLElBQUksR0FBRyxJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7Z0NBQ3JCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDOzZCQUNoQzt3QkFDTCxDQUFDLENBQUM7d0JBQ0YsUUFBTSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDakM7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksT0FBTyxPQUFPLENBQUMsR0FBRyxLQUFLLFFBQVEsRUFBRTs0QkFDakQsT0FBTyxDQUFDLElBQUksR0FBRyxPQUFLLHNCQUFzQixDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQzNFO3dCQUNELElBQUcsT0FBSyxXQUFXLElBQUksT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBQzs0QkFDdkcsT0FBSyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0lBQStJLENBQUMsQ0FBQzt5QkFDbkw7NkJBQUk7NEJBQ0QsT0FBSyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUNwQztxQkFDSjtpQkFDSjtxQkFBTTtvQkFDSCxPQUFPLENBQUMsSUFBSSxHQUFHLE9BQUssc0JBQXNCLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDeEUsT0FBSyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNwQzthQUNKOzs7UUEzQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO29CQUFqQyxDQUFDO1NBNENUO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFHTSxvQ0FBTSxHQUFiLFVBQWMsS0FBVTtRQUR4QixpQkFTQztRQVBHLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxLQUFLO1lBQ2xELEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBR00sd0NBQVUsR0FBakIsVUFBa0IsS0FBVTtRQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVNLG1EQUFxQixHQUE1QixVQUE2QixLQUFLO1FBQzlCLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELHVDQUFTLEdBQVQ7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELHdDQUFVLEdBQVYsVUFBVyxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUs7UUFDNUIsSUFBSSxLQUFLLEVBQUU7WUFDUCxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDM0I7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCwrQ0FBaUIsR0FBakI7UUFDSSxJQUFNLElBQUksR0FBRyxJQUFJLENBQUM7UUFFbEIsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLCtCQUErQixDQUFDLENBQUM7UUFDN0YsVUFBVSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsRUFBRSxVQUFDLEtBQUs7WUFDOUMsS0FBSyxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7WUFDcEIsSUFBTSxTQUFTLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFlBQVksQ0FBQztZQUNwRCxJQUFJLFNBQVMsS0FBSyxXQUFXLElBQUksU0FBUyxLQUFLLFNBQVMsRUFBRTtnQkFDdEQsaURBQWlEO2FBQ3BEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsVUFBQyxZQUFZOztZQUN2RCxJQUFJLE9BQU8sQ0FBQztZQUNaLElBQU0sc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1lBRWxDLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ2YsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQzlCLFlBQVksR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQ2pDOztnQkFFRCxLQUF1QixJQUFBLGlCQUFBLFNBQUEsWUFBWSxDQUFBLDBDQUFBLG9FQUFFO29CQUFoQyxJQUFNLFFBQVEseUJBQUE7b0JBQ2YsT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDYixPQUFPLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDN0IsT0FBTyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUM3QixPQUFPLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQzdCLE9BQU8sQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztvQkFDakMsT0FBTyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO29CQUN0QyxPQUFPLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxVQUFVLENBQUM7b0JBQ3BDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztvQkFDdEMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN4Qzs7Ozs7Ozs7O1lBRUQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQWMsR0FBZCxVQUFlLFFBQWdCO1FBQzNCLE9BQU8sUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFDRCxzQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1FBQ3hGLDJCQUEyQjtRQUMzQixtQ0FBbUM7UUFDbkMsSUFBSTtJQUNSLENBQUM7SUFFRCw2Q0FBZSxHQUFmO1FBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7Z0JBdFhtQixVQUFVO2dCQUNFLG1CQUFtQjtnQkFDaEIsc0JBQXNCO2dCQUN0QyxTQUFTO2dCQUNELGNBQWM7O0lBckJoQztRQUFSLEtBQUssRUFBRTs0REFBdUI7SUFDdEI7UUFBUixLQUFLLEVBQUU7eURBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFOzREQUFxQjtJQUNwQjtRQUFSLEtBQUssRUFBRTtpRUFBMkI7SUFDMUI7UUFBUixLQUFLLEVBQUU7K0RBQXFCO0lBRW5CO1FBQVQsTUFBTSxFQUFFOzJEQUFzRTtJQUNyRTtRQUFULE1BQU0sRUFBRTs0REFBdUU7SUFzU2hGO1FBREMsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FEQVNoQztJQUdEO1FBREMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lEQUdwQztJQTVUUSxtQkFBbUI7UUFOL0IsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixvMklBQTJDOztTQUU5QyxDQUFDO09BRVcsbUJBQW1CLENBMlkvQjtJQUFELDBCQUFDO0NBQUEsQUEzWUQsSUEyWUM7U0EzWVksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEFmdGVyVmlld0luaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyLCBpc0Rldk1vZGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IEFzc2V0RmlsZUNvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hc3NldC5maWxlLmNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcblxyXG5kZWNsYXJlIGNvbnN0IHFkc19vdG1tOiBhbnk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWZpbGUtdXBsb2FkJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9maWxlLXVwbG9hZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9maWxlLXVwbG9hZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgZmlsZUZvcm1hdHM6IHN0cmluZ1tdO1xyXG4gICAgQElucHV0KCkgbWF4RmlsZXM6IG51bWJlcjtcclxuICAgIEBJbnB1dCgpIG1heEZpbGVTaXplOiBudW1iZXI7XHJcbiAgICBASW5wdXQoKSBpc1JldmlzaW9uVXBsb2FkOiBib29sZWFuO1xyXG4gICAgQElucHV0KCkgZmlsZVRvUmV2aXNpb246IGFueTtcclxuXHJcbiAgICBAT3V0cHV0KCkgcHVibGljIGZpbGVzQWRkZWQ6IEV2ZW50RW1pdHRlcjxGaWxlW10+ID0gbmV3IEV2ZW50RW1pdHRlcjxGaWxlW10+KCk7XHJcbiAgICBAT3V0cHV0KCkgcHVibGljIHF1ZXVlZEZpbGVzOiBFdmVudEVtaXR0ZXI8RmlsZVtdPiA9IG5ldyBFdmVudEVtaXR0ZXI8RmlsZVtdPigpO1xyXG5cclxuICAgIG90bW1CYXNlVXJsOiBzdHJpbmc7XHJcbiAgICB0aHVtYm5haWxGb3JtYXRzID0gWydpbWFnZS9qcGVnJywgJ2ltYWdlL3BuZycsICdpbWFnZS9qcGcnLCAnaW1hZ2UvZ2lmJ107XHJcblxyXG4gICAgcHVibGljIGh0bWxFbGVtZW50OiBIVE1MRWxlbWVudDtcclxuICAgIHNlbGVjdGVkRmlsZXMgPSBbXTtcclxuICAgIGlzUURTVXBsb2FkID0gb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGVsZW1lbnQ6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFzc2V0RmlsZUNvbmZpZ1NlcnZpY2U6IEFzc2V0RmlsZUNvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMuaHRtbEVsZW1lbnQgPSB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUeXBlT2Yob2JqKSB7XHJcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblFEU0ZpbGVDaG9vc2VyKCkge1xyXG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzO1xyXG4gICAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5vcGVuRmlsZUNob29zZXIoKGZpbGVJbmZvTGlzdCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgbGV0IGZpbGVPYmo7XHJcbiAgICAgICAgICAgIGNvbnN0IHRyYW5zZmVyRmlsZU9iamVjdExpc3QgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIGlmICghZmlsZUluZm9MaXN0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShmaWxlSW5mb0xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlSW5mb0xpc3QgPSBbZmlsZUluZm9MaXN0XTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlSW5mbyBvZiBmaWxlSW5mb0xpc3QpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmogPSB7fTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoubmFtZSA9IGZpbGVJbmZvLm5hbWU7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnBhdGggPSBmaWxlSW5mby5wYXRoO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5zaXplID0gZmlsZUluZm8uc2l6ZTtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmoudHlwZSA9IGZpbGVJbmZvLnR5cGU7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnVybCA9IGZpbGVJbmZvLnRodW1ibmFpbDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmouYXNzZXRVcmwgPSBmaWxlSW5mby50aHVtYm5haWw7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLndpZHRoID0gZmlsZUluZm8uaW1hZ2VXaWR0aDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmouaGVpZ2h0ID0gZmlsZUluZm8uaW1hZ2VIZWlnaHQ7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2ZlckZpbGVPYmplY3RMaXN0LnB1c2goZmlsZU9iaik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoYXQucHJvY2Vzc0ZpbGVTZWxlY3Rpb24odHJhbnNmZXJGaWxlT2JqZWN0TGlzdCk7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbGN1bGF0ZVNlbGVjdGVkRmlsZVNpemUoKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgZmlsZVNpemUgPSAwO1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuc2VsZWN0ZWRGaWxlcykgJiYgdGhpcy5zZWxlY3RlZEZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBzZWxlY3RlZEZpbGUgb2YgdGhpcy5zZWxlY3RlZEZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRGaWxlICYmIHNlbGVjdGVkRmlsZS5zaXplXHJcbiAgICAgICAgICAgICAgICAgICAgJiYgc2VsZWN0ZWRGaWxlLnNpemUgIT0gbnVsbCAmJiBzZWxlY3RlZEZpbGUuc2l6ZSA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlU2l6ZSArPSBzZWxlY3RlZEZpbGUuc2l6ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmlsZVNpemU7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybWF0Qnl0ZXMoYnl0ZXM6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKGJ5dGVzID09PSAwKSB7IHJldHVybiAnMCBCeXRlcyc7IH1cclxuICAgICAgICBjb25zdCBrID0gMTAyNDtcclxuICAgICAgICBjb25zdCBkbSA9IDI7XHJcbiAgICAgICAgY29uc3Qgc2l6ZXMgPSBbJ0J5dGVzJywgJ0tCJywgJ01CJywgJ0dCJywgJ1RCJywgJ1BCJywgJ0VCJywgJ1pCJywgJ1lCJ107XHJcbiAgICAgICAgY29uc3QgaSA9IE1hdGguZmxvb3IoTWF0aC5sb2coYnl0ZXMpIC8gTWF0aC5sb2coaykpO1xyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KChieXRlcyAvIE1hdGgucG93KGssIGkpKS50b0ZpeGVkKGRtKSkgKyAnICcgKyBzaXplc1tpXTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja0ZpbGVzU2l6ZUNvc250cmFpbnQoZmlsZXM6IEZpbGVbXSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICghdGhpcy5tYXhGaWxlU2l6ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHZhbGlkaXR5ID0gdHJ1ZTtcclxuICAgICAgICBsZXQgY3VycmVudFNlbGVjdGVkRmlsZVNpemUgPSAwO1xyXG4gICAgICAgIGlmIChmaWxlcyAmJiBmaWxlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlIG9mIGZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZmlsZS5zaXplICYmIGZpbGUuc2l6ZSAhPSBudWxsICYmIGZpbGUuc2l6ZSA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50U2VsZWN0ZWRGaWxlU2l6ZSArPSBmaWxlLnNpemU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRTZWxlY3RlZEZpbGVTaXplIDw9IHRoaXMubWF4RmlsZVNpemUpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV4aXN0aW5nRmlsZVNpemUgPSB0aGlzLmNhbGN1bGF0ZVNlbGVjdGVkRmlsZVNpemUoKTtcclxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50U2VsZWN0ZWRGaWxlU2l6ZSArIGV4aXN0aW5nRmlsZVNpemUgPD0gdGhpcy5tYXhGaWxlU2l6ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkaXR5ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRpdHkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhbGlkaXR5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF2YWxpZGl0eSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnQ2Fubm90IHVwbG9hZCBmaWxlKHMpIG9mIHNpemUgbW9yZSB0aGFuICcgKyAodGhpcy5tYXhGaWxlU2l6ZSAvIDEwMDApICsgJ0tCJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWxpZGl0eTtcclxuICAgIH1cclxuXHJcbiAgICBmaWx0ZXJGaWxlcyhjdXJyZW50U2VsZWN0ZWRGaWxlcywgZXhpc3RpbmdGaWxlcyk6IEZpbGVbXSB7XHJcbiAgICAgICAgY29uc3QgdmFsaWRGaWxlcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGR1cGxpY2F0ZXMgPSBbXTtcclxuICAgICAgICBpZiAoY3VycmVudFNlbGVjdGVkRmlsZXMgJiYgY3VycmVudFNlbGVjdGVkRmlsZXMgIT0gbnVsbCAmJiBjdXJyZW50U2VsZWN0ZWRGaWxlcy5sZW5ndGggJiYgY3VycmVudFNlbGVjdGVkRmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGN1cnJlbnRTZWxlY3RlZEZpbGUgb2YgY3VycmVudFNlbGVjdGVkRmlsZXMpIHtcclxuICAgICAgICAgICAgICAgIGxldCBhbHJlYWR5RXhpc3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRGaWxlID0gY3VycmVudFNlbGVjdGVkRmlsZTtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgZXhpc3RpbmdGaWxlIG9mIGV4aXN0aW5nRmlsZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudEZpbGUubmFtZSA9PT0gZXhpc3RpbmdGaWxlLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxyZWFkeUV4aXN0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZHVwbGljYXRlcy5wdXNoKGN1cnJlbnRTZWxlY3RlZEZpbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoIWFscmVhZHlFeGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkRmlsZXMucHVzaChjdXJyZW50U2VsZWN0ZWRGaWxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZHVwbGljYXRlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnRmlsZSB3YXMgbm90IGFkZGVkIGJlY2F1c2UgaXRzIG5hbWUgbWF0Y2hlcyBhbiBleGlzdGluZyBmaWxlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHZhbGlkRmlsZXM7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHZhbGlkRmlsZXM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEZpbGVzV2Via2l0RGF0YVRyYW5zZmVySXRlbXMoZGF0YVRyYW5zZmVySXRlbXMpOiBQcm9taXNlPEZpbGVbXT4ge1xyXG4gICAgICAgIGNvbnN0IGZpbGVzOiBGaWxlW10gPSBbXTtcclxuICAgICAgICBmdW5jdGlvbiB0cmF2ZXJzZUZpbGVUcmVlUHJvbWlzZShpdGVtLCBwYXRoID0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKHJlc29sdmUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW0uaXNGaWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbS5maWxlKGZpbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlLmZpbGVwYXRoID0gcGF0aCArIGZpbGUubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXMucHVzaChmaWxlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShmaWxlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaXRlbS5pc0RpcmVjdG9yeSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRpclJlYWRlciA9IGl0ZW0uY3JlYXRlUmVhZGVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlyUmVhZGVyLnJlYWRFbnRyaWVzKGVudHJpZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBlbnRyaWVzUHJvbWlzZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBlbnRyIG9mIGVudHJpZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudHJpZXNQcm9taXNlcy5wdXNoKHRyYXZlcnNlRmlsZVRyZWVQcm9taXNlKGVudHIsIHBhdGggKyBpdGVtLm5hbWUgKyAnLycpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKFByb21pc2UuYWxsKGVudHJpZXNQcm9taXNlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZW50cmllc1Byb21pc2VzID0gW107XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgaXQgb2YgZGF0YVRyYW5zZmVySXRlbXMpIHtcclxuICAgICAgICAgICAgICAgIGVudHJpZXNQcm9taXNlcy5wdXNoKHRyYXZlcnNlRmlsZVRyZWVQcm9taXNlKGl0LndlYmtpdEdldEFzRW50cnkoKSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFByb21pc2UuYWxsKGVudHJpZXNQcm9taXNlcykudGhlbihlbnRyaWVzID0+IHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoZmlsZXMpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVGaWxlT2JqKGZpbGUsIGRhdGEpIHtcclxuICAgICAgICBpZiAoZmlsZSAmJiBmaWxlICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGZpbGUuZGF0YSA9IGRhdGEuc3BsaXQoJ2Jhc2U2NCwnKVsxXTtcclxuICAgICAgICAgICAgICAgIGZpbGUudXJsID0gZGF0YTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZmlsZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tOdW1iZXJPZkZpbGVDb25zdHJhaW50KGZpbGVzOiBGaWxlW10pOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoIXRoaXMubWF4RmlsZXMgfHwgZmlsZXMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgaXNWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFNlbGVjdGVkRmlsZXNDb3VudCA9IGZpbGVzLmxlbmd0aDtcclxuICAgICAgICBjb25zdCBhbHJlYWR5U2VsZWN0ZWRGaWxlc0NvdW50ID0gdGhpcy5zZWxlY3RlZEZpbGVzLmxlbmd0aDtcclxuICAgICAgICBpZiAodGhpcy5tYXhGaWxlcyA9PT0gYWxyZWFkeVNlbGVjdGVkRmlsZXNDb3VudCkge1xyXG4gICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLm1heEZpbGVzIDwgKGN1cnJlbnRTZWxlY3RlZEZpbGVzQ291bnQgKyBhbHJlYWR5U2VsZWN0ZWRGaWxlc0NvdW50KSkge1xyXG4gICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghaXNWYWxpZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc1JldmlzaW9uVXBsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnWW91IGNhbiBzZWxlY3Qgb25seSBtYXhpbXVtIG9mICdcclxuICAgICAgICAgICAgICAgICAgICArIHRoaXMubWF4RmlsZXMgKyAnIGZpbGUgaW4gUmV2aXNpb24gVXBsb2FkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWxyZWFkeVNlbGVjdGVkRmlsZXNDb3VudCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1lvdSBjYW4gc2VsZWN0IG9ubHkgYSBtYXhpbXVtICdcclxuICAgICAgICAgICAgICAgICAgICArIHRoaXMubWF4RmlsZXMgKyAnIGZpbGVzLCBCdXQgeW91IGFyZSB0cnlpbmcgdG8gc2VsZWN0ICcgKyBjdXJyZW50U2VsZWN0ZWRGaWxlc0NvdW50ICsgJyBmaWxlKHMpJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYWxyZWFkeVNlbGVjdGVkRmlsZXNDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdZb3UgY2FuIHNlbGVjdCBtYXhpbXVtIG9mICcgKyB0aGlzLm1heEZpbGVzXHJcbiAgICAgICAgICAgICAgICAgICAgKyAnIGZpbGUocyksIFlvdSBoYXZlIGFscmVhZHkgc2VsZWN0ZWQgJyArIGFscmVhZHlTZWxlY3RlZEZpbGVzQ291bnQgKyAnIGZpbGVzJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnWW91IHlvdSBjYW4gc2VsZWN0IG9ubHkgYSBtYXhpbXVtICcgKyB0aGlzLm1heEZpbGVzICsgJyBmaWxlcycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xyXG4gICAgfVxyXG4gICAgY2hlY2tGb3JTcGVjaWFsQ2hhcmFjdGVyKGZpbGVzOiBGaWxlW10pIHtcclxuICAgICAgICBpZiAoZmlsZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsZXNXaXRob3V0U3BlY2lhbENoYXJhY3RlciA9IFtdO1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlc1dpdGhTcGVjaWFsQ2hhcmFjdGVyID0gW107XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgZmlsZUluZm8gb2YgZmlsZXMpIHtcclxuICAgICAgICAgICAgICAgIChmaWxlSW5mbyAmJiBmaWxlSW5mby5uYW1lICYmIChmaWxlSW5mby5uYW1lLmluZGV4T2YoJ1xcJycpID49IDApKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZXNXaXRoU3BlY2lhbENoYXJhY3Rlci5wdXNoKGZpbGVJbmZvKSA6IGZpbGVzV2l0aG91dFNwZWNpYWxDaGFyYWN0ZXIucHVzaChmaWxlSW5mbyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGZpbGVzV2l0aFNwZWNpYWxDaGFyYWN0ZXIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnVGhlIHNwZWNpYWwgY2hhcmFjdGVyICQgYW5kIFxcJyB3aWxsIGJlIHJlcGxhY2VkIHdpdGggXy5EbyB5b3Ugd2FudCB0byBjb250aW51ZT8nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3NGaWxlU2VsZWN0aW9uKFsuLi5maWxlc1dpdGhvdXRTcGVjaWFsQ2hhcmFjdGVyLCAuLi5maWxlc1dpdGhTcGVjaWFsQ2hhcmFjdGVyXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3NGaWxlU2VsZWN0aW9uKGZpbGVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHByb2Nlc3NGaWxlU2VsZWN0aW9uKGZpbGVzKSB7XHJcbiAgICAgICAgaWYgKCh0aGlzLmlzUURTVXBsb2FkICYmIHRoaXMuY2hlY2tOdW1iZXJPZkZpbGVDb25zdHJhaW50KGZpbGVzKSkgfHxcclxuICAgICAgICAgICAgKHRoaXMuY2hlY2tOdW1iZXJPZkZpbGVDb25zdHJhaW50KGZpbGVzKSAmJiB0aGlzLmNoZWNrRmlsZXNTaXplQ29zbnRyYWludChmaWxlcykpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkRmlsZXMgPSB0aGlzLmZpbHRlckZpbGVzKGZpbGVzLCB0aGlzLnNlbGVjdGVkRmlsZXMpO1xyXG4gICAgICAgICAgICB0aGlzLmhhbmRsZUZpbGVQcm9jZXNzaW5nKHZhbGlkRmlsZXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVGaWxlUHJvY2Vzc2luZyh2YWxpZEZpbGVzKSB7XHJcbiAgICAgICAgdGhpcy5maWxlc0FkZGVkLm5leHQodmFsaWRGaWxlcyk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2YWxpZEZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGVPYmo6IGFueSA9IHZhbGlkRmlsZXNbaV07XHJcbiAgICAgICAgICAgIGNvbnN0IG9iamVjdFVybCA9IGZpbGVPYmouYXNzZXRVcmw7XHJcblxyXG4gICAgICAgICAgICBpZiAob2JqZWN0VXJsIGluc3RhbmNlb2YgUHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgb2JqZWN0VXJsXHJcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4ob2JqID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZU9iai53aWR0aCA9IG9iai5pbWFnZVdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZU9iai5oZWlnaHQgPSBvYmouaW1hZ2VIZWlnaHQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlT2JqLnVybCA9IG9iai50aHVtYm5haWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlT2JqLmFzc2V0VXJsID0gb2JqLnRodW1ibmFpbDtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaChlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnVW5hYmxlIHRvIGdlbmVyYXRlIHRodW1ibmFpbCA6JyArIGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChmaWxlT2JqICYmIGZpbGVPYmogIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGh1bWJuYWlsRm9ybWF0cy5pbmRleE9mKGZpbGVPYmoudHlwZSkgIT09IC0xICYmIGkgPCAxMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5pc1FEU1VwbG9hZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkZXIub25sb2FkID0gKGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGEgPSByZWFkZXIucmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgb2JqID0gdGhpcy5jcmVhdGVGaWxlT2JqKGZpbGVPYmosIGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9iaiAmJiBvYmogIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZXMucHVzaChvYmopO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWZpbGVPYmoudXJsIHx8IHR5cGVvZiBmaWxlT2JqLnVybCA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVPYmouaWNvbiA9IHRoaXMuYXNzZXRGaWxlQ29uZmlnU2VydmljZS5maW5kSWNvbkJ5TmFtZShmaWxlT2JqLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuaXNRRFNVcGxvYWQgJiYgZmlsZU9iai5uYW1lICYmIChmaWxlT2JqLm5hbWUuaW5kZXhPZignJCcpID49IDAgfHwgZmlsZU9iai5uYW1lLmluZGV4T2YoJ1xcJycpID49IDApKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihcIlNlbGVjdGVkIGZpbGUocykgbmFtZSBoYXMgc3BlY2lhbCBjaGFyYWN0ZXJzIGRvbGxhcigkKSBvciBzaW5nbGUgcXVvdGUoJykgd2hpY2ggaXMgbm90IGFsbG93ZWQuIFBsZWFzZSBjaGFuZ2UgdGhlIGZpbGUgbmFtZSBhbmQgdXBsb2FkIGFnYWluLlwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZXMucHVzaChmaWxlT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZU9iai5pY29uID0gdGhpcy5hc3NldEZpbGVDb25maWdTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGVPYmoubmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEZpbGVzLnB1c2goZmlsZU9iaik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5xdWV1ZWRGaWxlcy5uZXh0KHRoaXMuc2VsZWN0ZWRGaWxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZHJvcCcsIFsnJGV2ZW50J10pXHJcbiAgICBwdWJsaWMgb25Ecm9wKGV2ZW50OiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGl0ZW1zID0gZXZlbnQuZGF0YVRyYW5zZmVyLml0ZW1zO1xyXG4gICAgICAgIHRoaXMuZ2V0RmlsZXNXZWJraXREYXRhVHJhbnNmZXJJdGVtcyhpdGVtcykudGhlbihmaWxlcyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY2hlY2tGb3JTcGVjaWFsQ2hhcmFjdGVyKGZpbGVzKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LnZhbHVlID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZHJhZ292ZXInLCBbJyRldmVudCddKVxyXG4gICAgcHVibGljIG9uRHJvcE92ZXIoZXZlbnQ6IGFueSk6IGFueSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25GaWxlU2VsZWN0aW9uQ2hhbmdlKGV2ZW50KTogYW55IHtcclxuICAgICAgICBjb25zdCBmaWxlcyA9IGV2ZW50LnRhcmdldC5maWxlcztcclxuICAgICAgICB0aGlzLmNoZWNrRm9yU3BlY2lhbENoYXJhY3RlcihmaWxlcyk7XHJcbiAgICAgICAgZXZlbnQudGFyZ2V0LnZhbHVlID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlQWxsKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlcyA9IFtdO1xyXG4gICAgICAgIHRoaXMucXVldWVkRmlsZXMubmV4dCh0aGlzLnNlbGVjdGVkRmlsZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZUZpbGUoZmlsZU9iaiwgaW5kZXgsIGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB0aGlzLnF1ZXVlZEZpbGVzLm5leHQodGhpcy5zZWxlY3RlZEZpbGVzKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVRRFNEcm9wQXJlYSgpIHtcclxuICAgICAgICBjb25zdCB0aGF0ID0gdGhpcztcclxuXHJcbiAgICAgICAgY29uc3QgZHJvcHpvbmVFbCA9IHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2Rpdi5hY3Jvbi1maWxlLXVwbG9hZC13cmFwcGVyJyk7XHJcbiAgICAgICAgZHJvcHpvbmVFbC5hZGRFdmVudExpc3RlbmVyKCdxZHNfZHJhZ2Ryb3AnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgZXZlbnQgPSBldmVudCB8fCB7fTtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnRUeXBlID0gKGV2ZW50LmRldGFpbCB8fCB7fSkucWRzRXZlbnRUeXBlO1xyXG4gICAgICAgICAgICBpZiAoZXZlbnRUeXBlID09PSAnZHJhZ2xlYXZlJyB8fCBldmVudFR5cGUgPT09ICdhZGRmaWxlJykge1xyXG4gICAgICAgICAgICAgICAgLy8gICAkKGRyb3B6b25lRWwpLnRyaWdnZXIoJ2RyYWdEcm9wSGlkZVdpbmRvdycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5jcmVhdGVEcm9wQXJlYShkcm9wem9uZUVsLCAoZmlsZUluZm9MaXN0KSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBmaWxlT2JqO1xyXG4gICAgICAgICAgICBjb25zdCB0cmFuc2ZlckZpbGVPYmplY3RMaXN0ID0gW107XHJcblxyXG4gICAgICAgICAgICBpZiAoIWZpbGVJbmZvTGlzdCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoZmlsZUluZm9MaXN0KSkge1xyXG4gICAgICAgICAgICAgICAgZmlsZUluZm9MaXN0ID0gW2ZpbGVJbmZvTGlzdF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgZmlsZUluZm8gb2YgZmlsZUluZm9MaXN0KSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLm5hbWUgPSBmaWxlSW5mby5uYW1lO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai5wYXRoID0gZmlsZUluZm8ucGF0aDtcclxuICAgICAgICAgICAgICAgIGZpbGVPYmouc2l6ZSA9IGZpbGVJbmZvLnNpemU7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLnR5cGUgPSBmaWxlSW5mby50eXBlO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai51cmwgPSBmaWxlSW5mby50aHVtYm5haWw7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLmFzc2V0VXJsID0gZmlsZUluZm8udGh1bWJuYWlsO1xyXG4gICAgICAgICAgICAgICAgZmlsZU9iai53aWR0aCA9IGZpbGVJbmZvLmltYWdlV2lkdGg7XHJcbiAgICAgICAgICAgICAgICBmaWxlT2JqLmhlaWdodCA9IGZpbGVJbmZvLmltYWdlSGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgdHJhbnNmZXJGaWxlT2JqZWN0TGlzdC5wdXNoKGZpbGVPYmopO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGF0LnByb2Nlc3NGaWxlU2VsZWN0aW9uKHRyYW5zZmVyRmlsZU9iamVjdExpc3QpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmFtZUZpbGVOYW1lKGZpbGVOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBmaWxlTmFtZS5yZXBsYWNlKG5ldyBSZWdFeHAoJ1xcJycpLCAnXycpLnNwbGl0KCckJykuam9pbignXycpO1xyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEZpbGVzID0gW107XHJcbiAgICAgICAgdGhpcy5vdG1tQmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJy4vJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG4gICAgICAgIC8vIGlmICghdGhpcy5tYXhGaWxlU2l6ZSkge1xyXG4gICAgICAgIC8vICAgICB0aGlzLm1heEZpbGVTaXplID0gMTUwMDAwMDA7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5pc1FEU1VwbG9hZCkge1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZVFEU0Ryb3BBcmVhKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=