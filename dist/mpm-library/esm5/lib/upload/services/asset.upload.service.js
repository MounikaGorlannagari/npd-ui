import { __decorate, __values } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { UtilService } from '../../mpm-utils/services/util.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusTypes } from '../../mpm-utils/objects/StatusType';
import { AssetConstants } from '../../project/assets/asset_constants';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { QdsService } from './qds.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../mpm-utils/services/entity.appdef.service";
import * as i4 from "../../mpm-utils/services/util.service";
import * as i5 from "../../project/shared/services/category.service";
import * as i6 from "../../shared/services/field-config.service";
import * as i7 from "../../mpm-utils/services/sharing.service";
import * as i8 from "./qds.service";
var AssetUploadService = /** @class */ (function () {
    function AssetUploadService(appService, otmmService, entityAppDefService, utilService, categoryService, fieldConfigService, sharingService, qdsService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.entityAppDefService = entityAppDefService;
        this.utilService = utilService;
        this.categoryService = categoryService;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.qdsService = qdsService;
        this.IMPORT_DELIVERABLE_ASSET_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.IMPORT_DELIVERABLE_ASSET_WS = 'ImportDeliverableAsset';
    }
    AssetUploadService.prototype.constructMetadataFieldValuesFromEventHandler = function (metadataValues) {
        var e_1, _a;
        var metadataFieldValues = [];
        try {
            for (var _b = __values(Object.keys(metadataValues)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var prop = _c.value;
                metadataFieldValues[prop] = metadataValues[prop];
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return metadataFieldValues;
    };
    AssetUploadService.prototype.excludeFileFormat = function (name) {
        if (name.indexOf('.') !== -1) {
            return name.slice(0, name.lastIndexOf('.'));
        }
        else {
            return name;
        }
    };
    AssetUploadService.prototype.uploadCallBack = function (deliverable, job, uploadType) {
        var _this = this;
        return new Observable(function (observer) {
            if (deliverable && deliverable.deliverableItemId && job && uploadType) {
                var parameters = {
                    operation: uploadType,
                    jobId: job.process_instance_id,
                    applicationId: _this.entityAppDefService.getApplicationID(),
                    deliverableDetails: {
                        deliverable: deliverable.deliverableItemId
                    }
                };
                _this.appService.invokeRequest(_this.IMPORT_DELIVERABLE_ASSET_NS, _this.IMPORT_DELIVERABLE_ASSET_WS, parameters)
                    .subscribe(function (response) {
                    if (response) {
                        observer.next(true);
                        observer.complete();
                    }
                    else {
                        observer.error('Error while triggering Import OTMM Asset BPM.');
                    }
                }, function (error) {
                    observer.error(error);
                });
            }
            else {
                observer.error('Mandatory parameters are missing to trigger Import OTMM Asset BPM.');
            }
        });
    };
    AssetUploadService.prototype.checkInAsset = function (filesToCheckIn, JobId, folderId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.assetCheckIn(filesToCheckIn, JobId)
                .subscribe(function (response) {
                var uploadDetails = {
                    files: filesToCheckIn,
                    folderId: folderId,
                    processId: response && _this.utilService.isValid(response['job_handle'].process_instance_id) ?
                        response['job_handle'].process_instance_id : response['job_handle'].job_id
                };
                observer.next(uploadDetails);
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetUploadService.prototype.checkOutAsset = function (filesToCheckOut) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.assetCheckout(filesToCheckOut)
                .subscribe(function (assetCheckoutResponse) {
                _this.otmmService.lockAssets(filesToCheckOut)
                    .subscribe(function (lockAssetResponse) {
                    observer.next(lockAssetResponse);
                }, function (lockAssetError) {
                    observer.error(lockAssetError);
                });
            }, function (assetCheckoutError) {
                observer.error(assetCheckoutError);
            });
        });
    };
    AssetUploadService.prototype.uploadFilesViaHTTP = function (filesToUpload, eventData, isRevision) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(function (createOTMMJobResponse) {
                var jobId = createOTMMJobResponse['job_handle'].job_id;
                _this.otmmService.assetsRendition(filesToUpload, jobId).subscribe(function (res) {
                    _this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                        .subscribe(function (importOTMMJobResponse) {
                        eventData.files = filesToUpload;
                        eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                        if (eventData && eventData.data) {
                            if (eventData.processId && eventData.files) {
                                // this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                                observer.next(eventData);
                            }
                            else {
                                observer.error('Something went wrong while uploading the file(s).');
                            }
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }, function (importOTMMJobError) {
                        observer.error('Something went wrong while uploading the file(s).');
                    });
                }, function (createOTMMJobError) {
                    observer.error('Something went wrong while creating jobs to upload the file(s).');
                });
            }, function (error) {
                console.log(error);
            });
        });
    };
    AssetUploadService.prototype.uploadVersionViaHTTP = function (filesToUpload, eventData, isRevision) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(function (createOTMMJobResponse) {
                var jobId = createOTMMJobResponse['job_handle'].job_id;
                if (isRevision) {
                    filesToUpload[0].assetId = eventData.data.UPLOADED_ASSET_ID;
                }
                _this.otmmService.assetsRendition(filesToUpload, jobId).subscribe(function (res) {
                    _this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                        .subscribe(function (importOTMMJobResponse) {
                        eventData.files = filesToUpload;
                        eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                        if (eventData && eventData.data) {
                            if (eventData.processId && eventData.files) {
                                // this.qdsService.createQDSImportJob(eventData.processId, this.otmmService.getFilePaths(eventData.files));
                                observer.next(eventData);
                            }
                            else {
                                observer.error('Something went wrong while uploading the file(s).');
                            }
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }, function (importOTMMJobError) {
                        observer.error('Something went wrong while uploading the file(s).');
                    });
                }, function (createOTMMJobError) {
                    observer.error('Something went wrong while creating jobs to upload the file(s).');
                });
            }, function (error) {
                console.log(error);
            });
        });
    };
    AssetUploadService.prototype.uploadFilesViaQDS = function (filesToUpload, eventData, isRevision) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.createOTMMJob(filesToUpload[0].name)
                .subscribe(function (createOTMMJobResponse) {
                var jobId = createOTMMJobResponse['job_handle'].job_id;
                _this.otmmService.importOTMMJob(filesToUpload, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                    .subscribe(function (importOTMMJobResponse) {
                    eventData.files = filesToUpload;
                    eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                    if (eventData && eventData.data) {
                        if (eventData.processId && eventData.files) {
                            _this.qdsService.createQDSImportJob(eventData.processId, _this.otmmService.getFilePaths(eventData.files));
                            observer.next(eventData);
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }
                    else {
                        observer.error('Something went wrong while uploading the file(s).');
                    }
                }, function (importOTMMJobError) {
                    observer.error('Something went wrong while uploading the file(s).');
                });
            }, function (createOTMMJobError) {
                observer.error('Something went wrong while creating jobs to upload the file(s).');
            });
        });
    };
    AssetUploadService.prototype.uploadVersionsViaQDS = function (files, eventData, isRevision) {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.createOTMMJob(files[0].name)
                .subscribe(function (createOTMMJobResponse) {
                var jobId = createOTMMJobResponse['job_handle'].job_id;
                if (isRevision) {
                    files[0].assetId = eventData.data.UPLOADED_ASSET_ID;
                }
                _this.otmmService.importOTMMJob(files, eventData.otmmFields, eventData.otmmFieldValues, eventData.folderId, eventData.templateid, eventData.metadataModel, eventData.securityPolicyID, jobId, isRevision)
                    .subscribe(function (importOTMMJobResponse) {
                    eventData.files = files;
                    eventData.processId = importOTMMJobResponse['job_handle'].job_id;
                    if (eventData && eventData.data) {
                        if (eventData.processId && eventData.files) {
                            _this.qdsService.createQDSImportJob(eventData.processId, _this.otmmService.getFilePaths(eventData.files));
                            observer.next(eventData);
                        }
                        else {
                            observer.error('Something went wrong while uploading the file(s).');
                        }
                    }
                    else {
                        observer.error('Something went wrong while uploading the file(s).');
                    }
                }, function (importOTMMJobError) {
                    observer.error('Something went wrong while uploading the file(s).');
                });
            }, function (createOTMMJobError) {
                observer.error('Something went wrong while uploading the file(s).');
            });
        });
    };
    AssetUploadService.prototype.startUpload = function (files, deliverable, isRevision, folderId, project) {
        var _this = this;
        return new Observable(function (observer) {
            var uploadType = isRevision ? 'UPLOAD_REVISION' : 'UPLOAD';
            var metadata = _this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.ASSET);
            var formAssetMetadata;
            if (deliverable) {
                deliverable.metadata = _this.formAssetMetadataModel(deliverable);
            }
            else {
                formAssetMetadata = _this.formAssetMetadataModel(deliverable, project);
            }
            /** Following is used to add the extra metadata update while uploading */
            if (Array.isArray(deliverable.customMetadataFields) && deliverable.metadata &&
                deliverable.metadata.metadata_element_list
                && deliverable.metadata.metadata_element_list[0]
                && deliverable.metadata.metadata_element_list[0].metadata_element_list) {
                deliverable.metadata.metadata_element_list[0].metadata_element_list =
                    deliverable.metadata.metadata_element_list[0].metadata_element_list.concat(deliverable.customMetadataFields);
            }
            var eventData = {
                otmmFieldValues: deliverable ? deliverable.metadata : formAssetMetadata,
                metadataModel: metadata.METADATA_MODEL_ID,
                templateid: metadata.TEMPLATE_ID,
                securityPolicyID: metadata.SECURTIY_POLICY_IDS,
                folderId: deliverable ? deliverable.otmmProjectFolderId : folderId,
                data: deliverable ? deliverable : project
            };
            if (eventData.folderId) {
                if (!isRevision) {
                    if (!otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
                        _this.uploadFilesViaHTTP(files, eventData, isRevision)
                            .subscribe(function (uploadResponse) {
                            var job = {
                                process_instance_id: uploadResponse.processId
                            };
                            if (deliverable) {
                                _this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(function (uploadCallbackResponse) {
                                    observer.next(uploadCallbackResponse);
                                }, function (uploadCallbackError) {
                                    observer.error(uploadCallbackError);
                                });
                            }
                            else {
                                observer.next(uploadResponse);
                            }
                        }, function (uploadError) {
                            observer.error(uploadError);
                        });
                    }
                    else {
                        _this.uploadFilesViaQDS(files, eventData, isRevision)
                            .subscribe(function (uploadResponse) {
                            var job = {
                                process_instance_id: uploadResponse.processId
                            };
                            if (deliverable) {
                                _this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(function (uploadCallbackResponse) {
                                    observer.next(uploadCallbackResponse);
                                }, function (uploadCallbackError) {
                                    observer.error(uploadCallbackError);
                                });
                            }
                            else {
                                observer.next(uploadResponse);
                            }
                        }, function (uploadError) {
                            observer.error(uploadError);
                        });
                    }
                }
                else {
                    _this.checkOutAsset([deliverable.asset.asset_id])
                        .subscribe(function (checkOutAssetResponse) {
                        if (!otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
                            _this.uploadVersionViaHTTP(files, eventData, isRevision)
                                .subscribe(function (uploadResponse) {
                                var job = {
                                    process_instance_id: uploadResponse.processId
                                };
                                _this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(function (uploadCallbackResponse) {
                                    observer.next(uploadCallbackResponse);
                                }, function (uploadCallbackError) {
                                    observer.error(uploadCallbackError);
                                });
                            }, function (uploadError) {
                                observer.error(uploadError);
                            });
                        }
                        else {
                            _this.uploadVersionsViaQDS(files, eventData, isRevision)
                                .subscribe(function (uploadResponse) {
                                var job = {
                                    process_instance_id: uploadResponse.processId
                                };
                                _this.uploadCallBack(uploadResponse.data, job, uploadType)
                                    .subscribe(function (uploadCallbackResponse) {
                                    observer.next(uploadCallbackResponse);
                                }, function (uploadCallbackError) {
                                    observer.error(uploadCallbackError);
                                });
                            }, function (uploadError) {
                                observer.error(uploadError);
                            });
                        }
                    }, function (checkOutAssetError) {
                        observer.error(checkOutAssetError);
                    });
                }
            }
            else {
                observer.error('No folder found to upload the asset(s).');
            }
        });
    };
    AssetUploadService.prototype.formAssetMetadataModel = function (deliverableData, projectData) {
        var _this = this;
        var metadata = this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.ASSET);
        var currStatus = this.sharingService.getAllStatusConfig().find(function (data) {
            return data['STATUS_TYPE'] === StatusTypes.INITIAL && data.R_PO_CATAGORY_LEVEL['MPM_Category_Level-id'].Id === metadata['MPM_Category_Level-id'].Id;
        });
        var projectItemId = projectData ? projectData['Project-id'].ItemId : '';
        this.assetConstants = deliverableData ? AssetConstants.ASSET_UPLOAD_FIELDS : projectData ? AssetConstants.REFERENCE_ASSET_UPLOAD_FIELDS : '';
        var metadataFields = this.assetConstants.map(function (element) {
            var fieldName = _this.fieldConfigService.getIndexerIdByMapperValue(element.mapperName);
            return {
                id: element.id,
                type: 'com.artesia.metadata.MetadataField',
                value: {
                    value: {
                        type: 'string',
                        value: deliverableData[fieldName] ? deliverableData[fieldName] : projectData && fieldName === 'ID' ? projectItemId : ''
                    }
                }
            };
        });
        metadataFields.push({
            id: 'MPM.ASSET.IS_CLONED',
            type: 'com.artesia.metadata.MetadataField',
            value: {
                value: {
                    type: 'string',
                    value: 'false'
                }
            }
        });
        metadataFields.push({
            id: 'MPM.ASSET.IS_REFERENCE_ASSET',
            type: 'com.artesia.metadata.MetadataField',
            value: {
                value: {
                    type: 'string',
                    value: deliverableData ? 'false' : 'true'
                }
            }
        });
        return {
            metadata_element_list: [{
                    metadata_element_list: metadataFields
                }],
            metadata_model_id: 'MPM.ASSET',
            security_policy_list: [
                {
                    id: metadata.SECURTIY_POLICY_IDS
                }
            ]
        };
    };
    AssetUploadService.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService },
        { type: EntityAppDefService },
        { type: UtilService },
        { type: CategoryService },
        { type: FieldConfigService },
        { type: SharingService },
        { type: QdsService }
    ]; };
    AssetUploadService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AssetUploadService_Factory() { return new AssetUploadService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.EntityAppDefService), i0.ɵɵinject(i4.UtilService), i0.ɵɵinject(i5.CategoryService), i0.ɵɵinject(i6.FieldConfigService), i0.ɵɵinject(i7.SharingService), i0.ɵɵinject(i8.QdsService)); }, token: AssetUploadService, providedIn: "root" });
    AssetUploadService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AssetUploadService);
    return AssetUploadService;
}());
export { AssetUploadService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQudXBsb2FkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi91cGxvYWQvc2VydmljZXMvYXNzZXQudXBsb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR2hGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsV0FBVyxFQUFnQixNQUFNLG9DQUFvQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUVwRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7Ozs7O0FBTTNDO0lBQ0ksNEJBQ1csVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLFdBQXdCLEVBQ3hCLGVBQWdDLEVBQ2hDLGtCQUFzQyxFQUN0QyxjQUE4QixFQUM5QixVQUFzQjtRQVB0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFHakMsZ0NBQTJCLEdBQUcsc0RBQXNELENBQUM7UUFDckYsZ0NBQTJCLEdBQUcsd0JBQXdCLENBQUM7SUFIbkQsQ0FBQztJQU1MLHlFQUE0QyxHQUE1QyxVQUE2QyxjQUFjOztRQUN2RCxJQUFNLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzs7WUFDL0IsS0FBbUIsSUFBQSxLQUFBLFNBQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBM0MsSUFBTSxJQUFJLFdBQUE7Z0JBQ1gsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3BEOzs7Ozs7Ozs7UUFDRCxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7SUFFRCw4Q0FBaUIsR0FBakIsVUFBa0IsSUFBSTtRQUNsQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDMUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDL0M7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsMkNBQWMsR0FBZCxVQUFlLFdBQVcsRUFBRSxHQUFHLEVBQUUsVUFBVTtRQUEzQyxpQkEyQkM7UUExQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLGlCQUFpQixJQUFJLEdBQUcsSUFBSSxVQUFVLEVBQUU7Z0JBQ25FLElBQU0sVUFBVSxHQUFHO29CQUNmLFNBQVMsRUFBRSxVQUFVO29CQUNyQixLQUFLLEVBQUUsR0FBRyxDQUFDLG1CQUFtQjtvQkFDOUIsYUFBYSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDMUQsa0JBQWtCLEVBQUU7d0JBQ2hCLFdBQVcsRUFBRSxXQUFXLENBQUMsaUJBQWlCO3FCQUM3QztpQkFDSixDQUFDO2dCQUVGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQywyQkFBMkIsRUFBRSxLQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDO3FCQUN4RyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNmLElBQUksUUFBUSxFQUFFO3dCQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO3FCQUNuRTtnQkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxvRUFBb0UsQ0FBQyxDQUFDO2FBQ3hGO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQVksR0FBWixVQUFhLGNBQWMsRUFBRSxLQUFLLEVBQUUsUUFBUTtRQUE1QyxpQkFnQkM7UUFmRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDO2lCQUMvQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQU0sYUFBYSxHQUFHO29CQUNsQixLQUFLLEVBQUUsY0FBYztvQkFDckIsUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLFNBQVMsRUFBRSxRQUFRLElBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDekYsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTTtpQkFDakYsQ0FBQztnQkFFRixRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ2pDLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDBDQUFhLEdBQWIsVUFBYyxlQUFlO1FBQTdCLGlCQWNDO1FBYkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDO2lCQUMxQyxTQUFTLENBQUMsVUFBQSxxQkFBcUI7Z0JBQzVCLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQztxQkFDdkMsU0FBUyxDQUFDLFVBQUEsaUJBQWlCO29CQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3JDLENBQUMsRUFBRSxVQUFBLGNBQWM7b0JBQ2IsUUFBUSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUUsVUFBQSxrQkFBa0I7Z0JBQ2pCLFFBQVEsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUMzQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtDQUFrQixHQUFsQixVQUFtQixhQUFhLEVBQUUsU0FBUyxFQUFFLFVBQVU7UUFBdkQsaUJBZ0NDO1FBL0JHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ2hELFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjtnQkFDNUIsSUFBTSxLQUFLLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUN6RCxLQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsR0FBRztvQkFDL0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsRUFDekYsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEtBQUssRUFBRSxVQUFVLENBQUM7eUJBQ2hILFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjt3QkFDNUIsU0FBUyxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUM7d0JBQ2hDLFNBQVMsQ0FBQyxTQUFTLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO3dCQUNqRSxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFOzRCQUM3QixJQUFJLFNBQVMsQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRTtnQ0FDeEMsMkdBQTJHO2dDQUUzRyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDOzZCQUM1QjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7NkJBQ3ZFO3lCQUNBOzZCQUFNOzRCQUNQLFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQzt5QkFDdkU7b0JBQ0wsQ0FBQyxFQUFFLFVBQUEsa0JBQWtCO3dCQUNqQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7b0JBQ3hFLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsRUFBRSxVQUFBLGtCQUFrQjtvQkFDakIsUUFBUSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO2dCQUN0RixDQUFDLENBQUMsQ0FBQztZQUNILENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlEQUFvQixHQUFwQixVQUFxQixhQUFhLEVBQUMsU0FBUyxFQUFDLFVBQVU7UUFBdkQsaUJBbUNDO1FBbENHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7aUJBQ2hELFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjtnQkFDNUIsSUFBTSxLQUFLLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUN6RCxJQUFHLFVBQVUsRUFBQztvQkFDVixhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7aUJBQy9EO2dCQUNELEtBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGFBQWEsRUFBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO29CQUMvRCxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsZUFBZSxFQUN6RixTQUFTLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLFVBQVUsQ0FBQzt5QkFDaEgsU0FBUyxDQUFDLFVBQUEscUJBQXFCO3dCQUM1QixTQUFTLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQzt3QkFDaEMsU0FBUyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7d0JBQ2pFLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUU7NEJBQzdCLElBQUksU0FBUyxDQUFDLFNBQVMsSUFBSSxTQUFTLENBQUMsS0FBSyxFQUFFO2dDQUN4QywyR0FBMkc7Z0NBRTNHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NkJBQzVCO2lDQUFNO2dDQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQzs2QkFDdkU7eUJBQ0E7NkJBQU07NEJBQ1AsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3lCQUN2RTtvQkFDTCxDQUFDLEVBQUUsVUFBQSxrQkFBa0I7d0JBQ2pCLFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQztvQkFDeEUsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxFQUFFLFVBQUEsa0JBQWtCO29CQUNqQixRQUFRLENBQUMsS0FBSyxDQUFDLGlFQUFpRSxDQUFDLENBQUM7Z0JBQ3RGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDZixDQUFDO0lBRUQsOENBQWlCLEdBQWpCLFVBQWtCLGFBQWEsRUFBRSxTQUFTLEVBQUUsVUFBVTtRQUF0RCxpQkEyQkM7UUExQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDaEQsU0FBUyxDQUFDLFVBQUEscUJBQXFCO2dCQUM1QixJQUFNLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pELEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxlQUFlLEVBQ3pGLFNBQVMsQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLEVBQUUsVUFBVSxDQUFDO3FCQUNoSCxTQUFTLENBQUMsVUFBQSxxQkFBcUI7b0JBQzVCLFNBQVMsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDO29CQUNoQyxTQUFTLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQztvQkFDakUsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksRUFBRTt3QkFDN0IsSUFBSSxTQUFTLENBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxLQUFLLEVBQUU7NEJBQ3hDLEtBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs0QkFDeEcsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt5QkFDNUI7NkJBQU07NEJBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3lCQUN2RTtxQkFDQTt5QkFBTTt3QkFDUCxRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7cUJBQ3ZFO2dCQUNMLENBQUMsRUFBRSxVQUFBLGtCQUFrQjtvQkFDakIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO2dCQUN4RSxDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxVQUFBLGtCQUFrQjtnQkFDakIsUUFBUSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO1lBQ3RGLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaURBQW9CLEdBQXBCLFVBQXFCLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVTtRQUFqRCxpQkE4QkM7UUE3QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDeEMsU0FBUyxDQUFDLFVBQUEscUJBQXFCO2dCQUM1QixJQUFNLEtBQUssR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3pELElBQUksVUFBVSxFQUFFO29CQUNaLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztpQkFDdkQ7Z0JBQ0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGVBQWUsRUFDakYsU0FBUyxDQUFDLFFBQVEsRUFBRSxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEtBQUssRUFBRSxVQUFVLENBQUM7cUJBQ2hILFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjtvQkFDNUIsU0FBUyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7b0JBQ3hCLFNBQVMsQ0FBQyxTQUFTLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO29CQUNqRSxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO3dCQUM3QixJQUFJLFNBQVMsQ0FBQyxTQUFTLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRTs0QkFDeEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUN4RyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3lCQUM1Qjs2QkFBTTs0QkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7eUJBQ3ZFO3FCQUNKO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQztxQkFDdkU7Z0JBQ0wsQ0FBQyxFQUFFLFVBQUEsa0JBQWtCO29CQUNqQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7Z0JBQ3hFLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxFQUFFLFVBQUEsa0JBQWtCO2dCQUNqQixRQUFRLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7WUFDeEUsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksS0FBSyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBYyxFQUFFLE9BQWE7UUFBekUsaUJBK0dDO1FBOUdHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sVUFBVSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUM3RCxJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0RixJQUFJLGlCQUFpQixDQUFDO1lBQ3RCLElBQUksV0FBVyxFQUFFO2dCQUNiLFdBQVcsQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ25FO2lCQUFNO2dCQUNILGlCQUFpQixHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDekU7WUFFRCx5RUFBeUU7WUFDekUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLFdBQVcsQ0FBQyxRQUFRO2dCQUN2RSxXQUFXLENBQUMsUUFBUSxDQUFDLHFCQUFxQjttQkFDdkMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7bUJBQzdDLFdBQVcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLEVBQUU7Z0JBQ3hFLFdBQVcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCO29CQUMvRCxXQUFXLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsQ0FBQzthQUNwSDtZQUVELElBQU0sU0FBUyxHQUFHO2dCQUNkLGVBQWUsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLGlCQUFpQjtnQkFDdkUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxpQkFBaUI7Z0JBQ3pDLFVBQVUsRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDaEMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLG1CQUFtQjtnQkFDOUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxRQUFRO2dCQUNsRSxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU87YUFDNUMsQ0FBQztZQUNGLElBQUksU0FBUyxDQUFDLFFBQVEsRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDYixJQUFJLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFO3dCQUMzRCxLQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUM7NkJBQ2hELFNBQVMsQ0FBQyxVQUFBLGNBQWM7NEJBQ3JCLElBQU0sR0FBRyxHQUFHO2dDQUNSLG1CQUFtQixFQUFFLGNBQWMsQ0FBQyxTQUFTOzZCQUNoRCxDQUFDOzRCQUNGLElBQUksV0FBVyxFQUFFO2dDQUNiLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO3FDQUNwRCxTQUFTLENBQUMsVUFBQSxzQkFBc0I7b0NBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQ0FDMUMsQ0FBQyxFQUFFLFVBQUEsbUJBQW1CO29DQUNsQixRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0NBQ3hDLENBQUMsQ0FBQyxDQUFDOzZCQUNWO2lDQUFNO2dDQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7NkJBQ2pDO3dCQUNMLENBQUMsRUFBRSxVQUFBLFdBQVc7NEJBQ1YsUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDaEMsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDOzZCQUMvQyxTQUFTLENBQUMsVUFBQSxjQUFjOzRCQUNyQixJQUFNLEdBQUcsR0FBRztnQ0FDUixtQkFBbUIsRUFBRSxjQUFjLENBQUMsU0FBUzs2QkFDaEQsQ0FBQzs0QkFDRixJQUFJLFdBQVcsRUFBRTtnQ0FDYixLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQztxQ0FDcEQsU0FBUyxDQUFDLFVBQUEsc0JBQXNCO29DQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0NBQzFDLENBQUMsRUFBRSxVQUFBLG1CQUFtQjtvQ0FDbEIsUUFBUSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dDQUN4QyxDQUFDLENBQUMsQ0FBQzs2QkFDVjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzZCQUNqQzt3QkFDTCxDQUFDLEVBQUUsVUFBQSxXQUFXOzRCQUNWLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ2hDLENBQUMsQ0FBQyxDQUFDO3FCQUNWO2lCQUNKO3FCQUFNO29CQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUMzQyxTQUFTLENBQUMsVUFBQSxxQkFBcUI7d0JBQzVCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUU7NEJBQzNELEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQztpQ0FDbEQsU0FBUyxDQUFDLFVBQUEsY0FBYztnQ0FDckIsSUFBTSxHQUFHLEdBQUc7b0NBQ1IsbUJBQW1CLEVBQUUsY0FBYyxDQUFDLFNBQVM7aUNBQ2hELENBQUM7Z0NBQ0YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUM7cUNBQ3BELFNBQVMsQ0FBQyxVQUFBLHNCQUFzQjtvQ0FDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dDQUMxQyxDQUFDLEVBQUUsVUFBQSxtQkFBbUI7b0NBQ2xCLFFBQVEsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQ0FDeEMsQ0FBQyxDQUFDLENBQUM7NEJBQ1gsQ0FBQyxFQUFFLFVBQUEsV0FBVztnQ0FDVixRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzRCQUNoQyxDQUFDLENBQUMsQ0FBQzt5QkFDVjs2QkFBTTs0QkFDSCxLQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUM7aUNBQ2xELFNBQVMsQ0FBQyxVQUFBLGNBQWM7Z0NBQ3JCLElBQU0sR0FBRyxHQUFHO29DQUNSLG1CQUFtQixFQUFFLGNBQWMsQ0FBQyxTQUFTO2lDQUNoRCxDQUFDO2dDQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDO3FDQUNwRCxTQUFTLENBQUMsVUFBQSxzQkFBc0I7b0NBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQ0FDMUMsQ0FBQyxFQUFFLFVBQUEsbUJBQW1CO29DQUNsQixRQUFRLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0NBQ3hDLENBQUMsQ0FBQyxDQUFDOzRCQUNYLENBQUMsRUFBRSxVQUFBLFdBQVc7Z0NBQ1YsUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQzs0QkFDaEMsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxFQUFFLFVBQUEsa0JBQWtCO3dCQUNqQixRQUFRLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7b0JBQ3ZDLENBQUMsQ0FBQyxDQUFDO2lCQUNWO2FBQ0o7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO2FBQzdEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsbURBQXNCLEdBQXRCLFVBQXVCLGVBQWUsRUFBRSxXQUFpQjtRQUF6RCxpQkFvREM7UUFuREcsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEYsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUk7WUFDakUsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssV0FBVyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3hKLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBTSxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM3SSxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxVQUFBLE9BQU87WUFDbEQsSUFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN4RixPQUFPO2dCQUNILEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDZCxJQUFJLEVBQUUsb0NBQW9DO2dCQUMxQyxLQUFLLEVBQUU7b0JBQ0gsS0FBSyxFQUFFO3dCQUNILElBQUksRUFBRSxRQUFRO3dCQUNkLEtBQUssRUFBRSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxJQUFJLFNBQVMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsRUFBRTtxQkFDMUg7aUJBQ0o7YUFDSixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDSCxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ2hCLEVBQUUsRUFBRSxxQkFBcUI7WUFDekIsSUFBSSxFQUFFLG9DQUFvQztZQUMxQyxLQUFLLEVBQUU7Z0JBQ0gsS0FBSyxFQUFFO29CQUNILElBQUksRUFBRSxRQUFRO29CQUNkLEtBQUssRUFBRSxPQUFPO2lCQUNqQjthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsY0FBYyxDQUFDLElBQUksQ0FBQztZQUNoQixFQUFFLEVBQUUsOEJBQThCO1lBQ2xDLElBQUksRUFBRSxvQ0FBb0M7WUFDMUMsS0FBSyxFQUFFO2dCQUNILEtBQUssRUFBRTtvQkFDSCxJQUFJLEVBQUUsUUFBUTtvQkFDZCxLQUFLLEVBQUUsZUFBZSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU07aUJBQzVDO2FBQ0o7U0FDSixDQUFDLENBQUM7UUFFSCxPQUFPO1lBQ0gscUJBQXFCLEVBQUUsQ0FBQztvQkFDcEIscUJBQXFCLEVBQUUsY0FBYztpQkFDeEMsQ0FBQztZQUNGLGlCQUFpQixFQUFFLFdBQVc7WUFDOUIsb0JBQW9CLEVBQUU7Z0JBQ2xCO29CQUNJLEVBQUUsRUFBRSxRQUFRLENBQUMsbUJBQW1CO2lCQUNuQzthQUNKO1NBQ0osQ0FBQztJQUNOLENBQUM7O2dCQXJZc0IsVUFBVTtnQkFDVCxXQUFXO2dCQUNILG1CQUFtQjtnQkFDM0IsV0FBVztnQkFDUCxlQUFlO2dCQUNaLGtCQUFrQjtnQkFDdEIsY0FBYztnQkFDbEIsVUFBVTs7O0lBVHhCLGtCQUFrQjtRQUo5QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsa0JBQWtCLENBeVk5Qjs2QkEvWkQ7Q0ErWkMsQUF6WUQsSUF5WUM7U0F6WVksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IENhdGVnb3J5U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzVHlwZXMsIFN0YXR1c0xldmVscyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBBc3NldENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Byb2plY3QvYXNzZXRzL2Fzc2V0X2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBzdWJzY3JpYmVPbiB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4vcWRzLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXNzZXRVcGxvYWRTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgSU1QT1JUX0RFTElWRVJBQkxFX0FTU0VUX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG4gICAgSU1QT1JUX0RFTElWRVJBQkxFX0FTU0VUX1dTID0gJ0ltcG9ydERlbGl2ZXJhYmxlQXNzZXQnO1xyXG5cclxuICAgIGFzc2V0Q29uc3RhbnRzO1xyXG4gICAgY29uc3RydWN0TWV0YWRhdGFGaWVsZFZhbHVlc0Zyb21FdmVudEhhbmRsZXIobWV0YWRhdGFWYWx1ZXMpIHtcclxuICAgICAgICBjb25zdCBtZXRhZGF0YUZpZWxkVmFsdWVzID0gW107XHJcbiAgICAgICAgZm9yIChjb25zdCBwcm9wIG9mIE9iamVjdC5rZXlzKG1ldGFkYXRhVmFsdWVzKSkge1xyXG4gICAgICAgICAgICBtZXRhZGF0YUZpZWxkVmFsdWVzW3Byb3BdID0gbWV0YWRhdGFWYWx1ZXNbcHJvcF07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBtZXRhZGF0YUZpZWxkVmFsdWVzO1xyXG4gICAgfVxyXG5cclxuICAgIGV4Y2x1ZGVGaWxlRm9ybWF0KG5hbWUpIHtcclxuICAgICAgICBpZiAobmFtZS5pbmRleE9mKCcuJykgIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lLnNsaWNlKDAsIG5hbWUubGFzdEluZGV4T2YoJy4nKSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5hbWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZENhbGxCYWNrKGRlbGl2ZXJhYmxlLCBqb2IsIHVwbG9hZFR5cGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZSAmJiBkZWxpdmVyYWJsZS5kZWxpdmVyYWJsZUl0ZW1JZCAmJiBqb2IgJiYgdXBsb2FkVHlwZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBvcGVyYXRpb246IHVwbG9hZFR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgam9iSWQ6IGpvYi5wcm9jZXNzX2luc3RhbmNlX2lkLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwcGxpY2F0aW9uSWQ6IHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRBcHBsaWNhdGlvbklEKCksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlOiBkZWxpdmVyYWJsZS5kZWxpdmVyYWJsZUl0ZW1JZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5JTVBPUlRfREVMSVZFUkFCTEVfQVNTRVRfTlMsIHRoaXMuSU1QT1JUX0RFTElWRVJBQkxFX0FTU0VUX1dTLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ0Vycm9yIHdoaWxlIHRyaWdnZXJpbmcgSW1wb3J0IE9UTU0gQXNzZXQgQlBNLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignTWFuZGF0b3J5IHBhcmFtZXRlcnMgYXJlIG1pc3NpbmcgdG8gdHJpZ2dlciBJbXBvcnQgT1RNTSBBc3NldCBCUE0uJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja0luQXNzZXQoZmlsZXNUb0NoZWNrSW4sIEpvYklkLCBmb2xkZXJJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5hc3NldENoZWNrSW4oZmlsZXNUb0NoZWNrSW4sIEpvYklkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdXBsb2FkRGV0YWlscyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXM6IGZpbGVzVG9DaGVja0luLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb2xkZXJJZDogZm9sZGVySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NJZDogcmVzcG9uc2UgJiYgdGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKHJlc3BvbnNlWydqb2JfaGFuZGxlJ10ucHJvY2Vzc19pbnN0YW5jZV9pZCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5wcm9jZXNzX2luc3RhbmNlX2lkIDogcmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWRcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVwbG9hZERldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrT3V0QXNzZXQoZmlsZXNUb0NoZWNrT3V0KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmFzc2V0Q2hlY2tvdXQoZmlsZXNUb0NoZWNrT3V0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShhc3NldENoZWNrb3V0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UubG9ja0Fzc2V0cyhmaWxlc1RvQ2hlY2tPdXQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobG9ja0Fzc2V0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChsb2NrQXNzZXRSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGxvY2tBc3NldEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGxvY2tBc3NldEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCBhc3NldENoZWNrb3V0RXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGFzc2V0Q2hlY2tvdXRFcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZEZpbGVzVmlhSFRUUChmaWxlc1RvVXBsb2FkLCBldmVudERhdGEsIGlzUmV2aXNpb24pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY3JlYXRlT1RNTUpvYihmaWxlc1RvVXBsb2FkWzBdLm5hbWUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZU9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iSWQgPSBjcmVhdGVPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5hc3NldHNSZW5kaXRpb24oZmlsZXNUb1VwbG9hZCxqb2JJZCkuc3Vic2NyaWJlKHJlcyA9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5pbXBvcnRPVE1NSm9iKGZpbGVzVG9VcGxvYWQsIGV2ZW50RGF0YS5vdG1tRmllbGRzLCBldmVudERhdGEub3RtbUZpZWxkVmFsdWVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZvbGRlcklkLCBldmVudERhdGEudGVtcGxhdGVpZCwgZXZlbnREYXRhLm1ldGFkYXRhTW9kZWwsIGV2ZW50RGF0YS5zZWN1cml0eVBvbGljeUlELCBqb2JJZCwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoaW1wb3J0T1RNTUpvYlJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEuZmlsZXMgPSBmaWxlc1RvVXBsb2FkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5wcm9jZXNzSWQgPSBpbXBvcnRPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YSAmJiBldmVudERhdGEuZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnREYXRhLnByb2Nlc3NJZCAmJiBldmVudERhdGEuZmlsZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoaXMucWRzU2VydmljZS5jcmVhdGVRRFNJbXBvcnRKb2IoZXZlbnREYXRhLnByb2Nlc3NJZCwgdGhpcy5vdG1tU2VydmljZS5nZXRGaWxlUGF0aHMoZXZlbnREYXRhLmZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBpbXBvcnRPVE1NSm9iRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBjcmVhdGVPVE1NSm9iRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgY3JlYXRpbmcgam9icyB0byB1cGxvYWQgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBsb2FkVmVyc2lvblZpYUhUVFAoZmlsZXNUb1VwbG9hZCxldmVudERhdGEsaXNSZXZpc2lvbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jcmVhdGVPVE1NSm9iKGZpbGVzVG9VcGxvYWRbMF0ubmFtZSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JlYXRlT1RNTUpvYlJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2JJZCA9IGNyZWF0ZU9UTU1Kb2JSZXNwb25zZVsnam9iX2hhbmRsZSddLmpvYl9pZDtcclxuICAgICAgICAgICAgICAgICAgICBpZihpc1JldmlzaW9uKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXNUb1VwbG9hZFswXS5hc3NldElkID0gZXZlbnREYXRhLmRhdGEuVVBMT0FERURfQVNTRVRfSUQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuYXNzZXRzUmVuZGl0aW9uKGZpbGVzVG9VcGxvYWQsam9iSWQpLnN1YnNjcmliZShyZXMgPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuaW1wb3J0T1RNTUpvYihmaWxlc1RvVXBsb2FkLCBldmVudERhdGEub3RtbUZpZWxkcywgZXZlbnREYXRhLm90bW1GaWVsZFZhbHVlcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5mb2xkZXJJZCwgZXZlbnREYXRhLnRlbXBsYXRlaWQsIGV2ZW50RGF0YS5tZXRhZGF0YU1vZGVsLCBldmVudERhdGEuc2VjdXJpdHlQb2xpY3lJRCwgam9iSWQsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGltcG9ydE9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZpbGVzID0gZmlsZXNUb1VwbG9hZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEucHJvY2Vzc0lkID0gaW1wb3J0T1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudERhdGEgJiYgZXZlbnREYXRhLmRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YS5wcm9jZXNzSWQgJiYgZXZlbnREYXRhLmZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnFkc1NlcnZpY2UuY3JlYXRlUURTSW1wb3J0Sm9iKGV2ZW50RGF0YS5wcm9jZXNzSWQsIHRoaXMub3RtbVNlcnZpY2UuZ2V0RmlsZVBhdGhzKGV2ZW50RGF0YS5maWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgaW1wb3J0T1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgY3JlYXRlT1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGNyZWF0aW5nIGpvYnMgdG8gdXBsb2FkIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBsb2FkRmlsZXNWaWFRRFMoZmlsZXNUb1VwbG9hZCwgZXZlbnREYXRhLCBpc1JldmlzaW9uKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmNyZWF0ZU9UTU1Kb2IoZmlsZXNUb1VwbG9hZFswXS5uYW1lKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShjcmVhdGVPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGpvYklkID0gY3JlYXRlT1RNTUpvYlJlc3BvbnNlWydqb2JfaGFuZGxlJ10uam9iX2lkO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuaW1wb3J0T1RNTUpvYihmaWxlc1RvVXBsb2FkLCBldmVudERhdGEub3RtbUZpZWxkcywgZXZlbnREYXRhLm90bW1GaWVsZFZhbHVlcyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZvbGRlcklkLCBldmVudERhdGEudGVtcGxhdGVpZCwgZXZlbnREYXRhLm1ldGFkYXRhTW9kZWwsIGV2ZW50RGF0YS5zZWN1cml0eVBvbGljeUlELCBqb2JJZCwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShpbXBvcnRPVE1NSm9iUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhLmZpbGVzID0gZmlsZXNUb1VwbG9hZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5wcm9jZXNzSWQgPSBpbXBvcnRPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YS5wcm9jZXNzSWQgJiYgZXZlbnREYXRhLmZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5jcmVhdGVRRFNJbXBvcnRKb2IoZXZlbnREYXRhLnByb2Nlc3NJZCwgdGhpcy5vdG1tU2VydmljZS5nZXRGaWxlUGF0aHMoZXZlbnREYXRhLmZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGltcG9ydE9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0sIGNyZWF0ZU9UTU1Kb2JFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGNyZWF0aW5nIGpvYnMgdG8gdXBsb2FkIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBsb2FkVmVyc2lvbnNWaWFRRFMoZmlsZXMsIGV2ZW50RGF0YSwgaXNSZXZpc2lvbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jcmVhdGVPVE1NSm9iKGZpbGVzWzBdLm5hbWUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZU9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgam9iSWQgPSBjcmVhdGVPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzUmV2aXNpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXNbMF0uYXNzZXRJZCA9IGV2ZW50RGF0YS5kYXRhLlVQTE9BREVEX0FTU0VUX0lEO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmltcG9ydE9UTU1Kb2IoZmlsZXMsIGV2ZW50RGF0YS5vdG1tRmllbGRzLCBldmVudERhdGEub3RtbUZpZWxkVmFsdWVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEuZm9sZGVySWQsIGV2ZW50RGF0YS50ZW1wbGF0ZWlkLCBldmVudERhdGEubWV0YWRhdGFNb2RlbCwgZXZlbnREYXRhLnNlY3VyaXR5UG9saWN5SUQsIGpvYklkLCBpc1JldmlzaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGltcG9ydE9UTU1Kb2JSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEuZmlsZXMgPSBmaWxlcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YS5wcm9jZXNzSWQgPSBpbXBvcnRPVE1NSm9iUmVzcG9uc2VbJ2pvYl9oYW5kbGUnXS5qb2JfaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5kYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50RGF0YS5wcm9jZXNzSWQgJiYgZXZlbnREYXRhLmZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5jcmVhdGVRRFNJbXBvcnRKb2IoZXZlbnREYXRhLnByb2Nlc3NJZCwgdGhpcy5vdG1tU2VydmljZS5nZXRGaWxlUGF0aHMoZXZlbnREYXRhLmZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwbG9hZGluZyB0aGUgZmlsZShzKS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgaW1wb3J0T1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocykuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSwgY3JlYXRlT1RNTUpvYkVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBsb2FkaW5nIHRoZSBmaWxlKHMpLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnRVcGxvYWQoZmlsZXMsIGRlbGl2ZXJhYmxlLCBpc1JldmlzaW9uLCBmb2xkZXJJZD86IGFueSwgcHJvamVjdD86IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXBsb2FkVHlwZSA9IGlzUmV2aXNpb24gPyAnVVBMT0FEX1JFVklTSU9OJyA6ICdVUExPQUQnO1xyXG4gICAgICAgICAgICBjb25zdCBtZXRhZGF0YSA9IHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlUeXBlKE1QTV9MRVZFTFMuQVNTRVQpO1xyXG4gICAgICAgICAgICBsZXQgZm9ybUFzc2V0TWV0YWRhdGE7XHJcbiAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGUubWV0YWRhdGEgPSB0aGlzLmZvcm1Bc3NldE1ldGFkYXRhTW9kZWwoZGVsaXZlcmFibGUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9ybUFzc2V0TWV0YWRhdGEgPSB0aGlzLmZvcm1Bc3NldE1ldGFkYXRhTW9kZWwoZGVsaXZlcmFibGUsIHByb2plY3QpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvKiogRm9sbG93aW5nIGlzIHVzZWQgdG8gYWRkIHRoZSBleHRyYSBtZXRhZGF0YSB1cGRhdGUgd2hpbGUgdXBsb2FkaW5nICovXHJcbiAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGRlbGl2ZXJhYmxlLmN1c3RvbU1ldGFkYXRhRmllbGRzKSAmJiBkZWxpdmVyYWJsZS5tZXRhZGF0YSAmJlxyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGUubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0XHJcbiAgICAgICAgICAgICAgICAmJiBkZWxpdmVyYWJsZS5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF1cclxuICAgICAgICAgICAgICAgICYmIGRlbGl2ZXJhYmxlLm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3QgPVxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3QuY29uY2F0KGRlbGl2ZXJhYmxlLmN1c3RvbU1ldGFkYXRhRmllbGRzKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgb3RtbUZpZWxkVmFsdWVzOiBkZWxpdmVyYWJsZSA/IGRlbGl2ZXJhYmxlLm1ldGFkYXRhIDogZm9ybUFzc2V0TWV0YWRhdGEsXHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YU1vZGVsOiBtZXRhZGF0YS5NRVRBREFUQV9NT0RFTF9JRCxcclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlaWQ6IG1ldGFkYXRhLlRFTVBMQVRFX0lELFxyXG4gICAgICAgICAgICAgICAgc2VjdXJpdHlQb2xpY3lJRDogbWV0YWRhdGEuU0VDVVJUSVlfUE9MSUNZX0lEUyxcclxuICAgICAgICAgICAgICAgIGZvbGRlcklkOiBkZWxpdmVyYWJsZSA/IGRlbGl2ZXJhYmxlLm90bW1Qcm9qZWN0Rm9sZGVySWQgOiBmb2xkZXJJZCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRlbGl2ZXJhYmxlID8gZGVsaXZlcmFibGUgOiBwcm9qZWN0XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGlmIChldmVudERhdGEuZm9sZGVySWQpIHtcclxuICAgICAgICAgICAgICAgIGlmICghaXNSZXZpc2lvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRGaWxlc1ZpYUhUVFAoZmlsZXMsIGV2ZW50RGF0YSwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodXBsb2FkUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGpvYiA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc19pbnN0YW5jZV9pZDogdXBsb2FkUmVzcG9uc2UucHJvY2Vzc0lkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRDYWxsQmFjayh1cGxvYWRSZXNwb25zZS5kYXRhLCBqb2IsIHVwbG9hZFR5cGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVwbG9hZENhbGxiYWNrUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXBsb2FkQ2FsbGJhY2tSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRDYWxsYmFja0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcih1cGxvYWRDYWxsYmFja0Vycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXBsb2FkUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVwbG9hZEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcih1cGxvYWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZEZpbGVzVmlhUURTKGZpbGVzLCBldmVudERhdGEsIGlzUmV2aXNpb24pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVwbG9hZFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2IgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfaW5zdGFuY2VfaWQ6IHVwbG9hZFJlc3BvbnNlLnByb2Nlc3NJZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkQ2FsbEJhY2sodXBsb2FkUmVzcG9uc2UuZGF0YSwgam9iLCB1cGxvYWRUeXBlKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVwbG9hZENhbGxiYWNrUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdXBsb2FkQ2FsbGJhY2tFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkQ2FsbGJhY2tFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVwbG9hZFJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrT3V0QXNzZXQoW2RlbGl2ZXJhYmxlLmFzc2V0LmFzc2V0X2lkXSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjaGVja091dEFzc2V0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkVmVyc2lvblZpYUhUVFAoZmlsZXMsIGV2ZW50RGF0YSwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2IgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc19pbnN0YW5jZV9pZDogdXBsb2FkUmVzcG9uc2UucHJvY2Vzc0lkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRDYWxsQmFjayh1cGxvYWRSZXNwb25zZS5kYXRhLCBqb2IsIHVwbG9hZFR5cGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRDYWxsYmFja0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkQ2FsbGJhY2tFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVwbG9hZEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkVmVyc2lvbnNWaWFRRFMoZmlsZXMsIGV2ZW50RGF0YSwgaXNSZXZpc2lvbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBqb2IgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc19pbnN0YW5jZV9pZDogdXBsb2FkUmVzcG9uc2UucHJvY2Vzc0lkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRDYWxsQmFjayh1cGxvYWRSZXNwb25zZS5kYXRhLCBqb2IsIHVwbG9hZFR5cGUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1cGxvYWRDYWxsYmFja1Jlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB1cGxvYWRDYWxsYmFja0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodXBsb2FkQ2FsbGJhY2tFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVwbG9hZEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKHVwbG9hZEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGNoZWNrT3V0QXNzZXRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihjaGVja091dEFzc2V0RXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdObyBmb2xkZXIgZm91bmQgdG8gdXBsb2FkIHRoZSBhc3NldChzKS4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZm9ybUFzc2V0TWV0YWRhdGFNb2RlbChkZWxpdmVyYWJsZURhdGEsIHByb2plY3REYXRhPzogYW55KSB7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGEgPSB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeUxldmVsRGV0YWlsc0J5VHlwZShNUE1fTEVWRUxTLkFTU0VUKTtcclxuICAgICAgICBjb25zdCBjdXJyU3RhdHVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxTdGF0dXNDb25maWcoKS5maW5kKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0YVsnU1RBVFVTX1RZUEUnXSA9PT0gU3RhdHVzVHlwZXMuSU5JVElBTCAmJiBkYXRhLlJfUE9fQ0FUQUdPUllfTEVWRUxbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkID09PSBtZXRhZGF0YVsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQ7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdEl0ZW1JZCA9IHByb2plY3REYXRhID8gcHJvamVjdERhdGFbJ1Byb2plY3QtaWQnXS5JdGVtSWQgOiAnJztcclxuICAgICAgICB0aGlzLmFzc2V0Q29uc3RhbnRzID0gZGVsaXZlcmFibGVEYXRhID8gQXNzZXRDb25zdGFudHMuQVNTRVRfVVBMT0FEX0ZJRUxEUyA6IHByb2plY3REYXRhID8gQXNzZXRDb25zdGFudHMuUkVGRVJFTkNFX0FTU0VUX1VQTE9BRF9GSUVMRFMgOiAnJztcclxuICAgICAgICBjb25zdCBtZXRhZGF0YUZpZWxkcyA9IHRoaXMuYXNzZXRDb25zdGFudHMubWFwKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBmaWVsZE5hbWUgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRJbmRleGVySWRCeU1hcHBlclZhbHVlKGVsZW1lbnQubWFwcGVyTmFtZSk7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBpZDogZWxlbWVudC5pZCxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5tZXRhZGF0YS5NZXRhZGF0YUZpZWxkJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBkZWxpdmVyYWJsZURhdGFbZmllbGROYW1lXSA/IGRlbGl2ZXJhYmxlRGF0YVtmaWVsZE5hbWVdIDogcHJvamVjdERhdGEgJiYgZmllbGROYW1lID09PSAnSUQnID8gcHJvamVjdEl0ZW1JZCA6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIG1ldGFkYXRhRmllbGRzLnB1c2goe1xyXG4gICAgICAgICAgICBpZDogJ01QTS5BU1NFVC5JU19DTE9ORUQnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFGaWVsZCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdzdHJpbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnZmFsc2UnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBtZXRhZGF0YUZpZWxkcy5wdXNoKHtcclxuICAgICAgICAgICAgaWQ6ICdNUE0uQVNTRVQuSVNfUkVGRVJFTkNFX0FTU0VUJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLm1ldGFkYXRhLk1ldGFkYXRhRmllbGQnLFxyXG4gICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3RyaW5nJyxcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogZGVsaXZlcmFibGVEYXRhID8gJ2ZhbHNlJyA6ICd0cnVlJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIG1ldGFkYXRhX2VsZW1lbnRfbGlzdDogW3tcclxuICAgICAgICAgICAgICAgIG1ldGFkYXRhX2VsZW1lbnRfbGlzdDogbWV0YWRhdGFGaWVsZHNcclxuICAgICAgICAgICAgfV0sXHJcbiAgICAgICAgICAgIG1ldGFkYXRhX21vZGVsX2lkOiAnTVBNLkFTU0VUJyxcclxuICAgICAgICAgICAgc2VjdXJpdHlfcG9saWN5X2xpc3Q6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogbWV0YWRhdGEuU0VDVVJUSVlfUE9MSUNZX0lEU1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19