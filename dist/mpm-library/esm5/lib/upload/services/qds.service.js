import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { GloabalConfig } from '../../mpm-utils/config/config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../mpm-utils/services/entity.appdef.service";
import * as i3 from "../../notification/notification.service";
var QdsService = /** @class */ (function () {
    function QdsService(http, entityAppDefService, notificationService) {
        this.http = http;
        this.entityAppDefService = entityAppDefService;
        this.notificationService = notificationService;
        this.inlineDownload = function (downloadParams) {
            downloadParams = downloadParams || {};
            //var notification = downloadParams.notification;
            var resolve, reject;
            var self = this;
            var inlineDownloadPromise = new Promise(function (res, rej) {
                resolve = res;
                reject = rej;
            });
            self.notificationService.info("Files are being downloaded via QDS client, Check the download status in file transfer window");
            this._qdsConnect.createDownloadJob(downloadParams.assetObjects, self.postInlineDownload.bind(this, resolve, reject));
            inlineDownloadPromise.then(function (downloadJob) {
                downloadJob.startDownload(undefined, undefined, self.downloadError.bind(self));
                downloadJob.detach();
                resolve();
            });
            return inlineDownloadPromise;
        };
    }
    QdsService.prototype.getQDSConnect = function () {
        return this._qdsConnect;
    };
    QdsService.prototype.setQDSConnect = function (qdsConnect) {
        this._qdsConnect = qdsConnect;
    };
    QdsService.prototype.getQDSSession = function (userSessionId) {
        var _this = this;
        return new Observable(function (observer) {
            var baseUrl = isDevMode() ? './' : _this.entityAppDefService.getOtmmAssetUploadDetails().url;
            var url = baseUrl + otmmServicesConstants.otmmapiBaseUrl +
                _this.entityAppDefService.getOtmmAssetUploadDetails().apiVersion + '/' + otmmServicesConstants.qdsSessionUrl;
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'X-OTMM-Locale': 'en_US',
                    'X-Requested-By': userSessionId.toString(),
                    'X-Requested-With': 'XMLHttpRequest'
                })
            };
            _this.http.post(url, null, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    QdsService.prototype.connectToQDS = function (qdsResource, isUpload) {
        var _this = this;
        return new Observable(function (observer) {
            var that = _this;
            if (qds_otmm.isClientConnected && !isUpload) { //
                otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                console.log('QDS client already connected.');
                observer.next('1000');
                observer.complete();
            }
            else {
                if (GloabalConfig.config.qdsVersion === '1.5.4') {
                    qds_otmm.qdsConnect(_this.entityAppDefService.getOtmmAssetUploadDetails().qdsServerURL, qdsResource.authentication_token, function (qdsConnector, statusCode, msg, installPref) {
                        that.setQDSConnect(qdsConnector);
                        if (statusCode === '200' && qdsConnector) {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            that.notificationService.info('Connected to QDS, All the uploads/downloads will use QDS tranfer');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1000') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            console.log('QDS client already connected');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1001' || statusCode === '1002' || statusCode === '1008') {
                            that.notificationService.error("Unable to connect to QDS,\n                      All the uploads/downloads will use basic tranfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1003' || statusCode === '1007') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.warn("You have chosen not to install QDS Transfer Manager plugin,\n                      All the uploads/downloads will use basic transfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1005') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.error("An unexpexted error occurred while connecting to QDS,\n                      All the uploads/downloads will use basic tranfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1006') {
                            that.notificationService.warn('QDS client requires update');
                            observer.next(statusCode);
                            observer.complete();
                        }
                    }, qdsResource.user_id, false);
                    observer.next('1000');
                    observer.complete();
                }
                else if (GloabalConfig.config.qdsVersion === '1.3.0') {
                    qds_otmm.qdsConnect(_this.entityAppDefService.getOtmmAssetUploadDetails().qdsServerURL, qdsResource.authentication_token, function (qdsConnector, statusCode) {
                        that.setQDSConnect(qdsConnector);
                        if (statusCode === '200' && qdsConnector) {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            that.notificationService.info('Connected to QDS, All the uploads/downloads will use QDS tranfer');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1000') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload = true;
                            console.log('QDS client already connected');
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1001' || statusCode === '1002') {
                            that.notificationService.error("Unable to connect to QDS,\n                          Hence forth all the uploads/downloads will use basic tranfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1003') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.warn("You have chosen not to install QDS Transfer Manager plugin,\n                          All the uploads/downloads will use basic transfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1005') {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient = false;
                            that.notificationService.error("An unexpexted error occurred while connecting to QDS,\n                          All the uploads/downloads will use basic tranfer");
                            observer.next(statusCode);
                            observer.complete();
                        }
                        else if (statusCode === '1006') {
                            that.notificationService.warn("QDS client requires update");
                            observer.next(statusCode);
                            observer.complete();
                        }
                    }, qdsResource.user_id);
                }
            }
        });
    };
    QdsService.prototype.getJobs = function (includeCompleted) {
        return new Observable(function (observer) {
            qds_otmm.connector.listJobs(function (jobs) {
                observer.next(jobs);
                observer.complete();
            }, includeCompleted);
        });
    };
    QdsService.prototype.createQDSImportJob = function (jobId, filePaths) {
        qds_otmm.connector.createImportJob('IMPORT_WORKING_AREA', jobId, function (currentJob) {
            currentJob.startImport();
            currentJob.detach();
        }, filePaths);
    };
    QdsService.prototype.toggleQDSImportJob = function (jobId, toggleToState) {
        return new Observable(function (observer) {
            qds_otmm.connector.reloadImportJob(jobId, function (response) {
                var importJob = response.importJob;
                var isRunning = response.jobInfo.running;
                if (toggleToState === 'PAUSE') {
                    if (isRunning) {
                        importJob.detach();
                        importJob.pauseImport(function (status) {
                            observer.next(status);
                            observer.complete();
                        });
                    }
                    else {
                        var job_status = {
                            'status': 'job paused'
                        };
                    }
                }
                else {
                    importJob.startImport(function (name, status) {
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        if (status === undefined) {
                            status = 'COMPLETE';
                        }
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        observer.next(status);
                        observer.complete();
                    });
                }
            });
        });
    };
    QdsService.prototype.toggleQDSExportJob = function (jobId, toggleToState) {
        return new Observable(function (observer) {
            qds_otmm.connector.reloadExportJob(jobId, function (response) {
                var exportJob = response.exportJob;
                var isRunning = response.jobInfo.running;
                if (toggleToState === 'PAUSE') {
                    if (isRunning) {
                        exportJob.detach();
                        exportJob.pauseExport(function (status) {
                            observer.next(status);
                            observer.complete();
                        });
                    }
                    else {
                        var job_status = {
                            'status': 'job paused'
                        };
                    }
                }
                else {
                    exportJob.startExport(function (name, status) {
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        if (status === undefined) {
                            status = 'COMPLETE';
                        }
                        observer.next(status);
                        observer.complete();
                    }, function (name, status) {
                        observer.next(status);
                        observer.complete();
                    });
                }
            });
        });
    };
    QdsService.prototype.retryFailedJob = function (job) {
        return new Observable(function (observer) {
            var isImportJob = job.isImport;
            var currJobId = {
                'otmmJobId': job.otmmJobId,
                'qdsJobId': job.qdsJobId
            };
            if (isImportJob) {
                qds_otmm.connector.reloadImportJob(currJobId, function (response) {
                    var currentJob = response.importJob;
                    currentJob.detach();
                    currentJob.startImport();
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                qds_otmm.connector.reloadExportJob(currJobId, function (response) {
                    var currentJob = response.exportJob;
                    currentJob.detach();
                    currentJob.startExport();
                    observer.next(true);
                    observer.complete();
                });
            }
        });
    };
    QdsService.prototype.download = function (jobId, files) {
        return new Observable(function (observer) {
            qds_otmm.connector.createExportJob('DEFAULT_EXPORT_AREA', jobId, function (exportJob) {
                exportJob.startExport(undefined, undefined, function () {
                    observer.next('Unable to download files via QDS.');
                });
                exportJob.detach();
                observer.next('Files are being downloaded via QDS client. Check the download status in file transfer window.');
                observer.complete();
            }, files);
        });
    };
    QdsService.prototype.getDownloadFolder = function () {
        return new Observable(function (observer) {
            qds_otmm.connector.getDownloadFolder(function (downloadLocation) {
                observer.next(downloadLocation);
                observer.complete();
            });
        });
    };
    QdsService.prototype.chooseDownloadFolder = function () {
        return new Observable(function (observer) {
            qds_otmm.connector.chooseDownloadFolder(function (downloadLocation) {
                observer.next(downloadLocation);
                observer.complete();
            });
        });
    };
    QdsService.prototype.getFileTransferProgress = function (size, transferred) {
        return Math.round(transferred / size * 100);
    };
    QdsService.prototype.postInlineDownload = function (resolve, reject, downloadJob) {
        //this.currentJob = downloadJob;
        resolve(downloadJob);
    };
    ;
    QdsService.prototype.downloadError = function (errorHandler) {
        if (errorHandler instanceof Function)
            errorHandler();
        this.notificationService.info('Unable to download files via QDS');
    };
    ;
    QdsService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: EntityAppDefService },
        { type: NotificationService }
    ]; };
    QdsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function QdsService_Factory() { return new QdsService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.NotificationService)); }, token: QdsService, providedIn: "root" });
    QdsService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], QdsService);
    return QdsService;
}());
export { QdsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3RELE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDL0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sK0JBQStCLENBQUM7Ozs7O0FBTzlEO0lBSUUsb0JBQ1MsSUFBZ0IsRUFDaEIsbUJBQXdDLEVBQ3hDLG1CQUF3QztRQUZ4QyxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQXVSakQsbUJBQWMsR0FBRyxVQUFVLGNBQWM7WUFDdkMsY0FBYyxHQUFHLGNBQWMsSUFBSSxFQUFFLENBQUM7WUFDdEMsaURBQWlEO1lBQ2pELElBQUksT0FBTyxFQUFFLE1BQU0sQ0FBQztZQUNwQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRSxHQUFHO2dCQUN4RCxPQUFPLEdBQUcsR0FBRyxDQUFDO2dCQUNkLE1BQU0sR0FBRyxHQUFHLENBQUM7WUFDZixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsOEZBQThGLENBQUMsQ0FBQztZQUM5SCxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDckgscUJBQXFCLENBQUMsSUFBSSxDQUFDLFVBQVUsV0FBZ0I7Z0JBQ25ELFdBQVcsQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUMvRSxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxDQUFDO1lBQ1osQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLHFCQUFxQixDQUFDO1FBQy9CLENBQUMsQ0FBQTtJQXRTRyxDQUFDO0lBRUwsa0NBQWEsR0FBYjtRQUNFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUMxQixDQUFDO0lBRUQsa0NBQWEsR0FBYixVQUFjLFVBQVU7UUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUM7SUFDaEMsQ0FBQztJQUVELGtDQUFhLEdBQWIsVUFBYyxhQUFhO1FBQTNCLGlCQXdCQztRQXZCQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDOUYsSUFBTSxHQUFHLEdBQUcsT0FBTyxHQUFHLHFCQUFxQixDQUFDLGNBQWM7Z0JBQ3hELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcscUJBQXFCLENBQUMsYUFBYSxDQUFDO1lBRTlHLElBQU0sV0FBVyxHQUFHO2dCQUNsQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUN2QixjQUFjLEVBQUUsa0RBQWtEO29CQUNsRSxlQUFlLEVBQUUsT0FBTztvQkFDeEIsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDLFFBQVEsRUFBRTtvQkFDMUMsa0JBQWtCLEVBQUUsZ0JBQWdCO2lCQUNyQyxDQUFDO2FBQ0gsQ0FBQztZQUVGLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDO2lCQUNuQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDTixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsaUNBQVksR0FBWixVQUFhLFdBQWdCLEVBQUUsUUFBUztRQUF4QyxpQkF3RkM7UUF2RkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDO1lBQ2xCLElBQUksUUFBUSxDQUFDLGlCQUFpQixJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsRUFBRTtnQkFDOUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO2dCQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsSUFBSSxhQUFhLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxPQUFPLEVBQUU7b0JBQy9DLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLHlCQUF5QixFQUFFLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxvQkFBb0IsRUFDckgsVUFBVSxZQUFpQixFQUFFLFVBQWUsRUFBRSxHQUFRLEVBQUUsV0FBZ0I7d0JBQ3RFLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQ2pDLElBQUksVUFBVSxLQUFLLEtBQUssSUFBSSxZQUFZLEVBQUU7NEJBQ3hDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7NEJBQ2hFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsa0VBQWtFLENBQUMsQ0FBQzs0QkFDbEcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7NEJBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQzs0QkFDNUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUNsRixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1HQUN3QixDQUFDLENBQUM7NEJBQ3pELFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ3pELHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7NEJBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsc0lBQzBCLENBQUMsQ0FBQzs0QkFDMUQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7NEJBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0hBQ3dCLENBQUMsQ0FBQzs0QkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjs2QkFBTSxJQUFJLFVBQVUsS0FBSyxNQUFNLEVBQUU7NEJBQ2hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQzs0QkFDNUQsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUNyQjtvQkFDSCxDQUFDLEVBQUUsV0FBVyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNyQjtxQkFBTSxJQUFJLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtvQkFDdEQsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLG9CQUFvQixFQUNySCxVQUFVLFlBQWlCLEVBQUUsVUFBZTt3QkFDMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxVQUFVLEtBQUssS0FBSyxJQUFJLFlBQVksRUFBRTs0QkFDeEMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzs0QkFDaEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxrRUFBa0UsQ0FBQyxDQUFDOzRCQUNsRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sRUFBRTs0QkFDaEMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzs0QkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDOzRCQUM1QyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUMxQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3JCOzZCQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUN6RCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1IQUN3QyxDQUFDLENBQUM7NEJBQ3pFLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUNoQyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDOzRCQUNuRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLDBJQUM4QixDQUFDLENBQUM7NEJBQzlELFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUNoQyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDOzRCQUNuRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1JQUM0QixDQUFDLENBQUM7NEJBQzdELFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7NkJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFOzRCQUNoQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLENBQUM7NEJBQzVELFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDckI7b0JBQ0gsQ0FBQyxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDM0I7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUFPLEdBQVAsVUFBUSxnQkFBZ0I7UUFDdEIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsVUFBVSxJQUFTO2dCQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUNBQWtCLEdBQWxCLFVBQW1CLEtBQUssRUFBRSxTQUFTO1FBQ2pDLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxVQUFVLFVBQVU7WUFDbkYsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pCLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDaEIsQ0FBQztJQUVELHVDQUFrQixHQUFsQixVQUFtQixLQUFLLEVBQUUsYUFBYTtRQUNyQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixRQUFRLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsVUFBVSxRQUFhO2dCQUMvRCxJQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO2dCQUNyQyxJQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztnQkFDM0MsSUFBSSxhQUFhLEtBQUssT0FBTyxFQUFFO29CQUM3QixJQUFJLFNBQVMsRUFBRTt3QkFDYixTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7d0JBQ25CLFNBQVMsQ0FBQyxXQUFXLENBQUMsVUFBVSxNQUFNOzRCQUNwQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ3RCLENBQUMsQ0FBQyxDQUFDO3FCQUNKO3lCQUFNO3dCQUNMLElBQU0sVUFBVSxHQUFHOzRCQUNqQixRQUFRLEVBQUUsWUFBWTt5QkFDdkIsQ0FBQztxQkFDSDtpQkFDRjtxQkFBTTtvQkFDTCxTQUFTLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFBSSxFQUFFLE1BQU07d0JBQzFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxFQUFFLFVBQVUsSUFBSSxFQUFFLE1BQU07d0JBQ3ZCLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTs0QkFDeEIsTUFBTSxHQUFHLFVBQVUsQ0FBQzt5QkFDckI7d0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN0QixDQUFDLEVBQUUsVUFBVSxJQUFJLEVBQUUsTUFBTTt3QkFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN0QixDQUFDLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUNBQWtCLEdBQWxCLFVBQW1CLEtBQUssRUFBRSxhQUFhO1FBQ3JDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxVQUFVLFFBQWE7Z0JBQy9ELElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQ3JDLElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO2dCQUMzQyxJQUFJLGFBQWEsS0FBSyxPQUFPLEVBQUU7b0JBQzdCLElBQUksU0FBUyxFQUFFO3dCQUNiLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFDbkIsU0FBUyxDQUFDLFdBQVcsQ0FBQyxVQUFVLE1BQU07NEJBQ3BDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDdEIsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsSUFBTSxVQUFVLEdBQUc7NEJBQ2pCLFFBQVEsRUFBRSxZQUFZO3lCQUN2QixDQUFDO3FCQUNIO2lCQUNGO3FCQUFNO29CQUNMLFNBQVMsQ0FBQyxXQUFXLENBQUMsVUFBVSxJQUFJLEVBQUUsTUFBTTt3QkFDMUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN0QixDQUFDLEVBQUUsVUFBVSxJQUFJLEVBQUUsTUFBTTt3QkFDdkIsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFOzRCQUN4QixNQUFNLEdBQUcsVUFBVSxDQUFDO3lCQUNyQjt3QkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3RCLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxNQUFNO3dCQUN2QixRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxtQ0FBYyxHQUFkLFVBQWUsR0FBUTtRQUNyQixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFNLFdBQVcsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO1lBQ2pDLElBQU0sU0FBUyxHQUFHO2dCQUNoQixXQUFXLEVBQUUsR0FBRyxDQUFDLFNBQVM7Z0JBQzFCLFVBQVUsRUFBRSxHQUFHLENBQUMsUUFBUTthQUN6QixDQUFDO1lBQ0YsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsUUFBUSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLFVBQVUsUUFBUTtvQkFDOUQsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQztvQkFDdEMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUNwQixVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxRQUFRLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxRQUFRO29CQUM5RCxJQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO29CQUN0QyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ3BCLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkJBQVEsR0FBUixVQUFTLEtBQUssRUFBRSxLQUFLO1FBQ25CLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLFFBQVEsQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxVQUFVLFNBQVM7Z0JBQ2xGLFNBQVMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRTtvQkFDMUMsUUFBUSxDQUFDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUNyRCxDQUFDLENBQUMsQ0FBQztnQkFDSCxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBRW5CLFFBQVEsQ0FBQyxJQUFJLENBQUMsK0ZBQStGLENBQUMsQ0FBQztnQkFDL0csUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNaLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNDQUFpQixHQUFqQjtRQUNFLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLFFBQVEsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsVUFBVSxnQkFBZ0I7Z0JBQzdELFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDaEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQseUNBQW9CLEdBQXBCO1FBQ0UsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsUUFBUSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLGdCQUFnQjtnQkFDaEUsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNoQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBdUIsR0FBdkIsVUFBd0IsSUFBWSxFQUFFLFdBQW1CO1FBQ3ZELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFxQkQsdUNBQWtCLEdBQWxCLFVBQW1CLE9BQU8sRUFBRSxNQUFNLEVBQUUsV0FBVztRQUM3QyxnQ0FBZ0M7UUFDaEMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFBQSxDQUFDO0lBRUYsa0NBQWEsR0FBYixVQUFjLFlBQVk7UUFDeEIsSUFBSSxZQUFZLFlBQVksUUFBUTtZQUNsQyxZQUFZLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7SUFFcEUsQ0FBQztJQUFBLENBQUM7O2dCQXRUYSxVQUFVO2dCQUNLLG1CQUFtQjtnQkFDbkIsbUJBQW1COzs7SUFQdEMsVUFBVTtRQUh0QixVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csVUFBVSxDQTRUdEI7cUJBelVEO0NBeVVDLEFBNVRELElBNFRDO1NBNVRZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBpc0Rldk1vZGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL2NvbmZpZyc7XHJcblxyXG5kZWNsYXJlIGNvbnN0IHFkc19vdG1tOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBRZHNTZXJ2aWNlIHtcclxuXHJcbiAgcHVibGljIF9xZHNDb25uZWN0O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG5cclxuICApIHsgfVxyXG5cclxuICBnZXRRRFNDb25uZWN0KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuX3Fkc0Nvbm5lY3Q7XHJcbiAgfVxyXG5cclxuICBzZXRRRFNDb25uZWN0KHFkc0Nvbm5lY3QpIHtcclxuICAgIHRoaXMuX3Fkc0Nvbm5lY3QgPSBxZHNDb25uZWN0O1xyXG4gIH1cclxuXHJcbiAgZ2V0UURTU2Vzc2lvbih1c2VyU2Vzc2lvbklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGNvbnN0IGJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0T3RtbUFzc2V0VXBsb2FkRGV0YWlscygpLnVybDtcclxuICAgICAgY29uc3QgdXJsID0gYmFzZVVybCArIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5vdG1tYXBpQmFzZVVybCArXHJcbiAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldE90bW1Bc3NldFVwbG9hZERldGFpbHMoKS5hcGlWZXJzaW9uICsgJy8nICsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLnFkc1Nlc3Npb25Vcmw7XHJcblxyXG4gICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkOyBjaGFyc2V0PVVURi04JyxcclxuICAgICAgICAgICdYLU9UTU0tTG9jYWxlJzogJ2VuX1VTJyxcclxuICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IHVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICdYLVJlcXVlc3RlZC1XaXRoJzogJ1hNTEh0dHBSZXF1ZXN0J1xyXG4gICAgICAgIH0pXHJcbiAgICAgIH07XHJcblxyXG4gICAgICB0aGlzLmh0dHAucG9zdCh1cmwsIG51bGwsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY29ubmVjdFRvUURTKHFkc1Jlc291cmNlOiBhbnksIGlzVXBsb2FkPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCB0aGF0ID0gdGhpcztcclxuICAgICAgaWYgKHFkc19vdG1tLmlzQ2xpZW50Q29ubmVjdGVkICYmICFpc1VwbG9hZCkgey8vXHJcbiAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgPSB0cnVlO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdRRFMgY2xpZW50IGFscmVhZHkgY29ubmVjdGVkLicpO1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQoJzEwMDAnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlmIChHbG9hYmFsQ29uZmlnLmNvbmZpZy5xZHNWZXJzaW9uID09PSAnMS41LjQnKSB7XHJcbiAgICAgICAgICBxZHNfb3RtbS5xZHNDb25uZWN0KHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRPdG1tQXNzZXRVcGxvYWREZXRhaWxzKCkucWRzU2VydmVyVVJMLCBxZHNSZXNvdXJjZS5hdXRoZW50aWNhdGlvbl90b2tlbixcclxuICAgICAgICAgICAgZnVuY3Rpb24gKHFkc0Nvbm5lY3RvcjogYW55LCBzdGF0dXNDb2RlOiBhbnksIG1zZzogYW55LCBpbnN0YWxsUHJlZjogYW55KSB7XHJcbiAgICAgICAgICAgICAgdGhhdC5zZXRRRFNDb25uZWN0KHFkc0Nvbm5lY3Rvcik7XHJcbiAgICAgICAgICAgICAgaWYgKHN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHFkc0Nvbm5lY3Rvcikge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ0Nvbm5lY3RlZCB0byBRRFMsIEFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgUURTIHRyYW5mZXInKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDAnKSB7XHJcbiAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnUURTIGNsaWVudCBhbHJlYWR5IGNvbm5lY3RlZCcpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwMScgfHwgc3RhdHVzQ29kZSA9PT0gJzEwMDInIHx8IHN0YXR1c0NvZGUgPT09ICcxMDA4Jykge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBVbmFibGUgdG8gY29ubmVjdCB0byBRRFMsXHJcbiAgICAgICAgICAgICAgICAgICAgICBBbGwgdGhlIHVwbG9hZHMvZG93bmxvYWRzIHdpbGwgdXNlIGJhc2ljIHRyYW5mZXJgKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDMnIHx8IHN0YXR1c0NvZGUgPT09ICcxMDA3Jykge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLndhcm4oYFlvdSBoYXZlIGNob3NlbiBub3QgdG8gaW5zdGFsbCBRRFMgVHJhbnNmZXIgTWFuYWdlciBwbHVnaW4sXHJcbiAgICAgICAgICAgICAgICAgICAgICBBbGwgdGhlIHVwbG9hZHMvZG93bmxvYWRzIHdpbGwgdXNlIGJhc2ljIHRyYW5zZmVyYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDA1Jykge1xyXG4gICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBBbiB1bmV4cGV4dGVkIGVycm9yIG9jY3VycmVkIHdoaWxlIGNvbm5lY3RpbmcgdG8gUURTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgQWxsIHRoZSB1cGxvYWRzL2Rvd25sb2FkcyB3aWxsIHVzZSBiYXNpYyB0cmFuZmVyYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDA2Jykge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLndhcm4oJ1FEUyBjbGllbnQgcmVxdWlyZXMgdXBkYXRlJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIHFkc1Jlc291cmNlLnVzZXJfaWQsIGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoJzEwMDAnKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChHbG9hYmFsQ29uZmlnLmNvbmZpZy5xZHNWZXJzaW9uID09PSAnMS4zLjAnKSB7XHJcbiAgICAgICAgICBxZHNfb3RtbS5xZHNDb25uZWN0KHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRPdG1tQXNzZXRVcGxvYWREZXRhaWxzKCkucWRzU2VydmVyVVJMLCBxZHNSZXNvdXJjZS5hdXRoZW50aWNhdGlvbl90b2tlbixcclxuICAgICAgICAgICAgZnVuY3Rpb24gKHFkc0Nvbm5lY3RvcjogYW55LCBzdGF0dXNDb2RlOiBhbnkpIHtcclxuICAgICAgICAgICAgICB0aGF0LnNldFFEU0Nvbm5lY3QocWRzQ29ubmVjdG9yKTtcclxuICAgICAgICAgICAgICBpZiAoc3RhdHVzQ29kZSA9PT0gJzIwMCcgJiYgcWRzQ29ubmVjdG9yKSB7XHJcbiAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5pc1FEU1VwbG9hZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGF0Lm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnQ29ubmVjdGVkIHRvIFFEUywgQWxsIHRoZSB1cGxvYWRzL2Rvd25sb2FkcyB3aWxsIHVzZSBRRFMgdHJhbmZlcicpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwMCcpIHtcclxuICAgICAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmlzUURTVXBsb2FkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdRRFMgY2xpZW50IGFscmVhZHkgY29ubmVjdGVkJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDAxJyB8fCBzdGF0dXNDb2RlID09PSAnMTAwMicpIHtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgVW5hYmxlIHRvIGNvbm5lY3QgdG8gUURTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIEhlbmNlIGZvcnRoIGFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgYmFzaWMgdHJhbmZlcmApO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNDb2RlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdGF0dXNDb2RlID09PSAnMTAwMycpIHtcclxuICAgICAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLmZpbmRRRFNDbGllbnQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoYXQubm90aWZpY2F0aW9uU2VydmljZS53YXJuKGBZb3UgaGF2ZSBjaG9zZW4gbm90IHRvIGluc3RhbGwgUURTIFRyYW5zZmVyIE1hbmFnZXIgcGx1Z2luLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIEFsbCB0aGUgdXBsb2Fkcy9kb3dubG9hZHMgd2lsbCB1c2UgYmFzaWMgdHJhbnNmZXJgKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzQ29kZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3RhdHVzQ29kZSA9PT0gJzEwMDUnKSB7XHJcbiAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5maW5kUURTQ2xpZW50ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGF0Lm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoYEFuIHVuZXhwZXh0ZWQgZXJyb3Igb2NjdXJyZWQgd2hpbGUgY29ubmVjdGluZyB0byBRRFMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgQWxsIHRoZSB1cGxvYWRzL2Rvd25sb2FkcyB3aWxsIHVzZSBiYXNpYyB0cmFuZmVyYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHN0YXR1c0NvZGUgPT09ICcxMDA2Jykge1xyXG4gICAgICAgICAgICAgICAgdGhhdC5ub3RpZmljYXRpb25TZXJ2aWNlLndhcm4oYFFEUyBjbGllbnQgcmVxdWlyZXMgdXBkYXRlYCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1c0NvZGUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIHFkc1Jlc291cmNlLnVzZXJfaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRKb2JzKGluY2x1ZGVDb21wbGV0ZWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgcWRzX290bW0uY29ubmVjdG9yLmxpc3RKb2JzKGZ1bmN0aW9uIChqb2JzOiBhbnkpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KGpvYnMpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0sIGluY2x1ZGVDb21wbGV0ZWQpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVRRFNJbXBvcnRKb2Ioam9iSWQsIGZpbGVQYXRocykge1xyXG4gICAgcWRzX290bW0uY29ubmVjdG9yLmNyZWF0ZUltcG9ydEpvYignSU1QT1JUX1dPUktJTkdfQVJFQScsIGpvYklkLCBmdW5jdGlvbiAoY3VycmVudEpvYikge1xyXG4gICAgICBjdXJyZW50Sm9iLnN0YXJ0SW1wb3J0KCk7XHJcbiAgICAgIGN1cnJlbnRKb2IuZGV0YWNoKCk7XHJcbiAgICB9LCBmaWxlUGF0aHMpO1xyXG4gIH1cclxuXHJcbiAgdG9nZ2xlUURTSW1wb3J0Sm9iKGpvYklkLCB0b2dnbGVUb1N0YXRlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5yZWxvYWRJbXBvcnRKb2Ioam9iSWQsIGZ1bmN0aW9uIChyZXNwb25zZTogYW55KSB7XHJcbiAgICAgICAgY29uc3QgaW1wb3J0Sm9iID0gcmVzcG9uc2UuaW1wb3J0Sm9iO1xyXG4gICAgICAgIGNvbnN0IGlzUnVubmluZyA9IHJlc3BvbnNlLmpvYkluZm8ucnVubmluZztcclxuICAgICAgICBpZiAodG9nZ2xlVG9TdGF0ZSA9PT0gJ1BBVVNFJykge1xyXG4gICAgICAgICAgaWYgKGlzUnVubmluZykge1xyXG4gICAgICAgICAgICBpbXBvcnRKb2IuZGV0YWNoKCk7XHJcbiAgICAgICAgICAgIGltcG9ydEpvYi5wYXVzZUltcG9ydChmdW5jdGlvbiAoc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXMpO1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3Qgam9iX3N0YXR1cyA9IHtcclxuICAgICAgICAgICAgICAnc3RhdHVzJzogJ2pvYiBwYXVzZWQnXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGltcG9ydEpvYi5zdGFydEltcG9ydChmdW5jdGlvbiAobmFtZSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0sIGZ1bmN0aW9uIChuYW1lLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgc3RhdHVzID0gJ0NPTVBMRVRFJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBmdW5jdGlvbiAobmFtZSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZVFEU0V4cG9ydEpvYihqb2JJZCwgdG9nZ2xlVG9TdGF0ZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBxZHNfb3RtbS5jb25uZWN0b3IucmVsb2FkRXhwb3J0Sm9iKGpvYklkLCBmdW5jdGlvbiAocmVzcG9uc2U6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGV4cG9ydEpvYiA9IHJlc3BvbnNlLmV4cG9ydEpvYjtcclxuICAgICAgICBjb25zdCBpc1J1bm5pbmcgPSByZXNwb25zZS5qb2JJbmZvLnJ1bm5pbmc7XHJcbiAgICAgICAgaWYgKHRvZ2dsZVRvU3RhdGUgPT09ICdQQVVTRScpIHtcclxuICAgICAgICAgIGlmIChpc1J1bm5pbmcpIHtcclxuICAgICAgICAgICAgZXhwb3J0Sm9iLmRldGFjaCgpO1xyXG4gICAgICAgICAgICBleHBvcnRKb2IucGF1c2VFeHBvcnQoZnVuY3Rpb24gKHN0YXR1cykge1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzKTtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGpvYl9zdGF0dXMgPSB7XHJcbiAgICAgICAgICAgICAgJ3N0YXR1cyc6ICdqb2IgcGF1c2VkJ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBleHBvcnRKb2Iuc3RhcnRFeHBvcnQoZnVuY3Rpb24gKG5hbWUsIHN0YXR1cykge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBmdW5jdGlvbiAobmFtZSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgIHN0YXR1cyA9ICdDT01QTEVURSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXMpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZnVuY3Rpb24gKG5hbWUsIHN0YXR1cykge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHN0YXR1cyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICByZXRyeUZhaWxlZEpvYihqb2I6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBpc0ltcG9ydEpvYiA9IGpvYi5pc0ltcG9ydDtcclxuICAgICAgY29uc3QgY3VyckpvYklkID0ge1xyXG4gICAgICAgICdvdG1tSm9iSWQnOiBqb2Iub3RtbUpvYklkLFxyXG4gICAgICAgICdxZHNKb2JJZCc6IGpvYi5xZHNKb2JJZFxyXG4gICAgICB9O1xyXG4gICAgICBpZiAoaXNJbXBvcnRKb2IpIHtcclxuICAgICAgICBxZHNfb3RtbS5jb25uZWN0b3IucmVsb2FkSW1wb3J0Sm9iKGN1cnJKb2JJZCwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyZW50Sm9iID0gcmVzcG9uc2UuaW1wb3J0Sm9iO1xyXG4gICAgICAgICAgY3VycmVudEpvYi5kZXRhY2goKTtcclxuICAgICAgICAgIGN1cnJlbnRKb2Iuc3RhcnRJbXBvcnQoKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5yZWxvYWRFeHBvcnRKb2IoY3VyckpvYklkLCBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgIGNvbnN0IGN1cnJlbnRKb2IgPSByZXNwb25zZS5leHBvcnRKb2I7XHJcbiAgICAgICAgICBjdXJyZW50Sm9iLmRldGFjaCgpO1xyXG4gICAgICAgICAgY3VycmVudEpvYi5zdGFydEV4cG9ydCgpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZG93bmxvYWQoam9iSWQsIGZpbGVzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHFkc19vdG1tLmNvbm5lY3Rvci5jcmVhdGVFeHBvcnRKb2IoJ0RFRkFVTFRfRVhQT1JUX0FSRUEnLCBqb2JJZCwgZnVuY3Rpb24gKGV4cG9ydEpvYikge1xyXG4gICAgICAgIGV4cG9ydEpvYi5zdGFydEV4cG9ydCh1bmRlZmluZWQsIHVuZGVmaW5lZCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnVW5hYmxlIHRvIGRvd25sb2FkIGZpbGVzIHZpYSBRRFMuJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZXhwb3J0Sm9iLmRldGFjaCgpO1xyXG5cclxuICAgICAgICBvYnNlcnZlci5uZXh0KCdGaWxlcyBhcmUgYmVpbmcgZG93bmxvYWRlZCB2aWEgUURTIGNsaWVudC4gQ2hlY2sgdGhlIGRvd25sb2FkIHN0YXR1cyBpbiBmaWxlIHRyYW5zZmVyIHdpbmRvdy4nKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBmaWxlcyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldERvd25sb2FkRm9sZGVyKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBxZHNfb3RtbS5jb25uZWN0b3IuZ2V0RG93bmxvYWRGb2xkZXIoZnVuY3Rpb24gKGRvd25sb2FkTG9jYXRpb24pIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KGRvd25sb2FkTG9jYXRpb24pO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjaG9vc2VEb3dubG9hZEZvbGRlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgcWRzX290bW0uY29ubmVjdG9yLmNob29zZURvd25sb2FkRm9sZGVyKGZ1bmN0aW9uIChkb3dubG9hZExvY2F0aW9uKSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChkb3dubG9hZExvY2F0aW9uKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmlsZVRyYW5zZmVyUHJvZ3Jlc3Moc2l6ZTogbnVtYmVyLCB0cmFuc2ZlcnJlZDogbnVtYmVyKTogbnVtYmVyIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKHRyYW5zZmVycmVkIC8gc2l6ZSAqIDEwMCk7XHJcbiAgfVxyXG5cclxuICBpbmxpbmVEb3dubG9hZCA9IGZ1bmN0aW9uIChkb3dubG9hZFBhcmFtcykge1xyXG4gICAgZG93bmxvYWRQYXJhbXMgPSBkb3dubG9hZFBhcmFtcyB8fCB7fTtcclxuICAgIC8vdmFyIG5vdGlmaWNhdGlvbiA9IGRvd25sb2FkUGFyYW1zLm5vdGlmaWNhdGlvbjtcclxuICAgIHZhciByZXNvbHZlLCByZWplY3Q7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICB2YXIgaW5saW5lRG93bmxvYWRQcm9taXNlID0gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlcywgcmVqKSB7XHJcbiAgICAgIHJlc29sdmUgPSByZXM7XHJcbiAgICAgIHJlamVjdCA9IHJlajtcclxuICAgIH0pO1xyXG4gICAgc2VsZi5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oXCJGaWxlcyBhcmUgYmVpbmcgZG93bmxvYWRlZCB2aWEgUURTIGNsaWVudCwgQ2hlY2sgdGhlIGRvd25sb2FkIHN0YXR1cyBpbiBmaWxlIHRyYW5zZmVyIHdpbmRvd1wiKTtcclxuICAgIHRoaXMuX3Fkc0Nvbm5lY3QuY3JlYXRlRG93bmxvYWRKb2IoZG93bmxvYWRQYXJhbXMuYXNzZXRPYmplY3RzLCBzZWxmLnBvc3RJbmxpbmVEb3dubG9hZC5iaW5kKHRoaXMsIHJlc29sdmUsIHJlamVjdCkpO1xyXG4gICAgaW5saW5lRG93bmxvYWRQcm9taXNlLnRoZW4oZnVuY3Rpb24gKGRvd25sb2FkSm9iOiBhbnkpIHtcclxuICAgICAgZG93bmxvYWRKb2Iuc3RhcnREb3dubG9hZCh1bmRlZmluZWQsIHVuZGVmaW5lZCwgc2VsZi5kb3dubG9hZEVycm9yLmJpbmQoc2VsZikpO1xyXG4gICAgICBkb3dubG9hZEpvYi5kZXRhY2goKTtcclxuICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gaW5saW5lRG93bmxvYWRQcm9taXNlO1xyXG4gIH1cclxuXHJcbiAgcG9zdElubGluZURvd25sb2FkKHJlc29sdmUsIHJlamVjdCwgZG93bmxvYWRKb2IpIHtcclxuICAgIC8vdGhpcy5jdXJyZW50Sm9iID0gZG93bmxvYWRKb2I7XHJcbiAgICByZXNvbHZlKGRvd25sb2FkSm9iKTtcclxuICB9O1xyXG5cclxuICBkb3dubG9hZEVycm9yKGVycm9ySGFuZGxlcikge1xyXG4gICAgaWYgKGVycm9ySGFuZGxlciBpbnN0YW5jZW9mIEZ1bmN0aW9uKVxyXG4gICAgICBlcnJvckhhbmRsZXIoKTtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdVbmFibGUgdG8gZG93bmxvYWQgZmlsZXMgdmlhIFFEUycpO1xyXG5cclxuICB9O1xyXG59XHJcbiJdfQ==