import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var AssetFileConfigService = /** @class */ (function () {
    function AssetFileConfigService() {
    }
    AssetFileConfigService.prototype.findIconByName = function (fileName) {
        var unknownIcon = 'insert_drive_file';
        var documentIcon = 'description';
        var imageIcon = 'image';
        var videoIcon = 'movie_creation';
        var pdfIcon = 'picture_as_pdf';
        var gifIcon = 'gif';
        var zipIcon = 'archive';
        if (!fileName || fileName != null && fileName !== '') {
            var fileRegx = /(?:\.([^.]+))?$/;
            var fileExt = fileRegx.exec(fileName)[1];
            if (fileExt && fileExt != null && fileExt !== '') {
                fileExt = fileExt.toUpperCase();
            }
            if (fileExt === 'PDF') {
                return pdfIcon;
            }
            else if (fileExt === 'DOC' || fileExt === 'DOCX') {
                return documentIcon;
            }
            else if (fileExt === 'XLS' || fileExt === 'XLSX') {
                return documentIcon;
            }
            else if (fileExt === 'JPG' || fileExt === 'JPEG' || fileExt === 'PNG') {
                return imageIcon;
            }
            else if (fileExt === 'GIF') {
                return gifIcon;
            }
            else if (fileExt === 'MP4' || fileExt === 'AVI' || fileExt === 'MKV'
                || fileExt === 'FLV' || fileExt === 'WMV' || fileExt === 'MPEG' || fileExt === '3GP') {
                return videoIcon;
            }
            else if (fileExt === 'TXT') {
                return documentIcon;
            }
            else if (fileExt === 'MP3' || fileExt === 'AAC' || fileExt === 'FLAC'
                || fileExt === 'M4A' || fileExt === 'WAV' || fileExt === 'WMA') {
                return videoIcon;
            }
            else if (fileExt === 'EPS') {
                return documentIcon;
            }
            else if (fileExt === 'ZIP') {
                return zipIcon;
            }
            else {
                return unknownIcon;
            }
        }
        else {
            return unknownIcon;
        }
    };
    AssetFileConfigService.prototype.getfileFormatObject = function (filename) {
        var fileFormatObject;
        if (filename != null && filename !== '') {
            var fileRegx = /(?:\.([^.]+))?$/;
            var fileExt = fileRegx.exec(filename)[1];
            if (fileExt != null && fileExt !== '') {
                fileExt = fileExt.toUpperCase();
            }
            if (fileExt === 'PDF') {
                fileFormatObject = 'Acrobat';
                return fileFormatObject;
            }
            else if (fileExt === 'DOC' || fileExt === 'DOCX' || fileExt === 'XLS' || fileExt === 'XLSX' ||
                fileExt === 'PPT' || fileExt === 'PPTX') {
                fileFormatObject = 'Microsoft Office';
                return fileFormatObject;
            }
            else if (fileExt === 'JPG' || fileExt === 'JPEG' || fileExt === 'JPE' || fileExt === 'TIFF' ||
                fileExt === 'TIF' || fileExt === 'EPS' || fileExt === 'AI' || fileExt === 'PSD') {
                fileFormatObject = 'Image';
                return fileFormatObject;
            }
            else if (fileExt === 'GIF') {
                fileFormatObject = 'GIF';
                return fileFormatObject;
            }
            else if (fileExt === 'PNG') {
                fileFormatObject = 'PNG';
                return fileFormatObject;
            }
            else if (fileExt === 'BMP') {
                fileFormatObject = 'BMP';
                return fileFormatObject;
            }
            else if (fileExt === 'MP4' || fileExt === 'AVI' || fileExt === 'MKV' || fileExt === 'FLV' ||
                fileExt === 'WMV' || fileExt === 'MPEG' || fileExt === '3GP' || fileExt === 'MOV') {
                fileFormatObject = 'VIDEO';
                return fileFormatObject;
            }
            else if (fileExt === 'TXT') {
                fileFormatObject = 'TXT';
                return fileFormatObject;
            }
            else if (fileExt === 'EML') {
                fileFormatObject = 'EML';
                return fileFormatObject;
            }
            else if (fileExt === 'RTX') {
                fileFormatObject = 'RTX';
                return fileFormatObject;
            }
            else if (fileExt === 'MDB') {
                fileFormatObject = 'MDB';
                return fileFormatObject;
            }
            else if (fileExt === 'ZIP') {
                fileFormatObject = 'ZIP';
                return fileFormatObject;
            }
            else if (fileExt === 'SND' || fileExt === 'AIF' || fileExt === 'AIFF') {
                fileFormatObject = 'AUDIO';
                return fileFormatObject;
            }
            else {
                fileFormatObject = 'Unsupported Format';
                return fileFormatObject;
            }
        }
    };
    AssetFileConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AssetFileConfigService_Factory() { return new AssetFileConfigService(); }, token: AssetFileConfigService, providedIn: "root" });
    AssetFileConfigService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AssetFileConfigService);
    return AssetFileConfigService;
}());
export { AssetFileConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQuZmlsZS5jb25maWcuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3VwbG9hZC9zZXJ2aWNlcy9hc3NldC5maWxlLmNvbmZpZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQU0zQztJQUVJO0lBQWdCLENBQUM7SUFFakIsK0NBQWMsR0FBZCxVQUFlLFFBQVE7UUFDbkIsSUFBTSxXQUFXLEdBQUcsbUJBQW1CLENBQUM7UUFDeEMsSUFBTSxZQUFZLEdBQUcsYUFBYSxDQUFDO1FBQ25DLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFNLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztRQUNuQyxJQUFNLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQztRQUNqQyxJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDO1FBRTFCLElBQUksQ0FBQyxRQUFRLElBQUksUUFBUSxJQUFJLElBQUksSUFBSSxRQUFRLEtBQUssRUFBRSxFQUFFO1lBQ2xELElBQU0sUUFBUSxHQUFHLGlCQUFpQixDQUFDO1lBQ25DLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFekMsSUFBSSxPQUFPLElBQUksT0FBTyxJQUFJLElBQUksSUFBSSxPQUFPLEtBQUssRUFBRSxFQUFFO2dCQUM5QyxPQUFPLEdBQUcsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ25DO1lBRUQsSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNuQixPQUFPLE9BQU8sQ0FBQzthQUNsQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sRUFBRTtnQkFDaEQsT0FBTyxZQUFZLENBQUM7YUFDdkI7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLEVBQUU7Z0JBQ2hELE9BQU8sWUFBWSxDQUFDO2FBQ3ZCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQ3JFLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsT0FBTyxPQUFPLENBQUM7YUFDbEI7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUs7bUJBQy9ELE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQ3RGLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsT0FBTyxZQUFZLENBQUM7YUFDdkI7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU07bUJBQ2hFLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNoRSxPQUFPLFNBQVMsQ0FBQzthQUNwQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLE9BQU8sWUFBWSxDQUFDO2FBQ3ZCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsT0FBTyxPQUFPLENBQUM7YUFDbEI7aUJBQU07Z0JBQ0gsT0FBTyxXQUFXLENBQUM7YUFDdEI7U0FDSjthQUFNO1lBQ0gsT0FBTyxXQUFXLENBQUM7U0FDdEI7SUFDTCxDQUFDO0lBRUQsb0RBQW1CLEdBQW5CLFVBQW9CLFFBQVE7UUFDeEIsSUFBSSxnQkFBZ0IsQ0FBQztRQUNyQixJQUFJLFFBQVEsSUFBSSxJQUFJLElBQUksUUFBUSxLQUFLLEVBQUUsRUFBRTtZQUNyQyxJQUFNLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQztZQUNuQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXpDLElBQUksT0FBTyxJQUFJLElBQUksSUFBSSxPQUFPLEtBQUssRUFBRSxFQUFFO2dCQUNuQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ25DO1lBRUQsSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNuQixnQkFBZ0IsR0FBRyxTQUFTLENBQUM7Z0JBQzdCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTTtnQkFDekYsT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxFQUFFO2dCQUN6QyxnQkFBZ0IsR0FBRyxrQkFBa0IsQ0FBQztnQkFDdEMsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNO2dCQUN6RixPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLElBQUksSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNqRixnQkFBZ0IsR0FBRyxPQUFPLENBQUM7Z0JBQzNCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUMxQixnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUMxQixnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUMxQixnQkFBZ0IsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSztnQkFDdkYsT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDbkYsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDO2dCQUMzQixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLEVBQUU7Z0JBQ3JFLGdCQUFnQixHQUFHLE9BQU8sQ0FBQztnQkFDM0IsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTTtnQkFDSCxnQkFBZ0IsR0FBRyxvQkFBb0IsQ0FBQztnQkFDeEMsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtTQUNKO0lBQ0wsQ0FBQzs7SUE1R1Esc0JBQXNCO1FBSmxDLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxzQkFBc0IsQ0E2R2xDO2lDQW5IRDtDQW1IQyxBQTdHRCxJQTZHQztTQTdHWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBc3NldEZpbGVDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAgIGZpbmRJY29uQnlOYW1lKGZpbGVOYW1lKSB7XHJcbiAgICAgICAgY29uc3QgdW5rbm93bkljb24gPSAnaW5zZXJ0X2RyaXZlX2ZpbGUnO1xyXG4gICAgICAgIGNvbnN0IGRvY3VtZW50SWNvbiA9ICdkZXNjcmlwdGlvbic7XHJcbiAgICAgICAgY29uc3QgaW1hZ2VJY29uID0gJ2ltYWdlJztcclxuICAgICAgICBjb25zdCB2aWRlb0ljb24gPSAnbW92aWVfY3JlYXRpb24nO1xyXG4gICAgICAgIGNvbnN0IHBkZkljb24gPSAncGljdHVyZV9hc19wZGYnO1xyXG4gICAgICAgIGNvbnN0IGdpZkljb24gPSAnZ2lmJztcclxuICAgICAgICBjb25zdCB6aXBJY29uID0gJ2FyY2hpdmUnO1xyXG5cclxuICAgICAgICBpZiAoIWZpbGVOYW1lIHx8IGZpbGVOYW1lICE9IG51bGwgJiYgZmlsZU5hbWUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGVSZWd4ID0gLyg/OlxcLihbXi5dKykpPyQvO1xyXG4gICAgICAgICAgICBsZXQgZmlsZUV4dCA9IGZpbGVSZWd4LmV4ZWMoZmlsZU5hbWUpWzFdO1xyXG5cclxuICAgICAgICAgICAgaWYgKGZpbGVFeHQgJiYgZmlsZUV4dCAhPSBudWxsICYmIGZpbGVFeHQgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRXh0ID0gZmlsZUV4dC50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmlsZUV4dCA9PT0gJ1BERicpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwZGZJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdET0MnIHx8IGZpbGVFeHQgPT09ICdET0NYJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50SWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnWExTJyB8fCBmaWxlRXh0ID09PSAnWExTWCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudEljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ0pQRycgfHwgZmlsZUV4dCA9PT0gJ0pQRUcnIHx8IGZpbGVFeHQgPT09ICdQTkcnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaW1hZ2VJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdHSUYnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZ2lmSWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnTVA0JyB8fCBmaWxlRXh0ID09PSAnQVZJJyB8fCBmaWxlRXh0ID09PSAnTUtWJ1xyXG4gICAgICAgICAgICAgICAgfHwgZmlsZUV4dCA9PT0gJ0ZMVicgfHwgZmlsZUV4dCA9PT0gJ1dNVicgfHwgZmlsZUV4dCA9PT0gJ01QRUcnIHx8IGZpbGVFeHQgPT09ICczR1AnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmlkZW9JY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdUWFQnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnRJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdNUDMnIHx8IGZpbGVFeHQgPT09ICdBQUMnIHx8IGZpbGVFeHQgPT09ICdGTEFDJ1xyXG4gICAgICAgICAgICAgICAgfHwgZmlsZUV4dCA9PT0gJ000QScgfHwgZmlsZUV4dCA9PT0gJ1dBVicgfHwgZmlsZUV4dCA9PT0gJ1dNQScpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB2aWRlb0ljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ0VQUycpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudEljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1pJUCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB6aXBJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHVua25vd25JY29uO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVua25vd25JY29uO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRmaWxlRm9ybWF0T2JqZWN0KGZpbGVuYW1lKSB7XHJcbiAgICAgICAgbGV0IGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgaWYgKGZpbGVuYW1lICE9IG51bGwgJiYgZmlsZW5hbWUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGVSZWd4ID0gLyg/OlxcLihbXi5dKykpPyQvO1xyXG4gICAgICAgICAgICBsZXQgZmlsZUV4dCA9IGZpbGVSZWd4LmV4ZWMoZmlsZW5hbWUpWzFdO1xyXG5cclxuICAgICAgICAgICAgaWYgKGZpbGVFeHQgIT0gbnVsbCAmJiBmaWxlRXh0ICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUV4dCA9IGZpbGVFeHQudG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpbGVFeHQgPT09ICdQREYnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0Fjcm9iYXQnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ0RPQycgfHwgZmlsZUV4dCA9PT0gJ0RPQ1gnIHx8IGZpbGVFeHQgPT09ICdYTFMnIHx8IGZpbGVFeHQgPT09ICdYTFNYJyB8fFxyXG4gICAgICAgICAgICAgICAgZmlsZUV4dCA9PT0gJ1BQVCcgfHwgZmlsZUV4dCA9PT0gJ1BQVFgnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ01pY3Jvc29mdCBPZmZpY2UnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ0pQRycgfHwgZmlsZUV4dCA9PT0gJ0pQRUcnIHx8IGZpbGVFeHQgPT09ICdKUEUnIHx8IGZpbGVFeHQgPT09ICdUSUZGJyB8fFxyXG4gICAgICAgICAgICAgICAgZmlsZUV4dCA9PT0gJ1RJRicgfHwgZmlsZUV4dCA9PT0gJ0VQUycgfHwgZmlsZUV4dCA9PT0gJ0FJJyB8fCBmaWxlRXh0ID09PSAnUFNEJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdJbWFnZSc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnR0lGJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdHSUYnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1BORycpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnUE5HJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdCTVAnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0JNUCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnTVA0JyB8fCBmaWxlRXh0ID09PSAnQVZJJyB8fCBmaWxlRXh0ID09PSAnTUtWJyB8fCBmaWxlRXh0ID09PSAnRkxWJyB8fFxyXG4gICAgICAgICAgICAgICAgZmlsZUV4dCA9PT0gJ1dNVicgfHwgZmlsZUV4dCA9PT0gJ01QRUcnIHx8IGZpbGVFeHQgPT09ICczR1AnIHx8IGZpbGVFeHQgPT09ICdNT1YnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ1ZJREVPJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdUWFQnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ1RYVCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnRU1MJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdFTUwnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1JUWCcpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnUlRYJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdNREInKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ01EQic7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnWklQJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdaSVAnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1NORCcgfHwgZmlsZUV4dCA9PT0gJ0FJRicgfHwgZmlsZUV4dCA9PT0gJ0FJRkYnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0FVRElPJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdVbnN1cHBvcnRlZCBGb3JtYXQnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19