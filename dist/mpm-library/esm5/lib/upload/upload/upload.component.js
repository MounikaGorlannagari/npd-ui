import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../notification/notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AssetUploadService } from '../services/asset.upload.service';
import { LoaderService } from '../../loader/loader.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AssetConstants } from '../../project/assets/asset_constants';
var UploadComponent = /** @class */ (function () {
    function UploadComponent(dialogRef, data, assetUploadService, loaderService, notificationService, otmmService, sharingService, utilService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.assetUploadService = assetUploadService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.uploadedFiles = [];
        this.uploadProcessing = false;
        this.selected = 'Project';
        this.referenceAsset = ['Project', 'DeliverableName1', 'DeliverableName2'];
    }
    UploadComponent.prototype.closeDialog = function () {
        this.dialogRef.close(false);
    };
    UploadComponent.prototype.udploadFileEvent = function (files) {
        this.uploadedFiles = files;
    };
    UploadComponent.prototype.validateFile = function () {
        var _this = this;
        var isValidFile = false;
        var count = 0;
        this.uploadedFiles.map(function (file) {
            if (file) {
                var fileExtensionData = _this.assetConfig[ApplicationConfigConstants.MPM_ASSET_CONFIG.ALLOWED_FILE_TYPES];
                var isvalid_1 = false;
                if (!(_this.utilService.isNullOrEmpty(fileExtensionData))) {
                    var fileExtensionList = fileExtensionData.split(',');
                    if (fileExtensionList && fileExtensionList.length > 0) {
                        fileExtensionList.map(function (fileExtension) {
                            /*  if (fileExtensionListSplit && fileExtensionListSplit.length > 0) {
                                 fileExtensionListSplit.map(fileExtension => { */
                            if (file.name.substr(file.name.length - fileExtension.length, fileExtension.length).toLowerCase() ===
                                fileExtension.toLowerCase()) {
                                count++;
                                isvalid_1 = true;
                            }
                        });
                        if (isvalid_1) {
                            isValidFile = true;
                        }
                    }
                    else {
                        isValidFile = true;
                    }
                }
                else {
                    isValidFile = true;
                }
            }
        });
        if (count === this.uploadedFiles.length) {
            isValidFile = true;
        }
        else {
            isValidFile = false;
        }
        return isValidFile;
    };
    // validateFile() {
    //     let hasValidFiles = false;
    //     this.uploadedFiles.map(file => {
    //         if (file) {
    //             const fileExtensionList = FileTypes;
    //             for (const [key, value] of Object.entries(fileExtensionList)) {
    //                 if (file.name.substr(file.name.length - value.length, value.length).toLowerCase() ===
    //                     value.toLowerCase()) {
    //                     hasValidFiles = true;
    //                     break;
    //                 }
    //             }
    //         }
    //     });
    //     return hasValidFiles;
    // }
    UploadComponent.prototype.uploadFiles = function () {
        var _this = this;
        this.loaderService.show();
        console.log(this.data);
        if (this.validateFile()) {
            if (this.uploadedFiles.length > 0) {
                this.uploadProcessing = true;
                this.otmmService.checkOtmmSession(true)
                    .subscribe(function (checkOtmmSessionResponse) {
                    _this.assetUploadService.startUpload(_this.uploadedFiles, _this.data.deliverable, _this.data.isRevisionUpload, _this.data.folderId, _this.data.project)
                        .subscribe(function (response) {
                        _this.loaderService.hide();
                        _this.dialogRef.close(true);
                        _this.notificationService.info('Upload has been initiated');
                    }, function (error) {
                        _this.loaderService.hide();
                        _this.uploadProcessing = false;
                        _this.notificationService.error('Something went wrong while uploading the file(s)');
                    });
                }, function (checkOtmmSessionError) {
                    _this.loaderService.hide();
                    _this.notificationService.error('Something went wrong while getting OTMM session');
                    _this.uploadProcessing = false;
                });
            }
            else {
                this.loaderService.hide();
                this.notificationService.info('Please select or drag and drop files to upload');
            }
        }
        else {
            this.loaderService.hide();
            if (this.uploadedFiles.length > 0) {
                this.notificationService.info('Uploaded file format is not supported');
            }
            else {
                this.notificationService.info('Please select or drag and drop files to upload');
            }
        }
    };
    UploadComponent.prototype.changeReferenceType = function (option) {
    };
    UploadComponent.prototype.ngOnInit = function () {
        this.assetConfig = this.sharingService.getAssetConfig();
        this.referenceTypeObj = AssetConstants.REFERENCE_TYPES_OBJECTS;
    };
    UploadComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: AssetUploadService },
        { type: LoaderService },
        { type: NotificationService },
        { type: OTMMService },
        { type: SharingService },
        { type: UtilService }
    ]; };
    UploadComponent = __decorate([
        Component({
            selector: 'mpm-upload',
            template: "<div>\r\n    <div class=\"flex-row\">\r\n        <h1 mat-dialog-title class=\"upload-modal-title\">{{data.modalName}}</h1>\r\n        <mat-icon class=\"modal-close\" matTooltip=\"Close\" (click)=\"closeDialog()\">\r\n            close\r\n        </mat-icon>\r\n    </div>\r\n    <div class=\"flex-row\" *ngIf=\"data.needReference\">\r\n        <div class=\"referenceContainer\">\r\n            <div>Reference To</div>&nbsp;&nbsp;&nbsp;\r\n            <div>\r\n                <button  class=\"sort-btn\" [disabled]=\"true\"\r\n                    mat-stroked-button matTooltip=\"Select delivery format\" [matMenuTriggerFor]=\"referenceType\">\r\n                    <span> {{referenceTypeObj.selected.name}}</span>\r\n                    <mat-icon color=\"accent\">arrow_drop_down</mat-icon>\r\n                </button>\r\n                \r\n            </div>\r\n        </div>\r\n    </div>\r\n    <mat-dialog-content>\r\n        <mpm-file-upload (queuedFiles)=\"udploadFileEvent($event)\" [isRevisionUpload]=\"data.isRevisionUpload\"\r\n            [fileToRevision]=\"data.fileToRevision\" [maxFiles]=\"data.maxFiles\" [maxFileSize]=\"data.maxFileSize\">\r\n        </mpm-file-upload>\r\n    </mat-dialog-content>\r\n    <mat-dialog-actions align=\"end\">\r\n        <button mat-stroked-button (click)=\"closeDialog()\">\r\n            <span>Close</span>\r\n        </button>\r\n        <button color=\"primary\" mat-flat-button (click)=\"uploadFiles()\" [disabled]=\"uploadProcessing\">\r\n            <span>Finish</span>\r\n        </button>\r\n    </mat-dialog-actions>\r\n</div>\r\n\r\n<mat-menu #referenceType=\"matMenu\">\r\n    <span *ngFor=\"let option of referenceTypeObj.options\">\r\n        <button *ngIf=\"option.name != referenceTypeObj.selected\" mat-menu-item (click)=\"changeReferenceType(option)\">\r\n            <span>{{option.name}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
            styles: [".upload-modal-title{font-weight:700;flex-grow:1}button{margin:10px}mat-dialog-content{height:350px}.modal-close{cursor:pointer}.referenceContainer{display:flex;flex-wrap:nowrap;align-items:baseline}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], UploadComponent);
    return UploadComponent;
}());
export { UploadComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3VwbG9hZC91cGxvYWQvdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBU3RFO0lBRUkseUJBQ1csU0FBd0MsRUFDZixJQUFTLEVBQ2xDLGtCQUFzQyxFQUN0QyxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsV0FBd0I7UUFQeEIsY0FBUyxHQUFULFNBQVMsQ0FBK0I7UUFDZixTQUFJLEdBQUosSUFBSSxDQUFLO1FBQ2xDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFHbkMsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBRXpCLGFBQVEsR0FBRyxTQUFTLENBQUM7UUFDckIsbUJBQWMsR0FBRyxDQUFDLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO0lBTmpFLENBQUM7SUFRTCxxQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELDBDQUFnQixHQUFoQixVQUFpQixLQUFLO1FBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQy9CLENBQUM7SUFFRCxzQ0FBWSxHQUFaO1FBQUEsaUJBcUNDO1FBcENHLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7WUFDdkIsSUFBSSxJQUFJLEVBQUU7Z0JBRU4sSUFBTSxpQkFBaUIsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQzNHLElBQUksU0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDcEIsSUFBSSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO29CQUN0RCxJQUFNLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNuRCxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsVUFBQSxhQUFhOzRCQUMvQjtpRkFDcUQ7NEJBQ3JELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsV0FBVyxFQUFFO2dDQUM3RixhQUFhLENBQUMsV0FBVyxFQUFFLEVBQUU7Z0NBQzdCLEtBQUssRUFBRSxDQUFDO2dDQUNSLFNBQU8sR0FBRyxJQUFJLENBQUM7NkJBQ2xCO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3dCQUNILElBQUksU0FBTyxFQUFFOzRCQUNULFdBQVcsR0FBRyxJQUFJLENBQUM7eUJBQ3RCO3FCQUNKO3lCQUFNO3dCQUNILFdBQVcsR0FBRyxJQUFJLENBQUM7cUJBQ3RCO2lCQUNKO3FCQUFNO29CQUNILFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ3RCO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3JDLFdBQVcsR0FBRyxJQUFJLENBQUM7U0FDdEI7YUFBTTtZQUNILFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDdkI7UUFDRCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBRUQsbUJBQW1CO0lBQ25CLGlDQUFpQztJQUNqQyx1Q0FBdUM7SUFDdkMsc0JBQXNCO0lBQ3RCLG1EQUFtRDtJQUNuRCw4RUFBOEU7SUFDOUUsd0dBQXdHO0lBQ3hHLDZDQUE2QztJQUM3Qyw0Q0FBNEM7SUFDNUMsNkJBQTZCO0lBQzdCLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLFVBQVU7SUFFViw0QkFBNEI7SUFDNUIsSUFBSTtJQUVKLHFDQUFXLEdBQVg7UUFBQSxpQkFvQ0M7UUFuQ0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUNyQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7cUJBQ2xDLFNBQVMsQ0FBQyxVQUFBLHdCQUF3QjtvQkFDL0IsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUN6RSxLQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO3lCQUNqRSxTQUFTLENBQUMsVUFBQSxRQUFRO3dCQUNmLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFCLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUMzQixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7b0JBQy9ELENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQzt3QkFDOUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO29CQUN2RixDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDLEVBQUUsVUFBQSxxQkFBcUI7b0JBQ3BCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQztvQkFDbEYsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7YUFDbkY7U0FDSjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO2FBQzFFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0RBQWdELENBQUMsQ0FBQzthQUNuRjtTQUNKO0lBQ0wsQ0FBQztJQUVELDZDQUFtQixHQUFuQixVQUFvQixNQUFNO0lBRTFCLENBQUM7SUFFRCxrQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxjQUFjLENBQUMsdUJBQXVCLENBQUM7SUFDbkUsQ0FBQzs7Z0JBOUhxQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTtnQkFDSSxrQkFBa0I7Z0JBQ3ZCLGFBQWE7Z0JBQ1AsbUJBQW1CO2dCQUMzQixXQUFXO2dCQUNSLGNBQWM7Z0JBQ2pCLFdBQVc7O0lBVjFCLGVBQWU7UUFOM0IsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFlBQVk7WUFDdEIsdzREQUFzQzs7U0FFekMsQ0FBQztRQU1PLFdBQUEsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO09BSm5CLGVBQWUsQ0FtSTNCO0lBQUQsc0JBQUM7Q0FBQSxBQW5JRCxJQW1JQztTQW5JWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTUFUX0RJQUxPR19EQVRBLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBBc3NldFVwbG9hZFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hc3NldC51cGxvYWQuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpbGVUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0ZpbGVUeXBlcyc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEFzc2V0Q29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9hc3NldHMvYXNzZXRfY29uc3RhbnRzJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLXVwbG9hZCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdXBsb2FkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3VwbG9hZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVXBsb2FkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8VXBsb2FkQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgICAgICBwdWJsaWMgYXNzZXRVcGxvYWRTZXJ2aWNlOiBBc3NldFVwbG9hZFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgdXBsb2FkZWRGaWxlcyA9IFtdO1xyXG4gICAgdXBsb2FkUHJvY2Vzc2luZyA9IGZhbHNlO1xyXG4gICAgYXNzZXRDb25maWc6IGFueTtcclxuICAgIHNlbGVjdGVkID0gJ1Byb2plY3QnO1xyXG4gICAgcmVmZXJlbmNlQXNzZXQgPSBbJ1Byb2plY3QnLCAnRGVsaXZlcmFibGVOYW1lMScsICdEZWxpdmVyYWJsZU5hbWUyJ107XHJcbiAgICByZWZlcmVuY2VUeXBlT2JqO1xyXG4gICAgY2xvc2VEaWFsb2coKSB7XHJcbiAgICAgICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHVkcGxvYWRGaWxlRXZlbnQoZmlsZXMpIHtcclxuICAgICAgICB0aGlzLnVwbG9hZGVkRmlsZXMgPSBmaWxlcztcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZUZpbGUoKSB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWRGaWxlID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGNvdW50ID0gMDtcclxuICAgICAgICB0aGlzLnVwbG9hZGVkRmlsZXMubWFwKGZpbGUgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZmlsZSkge1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpbGVFeHRlbnNpb25EYXRhID0gdGhpcy5hc3NldENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVNTRVRfQ09ORklHLkFMTE9XRURfRklMRV9UWVBFU107XHJcbiAgICAgICAgICAgICAgICBsZXQgaXN2YWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgaWYgKCEodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGZpbGVFeHRlbnNpb25EYXRhKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWxlRXh0ZW5zaW9uTGlzdCA9IGZpbGVFeHRlbnNpb25EYXRhLnNwbGl0KCcsJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpbGVFeHRlbnNpb25MaXN0ICYmIGZpbGVFeHRlbnNpb25MaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZUV4dGVuc2lvbkxpc3QubWFwKGZpbGVFeHRlbnNpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLyogIGlmIChmaWxlRXh0ZW5zaW9uTGlzdFNwbGl0ICYmIGZpbGVFeHRlbnNpb25MaXN0U3BsaXQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlRXh0ZW5zaW9uTGlzdFNwbGl0Lm1hcChmaWxlRXh0ZW5zaW9uID0+IHsgKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWxlLm5hbWUuc3Vic3RyKGZpbGUubmFtZS5sZW5ndGggLSBmaWxlRXh0ZW5zaW9uLmxlbmd0aCwgZmlsZUV4dGVuc2lvbi5sZW5ndGgpLnRvTG93ZXJDYXNlKCkgPT09XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZUV4dGVuc2lvbi50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnQrKztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc3ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc3ZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1ZhbGlkRmlsZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1ZhbGlkRmlsZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpc1ZhbGlkRmlsZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoY291bnQgPT09IHRoaXMudXBsb2FkZWRGaWxlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgaXNWYWxpZEZpbGUgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlzVmFsaWRGaWxlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkRmlsZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyB2YWxpZGF0ZUZpbGUoKSB7XHJcbiAgICAvLyAgICAgbGV0IGhhc1ZhbGlkRmlsZXMgPSBmYWxzZTtcclxuICAgIC8vICAgICB0aGlzLnVwbG9hZGVkRmlsZXMubWFwKGZpbGUgPT4ge1xyXG4gICAgLy8gICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgLy8gICAgICAgICAgICAgY29uc3QgZmlsZUV4dGVuc2lvbkxpc3QgPSBGaWxlVHlwZXM7XHJcbiAgICAvLyAgICAgICAgICAgICBmb3IgKGNvbnN0IFtrZXksIHZhbHVlXSBvZiBPYmplY3QuZW50cmllcyhmaWxlRXh0ZW5zaW9uTGlzdCkpIHtcclxuICAgIC8vICAgICAgICAgICAgICAgICBpZiAoZmlsZS5uYW1lLnN1YnN0cihmaWxlLm5hbWUubGVuZ3RoIC0gdmFsdWUubGVuZ3RoLCB2YWx1ZS5sZW5ndGgpLnRvTG93ZXJDYXNlKCkgPT09XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIHZhbHVlLnRvTG93ZXJDYXNlKCkpIHtcclxuICAgIC8vICAgICAgICAgICAgICAgICAgICAgaGFzVmFsaWRGaWxlcyA9IHRydWU7XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgLy8gICAgICAgICAgICAgICAgIH1cclxuICAgIC8vICAgICAgICAgICAgIH1cclxuICAgIC8vICAgICAgICAgfVxyXG4gICAgLy8gICAgIH0pO1xyXG5cclxuICAgIC8vICAgICByZXR1cm4gaGFzVmFsaWRGaWxlcztcclxuICAgIC8vIH1cclxuXHJcbiAgICB1cGxvYWRGaWxlcygpIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0YSk7XHJcbiAgICAgICAgaWYgKHRoaXMudmFsaWRhdGVGaWxlKCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudXBsb2FkZWRGaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZFByb2Nlc3NpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5jaGVja090bW1TZXNzaW9uKHRydWUpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjaGVja090bW1TZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFzc2V0VXBsb2FkU2VydmljZS5zdGFydFVwbG9hZCh0aGlzLnVwbG9hZGVkRmlsZXMsIHRoaXMuZGF0YS5kZWxpdmVyYWJsZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0YS5pc1JldmlzaW9uVXBsb2FkLCB0aGlzLmRhdGEuZm9sZGVySWQsIHRoaXMuZGF0YS5wcm9qZWN0KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnVXBsb2FkIGhhcyBiZWVuIGluaXRpYXRlZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRQcm9jZXNzaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSB1cGxvYWRpbmcgdGhlIGZpbGUocyknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGNoZWNrT3RtbVNlc3Npb25FcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBPVE1NIHNlc3Npb24nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGxvYWRQcm9jZXNzaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1BsZWFzZSBzZWxlY3Qgb3IgZHJhZyBhbmQgZHJvcCBmaWxlcyB0byB1cGxvYWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVwbG9hZGVkRmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1VwbG9hZGVkIGZpbGUgZm9ybWF0IGlzIG5vdCBzdXBwb3J0ZWQnKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdQbGVhc2Ugc2VsZWN0IG9yIGRyYWcgYW5kIGRyb3AgZmlsZXMgdG8gdXBsb2FkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlUmVmZXJlbmNlVHlwZShvcHRpb24pIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5hc3NldENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXNzZXRDb25maWcoKTtcclxuICAgICAgICB0aGlzLnJlZmVyZW5jZVR5cGVPYmogPSBBc3NldENvbnN0YW50cy5SRUZFUkVOQ0VfVFlQRVNfT0JKRUNUUztcclxuICAgIH1cclxuXHJcbn1cclxuIl19