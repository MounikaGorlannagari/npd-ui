import { __decorate, __read, __spread } from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { CommentsService } from './services/comments.service';
import { CommentsUtilService } from './services/comments.util.service';
import { forkJoin } from 'rxjs';
import { ProjectService } from '../project/shared/services/project.service';
import { LoaderService } from '../loader/loader.service';
import { NotificationService } from '../notification/notification.service';
import { DeliverableService } from '../project/tasks/deliverable/deliverable.service';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { UtilService } from '../mpm-utils/services/util.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
var CommentsComponent = /** @class */ (function () {
    function CommentsComponent(commentsService, commentUtilService, projectService, loaderService, notification, deliverableService, sharingService, utilService, activatedRoute, route) {
        this.commentsService = commentsService;
        this.commentUtilService = commentUtilService;
        this.projectService = projectService;
        this.loaderService = loaderService;
        this.notification = notification;
        this.deliverableService = deliverableService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.activatedRoute = activatedRoute;
        this.route = route;
        this.commentsConfig = null;
        this.titleValue = null;
        this.isScrollDown = false;
        this.userListData = [];
        this.isReadonly = false;
        this.projectOwerId = null;
        this.isExternalUser = false;
        this.teamId = null;
        this.checked = false;
        this.deliverableIdFromUrl = 0;
        this.formNotification = false;
        this.commentsService.clearUserProjectDeliverableMapper();
    }
    CommentsComponent.prototype.initComments = function () {
        var _this = this;
        if (this.projectId) {
            this.commentsConfig = {
                commentList: [],
                deliverableList: [],
                pageDetails: this.commentUtilService.resetPageDetails(),
            };
            this.projectService.getProjectById((this.projectId + '')).subscribe(function (response) {
                var tempModal = null;
                tempModal = {
                    titleName: _this.commentUtilService.checkValueIsNullOrEmpty(response['PROJECT_NAME'], ''),
                    icon: 'fiber_manual_record',
                    isActive: _this.isManager && _this.deliverableIdFromUrl == 0 ? true : false,
                    isProject: true,
                    projectId: _this.projectId,
                    deliverableId: null,
                    status: _this.commentUtilService.getStatusNameById(_this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_STATUS', 'MPM_Status-id', 'Id'], 0, ''), _this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_CATEGORY', 'MPM_Category-id', 'Id'], 0, '')),
                    owner: _this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_PROJECT_OWNER', 'Identity-id', 'Id'], 0, '')
                };
                _this.projectOwerId = tempModal.owner;
                _this.teamId = _this.commentUtilService.getValueFormObjectByKeyList(response, ['R_PO_TEAM', 'MPM_Teams-id', 'Id'], 0, null);
                if (_this.teamId) {
                    var currTeam = _this.sharingService.getCRActions().find(function (data) {
                        return data['MPM_Teams-id']['Id'] === _this.teamId;
                    });
                    _this.isExternalUser = _this.utilService.getBooleanValue(_this.commentUtilService.getValueFormObjectByKeyList(currTeam, ['MPM_Team_Role_Mapping', 'IS_EXTERNAL'], 0, false));
                }
                if (_this.formNotification) {
                    _this.loadDataOnRefresh(_this.projectId, _this.deliverableIdFromUrl, false, false, tempModal);
                }
                else {
                    _this.loadDataOnRefresh(_this.projectId, _this.deliverableId, false, false, tempModal);
                }
            });
        }
    };
    CommentsComponent.prototype.loadDataOnRefresh = function (projecId, deliverableId, isContextChange, isLoadMore, tempModal) {
        var _this = this;
        this.loaderService.show();
        this.isScrollDown = false;
        var httpArrayList = [
            this.commentsService.getCommentsForProject(projecId, deliverableId, this.isExternalUser, this.commentsConfig.pageDetails.skip)
        ];
        deliverableId ? httpArrayList.push(this.commentsService.getUsersForDeliverable(this.reviewDeliverableId ? this.reviewDeliverableId : deliverableId)) : httpArrayList.push(this.commentsService.getUsersForProject(projecId));
        if (this.isManager && !isContextChange) {
            httpArrayList.push(this.commentsService.getProjectAndDeliverableDetails(projecId));
        }
        else if (!this.isManager && !isContextChange) {
            httpArrayList.push(this.deliverableService.readDeliverable((deliverableId + '')));
        }
        forkJoin(httpArrayList).subscribe(function (responses) {
            _this.loaderService.hide();
            if (responses && responses.length >= 2) {
                _this.commentsConfig.commentList = !isLoadMore ? responses[0] : __spread(responses[0], _this.commentsConfig.commentList);
                _this.userListData = responses[1];
                if (responses[2]) {
                    if (tempModal != null) {
                        _this.projectOwerId = tempModal.owner;
                    }
                    if (_this.isManager) {
                        _this.commentsConfig.deliverableList = responses[2] ? responses[2] : [];
                        _this.commentsConfig.deliverableList = __spread([tempModal], _this.commentsConfig.deliverableList);
                    }
                    else {
                        _this.commentsConfig.deliverableList = responses[2] ? _this.commentUtilService.formProjectDeliverableModal([responses[2]['Deliverable']], projecId, true) : [];
                        if (_this.commentsConfig.deliverableList.length > 0) {
                            _this.commentsConfig.deliverableList[0].isActive = true;
                        }
                        _this.commentsConfig.deliverableList = __spread(_this.commentsConfig.deliverableList, [tempModal]);
                    }
                    if (_this.commentsConfig.deliverableList.length > 0) {
                        _this.updateTheCommentTitle(_this.commentsConfig.deliverableList[0]);
                    }
                }
                _this.isScrollDown = responses[0].length > 0 ? true : false;
            }
        }, function (errorDetail) {
            _this.notification.error('Something went wrong while getting comments!');
            _this.commentsConfig = null;
            _this.loaderService.hide();
        });
    };
    // notificationHandle(menuItem:ProjectDeliverableModal):
    CommentsComponent.prototype.onCommentsIndexChange = function (menuItem) {
        this.commentsConfig.deliverableList = this.commentsConfig.deliverableList.map(function (element) {
            element.isActive = ((element.isProject && menuItem.isProject) ? menuItem.projectId === element.projectId : menuItem.deliverableId === element.deliverableId);
            return element;
        });
        this.commentsConfig.pageDetails = this.commentUtilService.resetPageDetails();
        this.loadDataOnRefresh(menuItem.projectId, menuItem.deliverableId, true, false, null);
        this.updateTheCommentTitle(menuItem);
    };
    CommentsComponent.prototype.onCreatingNewComment = function (commentDetails) {
        var _this = this;
        var currCommentItem = this.commentsConfig.deliverableList.find(function (data) { return data.isActive; });
        if (currCommentItem) {
            commentDetails = Object.assign(commentDetails, { ProjectID: currCommentItem.projectId, DeliverableID: currCommentItem.deliverableId });
            this.commentsService.createNewComment(commentDetails).subscribe(function (response) {
                if (response) {
                    _this.commentsConfig.pageDetails = _this.commentUtilService.resetPageDetails();
                    _this.loadDataOnRefresh(currCommentItem.projectId, currCommentItem.deliverableId, true, false, null);
                }
            });
        }
    };
    CommentsComponent.prototype.onLoadingMoreComments = function (pageDetails) {
        this.commentsConfig.pageDetails = pageDetails;
        var currCommentItem = this.commentsConfig.deliverableList.find(function (data) { return data.isActive; });
        this.loadDataOnRefresh(currCommentItem.projectId, currCommentItem.deliverableId, true, true, null);
    };
    CommentsComponent.prototype.updateTheCommentTitle = function (indexChangeData) {
        this.titleValue = indexChangeData.isProject ?
            ('Project: ' + indexChangeData.titleName) : ('Deliverable: ' + indexChangeData.titleName);
        this.isReadonly = this.isManager ? !(this.projectOwerId === this.sharingService.getCurrentUserItemID()) : false;
        this.isReadonly = (indexChangeData.status && (indexChangeData.status === 'COMPLETED' && this.isManager)) ? true : this.isReadonly;
    };
    CommentsComponent.prototype.handleToggleTask = function (eventData) {
        var currCommentItem = this.commentsConfig.deliverableList.find(function (data) { return !data.isActive; });
        this.checked = !this.checked;
        this.onCommentsIndexChange(currCommentItem);
    };
    CommentsComponent.prototype.refreshComments = function (eventData) {
        var currCommentItem = this.commentsConfig.deliverableList.find(function (data) { return data.isActive; });
        this.onCommentsIndexChange(currCommentItem);
    };
    CommentsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.activatedRoute.parent != null) {
            this.activatedRoute.parent.params.subscribe(function (parentRouteParams) {
                _this.projectId = parentRouteParams.projectId;
            });
        }
        this.activatedRoute.queryParams.subscribe(function (params) {
            if (params.deliverableId != undefined) {
                _this.deliverableIdFromUrl = params.deliverableId;
                _this.formNotification = true;
            }
            else {
                _this.formNotification = false;
                _this.deliverableIdFromUrl = 0;
            }
            _this.initComments();
        });
    };
    CommentsComponent.ctorParameters = function () { return [
        { type: CommentsService },
        { type: CommentsUtilService },
        { type: ProjectService },
        { type: LoaderService },
        { type: NotificationService },
        { type: DeliverableService },
        { type: SharingService },
        { type: UtilService },
        { type: ActivatedRoute },
        { type: Router }
    ]; };
    __decorate([
        Input()
    ], CommentsComponent.prototype, "projectId", void 0);
    __decorate([
        Input()
    ], CommentsComponent.prototype, "deliverableId", void 0);
    __decorate([
        Input()
    ], CommentsComponent.prototype, "reviewDeliverableId", void 0);
    __decorate([
        Input()
    ], CommentsComponent.prototype, "isManager", void 0);
    __decorate([
        Input()
    ], CommentsComponent.prototype, "enableToComment", void 0);
    CommentsComponent = __decorate([
        Component({
            selector: 'mpm-comments',
            template: "<div class=\"comment-container\">\r\n    <div class=\"flex-row\" *ngIf=\"commentsConfig\">\r\n        <div class=\"flex-row-item width-80\">\r\n            <mat-card class=\"comment-layout-container\">\r\n                <mat-card-title class=\"comment-title\" matTooltip=\"{{titleValue}}\" matTooltipClass=\"custom-tooltip\">\r\n                    <div>{{titleValue}}</div>\r\n                    <div class=\"approver-view\">\r\n                        <div class=\"mat-icon-refresh-btn\" matTooltip=\"Refresh Comments\"\r\n                            (click)=\"refreshComments($event)\">\r\n                            <mat-icon>refresh</mat-icon>\r\n                        </div>\r\n                        <div class=\"mat-icon-toggle-btn\" *ngIf=\"!isManager\">\r\n                            <mat-slide-toggle\r\n                                matTooltip=\"{{ !checked ? 'View Project Comments' : 'View Deliverable Comments'}}\"\r\n                                class=\"slider-internal active-icon\" color=\"primary\" formControlName=\"IsExternal\"\r\n                                [checked]=\"checked\" (change)=handleToggleTask($event)>\r\n                            </mat-slide-toggle>\r\n                        </div>\r\n                    </div>\r\n                </mat-card-title>\r\n                <mat-card-content class=\"comment-layout-container-scroll\">\r\n                    <mpm-comments-layout [commentDataList]=\"commentsConfig.commentList\"\r\n                        [pageDetails]=\"commentsConfig.pageDetails\" (creatingNewComment)=\"onCreatingNewComment($event)\"\r\n                        (loadingMoreComments)=\"onLoadingMoreComments($event)\" [isScrollDown]=\"isScrollDown\"\r\n                        [userListData]=\"userListData\" [isReadonly]=\"isReadonly\" [enableToComment]=\"enableToComment\"\r\n                        [isExternalUser]=\"isExternalUser\" [projectId]=\"projectId\">\r\n                    </mpm-comments-layout>\r\n                </mat-card-content>\r\n            </mat-card>\r\n        </div>\r\n        <div class=\"flex-row-item comment-index-container-flex\" *ngIf=\"isManager\">\r\n            <mat-card class=\"comment-index-container\">\r\n                <mat-card-content>\r\n                    <mpm-comments-index [commentDeliverableList]=\"commentsConfig.deliverableList\"\r\n                    [deliverableIdFromUrl]=\"deliverableIdFromUrl\"\r\n                    (commentsIndexChange)=\"onCommentsIndexChange($event)\">\r\n                </mpm-comments-index>\r\n                </mat-card-content>\r\n            </mat-card>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- [notifiactionValue]=\"notifiactionValue\"-->\r\n<!--  -->",
            encapsulation: ViewEncapsulation.None,
            styles: [".comment-container{display:flex;flex-direction:column;max-height:83vh;top:0;bottom:0;left:0;right:0;margin:1em auto;padding-bottom:0}.comment-container .width-80{width:80%}.comment-container .comment-index-container-flex{width:20%}.comment-container .comment-index-container,.comment-container .comment-layout-container{flex:1;margin:10px 10px 0;padding-bottom:0;top:8px}.comment-container .comment-index-container{margin-left:0!important;max-height:81vh;overflow-y:auto}.comment-container .comment-title{text-align:center;font-size:20px;display:flex;margin-bottom:8px;font-weight:500;position:relative}.comment-container span.approver-view{position:absolute;right:0}.comment-container span.approver-view .mat-icon-toggle-btn{position:absolute;top:0;right:2em}.comment-container span.approver-view .mat-icon-refresh-btn{cursor:pointer}.comment-title>div{width:60%}"]
        })
    ], CommentsComponent);
    return CommentsComponent;
}());
export { CommentsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBd0IsaUJBQWlCLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQzVILE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFNUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDakUsT0FBTyxFQUFFLGNBQWMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFXaEU7SUFtQkUsMkJBQ1MsZUFBZ0MsRUFDaEMsa0JBQXVDLEVBQ3ZDLGNBQThCLEVBQzlCLGFBQTRCLEVBQzVCLFlBQWlDLEVBQ2pDLGtCQUFzQyxFQUN0QyxjQUE4QixFQUM5QixXQUF3QixFQUN4QixjQUE4QixFQUM5QixLQUFhO1FBVGIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBcUI7UUFDdkMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUNqQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQXZCdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLHlCQUFvQixHQUFHLENBQUMsQ0FBQztRQUV6QixxQkFBZ0IsR0FBWSxLQUFLLENBQUE7UUFjL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQ0FBaUMsRUFBRSxDQUFDO0lBQzNELENBQUM7SUFFRCx3Q0FBWSxHQUFaO1FBQUEsaUJBcUNDO1FBcENDLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsY0FBYyxHQUFHO2dCQUNwQixXQUFXLEVBQUUsRUFBRTtnQkFDZixlQUFlLEVBQUUsRUFBRTtnQkFDbkIsV0FBVyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRTthQUN4RCxDQUFDO1lBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDMUUsSUFBSSxTQUFTLEdBQTRCLElBQUksQ0FBQztnQkFDOUMsU0FBUyxHQUFHO29CQUNWLFNBQVMsRUFBRSxLQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEVBQUUsQ0FBQztvQkFDeEYsSUFBSSxFQUFFLHFCQUFxQjtvQkFDM0IsUUFBUSxFQUFFLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLG9CQUFvQixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLO29CQUN6RSxTQUFTLEVBQUUsSUFBSTtvQkFDZixTQUFTLEVBQUUsS0FBSSxDQUFDLFNBQVM7b0JBQ3pCLGFBQWEsRUFBRSxJQUFJO29CQUNuQixNQUFNLEVBQUUsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGlCQUFpQixDQUMvQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLENBQUMsYUFBYSxFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQzVHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUNuSCxLQUFLLEVBQUUsS0FBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxDQUFDLG9CQUFvQixFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2lCQUN6SCxDQUFDO2dCQUNGLEtBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztnQkFDckMsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzFILElBQUksS0FBSSxDQUFDLE1BQU0sRUFBRTtvQkFDZixJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUk7d0JBQzNELE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEtBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3BELENBQUMsQ0FBQyxDQUFDO29CQUNILEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxDQUFDLHVCQUF1QixFQUFFLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO2lCQUMzSztnQkFDRCxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDekIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7aUJBQzVGO3FCQUNJO29CQUNILEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQTtpQkFDcEY7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELDZDQUFpQixHQUFqQixVQUFrQixRQUFnQixFQUFFLGFBQXFCLEVBQUUsZUFBd0IsRUFBRSxVQUFtQixFQUFFLFNBQWtDO1FBQTVJLGlCQThDQztRQTdDQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQU0sYUFBYSxHQUFHO1lBQ3BCLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsUUFBUSxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztTQUMvSCxDQUFDO1FBQ0YsYUFBYSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzdOLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsK0JBQStCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNwRjthQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQzlDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkY7UUFDRCxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsU0FBUztZQUN6QyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN0QyxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUssS0FBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckgsS0FBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNoQixJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7d0JBQ3JCLEtBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztxQkFDdEM7b0JBQ0QsSUFBSSxLQUFJLENBQUMsU0FBUyxFQUFFO3dCQUVsQixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUN2RSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsYUFBSSxTQUFTLEdBQUssS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztxQkFDM0Y7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3QkFDN0osSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUNsRCxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3lCQUN4RDt3QkFDRCxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsWUFBTyxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsR0FBRSxTQUFTLEVBQUMsQ0FBQztxQkFDM0Y7b0JBQ0QsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNsRCxLQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDcEU7aUJBQ0Y7Z0JBQ0QsS0FBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDNUQ7UUFDSCxDQUFDLEVBQUUsVUFBQyxXQUFXO1lBQ2IsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FBQztZQUN4RSxLQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBSUwsQ0FBQztJQUVELHdEQUF3RDtJQUV4RCxpREFBcUIsR0FBckIsVUFBc0IsUUFBaUM7UUFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFVBQUEsT0FBTztZQUNuRixPQUFPLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsS0FBSyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsYUFBYSxLQUFLLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUM3SixPQUFPLE9BQU8sQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RixJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELGdEQUFvQixHQUFwQixVQUFxQixjQUErQjtRQUFwRCxpQkFXQztRQVZDLElBQU0sZUFBZSxHQUE0QixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUE2QixJQUFLLE9BQUEsSUFBSSxDQUFDLFFBQVEsRUFBYixDQUFhLENBQUMsQ0FBQztRQUM1SSxJQUFJLGVBQWUsRUFBRTtZQUNuQixjQUFjLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsRUFBRSxTQUFTLEVBQUUsZUFBZSxDQUFDLFNBQVMsRUFBRSxhQUFhLEVBQUUsZUFBZSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDdkksSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUN0RSxJQUFJLFFBQVEsRUFBRTtvQkFDWixLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDN0UsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUNyRztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQsaURBQXFCLEdBQXJCLFVBQXNCLFdBQWdCO1FBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUM5QyxJQUFNLGVBQWUsR0FBNEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBNkIsSUFBSyxPQUFBLElBQUksQ0FBQyxRQUFRLEVBQWIsQ0FBYSxDQUFDLENBQUM7UUFDNUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3JHLENBQUM7SUFFRCxpREFBcUIsR0FBckIsVUFBc0IsZUFBd0M7UUFDNUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUYsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ2hILElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sS0FBSyxXQUFXLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUNwSSxDQUFDO0lBRUQsNENBQWdCLEdBQWhCLFVBQWlCLFNBQVM7UUFDeEIsSUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQTZCLElBQUssT0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQWQsQ0FBYyxDQUFDLENBQUM7UUFDN0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDN0IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFDRCwyQ0FBZSxHQUFmLFVBQWdCLFNBQVM7UUFDdkIsSUFBTSxlQUFlLEdBQTRCLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFDLElBQTZCLElBQUssT0FBQSxJQUFJLENBQUMsUUFBUSxFQUFiLENBQWEsQ0FBQyxDQUFDO1FBQzVJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUFBLGlCQW1CQztRQWxCQyxJQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxJQUFFLElBQUksRUFBQztZQUNwQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUEsaUJBQWlCO2dCQUNyRCxLQUFJLENBQUMsU0FBUyxHQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQTtZQUN4QyxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUN2QyxVQUFBLE1BQU07WUFDSixJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksU0FBUyxFQUFFO2dCQUNyQyxLQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQTtnQkFDaEQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTthQUM3QjtpQkFDSTtnQkFDSCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO2dCQUM3QixLQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFBO2FBQzlCO1lBQ0QsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQzs7Z0JBeEt5QixlQUFlO2dCQUNaLG1CQUFtQjtnQkFDdkIsY0FBYztnQkFDZixhQUFhO2dCQUNkLG1CQUFtQjtnQkFDYixrQkFBa0I7Z0JBQ3RCLGNBQWM7Z0JBQ2pCLFdBQVc7Z0JBQ1IsY0FBYztnQkFDdkIsTUFBTTs7SUE1QmI7UUFBUixLQUFLLEVBQUU7d0RBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFOzREQUF1QjtJQUN0QjtRQUFSLEtBQUssRUFBRTtrRUFBNkI7SUFDNUI7UUFBUixLQUFLLEVBQUU7d0RBQW9CO0lBQ25CO1FBQVIsS0FBSyxFQUFFOzhEQUEwQjtJQUx2QixpQkFBaUI7UUFON0IsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGNBQWM7WUFDeEIsNHFGQUF3QztZQUV4QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7U0FDdEMsQ0FBQztPQUNXLGlCQUFpQixDQTZMN0I7SUFBRCx3QkFBQztDQUFBLEFBN0xELElBNkxDO1NBN0xZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgSW5qZWN0LCBIb3N0TGlzdGVuZXIsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbWVudHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb21tZW50cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tbWVudHNVdGlsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgZm9ya0pvaW4sIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3REZWxpdmVyYWJsZU1vZGFsLCBOZXdDb21tZW50TW9kYWwgfSBmcm9tICcuL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBSb3V0aW5nQ29uc3RhbnRzIH0gZnJvbSAnLi4vc2hhcmVkL2NvbnN0YW50cy9yb3V0aW5nQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgVEhJU19FWFBSIH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXIvc3JjL291dHB1dC9vdXRwdXRfYXN0JztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jb21tZW50cycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnRzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50cy5jb21wb25lbnQuc2NzcyddLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBwcm9qZWN0SWQ6IG51bWJlcjtcclxuICBASW5wdXQoKSBkZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgQElucHV0KCkgcmV2aWV3RGVsaXZlcmFibGVJZDogbnVtYmVyO1xyXG4gIEBJbnB1dCgpIGlzTWFuYWdlcjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBlbmFibGVUb0NvbW1lbnQ6IGJvb2xlYW47XHJcbiAgY29tbWVudHNDb25maWcgPSBudWxsO1xyXG4gIHRpdGxlVmFsdWUgPSBudWxsO1xyXG4gIGlzU2Nyb2xsRG93biA9IGZhbHNlO1xyXG4gIHVzZXJMaXN0RGF0YSA9IFtdO1xyXG4gIGlzUmVhZG9ubHkgPSBmYWxzZTtcclxuICBwcm9qZWN0T3dlcklkID0gbnVsbDtcclxuICBpc0V4dGVybmFsVXNlciA9IGZhbHNlO1xyXG4gIHRlYW1JZCA9IG51bGw7XHJcbiAgY2hlY2tlZCA9IGZhbHNlO1xyXG4gIGRlbGl2ZXJhYmxlSWRGcm9tVXJsID0gMDtcclxuICBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvblxyXG4gIGZvcm1Ob3RpZmljYXRpb246IGJvb2xlYW4gPSBmYWxzZVxyXG4gIG5vdGlmaWNhdGlvbkRldGFpbHM7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgY29tbWVudFV0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVNlcnZpY2U6IERlbGl2ZXJhYmxlU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHB1YmxpYyByb3V0ZTogUm91dGVyXHJcbiAgKSB7XHJcbiAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5jbGVhclVzZXJQcm9qZWN0RGVsaXZlcmFibGVNYXBwZXIoKTtcclxuICB9XHJcblxyXG4gIGluaXRDb21tZW50cygpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnByb2plY3RJZCkge1xyXG4gICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnID0ge1xyXG4gICAgICAgIGNvbW1lbnRMaXN0OiBbXSxcclxuICAgICAgICBkZWxpdmVyYWJsZUxpc3Q6IFtdLFxyXG4gICAgICAgIHBhZ2VEZXRhaWxzOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5yZXNldFBhZ2VEZXRhaWxzKCksXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0UHJvamVjdEJ5SWQoKHRoaXMucHJvamVjdElkICsgJycpKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGxldCB0ZW1wTW9kYWw6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsID0gbnVsbDtcclxuICAgICAgICB0ZW1wTW9kYWwgPSB7XHJcbiAgICAgICAgICB0aXRsZU5hbWU6IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmNoZWNrVmFsdWVJc051bGxPckVtcHR5KHJlc3BvbnNlWydQUk9KRUNUX05BTUUnXSwgJycpLFxyXG4gICAgICAgICAgaWNvbjogJ2ZpYmVyX21hbnVhbF9yZWNvcmQnLFxyXG4gICAgICAgICAgaXNBY3RpdmU6IHRoaXMuaXNNYW5hZ2VyICYmIHRoaXMuZGVsaXZlcmFibGVJZEZyb21VcmwgPT0gMCA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgIGlzUHJvamVjdDogdHJ1ZSxcclxuICAgICAgICAgIHByb2plY3RJZDogdGhpcy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICBkZWxpdmVyYWJsZUlkOiBudWxsLFxyXG4gICAgICAgICAgc3RhdHVzOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRTdGF0dXNOYW1lQnlJZChcclxuICAgICAgICAgICAgdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0VmFsdWVGb3JtT2JqZWN0QnlLZXlMaXN0KHJlc3BvbnNlLCBbJ1JfUE9fU1RBVFVTJywgJ01QTV9TdGF0dXMtaWQnLCAnSWQnXSwgMCwgJycpLFxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRWYWx1ZUZvcm1PYmplY3RCeUtleUxpc3QocmVzcG9uc2UsIFsnUl9QT19DQVRFR09SWScsICdNUE1fQ2F0ZWdvcnktaWQnLCAnSWQnXSwgMCwgJycpKSxcclxuICAgICAgICAgIG93bmVyOiB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5nZXRWYWx1ZUZvcm1PYmplY3RCeUtleUxpc3QocmVzcG9uc2UsIFsnUl9QT19QUk9KRUNUX09XTkVSJywgJ0lkZW50aXR5LWlkJywgJ0lkJ10sIDAsICcnKVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0T3dlcklkID0gdGVtcE1vZGFsLm93bmVyO1xyXG4gICAgICAgIHRoaXMudGVhbUlkID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0VmFsdWVGb3JtT2JqZWN0QnlLZXlMaXN0KHJlc3BvbnNlLCBbJ1JfUE9fVEVBTScsICdNUE1fVGVhbXMtaWQnLCAnSWQnXSwgMCwgbnVsbCk7XHJcbiAgICAgICAgaWYgKHRoaXMudGVhbUlkKSB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyVGVhbSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q1JBY3Rpb25zKCkuZmluZChkYXRhID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGFbJ01QTV9UZWFtcy1pZCddWydJZCddID09PSB0aGlzLnRlYW1JZDtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5pc0V4dGVybmFsVXNlciA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldFZhbHVlRm9ybU9iamVjdEJ5S2V5TGlzdChjdXJyVGVhbSwgWydNUE1fVGVhbV9Sb2xlX01hcHBpbmcnLCAnSVNfRVhURVJOQUwnXSwgMCwgZmFsc2UpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybU5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgICAgdGhpcy5sb2FkRGF0YU9uUmVmcmVzaCh0aGlzLnByb2plY3RJZCwgdGhpcy5kZWxpdmVyYWJsZUlkRnJvbVVybCwgZmFsc2UsIGZhbHNlLCB0ZW1wTW9kYWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgIHRoaXMubG9hZERhdGFPblJlZnJlc2godGhpcy5wcm9qZWN0SWQsIHRoaXMuZGVsaXZlcmFibGVJZCwgZmFsc2UsIGZhbHNlLCB0ZW1wTW9kYWwpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGxvYWREYXRhT25SZWZyZXNoKHByb2plY0lkOiBudW1iZXIsIGRlbGl2ZXJhYmxlSWQ6IG51bWJlciwgaXNDb250ZXh0Q2hhbmdlOiBib29sZWFuLCBpc0xvYWRNb3JlOiBib29sZWFuLCB0ZW1wTW9kYWw6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5pc1Njcm9sbERvd24gPSBmYWxzZTtcclxuICAgIGNvbnN0IGh0dHBBcnJheUxpc3QgPSBbXHJcbiAgICAgIHRoaXMuY29tbWVudHNTZXJ2aWNlLmdldENvbW1lbnRzRm9yUHJvamVjdChwcm9qZWNJZCwgZGVsaXZlcmFibGVJZCwgdGhpcy5pc0V4dGVybmFsVXNlciwgdGhpcy5jb21tZW50c0NvbmZpZy5wYWdlRGV0YWlscy5za2lwKVxyXG4gICAgXTtcclxuICAgIGRlbGl2ZXJhYmxlSWQgPyBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0VXNlcnNGb3JEZWxpdmVyYWJsZSh0aGlzLnJldmlld0RlbGl2ZXJhYmxlSWQgPyB0aGlzLnJldmlld0RlbGl2ZXJhYmxlSWQgOiBkZWxpdmVyYWJsZUlkKSkgOiBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0VXNlcnNGb3JQcm9qZWN0KHByb2plY0lkKSk7XHJcbiAgICBpZiAodGhpcy5pc01hbmFnZXIgJiYgIWlzQ29udGV4dENoYW5nZSkge1xyXG4gICAgICBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5jb21tZW50c1NlcnZpY2UuZ2V0UHJvamVjdEFuZERlbGl2ZXJhYmxlRGV0YWlscyhwcm9qZWNJZCkpO1xyXG4gICAgfSBlbHNlIGlmICghdGhpcy5pc01hbmFnZXIgJiYgIWlzQ29udGV4dENoYW5nZSkge1xyXG4gICAgICBodHRwQXJyYXlMaXN0LnB1c2godGhpcy5kZWxpdmVyYWJsZVNlcnZpY2UucmVhZERlbGl2ZXJhYmxlKChkZWxpdmVyYWJsZUlkICsgJycpKSk7XHJcbiAgICB9XHJcbiAgICBmb3JrSm9pbihodHRwQXJyYXlMaXN0KS5zdWJzY3JpYmUocmVzcG9uc2VzID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgaWYgKHJlc3BvbnNlcyAmJiByZXNwb25zZXMubGVuZ3RoID49IDIpIHtcclxuICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmNvbW1lbnRMaXN0ID0gIWlzTG9hZE1vcmUgPyByZXNwb25zZXNbMF0gOiBbLi4ucmVzcG9uc2VzWzBdLCAuLi50aGlzLmNvbW1lbnRzQ29uZmlnLmNvbW1lbnRMaXN0XTtcclxuICAgICAgICB0aGlzLnVzZXJMaXN0RGF0YSA9IHJlc3BvbnNlc1sxXTtcclxuICAgICAgICBpZiAocmVzcG9uc2VzWzJdKSB7XHJcbiAgICAgICAgICBpZiAodGVtcE1vZGFsICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3dlcklkID0gdGVtcE1vZGFsLm93bmVyO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHRoaXMuaXNNYW5hZ2VyKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdCA9IHJlc3BvbnNlc1syXSA/IHJlc3BvbnNlc1syXSA6IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdCA9IFt0ZW1wTW9kYWwsIC4uLnRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0XTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0ID0gcmVzcG9uc2VzWzJdID8gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZm9ybVByb2plY3REZWxpdmVyYWJsZU1vZGFsKFtyZXNwb25zZXNbMl1bJ0RlbGl2ZXJhYmxlJ11dLCBwcm9qZWNJZCwgdHJ1ZSkgOiBbXTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdFswXS5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QgPSBbLi4udGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QsIHRlbXBNb2RhbF07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAodGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVRoZUNvbW1lbnRUaXRsZSh0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdFswXSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNTY3JvbGxEb3duID0gcmVzcG9uc2VzWzBdLmxlbmd0aCA+IDAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0sIChlcnJvckRldGFpbCkgPT4ge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbi5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBjb21tZW50cyEnKTtcclxuICAgICAgdGhpcy5jb21tZW50c0NvbmZpZyA9IG51bGw7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICB9KTtcclxuXHJcblxyXG5cclxuICB9XHJcblxyXG4gIC8vIG5vdGlmaWNhdGlvbkhhbmRsZShtZW51SXRlbTpQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCk6XHJcblxyXG4gIG9uQ29tbWVudHNJbmRleENoYW5nZShtZW51SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwpOiB2b2lkIHtcclxuICAgIHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0ID0gdGhpcy5jb21tZW50c0NvbmZpZy5kZWxpdmVyYWJsZUxpc3QubWFwKGVsZW1lbnQgPT4ge1xyXG4gICAgICBlbGVtZW50LmlzQWN0aXZlID0gKChlbGVtZW50LmlzUHJvamVjdCAmJiBtZW51SXRlbS5pc1Byb2plY3QpID8gbWVudUl0ZW0ucHJvamVjdElkID09PSBlbGVtZW50LnByb2plY3RJZCA6IG1lbnVJdGVtLmRlbGl2ZXJhYmxlSWQgPT09IGVsZW1lbnQuZGVsaXZlcmFibGVJZCk7XHJcbiAgICAgIHJldHVybiBlbGVtZW50O1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmNvbW1lbnRzQ29uZmlnLnBhZ2VEZXRhaWxzID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UucmVzZXRQYWdlRGV0YWlscygpO1xyXG4gICAgdGhpcy5sb2FkRGF0YU9uUmVmcmVzaChtZW51SXRlbS5wcm9qZWN0SWQsIG1lbnVJdGVtLmRlbGl2ZXJhYmxlSWQsIHRydWUsIGZhbHNlLCBudWxsKTtcclxuICAgIHRoaXMudXBkYXRlVGhlQ29tbWVudFRpdGxlKG1lbnVJdGVtKTtcclxuICB9XHJcblxyXG4gIG9uQ3JlYXRpbmdOZXdDb21tZW50KGNvbW1lbnREZXRhaWxzOiBOZXdDb21tZW50TW9kYWwpOiB2b2lkIHtcclxuICAgIGNvbnN0IGN1cnJDb21tZW50SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwgPSB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdC5maW5kKChkYXRhOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCkgPT4gZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICBpZiAoY3VyckNvbW1lbnRJdGVtKSB7XHJcbiAgICAgIGNvbW1lbnREZXRhaWxzID0gT2JqZWN0LmFzc2lnbihjb21tZW50RGV0YWlscywgeyBQcm9qZWN0SUQ6IGN1cnJDb21tZW50SXRlbS5wcm9qZWN0SWQsIERlbGl2ZXJhYmxlSUQ6IGN1cnJDb21tZW50SXRlbS5kZWxpdmVyYWJsZUlkIH0pO1xyXG4gICAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5jcmVhdGVOZXdDb21tZW50KGNvbW1lbnREZXRhaWxzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgdGhpcy5jb21tZW50c0NvbmZpZy5wYWdlRGV0YWlscyA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLnJlc2V0UGFnZURldGFpbHMoKTtcclxuICAgICAgICAgIHRoaXMubG9hZERhdGFPblJlZnJlc2goY3VyckNvbW1lbnRJdGVtLnByb2plY3RJZCwgY3VyckNvbW1lbnRJdGVtLmRlbGl2ZXJhYmxlSWQsIHRydWUsIGZhbHNlLCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25Mb2FkaW5nTW9yZUNvbW1lbnRzKHBhZ2VEZXRhaWxzOiBhbnkpOiB2b2lkIHtcclxuICAgIHRoaXMuY29tbWVudHNDb25maWcucGFnZURldGFpbHMgPSBwYWdlRGV0YWlscztcclxuICAgIGNvbnN0IGN1cnJDb21tZW50SXRlbTogUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwgPSB0aGlzLmNvbW1lbnRzQ29uZmlnLmRlbGl2ZXJhYmxlTGlzdC5maW5kKChkYXRhOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCkgPT4gZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICB0aGlzLmxvYWREYXRhT25SZWZyZXNoKGN1cnJDb21tZW50SXRlbS5wcm9qZWN0SWQsIGN1cnJDb21tZW50SXRlbS5kZWxpdmVyYWJsZUlkLCB0cnVlLCB0cnVlLCBudWxsKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZVRoZUNvbW1lbnRUaXRsZShpbmRleENoYW5nZURhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKTogdm9pZCB7XHJcbiAgICB0aGlzLnRpdGxlVmFsdWUgPSBpbmRleENoYW5nZURhdGEuaXNQcm9qZWN0ID9cclxuICAgICAgKCdQcm9qZWN0OiAnICsgaW5kZXhDaGFuZ2VEYXRhLnRpdGxlTmFtZSkgOiAoJ0RlbGl2ZXJhYmxlOiAnICsgaW5kZXhDaGFuZ2VEYXRhLnRpdGxlTmFtZSk7XHJcbiAgICB0aGlzLmlzUmVhZG9ubHkgPSB0aGlzLmlzTWFuYWdlciA/ICEodGhpcy5wcm9qZWN0T3dlcklkID09PSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCkpIDogZmFsc2U7XHJcbiAgICB0aGlzLmlzUmVhZG9ubHkgPSAoaW5kZXhDaGFuZ2VEYXRhLnN0YXR1cyAmJiAoaW5kZXhDaGFuZ2VEYXRhLnN0YXR1cyA9PT0gJ0NPTVBMRVRFRCcgJiYgdGhpcy5pc01hbmFnZXIpKSA/IHRydWUgOiB0aGlzLmlzUmVhZG9ubHk7XHJcbiAgfVxyXG5cclxuICBoYW5kbGVUb2dnbGVUYXNrKGV2ZW50RGF0YSk6IHZvaWQge1xyXG4gICAgY29uc3QgY3VyckNvbW1lbnRJdGVtOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCA9IHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0LmZpbmQoKGRhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKSA9PiAhZGF0YS5pc0FjdGl2ZSk7XHJcbiAgICB0aGlzLmNoZWNrZWQgPSAhdGhpcy5jaGVja2VkO1xyXG4gICAgdGhpcy5vbkNvbW1lbnRzSW5kZXhDaGFuZ2UoY3VyckNvbW1lbnRJdGVtKTtcclxuICB9XHJcbiAgcmVmcmVzaENvbW1lbnRzKGV2ZW50RGF0YSk6IHZvaWQge1xyXG4gICAgY29uc3QgY3VyckNvbW1lbnRJdGVtOiBQcm9qZWN0RGVsaXZlcmFibGVNb2RhbCA9IHRoaXMuY29tbWVudHNDb25maWcuZGVsaXZlcmFibGVMaXN0LmZpbmQoKGRhdGE6IFByb2plY3REZWxpdmVyYWJsZU1vZGFsKSA9PiBkYXRhLmlzQWN0aXZlKTtcclxuICAgIHRoaXMub25Db21tZW50c0luZGV4Q2hhbmdlKGN1cnJDb21tZW50SXRlbSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGlmKHRoaXMuYWN0aXZhdGVkUm91dGUucGFyZW50IT1udWxsKXtcclxuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucGFyZW50LnBhcmFtcy5zdWJzY3JpYmUocGFyZW50Um91dGVQYXJhbXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RJZD1wYXJlbnRSb3V0ZVBhcmFtcy5wcm9qZWN0SWRcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5xdWVyeVBhcmFtcy5zdWJzY3JpYmUoXHJcbiAgICAgIHBhcmFtcyA9PiB7XHJcbiAgICAgICAgaWYgKHBhcmFtcy5kZWxpdmVyYWJsZUlkICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUlkRnJvbVVybCA9IHBhcmFtcy5kZWxpdmVyYWJsZUlkXHJcbiAgICAgICAgICB0aGlzLmZvcm1Ob3RpZmljYXRpb24gPSB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5mb3JtTm90aWZpY2F0aW9uID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZEZyb21VcmwgPSAwXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaW5pdENvbW1lbnRzKCk7XHJcbiAgICAgIH1cclxuICAgIClcclxuICB9XHJcbn1cclxuIl19