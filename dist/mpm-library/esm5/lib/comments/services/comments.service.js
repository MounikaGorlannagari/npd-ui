import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { CommentsUtilService } from './comments.util.service';
import * as acronui from '../../mpm-utils/auth/utility';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/util.service";
import * as i3 from "./comments.util.service";
var CommentsService = /** @class */ (function () {
    function CommentsService(appService, utilService, commentUtilService) {
        this.appService = appService;
        this.utilService = utilService;
        this.commentUtilService = commentUtilService;
        this.COMMENTS_NS = 'http://schemas/AcheronMPMCore/Comments/operations';
        this.DELIVERABLE_OPERATION = 'http://schemas/AcheronMPMCore/Deliverable/operations';
        this.SAVE_COMMENT = 'http://schemas.acheron.com/mpm/comments/bpm/1.0';
        this.CURSOR_NS = 'http://schemas.opentext.com/bps/entity/core';
        this.USER_PROJECT_DELIVERABLE_MAPPER = {};
        this.GET_R_PM_STATUS_REASON_WS = 'GetR_PM_STATUS_REASON';
        this.GET_R_PM_STATUS_REASON_NS = 'http://schemas/AcheronMPMCore/Comments/operations';
    }
    CommentsService.prototype.createNewComment = function (newComment) {
        var _this = this;
        var param = {
            CreateComment: newComment
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.SAVE_COMMENT, 'SaveComments', param)
                .subscribe(function (response) {
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.next(false);
                observer.complete();
            });
        });
    };
    CommentsService.prototype.getCommentsForProject = function (projectId, deliverableId, isExternal, skip) {
        var _this = this;
        var param = {
            ProjectID: projectId,
            DeliverableID: deliverableId,
            Cursor: {
                '@xmlns': this.CURSOR_NS,
                '@offset': skip,
                '@limit': 100
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.COMMENTS_NS, (isExternal && (isExternal === true || isExternal === 'true')) ? 'GetCommentsForExternalUser' : 'GetCommentsByProjectAndDeliverableID', param)
                .subscribe(function (response) {
                var statuses = acronui.findObjectsByProp(response, 'Comments');
                var commentList = _this.commentUtilService.formCommentsModalStructure(statuses);
                observer.next(commentList);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    CommentsService.prototype.getProjectAndDeliverableDetails = function (projectId) {
        var _this = this;
        var param = {
            ProjectID: projectId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELIVERABLE_OPERATION, 'GetActionAndUploadDeliverableByProjectID', param)
                .subscribe(function (response) {
                var statuses = acronui.findObjectsByProp(response, 'Deliverable');
                var commentDeliverableList = _this.commentUtilService.formProjectDeliverableModal(statuses, projectId, false);
                observer.next(commentDeliverableList);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    CommentsService.prototype.getUsersForProject = function (projectId) {
        var _this = this;
        var param = {
            ProjectID: projectId
        };
        return new Observable(function (observer) {
            if (_this.getUserProjectDeliverableMapper(projectId)) {
                observer.next(_this.getUserProjectDeliverableMapper(projectId));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.SAVE_COMMENT, 'GetUsersForProject', param)
                    .subscribe(function (response) {
                    var statuses = acronui.findObjectsByProp(response, 'User');
                    var userList = _this.commentUtilService.formUserForProjectDetails(statuses);
                    _this.setUserProjectDeliverableMapper(projectId, userList);
                    observer.next(userList);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    CommentsService.prototype.getUsersForDeliverable = function (deliverableId) {
        var _this = this;
        var param = {
            DeliverableID: deliverableId
        };
        return new Observable(function (observer) {
            if (_this.getUserProjectDeliverableMapper(deliverableId)) {
                observer.next(_this.getUserProjectDeliverableMapper(deliverableId));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.SAVE_COMMENT, 'GetUsersForDeliverables', param)
                    .subscribe(function (response) {
                    var statuses = acronui.findObjectsByProp(response, 'User');
                    var userList = _this.commentUtilService.formUserForProjectDetails(statuses);
                    _this.setUserProjectDeliverableMapper(deliverableId, userList);
                    observer.next(userList);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    CommentsService.prototype.clearUserProjectDeliverableMapper = function () {
        this.USER_PROJECT_DELIVERABLE_MAPPER = {};
    };
    CommentsService.prototype.setUserProjectDeliverableMapper = function (key, value) {
        this.USER_PROJECT_DELIVERABLE_MAPPER[key] = value;
    };
    CommentsService.prototype.getUserProjectDeliverableMapper = function (key) {
        if (this.USER_PROJECT_DELIVERABLE_MAPPER[key]) {
            return this.USER_PROJECT_DELIVERABLE_MAPPER[key];
        }
        return null;
    };
    CommentsService.prototype.getReasonsByCommentId = function (commentId) {
        var _this = this;
        var getRequestObject = {
            'Comments-id': {
                Id: commentId
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.GET_R_PM_STATUS_REASON_NS, _this.GET_R_PM_STATUS_REASON_WS, getRequestObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                // this.notificationService.error('Something went wrong while getting StatusReason.');
                observer.error(error);
            });
        });
    };
    CommentsService.ctorParameters = function () { return [
        { type: AppService },
        { type: UtilService },
        { type: CommentsUtilService }
    ]; };
    CommentsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function CommentsService_Factory() { return new CommentsService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.UtilService), i0.ɵɵinject(i3.CommentsUtilService)); }, token: CommentsService, providedIn: "root" });
    CommentsService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CommentsService);
    return CommentsService;
}());
export { CommentsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2NvbW1lbnRzL3NlcnZpY2VzL2NvbW1lbnRzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sS0FBSyxPQUFPLE1BQU0sOEJBQThCLENBQUM7Ozs7O0FBS3hEO0lBRUUseUJBQ1MsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsa0JBQXVDO1FBRnZDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUVoRCxnQkFBVyxHQUFHLG1EQUFtRCxDQUFDO1FBQ2xFLDBCQUFxQixHQUFHLHNEQUFzRCxDQUFDO1FBQy9FLGlCQUFZLEdBQUcsaURBQWlELENBQUM7UUFDakUsY0FBUyxHQUFHLDZDQUE2QyxDQUFDO1FBQzFELG9DQUErQixHQUFHLEVBQUUsQ0FBQztRQUVyQyw4QkFBeUIsR0FBRyx1QkFBdUIsQ0FBQztRQUNwRCw4QkFBeUIsR0FBRyxtREFBbUQsQ0FBQztJQVI1RSxDQUFDO0lBVUwsMENBQWdCLEdBQWhCLFVBQWlCLFVBQTJCO1FBQTVDLGlCQWNDO1FBYkMsSUFBTSxLQUFLLEdBQUc7WUFDWixhQUFhLEVBQUUsVUFBVTtTQUMxQixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxjQUFjLEVBQUUsS0FBSyxDQUFDO2lCQUNwRSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDTixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwrQ0FBcUIsR0FBckIsVUFBc0IsU0FBaUIsRUFBRSxhQUFxQixFQUFFLFVBQWUsRUFBRSxJQUFZO1FBQTdGLGlCQXFCQztRQXBCQyxJQUFNLEtBQUssR0FBRztZQUNaLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLGFBQWEsRUFBRSxhQUFhO1lBQzVCLE1BQU0sRUFBRTtnQkFDTixRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0JBQ3hCLFNBQVMsRUFBRSxJQUFJO2dCQUNmLFFBQVEsRUFBRSxHQUFHO2FBQ2Q7U0FDRixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLElBQUksVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQyxzQ0FBc0MsRUFBRSxLQUFLLENBQUM7aUJBQzNMLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ2pFLElBQU0sV0FBVyxHQUF5QixLQUFJLENBQUMsa0JBQWtCLENBQUMsMEJBQTBCLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3ZHLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx5REFBK0IsR0FBL0IsVUFBZ0MsU0FBaUI7UUFBakQsaUJBZUM7UUFkQyxJQUFNLEtBQUssR0FBRztZQUNaLFNBQVMsRUFBRSxTQUFTO1NBQ3JCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMscUJBQXFCLEVBQUUsMENBQTBDLEVBQUUsS0FBSyxDQUFDO2lCQUN6RyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNqQixJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2dCQUNwRSxJQUFNLHNCQUFzQixHQUFtQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDL0ksUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUN0QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDTixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQWtCLEdBQWxCLFVBQW1CLFNBQWlCO1FBQXBDLGlCQXFCQztRQXBCQyxJQUFNLEtBQUssR0FBRztZQUNaLFNBQVMsRUFBRSxTQUFTO1NBQ3JCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFJLEtBQUksQ0FBQywrQkFBK0IsQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDbkQsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsK0JBQStCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDL0QsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3JCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsS0FBSyxDQUFDO3FCQUMxRSxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNqQixJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUM3RCxJQUFNLFFBQVEsR0FBeUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNuRyxLQUFJLENBQUMsK0JBQStCLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUMxRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdEQUFzQixHQUF0QixVQUF1QixhQUFxQjtRQUE1QyxpQkFxQkM7UUFwQkMsSUFBTSxLQUFLLEdBQUc7WUFDWixhQUFhLEVBQUUsYUFBYTtTQUM3QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxLQUFJLENBQUMsK0JBQStCLENBQUMsYUFBYSxDQUFDLEVBQUU7Z0JBQ3ZELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLCtCQUErQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ25FLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLHlCQUF5QixFQUFFLEtBQUssQ0FBQztxQkFDL0UsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDakIsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsSUFBTSxRQUFRLEdBQXlCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkcsS0FBSSxDQUFDLCtCQUErQixDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwyREFBaUMsR0FBakM7UUFDRSxJQUFJLENBQUMsK0JBQStCLEdBQUcsRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCx5REFBK0IsR0FBL0IsVUFBZ0MsR0FBVyxFQUFFLEtBQTJCO1FBQ3RFLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUM7SUFDcEQsQ0FBQztJQUNELHlEQUErQixHQUEvQixVQUFnQyxHQUFXO1FBQ3pDLElBQUksSUFBSSxDQUFDLCtCQUErQixDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzdDLE9BQU8sSUFBSSxDQUFDLCtCQUErQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2xEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsK0NBQXFCLEdBQXJCLFVBQXNCLFNBQVM7UUFBL0IsaUJBaUJDO1FBaEJDLElBQU0sZ0JBQWdCLEdBQUc7WUFDdkIsYUFBYSxFQUFFO2dCQUNYLEVBQUUsRUFBRSxTQUFTO2FBQ2hCO1NBQ0YsQ0FBQztRQUNBLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyx5QkFBeUIsRUFBRSxLQUFJLENBQUMseUJBQXlCLEVBQUUsZ0JBQWdCLENBQUM7aUJBQzFHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0wsc0ZBQXNGO2dCQUNyRixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFFTCxDQUFDOztnQkFsSm9CLFVBQVU7Z0JBQ1QsV0FBVztnQkFDSixtQkFBbUI7OztJQUxyQyxlQUFlO1FBSDNCLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyxlQUFlLENBc0ozQjswQkFqS0Q7Q0FpS0MsQUF0SkQsSUFzSkM7U0F0SlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tbWVudHNNb2RhbCwgUHJvamVjdERlbGl2ZXJhYmxlTW9kYWwsIE5ld0NvbW1lbnRNb2RhbCwgVXNlckluZm9Nb2RhbCB9IGZyb20gJy4uL29iamVjdHMvY29tbWVudC5tb2RhbCc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbW1lbnRzVXRpbFNlcnZpY2UgfSBmcm9tICcuL2NvbW1lbnRzLnV0aWwuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50c1NlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBjb21tZW50VXRpbFNlcnZpY2U6IENvbW1lbnRzVXRpbFNlcnZpY2VcclxuICApIHsgfVxyXG4gIENPTU1FTlRTX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0NvbW1lbnRzL29wZXJhdGlvbnMnO1xyXG4gIERFTElWRVJBQkxFX09QRVJBVElPTiA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9EZWxpdmVyYWJsZS9vcGVyYXRpb25zJztcclxuICBTQVZFX0NPTU1FTlQgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvbW1lbnRzL2JwbS8xLjAnO1xyXG4gIENVUlNPUl9OUyA9ICdodHRwOi8vc2NoZW1hcy5vcGVudGV4dC5jb20vYnBzL2VudGl0eS9jb3JlJztcclxuICBVU0VSX1BST0pFQ1RfREVMSVZFUkFCTEVfTUFQUEVSID0ge307XHJcblxyXG4gIEdFVF9SX1BNX1NUQVRVU19SRUFTT05fV1MgPSAnR2V0Ul9QTV9TVEFUVVNfUkVBU09OJztcclxuICBHRVRfUl9QTV9TVEFUVVNfUkVBU09OX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0NvbW1lbnRzL29wZXJhdGlvbnMnO1xyXG5cclxuICBjcmVhdGVOZXdDb21tZW50KG5ld0NvbW1lbnQ6IE5ld0NvbW1lbnRNb2RhbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgQ3JlYXRlQ29tbWVudDogbmV3Q29tbWVudFxyXG4gICAgfTtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU0FWRV9DT01NRU5ULCAnU2F2ZUNvbW1lbnRzJywgcGFyYW0pXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldENvbW1lbnRzRm9yUHJvamVjdChwcm9qZWN0SWQ6IG51bWJlciwgZGVsaXZlcmFibGVJZDogbnVtYmVyLCBpc0V4dGVybmFsOiBhbnksIHNraXA6IG51bWJlcik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgUHJvamVjdElEOiBwcm9qZWN0SWQsXHJcbiAgICAgIERlbGl2ZXJhYmxlSUQ6IGRlbGl2ZXJhYmxlSWQsXHJcbiAgICAgIEN1cnNvcjoge1xyXG4gICAgICAgICdAeG1sbnMnOiB0aGlzLkNVUlNPUl9OUyxcclxuICAgICAgICAnQG9mZnNldCc6IHNraXAsXHJcbiAgICAgICAgJ0BsaW1pdCc6IDEwMFxyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DT01NRU5UU19OUywgKGlzRXh0ZXJuYWwgJiYgKGlzRXh0ZXJuYWwgPT09IHRydWUgfHwgaXNFeHRlcm5hbCA9PT0gJ3RydWUnKSkgPyAnR2V0Q29tbWVudHNGb3JFeHRlcm5hbFVzZXInIDogJ0dldENvbW1lbnRzQnlQcm9qZWN0QW5kRGVsaXZlcmFibGVJRCcsIHBhcmFtKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgY29uc3Qgc3RhdHVzZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnQ29tbWVudHMnKTtcclxuICAgICAgICAgIGNvbnN0IGNvbW1lbnRMaXN0OiBBcnJheTxDb21tZW50c01vZGFsPiA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmZvcm1Db21tZW50c01vZGFsU3RydWN0dXJlKHN0YXR1c2VzKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoY29tbWVudExpc3QpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldFByb2plY3RBbmREZWxpdmVyYWJsZURldGFpbHMocHJvamVjdElkOiBudW1iZXIpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgY29uc3QgcGFyYW0gPSB7XHJcbiAgICAgIFByb2plY3RJRDogcHJvamVjdElkXHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxJVkVSQUJMRV9PUEVSQVRJT04sICdHZXRBY3Rpb25BbmRVcGxvYWREZWxpdmVyYWJsZUJ5UHJvamVjdElEJywgcGFyYW0pXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBzdGF0dXNlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdEZWxpdmVyYWJsZScpO1xyXG4gICAgICAgICAgY29uc3QgY29tbWVudERlbGl2ZXJhYmxlTGlzdDogQXJyYXk8UHJvamVjdERlbGl2ZXJhYmxlTW9kYWw+ID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZm9ybVByb2plY3REZWxpdmVyYWJsZU1vZGFsKHN0YXR1c2VzLCBwcm9qZWN0SWQsIGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQoY29tbWVudERlbGl2ZXJhYmxlTGlzdCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0VXNlcnNGb3JQcm9qZWN0KHByb2plY3RJZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICBQcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgfTtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmICh0aGlzLmdldFVzZXJQcm9qZWN0RGVsaXZlcmFibGVNYXBwZXIocHJvamVjdElkKSkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5nZXRVc2VyUHJvamVjdERlbGl2ZXJhYmxlTWFwcGVyKHByb2plY3RJZCkpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5TQVZFX0NPTU1FTlQsICdHZXRVc2Vyc0ZvclByb2plY3QnLCBwYXJhbSlcclxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXNlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdVc2VyJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0OiBBcnJheTxVc2VySW5mb01vZGFsPiA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmZvcm1Vc2VyRm9yUHJvamVjdERldGFpbHMoc3RhdHVzZXMpO1xyXG4gICAgICAgICAgICB0aGlzLnNldFVzZXJQcm9qZWN0RGVsaXZlcmFibGVNYXBwZXIocHJvamVjdElkLCB1c2VyTGlzdCk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlckxpc3QpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRVc2Vyc0ZvckRlbGl2ZXJhYmxlKGRlbGl2ZXJhYmxlSWQ6IG51bWJlcik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgRGVsaXZlcmFibGVJRDogZGVsaXZlcmFibGVJZFxyXG4gICAgfTtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmICh0aGlzLmdldFVzZXJQcm9qZWN0RGVsaXZlcmFibGVNYXBwZXIoZGVsaXZlcmFibGVJZCkpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0VXNlclByb2plY3REZWxpdmVyYWJsZU1hcHBlcihkZWxpdmVyYWJsZUlkKSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlNBVkVfQ09NTUVOVCwgJ0dldFVzZXJzRm9yRGVsaXZlcmFibGVzJywgcGFyYW0pXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgY29uc3Qgc3RhdHVzZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnVXNlcicpO1xyXG4gICAgICAgICAgICBjb25zdCB1c2VyTGlzdDogQXJyYXk8VXNlckluZm9Nb2RhbD4gPSB0aGlzLmNvbW1lbnRVdGlsU2VydmljZS5mb3JtVXNlckZvclByb2plY3REZXRhaWxzKHN0YXR1c2VzKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRVc2VyUHJvamVjdERlbGl2ZXJhYmxlTWFwcGVyKGRlbGl2ZXJhYmxlSWQsIHVzZXJMaXN0KTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyTGlzdCk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNsZWFyVXNlclByb2plY3REZWxpdmVyYWJsZU1hcHBlcigpIHtcclxuICAgIHRoaXMuVVNFUl9QUk9KRUNUX0RFTElWRVJBQkxFX01BUFBFUiA9IHt9O1xyXG4gIH1cclxuXHJcbiAgc2V0VXNlclByb2plY3REZWxpdmVyYWJsZU1hcHBlcihrZXk6IG51bWJlciwgdmFsdWU6IEFycmF5PFVzZXJJbmZvTW9kYWw+KTogdm9pZCB7XHJcbiAgICB0aGlzLlVTRVJfUFJPSkVDVF9ERUxJVkVSQUJMRV9NQVBQRVJba2V5XSA9IHZhbHVlO1xyXG4gIH1cclxuICBnZXRVc2VyUHJvamVjdERlbGl2ZXJhYmxlTWFwcGVyKGtleTogbnVtYmVyKTogQXJyYXk8VXNlckluZm9Nb2RhbD4ge1xyXG4gICAgaWYgKHRoaXMuVVNFUl9QUk9KRUNUX0RFTElWRVJBQkxFX01BUFBFUltrZXldKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLlVTRVJfUFJPSkVDVF9ERUxJVkVSQUJMRV9NQVBQRVJba2V5XTtcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuXHJcbiAgZ2V0UmVhc29uc0J5Q29tbWVudElkKGNvbW1lbnRJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAnQ29tbWVudHMtaWQnOiB7XHJcbiAgICAgICAgICBJZDogY29tbWVudElkXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfUl9QTV9TVEFUVVNfUkVBU09OX05TLCB0aGlzLkdFVF9SX1BNX1NUQVRVU19SRUFTT05fV1MsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgIC8vIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBTdGF0dXNSZWFzb24uJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gIH1cclxufVxyXG4iXX0=