import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
var CommentsModalComponent = /** @class */ (function () {
    function CommentsModalComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    CommentsModalComponent.prototype.closeCommentsPopup = function (data) {
        if (this.dialogRef) {
            this.dialogRef.close(data);
        }
    };
    CommentsModalComponent.prototype.ngOnInit = function () {
        if (this.data) {
            this.projectId = this.data.projectId;
            this.isManager = this.data.isManager;
            this.deliverableId = this.data.deliverableId;
            this.reviewDeliverableId = this.data.reviewDeliverableId;
            this.enableToComment = this.data.enableToComment;
            // this.isExternalUser = this.data.isExternalUser;
        }
    };
    CommentsModalComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    CommentsModalComponent = __decorate([
        Component({
            selector: 'mpm-comments-modal',
            template: "<mat-dialog-content>\r\n    <button matTooltip=\"Close Comments\" mat-icon-button color=\"primary\" [mat-dialog-close]=\"true\"\r\n        class=\"mat-icon-close-btn\">\r\n        <mat-icon>close</mat-icon>\r\n    </button>\r\n    <mpm-comments [projectId]=\"projectId\" [isManager]=\"isManager\" [deliverableId]=\"deliverableId\" [reviewDeliverableId]=\"reviewDeliverableId\" [enableToComment]=\"enableToComment\"></mpm-comments>\r\n</mat-dialog-content>",
            styles: ["mat-dialog-content{margin:0;padding:0;max-height:90vh;height:88vh;position:relative}.mat-icon-close-btn{top:-4px;right:.5em;position:absolute}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], CommentsModalComponent);
    return CommentsModalComponent;
}());
export { CommentsModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMtbW9kYWwvY29tbWVudHMtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFO0lBT0UsZ0NBQ1MsU0FBK0MsRUFDdEIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUFzQztRQUN0QixTQUFJLEdBQUosSUFBSSxDQUFBO0lBQ2xDLENBQUM7SUFFTCxtREFBa0IsR0FBbEIsVUFBbUIsSUFBSTtRQUNyQixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQseUNBQVEsR0FBUjtRQUNFLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNiLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQzdDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3pELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7WUFDbEQsa0RBQWtEO1NBQ2xEO0lBQ0gsQ0FBQzs7Z0JBbkJtQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTs7SUFUZCxzQkFBc0I7UUFMbEMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixtZEFBOEM7O1NBRS9DLENBQUM7UUFVRyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtPQVRmLHNCQUFzQixDQTZCbEM7SUFBRCw2QkFBQztDQUFBLEFBN0JELElBNkJDO1NBN0JZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY29tbWVudHMtbW9kYWwnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50cy1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29tbWVudHMtbW9kYWwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudHNNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgcHJvamVjdElkOiBudW1iZXI7XHJcbiAgaXNNYW5hZ2VyOiBib29sZWFuO1xyXG4gIGRlbGl2ZXJhYmxlSWQ6IG51bWJlcjtcclxuICByZXZpZXdEZWxpdmVyYWJsZUlkOiBudW1iZXI7XHJcbiAgZW5hYmxlVG9Db21tZW50OiBib29sZWFuO1xyXG4gIGlzRXh0ZXJuYWxVc2VyOiBhbnk7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8Q29tbWVudHNNb2RhbENvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGEsXHJcbiAgKSB7IH1cclxuXHJcbiAgY2xvc2VDb21tZW50c1BvcHVwKGRhdGEpIHtcclxuICAgIGlmICh0aGlzLmRpYWxvZ1JlZikge1xyXG4gICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShkYXRhKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICB0aGlzLnByb2plY3RJZCA9IHRoaXMuZGF0YS5wcm9qZWN0SWQ7XHJcbiAgICAgIHRoaXMuaXNNYW5hZ2VyID0gdGhpcy5kYXRhLmlzTWFuYWdlcjtcclxuICAgICAgdGhpcy5kZWxpdmVyYWJsZUlkID0gdGhpcy5kYXRhLmRlbGl2ZXJhYmxlSWQ7XHJcbiAgICAgIHRoaXMucmV2aWV3RGVsaXZlcmFibGVJZCA9IHRoaXMuZGF0YS5yZXZpZXdEZWxpdmVyYWJsZUlkO1xyXG4gICAgICB0aGlzLmVuYWJsZVRvQ29tbWVudCA9IHRoaXMuZGF0YS5lbmFibGVUb0NvbW1lbnQ7XHJcbiAgICAgLy8gdGhpcy5pc0V4dGVybmFsVXNlciA9IHRoaXMuZGF0YS5pc0V4dGVybmFsVXNlcjtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==