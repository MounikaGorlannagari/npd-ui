import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
var CommentsIndexComponent = /** @class */ (function () {
    function CommentsIndexComponent(activatedroute) {
        this.activatedroute = activatedroute;
        this.commentsIndexChange = new EventEmitter();
    }
    CommentsIndexComponent.prototype.ngOnChanges = function (changes) {
    };
    CommentsIndexComponent.prototype.ngOnInit = function () {
    };
    CommentsIndexComponent.prototype.routeToSelectedMenu = function (menuItem) {
        this.deliverableIdFromUrl = 0;
        this.commentsIndexChange.emit(menuItem);
    };
    CommentsIndexComponent.prototype.changeIsActiveOnNotification = function (event) {
        if (event.deliverableId === this.deliverableIdFromUrl)
            event.isActive = true;
        return event.isActive;
    };
    CommentsIndexComponent.ctorParameters = function () { return [
        { type: ActivatedRoute }
    ]; };
    __decorate([
        Input()
    ], CommentsIndexComponent.prototype, "commentDeliverableList", void 0);
    __decorate([
        Output()
    ], CommentsIndexComponent.prototype, "commentsIndexChange", void 0);
    __decorate([
        Input()
    ], CommentsIndexComponent.prototype, "notifiactionValue", void 0);
    __decorate([
        Input()
    ], CommentsIndexComponent.prototype, "deliverableIdFromUrl", void 0);
    CommentsIndexComponent = __decorate([
        Component({
            selector: 'mpm-comments-index',
            template: "<mat-nav-list class=\"comments-overview-side-menu\">\r\n    <a *ngFor=\"let menuItem of commentDeliverableList\" mat-list-item class=\"menu-item\"\r\n        [class.active]=\"changeIsActiveOnNotification(menuItem)\" (click)=\"routeToSelectedMenu(menuItem)\"\r\n        matTooltip=\"{{menuItem.titleName}}\" matTooltipClass=\"custom-tooltip\">\r\n        <mat-icon class=\"comment-index-icon\" *ngIf=\"!menuItem.isProject\">{{menuItem.icon}}</mat-icon>\r\n        <span class=\"menu-text\">{{menuItem.titleName}}</span>\r\n    </a>\r\n</mat-nav-list>",
            styles: ["mat-nav-list.comments-overview-side-menu a.active{background:#c0ca33;color:rgba(0,0,0,.87)}.comments-overview-side-menu .comment-index-icon{font-size:14px;padding-top:12px;padding-left:10px}.comments-overview-side-menu span.menu-text{overflow:hidden;flex-grow:1;width:200px;max-height:20px;text-overflow:ellipsis;white-space:nowrap}"]
        })
    ], CommentsIndexComponent);
    return CommentsIndexComponent;
}());
export { CommentsIndexComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtaW5kZXguY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMtaW5kZXgvY29tbWVudHMtaW5kZXguY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQUN6RyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFTakQ7SUFPRSxnQ0FBbUIsY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTHZDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFNeEQsQ0FBQztJQUNELDRDQUFXLEdBQVgsVUFBWSxPQUFzQjtJQUNsQyxDQUFDO0lBRUQseUNBQVEsR0FBUjtJQUVBLENBQUM7SUFDRCxvREFBbUIsR0FBbkIsVUFBb0IsUUFBUTtRQUMxQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxDQUFBO1FBQzdCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELDZEQUE0QixHQUE1QixVQUE2QixLQUFLO1FBQ2hDLElBQUksS0FBSyxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsb0JBQW9CO1lBQ25ELEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO1FBQ3ZCLE9BQU8sS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUN4QixDQUFDOztnQkFqQmtDLGNBQWM7O0lBTnhDO1FBQVIsS0FBSyxFQUFFOzBFQUF3RDtJQUN0RDtRQUFULE1BQU0sRUFBRTt1RUFBK0M7SUFDL0M7UUFBUixLQUFLLEVBQUU7cUVBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO3dFQUFxQjtJQUpsQixzQkFBc0I7UUFMbEMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixpakJBQThDOztTQUUvQyxDQUFDO09BQ1csc0JBQXNCLENBeUJsQztJQUFELDZCQUFDO0NBQUEsQUF6QkQsSUF5QkM7U0F6Qlksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVEhJU19FWFBSIH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXIvc3JjL291dHB1dC9vdXRwdXRfYXN0JztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgUm91dGluZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvcm91dGluZ0NvbnN0YW50cyc7XHJcbmltcG9ydCB7IFByb2plY3REZWxpdmVyYWJsZU1vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9jb21tZW50Lm1vZGFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWNvbW1lbnRzLWluZGV4JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudHMtaW5kZXguY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1lbnRzLWluZGV4LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRzSW5kZXhDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgQElucHV0KCkgY29tbWVudERlbGl2ZXJhYmxlTGlzdDogQXJyYXk8UHJvamVjdERlbGl2ZXJhYmxlTW9kYWw+O1xyXG4gIEBPdXRwdXQoKSBjb21tZW50c0luZGV4Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgbm90aWZpYWN0aW9uVmFsdWU7XHJcbiAgQElucHV0KCkgZGVsaXZlcmFibGVJZEZyb21VcmxcclxuICBkSWRUb3Bhc3M7XHJcbiAgbWVudUl0ZW1Ub1Bhc3M7XHJcbiAgY29uc3RydWN0b3IocHVibGljIGFjdGl2YXRlZHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge1xyXG4gIH1cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHJcbiAgfVxyXG4gIHJvdXRlVG9TZWxlY3RlZE1lbnUobWVudUl0ZW0pOiB2b2lkIHtcclxuICAgIHRoaXMuZGVsaXZlcmFibGVJZEZyb21VcmwgPSAwXHJcbiAgICB0aGlzLmNvbW1lbnRzSW5kZXhDaGFuZ2UuZW1pdChtZW51SXRlbSk7XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VJc0FjdGl2ZU9uTm90aWZpY2F0aW9uKGV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQuZGVsaXZlcmFibGVJZCA9PT0gdGhpcy5kZWxpdmVyYWJsZUlkRnJvbVVybClcclxuICAgICAgZXZlbnQuaXNBY3RpdmUgPSB0cnVlXHJcbiAgICByZXR1cm4gZXZlbnQuaXNBY3RpdmU7XHJcbiAgfVxyXG59XHJcbiJdfQ==