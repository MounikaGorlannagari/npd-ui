import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsLayoutComponent } from './comments-layout/comments-layout.component';
import { CommentsIndexComponent } from './comments-index/comments-index.component';
import { CommentsMessageComponent } from './comments-message/comments-message.component';
import { CommentsTextComponent } from './comments-text/comments-text.component';
import { CommentsComponent } from './comments.component';
import { MaterialModule } from '../material.module';
import { CommentsMessageLayoutComponent } from './comments-message-layout/comments-message-layout.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommentsModalComponent } from './comments-modal/comments-modal.component';
import { BulkCommentsComponent } from './bulk-comments/bulk-comments.component';
var CommentsModule = /** @class */ (function () {
    function CommentsModule() {
    }
    CommentsModule = __decorate([
        NgModule({
            declarations: [
                CommentsLayoutComponent,
                CommentsIndexComponent,
                CommentsMessageComponent,
                CommentsTextComponent,
                CommentsComponent,
                CommentsMessageLayoutComponent,
                CommentsModalComponent,
                BulkCommentsComponent
            ],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [
                CommentsLayoutComponent,
                CommentsIndexComponent,
                CommentsMessageComponent,
                CommentsTextComponent,
                CommentsComponent,
                CommentsMessageLayoutComponent,
                CommentsModalComponent,
                BulkCommentsComponent
            ],
            providers: [
                { provide: MatDialogRef, useValue: null },
                { provide: MAT_DIALOG_DATA, useValue: null }
            ]
        })
    ], CommentsModule);
    return CommentsModule;
}());
export { CommentsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvY29tbWVudHMvY29tbWVudHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUN0RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNuRixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sNkRBQTZELENBQUM7QUFDN0csT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNuRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQWdDaEY7SUFBQTtJQUE4QixDQUFDO0lBQWxCLGNBQWM7UUE5QjFCLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRTtnQkFDWix1QkFBdUI7Z0JBQ3ZCLHNCQUFzQjtnQkFDdEIsd0JBQXdCO2dCQUN4QixxQkFBcUI7Z0JBQ3JCLGlCQUFpQjtnQkFDakIsOEJBQThCO2dCQUM5QixzQkFBc0I7Z0JBQ3RCLHFCQUFxQjthQUN0QjtZQUNELE9BQU8sRUFBRTtnQkFDUCxZQUFZO2dCQUNaLGNBQWM7YUFDZjtZQUNELE9BQU8sRUFBRTtnQkFDUCx1QkFBdUI7Z0JBQ3ZCLHNCQUFzQjtnQkFDdEIsd0JBQXdCO2dCQUN4QixxQkFBcUI7Z0JBQ3JCLGlCQUFpQjtnQkFDakIsOEJBQThCO2dCQUM5QixzQkFBc0I7Z0JBQ3RCLHFCQUFxQjthQUN0QjtZQUNELFNBQVMsRUFBRTtnQkFDVCxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtnQkFDekMsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7YUFDN0M7U0FDRixDQUFDO09BQ1csY0FBYyxDQUFJO0lBQUQscUJBQUM7Q0FBQSxBQUEvQixJQUErQjtTQUFsQixjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgQ29tbWVudHNMYXlvdXRDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLWxheW91dC9jb21tZW50cy1sYXlvdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29tbWVudHNJbmRleENvbXBvbmVudCB9IGZyb20gJy4vY29tbWVudHMtaW5kZXgvY29tbWVudHMtaW5kZXguY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29tbWVudHNNZXNzYWdlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21tZW50cy1tZXNzYWdlL2NvbW1lbnRzLW1lc3NhZ2UuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29tbWVudHNUZXh0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21tZW50cy10ZXh0L2NvbW1lbnRzLXRleHQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29tbWVudHNDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgQ29tbWVudHNNZXNzYWdlTGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21tZW50cy1tZXNzYWdlLWxheW91dC9jb21tZW50cy1tZXNzYWdlLWxheW91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IENvbW1lbnRzTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbW1lbnRzLW1vZGFsL2NvbW1lbnRzLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEJ1bGtDb21tZW50c0NvbXBvbmVudCB9IGZyb20gJy4vYnVsay1jb21tZW50cy9idWxrLWNvbW1lbnRzLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgQ29tbWVudHNMYXlvdXRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c0luZGV4Q29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNNZXNzYWdlQ29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNUZXh0Q29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01lc3NhZ2VMYXlvdXRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01vZGFsQ29tcG9uZW50LFxyXG4gICAgQnVsa0NvbW1lbnRzQ29tcG9uZW50XHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgQ29tbWVudHNMYXlvdXRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c0luZGV4Q29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNNZXNzYWdlQ29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNUZXh0Q29tcG9uZW50LFxyXG4gICAgQ29tbWVudHNDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01lc3NhZ2VMYXlvdXRDb21wb25lbnQsXHJcbiAgICBDb21tZW50c01vZGFsQ29tcG9uZW50LFxyXG4gICAgQnVsa0NvbW1lbnRzQ29tcG9uZW50XHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHsgcHJvdmlkZTogTWF0RGlhbG9nUmVmLCB1c2VWYWx1ZTogbnVsbCB9LFxyXG4gICAgeyBwcm92aWRlOiBNQVRfRElBTE9HX0RBVEEsIHVzZVZhbHVlOiBudWxsIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50c01vZHVsZSB7IH1cclxuIl19