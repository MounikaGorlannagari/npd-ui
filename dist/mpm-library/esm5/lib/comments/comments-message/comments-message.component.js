import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CommentsUtilService } from '../services/comments.util.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { CommentsService } from '../services/comments.service';
var CommentsMessageComponent = /** @class */ (function () {
    function CommentsMessageComponent(commentUtilService, sharingService, commentsService) {
        this.commentUtilService = commentUtilService;
        this.sharingService = sharingService;
        this.commentsService = commentsService;
        this.replyToComment = new EventEmitter();
        this.clickingOnReplyMessage = new EventEmitter();
        this.commentTextTemp = null;
        this.currUserId = null;
        this.arrayInfo = [];
        this.reasonInfo = [];
        this.restartReasonInfo = [];
        this.currUserId = this.sharingService.getCurrentUserItemID();
    }
    CommentsMessageComponent.prototype.ngOnInit = function () {
        this.hover = false;
        this.commentTextTemp = this.handleUserInfoDetailsTemp(this.commentData.commentText, this.userListData.map(function (data) { return data.displayName; }));
    };
    CommentsMessageComponent.prototype.onclickToReply = function (data) {
        if (this.replyToComment && typeof this.replyToComment.emit === 'function') {
            this.replyToComment.emit(data);
        }
    };
    CommentsMessageComponent.prototype.toggleEvent = function (event) {
        this.hover = event;
    };
    CommentsMessageComponent.prototype.handleDateManipulation = function (dateString) {
        if (!dateString) {
            return '';
        }
        if (dateString && !(dateString instanceof Date)) {
            dateString = new Date(dateString);
        }
        var numericValueDiff = 0;
        numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'day');
        if (numericValueDiff < 1) {
            numericValueDiff = this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours');
            if (numericValueDiff < 1) {
                return Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'minutes')) + ' mins ago.';
            }
            else {
                return Math.floor(this.commentUtilService.getDaysBetweenTwoDates(new Date(), dateString, 'hours')) + ' hours ago.';
            }
        }
        return this.commentUtilService.getDateByGiveFormat(dateString, 'MMM D Y, h:mm a');
    };
    CommentsMessageComponent.prototype.onParentCommentNavigation = function (parentCommentId) {
        this.clickingOnReplyMessage.emit(parentCommentId);
    };
    CommentsMessageComponent.prototype.handleUserInfoDetailsTemp = function (commentText, displayName) {
        var _this = this;
        if (this.commentData.commentId) {
            this.reasonInfo = [];
            // removed for code hardening
            //    this.commentsService.getReasonsByCommentId(this.commentData.commentId).subscribe(reasonResponse => {
            /*    console.log(reasonResponse);
               const reasons = reasonResponse && reasonResponse.MPM_Status_Reason ? reasonResponse.MPM_Status_Reason : null;
               if (reasons) {
                 if (Array.isArray(reasons)) {
                   reasons.forEach(reason => {
                     this.reasonInfo.push(reason.NAME);
                   });
                 } else {
                   this.reasonInfo.push(reasons.NAME);
                 }
               }
               console.log(this.reasonInfo); */
            if (!commentText) {
                return 'SUCCESS';
            }
            var positionofAt = commentText.indexOf('@');
            if (positionofAt >= 0) {
                this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
                commentText = commentText.substr(positionofAt + 1);
                var isUserThere_1 = false;
                displayName.forEach(function (data) {
                    if (commentText.indexOf(data) === 0) {
                        isUserThere_1 = true;
                        _this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
                        commentText = commentText.substr(data.length);
                    }
                });
                if (!isUserThere_1) {
                    this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
                }
            }
            else {
                if (commentText && commentText.split(':') && commentText.split(':').length > 1) {
                    this.restartReasonInfo = [];
                    this.arrayInfo.push({ text: commentText.split(':')[1], isUser: false });
                    var reasons = void 0;
                    reasons = commentText.split(':') && commentText.split(':')[0] && commentText.split(':')[0].split(',') ? commentText.split(':')[0].split(',') : '';
                    if (reasons) {
                        reasons.forEach(function (reason) {
                            _this.restartReasonInfo.push(reason);
                        });
                    }
                    commentText = '';
                }
                else {
                    this.arrayInfo.push({ text: commentText, isUser: false });
                    commentText = '';
                }
            }
            return this.handleUserInfoDetailsTemp(commentText, displayName);
            //  });
        }
        /*    if (!commentText) {
             return 'SUCCESS';
           }
           const positionofAt = commentText.indexOf('@');
           if (positionofAt >= 0) {
             this.arrayInfo.push({ text: commentText.substr(0, (positionofAt)), isUser: false });
             commentText = commentText.substr(positionofAt + 1);
             let isUserThere = false;
             displayName.forEach(data => {
               if (commentText.indexOf(data) === 0) {
                 isUserThere = true;
                 this.arrayInfo.push({ text: commentText.substr(0, data.length), isUser: true });
                 commentText = commentText.substr(data.length);
               }
             });
             if (!isUserThere) {
               this.arrayInfo[this.arrayInfo.length - 1].text = (this.arrayInfo[this.arrayInfo.length - 1].text + '@');
             }
           } else {
             this.arrayInfo.push({ text: commentText, isUser: false });
             commentText = '';
           }
           return this.handleUserInfoDetailsTemp(commentText, displayName); */
    };
    CommentsMessageComponent.ctorParameters = function () { return [
        { type: CommentsUtilService },
        { type: SharingService },
        { type: CommentsService }
    ]; };
    __decorate([
        Input()
    ], CommentsMessageComponent.prototype, "commentData", void 0);
    __decorate([
        Input()
    ], CommentsMessageComponent.prototype, "isReply", void 0);
    __decorate([
        Input()
    ], CommentsMessageComponent.prototype, "userListData", void 0);
    __decorate([
        Output()
    ], CommentsMessageComponent.prototype, "replyToComment", void 0);
    __decorate([
        Output()
    ], CommentsMessageComponent.prototype, "clickingOnReplyMessage", void 0);
    CommentsMessageComponent = __decorate([
        Component({
            selector: 'mpm-comments-message',
            template: "<mat-card class=\"comment-message-item \" *ngIf=\"commentData\" [ngClass]=\"{'comment-parent-item': !isReply}\"\r\n    (mouseover)=\"toggleEvent(true)\" (mouseleave)=\"toggleEvent(false)\" id=\"message_{{commentData.commentId}}\">\r\n    <mat-card-header class=\"comment-header\">\r\n        <!-- <div mat-card-avatar class=\"comment-header-image\">\r\n            <span>{{commentData.userInfo.displayName | uppercase | slice:0:2 }}</span>\r\n        </div> -->\r\n        <mat-card-title class=\"comment-header-title\">\r\n            <span\r\n                class=\"comment-user\">{{currUserId != commentData.userInfo.id ? commentData.userInfo.displayName : 'Me'}}</span>\r\n            <span class=\"comment-date\" *ngIf=\"isReply === true\"\r\n                matTooltip=\"{{commentData.timeStamp | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(commentData.timeStamp)}}</span>\r\n            <span>\r\n                <mat-chip class=\"status-chip\" *ngIf=\"commentData.projectStatus\">{{commentData.projectStatus}}\r\n                </mat-chip>\r\n            </span>\r\n        </mat-card-title>\r\n    </mat-card-header>\r\n\r\n    <mat-card-content class=\"reply-comment-item\" *ngIf=\"commentData.refComment\">\r\n        <p (click)=\"onParentCommentNavigation(commentData.refComment.refCommentId)\">\r\n            {{commentData.refComment.refCommentText}}\r\n        </p>\r\n    </mat-card-content>\r\n    <mat-card-content class=\"original-comment-item\">\r\n        <p class=\"original-comment-item-p\">\r\n            <!-- *ngIf=\"commentTextTemp\" -->\r\n            <span *ngFor=\"let text of arrayInfo\"\r\n                [ngClass]=\"{'user-info-container': text.isUser}\">{{text.isUser?'@'+text.text:text.text}}</span>\r\n        </p>\r\n    </mat-card-content>\r\n\r\n    <mat-card-content class=\"original-comment-item\">\r\n\r\n        <mat-label style=\"font-weight: bold;\" *ngIf=\"restartReasonInfo.length > 0\">Reasons</mat-label>\r\n            <mat-option class=\"matOption\" *ngFor=\"let reason of restartReasonInfo\" value=\"{{reason}}\">\r\n                <span matTooltip=\"{{reason}}\">{{reason}}</span>\r\n            </mat-option>\r\n</mat-card-content>\r\n\r\n    <mat-card-content class=\"original-comment-item\">\r\n\r\n            <mat-label style=\"font-weight: bold;\" *ngIf=\"reasonInfo.length > 0\">Reasons</mat-label>\r\n                <mat-option class=\"matOption\" *ngFor=\"let reason of reasonInfo\" value=\"{{reason}}\">\r\n                    <span matTooltip=\"{{reason}}\">{{reason}}</span>\r\n                </mat-option>\r\n    </mat-card-content>\r\n\r\n    <div *ngIf=\"hover === true\" class=\"comment-message-item-action\">\r\n        <mat-icon (click)=\"onclickToReply(commentData)\"\r\n            matTooltip=\"{{isReply === true ? 'Reply to this message' : 'Remove this message'}}\" class=\"active-icon\">\r\n            {{isReply === true ? 'reply' : 'highlight_off'}}</mat-icon>\r\n    </div>\r\n</mat-card>",
            styles: [".comment-message-item{padding:5px 15px 5px 10px;margin:10px 10px 0 1px;position:relative;max-width:90%}.comment-message-item mat-card-title{margin-bottom:5px;font-size:14px;margin-top:10px}.comment-message-item mat-card-title .comment-date{color:rgba(0,0,0,.54);font-size:12px;padding:8px 10px;font-weight:400;cursor:default}.comment-message-item .active-icon{padding:0;cursor:pointer}.comment-message-item .comment-message-item-action{position:absolute;right:1%;padding:5px;margin:0;z-index:999;top:0;cursor:pointer}.comment-message-item mat-card-content{margin-left:18px;margin-top:5px}.comment-message-item .original-comment-item{margin-bottom:5px;overflow-wrap:anywhere}.comment-message-item .original-comment-item p{white-space:pre-wrap;line-height:20px}.comment-message-item .original-comment-item p span.user-info-container{color:rgba(0,0,0,.87);padding:2px 8px 3px;border-radius:16px;align-items:center;cursor:default;position:relative;font-weight:500}.comment-message-item .comment-header{position:relative}.comment-message-item .comment-header-image{background-size:cover}.comment-message-item .comment-header-image span{position:absolute;left:.7%;top:18%;font-size:20px}.comment-parent-item{position:relative;border-radius:5px 5px 0 0;margin-right:1px!important;background-color:#efefef;max-width:100%!important;padding-right:0}.comment-parent-item .reply-comment-item{background-color:#fff}.comment-user{cursor:default}.reply-comment-item{margin-bottom:5px;background-color:#efefef70;padding:2px 10px;margin-left:18px!important;border-radius:10px;min-height:28px;max-height:28px;display:flex;align-items:center;overflow:hidden;flex-grow:1;width:92%}.reply-comment-item p{font-size:13.2px;overflow:hidden;flex-grow:1;width:200px;max-height:20px;text-overflow:ellipsis;white-space:nowrap}.matOption{height:25px!important;cursor:auto}.status-chip{display:inline-block;padding:6px;border-radius:10px}"]
        })
    ], CommentsMessageComponent);
    return CommentsMessageComponent;
}());
export { CommentsMessageComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbWVzc2FnZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50cy9jb21tZW50cy1tZXNzYWdlL2NvbW1lbnRzLW1lc3NhZ2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUxRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFRL0Q7SUFZRSxrQ0FDUyxrQkFBdUMsRUFDdkMsY0FBOEIsRUFDOUIsZUFBZ0M7UUFGaEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUN2QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBWC9CLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN6QywyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzNELG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFFbEIsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFDaEIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO1FBTXJCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQy9ELENBQUM7SUFFRCwyQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsV0FBVyxFQUFoQixDQUFnQixDQUFDLENBQUMsQ0FBQztJQUN2SSxDQUFDO0lBQ0QsaURBQWMsR0FBZCxVQUFlLElBQUk7UUFDakIsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO1lBQ3pFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUNELDhDQUFXLEdBQVgsVUFBWSxLQUFLO1FBQ2YsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUNELHlEQUFzQixHQUF0QixVQUF1QixVQUFVO1FBQy9CLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDZixPQUFPLEVBQUUsQ0FBQztTQUNYO1FBQ0QsSUFBSSxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsWUFBWSxJQUFJLENBQUMsRUFBRTtZQUMvQyxVQUFVLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbkM7UUFDRCxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUN6QixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDakcsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEVBQUU7WUFDeEIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ25HLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDO2FBQ3JIO2lCQUFNO2dCQUNMLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxhQUFhLENBQUM7YUFDcEg7U0FDRjtRQUNELE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7SUFDRCw0REFBeUIsR0FBekIsVUFBMEIsZUFBZTtRQUN2QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCw0REFBeUIsR0FBekIsVUFBMEIsV0FBbUIsRUFBRSxXQUF1QjtRQUF0RSxpQkFpRkM7UUFoRkMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRTtZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUNyQiw2QkFBNkI7WUFDN0IsMEdBQTBHO1lBQzFHOzs7Ozs7Ozs7OzsrQ0FXbUM7WUFFbkMsSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDaEIsT0FBTyxTQUFTLENBQUM7YUFDbEI7WUFDRCxJQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlDLElBQUksWUFBWSxJQUFJLENBQUMsRUFBRTtnQkFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRixXQUFXLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELElBQUksYUFBVyxHQUFHLEtBQUssQ0FBQztnQkFDeEIsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ3RCLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ25DLGFBQVcsR0FBRyxJQUFJLENBQUM7d0JBQ25CLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDaEYsV0FBVyxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUMvQztnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsYUFBVyxFQUFFO29CQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUN6RzthQUNGO2lCQUFNO2dCQUNMLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUM5RSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO29CQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO29CQUV4RSxJQUFJLE9BQU8sU0FBQSxDQUFDO29CQUNaLE9BQU8sR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xKLElBQUksT0FBTyxFQUFFO3dCQUNYLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNOzRCQUNwQixLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUN0QyxDQUFDLENBQUMsQ0FBQztxQkFDSjtvQkFDRCxXQUFXLEdBQUcsRUFBRSxDQUFDO2lCQUVsQjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7b0JBQzFELFdBQVcsR0FBRyxFQUFFLENBQUM7aUJBQ2xCO2FBQ0Y7WUFDRCxPQUFPLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDaEUsT0FBTztTQUNSO1FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OEVBc0JzRTtJQUN4RSxDQUFDOztnQkEzSDRCLG1CQUFtQjtnQkFDdkIsY0FBYztnQkFDYixlQUFlOztJQWRoQztRQUFSLEtBQUssRUFBRTtpRUFBNEI7SUFDM0I7UUFBUixLQUFLLEVBQUU7NkRBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFO2tFQUFvQztJQUNsQztRQUFULE1BQU0sRUFBRTtvRUFBMEM7SUFDekM7UUFBVCxNQUFNLEVBQUU7NEVBQWtEO0lBTGhELHdCQUF3QjtRQUxwQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLGs3RkFBZ0Q7O1NBRWpELENBQUM7T0FDVyx3QkFBd0IsQ0F5SXBDO0lBQUQsK0JBQUM7Q0FBQSxBQXpJRCxJQXlJQztTQXpJWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1lbnRzVXRpbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9jb21tZW50cy51dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VySW5mb01vZGFsLCBDb21tZW50c01vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9jb21tZW50Lm1vZGFsJztcclxuaW1wb3J0IHsgQ29tbWVudHNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvY29tbWVudHMuc2VydmljZSc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY29tbWVudHMtbWVzc2FnZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnRzLW1lc3NhZ2UuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1lbnRzLW1lc3NhZ2UuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudHNNZXNzYWdlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBjb21tZW50RGF0YTogQ29tbWVudHNNb2RhbDtcclxuICBASW5wdXQoKSBpc1JlcGx5OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIHVzZXJMaXN0RGF0YTogQXJyYXk8VXNlckluZm9Nb2RhbD47XHJcbiAgQE91dHB1dCgpIHJlcGx5VG9Db21tZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGNsaWNraW5nT25SZXBseU1lc3NhZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBjb21tZW50VGV4dFRlbXAgPSBudWxsO1xyXG4gIGN1cnJVc2VySWQgPSBudWxsO1xyXG4gIGhvdmVyOiBib29sZWFuO1xyXG4gIGFycmF5SW5mbyA9IFtdO1xyXG4gIHJlYXNvbkluZm8gPSBbXTtcclxuICByZXN0YXJ0UmVhc29uSW5mbyA9IFtdO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGNvbW1lbnRVdGlsU2VydmljZTogQ29tbWVudHNVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgY29tbWVudHNTZXJ2aWNlOiBDb21tZW50c1NlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMuY3VyclVzZXJJZCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5ob3ZlciA9IGZhbHNlO1xyXG4gICAgdGhpcy5jb21tZW50VGV4dFRlbXAgPSB0aGlzLmhhbmRsZVVzZXJJbmZvRGV0YWlsc1RlbXAodGhpcy5jb21tZW50RGF0YS5jb21tZW50VGV4dCwgdGhpcy51c2VyTGlzdERhdGEubWFwKGRhdGEgPT4gZGF0YS5kaXNwbGF5TmFtZSkpO1xyXG4gIH1cclxuICBvbmNsaWNrVG9SZXBseShkYXRhKSB7XHJcbiAgICBpZiAodGhpcy5yZXBseVRvQ29tbWVudCAmJiB0eXBlb2YgdGhpcy5yZXBseVRvQ29tbWVudC5lbWl0ID09PSAnZnVuY3Rpb24nKSB7XHJcbiAgICAgIHRoaXMucmVwbHlUb0NvbW1lbnQuZW1pdChkYXRhKTtcclxuICAgIH1cclxuICB9XHJcbiAgdG9nZ2xlRXZlbnQoZXZlbnQpIHtcclxuICAgIHRoaXMuaG92ZXIgPSBldmVudDtcclxuICB9XHJcbiAgaGFuZGxlRGF0ZU1hbmlwdWxhdGlvbihkYXRlU3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGlmICghZGF0ZVN0cmluZykge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgICBpZiAoZGF0ZVN0cmluZyAmJiAhKGRhdGVTdHJpbmcgaW5zdGFuY2VvZiBEYXRlKSkge1xyXG4gICAgICBkYXRlU3RyaW5nID0gbmV3IERhdGUoZGF0ZVN0cmluZyk7XHJcbiAgICB9XHJcbiAgICBsZXQgbnVtZXJpY1ZhbHVlRGlmZiA9IDA7XHJcbiAgICBudW1lcmljVmFsdWVEaWZmID0gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnZGF5Jyk7XHJcbiAgICBpZiAobnVtZXJpY1ZhbHVlRGlmZiA8IDEpIHtcclxuICAgICAgbnVtZXJpY1ZhbHVlRGlmZiA9IHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ2hvdXJzJyk7XHJcbiAgICAgIGlmIChudW1lcmljVmFsdWVEaWZmIDwgMSkge1xyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKHRoaXMuY29tbWVudFV0aWxTZXJ2aWNlLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ21pbnV0ZXMnKSkgKyAnIG1pbnMgYWdvLic7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IodGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnaG91cnMnKSkgKyAnIGhvdXJzIGFnby4nO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0RGF0ZUJ5R2l2ZUZvcm1hdChkYXRlU3RyaW5nLCAnTU1NIEQgWSwgaDptbSBhJyk7XHJcbiAgfVxyXG4gIG9uUGFyZW50Q29tbWVudE5hdmlnYXRpb24ocGFyZW50Q29tbWVudElkKSB7XHJcbiAgICB0aGlzLmNsaWNraW5nT25SZXBseU1lc3NhZ2UuZW1pdChwYXJlbnRDb21tZW50SWQpO1xyXG4gIH1cclxuXHJcbiAgaGFuZGxlVXNlckluZm9EZXRhaWxzVGVtcChjb21tZW50VGV4dDogc3RyaW5nLCBkaXNwbGF5TmFtZTogQXJyYXk8YW55Pikge1xyXG4gICAgaWYgKHRoaXMuY29tbWVudERhdGEuY29tbWVudElkKSB7XHJcbiAgICAgIHRoaXMucmVhc29uSW5mbyA9IFtdO1xyXG4gICAgICAvLyByZW1vdmVkIGZvciBjb2RlIGhhcmRlbmluZ1xyXG4gICAgICAvLyAgICB0aGlzLmNvbW1lbnRzU2VydmljZS5nZXRSZWFzb25zQnlDb21tZW50SWQodGhpcy5jb21tZW50RGF0YS5jb21tZW50SWQpLnN1YnNjcmliZShyZWFzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgIC8qICAgIGNvbnNvbGUubG9nKHJlYXNvblJlc3BvbnNlKTtcclxuICAgICAgICAgY29uc3QgcmVhc29ucyA9IHJlYXNvblJlc3BvbnNlICYmIHJlYXNvblJlc3BvbnNlLk1QTV9TdGF0dXNfUmVhc29uID8gcmVhc29uUmVzcG9uc2UuTVBNX1N0YXR1c19SZWFzb24gOiBudWxsO1xyXG4gICAgICAgICBpZiAocmVhc29ucykge1xyXG4gICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHJlYXNvbnMpKSB7XHJcbiAgICAgICAgICAgICByZWFzb25zLmZvckVhY2gocmVhc29uID0+IHtcclxuICAgICAgICAgICAgICAgdGhpcy5yZWFzb25JbmZvLnB1c2gocmVhc29uLk5BTUUpO1xyXG4gICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgIHRoaXMucmVhc29uSW5mby5wdXNoKHJlYXNvbnMuTkFNRSk7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMucmVhc29uSW5mbyk7ICovXHJcblxyXG4gICAgICBpZiAoIWNvbW1lbnRUZXh0KSB7XHJcbiAgICAgICAgcmV0dXJuICdTVUNDRVNTJztcclxuICAgICAgfVxyXG4gICAgICBjb25zdCBwb3NpdGlvbm9mQXQgPSBjb21tZW50VGV4dC5pbmRleE9mKCdAJyk7XHJcbiAgICAgIGlmIChwb3NpdGlvbm9mQXQgPj0gMCkge1xyXG4gICAgICAgIHRoaXMuYXJyYXlJbmZvLnB1c2goeyB0ZXh0OiBjb21tZW50VGV4dC5zdWJzdHIoMCwgKHBvc2l0aW9ub2ZBdCkpLCBpc1VzZXI6IGZhbHNlIH0pO1xyXG4gICAgICAgIGNvbW1lbnRUZXh0ID0gY29tbWVudFRleHQuc3Vic3RyKHBvc2l0aW9ub2ZBdCArIDEpO1xyXG4gICAgICAgIGxldCBpc1VzZXJUaGVyZSA9IGZhbHNlO1xyXG4gICAgICAgIGRpc3BsYXlOYW1lLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICBpZiAoY29tbWVudFRleHQuaW5kZXhPZihkYXRhKSA9PT0gMCkge1xyXG4gICAgICAgICAgICBpc1VzZXJUaGVyZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuYXJyYXlJbmZvLnB1c2goeyB0ZXh0OiBjb21tZW50VGV4dC5zdWJzdHIoMCwgZGF0YS5sZW5ndGgpLCBpc1VzZXI6IHRydWUgfSk7XHJcbiAgICAgICAgICAgIGNvbW1lbnRUZXh0ID0gY29tbWVudFRleHQuc3Vic3RyKGRhdGEubGVuZ3RoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAoIWlzVXNlclRoZXJlKSB7XHJcbiAgICAgICAgICB0aGlzLmFycmF5SW5mb1t0aGlzLmFycmF5SW5mby5sZW5ndGggLSAxXS50ZXh0ID0gKHRoaXMuYXJyYXlJbmZvW3RoaXMuYXJyYXlJbmZvLmxlbmd0aCAtIDFdLnRleHQgKyAnQCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoY29tbWVudFRleHQgJiYgY29tbWVudFRleHQuc3BsaXQoJzonKSAmJiBjb21tZW50VGV4dC5zcGxpdCgnOicpLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgIHRoaXMucmVzdGFydFJlYXNvbkluZm8gPSBbXTtcclxuICAgICAgICAgIHRoaXMuYXJyYXlJbmZvLnB1c2goeyB0ZXh0OiBjb21tZW50VGV4dC5zcGxpdCgnOicpWzFdLCBpc1VzZXI6IGZhbHNlIH0pO1xyXG5cclxuICAgICAgICAgIGxldCByZWFzb25zO1xyXG4gICAgICAgICAgcmVhc29ucyA9IGNvbW1lbnRUZXh0LnNwbGl0KCc6JykgJiYgY29tbWVudFRleHQuc3BsaXQoJzonKVswXSAmJiBjb21tZW50VGV4dC5zcGxpdCgnOicpWzBdLnNwbGl0KCcsJykgPyBjb21tZW50VGV4dC5zcGxpdCgnOicpWzBdLnNwbGl0KCcsJykgOiAnJztcclxuICAgICAgICAgIGlmIChyZWFzb25zKSB7XHJcbiAgICAgICAgICAgIHJlYXNvbnMuZm9yRWFjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMucmVzdGFydFJlYXNvbkluZm8ucHVzaChyZWFzb24pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNvbW1lbnRUZXh0ID0gJyc7XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQsIGlzVXNlcjogZmFsc2UgfSk7XHJcbiAgICAgICAgICBjb21tZW50VGV4dCA9ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcy5oYW5kbGVVc2VySW5mb0RldGFpbHNUZW1wKGNvbW1lbnRUZXh0LCBkaXNwbGF5TmFtZSk7XHJcbiAgICAgIC8vICB9KTtcclxuICAgIH1cclxuICAgIC8qICAgIGlmICghY29tbWVudFRleHQpIHtcclxuICAgICAgICAgcmV0dXJuICdTVUNDRVNTJztcclxuICAgICAgIH1cclxuICAgICAgIGNvbnN0IHBvc2l0aW9ub2ZBdCA9IGNvbW1lbnRUZXh0LmluZGV4T2YoJ0AnKTtcclxuICAgICAgIGlmIChwb3NpdGlvbm9mQXQgPj0gMCkge1xyXG4gICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQuc3Vic3RyKDAsIChwb3NpdGlvbm9mQXQpKSwgaXNVc2VyOiBmYWxzZSB9KTtcclxuICAgICAgICAgY29tbWVudFRleHQgPSBjb21tZW50VGV4dC5zdWJzdHIocG9zaXRpb25vZkF0ICsgMSk7XHJcbiAgICAgICAgIGxldCBpc1VzZXJUaGVyZSA9IGZhbHNlO1xyXG4gICAgICAgICBkaXNwbGF5TmFtZS5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICAgICAgIGlmIChjb21tZW50VGV4dC5pbmRleE9mKGRhdGEpID09PSAwKSB7XHJcbiAgICAgICAgICAgICBpc1VzZXJUaGVyZSA9IHRydWU7XHJcbiAgICAgICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQuc3Vic3RyKDAsIGRhdGEubGVuZ3RoKSwgaXNVc2VyOiB0cnVlIH0pO1xyXG4gICAgICAgICAgICAgY29tbWVudFRleHQgPSBjb21tZW50VGV4dC5zdWJzdHIoZGF0YS5sZW5ndGgpO1xyXG4gICAgICAgICAgIH1cclxuICAgICAgICAgfSk7XHJcbiAgICAgICAgIGlmICghaXNVc2VyVGhlcmUpIHtcclxuICAgICAgICAgICB0aGlzLmFycmF5SW5mb1t0aGlzLmFycmF5SW5mby5sZW5ndGggLSAxXS50ZXh0ID0gKHRoaXMuYXJyYXlJbmZvW3RoaXMuYXJyYXlJbmZvLmxlbmd0aCAtIDFdLnRleHQgKyAnQCcpO1xyXG4gICAgICAgICB9XHJcbiAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICB0aGlzLmFycmF5SW5mby5wdXNoKHsgdGV4dDogY29tbWVudFRleHQsIGlzVXNlcjogZmFsc2UgfSk7XHJcbiAgICAgICAgIGNvbW1lbnRUZXh0ID0gJyc7XHJcbiAgICAgICB9XHJcbiAgICAgICByZXR1cm4gdGhpcy5oYW5kbGVVc2VySW5mb0RldGFpbHNUZW1wKGNvbW1lbnRUZXh0LCBkaXNwbGF5TmFtZSk7ICovXHJcbiAgfVxyXG59XHJcbiJdfQ==