import { __assign, __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var CommentsLayoutComponent = /** @class */ (function () {
    function CommentsLayoutComponent() {
        this.creatingNewComment = new EventEmitter();
        this.loadingMoreComments = new EventEmitter();
        this.parentComment = null;
    }
    CommentsLayoutComponent.prototype.ngOnInit = function () {
    };
    CommentsLayoutComponent.prototype.onReplyToComment = function (parentComment) {
        this.parentComment = __assign({}, parentComment);
    };
    CommentsLayoutComponent.prototype.onCreatingNewComment = function (newComment) {
        this.creatingNewComment.emit(newComment);
    };
    CommentsLayoutComponent.prototype.onLoadingMoreComments = function (pageDetail) {
        this.loadingMoreComments.emit(pageDetail);
    };
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "commentDataList", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "pageDetails", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "isScrollDown", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "isReadonly", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "userListData", void 0);
    __decorate([
        Output()
    ], CommentsLayoutComponent.prototype, "creatingNewComment", void 0);
    __decorate([
        Output()
    ], CommentsLayoutComponent.prototype, "loadingMoreComments", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "enableToComment", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "isExternalUser", void 0);
    __decorate([
        Input()
    ], CommentsLayoutComponent.prototype, "projectId", void 0);
    CommentsLayoutComponent = __decorate([
        Component({
            selector: 'mpm-comments-layout',
            template: "<mpm-comments-message-layout [commentListData]=\"commentDataList\" (replyToComment)=\"onReplyToComment($event)\"\r\n    [pageDetails]=\"pageDetails\" (loadingMoreComments)=\"onLoadingMoreComments($event)\" [isScrollDown]=\"isScrollDown\"\r\n    [userListData]=\"userListData\">\r\n</mpm-comments-message-layout>\r\n<mpm-comments-text [parentComment]=\"parentComment\" (creatingNewComment)=\"onCreatingNewComment($event)\"\r\n    [userListData]=\"userListData\" *ngIf=\"!isReadonly\" [isExternalUser]=\"isExternalUser\" [enableToComment]=\"enableToComment\" [projectId]=\"projectId\">\r\n</mpm-comments-text>",
            styles: [""]
        })
    ], CommentsLayoutComponent);
    return CommentsLayoutComponent;
}());
export { CommentsLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2NvbW1lbnRzL2NvbW1lbnRzLWxheW91dC9jb21tZW50cy1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUS9FO0lBWUU7UUFOVSx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzdDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJeEQsa0JBQWEsR0FBRyxJQUFJLENBQUM7SUFHckIsQ0FBQztJQUVELDBDQUFRLEdBQVI7SUFDQSxDQUFDO0lBQ0Qsa0RBQWdCLEdBQWhCLFVBQWlCLGFBQWE7UUFDNUIsSUFBSSxDQUFDLGFBQWEsZ0JBQVEsYUFBYSxDQUFFLENBQUM7SUFDNUMsQ0FBQztJQUNELHNEQUFvQixHQUFwQixVQUFxQixVQUEyQjtRQUM5QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDRCx1REFBcUIsR0FBckIsVUFBc0IsVUFBVTtRQUM5QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUF6QlE7UUFBUixLQUFLLEVBQUU7b0VBQTZCO0lBQzVCO1FBQVIsS0FBSyxFQUFFO2dFQUFrQjtJQUNqQjtRQUFSLEtBQUssRUFBRTtpRUFBdUI7SUFDdEI7UUFBUixLQUFLLEVBQUU7K0RBQXFCO0lBQ3BCO1FBQVIsS0FBSyxFQUFFO2lFQUFvQztJQUNsQztRQUFULE1BQU0sRUFBRTt1RUFBOEM7SUFDN0M7UUFBVCxNQUFNLEVBQUU7d0VBQStDO0lBQy9DO1FBQVIsS0FBSyxFQUFFO29FQUEwQjtJQUN6QjtRQUFSLEtBQUssRUFBRTttRUFBcUI7SUFDcEI7UUFBUixLQUFLLEVBQUU7OERBQVc7SUFWUix1QkFBdUI7UUFMbkMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQiwybUJBQStDOztTQUVoRCxDQUFDO09BQ1csdUJBQXVCLENBMkJuQztJQUFELDhCQUFDO0NBQUEsQUEzQkQsSUEyQkM7U0EzQlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZXdDb21tZW50TW9kYWwsIFVzZXJJbmZvTW9kYWwgfSBmcm9tICcuLi9vYmplY3RzL2NvbW1lbnQubW9kYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY29tbWVudHMtbGF5b3V0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudHMtbGF5b3V0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb21tZW50cy1sYXlvdXQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudHNMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGNvbW1lbnREYXRhTGlzdDogQXJyYXk8YW55PjtcclxuICBASW5wdXQoKSBwYWdlRGV0YWlsczogYW55O1xyXG4gIEBJbnB1dCgpIGlzU2Nyb2xsRG93bjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBpc1JlYWRvbmx5OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIHVzZXJMaXN0RGF0YTogQXJyYXk8VXNlckluZm9Nb2RhbD47XHJcbiAgQE91dHB1dCgpIGNyZWF0aW5nTmV3Q29tbWVudCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBsb2FkaW5nTW9yZUNvbW1lbnRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgZW5hYmxlVG9Db21tZW50OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGlzRXh0ZXJuYWxVc2VyOiBhbnk7XHJcbiAgQElucHV0KCkgcHJvamVjdElkO1xyXG4gIHBhcmVudENvbW1lbnQgPSBudWxsO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICkge1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgfVxyXG4gIG9uUmVwbHlUb0NvbW1lbnQocGFyZW50Q29tbWVudCk6IHZvaWQge1xyXG4gICAgdGhpcy5wYXJlbnRDb21tZW50ID0geyAuLi5wYXJlbnRDb21tZW50IH07XHJcbiAgfVxyXG4gIG9uQ3JlYXRpbmdOZXdDb21tZW50KG5ld0NvbW1lbnQ6IE5ld0NvbW1lbnRNb2RhbCk6IHZvaWQge1xyXG4gICAgdGhpcy5jcmVhdGluZ05ld0NvbW1lbnQuZW1pdChuZXdDb21tZW50KTtcclxuICB9XHJcbiAgb25Mb2FkaW5nTW9yZUNvbW1lbnRzKHBhZ2VEZXRhaWwpOiB2b2lkIHtcclxuICAgIHRoaXMubG9hZGluZ01vcmVDb21tZW50cy5lbWl0KHBhZ2VEZXRhaWwpO1xyXG4gIH1cclxufVxyXG4iXX0=