import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { ProjectService } from '../../project/shared/services/project.service';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
var CommentsTextComponent = /** @class */ (function () {
    function CommentsTextComponent(formBuilder, sharingService, utilService, projectService, entityAppDefService) {
        this.formBuilder = formBuilder;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.projectService = projectService;
        this.entityAppDefService = entityAppDefService;
        this.creatingNewComment = new EventEmitter();
        this.disableSendIcon = true;
        this.isTagUser = false;
        this.taggedUserList = [];
        this.disableToggleButton = false;
    }
    CommentsTextComponent.prototype.toggleComment = function (event) {
        if (this.isExternalUser) {
            if (event && event.checked) {
                this.commentFormGroup.controls.CommentText.enable();
                this.disableSendIcon = false;
            }
            else {
                this.commentFormGroup.controls.CommentText.disable();
                this.disableSendIcon = true;
            }
        }
    };
    CommentsTextComponent.prototype.initalizeCommentText = function (commentText, isExternal) {
        var _this = this;
        this.disableSendIcon = commentText ? false : true;
        this.commentFormGroup = null;
        this.commentFormGroup = this.formBuilder.group({
            CommentText: new FormControl(commentText, [Validators.maxLength(500)]),
            IsExternal: new FormControl(isExternal)
        });
        if (this.enableToComment == false) {
            this.commentFormGroup.disable();
        }
        if (this.commentFormGroup.controls.CommentText) {
            this.commentFormGroup.controls.CommentText.valueChanges.subscribe(function (data) {
                if (data && typeof data === 'string' && data.trim() === '') {
                    _this.disableSendIcon = true;
                }
                else {
                    _this.disableSendIcon = _this.commentFormGroup.controls.CommentText.invalid ? true : false;
                }
            });
        }
        this.filterUserListData = this.commentFormGroup.get('CommentText').valueChanges
            .pipe(startWith(''), map(function (state) { return _this.filterUserDetails(state && typeof state === 'string' ? state.trim() : ''); }));
    };
    CommentsTextComponent.prototype.tagUserForComment = function (changeEvent) {
        this.isTagUser = true;
    };
    CommentsTextComponent.prototype.createNewComment = function (clickEvent) {
        var newComment = null;
        newComment = {
            ProjectID: null,
            DeliverableID: null,
            CommentText: '',
            RefCommentID: (this.parentComment && this.parentComment.commentId) ? this.parentComment.commentId : null,
            IsExternal: false,
            IsRead: false,
            TagUser: this.taggedUserList.length > 0 ? {
                R_PM_TAG_USER: this.taggedUserList.map(function (data) {
                    return {
                        'Identity-id': {
                            Id: data
                        },
                    };
                })
            } : null
        };
        newComment = Object.assign(newComment, this.commentFormGroup.value);
        if (newComment.CommentText.trim() && newComment.CommentText.trim().length > 0) {
            this.taggedUserList = [];
            this.removeParentComment(null);
            this.initalizeCommentText('', this.isExternalComment);
            this.creatingNewComment.emit(newComment);
        }
        else {
            this.disableSendIcon = true;
        }
    };
    CommentsTextComponent.prototype.removeParentComment = function (removeEvent) {
        this.parentComment = null;
    };
    CommentsTextComponent.prototype.manageFiltervalue = function (filterValue) {
        return null;
    };
    CommentsTextComponent.prototype.filterUserDetails = function (filterValue) {
        var _this = this;
        // this.disableSendIcon = filterValue && filterValue.length > 0 ? false : true;
        if (filterValue && typeof filterValue === 'string' && filterValue.indexOf('@') >= 0) {
            var filterValueTemp_1 = filterValue.substr(filterValue.lastIndexOf('@') + 1).toLowerCase();
            var filteredResult = this.userListData.filter(function (data) {
                return data.displayName.toLowerCase().indexOf(filterValueTemp_1) >= 0 && data.id !== _this.sharingService.getCurrentUserItemID();
            }).map(function (data) {
                return Object.assign(data, { commentText: filterValue });
            });
            return filteredResult;
        } /* else {
          this.disableSendIcon = this.commentFormGroup.value.CommentText ? false : true;
        } */
        /*     if (this.commentFormGroup.invalid) {
              this.disableSendIcon = true;
            } */
        return [];
    };
    CommentsTextComponent.prototype.onKeyUp = function () {
        if (!this.commentFormGroup.invalid || this.commentFormGroup.value.CommentText.length <= 501) {
            this.createNewComment(null);
        }
    };
    CommentsTextComponent.prototype.onOptionSelected = function (eventData) {
        if (eventData && eventData.option && eventData.option.value && this.commentFormGroup) {
            var subjectData = eventData.option.value;
            if (this.taggedUserList.indexOf(eventData.option.value.id) < 0) {
                this.taggedUserList.push(eventData.option.value.id);
            }
            this.initalizeCommentText(subjectData.commentText.substr(0, subjectData.commentText.lastIndexOf('@')) + ('@' + subjectData.displayName + ' '), this.commentFormGroup.value.IsExternal);
        }
    };
    CommentsTextComponent.prototype.displayFunction = function (subjectData) {
        if (subjectData && subjectData.commentText) {
            return subjectData.commentText.substr(0, subjectData.commentText.lastIndexOf('@')) + ('@' + subjectData.displayName + ' ');
        }
        else if (typeof subjectData === 'string') {
            return subjectData;
        }
        return '';
    };
    CommentsTextComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.taggedUserList = [];
        this.commentConfig = this.sharingService.getCommentConfig();
        this.showExternalInternalCommentSwitch = this.utilService.getBooleanValue(this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.SHOW_EXTERNAL_INTERNAL_COMMENT_SWITCH]);
        this.defaultComment = this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.DEFAULT_COMMENT];
        this.externalInternalCommentSwitchLabel = this.utilService.isNullOrEmpty(this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL]) ? '' :
            this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL];
        this.externalCommentLabel = this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.EXTERNAL_COMMENT_LABEL];
        this.internalCommentLabel = this.commentConfig[ApplicationConfigConstants.MPM_COMMENTS_CONFIG.INTERNAL_COMMENT_LABEL];
        if (this.defaultComment === 'EXTERNAL') {
            this.isExternalComment = true;
        }
        else {
            this.isExternalComment = false;
        }
        this.initalizeCommentText('', this.isExternalComment);
        if (this.projectId) {
            this.projectService.getProjectById(this.projectId)
                .subscribe(function (response) {
                _this.statusId = response.R_PO_STATUS["MPM_Status-id"].Id;
                _this.entityAppDefService.getStatusById(_this.statusId)
                    .subscribe(function (projectStatus) {
                    if (projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED || projectStatus.TYPE === ProjectConstant.STATUS_TYPE_ONHOLD) {
                        _this.commentFormGroup.controls.CommentText.disable();
                        _this.disableSendIcon = true;
                        _this.disableToggleButton = true;
                    }
                });
            });
        }
    };
    CommentsTextComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: SharingService },
        { type: UtilService },
        { type: ProjectService },
        { type: EntityAppDefService }
    ]; };
    __decorate([
        Input()
    ], CommentsTextComponent.prototype, "parentComment", void 0);
    __decorate([
        Input()
    ], CommentsTextComponent.prototype, "userListData", void 0);
    __decorate([
        Output()
    ], CommentsTextComponent.prototype, "creatingNewComment", void 0);
    __decorate([
        Input()
    ], CommentsTextComponent.prototype, "enableToComment", void 0);
    __decorate([
        Input()
    ], CommentsTextComponent.prototype, "isExternalUser", void 0);
    __decorate([
        Input()
    ], CommentsTextComponent.prototype, "projectId", void 0);
    __decorate([
        HostListener('window:keyup.enter')
    ], CommentsTextComponent.prototype, "onKeyUp", null);
    CommentsTextComponent = __decorate([
        Component({
            selector: 'mpm-comments-text',
            template: "<form [formGroup]=\"commentFormGroup\">\r\n    <div class=\"flex-row\" *ngIf=\"parentComment\">\r\n        <div class=\"flex-row-item flex-10\">\r\n            <div class=\"parent-comment\">\r\n                <mpm-comments-message [commentData]=\"parentComment\" [isReply]=\"false\"\r\n                    (replyToComment)=\"removeParentComment($event)\" [userListData]=\"userListData\">\r\n                </mpm-comments-message>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item flex-10\">\r\n            <mat-form-field appearance=\"outline\" class=\"text-comment-area\">\r\n                <textarea matInput placeholder=\"Type a Comment\" aria-label=\"label\"\r\n                    (keydown.shift.@)=\"tagUserForComment($event)\" formControlName=\"CommentText\" rows=\"3\"\r\n                    [matAutocomplete]=\"userList\">\r\n                </textarea>\r\n                <mat-autocomplete #userList=\"matAutocomplete\" matAutocompletePosition=\"above\"\r\n                    autoActiveFirstOption=\"true\" (optionSelected)=\"onOptionSelected($event)\"\r\n                    [displayWith]=\"displayFunction\">\r\n                    <mat-option *ngFor=\"let userData of filterUserListData | async\" [value]=\"userData\">\r\n                        <span>{{userData.displayName}}</span>\r\n                    </mat-option>\r\n                </mat-autocomplete>\r\n                <mat-slide-toggle *ngIf=\"showExternalInternalCommentSwitch\" (change)=\"toggleComment($event)\"\r\n                    [disabled]=\"disableToggleButton\"\r\n                    matTooltip=\"{{this.commentFormGroup.value.IsExternal ? externalCommentLabel : internalCommentLabel}}\"\r\n                    class=\"slider-internal active-icon\" color=\"primary\" formControlName=\"IsExternal\">\r\n                </mat-slide-toggle>\r\n                <span class=\"switch-label\">{{externalInternalCommentSwitchLabel}}</span>\r\n                <button color=\"primary\" mat-icon-button matSuffix (click)=\"createNewComment($event)\"\r\n                    class=\"text-comment-area-btn\" [disabled]=\"disableSendIcon\">\r\n                    <mat-icon matTooltip=\"Send comments\">send</mat-icon>\r\n                </button>\r\n                <mat-hint align=\"start\" *ngIf=\"commentFormGroup.get('CommentText').hasError('maxlength')\">Comment\r\n                    can be maximum 500 characters long. </mat-hint>\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n</form>",
            styles: [".flex-10{flex:10}form{margin:10px 0 0}.switch-label{margin:4px;font-size:larger}.parent-comment,.text-comment-area{width:100%}.text-comment-area textarea{resize:none}.text-comment-area .text-comment-area-btn{right:6px;top:-2px}.text-comment-area .text-comment-area-btn mat-icon{font-size:250%}.text-comment-area .mat-form-field-appearance-outline .mat-form-field-wrapper{margin-top:0!important;padding:0}.disableComment:disabled{background:#e4e4e4!important}mat-form-field.mat-form-field.text-comment-area.ng-tns-c85-57.mat-primary.disableComment.mat-form-field-type-mat-input.mat-form-field-appearance-outline.mat-form-field-can-float.ng-valid.mat-form-field-should-float.ng-dirty.ng-touched{background:red!important}"]
        })
    ], CommentsTextComponent);
    return CommentsTextComponent;
}());
export { CommentsTextComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMtdGV4dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9jb21tZW50cy9jb21tZW50cy10ZXh0L2NvbW1lbnRzLXRleHQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RixPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHakYsT0FBTyxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDakcsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFFbkYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFPckY7SUF3QkUsK0JBQ1MsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsbUJBQXdDO1FBSnhDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQTFCdkMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQU12RCxvQkFBZSxHQUFHLElBQUksQ0FBQztRQUV2QixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBVXBCLHdCQUFtQixHQUFZLEtBQUssQ0FBQztJQVFqQyxDQUFDO0lBRUwsNkNBQWEsR0FBYixVQUFjLEtBQUs7UUFDakIsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNwRCxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDckQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7YUFDN0I7U0FDRjtJQUNILENBQUM7SUFFRCxvREFBb0IsR0FBcEIsVUFBcUIsV0FBbUIsRUFBRSxVQUFtQjtRQUE3RCxpQkF3QkM7UUF2QkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2xELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQzdDLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdEUsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLFVBQVUsQ0FBQztTQUN4QyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksS0FBSyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNqQztRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7Z0JBQ3BFLElBQUksSUFBSSxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUMxRCxLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztpQkFDN0I7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2lCQUMxRjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxZQUFZO2FBQzVFLElBQUksQ0FDSCxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQTlFLENBQThFLENBQUMsQ0FDN0YsQ0FBQztJQUNOLENBQUM7SUFFRCxpREFBaUIsR0FBakIsVUFBa0IsV0FBVztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBRUQsZ0RBQWdCLEdBQWhCLFVBQWlCLFVBQVU7UUFDekIsSUFBSSxVQUFVLEdBQW9CLElBQUksQ0FBQztRQUN2QyxVQUFVLEdBQUc7WUFDWCxTQUFTLEVBQUUsSUFBSTtZQUNmLGFBQWEsRUFBRSxJQUFJO1lBQ25CLFdBQVcsRUFBRSxFQUFFO1lBQ2YsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN4RyxVQUFVLEVBQUUsS0FBSztZQUNqQixNQUFNLEVBQUUsS0FBSztZQUNiLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxhQUFhLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUN6QyxPQUFPO3dCQUNMLGFBQWEsRUFBRTs0QkFDYixFQUFFLEVBQUUsSUFBSTt5QkFDVDtxQkFDRixDQUFDO2dCQUNKLENBQUMsQ0FBQzthQUNILENBQUMsQ0FBQyxDQUFDLElBQUk7U0FDVCxDQUFDO1FBQ0YsVUFBVSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRSxJQUFJLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUksVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQzdCO0lBQ0gsQ0FBQztJQUNELG1EQUFtQixHQUFuQixVQUFvQixXQUFXO1FBQzdCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFDRCxpREFBaUIsR0FBakIsVUFBa0IsV0FBbUI7UUFDbkMsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBQ0QsaURBQWlCLEdBQWpCLFVBQWtCLFdBQW1CO1FBQXJDLGlCQWlCQztRQWhCQywrRUFBK0U7UUFDL0UsSUFBSSxXQUFXLElBQUksT0FBTyxXQUFXLEtBQUssUUFBUSxJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25GLElBQU0saUJBQWUsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDM0YsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJO2dCQUNsRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLGlCQUFlLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDaEksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtnQkFDVCxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDM0QsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLGNBQWMsQ0FBQztTQUN2QixDQUFDOztZQUVFO1FBQ0o7O2dCQUVRO1FBQ1IsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRW1DLHVDQUFPLEdBQVA7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtZQUMzRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBQ0QsZ0RBQWdCLEdBQWhCLFVBQWlCLFNBQVM7UUFDeEIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sSUFBSSxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDcEYsSUFBTSxXQUFXLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDM0MsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzlELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3JEO1lBQ0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxXQUFXLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN4TDtJQUNILENBQUM7SUFDRCwrQ0FBZSxHQUFmLFVBQWdCLFdBQTBCO1FBQ3hDLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFDMUMsT0FBTyxXQUFXLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxXQUFXLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQzVIO2FBQU0sSUFBSSxPQUFPLFdBQVcsS0FBSyxRQUFRLEVBQUU7WUFDMUMsT0FBTyxXQUFXLENBQUM7U0FDcEI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFDRCx3Q0FBUSxHQUFSO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVELElBQUksQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLG1CQUFtQixDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUNwTCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDekcsSUFBSSxDQUFDLGtDQUFrQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsbUJBQW1CLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN4TCxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLG1CQUFtQixDQUFDLHNDQUFzQyxDQUFDLENBQUM7UUFDNUcsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsbUJBQW1CLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUN0SCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ3RILElBQUksSUFBSSxDQUFDLGNBQWMsS0FBSyxVQUFVLEVBQUU7WUFDdEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztTQUMvQjthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztTQUNoQztRQUNELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDdEQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7aUJBQy9DLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3pELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQztxQkFDbEQsU0FBUyxDQUFDLFVBQUEsYUFBYTtvQkFDdEIsSUFBSSxhQUFhLENBQUMsV0FBVyxLQUFLLGVBQWUsQ0FBQywyQkFBMkIsSUFBSSxhQUFhLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRTt3QkFDMUksS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ3JELEtBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO3dCQUM1QixLQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO3FCQUNqQztnQkFDSCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDOztnQkExSnFCLFdBQVc7Z0JBQ1IsY0FBYztnQkFDakIsV0FBVztnQkFDUixjQUFjO2dCQUNULG1CQUFtQjs7SUE1QnhDO1FBQVIsS0FBSyxFQUFFO2dFQUE4QjtJQUM3QjtRQUFSLEtBQUssRUFBRTsrREFBb0M7SUFDbEM7UUFBVCxNQUFNLEVBQUU7cUVBQThDO0lBQzlDO1FBQVIsS0FBSyxFQUFFO2tFQUEwQjtJQUN6QjtRQUFSLEtBQUssRUFBRTtpRUFBcUI7SUFDcEI7UUFBUixLQUFLLEVBQUU7NERBQVc7SUEwSGlCO1FBQW5DLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQzt3REFJbEM7SUFwSVUscUJBQXFCO1FBTGpDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsaWhGQUE2Qzs7U0FFOUMsQ0FBQztPQUNXLHFCQUFxQixDQW9MakM7SUFBRCw0QkFBQztDQUFBLEFBcExELElBb0xDO1NBcExZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTmV3Q29tbWVudE1vZGFsLCBDb21tZW50c01vZGFsLCBVc2VySW5mb01vZGFsIH0gZnJvbSAnLi4vb2JqZWN0cy9jb21tZW50Lm1vZGFsJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAsIHN0YXJ0V2l0aCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uLy4uL3Byb2plY3QvcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcblxyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jb21tZW50cy10ZXh0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29tbWVudHMtdGV4dC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29tbWVudHMtdGV4dC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50c1RleHRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIHBhcmVudENvbW1lbnQ6IENvbW1lbnRzTW9kYWw7XHJcbiAgQElucHV0KCkgdXNlckxpc3REYXRhOiBBcnJheTxVc2VySW5mb01vZGFsPjtcclxuICBAT3V0cHV0KCkgY3JlYXRpbmdOZXdDb21tZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgZW5hYmxlVG9Db21tZW50OiBib29sZWFuO1xyXG4gIEBJbnB1dCgpIGlzRXh0ZXJuYWxVc2VyOiBhbnk7XHJcbiAgQElucHV0KCkgcHJvamVjdElkO1xyXG5cclxuICBjb21tZW50Rm9ybUdyb3VwOiBGb3JtR3JvdXA7XHJcbiAgZGlzYWJsZVNlbmRJY29uID0gdHJ1ZTtcclxuICBmaWx0ZXJVc2VyTGlzdERhdGE6IE9ic2VydmFibGU8QXJyYXk8VXNlckluZm9Nb2RhbD4+O1xyXG4gIGlzVGFnVXNlciA9IGZhbHNlO1xyXG4gIHRhZ2dlZFVzZXJMaXN0ID0gW107XHJcblxyXG4gIGNvbW1lbnRDb25maWc6IGFueTtcclxuICBzaG93RXh0ZXJuYWxJbnRlcm5hbENvbW1lbnRTd2l0Y2g6IGJvb2xlYW47XHJcbiAgZGVmYXVsdENvbW1lbnQ6IHN0cmluZztcclxuICBleHRlcm5hbEludGVybmFsQ29tbWVudFN3aXRjaExhYmVsOiBzdHJpbmc7XHJcbiAgZXh0ZXJuYWxDb21tZW50TGFiZWw6IHN0cmluZztcclxuICBpbnRlcm5hbENvbW1lbnRMYWJlbDogc3RyaW5nO1xyXG4gIGlzRXh0ZXJuYWxDb21tZW50OiBib29sZWFuO1xyXG4gIHN0YXR1c0lkOiBhbnk7XHJcbiAgZGlzYWJsZVRvZ2dsZUJ1dHRvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICApIHsgfVxyXG5cclxuICB0b2dnbGVDb21tZW50KGV2ZW50KSB7XHJcbiAgICBpZiAodGhpcy5pc0V4dGVybmFsVXNlcikge1xyXG4gICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuY2hlY2tlZCkge1xyXG4gICAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cC5jb250cm9scy5Db21tZW50VGV4dC5lbmFibGUoKTtcclxuICAgICAgICB0aGlzLmRpc2FibGVTZW5kSWNvbiA9IGZhbHNlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cC5jb250cm9scy5Db21tZW50VGV4dC5kaXNhYmxlKCk7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlU2VuZEljb24gPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBpbml0YWxpemVDb21tZW50VGV4dChjb21tZW50VGV4dDogc3RyaW5nLCBpc0V4dGVybmFsOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVTZW5kSWNvbiA9IGNvbW1lbnRUZXh0ID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgdGhpcy5jb21tZW50Rm9ybUdyb3VwID0gbnVsbDtcclxuICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICBDb21tZW50VGV4dDogbmV3IEZvcm1Db250cm9sKGNvbW1lbnRUZXh0LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoNTAwKV0pLFxyXG4gICAgICBJc0V4dGVybmFsOiBuZXcgRm9ybUNvbnRyb2woaXNFeHRlcm5hbClcclxuICAgIH0pO1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlVG9Db21tZW50ID09IGZhbHNlKSB7XHJcbiAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cC5kaXNhYmxlKCk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jb21tZW50Rm9ybUdyb3VwLmNvbnRyb2xzLkNvbW1lbnRUZXh0KSB7XHJcbiAgICAgIHRoaXMuY29tbWVudEZvcm1Hcm91cC5jb250cm9scy5Db21tZW50VGV4dC52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIGlmIChkYXRhICYmIHR5cGVvZiBkYXRhID09PSAnc3RyaW5nJyAmJiBkYXRhLnRyaW0oKSA9PT0gJycpIHtcclxuICAgICAgICAgIHRoaXMuZGlzYWJsZVNlbmRJY29uID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5kaXNhYmxlU2VuZEljb24gPSB0aGlzLmNvbW1lbnRGb3JtR3JvdXAuY29udHJvbHMuQ29tbWVudFRleHQuaW52YWxpZCA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5maWx0ZXJVc2VyTGlzdERhdGEgPSB0aGlzLmNvbW1lbnRGb3JtR3JvdXAuZ2V0KCdDb21tZW50VGV4dCcpLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAucGlwZShcclxuICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgIG1hcChzdGF0ZSA9PiB0aGlzLmZpbHRlclVzZXJEZXRhaWxzKHN0YXRlICYmIHR5cGVvZiBzdGF0ZSA9PT0gJ3N0cmluZycgPyBzdGF0ZS50cmltKCkgOiAnJykpXHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuICB0YWdVc2VyRm9yQ29tbWVudChjaGFuZ2VFdmVudCk6IHZvaWQge1xyXG4gICAgdGhpcy5pc1RhZ1VzZXIgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlTmV3Q29tbWVudChjbGlja0V2ZW50KTogdm9pZCB7XHJcbiAgICBsZXQgbmV3Q29tbWVudDogTmV3Q29tbWVudE1vZGFsID0gbnVsbDtcclxuICAgIG5ld0NvbW1lbnQgPSB7XHJcbiAgICAgIFByb2plY3RJRDogbnVsbCxcclxuICAgICAgRGVsaXZlcmFibGVJRDogbnVsbCxcclxuICAgICAgQ29tbWVudFRleHQ6ICcnLFxyXG4gICAgICBSZWZDb21tZW50SUQ6ICh0aGlzLnBhcmVudENvbW1lbnQgJiYgdGhpcy5wYXJlbnRDb21tZW50LmNvbW1lbnRJZCkgPyB0aGlzLnBhcmVudENvbW1lbnQuY29tbWVudElkIDogbnVsbCxcclxuICAgICAgSXNFeHRlcm5hbDogZmFsc2UsXHJcbiAgICAgIElzUmVhZDogZmFsc2UsXHJcbiAgICAgIFRhZ1VzZXI6IHRoaXMudGFnZ2VkVXNlckxpc3QubGVuZ3RoID4gMCA/IHtcclxuICAgICAgICBSX1BNX1RBR19VU0VSOiB0aGlzLnRhZ2dlZFVzZXJMaXN0Lm1hcChkYXRhID0+IHtcclxuICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICdJZGVudGl0eS1pZCc6IHtcclxuICAgICAgICAgICAgICBJZDogZGF0YVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KVxyXG4gICAgICB9IDogbnVsbFxyXG4gICAgfTtcclxuICAgIG5ld0NvbW1lbnQgPSBPYmplY3QuYXNzaWduKG5ld0NvbW1lbnQsIHRoaXMuY29tbWVudEZvcm1Hcm91cC52YWx1ZSk7XHJcbiAgICBpZiAobmV3Q29tbWVudC5Db21tZW50VGV4dC50cmltKCkgJiYgbmV3Q29tbWVudC5Db21tZW50VGV4dC50cmltKCkubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLnRhZ2dlZFVzZXJMaXN0ID0gW107XHJcbiAgICAgIHRoaXMucmVtb3ZlUGFyZW50Q29tbWVudChudWxsKTtcclxuICAgICAgdGhpcy5pbml0YWxpemVDb21tZW50VGV4dCgnJywgdGhpcy5pc0V4dGVybmFsQ29tbWVudCk7XHJcbiAgICAgIHRoaXMuY3JlYXRpbmdOZXdDb21tZW50LmVtaXQobmV3Q29tbWVudCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRpc2FibGVTZW5kSWNvbiA9IHRydWU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJlbW92ZVBhcmVudENvbW1lbnQocmVtb3ZlRXZlbnQpOiB2b2lkIHtcclxuICAgIHRoaXMucGFyZW50Q29tbWVudCA9IG51bGw7XHJcbiAgfVxyXG4gIG1hbmFnZUZpbHRlcnZhbHVlKGZpbHRlclZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG4gIGZpbHRlclVzZXJEZXRhaWxzKGZpbHRlclZhbHVlOiBzdHJpbmcpOiBBcnJheTxVc2VySW5mb01vZGFsPiB7XHJcbiAgICAvLyB0aGlzLmRpc2FibGVTZW5kSWNvbiA9IGZpbHRlclZhbHVlICYmIGZpbHRlclZhbHVlLmxlbmd0aCA+IDAgPyBmYWxzZSA6IHRydWU7XHJcbiAgICBpZiAoZmlsdGVyVmFsdWUgJiYgdHlwZW9mIGZpbHRlclZhbHVlID09PSAnc3RyaW5nJyAmJiBmaWx0ZXJWYWx1ZS5pbmRleE9mKCdAJykgPj0gMCkge1xyXG4gICAgICBjb25zdCBmaWx0ZXJWYWx1ZVRlbXAgPSBmaWx0ZXJWYWx1ZS5zdWJzdHIoZmlsdGVyVmFsdWUubGFzdEluZGV4T2YoJ0AnKSArIDEpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgIGNvbnN0IGZpbHRlcmVkUmVzdWx0ID0gdGhpcy51c2VyTGlzdERhdGEuZmlsdGVyKGRhdGEgPT4ge1xyXG4gICAgICAgIHJldHVybiBkYXRhLmRpc3BsYXlOYW1lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihmaWx0ZXJWYWx1ZVRlbXApID49IDAgJiYgZGF0YS5pZCAhPT0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICB9KS5tYXAoZGF0YSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oZGF0YSwgeyBjb21tZW50VGV4dDogZmlsdGVyVmFsdWUgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICByZXR1cm4gZmlsdGVyZWRSZXN1bHQ7XHJcbiAgICB9IC8qIGVsc2Uge1xyXG4gICAgICB0aGlzLmRpc2FibGVTZW5kSWNvbiA9IHRoaXMuY29tbWVudEZvcm1Hcm91cC52YWx1ZS5Db21tZW50VGV4dCA/IGZhbHNlIDogdHJ1ZTtcclxuICAgIH0gKi9cclxuICAgIC8qICAgICBpZiAodGhpcy5jb21tZW50Rm9ybUdyb3VwLmludmFsaWQpIHtcclxuICAgICAgICAgIHRoaXMuZGlzYWJsZVNlbmRJY29uID0gdHJ1ZTtcclxuICAgICAgICB9ICovXHJcbiAgICByZXR1cm4gW107XHJcbiAgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6a2V5dXAuZW50ZXInKSBvbktleVVwKCkge1xyXG4gICAgaWYgKCF0aGlzLmNvbW1lbnRGb3JtR3JvdXAuaW52YWxpZCB8fCB0aGlzLmNvbW1lbnRGb3JtR3JvdXAudmFsdWUuQ29tbWVudFRleHQubGVuZ3RoIDw9IDUwMSkge1xyXG4gICAgICB0aGlzLmNyZWF0ZU5ld0NvbW1lbnQobnVsbCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uT3B0aW9uU2VsZWN0ZWQoZXZlbnREYXRhKTogdm9pZCB7XHJcbiAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5vcHRpb24gJiYgZXZlbnREYXRhLm9wdGlvbi52YWx1ZSAmJiB0aGlzLmNvbW1lbnRGb3JtR3JvdXApIHtcclxuICAgICAgY29uc3Qgc3ViamVjdERhdGEgPSBldmVudERhdGEub3B0aW9uLnZhbHVlO1xyXG4gICAgICBpZiAodGhpcy50YWdnZWRVc2VyTGlzdC5pbmRleE9mKGV2ZW50RGF0YS5vcHRpb24udmFsdWUuaWQpIDwgMCkge1xyXG4gICAgICAgIHRoaXMudGFnZ2VkVXNlckxpc3QucHVzaChldmVudERhdGEub3B0aW9uLnZhbHVlLmlkKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmluaXRhbGl6ZUNvbW1lbnRUZXh0KHN1YmplY3REYXRhLmNvbW1lbnRUZXh0LnN1YnN0cigwLCBzdWJqZWN0RGF0YS5jb21tZW50VGV4dC5sYXN0SW5kZXhPZignQCcpKSArICgnQCcgKyBzdWJqZWN0RGF0YS5kaXNwbGF5TmFtZSArICcgJyksIHRoaXMuY29tbWVudEZvcm1Hcm91cC52YWx1ZS5Jc0V4dGVybmFsKTtcclxuICAgIH1cclxuICB9XHJcbiAgZGlzcGxheUZ1bmN0aW9uKHN1YmplY3REYXRhOiBVc2VySW5mb01vZGFsKTogc3RyaW5nIHtcclxuICAgIGlmIChzdWJqZWN0RGF0YSAmJiBzdWJqZWN0RGF0YS5jb21tZW50VGV4dCkge1xyXG4gICAgICByZXR1cm4gc3ViamVjdERhdGEuY29tbWVudFRleHQuc3Vic3RyKDAsIHN1YmplY3REYXRhLmNvbW1lbnRUZXh0Lmxhc3RJbmRleE9mKCdAJykpICsgKCdAJyArIHN1YmplY3REYXRhLmRpc3BsYXlOYW1lICsgJyAnKTtcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIHN1YmplY3REYXRhID09PSAnc3RyaW5nJykge1xyXG4gICAgICByZXR1cm4gc3ViamVjdERhdGE7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gJyc7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy50YWdnZWRVc2VyTGlzdCA9IFtdO1xyXG4gICAgdGhpcy5jb21tZW50Q29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDb21tZW50Q29uZmlnKCk7XHJcbiAgICB0aGlzLnNob3dFeHRlcm5hbEludGVybmFsQ29tbWVudFN3aXRjaCA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuY29tbWVudENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQ09NTUVOVFNfQ09ORklHLlNIT1dfRVhURVJOQUxfSU5URVJOQUxfQ09NTUVOVF9TV0lUQ0hdKTtcclxuICAgIHRoaXMuZGVmYXVsdENvbW1lbnQgPSB0aGlzLmNvbW1lbnRDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0NPTU1FTlRTX0NPTkZJRy5ERUZBVUxUX0NPTU1FTlRdO1xyXG4gICAgdGhpcy5leHRlcm5hbEludGVybmFsQ29tbWVudFN3aXRjaExhYmVsID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuY29tbWVudENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQ09NTUVOVFNfQ09ORklHLkVYVEVSTkFMX0lOVEVSTkFMX0NPTU1FTlRfU1dJVENIX0xBQkVMXSkgPyAnJyA6XHJcbiAgICAgIHRoaXMuY29tbWVudENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQ09NTUVOVFNfQ09ORklHLkVYVEVSTkFMX0lOVEVSTkFMX0NPTU1FTlRfU1dJVENIX0xBQkVMXTtcclxuICAgIHRoaXMuZXh0ZXJuYWxDb21tZW50TGFiZWwgPSB0aGlzLmNvbW1lbnRDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0NPTU1FTlRTX0NPTkZJRy5FWFRFUk5BTF9DT01NRU5UX0xBQkVMXTtcclxuICAgIHRoaXMuaW50ZXJuYWxDb21tZW50TGFiZWwgPSB0aGlzLmNvbW1lbnRDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0NPTU1FTlRTX0NPTkZJRy5JTlRFUk5BTF9DT01NRU5UX0xBQkVMXTtcclxuICAgIGlmICh0aGlzLmRlZmF1bHRDb21tZW50ID09PSAnRVhURVJOQUwnKSB7XHJcbiAgICAgIHRoaXMuaXNFeHRlcm5hbENvbW1lbnQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pc0V4dGVybmFsQ29tbWVudCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbml0YWxpemVDb21tZW50VGV4dCgnJywgdGhpcy5pc0V4dGVybmFsQ29tbWVudCk7XHJcbiAgICBpZiAodGhpcy5wcm9qZWN0SWQpIHtcclxuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRQcm9qZWN0QnlJZCh0aGlzLnByb2plY3RJZClcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIHRoaXMuc3RhdHVzSWQgPSByZXNwb25zZS5SX1BPX1NUQVRVU1tcIk1QTV9TdGF0dXMtaWRcIl0uSWQ7XHJcbiAgICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZCh0aGlzLnN0YXR1c0lkKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHByb2plY3RTdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChwcm9qZWN0U3RhdHVzLlNUQVRVU19UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEIHx8IHByb2plY3RTdGF0dXMuVFlQRSA9PT0gUHJvamVjdENvbnN0YW50LlNUQVRVU19UWVBFX09OSE9MRCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb21tZW50Rm9ybUdyb3VwLmNvbnRyb2xzLkNvbW1lbnRUZXh0LmRpc2FibGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVNlbmRJY29uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRvZ2dsZUJ1dHRvbiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19