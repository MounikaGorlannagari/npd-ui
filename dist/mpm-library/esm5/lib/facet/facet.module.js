import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacetComponent } from './facet.component';
import { MaterialModule } from '../material.module';
import { FacetButtonComponent } from './facet-button/facet-button.component';
import { ChickletComponent } from './chicklet/chicklet.component';
var FacetModule = /** @class */ (function () {
    function FacetModule() {
    }
    FacetModule = __decorate([
        NgModule({
            declarations: [
                FacetComponent,
                FacetButtonComponent,
                ChickletComponent
            ],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [
                FacetComponent,
                FacetButtonComponent,
                ChickletComponent
            ]
        })
    ], FacetModule);
    return FacetModule;
}());
export { FacetModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvZmFjZXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBa0JsRTtJQUFBO0lBQTJCLENBQUM7SUFBZixXQUFXO1FBaEJ2QixRQUFRLENBQUM7WUFDUixZQUFZLEVBQUU7Z0JBQ1osY0FBYztnQkFDZCxvQkFBb0I7Z0JBQ3BCLGlCQUFpQjthQUNsQjtZQUNELE9BQU8sRUFBRTtnQkFDUCxZQUFZO2dCQUNaLGNBQWM7YUFDZjtZQUNELE9BQU8sRUFBRTtnQkFDUCxjQUFjO2dCQUNkLG9CQUFvQjtnQkFDcEIsaUJBQWlCO2FBQ2xCO1NBQ0YsQ0FBQztPQUNXLFdBQVcsQ0FBSTtJQUFELGtCQUFDO0NBQUEsQUFBNUIsSUFBNEI7U0FBZixXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRmFjZXRDb21wb25lbnQgfSBmcm9tICcuL2ZhY2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgRmFjZXRCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL2ZhY2V0LWJ1dHRvbi9mYWNldC1idXR0b24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2hpY2tsZXRDb21wb25lbnQgfSBmcm9tICcuL2NoaWNrbGV0L2NoaWNrbGV0LmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgRmFjZXRDb21wb25lbnQsXHJcbiAgICBGYWNldEJ1dHRvbkNvbXBvbmVudCxcclxuICAgIENoaWNrbGV0Q29tcG9uZW50XHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgRmFjZXRDb21wb25lbnQsXHJcbiAgICBGYWNldEJ1dHRvbkNvbXBvbmVudCxcclxuICAgIENoaWNrbGV0Q29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmFjZXRNb2R1bGUgeyB9XHJcbiJdfQ==