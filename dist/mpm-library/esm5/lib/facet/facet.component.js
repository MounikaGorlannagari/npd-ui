import { __decorate } from "tslib";
import { Component, ViewChild, Input } from '@angular/core';
import { FacetChangeEventService } from './services/facet-change-event.service';
import { FacetChickletEventService } from './services/facet-chicklet-event.service';
import { SearchChangeEventService } from '../search/services/search-change-event.service';
import { FormatToLocalePipe } from '../shared/pipe/format-to-locale.pipe';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { FacetService } from './services/facet.service';
import { LoaderService } from '../loader/loader.service';
var FacetComponent = /** @class */ (function () {
    function FacetComponent(adapter, formatToLocalePipe, sharingService, facetService, searchChangeEventService, facetChangeEventService, facetChickletEventService, loaderService) {
        this.adapter = adapter;
        this.formatToLocalePipe = formatToLocalePipe;
        this.sharingService = sharingService;
        this.facetService = facetService;
        this.searchChangeEventService = searchChangeEventService;
        this.facetChangeEventService = facetChangeEventService;
        this.facetChickletEventService = facetChickletEventService;
        this.loaderService = loaderService;
        this.chickletData = [];
        this.facetFilters = [];
        this.allMPMFields = [];
        this.selectedFacetFilters = [];
        this.allExpandState = true;
        this.facetGenBehaviour = 'EXCLUDE';
        this.isShowAll = false;
    }
    FacetComponent.prototype.toggleExpandState = function () {
        this.allExpandState = !this.allExpandState;
        if (this.allExpandState) {
            this.facetAccordion.openAll();
        }
        else {
            this.facetAccordion.closeAll();
        }
    };
    FacetComponent.prototype.toggleShowMore = function (facet) {
        facet.isShowAll = !facet.isShowAll;
        if (!facet.isShowAll) {
            facet.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px'; //23
        }
        else {
            facet.height = 'auto';
        }
    };
    FacetComponent.prototype.toggleShowAll = function () {
        this.isShowAll = !this.isShowAll;
    };
    FacetComponent.prototype.getFieldDetails = function (fieldId) {
        return this.allMPMFields.find(function (field) {
            return field.INDEXER_FIELD_ID === fieldId;
        });
    };
    FacetComponent.prototype.publishFacetFieldRestriction = function () {
        var facetRestrictionList = {
            facet_condition: this.selectedFacetFilters
        };
        this.facetChangeEventService.updateFacetSelectFacetCondition(facetRestrictionList);
    };
    FacetComponent.prototype.getValue = function (value) {
        if (!value) {
            return;
        }
        if (typeof value === 'string') {
            return value;
        }
        else if (typeof value === 'object' && value !== null) {
            if (value.value) {
                return value.value;
            }
            else if (value.range_label) {
                return value.range_label;
            }
            else if (value.interval_label) {
                return value.interval_label;
            }
        }
        return;
    };
    FacetComponent.prototype.updateChicklet = function () {
        var _this = this;
        var chicklets = [];
        this.selectedFacetFilters.forEach(function (item) {
            item.values.forEach(function (value) {
                var data = {
                    fieldId: item.field_id,
                    value: _this.getValue(value),
                    originalValue: value
                };
                chicklets.push(data);
            });
        });
        this.facetChickletEventService.updateChickletFilters(chicklets);
    };
    FacetComponent.prototype.clearAllFacets = function () {
        this.loaderService.show();
        this.selectedFacetFilters = Object.assign([]);
        this.updateChicklet();
        this.publishFacetFieldRestriction();
    };
    FacetComponent.prototype.getFactFromSelectedFacets = function (fieldId) {
        return this.selectedFacetFilters.find(function (facetFilterItem) {
            return facetFilterItem.field_id === fieldId;
        });
    };
    FacetComponent.prototype.isValueSelected = function (fieldId, facetValue) {
        if (!fieldId || !facetValue) {
            return;
        }
        var fieldRestrictionItem = this.getFactFromSelectedFacets(fieldId);
        var facetObj = this.facetFilters.find(function (filterItrItem) {
            return filterItrItem.facet_field_request.field_id === fieldId;
        });
        if (!fieldRestrictionItem || !facetObj) {
            return;
        }
        if (typeof facetValue === 'string') {
            if (fieldRestrictionItem.values.indexOf(facetValue) !== -1) {
                return true;
            }
        }
        else if (typeof facetValue === 'object' && facetValue !== null) {
            var value_1 = null;
            if (facetObj.isDate && facetObj.isRange) {
                value_1 = facetValue.range_label || facetValue.date_range.range_label;
            }
            else if (facetObj.isDate && facetObj.isInterval) {
                value_1 = facetValue.interval_label || facetValue.date_interval.interval_label;
            }
            if (facetObj.isNumeric && facetObj.isRange) {
                value_1 = facetValue.range_label || facetValue.numeric_range.range_label;
            }
            else if (facetObj.isNumeric && facetObj.isInterval) {
                value_1 = facetValue.interval_label || facetValue.numeric_interval.interval_label;
            }
            else if (facetValue.value) {
                value_1 = facetValue.value;
            }
            if (!value_1) {
                return;
            }
            return fieldRestrictionItem.values.find(function (restrictionItemValue) {
                if (typeof restrictionItemValue === 'string') {
                    if (fieldRestrictionItem.values.indexOf(value_1) !== -1) {
                        return true;
                    }
                }
                else if (typeof facetValue === 'object' && facetValue !== null) {
                    return (restrictionItemValue.range_label === value_1 ||
                        restrictionItemValue.interval_label === value_1 ||
                        restrictionItemValue.value === value_1);
                }
            });
        }
    };
    FacetComponent.prototype.removeFilterSelection = function (fieldId, facetValue) {
        if (!fieldId || !facetValue) {
            return;
        }
        var fieldSelected = this.getFactFromSelectedFacets(fieldId);
        if (!fieldSelected) {
            return;
        }
        var selectedFacetFiltersCopy = Object.assign([], this.selectedFacetFilters);
        if (fieldSelected.values.length > 1) {
            var filteredValues = fieldSelected.values.filter(function (valueItem) {
                if (!facetValue) {
                    return true;
                }
                if (facetValue === valueItem) {
                    return false;
                }
                if (facetValue && facetValue.range_label && facetValue.range_label === valueItem.range_label) {
                    return false;
                }
                else if (facetValue && facetValue.interval_label && facetValue.interval_label === valueItem.interval_label) {
                    return false;
                }
                if (facetValue.value && valueItem.value && (facetValue.value === valueItem.value)) {
                    return false;
                }
                else {
                    return true;
                }
            });
            fieldSelected.values = filteredValues;
        }
        else {
            this.selectedFacetFilters = selectedFacetFiltersCopy.filter(function (selectionItem) {
                return selectionItem.field_id !== fieldId;
            });
        }
    };
    FacetComponent.prototype.handleFacetSelection = function (fieldId, facetValue) {
        var facetObj = this.facetFilters.find(function (filterItrItem) {
            return filterItrItem.facet_field_request.field_id === fieldId;
        });
        if (this.selectedFacetFilters.length === 0) {
            this.selectedFacetFilters.push({
                type: facetObj.type,
                behavior: this.facetGenBehaviour,
                field_id: fieldId,
                values: [facetValue]
            });
        }
        else {
            var fieldSelected = this.getFactFromSelectedFacets(fieldId);
            if (fieldSelected) {
                var isValueSelected = this.isValueSelected(fieldId, facetValue);
                if (isValueSelected) {
                    this.removeFilterSelection(fieldId, facetValue);
                }
                else {
                    if (facetObj) {
                        if (facetObj.facet_field_request.multi_select) {
                            fieldSelected.values.push(facetValue);
                        }
                        else {
                            fieldSelected.values = [facetValue];
                        }
                    }
                    else {
                        this.removeFilterSelection(fieldId, facetValue);
                    }
                }
            }
            else {
                this.selectedFacetFilters.push({
                    type: facetObj.type,
                    behavior: this.facetGenBehaviour,
                    field_id: fieldId,
                    values: [facetValue]
                });
            }
        }
        this.updateChicklet();
        this.publishFacetFieldRestriction();
    };
    FacetComponent.prototype.filterByCustomFacet = function (facet) {
        var customValue;
        if (facet.isDate) {
            if (facet.isInterval) {
                customValue = {
                    custom_range: true,
                    fixed_end_date: this.toDateString(facet.formGroup.get('end_date').value),
                    fixed_start_date: this.toDateString(facet.formGroup.get('start_date').value),
                    interval_label: this.converToLocalDate(facet.formGroup.get('start_date').value)
                        + ' to ' + this.converToLocalDate(facet.formGroup.get('end_date').value)
                };
            }
            else if (facet.isRange) {
                customValue = {
                    custom_range: true,
                    end_date: this.toDateString(facet.formGroup.get('end_date').value),
                    start_date: this.toDateString(facet.formGroup.get('start_date').value),
                    range_label: this.converToLocalDate(facet.formGroup.get('start_date').value)
                        + ' to ' + this.converToLocalDate(facet.formGroup.get('end_date').value)
                };
            }
        }
        else if (facet.isNumeric) {
            if (facet.isInterval) {
                customValue = {
                    custom_range: true,
                    range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
                    end_value: facet.formGroup.get('end_value').value,
                    start_value: facet.formGroup.get('start_value').value
                };
            }
            else if (facet.isRange) {
                customValue = {
                    custom_range: true,
                    range_label: facet.formGroup.get('start_value').value + ' - ' + facet.formGroup.get('end_value').value,
                    end_value: facet.formGroup.get('end_value').value,
                    start_value: facet.formGroup.get('start_value').value
                };
            }
        }
        if (customValue) {
            this.handleFacetSelection(facet.facet_field_request.field_id, customValue);
        }
    };
    FacetComponent.prototype.filterByFacets = function (filterId, filterValue) {
        this.loaderService.show();
        if (!filterId || !filterValue) {
            return;
        }
        this.handleFacetSelection(filterId, filterValue);
    };
    FacetComponent.prototype.chickletRemoved = function (removedFilters) {
        this.loaderService.show();
        var removedFilter = removedFilters[0];
        this.handleFacetSelection(removedFilter.fieldId, removedFilter.originalValue);
    };
    FacetComponent.prototype.setFilterSelectinAndViewType = function (filter) {
        var _this = this;
        var isFilterSelected = false;
        var fieldRestrictionItem = this.selectedFacetFilters.find(function (facetFilterItem) {
            return facetFilterItem.field_id === filter.facet_field_request.field_id;
        });
        if (fieldRestrictionItem && Array.isArray(fieldRestrictionItem.values) && fieldRestrictionItem.values.length > 0) {
            if (filter.facet_field_request.multi_select) {
                filter.facet_value_list.forEach(function (valueObj) {
                    if (_this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
                        valueObj.isSelected = true;
                        isFilterSelected = true;
                    }
                });
            }
            else {
                filter.facet_value_list.forEach(function (valueObj) {
                    if (_this.isValueSelected(fieldRestrictionItem.field_id, valueObj)) {
                        if (typeof valueObj === 'string') {
                            filter.selectedItem = valueObj;
                        }
                        else if (typeof valueObj === 'object' && valueObj !== null) {
                            var valueToAssign = void 0;
                            var dataType = void 0;
                            if (filter.isDate) {
                                dataType = 'date';
                            }
                            else if (filter.isNumeric) {
                                dataType = 'numeric';
                            }
                            else {
                                valueToAssign = _this.getValue(valueObj);
                            }
                            if (dataType) {
                                if (filter.isInterval) {
                                    valueToAssign = _this.getValue(valueObj[dataType + '_' + 'interval']);
                                }
                                else if (filter.isRange) {
                                    valueToAssign = _this.getValue(valueObj[dataType + '_' + 'range']);
                                }
                            }
                            if (valueToAssign) {
                                filter.selectedItem = valueToAssign;
                            }
                        }
                        isFilterSelected = true;
                    }
                });
            }
            if (this.activeFacetConfiguration.valueOrder === 'COUNT') {
                filter.facet_value_list.sort(function (a, b) { return (a.asset_count < b.asset_count) ? 1 : -1; });
            }
        }
        if (this.activeFacetConfiguration.defaultFacetValueDisplayed &&
            Array.isArray(filter.facet_value_list) && filter.facet_value_list.length >
            this.activeFacetConfiguration.defaultFacetValueDisplayed) {
            if (!isFilterSelected) {
                filter.height = (this.activeFacetConfiguration.defaultFacetValueDisplayed * 33) + 'px';
                filter.isShowAll = false;
            }
            else {
                filter.height = 'auto';
                filter.isShowAll = true;
            }
        }
        else {
            filter.height = 'auto';
            filter.isShowAll = true;
        }
    };
    FacetComponent.prototype.converToLocalDate = function (date) {
        return this.formatToLocalePipe.transform(date);
    };
    FacetComponent.prototype.toDateString = function (date) {
        return date.toISOString();
    };
    FacetComponent.prototype.formRangeLabel = function (filter) {
        var _this = this;
        var isDateType = filter.isDate;
        var isNumeric = filter.isNumeric;
        filter.facet_value_list.map(function (value) {
            if (isDateType) {
                value.date_range.range_label = ((value.date_range.start_date ? _this.converToLocalDate(value.date_range.start_date) : 'before')
                    + ' - ' +
                    (value.date_range.end_date ? _this.converToLocalDate(value.date_range.end_date) : 'after'));
            }
            else if (isNumeric) {
                if (value.numeric_range.start_value && value.numeric_range.end_value) {
                    value.numeric_range.range_label = value.numeric_range.start_value + ' - ' + value.numeric_range.end_value;
                }
                else if (value.numeric_range.start_value) {
                    value.numeric_range.range_label = ' > ' + value.numeric_range.start_value;
                }
                else if (value.numeric_range.end_value) {
                    value.numeric_range.range_label = ' < ' + value.numeric_range.end_value;
                }
            }
        });
    };
    FacetComponent.prototype.initalizeFacets = function () {
        var _this = this;
        var activeFacetId = this.searchChangeEventService.getFacetConfigIdForActiveSearch();
        this.activeFacetConfiguration = this.facetConfigurations.find(function (config) {
            return config.id === activeFacetId;
        });
        if (this.activeFacetConfiguration) {
            this.facetFilters.forEach(function (filter) {
                if (filter && filter.type) {
                    var typeSplitted = filter.type.split('Response');
                    if (typeSplitted.length > 1) {
                        filter.type = typeSplitted[0] + 'Restriction';
                    }
                }
                if (filter.type.indexOf('Date') !== -1) {
                    filter.isDate = true;
                    filter.formGroup = new FormGroup({
                        start_date: new FormControl('', Validators.required),
                        end_date: new FormControl('', Validators.required)
                    });
                }
                if (filter.type.indexOf('Numeric') !== -1) {
                    filter.isNumeric = true;
                    filter.formGroup = new FormGroup({
                        start_value: new FormControl('', Validators.required),
                        end_value: new FormControl('', Validators.required)
                    });
                }
                if (filter.type.indexOf('Range') !== -1) {
                    filter.isRange = true;
                    _this.formRangeLabel(filter);
                }
                if (filter.type.indexOf('Interval') !== -1) {
                    _this.facetService.facetIntervalDisplayValues(filter);
                    filter.isInterval = true;
                }
                if (!filter.facet_field_request.name) {
                    var fieldDetails = _this.getFieldDetails(filter.facet_field_request.field_id);
                    filter.facet_field_request.name = fieldDetails ? fieldDetails.DISPLAY_NAME : '';
                }
                _this.setFilterSelectinAndViewType(filter);
            });
        }
        else {
            console.error('Facet Configration not found');
        }
    };
    FacetComponent.prototype.resetFacet = function () {
        this.facetFilters = Object.assign([]);
        this.selectedFacetFilters = Object.assign([]);
        this.updateChicklet();
    };
    FacetComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        this.allMPMFields = this.sharingService.getAllApplicationFields();
        this.tmpFacetChangeEventSubscription$ = this.facetChangeEventService.onFacetFiltersChange.subscribe(function (facet) {
            _this.facetFilters = facet || Object.assign([]);
            if (Array.isArray(_this.facetConfigurations) && _this.facetConfigurations.length > 0) {
                _this.initalizeFacets();
            }
            else {
                _this.facetService.setFacetConfigurations().subscribe(function (facetConfigurations) {
                    _this.facetConfigurations = facetConfigurations;
                    _this.initalizeFacets();
                }, function (error) {
                    _this.facetFilters = Object.assign([]);
                    _this.selectedFacetFilters = [];
                    _this.initalizeFacets();
                });
            }
        });
        this.tmpFacetResetEventSubscription$ = this.facetChangeEventService.resetFacet$.subscribe(function (data) {
            _this.resetFacet();
        });
    };
    FacetComponent.prototype.ngOnDestroy = function () {
        this.resetFacet();
        // this.facetChangeEventService.updateFacetSelectFacetCondition(null); 
        if (this.tmpFacetChangeEventSubscription$) {
            this.tmpFacetChangeEventSubscription$.unsubscribe();
        }
        if (this.tmpFacetResetEventSubscription$) {
            this.tmpFacetResetEventSubscription$.unsubscribe();
        }
    };
    FacetComponent.ctorParameters = function () { return [
        { type: DateAdapter },
        { type: FormatToLocalePipe },
        { type: SharingService },
        { type: FacetService },
        { type: SearchChangeEventService },
        { type: FacetChangeEventService },
        { type: FacetChickletEventService },
        { type: LoaderService }
    ]; };
    __decorate([
        ViewChild('facetAccordion')
    ], FacetComponent.prototype, "facetAccordion", void 0);
    __decorate([
        Input()
    ], FacetComponent.prototype, "dataCount", void 0);
    FacetComponent = __decorate([
        Component({
            selector: 'mpm-facet',
            template: "<div class=\"facet-wrapper\">\r\n  <div class=\"facet\">\r\n    <div class=\"facet-header\">\r\n      <span class=\"facet-title\">Refine Your Search</span>\r\n      <span class=\"facet-actions\" *ngIf=\"facetFilters.length\">\r\n        <button mat-stroked-button class=\"filter-action-btn\"\r\n          matTooltip=\"{{ allExpandState ? 'Collapse All' : 'Expand All' }}\"\r\n          (click)=\"toggleExpandState()\">{{ allExpandState ? \"Collapse All\" : \"Expand All\" }}</button>\r\n        <button mat-stroked-button *ngIf=\"selectedFacetFilters.length\" class=\"filter-action-btn\"\r\n          matTooltip=\"Clear all applied facets\" (click)=\"clearAllFacets()\">Clear All</button>\r\n      </span>\r\n      <div class=\"facet-chicklet-area\">\r\n        <mpm-chicklet (removed)=\"chickletRemoved($event)\"></mpm-chicklet>\r\n      </div>\r\n    </div>\r\n    <mat-accordion *ngIf=\"dataCount > 0\" class=\"facet-panel-accordion\" #facetAccordion=\"matAccordion\" [multi]=\"true\">\r\n      <ng-container *ngFor=\"let facet of facetFilters; let i = index\">\r\n        <mat-expansion-panel class=\"facet-panel\" [expanded]=\"true\"\r\n          *ngIf=\"((activeFacetConfiguration && (activeFacetConfiguration.defaultFacetDisplayed > i)) || isShowAll ) && facet.facet_value_list.length > 0 \">\r\n          <mat-expansion-panel-header>\r\n            <mat-panel-title>{{facet.facet_field_request.name}}</mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <div class=\"panel-content\">\r\n            <ul class=\"facet-list\" [style.height]=\"facet.height\">\r\n\r\n              <ng-container *ngIf=\"facet.facet_field_request.multi_select\">\r\n\r\n                <ng-container *ngIf=\"facet.isDate\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.date_range && !facetItem.date_range.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_range)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.date_range.range_label}}\">{{facetItem.date_range.range_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet\">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.date_interval && !facetItem.date_interval.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_interval)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.displayName}}\">{{facetItem.displayName}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                  <div>\r\n                    <br />\r\n                    <form [formGroup]=\"facet.formGroup\">\r\n                      <h4>Date Range</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <input formControlName=\"start_date\" matInput [matDatepicker]=\"startDate\"\r\n                          placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                        <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                        <mat-datepicker #startDate></mat-datepicker>\r\n                      </mat-form-field>\r\n                      <h4>To</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <input formControlName=\"end_date\" matInput [matDatepicker]=\"endDate\"\r\n                          placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                        <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                        <mat-datepicker #endDate></mat-datepicker>\r\n                      </mat-form-field>\r\n                      <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                        (click)=\"filterByCustomFacet(facet)\"\r\n                        [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                    </form>\r\n                  </div>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"facet.isNumeric\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.numeric_range && !facetItem.numeric_range.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_range)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.numeric_range.range_label}}\">{{facetItem.numeric_range.range_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                    <li class=\"filter-item\"\r\n                      *ngIf=\"facetItem.numeric_interval && !facetItem.numeric_interval.custom_range\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_interval)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\"\r\n                        matTooltip=\"{{facetItem.numeric_interval.interval_label}}\">{{facetItem.numeric_interval.interval_label}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                  <div>\r\n                    <br />\r\n                    <form [formGroup]=\"facet.formGroup\">\r\n                      <h4>From</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <mat-label>From</mat-label>\r\n                        <input formControlName=\"start_value\" matInput type=\"number\" placeholder=\"From\">\r\n                      </mat-form-field>\r\n                      <h4>To</h4>\r\n                      <mat-form-field appearance=\"outline\">\r\n                        <mat-label>To</mat-label>\r\n                        <input formControlName=\"end_value\" matInput type=\"number\" placeholder=\"From\">\r\n                      </mat-form-field>\r\n                      <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                        (click)=\"filterByCustomFacet(facet)\"\r\n                        [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                    </form>\r\n                  </div>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"(!facet.isDate && !facet.isNumeric)\">\r\n                  <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                    <li class=\"filter-item\" *ngIf=\"facetItem.value\">\r\n                      <span class=\"selection-align\">\r\n                        <mat-checkbox class=\"facet-checkbox\" class=\"select-facet\" color=\"primary\"\r\n                          [(ngModel)]=\"facetItem.isSelected\"\r\n                          (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.value)\">\r\n                        </mat-checkbox>\r\n                      </span>\r\n                      <span class=\"label\" matTooltip=\"{{facetItem.value}}\">{{facetItem.value}}</span>\r\n                      <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">{{facetItem.asset_count}}\r\n                      </mat-chip>\r\n                    </li>\r\n                  </ng-container>\r\n                </ng-container>\r\n\r\n              </ng-container>\r\n\r\n              <ng-container *ngIf=\"!facet.facet_field_request.multi_select\">\r\n\r\n                <mat-radio-group aria-labelledby=\"facet-radio-input\" class=\"facet-radio\"\r\n                  [(ngModel)]=\"facet.selectedItem\">\r\n                  <ng-container *ngIf=\"facet.isDate\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.date_range && !facetItem.date_range.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.date_range.range_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_range)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.date_range.range_label}}\">{{facetItem.date_range.range_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.date_interval && !facetItem.date_interval.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.displayName\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.date_interval)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.displayName}}\">{{facetItem.displayName}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                    <div>\r\n                      <br />\r\n                      <form [formGroup]=\"facet.formGroup\">\r\n                        <h4>Date Range</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <input formControlName=\"start_date\" matInput [matDatepicker]=\"startDate\"\r\n                            placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                          <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                          <mat-datepicker #startDate></mat-datepicker>\r\n                        </mat-form-field>\r\n                        <h4>To</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <input formControlName=\"end_date\" matInput [matDatepicker]=\"endDate\"\r\n                            placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n                          <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                          <mat-datepicker #endDate></mat-datepicker>\r\n                        </mat-form-field>\r\n                        <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                          (click)=\"filterByCustomFacet(facet)\"\r\n                          [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                      </form>\r\n                    </div>\r\n                  </ng-container>\r\n\r\n                  <ng-container *ngIf=\"facet.isNumeric\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.numeric_range && !facetItem.numeric_range.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.numeric_range.range_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_range)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.numeric_range.range_label}}\">{{facetItem.numeric_range.range_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                      <li class=\"filter-item\"\r\n                        *ngIf=\"facetItem.numeric_interval && !facetItem.numeric_interval.custom_range\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\"\r\n                            [value]=\"facetItem.numeric_interval.interval_label\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.numeric_interval)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\"\r\n                          matTooltip=\"{{facetItem.numeric_interval.interval_label}}\">{{facetItem.numeric_interval.interval_label}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                    <div>\r\n                      <br />\r\n                      <form [formGroup]=\"facet.formGroup\">\r\n                        <h4>From</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <mat-label>From</mat-label>\r\n                          <input formControlName=\"start_value\" matInput type=\"number\" placeholder=\"From\">\r\n                        </mat-form-field>\r\n                        <h4>To</h4>\r\n                        <mat-form-field appearance=\"outline\">\r\n                          <mat-label>To</mat-label>\r\n                          <input formControlName=\"end_value\" matInput type=\"number\" placeholder=\"From\">\r\n                        </mat-form-field>\r\n                        <button mat-stroked-button class=\"more-btn\" matTooltip=\"Click to search\"\r\n                          (click)=\"filterByCustomFacet(facet)\"\r\n                          [disabled]=\"!facet.formGroup || facet.formGroup.invalid\">Apply</button>\r\n                      </form>\r\n                    </div>\r\n                  </ng-container>\r\n\r\n                  <ng-container *ngIf=\"(!facet.isDate && !facet.isNumeric)\">\r\n                    <ng-container *ngFor=\"let facetItem of facet.facet_value_list\">\r\n                      <li class=\"filter-item\" *ngIf=\"facetItem.value\">\r\n                        <span class=\"selection-align\">\r\n                          <mat-radio-button class=\"select-facet\" color=\"primary\" [value]=\"facetItem.value\"\r\n                            (change)=\"filterByFacets(facet.facet_field_request.field_id, facetItem.value)\">\r\n                          </mat-radio-button>\r\n                        </span>\r\n                        <span class=\"label\" matTooltip=\"{{facetItem.value}}\">{{facetItem.value}}</span>\r\n                        <mat-chip class=\"facet-count\" matTooltip=\"No. of Items for this facet \">\r\n                          {{facetItem.asset_count}}\r\n                        </mat-chip>\r\n                      </li>\r\n                    </ng-container>\r\n                  </ng-container>\r\n\r\n                </mat-radio-group>\r\n              </ng-container>\r\n            </ul>\r\n            <button class=\"more-btn\" mat-button *ngIf=\"activeFacetConfiguration && \r\n            (facet.facet_value_list.length > activeFacetConfiguration.defaultFacetValueDisplayed)\"\r\n              matTooltip=\"{{facet.isShowAll ? 'Show less filters' : 'Show more filters'}}\"\r\n              (click)=\"toggleShowMore(facet)\">{{facet.isShowAll ? 'Show less' : 'Show more...'}}</button>\r\n          </div>\r\n        </mat-expansion-panel>\r\n      </ng-container>\r\n      <button class=\"more-btn full-width\" mat-stroked-button *ngIf=\"(activeFacetConfiguration && \r\n            (facetFilters.length > activeFacetConfiguration.defaultFacetDisplayed) && !isShowAll)\"\r\n        matTooltip=\"{{isShowAll ? 'Show less facets' : 'Show more facets'}}\"\r\n        (click)=\"toggleShowAll()\">{{isShowAll ? 'Show less facets' : 'Show more facets'}}</button>\r\n    </mat-accordion>\r\n    <p *ngIf=\"(!facetFilters || !facetFilters.length || dataCount ==0)\">No Facets available for the current view !</p>\r\n  </div>\r\n</div>",
            styles: [".facet{width:300px;overflow:auto}.facet-header{padding:10px}.facet-title{font-size:14px}.facet-actions{display:inline-block}.facet-actions button{padding:0 5px;margin:5px;font-size:12px;height:25px;line-height:1px}.facet-chicklet-area{margin-top:10px;max-height:30vh;overflow-y:auto;overflow-x:hidden}.facet-list{overflow:hidden}.facet-list li{list-style-type:none;margin:10px 0;height:22px}.selection-align{margin:0 10px}.facet-panel{border-radius:0!important;box-shadow:none}.facet-wrapper{margin-bottom:80px}.facet-wrapper p{padding:30px}.more-btn{float:right;font-weight:600}.mat-badge-content{top:-3px!important}.mat-badge{padding-right:10px}.facet-count{min-height:15px;float:right;background-color:transparent!important;padding:8px;color:#737373!important}.filter-item .label{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;width:65%;margin-bottom:-5px;display:inline-block}.full-width{width:100%}mat-expansion-panel.facet-panel{border:1px solid #e4e4e4;margin:0!important}mat-expansion-panel.facet-panel mat-expansion-panel-header{height:50px}mat-form-field{width:212px}"]
        })
    ], FacetComponent);
    return FacetComponent;
}());
export { FacetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvZmFjZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBR2hGLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRXBGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFHdkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQU96RDtJQW9CRSx3QkFDUyxPQUF5QixFQUN6QixrQkFBc0MsRUFDdEMsY0FBOEIsRUFDOUIsWUFBMEIsRUFDMUIsd0JBQWtELEVBQ2xELHVCQUFnRCxFQUNoRCx5QkFBb0QsRUFDcEQsYUFBNEI7UUFQNUIsWUFBTyxHQUFQLE9BQU8sQ0FBa0I7UUFDekIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUNsRCw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBQ2hELDhCQUF5QixHQUF6Qix5QkFBeUIsQ0FBMkI7UUFDcEQsa0JBQWEsR0FBYixhQUFhLENBQWU7UUExQnJDLGlCQUFZLEdBQVUsRUFBRSxDQUFDO1FBQ3pCLGlCQUFZLEdBQVksRUFBRSxDQUFDO1FBQzNCLGlCQUFZLEdBQWUsRUFBRSxDQUFDO1FBQzlCLHlCQUFvQixHQUEwQixFQUFFLENBQUM7UUFHakQsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFFdEIsc0JBQWlCLEdBQUcsU0FBUyxDQUFDO1FBQzlCLGNBQVMsR0FBRyxLQUFLLENBQUM7SUFrQmQsQ0FBQztJQUVMLDBDQUFpQixHQUFqQjtRQUNFLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQzNDLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQy9CO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2xCLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO1FBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQ3BCLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsMEJBQTBCLEdBQUcsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsSUFBSTtTQUM1RjthQUFNO1lBQ0wsS0FBSyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdkI7SUFDSCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLE9BQWU7UUFDN0IsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLEtBQUs7WUFDakMsT0FBTyxLQUFLLENBQUMsZ0JBQWdCLEtBQUssT0FBTyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHFEQUE0QixHQUE1QjtRQUNFLElBQU0sb0JBQW9CLEdBQXVCO1lBQy9DLGVBQWUsRUFBRSxJQUFJLENBQUMsb0JBQW9CO1NBQzNDLENBQUM7UUFDRixJQUFJLENBQUMsdUJBQXVCLENBQUMsK0JBQStCLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUNyRixDQUFDO0lBRUQsaUNBQVEsR0FBUixVQUFTLEtBQUs7UUFDWixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1YsT0FBTztTQUNSO1FBQ0QsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLEtBQUssS0FBSyxJQUFJLEVBQUU7WUFDdEQsSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO2dCQUNmLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQzthQUNwQjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUU7Z0JBQzVCLE9BQU8sS0FBSyxDQUFDLFdBQVcsQ0FBQzthQUMxQjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxjQUFjLEVBQUU7Z0JBQy9CLE9BQU8sS0FBSyxDQUFDLGNBQWMsQ0FBQzthQUM3QjtTQUNGO1FBQ0QsT0FBTztJQUNULENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQUEsaUJBYUM7UUFaQyxJQUFNLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDcEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO2dCQUN2QixJQUFNLElBQUksR0FBRztvQkFDWCxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVE7b0JBQ3RCLEtBQUssRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDM0IsYUFBYSxFQUFFLEtBQUs7aUJBQ3JCLENBQUM7Z0JBQ0YsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHlCQUF5QixDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELGtEQUF5QixHQUF6QixVQUEwQixPQUFPO1FBQy9CLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxVQUFBLGVBQWU7WUFDbkQsT0FBTyxlQUFlLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQztRQUM5QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLE9BQU8sRUFBRSxVQUFVO1FBQ2pDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDM0IsT0FBTztTQUNSO1FBQ0QsSUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckUsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxhQUFhO1lBQ25ELE9BQU8sYUFBYSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7UUFDaEUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDdEMsT0FBTztTQUNSO1FBQ0QsSUFBSSxPQUFPLFVBQVUsS0FBSyxRQUFRLEVBQUU7WUFDbEMsSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUMxRCxPQUFPLElBQUksQ0FBQzthQUNiO1NBQ0Y7YUFBTSxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsSUFBSSxVQUFVLEtBQUssSUFBSSxFQUFFO1lBQ2hFLElBQUksT0FBSyxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDdkMsT0FBSyxHQUFHLFVBQVUsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7YUFDckU7aUJBQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pELE9BQUssR0FBRyxVQUFVLENBQUMsY0FBYyxJQUFJLFVBQVUsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDO2FBQzlFO1lBQUMsSUFBSSxRQUFRLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7Z0JBQzVDLE9BQUssR0FBRyxVQUFVLENBQUMsV0FBVyxJQUFJLFVBQVUsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO2FBQ3hFO2lCQUFNLElBQUksUUFBUSxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsVUFBVSxFQUFFO2dCQUNwRCxPQUFLLEdBQUcsVUFBVSxDQUFDLGNBQWMsSUFBSSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDO2FBQ2pGO2lCQUFNLElBQUksVUFBVSxDQUFDLEtBQUssRUFBRTtnQkFDM0IsT0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUM7YUFDMUI7WUFDRCxJQUFJLENBQUMsT0FBSyxFQUFFO2dCQUNWLE9BQU87YUFDUjtZQUNELE9BQU8sb0JBQW9CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFBLG9CQUFvQjtnQkFDMUQsSUFBSSxPQUFPLG9CQUFvQixLQUFLLFFBQVEsRUFBRTtvQkFDNUMsSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUNyRCxPQUFPLElBQUksQ0FBQztxQkFDYjtpQkFDRjtxQkFBTSxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsSUFBSSxVQUFVLEtBQUssSUFBSSxFQUFFO29CQUNoRSxPQUFPLENBQ0wsb0JBQW9CLENBQUMsV0FBVyxLQUFLLE9BQUs7d0JBQzFDLG9CQUFvQixDQUFDLGNBQWMsS0FBSyxPQUFLO3dCQUM3QyxvQkFBb0IsQ0FBQyxLQUFLLEtBQUssT0FBSyxDQUNyQyxDQUFDO2lCQUNIO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRCw4Q0FBcUIsR0FBckIsVUFBc0IsT0FBTyxFQUFFLFVBQVU7UUFDdkMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMzQixPQUFPO1NBQ1I7UUFDRCxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNsQixPQUFPO1NBQ1I7UUFDRCxJQUFNLHdCQUF3QixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzlFLElBQUksYUFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLElBQU0sY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQUEsU0FBUztnQkFDMUQsSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDZixPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFDRCxJQUFJLFVBQVUsS0FBSyxTQUFTLEVBQUU7b0JBQzVCLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2dCQUFDLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxXQUFXLElBQUksVUFBVSxDQUFDLFdBQVcsS0FBSyxTQUFTLENBQUMsV0FBVyxFQUFFO29CQUM5RixPQUFPLEtBQUssQ0FBQztpQkFDZDtxQkFBTSxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsY0FBYyxJQUFJLFVBQVUsQ0FBQyxjQUFjLEtBQUssU0FBUyxDQUFDLGNBQWMsRUFBRTtvQkFDNUcsT0FBTyxLQUFLLENBQUM7aUJBQ2Q7Z0JBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkYsT0FBTyxLQUFLLENBQUM7aUJBQ2Q7cUJBQU07b0JBQ0wsT0FBTyxJQUFJLENBQUM7aUJBQ2I7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILGFBQWEsQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDO1NBQ3ZDO2FBQU07WUFDTCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsd0JBQXdCLENBQUMsTUFBTSxDQUFDLFVBQUEsYUFBYTtnQkFDdkUsT0FBTyxhQUFhLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQztZQUM1QyxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELDZDQUFvQixHQUFwQixVQUFxQixPQUFPLEVBQUUsVUFBVTtRQUN0QyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLGFBQWE7WUFDbkQsT0FBTyxhQUFhLENBQUMsbUJBQW1CLENBQUMsUUFBUSxLQUFLLE9BQU8sQ0FBQztRQUNoRSxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDMUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQztnQkFDN0IsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJO2dCQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtnQkFDaEMsUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLE1BQU0sRUFBRSxDQUFDLFVBQVUsQ0FBQzthQUNyQixDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlELElBQUksYUFBYSxFQUFFO2dCQUNqQixJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDbEUsSUFBSSxlQUFlLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7aUJBQ2pEO3FCQUFNO29CQUNMLElBQUksUUFBUSxFQUFFO3dCQUNaLElBQUksUUFBUSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRTs0QkFDN0MsYUFBYSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQ3ZDOzZCQUFNOzRCQUNMLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDckM7cUJBQ0Y7eUJBQU07d0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztxQkFDakQ7aUJBQ0Y7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDO29CQUM3QixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7b0JBQ25CLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCO29CQUNoQyxRQUFRLEVBQUUsT0FBTztvQkFDakIsTUFBTSxFQUFFLENBQUMsVUFBVSxDQUFDO2lCQUNyQixDQUFDLENBQUM7YUFDSjtTQUNGO1FBQ0QsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFFRCw0Q0FBbUIsR0FBbkIsVUFBb0IsS0FBWTtRQUM5QixJQUFJLFdBQVcsQ0FBQztRQUNoQixJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDaEIsSUFBSSxLQUFLLENBQUMsVUFBVSxFQUFFO2dCQUNwQixXQUFXLEdBQUc7b0JBQ1osWUFBWSxFQUFFLElBQUk7b0JBQ2xCLGNBQWMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDeEUsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQzVFLGNBQWMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDOzBCQUMzRSxNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQkFDM0UsQ0FBQzthQUNIO2lCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDeEIsV0FBVyxHQUFHO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ2xFLFVBQVUsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDdEUsV0FBVyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLLENBQUM7MEJBQ3hFLE1BQU0sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDO2lCQUMzRSxDQUFDO2FBQ0g7U0FDRjthQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtZQUMxQixJQUFJLEtBQUssQ0FBQyxVQUFVLEVBQUU7Z0JBQ3BCLFdBQVcsR0FBRztvQkFDWixZQUFZLEVBQUUsSUFBSTtvQkFDbEIsV0FBVyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSztvQkFDdEcsU0FBUyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUs7b0JBQ2pELFdBQVcsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLO2lCQUN0RCxDQUFDO2FBQ0g7aUJBQU0sSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO2dCQUN4QixXQUFXLEdBQUc7b0JBQ1osWUFBWSxFQUFFLElBQUk7b0JBQ2xCLFdBQVcsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUs7b0JBQ3RHLFNBQVMsRUFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxLQUFLO29CQUNqRCxXQUFXLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSztpQkFDdEQsQ0FBQzthQUNIO1NBQ0Y7UUFDRCxJQUFJLFdBQVcsRUFBRTtZQUNmLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQzVFO0lBQ0gsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxRQUFRLEVBQUUsV0FBVztRQUNsQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDN0IsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixjQUFjO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBTSxhQUFhLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRUQscURBQTRCLEdBQTVCLFVBQTZCLE1BQU07UUFBbkMsaUJBNkRDO1FBNURDLElBQUksZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxVQUFBLGVBQWU7WUFDekUsT0FBTyxlQUFlLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUM7UUFDMUUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLG9CQUFvQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLElBQUksb0JBQW9CLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDaEgsSUFBSSxNQUFNLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFO2dCQUMzQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtvQkFDdEMsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsRUFBRTt3QkFDakUsUUFBUSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQzNCLGdCQUFnQixHQUFHLElBQUksQ0FBQztxQkFDekI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtvQkFDdEMsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsRUFBRTt3QkFDakUsSUFBSSxPQUFPLFFBQVEsS0FBSyxRQUFRLEVBQUU7NEJBQ2hDLE1BQU0sQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO3lCQUNoQzs2QkFBTSxJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsSUFBSSxRQUFRLEtBQUssSUFBSSxFQUFFOzRCQUM1RCxJQUFJLGFBQWEsU0FBQSxDQUFDOzRCQUNsQixJQUFJLFFBQVEsU0FBQSxDQUFDOzRCQUNiLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtnQ0FDakIsUUFBUSxHQUFHLE1BQU0sQ0FBQzs2QkFDbkI7aUNBQU0sSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO2dDQUMzQixRQUFRLEdBQUcsU0FBUyxDQUFDOzZCQUN0QjtpQ0FBTTtnQ0FDTCxhQUFhLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzs2QkFDekM7NEJBQ0QsSUFBSSxRQUFRLEVBQUU7Z0NBQ1osSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFO29DQUNyQixhQUFhLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO2lDQUN0RTtxQ0FBTSxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0NBQ3pCLGFBQWEsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7aUNBQ25FOzZCQUNGOzRCQUNELElBQUksYUFBYSxFQUFFO2dDQUNqQixNQUFNLENBQUMsWUFBWSxHQUFHLGFBQWEsQ0FBQzs2QkFDckM7eUJBQ0Y7d0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3FCQUN6QjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxLQUFLLE9BQU8sRUFBRTtnQkFDeEQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUF4QyxDQUF3QyxDQUFDLENBQUM7YUFDbEY7U0FDRjtRQUNELElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLDBCQUEwQjtZQUMxRCxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNO1lBQ3hFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQywwQkFBMEIsRUFBRTtZQUMxRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3JCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsMEJBQTBCLEdBQUcsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO2dCQUN2RixNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzthQUMxQjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFDdkIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7YUFDekI7U0FDRjthQUFNO1lBQ0wsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDdkIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBRUQsMENBQWlCLEdBQWpCLFVBQWtCLElBQUk7UUFDcEIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxxQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNmLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCx1Q0FBYyxHQUFkLFVBQWUsTUFBTTtRQUFyQixpQkFvQkM7UUFuQkMsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBQSxLQUFLO1lBQy9CLElBQUksVUFBVSxFQUFFO2dCQUNkLEtBQUssQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFHLENBQzdCLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7c0JBQzVGLEtBQUs7b0JBQ1AsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUMxRixDQUFDO2FBQ0g7aUJBQU0sSUFBSSxTQUFTLEVBQUU7Z0JBQ3BCLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUU7b0JBQ3BFLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztpQkFDM0c7cUJBQU0sSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRTtvQkFDMUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO2lCQUMzRTtxQkFBTSxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFO29CQUN4QyxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7aUJBQ3pFO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx3Q0FBZSxHQUFmO1FBQUEsaUJBNENDO1FBM0NDLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1FBQ3RGLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNsRSxPQUFPLE1BQU0sQ0FBQyxFQUFFLEtBQUssYUFBYSxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO2dCQUM5QixJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxFQUFFO29CQUN6QixJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDbkQsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDM0IsTUFBTSxDQUFDLElBQUksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsYUFBYSxDQUFDO3FCQUMvQztpQkFDRjtnQkFDRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUN0QyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDckIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQzt3QkFDL0IsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO3dCQUNwRCxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7cUJBQ25ELENBQUMsQ0FBQztpQkFDSjtnQkFDRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUN6QyxNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDeEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQzt3QkFDL0IsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO3dCQUNyRCxTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7cUJBQ3BELENBQUMsQ0FBQztpQkFDSjtnQkFDRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUN2QyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDdEIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtvQkFDMUMsS0FBSSxDQUFDLFlBQVksQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDckQsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7aUJBQzFCO2dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO29CQUNwQyxJQUFNLFlBQVksR0FBYSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekYsTUFBTSxDQUFDLG1CQUFtQixDQUFDLElBQUksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDakY7Z0JBQ0QsS0FBSSxDQUFDLDRCQUE0QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQztTQUMvQztJQUNILENBQUM7SUFFRCxtQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUFBLGlCQTBCQztRQXpCQyxJQUFJLFNBQVMsQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM1QztRQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBRWxFLElBQUksQ0FBQyxnQ0FBZ0MsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFVBQUEsS0FBSztZQUN2RyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbEYsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUI7b0JBQ3RFLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxtQkFBbUIsQ0FBQztvQkFDL0MsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN6QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDdEMsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztvQkFDL0IsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN6QixDQUFDLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsK0JBQStCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQzVGLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxvQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ2xCLHVFQUF1RTtRQUN2RSxJQUFJLElBQUksQ0FBQyxnQ0FBZ0MsRUFBRTtZQUN6QyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDckQ7UUFDRCxJQUFJLElBQUksQ0FBQywrQkFBK0IsRUFBRTtZQUN4QyxJQUFJLENBQUMsK0JBQStCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEQ7SUFDSCxDQUFDOztnQkFuY2lCLFdBQVc7Z0JBQ0Esa0JBQWtCO2dCQUN0QixjQUFjO2dCQUNoQixZQUFZO2dCQUNBLHdCQUF3QjtnQkFDekIsdUJBQXVCO2dCQUNyQix5QkFBeUI7Z0JBQ3JDLGFBQWE7O0lBWlI7UUFBNUIsU0FBUyxDQUFDLGdCQUFnQixDQUFDOzBEQUE4QjtJQUVqRDtRQUFSLEtBQUssRUFBRTtxREFBVztJQWxCUixjQUFjO1FBTDFCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1lBQ3JCLG16a0JBQXFDOztTQUV0QyxDQUFDO09BQ1csY0FBYyxDQTBkMUI7SUFBRCxxQkFBQztDQUFBLEFBMWRELElBMGRDO1NBMWRZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBWaWV3Q2hpbGQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9mYWNldC1jaGFuZ2UtZXZlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEZhY2V0IH0gZnJvbSAnLi9vYmplY3RzL01QTUZhY2V0JztcclxuaW1wb3J0IHsgTWF0QWNjb3JkaW9uIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZXhwYW5zaW9uJztcclxuaW1wb3J0IHsgRmFjZXRDaGlja2xldEV2ZW50U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZmFjZXQtY2hpY2tsZXQtZXZlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEZhY2V0Q29uZGl0aW9uTGlzdCwgRmFjZXRGaWVsZENvbmRpdGlvbiB9IGZyb20gJy4vb2JqZWN0cy9NUE1GYWNldENvbmRpdGlvbkxpc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UgfSBmcm9tICcuLi9zZWFyY2gvc2VydmljZXMvc2VhcmNoLWNoYW5nZS1ldmVudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybWF0VG9Mb2NhbGVQaXBlIH0gZnJvbSAnLi4vc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS5waXBlJztcclxuaW1wb3J0IHsgVmFsaWRhdG9ycywgRm9ybUdyb3VwLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IE1QTUZhY2V0Q29uZmlnIH0gZnJvbSAnLi9vYmplY3RzL01QTUZhY2V0Q29uZmlnJztcclxuaW1wb3J0IHsgRmFjZXRTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9mYWNldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1mYWNldCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZhY2V0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mYWNldC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGYWNldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgY2hpY2tsZXREYXRhOiBhbnlbXSA9IFtdO1xyXG4gIGZhY2V0RmlsdGVyczogRmFjZXRbXSA9IFtdO1xyXG4gIGFsbE1QTUZpZWxkczogTVBNRmllbGRbXSA9IFtdO1xyXG4gIHNlbGVjdGVkRmFjZXRGaWx0ZXJzOiBGYWNldEZpZWxkQ29uZGl0aW9uW10gPSBbXTtcclxuICBmYWNldENvbmZpZ3VyYXRpb25zOiBNUE1GYWNldENvbmZpZ1tdO1xyXG4gIGFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbjogTVBNRmFjZXRDb25maWc7XHJcbiAgYWxsRXhwYW5kU3RhdGUgPSB0cnVlO1xyXG5cclxuICBmYWNldEdlbkJlaGF2aW91ciA9ICdFWENMVURFJztcclxuICBpc1Nob3dBbGwgPSBmYWxzZTtcclxuXHJcbiAgdG1wRmFjZXRSZXNldEV2ZW50U3Vic2NyaXB0aW9uJDtcclxuICB0bXBGYWNldENoYW5nZUV2ZW50U3Vic2NyaXB0aW9uJDtcclxuXHJcbiAgQFZpZXdDaGlsZCgnZmFjZXRBY2NvcmRpb24nKSBmYWNldEFjY29yZGlvbjogTWF0QWNjb3JkaW9uO1xyXG5cclxuICBASW5wdXQoKSBkYXRhQ291bnQ7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGFkYXB0ZXI6IERhdGVBZGFwdGVyPGFueT4sXHJcbiAgICBwdWJsaWMgZm9ybWF0VG9Mb2NhbGVQaXBlOiBGb3JtYXRUb0xvY2FsZVBpcGUsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZhY2V0U2VydmljZTogRmFjZXRTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNlYXJjaENoYW5nZUV2ZW50U2VydmljZTogU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlOiBGYWNldENoYW5nZUV2ZW50U2VydmljZSxcclxuICAgIHB1YmxpYyBmYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlOiBGYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlLFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICB0b2dnbGVFeHBhbmRTdGF0ZSgpIHtcclxuICAgIHRoaXMuYWxsRXhwYW5kU3RhdGUgPSAhdGhpcy5hbGxFeHBhbmRTdGF0ZTtcclxuICAgIGlmICh0aGlzLmFsbEV4cGFuZFN0YXRlKSB7XHJcbiAgICAgIHRoaXMuZmFjZXRBY2NvcmRpb24ub3BlbkFsbCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5mYWNldEFjY29yZGlvbi5jbG9zZUFsbCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdG9nZ2xlU2hvd01vcmUoZmFjZXQpIHtcclxuICAgIGZhY2V0LmlzU2hvd0FsbCA9ICFmYWNldC5pc1Nob3dBbGw7XHJcbiAgICBpZiAoIWZhY2V0LmlzU2hvd0FsbCkge1xyXG4gICAgICBmYWNldC5oZWlnaHQgPSAodGhpcy5hY3RpdmVGYWNldENvbmZpZ3VyYXRpb24uZGVmYXVsdEZhY2V0VmFsdWVEaXNwbGF5ZWQgKiAzMykgKyAncHgnOyAvLzIzXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBmYWNldC5oZWlnaHQgPSAnYXV0byc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB0b2dnbGVTaG93QWxsKCkge1xyXG4gICAgdGhpcy5pc1Nob3dBbGwgPSAhdGhpcy5pc1Nob3dBbGw7XHJcbiAgfVxyXG5cclxuICBnZXRGaWVsZERldGFpbHMoZmllbGRJZDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy5hbGxNUE1GaWVsZHMuZmluZChmaWVsZCA9PiB7XHJcbiAgICAgIHJldHVybiBmaWVsZC5JTkRFWEVSX0ZJRUxEX0lEID09PSBmaWVsZElkO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaXNoRmFjZXRGaWVsZFJlc3RyaWN0aW9uKCkge1xyXG4gICAgY29uc3QgZmFjZXRSZXN0cmljdGlvbkxpc3Q6IEZhY2V0Q29uZGl0aW9uTGlzdCA9IHtcclxuICAgICAgZmFjZXRfY29uZGl0aW9uOiB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzXHJcbiAgICB9O1xyXG4gICAgdGhpcy5mYWNldENoYW5nZUV2ZW50U2VydmljZS51cGRhdGVGYWNldFNlbGVjdEZhY2V0Q29uZGl0aW9uKGZhY2V0UmVzdHJpY3Rpb25MaXN0KTtcclxuICB9XHJcblxyXG4gIGdldFZhbHVlKHZhbHVlKSB7XHJcbiAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAhPT0gbnVsbCkge1xyXG4gICAgICBpZiAodmFsdWUudmFsdWUpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUudmFsdWU7XHJcbiAgICAgIH0gZWxzZSBpZiAodmFsdWUucmFuZ2VfbGFiZWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUucmFuZ2VfbGFiZWw7XHJcbiAgICAgIH0gZWxzZSBpZiAodmFsdWUuaW50ZXJ2YWxfbGFiZWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuaW50ZXJ2YWxfbGFiZWw7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHVwZGF0ZUNoaWNrbGV0KCkge1xyXG4gICAgY29uc3QgY2hpY2tsZXRzID0gW107XHJcbiAgICB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGl0ZW0udmFsdWVzLmZvckVhY2godmFsdWUgPT4ge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB7XHJcbiAgICAgICAgICBmaWVsZElkOiBpdGVtLmZpZWxkX2lkLFxyXG4gICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWUodmFsdWUpLFxyXG4gICAgICAgICAgb3JpZ2luYWxWYWx1ZTogdmFsdWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNoaWNrbGV0cy5wdXNoKGRhdGEpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5mYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlLnVwZGF0ZUNoaWNrbGV0RmlsdGVycyhjaGlja2xldHMpO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJBbGxGYWNldHMoKSB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycyA9IE9iamVjdC5hc3NpZ24oW10pO1xyXG4gICAgdGhpcy51cGRhdGVDaGlja2xldCgpO1xyXG4gICAgdGhpcy5wdWJsaXNoRmFjZXRGaWVsZFJlc3RyaWN0aW9uKCk7XHJcbiAgfVxyXG5cclxuICBnZXRGYWN0RnJvbVNlbGVjdGVkRmFjZXRzKGZpZWxkSWQpIHtcclxuICAgIHJldHVybiB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzLmZpbmQoZmFjZXRGaWx0ZXJJdGVtID0+IHtcclxuICAgICAgcmV0dXJuIGZhY2V0RmlsdGVySXRlbS5maWVsZF9pZCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaXNWYWx1ZVNlbGVjdGVkKGZpZWxkSWQsIGZhY2V0VmFsdWUpIHtcclxuICAgIGlmICghZmllbGRJZCB8fCAhZmFjZXRWYWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBmaWVsZFJlc3RyaWN0aW9uSXRlbSA9IHRoaXMuZ2V0RmFjdEZyb21TZWxlY3RlZEZhY2V0cyhmaWVsZElkKTtcclxuICAgIGNvbnN0IGZhY2V0T2JqID0gdGhpcy5mYWNldEZpbHRlcnMuZmluZChmaWx0ZXJJdHJJdGVtID0+IHtcclxuICAgICAgcmV0dXJuIGZpbHRlckl0ckl0ZW0uZmFjZXRfZmllbGRfcmVxdWVzdC5maWVsZF9pZCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gICAgaWYgKCFmaWVsZFJlc3RyaWN0aW9uSXRlbSB8fCAhZmFjZXRPYmopIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiBmYWNldFZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICBpZiAoZmllbGRSZXN0cmljdGlvbkl0ZW0udmFsdWVzLmluZGV4T2YoZmFjZXRWYWx1ZSkgIT09IC0xKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAodHlwZW9mIGZhY2V0VmFsdWUgPT09ICdvYmplY3QnICYmIGZhY2V0VmFsdWUgIT09IG51bGwpIHtcclxuICAgICAgbGV0IHZhbHVlID0gbnVsbDtcclxuICAgICAgaWYgKGZhY2V0T2JqLmlzRGF0ZSAmJiBmYWNldE9iai5pc1JhbmdlKSB7XHJcbiAgICAgICAgdmFsdWUgPSBmYWNldFZhbHVlLnJhbmdlX2xhYmVsIHx8IGZhY2V0VmFsdWUuZGF0ZV9yYW5nZS5yYW5nZV9sYWJlbDtcclxuICAgICAgfSBlbHNlIGlmIChmYWNldE9iai5pc0RhdGUgJiYgZmFjZXRPYmouaXNJbnRlcnZhbCkge1xyXG4gICAgICAgIHZhbHVlID0gZmFjZXRWYWx1ZS5pbnRlcnZhbF9sYWJlbCB8fCBmYWNldFZhbHVlLmRhdGVfaW50ZXJ2YWwuaW50ZXJ2YWxfbGFiZWw7XHJcbiAgICAgIH0gaWYgKGZhY2V0T2JqLmlzTnVtZXJpYyAmJiBmYWNldE9iai5pc1JhbmdlKSB7XHJcbiAgICAgICAgdmFsdWUgPSBmYWNldFZhbHVlLnJhbmdlX2xhYmVsIHx8IGZhY2V0VmFsdWUubnVtZXJpY19yYW5nZS5yYW5nZV9sYWJlbDtcclxuICAgICAgfSBlbHNlIGlmIChmYWNldE9iai5pc051bWVyaWMgJiYgZmFjZXRPYmouaXNJbnRlcnZhbCkge1xyXG4gICAgICAgIHZhbHVlID0gZmFjZXRWYWx1ZS5pbnRlcnZhbF9sYWJlbCB8fCBmYWNldFZhbHVlLm51bWVyaWNfaW50ZXJ2YWwuaW50ZXJ2YWxfbGFiZWw7XHJcbiAgICAgIH0gZWxzZSBpZiAoZmFjZXRWYWx1ZS52YWx1ZSkge1xyXG4gICAgICAgIHZhbHVlID0gZmFjZXRWYWx1ZS52YWx1ZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBmaWVsZFJlc3RyaWN0aW9uSXRlbS52YWx1ZXMuZmluZChyZXN0cmljdGlvbkl0ZW1WYWx1ZSA9PiB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiByZXN0cmljdGlvbkl0ZW1WYWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIGlmIChmaWVsZFJlc3RyaWN0aW9uSXRlbS52YWx1ZXMuaW5kZXhPZih2YWx1ZSkgIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGZhY2V0VmFsdWUgPT09ICdvYmplY3QnICYmIGZhY2V0VmFsdWUgIT09IG51bGwpIHtcclxuICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIHJlc3RyaWN0aW9uSXRlbVZhbHVlLnJhbmdlX2xhYmVsID09PSB2YWx1ZSB8fFxyXG4gICAgICAgICAgICByZXN0cmljdGlvbkl0ZW1WYWx1ZS5pbnRlcnZhbF9sYWJlbCA9PT0gdmFsdWUgfHxcclxuICAgICAgICAgICAgcmVzdHJpY3Rpb25JdGVtVmFsdWUudmFsdWUgPT09IHZhbHVlXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW1vdmVGaWx0ZXJTZWxlY3Rpb24oZmllbGRJZCwgZmFjZXRWYWx1ZSkge1xyXG4gICAgaWYgKCFmaWVsZElkIHx8ICFmYWNldFZhbHVlKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNvbnN0IGZpZWxkU2VsZWN0ZWQgPSB0aGlzLmdldEZhY3RGcm9tU2VsZWN0ZWRGYWNldHMoZmllbGRJZCk7XHJcbiAgICBpZiAoIWZpZWxkU2VsZWN0ZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VsZWN0ZWRGYWNldEZpbHRlcnNDb3B5ID0gT2JqZWN0LmFzc2lnbihbXSwgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycyk7XHJcbiAgICBpZiAoZmllbGRTZWxlY3RlZC52YWx1ZXMubGVuZ3RoID4gMSkge1xyXG4gICAgICBjb25zdCBmaWx0ZXJlZFZhbHVlcyA9IGZpZWxkU2VsZWN0ZWQudmFsdWVzLmZpbHRlcih2YWx1ZUl0ZW0gPT4ge1xyXG4gICAgICAgIGlmICghZmFjZXRWYWx1ZSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmYWNldFZhbHVlID09PSB2YWx1ZUl0ZW0pIHtcclxuICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9IGlmIChmYWNldFZhbHVlICYmIGZhY2V0VmFsdWUucmFuZ2VfbGFiZWwgJiYgZmFjZXRWYWx1ZS5yYW5nZV9sYWJlbCA9PT0gdmFsdWVJdGVtLnJhbmdlX2xhYmVsKSB7XHJcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIGlmIChmYWNldFZhbHVlICYmIGZhY2V0VmFsdWUuaW50ZXJ2YWxfbGFiZWwgJiYgZmFjZXRWYWx1ZS5pbnRlcnZhbF9sYWJlbCA9PT0gdmFsdWVJdGVtLmludGVydmFsX2xhYmVsKSB7XHJcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSBpZiAoZmFjZXRWYWx1ZS52YWx1ZSAmJiB2YWx1ZUl0ZW0udmFsdWUgJiYgKGZhY2V0VmFsdWUudmFsdWUgPT09IHZhbHVlSXRlbS52YWx1ZSkpIHtcclxuICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgZmllbGRTZWxlY3RlZC52YWx1ZXMgPSBmaWx0ZXJlZFZhbHVlcztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMgPSBzZWxlY3RlZEZhY2V0RmlsdGVyc0NvcHkuZmlsdGVyKHNlbGVjdGlvbkl0ZW0gPT4ge1xyXG4gICAgICAgIHJldHVybiBzZWxlY3Rpb25JdGVtLmZpZWxkX2lkICE9PSBmaWVsZElkO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGhhbmRsZUZhY2V0U2VsZWN0aW9uKGZpZWxkSWQsIGZhY2V0VmFsdWUpIHtcclxuICAgIGNvbnN0IGZhY2V0T2JqID0gdGhpcy5mYWNldEZpbHRlcnMuZmluZChmaWx0ZXJJdHJJdGVtID0+IHtcclxuICAgICAgcmV0dXJuIGZpbHRlckl0ckl0ZW0uZmFjZXRfZmllbGRfcmVxdWVzdC5maWVsZF9pZCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRGYWNldEZpbHRlcnMucHVzaCh7XHJcbiAgICAgICAgdHlwZTogZmFjZXRPYmoudHlwZSxcclxuICAgICAgICBiZWhhdmlvcjogdGhpcy5mYWNldEdlbkJlaGF2aW91cixcclxuICAgICAgICBmaWVsZF9pZDogZmllbGRJZCxcclxuICAgICAgICB2YWx1ZXM6IFtmYWNldFZhbHVlXVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNvbnN0IGZpZWxkU2VsZWN0ZWQgPSB0aGlzLmdldEZhY3RGcm9tU2VsZWN0ZWRGYWNldHMoZmllbGRJZCk7XHJcbiAgICAgIGlmIChmaWVsZFNlbGVjdGVkKSB7XHJcbiAgICAgICAgY29uc3QgaXNWYWx1ZVNlbGVjdGVkID0gdGhpcy5pc1ZhbHVlU2VsZWN0ZWQoZmllbGRJZCwgZmFjZXRWYWx1ZSk7XHJcbiAgICAgICAgaWYgKGlzVmFsdWVTZWxlY3RlZCkge1xyXG4gICAgICAgICAgdGhpcy5yZW1vdmVGaWx0ZXJTZWxlY3Rpb24oZmllbGRJZCwgZmFjZXRWYWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmIChmYWNldE9iaikge1xyXG4gICAgICAgICAgICBpZiAoZmFjZXRPYmouZmFjZXRfZmllbGRfcmVxdWVzdC5tdWx0aV9zZWxlY3QpIHtcclxuICAgICAgICAgICAgICBmaWVsZFNlbGVjdGVkLnZhbHVlcy5wdXNoKGZhY2V0VmFsdWUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGZpZWxkU2VsZWN0ZWQudmFsdWVzID0gW2ZhY2V0VmFsdWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZUZpbHRlclNlbGVjdGlvbihmaWVsZElkLCBmYWNldFZhbHVlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEZhY2V0RmlsdGVycy5wdXNoKHtcclxuICAgICAgICAgIHR5cGU6IGZhY2V0T2JqLnR5cGUsXHJcbiAgICAgICAgICBiZWhhdmlvcjogdGhpcy5mYWNldEdlbkJlaGF2aW91cixcclxuICAgICAgICAgIGZpZWxkX2lkOiBmaWVsZElkLFxyXG4gICAgICAgICAgdmFsdWVzOiBbZmFjZXRWYWx1ZV1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy51cGRhdGVDaGlja2xldCgpO1xyXG4gICAgdGhpcy5wdWJsaXNoRmFjZXRGaWVsZFJlc3RyaWN0aW9uKCk7XHJcbiAgfVxyXG5cclxuICBmaWx0ZXJCeUN1c3RvbUZhY2V0KGZhY2V0OiBGYWNldCkge1xyXG4gICAgbGV0IGN1c3RvbVZhbHVlO1xyXG4gICAgaWYgKGZhY2V0LmlzRGF0ZSkge1xyXG4gICAgICBpZiAoZmFjZXQuaXNJbnRlcnZhbCkge1xyXG4gICAgICAgIGN1c3RvbVZhbHVlID0ge1xyXG4gICAgICAgICAgY3VzdG9tX3JhbmdlOiB0cnVlLFxyXG4gICAgICAgICAgZml4ZWRfZW5kX2RhdGU6IHRoaXMudG9EYXRlU3RyaW5nKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF9kYXRlJykudmFsdWUpLFxyXG4gICAgICAgICAgZml4ZWRfc3RhcnRfZGF0ZTogdGhpcy50b0RhdGVTdHJpbmcoZmFjZXQuZm9ybUdyb3VwLmdldCgnc3RhcnRfZGF0ZScpLnZhbHVlKSxcclxuICAgICAgICAgIGludGVydmFsX2xhYmVsOiB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X2RhdGUnKS52YWx1ZSlcclxuICAgICAgICAgICAgKyAnIHRvICcgKyB0aGlzLmNvbnZlclRvTG9jYWxEYXRlKGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF9kYXRlJykudmFsdWUpXHJcbiAgICAgICAgfTtcclxuICAgICAgfSBlbHNlIGlmIChmYWNldC5pc1JhbmdlKSB7XHJcbiAgICAgICAgY3VzdG9tVmFsdWUgPSB7XHJcbiAgICAgICAgICBjdXN0b21fcmFuZ2U6IHRydWUsXHJcbiAgICAgICAgICBlbmRfZGF0ZTogdGhpcy50b0RhdGVTdHJpbmcoZmFjZXQuZm9ybUdyb3VwLmdldCgnZW5kX2RhdGUnKS52YWx1ZSksXHJcbiAgICAgICAgICBzdGFydF9kYXRlOiB0aGlzLnRvRGF0ZVN0cmluZyhmYWNldC5mb3JtR3JvdXAuZ2V0KCdzdGFydF9kYXRlJykudmFsdWUpLFxyXG4gICAgICAgICAgcmFuZ2VfbGFiZWw6IHRoaXMuY29udmVyVG9Mb2NhbERhdGUoZmFjZXQuZm9ybUdyb3VwLmdldCgnc3RhcnRfZGF0ZScpLnZhbHVlKVxyXG4gICAgICAgICAgICArICcgdG8gJyArIHRoaXMuY29udmVyVG9Mb2NhbERhdGUoZmFjZXQuZm9ybUdyb3VwLmdldCgnZW5kX2RhdGUnKS52YWx1ZSlcclxuICAgICAgICB9O1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGZhY2V0LmlzTnVtZXJpYykge1xyXG4gICAgICBpZiAoZmFjZXQuaXNJbnRlcnZhbCkge1xyXG4gICAgICAgIGN1c3RvbVZhbHVlID0ge1xyXG4gICAgICAgICAgY3VzdG9tX3JhbmdlOiB0cnVlLFxyXG4gICAgICAgICAgcmFuZ2VfbGFiZWw6IGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X3ZhbHVlJykudmFsdWUgKyAnIC0gJyArIGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF92YWx1ZScpLnZhbHVlLFxyXG4gICAgICAgICAgZW5kX3ZhbHVlOiBmYWNldC5mb3JtR3JvdXAuZ2V0KCdlbmRfdmFsdWUnKS52YWx1ZSxcclxuICAgICAgICAgIHN0YXJ0X3ZhbHVlOiBmYWNldC5mb3JtR3JvdXAuZ2V0KCdzdGFydF92YWx1ZScpLnZhbHVlXHJcbiAgICAgICAgfTtcclxuICAgICAgfSBlbHNlIGlmIChmYWNldC5pc1JhbmdlKSB7XHJcbiAgICAgICAgY3VzdG9tVmFsdWUgPSB7XHJcbiAgICAgICAgICBjdXN0b21fcmFuZ2U6IHRydWUsXHJcbiAgICAgICAgICByYW5nZV9sYWJlbDogZmFjZXQuZm9ybUdyb3VwLmdldCgnc3RhcnRfdmFsdWUnKS52YWx1ZSArICcgLSAnICsgZmFjZXQuZm9ybUdyb3VwLmdldCgnZW5kX3ZhbHVlJykudmFsdWUsXHJcbiAgICAgICAgICBlbmRfdmFsdWU6IGZhY2V0LmZvcm1Hcm91cC5nZXQoJ2VuZF92YWx1ZScpLnZhbHVlLFxyXG4gICAgICAgICAgc3RhcnRfdmFsdWU6IGZhY2V0LmZvcm1Hcm91cC5nZXQoJ3N0YXJ0X3ZhbHVlJykudmFsdWVcclxuICAgICAgICB9O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBpZiAoY3VzdG9tVmFsdWUpIHtcclxuICAgICAgdGhpcy5oYW5kbGVGYWNldFNlbGVjdGlvbihmYWNldC5mYWNldF9maWVsZF9yZXF1ZXN0LmZpZWxkX2lkLCBjdXN0b21WYWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBmaWx0ZXJCeUZhY2V0cyhmaWx0ZXJJZCwgZmlsdGVyVmFsdWUpIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICBpZiAoIWZpbHRlcklkIHx8ICFmaWx0ZXJWYWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB0aGlzLmhhbmRsZUZhY2V0U2VsZWN0aW9uKGZpbHRlcklkLCBmaWx0ZXJWYWx1ZSk7XHJcbiAgfVxyXG5cclxuICBjaGlja2xldFJlbW92ZWQocmVtb3ZlZEZpbHRlcnMpIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICBjb25zdCByZW1vdmVkRmlsdGVyID0gcmVtb3ZlZEZpbHRlcnNbMF07XHJcbiAgICB0aGlzLmhhbmRsZUZhY2V0U2VsZWN0aW9uKHJlbW92ZWRGaWx0ZXIuZmllbGRJZCwgcmVtb3ZlZEZpbHRlci5vcmlnaW5hbFZhbHVlKTtcclxuICB9XHJcblxyXG4gIHNldEZpbHRlclNlbGVjdGluQW5kVmlld1R5cGUoZmlsdGVyKSB7XHJcbiAgICBsZXQgaXNGaWx0ZXJTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgY29uc3QgZmllbGRSZXN0cmljdGlvbkl0ZW0gPSB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzLmZpbmQoZmFjZXRGaWx0ZXJJdGVtID0+IHtcclxuICAgICAgcmV0dXJuIGZhY2V0RmlsdGVySXRlbS5maWVsZF9pZCA9PT0gZmlsdGVyLmZhY2V0X2ZpZWxkX3JlcXVlc3QuZmllbGRfaWQ7XHJcbiAgICB9KTtcclxuICAgIGlmIChmaWVsZFJlc3RyaWN0aW9uSXRlbSAmJiBBcnJheS5pc0FycmF5KGZpZWxkUmVzdHJpY3Rpb25JdGVtLnZhbHVlcykgJiYgZmllbGRSZXN0cmljdGlvbkl0ZW0udmFsdWVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgaWYgKGZpbHRlci5mYWNldF9maWVsZF9yZXF1ZXN0Lm11bHRpX3NlbGVjdCkge1xyXG4gICAgICAgIGZpbHRlci5mYWNldF92YWx1ZV9saXN0LmZvckVhY2godmFsdWVPYmogPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuaXNWYWx1ZVNlbGVjdGVkKGZpZWxkUmVzdHJpY3Rpb25JdGVtLmZpZWxkX2lkLCB2YWx1ZU9iaikpIHtcclxuICAgICAgICAgICAgdmFsdWVPYmouaXNTZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIGlzRmlsdGVyU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGZpbHRlci5mYWNldF92YWx1ZV9saXN0LmZvckVhY2godmFsdWVPYmogPT4ge1xyXG4gICAgICAgICAgaWYgKHRoaXMuaXNWYWx1ZVNlbGVjdGVkKGZpZWxkUmVzdHJpY3Rpb25JdGVtLmZpZWxkX2lkLCB2YWx1ZU9iaikpIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZU9iaiA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICBmaWx0ZXIuc2VsZWN0ZWRJdGVtID0gdmFsdWVPYmo7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlT2JqID09PSAnb2JqZWN0JyAmJiB2YWx1ZU9iaiAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgIGxldCB2YWx1ZVRvQXNzaWduO1xyXG4gICAgICAgICAgICAgIGxldCBkYXRhVHlwZTtcclxuICAgICAgICAgICAgICBpZiAoZmlsdGVyLmlzRGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgZGF0YVR5cGUgPSAnZGF0ZSc7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChmaWx0ZXIuaXNOdW1lcmljKSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhVHlwZSA9ICdudW1lcmljJztcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFsdWVUb0Fzc2lnbiA9IHRoaXMuZ2V0VmFsdWUodmFsdWVPYmopO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpZiAoZGF0YVR5cGUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChmaWx0ZXIuaXNJbnRlcnZhbCkge1xyXG4gICAgICAgICAgICAgICAgICB2YWx1ZVRvQXNzaWduID0gdGhpcy5nZXRWYWx1ZSh2YWx1ZU9ialtkYXRhVHlwZSArICdfJyArICdpbnRlcnZhbCddKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsdGVyLmlzUmFuZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgdmFsdWVUb0Fzc2lnbiA9IHRoaXMuZ2V0VmFsdWUodmFsdWVPYmpbZGF0YVR5cGUgKyAnXycgKyAncmFuZ2UnXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGlmICh2YWx1ZVRvQXNzaWduKSB7XHJcbiAgICAgICAgICAgICAgICBmaWx0ZXIuc2VsZWN0ZWRJdGVtID0gdmFsdWVUb0Fzc2lnbjtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaXNGaWx0ZXJTZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuYWN0aXZlRmFjZXRDb25maWd1cmF0aW9uLnZhbHVlT3JkZXIgPT09ICdDT1VOVCcpIHtcclxuICAgICAgICBmaWx0ZXIuZmFjZXRfdmFsdWVfbGlzdC5zb3J0KChhLCBiKSA9PiAoYS5hc3NldF9jb3VudCA8IGIuYXNzZXRfY291bnQpID8gMSA6IC0xKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuYWN0aXZlRmFjZXRDb25maWd1cmF0aW9uLmRlZmF1bHRGYWNldFZhbHVlRGlzcGxheWVkICYmXHJcbiAgICAgIEFycmF5LmlzQXJyYXkoZmlsdGVyLmZhY2V0X3ZhbHVlX2xpc3QpICYmIGZpbHRlci5mYWNldF92YWx1ZV9saXN0Lmxlbmd0aCA+XHJcbiAgICAgIHRoaXMuYWN0aXZlRmFjZXRDb25maWd1cmF0aW9uLmRlZmF1bHRGYWNldFZhbHVlRGlzcGxheWVkKSB7XHJcbiAgICAgIGlmICghaXNGaWx0ZXJTZWxlY3RlZCkge1xyXG4gICAgICAgIGZpbHRlci5oZWlnaHQgPSAodGhpcy5hY3RpdmVGYWNldENvbmZpZ3VyYXRpb24uZGVmYXVsdEZhY2V0VmFsdWVEaXNwbGF5ZWQgKiAzMykgKyAncHgnO1xyXG4gICAgICAgIGZpbHRlci5pc1Nob3dBbGwgPSBmYWxzZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBmaWx0ZXIuaGVpZ2h0ID0gJ2F1dG8nO1xyXG4gICAgICAgIGZpbHRlci5pc1Nob3dBbGwgPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBmaWx0ZXIuaGVpZ2h0ID0gJ2F1dG8nO1xyXG4gICAgICBmaWx0ZXIuaXNTaG93QWxsID0gdHJ1ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbnZlclRvTG9jYWxEYXRlKGRhdGUpIHtcclxuICAgIHJldHVybiB0aGlzLmZvcm1hdFRvTG9jYWxlUGlwZS50cmFuc2Zvcm0oZGF0ZSk7XHJcbiAgfVxyXG5cclxuICB0b0RhdGVTdHJpbmcoZGF0ZSkge1xyXG4gICAgcmV0dXJuIGRhdGUudG9JU09TdHJpbmcoKTtcclxuICB9XHJcblxyXG4gIGZvcm1SYW5nZUxhYmVsKGZpbHRlcikge1xyXG4gICAgY29uc3QgaXNEYXRlVHlwZSA9IGZpbHRlci5pc0RhdGU7XHJcbiAgICBjb25zdCBpc051bWVyaWMgPSBmaWx0ZXIuaXNOdW1lcmljO1xyXG4gICAgZmlsdGVyLmZhY2V0X3ZhbHVlX2xpc3QubWFwKHZhbHVlID0+IHtcclxuICAgICAgaWYgKGlzRGF0ZVR5cGUpIHtcclxuICAgICAgICB2YWx1ZS5kYXRlX3JhbmdlLnJhbmdlX2xhYmVsID0gKFxyXG4gICAgICAgICAgKHZhbHVlLmRhdGVfcmFuZ2Uuc3RhcnRfZGF0ZSA/IHRoaXMuY29udmVyVG9Mb2NhbERhdGUodmFsdWUuZGF0ZV9yYW5nZS5zdGFydF9kYXRlKSA6ICdiZWZvcmUnKVxyXG4gICAgICAgICAgKyAnIC0gJyArXHJcbiAgICAgICAgICAodmFsdWUuZGF0ZV9yYW5nZS5lbmRfZGF0ZSA/IHRoaXMuY29udmVyVG9Mb2NhbERhdGUodmFsdWUuZGF0ZV9yYW5nZS5lbmRfZGF0ZSkgOiAnYWZ0ZXInKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXNOdW1lcmljKSB7XHJcbiAgICAgICAgaWYgKHZhbHVlLm51bWVyaWNfcmFuZ2Uuc3RhcnRfdmFsdWUgJiYgdmFsdWUubnVtZXJpY19yYW5nZS5lbmRfdmFsdWUpIHtcclxuICAgICAgICAgIHZhbHVlLm51bWVyaWNfcmFuZ2UucmFuZ2VfbGFiZWwgPSB2YWx1ZS5udW1lcmljX3JhbmdlLnN0YXJ0X3ZhbHVlICsgJyAtICcgKyB2YWx1ZS5udW1lcmljX3JhbmdlLmVuZF92YWx1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHZhbHVlLm51bWVyaWNfcmFuZ2Uuc3RhcnRfdmFsdWUpIHtcclxuICAgICAgICAgIHZhbHVlLm51bWVyaWNfcmFuZ2UucmFuZ2VfbGFiZWwgPSAnID4gJyArIHZhbHVlLm51bWVyaWNfcmFuZ2Uuc3RhcnRfdmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5udW1lcmljX3JhbmdlLmVuZF92YWx1ZSkge1xyXG4gICAgICAgICAgdmFsdWUubnVtZXJpY19yYW5nZS5yYW5nZV9sYWJlbCA9ICcgPCAnICsgdmFsdWUubnVtZXJpY19yYW5nZS5lbmRfdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGluaXRhbGl6ZUZhY2V0cygpIHtcclxuICAgIGNvbnN0IGFjdGl2ZUZhY2V0SWQgPSB0aGlzLnNlYXJjaENoYW5nZUV2ZW50U2VydmljZS5nZXRGYWNldENvbmZpZ0lkRm9yQWN0aXZlU2VhcmNoKCk7XHJcbiAgICB0aGlzLmFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbiA9IHRoaXMuZmFjZXRDb25maWd1cmF0aW9ucy5maW5kKGNvbmZpZyA9PiB7XHJcbiAgICAgIHJldHVybiBjb25maWcuaWQgPT09IGFjdGl2ZUZhY2V0SWQ7XHJcbiAgICB9KTtcclxuICAgIGlmICh0aGlzLmFjdGl2ZUZhY2V0Q29uZmlndXJhdGlvbikge1xyXG4gICAgICB0aGlzLmZhY2V0RmlsdGVycy5mb3JFYWNoKGZpbHRlciA9PiB7XHJcbiAgICAgICAgaWYgKGZpbHRlciAmJiBmaWx0ZXIudHlwZSkge1xyXG4gICAgICAgICAgY29uc3QgdHlwZVNwbGl0dGVkID0gZmlsdGVyLnR5cGUuc3BsaXQoJ1Jlc3BvbnNlJyk7XHJcbiAgICAgICAgICBpZiAodHlwZVNwbGl0dGVkLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgZmlsdGVyLnR5cGUgPSB0eXBlU3BsaXR0ZWRbMF0gKyAnUmVzdHJpY3Rpb24nO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmlsdGVyLnR5cGUuaW5kZXhPZignRGF0ZScpICE9PSAtMSkge1xyXG4gICAgICAgICAgZmlsdGVyLmlzRGF0ZSA9IHRydWU7XHJcbiAgICAgICAgICBmaWx0ZXIuZm9ybUdyb3VwID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgICAgICAgIHN0YXJ0X2RhdGU6IG5ldyBGb3JtQ29udHJvbCgnJywgVmFsaWRhdG9ycy5yZXF1aXJlZCksXHJcbiAgICAgICAgICAgIGVuZF9kYXRlOiBuZXcgRm9ybUNvbnRyb2woJycsIFZhbGlkYXRvcnMucmVxdWlyZWQpXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpbHRlci50eXBlLmluZGV4T2YoJ051bWVyaWMnKSAhPT0gLTEpIHtcclxuICAgICAgICAgIGZpbHRlci5pc051bWVyaWMgPSB0cnVlO1xyXG4gICAgICAgICAgZmlsdGVyLmZvcm1Hcm91cCA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICBzdGFydF92YWx1ZTogbmV3IEZvcm1Db250cm9sKCcnLCBWYWxpZGF0b3JzLnJlcXVpcmVkKSxcclxuICAgICAgICAgICAgZW5kX3ZhbHVlOiBuZXcgRm9ybUNvbnRyb2woJycsIFZhbGlkYXRvcnMucmVxdWlyZWQpXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGZpbHRlci50eXBlLmluZGV4T2YoJ1JhbmdlJykgIT09IC0xKSB7XHJcbiAgICAgICAgICBmaWx0ZXIuaXNSYW5nZSA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLmZvcm1SYW5nZUxhYmVsKGZpbHRlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmaWx0ZXIudHlwZS5pbmRleE9mKCdJbnRlcnZhbCcpICE9PSAtMSkge1xyXG4gICAgICAgICAgdGhpcy5mYWNldFNlcnZpY2UuZmFjZXRJbnRlcnZhbERpc3BsYXlWYWx1ZXMoZmlsdGVyKTtcclxuICAgICAgICAgIGZpbHRlci5pc0ludGVydmFsID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFmaWx0ZXIuZmFjZXRfZmllbGRfcmVxdWVzdC5uYW1lKSB7XHJcbiAgICAgICAgICBjb25zdCBmaWVsZERldGFpbHM6IE1QTUZpZWxkID0gdGhpcy5nZXRGaWVsZERldGFpbHMoZmlsdGVyLmZhY2V0X2ZpZWxkX3JlcXVlc3QuZmllbGRfaWQpO1xyXG4gICAgICAgICAgZmlsdGVyLmZhY2V0X2ZpZWxkX3JlcXVlc3QubmFtZSA9IGZpZWxkRGV0YWlscyA/IGZpZWxkRGV0YWlscy5ESVNQTEFZX05BTUUgOiAnJztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRGaWx0ZXJTZWxlY3RpbkFuZFZpZXdUeXBlKGZpbHRlcik7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc29sZS5lcnJvcignRmFjZXQgQ29uZmlncmF0aW9uIG5vdCBmb3VuZCcpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVzZXRGYWNldCgpIHtcclxuICAgIHRoaXMuZmFjZXRGaWx0ZXJzID0gT2JqZWN0LmFzc2lnbihbXSk7XHJcbiAgICB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzID0gT2JqZWN0LmFzc2lnbihbXSk7XHJcbiAgICB0aGlzLnVwZGF0ZUNoaWNrbGV0KCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGlmIChuYXZpZ2F0b3IubGFuZ3VhZ2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICB0aGlzLmFkYXB0ZXIuc2V0TG9jYWxlKG5hdmlnYXRvci5sYW5ndWFnZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5hbGxNUE1GaWVsZHMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbEFwcGxpY2F0aW9uRmllbGRzKCk7XHJcblxyXG4gICAgdGhpcy50bXBGYWNldENoYW5nZUV2ZW50U3Vic2NyaXB0aW9uJCA9IHRoaXMuZmFjZXRDaGFuZ2VFdmVudFNlcnZpY2Uub25GYWNldEZpbHRlcnNDaGFuZ2Uuc3Vic2NyaWJlKGZhY2V0ID0+IHtcclxuICAgICAgdGhpcy5mYWNldEZpbHRlcnMgPSBmYWNldCB8fCBPYmplY3QuYXNzaWduKFtdKTtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5mYWNldENvbmZpZ3VyYXRpb25zKSAmJiB0aGlzLmZhY2V0Q29uZmlndXJhdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuaW5pdGFsaXplRmFjZXRzKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5mYWNldFNlcnZpY2Uuc2V0RmFjZXRDb25maWd1cmF0aW9ucygpLnN1YnNjcmliZShmYWNldENvbmZpZ3VyYXRpb25zID0+IHtcclxuICAgICAgICAgIHRoaXMuZmFjZXRDb25maWd1cmF0aW9ucyA9IGZhY2V0Q29uZmlndXJhdGlvbnM7XHJcbiAgICAgICAgICB0aGlzLmluaXRhbGl6ZUZhY2V0cygpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMuZmFjZXRGaWx0ZXJzID0gT2JqZWN0LmFzc2lnbihbXSk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRmFjZXRGaWx0ZXJzID0gW107XHJcbiAgICAgICAgICB0aGlzLmluaXRhbGl6ZUZhY2V0cygpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnRtcEZhY2V0UmVzZXRFdmVudFN1YnNjcmlwdGlvbiQgPSB0aGlzLmZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlLnJlc2V0RmFjZXQkLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgdGhpcy5yZXNldEZhY2V0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5yZXNldEZhY2V0KCk7XHJcbiAgICAvLyB0aGlzLmZhY2V0Q2hhbmdlRXZlbnRTZXJ2aWNlLnVwZGF0ZUZhY2V0U2VsZWN0RmFjZXRDb25kaXRpb24obnVsbCk7IFxyXG4gICAgaWYgKHRoaXMudG1wRmFjZXRDaGFuZ2VFdmVudFN1YnNjcmlwdGlvbiQpIHtcclxuICAgICAgdGhpcy50bXBGYWNldENoYW5nZUV2ZW50U3Vic2NyaXB0aW9uJC51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMudG1wRmFjZXRSZXNldEV2ZW50U3Vic2NyaXB0aW9uJCkge1xyXG4gICAgICB0aGlzLnRtcEZhY2V0UmVzZXRFdmVudFN1YnNjcmlwdGlvbiQudW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==