import { __decorate } from "tslib";
import { Component, Output, Input, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
var FacetButtonComponent = /** @class */ (function () {
    function FacetButtonComponent() {
        this.facetToggled = new EventEmitter();
        this.expansionState = false;
        this.leftState = 'default';
        this.isLeftNavExpanded = true;
    }
    FacetButtonComponent.prototype.toggleFacet = function () {
        if (this.isRMView === true) {
            this.leftState === 'default' ? this.isLeftNavExpanded = false : this.isLeftNavExpanded = true;
            this.leftState = (this.leftState === 'default' ? 'rotated' : 'default');
        }
        this.expansionState = !this.expansionState;
        this.facetToggled.next(this.expansionState);
    };
    FacetButtonComponent.prototype.ngOnInit = function () {
        this.disabled = this.disabled ? true : false;
        this.expansionState = this.initalState ? true : false;
    };
    __decorate([
        Input()
    ], FacetButtonComponent.prototype, "disabled", void 0);
    __decorate([
        Input()
    ], FacetButtonComponent.prototype, "initalState", void 0);
    __decorate([
        Input()
    ], FacetButtonComponent.prototype, "isRMView", void 0);
    __decorate([
        Output()
    ], FacetButtonComponent.prototype, "facetToggled", void 0);
    FacetButtonComponent = __decorate([
        Component({
            selector: 'mpm-facet-button',
            template: "<button class=\"mpm-facet-btn\" mat-icon-button (click)=\"toggleFacet()\"\r\n    [disabled]=\"disabled\" matTooltip=\"Click to toggle Facet View\">\r\n    <mat-icon *ngIf=\"isRMView === true\" color=\"primary\" [@rotatedState]='leftState'>chevron_left\r\n    </mat-icon>\r\n    <mat-icon *ngIf=\"isRMView === false\" color=\"primary\">filter_list\r\n    </mat-icon>\r\n</button>",
            animations: [
                trigger('rotatedState', [
                    state('default', style({ transform: 'rotate(0deg)' })),
                    state('rotated', style({ transform: 'rotate(180deg)' })),
                    transition('rotated => default', animate('250ms ease-out')),
                    transition('default => rotated', animate('250ms ease-in'))
                ])
            ],
            styles: [""]
        })
    ], FacetButtonComponent);
    return FacetButtonComponent;
}());
export { FacetButtonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQtYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L2ZhY2V0LWJ1dHRvbi9mYWNldC1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFlakY7SUFhRTtRQVBVLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUU1QyxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUV2QixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLHNCQUFpQixHQUFHLElBQUksQ0FBQztJQUVULENBQUM7SUFFakIsMENBQVcsR0FBWDtRQUNFLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDOUYsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3pFO1FBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBRTlDLENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM3QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3hELENBQUM7SUExQlE7UUFBUixLQUFLLEVBQUU7MERBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTs2REFBYTtJQUNaO1FBQVIsS0FBSyxFQUFFOzBEQUFVO0lBRVI7UUFBVCxNQUFNLEVBQUU7OERBQW1DO0lBTmpDLG9CQUFvQjtRQWJoQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLHNZQUE0QztZQUU1QyxVQUFVLEVBQUU7Z0JBQ1YsT0FBTyxDQUFDLGNBQWMsRUFBRTtvQkFDcEIsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsY0FBYyxFQUFFLENBQUMsQ0FBQztvQkFDdEQsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDO29CQUN4RCxVQUFVLENBQUMsb0JBQW9CLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQzNELFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7aUJBQzdELENBQUM7YUFDTDs7U0FDQSxDQUFDO09BQ1csb0JBQW9CLENBOEJoQztJQUFELDJCQUFDO0NBQUEsQUE5QkQsSUE4QkM7U0E5Qlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE91dHB1dCwgSW5wdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgdHJpZ2dlciwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCBhbmltYXRlIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1mYWNldC1idXR0b24nLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mYWNldC1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZhY2V0LWJ1dHRvbi5jb21wb25lbnQuc2NzcyddLFxyXG4gIGFuaW1hdGlvbnM6IFtcclxuICAgIHRyaWdnZXIoJ3JvdGF0ZWRTdGF0ZScsIFtcclxuICAgICAgICBzdGF0ZSgnZGVmYXVsdCcsIHN0eWxlKHsgdHJhbnNmb3JtOiAncm90YXRlKDBkZWcpJyB9KSksXHJcbiAgICAgICAgc3RhdGUoJ3JvdGF0ZWQnLCBzdHlsZSh7IHRyYW5zZm9ybTogJ3JvdGF0ZSgxODBkZWcpJyB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbigncm90YXRlZCA9PiBkZWZhdWx0JywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1vdXQnKSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignZGVmYXVsdCA9PiByb3RhdGVkJywgYW5pbWF0ZSgnMjUwbXMgZWFzZS1pbicpKVxyXG4gICAgXSlcclxuXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEZhY2V0QnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgZGlzYWJsZWQ7XHJcbiAgQElucHV0KCkgaW5pdGFsU3RhdGU7XHJcbiAgQElucHV0KCkgaXNSTVZpZXc7XHJcblxyXG4gIEBPdXRwdXQoKSBmYWNldFRvZ2dsZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGV4cGFuc2lvblN0YXRlID0gZmFsc2U7XHJcblxyXG4gIGxlZnRTdGF0ZSA9ICdkZWZhdWx0JztcclxuICBpc0xlZnROYXZFeHBhbmRlZCA9IHRydWU7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHRvZ2dsZUZhY2V0KCkge1xyXG4gICAgaWYgKHRoaXMuaXNSTVZpZXcgPT09IHRydWUpIHtcclxuICAgICAgdGhpcy5sZWZ0U3RhdGUgPT09ICdkZWZhdWx0JyA/IHRoaXMuaXNMZWZ0TmF2RXhwYW5kZWQgPSBmYWxzZSA6IHRoaXMuaXNMZWZ0TmF2RXhwYW5kZWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLmxlZnRTdGF0ZSA9ICh0aGlzLmxlZnRTdGF0ZSA9PT0gJ2RlZmF1bHQnID8gJ3JvdGF0ZWQnIDogJ2RlZmF1bHQnKTtcclxuICAgIH1cclxuICAgIHRoaXMuZXhwYW5zaW9uU3RhdGUgPSAhdGhpcy5leHBhbnNpb25TdGF0ZTtcclxuICAgIHRoaXMuZmFjZXRUb2dnbGVkLm5leHQodGhpcy5leHBhbnNpb25TdGF0ZSk7XHJcblxyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5kaXNhYmxlZCA/IHRydWUgOiBmYWxzZTtcclxuICAgIHRoaXMuZXhwYW5zaW9uU3RhdGUgPSB0aGlzLmluaXRhbFN0YXRlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gIH1cclxuXHJcbn1cclxuIl19