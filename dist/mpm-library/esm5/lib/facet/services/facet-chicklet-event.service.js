import { __decorate } from "tslib";
import { Injectable, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
var FacetChickletEventService = /** @class */ (function () {
    function FacetChickletEventService() {
        this.chickletFacetFilterChanged$ = new EventEmitter();
        this.chickletRemoved$ = new EventEmitter();
        this.chickletFacetFilterChanged$ = new EventEmitter();
        this.chickletRemoved$ = new EventEmitter();
    }
    FacetChickletEventService.prototype.updateChickletFilters = function (filters) {
        this.chickletFacetFilterChanged$.emit(filters);
    };
    FacetChickletEventService.prototype.removeChicklet = function (chicklet) {
        this.chickletRemoved$.emit(chicklet);
    };
    FacetChickletEventService.ɵprov = i0.ɵɵdefineInjectable({ factory: function FacetChickletEventService_Factory() { return new FacetChickletEventService(); }, token: FacetChickletEventService, providedIn: "root" });
    FacetChickletEventService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], FacetChickletEventService);
    return FacetChickletEventService;
}());
export { FacetChickletEventService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQtY2hpY2tsZXQtZXZlbnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L3NlcnZpY2VzL2ZhY2V0LWNoaWNrbGV0LWV2ZW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUt6RDtJQUlFO1FBSE8sZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqRCxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRzNDLElBQUksQ0FBQywyQkFBMkIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFFRCx5REFBcUIsR0FBckIsVUFBc0IsT0FBTztRQUMzQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxrREFBYyxHQUFkLFVBQWUsUUFBUTtRQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7O0lBZlUseUJBQXlCO1FBSHJDLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyx5QkFBeUIsQ0FpQnJDO29DQXRCRDtDQXNCQyxBQWpCRCxJQWlCQztTQWpCWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZhY2V0Q2hpY2tsZXRFdmVudFNlcnZpY2Uge1xyXG4gIHB1YmxpYyBjaGlja2xldEZhY2V0RmlsdGVyQ2hhbmdlZCQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgcHVibGljIGNoaWNrbGV0UmVtb3ZlZCQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgdGhpcy5jaGlja2xldEZhY2V0RmlsdGVyQ2hhbmdlZCQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgICB0aGlzLmNoaWNrbGV0UmVtb3ZlZCQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVDaGlja2xldEZpbHRlcnMoZmlsdGVycykge1xyXG4gICAgdGhpcy5jaGlja2xldEZhY2V0RmlsdGVyQ2hhbmdlZCQuZW1pdChmaWx0ZXJzKTtcclxuICB9XHJcblxyXG4gIHJlbW92ZUNoaWNrbGV0KGNoaWNrbGV0KSB7XHJcbiAgICB0aGlzLmNoaWNrbGV0UmVtb3ZlZCQuZW1pdChjaGlja2xldCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=