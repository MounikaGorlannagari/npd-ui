import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../shared/services/field-config.service";
import * as i3 from "../../notification/notification.service";
var FacetService = /** @class */ (function () {
    function FacetService(appService, fieldConfigService, notificationService) {
        this.appService = appService;
        this.fieldConfigService = fieldConfigService;
        this.notificationService = notificationService;
        this.allFacetConfigs = [];
        this.GetFacetConfigurationWS = 'GetFacetConfiguration';
        this.GetFacetConfigurationNS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
    }
    FacetService.prototype.getCastedFacetConfigResponse = function (facetConfigRes) {
        var _this = this;
        var facetConfigs = [];
        if (Array.isArray(facetConfigRes) && facetConfigRes.length) {
            facetConfigRes.forEach(function (config) {
                var facetConfig = {
                    id: config.id,
                    name: config.name,
                    multiSelect: (config.multiSelect === 'true') ? true : false,
                    description: config.description,
                    defaultFacetDisplayed: parseInt(config.defaultFacetDisplayed, 0),
                    defaultFacetValueDisplayed: parseInt(config.defaultFacetValueDisplayed, 0),
                    valueOrder: config.valueOrder,
                    valueLimit: parseInt(config.valueLimit, 0),
                    fields: {
                        facetField: []
                    }
                };
                var faceFields = acronui.findObjectsByProp(config, 'facetField');
                if (Array.isArray(faceFields) && faceFields.length) {
                    faceFields.forEach(function (field) {
                        if (field && field.id) {
                            var matchField = _this.fieldConfigService.getFieldByKeyValue('Id', field.fieldId);
                            if (matchField) {
                                facetConfig.fields.facetField.push(matchField);
                            }
                        }
                    });
                }
                facetConfigs.push(facetConfig);
            });
        }
        return facetConfigs;
    };
    /* setFacetConfigurations(): Observable<any[]> {
      return new Observable(observer => {
        if (Array.isArray(this.allFacetConfigs) && this.allFacetConfigs.length) {
          observer.next(this.allFacetConfigs);
          observer.complete();
          return;
        }
        this.appService.invokeRequest(this.GetFacetConfigurationNS, this.GetFacetConfigurationWS, null).subscribe(response => {
          const facetConfig: any[] = acronui.findObjectsByProp(response, 'FacetConfig');
          this.allFacetConfigs = this.getCastedFacetConfigResponse(facetConfig);
          observer.next(this.allFacetConfigs);
          observer.complete();
        }, err => {
          this.notificationService.error('Something went wrong while getting Facet config');
          observer.error();
          observer.complete();
        });
      });
    } */
    FacetService.prototype.setFacetConfigurations = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.FACET_CONFIGURATIONS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.FACET_CONFIGURATIONS)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.GetFacetConfigurationNS, _this.GetFacetConfigurationWS, null)
                    .subscribe(function (response) {
                    var facetConfig = acronui.findObjectsByProp(response, 'FacetConfig');
                    _this.allFacetConfigs = _this.getCastedFacetConfigResponse(facetConfig);
                    sessionStorage.setItem(SessionStorageConstants.FACET_CONFIGURATIONS, JSON.stringify(_this.allFacetConfigs));
                    observer.next(_this.allFacetConfigs);
                    observer.complete();
                }, function (error) {
                    _this.notificationService.error('Something went wrong while getting Facet config');
                    observer.error();
                    observer.complete();
                });
            }
        });
    };
    FacetService.prototype.facetIntervalDisplayValues = function (facet) {
        var _this = this;
        facet.facet_value_list.forEach(function (element) {
            var displayName = '';
            var displayYear;
            var intervalLabel = (element.date_interval.interval_label.split(' '));
            intervalLabel.forEach(function (elementName) {
                if (elementName === '%%YEAR%%') {
                    displayName = displayName + new Date().getFullYear();
                }
                else {
                    if (elementName.includes('%%YEAR')) {
                        var yearCreation = elementName.split('%%YEAR');
                        yearCreation.forEach(function (data) {
                            var yearValue = data.split('%%');
                            yearValue.forEach(function (value) {
                                if (_this.isNumber(value)) {
                                    var adjustmentOffset = +value;
                                    displayYear = new Date().getFullYear() + (adjustmentOffset);
                                    displayName = displayName + ' ' + displayYear;
                                }
                                else {
                                    displayName = displayName + ' ';
                                }
                            });
                        });
                    }
                    else {
                        displayName = displayName + elementName + ' ';
                    }
                }
            });
            if (element.date_interval) {
                element.date_interval["displayName"] = displayName;
            }
            element.displayName = displayName;
        });
    };
    FacetService.prototype.isNumber = function (data) {
        return ((data <= 0 || data > 0) && (data.length > 0)) ? true : false;
    };
    FacetService.ctorParameters = function () { return [
        { type: AppService },
        { type: FieldConfigService },
        { type: NotificationService }
    ]; };
    FacetService.ɵprov = i0.ɵɵdefineInjectable({ factory: function FacetService_Factory() { return new FacetService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.FieldConfigService), i0.ɵɵinject(i3.NotificationService)); }, token: FacetService, providedIn: "root" });
    FacetService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], FacetService);
    return FacetService;
}());
export { FacetService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2ZhY2V0L3NlcnZpY2VzL2ZhY2V0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUVsRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQzs7Ozs7QUFLM0Y7SUFFRSxzQkFDUyxVQUFzQixFQUN0QixrQkFBc0MsRUFDdEMsbUJBQXdDO1FBRnhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBR2pELG9CQUFlLEdBQXFCLEVBQUUsQ0FBQztRQUV2Qyw0QkFBdUIsR0FBRyx1QkFBdUIsQ0FBQztRQUNsRCw0QkFBdUIsR0FBRywrQ0FBK0MsQ0FBQztJQUx0RSxDQUFDO0lBT0wsbURBQTRCLEdBQTVCLFVBQTZCLGNBQXFCO1FBQWxELGlCQWdDQztRQS9CQyxJQUFNLFlBQVksR0FBcUIsRUFBRSxDQUFDO1FBQzFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxFQUFFO1lBQzFELGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO2dCQUMzQixJQUFNLFdBQVcsR0FBbUI7b0JBQ2xDLEVBQUUsRUFBRSxNQUFNLENBQUMsRUFBRTtvQkFDYixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7b0JBQ2pCLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSztvQkFDM0QsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXO29CQUMvQixxQkFBcUIsRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQztvQkFDaEUsMEJBQTBCLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQywwQkFBMEIsRUFBRSxDQUFDLENBQUM7b0JBQzFFLFVBQVUsRUFBRSxNQUFNLENBQUMsVUFBVTtvQkFDN0IsVUFBVSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztvQkFDMUMsTUFBTSxFQUFFO3dCQUNOLFVBQVUsRUFBRSxFQUFFO3FCQUNmO2lCQUNGLENBQUM7Z0JBQ0YsSUFBTSxVQUFVLEdBQVUsT0FBTyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFDMUUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxNQUFNLEVBQUU7b0JBQ2xELFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO3dCQUN0QixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsRUFBRSxFQUFFOzRCQUNyQixJQUFNLFVBQVUsR0FBYSxLQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDN0YsSUFBSSxVQUFVLEVBQUU7Z0NBQ2QsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzZCQUNoRDt5QkFDRjtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxZQUFZLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxPQUFPLFlBQVksQ0FBQztJQUN0QixDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQWtCSTtJQUVKLDZDQUFzQixHQUF0QjtRQUFBLGlCQW9CQztRQW5CQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ2pGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHVCQUF1QixFQUFFLEtBQUksQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUM7cUJBQzVGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2pCLElBQU0sV0FBVyxHQUFVLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsYUFBYSxDQUFDLENBQUM7b0JBQzlFLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLDRCQUE0QixDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN0RSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7b0JBQzNHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO29CQUNsRixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlEQUEwQixHQUExQixVQUEyQixLQUFLO1FBQWhDLGlCQWtDQztRQWpDQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUNwQyxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDckIsSUFBSSxXQUFXLENBQUM7WUFDaEIsSUFBTSxhQUFhLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN4RSxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsV0FBVztnQkFDL0IsSUFBSSxXQUFXLEtBQUssVUFBVSxFQUFFO29CQUM5QixXQUFXLEdBQUcsV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3REO3FCQUFNO29CQUNMLElBQUksV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbEMsSUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDakQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ3ZCLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ25DLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO2dDQUNyQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7b0NBQ3hCLElBQU0sZ0JBQWdCLEdBQVcsQ0FBQyxLQUFLLENBQUM7b0NBQ3hDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQ0FDNUQsV0FBVyxHQUFHLFdBQVcsR0FBRyxHQUFHLEdBQUcsV0FBVyxDQUFDO2lDQUMvQztxQ0FBTTtvQ0FDTCxXQUFXLEdBQUcsV0FBVyxHQUFHLEdBQUcsQ0FBQztpQ0FDakM7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7d0JBRUwsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsV0FBVyxHQUFHLFdBQVcsR0FBRyxXQUFXLEdBQUcsR0FBRyxDQUFDO3FCQUMvQztpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBRyxPQUFPLENBQUMsYUFBYSxFQUFFO2dCQUN4QixPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxHQUFDLFdBQVcsQ0FBQzthQUNsRDtZQUNELE9BQU8sQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELCtCQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1gsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3ZFLENBQUM7O2dCQTVIb0IsVUFBVTtnQkFDRixrQkFBa0I7Z0JBQ2pCLG1CQUFtQjs7O0lBTHRDLFlBQVk7UUFIeEIsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLFlBQVksQ0FpSXhCO3VCQTlJRDtDQThJQyxBQWpJRCxJQWlJQztTQWpJWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IE1QTUZhY2V0Q29uZmlnIH0gZnJvbSAnLi4vb2JqZWN0cy9NUE1GYWNldENvbmZpZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZhY2V0U2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIGFsbEZhY2V0Q29uZmlnczogTVBNRmFjZXRDb25maWdbXSA9IFtdO1xyXG5cclxuICBHZXRGYWNldENvbmZpZ3VyYXRpb25XUyA9ICdHZXRGYWNldENvbmZpZ3VyYXRpb24nO1xyXG4gIEdldEZhY2V0Q29uZmlndXJhdGlvbk5TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcblxyXG4gIGdldENhc3RlZEZhY2V0Q29uZmlnUmVzcG9uc2UoZmFjZXRDb25maWdSZXM6IGFueVtdKTogTVBNRmFjZXRDb25maWdbXSB7XHJcbiAgICBjb25zdCBmYWNldENvbmZpZ3M6IE1QTUZhY2V0Q29uZmlnW10gPSBbXTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KGZhY2V0Q29uZmlnUmVzKSAmJiBmYWNldENvbmZpZ1Jlcy5sZW5ndGgpIHtcclxuICAgICAgZmFjZXRDb25maWdSZXMuZm9yRWFjaChjb25maWcgPT4ge1xyXG4gICAgICAgIGNvbnN0IGZhY2V0Q29uZmlnOiBNUE1GYWNldENvbmZpZyA9IHtcclxuICAgICAgICAgIGlkOiBjb25maWcuaWQsXHJcbiAgICAgICAgICBuYW1lOiBjb25maWcubmFtZSxcclxuICAgICAgICAgIG11bHRpU2VsZWN0OiAoY29uZmlnLm11bHRpU2VsZWN0ID09PSAndHJ1ZScpID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246IGNvbmZpZy5kZXNjcmlwdGlvbixcclxuICAgICAgICAgIGRlZmF1bHRGYWNldERpc3BsYXllZDogcGFyc2VJbnQoY29uZmlnLmRlZmF1bHRGYWNldERpc3BsYXllZCwgMCksXHJcbiAgICAgICAgICBkZWZhdWx0RmFjZXRWYWx1ZURpc3BsYXllZDogcGFyc2VJbnQoY29uZmlnLmRlZmF1bHRGYWNldFZhbHVlRGlzcGxheWVkLCAwKSxcclxuICAgICAgICAgIHZhbHVlT3JkZXI6IGNvbmZpZy52YWx1ZU9yZGVyLFxyXG4gICAgICAgICAgdmFsdWVMaW1pdDogcGFyc2VJbnQoY29uZmlnLnZhbHVlTGltaXQsIDApLFxyXG4gICAgICAgICAgZmllbGRzOiB7XHJcbiAgICAgICAgICAgIGZhY2V0RmllbGQ6IFtdXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBmYWNlRmllbGRzOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoY29uZmlnLCAnZmFjZXRGaWVsZCcpO1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZhY2VGaWVsZHMpICYmIGZhY2VGaWVsZHMubGVuZ3RoKSB7XHJcbiAgICAgICAgICBmYWNlRmllbGRzLmZvckVhY2goZmllbGQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZmllbGQgJiYgZmllbGQuaWQpIHtcclxuICAgICAgICAgICAgICBjb25zdCBtYXRjaEZpZWxkOiBNUE1GaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZSgnSWQnLCBmaWVsZC5maWVsZElkKTtcclxuICAgICAgICAgICAgICBpZiAobWF0Y2hGaWVsZCkge1xyXG4gICAgICAgICAgICAgICAgZmFjZXRDb25maWcuZmllbGRzLmZhY2V0RmllbGQucHVzaChtYXRjaEZpZWxkKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmYWNldENvbmZpZ3MucHVzaChmYWNldENvbmZpZyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhY2V0Q29uZmlncztcclxuICB9XHJcblxyXG4gIC8qIHNldEZhY2V0Q29uZmlndXJhdGlvbnMoKTogT2JzZXJ2YWJsZTxhbnlbXT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5hbGxGYWNldENvbmZpZ3MpICYmIHRoaXMuYWxsRmFjZXRDb25maWdzLmxlbmd0aCkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hbGxGYWNldENvbmZpZ3MpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR2V0RmFjZXRDb25maWd1cmF0aW9uTlMsIHRoaXMuR2V0RmFjZXRDb25maWd1cmF0aW9uV1MsIG51bGwpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgY29uc3QgZmFjZXRDb25maWc6IGFueVtdID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ0ZhY2V0Q29uZmlnJyk7XHJcbiAgICAgICAgdGhpcy5hbGxGYWNldENvbmZpZ3MgPSB0aGlzLmdldENhc3RlZEZhY2V0Q29uZmlnUmVzcG9uc2UoZmFjZXRDb25maWcpO1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hbGxGYWNldENvbmZpZ3MpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0sIGVyciA9PiB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIEZhY2V0IGNvbmZpZycpO1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9ICovXHJcblxyXG4gIHNldEZhY2V0Q29uZmlndXJhdGlvbnMoKTogT2JzZXJ2YWJsZTxhbnlbXT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuRkFDRVRfQ09ORklHVVJBVElPTlMpICE9PSBudWxsKSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuRkFDRVRfQ09ORklHVVJBVElPTlMpKSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdldEZhY2V0Q29uZmlndXJhdGlvbk5TLCB0aGlzLkdldEZhY2V0Q29uZmlndXJhdGlvbldTLCBudWxsKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZhY2V0Q29uZmlnOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdGYWNldENvbmZpZycpO1xyXG4gICAgICAgICAgICB0aGlzLmFsbEZhY2V0Q29uZmlncyA9IHRoaXMuZ2V0Q2FzdGVkRmFjZXRDb25maWdSZXNwb25zZShmYWNldENvbmZpZyk7XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuRkFDRVRfQ09ORklHVVJBVElPTlMsIEpTT04uc3RyaW5naWZ5KHRoaXMuYWxsRmFjZXRDb25maWdzKSk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hbGxGYWNldENvbmZpZ3MpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgRmFjZXQgY29uZmlnJyk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBmYWNldEludGVydmFsRGlzcGxheVZhbHVlcyhmYWNldCkge1xyXG4gICAgZmFjZXQuZmFjZXRfdmFsdWVfbGlzdC5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICBsZXQgZGlzcGxheU5hbWUgPSAnJztcclxuICAgICAgbGV0IGRpc3BsYXlZZWFyO1xyXG4gICAgICBjb25zdCBpbnRlcnZhbExhYmVsID0gKGVsZW1lbnQuZGF0ZV9pbnRlcnZhbC5pbnRlcnZhbF9sYWJlbC5zcGxpdCgnICcpKTtcclxuICAgICAgaW50ZXJ2YWxMYWJlbC5mb3JFYWNoKGVsZW1lbnROYW1lID0+IHtcclxuICAgICAgICBpZiAoZWxlbWVudE5hbWUgPT09ICclJVlFQVIlJScpIHtcclxuICAgICAgICAgIGRpc3BsYXlOYW1lID0gZGlzcGxheU5hbWUgKyBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmIChlbGVtZW50TmFtZS5pbmNsdWRlcygnJSVZRUFSJykpIHtcclxuICAgICAgICAgICAgY29uc3QgeWVhckNyZWF0aW9uID0gZWxlbWVudE5hbWUuc3BsaXQoJyUlWUVBUicpO1xyXG4gICAgICAgICAgICB5ZWFyQ3JlYXRpb24uZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICAgICBjb25zdCB5ZWFyVmFsdWUgPSBkYXRhLnNwbGl0KCclJScpO1xyXG4gICAgICAgICAgICAgIHllYXJWYWx1ZS5mb3JFYWNoKHZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzTnVtYmVyKHZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICBjb25zdCBhZGp1c3RtZW50T2Zmc2V0OiBudW1iZXIgPSArdmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgIGRpc3BsYXlZZWFyID0gbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpICsgKGFkanVzdG1lbnRPZmZzZXQpO1xyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lICsgJyAnICsgZGlzcGxheVllYXI7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lICsgJyAnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lICsgZWxlbWVudE5hbWUgKyAnICc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgaWYoZWxlbWVudC5kYXRlX2ludGVydmFsKSB7XHJcbiAgICAgICAgZWxlbWVudC5kYXRlX2ludGVydmFsW1wiZGlzcGxheU5hbWVcIl09ZGlzcGxheU5hbWU7XHJcbiAgICAgIH1cclxuICAgICAgZWxlbWVudC5kaXNwbGF5TmFtZSA9IGRpc3BsYXlOYW1lO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBpc051bWJlcihkYXRhKSB7XHJcbiAgICByZXR1cm4gKChkYXRhIDw9IDAgfHwgZGF0YSA+IDApICYmIChkYXRhLmxlbmd0aCA+IDApKSA/IHRydWUgOiBmYWxzZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==