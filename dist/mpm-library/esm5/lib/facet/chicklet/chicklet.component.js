import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { FacetChickletEventService } from '../services/facet-chicklet-event.service';
var ChickletComponent = /** @class */ (function () {
    function ChickletComponent(facetChickletEventService) {
        this.facetChickletEventService = facetChickletEventService;
        this.filters = [];
        this.removed = new EventEmitter();
    }
    ChickletComponent.prototype.removeFilter = function (filter) {
        this.removed.next([filter]);
    };
    ChickletComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.facetChickletEventService.chickletFacetFilterChanged$.subscribe(function (items) {
            _this.filters = items;
        });
    };
    ChickletComponent.ctorParameters = function () { return [
        { type: FacetChickletEventService }
    ]; };
    __decorate([
        Output()
    ], ChickletComponent.prototype, "removed", void 0);
    ChickletComponent = __decorate([
        Component({
            selector: 'mpm-chicklet',
            template: "<div class=\"facet-chicklet-wrapper\">\r\n    <mat-chip-list class=\"chicklet-list\">\r\n        <mat-chip class=\"chicklet-chip\" *ngFor=\"let filter of filters\">\r\n            <span class=\"chicklet-value\" matTooltip=\"{{filter.value}}\">{{filter.value}}</span>\r\n            <mat-icon matChipRemove (click)=\"removeFilter(filter)\" matTooltip=\"Remove this filter\" mat-icon-button\r\n                aria-label=\"Remove Filter\">cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n</div>",
            styles: [".title{padding:0 5px}.chicklet-chip{padding:0 10px;font-size:12px;min-height:25px!important}.chicklet-chip .chicklet-value{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;max-width:200px;display:inline-block}.chicklet-chip:hover .chicklet-value{text-decoration:line-through}"]
        })
    ], ChickletComponent);
    return ChickletComponent;
}());
export { ChickletComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hpY2tsZXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZmFjZXQvY2hpY2tsZXQvY2hpY2tsZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFPckY7SUFNRSwyQkFDUyx5QkFBb0Q7UUFBcEQsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjtRQUw3RCxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBRUgsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFTLENBQUM7SUFJMUMsQ0FBQztJQUVMLHdDQUFZLEdBQVosVUFBYSxNQUFNO1FBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUFBLGlCQUlDO1FBSEMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLDJCQUEyQixDQUFDLFNBQVMsQ0FBQyxVQUFBLEtBQUs7WUFDeEUsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkFYbUMseUJBQXlCOztJQUhuRDtRQUFULE1BQU0sRUFBRTtzREFBcUM7SUFKbkMsaUJBQWlCO1FBTDdCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLDJnQkFBd0M7O1NBRXpDLENBQUM7T0FDVyxpQkFBaUIsQ0FvQjdCO0lBQUQsd0JBQUM7Q0FBQSxBQXBCRCxJQW9CQztTQXBCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRmFjZXRDaGlja2xldEV2ZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2ZhY2V0LWNoaWNrbGV0LWV2ZW50LnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY2hpY2tsZXQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jaGlja2xldC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY2hpY2tsZXQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2hpY2tsZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBmaWx0ZXJzID0gW107XHJcblxyXG4gIEBPdXRwdXQoKSByZW1vdmVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZmFjZXRDaGlja2xldEV2ZW50U2VydmljZTogRmFjZXRDaGlja2xldEV2ZW50U2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIHJlbW92ZUZpbHRlcihmaWx0ZXIpIHtcclxuICAgIHRoaXMucmVtb3ZlZC5uZXh0KFtmaWx0ZXJdKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5mYWNldENoaWNrbGV0RXZlbnRTZXJ2aWNlLmNoaWNrbGV0RmFjZXRGaWx0ZXJDaGFuZ2VkJC5zdWJzY3JpYmUoaXRlbXMgPT4ge1xyXG4gICAgICB0aGlzLmZpbHRlcnMgPSBpdGVtcztcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbn1cclxuIl19