import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { ShareAssetService } from './share-asset.service';
import { NotificationService } from '../notification/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../shared/components/confirmation-modal/confirmation-modal.component';
import { RequiredValidator } from '../shared/validators/required.validator';
import { EmailValidator } from '../shared/validators/email.validator';
import { LoaderService } from '../loader/loader.service';
import { ShareAssetEmailService } from './services/share-asset-email.service';
import { ProjectService } from '../project/shared/services/project.service';
import { DeliverableConstants } from '../project/tasks/deliverable/deliverable.constants';
var ShareAssetsComponent = /** @class */ (function () {
    function ShareAssetsComponent(fb, dialog, sharingService, shareAssetService, notificationService, loaderService, shareAssetEmailService, projectService) {
        this.fb = fb;
        this.dialog = dialog;
        this.sharingService = sharingService;
        this.shareAssetService = shareAssetService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.shareAssetEmailService = shareAssetEmailService;
        this.projectService = projectService;
        this.closeModalHandler = new EventEmitter();
        this.sharingOptions = [{
                name: 'Email with attachment',
                value: 'SEND_FILES',
                isDefault: true,
                emailSubject: 'Content sent from Media Manager via MPM.',
                emailMessage: 'Please find the attached content.'
            }, {
                name: 'Email with link to asset',
                value: 'SEND_LINK',
                isDefault: false,
                emailSubject: 'Content sent from Media Manager via MPM.',
                emailMessage: 'I am sending you assets from OpenText Media Manager via MPM. Please click the above link to access these in the Media Manager application.'
            }];
        this.required = true;
    }
    ShareAssetsComponent.prototype.onFieldChange = function (event) {
        this.shareAssetForm.updateValueAndValidity();
    };
    ShareAssetsComponent.prototype.closeDialog = function () {
        var _this = this;
        if (this.shareAssetForm.pristine) {
            this.closeModalHandler.emit(true);
        }
        else {
            var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '25%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result.isTrue) {
                    _this.closeModalHandler.emit(true);
                }
            });
        }
    };
    ShareAssetsComponent.prototype.getProperty = function (projectData, propertyId) {
        return this.projectService.getProperty(projectData, propertyId);
    };
    ShareAssetsComponent.prototype.shareAsset = function () {
        var _this = this;
        this.shareAssetForm.get('to').markAsTouched();
        this.shareAssetForm.get('from').markAsTouched();
        this.shareAssetForm.get('cc').markAsTouched();
        this.shareAssetForm.get('replyTo').markAsTouched();
        if (this.shareAssetForm.valid) {
            this.loaderService.show();
            this.shareAssetService.shareAsset(this.assets, this.shareAssetForm.getRawValue())
                .subscribe(function (response) {
                if (response) {
                    if (_this.assets.length === 1) {
                        var toAddress = _this.shareAssetForm.get('to').value.join(', ');
                        var deliverableId = _this.getProperty(_this.assets[0], DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID);
                        var assetName = _this.assets[0].name;
                        _this.shareAssetEmailService.sendShareAssetEmail(_this.sharingService.getCurrentUserDisplayName(), deliverableId, assetName, toAddress);
                    }
                    _this.loaderService.hide();
                    _this.notificationService.success('Asset(s) shared successfully');
                    _this.closeModalHandler.emit(true);
                }
                else {
                    _this.loaderService.hide();
                    _this.notificationService.error('Something went wrong while sharing assets');
                    _this.closeModalHandler.emit(true);
                }
            }, function (error) {
                _this.loaderService.hide();
                _this.notificationService.error('Something went wrong while sharing assets');
                _this.closeModalHandler.emit(true);
            });
        }
    };
    ShareAssetsComponent.prototype.intializeForm = function () {
        var _this = this;
        this.shareAssetForm = this.fb.group({
            sharingMethod: new FormControl(this.sharingOptions[0], [Validators.required]),
            to: new FormControl([], [Validators.required, RequiredValidator.validateRequired, EmailValidator.validateEmails]),
            from: new FormControl([this.sharingService.getCurrentUserEmailId()], [Validators.required, RequiredValidator.validateRequired, EmailValidator.validateEmails]),
            cc: new FormControl([], [EmailValidator.validateEmails]),
            replyTo: new FormControl([this.sharingService.getCurrentUserEmailId()], [EmailValidator.validateEmails]),
            subject: new FormControl(this.sharingOptions[0].emailSubject, [Validators.required]),
            message: new FormControl(this.sharingOptions[0].emailMessage),
            compressFiles: new FormControl(true)
        });
        this.shareAssetForm.get('from').disable();
        this.shareAssetForm.get('sharingMethod').valueChanges.subscribe(function (value) {
            _this.shareAssetForm.get('subject').patchValue(_this.shareAssetForm.get('sharingMethod').value.emailSubject);
            _this.shareAssetForm.get('message').patchValue(_this.shareAssetForm.get('sharingMethod').value.emailMessage);
            _this.shareAssetForm.updateValueAndValidity();
        });
    };
    ShareAssetsComponent.prototype.ngOnInit = function () {
        this.intializeForm();
    };
    ShareAssetsComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: MatDialog },
        { type: SharingService },
        { type: ShareAssetService },
        { type: NotificationService },
        { type: LoaderService },
        { type: ShareAssetEmailService },
        { type: ProjectService }
    ]; };
    __decorate([
        Input()
    ], ShareAssetsComponent.prototype, "assets", void 0);
    __decorate([
        Output()
    ], ShareAssetsComponent.prototype, "closeModalHandler", void 0);
    ShareAssetsComponent = __decorate([
        Component({
            selector: 'mpm-share-assets',
            template: "<div class=\"share-asset-container\">\r\n    <form class=\"share-asset-form\" *ngIf=\"shareAssetForm\" [formGroup]=\"shareAssetForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item sharing-method-radio-group full-width\">\r\n                <label id=\"sharing-option-label\">Sharing method: </label>\r\n                <mat-radio-group aria-labelledby=\"sharing-option-label\" formControlName=\"sharingMethod\">\r\n                    <mat-radio-button class=\"sharing-method-radio-btn\" color=\"primary\"\r\n                        *ngFor=\"let sharingOption of sharingOptions\" [value]=\"sharingOption\"\r\n                        [checked]=\"sharingOption.isDefault\">\r\n                        {{sharingOption.name}}\r\n                    </mat-radio-button>\r\n                </mat-radio-group>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'To'\" [emailFieldFormControl]=\"shareAssetForm.get('to')\"\r\n            (valueChanges)=\"onFieldChange($event)\" [required]=\"required\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'From'\" [emailFieldFormControl]=\"shareAssetForm.get('from')\"\r\n            (valueChanges)=\"onFieldChange($event)\" [required]=\"required\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'CC'\" [emailFieldFormControl]=\"shareAssetForm.get('cc')\"\r\n            (valueChanges)=\"onFieldChange($event)\">\r\n        </mpm-custom-email-field>\r\n\r\n        <mpm-custom-email-field class=\"full-width\" [label]=\"'Reply To'\"\r\n            [emailFieldFormControl]=\"shareAssetForm.get('replyTo')\" (valueChanges)=\"onFieldChange($event)\">\r\n        </mpm-custom-email-field>\r\n\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" class=\"full-width\">\r\n                    <mat-label>Subject</mat-label>\r\n                    <input matInput formControlName=\"subject\" required>\r\n                    <mat-error\r\n                        *ngIf=\"shareAssetForm.get('subject').errors && shareAssetForm.get('subject').errors.required\">\r\n                        Email subject is required.\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" class=\"full-width\">\r\n                    <mat-label>Message</mat-label>\r\n                    <textarea matInput formControlName=\"message\" rows=\"3\"></textarea>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-checkbox color=\"primary\" formControlName=\"compressFiles\" class=\"full-width\"\r\n                    [ngClass]=\"shareAssetForm.get('sharingMethod').value.value === 'SEND_FILES' ? 'visible' : 'hidden'\">\r\n                    <span>Compress the assets into one file?</span>\r\n                </mat-checkbox>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"form-actions\">\r\n                    <button mat-stroked-button type=\"button\" (click)=\"closeDialog()\">Cancel</button>\r\n                    <button color='primary' mat-flat-button type=\"button\" (click)=\"shareAsset()\"\r\n                        [disabled]=\"(shareAssetForm && shareAssetForm.invalid) || (shareAssetForm && shareAssetForm.pristine) \">Share</button>\r\n                </span>\r\n            </div>\r\n        </div>\r\n\r\n    </form>\r\n</div>",
            styles: [".full-width{width:100%;padding:0 8px}.share-asset-container{padding:8px;font-size:14px}.share-asset-container .share-asset-form{display:flex;flex-direction:column;flex-wrap:wrap}.share-asset-container .share-asset-form .sharing-method-radio-group{align-items:center;padding-bottom:16px}.share-asset-container .share-asset-form .sharing-method-radio-group .sharing-method-radio-btn{padding:0 8px}.share-asset-container .share-asset-form .form-actions{padding:0 16px;margin-left:auto}.share-asset-container .share-asset-form .form-actions button{margin-left:16px}.share-asset-container .share-asset-form :ng-deep mpm-custom-email-field .mat-form-field-wrapper{padding-bottom:0!important}.visible{visibility:visible}.hidden{visibility:hidden}"]
        })
    ], ShareAssetsComponent);
    return ShareAssetsComponent;
}());
export { ShareAssetsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDM0UsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHNFQUFzRSxDQUFDO0FBQ2xILE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBUTFGO0lBc0JJLDhCQUNXLEVBQWUsRUFDZixNQUFpQixFQUNqQixjQUE4QixFQUM5QixpQkFBb0MsRUFDcEMsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLHNCQUE4QyxFQUM5QyxjQUE4QjtRQVA5QixPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBM0IvQixzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRXRELG1CQUFjLEdBQUcsQ0FBQztnQkFDZCxJQUFJLEVBQUUsdUJBQXVCO2dCQUM3QixLQUFLLEVBQUUsWUFBWTtnQkFDbkIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsWUFBWSxFQUFFLDBDQUEwQztnQkFDeEQsWUFBWSxFQUFFLG1DQUFtQzthQUNwRCxFQUFFO2dCQUNDLElBQUksRUFBRSwwQkFBMEI7Z0JBQ2hDLEtBQUssRUFBRSxXQUFXO2dCQUNsQixTQUFTLEVBQUUsS0FBSztnQkFDaEIsWUFBWSxFQUFFLDBDQUEwQztnQkFDeEQsWUFBWSxFQUFFLDRJQUE0STthQUM3SixDQUFDLENBQUM7UUFHSCxhQUFRLEdBQUcsSUFBSSxDQUFDO0lBV1osQ0FBQztJQUVMLDRDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2pELENBQUM7SUFFRCwwQ0FBVyxHQUFYO1FBQUEsaUJBbUJDO1FBbEJHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUU7WUFDOUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyQzthQUFNO1lBQ0gsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzNELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxFQUFFLGlDQUFpQztvQkFDMUMsWUFBWSxFQUFFLEtBQUs7b0JBQ25CLFlBQVksRUFBRSxJQUFJO2lCQUNyQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO2dCQUNwQyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO29CQUN6QixLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNyQztZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRUQsMENBQVcsR0FBWCxVQUFZLFdBQVcsRUFBRSxVQUFVO1FBQy9CLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCx5Q0FBVSxHQUFWO1FBQUEsaUJBZ0NDO1FBL0JHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRW5ELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDNUUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsRUFBRTtvQkFDVixJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDMUIsSUFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDakUsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLG9CQUFvQixDQUFDLG9DQUFvQyxDQUFDLENBQUM7d0JBQ2xILElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUN0QyxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQzNDLEtBQUksQ0FBQyxjQUFjLENBQUMseUJBQXlCLEVBQUUsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3FCQUM3RjtvQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLDhCQUE4QixDQUFDLENBQUM7b0JBQ2pFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3JDO3FCQUFNO29CQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztvQkFDNUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckM7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztnQkFDNUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQztTQUNWO0lBQ0wsQ0FBQztJQUVELDRDQUFhLEdBQWI7UUFBQSxpQkFvQkM7UUFuQkcsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztZQUNoQyxhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3RSxFQUFFLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDakgsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUM5SixFQUFFLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hELE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hHLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwRixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7WUFDN0QsYUFBYSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQztTQUN2QyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUUxQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsS0FBSztZQUNqRSxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNHLEtBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7WUFFM0csS0FBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7Z0JBakdjLFdBQVc7Z0JBQ1AsU0FBUztnQkFDRCxjQUFjO2dCQUNYLGlCQUFpQjtnQkFDZixtQkFBbUI7Z0JBQ3pCLGFBQWE7Z0JBQ0osc0JBQXNCO2dCQUM5QixjQUFjOztJQTVCaEM7UUFBUixLQUFLLEVBQUU7d0RBQW9CO0lBQ2xCO1FBQVQsTUFBTSxFQUFFO21FQUE2QztJQUg3QyxvQkFBb0I7UUFOaEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGtCQUFrQjtZQUM1Qiw2MEhBQTRDOztTQUUvQyxDQUFDO09BRVcsb0JBQW9CLENBMEhoQztJQUFELDJCQUFDO0NBQUEsQUExSEQsSUEwSEM7U0ExSFksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmVBc3NldFNlcnZpY2UgfSBmcm9tICcuL3NoYXJlLWFzc2V0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFJlcXVpcmVkVmFsaWRhdG9yIH0gZnJvbSAnLi4vc2hhcmVkL3ZhbGlkYXRvcnMvcmVxdWlyZWQudmFsaWRhdG9yJztcclxuaW1wb3J0IHsgRW1haWxWYWxpZGF0b3IgfSBmcm9tICcuLi9zaGFyZWQvdmFsaWRhdG9ycy9lbWFpbC52YWxpZGF0b3InO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmVBc3NldEVtYWlsU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvc2hhcmUtYXNzZXQtZW1haWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUuY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tc2hhcmUtYXNzZXRzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zaGFyZS1hc3NldHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vc2hhcmUtYXNzZXRzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyZUFzc2V0c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgYXNzZXRzOiBBcnJheTxhbnk+O1xyXG4gICAgQE91dHB1dCgpIGNsb3NlTW9kYWxIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgc2hhcmluZ09wdGlvbnMgPSBbe1xyXG4gICAgICAgIG5hbWU6ICdFbWFpbCB3aXRoIGF0dGFjaG1lbnQnLFxyXG4gICAgICAgIHZhbHVlOiAnU0VORF9GSUxFUycsXHJcbiAgICAgICAgaXNEZWZhdWx0OiB0cnVlLFxyXG4gICAgICAgIGVtYWlsU3ViamVjdDogJ0NvbnRlbnQgc2VudCBmcm9tIE1lZGlhIE1hbmFnZXIgdmlhIE1QTS4nLFxyXG4gICAgICAgIGVtYWlsTWVzc2FnZTogJ1BsZWFzZSBmaW5kIHRoZSBhdHRhY2hlZCBjb250ZW50LidcclxuICAgIH0sIHtcclxuICAgICAgICBuYW1lOiAnRW1haWwgd2l0aCBsaW5rIHRvIGFzc2V0JyxcclxuICAgICAgICB2YWx1ZTogJ1NFTkRfTElOSycsXHJcbiAgICAgICAgaXNEZWZhdWx0OiBmYWxzZSxcclxuICAgICAgICBlbWFpbFN1YmplY3Q6ICdDb250ZW50IHNlbnQgZnJvbSBNZWRpYSBNYW5hZ2VyIHZpYSBNUE0uJyxcclxuICAgICAgICBlbWFpbE1lc3NhZ2U6ICdJIGFtIHNlbmRpbmcgeW91IGFzc2V0cyBmcm9tIE9wZW5UZXh0IE1lZGlhIE1hbmFnZXIgdmlhIE1QTS4gUGxlYXNlIGNsaWNrIHRoZSBhYm92ZSBsaW5rIHRvIGFjY2VzcyB0aGVzZSBpbiB0aGUgTWVkaWEgTWFuYWdlciBhcHBsaWNhdGlvbi4nXHJcbiAgICB9XTtcclxuXHJcbiAgICBzaGFyZUFzc2V0Rm9ybTogRm9ybUdyb3VwO1xyXG4gICAgcmVxdWlyZWQgPSB0cnVlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBmYjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJlQXNzZXRTZXJ2aWNlOiBTaGFyZUFzc2V0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmVBc3NldEVtYWlsU2VydmljZTogU2hhcmVBc3NldEVtYWlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIG9uRmllbGRDaGFuZ2UoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZURpYWxvZygpIHtcclxuICAgICAgICBpZiAodGhpcy5zaGFyZUFzc2V0Rm9ybS5wcmlzdGluZSkge1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlTW9kYWxIYW5kbGVyLmVtaXQodHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICcyNSUnLFxyXG4gICAgICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gY2xvc2U/JyxcclxuICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlTW9kYWxIYW5kbGVyLmVtaXQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9wZXJ0eShwcm9qZWN0RGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb3BlcnR5KHByb2plY3REYXRhLCBwcm9wZXJ0eUlkKTtcclxuICAgIH1cclxuXHJcbiAgICBzaGFyZUFzc2V0KCkge1xyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCd0bycpLm1hcmtBc1RvdWNoZWQoKTtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnZnJvbScpLm1hcmtBc1RvdWNoZWQoKTtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnY2MnKS5tYXJrQXNUb3VjaGVkKCk7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ3JlcGx5VG8nKS5tYXJrQXNUb3VjaGVkKCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnNoYXJlQXNzZXRGb3JtLnZhbGlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcmVBc3NldFNlcnZpY2Uuc2hhcmVBc3NldCh0aGlzLmFzc2V0cywgdGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXRSYXdWYWx1ZSgpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmFzc2V0cy5sZW5ndGggPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRvQWRkcmVzcyA9IHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCd0bycpLnZhbHVlLmpvaW4oJywgJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZUlkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLmFzc2V0c1swXSwgRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfSVRFTV9JRF9NRVRBREFUQUZJRUxEX0lEKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFzc2V0TmFtZSA9IHRoaXMuYXNzZXRzWzBdLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJlQXNzZXRFbWFpbFNlcnZpY2Uuc2VuZFNoYXJlQXNzZXRFbWFpbChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyRGlzcGxheU5hbWUoKSwgZGVsaXZlcmFibGVJZCwgYXNzZXROYW1lLCB0b0FkZHJlc3MpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdBc3NldChzKSBzaGFyZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VNb2RhbEhhbmRsZXIuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHNoYXJpbmcgYXNzZXRzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VNb2RhbEhhbmRsZXIuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHNoYXJpbmcgYXNzZXRzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZU1vZGFsSGFuZGxlci5lbWl0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGludGlhbGl6ZUZvcm0oKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICBzaGFyaW5nTWV0aG9kOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5zaGFyaW5nT3B0aW9uc1swXSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgdG86IG5ldyBGb3JtQ29udHJvbChbXSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFJlcXVpcmVkVmFsaWRhdG9yLnZhbGlkYXRlUmVxdWlyZWQsIEVtYWlsVmFsaWRhdG9yLnZhbGlkYXRlRW1haWxzXSksXHJcbiAgICAgICAgICAgIGZyb206IG5ldyBGb3JtQ29udHJvbChbdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckVtYWlsSWQoKV0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBSZXF1aXJlZFZhbGlkYXRvci52YWxpZGF0ZVJlcXVpcmVkLCBFbWFpbFZhbGlkYXRvci52YWxpZGF0ZUVtYWlsc10pLFxyXG4gICAgICAgICAgICBjYzogbmV3IEZvcm1Db250cm9sKFtdLCBbRW1haWxWYWxpZGF0b3IudmFsaWRhdGVFbWFpbHNdKSxcclxuICAgICAgICAgICAgcmVwbHlUbzogbmV3IEZvcm1Db250cm9sKFt0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyRW1haWxJZCgpXSwgW0VtYWlsVmFsaWRhdG9yLnZhbGlkYXRlRW1haWxzXSksXHJcbiAgICAgICAgICAgIHN1YmplY3Q6IG5ldyBGb3JtQ29udHJvbCh0aGlzLnNoYXJpbmdPcHRpb25zWzBdLmVtYWlsU3ViamVjdCwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgbWVzc2FnZTogbmV3IEZvcm1Db250cm9sKHRoaXMuc2hhcmluZ09wdGlvbnNbMF0uZW1haWxNZXNzYWdlKSxcclxuICAgICAgICAgICAgY29tcHJlc3NGaWxlczogbmV3IEZvcm1Db250cm9sKHRydWUpXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuc2hhcmVBc3NldEZvcm0uZ2V0KCdmcm9tJykuZGlzYWJsZSgpO1xyXG5cclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnc2hhcmluZ01ldGhvZCcpLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUodmFsdWUgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnc3ViamVjdCcpLnBhdGNoVmFsdWUodGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ3NoYXJpbmdNZXRob2QnKS52YWx1ZS5lbWFpbFN1YmplY3QpO1xyXG4gICAgICAgICAgICB0aGlzLnNoYXJlQXNzZXRGb3JtLmdldCgnbWVzc2FnZScpLnBhdGNoVmFsdWUodGhpcy5zaGFyZUFzc2V0Rm9ybS5nZXQoJ3NoYXJpbmdNZXRob2QnKS52YWx1ZS5lbWFpbE1lc3NhZ2UpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zaGFyZUFzc2V0Rm9ybS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pbnRpYWxpemVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==