import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareAssetsComponent } from './share-assets.component';
import { MaterialModule } from '../material.module';
import { CustomEmailFieldModule } from '../shared/components/custom-email-field/custom-email-field.module';
var ShareAssetsModule = /** @class */ (function () {
    function ShareAssetsModule() {
    }
    ShareAssetsModule = __decorate([
        NgModule({
            declarations: [
                ShareAssetsComponent
            ],
            imports: [
                CommonModule,
                MaterialModule,
                CustomEmailFieldModule
            ],
            exports: [
                ShareAssetsComponent
            ]
        })
    ], ShareAssetsModule);
    return ShareAssetsModule;
}());
export { ShareAssetsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXRzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sbUVBQW1FLENBQUM7QUFnQjNHO0lBQUE7SUFBaUMsQ0FBQztJQUFyQixpQkFBaUI7UUFkN0IsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFO2dCQUNWLG9CQUFvQjthQUN2QjtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLGNBQWM7Z0JBQ2Qsc0JBQXNCO2FBQ3pCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLG9CQUFvQjthQUN2QjtTQUNKLENBQUM7T0FFVyxpQkFBaUIsQ0FBSTtJQUFELHdCQUFDO0NBQUEsQUFBbEMsSUFBa0M7U0FBckIsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU2hhcmVBc3NldHNDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlLWFzc2V0cy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IEN1c3RvbUVtYWlsRmllbGRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tZW1haWwtZmllbGQvY3VzdG9tLWVtYWlsLWZpZWxkLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgU2hhcmVBc3NldHNDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIEN1c3RvbUVtYWlsRmllbGRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVBc3NldHNDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTaGFyZUFzc2V0c01vZHVsZSB7IH1cclxuIl19