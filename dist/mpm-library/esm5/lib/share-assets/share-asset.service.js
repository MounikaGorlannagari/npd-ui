import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { SharingService } from '../mpm-utils/services/sharing.service';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { otmmServicesConstants } from '../mpm-utils/config/otmmService.constant';
import { OTMMService } from '../mpm-utils/services/otmm.service';
import { SessionStorageConstants } from '../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../mpm-utils/services/sharing.service";
import * as i3 from "../mpm-utils/services/otmm.service";
var ShareAssetService = /** @class */ (function () {
    function ShareAssetService(http, sharingService, otmmService) {
        this.http = http;
        this.sharingService = sharingService;
        this.otmmService = otmmService;
    }
    ShareAssetService.prototype.formDownloadName = function () {
        return 'Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + '.zip';
    };
    ShareAssetService.prototype.shareAsset = function (assets, sharingDetails) {
        var _this = this;
        var assetIds = [];
        var contentTypes = [];
        var contentTypeTransformerMap = [];
        var exportJobTransformers = [{
                transformer_id: 'ARTESIA.TRANSFORMER.ZIP COMPRESSION.DEFAULT',
                arguments: [
                    null,
                    this.formDownloadName()
                ]
            }];
        var otmmVersion = sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION) != undefined ? parseFloat(JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION))) : 0;
        var to = sharingDetails.to.join(';');
        var from = sharingDetails.from.join(';');
        var cc = sharingDetails.cc.join(';');
        var replyTo = sharingDetails.replyTo.join(';');
        if (otmmVersion == 0 || otmmVersion == null) {
            this.otmmService.getOTMMSystemDetails();
            otmmVersion = sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION) != undefined ? parseFloat(sessionStorage.getItem(SessionStorageConstants.OTMM_VERSION)) : 0;
        }
        return new Observable(function (observer) {
            var baseUrl = isDevMode() ? './' : _this.sharingService.getMediaManagerConfig().url;
            var version = _this.sharingService.getMediaManagerConfig().apiVersion;
            var url = baseUrl + 'otmmapi/' + version + '/jobs/exports';
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED,
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            assets.forEach(function (asset) {
                assetIds.push(asset.asset_id);
                contentTypes.push(asset.content_type);
                contentTypeTransformerMap.push({
                    content_type: asset.content_type,
                    include_original: true,
                    include_preview: false,
                    include_sub_files: false,
                    trans_arguments: [],
                    exclusion_asset_id_list: []
                });
            });
            var selectionContext = {
                selection_context_param: {
                    selection_context: {
                        asset_ids: assetIds,
                        asssetContentType: contentTypes,
                        type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                        include_descendants: 'NONE'
                    }
                }
            };
            var exportRequest = {
                export_request_param: {
                    export_request: {
                        content_request: {
                            export_master_content: true,
                            export_preview_content: true
                        },
                        error_handling_action: 'DEFAULT_HANDLING',
                        export_job_transformers: (sharingDetails.sharingMethod.value === 'SEND_FILES' && sharingDetails.compressFiles) ? exportJobTransformers : [],
                        preserve_folder_hierarchy: false,
                        delivery_template: {
                            attribute_values: [{
                                    argument_number: 3,
                                    value: to
                                }, {
                                    argument_number: 4,
                                    value: from
                                }, {
                                    argument_number: 5,
                                    value: cc
                                }, {
                                    argument_number: 6,
                                    value: replyTo
                                }, {
                                    argument_number: otmmVersion > 22 ? 8 : 7,
                                    value: sharingDetails.subject
                                }, {
                                    argument_number: otmmVersion > 22 ? 9 : 8,
                                    value: sharingDetails.message
                                }],
                            id: sharingDetails.sharingMethod.value === 'SEND_FILES' ? 'ARTESIA.DELIVERY.EMAIL.DEFAULT' : 'ARTESIA.DELIVERY.EMAILURL.DEFAULT',
                            transformer_id: sharingDetails.sharingMethod.value === 'SEND_FILES' ? 'ARTESIA.TRANSFORMER.PROFILE.E-MAIL' : 'ARTESIA.TRANSFORMER.PROFILE.E-MAILURL'
                        },
                        write_xml: true,
                        write_csv: false
                    }
                }
            };
            var contentTypeToTransformerMap = {
                content_type_transformer_map_list: {
                    content_type_transformer_map: contentTypeTransformerMap
                }
            };
            var assetIdToTransformerMap = {
                asset_id_transformer_map_list: {
                    asset_id_transformer_map: []
                }
            };
            var params = 'selection_context=' + encodeURIComponent(JSON.stringify(selectionContext)) +
                '&export_contents=assets_and_metadata' +
                '&job_name=Assets sent via Email' +
                '&export_request=' + encodeURIComponent(JSON.stringify(exportRequest)) +
                '&content_type_to_transformers_map=' + encodeURIComponent(JSON.stringify(contentTypeToTransformerMap)) +
                '&asset_id_to_transformers_map=' + encodeURIComponent(JSON.stringify(assetIdToTransformerMap));
            _this.http.post(url, params, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ShareAssetService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: SharingService },
        { type: OTMMService }
    ]; };
    ShareAssetService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ShareAssetService_Factory() { return new ShareAssetService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.OTMMService)); }, token: ShareAssetService, providedIn: "root" });
    ShareAssetService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ShareAssetService);
    return ShareAssetService;
}());
export { ShareAssetService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zaGFyZS1hc3NldC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDdkUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNqRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQzs7Ozs7QUFNeEY7SUFFSSwyQkFDVyxJQUFnQixFQUNoQixjQUE4QixFQUM5QixXQUF3QjtRQUZ4QixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtJQUUvQixDQUFDO0lBRUwsNENBQWdCLEdBQWhCO1FBQ0ksT0FBTyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7SUFDekYsQ0FBQztJQUVELHNDQUFVLEdBQVYsVUFBVyxNQUFrQixFQUFFLGNBQW1CO1FBQWxELGlCQWdJQztRQS9IRyxJQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBTSxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQU0seUJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLElBQU0scUJBQXFCLEdBQUcsQ0FBQztnQkFDM0IsY0FBYyxFQUFFLDZDQUE2QztnQkFDN0QsU0FBUyxFQUFFO29CQUNQLElBQUk7b0JBQ0osSUFBSSxDQUFDLGdCQUFnQixFQUFFO2lCQUMxQjthQUNKLENBQUMsQ0FBQztRQUVILElBQUksV0FBVyxHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUUsQ0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBRXZMLElBQU0sRUFBRSxHQUFHLGNBQWMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLElBQU0sSUFBSSxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQU0sRUFBRSxHQUFHLGNBQWMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLElBQU0sT0FBTyxHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWpELElBQUcsV0FBVyxJQUFFLENBQUMsSUFBSSxXQUFXLElBQUcsSUFBSSxFQUFDO1lBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLEVBQUUsQ0FBQTtZQUN0QyxXQUFXLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtTQUMxSztRQUdELE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDckYsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUN2RSxJQUFNLEdBQUcsR0FBRyxPQUFPLEdBQUcsVUFBVSxHQUFDLE9BQU8sR0FBQyxlQUFlLENBQUM7WUFFekQsSUFBTSxXQUFXLEdBQUc7Z0JBQ2hCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7b0JBQ3JCLGNBQWMsRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsVUFBVTtvQkFDN0QsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTtpQkFDMUYsQ0FBQzthQUNMLENBQUM7WUFFRixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztnQkFDaEIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzlCLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN0Qyx5QkFBeUIsQ0FBQyxJQUFJLENBQUM7b0JBQzNCLFlBQVksRUFBRSxLQUFLLENBQUMsWUFBWTtvQkFDaEMsZ0JBQWdCLEVBQUUsSUFBSTtvQkFDdEIsZUFBZSxFQUFFLEtBQUs7b0JBQ3RCLGlCQUFpQixFQUFFLEtBQUs7b0JBQ3hCLGVBQWUsRUFBRSxFQUFFO29CQUNuQix1QkFBdUIsRUFBRSxFQUFFO2lCQUM5QixDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztZQUVILElBQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLHVCQUF1QixFQUFFO29CQUNyQixpQkFBaUIsRUFBRTt3QkFDZixTQUFTLEVBQUUsUUFBUTt3QkFDbkIsaUJBQWlCLEVBQUUsWUFBWTt3QkFDL0IsSUFBSSxFQUFFLHNEQUFzRDt3QkFDNUQsbUJBQW1CLEVBQUUsTUFBTTtxQkFDOUI7aUJBQ0o7YUFDSixDQUFDO1lBRUYsSUFBTSxhQUFhLEdBQUc7Z0JBQ2xCLG9CQUFvQixFQUFFO29CQUNsQixjQUFjLEVBQUU7d0JBQ1osZUFBZSxFQUFFOzRCQUNiLHFCQUFxQixFQUFFLElBQUk7NEJBQzNCLHNCQUFzQixFQUFFLElBQUk7eUJBQy9CO3dCQUNELHFCQUFxQixFQUFFLGtCQUFrQjt3QkFDekMsdUJBQXVCLEVBQUUsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssS0FBSyxZQUFZLElBQUksY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDM0kseUJBQXlCLEVBQUUsS0FBSzt3QkFDaEMsaUJBQWlCLEVBQUU7NEJBQ2YsZ0JBQWdCLEVBQUUsQ0FBQztvQ0FDZixlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLEVBQUU7aUNBQ1osRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLElBQUk7aUNBQ2QsRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLEVBQUU7aUNBQ1osRUFBRTtvQ0FDQyxlQUFlLEVBQUUsQ0FBQztvQ0FDbEIsS0FBSyxFQUFFLE9BQU87aUNBQ2pCLEVBQUU7b0NBQ0MsZUFBZSxFQUFFLFdBQVcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQ0FDeEMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxPQUFPO2lDQUNoQyxFQUFFO29DQUNDLGVBQWUsRUFBRSxXQUFXLEdBQUUsRUFBRSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBLENBQUM7b0NBQ3RDLEtBQUssRUFBRSxjQUFjLENBQUMsT0FBTztpQ0FDaEMsQ0FBQzs0QkFDRixFQUFFLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsbUNBQW1DOzRCQUNoSSxjQUFjLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUMsdUNBQXVDO3lCQUN2Sjt3QkFDRCxTQUFTLEVBQUUsSUFBSTt3QkFDZixTQUFTLEVBQUUsS0FBSztxQkFDbkI7aUJBQ0o7YUFDSixDQUFDO1lBRUYsSUFBTSwyQkFBMkIsR0FBRztnQkFDaEMsaUNBQWlDLEVBQUU7b0JBQy9CLDRCQUE0QixFQUFFLHlCQUF5QjtpQkFDMUQ7YUFDSixDQUFDO1lBRUYsSUFBTSx1QkFBdUIsR0FBRztnQkFDNUIsNkJBQTZCLEVBQUU7b0JBQzNCLHdCQUF3QixFQUFFLEVBQUU7aUJBQy9CO2FBQ0osQ0FBQztZQUVGLElBQU0sTUFBTSxHQUFHLG9CQUFvQixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdEYsc0NBQXNDO2dCQUN0QyxpQ0FBaUM7Z0JBQ2pDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3RFLG9DQUFvQyxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMkJBQTJCLENBQUMsQ0FBQztnQkFDdEcsZ0NBQWdDLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7WUFFbkcsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUM7aUJBQ25DLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBMUlnQixVQUFVO2dCQUNBLGNBQWM7Z0JBQ2pCLFdBQVc7OztJQUwxQixpQkFBaUI7UUFKN0IsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUVXLGlCQUFpQixDQThJN0I7NEJBMUpEO0NBMEpDLEFBOUlELElBOElDO1NBOUlZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEh0dHBIZWFkZXJzLCBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2hhcmVBc3NldFNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZVxyXG5cclxuICAgICkgeyB9XHJcblxyXG4gICAgZm9ybURvd25sb2FkTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiAnRG93bmxvYWRfJyArIG5ldyBEYXRlKCkudG9KU09OKCkucmVwbGFjZSgvWy06XS9nLCAnJykuc3BsaXQoJy4nKVswXSArICcuemlwJztcclxuICAgIH1cclxuXHJcbiAgICBzaGFyZUFzc2V0KGFzc2V0czogQXJyYXk8YW55Piwgc2hhcmluZ0RldGFpbHM6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGFzc2V0SWRzID0gW107XHJcbiAgICAgICAgY29uc3QgY29udGVudFR5cGVzID0gW107XHJcbiAgICAgICAgY29uc3QgY29udGVudFR5cGVUcmFuc2Zvcm1lck1hcCA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGV4cG9ydEpvYlRyYW5zZm9ybWVycyA9IFt7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybWVyX2lkOiAnQVJURVNJQS5UUkFOU0ZPUk1FUi5aSVAgQ09NUFJFU1NJT04uREVGQVVMVCcsXHJcbiAgICAgICAgICAgIGFyZ3VtZW50czogW1xyXG4gICAgICAgICAgICAgICAgbnVsbCxcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybURvd25sb2FkTmFtZSgpXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9XTtcclxuXHJcbiAgICAgICAgdmFyIG90bW1WZXJzaW9uID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVE1NX1ZFUlNJT04pICE9IHVuZGVmaW5lZCA/IHBhcnNlRmxvYXQoIEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVE1NX1ZFUlNJT04pKSApOiAwXHJcblxyXG4gICAgICAgIGNvbnN0IHRvID0gc2hhcmluZ0RldGFpbHMudG8uam9pbignOycpO1xyXG4gICAgICAgIGNvbnN0IGZyb20gPSBzaGFyaW5nRGV0YWlscy5mcm9tLmpvaW4oJzsnKTtcclxuICAgICAgICBjb25zdCBjYyA9IHNoYXJpbmdEZXRhaWxzLmNjLmpvaW4oJzsnKTtcclxuICAgICAgICBjb25zdCByZXBseVRvID0gc2hhcmluZ0RldGFpbHMucmVwbHlUby5qb2luKCc7Jyk7XHJcblxyXG4gICAgICAgIGlmKG90bW1WZXJzaW9uPT0wIHx8IG90bW1WZXJzaW9uPT0gbnVsbCl7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuZ2V0T1RNTVN5c3RlbURldGFpbHMoKVxyXG4gICAgICAgICAgICAgb3RtbVZlcnNpb24gPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTikgIT0gdW5kZWZpbmVkID8gcGFyc2VGbG9hdChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTikpIDogMFxyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICAgICAgY29uc3QgdmVyc2lvbiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuYXBpVmVyc2lvbjtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gYmFzZVVybCArICdvdG1tYXBpLycrdmVyc2lvbisnL2pvYnMvZXhwb3J0cyc7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuVVJMRU5DT0RFRCxcclxuICAgICAgICAgICAgICAgICAgICAnWC1SZXF1ZXN0ZWQtQnknOiBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy51c2VyU2Vzc2lvbklkLnRvU3RyaW5nKCksXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgYXNzZXRzLmZvckVhY2goYXNzZXQgPT4ge1xyXG4gICAgICAgICAgICAgICAgYXNzZXRJZHMucHVzaChhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50VHlwZXMucHVzaChhc3NldC5jb250ZW50X3R5cGUpO1xyXG4gICAgICAgICAgICAgICAgY29udGVudFR5cGVUcmFuc2Zvcm1lck1hcC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50X3R5cGU6IGFzc2V0LmNvbnRlbnRfdHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBpbmNsdWRlX29yaWdpbmFsOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGluY2x1ZGVfcHJldmlldzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgaW5jbHVkZV9zdWJfZmlsZXM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zX2FyZ3VtZW50czogW10sXHJcbiAgICAgICAgICAgICAgICAgICAgZXhjbHVzaW9uX2Fzc2V0X2lkX2xpc3Q6IFtdXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3Rpb25Db250ZXh0ID0ge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uX2NvbnRleHRfcGFyYW06IHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NldF9pZHM6IGFzc2V0SWRzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NzZXRDb250ZW50VHlwZTogY29udGVudFR5cGVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEuYXNzZXQuc2VsZWN0aW9uLkFzc2V0SWRzU2VsZWN0aW9uQ29udGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluY2x1ZGVfZGVzY2VuZGFudHM6ICdOT05FJ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV4cG9ydFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBleHBvcnRfcmVxdWVzdF9wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRfcmVxdWVzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X21hc3Rlcl9jb250ZW50OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3ByZXZpZXdfY29udGVudDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcl9oYW5kbGluZ19hY3Rpb246ICdERUZBVUxUX0hBTkRMSU5HJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X2pvYl90cmFuc2Zvcm1lcnM6IChzaGFyaW5nRGV0YWlscy5zaGFyaW5nTWV0aG9kLnZhbHVlID09PSAnU0VORF9GSUxFUycgJiYgc2hhcmluZ0RldGFpbHMuY29tcHJlc3NGaWxlcykgPyBleHBvcnRKb2JUcmFuc2Zvcm1lcnMgOiBbXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJlc2VydmVfZm9sZGVyX2hpZXJhcmNoeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJ5X3RlbXBsYXRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGVfdmFsdWVzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogMyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdG9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IDQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGZyb21cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGNjXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiA2LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiByZXBseVRvXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiBvdG1tVmVyc2lvbiA+IDIyID8gOCA6NyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2hhcmluZ0RldGFpbHMuc3ViamVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogb3RtbVZlcnNpb24+IDIyPyA5IDo4LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzaGFyaW5nRGV0YWlscy5tZXNzYWdlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBzaGFyaW5nRGV0YWlscy5zaGFyaW5nTWV0aG9kLnZhbHVlID09PSAnU0VORF9GSUxFUycgPyAnQVJURVNJQS5ERUxJVkVSWS5FTUFJTC5ERUZBVUxUJyA6ICdBUlRFU0lBLkRFTElWRVJZLkVNQUlMVVJMLkRFRkFVTFQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtZXJfaWQ6IHNoYXJpbmdEZXRhaWxzLnNoYXJpbmdNZXRob2QudmFsdWUgPT09ICdTRU5EX0ZJTEVTJyA/ICdBUlRFU0lBLlRSQU5TRk9STUVSLlBST0ZJTEUuRS1NQUlMJyA6ICdBUlRFU0lBLlRSQU5TRk9STUVSLlBST0ZJTEUuRS1NQUlMVVJMJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3cml0ZV94bWw6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdyaXRlX2NzdjogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBjb250ZW50VHlwZVRvVHJhbnNmb3JtZXJNYXAgPSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50X3R5cGVfdHJhbnNmb3JtZXJfbWFwX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50X3R5cGVfdHJhbnNmb3JtZXJfbWFwOiBjb250ZW50VHlwZVRyYW5zZm9ybWVyTWFwXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhc3NldElkVG9UcmFuc2Zvcm1lck1hcCA9IHtcclxuICAgICAgICAgICAgICAgIGFzc2V0X2lkX3RyYW5zZm9ybWVyX21hcF9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXRfaWRfdHJhbnNmb3JtZXJfbWFwOiBbXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gJ3NlbGVjdGlvbl9jb250ZXh0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc2VsZWN0aW9uQ29udGV4dCkpICtcclxuICAgICAgICAgICAgICAgICcmZXhwb3J0X2NvbnRlbnRzPWFzc2V0c19hbmRfbWV0YWRhdGEnICtcclxuICAgICAgICAgICAgICAgICcmam9iX25hbWU9QXNzZXRzIHNlbnQgdmlhIEVtYWlsJyArXHJcbiAgICAgICAgICAgICAgICAnJmV4cG9ydF9yZXF1ZXN0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoZXhwb3J0UmVxdWVzdCkpICtcclxuICAgICAgICAgICAgICAgICcmY29udGVudF90eXBlX3RvX3RyYW5zZm9ybWVyc19tYXA9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShjb250ZW50VHlwZVRvVHJhbnNmb3JtZXJNYXApKSArXHJcbiAgICAgICAgICAgICAgICAnJmFzc2V0X2lkX3RvX3RyYW5zZm9ybWVyc19tYXA9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShhc3NldElkVG9UcmFuc2Zvcm1lck1hcCkpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5odHRwLnBvc3QodXJsLCBwYXJhbXMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=