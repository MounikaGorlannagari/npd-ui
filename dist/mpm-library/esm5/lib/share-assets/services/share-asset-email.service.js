import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
var ShareAssetEmailService = /** @class */ (function () {
    function ShareAssetEmailService(appService) {
        this.appService = appService;
    }
    ShareAssetEmailService.prototype.sendShareAssetEmail = function (performedUserName, deliverableId, assetName, sharedToMailAddress) {
        var param = {
            performedUserName: performedUserName,
            deliverables: {
                deliverable: {
                    id: deliverableId
                }
            },
            assetName: assetName,
            sharedToMailAddress: sharedToMailAddress,
            assetVersion: ''
        };
        this.appService.invokeRequest('http://schemas.acheron.com/mpm/email/bpm/1.0', 'ShareAssetEmailHandler', param).subscribe(function (data) { return ''; });
    };
    ShareAssetEmailService.ctorParameters = function () { return [
        { type: AppService }
    ]; };
    ShareAssetEmailService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ShareAssetEmailService_Factory() { return new ShareAssetEmailService(i0.ɵɵinject(i1.AppService)); }, token: ShareAssetEmailService, providedIn: "root" });
    ShareAssetEmailService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ShareAssetEmailService);
    return ShareAssetEmailService;
}());
export { ShareAssetEmailService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmUtYXNzZXQtZW1haWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlLWFzc2V0cy9zZXJ2aWNlcy9zaGFyZS1hc3NldC1lbWFpbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQzs7O0FBS2xFO0lBRUUsZ0NBQ1MsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMzQixDQUFDO0lBRUwsb0RBQW1CLEdBQW5CLFVBQW9CLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsbUJBQW1CO1FBQ2xGLElBQU0sS0FBSyxHQUFHO1lBQ1osaUJBQWlCLG1CQUFBO1lBQ2pCLFlBQVksRUFBRTtnQkFDWixXQUFXLEVBQUU7b0JBQ1gsRUFBRSxFQUFFLGFBQWE7aUJBQ2xCO2FBQ0Y7WUFDRCxTQUFTLFdBQUE7WUFDVCxtQkFBbUIscUJBQUE7WUFDbkIsWUFBWSxFQUFFLEVBQUU7U0FDakIsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLDhDQUE4QyxFQUFFLHdCQUF3QixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEVBQUUsRUFBRixDQUFFLENBQUMsQ0FBQztJQUN2SSxDQUFDOztnQkFoQm9CLFVBQVU7OztJQUhwQixzQkFBc0I7UUFIbEMsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLHNCQUFzQixDQW9CbEM7aUNBMUJEO0NBMEJDLEFBcEJELElBb0JDO1NBcEJZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZUFzc2V0RW1haWxTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIHNlbmRTaGFyZUFzc2V0RW1haWwocGVyZm9ybWVkVXNlck5hbWUsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0TmFtZSwgc2hhcmVkVG9NYWlsQWRkcmVzcykge1xyXG4gICAgY29uc3QgcGFyYW0gPSB7XHJcbiAgICAgIHBlcmZvcm1lZFVzZXJOYW1lLFxyXG4gICAgICBkZWxpdmVyYWJsZXM6IHtcclxuICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgaWQ6IGRlbGl2ZXJhYmxlSWRcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGFzc2V0TmFtZSxcclxuICAgICAgc2hhcmVkVG9NYWlsQWRkcmVzcyxcclxuICAgICAgYXNzZXRWZXJzaW9uOiAnJ1xyXG4gICAgfTtcclxuICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KCdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vZW1haWwvYnBtLzEuMCcsICdTaGFyZUFzc2V0RW1haWxIYW5kbGVyJywgcGFyYW0pLnN1YnNjcmliZShkYXRhID0+ICcnKTtcclxuICB9XHJcbn1cclxuIl19