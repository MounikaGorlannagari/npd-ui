import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, isDevMode } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { SelectionModel } from '@angular/cdk/collections';
import { AssetService } from '../shared/services/asset.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { TaskConstants } from '../tasks/task.constants';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as acronui from '../../../lib/mpm-utils/auth/utility';
import { MPM_ROLES } from '../../mpm-utils/objects/Role';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { Observable } from 'rxjs';
import { otmmServicesConstants } from '../../mpm-utils/config/otmmService.constant';
import { QdsService } from '../../upload/services/qds.service';
var AssetsComponent = /** @class */ (function () {
    function AssetsComponent(taskService, entityAppdefService, sharingService, assetService, loaderService, qdsService, notificationService, otmmService, appService) {
        this.taskService = taskService;
        this.entityAppdefService = entityAppdefService;
        this.sharingService = sharingService;
        this.assetService = assetService;
        this.loaderService = loaderService;
        this.qdsService = qdsService;
        this.notificationService = notificationService;
        this.otmmService = otmmService;
        this.appService = appService;
        this.notificationHandler = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.assetsCallBackHandler = new EventEmitter();
        this.selectedAssetsCallbackHandler = new EventEmitter();
        this.selectAllDeliverables = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.downloadAssetCallBackHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.OrderedDisplayableFields = new EventEmitter();
        this.openCreativeReviewHandler = new EventEmitter();
        this.skip = 0;
        this.top = 100;
        this.allSelectedAssets = [];
        this.allSelectedAssetsVal = false;
        this.selectedDeliverables = new SelectionModel(true, []);
    }
    AssetsComponent.prototype.getSelectedDeliverable = function (asset) {
        var selectedAsset = this.selectedDeliverables.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
        if (!selectedAsset) {
            this.selectedDeliverables.select(asset);
        }
        this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
    };
    AssetsComponent.prototype.getUnSelectedDeliverable = function (asset) {
        var selectedAsset = this.selectedDeliverables.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
        if (selectedAsset) {
            this.selectedDeliverables.deselect(selectedAsset);
        }
        this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
    };
    /* isAllSelected() {
        return this.selectedDeliverables.selected.length === this.totalAssetsCount;
    } */
    AssetsComponent.prototype.isAllSelected = function () {
        var _this = this;
        var selectedAssets = [];
        this.assetList.forEach(function (asset) {
            var selectedAsset = _this.selectedDeliverables.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length === this.totalAssetsCount) {
            this.selectAllChecked = true;
        }
        return selectedAssets.length === this.totalAssetsCount ? true : false;
    };
    AssetsComponent.prototype.isPartialSelected = function () {
        var _this = this;
        var selectedAssets = [];
        this.assetList.forEach(function (asset) {
            var selectedAsset = _this.selectedDeliverables.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length !== this.totalAssetsCount) {
            this.selectAllChecked = false;
        }
        return selectedAssets.length > 0 && selectedAssets.length !== this.totalAssetsCount ? true : false;
    };
    AssetsComponent.prototype.masterToggle = function () {
        this.selectAllChecked ? this.checkAllDeliverables() : this.uncheckAllDeliverables();
    };
    AssetsComponent.prototype.checkAllDeliverables = function () {
        this.selectAllDeliverables.emit(true);
    };
    AssetsComponent.prototype.uncheckAllDeliverables = function () {
        this.selectAllDeliverables.emit(false);
    };
    AssetsComponent.prototype.retrieveAssetData = function (asset) {
        this.assetDetailsCallBackHandler.next(asset);
    };
    AssetsComponent.prototype.downloadAsset = function (event) {
        this.downloadAssetCallBackHandler.next(event);
    };
    AssetsComponent.prototype.retrieveAssetVersionsData = function (asset) {
        this.assetVersionsCallBackHandler.next(asset);
    };
    AssetsComponent.prototype.retrieveAssetPreviewData = function (asset) {
        this.assetPreviewCallBackHandler.next(asset);
    };
    AssetsComponent.prototype.shareAsset = function (event) {
        this.shareAssetsCallbackHandler.next(event);
    };
    AssetsComponent.prototype.download = function (asset) {
        var _this = this;
        var assetURL = this.otmmBaseUrl + asset.master_content_info.url.slice(1);
        //this.assetService.initiateDownload(assetURL, this.asset.name);
        if (otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload) {
            var downloadParams = {
                assetObjects: [{
                        id: asset.asset_id,
                        type: 'ORIGINAL'
                    }]
            };
            this.qdsService.inlineDownload(downloadParams);
        }
        else {
            var assetConfig = this.sharingService.getAssetConfig();
            var assetSize = asset.asset_content_info.master_content.content_size / 1024;
            if (assetSize < assetConfig.MIN_EXPORT_SIZE_HTTP) {
                this.assetService.initiateDownload(assetURL, asset.name);
            }
            else {
                var downloadType = asset.mime_type.split("/")[1];
                this.loaderService.show();
                this.otmmService.createExportJob([asset.asset_id], downloadType)
                    .subscribe(function (response) {
                    _this.loaderService.hide();
                    if (response && response.export_job_handle && response.export_job_handle.export_response &&
                        response.export_job_handle.export_response.exportable_count &&
                        response.export_job_handle.export_response.exportable_count > 0) {
                        _this.notificationService.info('Download has been initiated, Please check the download tray');
                    }
                }, function (error) {
                    _this.loaderService.hide();
                });
            }
        }
        // this.downloadCallBackHandler.emit({ assetURL: assetURL, assetName: this.asset.name });
    };
    AssetsComponent.prototype.openCR = function (asset) {
        var _this = this;
        var appId = this.entityAppdefService.getApplicationID();
        var deliverableId = this.taskService.getProperty(asset, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        var taskId = this.taskService.getProperty(asset, TaskConstants.TASK_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        var assetId = asset.original_asset_id;
        var teamItemId = this.projectData.R_PO_TEAM['MPM_Teams-id'].Id;
        var crActions = this.sharingService.getCRActions();
        this.taskService.getTaskById(taskId).subscribe(function (task) {
            _this.taskData = task;
            _this.taskName = _this.taskData.NAME;
        });
        if (crActions && crActions.length > 0) {
            var crAction = crActions.find(function (el) { return el['MPM_Teams-id'].Id === teamItemId; });
            if (crAction) {
                var userRoles = acronui.findObjectsByProp(crAction, 'MPM_Team_Role_Mapping');
                var isManager_1 = false;
                userRoles.forEach(function (role) {
                    var appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                    var mangerRoleObj = appRoles.find(function (appRole) {
                        return appRole.ROLE_NAME === MPM_ROLES.MANAGER;
                    });
                    if (mangerRoleObj) {
                        isManager_1 = true;
                    }
                });
                this.assetService.GetCRIsViewer(true).subscribe(function (crResponse) {
                    _this.crRole = crResponse.MPM_CR_Role;
                    _this.loaderService.show();
                    _this.getFileProofingSessionData(appId, deliverableId, assetId, _this.crRole.ROLE_ID, 'true', _this.taskName, asset)
                        .subscribe(function (crData) {
                        _this.loaderService.hide();
                        _this.crHandler.next(crData);
                    }, function (error) {
                        _this.loaderService.hide();
                        _this.notificationService.error('Something went wrong while opening Creative Review');
                    });
                });
            }
            else {
                this.notificationService.error('Something went wrong while opening Creative Review');
            }
        }
        else {
            this.notificationService.error('Something went wrong while opening Creative Review');
        }
    };
    AssetsComponent.prototype.getProperty = function (asset, property) {
        return this.taskService.getProperty(asset, property);
    };
    AssetsComponent.prototype.getFileProofingSessionData = function (appId, deliverableId, assetId, crRoleId, versioning, taskName, asset) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.getFileProofingSession(appId, deliverableId, assetId, taskName, crRoleId, versioning)
                .subscribe(function (response) {
                if (response && response.sessionDetails && response.sessionDetails.sessionUrl) {
                    _this.loaderService.hide();
                    var crData = {
                        url: response.sessionDetails.sessionUrl,
                        headerInfos: [{
                                title: 'Name',
                                value: _this.getProperty(asset, DeliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID),
                                type: 'string'
                            }]
                    };
                    console.log(crData);
                    observer.next(crData);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while opening Creative Review.');
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetsComponent.prototype.getOrderedDisplayableFields = function (columns) {
        this.OrderedDisplayableFields.next(columns);
    };
    AssetsComponent.prototype.ngOnChanges = function (changes) {
        if (changes && changes.assets && (changes.assets.previousValue !== changes.assets.currentValue)) {
            this.assetList = changes.assets.currentValue;
            this.totalAssetsCount = this.assetList.length;
        }
    };
    AssetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.assetList = this.assets;
        this.selectAllChecked = false;
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.totalAssetsCount = this.assets.length;
        if (this.selectedAssetData && this.selectedAssetData.length > 0) {
            this.selectedAssetData.forEach(function (assetId) {
                var selectedAsset = _this.allAssetData.find(function (asset) { return asset.asset_id === assetId; });
                if (selectedAsset) {
                    _this.selectedDeliverables.select(selectedAsset);
                }
            });
            if (this.selectedDeliverables.selected && this.selectedDeliverables.selected.length > 0) {
                this.selectedAssetsCallbackHandler.next(this.selectedDeliverables);
            }
        }
    };
    AssetsComponent.ctorParameters = function () { return [
        { type: TaskService },
        { type: EntityAppDefService },
        { type: SharingService },
        { type: AssetService },
        { type: LoaderService },
        { type: QdsService },
        { type: NotificationService },
        { type: OTMMService },
        { type: AppService }
    ]; };
    __decorate([
        Input()
    ], AssetsComponent.prototype, "assets", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "selectedAssetData", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "isCampaignAssetView", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "allAssetData", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "isAssetListView", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "assetDisplayableFields", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "assetConfig", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "assetCardFields", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "projectData", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "taskData", void 0);
    __decorate([
        Input()
    ], AssetsComponent.prototype, "freezeCount", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "notificationHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "loadingHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "assetDetailsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "assetVersionsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "assetsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "selectedAssetsCallbackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "selectAllDeliverables", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "shareAssetsCallbackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "assetPreviewCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "downloadAssetCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "crHandler", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "OrderedDisplayableFields", void 0);
    __decorate([
        Output()
    ], AssetsComponent.prototype, "openCreativeReviewHandler", void 0);
    AssetsComponent = __decorate([
        Component({
            selector: 'mpm-assets',
            template: "<div class=\"flex-row align-center\">\r\n    <div class=\"flex-row-item padding-left-10\" *ngIf=\"totalAssetsCount > 0 || assetList.length > 0\">\r\n        <!-- || assets.length > 0 -->\r\n        <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"selectedDeliverables.hasValue() && isAllSelected()\" [indeterminate]=\"selectedDeliverables.hasValue() && !isAllSelected()\"> Select all assets in this page\r\n        </mat-checkbox>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"assets-wrapper\">\r\n    <div class=\"assets\">\r\n        <mpm-asset-card *ngFor=\"let asset of assetList\" [asset]=\"asset\" [isVersion]=\"false\" [selectAllDeliverables]=\"selectAllDeliverables\" (assetVersionsCallBackHandler)=\"retrieveAssetVersionsData($event)\" (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\"\r\n            (selectedAssetsCallBackHandler)=\"getSelectedDeliverable(asset)\" (unSelectedAssetsCallBackHandler)=\"getUnSelectedDeliverable(asset)\" (shareAssetsCallbackHandler)=\"shareAsset($event)\" (assetPreviewCallBackHandler)=\"retrieveAssetPreviewData($event)\"\r\n            (downloadCallBackHandler)=\"downloadAsset($event)\" (openCreativeReview)=\"openCR($event)\"></mpm-asset-card>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"assets-wrapper\" *ngIf=\"isAssetListView\">\r\n    <div class=\"assets\">\r\n        <mpm-asset-list [assetConfig]=\"assetConfig\" (orderedDisplayableFields)=\"getOrderedDisplayableFields($event)\" [assets]=\"assets\" [taskData]=\"taskData\" [projectData]=\"projectData\" [assetDisplayableFields]=\"assetDisplayableFields\" [selectedAssetData]=\"selectedAssetData\"\r\n            [isVersion]=\"false\" [selectAllDeliverables]=\"selectAllDeliverables\" (shareAssetsCallbackHandler)=\"shareAsset($event)\" (openCreativeReview)=\"openCR($event)\"></mpm-asset-list>\r\n    </div>\r\n</div>",
            styles: [".align-center{align-items:center}.padding-left-10{padding-left:10px}.no-asset-found{text-align:center;font-weight:700}.assets-wrapper{padding:.5rem;position:relative;outline:0;display:flex;flex-flow:column;flex:1 0 0px;overflow-x:hidden;overflow-y:auto;height:calc(100vh - 256px);overflow:auto}.assets-wrapper .assets{display:flex;flex-wrap:wrap;margin:-.5rem;position:relative;flex-direction:row;flex-grow:1}.assets-wrapper mpm-asset-card{min-width:230px;box-sizing:border-box;position:relative;margin:.5rem;display:flex;flex-direction:column}"]
        })
    ], AssetsComponent);
    return AssetsComponent;
}());
export { AssetsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvYXNzZXRzL2Fzc2V0cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQWlCLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFFcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxLQUFLLE9BQU8sTUFBTSxxQ0FBcUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDekQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzVELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBUS9EO0lBeUNJLHlCQUNXLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxjQUE4QixFQUM5QixZQUEwQixFQUMxQixhQUE0QixFQUM1QixVQUFzQixFQUN0QixtQkFBd0MsRUFDeEMsV0FBd0IsRUFDeEIsVUFBc0I7UUFSdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFwQ3ZCLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3pDLGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsaUNBQTRCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2RCwwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ2hELGtDQUE2QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDeEQsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQztRQUNwRCwrQkFBMEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3JELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsaUNBQTRCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2RCxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJOUQsU0FBSSxHQUFHLENBQUMsQ0FBQztRQUNULFFBQUcsR0FBRyxHQUFHLENBQUM7UUFFVixzQkFBaUIsR0FBVSxFQUFFLENBQUM7UUFDOUIseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBQzdCLHlCQUFvQixHQUFHLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQWdCaEQsQ0FBQztJQUVMLGdEQUFzQixHQUF0QixVQUF1QixLQUFVO1FBQzdCLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsYUFBYSxJQUFJLE9BQUEsYUFBYSxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsUUFBUSxFQUF6QyxDQUF5QyxDQUFDLENBQUM7UUFDMUgsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNoQixJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsa0RBQXdCLEdBQXhCLFVBQXlCLEtBQVU7UUFDL0IsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLEVBQXpDLENBQXlDLENBQUMsQ0FBQztRQUMxSCxJQUFJLGFBQWEsRUFBRTtZQUNmLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDckQ7UUFDRCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7UUFFSTtJQUVKLHVDQUFhLEdBQWI7UUFBQSxpQkFZQztRQVhHLElBQU0sY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDeEIsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLEVBQXpDLENBQXlDLENBQUMsQ0FBQztZQUMxSCxJQUFJLGFBQWEsRUFBRTtnQkFDZixjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3RDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7UUFDRCxPQUFPLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMxRSxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCO1FBQUEsaUJBWUM7UUFYRyxJQUFNLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQ3hCLElBQU0sYUFBYSxHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsYUFBYSxJQUFJLE9BQUEsYUFBYSxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsUUFBUSxFQUF6QyxDQUF5QyxDQUFDLENBQUM7WUFDMUgsSUFBSSxhQUFhLEVBQUU7Z0JBQ2YsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN0QztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUNqRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1NBQ2pDO1FBQ0QsT0FBTyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkcsQ0FBQztJQUVELHNDQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUN4RixDQUFDO0lBRUQsOENBQW9CLEdBQXBCO1FBQ0ksSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsZ0RBQXNCLEdBQXRCO1FBQ0ksSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsMkNBQWlCLEdBQWpCLFVBQWtCLEtBQUs7UUFDbkIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsdUNBQWEsR0FBYixVQUFjLEtBQUs7UUFDZixJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxtREFBeUIsR0FBekIsVUFBMEIsS0FBSztRQUMzQixJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxrREFBd0IsR0FBeEIsVUFBeUIsS0FBSztRQUMxQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxvQ0FBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELGtDQUFRLEdBQVIsVUFBUyxLQUFLO1FBQWQsaUJBaUNDO1FBaENHLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0UsZ0VBQWdFO1FBQ2hFLElBQUkscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFO1lBQzFELElBQU0sY0FBYyxHQUFHO2dCQUNuQixZQUFZLEVBQUUsQ0FBQzt3QkFDWCxFQUFFLEVBQUUsS0FBSyxDQUFDLFFBQVE7d0JBQ2xCLElBQUksRUFBRSxVQUFVO3FCQUNuQixDQUFDO2FBQ0wsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ2xEO2FBQU07WUFDSCxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3pELElBQU0sU0FBUyxHQUFHLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsWUFBWSxHQUFDLElBQUksQ0FBQztZQUM1RSxJQUFHLFNBQVMsR0FBRyxXQUFXLENBQUMsb0JBQW9CLEVBQUM7Z0JBQzVDLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM1RDtpQkFBSTtnQkFDRCxJQUFNLFlBQVksR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsWUFBWSxDQUFDO3FCQUMzRCxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNuQixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsaUJBQWlCLElBQUksUUFBUSxDQUFDLGlCQUFpQixDQUFDLGVBQWU7d0JBQ3BGLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCO3dCQUMzRCxRQUFRLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGdCQUFnQixHQUFHLENBQUMsRUFBRTt3QkFDakUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyw2REFBNkQsQ0FBQyxDQUFDO3FCQUNoRztnQkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzlCLENBQUMsQ0FBQyxDQUFDO2FBQ047U0FDSjtRQUNELHlGQUF5RjtJQUM3RixDQUFDO0lBRUQsZ0NBQU0sR0FBTixVQUFPLEtBQUs7UUFBWixpQkE0Q0M7UUEzQ0csSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDMUQsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLG9CQUFvQixDQUFDLG9DQUFvQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25JLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUcsSUFBTSxPQUFPLEdBQUcsS0FBSyxDQUFDLGlCQUFpQixDQUFDO1FBQ3hDLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNqRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDNUMsS0FBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQTtRQUNSLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxLQUFLLFVBQVUsRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO1lBQzVFLElBQUksUUFBUSxFQUFFO2dCQUNWLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFDL0UsSUFBSSxXQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN0QixTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDbEIsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztvQkFDbEUsSUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87d0JBQ3ZDLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDO29CQUNuRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLGFBQWEsRUFBRTt3QkFDZixXQUFTLEdBQUcsSUFBSSxDQUFDO3FCQUNwQjtnQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxVQUFVO29CQUN0RCxLQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxXQUFXLENBQUM7b0JBQ3JDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQywwQkFBMEIsQ0FBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsS0FBSSxDQUFDLFFBQVEsRUFBQyxLQUFLLENBQUM7eUJBQzNHLFNBQVMsQ0FBQyxVQUFBLE1BQU07d0JBQ2IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2hDLENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO29CQUN6RixDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQzthQUN4RjtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7U0FDeEY7SUFDTCxDQUFDO0lBRUQscUNBQVcsR0FBWCxVQUFZLEtBQUssRUFBRSxRQUFRO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCxvREFBMEIsR0FBMUIsVUFBMkIsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUMsS0FBSztRQUE5RixpQkF3QkM7UUF2QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQztpQkFDaEcsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsY0FBYyxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFO29CQUMzRSxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFNLE1BQU0sR0FBRzt3QkFDWCxHQUFHLEVBQUUsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVO3dCQUN2QyxXQUFXLEVBQUUsQ0FBQztnQ0FDVixLQUFLLEVBQUUsTUFBTTtnQ0FDYixLQUFLLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsb0JBQW9CLENBQUMsaUNBQWlDLENBQUM7Z0NBQ3RGLElBQUksRUFBRSxRQUFROzZCQUNqQixDQUFDO3FCQUNMLENBQUM7b0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7aUJBQ3pFO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscURBQTJCLEdBQTNCLFVBQTRCLE9BQU87UUFDL0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQscUNBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQzlCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQzdGLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDN0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFBQSxpQkFnQkM7UUFmRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDN0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFDeEYsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzNDLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUNsQyxJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxRQUFRLEtBQUssT0FBTyxFQUExQixDQUEwQixDQUFDLENBQUM7Z0JBQ2xGLElBQUksYUFBYSxFQUFFO29CQUNmLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7aUJBQ25EO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNyRixJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2FBQ3RFO1NBQ0o7SUFDTCxDQUFDOztnQkFyT3VCLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUN4QixjQUFjO2dCQUNoQixZQUFZO2dCQUNYLGFBQWE7Z0JBQ2hCLFVBQVU7Z0JBQ0QsbUJBQW1CO2dCQUMzQixXQUFXO2dCQUNaLFVBQVU7O0lBaER4QjtRQUFSLEtBQUssRUFBRTttREFBUTtJQUNQO1FBQVIsS0FBSyxFQUFFOzhEQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTtnRUFBcUI7SUFDcEI7UUFBUixLQUFLLEVBQUU7eURBQWM7SUFDYjtRQUFSLEtBQUssRUFBRTs0REFBMEI7SUFDekI7UUFBUixLQUFLLEVBQUU7bUVBQXdCO0lBQ3ZCO1FBQVIsS0FBSyxFQUFFO3dEQUFhO0lBQ1o7UUFBUixLQUFLLEVBQUU7NERBQWlCO0lBQ2hCO1FBQVIsS0FBSyxFQUFFO3dEQUFhO0lBQ1o7UUFBUixLQUFLLEVBQUU7cURBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTt3REFBYTtJQUVYO1FBQVQsTUFBTSxFQUFFO2dFQUErQztJQUM5QztRQUFULE1BQU0sRUFBRTsyREFBMEM7SUFDekM7UUFBVCxNQUFNLEVBQUU7d0VBQXVEO0lBQ3REO1FBQVQsTUFBTSxFQUFFO3lFQUF3RDtJQUN2RDtRQUFULE1BQU0sRUFBRTtrRUFBaUQ7SUFDaEQ7UUFBVCxNQUFNLEVBQUU7MEVBQXlEO0lBQ3hEO1FBQVQsTUFBTSxFQUFFO2tFQUFxRDtJQUNwRDtRQUFULE1BQU0sRUFBRTt1RUFBc0Q7SUFDckQ7UUFBVCxNQUFNLEVBQUU7d0VBQXVEO0lBQ3REO1FBQVQsTUFBTSxFQUFFO3lFQUF3RDtJQUN2RDtRQUFULE1BQU0sRUFBRTtzREFBcUM7SUFDcEM7UUFBVCxNQUFNLEVBQUU7cUVBQW9EO0lBQ25EO1FBQVQsTUFBTSxFQUFFO3NFQUFxRDtJQTFCckQsZUFBZTtRQUwzQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0Qix3M0RBQXNDOztTQUV6QyxDQUFDO09BQ1csZUFBZSxDQWdSM0I7SUFBRCxzQkFBQztDQUFBLEFBaFJELElBZ1JDO1NBaFJZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzLCBpc0Rldk1vZGUsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHsgQXNzZXRTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL2Fzc2V0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uL3Rhc2tzL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVGFza0NvbnN0YW50cyB9IGZyb20gJy4uL3Rhc2tzL3Rhc2suY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgTVBNX1JPTEVTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUm9sZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1hc3NldHMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Fzc2V0cy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hc3NldHMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXNzZXRzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIGFzc2V0cztcclxuICAgIEBJbnB1dCgpIHNlbGVjdGVkQXNzZXREYXRhO1xyXG4gICAgQElucHV0KCkgaXNDYW1wYWlnbkFzc2V0VmlldztcclxuICAgIEBJbnB1dCgpIGFsbEFzc2V0RGF0YTtcclxuICAgIEBJbnB1dCgpIGlzQXNzZXRMaXN0VmlldzogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIGFzc2V0RGlzcGxheWFibGVGaWVsZHM7XHJcbiAgICBASW5wdXQoKSBhc3NldENvbmZpZztcclxuICAgIEBJbnB1dCgpIGFzc2V0Q2FyZEZpZWxkcztcclxuICAgIEBJbnB1dCgpIHByb2plY3REYXRhO1xyXG4gICAgQElucHV0KCkgdGFza0RhdGE7XHJcbiAgICBASW5wdXQoKSBmcmVlemVDb3VudDtcclxuICAgIFxyXG4gICAgQE91dHB1dCgpIG5vdGlmaWNhdGlvbkhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBsb2FkaW5nSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGFzc2V0RGV0YWlsc0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGFzc2V0VmVyc2lvbnNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZEFzc2V0c0NhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNlbGVjdEFsbERlbGl2ZXJhYmxlcyA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcclxuICAgIEBPdXRwdXQoKSBzaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGFzc2V0UHJldmlld0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRvd25sb2FkQXNzZXRDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBPcmRlcmVkRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBvcGVuQ3JlYXRpdmVSZXZpZXdIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgYXNzZXRMaXN0O1xyXG4gICAgdG90YWxBc3NldHNDb3VudDtcclxuICAgIHNraXAgPSAwO1xyXG4gICAgdG9wID0gMTAwO1xyXG5cclxuICAgIGFsbFNlbGVjdGVkQXNzZXRzOiBhbnlbXSA9IFtdO1xyXG4gICAgYWxsU2VsZWN0ZWRBc3NldHNWYWwgPSBmYWxzZTtcclxuICAgIHNlbGVjdGVkRGVsaXZlcmFibGVzID0gbmV3IFNlbGVjdGlvbk1vZGVsKHRydWUsIFtdKTtcclxuICAgIHNlbGVjdEFsbENoZWNrZWQ6IGJvb2xlYW47XHJcbiAgICB0YXNrTmFtZTtcclxuICAgIGNyUm9sZTogYW55O1xyXG4gICAgb3RtbUJhc2VVcmw6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBkZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGdldFNlbGVjdGVkRGVsaXZlcmFibGUoYXNzZXQ6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdGVkLmZpbmQoc2VsZWN0ZWRBc3NldCA9PiBzZWxlY3RlZEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgaWYgKCFzZWxlY3RlZEFzc2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMuc2VsZWN0KGFzc2V0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVuU2VsZWN0ZWREZWxpdmVyYWJsZShhc3NldDogYW55KSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRBc3NldCA9IHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMuc2VsZWN0ZWQuZmluZChzZWxlY3RlZEFzc2V0ID0+IHNlbGVjdGVkQXNzZXQuYXNzZXRfaWQgPT09IGFzc2V0LmFzc2V0X2lkKTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLmRlc2VsZWN0KHNlbGVjdGVkQXNzZXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzQ2FsbGJhY2tIYW5kbGVyLm5leHQodGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogaXNBbGxTZWxlY3RlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3RlZC5sZW5ndGggPT09IHRoaXMudG90YWxBc3NldHNDb3VudDtcclxuICAgIH0gKi9cclxuXHJcbiAgICBpc0FsbFNlbGVjdGVkKCkge1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXRzID0gW107XHJcbiAgICAgICAgdGhpcy5hc3NldExpc3QuZm9yRWFjaChhc3NldCA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdGVkLmZpbmQoc2VsZWN0ZWRBc3NldCA9PiBzZWxlY3RlZEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEFzc2V0KSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZEFzc2V0cy5wdXNoKHNlbGVjdGVkQXNzZXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXRzLmxlbmd0aCA9PT0gdGhpcy50b3RhbEFzc2V0c0NvdW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0QWxsQ2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZEFzc2V0cy5sZW5ndGggPT09IHRoaXMudG90YWxBc3NldHNDb3VudCA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1BhcnRpYWxTZWxlY3RlZCgpIHtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuYXNzZXRMaXN0LmZvckVhY2goYXNzZXQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0ID0gdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlcy5zZWxlY3RlZC5maW5kKHNlbGVjdGVkQXNzZXQgPT4gc2VsZWN0ZWRBc3NldC5hc3NldF9pZCA9PT0gYXNzZXQuYXNzZXRfaWQpO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldCkge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRBc3NldHMucHVzaChzZWxlY3RlZEFzc2V0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZEFzc2V0cy5sZW5ndGggIT09IHRoaXMudG90YWxBc3NldHNDb3VudCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGVkQXNzZXRzLmxlbmd0aCA+IDAgJiYgc2VsZWN0ZWRBc3NldHMubGVuZ3RoICE9PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgbWFzdGVyVG9nZ2xlKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0QWxsQ2hlY2tlZCA/IHRoaXMuY2hlY2tBbGxEZWxpdmVyYWJsZXMoKSA6IHRoaXMudW5jaGVja0FsbERlbGl2ZXJhYmxlcygpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrQWxsRGVsaXZlcmFibGVzKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0QWxsRGVsaXZlcmFibGVzLmVtaXQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdW5jaGVja0FsbERlbGl2ZXJhYmxlcygpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcy5lbWl0KGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICByZXRyaWV2ZUFzc2V0RGF0YShhc3NldCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyLm5leHQoYXNzZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIGRvd25sb2FkQXNzZXQoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkQXNzZXRDYWxsQmFja0hhbmRsZXIubmV4dChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0cmlldmVBc3NldFZlcnNpb25zRGF0YShhc3NldCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRWZXJzaW9uc0NhbGxCYWNrSGFuZGxlci5uZXh0KGFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICByZXRyaWV2ZUFzc2V0UHJldmlld0RhdGEoYXNzZXQpIHtcclxuICAgICAgICB0aGlzLmFzc2V0UHJldmlld0NhbGxCYWNrSGFuZGxlci5uZXh0KGFzc2V0KTtcclxuICAgIH1cclxuXHJcbiAgICBzaGFyZUFzc2V0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBkb3dubG9hZChhc3NldCkge1xyXG4gICAgICAgIGNvbnN0IGFzc2V0VVJMID0gdGhpcy5vdG1tQmFzZVVybCArIGFzc2V0Lm1hc3Rlcl9jb250ZW50X2luZm8udXJsLnNsaWNlKDEpO1xyXG4gICAgICAgIC8vdGhpcy5hc3NldFNlcnZpY2UuaW5pdGlhdGVEb3dubG9hZChhc3NldFVSTCwgdGhpcy5hc3NldC5uYW1lKTtcclxuICAgICAgICBpZiAob3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQpIHtcclxuICAgICAgICAgICAgY29uc3QgZG93bmxvYWRQYXJhbXMgPSB7XHJcbiAgICAgICAgICAgICAgICBhc3NldE9iamVjdHM6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IGFzc2V0LmFzc2V0X2lkLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdPUklHSU5BTCdcclxuICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5pbmxpbmVEb3dubG9hZChkb3dubG9hZFBhcmFtcyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgYXNzZXRDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFzc2V0Q29uZmlnKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFzc2V0U2l6ZSA9IGFzc2V0LmFzc2V0X2NvbnRlbnRfaW5mby5tYXN0ZXJfY29udGVudC5jb250ZW50X3NpemUvMTAyNDtcclxuICAgICAgICAgICAgaWYoYXNzZXRTaXplIDwgYXNzZXRDb25maWcuTUlOX0VYUE9SVF9TSVpFX0hUVFApe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hc3NldFNlcnZpY2UuaW5pdGlhdGVEb3dubG9hZChhc3NldFVSTCwgYXNzZXQubmFtZSk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZG93bmxvYWRUeXBlID0gYXNzZXQubWltZV90eXBlLnNwbGl0KFwiL1wiKVsxXTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmNyZWF0ZUV4cG9ydEpvYihbYXNzZXQuYXNzZXRfaWRdLCBkb3dubG9hZFR5cGUpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuZXhwb3J0X2pvYl9oYW5kbGUgJiYgcmVzcG9uc2UuZXhwb3J0X2pvYl9oYW5kbGUuZXhwb3J0X3Jlc3BvbnNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmV4cG9ydF9qb2JfaGFuZGxlLmV4cG9ydF9yZXNwb25zZS5leHBvcnRhYmxlX2NvdW50ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmV4cG9ydF9qb2JfaGFuZGxlLmV4cG9ydF9yZXNwb25zZS5leHBvcnRhYmxlX2NvdW50ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnRG93bmxvYWQgaGFzIGJlZW4gaW5pdGlhdGVkLCBQbGVhc2UgY2hlY2sgdGhlIGRvd25sb2FkIHRyYXknKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHRoaXMuZG93bmxvYWRDYWxsQmFja0hhbmRsZXIuZW1pdCh7IGFzc2V0VVJMOiBhc3NldFVSTCwgYXNzZXROYW1lOiB0aGlzLmFzc2V0Lm5hbWUgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNSKGFzc2V0KSB7XHJcbiAgICAgICAgY29uc3QgYXBwSWQgPSB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0QXBwbGljYXRpb25JRCgpO1xyXG4gICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlSWQgPSB0aGlzLnRhc2tTZXJ2aWNlLmdldFByb3BlcnR5KGFzc2V0LCBEZWxpdmVyYWJsZUNvbnN0YW50cy5ERUxJVkVSQUJMRV9JVEVNX0lEX01FVEFEQVRBRklFTERfSUQpLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgY29uc3QgdGFza0lkID0gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eShhc3NldCwgVGFza0NvbnN0YW50cy5UQVNLX0lURU1fSURfTUVUQURBVEFGSUVMRF9JRCkuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICBjb25zdCBhc3NldElkID0gYXNzZXQub3JpZ2luYWxfYXNzZXRfaWQ7XHJcbiAgICAgICAgY29uc3QgdGVhbUl0ZW1JZCA9IHRoaXMucHJvamVjdERhdGEuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZDtcclxuICAgICAgICBjb25zdCBjckFjdGlvbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENSQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0VGFza0J5SWQodGFza0lkKS5zdWJzY3JpYmUoKHRhc2spPT57XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tEYXRhID0gdGFzaztcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza05hbWUgPSB0aGlzLnRhc2tEYXRhLk5BTUU7XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICBpZiAoY3JBY3Rpb25zICYmIGNyQWN0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNyQWN0aW9uID0gY3JBY3Rpb25zLmZpbmQoZWwgPT4gZWxbJ01QTV9UZWFtcy1pZCddLklkID09PSB0ZWFtSXRlbUlkKTtcclxuICAgICAgICAgICAgaWYgKGNyQWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1c2VyUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKGNyQWN0aW9uLCAnTVBNX1RlYW1fUm9sZV9NYXBwaW5nJyk7XHJcbiAgICAgICAgICAgICAgICBsZXQgaXNNYW5hZ2VyID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB1c2VyUm9sZXMuZm9yRWFjaChyb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBSb2xlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aocm9sZSwgJ01QTV9BUFBfUm9sZXMnKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtYW5nZXJSb2xlT2JqID0gYXBwUm9sZXMuZmluZChhcHBSb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFwcFJvbGUuUk9MRV9OQU1FID09PSBNUE1fUk9MRVMuTUFOQUdFUjtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobWFuZ2VyUm9sZU9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc01hbmFnZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuYXNzZXRTZXJ2aWNlLkdldENSSXNWaWV3ZXIodHJ1ZSkuc3Vic2NyaWJlKGNyUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY3JSb2xlID0gY3JSZXNwb25zZS5NUE1fQ1JfUm9sZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0RmlsZVByb29maW5nU2Vzc2lvbkRhdGEoYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIHRoaXMuY3JSb2xlLlJPTEVfSUQsICd0cnVlJywgdGhpcy50YXNrTmFtZSxhc3NldClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjckRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JIYW5kbGVyLm5leHQoY3JEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgb3BlbmluZyBDcmVhdGl2ZSBSZXZpZXcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgb3BlbmluZyBDcmVhdGl2ZSBSZXZpZXcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgb3BlbmluZyBDcmVhdGl2ZSBSZXZpZXcnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvcGVydHkoYXNzZXQsIHByb3BlcnR5KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGFza1NlcnZpY2UuZ2V0UHJvcGVydHkoYXNzZXQsIHByb3BlcnR5KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWxlUHJvb2ZpbmdTZXNzaW9uRGF0YShhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgY3JSb2xlSWQsIHZlcnNpb25pbmcsIHRhc2tOYW1lLGFzc2V0KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0RmlsZVByb29maW5nU2Vzc2lvbihhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgdGFza05hbWUsIGNyUm9sZUlkLCB2ZXJzaW9uaW5nKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnNlc3Npb25EZXRhaWxzICYmIHJlc3BvbnNlLnNlc3Npb25EZXRhaWxzLnNlc3Npb25VcmwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY3JEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiByZXNwb25zZS5zZXNzaW9uRGV0YWlscy5zZXNzaW9uVXJsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVySW5mb3M6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdOYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRQcm9wZXJ0eShhc3NldCwgRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfTkFNRV9NRVRBREFUQUZJRUxEX0lEKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnc3RyaW5nJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coY3JEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChjckRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBvcGVuaW5nIENyZWF0aXZlIFJldmlldy4nKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3JkZXJlZERpc3BsYXlhYmxlRmllbGRzKGNvbHVtbnMpe1xyXG4gICAgICAgIHRoaXMuT3JkZXJlZERpc3BsYXlhYmxlRmllbGRzLm5leHQoY29sdW1ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuYXNzZXRzICYmIChjaGFuZ2VzLmFzc2V0cy5wcmV2aW91c1ZhbHVlICE9PSBjaGFuZ2VzLmFzc2V0cy5jdXJyZW50VmFsdWUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXNzZXRMaXN0ID0gY2hhbmdlcy5hc3NldHMuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPSB0aGlzLmFzc2V0TGlzdC5sZW5ndGg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRMaXN0ID0gdGhpcy5hc3NldHM7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RBbGxDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5vdG1tQmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJy4vJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG4gICAgICAgIHRoaXMudG90YWxBc3NldHNDb3VudCA9IHRoaXMuYXNzZXRzLmxlbmd0aDtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZEFzc2V0RGF0YSAmJiB0aGlzLnNlbGVjdGVkQXNzZXREYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0RGF0YS5mb3JFYWNoKGFzc2V0SWQgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRBc3NldCA9IHRoaXMuYWxsQXNzZXREYXRhLmZpbmQoYXNzZXQgPT4gYXNzZXQuYXNzZXRfaWQgPT09IGFzc2V0SWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdChzZWxlY3RlZEFzc2V0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVzLnNlbGVjdGVkICYmIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMuc2VsZWN0ZWQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxiYWNrSGFuZGxlci5uZXh0KHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==