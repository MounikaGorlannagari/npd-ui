import { __decorate } from "tslib";
import { Component, EventEmitter, Input, isDevMode, OnInit, Output, Renderer2, SimpleChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UtilService } from '../../mpm-utils/services/util.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import * as $ from 'jquery';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SelectionModel } from '@angular/cdk/collections';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../notification/notification.service';
import { LoaderService } from '../../loader/loader.service';
import { AssetService } from '../shared/services/asset.service';
import { QdsService } from '../../upload/services/qds.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { Observable } from 'rxjs';
import { DeliverableService } from '../tasks/deliverable/deliverable.service';
import { AppService } from '../../mpm-utils/services/app.service';
var AssetListComponent = /** @class */ (function () {
    function AssetListComponent(utilService, renderer, fieldConfigService, sharingService, otmmService, notificationService, loaderService, qdsService, entityAppdefService, taskService, assetService, deliverableService, appService) {
        var _this = this;
        this.utilService = utilService;
        this.renderer = renderer;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.qdsService = qdsService;
        this.entityAppdefService = entityAppdefService;
        this.taskService = taskService;
        this.assetService = assetService;
        this.deliverableService = deliverableService;
        this.appService = appService;
        this.resizeDisplayableFields = new EventEmitter();
        this.selectedAssetsCallBackHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.openCreativeReview = new EventEmitter();
        this.downloadHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.unSelectedAssetsCallBackHandler = new EventEmitter();
        this.assetListMasterCheckboxHandler = new EventEmitter();
        // selection = new SelectionModel<any>(true, []);
        this.selectedAssets = new SelectionModel(true, []);
        this.displayableMetadataFields = [];
        this.mpmFieldConstants = MPMFieldConstants;
        this.assetDataDataSource = new MatTableDataSource([]);
        this.assetsData = [];
        this.selectAllChecked = false;
        this.assetDescription = "";
        this.onClickEdit = function (rowData) {
            _this.currentAsset = rowData;
            _this.assetDescription = "";
        };
    }
    AssetListComponent.prototype.onResizeMouseDown = function (event, column) {
        event.stopPropagation();
        event.preventDefault();
        var start = event.target;
        var pressed = true;
        var startX = event.x;
        var startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    };
    AssetListComponent.prototype.initResizableColumns = function (start, pressed, startX, startWidth, column) {
        var _this = this;
        this.renderer.listen('body', 'mousemove', function (event) {
            if (pressed) {
                var width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', function (event) {
            if (pressed) {
                pressed = false;
                _this.resizeDisplayableFields.next();
            }
        });
    };
    AssetListComponent.prototype.isSelectedAll = function () {
        var _this = this;
        var selectedAssets = [];
        this.assetList.forEach(function (asset) {
            var selectedAsset = _this.selectedAssets.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length === this.totalAssetsCount) {
            this.selectAllChecked = true;
        }
        return selectedAssets.length === this.totalAssetsCount ? true : false;
    };
    AssetListComponent.prototype.isSelectedPartial = function () {
        var _this = this;
        var selectedAssets = [];
        this.assetList.forEach(function (asset) {
            var selectedAsset = _this.selectedAssets.selected.find(function (selectedAsset) { return selectedAsset.asset_id === asset.asset_id; });
            if (selectedAsset) {
                selectedAssets.push(selectedAsset);
            }
        });
        if (selectedAssets.length !== this.totalAssetsCount) {
            this.selectAllChecked = false;
        }
        return selectedAssets.length > 0 && selectedAssets.length !== this.totalAssetsCount ? true : false;
    };
    AssetListComponent.prototype.masterToggle = function () {
        this.selectAllAssetChecked ? this.checkAllProjects() : this.uncheckAllProjects();
    };
    AssetListComponent.prototype.checkAllProjects = function () {
        var _this = this;
        this.assetDataDataSource.data.forEach(function (row) {
            row.selected = true;
            _this.selectedAssets.select(row);
            _this.selectedAssetsCallBackHandler.emit(row);
        });
    };
    AssetListComponent.prototype.uncheckAllProjects = function () {
        var _this = this;
        this.assetDataDataSource.data.forEach(function (row) {
            row.selected = false;
            _this.selectedAssets.deselect(row);
            _this.unSelectedAssetsCallBackHandler.emit(row);
        });
    };
    AssetListComponent.prototype.checkDeliverable = function (AssetData, event) {
        if (event.checked) {
            AssetData.selected = true;
            this.selectedAssets.select(AssetData);
            this.selectedAssetsCallBackHandler.emit(AssetData);
        }
        else {
            this.selectedAssets.deselect(AssetData);
            this.unSelectedAssetsCallBackHandler.emit(AssetData);
        }
    };
    AssetListComponent.prototype.loadingHandler = function (event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    };
    AssetListComponent.prototype.notificationHandler = function (event) {
        if (event.message) {
            this.notificationService.error(event.message);
        }
    };
    AssetListComponent.prototype.onClickMoreActions = function (asset) {
        this.selectedAsset = asset;
    };
    AssetListComponent.prototype.updateDescription = function () {
        var _this = this;
        this.loadingHandler(true);
        var assetId = this.currentAsset.asset_id;
        var body = {
            "edited_asset": {
                "data": {
                    "asset_identifier": "string",
                    "metadata": [
                        {
                            "id": "ARTESIA.FIELD.ASSET DESCRIPTION",
                            "name": "Description",
                            "type": "com.artesia.metadata.MetadataField",
                            "value": {
                                "value": {
                                    "type": "string",
                                    "value": this.assetDescription
                                }
                            }
                        }
                    ]
                }
            }
        };
        this.otmmService.updateMetadata(assetId, 'Asset', body).subscribe(function (res) {
            _this.notificationService.success('metadata Successfully updated');
        }, function (error) {
            _this.notificationService.error('Something Went Wrong While Updating the Metadata');
        });
        this.loadingHandler(false);
    };
    AssetListComponent.prototype.ngOnChanges = function (changes) {
        if (changes.assetDisplayableFields && !changes.assetDisplayableFields.firstChange &&
            (JSON.stringify(changes.assetDisplayableFields.currentValue) !== JSON.stringify(changes.assetDisplayableFields.previousValue))) {
            this.refreshData();
        }
        if (changes && changes.assets && (changes.assets.previousValue !== changes.assets.currentValue)) {
            this.assetList = changes.assets.currentValue;
            this.totalAssetsCount = this.assetList.length;
        }
        if (changes.assets) {
            this.assetDataDataSource.data = [];
            this.assets.forEach(function (eachAsset) {
                if (eachAsset.inherited_metadata_collections && eachAsset.inherited_metadata_collections[0]) {
                    eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list.concat(eachAsset.inherited_metadata_collections[0].inherited_metadata_values);
                }
                else {
                    eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list;
                }
            });
            this.assetDataDataSource.data = this.assets;
        }
    };
    AssetListComponent.prototype.findDocType = function (fileName) {
        return this.assetService.findIconByName(fileName);
    };
    AssetListComponent.prototype.refreshData = function () {
        var _this = this;
        this.assetDataDataSource.data = [];
        this.assets.forEach(function (eachAsset) {
            var _a, _b;
            if (eachAsset.inherited_metadata_collections && eachAsset.inherited_metadata_collections[0]) {
                eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list.concat((_b = (_a = eachAsset) === null || _a === void 0 ? void 0 : _a.inherited_metadata_collections[0]) === null || _b === void 0 ? void 0 : _b.inherited_metadata_values);
            }
            else {
                eachAsset.allMetadata = eachAsset.metadata.metadata_element_list[0].metadata_element_list;
            }
        });
        this.assetDataDataSource.data = this.assets;
        this.displayableMetadataFields = this.assetConfig.assetViewConfig ?
            this.utilService.getDisplayOrderData(this.assetDisplayableFields, this.assetConfig.assetViewConfig.listFieldOrder) : [];
        this.displayColumns = this.displayableMetadataFields.map(function (column) { return column.INDEXER_FIELD_ID; });
        this.displayColumns.unshift('Image');
        this.displayColumns.unshift('Select');
        this.displayColumns.push('Actions');
        var orderedFields = [];
        this.displayColumns.forEach(function (column) {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(_this.assetDisplayableFields.find(function (displayableField) { return displayableField.INDEXER_FIELD_ID === column; }));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    };
    AssetListComponent.prototype.download = function (asset) {
        this.downloadHandler.next(asset);
    };
    AssetListComponent.prototype.getDeliverableDetails = function (deliverableId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.deliverableService.readDeliverable(deliverableId)
                .subscribe(function (response) {
                observer.next(response.Deliverable);
                observer.complete();
            }, function (error) {
                observer.error();
            });
        });
    };
    AssetListComponent.prototype.getAssetVersions = function () {
        var _this = this;
        this.otmmService.getAssetVersionsByAssetId(this.selectedAsset.asset_id).subscribe(function (response) {
            _this.assetVersionsCallBackHandler.next(response[0]);
        });
    };
    AssetListComponent.prototype.getAssetDetails = function () {
        this.assetDetailsCallBackHandler.next(this.selectedAsset);
    };
    AssetListComponent.prototype.getAssetPreview = function () {
        this.assetPreviewCallBackHandler.next(this.selectedAsset);
    };
    AssetListComponent.prototype.getProperty = function (asset, property) {
        return this.taskService.getProperty(asset, property);
    };
    AssetListComponent.prototype.getFileProofingSessionData = function (asset, appId, deliverableId, assetId, crRoleId, versioning, taskName) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.getFileProofingSession(appId, deliverableId, assetId, taskName, crRoleId, versioning)
                .subscribe(function (response) {
                if (response && response.sessionDetails && response.sessionDetails.sessionUrl) {
                    _this.loaderService.hide();
                    var crData = {
                        url: response.sessionDetails.sessionUrl,
                        headerInfos: [{
                                title: 'Name',
                                value: _this.getProperty(asset, DeliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID),
                                type: 'string'
                            }]
                    };
                    observer.next(crData);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while opening Creative Review.');
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetListComponent.prototype.openCR = function (asset) {
        this.openCreativeReview.next(asset);
    };
    AssetListComponent.prototype.getColumnProperty = function (column, data) {
        for (var row = 0; row < data.length; row++) {
            if (column.OTMM_FIELD_ID === data[row].id) {
                if (data[row].value && data[row].value.value && data[row].value.value.value) {
                    return data[row].value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value) {
                    return data[row].metadata_element.value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.field_value) {
                    return data[row].metadata_element.value.value.field_value.value;
                }
            }
        }
        return 'NA';
    };
    AssetListComponent.prototype.shareAsset = function (asset) {
        this.shareAssetsCallbackHandler.next([asset]);
    };
    AssetListComponent.prototype.ngOnInit = function () {
        this.assetList = this.assets;
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.displayColumns = this.assetDisplayableFields.map(function (column) { return column.INDEXER_FIELD_ID; });
        this.crActionData = this.sharingService.getCRActions();
        this.refreshData();
    };
    AssetListComponent.ctorParameters = function () { return [
        { type: UtilService },
        { type: Renderer2 },
        { type: FieldConfigService },
        { type: SharingService },
        { type: OTMMService },
        { type: NotificationService },
        { type: LoaderService },
        { type: QdsService },
        { type: EntityAppDefService },
        { type: TaskService },
        { type: AssetService },
        { type: DeliverableService },
        { type: AppService }
    ]; };
    __decorate([
        Input()
    ], AssetListComponent.prototype, "selectedAssetData", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "isVersion", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "selectAllDeliverables", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "assets", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "assetDisplayableFields", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "assetConfig", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "projectData", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "taskData", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "isAllSelected", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "isAssetListView", void 0);
    __decorate([
        Input()
    ], AssetListComponent.prototype, "isPartialSelected", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "resizeDisplayableFields", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "selectedAssetsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "crHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "shareAssetsCallbackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "orderedDisplayableFields", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "openCreativeReview", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "downloadHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "assetPreviewCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "assetDetailsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "assetVersionsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "unSelectedAssetsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetListComponent.prototype, "assetListMasterCheckboxHandler", void 0);
    AssetListComponent = __decorate([
        Component({
            selector: 'mpm-asset-list',
            template: "<table *ngIf=\"displayableMetadataFields.length > 0 && assetDataDataSource.data.length > 0\" class=\"project-list\" mat-table [dataSource]=\"assetDataDataSource.data\" matSort multiTemplateDataRows cdkDropList cdkDropListOrientation=\"horizontal\">\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"selectAllAssetChecked\" (change)=\"$event ? masterToggle() : null\" [checked]=\"isSelectedAll()\" [indeterminate]=\"isSelectedPartial()\">\r\n            </mat-checkbox>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-checkbox color=\"primary\" [(ngModel)]=\"row.selected\" (click)=\"$event.stopPropagation()\" (change)=\"checkDeliverable(row,$event)\" [checked]=\"selectedAssets.isSelected(row)\">\r\n            </mat-checkbox>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Image\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <span>Image</span>\r\n            <span class=\"ui-column-resizer\" (click)=\"$event.stopPropagation()\">\r\n                <mat-icon cdkDragHandle>more_vert</mat-icon>\r\n            </span>\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div *ngIf=\"row.rendition_content && row.rendition_content.thumbnail_content && row.rendition_content.thumbnail_content.url\">\r\n                <div class=\"asset-holder\">\r\n                    <img src=\"{{row.rendition_content.thumbnail_content.url}}\" alt=\"asset\" style=\"height: 50px;\">\r\n                </div>\r\n            </div>\r\n            <div *ngIf=\"!row.rendition_content || !row.rendition_content.thumbnail_content\">\r\n                <div class=\"asset-holder\">\r\n                    <mat-icon style=\"height: 50px;\" matTooltip=\"{{row.name}}\">\r\n                        {{findDocType(row.name)}}\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields;\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\" cdkDrag [style.min-width.px]=\"column.width\">\r\n            <span>{{column.DISPLAY_NAME}}</span>\r\n            <span class=\"ui-column-resizer\" (click)=\"$event.stopPropagation()\" (mousedown)=\"onResizeMouseDown($event, column)\">\r\n                <mat-icon cdkDragHandle>more_vert</mat-icon>\r\n            </span>\r\n        </th>\r\n\r\n        <td mat-cell *matCellDef=\"let row\" [style.max-width.px]=\"column.width\">\r\n            <span>\r\n            <div >\r\n                <span *ngIf=\"column.DATA_TYPE ==='dateTime' && getColumnProperty(column,row.allMetadata) != 'NA'\">{{getColumnProperty(column,row.allMetadata) | date:'yyyy/MM/dd'}}</span>\r\n            <span *ngIf=\"column.DATA_TYPE ==='dateTime' && getColumnProperty(column,row.allMetadata) === 'NA'\">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <span *ngIf=\"column.MAPPER_NAME !='DESCRIPTION' && column.DATA_TYPE !='dateTime' \">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <span *ngIf=\"column.MAPPER_NAME==='DESCRIPTION'\" class=\"asset-description\">{{getColumnProperty(column,row.allMetadata)}}</span>\r\n            <button *ngIf=\"column.MAPPER_NAME==='DESCRIPTION'\" mat-icon-button (click)=\"onClickEdit(row)\" [matMenuTriggerFor]=\"DescriptionEditor\">\r\n                    <mat-icon matTooltip=\"Edit Asset Description\">edit</mat-icon>\r\n                </button>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div class=\"min-width-action flex-card\">\r\n                <button mat-icon-button matTooltip=\"open creative review\" (click)=\"openCR(row)\">\r\n                    <mat-icon>rate_review</mat-icon>\r\n                </button>\r\n                <button mat-icon-button matTooltip=\"download asset\" (click)=\"download(row)\">\r\n                    <mat-icon>get_app</mat-icon>\r\n                </button>\r\n\r\n                <button mat-icon-button matTooltip=\"share asset\" (click)=\"shareAsset(row)\">\r\n                    <mat-icon>share</mat-icon>\r\n                </button>\r\n                <button mat-icon-button color=\"primary\" matTooltip=\"More Actions\" (click)=\"onClickMoreActions(row)\" [matMenuTriggerFor]=\"AssetMenu\">\r\n                    <mat-icon matSuffix>more_vert</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns sticky: true\"></tr>\r\n    <tr mat-row *matRowDef=\"let row; columns:displayColumns;\"></tr>\r\n\r\n</table>\r\n\r\n<div class=\"pagination\">\r\n    <button color=\"primary\" *ngIf=\"displayableMetadataFields.length === 0\" disabled mat-flat-button>No Columns Configured</button>\r\n</div>\r\n\r\n<mat-menu #DescriptionEditor=\"matMenu\">\r\n    <div>\r\n        <mat-form-field class=\"description-edit-field\" appearance=\"fill\">\r\n            <mat-label>Asset Description</mat-label>\r\n            <input matInput type=\"text\" placeholder=\"Enter Asset Description\" style=\"border: 2px;\" (click)=\"$event.stopPropagation()\" [(ngModel)]=\"assetDescription\">\r\n            <button matSuffix mat-icon-button aria-label=\"Clear\">\r\n              <mat-icon>close</mat-icon>\r\n            </button>\r\n            <button matSuffix mat-icon-button aria-label=\"Clear\" (click)=\"updateDescription()\">\r\n                <mat-icon>save</mat-icon>\r\n            </button>\r\n        </mat-form-field>\r\n    </div>\r\n</mat-menu>\r\n\r\n<mat-menu #AssetMenu=\"matMenu\">\r\n    <button mat-menu-item (click)=\"getAssetDetails()\">\r\n        <mat-icon>info</mat-icon>\r\n        <span>View Details</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetVersions()\">\r\n        <mat-icon>collections</mat-icon>\r\n        <span>View Versions</span>\r\n    </button>\r\n    <button mat-menu-item *ngIf=\"((selectedAsset && selectedAsset.rendition_content && selectedAsset.rendition_content.preview_content !==undefined && selectedAsset.rendition_content.preview_content.mime_type !='audio/mpeg')||\r\n       (selectedAsset && selectedAsset.rendition_content && selectedAsset.rendition_content.pdf_preview_content !==undefined && !selectedAsset.rendition_content.pdf_preview_content))\" (click)=\"getAssetPreview()\">\r\n        <mat-icon>remove_red_eye</mat-icon>\r\n        <span>Preview</span>\r\n    </button>\r\n</mat-menu>",
            styles: ["table.project-list{width:100%}table.project-list tr{cursor:pointer}table.project-list tr .mat-checkbox{padding:0 8px}table.project-list tr th{padding-left:10px;padding-right:10px}table.project-list tr td:last-child{padding-left:10px}table.project-list tr td span div{display:flex;align-items:center}table.project-list tr td{padding-left:15px;padding-right:10px}table.project-list tr td a.mat-button{padding:0;text-align:left}table.project-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.project-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}table.project-list tr td mat-progress-bar{max-width:7em}.pagination{width:85vw;display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;align-items:center;margin-bottom:20px;margin-top:20px}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.project-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.project-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.project-detail{padding:0!important}tr.project-detail-row{height:0}.project-element-row td{border-bottom-width:0}.project-element-detail{overflow:hidden;display:flex;flex-wrap:wrap;cursor:auto!important}.project-element-diagram{min-width:80px;padding:8px;font-weight:lighter;margin:8px 0}.task-info{min-width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex}.task-info.data{justify-content:left}.task-info.no-data{justify-content:center}td.wrap-text-custom span>div{max-width:12em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:9em}.min-width-action ::ng-deep .mat-icon-button{max-width:30px!important}.mattooltipclass{background-color:red;margin-left:70px}.campaignLink:hover{cursor:pointer;-webkit-text-decoration-line:underline;text-decoration-line:underline}::ng-deep .mat-sort-header-button{position:unset!important}th.mat-header-cell:hover .ui-column-resizer{display:block}.ui-column-resizer{display:none;position:absolute;right:0;padding:0;cursor:col-resize}.cdk-drag-preview{box-sizing:border-box;position:relative;padding-top:20px;white-space:nowrap}.cdk-drag-preview::after{content:\"\";position:absolute;top:0;bottom:0;left:0;right:0;border-radius:4px;border:1px solid rgba(0,0,0,.4);box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{color:transparent;position:relative;transition:transform 250ms cubic-bezier(0,0,.2,1)}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.cdk-drag-placeholder::after{content:\"\";position:absolute;top:0;bottom:0;left:0;right:0;border-radius:4px;background:rgba(0,0,0,.1);border:1px dashed rgba(0,0,0,.4)}table.project-table-list{width:100rem}.asset-description{width:60px;text-overflow:ellipsis;overflow:hidden}"]
        })
    ], AssetListComponent);
    return AssetListComponent;
}());
export { AssetListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L2Fzc2V0LWxpc3QvYXNzZXQtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTdELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUMvRSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUVoRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFFNUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFbEYsT0FBTyxFQUFZLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUc5RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFRbEU7SUFFRSw0QkFDVSxXQUF3QixFQUN4QixRQUFtQixFQUNuQixrQkFBc0MsRUFDdEMsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxXQUF3QixFQUN4QixZQUEwQixFQUMxQixrQkFBc0MsRUFDdEMsVUFBc0I7UUFiaEMsaUJBY0s7UUFiSyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQzFCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQWN0Qiw0QkFBdUIsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3BELGtDQUE2QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDeEQsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDcEMsK0JBQTBCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNyRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3JELHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0Msb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0RCxpQ0FBNEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3ZELG9DQUErQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUQsbUNBQThCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUVuRSxpREFBaUQ7UUFDakQsbUJBQWMsR0FBRyxJQUFJLGNBQWMsQ0FBTSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbkQsOEJBQXlCLEdBQW9CLEVBQUUsQ0FBQztRQUVoRCxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUd0Qyx3QkFBbUIsR0FBRyxJQUFJLGtCQUFrQixDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3RELGVBQVUsR0FBRyxFQUFFLENBQUM7UUFrQmhCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFXdEIsZ0JBQVcsR0FBRyxVQUFDLE9BQU87WUFDcEIsS0FBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUM7WUFDNUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUE7SUFuRUcsQ0FBQztJQXNETCw4Q0FBaUIsR0FBakIsVUFBa0IsS0FBSyxFQUFFLE1BQU07UUFDN0IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzNCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLElBQU0sVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFPRCxpREFBb0IsR0FBcEIsVUFBcUIsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFBL0QsaUJBYUM7UUFaRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLFVBQUMsS0FBSztZQUM1QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFNLEtBQUssR0FBRyxVQUFVLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxVQUFDLEtBQUs7WUFDMUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMENBQWEsR0FBYjtRQUFBLGlCQVlEO1FBWEcsSUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztZQUN4QixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLEVBQXpDLENBQXlDLENBQUMsQ0FBQztZQUNwSCxJQUFJLGFBQWEsRUFBRTtnQkFDZixjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ3RDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDaEM7UUFDRCxPQUFPLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUM5RSxDQUFDO0lBRUQsOENBQWlCLEdBQWpCO1FBQUEsaUJBWUM7UUFYQyxJQUFNLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQ3hCLElBQU0sYUFBYSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLGFBQWEsQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsRUFBekMsQ0FBeUMsQ0FBQyxDQUFDO1lBQ3BILElBQUksYUFBYSxFQUFFO2dCQUNmLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDdEM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDakQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELE9BQU8sY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzNHLENBQUM7SUFFQyx5Q0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDbkYsQ0FBQztJQUVELDZDQUFnQixHQUFoQjtRQUFBLGlCQU9EO1FBTkcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHO1lBQ3JDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakQsQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQsK0NBQWtCLEdBQWxCO1FBQUEsaUJBT0M7UUFORyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7WUFDckMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDckIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbEMsS0FBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuRCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCw2Q0FBZ0IsR0FBaEIsVUFBaUIsU0FBUyxFQUFFLEtBQUs7UUFDL0IsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2YsU0FBUyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN0RDthQUFNO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN4RDtJQUVILENBQUM7SUFFRCwyQ0FBYyxHQUFkLFVBQWUsS0FBSztRQUNsQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2xFLENBQUM7SUFFRCxnREFBbUIsR0FBbkIsVUFBb0IsS0FBSztRQUN2QixJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDZixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7SUFFRCwrQ0FBa0IsR0FBbEIsVUFBbUIsS0FBSztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQsOENBQWlCLEdBQWpCO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDekMsSUFBSSxJQUFJLEdBQUc7WUFDVCxjQUFjLEVBQUM7Z0JBQ1osTUFBTSxFQUFDO29CQUNKLGtCQUFrQixFQUFDLFFBQVE7b0JBQzNCLFVBQVUsRUFBQzt3QkFDUjs0QkFDRyxJQUFJLEVBQUMsaUNBQWlDOzRCQUN0QyxNQUFNLEVBQUMsYUFBYTs0QkFDcEIsTUFBTSxFQUFDLG9DQUFvQzs0QkFDM0MsT0FBTyxFQUFDO2dDQUNMLE9BQU8sRUFBQztvQ0FDTCxNQUFNLEVBQUMsUUFBUTtvQ0FDZixPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtpQ0FDaEM7NkJBQ0g7eUJBQ0g7cUJBQ0g7aUJBQ0g7YUFDSDtTQUNILENBQUE7UUFDQSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUMsT0FBTyxFQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLEdBQUc7WUFDbEUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQ3BFLENBQUMsRUFBRSxVQUFDLEtBQUs7WUFDTixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7UUFDdEYsQ0FBQyxDQUFDLENBQUM7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsc0JBQXNCLElBQUksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsV0FBVztZQUM3RSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7WUFDaEksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDL0YsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztZQUM3QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7U0FDL0M7UUFDRCxJQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUM7WUFDaEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxTQUFTO2dCQUMzQixJQUFHLFNBQVMsQ0FBQyw4QkFBOEIsSUFBSSxTQUFTLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDLEVBQUM7b0JBQ3pGLFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7aUJBQ3pLO3FCQUFJO29CQUNILFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQTtpQkFDMUY7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztTQUM3QztJQUNILENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQUEsaUJBdUJDO1FBdEJDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsU0FBUzs7WUFDM0IsSUFBRyxTQUFTLENBQUMsOEJBQThCLElBQUksU0FBUyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxFQUFDO2dCQUN6RixTQUFTLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsTUFBTSxhQUFDLFNBQVMsMENBQUUsOEJBQThCLENBQUMsQ0FBQywyQ0FBRyx5QkFBeUIsQ0FBQyxDQUFDO2FBQzNLO2lCQUFJO2dCQUNILFNBQVMsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQTthQUMxRjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzVDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDeEgsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsR0FBRyxDQUFDLFVBQUMsTUFBZ0IsSUFBSyxPQUFBLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO1FBQzVHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07WUFDOUIsSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxTQUFTLENBQUMsRUFBRTtnQkFDdkUsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFVBQUEsZ0JBQWdCLElBQUksT0FBQSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQTVDLENBQTRDLENBQUMsQ0FBQyxDQUFDO2FBQzFIO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxxQ0FBUSxHQUFSLFVBQVMsS0FBSztRQUNaLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxrREFBcUIsR0FBckIsVUFBc0IsYUFBYTtRQUFuQyxpQkFVQztRQVRDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDO2lCQUNqRCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2Q0FBZ0IsR0FBaEI7UUFBQSxpQkFJQztRQUhDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ3RGLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQWUsR0FBZjtRQUNFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCw0Q0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELHdDQUFXLEdBQVgsVUFBWSxLQUFLLEVBQUUsUUFBUTtRQUN6QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsdURBQTBCLEdBQTFCLFVBQTJCLEtBQUssRUFBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFFBQVE7UUFBOUYsaUJBd0JDO1FBdkJDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUM7aUJBQ2hHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtvQkFDM0UsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBTSxNQUFNLEdBQUc7d0JBQ1gsR0FBRyxFQUFFLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVTt3QkFDdkMsV0FBVyxFQUFFLENBQUM7Z0NBQ1YsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsS0FBSyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLG9CQUFvQixDQUFDLGlDQUFpQyxDQUFDO2dDQUN0RixJQUFJLEVBQUUsUUFBUTs2QkFDakIsQ0FBQztxQkFDTCxDQUFDO29CQUVGLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO2lCQUN6RTtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG1DQUFNLEdBQU4sVUFBTyxLQUFLO1FBQ1YsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBR0QsOENBQWlCLEdBQWpCLFVBQWtCLE1BQU0sRUFBQyxJQUFJO1FBQzNCLEtBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFDO1lBQ3hDLElBQUcsTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFDO2dCQUN2QyxJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFDO29CQUN6RSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDcEM7cUJBQ0ksSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUM7b0JBQy9KLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO2lCQUNyRDtxQkFDSSxJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBQztvQkFDckssT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2lCQUNqRTthQUNGO1NBQ0Y7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCx1Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFQyxxQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUN4RixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsVUFBQyxNQUFnQixJQUFLLE9BQUEsTUFBTSxDQUFDLGdCQUFnQixFQUF2QixDQUF1QixDQUFDLENBQUM7UUFDckcsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDOztnQkEzVnNCLFdBQVc7Z0JBQ2QsU0FBUztnQkFDQyxrQkFBa0I7Z0JBQ3RCLGNBQWM7Z0JBQ2pCLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUN6QixhQUFhO2dCQUNoQixVQUFVO2dCQUNELG1CQUFtQjtnQkFDM0IsV0FBVztnQkFDVixZQUFZO2dCQUNOLGtCQUFrQjtnQkFDMUIsVUFBVTs7SUFFdkI7UUFBUixLQUFLLEVBQUU7aUVBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO3lEQUFXO0lBQ1Y7UUFBUixLQUFLLEVBQUU7cUVBQXVCO0lBQ3RCO1FBQVIsS0FBSyxFQUFFO3NEQUFRO0lBQ1A7UUFBUixLQUFLLEVBQUU7c0VBQXdCO0lBQ3ZCO1FBQVIsS0FBSyxFQUFFOzJEQUFhO0lBQ1o7UUFBUixLQUFLLEVBQUU7MkRBQWE7SUFDWjtRQUFSLEtBQUssRUFBRTt3REFBVTtJQUNUO1FBQVIsS0FBSyxFQUFFOzZEQUFlO0lBQ2Q7UUFBUixLQUFLLEVBQUU7K0RBQWlCO0lBQ2hCO1FBQVIsS0FBSyxFQUFFO2lFQUFtQjtJQUVqQjtRQUFULE1BQU0sRUFBRTt1RUFBcUQ7SUFDcEQ7UUFBVCxNQUFNLEVBQUU7NkVBQXlEO0lBQ3hEO1FBQVQsTUFBTSxFQUFFO3lEQUFxQztJQUNwQztRQUFULE1BQU0sRUFBRTswRUFBc0Q7SUFDckQ7UUFBVCxNQUFNLEVBQUU7d0VBQXNEO0lBQ3JEO1FBQVQsTUFBTSxFQUFFO2tFQUE4QztJQUM3QztRQUFULE1BQU0sRUFBRTsrREFBMkM7SUFDMUM7UUFBVCxNQUFNLEVBQUU7MkVBQXVEO0lBQ3REO1FBQVQsTUFBTSxFQUFFOzJFQUF1RDtJQUN0RDtRQUFULE1BQU0sRUFBRTs0RUFBd0Q7SUFDdkQ7UUFBVCxNQUFNLEVBQUU7K0VBQTJEO0lBQzFEO1FBQVQsTUFBTSxFQUFFOzhFQUEwRDtJQXhDeEQsa0JBQWtCO1FBTDlCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsZ3ZOQUEwQzs7U0FFM0MsQ0FBQztPQUNXLGtCQUFrQixDQWdXOUI7SUFBRCx5QkFBQztDQUFBLEFBaFdELElBZ1dDO1NBaFdZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgaXNEZXZNb2RlLCBPbkluaXQsIE91dHB1dCwgUmVuZGVyZXIyLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYmxlJztcclxuaW1wb3J0IHsgTVBNRmllbGQgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IFFkc1NlcnZpY2UgfSBmcm9tICcuLi8uLi91cGxvYWQvc2VydmljZXMvcWRzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vdGFza3MvdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVDb25zdGFudHMgfSBmcm9tICcuLi90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFza3MvdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBmb3JrSm9pbiwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNlcnZpY2UgfSBmcm9tICcuLi90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTV9ST0xFUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1JvbGUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgZm9ybWF0RGF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1hc3NldC1saXN0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vYXNzZXQtbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vYXNzZXQtbGlzdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBc3NldExpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHJpdmF0ZSBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgIHByaXZhdGUgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHFkc1NlcnZpY2U6IFFkc1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIGVudGl0eUFwcGRlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgIHByaXZhdGUgYXNzZXRTZXJ2aWNlOiBBc3NldFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGRlbGl2ZXJhYmxlU2VydmljZTogRGVsaXZlcmFibGVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlXHJcbiAgKSB7IH1cclxuICBASW5wdXQoKSBzZWxlY3RlZEFzc2V0RGF0YTtcclxuICBASW5wdXQoKSBpc1ZlcnNpb247XHJcbiAgQElucHV0KCkgc2VsZWN0QWxsRGVsaXZlcmFibGVzO1xyXG4gIEBJbnB1dCgpIGFzc2V0cztcclxuICBASW5wdXQoKSBhc3NldERpc3BsYXlhYmxlRmllbGRzO1xyXG4gIEBJbnB1dCgpIGFzc2V0Q29uZmlnO1xyXG4gIEBJbnB1dCgpIHByb2plY3REYXRhO1xyXG4gIEBJbnB1dCgpIHRhc2tEYXRhO1xyXG4gIEBJbnB1dCgpIGlzQWxsU2VsZWN0ZWQ7XHJcbiAgQElucHV0KCkgaXNBc3NldExpc3RWaWV3O1xyXG4gIEBJbnB1dCgpIGlzUGFydGlhbFNlbGVjdGVkO1xyXG5cclxuICBAT3V0cHV0KCkgcmVzaXplRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBjckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgb3JkZXJlZERpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnlbXT4oKTtcclxuICBAT3V0cHV0KCkgb3BlbkNyZWF0aXZlUmV2aWV3ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGRvd25sb2FkSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBhc3NldFByZXZpZXdDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGFzc2V0VmVyc2lvbnNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBAT3V0cHV0KCkgdW5TZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBhc3NldExpc3RNYXN0ZXJDaGVja2JveEhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgLy8gc2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPGFueT4odHJ1ZSwgW10pO1xyXG4gIHNlbGVjdGVkQXNzZXRzID0gbmV3IFNlbGVjdGlvbk1vZGVsPGFueT4odHJ1ZSwgW10pO1xyXG4gIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM6IEFycmF5PE1QTUZpZWxkPiA9IFtdO1xyXG5cclxuICBtcG1GaWVsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzO1xyXG4gIGRpc3BsYXlDb2x1bW5zO1xyXG4gIHNlbGVjdEFsbEFzc2V0Q2hlY2tlZDogYm9vbGVhbjtcclxuICBhc3NldERhdGFEYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+KFtdKTtcclxuICBhc3NldHNEYXRhID0gW107XHJcbiAgY3VycmVudEFzc2V0O1xyXG4gIGRlbGl2ZXJhYmxlSWQ7XHJcbiAgc2Vzc2lvbklkO1xyXG4gIG90bW1CYXNlVXJsO1xyXG4gIGlzT25Ib2xkUHJvamVjdDtcclxuICBwcm9qZWN0Q29tcGxldGVkU3RhdHVzO1xyXG4gIGN1cnJlbnRVc2VyQ1JEYXRhO1xyXG4gIGNyQWN0aW9uRGF0YTtcclxuICBjdXJyZW50VXNlclRlYW1Sb2xlTWFwcGluZztcclxuICBkZWxpdmVyYWJsZVN0YXR1c1xyXG4gIHRhc2tOYW1lXHJcbiAgY3JSb2xlXHJcbiAgdGFza0lkO1xyXG4gIHNlbGVjdGVkQXNzZXQ7XHJcbiAgY3VyclByb2plY3RJZDtcclxuICBhc3NldExpc3Q7XHJcbiAgdG90YWxBc3NldHNDb3VudDtcclxuICBzZWxlY3RBbGxDaGVja2VkOiBib29sZWFuID0gZmFsc2U7XHJcbiAgYXNzZXREZXNjcmlwdGlvbiA9IFwiXCI7XHJcbiAgb25SZXNpemVNb3VzZURvd24oZXZlbnQsIGNvbHVtbikge1xyXG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgY29uc3Qgc3RhcnQgPSBldmVudC50YXJnZXQ7XHJcbiAgICBjb25zdCBwcmVzc2VkID0gdHJ1ZTtcclxuICAgIGNvbnN0IHN0YXJ0WCA9IGV2ZW50Lng7XHJcbiAgICBjb25zdCBzdGFydFdpZHRoID0gJChzdGFydCkucGFyZW50KCkud2lkdGgoKTtcclxuICAgIHRoaXMuaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKTtcclxuICB9XHJcblxyXG4gIG9uQ2xpY2tFZGl0ID0gKHJvd0RhdGEpID0+IHtcclxuICAgIHRoaXMuY3VycmVudEFzc2V0ID0gcm93RGF0YTtcclxuICAgIHRoaXMuYXNzZXREZXNjcmlwdGlvbiA9IFwiXCI7XHJcbiAgfVxyXG5cclxuICBpbml0UmVzaXphYmxlQ29sdW1ucyhzdGFydCwgcHJlc3NlZCwgc3RhcnRYLCBzdGFydFdpZHRoLCBjb2x1bW4pIHtcclxuICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2Vtb3ZlJywgKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICBpZiAocHJlc3NlZCkge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHdpZHRoID0gc3RhcnRXaWR0aCArIChldmVudC54IC0gc3RhcnRYKTtcclxuICAgICAgICAgICAgICBjb2x1bW4ud2lkdGggPSB3aWR0aDtcclxuICAgICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMucmVuZGVyZXIubGlzdGVuKCdib2R5JywgJ21vdXNldXAnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgcHJlc3NlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIHRoaXMucmVzaXplRGlzcGxheWFibGVGaWVsZHMubmV4dCgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGlzU2VsZWN0ZWRBbGwoKSB7XHJcbiAgICBjb25zdCBzZWxlY3RlZEFzc2V0cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuYXNzZXRMaXN0LmZvckVhY2goYXNzZXQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZEFzc2V0ID0gdGhpcy5zZWxlY3RlZEFzc2V0cy5zZWxlY3RlZC5maW5kKHNlbGVjdGVkQXNzZXQgPT4gc2VsZWN0ZWRBc3NldC5hc3NldF9pZCA9PT0gYXNzZXQuYXNzZXRfaWQpO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRBc3NldCkge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRBc3NldHMucHVzaChzZWxlY3RlZEFzc2V0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZEFzc2V0cy5sZW5ndGggPT09IHRoaXMudG90YWxBc3NldHNDb3VudCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRBc3NldHMubGVuZ3RoID09PSB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPyB0cnVlIDogZmFsc2U7XHJcbn1cclxuXHJcbmlzU2VsZWN0ZWRQYXJ0aWFsKCkge1xyXG4gIGNvbnN0IHNlbGVjdGVkQXNzZXRzID0gW107XHJcbiAgICAgICAgdGhpcy5hc3NldExpc3QuZm9yRWFjaChhc3NldCA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXNzZXQgPSB0aGlzLnNlbGVjdGVkQXNzZXRzLnNlbGVjdGVkLmZpbmQoc2VsZWN0ZWRBc3NldCA9PiBzZWxlY3RlZEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEFzc2V0KSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZEFzc2V0cy5wdXNoKHNlbGVjdGVkQXNzZXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkQXNzZXRzLmxlbmd0aCAhPT0gdGhpcy50b3RhbEFzc2V0c0NvdW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0QWxsQ2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRBc3NldHMubGVuZ3RoID4gMCAmJiBzZWxlY3RlZEFzc2V0cy5sZW5ndGggIT09IHRoaXMudG90YWxBc3NldHNDb3VudCA/IHRydWUgOiBmYWxzZTtcclxufVxyXG5cclxuICBtYXN0ZXJUb2dnbGUoKSB7XHJcbiAgICB0aGlzLnNlbGVjdEFsbEFzc2V0Q2hlY2tlZCA/IHRoaXMuY2hlY2tBbGxQcm9qZWN0cygpIDogdGhpcy51bmNoZWNrQWxsUHJvamVjdHMoKTtcclxuICB9XHJcblxyXG4gIGNoZWNrQWxsUHJvamVjdHMoKSB7XHJcbiAgICB0aGlzLmFzc2V0RGF0YURhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiAgICAgICAgcm93LnNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzLnNlbGVjdChyb3cpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIuZW1pdChyb3cpO1xyXG4gICAgfSk7XHJcbiAgICBcclxufVxyXG5cclxudW5jaGVja0FsbFByb2plY3RzKCkge1xyXG4gICAgdGhpcy5hc3NldERhdGFEYXRhU291cmNlLmRhdGEuZm9yRWFjaChyb3cgPT4ge1xyXG4gICAgICAgIHJvdy5zZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRBc3NldHMuZGVzZWxlY3Qocm93KTtcclxuICAgICAgICB0aGlzLnVuU2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIuZW1pdChyb3cpO1xyXG4gICAgfSk7XHJcbiAgICBcclxufVxyXG5cclxuY2hlY2tEZWxpdmVyYWJsZShBc3NldERhdGEsIGV2ZW50KSB7XHJcbiAgaWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuICAgICAgQXNzZXREYXRhLnNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0cy5zZWxlY3QoQXNzZXREYXRhKTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KEFzc2V0RGF0YSk7XHJcbiAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0cy5kZXNlbGVjdChBc3NldERhdGEpO1xyXG4gICAgICB0aGlzLnVuU2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIuZW1pdChBc3NldERhdGEpO1xyXG4gIH1cclxuXHJcbn1cclxuXHJcbmxvYWRpbmdIYW5kbGVyKGV2ZW50KSB7XHJcbiAgKGV2ZW50KSA/IHRoaXMubG9hZGVyU2VydmljZS5zaG93KCkgOiB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG59XHJcblxyXG5ub3RpZmljYXRpb25IYW5kbGVyKGV2ZW50KSB7XHJcbiAgaWYgKGV2ZW50Lm1lc3NhZ2UpIHtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGV2ZW50Lm1lc3NhZ2UpO1xyXG4gIH0gXHJcbn1cclxuXHJcbm9uQ2xpY2tNb3JlQWN0aW9ucyhhc3NldCl7XHJcbiAgdGhpcy5zZWxlY3RlZEFzc2V0ID0gYXNzZXQ7XHJcbn1cclxuXHJcbnVwZGF0ZURlc2NyaXB0aW9uKCkge1xyXG4gIHRoaXMubG9hZGluZ0hhbmRsZXIodHJ1ZSk7XHJcbiAgbGV0IGFzc2V0SWQgPSB0aGlzLmN1cnJlbnRBc3NldC5hc3NldF9pZDtcclxuICBsZXQgYm9keSA9IHtcclxuICAgIFwiZWRpdGVkX2Fzc2V0XCI6e1xyXG4gICAgICAgXCJkYXRhXCI6e1xyXG4gICAgICAgICAgXCJhc3NldF9pZGVudGlmaWVyXCI6XCJzdHJpbmdcIixcclxuICAgICAgICAgIFwibWV0YWRhdGFcIjpbXHJcbiAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcImlkXCI6XCJBUlRFU0lBLkZJRUxELkFTU0VUIERFU0NSSVBUSU9OXCIsXHJcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjpcIkRlc2NyaXB0aW9uXCIsXHJcbiAgICAgICAgICAgICAgICBcInR5cGVcIjpcImNvbS5hcnRlc2lhLm1ldGFkYXRhLk1ldGFkYXRhRmllbGRcIixcclxuICAgICAgICAgICAgICAgIFwidmFsdWVcIjp7XHJcbiAgICAgICAgICAgICAgICAgICBcInZhbHVlXCI6e1xyXG4gICAgICAgICAgICAgICAgICAgICAgXCJ0eXBlXCI6XCJzdHJpbmdcIixcclxuICAgICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjogdGhpcy5hc3NldERlc2NyaXB0aW9uXHJcbiAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICBdXHJcbiAgICAgICB9XHJcbiAgICB9XHJcbiB9XHJcbiAgdGhpcy5vdG1tU2VydmljZS51cGRhdGVNZXRhZGF0YShhc3NldElkLCdBc3NldCcsYm9keSkuc3Vic2NyaWJlKChyZXMpID0+IHtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdtZXRhZGF0YSBTdWNjZXNzZnVsbHkgdXBkYXRlZCcpO1xyXG4gIH0sIChlcnJvcikgPT4ge1xyXG4gICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIFdlbnQgV3JvbmcgV2hpbGUgVXBkYXRpbmcgdGhlIE1ldGFkYXRhJyk7XHJcbiAgfSk7XHJcbiAgICB0aGlzLmxvYWRpbmdIYW5kbGVyKGZhbHNlKTtcclxufVxyXG5cclxubmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gIGlmIChjaGFuZ2VzLmFzc2V0RGlzcGxheWFibGVGaWVsZHMgJiYgIWNoYW5nZXMuYXNzZXREaXNwbGF5YWJsZUZpZWxkcy5maXJzdENoYW5nZSAmJlxyXG4gICAgICAoSlNPTi5zdHJpbmdpZnkoY2hhbmdlcy5hc3NldERpc3BsYXlhYmxlRmllbGRzLmN1cnJlbnRWYWx1ZSkgIT09IEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuYXNzZXREaXNwbGF5YWJsZUZpZWxkcy5wcmV2aW91c1ZhbHVlKSkpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gIH1cclxuICBpZiAoY2hhbmdlcyAmJiBjaGFuZ2VzLmFzc2V0cyAmJiAoY2hhbmdlcy5hc3NldHMucHJldmlvdXNWYWx1ZSAhPT0gY2hhbmdlcy5hc3NldHMuY3VycmVudFZhbHVlKSkge1xyXG4gICAgdGhpcy5hc3NldExpc3QgPSBjaGFuZ2VzLmFzc2V0cy5jdXJyZW50VmFsdWU7XHJcbiAgICB0aGlzLnRvdGFsQXNzZXRzQ291bnQgPSB0aGlzLmFzc2V0TGlzdC5sZW5ndGg7XHJcbiAgfVxyXG4gIGlmKGNoYW5nZXMuYXNzZXRzKXtcclxuICAgIHRoaXMuYXNzZXREYXRhRGF0YVNvdXJjZS5kYXRhID0gW107XHJcbiAgICB0aGlzLmFzc2V0cy5mb3JFYWNoKGVhY2hBc3NldCA9PiB7XHJcbiAgICAgIGlmKGVhY2hBc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnMgJiYgZWFjaEFzc2V0LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9uc1swXSl7XHJcbiAgICAgICAgZWFjaEFzc2V0LmFsbE1ldGFkYXRhID0gZWFjaEFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFswXS5tZXRhZGF0YV9lbGVtZW50X2xpc3QuY29uY2F0KGVhY2hBc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnNbMF0uaW5oZXJpdGVkX21ldGFkYXRhX3ZhbHVlcyk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGVhY2hBc3NldC5hbGxNZXRhZGF0YSA9IGVhY2hBc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5hc3NldERhdGFEYXRhU291cmNlLmRhdGEgPSB0aGlzLmFzc2V0cztcclxuICB9XHJcbn1cclxuXHJcbmZpbmREb2NUeXBlKGZpbGVOYW1lKSB7XHJcbiAgcmV0dXJuIHRoaXMuYXNzZXRTZXJ2aWNlLmZpbmRJY29uQnlOYW1lKGZpbGVOYW1lKTtcclxufVxyXG5cclxucmVmcmVzaERhdGEoKSB7XHJcbiAgdGhpcy5hc3NldERhdGFEYXRhU291cmNlLmRhdGEgPSBbXTtcclxuICB0aGlzLmFzc2V0cy5mb3JFYWNoKGVhY2hBc3NldCA9PiB7XHJcbiAgICBpZihlYWNoQXNzZXQuaW5oZXJpdGVkX21ldGFkYXRhX2NvbGxlY3Rpb25zICYmIGVhY2hBc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnNbMF0pe1xyXG4gICAgICBlYWNoQXNzZXQuYWxsTWV0YWRhdGEgPSBlYWNoQXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0WzBdLm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5jb25jYXQoZWFjaEFzc2V0Py5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnNbMF0/LmluaGVyaXRlZF9tZXRhZGF0YV92YWx1ZXMpO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIGVhY2hBc3NldC5hbGxNZXRhZGF0YSA9IGVhY2hBc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0XHJcbiAgICB9XHJcbiAgfSk7XHJcbiAgdGhpcy5hc3NldERhdGFEYXRhU291cmNlLmRhdGEgPSB0aGlzLmFzc2V0cztcclxuICB0aGlzLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSB0aGlzLmFzc2V0Q29uZmlnLmFzc2V0Vmlld0NvbmZpZyA/XHJcbiAgICAgIHRoaXMudXRpbFNlcnZpY2UuZ2V0RGlzcGxheU9yZGVyRGF0YSh0aGlzLmFzc2V0RGlzcGxheWFibGVGaWVsZHMsIHRoaXMuYXNzZXRDb25maWcuYXNzZXRWaWV3Q29uZmlnLmxpc3RGaWVsZE9yZGVyKSA6IFtdO1xyXG4gICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLm1hcCgoY29sdW1uOiBNUE1GaWVsZCkgPT4gY29sdW1uLklOREVYRVJfRklFTERfSUQpO1xyXG4gIHRoaXMuZGlzcGxheUNvbHVtbnMudW5zaGlmdCgnSW1hZ2UnKTtcclxuICB0aGlzLmRpc3BsYXlDb2x1bW5zLnVuc2hpZnQoJ1NlbGVjdCcpO1xyXG4gIHRoaXMuZGlzcGxheUNvbHVtbnMucHVzaCgnQWN0aW9ucycpO1xyXG4gIGNvbnN0IG9yZGVyZWRGaWVsZHMgPSBbXTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcclxuICAgICAgICAgICAgaWYgKCEoY29sdW1uID09PSAnU2VsZWN0JyB8fCBjb2x1bW4gPT09ICdFeHBhbmQnIHx8IGNvbHVtbiA9PT0gJ0FjdGlvbnMnKSkge1xyXG4gICAgICAgICAgICAgICAgb3JkZXJlZEZpZWxkcy5wdXNoKHRoaXMuYXNzZXREaXNwbGF5YWJsZUZpZWxkcy5maW5kKGRpc3BsYXlhYmxlRmllbGQgPT4gZGlzcGxheWFibGVGaWVsZC5JTkRFWEVSX0ZJRUxEX0lEID09PSBjb2x1bW4pKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3JkZXJlZERpc3BsYXlhYmxlRmllbGRzLm5leHQob3JkZXJlZEZpZWxkcyk7XHJcbn1cclxuXHJcbmRvd25sb2FkKGFzc2V0KSB7XHJcbiAgdGhpcy5kb3dubG9hZEhhbmRsZXIubmV4dChhc3NldCk7XHJcbn1cclxuXHJcbmdldERlbGl2ZXJhYmxlRGV0YWlscyhkZWxpdmVyYWJsZUlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmRlbGl2ZXJhYmxlU2VydmljZS5yZWFkRGVsaXZlcmFibGUoZGVsaXZlcmFibGVJZClcclxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuRGVsaXZlcmFibGUpO1xyXG4gICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgIH0pO1xyXG4gIH0pO1xyXG59XHJcblxyXG5nZXRBc3NldFZlcnNpb25zKCkge1xyXG4gIHRoaXMub3RtbVNlcnZpY2UuZ2V0QXNzZXRWZXJzaW9uc0J5QXNzZXRJZCh0aGlzLnNlbGVjdGVkQXNzZXQuYXNzZXRfaWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMuYXNzZXRWZXJzaW9uc0NhbGxCYWNrSGFuZGxlci5uZXh0KHJlc3BvbnNlWzBdKTtcclxuICB9KTtcclxufVxyXG5cclxuZ2V0QXNzZXREZXRhaWxzKCkge1xyXG4gIHRoaXMuYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyLm5leHQodGhpcy5zZWxlY3RlZEFzc2V0KTtcclxufVxyXG5cclxuZ2V0QXNzZXRQcmV2aWV3KCkge1xyXG4gIHRoaXMuYXNzZXRQcmV2aWV3Q2FsbEJhY2tIYW5kbGVyLm5leHQodGhpcy5zZWxlY3RlZEFzc2V0KTtcclxufVxyXG5cclxuZ2V0UHJvcGVydHkoYXNzZXQsIHByb3BlcnR5KSB7XHJcbiAgcmV0dXJuIHRoaXMudGFza1NlcnZpY2UuZ2V0UHJvcGVydHkoYXNzZXQsIHByb3BlcnR5KTtcclxufVxyXG5cclxuZ2V0RmlsZVByb29maW5nU2Vzc2lvbkRhdGEoYXNzZXQsYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIGNyUm9sZUlkLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEZpbGVQcm9vZmluZ1Nlc3Npb24oYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIHRhc2tOYW1lLCBjclJvbGVJZCwgdmVyc2lvbmluZylcclxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5zZXNzaW9uRGV0YWlscyAmJiByZXNwb25zZS5zZXNzaW9uRGV0YWlscy5zZXNzaW9uVXJsKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnN0IGNyRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgIHVybDogcmVzcG9uc2Uuc2Vzc2lvbkRldGFpbHMuc2Vzc2lvblVybCxcclxuICAgICAgICAgICAgICAgICAgICAgIGhlYWRlckluZm9zOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnTmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0UHJvcGVydHkoYXNzZXQsIERlbGl2ZXJhYmxlQ29uc3RhbnRzLkRFTElWRVJBQkxFX05BTUVfTUVUQURBVEFGSUVMRF9JRCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3N0cmluZydcclxuICAgICAgICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGNyRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIG9wZW5pbmcgQ3JlYXRpdmUgUmV2aWV3LicpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICB9KTtcclxuICB9KTtcclxufVxyXG5cclxub3BlbkNSKGFzc2V0KXtcclxuICB0aGlzLm9wZW5DcmVhdGl2ZVJldmlldy5uZXh0KGFzc2V0KTtcclxufVxyXG5cclxuXHJcbmdldENvbHVtblByb3BlcnR5KGNvbHVtbixkYXRhKXtcclxuICBmb3IobGV0IHJvdyA9IDA7IHJvdyA8IGRhdGEubGVuZ3RoOyByb3crKyl7XHJcbiAgICBpZihjb2x1bW4uT1RNTV9GSUVMRF9JRCA9PT0gZGF0YVtyb3ddLmlkKXtcclxuICAgICAgaWYoZGF0YVtyb3ddLnZhbHVlICYmIGRhdGFbcm93XS52YWx1ZS52YWx1ZSAmJiBkYXRhW3Jvd10udmFsdWUudmFsdWUudmFsdWUpe1xyXG4gICAgICAgIHJldHVybiBkYXRhW3Jvd10udmFsdWUudmFsdWUudmFsdWU7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZSBpZihkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudCAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZSAmJiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZS52YWx1ZSl7XHJcbiAgICAgICAgcmV0dXJuIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlLnZhbHVlO1xyXG4gICAgICB9XHJcbiAgICAgIGVsc2UgaWYoZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQgJiYgZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUgJiYgZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUudmFsdWUgJiYgZGF0YVtyb3ddLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUudmFsdWUuZmllbGRfdmFsdWUpe1xyXG4gICAgICAgIHJldHVybiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZS5maWVsZF92YWx1ZS52YWx1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gJ05BJztcclxufVxyXG5cclxuc2hhcmVBc3NldChhc3NldCkge1xyXG4gIHRoaXMuc2hhcmVBc3NldHNDYWxsYmFja0hhbmRsZXIubmV4dChbYXNzZXRdKTtcclxufVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuYXNzZXRMaXN0ID0gdGhpcy5hc3NldHM7XHJcbiAgICB0aGlzLm90bW1CYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gdGhpcy5hc3NldERpc3BsYXlhYmxlRmllbGRzLm1hcCgoY29sdW1uOiBNUE1GaWVsZCkgPT4gY29sdW1uLklOREVYRVJfRklFTERfSUQpO1xyXG4gICAgdGhpcy5jckFjdGlvbkRhdGEgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENSQWN0aW9ucygpO1xyXG4gICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19