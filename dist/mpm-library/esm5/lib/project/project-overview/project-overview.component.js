import { __assign, __decorate, __values } from "tslib";
/* Usage and parameter details
<mpm-project-overview [projectId]="projectId" [isTemplate]="false" [teamsOptions]="teamsOptions"
     (closeCallbackHandler)="afterCloseProject($event)"
    (saveCallBackHandler)="afterSaveProject($event)" (notificationHandler)="notificationHandler($event)"
    (loadingHandler)="loadingHandler($event)"></mpm-project-overview>

<!--
    projectId: Id of the existing project to be updated -- Optional for new project/ template
    isTemplate: true for Template type and false for Project type
    teamsOptions: Persisted Array of Teams from the response of GetAllTeam Webservice -- optional;
    closeCallbackHandler: function handler for cancel create/update project/ template
    saveCallBackHandler: function handler for after save/update project/ template @output(projectId, isTemplate)
    notificationHandler: function to show notifications @output({message, type}) -- type is NOTIFICATION_TYPE
    loadingHandler: function to show/ hide loading message @output(isShow) -- isShow: boolean true for showing and false for hiding
-->
*/
import { Component, Input, ViewChild, Output, EventEmitter, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { ProjectConstant } from './project.constants';
import { Observable, forkJoin } from 'rxjs';
import { CustomSearchFieldComponent } from '../../shared/components/custom-search-field/custom-search-field.component';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { MPM_ROLES } from '../../mpm-utils/objects/Role';
import * as acronui from '../../mpm-utils/auth/utility';
import { CategoryService } from '../shared/services/category.service';
import { DateValidators } from '../../shared/validators/date.validator';
import { ProjectTypes } from '../../mpm-utils/objects/ProjectTypes';
import { StatusTypes, StatusLevels } from '../../mpm-utils/objects/StatusType';
import { StatusService } from '../../shared/services/status.service';
import { DataTypeConstants } from '../shared/constants/data-type.constants';
import { ProjectUtilService } from '../shared/services/project-util.service';
import { ProjectService } from '../shared/services/project.service';
import { newArray } from '@angular/compiler/src/util';
import { ProjectFromTemplateService } from '../shared/services/project-from-template.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { ActivatedRoute } from '@angular/router';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CommentsService } from '../../comments/services/comments.service';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { BulkCommentsComponent } from '../../comments/bulk-comments/bulk-comments.component';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { DatePipe } from '@angular/common';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
var ProjectOverviewComponent = /** @class */ (function () {
    function ProjectOverviewComponent(fb, adapter, dialog, sharingService, utilService, appService, otmmService, otmmMetadataService, categoryService, statusService, projectUtilService, projectService, projectFromTemplateService, activatedRoute, entityAppDefService, commentsService, notificationService, loaderService, datePipe) {
        var _this = this;
        this.fb = fb;
        this.adapter = adapter;
        this.dialog = dialog;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.appService = appService;
        this.otmmService = otmmService;
        this.otmmMetadataService = otmmMetadataService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.projectUtilService = projectUtilService;
        this.projectService = projectService;
        this.projectFromTemplateService = projectFromTemplateService;
        this.activatedRoute = activatedRoute;
        this.entityAppDefService = entityAppDefService;
        this.commentsService = commentsService;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.datePipe = datePipe;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.restartProjectHandler = new EventEmitter();
        this.PROJECT_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.TEAM_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
        this.PRIORITY_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Priority/operations';
        this.PROJECT_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.CAMPAIGN_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';
        this.TEAM_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Teams/operations';
        this.GET_TEAM_WS_METHOD_NAME = 'GetTeamsForUser';
        this.GET_PRIORITY_WS_METHOD_NAME = 'GetAllPriorityForCategoryLevelName';
        this.GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
        this.CREATE_PROJECT_WS_METHOD_NAME = 'CreateProject';
        this.UPDATE_PROJECT_WS_METHOD_NAME = 'UpdateProjectDetails';
        this.CREATE_CAMPAIGN_WS_METHOD_NAME = 'CreateCampaign';
        this.GET_CAMPAIGN_WS_METHOD_NAME = 'GetCampaignById';
        this.GET_ALL_CAMPAIGN_WS_METHOD_NAME = 'GetAllCampaign';
        this.GET_ALL_TEAMS_WS_METHOD_NAME = 'GetAllTeams';
        this.GET_USERS_FOR_TEAM_ROLE_WS_METHOD_NAME = 'GetUsersForTeamRole';
        this.GET_ALL_cAMPAIGN_BY_NAME_WS_METHOD_NAME = 'GetAllCampaignByName';
        this.minStartDate = new Date();
        this.disableOptions = true;
        this.disableStatusOptions = true;
        this.disableEditOptions = false;
        this.showStandardStatus = true;
        this.disableProjectSave = false;
        this.disableStartDate = false;
        this.disableEndDate = false;
        this.enableRestartProject = false;
        this.keyRestriction = {
            NUMBER: [69, 187, 188, 107]
        };
        this.isDefaultPriortiySet = false;
        this.dateFilter = function (date) {
            if (_this.enableWorkWeek) {
                return true;
            }
            else {
                var day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
        this.oldProjectMetadata = null;
    }
    ProjectOverviewComponent.prototype.ngOnChanges = function (changes) {
    };
    ProjectOverviewComponent.prototype.canDeactivate = function () {
        var _this = this;
        this.loadingHandler.next(true);
        return new Observable(function (observer) {
            observer.next(true);
            observer.complete();
            _this.loadingHandler.next(false);
        });
    };
    ProjectOverviewComponent.prototype.restrictKeysOnType = function (event, datatype) {
        if (this.keyRestriction[datatype] && this.keyRestriction[datatype].includes(event.keyCode)) {
            event.preventDefault();
        }
    };
    ProjectOverviewComponent.prototype.refreshView = function () {
        this.disableProjectSave = false;
        this.oldProjectMetadata = null;
        this.loadingHandler.next(true);
        this.initialiseOverviewConfig();
        if (this.isCampaign) {
            this.overViewConfig.isCampaign = true;
            this.overViewConfig.isProject = false;
            this.overViewConfig.isTemplate = false;
            this.overViewConfig.formType = ProjectTypes.CAMPAIGN;
        }
        else if (this.isTemplate) {
            this.overViewConfig.isProject = false;
            this.overViewConfig.isTemplate = true;
            this.overViewConfig.formType = ProjectTypes.TEMPLATE;
        }
        else {
            this.overViewConfig.isProject = true;
            this.overViewConfig.isTemplate = false;
            this.overViewConfig.formType = ProjectTypes.PROJECT;
        }
        if (this.campaignId) {
            this.getCampaignDetail(this.campaignId);
        }
        else if (this.projectId) {
            this.getProjectDetail(this.projectId);
        }
        else {
            this.utilService.setProjectCustomMetadata(null);
            this.getFilters();
            this.loadingHandler.next(false);
        }
    };
    ProjectOverviewComponent.prototype.getCampaignOwnerDetailsId = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            if (!userId) {
                observer.next();
                observer.complete();
            }
            var parameters = {
                userId: userId
            };
            _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (personDetails) {
                _this.campaignOwnerDetails = personDetails;
                observer.next();
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.getProjectOwnerDetails = function (userCN) {
        var _this = this;
        return new Observable(function (observer) {
            if (!userCN) {
                observer.error();
            }
            var parameters = {
                userCN: userCN
            };
            _this.appService.getPersonByUserId(parameters).subscribe(function (personDetails) {
                _this.projectOwnerDetails = personDetails;
                observer.next();
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.getFilters = function () {
        var _this = this;
        var isUpdate, categoryLevel;
        this.loadingHandler.next(true);
        if (this.campaignId || this.projectId) {
            isUpdate = true;
            categoryLevel = this.campaignId ? ProjectConstant.CATEGORY_LEVEL_CAMPAIGN : ProjectConstant.CATEGORY_LEVEL_PROJECT;
        }
        else {
            isUpdate = false;
            if (this.isCampaign) {
                categoryLevel = ProjectConstant.CATEGORY_LEVEL_CAMPAIGN;
            }
            else {
                categoryLevel = ProjectConstant.CATEGORY_LEVEL_PROJECT;
            }
        }
        this.campaignIds = this.selectedCampaignId ? this.selectedCampaignId : (this.overViewConfig &&
            this.overViewConfig.selectedProject && this.overViewConfig.selectedProject.R_PO_CAMPAIGN && this.overViewConfig.selectedProject.R_PO_CAMPAIGN['Campaign-id'] &&
            this.overViewConfig.selectedProject.R_PO_CAMPAIGN['Campaign-id'].Id) ? this.overViewConfig.selectedProject.R_PO_CAMPAIGN['Campaign-id'].Id : '';
        forkJoin(
        /*  [(!isUpdate || this.overViewConfig.isTemplate ? this.statusService.getStatusByCategoryLevelAndStatusType(
             categoryLevel, StatusTypes.INITIAL) : this.statusService.getStatusByCategoryLevelName(
                 categoryLevel)),
         this.getPriorities(categoryLevel),
         this.getTeams(), this.getCampaign(),
         this.projectService.getAllCategoryMetadata()] */
        //201
        /* (!isUpdate || this.overViewConfig.isTemplate ? this.statusService.getStatusByCategoryLevelAndStatusType(
                categoryLevel, StatusTypes.INITIAL) : this.statusService.getAllStatusBycategoryName(
                    categoryLevel)),
            this.getPriorities(categoryLevel),
            this.getTeams(), this.getCampaign(),
            this.projectService.getAllCategoryMetadata()] */
        (!this.isCampaign && (this.selectedCampaignId || this.campaignIds)) ?
            [(!isUpdate || this.overViewConfig.isTemplate ? this.statusService.getStatusByCategoryLevelAndStatusType(categoryLevel, StatusTypes.INITIAL) : this.statusService.getAllStatusBycategoryName(categoryLevel)),
                this.getPriorities(categoryLevel),
                this.getTeams(), /* this.getCampaignById(this.selectedCampaignId), */ this.getCampaignById(this.campaignIds),
                this.projectService.getAllCategoryMetadata()] : !this.isCampaign ? [(!isUpdate || this.overViewConfig.isTemplate ? this.statusService.getStatusByCategoryLevelAndStatusType(categoryLevel, StatusTypes.INITIAL) : this.statusService.getAllStatusBycategoryName(categoryLevel)),
            this.getPriorities(categoryLevel),
            this.getTeams(),
            this.projectService.getAllCategoryMetadata()] : [(!isUpdate || this.overViewConfig.isTemplate ? this.statusService.getStatusByCategoryLevelAndStatusType(categoryLevel, StatusTypes.INITIAL) : this.statusService.getAllStatusBycategoryName(categoryLevel)),
            this.getPriorities(categoryLevel),
            this.projectService.getAllCategoryMetadata()]).subscribe(function (forkedData) {
            /* this.overViewConfig.categoryMetadataConfig.filterOptions = forkedData[4] ? forkedData[4].map(data => {
                return { displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME, ...data };
            }) : []; */
            // above forkjoin are changed
            if (!_this.isCampaign && (_this.selectedCampaignId || _this.campaignIds)) {
                _this.overViewConfig.categoryMetadataConfig.filterOptions = forkedData[4] ? forkedData[4].map(function (data) {
                    return __assign({ displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME }, data);
                }) : [];
            }
            else if (!_this.isCampaign) {
                _this.overViewConfig.categoryMetadataConfig.filterOptions = forkedData[3] ? forkedData[3].map(function (data) {
                    return __assign({ displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME }, data);
                }) : [];
            }
            else {
                _this.overViewConfig.categoryMetadataConfig.filterOptions = forkedData[2] ? forkedData[2].map(function (data) {
                    return __assign({ displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME }, data);
                }) : [];
            }
            _this.overViewConfig.statusOptions = !isUpdate || _this.overViewConfig.isTemplate ? forkedData[0] : (_this.isCampaign ?
                _this.projectUtilService.filterStatusOptions(_this.overViewConfig.selectedCampaign.R_PO_STATUS['MPM_Status-id'].Id, forkedData[0], MPM_LEVELS.CAMPAIGN) :
                _this.projectUtilService.filterStatusOptions(_this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id'].Id, forkedData[0], MPM_LEVELS.PROJECT));
            if (!_this.selectedStatus && _this.overViewConfig.statusOptions && _this.overViewConfig.statusOptions.length > 0) {
                _this.selectedStatus = _this.overViewConfig.statusOptions[0]['MPM_Status-id'].Id;
            }
            _this.overViewConfig.priorityOptions = forkedData[1];
            // TODO: selecting Default Pririty - column to be added to entity
            if (!_this.selectedPriority && _this.overViewConfig.priorityOptions && _this.overViewConfig.priorityOptions.length > 0) {
                _this.overViewConfig.priorityOptions.sort(function (a, b) {
                    if (parseInt(a.DISPLAY_ORDER) < parseInt(b.DISPLAY_ORDER)) {
                        return -1;
                    }
                    else
                        1;
                });
                _this.overViewConfig.priorityOptions.forEach(function (p1) {
                    if (p1.IS_DEFAULT.toString() == 'true') {
                        _this.isDefaultPriortiySet = true;
                        _this.selectedPriority = p1['MPM_Priority-id'].Id;
                    }
                });
                if (!_this.isDefaultPriortiySet) {
                    _this.selectedPriority = _this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id;
                }
                // this.selectedPriority = this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id;
            }
            //128
            _this.hasTeams = forkedData[2] && forkedData[2].length > 0 ? true : false;
            _this.overViewConfig.teamsOptions = forkedData[2] && forkedData[2].length > 0 ? forkedData[2] : _this.sharingService.teams;
            // this.overViewConfig.categoryOptions = this.sharingService.getAllCategories();
            _this.overViewConfig.ownerFieldConfig.filterOptions = [{
                    name: '',
                    value: '',
                    displayName: ''
                }];
            if (_this.overViewConfig.selectedCampaign
                && _this.overViewConfig.selectedCampaign.R_PO_CAMPAIGN_OWNER
                && _this.overViewConfig.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id) {
                var parameters = {
                    userId: _this.overViewConfig.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (personDetails) {
                    _this.projectOwnerDetails = personDetails;
                    _this.populateForm();
                });
                _this.loadingHandler.next(false);
            }
            else if (_this.overViewConfig.selectedProject
                && _this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER
                && _this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                var parameters = {
                    userId: _this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (personDetails) {
                    _this.projectOwnerDetails = personDetails;
                    _this.populateForm();
                    _this.loadingHandler.next(false);
                });
            }
            else {
                _this.populateForm();
                //
                _this.loadingHandler.next(false);
            }
        });
    };
    ProjectOverviewComponent.prototype.formCampaignValue = function (campaign) {
        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id) {
            return campaign['Campaign-id'].Id + '-' + campaign.CAMPAIGN_NAME;
        }
    };
    /* formCampaign(campaignId) {
        if (campaignId) {
            const selectedCampaign = this.overViewConfig.campaignDataConfig.filterOptions.find(element => element.value === campaignId);
            return selectedCampaign ? selectedCampaign : '';
        } else {
            return '';
        }
    } */
    ProjectOverviewComponent.prototype.formCampaign = function (campaignId) {
        if (campaignId) {
            var selectedCampaign = void 0;
            selectedCampaign = this.overViewConfig.campaignDataConfig.filterOptions.find(function (element) { return element.value === campaignId; });
            return selectedCampaign ? selectedCampaign : '';
        }
        else {
            return '';
        }
    };
    ProjectOverviewComponent.prototype.getUsersForTeamRole = function () {
        var _this = this;
        return new Observable(function (observer) {
            var roleDetailObj = _this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
            var param = {
                teamID: _this.selectedTeam['MPM_Teams-id'].Id,
                roleDN: roleDetailObj.ROLE_DN
            };
            _this.loadingHandler.next(true);
            _this.appService.getUsersForTeamRole(param)
                .subscribe(function (response) {
                if (response && response.users && response.users.user) {
                    var userList = acronui.findObjectsByProp(response, 'user');
                    var users_1 = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(function (user) {
                            var fieldObj = users_1.find(function (e) { return e.value === user.cn; });
                            if (!fieldObj) {
                                users_1.push({
                                    name: user.name,
                                    value: user.cn,
                                    displayName: user.name + ' (' + user.cn + ')'
                                });
                            }
                        });
                    }
                    _this.overViewConfig.ownerFieldConfig.filterOptions = users_1;
                }
                else {
                    _this.overViewConfig.ownerFieldConfig.filterOptions = [];
                    var eventData = { message: 'Manager role in the selected team is not available', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                }
                observer.next();
                observer.complete();
            }, function (error) {
                _this.loadingHandler.next(false);
                var eventData = { message: 'Something went wrong while fetching Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next([]);
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.getTeams = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.overViewConfig.isProject && _this.isCampaignProject) {
                _this.loadingHandler.next(true);
                _this.appService.invokeRequest(_this.TEAM_METHOD_NS, _this.GET_ALL_TEAMS_WS_METHOD_NAME, null)
                    .subscribe(function (response) {
                    var teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                    //  this.loadingHandler.next(false);
                }, function (error) {
                    _this.loadingHandler.next(false);
                    var eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next([]);
                    observer.complete();
                });
            }
            else {
                var roleDetailObj = _this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
                var param = {
                    roleDN: roleDetailObj.ROLE_DN
                };
                _this.loadingHandler.next(true);
                _this.appService.invokeRequest(_this.TEAM_BPM_METHOD_NS, _this.GET_TEAM_WS_METHOD_NAME, param)
                    .subscribe(function (response) {
                    var teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                    //  this.loadingHandler.next(false);
                }, function (error) {
                    _this.loadingHandler.next(false);
                    var eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next([]);
                    observer.complete();
                });
            }
        });
    };
    /* getPriorities(categoryLevelName): Observable<Array<Priority>> {
        const getRequestObject = {
            categoryLevelName: categoryLevelName
        };
        this.loadingHandler.next(true);
        return new Observable(observer => {
            this.appService.invokeRequest(this.PRIORITY_METHOD_NS, this.GET_PRIORITY_WS_METHOD_NAME, getRequestObject)
                .subscribe(response => {
                    const priorities: Array<Priority> = acronui.findObjectsByProp(response, 'MPM_Priority');
                    observer.next(priorities);
                    observer.complete();

                    // this.loadingHandler.next(false);
                }, error => {
                    this.loadingHandler.next(false);
                    const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next([]);
                    observer.complete();
                });
        });
    } */
    ProjectOverviewComponent.prototype.getPriorities = function (categoryLevelName) {
        var currCategoryObj = this.sharingService.getCategoryLevels().find(function (data) {
            return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
        });
        this.loadingHandler.next(true);
        if (currCategoryObj) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null) {
                var allStatuses = JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES));
                var allStatus = allStatuses.filter(function (status) {
                    return status.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currCategoryObj['MPM_Category_Level-id'].Id;
                });
                var status_1 = [];
                status_1.push(allStatus);
                return status_1;
            }
            else {
                var allStatus = this.sharingService.getAllPriorities().filter(function (status) {
                    return status.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currCategoryObj['MPM_Category_Level-id'].Id;
                });
                var status_2 = [];
                status_2.push(allStatus);
                return status_2;
            }
        }
        return [];
    };
    //201
    ProjectOverviewComponent.prototype.getAllCampaignByName = function (campaignName) {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                CampaignName: campaignName
            };
            _this.appService.invokeRequest(_this.CAMPAIGN_DETAILS_METHOD_NS, _this.GET_ALL_cAMPAIGN_BY_NAME_WS_METHOD_NAME, getRequestObject)
                // this.appService.getAllCampaign()
                .subscribe(function (response) {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    _this.campaignList = response.Campaign;
                    if (_this.projectOverviewForm.controls.campaign) {
                        _this.projectOverviewForm.controls.campaign.valueChanges.subscribe(function (campaign) {
                            if (campaign) {
                                _this.selectedCampaign = _this.campaignList.find(function (data) { return data['Campaign-id'].Id === campaign.split(/-(.+)/)[0]; });
                                if (_this.selectedCampaign) {
                                    _this.loadingHandler.next(true);
                                    _this.minStartDate = _this.datePipe.transform(_this.selectedCampaign.CAMPAIGN_START_DATE, 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd') ? _this.selectedCampaign.CAMPAIGN_START_DATE : new Date();
                                    _this.getCampaignOwnerDetailsId(_this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(function (response) {
                                        _this.loadingHandler.next(false);
                                        _this.projectOverviewForm.controls.campaignOwner.setValue(_this.formUserDetails({
                                            UserId: _this.campaignOwnerDetails.userCN,
                                            FullName: _this.campaignOwnerDetails.fullName
                                        }, _this.campaignOwnerDetails.userCN));
                                    });
                                }
                                else {
                                    _this.projectOverviewForm.controls.campaignOwner.setValue('');
                                }
                            }
                            else {
                                _this.selectedCampaign = null;
                                _this.projectOverviewForm.controls.campaignOwner.setValue('');
                                // 128
                                _this.overViewConfig.campaignDataConfig.filterOptions = [];
                            }
                        });
                    }
                    if (_this.projectOverviewForm.controls.startDate) {
                        _this.projectOverviewForm.controls.startDate.valueChanges.subscribe(function () {
                            if (_this.selectedCampaign) {
                                if ((new Date(_this.projectOverviewForm.controls.startDate.value) < new Date(_this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                    new Date(_this.projectOverviewForm.controls.startDate.value) > new Date(_this.selectedCampaign.CAMPAIGN_END_DATE)) &&
                                    _this.datePipe.transform(new Date(_this.projectOverviewForm.controls.startDate.value), 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                                    _this.startDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                                }
                                else {
                                    _this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                                }
                            }
                            else {
                                _this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                            }
                        });
                    }
                    if (_this.projectOverviewForm.controls.endDate) {
                        _this.projectOverviewForm.controls.endDate.valueChanges.subscribe(function () {
                            if (_this.selectedCampaign) {
                                if (new Date(_this.projectOverviewForm.controls.endDate.value) < new Date(_this.selectedCampaign.CAMPAIGN_START_DATE) ||
                                    new Date(_this.projectOverviewForm.controls.endDate.value) > new Date(_this.selectedCampaign.CAMPAIGN_END_DATE)) {
                                    _this.endDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                                }
                                else {
                                    _this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                                }
                            }
                            else {
                                if (_this.isCampaign) {
                                    _this.endDateErrorMessage = ProjectConstant.CAMPAIGN_START_DATE_ERROR_MESSAGE;
                                }
                                else {
                                    _this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                                }
                            }
                        });
                    }
                    var campaignFilterList_1 = [];
                    _this.campaignList.map(function (campaign) {
                        var value = _this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            _this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList_1.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value,
                                ownerId: campaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id
                            });
                        }
                    });
                    _this.overViewConfig.campaignDataConfig.filterOptions = campaignFilterList_1;
                }
                else {
                    _this.campaignList = [];
                    _this.overViewConfig.campaignDataConfig.filterOptions = [];
                }
                _this.loadingHandler.next(false);
                observer.next(_this.overViewConfig.campaignDataConfig.filterOptions);
                observer.complete();
            });
        });
    };
    /* this.getCampaignOwnerDetailsId(this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(response => {
                                this.loadingHandler.next(false);
                                this.projectOverviewForm.controls.campaignOwner.setValue(this.formUserDetails({
                                    UserId: this.campaignOwnerDetails.userCN,
                                    FullName: this.campaignOwnerDetails.fullName
                                }, this.campaignOwnerDetails.userCN));
                            }); */
    ProjectOverviewComponent.prototype.getCampaigns = function (campaignName) {
        var _this = this;
        if (campaignName.target.value.length > 2) {
            this.getAllCampaignByName(campaignName.target.value).subscribe(function (campaignResponse) {
                _this.filterOptions = campaignResponse;
            });
        }
    };
    ProjectOverviewComponent.prototype.changeAutoComplete = function (event) {
        var _this = this;
        this.campaignId = event.option.value.split(/-(.+)/)[0];
        var selectedCampaign = this.filterOptions.find(function (element) { return element.name === event.option.value.split(/-(.+)/)[1]; });
        this.getCampaignOwnerDetailsId(selectedCampaign.ownerId).subscribe(function (response) {
            _this.projectOverviewForm.controls.campaignOwner.setValue(_this.formUserDetails({
                UserId: _this.campaignOwnerDetails.userCN,
                FullName: _this.campaignOwnerDetails.fullName
            }, _this.campaignOwnerDetails.userCN));
            _this.overViewConfig.campaignOwnerFieldConfig.formControl = _this.projectOverviewForm.controls.campaignOwner;
        });
    };
    ProjectOverviewComponent.prototype.getCampaign = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.CAMPAIGN_DETAILS_METHOD_NS, _this.GET_ALL_CAMPAIGN_WS_METHOD_NAME, null)
                // this.appService.getAllCampaign()
                .subscribe(function (response) {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    _this.campaignList = response.Campaign;
                    var campaignFilterList_2 = [];
                    _this.campaignList.map(function (campaign) {
                        var value = _this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            _this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList_2.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    _this.overViewConfig.campaignDataConfig.filterOptions = campaignFilterList_2;
                }
                else {
                    _this.campaignList = [];
                    _this.overViewConfig.campaignDataConfig.filterOptions = [];
                }
                _this.loadingHandler.next(false);
                observer.next(true);
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.getLocalValueById = function (metadataFieldId) {
        var customMetadata = this.utilService.getProjectCustomMetadata();
        if (!customMetadata) {
            return '';
        }
        var resultValue = '';
        customMetadata.Metadata.metadataFields.forEach(function (element) {
            if (element.fieldId === metadataFieldId) {
                resultValue = element.defaultValue;
            }
        });
        return resultValue;
    };
    ProjectOverviewComponent.prototype.getMetaData = function (categoryLevel, categoryMetadatId) {
        var _this = this;
        var categoryLevelDetails = this.categoryService.getCategoryLevelDetailsByType(categoryLevel);
        if (categoryMetadatId) {
            var selectedCategoryMetadata = this.formCategoryMetadata(categoryMetadatId);
            categoryLevelDetails = selectedCategoryMetadata ? selectedCategoryMetadata : categoryLevelDetails;
        }
        this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
            .subscribe(function (metaDataModelDetails) {
            var e_1, _a, e_2, _b;
            var fieldsetGroup = [];
            var fieldGroups = [];
            _this.customMetadataFields = [];
            if (_this.projectOverviewForm.get('CustomFieldGroup')) {
                _this.projectOverviewForm.removeControl('CustomFieldGroup');
            }
            if (metaDataModelDetails && metaDataModelDetails.metadata_element_list &&
                Array.isArray(metaDataModelDetails.metadata_element_list)) {
                try {
                    for (var _c = __values(metaDataModelDetails.metadata_element_list), _d = _c.next(); !_d.done; _d = _c.next()) {
                        var metadataFieldGroup = _d.value;
                        var formControlArray = [];
                        if (metadataFieldGroup.id === ProjectConstant.MPM_PROJECT_METADATA_GROUP || metadataFieldGroup.id === ProjectConstant.MPM_CAMPAIGN_METADATA_GROUP) {
                            continue;
                        }
                        if (metadataFieldGroup.metadata_element_list &&
                            Array.isArray(metadataFieldGroup.metadata_element_list)) {
                            try {
                                for (var _e = (e_2 = void 0, __values(metadataFieldGroup.metadata_element_list)), _f = _e.next(); !_f.done; _f = _e.next()) {
                                    var metadataField = _f.value;
                                    _this.customMetadataFields.push(metadataField);
                                    var validators = [];
                                    var value = (_this.overViewConfig.metaDataValues) ?
                                        _this.otmmMetadataService.getFieldValueById(_this.overViewConfig.metaDataValues, metadataField.id) : _this.getLocalValueById(metadataField.id);
                                    var formControlObj = new FormControl({
                                        value: typeof value !== 'undefined' ? value : '',
                                        disabled: !metadataField.editable || ((_this.projectId && _this.overViewConfig.isReadOnly) || (_this.campaignId && _this.overViewConfig.isReadOnly)),
                                    });
                                    if (metadataField.required) {
                                        validators.push(Validators.required);
                                    }
                                    if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
                                        validators.push(DateValidators.dateFormat);
                                    }
                                    if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                                        metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
                                        validators.push(Validators.maxLength(metadataField.data_length));
                                    }
                                    if (metadataField.data_type === ProjectConstant.METADATA_EDIT_TYPES.NUMBER) {
                                        var numericeVlalue = _this.getMaxMinValueForCustomMetadata(metadataField.data_length, metadataField.scale);
                                        validators.push(Validators.max(numericeVlalue), Validators.min(-(numericeVlalue)));
                                        // (!metadataField.scale) ? validators.push(Validators.pattern(new RegExp('(-(\d{0,5}))|(\d{0,5})$')))
                                        //     : validators.push(Validators.pattern(new RegExp('-((\d+\.)?\d{0,' + metadataField.scale + '}$) | ((\d+\.)?\d{0,' + metadataField.scale + '})$')));
                                    }
                                    formControlObj.setValidators(validators);
                                    formControlObj.updateValueAndValidity();
                                    formControlObj['fieldset'] = metadataField;
                                    formControlObj['name'] = metadataField.name;
                                    if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
                                        metadataField.values = _this.utilService.getLookupDomainValuesById(metadataField.domain_id);
                                    }
                                    fieldsetGroup.push(formControlObj);
                                    // new code
                                    formControlArray.push(formControlObj);
                                }
                            }
                            catch (e_2_1) { e_2 = { error: e_2_1 }; }
                            finally {
                                try {
                                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                                }
                                finally { if (e_2) throw e_2.error; }
                            }
                        }
                        var fieldGroup = _this.fb.group({ fieldset: new FormArray(formControlArray) });
                        fieldGroup['name'] = metadataFieldGroup.name;
                        fieldGroups.push(fieldGroup);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            if (_this.customMetadataFields && Array.isArray(_this.customMetadataFields) && _this.customMetadataFields.length > 0) {
                // Need to be tested
                fieldsetGroup.forEach(function (fieldSet) {
                    if (fieldSet.fieldset.data_type === DataTypeConstants.DATE && fieldSet.value) {
                        fieldSet.value = new Date(fieldSet.value);
                    }
                });
                _this.customFieldsGroup = _this.fb.group({ fieldset: new FormArray(fieldsetGroup) });
                _this.projectOverviewForm.addControl('CustomFieldGroup', new FormArray(fieldGroups, { updateOn: 'blur' }));
            }
            _this.projectOverviewForm.updateValueAndValidity();
            _this.oldProjectMetadata = _this.projectId ? _this.createNewProjectUpdate() : null;
        });
    };
    ProjectOverviewComponent.prototype.getMaxMinValueForNumber = function (scale) {
        var numericeVlalue = '99999';
        if (scale) {
            numericeVlalue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericeVlalue) : parseInt(numericeVlalue);
    };
    ProjectOverviewComponent.prototype.getMaxMinValueForCustomMetadata = function (dataLength, scale) {
        var data = Math.abs(dataLength - scale);
        var numericValue = newArray(data).fill('9').join('');
        if (scale) {
            numericValue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericValue) : parseInt(numericValue);
    };
    ProjectOverviewComponent.prototype.validateTaskStatusUpdate = function (selectedStatus) {
        /*    this.loaderService.show();
           this.entityAppDefService.getStatusById(selectedStatus.value)
               .subscribe(projectStatus => {
                   if (projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED) {
                       this.loaderService.hide();
                       const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                           width: '40%',
                           disableClose: true,
                           data: {
                               message: 'Are you sure you want to cancel the project?',
                               hasConfirmationComment: true,
                               commentText: 'Comments',
                               isCommentRequired: true,
                               errorMessage: 'Kindly fill the comments field',
                               submitButton: 'Yes',
                               cancelButton: 'No'
                           }
                       });
                       dialogRef.afterClosed().subscribe(result => {
                           if (result && result.isTrue) {
                               console.log(result.confirmationComment);
                               this.cancelComment = result.confirmationComment;
                               this.notificationService.success('Project cancellation process is in progress and it might take some time to complete');
                           }
                       });
                   } else {
                       this.loaderService.hide();
                   }
               }, projectStatusError => {
                   console.log(projectStatusError);
               }); */
        // TODO: Add validations in Edit when any status of Type Final is selected, validations for task and deliverable status.
    };
    ProjectOverviewComponent.prototype.validateProjectStartDate = function (selectedDateObj) {
        if (!this.overViewConfig.selectedProject) {
            return;
        }
        // TODO: Add validations for Edit Project
    };
    ProjectOverviewComponent.prototype.validateProjectEndDate = function (selectedDateObj) {
        if (!this.overViewConfig.selectedProject) {
            return;
        }
        // TODO: Add validations for Edit Project
    };
    /* formStatus(statusId, statusName) {
        const status = {
            'MPM_Status-id': {
                Id: statusId
            },
            NAME: statusName
        };
        return status;
    }

    formPriority(priorityId, priorityName) {
        const priority = {
            'MPM_Priority-id': {
                Id: priorityId
            },
            DESCRIPTION: priorityName
        };
        return priority;
    } */
    ProjectOverviewComponent.prototype.populateForm = function () {
        var _this = this;
        var disableAdminFields = true;
        var disableFields = true;
        var pattern = ProjectConstant.NAME_STRING_PATTERN;
        var numericeVlalue = this.getMaxMinValueForNumber(0);
        if (this.projectOverviewForm && this.projectOverviewForm.controls) {
            this.projectOverviewForm.controls = null;
        }
        if (this.customFieldsGroup && this.customFieldsGroup.controls) {
            this.customFieldsGroup.controls = null;
        }
        this.projectOverviewForm = null;
        this.customFieldsGroup = null;
        this.overViewConfig.isReadOnly = true;
        this.isReadOnly = this.overViewConfig.isReadOnly;
        if (this.overViewConfig.selectedProject) {
            this.disableEditOptions = true;
            if (this.overViewConfig.currentUserItemId === this.overViewConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                this.overViewConfig.isReadOnly = false;
                this.isReadOnly = this.overViewConfig.isReadOnly;
            }
            if (this.overViewConfig.hasProjectAdminRole) {
                disableAdminFields = false;
            }
        }
        else if (this.overViewConfig.selectedCampaign) {
            this.disableEditOptions = true;
            if (this.overViewConfig.currentUserItemId === this.overViewConfig.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id) {
                this.overViewConfig.isReadOnly = false;
                this.isReadOnly = this.overViewConfig.isReadOnly;
            }
            if (this.overViewConfig.hasProjectAdminRole) {
                disableAdminFields = false;
            }
        } /* else if (!this.hasTeams) {

        } */ //128
        disableFields = this.overViewConfig.isReadOnly;
        this.disableOptions = disableFields;
        /* if (!this.overViewConfig.selectedProject) {
            this.disableOptions = false;
        }  */
        if (this.overViewConfig.selectedProject && !this.overViewConfig.selectedProject) {
            this.disableOptions = false;
        }
        if (this.overViewConfig.selectedCampaign && !this.overViewConfig.selectedCampaign) {
            this.disableOptions = false;
        }
        this.disableStatusOptions = this.disableOptions;
        if (this.overViewConfig.isCampaign) {
            if (this.overViewConfig.selectedCampaign) {
                var campaign = this.overViewConfig.selectedCampaign;
                this.selectedStatus = (campaign.R_PO_STATUS && campaign.R_PO_STATUS['MPM_Status-id']) ? campaign.R_PO_STATUS['MPM_Status-id'].Id : '';
                var selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.selectedStatus, this.overViewConfig.statusOptions);
                if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
                    this.overViewConfig.isReadOnly = true;
                    this.isReadOnly = this.overViewConfig.isReadOnly;
                    disableFields = true;
                    this.disableStatusOptions = true;
                }
                this.selectedPriority = (campaign.R_PO_PRIORITY && campaign.R_PO_PRIORITY['MPM_Priority-id']) ? campaign.R_PO_PRIORITY['MPM_Priority-id'].Id : '';
                this.disableStartDate = disableFields || (selectedStatusInfo && selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.INTERMEDIATE);
                this.disableEndDate = disableFields;
                var numericeVlalue_1 = this.getMaxMinValueForNumber(0);
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl({ value: campaign.CAMPAIGN_NAME, disabled: disableFields }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    owner: new FormControl({
                        value: this.formUserDetails({
                            UserId: this.projectOwnerDetails.userCN,
                            FullName: this.projectOwnerDetails.fullName
                        }, this.projectOwnerDetails.userCN),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    startDate: new FormControl({
                        value: new Date(campaign.CAMPAIGN_START_DATE), disabled: (disableFields ||
                            (selectedStatusInfo && selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.INTERMEDIATE))
                    }, [Validators.required]),
                    endDate: new FormControl({ value: new Date(campaign.CAMPAIGN_END_DATE), disabled: disableFields }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(campaign.DESCRIPTION) ? '' : campaign.DESCRIPTION, disabled: disableFields }, [Validators.maxLength(1000)]),
                    category: new FormControl({ value: campaign.R_PO_CATEGORY['MPM_Category-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                    priority: new FormControl({ value: this.selectedPriority, disabled: disableFields }, [Validators.required]),
                    status: new FormControl({ value: this.selectedStatus, disabled: this.disableStatusOptions }, [Validators.required]),
                    /*
                   impoment requestor logic for ideation integration
                   requester: new FormControl({
                       value: (project.R_PO_REQUESTER_USER &&
                           project.R_PO_REQUESTER_USER.UserId ? project.R_PO_REQUESTER_USER.UserId : '') || 'NA',
                       disabled: true
                   }),
                   */
                    standardStatus: new FormControl({ value: 'true' }, [Validators.required]),
                });
                this.selectedStartDate = new Date(campaign.CAMPAIGN_START_DATE);
                this.selectedEndDate = new Date(campaign.CAMPAIGN_END_DATE);
                this.getProjectOTMMAsset(MPM_LEVELS.CAMPAIGN);
            }
            else {
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl('New Campaign', [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    owner: new FormControl({
                        value: this.formUserDetails(this.overViewConfig.currentUserInfo, this.overViewConfig.currentUserId),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    status: new FormControl(this.overViewConfig.statusOptions && this.overViewConfig.statusOptions[0] &&
                        this.overViewConfig.statusOptions[0]['MPM_Status-id'] ? this.overViewConfig.statusOptions[0]['MPM_Status-id'].Id : ''),
                    priority: new FormControl(this.overViewConfig.priorityOptions && this.overViewConfig.priorityOptions[0] &&
                        this.overViewConfig.priorityOptions[0]['MPM_Priority-id'] ? this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id : ''),
                    startDate: new FormControl('', [Validators.required]),
                    endDate: new FormControl('', [Validators.required]),
                    description: new FormControl('', [Validators.maxLength(1000)]),
                    category: new FormControl('')
                });
                this.getMetaData(MPM_LEVELS.CAMPAIGN, null);
            }
        }
        else if (this.overViewConfig.isProject) {
            if (this.overViewConfig.selectedProject) {
                var project = this.overViewConfig.selectedProject;
                if (this.overViewConfig.selectedProject.PROJECT_TEMPLATE_REF_ID && typeof (this.overViewConfig.selectedProject.PROJECT_TEMPLATE_REF_ID) === 'string') {
                    this.overViewConfig.showRequesterField = true;
                }
                /* impolement cancel disable logic */
                this.selectedStatus = (project.R_PO_STATUS && project.R_PO_STATUS['MPM_Status-id']) ? project.R_PO_STATUS['MPM_Status-id'].Id : '';
                var selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.selectedStatus, this.overViewConfig.statusOptions);
                // to have the old status type
                this.oldStatusInfo = selectedStatusInfo;
                if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
                    this.overViewConfig.isReadOnly = true;
                    this.isReadOnly = this.overViewConfig.isReadOnly;
                    disableFields = true;
                    this.disableStatusOptions = true;
                }
                if (selectedStatusInfo.TYPE === StatusTypes.ONHOLD) {
                    this.overViewConfig.isReadOnly = true;
                    this.isReadOnly = this.overViewConfig.isReadOnly;
                    disableFields = true;
                }
                this.selectedCampaignId = (project.R_PO_CAMPAIGN && project.R_PO_CAMPAIGN['Campaign-id']) ? project.R_PO_CAMPAIGN['Campaign-id'].Id : this.campaignIds ? this.campaignIds : '';
                this.selectedPriority = (project.R_PO_PRIORITY && project.R_PO_PRIORITY['MPM_Priority-id']) ? project.R_PO_PRIORITY['MPM_Priority-id'].Id : '';
                this.disableStartDate = disableFields || (selectedStatusInfo && selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.INTERMEDIATE);
                this.disableEndDate = disableFields;
                /* this.getCampaignById(this.selectedCampaignId).subscribe(response => { */
                var numericeVlalue_2 = this.getMaxMinValueForNumber(0);
                var categoryMetadatavalue = (project && project.R_PO_CATEGORY_METADATA) ?
                    this.formCategoryMetadata(project.R_PO_CATEGORY_METADATA['MPM_Category_Metadata-id'].Id) : '';
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl({ value: project.PROJECT_NAME, disabled: disableFields }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    categoryMetadata: new FormControl({ value: categoryMetadatavalue, disabled: true }),
                    owner: new FormControl({
                        value: this.formUserDetails({
                            UserId: this.projectOwnerDetails.userCN,
                            FullName: this.projectOwnerDetails.fullName
                        }, this.projectOwnerDetails.userCN),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    campaign: new FormControl({ value: this.formCampaign(this.selectedCampaignId), disabled: true }),
                    campaignOwner: new FormControl({
                        value: this.campaignOwnerDetails ? this.formUserDetails({
                            UserId: this.campaignOwnerDetails.userCN,
                            FullName: this.campaignOwnerDetails.fullName
                        }, this.campaignOwnerDetails.userCN) : '', disabled: true
                    }),
                    startDate: new FormControl({
                        value: new Date(project.START_DATE), disabled: (disableFields ||
                            (selectedStatusInfo && selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.INTERMEDIATE))
                    }, [Validators.required]),
                    endDate: new FormControl({ value: new Date(project.DUE_DATE), disabled: disableFields }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(project.DESCRIPTION) ? '' : project.DESCRIPTION, disabled: disableFields }, [Validators.maxLength(1000)]),
                    teams: new FormControl({ value: project.R_PO_TEAM['MPM_Teams-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                    category: new FormControl({ value: project.R_PO_CATEGORY['MPM_Category-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                    priority: new FormControl({ value: this.selectedPriority, disabled: disableFields }, [Validators.required]),
                    status: new FormControl({ value: this.selectedStatus, disabled: this.disableStatusOptions }, [Validators.required]),
                    /* expectedDuration: new FormControl(project.EXPECTED_DURATION, [Validators.pattern('^[0-9]*$')]), */
                    expectedDuration: new FormControl({ value: this.utilService.isNullOrEmpty(project.EXPECTED_DURATION) ? '' : project.EXPECTED_DURATION, disabled: disableFields }, [Validators.max(numericeVlalue_2), Validators.min(-(numericeVlalue_2))]),
                    /*
                    impoment requestor logic for ideation integration
                    requester: new FormControl({
                        value: (project.R_PO_REQUESTER_USER &&
                            project.R_PO_REQUESTER_USER.UserId ? project.R_PO_REQUESTER_USER.UserId : '') || 'NA',
                        disabled: true
                    }),
                    */
                    standardStatus: new FormControl({ value: 'true' }, [Validators.required]),
                    isCustomWorkflow: new FormControl({ value: this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW), disabled: true })
                });
                this.selectedStartDate = new Date(project.START_DATE);
                this.selectedEndDate = new Date(project.DUE_DATE);
                this.getProjectOTMMAsset(MPM_LEVELS.PROJECT);
                /*   }) */ ;
                /*
                                const numericeVlalue = this.getMaxMinValueForNumber(0);
                                const categoryMetadatavalue = (project && project.R_PO_CATEGORY_METADATA) ?
                                    this.formCategoryMetadata(project.R_PO_CATEGORY_METADATA['MPM_Category_Metadata-id'].Id) : '';
                                this.projectOverviewForm = this.fb.group({
                                    name: new FormControl({ value: project.PROJECT_NAME, disabled: disableFields },
                                        [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                                    categoryMetadata: new FormControl({ value: categoryMetadatavalue, disabled: true }),
                                    owner: new FormControl({
                                        value: this.formUserDetails({
                                            UserId: this.projectOwnerDetails.userCN,
                                            FullName: this.projectOwnerDetails.fullName
                                        }, this.projectOwnerDetails.userCN),
                                        disabled: disableAdminFields
                                    }, [Validators.required]),
                                    campaign: new FormControl({ value: this.formCampaign(this.selectedCampaignId), disabled: true }),
                                    campaignOwner: new FormControl({
                                        value: this.campaignOwnerDetails ? this.formUserDetails({
                                            UserId: this.campaignOwnerDetails.userCN,
                                            FullName: this.campaignOwnerDetails.fullName
                                        }, this.campaignOwnerDetails.userCN) : '', disabled: true
                                    }),
                                    startDate: new FormControl({
                                        value: new Date(project.START_DATE), disabled: (disableFields ||
                                            (selectedStatusInfo && selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.INTERMEDIATE))
                                    }, [Validators.required]),
                                    endDate: new FormControl({ value: new Date(project.DUE_DATE), disabled: disableFields }, [Validators.required]),
                                    description: new FormControl({ value: this.utilService.isNullOrEmpty(project.DESCRIPTION) ? '' : project.DESCRIPTION, disabled: disableFields }, [Validators.maxLength(1000)]),
                                    teams: new FormControl({ value: project.R_PO_TEAM['MPM_Teams-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                                    category: new FormControl({ value: project.R_PO_CATEGORY['MPM_Category-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                                    priority: new FormControl({ value: this.selectedPriority, disabled: disableFields }, [Validators.required]),
                                    status: new FormControl({ value: this.selectedStatus, disabled: this.disableStatusOptions }, [Validators.required]),
                                   expectedDuration: new FormControl({ value: this.utilService.isNullOrEmpty(project.EXPECTED_DURATION) ? '' : project.EXPECTED_DURATION, disabled: disableFields },
                                        [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))]),
                                   
                                    standardStatus: new FormControl({ value: 'true' }, [Validators.required]),
                                    isCustomWorkflow: new FormControl({ value: this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW), disabled: true })
                                });
                                this.selectedStartDate = new Date(project.START_DATE);
                                this.selectedEndDate = new Date(project.DUE_DATE);
                                this.getProjectOTMMAsset(MPM_LEVELS.PROJECT); */
            }
            else {
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl('New Project', [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    categoryMetadata: new FormControl({ value: '', disabled: false }),
                    campaign: new FormControl(),
                    campaignOwner: new FormControl({ value: '', disabled: true }),
                    owner: new FormControl({
                        value: this.isCampaignProject ? '' : this.formUserDetails(this.overViewConfig.currentUserInfo, this.overViewConfig.currentUserId),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    //128
                    /* owner: new FormControl({
                        value: !this.isCampaignProject ? '' : this.formUserDetails(this.overViewConfig.currentUserInfo, this.overViewConfig.currentUserId),
                        disabled: disableAdminFields
                    }, [Validators.required]), */
                    teams: new FormControl(''),
                    category: new FormControl(''),
                    status: new FormControl(this.overViewConfig.statusOptions && this.overViewConfig.statusOptions[0] &&
                        this.overViewConfig.statusOptions[0]['MPM_Status-id'] ? this.overViewConfig.statusOptions[0]['MPM_Status-id'].Id : ''),
                    priority: new FormControl(this.overViewConfig.priorityOptions && this.overViewConfig.priorityOptions[0] &&
                        this.overViewConfig.priorityOptions[0]['MPM_Priority-id'] ? this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id : ''),
                    startDate: new FormControl('', [Validators.required]),
                    endDate: new FormControl('', [Validators.required]),
                    description: new FormControl('', [Validators.maxLength(1000)]),
                    expectedDuration: new FormControl('', [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))]),
                    standardStatus: new FormControl('true'),
                    isCustomWorkflow: new FormControl({ value: this.enableCustomWorkflow, disabled: !this.enableCustomWorkflow })
                });
                this.getMetaData(MPM_LEVELS.PROJECT, null);
            }
        }
        else {
            if (this.overViewConfig.selectedProject) {
                this.disableStartDate = disableFields;
                this.disableEndDate = disableFields;
                // TODO: Edit project Template logic
                var project = this.overViewConfig.selectedProject;
                if (this.overViewConfig.selectedProject.PROJECT_TEMPLATE_REF_ID && typeof (this.overViewConfig.selectedProject.PROJECT_TEMPLATE_REF_ID) === 'string') {
                    this.overViewConfig.showRequesterField = true;
                }
                /* impolement cancel disable logic */
                this.selectedStatus = (project.R_PO_STATUS && project.R_PO_STATUS['MPM_Status-id']) ? project.R_PO_STATUS['MPM_Status-id'].Id : '';
                this.selectedPriority = (project.R_PO_PRIORITY && project.R_PO_PRIORITY['MPM_Priority-id']) ? project.R_PO_PRIORITY['MPM_Priority-id'].Id : '';
                var categoryMetadatavalue = (project && project.R_PO_CATEGORY_METADATA) ?
                    this.formCategoryMetadata(project.R_PO_CATEGORY_METADATA['MPM_Category_Metadata-id'].Id) : '';
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl({ value: project.PROJECT_NAME, disabled: disableFields }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    categoryMetadata: new FormControl({ value: categoryMetadatavalue, disabled: true }),
                    owner: new FormControl({
                        value: this.formUserDetails({
                            UserId: this.projectOwnerDetails.userCN,
                            FullName: this.projectOwnerDetails.fullName
                        }, this.projectOwnerDetails.userCN),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    startDate: new FormControl({ value: project.START_DATE, disabled: disableFields }),
                    endDate: new FormControl({ value: project.DUE_DATE, disabled: disableFields }),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(project.DESCRIPTION) ? '' : project.DESCRIPTION, disabled: disableFields }, [Validators.maxLength(1000)]),
                    teams: new FormControl({ value: project.R_PO_TEAM['MPM_Teams-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                    category: new FormControl({ value: project.R_PO_CATEGORY['MPM_Category-id'].Id, disabled: this.disableEditOptions }, [Validators.required]),
                    status: new FormControl({ value: this.selectedStatus, disabled: this.disableStatusOptions }, [Validators.required]),
                    priority: new FormControl({ value: this.selectedPriority, disabled: disableFields }, [Validators.required]),
                    expectedDuration: new FormControl({ value: this.utilService.isNullOrEmpty(project.EXPECTED_DURATION) ? '' : project.EXPECTED_DURATION, disabled: disableFields }, [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))]),
                    /* expectedDuration: new FormControl(project.EXPECTED_DURATION, [Validators.pattern('^[0-9]*$')]), */
                    /*
                    impoment requestor logic for ideation integration
                    requester: new FormControl({
                        value: (project.R_PO_REQUESTER_USER &&
                            project.R_PO_REQUESTER_USER.UserId ? project.R_PO_REQUESTER_USER.UserId : '') || 'NA',
                        disabled: true
                    }),
                    */
                    standardStatus: new FormControl({ value: 'true' }, [Validators.required]),
                    isCustomWorkflow: new FormControl({
                        value: this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW),
                        disabled: this.enableCustomWorkflow ? (this.utilService.getBooleanValue(project.IS_CUSTOM_WORKFLOW)) : true
                    })
                });
                this.selectedStartDate = project.START_DATE;
                this.selectedEndDate = project.DUE_DATE;
                this.getProjectOTMMAsset(MPM_LEVELS.PROJECT);
            }
            else {
                this.projectOverviewForm = this.fb.group({
                    name: new FormControl('New Template', [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    categoryMetadata: new FormControl({ value: '', disabled: false }),
                    owner: new FormControl({
                        value: this.formUserDetails(this.overViewConfig.currentUserInfo, this.overViewConfig.currentUserId),
                        disabled: disableAdminFields
                    }, [Validators.required]),
                    teams: new FormControl(''),
                    category: new FormControl(''),
                    status: new FormControl({
                        value: this.overViewConfig.statusOptions && this.overViewConfig.statusOptions[0] &&
                            this.overViewConfig.statusOptions[0]['MPM_Status-id'] ? this.overViewConfig.statusOptions[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    priority: new FormControl(this.overViewConfig.priorityOptions && this.overViewConfig.priorityOptions[0] &&
                        this.overViewConfig.priorityOptions[0]['MPM_Priority-id'] ? this.overViewConfig.priorityOptions[0]['MPM_Priority-id'].Id : ''),
                    description: new FormControl('', [Validators.maxLength(1000)]),
                    standardStatus: new FormControl('true'),
                    isCustomWorkflow: new FormControl({ value: this.enableCustomWorkflow, disabled: !this.enableCustomWorkflow })
                });
                this.getMetaData(MPM_LEVELS.PROJECT, null);
            }
        }
        if (this.selectedCampaignId && !this.projectOverviewForm.getRawValue().campaign && this.projectOverviewForm.controls.campaign) {
            this.projectOverviewForm.controls.campaign.setValue(this.formCampaign(this.selectedCampaignId));
        }
        this.overViewConfig.ownerFieldConfig.formControl = this.projectOverviewForm.controls.owner;
        this.overViewConfig.campaignOwnerFieldConfig.formControl = this.projectOverviewForm.controls.campaignOwner;
        this.overViewConfig.campaignDataConfig.formControl = this.projectOverviewForm.controls.campaign;
        this.overViewConfig.categoryMetadataConfig.formControl = this.projectOverviewForm.controls.categoryMetadata;
        if (this.selectedCampaignId && this.projectOverviewForm.controls.campaign && this.projectOverviewForm.controls.campaign.status !== 'DISABLED') {
            this.projectOverviewForm.controls.campaign.disable();
        }
        if (this.projectOverviewForm.controls.campaign) {
            this.projectOverviewForm.controls.campaign.valueChanges.subscribe(function (campaign) {
                if (campaign) {
                    // 128
                    _this.selectedCampaign = _this.campaignList ? _this.campaignList.find(function (data) { return data['Campaign-id'].Id === campaign.value; }) : '';
                    if (_this.selectedCampaign) {
                        _this.loadingHandler.next(true);
                        _this.minStartDate = _this.datePipe.transform(_this.selectedCampaign.CAMPAIGN_START_DATE, 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd') ? _this.selectedCampaign.CAMPAIGN_START_DATE : new Date();
                        _this.getCampaignOwnerDetailsId(_this.selectedCampaign.R_PO_CAMPAIGN_OWNER['Identity-id'].Id).subscribe(function (response) {
                            _this.loadingHandler.next(false);
                            _this.projectOverviewForm.controls.campaignOwner.setValue(_this.formUserDetails({
                                UserId: _this.campaignOwnerDetails.userCN,
                                FullName: _this.campaignOwnerDetails.fullName
                            }, _this.campaignOwnerDetails.userCN));
                        });
                    }
                    else {
                        _this.projectOverviewForm.controls.campaignOwner.setValue('');
                    }
                }
                else {
                    _this.selectedCampaign = null;
                    _this.projectOverviewForm.controls.campaignOwner.setValue('');
                }
            });
        }
        if (this.projectOverviewForm.controls.startDate) {
            this.projectOverviewForm.controls.startDate.valueChanges.subscribe(function () {
                if (_this.selectedCampaign) {
                    if ((new Date(_this.projectOverviewForm.controls.startDate.value) < new Date(_this.selectedCampaign.CAMPAIGN_START_DATE) ||
                        new Date(_this.projectOverviewForm.controls.startDate.value) > new Date(_this.selectedCampaign.CAMPAIGN_END_DATE)) &&
                        _this.datePipe.transform(new Date(_this.projectOverviewForm.controls.startDate.value), 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                        _this.startDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                    }
                    else {
                        _this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                    }
                }
                else {
                    _this.startDateErrorMessage = ProjectConstant.START_DATE_ERROR_MESSAGE;
                }
            });
        }
        if (this.projectOverviewForm.controls.endDate) {
            this.projectOverviewForm.controls.endDate.valueChanges.subscribe(function () {
                if (_this.selectedCampaign) {
                    if (new Date(_this.projectOverviewForm.controls.endDate.value) < new Date(_this.selectedCampaign.CAMPAIGN_START_DATE) ||
                        new Date(_this.projectOverviewForm.controls.endDate.value) > new Date(_this.selectedCampaign.CAMPAIGN_END_DATE)) {
                        _this.endDateErrorMessage = ProjectConstant.PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE;
                    }
                    else {
                        _this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                    }
                }
                else {
                    if (_this.isCampaign) {
                        _this.endDateErrorMessage = ProjectConstant.CAMPAIGN_START_DATE_ERROR_MESSAGE;
                    }
                    else {
                        _this.endDateErrorMessage = ProjectConstant.PROJECT_START_DATE_ERROR_MESSAGE;
                    }
                }
            });
        }
        // 128
        /*  if (this.projectOverviewForm.controls.teams && this.projectOverviewForm.controls.owner && this.isCampaignProject) { */
        if (this.projectOverviewForm.controls.teams && this.projectOverviewForm.controls.owner && (this.isCampaignProject || this.isCampaignManager && !this.hasTeams)) {
            this.projectOverviewForm.controls.teams.valueChanges.subscribe(function (team) {
                if (team) {
                    _this.selectedTeam = _this.overViewConfig.teamsOptions.find(function (data) { return data['MPM_Teams-id'].Id === team; });
                    if (_this.selectedTeam) {
                        _this.loadingHandler.next(true);
                        _this.getUsersForTeamRole().subscribe(function (response) {
                            _this.projectOverviewForm.controls.owner.enable();
                            _this.loadingHandler.next(false);
                        });
                    }
                    _this.projectOverviewForm.controls.owner.setValue('');
                }
                else {
                    _this.projectOverviewForm.controls.owner.setValue('');
                }
            });
        }
        if (this.projectOverviewForm.controls.categoryMetadata) {
            this.projectOverviewForm.controls.categoryMetadata.valueChanges.subscribe(function (data) {
                if (data) {
                    _this.getMetaData(MPM_LEVELS.PROJECT, data.value);
                }
            });
        }
        /* if (this.projectOverviewForm.controls.category) {
            this.projectOverviewForm.controls.category.valueChanges.subscribe(category => {
                if (category) {
                    this.loaderService.show();
                    this.categoryService.getCategoryDetails(category).subscribe(response => {
                        this.sharingService.setCategoryDetails(response);
                        const currentCategoryLevel = this.isCampaign ? MPM_LEVELS.CAMPAIGN : MPM_LEVELS.PROJECT;
                        const categoryLevels = response && response.categoryLevels && response.categoryLevels.categoryLevel ? response.categoryLevels.categoryLevel : [];
                        const selectedCategoryLevel = categoryLevels.find(categoryLevel => categoryLevel.categoryLevelType === currentCategoryLevel);
                        const priorityList = selectedCategoryLevel.levelPriority.Priority;
                        this.overViewConfig.priorityOptions = [];
                    });
                }
            });
        } */
    };
    ProjectOverviewComponent.prototype.getProjectOTMMAsset = function (categoryLevel) {
        var _this = this;
        if (this.overViewConfig.selectedCampaign && this.overViewConfig.selectedCampaign.OTMM_FOLDER_ID && typeof this.overViewConfig.selectedCampaign.OTMM_FOLDER_ID === 'string') {
            this.otmmService.getOTMMFodlerById(this.overViewConfig.selectedCampaign.OTMM_FOLDER_ID)
                .subscribe(function (folderDetails) {
                if (folderDetails && folderDetails.metadata) {
                    _this.overViewConfig.metaDataValues = folderDetails.metadata;
                }
                if (_this.overViewConfig.selectedCampaign && _this.overViewConfig.selectedCampaign.R_PO_CATEGORY
                    && _this.overViewConfig.selectedCampaign.R_PO_CATEGORY['MPM_Category-id']
                    && _this.overViewConfig.selectedCampaign.R_PO_CATEGORY['MPM_Category-id'].Id) {
                    _this.getMetaData(categoryLevel, null);
                }
            });
        }
        else if (this.overViewConfig.selectedProject && this.overViewConfig.selectedProject.OTMM_FOLDER_ID && typeof this.overViewConfig.selectedProject.OTMM_FOLDER_ID === 'string') {
            this.otmmService.getOTMMFodlerById(this.overViewConfig.selectedProject.OTMM_FOLDER_ID)
                .subscribe(function (folderDetails) {
                if (folderDetails && folderDetails.metadata) {
                    _this.overViewConfig.metaDataValues = folderDetails.metadata;
                }
                if (_this.overViewConfig.selectedProject && _this.overViewConfig.selectedProject.R_PO_CATEGORY
                    && _this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id']
                    && _this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id'].Id) {
                    var categoryMetadataID = (_this.overViewConfig.selectedProject && _this.overViewConfig.selectedProject.R_PO_CATEGORY_METADATA) ?
                        _this.overViewConfig.selectedProject.R_PO_CATEGORY_METADATA['MPM_Category_Metadata-id'].Id : null;
                    _this.getMetaData(categoryLevel, categoryMetadataID);
                }
            });
        }
        else {
            this.getMetaData(categoryLevel, null);
        }
    };
    ProjectOverviewComponent.prototype.onCustomMetadataFieldChange = function (event) {
        this.projectOverviewForm.get('CustomFieldGroup')['controls'].forEach(function (formGroup) {
            formGroup['controls']['fieldset'].updateValueAndValidity();
        });
    };
    ProjectOverviewComponent.prototype.getProjectDetail = function (projectId) {
        var _this = this;
        var getRequestObject = {
            projectID: projectId
        };
        this.loadingHandler.next(true);
        forkJoin([this.appService.invokeRequest(this.PROJECT_DETAILS_METHOD_NS, this.GET_PROJECT_DETAILS_WS_METHOD_NAME, getRequestObject) /*, this.projectService.getProjectDataValues(projectId)*/])
            .subscribe(function (response) {
            if (response && response[0].Project) {
                var project = response[0].Project;
                _this.overViewConfig.selectedProject = project;
                var projectStatuses = _this.statusService.getAllStatusBycategoryName(MPM_LEVELS.PROJECT);
                var projectStatus = projectStatuses && projectStatuses.length > 0 ? projectStatuses[0] : projectStatuses;
                //  const initialProjectStatus = projectStatuses.find(status => status.STATUS_TYPE === StatusTypes.INITIAL);
                var initialProjectStatus = projectStatus.find(function (status) { return status.STATUS_TYPE === StatusTypes.INITIAL; });
                if (initialProjectStatus['MPM_Status-id'].Id !== _this.overViewConfig.selectedProject.R_PO_STATUS['MPM_Status-id'].Id && _this.enableProjectRestartConfig) {
                    _this.enableRestartProject = true;
                }
                _this.projectService.setUpdatedProjectInfo(project);
                //this.overViewConfig.dateValidation = response[1];
                _this.getFilters();
            }
            else {
                //
                _this.loadingHandler.next(false);
            }
        }, function (error) {
            _this.loadingHandler.next(false);
            var eventData = { message: 'Something went wrong while fetching project details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            _this.notificationHandler.next(eventData);
        });
    };
    ProjectOverviewComponent.prototype.getCampaignDetail = function (campaignId) {
        var _this = this;
        var getRequestObject = {
            CampaignId: campaignId
        };
        this.loadingHandler.next(true);
        forkJoin([this.appService.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_CAMPAIGN_WS_METHOD_NAME, getRequestObject) /* , this.projectService.getProjectDataValues(projectId) */
        ])
            .subscribe(function (response) {
            if (response && response[0].Campaign) {
                var campaign = response[0].Campaign;
                _this.overViewConfig.selectedCampaign = campaign;
                /* this.projectService.setUpdatedProjectInfo(campaign);
                this.overViewConfig.dateValidation = response[1];*/
                _this.getFilters();
            }
            else {
                //
                _this.loadingHandler.next(false);
            }
        }, function (error) {
            _this.loadingHandler.next(false);
            var eventData = { message: 'Something went wrong while fetching campaign details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            _this.notificationHandler.next(eventData);
        });
    };
    // 201
    ProjectOverviewComponent.prototype.getCampaignById = function (campaignId) {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                CampaignId: campaignId
            };
            _this.appService.invokeRequest(_this.CAMPAIGN_DETAILS_METHOD_NS, _this.GET_CAMPAIGN_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response.Campaign) {
                    if (!Array.isArray(response.Campaign)) {
                        response.Campaign = [response.Campaign];
                    }
                    _this.campaignList = response.Campaign;
                    var campaignFilterList_3 = [];
                    _this.campaignList.map(function (campaign) {
                        var value = _this.formCampaignValue(campaign);
                        if (campaign.CAMPAIGN_NAME && campaign['Campaign-id'] && campaign['Campaign-id'].Id &&
                            _this.datePipe.transform(campaign.CAMPAIGN_END_DATE, 'yyyy-MM-dd') >= _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                            campaignFilterList_3.push({
                                name: campaign.CAMPAIGN_NAME,
                                value: campaign['Campaign-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    _this.overViewConfig.campaignDataConfig.filterOptions = campaignFilterList_3;
                }
                else {
                    _this.campaignList = [];
                    _this.overViewConfig.campaignDataConfig.filterOptions = [];
                }
                observer.next(_this.overViewConfig.campaignDataConfig.filterOptions);
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.getUsersRole = function (getRequestObject) {
        return this.utilService.currentUserHasRole(getRequestObject);
    };
    ProjectOverviewComponent.prototype.formUserDetails = function (userObj, userId) {
        if (userObj && userObj.UserDisplayName && userId && userId !== '') {
            return {
                name: userObj.UserDisplayName,
                value: userId,
                displayName: userObj.UserDisplayName + ' (' + userId + ')'
            };
        }
        else if (userObj && userObj.UserId && userObj.UserId !== '') {
            return {
                name: userObj.Description || userObj.FullName,
                value: userObj.UserId,
                displayName: (userObj.Description || userObj.FullName) + ' (' + userObj.UserId + ')'
            };
        }
        else {
            return null;
        }
    };
    ProjectOverviewComponent.prototype.formCategoryMetadata = function (metadataId) {
        var selectedCategoryMetadata = this.overViewConfig.categoryMetadataConfig.filterOptions.find(function (categoryMetadata) { return categoryMetadata.value === metadataId; });
        return selectedCategoryMetadata ? selectedCategoryMetadata : null;
    };
    ProjectOverviewComponent.prototype.initialiseOverviewConfig = function () {
        this.overViewConfig = {
            isProject: true,
            formType: null,
            statusOptions: null,
            priorityOptions: null,
            teamsOptions: null,
            // categoryOptions: null,
            currentUserInfo: this.sharingService.getCurrentUserObject(),
            hasProjectAdminRole: false,
            currentUserId: this.sharingService.getCurrentUserID(),
            currentUserItemId: this.sharingService.getCurrentUserItemID(),
            projectOwners: null,
            selectedProject: null,
            metaDataValues: null,
            isReadOnly: true,
            showRequesterField: false,
            ownerFieldConfig: {
                label: 'Owner',
                filterOptions: [],
                formControl: null
            },
            campaignOwnerFieldConfig: {
                label: 'Campaign Owner',
                filterOptions: [],
                formControl: null
            },
            campaignDataConfig: {
                label: 'Campaign Name',
                filterOptions: [],
                formControl: null
            },
            categoryMetadataConfig: {
                formControl: null,
                label: 'Category Metadata',
                filterOptions: []
            },
            dateValidation: null,
        };
    };
    ProjectOverviewComponent.prototype.cancelProjectCreation = function () {
        this.confirmCancel();
    };
    ProjectOverviewComponent.prototype.updateProject = function () {
        var _this = this;
        // if existing project
        this.loaderService.show();
        this.entityAppDefService.getStatusById(this.projectOverviewForm.getRawValue().status)
            .subscribe(function (projectStatus) {
            if (projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED || projectStatus.TYPE === ProjectConstant.STATUS_TYPE_ONHOLD || (_this.oldStatusInfo && _this.oldStatusInfo.TYPE === 'ONHOLD' && projectStatus.TYPE === ProjectConstant.STATUS_TYPE_IN_PROGRESS)) {
                _this.loaderService.hide();
                /* const dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'Are you sure you want to cancel the project?',
                        hasConfirmationComment: true,
                        commentText: 'Comments',
                        isCommentRequired: true,
                        errorMessage: 'Kindly fill the comments field',
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                }); */
                var confirmationMessage_1 = projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? 'Are you sure you want to cancel the project?' : projectStatus.TYPE === ProjectConstant.STATUS_TYPE_ONHOLD ? 'Are you sure you want to put the project onHold?' : (_this.oldStatusInfo && _this.oldStatusInfo.TYPE === 'ONHOLD' && projectStatus.TYPE === ProjectConstant.STATUS_TYPE_IN_PROGRESS) ? 'Are you sure you want to resume the project?' : '';
                _this.statusService.GetStatusReasonByStatusId(projectStatus['MPM_Status-id'].Id).subscribe(function (statusReason) {
                    console.log(statusReason);
                    projectStatus['MPM_Status_Reason'] = statusReason;
                    var dialogRef = _this.dialog.open(BulkCommentsComponent, {
                        width: '40%',
                        disableClose: true,
                        data: {
                            // message: 'Are you sure you want to cancel the project?',
                            message: confirmationMessage_1,
                            hasConfirmationComment: true,
                            commentText: 'Comments',
                            isCommentRequired: true,
                            errorMessage: 'Kindly fill the comments field',
                            submitButton: 'Yes',
                            cancelButton: 'No',
                            statusData: projectStatus,
                            statusMessage: 'Project Status'
                            // isStatusReason: true
                        }
                    });
                    dialogRef.afterClosed().subscribe(function (result) {
                        /*  console.log(result);
                         this.loaderService.show();
                         if (result && result !== 'true') {
                             this.loaderService.show();
                             // this.loadingHandler.next(true);
                             console.log(result);
                             this.cancelComment = result.comment;
                             this.reasons = result.SelectedReasons;
                             const successMessage = projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? 'Project cancellation is in progress and it might take some time to complete' : projectStatus.NAME ? 'Project is moved to ' + projectStatus.NAME + ' state' : '';
                             //  this.notificationService.success('Project ' + projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? 'cancellation' : projectStatus.NAME + ' process is in progress and it might take some time to complete');
                             this.notificationService.success(successMessage);
                             if (this.projectOverviewForm.status === 'VALID') {
                                 this.disableProjectSave = true;
                                 if (this.projectOverviewForm.controls.name.value.trim() === '') {
                                     let eventData;
                                     if (this.isCampaign) {
                                         eventData = { message: 'Please provide a valid campaign name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                     } else if (this.isTemplate) {
                                         eventData = { message: 'Please provide a valid template name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                     } else {
                                         eventData = { message: 'Please provide a valid project name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                     }
                                     this.notificationHandler.next(eventData);
                                     this.disableProjectSave = false;
                                 } else {
                                     this.projectOverviewForm.controls.name.setValue(this.projectOverviewForm.controls.name.value.trim());
                                     if (this.overViewConfig.selectedProject) {
                                         // TODO: For Edit Project checkProjectOwnerChange
                                         this.updateProjectDetails();
                                         //  this.loaderService.hide();
                                     } else {
                                         this.getProjectOwnerDetails(this.projectOverviewForm.controls.owner.value.value)
                                             .subscribe(
                                                 success => {
                                                     this.updateProjectDetails();
                                                 }
                                             );
                                     }
                                 }
                             } else {
                                 const eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                 this.notificationHandler.next(eventData);
                                 this.disableProjectSave = false;
                                 this.loadingHandler.next(false);
                                 this.loaderService.hide();
                             }
                         } else {
                             this.loaderService.hide();
                         }
                     }); */
                        console.log(result);
                        _this.loaderService.show();
                        if (result && result !== 'true') {
                            _this.loaderService.show();
                            // this.loadingHandler.next(true);
                            console.log(result);
                            //      this.cancelComment = result.comment;
                            //   this.reasons = result.SelectedReasons;
                            var reason_1 = '';
                            result.SelectedReasons.forEach(function (selectedReason, index) {
                                var selectedStatusReason = statusReason.MPM_Status_Reason.find(function (statusReason) { return statusReason['MPM_Status_Reason-id'].Id === selectedReason.Id; });
                                reason_1 = reason_1 + selectedStatusReason.NAME;
                                if (index < (result.SelectedReasons.length - 1)) {
                                    reason_1 = reason_1 + ',';
                                }
                            });
                            var newComment = void 0;
                            if (reason_1) {
                                newComment = reason_1 + ' : ' + result.comment;
                            }
                            else {
                                newComment = result.comment;
                            }
                            _this.cancelComment = newComment;
                            var successMessage = projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? 'Project cancellation is in progress and it might take some time to complete' : projectStatus.NAME ? 'Project is moved to ' + projectStatus.NAME + ' state' : '';
                            //  this.notificationService.success('Project ' + projectStatus.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? 'cancellation' : projectStatus.NAME + ' process is in progress and it might take some time to complete');
                            _this.notificationService.success(successMessage);
                            if (_this.projectOverviewForm.status === 'VALID') {
                                _this.disableProjectSave = true;
                                if (_this.projectOverviewForm.controls.name.value.trim() === '') {
                                    var eventData = void 0;
                                    if (_this.isCampaign) {
                                        eventData = { message: 'Please provide a valid campaign name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                    }
                                    else if (_this.isTemplate) {
                                        eventData = { message: 'Please provide a valid template name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                    }
                                    else {
                                        eventData = { message: 'Please provide a valid project name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                    }
                                    _this.notificationHandler.next(eventData);
                                    _this.disableProjectSave = false;
                                }
                                else {
                                    _this.projectOverviewForm.controls.name.setValue(_this.projectOverviewForm.controls.name.value.trim());
                                    if (_this.overViewConfig.selectedProject) {
                                        // TODO: For Edit Project checkProjectOwnerChange
                                        _this.updateProjectDetails();
                                        //  this.loaderService.hide();
                                    }
                                    else {
                                        _this.getProjectOwnerDetails(_this.projectOverviewForm.controls.owner.value.value)
                                            .subscribe(function (success) {
                                            _this.updateProjectDetails();
                                        });
                                    }
                                }
                            }
                            else {
                                var eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                                _this.notificationHandler.next(eventData);
                                _this.disableProjectSave = false;
                                _this.loadingHandler.next(false);
                                _this.loaderService.hide();
                            }
                        }
                        else {
                            _this.loaderService.hide();
                        }
                    });
                });
            }
            else {
                _this.loadingHandler.next(false);
                if (_this.projectOverviewForm.status === 'VALID') {
                    _this.disableProjectSave = true;
                    if (_this.projectOverviewForm.controls.name.value.trim() === '') {
                        var eventData = void 0;
                        if (_this.isCampaign) {
                            eventData = { message: 'Please provide a valid campaign name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                        }
                        else if (_this.isTemplate) {
                            eventData = { message: 'Please provide a valid template name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                        }
                        else {
                            eventData = { message: 'Please provide a valid project name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                        }
                        _this.notificationHandler.next(eventData);
                        _this.disableProjectSave = false;
                    }
                    else {
                        _this.projectOverviewForm.controls.name.setValue(_this.projectOverviewForm.controls.name.value.trim());
                        if (_this.overViewConfig.selectedProject) {
                            // TODO: For Edit Project checkProjectOwnerChange
                            _this.updateProjectDetails();
                        }
                        else {
                            _this.getProjectOwnerDetails(_this.projectOverviewForm.controls.owner.value.value)
                                .subscribe(function (success) {
                                _this.updateProjectDetails();
                            });
                        }
                    }
                }
                else {
                    var eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                    _this.notificationHandler.next(eventData);
                    _this.disableProjectSave = false;
                }
            }
        }, function (projectStatusError) {
            console.log(projectStatusError);
        });
        /* if (this.projectOverviewForm.status === 'VALID') {
            this.disableProjectSave = true;
            if (this.projectOverviewForm.controls.name.value.trim() === '') {
                let eventData;
                if (this.isCampaign) {
                    eventData = { message: 'Please provide a valid campaign name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                } else if (this.isTemplate) {
                    eventData = { message: 'Please provide a valid template name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                } else {
                    eventData = { message: 'Please provide a valid project name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                }
                this.notificationHandler.next(eventData);
                this.disableProjectSave = false;
            } else {
                this.projectOverviewForm.controls.name.setValue(this.projectOverviewForm.controls.name.value.trim());
                if (this.overViewConfig.selectedProject) {
                    // TODO: For Edit Project checkProjectOwnerChange
                    this.updateProjectDetails();
                } else {
                    this.getProjectOwnerDetails(this.projectOverviewForm.controls.owner.value.value)
                        .subscribe(
                            success => {
                                this.updateProjectDetails();
                            }
                        );
                }
            }
        } else {
            const eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
            this.notificationHandler.next(eventData);
            this.disableProjectSave = false;
        } */
    };
    ProjectOverviewComponent.prototype.updateProjectDetails = function () {
        var ProjectObject = this.createNewProjectUpdate();
        this.saveProject(ProjectObject);
    };
    ProjectOverviewComponent.prototype.saveProject = function (ProjectObject) {
        var _this = this;
        this.loadingHandler.next(true);
        var standardStatusValue;
        if (!this.overViewConfig.selectedProject) {
            standardStatusValue = this.projectOverviewForm.value.standardStatus;
        }
        if (this.overViewConfig.selectedProject) {
            this.disableProjectSave = true;
            ProjectObject = this.projectUtilService.compareTwoProjectDetails(ProjectObject, this.oldProjectMetadata);
            if (Object.keys(ProjectObject).length > 1) {
                ProjectObject.BulkOperation = false;
                this.projectService.updateProjectDetails(ProjectObject, this.cancelComment, this.reasons).subscribe(function (response) {
                    var eventData = { message: _this.overViewConfig.formType + ' updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.handleProjectNotification(response, eventData, false, false);
                }, function (error) {
                    var eventData = { message: 'Something went wrong while updating project', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.handleProjectNotification(null, eventData, false, true);
                });
            }
            else {
                this.loadingHandler.next(false);
                this.disableProjectSave = false;
            }
        }
        else {
            if (this.isCampaign) {
                var requestObj = {
                    CampaignObject: ProjectObject
                };
                this.utilService.setProjectCustomMetadata(ProjectObject.CustomMetadata);
                this.appService.invokeRequest(this.PROJECT_BPM_METHOD_NS, this.CREATE_CAMPAIGN_WS_METHOD_NAME, requestObj)
                    .subscribe(function (response) {
                    var eventData = { message: _this.overViewConfig.formType + ' created successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.handleProjectNotification(response, eventData, true, false);
                }, function (error) {
                    var eventData = { message: 'Something went wrong on while creating campaign', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.handleProjectNotification(null, eventData, true, true);
                });
            }
            else {
                var requestObj = {
                    ProjectObject: ProjectObject
                };
                this.utilService.setProjectCustomMetadata(ProjectObject.CustomMetadata);
                this.appService.invokeRequest(this.PROJECT_BPM_METHOD_NS, this.CREATE_PROJECT_WS_METHOD_NAME, requestObj)
                    .subscribe(function (response) {
                    var eventData = { message: _this.overViewConfig.formType + ' created successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.handleProjectNotification(response, eventData, true, false);
                }, function (error) {
                    var eventData = { message: 'Something went wrong on while creating project', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.handleProjectNotification(null, eventData, true, true);
                });
            }
        }
    };
    ProjectOverviewComponent.prototype.restartProject = function () {
        var _this = this;
        this.statusService.getAllStatusReasons().subscribe(function (response) {
            var noteMessage = ((_this.projectOverviewForm && _this.projectOverviewForm.dirty) || (_this.customFieldsGroup && _this.customFieldsGroup.dirty)) ?
                'On restarting project, changes will be discarded.' : null;
            var projectStatus = {
                //REQUIRE_REASON: 'true',
                //REQUIRE_COMMENTS: 'true',
                MPM_Status_Reason: ''
            };
            projectStatus.MPM_Status_Reason = response;
            var dialogRef = _this.dialog.open(BulkCommentsComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Restart project',
                    hasConfirmationComment: true,
                    commentText: 'Comments',
                    isCommentRequired: true,
                    errorMessage: 'Kindly fill the comments field',
                    submitButton: 'Yes',
                    cancelButton: 'No',
                    statusData: projectStatus,
                    status: '',
                    note: noteMessage
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result !== 'true') {
                    var reason_2 = '';
                    result.SelectedReasons.forEach(function (selectedReason, index) {
                        var selectedStatusReason = response.MPM_Status_Reason.find(function (statusReason) { return statusReason['MPM_Status_Reason-id'].Id === selectedReason.Id; });
                        reason_2 = reason_2 + selectedStatusReason.NAME;
                        if (index < (result.SelectedReasons.length - 1)) {
                            reason_2 = reason_2 + ',';
                        }
                    });
                    var newComment = void 0;
                    if (reason_2) {
                        newComment = reason_2 + ' : ' + result.comment;
                    }
                    else {
                        newComment = result.comment;
                    }
                    _this.triggerRestartProject(newComment);
                }
            });
        });
    };
    ProjectOverviewComponent.prototype.triggerRestartProject = function (newComment) {
        var _this = this;
        var comment = newComment;
        var selectedPriority = this.overViewConfig.priorityOptions.find(function (priority) { return priority['MPM_Priority-id'].Id === _this.overViewConfig.selectedProject.R_PO_PRIORITY['MPM_Priority-id'].Id; });
        var selectedTeam = this.overViewConfig.teamsOptions.find(function (team) { return team['MPM_Teams-id'].Id === _this.overViewConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id; });
        var intermediateStatus = this.sharingService.getAllStatusConfig().find(function (status) { return status.STATUS_TYPE === StatusTypes.INTERMEDIATE; });
        var currentUserObject = this.sharingService.getCurrentUserObject();
        var projectMetaData = {
            PROJECT_IS_ACTIVE: this.overViewConfig.selectedProject.IS_ACTIVE,
            PROJECT_IS_DELETED: false,
            PROJECT_CATEGORY: this.sharingService.selectedCategory.CATEGORY_NAME,
            PROJECT_DESCRIPTION: this.utilService.isNullOrEmpty(this.overViewConfig.selectedProject.DESCRIPTION),
            PROJECT_DUE_DATE: this.overViewConfig.selectedProject.DUE_DATE,
            PROJECT_END_DATE: this.overViewConfig.selectedProject.END_DATE,
            PROJECT_NAME: this.overViewConfig.selectedProject.PROJECT_NAME,
            PROJECT_OWNER_NAME: currentUserObject.UserDisplayName,
            PROJECT_PRIORITY: selectedPriority.DESCRIPTION,
            PROJECT_START_DATE: this.overViewConfig.selectedProject.START_DATE,
            PROJECT_STATUS: intermediateStatus.NAME,
            PROJECT_TEAM: selectedTeam.NAME,
            DATA_TYPE: OTMMMPMDataTypes.PROJECT,
            PROJECT_ITEM_ID: this.overViewConfig.selectedProject['Project-id'].ItemId,
            PROJECT_PROGRESS: 0,
            PROJECT_TYPE: this.overViewConfig.selectedProject.PROJECT_TYPE,
            IS_STANDARD_STATUS: this.overViewConfig.selectedProject.IS_STANDARD_STATUS,
            STATUS_ID: intermediateStatus['MPM_Status-id'].Id,
            PROJECT_STATUS_TYPE: intermediateStatus.STATUS_TYPE,
            PROJECT_ID: this.overViewConfig.selectedProject['Project-id'].Id,
            ORIGINAL_PROJECT_START_DATE: this.overViewConfig.selectedProject.START_DATE,
            ORIGINAL_PROJECT_DUE_DATE: this.overViewConfig.selectedProject.DUE_DATE,
            EXPECTED_PROJECT_DURATION: this.utilService.isNullOrEmpty(this.overViewConfig.selectedProject.EXPECTED_DURATION),
            PROJECT_TIME_SPENT: 0,
            PROJECT_CAMPAIGN_ID: this.overViewConfig.selectedProject.R_PO_CAMPAIGN && this.overViewConfig.selectedProject.R_PO_CAMPAIGN['Campaign-id'].Id
        };
        var customMetadata = this.projectFromTemplateService.getCustomMetadata(this.customFieldsGroup);
        this.projectService.restartProject(projectMetaData, customMetadata, comment).subscribe(function (response) {
            if (response) {
                _this.notificationService.info('Restart project initiated successfully');
            }
        });
    };
    ProjectOverviewComponent.prototype.confirmExitOnEdit = function () {
        var _this = this;
        return new Observable(function (observer) {
            var dialogRef = _this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to discard the changes and Exit ?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                observer.next((result && result.isTrue));
                observer.complete();
            });
        });
    };
    ProjectOverviewComponent.prototype.confirmCancel = function () {
        var _this = this;
        var dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: 'Are you sure you want to Discard the changes?',
                submitButton: 'Yes',
                cancelButton: 'No'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.isTrue) {
                _this.refreshView();
            }
        });
    };
    ProjectOverviewComponent.prototype.createNewProjectUpdate = function () {
        var ProjectObject;
        var projectValue = this.projectOverviewForm.getRawValue();
        /*   if(projectValue && projectValue.campaign.value && projectValue.campaign.split('-')[0]) */
        projectValue.campaign = this.campaignId ? this.campaignId : this.selectedCampaignId ? this.selectedCampaignId : '';
        var projectType = (this.isTemplate) ? ProjectConstant.PROJECT_TYPE_TEMPLATE : ProjectConstant.PROJECT_TYPE_PROJECT;
        if (this.isCampaign) {
            ProjectObject = this.projectUtilService.createNewCampaignUpdateObject(projectValue, this.projectOwnerDetails.userId);
        }
        else {
            ProjectObject = this.projectUtilService.createNewProjectUpdateObject(projectValue, projectType, this.projectOwnerDetails.userId);
            //we can remove this block of code
            if (this.overViewConfig.selectedProject && this.overViewConfig.selectedProject['Project-id']
                && this.overViewConfig.selectedProject['Project-id'].Id) {
                ProjectObject.ProjectId.Id = this.overViewConfig.selectedProject['Project-id'].Id;
                if (this.overViewConfig.selectedProject.R_PO_TEAM && this.overViewConfig.selectedProject.R_PO_TEAM['MPM_Teams-id']
                    && this.overViewConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id) {
                    ProjectObject.RPOTeam.TeamID.Id = this.overViewConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id;
                }
                if (this.overViewConfig.selectedProject.R_PO_CATEGORY && this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id']
                    && this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id'].Id) {
                    ProjectObject.RPOCategory.CategoryID.Id = this.overViewConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id'].Id;
                }
            }
        }
        if (!this.overViewConfig.selectedProject && !this.overViewConfig.isProject) {
            ProjectObject.RPOStatus.StatusID.Id = this.overViewConfig.statusOptions[0]['MPM_Status-id'].Id;
        }
        ProjectObject.CustomMetadata = this.projectFromTemplateService.getCustomMetadata(this.customFieldsGroup);
        return ProjectObject;
    };
    ProjectOverviewComponent.prototype.handleProjectNotification = function (response, eventData, isCreation, isError) {
        var _a;
        this.loadingHandler.next(false);
        if (!isError) {
            if (response && response.APIResponse) {
                if (response.APIResponse.statusCode === '500') {
                    eventData = {
                        message: response.APIResponse.error.errorMessage,
                        type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                    };
                    this.disableProjectSave = false;
                    this.notificationHandler.next(eventData);
                }
                else if (response.APIResponse.statusCode === '200' &&
                    (((response.APIResponse.data && response.APIResponse.data.Project
                        && response.APIResponse.data.Project['Project-id']
                        && response.APIResponse.data.Project['Project-id'].Id) || !isCreation) || (response.APIResponse.data && response.APIResponse.data.Campaign
                        && response.APIResponse.data.Campaign['Campaign-id']
                        && response.APIResponse.data.Campaign['Campaign-id'].Id))) {
                    this.notificationHandler.next(eventData);
                    if (isCreation) {
                        this.projectOverviewForm.reset();
                        (_a = this.customFieldsGroup) === null || _a === void 0 ? void 0 : _a.reset();
                        if (response.APIResponse.data.Project) {
                            this.saveCallBackHandler.next({
                                projectId: response.APIResponse.data.Project['Project-id'].Id,
                                isTemplate: (this.overViewConfig.isTemplate ? true : false)
                            });
                        }
                        else {
                            this.saveCallBackHandler.next({
                                campaignId: response.APIResponse.data.Campaign['Campaign-id'].Id
                            });
                        }
                    }
                    else {
                        // this.projectOverviewForm.reset();
                        // this.customFieldsGroup?.reset();
                        // window.location.reload();
                        this.ngOnInit();
                    }
                }
            }
        }
        else {
            this.disableProjectSave = false;
            this.loadingHandler.next(false);
            this.notificationHandler.next(eventData);
        }
    };
    ProjectOverviewComponent.prototype.getCamp = function () {
        console.log("kokoko");
    };
    ProjectOverviewComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        var projectConfig = this.sharingService.getProjectConfig();
        this.appConfig = this.sharingService.getAppConfig();
        this.enableWorkWeek = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        this.enableCampaign = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CAMPAIGN]);
        this.enableCustomWorkflow = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_CUSTOM_WORKFLOW]);
        this.enableProjectRestartConfig = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_PROJECT_RESTART]);
        this.isCampaignManager = this.sharingService.getCampaignRole();
        this.activatedRoute.queryParams.subscribe(function (params) {
            if (params.campaignId) {
                _this.selectedCampaignId = params.campaignId;
                _this.isCampaignProject = true;
            }
            else {
                _this.selectedCampaignId = null;
                _this.isCampaignProject = false;
            }
        });
        this.refreshView();
    };
    ProjectOverviewComponent.prototype.hasUnsavedData = function () {
        return this.projectOverviewForm.dirty;
    };
    ProjectOverviewComponent.prototype.unloadNotification = function ($event) {
        if (this.hasUnsavedData()) {
            $event.returnValue = true;
        }
    };
    ProjectOverviewComponent.prototype.compareObjects = function (o1, o2) {
        if (o2 == o1) {
            return true;
        }
    };
    ProjectOverviewComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: DateAdapter },
        { type: MatDialog },
        { type: SharingService },
        { type: UtilService },
        { type: AppService },
        { type: OTMMService },
        { type: OtmmMetadataService },
        { type: CategoryService },
        { type: StatusService },
        { type: ProjectUtilService },
        { type: ProjectService },
        { type: ProjectFromTemplateService },
        { type: ActivatedRoute },
        { type: EntityAppDefService },
        { type: CommentsService },
        { type: NotificationService },
        { type: LoaderService },
        { type: DatePipe }
    ]; };
    __decorate([
        Input()
    ], ProjectOverviewComponent.prototype, "projectId", void 0);
    __decorate([
        Input()
    ], ProjectOverviewComponent.prototype, "isTemplate", void 0);
    __decorate([
        Input()
    ], ProjectOverviewComponent.prototype, "teamsOptions", void 0);
    __decorate([
        Input()
    ], ProjectOverviewComponent.prototype, "isCampaign", void 0);
    __decorate([
        Input()
    ], ProjectOverviewComponent.prototype, "campaignId", void 0);
    __decorate([
        Output()
    ], ProjectOverviewComponent.prototype, "closeCallbackHandler", void 0);
    __decorate([
        Output()
    ], ProjectOverviewComponent.prototype, "saveCallBackHandler", void 0);
    __decorate([
        Output()
    ], ProjectOverviewComponent.prototype, "notificationHandler", void 0);
    __decorate([
        Output()
    ], ProjectOverviewComponent.prototype, "loadingHandler", void 0);
    __decorate([
        Output()
    ], ProjectOverviewComponent.prototype, "restartProjectHandler", void 0);
    __decorate([
        ViewChild(CustomSearchFieldComponent)
    ], ProjectOverviewComponent.prototype, "customSearchFieldComponent", void 0);
    __decorate([
        HostListener('window:beforeunload', ['$event'])
    ], ProjectOverviewComponent.prototype, "unloadNotification", null);
    ProjectOverviewComponent = __decorate([
        Component({
            selector: 'mpm-project-overview',
            template: "<div class=\"overview-content\">\r\n    <form class=\"project-overview-form\" *ngIf=\"projectOverviewForm\" [formGroup]=\"projectOverviewForm\">\r\n        <div class=\"flex-col\">\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>{{overViewConfig.formType}} Name</mat-label>\r\n                            <input [appAutoFocus]=\"true\" formControlName=\"name\" matInput\r\n                                placeholder=\"{{overViewConfig.formType}} Name\" required>\r\n                            <mat-hint align=\"end\"\r\n                                *ngIf=\"projectOverviewForm.get('name').errors && projectOverviewForm.get('name').errors.maxlength\">\r\n                                {{projectOverviewForm.get('name').errors.maxlength.actualLength}}/{{projectOverviewForm.get('name').errors.maxlength.requiredLength}}\r\n                            </mat-hint>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\" *ngIf=\"overViewConfig.showRequesterField\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Requester</mat-label>\r\n                            <input formControlName=\"requester\" matInput placeholder=\"Requester\">\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div> -->\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\" *ngIf=\"enableCampaign && overViewConfig.isProject\">\r\n                    <div class=\"flex-row-item\">\r\n                        <mpm-custom-search-field [searchFieldConfig]=\"overViewConfig.campaignDataConfig\">\r\n                        </mpm-custom-search-field>\r\n                    </div>\r\n                    <div class=\"flex-row-item\">\r\n                        <mpm-custom-search-field [searchFieldConfig]=\"overViewConfig.campaignOwnerFieldConfig\">\r\n                        </mpm-custom-search-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item max-width\" *ngIf=\"overViewConfig.isProject || overViewConfig.isTemplate\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Teams</mat-label>\r\n                            <mat-select formControlName=\"teams\" placeholder=\"Teams\" name=\"projectTeam\"\r\n                                [required]=\"true\">\r\n                                <span *ngFor=\"let team of overViewConfig.teamsOptions\">\r\n                                    <mat-option value=\"{{team['MPM_Teams-id'] .Id}}\">\r\n                                        {{team.NAME}}\r\n                                    </mat-option>\r\n                                </span>\r\n                            </mat-select>\r\n                        </mat-form-field>\r\n                    </div>\r\n                    <div class=\"flex-row-item\">\r\n                        <!-- <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Category</mat-label>\r\n                            <mat-select formControlName=\"category\" placeholder=\"Category\" name=\"projectCategory\"\r\n                                [required]=\"true\">\r\n                                <span *ngFor=\"let category of overViewConfig.categoryOptions\">\r\n                                    <mat-option value=\"{{category['MPM_Category-id'] .Id}}\">\r\n                                        {{category.CATEGORY_NAME}}\r\n                                    </mat-option>\r\n                                </span>\r\n                            </mat-select>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\"> -->\r\n                        <mpm-custom-search-field [searchFieldConfig]=\"overViewConfig.ownerFieldConfig\"\r\n                            [required]=\"true\">\r\n                        </mpm-custom-search-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item max-width\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Status</mat-label>\r\n                            <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"projectStatus\"\r\n                                (selectionChange)=\"validateTaskStatusUpdate($event)\" [required]=\"true\"\r\n                                [disabled]=\"projectOverviewForm.get('status').status === 'DISABLED' ? true : false\">\r\n                                <span *ngFor=\"let status of overViewConfig.statusOptions\">\r\n                                    <!-- selectedStatus -->\r\n                                    <mat-option value=\"{{status['MPM_Status-id'].Id}}\">\r\n                                        {{status.NAME}}\r\n                                    </mat-option>\r\n                                </span>\r\n                            </mat-select>\r\n                        </mat-form-field>\r\n                    </div>\r\n                    <div class=\"flex-row-item max-width\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Priority</mat-label>\r\n                            <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\"\r\n                                [required]=\"true\"\r\n                                [disabled]=\"projectOverviewForm.get('priority').status === 'DISABLED' ? true : false\">\r\n                                <mat-option *ngFor=\"let priority of overViewConfig.priorityOptions\"\r\n                                    value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                                    {{priority.DESCRIPTION}}\r\n                                </mat-option>\r\n                            </mat-select>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\">\r\n                        <mat-form-field appearance=\"outline\"\r\n                            *ngIf=\"overViewConfig.isProject || overViewConfig.isCampaign\">\r\n                            <mat-label>Start Date</mat-label>\r\n                            <input formControlName=\"startDate\" matInput [matDatepicker]=\"startDate\"\r\n                                placeholder=\"Start Date (MM/DD/YYYY)\" required\r\n                                [min]=\"selectedCampaign && selectedCampaign.CAMPAIGN_START_DATE ? selectedCampaign.CAMPAIGN_START_DATE : (this.overViewConfig && this.overViewConfig.selectedProject && this.overViewConfig.selectedProject.START_DATE ? this.overViewConfig.selectedProject.START_DATE : (this.overViewConfig && overViewConfig.selectedCampaign && this.overViewConfig.selectedCampaign.CAMPAIGN_START_DATE) ? this.overViewConfig.selectedCampaign.CAMPAIGN_START_DATE : minStartDate)\"\r\n                                [(value)]=\"selectedStartDate\" (dateChange)=\"validateProjectStartDate($event)\"\r\n                                [max]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.startMaxDate ? overViewConfig.dateValidation.startMaxDate : (selectedCampaign && selectedCampaign.CAMPAIGN_END_DATE ? selectedCampaign.CAMPAIGN_END_DATE : null)\">\r\n                            <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                            <mat-datepicker #startDate [disabled]=\"disableStartDate\"></mat-datepicker>\r\n                            <mat-error\r\n                                *ngIf=\"projectOverviewForm.get('startDate') && (projectOverviewForm.get('startDate').hasError('matDatepickerMax') || projectOverviewForm.get('startDate').hasError('matDatepickerMin'))\">\r\n                                {{startDateErrorMessage}}\r\n                            </mat-error>\r\n                        </mat-form-field>\r\n                    </div>\r\n                    <div class=\"flex-row-item\">\r\n                        <mat-form-field appearance=\"outline\"\r\n                            *ngIf=\"overViewConfig.isProject || overViewConfig.isCampaign\">\r\n                            <mat-label>End Date</mat-label>\r\n                            <input formControlName=\"endDate\"\r\n                                [min]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.endMinDate ? overViewConfig.dateValidation.endMinDate : (projectOverviewForm.value.startDate ? projectOverviewForm.value.startDate : selectedStartDate)\"\r\n                                matInput [matDatepicker]=\"endDate\" placeholder=\"End Date (MM/DD/YYYY)\" required\r\n                                [(value)]=\"selectedEndDate\" (dateChange)=\"validateProjectEndDate($event)\"\r\n                                [max]=\"selectedCampaign && selectedCampaign.CAMPAIGN_END_DATE ? selectedCampaign.CAMPAIGN_END_DATE : null\">\r\n                            <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                            <mat-datepicker #endDate [disabled]=\"disableEndDate\"></mat-datepicker>\r\n                            <mat-error\r\n                                *ngIf=\"projectOverviewForm.get('endDate') && (projectOverviewForm.get('endDate').hasError('matDatepickerMax') || projectOverviewForm.get('endDate').hasError('matDatepickerMin'))\">\r\n                                {{endDateErrorMessage}}\r\n                            </mat-error>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\">\r\n                        <mat-form-field appearance=\"outline\" *ngIf=\"overViewConfig.isProject\">\r\n                            <mat-label>Expected Duration (Days)</mat-label>\r\n                            <input matInput placeholder=\"Expected Duration (Days)\" formControlName=\"expectedDuration\"\r\n                                (keydown)=\"restrictKeysOnType($event, 'NUMBER')\" type=\"number\">\r\n                            <mat-hint align=\"end\" *ngIf=\"projectOverviewForm.get('expectedDuration').errors\">\r\n                                Please enter the number lesser than 99999\r\n                            </mat-hint>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item\">\r\n                        <mat-form-field appearance=\"outline\">\r\n                            <mat-label>Description</mat-label>\r\n                            <textarea formControlName=\"description\" matInput placeholder=\"Description\"\r\n                                rows=\"3\"></textarea>\r\n                            <mat-hint align=\"end\"\r\n                                *ngIf=\"projectOverviewForm.get('description').errors && projectOverviewForm.get('description').errors.maxlength\">\r\n                                {{projectOverviewForm.get('description').errors.maxlength.actualLength}}/{{projectOverviewForm.get('description').errors.maxlength.requiredLength}}\r\n                            </mat-hint>\r\n                        </mat-form-field>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"flex-col-item\" *ngIf=\"projectOverviewForm.controls.CustomFieldGroup\"\r\n            formGroupName=\"CustomFieldGroup\">\r\n            <mat-accordion displayMode=\"flat\" togglePosition=\"before\" multi=\"true\">\r\n                <mat-expansion-panel class=\"mat-elevation-z0 form-group-panel\" [expanded]=\"true\"\r\n                    *ngFor=\"let formGroup of projectOverviewForm.get('CustomFieldGroup')['controls']\"> \r\n                    <mat-expansion-panel-header>\r\n                        <mat-panel-title >{{formGroup.name}}</mat-panel-title>\r\n                    </mat-expansion-panel-header>\r\n\r\n                    <div class=\"flex-row\" *ngFor=\"let formControl of formGroup['controls']['fieldset']['controls']\">\r\n                        <div class=\"flex-row-item\">\r\n                            <mpm-custom-metadata-field [metadataFieldFormControl]=\"formControl\"\r\n                                (valueChanges)=\"onCustomMetadataFieldChange($event)\"></mpm-custom-metadata-field>\r\n                        </div>\r\n                    </div>\r\n                </mat-expansion-panel>\r\n            </mat-accordion>\r\n        </div>\r\n\r\n        <div class=\"flex-col-item\" id=\"restart-action\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item\" *ngIf=\"!overViewConfig.isCampaign\">\r\n                    <mat-slide-toggle class=\"flex-row-item\" color=\"primary\" formControlName=\"isCustomWorkflow\">\r\n                        Custom Workflow\r\n                    </mat-slide-toggle>\r\n                </div>\r\n                <div class=\"flex-row-item\">\r\n                    <span class=\"form-actions\">\r\n                        <button color='primary'\r\n                            *ngIf=\"enableRestartProject && overViewConfig.isProject \r\n                        && overViewConfig.currentUserItemId === overViewConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id\"\r\n                            mat-flat-button (click)=\"restartProject()\"  type=\"submit\"\r\n                            class=\"ang-btn\">Restart</button>\r\n                        <button mat-stroked-button [disabled]=\"(projectOverviewForm && !projectOverviewForm.dirty) \r\n                        && ((customFieldsGroup && !customFieldsGroup.dirty) || !customFieldsGroup)\"\r\n                            (click)=\"cancelProjectCreation()\">Discard Changes</button>\r\n                        <button color='primary' mat-flat-button (click)=\"updateProject()\" type=\"submit\" class=\"ang-btn\"\r\n                            [disabled]=\"(projectOverviewForm && projectOverviewForm.invalid) || \r\n                            ((projectOverviewForm && projectOverviewForm.pristine) && \r\n                            ((customFieldsGroup && customFieldsGroup.pristine) || !customFieldsGroup)) ||\r\n                            (customFieldsGroup && customFieldsGroup.invalid) || disableProjectSave\">Save</button>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>",
            styles: [".overview-content{padding:20px}.project-overview-form{display:flex;flex-direction:column;flex-wrap:wrap;justify-content:center;padding:10px 50px;width:50vw}.restart-action{float:left;margin-bottom:5px}.form-actions{padding:0 15px;margin-left:auto}.form-actions button{margin-left:15px}.project-overview-form .mat-form-field,mpm-custom-search-field{width:100%;margin:0 15px 0 0}.form-action{margin-left:20px;margin-bottom:20px}::ng-deep .mat-form-field-suffix button{position:relative;margin:0 -20px 0 -30px}.mat-radio-button{margin-left:10px}.max-width{max-width:50%}.form-group-panel{background:0 0}.form-group-panel ::ng-deep mpm-custom-metadata-field{width:100%}.form-group-panel ::ng-deep .mat-expansion-panel-header{padding:0 4px}.form-group-panel ::ng-deep .mat-expansion-panel-body{padding:0 16px 16px 0}::ng-deep .mat-hint{color:red}"]
        })
    ], ProjectOverviewComponent);
    return ProjectOverviewComponent;
}());
export { ProjectOverviewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1vdmVydmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC1vdmVydmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7RUFlRTtBQUNGLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDbEksT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1RixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFHcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVDLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDJFQUEyRSxDQUFDO0FBQ3ZILE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFDckgsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQzNELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV6RCxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUV0RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFHeEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBR3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDL0UsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDdEQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDOUYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFakcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUzRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDN0YsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDNUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRTNDLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBUTNGO0lBZ0dJLGtDQUNXLEVBQWUsRUFDZixPQUF5QixFQUN6QixNQUFpQixFQUNqQixjQUE4QixFQUM5QixXQUF3QixFQUN4QixVQUFzQixFQUN0QixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsZUFBZ0MsRUFDaEMsYUFBNEIsRUFDNUIsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLDBCQUFzRCxFQUN0RCxjQUE4QixFQUM5QixtQkFBd0MsRUFDeEMsZUFBZ0MsRUFDaEMsbUJBQXdDLEVBQ3hDLGFBQTRCLEVBQzVCLFFBQWtCO1FBbkI3QixpQkFzQkM7UUFyQlUsT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLFlBQU8sR0FBUCxPQUFPLENBQWtCO1FBQ3pCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUN0RCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBM0duQix5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5QyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDekMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUcxRCwwQkFBcUIsR0FBRyxzREFBc0QsQ0FBQztRQUMvRSx1QkFBa0IsR0FBRyw4Q0FBOEMsQ0FBQztRQUNwRSx1QkFBa0IsR0FBRyx1REFBdUQsQ0FBQztRQUM3RSw4QkFBeUIsR0FBRyxrREFBa0QsQ0FBQztRQUMvRSwrQkFBMEIsR0FBRyxtREFBbUQsQ0FBQztRQUNqRixtQkFBYyxHQUFHLG9EQUFvRCxDQUFDO1FBRXRFLDRCQUF1QixHQUFHLGlCQUFpQixDQUFDO1FBQzVDLGdDQUEyQixHQUFHLG9DQUFvQyxDQUFDO1FBQ25FLHVDQUFrQyxHQUFHLGdCQUFnQixDQUFDO1FBQ3RELGtDQUE2QixHQUFHLGVBQWUsQ0FBQztRQUNoRCxrQ0FBNkIsR0FBRyxzQkFBc0IsQ0FBQztRQUN2RCxtQ0FBOEIsR0FBRyxnQkFBZ0IsQ0FBQztRQUNsRCxnQ0FBMkIsR0FBRyxpQkFBaUIsQ0FBQztRQUNoRCxvQ0FBK0IsR0FBRyxnQkFBZ0IsQ0FBQztRQUNuRCxpQ0FBNEIsR0FBRyxhQUFhLENBQUM7UUFDN0MsMkNBQXNDLEdBQUcscUJBQXFCLENBQUM7UUFDL0QsNENBQXVDLEdBQUcsc0JBQXNCLENBQUM7UUFLakUsaUJBQVksR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBRWhDLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1Qix1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDM0IsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBSzFCLHVCQUFrQixHQUFHLEtBQUssQ0FBQztRQU0zQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFldkIseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBRzdCLG1CQUFjLEdBQUc7WUFDYixNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7U0FDOUIsQ0FBQztRQU9GLHlCQUFvQixHQUFXLEtBQUssQ0FBQztRQTA3RHJDLGVBQVUsR0FDTixVQUFDLElBQWlCO1lBQ2QsSUFBSSxLQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNyQixPQUFPLElBQUksQ0FBQzthQUNmO2lCQUFNO2dCQUNILElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDMUIsT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUM7Z0JBQzlCLGdCQUFnQjtnQkFDaEIsa0JBQWtCO2FBQ3JCO1FBQ0wsQ0FBQyxDQUFBO1FBaDZERCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFDRCw4Q0FBVyxHQUFYLFVBQVksT0FBc0I7SUFDbEMsQ0FBQztJQUVNLGdEQUFhLEdBQXBCO1FBQUEsaUJBT0M7UUFORyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNwQixLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxREFBa0IsR0FBbEIsVUFBbUIsS0FBSyxFQUFFLFFBQVE7UUFDOUIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUN4RixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDMUI7SUFDTCxDQUFDO0lBRUQsOENBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQztTQUN4RDthQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN4QixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUM7U0FDeEQ7YUFBTTtZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQztTQUN2RDtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzNDO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDekM7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEQsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQztJQUVELDREQUF5QixHQUF6QixVQUEwQixNQUFjO1FBQXhDLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDVCxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtZQUNELElBQU0sVUFBVSxHQUFHO2dCQUNmLE1BQU0sRUFBRSxNQUFNO2FBQ2pCLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGFBQWE7Z0JBQ3pFLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxhQUFhLENBQUM7Z0JBQzFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDaEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseURBQXNCLEdBQXRCLFVBQXVCLE1BQWM7UUFBckMsaUJBZUM7UUFkRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNULFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNwQjtZQUNELElBQU0sVUFBVSxHQUFHO2dCQUNmLE1BQU0sRUFBRSxNQUFNO2FBQ2pCLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGFBQWE7Z0JBQ2pFLEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxhQUFhLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDaEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBRVAsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQVUsR0FBVjtRQUFBLGlCQXNJQztRQXJJRyxJQUFJLFFBQVEsRUFBRSxhQUFhLENBQUM7UUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNoQixhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUM7U0FDdEg7YUFBTTtZQUNILFFBQVEsR0FBRyxLQUFLLENBQUM7WUFDakIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixhQUFhLEdBQUcsZUFBZSxDQUFDLHVCQUF1QixDQUFDO2FBQzNEO2lCQUFNO2dCQUNILGFBQWEsR0FBRyxlQUFlLENBQUMsc0JBQXNCLENBQUM7YUFDMUQ7U0FDSjtRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWM7WUFDdkYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7WUFDNUosSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7UUFFbkosUUFBUTtRQUNKOzs7Ozt5REFLaUQ7UUFFakQsS0FBSztRQUNMOzs7Ozs0REFLb0Q7UUFDcEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRSxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMscUNBQXFDLENBQ3BHLGFBQWEsRUFBRSxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQy9FLGFBQWEsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQztnQkFDakMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLG9EQUFvRCxDQUFBLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDM0csSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLHFDQUFxQyxDQUN2SyxhQUFhLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUMvRSxhQUFhLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQ0FBcUMsQ0FDcEosYUFBYSxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FDL0UsYUFBYSxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7WUFDakMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQ3BELENBQUMsU0FBUyxDQUNQLFVBQUEsVUFBVTtZQUNOOzt1QkFFVztZQUNYLDZCQUE2QjtZQUM3QixJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7Z0JBQ25FLEtBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7b0JBQzdGLGtCQUFTLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxhQUFhLElBQUssSUFBSSxFQUFHO2dCQUM5SCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7b0JBQzdGLGtCQUFTLFdBQVcsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxhQUFhLElBQUssSUFBSSxFQUFHO2dCQUM5SCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1g7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtvQkFDN0Ysa0JBQVMsV0FBVyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsSUFBSyxJQUFJLEVBQUc7Z0JBQzlILENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDWDtZQUVELEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxHQUFHLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNoSCxLQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDdkosS0FBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3pKLElBQUksQ0FBQyxLQUFJLENBQUMsY0FBYyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzNHLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ2xGO1lBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BELGlFQUFpRTtZQUNqRSxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2pILEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO29CQUMxQyxJQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLEdBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsRUFBQzt3QkFDcEQsT0FBTyxDQUFDLENBQUMsQ0FBQTtxQkFDWjs7d0JBQ0ksQ0FBQyxDQUFDO2dCQUNYLENBQUMsQ0FBQyxDQUFDO2dCQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEVBQUU7b0JBQzFDLElBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBRSxNQUFNLEVBQUM7d0JBQ2hDLEtBQUksQ0FBQyxvQkFBb0IsR0FBRSxJQUFJLENBQUM7d0JBQ2hDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRSxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7cUJBQ25EO2dCQUNMLENBQUMsQ0FBQyxDQUFBO2dCQUNGLElBQUcsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLEVBQUM7b0JBQzFCLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDdkY7Z0JBQ0Ysd0ZBQXdGO2FBQzFGO1lBRUQsS0FBSztZQUNMLEtBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN6RSxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUM7WUFDekgsZ0ZBQWdGO1lBQ2hGLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxHQUFHLENBQUM7b0JBQ2xELElBQUksRUFBRSxFQUFFO29CQUNSLEtBQUssRUFBRSxFQUFFO29CQUNULFdBQVcsRUFBRSxFQUFFO2lCQUNsQixDQUFDLENBQUM7WUFDSCxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCO21CQUNqQyxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQjttQkFDeEQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQy9FLElBQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7aUJBQ3JGLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhO29CQUN6RSxLQUFJLENBQUMsbUJBQW1CLEdBQUcsYUFBYSxDQUFDO29CQUN6QyxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2dCQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO2lCQUFNLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlO21CQUN2QyxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0I7bUJBQ3RELEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDN0UsSUFBTSxVQUFVLEdBQUc7b0JBQ2YsTUFBTSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7aUJBQ25GLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhO29CQUN6RSxLQUFJLENBQUMsbUJBQW1CLEdBQUcsYUFBYSxDQUFDO29CQUN6QyxLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ3BCLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwQyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDcEIsRUFBRTtnQkFDRixLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuQztRQUNMLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUVELG9EQUFpQixHQUFqQixVQUFrQixRQUFRO1FBQ3RCLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNqRixPQUFPLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7U0FDcEU7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7UUFPSTtJQUVKLCtDQUFZLEdBQVosVUFBYSxVQUFVO1FBQ25CLElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxnQkFBZ0IsU0FBQSxDQUFDO1lBQ3JCLGdCQUFnQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxLQUFLLEtBQUssVUFBVSxFQUE1QixDQUE0QixDQUFDLENBQUM7WUFDdEgsT0FBTyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNuRDthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFFRCxzREFBbUIsR0FBbkI7UUFBQSxpQkF5Q0M7UUF4Q0csT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxhQUFhLEdBQVUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xGLElBQU0sS0FBSyxHQUFHO2dCQUNWLE1BQU0sRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzVDLE1BQU0sRUFBRSxhQUFhLENBQUMsT0FBTzthQUNoQyxDQUFDO1lBQ0YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUM7aUJBQ3JDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtvQkFDbkQsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsSUFBTSxPQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNwRCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs0QkFDakIsSUFBTSxRQUFRLEdBQUcsT0FBSyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDOzRCQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO2dDQUNYLE9BQUssQ0FBQyxJQUFJLENBQUM7b0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO29DQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHO2lDQUNoRCxDQUFDLENBQUM7NkJBQ047d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEdBQUcsT0FBSyxDQUFDO2lCQUM5RDtxQkFBTTtvQkFDSCxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7b0JBQ3hELElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLG9EQUFvRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3JJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQzVDO2dCQUNELFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDaEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQzVILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFBQSxpQkF3Q0M7UUF2Q0csT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsSUFBSSxLQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3pELEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLEtBQUksQ0FBQyw0QkFBNEIsRUFBRSxJQUFJLENBQUM7cUJBQ3RGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBTSxLQUFLLEdBQWdCLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBQzVFLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFFcEIsb0NBQW9DO2dCQUN4QyxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNoQyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUM1SCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsSUFBTSxhQUFhLEdBQVUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNsRixJQUFNLEtBQUssR0FBRztvQkFDVixNQUFNLEVBQUUsYUFBYSxDQUFDLE9BQU87aUJBQ2hDLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsdUJBQXVCLEVBQUUsS0FBSyxDQUFDO3FCQUN0RixTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNmLElBQU0sS0FBSyxHQUFnQixPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBRXBCLG9DQUFvQztnQkFDeEMsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDaEMsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDNUgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNWO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQXFCSTtJQUVKLGdEQUFhLEdBQWIsVUFBYyxpQkFBaUI7UUFDM0IsSUFBTSxlQUFlLEdBQWtCLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ3BGLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixLQUFLLGlCQUFpQixDQUFDO1FBQzFELENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFL0IsSUFBSSxlQUFlLEVBQUU7WUFDakIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDekUsSUFBSSxXQUFXLEdBQW9CLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUM5RyxJQUFJLFNBQVMsR0FBb0IsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFDLE1BQWdCO29CQUNqRSxPQUFPLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2xILENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksUUFBTSxHQUFHLEVBQUUsQ0FBQztnQkFDaEIsUUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDdkIsT0FBTyxRQUFNLENBQUM7YUFHakI7aUJBQU07Z0JBQ0gsSUFBSSxTQUFTLEdBQW9CLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxNQUFNLENBQUMsVUFBQyxNQUFnQjtvQkFDNUYsT0FBTyxNQUFNLENBQUMsbUJBQW1CLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNsSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLFFBQU0sR0FBRyxFQUFFLENBQUM7Z0JBQ2hCLFFBQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3ZCLE9BQU8sUUFBTSxDQUFDO2FBRWpCO1NBQ0o7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUVkLENBQUM7SUFFRCxLQUFLO0lBRUwsdURBQW9CLEdBQXBCLFVBQXFCLFlBQVk7UUFBakMsaUJBZ0dDO1FBL0ZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLFlBQVksRUFBRSxZQUFZO2FBQzdCLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMEJBQTBCLEVBQUUsS0FBSSxDQUFDLHVDQUF1QyxFQUFFLGdCQUFnQixDQUFDO2dCQUMxSCxtQ0FBbUM7aUJBQ2xDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLENBQUMsUUFBUSxFQUFFO29CQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUU7d0JBQ25DLFFBQVEsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQzNDO29CQUNELEtBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztvQkFFdEMsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRTt3QkFDNUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7NEJBQ3RFLElBQUksUUFBUSxFQUFFO2dDQUNWLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBckQsQ0FBcUQsQ0FBQyxDQUFDO2dDQUM5RyxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQ0FDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0NBQy9CLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLFlBQVksQ0FBQyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztvQ0FDbk4sS0FBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO3dDQUMxRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3Q0FDaEMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7NENBQzFFLE1BQU0sRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTTs0Q0FDeEMsUUFBUSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRO3lDQUMvQyxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29DQUMxQyxDQUFDLENBQUMsQ0FBQztpQ0FDTjtxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7aUNBQ2hFOzZCQUNKO2lDQUFNO2dDQUNILEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0NBQzdCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQ0FDN0QsTUFBTTtnQ0FDTixLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7NkJBQzdEO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELElBQUksS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUU7d0JBQzdDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7NEJBQy9ELElBQUksS0FBSSxDQUFDLGdCQUFnQixFQUFFO2dDQUN2QixJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDO29DQUNsSCxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztvQ0FDaEgsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsWUFBWSxDQUFDLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxZQUFZLENBQUMsRUFBRTtvQ0FDekosS0FBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyxtQ0FBbUMsQ0FBQztpQ0FDcEY7cUNBQU07b0NBQ0gsS0FBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQztpQ0FDekU7NkJBQ0o7aUNBQU07Z0NBQ0gsS0FBSSxDQUFDLHFCQUFxQixHQUFHLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQzs2QkFDekU7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRTt3QkFDM0MsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQzs0QkFDN0QsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0NBQ3ZCLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDO29DQUMvRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsRUFBRTtvQ0FDL0csS0FBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxtQ0FBbUMsQ0FBQztpQ0FDbEY7cUNBQU07b0NBQ0gsS0FBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxnQ0FBZ0MsQ0FBQztpQ0FDL0U7NkJBQ0o7aUNBQU07Z0NBQ0gsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO29DQUNqQixLQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGlDQUFpQyxDQUFDO2lDQUNoRjtxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGdDQUFnQyxDQUFDO2lDQUMvRTs2QkFDSjt3QkFDTCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFFRCxJQUFNLG9CQUFrQixHQUFHLEVBQUUsQ0FBQztvQkFDOUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxRQUFRO3dCQUMxQixJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQy9DLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7NEJBQy9FLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxFQUFFOzRCQUN4SCxvQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0NBQ3BCLElBQUksRUFBRSxRQUFRLENBQUMsYUFBYTtnQ0FDNUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO2dDQUNqQyxXQUFXLEVBQUUsS0FBSztnQ0FDbEIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFOzZCQUMxRCxDQUFDLENBQUM7eUJBQ047b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsS0FBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEdBQUcsb0JBQWtCLENBQUM7aUJBQzdFO3FCQUFNO29CQUNILEtBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO29CQUN2QixLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7aUJBQzdEO2dCQUNELEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNEOzs7Ozs7a0NBTThCO0lBQzlCLCtDQUFZLEdBQVosVUFBYSxZQUFZO1FBQXpCLGlCQU1DO1FBTEcsSUFBSSxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGdCQUFnQjtnQkFDM0UsS0FBSSxDQUFDLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQztZQUMxQyxDQUFDLENBQUMsQ0FBQTtTQUNMO0lBQ0wsQ0FBQztJQUNELHFEQUFrQixHQUFsQixVQUFtQixLQUFLO1FBQXhCLGlCQVdDO1FBVkcsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7UUFDbkgsSUFBSSxDQUFDLHlCQUF5QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDdkUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7Z0JBQzFFLE1BQU0sRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTTtnQkFDeEMsUUFBUSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRO2FBQy9DLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFFdEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDL0csQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsOENBQVcsR0FBWDtRQUFBLGlCQWdDQztRQS9CRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMEJBQTBCLEVBQUUsS0FBSSxDQUFDLCtCQUErQixFQUFFLElBQUksQ0FBQztnQkFDdEcsbUNBQW1DO2lCQUNsQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNuQyxRQUFRLENBQUMsUUFBUSxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUMzQztvQkFDRCxLQUFJLENBQUMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7b0JBQ3RDLElBQU0sb0JBQWtCLEdBQUcsRUFBRSxDQUFDO29CQUM5QixLQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFBLFFBQVE7d0JBQzFCLElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDL0MsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTs0QkFDL0UsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGlCQUFpQixFQUFFLFlBQVksQ0FBQyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7NEJBQ3hILG9CQUFrQixDQUFDLElBQUksQ0FBQztnQ0FDcEIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxhQUFhO2dDQUM1QixLQUFLLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0NBQ2pDLFdBQVcsRUFBRSxLQUFLOzZCQUNyQixDQUFDLENBQUM7eUJBQ047b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsS0FBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEdBQUcsb0JBQWtCLENBQUM7aUJBQzdFO3FCQUFNO29CQUNILEtBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO29CQUN2QixLQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7aUJBQzdEO2dCQUNELEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvREFBaUIsR0FBakIsVUFBa0IsZUFBZTtRQUM3QixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHdCQUF3QixFQUFFLENBQUM7UUFDbkUsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNqQixPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLGNBQWMsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDbEQsSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLGVBQWUsRUFBRTtnQkFDckMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUM7YUFDdEM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sV0FBVyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCw4Q0FBVyxHQUFYLFVBQVksYUFBYSxFQUFFLGlCQUFpQjtRQUE1QyxpQkF3RkM7UUF2RkcsSUFBSSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLDZCQUE2QixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdGLElBQUksaUJBQWlCLEVBQUU7WUFDbkIsSUFBTSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM5RSxvQkFBb0IsR0FBRyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLG9CQUFvQixDQUFDO1NBQ3JHO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxvQkFBb0IsQ0FBQyxpQkFBaUIsQ0FBQzthQUN2RSxTQUFTLENBQUMsVUFBQSxvQkFBb0I7O1lBQzNCLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFNLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdkIsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztZQUMvQixJQUFJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsRUFBRTtnQkFDbEQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQzlEO1lBQ0QsSUFBSSxvQkFBb0IsSUFBSSxvQkFBb0IsQ0FBQyxxQkFBcUI7Z0JBQ2xFLEtBQUssQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMscUJBQXFCLENBQUMsRUFBRTs7b0JBQzNELEtBQWlDLElBQUEsS0FBQSxTQUFBLG9CQUFvQixDQUFDLHFCQUFxQixDQUFBLGdCQUFBLDRCQUFFO3dCQUF4RSxJQUFNLGtCQUFrQixXQUFBO3dCQUN6QixJQUFNLGdCQUFnQixHQUFHLEVBQUUsQ0FBQzt3QkFDNUIsSUFBSSxrQkFBa0IsQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLDBCQUEwQixJQUFJLGtCQUFrQixDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUMsMkJBQTJCLEVBQUU7NEJBQy9JLFNBQVM7eUJBQ1o7d0JBQ0QsSUFBSSxrQkFBa0IsQ0FBQyxxQkFBcUI7NEJBQ3hDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsRUFBRTs7Z0NBQ3pELEtBQTRCLElBQUEsb0JBQUEsU0FBQSxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQSxDQUFBLGdCQUFBLDRCQUFFO29DQUFqRSxJQUFNLGFBQWEsV0FBQTtvQ0FDcEIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztvQ0FDOUMsSUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO29DQUN0QixJQUFNLEtBQUssR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzt3Q0FDaEQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxFQUN6RSxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7b0NBRXJFLElBQU0sY0FBYyxHQUFHLElBQUksV0FBVyxDQUFDO3dDQUNuQyxLQUFLLEVBQUUsT0FBTyxLQUFLLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0NBQ2hELFFBQVEsRUFBRSxDQUFDLGFBQWEsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDbkosQ0FBQyxDQUFDO29DQUVILElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTt3Q0FDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7cUNBQ3hDO29DQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO3dDQUN0RSxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDOUM7b0NBRUQsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNO3dDQUN0RSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUU7d0NBQzFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztxQ0FDcEU7b0NBQ0QsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUU7d0NBQ3hFLElBQU0sY0FBYyxHQUFHLEtBQUksQ0FBQywrQkFBK0IsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3Q0FDNUcsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3Q0FDbkYsc0dBQXNHO3dDQUN0Ryx5SkFBeUo7cUNBQzVKO29DQUNELGNBQWMsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3pDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29DQUV4QyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsYUFBYSxDQUFDO29DQUMzQyxjQUFjLENBQUMsTUFBTSxDQUFDLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQztvQ0FFNUMsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUU7d0NBQ3ZFLGFBQWEsQ0FBQyxNQUFNLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7cUNBQzlGO29DQUVELGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0NBRW5DLFdBQVc7b0NBQ1gsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lDQUN6Qzs7Ozs7Ozs7O3lCQUNKO3dCQUNELElBQU0sVUFBVSxHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksU0FBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNoRixVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDO3dCQUU3QyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUNoQzs7Ozs7Ozs7O2FBQ0o7WUFDRCxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMvRyxvQkFBb0I7Z0JBQ3BCLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO29CQUMxQixJQUFJLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLGlCQUFpQixDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsS0FBSyxFQUFFO3dCQUMxRSxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDN0M7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksU0FBUyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbkYsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzdHO1lBQ0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDbEQsS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDcEYsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBQ0QsMERBQXVCLEdBQXZCLFVBQXdCLEtBQUs7UUFDekIsSUFBSSxjQUFjLEdBQUcsT0FBTyxDQUFDO1FBQzdCLElBQUksS0FBSyxFQUFFO1lBQ1AsY0FBYyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM5RDtRQUNELGtDQUFrQztRQUNsQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELGtFQUErQixHQUEvQixVQUFnQyxVQUFVLEVBQUUsS0FBSztRQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsQ0FBQztRQUN4QyxJQUFJLFlBQVksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNyRCxJQUFJLEtBQUssRUFBRTtZQUNQLFlBQVksSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUQ7UUFDRCxrQ0FBa0M7UUFDbEMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3JFLENBQUM7SUFFRCwyREFBd0IsR0FBeEIsVUFBeUIsY0FBYztRQUNuQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3FCQThCYTtRQUNiLHdIQUF3SDtJQUM1SCxDQUFDO0lBRUQsMkRBQXdCLEdBQXhCLFVBQXlCLGVBQWU7UUFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFO1lBQ3RDLE9BQU87U0FDVjtRQUNELHlDQUF5QztJQUM3QyxDQUFDO0lBRUQseURBQXNCLEdBQXRCLFVBQXVCLGVBQWU7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFO1lBQ3RDLE9BQU87U0FDVjtRQUNELHlDQUF5QztJQUM3QyxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQWtCSTtJQUVKLCtDQUFZLEdBQVo7UUFBQSxpQkFpY0M7UUFoY0csSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQU0sT0FBTyxHQUFHLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQztRQUNwRCxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRTtZQUMvRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUM1QztRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUU7WUFDM0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FDMUM7UUFDRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7UUFDakQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTtZQUNyQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1lBQy9CLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3BILElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQzthQUNwRDtZQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDekMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2FBQzlCO1NBQ0o7YUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7WUFDN0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RILElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQzthQUNwRDtZQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDekMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2FBQzlCO1NBQ0osQ0FBQzs7WUFFRSxDQUFBLEtBQUs7UUFDVCxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7UUFDL0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7UUFDcEM7O2FBRUs7UUFDTCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUU7WUFDN0UsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7U0FDL0I7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFO1lBQy9FLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDaEQsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRTtZQUNoQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3RDLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ3RELElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDdEksSUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUMvSCxJQUFJLGtCQUFrQixJQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLEtBQUssWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNuRixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7b0JBQ2pELGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7aUJBQ3BDO2dCQUNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLFFBQVEsQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDbEosSUFBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsSUFBSSxDQUFDLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxLQUFLLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDbEksSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7Z0JBQ3BDLElBQU0sZ0JBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvQkFDckMsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxhQUFhLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUM1RSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDbkIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUM7NEJBQ3hCLE1BQU0sRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTTs0QkFDdkMsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRO3lCQUM5QyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7d0JBQ25DLFFBQVEsRUFBRSxrQkFBa0I7cUJBQy9CLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdkIsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLFFBQVEsRUFBRSxDQUFDLGFBQWE7NEJBQ25FLENBQUMsa0JBQWtCLElBQUksa0JBQWtCLENBQUMsY0FBYyxDQUFDLEtBQUssWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO3FCQUNoRyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6SCxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNoTCxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzVJLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRyxNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ25IOzs7Ozs7O3FCQU9DO29CQUNELGNBQWMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDNUUsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDaEUsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNqRDtpQkFBTTtnQkFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7b0JBQ3JDLElBQUksRUFBRSxJQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQ2hDLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDakcsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNuQixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQzt3QkFDbkcsUUFBUSxFQUFFLGtCQUFrQjtxQkFDL0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzt3QkFDN0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUMxSCxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO3dCQUNuRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNsSSxTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNyRCxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNuRCxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUM5RCxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO2lCQUNoQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQy9DO1NBQ0o7YUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFO1lBQ3RDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUU7Z0JBQ3JDLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDO2dCQUNwRCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLHVCQUF1QixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLFFBQVEsRUFBRTtvQkFDbEosSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7aUJBQ2pEO2dCQUNELHFDQUFxQztnQkFDckMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNuSSxJQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQy9ILDhCQUE4QjtnQkFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxrQkFBa0IsQ0FBQztnQkFDeEMsSUFBSSxrQkFBa0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxLQUFLLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDO29CQUNqRCxhQUFhLEdBQUcsSUFBSSxDQUFDO29CQUNyQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztnQkFDRCxJQUFJLGtCQUFrQixDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsTUFBTSxFQUFFO29CQUNoRCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7b0JBQ2pELGFBQWEsR0FBRyxJQUFJLENBQUM7aUJBQ3hCO2dCQUNELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLElBQUksT0FBTyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUMvSyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxPQUFPLENBQUMsYUFBYSxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQy9JLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxhQUFhLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxjQUFjLENBQUMsS0FBSyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2xJLElBQUksQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDO2dCQUVwQywyRUFBMkU7Z0JBRTNFLElBQU0sZ0JBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZELElBQU0scUJBQXFCLEdBQUcsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztvQkFDdkUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2xHLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvQkFDckMsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUMxRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLHFCQUFxQixFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQztvQkFDbkYsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNuQixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQzs0QkFDeEIsTUFBTSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNOzRCQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVE7eUJBQzlDLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQzt3QkFDbkMsUUFBUSxFQUFFLGtCQUFrQjtxQkFDL0IsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO29CQUNoRyxhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzNCLEtBQUssRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7NEJBQ3BELE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTTs0QkFDeEMsUUFBUSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRO3lCQUMvQyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJO3FCQUM1RCxDQUFDO29CQUNGLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdkIsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLEVBQUUsQ0FBQyxhQUFhOzRCQUN6RCxDQUFDLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxLQUFLLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQztxQkFDaEcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQy9HLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzlLLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ2pJLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDM0ksUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzNHLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkgscUdBQXFHO29CQUNyRyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUM1SixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsZ0JBQWMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLGdCQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hFOzs7Ozs7O3NCQU9FO29CQUNGLGNBQWMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekUsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO2lCQUM3SCxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQy9DLFVBQVUsQ0FBQSxDQUFDO2dCQUNUOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2dGQXdDZ0U7YUFDbkU7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO29CQUNyQyxJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUMvQixDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLENBQUM7b0JBQ2pFLFFBQVEsRUFBRSxJQUFJLFdBQVcsRUFBRTtvQkFDM0IsYUFBYSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUM7b0JBQzVELEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDbkIsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO3dCQUNqSSxRQUFRLEVBQUUsa0JBQWtCO3FCQUMvQixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMxQixLQUFLO29CQUNMOzs7aURBRzZCO29CQUM3QixLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO29CQUMxQixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO29CQUM3QixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO3dCQUM3RixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQzFILFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7d0JBQ25HLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xJLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3JELE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ25ELFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzlELGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMxRyxjQUFjLEVBQUUsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDO29CQUN2QyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7aUJBQ2hILENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDOUM7U0FDSjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTtnQkFDckMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGFBQWEsQ0FBQztnQkFDdEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7Z0JBQ3BDLG9DQUFvQztnQkFDcEMsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUM7Z0JBQ3BELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsdUJBQXVCLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLHVCQUF1QixDQUFDLEtBQUssUUFBUSxFQUFFO29CQUNsSixJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztpQkFDakQ7Z0JBQ0QscUNBQXFDO2dCQUNyQyxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ25JLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxhQUFhLElBQUksT0FBTyxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDL0ksSUFBTSxxQkFBcUIsR0FBRyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO29CQUN2RSxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDbEcsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO29CQUNyQyxJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLEVBQzFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDakcsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUscUJBQXFCLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUNqRjtvQkFDRCxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDOzRCQUN4QixNQUFNLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU07NEJBQ3ZDLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUTt5QkFDOUMsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDO3dCQUNuQyxRQUFRLEVBQUUsa0JBQWtCO3FCQUMvQixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsYUFBYSxFQUFFLENBQUM7b0JBQ2xGLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsQ0FBQztvQkFDOUUsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDOUssS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDakksUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzSSxNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ25ILFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RPLHFHQUFxRztvQkFDckc7Ozs7Ozs7c0JBT0U7b0JBQ0YsY0FBYyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6RSxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQzt3QkFDbkUsUUFBUSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO3FCQUM5RyxDQUFDO2lCQUNMLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2hEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztvQkFDckMsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLGNBQWMsRUFDaEMsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNqRyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO29CQUNqRSxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ25CLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO3dCQUNuRyxRQUFRLEVBQUUsa0JBQWtCO3FCQUMvQixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO29CQUMxQixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO29CQUM3QixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7NEJBQzVFLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSTtxQkFDNUksQ0FBQztvQkFDRixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO3dCQUNuRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNsSSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUM5RCxjQUFjLEVBQUUsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDO29CQUN2QyxnQkFBZ0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7aUJBQ2hILENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDOUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRTtZQUMzSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1NBQ25HO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDM0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDM0csSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFDaEcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUM1RyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQzNJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3hEO1FBQ0QsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRTtZQUM1QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDdEUsSUFBSSxRQUFRLEVBQUU7b0JBQ1YsTUFBTTtvQkFDTixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyxLQUFLLEVBQXpDLENBQXlDLENBQUMsQ0FBQSxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUMxSCxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQy9CLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLFlBQVksQ0FBQyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQzt3QkFDbk4sS0FBSSxDQUFDLHlCQUF5QixDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFROzRCQUMxRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDaEMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUM7Z0NBQzFFLE1BQU0sRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTTtnQ0FDeEMsUUFBUSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFROzZCQUMvQyxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUMxQyxDQUFDLENBQUMsQ0FBQztxQkFDTjt5QkFBTTt3QkFDSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7cUJBQ2hFO2lCQUNKO3FCQUFNO29CQUNILEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDaEU7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRTtZQUM3QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUMvRCxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDdkIsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDbEgsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLENBQUM7d0JBQ2hILEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLFlBQVksQ0FBQyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7d0JBQ3pKLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxlQUFlLENBQUMsbUNBQW1DLENBQUM7cUJBQ3BGO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUM7cUJBQ3pFO2lCQUNKO3FCQUFNO29CQUNILEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxlQUFlLENBQUMsd0JBQXdCLENBQUM7aUJBQ3pFO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUU7WUFDM0MsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDN0QsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLEVBQUU7b0JBQ3ZCLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDO3dCQUMvRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsRUFBRTt3QkFDL0csS0FBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxtQ0FBbUMsQ0FBQztxQkFDbEY7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxnQ0FBZ0MsQ0FBQztxQkFDL0U7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO3dCQUNqQixLQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGlDQUFpQyxDQUFDO3FCQUNoRjt5QkFBTTt3QkFDSCxLQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDLGdDQUFnQyxDQUFDO3FCQUMvRTtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxNQUFNO1FBQ1AsMEhBQTBIO1FBQ3pILElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzVKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO2dCQUMvRCxJQUFJLElBQUksRUFBRTtvQkFDTixLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxFQUFoQyxDQUFnQyxDQUFDLENBQUM7b0JBQ3BHLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTt3QkFDbkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQy9CLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7NEJBQ3pDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDOzRCQUNqRCxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDcEMsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUN4RDtxQkFBTTtvQkFDSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3hEO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNwRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO2dCQUMxRSxJQUFJLElBQUksRUFBRTtvQkFDTixLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNwRDtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRDs7Ozs7Ozs7Ozs7Ozs7WUFjSTtJQUNSLENBQUM7SUFFRCxzREFBbUIsR0FBbkIsVUFBb0IsYUFBYTtRQUFqQyxpQkE4QkM7UUE3QkcsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEtBQUssUUFBUSxFQUFFO1lBQ3hLLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7aUJBQ2xGLFNBQVMsQ0FBQyxVQUFBLGFBQWE7Z0JBQ3BCLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxRQUFRLEVBQUU7b0JBQ3pDLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxRQUFRLENBQUM7aUJBQy9EO2dCQUNELElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWE7dUJBQ3ZGLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDO3VCQUNyRSxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0UsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ3pDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDVjthQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsY0FBYyxJQUFJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsY0FBYyxLQUFLLFFBQVEsRUFBRTtZQUM1SyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQztpQkFDakYsU0FBUyxDQUFDLFVBQUEsYUFBYTtnQkFDcEIsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTtvQkFDekMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztpQkFDL0Q7Z0JBQ0QsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhO3VCQUNyRixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUM7dUJBQ3BFLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDNUUsSUFBTSxrQkFBa0IsR0FBRyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQzt3QkFDNUgsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDckcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztpQkFDdkQ7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNWO2FBQU07WUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7SUFFRCw4REFBMkIsR0FBM0IsVUFBNEIsS0FBSztRQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUEsU0FBUztZQUMxRSxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUMvRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtREFBZ0IsR0FBaEIsVUFBaUIsU0FBUztRQUExQixpQkE2QkM7UUE1QkcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixTQUFTLEVBQUUsU0FBUztTQUN2QixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBLHlEQUF5RCxDQUFDLENBQUM7YUFDeEwsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUU7Z0JBQ2pDLElBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQztnQkFDOUMsSUFBTSxlQUFlLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFGLElBQU0sYUFBYSxHQUFRLGVBQWUsSUFBSSxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUM7Z0JBQ2hILDRHQUE0RztnQkFDNUcsSUFBTSxvQkFBb0IsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsT0FBTyxFQUExQyxDQUEwQyxDQUFDLENBQUM7Z0JBQ3RHLElBQUksb0JBQW9CLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksS0FBSSxDQUFDLDBCQUEwQixFQUFFO29CQUNySixLQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztnQkFDRCxLQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNuRCxtREFBbUQ7Z0JBQ25ELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDSCxFQUFFO2dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO1FBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHFEQUFxRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxvREFBaUIsR0FBakIsVUFBa0IsVUFBVTtRQUE1QixpQkF1QkM7UUF0QkcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFBLDJEQUEyRDtTQUN0TCxDQUFDO2FBQ0csU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2xDLElBQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDO2dCQUNoRDttRUFDbUQ7Z0JBQ25ELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDSCxFQUFFO2dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO1FBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNKLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxNQUFNO0lBQ04sa0RBQWUsR0FBZixVQUFnQixVQUFVO1FBQTFCLGlCQW9DQztRQW5DRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLGdCQUFnQixHQUFHO2dCQUNyQixVQUFVLEVBQUUsVUFBVTthQUN6QixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLDBCQUEwQixFQUFFLEtBQUksQ0FBQywyQkFBMkIsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDN0csU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFDbkMsUUFBUSxDQUFDLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDM0M7b0JBQ0QsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO29CQUl0QyxJQUFNLG9CQUFrQixHQUFHLEVBQUUsQ0FBQztvQkFDOUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxRQUFRO3dCQUMxQixJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQy9DLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7NEJBQy9FLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxZQUFZLENBQUMsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxFQUFFOzRCQUN4SCxvQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0NBQ3BCLElBQUksRUFBRSxRQUFRLENBQUMsYUFBYTtnQ0FDNUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO2dDQUNqQyxXQUFXLEVBQUUsS0FBSzs2QkFDckIsQ0FBQyxDQUFDO3lCQUNOO29CQUNMLENBQUMsQ0FBQyxDQUFDO29CQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsYUFBYSxHQUFHLG9CQUFrQixDQUFDO2lCQUM3RTtxQkFBTTtvQkFDSCxLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztvQkFDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUM3RDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELCtDQUFZLEdBQVosVUFBYSxnQkFBZ0I7UUFDekIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVELGtEQUFlLEdBQWYsVUFBZ0IsT0FBWSxFQUFFLE1BQWU7UUFDekMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLGVBQWUsSUFBSSxNQUFNLElBQUksTUFBTSxLQUFLLEVBQUUsRUFBRTtZQUMvRCxPQUFPO2dCQUNILElBQUksRUFBRSxPQUFPLENBQUMsZUFBZTtnQkFDN0IsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsV0FBVyxFQUFFLE9BQU8sQ0FBQyxlQUFlLEdBQUcsSUFBSSxHQUFHLE1BQU0sR0FBRyxHQUFHO2FBQzdELENBQUM7U0FDTDthQUFNLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7WUFDM0QsT0FBTztnQkFDSCxJQUFJLEVBQUUsT0FBTyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsUUFBUTtnQkFDN0MsS0FBSyxFQUFFLE9BQU8sQ0FBQyxNQUFNO2dCQUNyQixXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sR0FBRyxHQUFHO2FBQ3ZGLENBQUM7U0FDTDthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFRCx1REFBb0IsR0FBcEIsVUFBcUIsVUFBZTtRQUNoQyxJQUFNLHdCQUF3QixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLGdCQUFnQixJQUFJLE9BQUEsZ0JBQWdCLENBQUMsS0FBSyxLQUFLLFVBQVUsRUFBckMsQ0FBcUMsQ0FBQyxDQUFDO1FBQzFKLE9BQU8sd0JBQXdCLENBQUMsQ0FBQyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDdEUsQ0FBQztJQUVELDJEQUF3QixHQUF4QjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUc7WUFDbEIsU0FBUyxFQUFFLElBQUk7WUFDZixRQUFRLEVBQUUsSUFBSTtZQUNkLGFBQWEsRUFBRSxJQUFJO1lBQ25CLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLHlCQUF5QjtZQUN6QixlQUFlLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRTtZQUMzRCxtQkFBbUIsRUFBRSxLQUFLO1lBQzFCLGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFO1lBQ3JELGlCQUFpQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7WUFDN0QsYUFBYSxFQUFFLElBQUk7WUFDbkIsZUFBZSxFQUFFLElBQUk7WUFDckIsY0FBYyxFQUFFLElBQUk7WUFDcEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsa0JBQWtCLEVBQUUsS0FBSztZQUN6QixnQkFBZ0IsRUFBRTtnQkFDZCxLQUFLLEVBQUUsT0FBTztnQkFDZCxhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCx3QkFBd0IsRUFBRTtnQkFDdEIsS0FBSyxFQUFFLGdCQUFnQjtnQkFDdkIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0Qsa0JBQWtCLEVBQUU7Z0JBQ2hCLEtBQUssRUFBRSxlQUFlO2dCQUN0QixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxzQkFBc0IsRUFBRTtnQkFDcEIsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLEtBQUssRUFBRSxtQkFBbUI7Z0JBQzFCLGFBQWEsRUFBRSxFQUFFO2FBQ3BCO1lBQ0QsY0FBYyxFQUFFLElBQUk7U0FDdkIsQ0FBQztJQUNOLENBQUM7SUFFRCx3REFBcUIsR0FBckI7UUFDSSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVELGdEQUFhLEdBQWI7UUFBQSxpQkF5T0M7UUF4T0csc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDO2FBQ2hGLFNBQVMsQ0FBQyxVQUFBLGFBQWE7WUFDcEIsSUFBSSxhQUFhLENBQUMsV0FBVyxLQUFLLGVBQWUsQ0FBQywyQkFBMkIsSUFBSSxhQUFhLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxrQkFBa0IsSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssUUFBUSxJQUFJLGFBQWEsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLHVCQUF1QixDQUFDLEVBQUU7Z0JBQzFRLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCOzs7Ozs7Ozs7Ozs7c0JBWU07Z0JBQ04sSUFBTSxxQkFBbUIsR0FBRyxhQUFhLENBQUMsV0FBVyxLQUFLLGVBQWUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsOENBQThDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssZUFBZSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxrREFBa0QsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxLQUFLLFFBQVEsSUFBSSxhQUFhLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUMvYixLQUFJLENBQUMsYUFBYSxDQUFDLHlCQUF5QixDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxZQUFZO29CQUNsRyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUMxQixhQUFhLENBQUMsbUJBQW1CLENBQUMsR0FBRyxZQUFZLENBQUM7b0JBQ2xELElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFO3dCQUN0RCxLQUFLLEVBQUUsS0FBSzt3QkFDWixZQUFZLEVBQUUsSUFBSTt3QkFDbEIsSUFBSSxFQUNKOzRCQUNJLDJEQUEyRDs0QkFDM0QsT0FBTyxFQUFFLHFCQUFtQjs0QkFDNUIsc0JBQXNCLEVBQUUsSUFBSTs0QkFDNUIsV0FBVyxFQUFFLFVBQVU7NEJBQ3ZCLGlCQUFpQixFQUFFLElBQUk7NEJBQ3ZCLFlBQVksRUFBRSxnQ0FBZ0M7NEJBQzlDLFlBQVksRUFBRSxLQUFLOzRCQUNuQixZQUFZLEVBQUUsSUFBSTs0QkFDbEIsVUFBVSxFQUFFLGFBQWE7NEJBQ3pCLGFBQWEsRUFBRSxnQkFBZ0I7NEJBQy9CLHVCQUF1Qjt5QkFDMUI7cUJBQ0osQ0FBQyxDQUFDO29CQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO3dCQUNwQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsyQkFpREc7d0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDcEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsSUFBSSxNQUFNLElBQUksTUFBTSxLQUFLLE1BQU0sRUFBRTs0QkFDN0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDMUIsa0NBQWtDOzRCQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUNwQiw0Q0FBNEM7NEJBQzVDLDJDQUEyQzs0QkFHM0MsSUFBSSxRQUFNLEdBQUcsRUFBRSxDQUFDOzRCQUNoQixNQUFNLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFDLGNBQWMsRUFBRSxLQUFLO2dDQUNqRCxJQUFNLG9CQUFvQixHQUFHLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBN0QsQ0FBNkQsQ0FBQyxDQUFDO2dDQUNoSixRQUFNLEdBQUcsUUFBTSxHQUFHLG9CQUFvQixDQUFDLElBQUksQ0FBQztnQ0FDNUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRTtvQ0FDN0MsUUFBTSxHQUFHLFFBQU0sR0FBRyxHQUFHLENBQUM7aUNBQ3pCOzRCQUNMLENBQUMsQ0FBQyxDQUFDOzRCQUNILElBQUksVUFBVSxTQUFBLENBQUM7NEJBQ2YsSUFBSSxRQUFNLEVBQUU7Z0NBQ1IsVUFBVSxHQUFHLFFBQU0sR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQzs2QkFDaEQ7aUNBQU07Z0NBQ0gsVUFBVSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7NkJBQy9COzRCQUNELEtBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDOzRCQUVoQyxJQUFNLGNBQWMsR0FBRyxhQUFhLENBQUMsV0FBVyxLQUFLLGVBQWUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsNkVBQTZFLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixHQUFHLGFBQWEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQ3BRLHVPQUF1Tzs0QkFDdk8sS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQzs0QkFDakQsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTtnQ0FDN0MsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztnQ0FDL0IsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO29DQUM1RCxJQUFJLFNBQVMsU0FBQSxDQUFDO29DQUNkLElBQUksS0FBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDakIsU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7cUNBQ25IO3lDQUFNLElBQUksS0FBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDeEIsU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7cUNBQ25IO3lDQUFNO3dDQUNILFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxxQ0FBcUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO3FDQUNsSDtvQ0FDRCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUN6QyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO2lDQUNuQztxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7b0NBQ3JHLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUU7d0NBQ3JDLGlEQUFpRDt3Q0FDakQsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7d0NBQzVCLDhCQUE4QjtxQ0FDakM7eUNBQU07d0NBQ0gsS0FBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7NkNBQzNFLFNBQVMsQ0FDTixVQUFBLE9BQU87NENBQ0gsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7d0NBQ2hDLENBQUMsQ0FDSixDQUFDO3FDQUNUO2lDQUNKOzZCQUNKO2lDQUFNO2dDQUNILElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQ2xILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0NBQ3pDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7Z0NBQ2hDLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dDQUNoQyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDOzZCQUM3Qjt5QkFDSjs2QkFBTTs0QkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3lCQUM3QjtvQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEtBQUssT0FBTyxFQUFFO29CQUM3QyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO29CQUMvQixJQUFJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7d0JBQzVELElBQUksU0FBUyxTQUFBLENBQUM7d0JBQ2QsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFOzRCQUNqQixTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsc0NBQXNDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt5QkFDbkg7NkJBQU0sSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFOzRCQUN4QixTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsc0NBQXNDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt5QkFDbkg7NkJBQU07NEJBQ0gsU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHFDQUFxQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7eUJBQ2xIO3dCQUNELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ3pDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7cUJBQ25DO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDckcsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTs0QkFDckMsaURBQWlEOzRCQUNqRCxLQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQzt5QkFDL0I7NkJBQU07NEJBQ0gsS0FBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7aUNBQzNFLFNBQVMsQ0FDTixVQUFBLE9BQU87Z0NBQ0gsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7NEJBQ2hDLENBQUMsQ0FDSixDQUFDO3lCQUNUO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQ2xILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7aUJBQ25DO2FBQ0o7UUFFTCxDQUFDLEVBQUUsVUFBQSxrQkFBa0I7WUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ1A7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUErQkk7SUFDUixDQUFDO0lBRUQsdURBQW9CLEdBQXBCO1FBQ0ksSUFBTSxhQUFhLEdBQXlDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzFGLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELDhDQUFXLEdBQVgsVUFBWSxhQUFhO1FBQXpCLGlCQTBEQztRQXhERyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLG1CQUFtQixDQUFDO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRTtZQUN0QyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQztTQUN2RTtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUU7WUFDckMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUN6RyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkMsYUFBYSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBRXBDLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ3hHLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLHVCQUF1QixFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3pJLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDdEUsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSw2Q0FBNkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUM5SCxLQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ2pFLENBQUMsQ0FBQyxDQUFDO2FBRU47aUJBQ0k7Z0JBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7YUFDbkM7U0FDSjthQUFNO1lBRUgsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixJQUFNLFVBQVUsR0FBRztvQkFDZixjQUFjLEVBQUUsYUFBYTtpQkFDaEMsQ0FBQztnQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxVQUFVLENBQUM7cUJBQ3JHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsdUJBQXVCLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDekksS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNyRSxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGlEQUFpRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2xJLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDaEUsQ0FBQyxDQUFDLENBQUM7YUFFVjtpQkFBTTtnQkFDSCxJQUFNLFVBQVUsR0FBRztvQkFDZixhQUFhLEVBQUUsYUFBYTtpQkFDL0IsQ0FBQztnQkFDRixJQUFJLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxVQUFVLENBQUM7cUJBQ3BHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsdUJBQXVCLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDekksS0FBSSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUNyRSxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdEQUFnRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2pJLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDaEUsQ0FBQyxDQUFDLENBQUM7YUFDVjtTQUVKO0lBQ0wsQ0FBQztJQUVELGlEQUFjLEdBQWQ7UUFBQSxpQkErQ0M7UUE5Q0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDdkQsSUFBTSxXQUFXLEdBQUcsQ0FBQyxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsaUJBQWlCLElBQUksS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUksbURBQW1ELENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUMvRCxJQUFNLGFBQWEsR0FBRztnQkFDbEIseUJBQXlCO2dCQUN6QiwyQkFBMkI7Z0JBQzNCLGlCQUFpQixFQUFFLEVBQUU7YUFDeEIsQ0FBQztZQUNGLGFBQWEsQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUM7WUFDM0MsSUFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUU7Z0JBQ3RELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQ0o7b0JBQ0ksT0FBTyxFQUFFLGlCQUFpQjtvQkFDMUIsc0JBQXNCLEVBQUUsSUFBSTtvQkFDNUIsV0FBVyxFQUFFLFVBQVU7b0JBQ3ZCLGlCQUFpQixFQUFFLElBQUk7b0JBQ3ZCLFlBQVksRUFBRSxnQ0FBZ0M7b0JBQzlDLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtvQkFDbEIsVUFBVSxFQUFFLGFBQWE7b0JBQ3pCLE1BQU0sRUFBRSxFQUFFO29CQUNWLElBQUksRUFBRSxXQUFXO2lCQUNwQjthQUNKLENBQUMsQ0FBQztZQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO2dCQUNwQyxJQUFJLE1BQU0sSUFBSSxNQUFNLEtBQUssTUFBTSxFQUFFO29CQUM3QixJQUFJLFFBQU0sR0FBRyxFQUFFLENBQUM7b0JBQ2hCLE1BQU0sQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUMsY0FBYyxFQUFFLEtBQUs7d0JBQ2pELElBQU0sb0JBQW9CLEdBQUcsUUFBUSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxjQUFjLENBQUMsRUFBRSxFQUE3RCxDQUE2RCxDQUFDLENBQUM7d0JBQzVJLFFBQU0sR0FBRyxRQUFNLEdBQUcsb0JBQW9CLENBQUMsSUFBSSxDQUFDO3dCQUM1QyxJQUFJLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxFQUFFOzRCQUM3QyxRQUFNLEdBQUcsUUFBTSxHQUFHLEdBQUcsQ0FBQzt5QkFDekI7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxVQUFVLFNBQUEsQ0FBQztvQkFDZixJQUFJLFFBQU0sRUFBRTt3QkFDUixVQUFVLEdBQUcsUUFBTSxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO3FCQUNoRDt5QkFBTTt3QkFDSCxVQUFVLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztxQkFDL0I7b0JBQ0QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUMxQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0RBQXFCLEdBQXJCLFVBQXNCLFVBQVU7UUFBaEMsaUJBdUNDO1FBdENHLElBQU0sT0FBTyxHQUFHLFVBQVUsQ0FBQztRQUMzQixJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEVBQTFHLENBQTBHLENBQUMsQ0FBQztRQUMxTCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEVBQTVGLENBQTRGLENBQUMsQ0FBQztRQUNqSyxJQUFNLGtCQUFrQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxZQUFZLEVBQS9DLENBQStDLENBQUMsQ0FBQztRQUNwSSxJQUFNLGlCQUFpQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUNyRSxJQUFNLGVBQWUsR0FBRztZQUNwQixpQkFBaUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxTQUFTO1lBQ2hFLGtCQUFrQixFQUFFLEtBQUs7WUFDekIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhO1lBQ3BFLG1CQUFtQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQztZQUNwRyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxRQUFRO1lBQzlELGdCQUFnQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFFBQVE7WUFDOUQsWUFBWSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFlBQVk7WUFDOUQsa0JBQWtCLEVBQUUsaUJBQWlCLENBQUMsZUFBZTtZQUNyRCxnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQyxXQUFXO1lBQzlDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFVBQVU7WUFDbEUsY0FBYyxFQUFFLGtCQUFrQixDQUFDLElBQUk7WUFDdkMsWUFBWSxFQUFFLFlBQVksQ0FBQyxJQUFJO1lBQy9CLFNBQVMsRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO1lBQ25DLGVBQWUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNO1lBQ3pFLGdCQUFnQixFQUFFLENBQUM7WUFDbkIsWUFBWSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFlBQVk7WUFDOUQsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsa0JBQWtCO1lBQzFFLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO1lBQ2pELG1CQUFtQixFQUFFLGtCQUFrQixDQUFDLFdBQVc7WUFDbkQsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUU7WUFDaEUsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsVUFBVTtZQUMzRSx5QkFBeUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxRQUFRO1lBQ3ZFLHlCQUF5QixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDO1lBQ2hILGtCQUFrQixFQUFFLENBQUM7WUFDckIsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO1NBQ2hKLENBQUM7UUFDRixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQzNGLElBQUksUUFBUSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsd0NBQXdDLENBQUMsQ0FBQzthQUMzRTtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9EQUFpQixHQUFqQjtRQUFBLGlCQWtCQztRQWpCRyxPQUFPLElBQUksVUFBVSxDQUNqQixVQUFBLFFBQVE7WUFDSixJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUseURBQXlEO29CQUNsRSxZQUFZLEVBQUUsS0FBSztvQkFDbkIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07Z0JBQ3BDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUVELGdEQUFhLEdBQWI7UUFBQSxpQkFlQztRQWRHLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQzNELEtBQUssRUFBRSxLQUFLO1lBQ1osWUFBWSxFQUFFLElBQUk7WUFDbEIsSUFBSSxFQUFFO2dCQUNGLE9BQU8sRUFBRSwrQ0FBK0M7Z0JBQ3hELFlBQVksRUFBRSxLQUFLO2dCQUNuQixZQUFZLEVBQUUsSUFBSTthQUNyQjtTQUNKLENBQUMsQ0FBQztRQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ3BDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN0QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlEQUFzQixHQUF0QjtRQUNJLElBQUksYUFBbUQsQ0FBQztRQUN4RCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUQsOEZBQThGO1FBQzlGLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuSCxJQUFNLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7UUFDckgsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNkJBQTZCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN4SDthQUFNO1lBQ0gsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqSSxrQ0FBa0M7WUFDbEMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7bUJBQ3JGLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDekQsYUFBYSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNsRixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDO3VCQUMzRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUNyRSxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDdEc7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDO3VCQUN0SCxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQzVFLGFBQWEsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ3JIO2FBQ0o7U0FDSjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFO1lBQ3hFLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDbEc7UUFFRCxhQUFhLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUN6RyxPQUFPLGFBQWEsQ0FBQztJQUN6QixDQUFDO0lBQ0QsNERBQXlCLEdBQXpCLFVBQTBCLFFBQWEsRUFBRSxTQUFjLEVBQUUsVUFBbUIsRUFBRSxPQUFnQjs7UUFDMUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xDLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO29CQUMzQyxTQUFTLEdBQUc7d0JBQ1IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFlBQVk7d0JBQ2hELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSztxQkFDbEQsQ0FBQztvQkFDRixJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO29CQUNoQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM1QztxQkFBTSxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUs7b0JBQ2hELENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU87MkJBQzFELFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7MkJBQy9DLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUTsyQkFDbkksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQzsyQkFDakQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUU7b0JBQ25FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLElBQUksVUFBVSxFQUFFO3dCQUNaLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDakMsTUFBQSxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLEtBQUssR0FBRzt3QkFDaEMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7NEJBQ25DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7Z0NBQzFCLFNBQVMsRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRTtnQ0FDN0QsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDOzZCQUM5RCxDQUFDLENBQUM7eUJBQ047NkJBQU07NEJBQ0gsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQztnQ0FDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFOzZCQUNuRSxDQUFDLENBQUM7eUJBQ047cUJBRUo7eUJBQU07d0JBQ0gsb0NBQW9DO3dCQUNwQyxtQ0FBbUM7d0JBQ25DLDRCQUE0Qjt3QkFDNUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUNuQjtpQkFDSjthQUNKO1NBQ0o7YUFBTTtZQUNILElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM1QztJQUNMLENBQUM7SUFFRCwwQ0FBTyxHQUFQO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUN6QixDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUFBLGlCQXVCQztRQXRCRyxJQUFJLFNBQVMsQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QztRQUNELElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM3RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3RJLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNJLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO1FBQ2xJLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDL0ksSUFBSSxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNySixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzVDLElBQUksTUFBTSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsS0FBSSxDQUFDLGtCQUFrQixHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7YUFDakM7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztnQkFDL0IsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQzthQUNsQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFTSxpREFBYyxHQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQztJQUMxQyxDQUFDO0lBR0QscURBQWtCLEdBQWxCLFVBQW1CLE1BQVc7UUFDMUIsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUU7WUFDdkIsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7U0FDN0I7SUFDTCxDQUFDO0lBRUQsaURBQWMsR0FBZCxVQUFlLEVBQU8sRUFBRSxFQUFPO1FBQzNCLElBQUcsRUFBRSxJQUFHLEVBQUUsRUFBQztZQUNULE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDOztnQkF4NkRZLFdBQVc7Z0JBQ04sV0FBVztnQkFDWixTQUFTO2dCQUNELGNBQWM7Z0JBQ2pCLFdBQVc7Z0JBQ1osVUFBVTtnQkFDVCxXQUFXO2dCQUNILG1CQUFtQjtnQkFDdkIsZUFBZTtnQkFDakIsYUFBYTtnQkFDUixrQkFBa0I7Z0JBQ3RCLGNBQWM7Z0JBQ0YsMEJBQTBCO2dCQUN0QyxjQUFjO2dCQUNULG1CQUFtQjtnQkFDdkIsZUFBZTtnQkFDWCxtQkFBbUI7Z0JBQ3pCLGFBQWE7Z0JBQ2xCLFFBQVE7O0lBakhwQjtRQUFSLEtBQUssRUFBRTsrREFBVztJQUNWO1FBQVIsS0FBSyxFQUFFO2dFQUFZO0lBQ1g7UUFBUixLQUFLLEVBQUU7a0VBQWM7SUFDYjtRQUFSLEtBQUssRUFBRTtnRUFBWTtJQUNYO1FBQVIsS0FBSyxFQUFFO2dFQUFZO0lBRVY7UUFBVCxNQUFNLEVBQUU7MEVBQWdEO0lBQy9DO1FBQVQsTUFBTSxFQUFFO3lFQUErQztJQUM5QztRQUFULE1BQU0sRUFBRTt5RUFBK0M7SUFDOUM7UUFBVCxNQUFNLEVBQUU7b0VBQTBDO0lBQ3pDO1FBQVQsTUFBTSxFQUFFOzJFQUFpRDtJQXVFbkI7UUFBdEMsU0FBUyxDQUFDLDBCQUEwQixDQUFDO2dGQUF3RDtJQTQ2RDlGO1FBREMsWUFBWSxDQUFDLHFCQUFxQixFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7c0VBSy9DO0lBbmdFUSx3QkFBd0I7UUFOcEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHNCQUFzQjtZQUNoQywwM2VBQWdEOztTQUVuRCxDQUFDO09BRVcsd0JBQXdCLENBc2hFcEM7SUFBRCwrQkFBQztDQUFBLEFBdGhFRCxJQXNoRUM7U0F0aEVZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIFVzYWdlIGFuZCBwYXJhbWV0ZXIgZGV0YWlsc1xyXG48bXBtLXByb2plY3Qtb3ZlcnZpZXcgW3Byb2plY3RJZF09XCJwcm9qZWN0SWRcIiBbaXNUZW1wbGF0ZV09XCJmYWxzZVwiIFt0ZWFtc09wdGlvbnNdPVwidGVhbXNPcHRpb25zXCJcclxuICAgICAoY2xvc2VDYWxsYmFja0hhbmRsZXIpPVwiYWZ0ZXJDbG9zZVByb2plY3QoJGV2ZW50KVwiXHJcbiAgICAoc2F2ZUNhbGxCYWNrSGFuZGxlcik9XCJhZnRlclNhdmVQcm9qZWN0KCRldmVudClcIiAobm90aWZpY2F0aW9uSGFuZGxlcik9XCJub3RpZmljYXRpb25IYW5kbGVyKCRldmVudClcIlxyXG4gICAgKGxvYWRpbmdIYW5kbGVyKT1cImxvYWRpbmdIYW5kbGVyKCRldmVudClcIj48L21wbS1wcm9qZWN0LW92ZXJ2aWV3PlxyXG5cclxuPCEtLVxyXG4gICAgcHJvamVjdElkOiBJZCBvZiB0aGUgZXhpc3RpbmcgcHJvamVjdCB0byBiZSB1cGRhdGVkIC0tIE9wdGlvbmFsIGZvciBuZXcgcHJvamVjdC8gdGVtcGxhdGVcclxuICAgIGlzVGVtcGxhdGU6IHRydWUgZm9yIFRlbXBsYXRlIHR5cGUgYW5kIGZhbHNlIGZvciBQcm9qZWN0IHR5cGVcclxuICAgIHRlYW1zT3B0aW9uczogUGVyc2lzdGVkIEFycmF5IG9mIFRlYW1zIGZyb20gdGhlIHJlc3BvbnNlIG9mIEdldEFsbFRlYW0gV2Vic2VydmljZSAtLSBvcHRpb25hbDtcclxuICAgIGNsb3NlQ2FsbGJhY2tIYW5kbGVyOiBmdW5jdGlvbiBoYW5kbGVyIGZvciBjYW5jZWwgY3JlYXRlL3VwZGF0ZSBwcm9qZWN0LyB0ZW1wbGF0ZVxyXG4gICAgc2F2ZUNhbGxCYWNrSGFuZGxlcjogZnVuY3Rpb24gaGFuZGxlciBmb3IgYWZ0ZXIgc2F2ZS91cGRhdGUgcHJvamVjdC8gdGVtcGxhdGUgQG91dHB1dChwcm9qZWN0SWQsIGlzVGVtcGxhdGUpXHJcbiAgICBub3RpZmljYXRpb25IYW5kbGVyOiBmdW5jdGlvbiB0byBzaG93IG5vdGlmaWNhdGlvbnMgQG91dHB1dCh7bWVzc2FnZSwgdHlwZX0pIC0tIHR5cGUgaXMgTk9USUZJQ0FUSU9OX1RZUEVcclxuICAgIGxvYWRpbmdIYW5kbGVyOiBmdW5jdGlvbiB0byBzaG93LyBoaWRlIGxvYWRpbmcgbWVzc2FnZSBAb3V0cHV0KGlzU2hvdykgLS0gaXNTaG93OiBib29sZWFuIHRydWUgZm9yIHNob3dpbmcgYW5kIGZhbHNlIGZvciBoaWRpbmdcclxuLS0+XHJcbiovXHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgVmlld0NoaWxkLCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgSG9zdExpc3RlbmVyLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycywgRm9ybUFycmF5LCBGb3JtQnVpbGRlciB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RVcGRhdGVPYmogfSBmcm9tICcuL3Byb2plY3QudXBkYXRlJztcclxuaW1wb3J0IHsgUHJvamVjdE1ldGFkYXRhT2JqIH0gZnJvbSAnLi9wcm9qZWN0Lm1ldGFkYXRhJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY3VzdG9tLXNlYXJjaC1maWVsZC9jdXN0b20tc2VhcmNoLWZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBPdG1tTWV0YWRhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE1QTV9ST0xFUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1JvbGUnO1xyXG5pbXBvcnQgeyBSb2xlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1JvbGVzJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUZWFtIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVGVhbSc7XHJcbmltcG9ydCB7IERhdGVWYWxpZGF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3ZhbGlkYXRvcnMvZGF0ZS52YWxpZGF0b3InO1xyXG5pbXBvcnQgeyBEZWFjdGl2YXRpb25HdWFyZGVkIH0gZnJvbSAnLi9jYW5EZWFjdGl2YXRlLmd1YXJkJztcclxuaW1wb3J0IHsgUHJpb3JpdHkgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9Qcmlvcml0eSc7XHJcbmltcG9ydCB7IFByb2plY3RUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1Byb2plY3RUeXBlcyc7XHJcbmltcG9ydCB7IE92ZXJWaWV3Q29uZmlnIH0gZnJvbSAnLi9PdmVydmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFBlcnNvbiB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1BlcnNvbic7XHJcbmltcG9ydCB7IFN0YXR1c1R5cGVzLCBTdGF0dXNMZXZlbHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgU3RhdHVzU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zdGF0dXMuc2VydmljZSc7XHJcbmltcG9ydCB7IERhdGFUeXBlQ29uc3RhbnRzIH0gZnJvbSAnLi4vc2hhcmVkL2NvbnN0YW50cy9kYXRhLXR5cGUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgUHJvamVjdFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbmV3QXJyYXkgfSBmcm9tICdAYW5ndWxhci9jb21waWxlci9zcmMvdXRpbCc7XHJcbmltcG9ydCB7IFByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtZnJvbS10ZW1wbGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBDYW1wYWlnblVwZGF0ZU9iaiB9IGZyb20gJy4vY2FtcGFpZ24udXBkYXRlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbW1lbnRzU2VydmljZSB9IGZyb20gJy4uLy4uL2NvbW1lbnRzL3NlcnZpY2VzL2NvbW1lbnRzLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBCdWxrQ29tbWVudHNDb21wb25lbnQgfSBmcm9tICcuLi8uLi9jb21tZW50cy9idWxrLWNvbW1lbnRzL2J1bGstY29tbWVudHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgT1RNTU1QTURhdGFUeXBlcyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL09UTU1NUE1EYXRhVHlwZXMnO1xyXG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IENhdGVnb3J5TGV2ZWwgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9DYXRlZ29yeUxldmVsJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1wcm9qZWN0LW92ZXJ2aWV3JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wcm9qZWN0LW92ZXJ2aWV3LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Byb2plY3Qtb3ZlcnZpZXcuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFByb2plY3RPdmVydmlld0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgRGVhY3RpdmF0aW9uR3VhcmRlZCxPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIHByb2plY3RJZDtcclxuICAgIEBJbnB1dCgpIGlzVGVtcGxhdGU7XHJcbiAgICBASW5wdXQoKSB0ZWFtc09wdGlvbnM7XHJcbiAgICBASW5wdXQoKSBpc0NhbXBhaWduO1xyXG4gICAgQElucHV0KCkgY2FtcGFpZ25JZDtcclxuXHJcbiAgICBAT3V0cHV0KCkgY2xvc2VDYWxsYmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzYXZlQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgbm90aWZpY2F0aW9uSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGxvYWRpbmdIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVzdGFydFByb2plY3RIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG5cclxuICAgIFBST0pFQ1RfQlBNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIFRFQU1fQlBNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vdGVhbXMvYnBtLzEuMCc7XHJcbiAgICBQUklPUklUWV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1ByaW9yaXR5L29wZXJhdGlvbnMnO1xyXG4gICAgUFJPSkVDVF9ERVRBSUxTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Qcm9qZWN0L29wZXJhdGlvbnMnO1xyXG4gICAgQ0FNUEFJR05fREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvQ2FtcGFpZ24vb3BlcmF0aW9ucyc7XHJcbiAgICBURUFNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fVGVhbXMvb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgR0VUX1RFQU1fV1NfTUVUSE9EX05BTUUgPSAnR2V0VGVhbXNGb3JVc2VyJztcclxuICAgIEdFVF9QUklPUklUWV9XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxQcmlvcml0eUZvckNhdGVnb3J5TGV2ZWxOYW1lJztcclxuICAgIEdFVF9QUk9KRUNUX0RFVEFJTFNfV1NfTUVUSE9EX05BTUUgPSAnR2V0UHJvamVjdEJ5SUQnO1xyXG4gICAgQ1JFQVRFX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUgPSAnQ3JlYXRlUHJvamVjdCc7XHJcbiAgICBVUERBVEVfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSA9ICdVcGRhdGVQcm9qZWN0RGV0YWlscyc7XHJcbiAgICBDUkVBVEVfQ0FNUEFJR05fV1NfTUVUSE9EX05BTUUgPSAnQ3JlYXRlQ2FtcGFpZ24nO1xyXG4gICAgR0VUX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FID0gJ0dldENhbXBhaWduQnlJZCc7XHJcbiAgICBHRVRfQUxMX0NBTVBBSUdOX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbENhbXBhaWduJztcclxuICAgIEdFVF9BTExfVEVBTVNfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsVGVhbXMnO1xyXG4gICAgR0VUX1VTRVJTX0ZPUl9URUFNX1JPTEVfV1NfTUVUSE9EX05BTUUgPSAnR2V0VXNlcnNGb3JUZWFtUm9sZSc7XHJcbiAgICBHRVRfQUxMX2NBTVBBSUdOX0JZX05BTUVfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsQ2FtcGFpZ25CeU5hbWUnO1xyXG5cclxuICAgIHByb2plY3RPdmVydmlld0Zvcm06IEZvcm1Hcm91cDtcclxuICAgIGN1c3RvbUZpZWxkc0dyb3VwO1xyXG5cclxuICAgIG1pblN0YXJ0RGF0ZTogRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICBvdmVyVmlld0NvbmZpZzogT3ZlclZpZXdDb25maWc7XHJcbiAgICBkaXNhYmxlT3B0aW9ucyA9IHRydWU7XHJcbiAgICBkaXNhYmxlU3RhdHVzT3B0aW9ucyA9IHRydWU7XHJcbiAgICBkaXNhYmxlRWRpdE9wdGlvbnMgPSBmYWxzZTtcclxuICAgIHNob3dTdGFuZGFyZFN0YXR1cyA9IHRydWU7XHJcbiAgICBzZWxlY3RlZFN0YXR1czogc3RyaW5nO1xyXG4gICAgc2VsZWN0ZWRQcmlvcml0eTogYW55O1xyXG4gICAgc2VsZWN0ZWRTdGFydERhdGU7XHJcbiAgICBzZWxlY3RlZEVuZERhdGU7XHJcbiAgICBkaXNhYmxlUHJvamVjdFNhdmUgPSBmYWxzZTtcclxuICAgIGN1c3RvbU1ldGFkYXRhRmllbGRzO1xyXG4gICAgcHJvamVjdE93bmVyRGV0YWlsczogUGVyc29uO1xyXG4gICAgcHJvamVjdE93bmVySWQ6IHN0cmluZztcclxuICAgIG9sZFByb2plY3RNZXRhZGF0YTogUHJvamVjdFVwZGF0ZU9iaiB8IENhbXBhaWduVXBkYXRlT2JqO1xyXG4gICAgcHJvamVjdE1ldGFkYXRhOiBQcm9qZWN0TWV0YWRhdGFPYmo7XHJcbiAgICBkaXNhYmxlU3RhcnREYXRlID0gZmFsc2U7XHJcbiAgICBkaXNhYmxlRW5kRGF0ZSA9IGZhbHNlO1xyXG5cclxuICAgIGFwcENvbmZpZztcclxuICAgIG5hbWVTdHJpbmdQYXR0ZXJuO1xyXG4gICAgZW5hYmxlQ2FtcGFpZ247XHJcbiAgICBjYW1wYWlnbkxpc3Q7XHJcbiAgICBzZWxlY3RlZENhbXBhaWduSWQ7XHJcbiAgICBzZWxlY3RlZENhbXBhaWduO1xyXG4gICAgY2FtcGFpZ25Pd25lckRldGFpbHM7XHJcbiAgICBpc1JlYWRPbmx5O1xyXG4gICAgY2FuY2VsQ29tbWVudDtcclxuICAgIGlzQ2FtcGFpZ25Qcm9qZWN0O1xyXG4gICAgc2VsZWN0ZWRUZWFtO1xyXG4gICAgZW5hYmxlQ3VzdG9tV29ya2Zsb3c7XHJcblxyXG4gICAgZW5hYmxlUmVzdGFydFByb2plY3QgPSBmYWxzZTtcclxuICAgIGVuYWJsZVByb2plY3RSZXN0YXJ0Q29uZmlnO1xyXG5cclxuICAgIGtleVJlc3RyaWN0aW9uID0ge1xyXG4gICAgICAgIE5VTUJFUjogWzY5LCAxODcsIDE4OCwgMTA3XVxyXG4gICAgfTtcclxuXHJcbiAgICBzdGFydERhdGVFcnJvck1lc3NhZ2U7XHJcbiAgICBlbmREYXRlRXJyb3JNZXNzYWdlO1xyXG5cclxuICAgIG9sZFN0YXR1c0luZm87XHJcbiAgICByZWFzb25zO1xyXG4gICAgaXNEZWZhdWx0UHJpb3J0aXlTZXQ6IGJvb2xlYW49IGZhbHNlO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoQ3VzdG9tU2VhcmNoRmllbGRDb21wb25lbnQpIGN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50OiBDdXN0b21TZWFyY2hGaWVsZENvbXBvbmVudDtcclxuICAgIHRlbXBsYXRlQXV0b2NvbXBsZXRlRmllbGQ6IHsgZm9ybUNvbnRyb2w6IGFueTsgbGFiZWw6IHN0cmluZzsgZmlsdGVyT3B0aW9uczogYW55OyB9O1xyXG4gICAgY2F0ZWdvcnlNZXRhZGF0YU9wdGlvbnM6IGFueTtcclxuXHJcbiAgICBlbmFibGVXb3JrV2VlaztcclxuXHJcbiAgICBjYW1wYWlnbkRldGFpbDtcclxuICAgIGZpbHRlck9wdGlvbnM7XHJcbiAgICBjYW1wYWlnbklkcztcclxuXHJcbiAgICBpc0NhbXBhaWduTWFuYWdlcjtcclxuICAgIGhhc1RlYW1zO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBmYjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHVibGljIGFkYXB0ZXI6IERhdGVBZGFwdGVyPGFueT4sXHJcbiAgICAgICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBjYXRlZ29yeVNlcnZpY2U6IENhdGVnb3J5U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc3RhdHVzU2VydmljZTogU3RhdHVzU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFV0aWxTZXJ2aWNlOiBQcm9qZWN0VXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHByb2plY3RTZXJ2aWNlOiBQcm9qZWN0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2U6IFByb2plY3RGcm9tVGVtcGxhdGVTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNvbW1lbnRzU2VydmljZTogQ29tbWVudHNTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkYXRlUGlwZTogRGF0ZVBpcGVcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMub2xkUHJvamVjdE1ldGFkYXRhID0gbnVsbDtcclxuICAgIH1cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY2FuRGVhY3RpdmF0ZSgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZXN0cmljdEtleXNPblR5cGUoZXZlbnQsIGRhdGF0eXBlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMua2V5UmVzdHJpY3Rpb25bZGF0YXR5cGVdICYmIHRoaXMua2V5UmVzdHJpY3Rpb25bZGF0YXR5cGVdLmluY2x1ZGVzKGV2ZW50LmtleUNvZGUpKSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hWaWV3KCkge1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZVByb2plY3RTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5vbGRQcm9qZWN0TWV0YWRhdGEgPSBudWxsO1xyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLmluaXRpYWxpc2VPdmVydmlld0NvbmZpZygpO1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc0NhbXBhaWduID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1Byb2plY3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuZm9ybVR5cGUgPSBQcm9qZWN0VHlwZXMuQ0FNUEFJR047XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1Byb2plY3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5mb3JtVHlwZSA9IFByb2plY3RUeXBlcy5URU1QTEFURTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUHJvamVjdCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuaXNUZW1wbGF0ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmZvcm1UeXBlID0gUHJvamVjdFR5cGVzLlBST0pFQ1Q7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmNhbXBhaWduSWQpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRDYW1wYWlnbkRldGFpbCh0aGlzLmNhbXBhaWduSWQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0RGV0YWlsKHRoaXMucHJvamVjdElkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLnNldFByb2plY3RDdXN0b21NZXRhZGF0YShudWxsKTtcclxuICAgICAgICAgICAgdGhpcy5nZXRGaWx0ZXJzKCk7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldENhbXBhaWduT3duZXJEZXRhaWxzSWQodXNlcklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFBlcnNvbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICB1c2VySWQ6IHVzZXJJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocGVyc29uRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzID0gcGVyc29uRGV0YWlscztcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RPd25lckRldGFpbHModXNlckNOOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdXNlckNOKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICB1c2VyQ046IHVzZXJDTlxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHBlcnNvbkRldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3duZXJEZXRhaWxzID0gcGVyc29uRGV0YWlscztcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWx0ZXJzKCkge1xyXG4gICAgICAgIGxldCBpc1VwZGF0ZSwgY2F0ZWdvcnlMZXZlbDtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuY2FtcGFpZ25JZCB8fCB0aGlzLnByb2plY3RJZCkge1xyXG4gICAgICAgICAgICBpc1VwZGF0ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWwgPSB0aGlzLmNhbXBhaWduSWQgPyBQcm9qZWN0Q29uc3RhbnQuQ0FURUdPUllfTEVWRUxfQ0FNUEFJR04gOiBQcm9qZWN0Q29uc3RhbnQuQ0FURUdPUllfTEVWRUxfUFJPSkVDVDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpc1VwZGF0ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc0NhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICBjYXRlZ29yeUxldmVsID0gUHJvamVjdENvbnN0YW50LkNBVEVHT1JZX0xFVkVMX0NBTVBBSUdOO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbCA9IFByb2plY3RDb25zdGFudC5DQVRFR09SWV9MRVZFTF9QUk9KRUNUO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuY2FtcGFpZ25JZHMgPSB0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCA/IHRoaXMuc2VsZWN0ZWRDYW1wYWlnbklkIDogKHRoaXMub3ZlclZpZXdDb25maWcgJiZcclxuICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQU1QQUlHTiAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX0NBTVBBSUdOWydDYW1wYWlnbi1pZCddICYmXHJcbiAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fQ0FNUEFJR05bJ0NhbXBhaWduLWlkJ10uSWQpID8gdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQU1QQUlHTlsnQ2FtcGFpZ24taWQnXS5JZCA6ICcnXHJcblxyXG4gICAgICAgIGZvcmtKb2luKFxyXG4gICAgICAgICAgICAvKiAgWyghaXNVcGRhdGUgfHwgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID8gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbEFuZFN0YXR1c1R5cGUoXHJcbiAgICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbCwgU3RhdHVzVHlwZXMuSU5JVElBTCkgOiB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsTmFtZShcclxuICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbCkpLFxyXG4gICAgICAgICAgICAgdGhpcy5nZXRQcmlvcml0aWVzKGNhdGVnb3J5TGV2ZWwpLFxyXG4gICAgICAgICAgICAgdGhpcy5nZXRUZWFtcygpLCB0aGlzLmdldENhbXBhaWduKCksXHJcbiAgICAgICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldEFsbENhdGVnb3J5TWV0YWRhdGEoKV0gKi9cclxuXHJcbiAgICAgICAgICAgIC8vMjAxXHJcbiAgICAgICAgICAgIC8qICghaXNVcGRhdGUgfHwgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID8gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbEFuZFN0YXR1c1R5cGUoXHJcbiAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbCwgU3RhdHVzVHlwZXMuSU5JVElBTCkgOiB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0QWxsU3RhdHVzQnljYXRlZ29yeU5hbWUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWwpKSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJpb3JpdGllcyhjYXRlZ29yeUxldmVsKSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGVhbXMoKSwgdGhpcy5nZXRDYW1wYWlnbigpLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRBbGxDYXRlZ29yeU1ldGFkYXRhKCldICovXHJcbiAgICAgICAgICAgICghdGhpcy5pc0NhbXBhaWduICYmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCB8fCB0aGlzLmNhbXBhaWduSWRzKSkgP1xyXG4gICAgICAgICAgICAgICAgWyghaXNVcGRhdGUgfHwgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID8gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbEFuZFN0YXR1c1R5cGUoXHJcbiAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbCwgU3RhdHVzVHlwZXMuSU5JVElBTCkgOiB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0QWxsU3RhdHVzQnljYXRlZ29yeU5hbWUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWwpKSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJpb3JpdGllcyhjYXRlZ29yeUxldmVsKSxcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGVhbXMoKSwgLyogdGhpcy5nZXRDYW1wYWlnbkJ5SWQodGhpcy5zZWxlY3RlZENhbXBhaWduSWQpLCAqL3RoaXMuZ2V0Q2FtcGFpZ25CeUlkKHRoaXMuY2FtcGFpZ25JZHMpLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRBbGxDYXRlZ29yeU1ldGFkYXRhKCldIDogIXRoaXMuaXNDYW1wYWlnbiA/IFsoIWlzVXBkYXRlIHx8IHRoaXMub3ZlclZpZXdDb25maWcuaXNUZW1wbGF0ZSA/IHRoaXMuc3RhdHVzU2VydmljZS5nZXRTdGF0dXNCeUNhdGVnb3J5TGV2ZWxBbmRTdGF0dXNUeXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWwsIFN0YXR1c1R5cGVzLklOSVRJQUwpIDogdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0J5Y2F0ZWdvcnlOYW1lKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeUxldmVsKSksXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFByaW9yaXRpZXMoY2F0ZWdvcnlMZXZlbCksXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFRlYW1zKCksIC8qIHRoaXMuZ2V0Q2FtcGFpZ24oKSwgKi9cclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2UuZ2V0QWxsQ2F0ZWdvcnlNZXRhZGF0YSgpXSA6IFsoIWlzVXBkYXRlIHx8IHRoaXMub3ZlclZpZXdDb25maWcuaXNUZW1wbGF0ZSA/IHRoaXMuc3RhdHVzU2VydmljZS5nZXRTdGF0dXNCeUNhdGVnb3J5TGV2ZWxBbmRTdGF0dXNUeXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWwsIFN0YXR1c1R5cGVzLklOSVRJQUwpIDogdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0J5Y2F0ZWdvcnlOYW1lKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeUxldmVsKSksXHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFByaW9yaXRpZXMoY2F0ZWdvcnlMZXZlbCksXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RTZXJ2aWNlLmdldEFsbENhdGVnb3J5TWV0YWRhdGEoKV1cclxuICAgICAgICApLnN1YnNjcmliZShcclxuICAgICAgICAgICAgZm9ya2VkRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvKiB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhdGVnb3J5TWV0YWRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyA9IGZvcmtlZERhdGFbNF0gPyBmb3JrZWREYXRhWzRdLm1hcChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4geyBkaXNwbGF5TmFtZTogZGF0YS5NRVRBREFUQV9OQU1FLCB2YWx1ZTogZGF0YVsnTVBNX0NhdGVnb3J5X01ldGFkYXRhLWlkJ10uSWQsIG5hbWU6IGRhdGEuTUVUQURBVEFfTkFNRSwgLi4uZGF0YSB9O1xyXG4gICAgICAgICAgICAgICAgfSkgOiBbXTsgKi9cclxuICAgICAgICAgICAgICAgIC8vIGFib3ZlIGZvcmtqb2luIGFyZSBjaGFuZ2VkXHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNDYW1wYWlnbiAmJiAodGhpcy5zZWxlY3RlZENhbXBhaWduSWQgfHwgdGhpcy5jYW1wYWlnbklkcykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhdGVnb3J5TWV0YWRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyA9IGZvcmtlZERhdGFbNF0gPyBmb3JrZWREYXRhWzRdLm1hcChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHsgZGlzcGxheU5hbWU6IGRhdGEuTUVUQURBVEFfTkFNRSwgdmFsdWU6IGRhdGFbJ01QTV9DYXRlZ29yeV9NZXRhZGF0YS1pZCddLklkLCBuYW1lOiBkYXRhLk1FVEFEQVRBX05BTUUsIC4uLmRhdGEgfTtcclxuICAgICAgICAgICAgICAgICAgICB9KSA6IFtdO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghdGhpcy5pc0NhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYXRlZ29yeU1ldGFkYXRhQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBmb3JrZWREYXRhWzNdID8gZm9ya2VkRGF0YVszXS5tYXAoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7IGRpc3BsYXlOYW1lOiBkYXRhLk1FVEFEQVRBX05BTUUsIHZhbHVlOiBkYXRhWydNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEtaWQnXS5JZCwgbmFtZTogZGF0YS5NRVRBREFUQV9OQU1FLCAuLi5kYXRhIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSkgOiBbXTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYXRlZ29yeU1ldGFkYXRhQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBmb3JrZWREYXRhWzJdID8gZm9ya2VkRGF0YVsyXS5tYXAoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7IGRpc3BsYXlOYW1lOiBkYXRhLk1FVEFEQVRBX05BTUUsIHZhbHVlOiBkYXRhWydNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEtaWQnXS5JZCwgbmFtZTogZGF0YS5NRVRBREFUQV9OQU1FLCAuLi5kYXRhIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSkgOiBbXTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnMgPSAhaXNVcGRhdGUgfHwgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1RlbXBsYXRlID8gZm9ya2VkRGF0YVswXSA6ICh0aGlzLmlzQ2FtcGFpZ24gP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmZpbHRlclN0YXR1c09wdGlvbnModGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQsIGZvcmtlZERhdGFbMF0sIE1QTV9MRVZFTFMuQ0FNUEFJR04pIDpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RVdGlsU2VydmljZS5maWx0ZXJTdGF0dXNPcHRpb25zKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQsIGZvcmtlZERhdGFbMF0sIE1QTV9MRVZFTFMuUFJPSkVDVCkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnNlbGVjdGVkU3RhdHVzICYmIHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9ucyAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGF0dXMgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnNbMF1bJ01QTV9TdGF0dXMtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zID0gZm9ya2VkRGF0YVsxXTtcclxuICAgICAgICAgICAgICAgIC8vIFRPRE86IHNlbGVjdGluZyBEZWZhdWx0IFByaXJpdHkgLSBjb2x1bW4gdG8gYmUgYWRkZWQgdG8gZW50aXR5XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRQcmlvcml0eSAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucyAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5wcmlvcml0eU9wdGlvbnMuc29ydCgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihwYXJzZUludChhLkRJU1BMQVlfT1JERVIpPCBwYXJzZUludChiLkRJU1BMQVlfT1JERVIpKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAtMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2UgMTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucy5mb3JFYWNoKHAxPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHAxLklTX0RFRkFVTFQudG9TdHJpbmcoKT09J3RydWUnKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNEZWZhdWx0UHJpb3J0aXlTZXQ9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkUHJpb3JpdHk9IHAxWydNUE1fUHJpb3JpdHktaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoIXRoaXMuaXNEZWZhdWx0UHJpb3J0aXlTZXQpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkUHJpb3JpdHk9IHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zWzBdWydNUE1fUHJpb3JpdHktaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnNlbGVjdGVkUHJpb3JpdHkgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9uc1swXVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIC8vMTI4XHJcbiAgICAgICAgICAgICAgICB0aGlzLmhhc1RlYW1zID0gZm9ya2VkRGF0YVsyXSAmJiBmb3JrZWREYXRhWzJdLmxlbmd0aCA+IDAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnRlYW1zT3B0aW9ucyA9IGZvcmtlZERhdGFbMl0gJiYgZm9ya2VkRGF0YVsyXS5sZW5ndGggPiAwID8gZm9ya2VkRGF0YVsyXSA6IHRoaXMuc2hhcmluZ1NlcnZpY2UudGVhbXM7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhdGVnb3J5T3B0aW9ucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQ2F0ZWdvcmllcygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5vd25lckZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogJydcclxuICAgICAgICAgICAgICAgIH1dO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnblxyXG4gICAgICAgICAgICAgICAgICAgICYmIHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBTVBBSUdOX09XTkVSXHJcbiAgICAgICAgICAgICAgICAgICAgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduLlJfUE9fQ0FNUEFJR05fT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBTVBBSUdOX09XTkVSWydJZGVudGl0eS1pZCddLklkXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocGVyc29uRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE93bmVyRGV0YWlscyA9IHBlcnNvbkRldGFpbHM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucG9wdWxhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3RcclxuICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJcclxuICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fUFJPSkVDVF9PV05FUlsnSWRlbnRpdHktaWQnXS5JZFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHBlcnNvbkRldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPd25lckRldGFpbHMgPSBwZXJzb25EZXRhaWxzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBvcHVsYXRlRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnBvcHVsYXRlRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUNhbXBhaWduVmFsdWUoY2FtcGFpZ24pIHtcclxuICAgICAgICBpZiAoY2FtcGFpZ24uQ0FNUEFJR05fTkFNRSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQgKyAnLScgKyBjYW1wYWlnbi5DQU1QQUlHTl9OQU1FO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKiBmb3JtQ2FtcGFpZ24oY2FtcGFpZ25JZCkge1xyXG4gICAgICAgIGlmIChjYW1wYWlnbklkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQ2FtcGFpZ24gPSB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5maWx0ZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50LnZhbHVlID09PSBjYW1wYWlnbklkKTtcclxuICAgICAgICAgICAgcmV0dXJuIHNlbGVjdGVkQ2FtcGFpZ24gPyBzZWxlY3RlZENhbXBhaWduIDogJyc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH0gKi9cclxuXHJcbiAgICBmb3JtQ2FtcGFpZ24oY2FtcGFpZ25JZCkge1xyXG4gICAgICAgIGlmIChjYW1wYWlnbklkKSB7XHJcbiAgICAgICAgICAgIGxldCBzZWxlY3RlZENhbXBhaWduO1xyXG4gICAgICAgICAgICBzZWxlY3RlZENhbXBhaWduID0gdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbkRhdGFDb25maWcuZmlsdGVyT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC52YWx1ZSA9PT0gY2FtcGFpZ25JZCk7XHJcbiAgICAgICAgICAgIHJldHVybiBzZWxlY3RlZENhbXBhaWduID8gc2VsZWN0ZWRDYW1wYWlnbiA6ICcnO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlcnNGb3JUZWFtUm9sZSgpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCByb2xlRGV0YWlsT2JqOiBSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Um9sZUJ5TmFtZShNUE1fUk9MRVMuTUFOQUdFUik7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLnNlbGVjdGVkVGVhbVsnTVBNX1RlYW1zLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICByb2xlRE46IHJvbGVEZXRhaWxPYmouUk9MRV9ETlxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW1Sb2xlKHBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSB1c2Vycy5maW5kKGUgPT4gZS52YWx1ZSA9PT0gdXNlci5jbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHVzZXIubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHVzZXIubmFtZSArICcgKCcgKyB1c2VyLmNuICsgJyknXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcub3duZXJGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zID0gdXNlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5vd25lckZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnTWFuYWdlciByb2xlIGluIHRoZSBzZWxlY3RlZCB0ZWFtIGlzIG5vdCBhdmFpbGFibGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFVzZXJzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoW10pO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUZWFtcygpOiBPYnNlcnZhYmxlPEFycmF5PFRlYW0+PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuaXNQcm9qZWN0ICYmIHRoaXMuaXNDYW1wYWlnblByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTV9NRVRIT0RfTlMsIHRoaXMuR0VUX0FMTF9URUFNU19XU19NRVRIT0RfTkFNRSwgbnVsbClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVhbXM6IEFycmF5PFRlYW0+ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9UZWFtcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRlYW1zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgVGVhbXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KFtdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvbGVEZXRhaWxPYmo6IFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRSb2xlQnlOYW1lKE1QTV9ST0xFUy5NQU5BR0VSKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogcm9sZURldGFpbE9iai5ST0xFX0ROXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5URUFNX0JQTV9NRVRIT0RfTlMsIHRoaXMuR0VUX1RFQU1fV1NfTUVUSE9EX05BTUUsIHBhcmFtKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZWFtczogQXJyYXk8VGVhbT4gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1RlYW1zJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGVhbXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUZWFtcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoW10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogZ2V0UHJpb3JpdGllcyhjYXRlZ29yeUxldmVsTmFtZSk6IE9ic2VydmFibGU8QXJyYXk8UHJpb3JpdHk+PiB7XHJcbiAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbE5hbWU6IGNhdGVnb3J5TGV2ZWxOYW1lXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUklPUklUWV9NRVRIT0RfTlMsIHRoaXMuR0VUX1BSSU9SSVRZX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJpb3JpdGllczogQXJyYXk8UHJpb3JpdHk+ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9Qcmlvcml0eScpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocHJpb3JpdGllcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChbXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldFByaW9yaXRpZXMoY2F0ZWdvcnlMZXZlbE5hbWUpOiBBcnJheTxQcmlvcml0eT4ge1xyXG4gICAgICAgIGNvbnN0IGN1cnJDYXRlZ29yeU9iajogQ2F0ZWdvcnlMZXZlbCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q2F0ZWdvcnlMZXZlbHMoKS5maW5kKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0YS5DQVRFR09SWV9MRVZFTF9UWVBFID09PSBjYXRlZ29yeUxldmVsTmFtZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcblxyXG4gICAgICAgIGlmIChjdXJyQ2F0ZWdvcnlPYmopIHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1BSSU9SSVRJRVMpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYWxsU3RhdHVzZXM6IEFycmF5PFByaW9yaXR5PiA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUFJJT1JJVElFUykpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGFsbFN0YXR1czogQXJyYXk8UHJpb3JpdHk+ID0gYWxsU3RhdHVzZXMuZmlsdGVyKChzdGF0dXM6IFByaW9yaXR5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN0YXR1cy5SX1BPX0NBVEVHT1JZX0xFVkVMWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXS5JZCA9PT0gY3VyckNhdGVnb3J5T2JqWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgbGV0IHN0YXR1cyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgc3RhdHVzLnB1c2goYWxsU3RhdHVzKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBzdGF0dXM7XHJcblxyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBhbGxTdGF0dXM6IEFycmF5PFByaW9yaXR5PiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUHJpb3JpdGllcygpLmZpbHRlcigoc3RhdHVzOiBQcmlvcml0eSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdGF0dXMuUl9QT19DQVRFR09SWV9MRVZFTFsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQgPT09IGN1cnJDYXRlZ29yeU9ialsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGxldCBzdGF0dXMgPSBbXTtcclxuICAgICAgICAgICAgICAgIHN0YXR1cy5wdXNoKGFsbFN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc3RhdHVzO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gW107XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8vMjAxXHJcblxyXG4gICAgZ2V0QWxsQ2FtcGFpZ25CeU5hbWUoY2FtcGFpZ25OYW1lKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgQ2FtcGFpZ25OYW1lOiBjYW1wYWlnbk5hbWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQU1QQUlHTl9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX2NBTVBBSUdOX0JZX05BTUVfV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsQ2FtcGFpZ24oKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLkNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5DYW1wYWlnbikpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLkNhbXBhaWduID0gW3Jlc3BvbnNlLkNhbXBhaWduXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IHJlc3BvbnNlLkNhbXBhaWduO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoY2FtcGFpZ24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSB0aGlzLmNhbXBhaWduTGlzdC5maW5kKGRhdGEgPT4gZGF0YVsnQ2FtcGFpZ24taWQnXS5JZCA9PT0gY2FtcGFpZ24uc3BsaXQoLy0oLispLylbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1pblN0YXJ0RGF0ZSA9IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFLCAneXl5eS1NTS1kZCcpID49IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykgPyB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSA6IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldENhbXBhaWduT3duZXJEZXRhaWxzSWQodGhpcy5zZWxlY3RlZENhbXBhaWduLlJfUE9fQ0FNUEFJR05fT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVzZXJJZDogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAxMjhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbkRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKChuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSA8IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fRU5EX0RBVEUpKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSksICd5eXl5LU1NLWRkJykgPj0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUoKSwgJ3l5eXktTU0tZGQnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGFydERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9DQU1QQUlHTl9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0RGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0RGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5lbmREYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ldyBEYXRlKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX0NBTVBBSUdOX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5DQU1QQUlHTl9TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25GaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0Lm1hcChjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZm9ybUNhbXBhaWduVmFsdWUoY2FtcGFpZ24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhbXBhaWduLkNBTVBBSUdOX05BTUUgJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10gJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSwgJ3l5eXktTU0tZGQnKSA+PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FtcGFpZ25GaWx0ZXJMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBjYW1wYWlnbi5DQU1QQUlHTl9OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB2YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3duZXJJZDogY2FtcGFpZ24uUl9QT19DQU1QQUlHTl9PV05FUlsnSWRlbnRpdHktaWQnXS5JZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbkRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyA9IGNhbXBhaWduRmlsdGVyTGlzdDtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5maWx0ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5maWx0ZXJPcHRpb25zKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICAvKiB0aGlzLmdldENhbXBhaWduT3duZXJEZXRhaWxzSWQodGhpcy5zZWxlY3RlZENhbXBhaWduLlJfUE9fQ0FNUEFJR05fT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVzZXJJZDogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgZ2V0Q2FtcGFpZ25zKGNhbXBhaWduTmFtZSkge1xyXG4gICAgICAgIGlmIChjYW1wYWlnbk5hbWUudGFyZ2V0LnZhbHVlLmxlbmd0aCA+IDIpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRBbGxDYW1wYWlnbkJ5TmFtZShjYW1wYWlnbk5hbWUudGFyZ2V0LnZhbHVlKS5zdWJzY3JpYmUoY2FtcGFpZ25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpbHRlck9wdGlvbnMgPSBjYW1wYWlnblJlc3BvbnNlO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudCkge1xyXG4gICAgICAgIHRoaXMuY2FtcGFpZ25JZCA9IGV2ZW50Lm9wdGlvbi52YWx1ZS5zcGxpdCgvLSguKykvKVswXTtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZENhbXBhaWduID0gdGhpcy5maWx0ZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IGV2ZW50Lm9wdGlvbi52YWx1ZS5zcGxpdCgvLSguKykvKVsxXSk7XHJcbiAgICAgICAgdGhpcy5nZXRDYW1wYWlnbk93bmVyRGV0YWlsc0lkKHNlbGVjdGVkQ2FtcGFpZ24ub3duZXJJZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUodGhpcy5mb3JtVXNlckRldGFpbHMoe1xyXG4gICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgIH0sIHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMudXNlckNOKSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduT3duZXJGaWVsZENvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbk93bmVyO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgZ2V0Q2FtcGFpZ24oKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNBTVBBSUdOX0RFVEFJTFNfTUVUSE9EX05TLCB0aGlzLkdFVF9BTExfQ0FNUEFJR05fV1NfTUVUSE9EX05BTUUsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsQ2FtcGFpZ24oKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLkNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5DYW1wYWlnbikpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLkNhbXBhaWduID0gW3Jlc3BvbnNlLkNhbXBhaWduXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IHJlc3BvbnNlLkNhbXBhaWduO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjYW1wYWlnbkZpbHRlckxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkxpc3QubWFwKGNhbXBhaWduID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5mb3JtQ2FtcGFpZ25WYWx1ZShjYW1wYWlnbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2FtcGFpZ24uQ0FNUEFJR05fTkFNRSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXSAmJiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFLCAneXl5eS1NTS1kZCcpID49IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYW1wYWlnbkZpbHRlckxpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGNhbXBhaWduLkNBTVBBSUdOX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBjYW1wYWlnblsnQ2FtcGFpZ24taWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5maWx0ZXJPcHRpb25zID0gY2FtcGFpZ25GaWx0ZXJMaXN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuY2FtcGFpZ25EYXRhQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRMb2NhbFZhbHVlQnlJZChtZXRhZGF0YUZpZWxkSWQpIHtcclxuICAgICAgICBjb25zdCBjdXN0b21NZXRhZGF0YSA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0UHJvamVjdEN1c3RvbU1ldGFkYXRhKCk7XHJcbiAgICAgICAgaWYgKCFjdXN0b21NZXRhZGF0YSkge1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCByZXN1bHRWYWx1ZSA9ICcnO1xyXG4gICAgICAgIGN1c3RvbU1ldGFkYXRhLk1ldGFkYXRhLm1ldGFkYXRhRmllbGRzLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlbGVtZW50LmZpZWxkSWQgPT09IG1ldGFkYXRhRmllbGRJZCkge1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0VmFsdWUgPSBlbGVtZW50LmRlZmF1bHRWYWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiByZXN1bHRWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNZXRhRGF0YShjYXRlZ29yeUxldmVsLCBjYXRlZ29yeU1ldGFkYXRJZCkge1xyXG4gICAgICAgIGxldCBjYXRlZ29yeUxldmVsRGV0YWlscyA9IHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlUeXBlKGNhdGVnb3J5TGV2ZWwpO1xyXG4gICAgICAgIGlmIChjYXRlZ29yeU1ldGFkYXRJZCkge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgPSB0aGlzLmZvcm1DYXRlZ29yeU1ldGFkYXRhKGNhdGVnb3J5TWV0YWRhdElkKTtcclxuICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbERldGFpbHMgPSBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgPyBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgOiBjYXRlZ29yeUxldmVsRGV0YWlscztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5vdG1tU2VydmljZS5nZXRNZXRhZGF0TW9kZWxCeUlkKGNhdGVnb3J5TGV2ZWxEZXRhaWxzLk1FVEFEQVRBX01PREVMX0lEKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKG1ldGFEYXRhTW9kZWxEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkc2V0R3JvdXAgPSBbXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkR3JvdXBzID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmdldCgnQ3VzdG9tRmllbGRHcm91cCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLnJlbW92ZUNvbnRyb2woJ0N1c3RvbUZpZWxkR3JvdXAnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChtZXRhRGF0YU1vZGVsRGV0YWlscyAmJiBtZXRhRGF0YU1vZGVsRGV0YWlscy5tZXRhZGF0YV9lbGVtZW50X2xpc3QgJiZcclxuICAgICAgICAgICAgICAgICAgICBBcnJheS5pc0FycmF5KG1ldGFEYXRhTW9kZWxEZXRhaWxzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IG1ldGFkYXRhRmllbGRHcm91cCBvZiBtZXRhRGF0YU1vZGVsRGV0YWlscy5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZm9ybUNvbnRyb2xBcnJheSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZEdyb3VwLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX1BST0pFQ1RfTUVUQURBVEFfR1JPVVAgfHwgbWV0YWRhdGFGaWVsZEdyb3VwLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX0NBTVBBSUdOX01FVEFEQVRBX0dST1VQKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZEdyb3VwLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQXJyYXkuaXNBcnJheShtZXRhZGF0YUZpZWxkR3JvdXAubWV0YWRhdGFfZWxlbWVudF9saXN0KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBtZXRhZGF0YUZpZWxkIG9mIG1ldGFkYXRhRmllbGRHcm91cC5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbU1ldGFkYXRhRmllbGRzLnB1c2gobWV0YWRhdGFGaWVsZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9ycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gKHRoaXMub3ZlclZpZXdDb25maWcubWV0YURhdGFWYWx1ZXMpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKHRoaXMub3ZlclZpZXdDb25maWcubWV0YURhdGFWYWx1ZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkLmlkKSA6IHRoaXMuZ2V0TG9jYWxWYWx1ZUJ5SWQobWV0YWRhdGFGaWVsZC5pZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZvcm1Db250cm9sT2JqID0gbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcgPyB2YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogIW1ldGFkYXRhRmllbGQuZWRpdGFibGUgfHwgKCh0aGlzLnByb2plY3RJZCAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHkpIHx8ICh0aGlzLmNhbXBhaWduSWQgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1JlYWRPbmx5KSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLnJlcXVpcmVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuREFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goRGF0ZVZhbGlkYXRvcnMuZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLlNJTVBMRSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuVEVYVEFSRUEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMubWF4TGVuZ3RoKG1ldGFkYXRhRmllbGQuZGF0YV9sZW5ndGgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZGF0YV90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5OVU1CRVIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbnVtZXJpY2VWbGFsdWUgPSB0aGlzLmdldE1heE1pblZhbHVlRm9yQ3VzdG9tTWV0YWRhdGEobWV0YWRhdGFGaWVsZC5kYXRhX2xlbmd0aCwgbWV0YWRhdGFGaWVsZC5zY2FsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICghbWV0YWRhdGFGaWVsZC5zY2FsZSkgPyB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5wYXR0ZXJuKG5ldyBSZWdFeHAoJygtKFxcZHswLDV9KSl8KFxcZHswLDV9KSQnKSkpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICA6IHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLnBhdHRlcm4obmV3IFJlZ0V4cCgnLSgoXFxkK1xcLik/XFxkezAsJyArIG1ldGFkYXRhRmllbGQuc2NhbGUgKyAnfSQpIHwgKChcXGQrXFwuKT9cXGR7MCwnICsgbWV0YWRhdGFGaWVsZC5zY2FsZSArICd9KSQnKSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE9iai5zZXRWYWxpZGF0b3JzKHZhbGlkYXRvcnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1Db250cm9sT2JqLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xPYmpbJ2ZpZWxkc2V0J10gPSBtZXRhZGF0YUZpZWxkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1Db250cm9sT2JqWyduYW1lJ10gPSBtZXRhZGF0YUZpZWxkLm5hbWU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuQ09NQk8pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZC52YWx1ZXMgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldExvb2t1cERvbWFpblZhbHVlc0J5SWQobWV0YWRhdGFGaWVsZC5kb21haW5faWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRzZXRHcm91cC5wdXNoKGZvcm1Db250cm9sT2JqKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbmV3IGNvZGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbEFycmF5LnB1c2goZm9ybUNvbnRyb2xPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkR3JvdXAgPSB0aGlzLmZiLmdyb3VwKHsgZmllbGRzZXQ6IG5ldyBGb3JtQXJyYXkoZm9ybUNvbnRyb2xBcnJheSkgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkR3JvdXBbJ25hbWUnXSA9IG1ldGFkYXRhRmllbGRHcm91cC5uYW1lO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRHcm91cHMucHVzaChmaWVsZEdyb3VwKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXN0b21NZXRhZGF0YUZpZWxkcyAmJiBBcnJheS5pc0FycmF5KHRoaXMuY3VzdG9tTWV0YWRhdGFGaWVsZHMpICYmIHRoaXMuY3VzdG9tTWV0YWRhdGFGaWVsZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIE5lZWQgdG8gYmUgdGVzdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRzZXRHcm91cC5mb3JFYWNoKGZpZWxkU2V0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkU2V0LmZpZWxkc2V0LmRhdGFfdHlwZSA9PT0gRGF0YVR5cGVDb25zdGFudHMuREFURSAmJiBmaWVsZFNldC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRTZXQudmFsdWUgPSBuZXcgRGF0ZShmaWVsZFNldC52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbUZpZWxkc0dyb3VwID0gdGhpcy5mYi5ncm91cCh7IGZpZWxkc2V0OiBuZXcgRm9ybUFycmF5KGZpZWxkc2V0R3JvdXApIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5hZGRDb250cm9sKCdDdXN0b21GaWVsZEdyb3VwJywgbmV3IEZvcm1BcnJheShmaWVsZEdyb3VwcywgeyB1cGRhdGVPbjogJ2JsdXInIH0pKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9sZFByb2plY3RNZXRhZGF0YSA9IHRoaXMucHJvamVjdElkID8gdGhpcy5jcmVhdGVOZXdQcm9qZWN0VXBkYXRlKCkgOiBudWxsO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGdldE1heE1pblZhbHVlRm9yTnVtYmVyKHNjYWxlKTogYW55IHtcclxuICAgICAgICBsZXQgbnVtZXJpY2VWbGFsdWUgPSAnOTk5OTknO1xyXG4gICAgICAgIGlmIChzY2FsZSkge1xyXG4gICAgICAgICAgICBudW1lcmljZVZsYWx1ZSArPSAnLicgKyBuZXdBcnJheShzY2FsZSkuZmlsbCgnOScpLmpvaW4oJycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHJhZGl4XHJcbiAgICAgICAgcmV0dXJuIHNjYWxlID8gcGFyc2VGbG9hdChudW1lcmljZVZsYWx1ZSkgOiBwYXJzZUludChudW1lcmljZVZsYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWF4TWluVmFsdWVGb3JDdXN0b21NZXRhZGF0YShkYXRhTGVuZ3RoLCBzY2FsZSk6IGFueSB7XHJcbiAgICAgICAgbGV0IGRhdGEgPSBNYXRoLmFicyhkYXRhTGVuZ3RoIC0gc2NhbGUpO1xyXG4gICAgICAgIGxldCBudW1lcmljVmFsdWUgPSBuZXdBcnJheShkYXRhKS5maWxsKCc5Jykuam9pbignJyk7XHJcbiAgICAgICAgaWYgKHNjYWxlKSB7XHJcbiAgICAgICAgICAgIG51bWVyaWNWYWx1ZSArPSAnLicgKyBuZXdBcnJheShzY2FsZSkuZmlsbCgnOScpLmpvaW4oJycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHJhZGl4XHJcbiAgICAgICAgcmV0dXJuIHNjYWxlID8gcGFyc2VGbG9hdChudW1lcmljVmFsdWUpIDogcGFyc2VJbnQobnVtZXJpY1ZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVRhc2tTdGF0dXNVcGRhdGUoc2VsZWN0ZWRTdGF0dXMpIHtcclxuICAgICAgICAvKiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRTdGF0dXNCeUlkKHNlbGVjdGVkU3RhdHVzLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAuc3Vic2NyaWJlKHByb2plY3RTdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgaWYgKHByb2plY3RTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gY2FuY2VsIHRoZSBwcm9qZWN0PycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXNDb25maXJtYXRpb25Db21tZW50OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudFRleHQ6ICdDb21tZW50cycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0NvbW1lbnRSZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yTWVzc2FnZTogJ0tpbmRseSBmaWxsIHRoZSBjb21tZW50cyBmaWVsZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQuY29uZmlybWF0aW9uQ29tbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbmNlbENvbW1lbnQgPSByZXN1bHQuY29uZmlybWF0aW9uQ29tbWVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdQcm9qZWN0IGNhbmNlbGxhdGlvbiBwcm9jZXNzIGlzIGluIHByb2dyZXNzIGFuZCBpdCBtaWdodCB0YWtlIHNvbWUgdGltZSB0byBjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgIH0sIHByb2plY3RTdGF0dXNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcm9qZWN0U3RhdHVzRXJyb3IpO1xyXG4gICAgICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICAvLyBUT0RPOiBBZGQgdmFsaWRhdGlvbnMgaW4gRWRpdCB3aGVuIGFueSBzdGF0dXMgb2YgVHlwZSBGaW5hbCBpcyBzZWxlY3RlZCwgdmFsaWRhdGlvbnMgZm9yIHRhc2sgYW5kIGRlbGl2ZXJhYmxlIHN0YXR1cy5cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVByb2plY3RTdGFydERhdGUoc2VsZWN0ZWREYXRlT2JqKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFRPRE86IEFkZCB2YWxpZGF0aW9ucyBmb3IgRWRpdCBQcm9qZWN0XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGVQcm9qZWN0RW5kRGF0ZShzZWxlY3RlZERhdGVPYmopIHtcclxuICAgICAgICBpZiAoIXRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gVE9ETzogQWRkIHZhbGlkYXRpb25zIGZvciBFZGl0IFByb2plY3RcclxuICAgIH1cclxuXHJcbiAgICAvKiBmb3JtU3RhdHVzKHN0YXR1c0lkLCBzdGF0dXNOYW1lKSB7XHJcbiAgICAgICAgY29uc3Qgc3RhdHVzID0ge1xyXG4gICAgICAgICAgICAnTVBNX1N0YXR1cy1pZCc6IHtcclxuICAgICAgICAgICAgICAgIElkOiBzdGF0dXNJZFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBOQU1FOiBzdGF0dXNOYW1lXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gc3RhdHVzO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1Qcmlvcml0eShwcmlvcml0eUlkLCBwcmlvcml0eU5hbWUpIHtcclxuICAgICAgICBjb25zdCBwcmlvcml0eSA9IHtcclxuICAgICAgICAgICAgJ01QTV9Qcmlvcml0eS1pZCc6IHtcclxuICAgICAgICAgICAgICAgIElkOiBwcmlvcml0eUlkXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIERFU0NSSVBUSU9OOiBwcmlvcml0eU5hbWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBwcmlvcml0eTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBwb3B1bGF0ZUZvcm0oKSB7XHJcbiAgICAgICAgbGV0IGRpc2FibGVBZG1pbkZpZWxkcyA9IHRydWU7XHJcbiAgICAgICAgbGV0IGRpc2FibGVGaWVsZHMgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHBhdHRlcm4gPSBQcm9qZWN0Q29uc3RhbnQuTkFNRV9TVFJJTkdfUEFUVEVSTjtcclxuICAgICAgICBjb25zdCBudW1lcmljZVZsYWx1ZSA9IHRoaXMuZ2V0TWF4TWluVmFsdWVGb3JOdW1iZXIoMCk7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybSAmJiB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY3VzdG9tRmllbGRzR3JvdXAgJiYgdGhpcy5jdXN0b21GaWVsZHNHcm91cC5jb250cm9scykge1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbUZpZWxkc0dyb3VwLmNvbnRyb2xzID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtID0gbnVsbDtcclxuICAgICAgICB0aGlzLmN1c3RvbUZpZWxkc0dyb3VwID0gbnVsbDtcclxuICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHkgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuaXNSZWFkT25seSA9IHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlRWRpdE9wdGlvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5jdXJyZW50VXNlckl0ZW1JZCA9PT0gdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNSZWFkT25seSA9IHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5oYXNQcm9qZWN0QWRtaW5Sb2xlKSB7XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQWRtaW5GaWVsZHMgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZUVkaXRPcHRpb25zID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuY3VycmVudFVzZXJJdGVtSWQgPT09IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBTVBBSUdOX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHkgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNSZWFkT25seSA9IHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5oYXNQcm9qZWN0QWRtaW5Sb2xlKSB7XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQWRtaW5GaWVsZHMgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gLyogZWxzZSBpZiAoIXRoaXMuaGFzVGVhbXMpIHtcclxuXHJcbiAgICAgICAgfSAqLy8vMTI4XHJcbiAgICAgICAgZGlzYWJsZUZpZWxkcyA9IHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICB0aGlzLmRpc2FibGVPcHRpb25zID0gZGlzYWJsZUZpZWxkcztcclxuICAgICAgICAvKiBpZiAoIXRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZU9wdGlvbnMgPSBmYWxzZTtcclxuICAgICAgICB9ICAqL1xyXG4gICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJiAhdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlT3B0aW9ucyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduICYmICF0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlT3B0aW9ucyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmRpc2FibGVTdGF0dXNPcHRpb25zID0gdGhpcy5kaXNhYmxlT3B0aW9ucztcclxuICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5pc0NhbXBhaWduKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduID0gdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFN0YXR1cyA9IChjYW1wYWlnbi5SX1BPX1NUQVRVUyAmJiBjYW1wYWlnbi5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddKSA/IGNhbXBhaWduLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkU3RhdHVzSW5mbyA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmdldFN0YXR1c0J5U3RhdHVzSWQodGhpcy5zZWxlY3RlZFN0YXR1cywgdGhpcy5vdmVyVmlld0NvbmZpZy5zdGF0dXNPcHRpb25zKTtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFN0YXR1c0luZm8gJiYgKHNlbGVjdGVkU3RhdHVzSW5mb1snU1RBVFVTX0xFVkVMJ10gPT09IFN0YXR1c0xldmVscy5GSU5BTCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNSZWFkT25seSA9IHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlRmllbGRzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVTdGF0dXNPcHRpb25zID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRQcmlvcml0eSA9IChjYW1wYWlnbi5SX1BPX1BSSU9SSVRZICYmIGNhbXBhaWduLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddKSA/IGNhbXBhaWduLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVTdGFydERhdGUgPSBkaXNhYmxlRmllbGRzIHx8IChzZWxlY3RlZFN0YXR1c0luZm8gJiYgc2VsZWN0ZWRTdGF0dXNJbmZvWydTVEFUVVNfTEVWRUwnXSA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVFbmREYXRlID0gZGlzYWJsZUZpZWxkcztcclxuICAgICAgICAgICAgICAgIGNvbnN0IG51bWVyaWNlVmxhbHVlID0gdGhpcy5nZXRNYXhNaW5WYWx1ZUZvck51bWJlcigwKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBjYW1wYWlnbi5DQU1QQUlHTl9OQU1FLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkcyB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBVc2VySWQ6IHRoaXMucHJvamVjdE93bmVyRGV0YWlscy51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBGdWxsTmFtZTogdGhpcy5wcm9qZWN0T3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHRoaXMucHJvamVjdE93bmVyRGV0YWlscy51c2VyQ04pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUFkbWluRmllbGRzXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBuZXcgRGF0ZShjYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFKSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGRzIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHNlbGVjdGVkU3RhdHVzSW5mb1snU1RBVFVTX0xFVkVMJ10gPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEUpKVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IG5ldyBEYXRlKGNhbXBhaWduLkNBTVBBSUdOX0VORF9EQVRFKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShjYW1wYWlnbi5ERVNDUklQVElPTikgPyAnJyA6IGNhbXBhaWduLkRFU0NSSVBUSU9OLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkcyB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IGNhbXBhaWduLlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddLklkLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlRWRpdE9wdGlvbnMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuc2VsZWN0ZWRQcmlvcml0eSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNlbGVjdGVkU3RhdHVzLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlU3RhdHVzT3B0aW9ucyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgICBpbXBvbWVudCByZXF1ZXN0b3IgbG9naWMgZm9yIGlkZWF0aW9uIGludGVncmF0aW9uXHJcbiAgICAgICAgICAgICAgICAgICByZXF1ZXN0ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IChwcm9qZWN0LlJfUE9fUkVRVUVTVEVSX1VTRVIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdC5SX1BPX1JFUVVFU1RFUl9VU0VSLlVzZXJJZCA/IHByb2plY3QuUl9QT19SRVFVRVNURVJfVVNFUi5Vc2VySWQgOiAnJykgfHwgJ05BJyxcclxuICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgICAgIHN0YW5kYXJkU3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJ3RydWUnIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGFydERhdGUgPSBuZXcgRGF0ZShjYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRFbmREYXRlID0gbmV3IERhdGUoY2FtcGFpZ24uQ0FNUEFJR05fRU5EX0RBVEUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0T1RNTUFzc2V0KE1QTV9MRVZFTFMuQ0FNUEFJR04pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtID0gdGhpcy5mYi5ncm91cCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogbmV3IEZvcm1Db250cm9sKCdOZXcgQ2FtcGFpZ24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Vc2VyRGV0YWlscyh0aGlzLm92ZXJWaWV3Q29uZmlnLmN1cnJlbnRVc2VySW5mbywgdGhpcy5vdmVyVmlld0NvbmZpZy5jdXJyZW50VXNlcklkKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVBZG1pbkZpZWxkc1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5vdmVyVmlld0NvbmZpZy5zdGF0dXNPcHRpb25zICYmIHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9uc1swXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnNbMF1bJ01QTV9TdGF0dXMtaWQnXSA/IHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9uc1swXVsnTVBNX1N0YXR1cy1pZCddLklkIDogJycpLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaW9yaXR5OiBuZXcgRm9ybUNvbnRyb2wodGhpcy5vdmVyVmlld0NvbmZpZy5wcmlvcml0eU9wdGlvbnMgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5wcmlvcml0eU9wdGlvbnNbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5wcmlvcml0eU9wdGlvbnNbMF1bJ01QTV9Qcmlvcml0eS1pZCddID8gdGhpcy5vdmVyVmlld0NvbmZpZy5wcmlvcml0eU9wdGlvbnNbMF1bJ01QTV9Qcmlvcml0eS1pZCddLklkIDogJycpLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZTogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGVuZERhdGU6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogbmV3IEZvcm1Db250cm9sKCcnKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE1ldGFEYXRhKE1QTV9MRVZFTFMuQ0FNUEFJR04sIG51bGwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUHJvamVjdCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHByb2plY3QgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdDtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5QUk9KRUNUX1RFTVBMQVRFX1JFRl9JRCAmJiB0eXBlb2YgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlBST0pFQ1RfVEVNUExBVEVfUkVGX0lEKSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnNob3dSZXF1ZXN0ZXJGaWVsZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvKiBpbXBvbGVtZW50IGNhbmNlbCBkaXNhYmxlIGxvZ2ljICovXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkU3RhdHVzID0gKHByb2plY3QuUl9QT19TVEFUVVMgJiYgcHJvamVjdC5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddKSA/IHByb2plY3QuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRTdGF0dXNJbmZvID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuZ2V0U3RhdHVzQnlTdGF0dXNJZCh0aGlzLnNlbGVjdGVkU3RhdHVzLCB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnMpO1xyXG4gICAgICAgICAgICAgICAgLy8gdG8gaGF2ZSB0aGUgb2xkIHN0YXR1cyB0eXBlXHJcbiAgICAgICAgICAgICAgICB0aGlzLm9sZFN0YXR1c0luZm8gPSBzZWxlY3RlZFN0YXR1c0luZm87XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRTdGF0dXNJbmZvICYmIChzZWxlY3RlZFN0YXR1c0luZm9bJ1NUQVRVU19MRVZFTCddID09PSBTdGF0dXNMZXZlbHMuRklOQUwpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5pc1JlYWRPbmx5ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzUmVhZE9ubHkgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUmVhZE9ubHk7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZUZpZWxkcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlU3RhdHVzT3B0aW9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRTdGF0dXNJbmZvLlRZUEUgPT09IFN0YXR1c1R5cGVzLk9OSE9MRCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuaXNSZWFkT25seSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gdGhpcy5vdmVyVmlld0NvbmZpZy5pc1JlYWRPbmx5O1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVGaWVsZHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduSWQgPSAocHJvamVjdC5SX1BPX0NBTVBBSUdOICYmIHByb2plY3QuUl9QT19DQU1QQUlHTlsnQ2FtcGFpZ24taWQnXSkgPyBwcm9qZWN0LlJfUE9fQ0FNUEFJR05bJ0NhbXBhaWduLWlkJ10uSWQgOiB0aGlzLmNhbXBhaWduSWRzID8gdGhpcy5jYW1wYWlnbklkcyA6ICcnO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByaW9yaXR5ID0gKHByb2plY3QuUl9QT19QUklPUklUWSAmJiBwcm9qZWN0LlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddKSA/IHByb2plY3QuUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVN0YXJ0RGF0ZSA9IGRpc2FibGVGaWVsZHMgfHwgKHNlbGVjdGVkU3RhdHVzSW5mbyAmJiBzZWxlY3RlZFN0YXR1c0luZm9bJ1NUQVRVU19MRVZFTCddID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZUVuZERhdGUgPSBkaXNhYmxlRmllbGRzO1xyXG5cclxuICAgICAgICAgICAgICAgIC8qIHRoaXMuZ2V0Q2FtcGFpZ25CeUlkKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4geyAqL1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG51bWVyaWNlVmxhbHVlID0gdGhpcy5nZXRNYXhNaW5WYWx1ZUZvck51bWJlcigwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhdGVnb3J5TWV0YWRhdGF2YWx1ZSA9IChwcm9qZWN0ICYmIHByb2plY3QuUl9QT19DQVRFR09SWV9NRVRBREFUQSkgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybUNhdGVnb3J5TWV0YWRhdGEocHJvamVjdC5SX1BPX0NBVEVHT1JZX01FVEFEQVRBWydNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEtaWQnXS5JZCkgOiAnJztcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBwcm9qZWN0LlBST0pFQ1RfTkFNRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TWV0YWRhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBjYXRlZ29yeU1ldGFkYXRhdmFsdWUsIGRpc2FibGVkOiB0cnVlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtVXNlckRldGFpbHMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRnVsbE5hbWU6IHRoaXMucHJvamVjdE93bmVyRGV0YWlscy5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlckNOKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVBZG1pbkZpZWxkc1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgY2FtcGFpZ246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLmZvcm1DYW1wYWlnbih0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCksIGRpc2FibGVkOiB0cnVlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbXBhaWduT3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzID8gdGhpcy5mb3JtVXNlckRldGFpbHMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZ1bGxOYW1lOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMudXNlckNOKSA6ICcnLCBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG5ldyBEYXRlKHByb2plY3QuU1RBUlRfREFURSksIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkcyB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGVkU3RhdHVzSW5mbyAmJiBzZWxlY3RlZFN0YXR1c0luZm9bJ1NUQVRVU19MRVZFTCddID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFKSlcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGVuZERhdGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBuZXcgRGF0ZShwcm9qZWN0LkRVRV9EQVRFKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShwcm9qZWN0LkRFU0NSSVBUSU9OKSA/ICcnIDogcHJvamVjdC5ERVNDUklQVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbXM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBwcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVFZGl0T3B0aW9ucyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5OiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogcHJvamVjdC5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCwgZGlzYWJsZWQ6IHRoaXMuZGlzYWJsZUVkaXRPcHRpb25zIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNlbGVjdGVkUHJpb3JpdHksIGRpc2FibGVkOiBkaXNhYmxlRmllbGRzIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5zZWxlY3RlZFN0YXR1cywgZGlzYWJsZWQ6IHRoaXMuZGlzYWJsZVN0YXR1c09wdGlvbnMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAvKiBleHBlY3RlZER1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2wocHJvamVjdC5FWFBFQ1RFRF9EVVJBVElPTiwgW1ZhbGlkYXRvcnMucGF0dGVybignXlswLTldKiQnKV0pLCAqL1xyXG4gICAgICAgICAgICAgICAgICAgIGV4cGVjdGVkRHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkocHJvamVjdC5FWFBFQ1RFRF9EVVJBVElPTikgPyAnJyA6IHByb2plY3QuRVhQRUNURURfRFVSQVRJT04sIGRpc2FibGVkOiBkaXNhYmxlRmllbGRzIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgICAgaW1wb21lbnQgcmVxdWVzdG9yIGxvZ2ljIGZvciBpZGVhdGlvbiBpbnRlZ3JhdGlvblxyXG4gICAgICAgICAgICAgICAgICAgIHJlcXVlc3RlcjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IChwcm9qZWN0LlJfUE9fUkVRVUVTVEVSX1VTRVIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3QuUl9QT19SRVFVRVNURVJfVVNFUi5Vc2VySWQgPyBwcm9qZWN0LlJfUE9fUkVRVUVTVEVSX1VTRVIuVXNlcklkIDogJycpIHx8ICdOQScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgICAgICBzdGFuZGFyZFN0YXR1czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICd0cnVlJyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQ3VzdG9tV29ya2Zsb3c6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0LklTX0NVU1RPTV9XT1JLRkxPVyksIGRpc2FibGVkOiB0cnVlIH0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGFydERhdGUgPSBuZXcgRGF0ZShwcm9qZWN0LlNUQVJUX0RBVEUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEVuZERhdGUgPSBuZXcgRGF0ZShwcm9qZWN0LkRVRV9EQVRFKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdE9UTU1Bc3NldChNUE1fTEVWRUxTLlBST0pFQ1QpO1xyXG4gICAgICAgICAgICAgIC8qICAgfSkgKi87XHJcbiAgICAgICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBudW1lcmljZVZsYWx1ZSA9IHRoaXMuZ2V0TWF4TWluVmFsdWVGb3JOdW1iZXIoMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY2F0ZWdvcnlNZXRhZGF0YXZhbHVlID0gKHByb2plY3QgJiYgcHJvamVjdC5SX1BPX0NBVEVHT1JZX01FVEFEQVRBKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybUNhdGVnb3J5TWV0YWRhdGEocHJvamVjdC5SX1BPX0NBVEVHT1JZX01FVEFEQVRBWydNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEtaWQnXS5JZCkgOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0gPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHByb2plY3QuUFJPSkVDVF9OQU1FLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkcyB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeU1ldGFkYXRhOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogY2F0ZWdvcnlNZXRhZGF0YXZhbHVlLCBkaXNhYmxlZDogdHJ1ZSB9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtVXNlckRldGFpbHMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVzZXJJZDogdGhpcy5wcm9qZWN0T3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGdWxsTmFtZTogdGhpcy5wcm9qZWN0T3duZXJEZXRhaWxzLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlckNOKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlQWRtaW5GaWVsZHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FtcGFpZ246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLmZvcm1DYW1wYWlnbih0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCksIGRpc2FibGVkOiB0cnVlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYW1wYWlnbk93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuY2FtcGFpZ25Pd25lckRldGFpbHMgPyB0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGdWxsTmFtZTogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04pIDogJycsIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbmV3IERhdGUocHJvamVjdC5TVEFSVF9EQVRFKSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGRzIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGVkU3RhdHVzSW5mbyAmJiBzZWxlY3RlZFN0YXR1c0luZm9bJ1NUQVRVU19MRVZFTCddID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IG5ldyBEYXRlKHByb2plY3QuRFVFX0RBVEUpLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkcyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShwcm9qZWN0LkRFU0NSSVBUSU9OKSA/ICcnIDogcHJvamVjdC5ERVNDUklQVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlYW1zOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogcHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlRWRpdE9wdGlvbnMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnk6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBwcm9qZWN0LlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddLklkLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlRWRpdE9wdGlvbnMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNlbGVjdGVkUHJpb3JpdHksIGRpc2FibGVkOiBkaXNhYmxlRmllbGRzIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuc2VsZWN0ZWRTdGF0dXMsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVTdGF0dXNPcHRpb25zIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShwcm9qZWN0LkVYUEVDVEVEX0RVUkFUSU9OKSA/ICcnIDogcHJvamVjdC5FWFBFQ1RFRF9EVVJBVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGFuZGFyZFN0YXR1czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICd0cnVlJyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0N1c3RvbVdvcmtmbG93OiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpLCBkaXNhYmxlZDogdHJ1ZSB9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGFydERhdGUgPSBuZXcgRGF0ZShwcm9qZWN0LlNUQVJUX0RBVEUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRFbmREYXRlID0gbmV3IERhdGUocHJvamVjdC5EVUVfREFURSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0T1RNTUFzc2V0KE1QTV9MRVZFTFMuUFJPSkVDVCk7ICovXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0gPSB0aGlzLmZiLmdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBuZXcgRm9ybUNvbnRyb2woJ05ldyBQcm9qZWN0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TWV0YWRhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGZhbHNlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbXBhaWduOiBuZXcgRm9ybUNvbnRyb2woKSxcclxuICAgICAgICAgICAgICAgICAgICBjYW1wYWlnbk93bmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiB0cnVlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICBvd25lcjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmlzQ2FtcGFpZ25Qcm9qZWN0ID8gJycgOiB0aGlzLmZvcm1Vc2VyRGV0YWlscyh0aGlzLm92ZXJWaWV3Q29uZmlnLmN1cnJlbnRVc2VySW5mbywgdGhpcy5vdmVyVmlld0NvbmZpZy5jdXJyZW50VXNlcklkKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlQWRtaW5GaWVsZHNcclxuICAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICAvLzEyOFxyXG4gICAgICAgICAgICAgICAgICAgIC8qIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogIXRoaXMuaXNDYW1wYWlnblByb2plY3QgPyAnJyA6IHRoaXMuZm9ybVVzZXJEZXRhaWxzKHRoaXMub3ZlclZpZXdDb25maWcuY3VycmVudFVzZXJJbmZvLCB0aGlzLm92ZXJWaWV3Q29uZmlnLmN1cnJlbnRVc2VySWQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUFkbWluRmllbGRzXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSwgKi9cclxuICAgICAgICAgICAgICAgICAgICB0ZWFtczogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogbmV3IEZvcm1Db250cm9sKCcnKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnMgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zdGF0dXNPcHRpb25zWzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9uc1swXVsnTVBNX1N0YXR1cy1pZCddID8gdGhpcy5vdmVyVmlld0NvbmZpZy5zdGF0dXNPcHRpb25zWzBdWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucyAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9uc1swXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9uc1swXVsnTVBNX1ByaW9yaXR5LWlkJ10gPyB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9uc1swXVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnREYXRlOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGV4cGVjdGVkRHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMubWF4KG51bWVyaWNlVmxhbHVlKSwgVmFsaWRhdG9ycy5taW4oLShudW1lcmljZVZsYWx1ZSkpXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhbmRhcmRTdGF0dXM6IG5ldyBGb3JtQ29udHJvbCgndHJ1ZScpLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQ3VzdG9tV29ya2Zsb3c6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLmVuYWJsZUN1c3RvbVdvcmtmbG93LCBkaXNhYmxlZDogIXRoaXMuZW5hYmxlQ3VzdG9tV29ya2Zsb3cgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRNZXRhRGF0YShNUE1fTEVWRUxTLlBST0pFQ1QsIG51bGwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVTdGFydERhdGUgPSBkaXNhYmxlRmllbGRzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlRW5kRGF0ZSA9IGRpc2FibGVGaWVsZHM7XHJcbiAgICAgICAgICAgICAgICAvLyBUT0RPOiBFZGl0IHByb2plY3QgVGVtcGxhdGUgbG9naWNcclxuICAgICAgICAgICAgICAgIGNvbnN0IHByb2plY3QgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdDtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5QUk9KRUNUX1RFTVBMQVRFX1JFRl9JRCAmJiB0eXBlb2YgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlBST0pFQ1RfVEVNUExBVEVfUkVGX0lEKSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnNob3dSZXF1ZXN0ZXJGaWVsZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvKiBpbXBvbGVtZW50IGNhbmNlbCBkaXNhYmxlIGxvZ2ljICovXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkU3RhdHVzID0gKHByb2plY3QuUl9QT19TVEFUVVMgJiYgcHJvamVjdC5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddKSA/IHByb2plY3QuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFByaW9yaXR5ID0gKHByb2plY3QuUl9QT19QUklPUklUWSAmJiBwcm9qZWN0LlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddKSA/IHByb2plY3QuUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhdGVnb3J5TWV0YWRhdGF2YWx1ZSA9IChwcm9qZWN0ICYmIHByb2plY3QuUl9QT19DQVRFR09SWV9NRVRBREFUQSkgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybUNhdGVnb3J5TWV0YWRhdGEocHJvamVjdC5SX1BPX0NBVEVHT1JZX01FVEFEQVRBWydNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEtaWQnXS5JZCkgOiAnJztcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBwcm9qZWN0LlBST0pFQ1RfTkFNRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TWV0YWRhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiBjYXRlZ29yeU1ldGFkYXRhdmFsdWUsIGRpc2FibGVkOiB0cnVlIH1cclxuICAgICAgICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtVXNlckRldGFpbHMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlckNOLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRnVsbE5hbWU6IHRoaXMucHJvamVjdE93bmVyRGV0YWlscy5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlckNOKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVBZG1pbkZpZWxkc1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhcnREYXRlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogcHJvamVjdC5TVEFSVF9EQVRFLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkcyB9KSxcclxuICAgICAgICAgICAgICAgICAgICBlbmREYXRlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogcHJvamVjdC5EVUVfREFURSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkocHJvamVjdC5ERVNDUklQVElPTikgPyAnJyA6IHByb2plY3QuREVTQ1JJUFRJT04sIGRpc2FibGVkOiBkaXNhYmxlRmllbGRzIH0sIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1zOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogcHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlRWRpdE9wdGlvbnMgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHByb2plY3QuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVFZGl0T3B0aW9ucyB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuc2VsZWN0ZWRTdGF0dXMsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVTdGF0dXNPcHRpb25zIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNlbGVjdGVkUHJpb3JpdHksIGRpc2FibGVkOiBkaXNhYmxlRmllbGRzIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShwcm9qZWN0LkVYUEVDVEVEX0RVUkFUSU9OKSA/ICcnIDogcHJvamVjdC5FWFBFQ1RFRF9EVVJBVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZHMgfSwgW1ZhbGlkYXRvcnMubWF4KG51bWVyaWNlVmxhbHVlKSwgVmFsaWRhdG9ycy5taW4oLShudW1lcmljZVZsYWx1ZSkpXSksXHJcbiAgICAgICAgICAgICAgICAgICAgLyogZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHByb2plY3QuRVhQRUNURURfRFVSQVRJT04sIFtWYWxpZGF0b3JzLnBhdHRlcm4oJ15bMC05XSokJyldKSwgKi9cclxuICAgICAgICAgICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgICAgICAgIGltcG9tZW50IHJlcXVlc3RvciBsb2dpYyBmb3IgaWRlYXRpb24gaW50ZWdyYXRpb25cclxuICAgICAgICAgICAgICAgICAgICByZXF1ZXN0ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAocHJvamVjdC5SX1BPX1JFUVVFU1RFUl9VU0VSICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0LlJfUE9fUkVRVUVTVEVSX1VTRVIuVXNlcklkID8gcHJvamVjdC5SX1BPX1JFUVVFU1RFUl9VU0VSLlVzZXJJZCA6ICcnKSB8fCAnTkEnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhbmRhcmRTdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAndHJ1ZScgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBpc0N1c3RvbVdvcmtmbG93OiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdGhpcy5lbmFibGVDdXN0b21Xb3JrZmxvdyA/ICh0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0LklTX0NVU1RPTV9XT1JLRkxPVykpIDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGFydERhdGUgPSBwcm9qZWN0LlNUQVJUX0RBVEU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkRW5kRGF0ZSA9IHByb2plY3QuRFVFX0RBVEU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFByb2plY3RPVE1NQXNzZXQoTVBNX0xFVkVMUy5QUk9KRUNUKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IG5ldyBGb3JtQ29udHJvbCgnTmV3IFRlbXBsYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TWV0YWRhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGZhbHNlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtVXNlckRldGFpbHModGhpcy5vdmVyVmlld0NvbmZpZy5jdXJyZW50VXNlckluZm8sIHRoaXMub3ZlclZpZXdDb25maWcuY3VycmVudFVzZXJJZCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlQWRtaW5GaWVsZHNcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1zOiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5OiBuZXcgRm9ybUNvbnRyb2woJycpLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9ucyAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnN0YXR1c09wdGlvbnNbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9uc1swXVsnTVBNX1N0YXR1cy1pZCddID8gdGhpcy5vdmVyVmlld0NvbmZpZy5zdGF0dXNPcHRpb25zWzBdWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zICYmIHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zWzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zWzBdWydNUE1fUHJpb3JpdHktaWQnXSA/IHRoaXMub3ZlclZpZXdDb25maWcucHJpb3JpdHlPcHRpb25zWzBdWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGFuZGFyZFN0YXR1czogbmV3IEZvcm1Db250cm9sKCd0cnVlJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaXNDdXN0b21Xb3JrZmxvdzogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuZW5hYmxlQ3VzdG9tV29ya2Zsb3csIGRpc2FibGVkOiAhdGhpcy5lbmFibGVDdXN0b21Xb3JrZmxvdyB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE1ldGFEYXRhKE1QTV9MRVZFTFMuUFJPSkVDVCwgbnVsbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCAmJiAhdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmdldFJhd1ZhbHVlKCkuY2FtcGFpZ24gJiYgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbi5zZXRWYWx1ZSh0aGlzLmZvcm1DYW1wYWlnbih0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLm93bmVyRmllbGRDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXI7XHJcbiAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbk93bmVyRmllbGRDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lcjtcclxuICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbjtcclxuICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhdGVnb3J5TWV0YWRhdGFDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2F0ZWdvcnlNZXRhZGF0YTtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENhbXBhaWduSWQgJiYgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduICYmIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbi5zdGF0dXMgIT09ICdESVNBQkxFRCcpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduLmRpc2FibGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYW1wYWlnbikge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ24udmFsdWVDaGFuZ2VzLnN1YnNjcmliZShjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyAxMjhcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24gPSB0aGlzLmNhbXBhaWduTGlzdCA/IHRoaXMuY2FtcGFpZ25MaXN0LmZpbmQoZGF0YSA9PiBkYXRhWydDYW1wYWlnbi1pZCddLklkID09PSBjYW1wYWlnbi52YWx1ZSk6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1pblN0YXJ0RGF0ZSA9IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9TVEFSVF9EQVRFLCAneXl5eS1NTS1kZCcpID49IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykgPyB0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSA6IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0Q2FtcGFpZ25Pd25lckRldGFpbHNJZCh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uUl9QT19DQU1QQUlHTl9PV05FUlsnSWRlbnRpdHktaWQnXS5JZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSh0aGlzLmZvcm1Vc2VyRGV0YWlscyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVXNlcklkOiB0aGlzLmNhbXBhaWduT3duZXJEZXRhaWxzLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGdWxsTmFtZTogdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy5mdWxsTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgdGhpcy5jYW1wYWlnbk93bmVyRGV0YWlscy51c2VyQ04pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhbXBhaWduT3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2FtcGFpZ25Pd25lci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgobmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fU1RBUlRfREFURSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24uQ0FNUEFJR05fRU5EX0RBVEUpKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSwgJ3l5eXktTU0tZGQnKSA+PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhcnREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlBST0pFQ1RfQ0FNUEFJR05fREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhcnREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhcnREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuZW5kRGF0ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobmV3IERhdGUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmVuZERhdGUudmFsdWUpIDwgbmV3IERhdGUodGhpcy5zZWxlY3RlZENhbXBhaWduLkNBTVBBSUdOX1NUQVJUX0RBVEUpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ldyBEYXRlKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMuc2VsZWN0ZWRDYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbmREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LlBST0pFQ1RfQ0FNUEFJR05fREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW5kRGF0ZUVycm9yTWVzc2FnZSA9IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbmREYXRlRXJyb3JNZXNzYWdlID0gUHJvamVjdENvbnN0YW50LkNBTVBBSUdOX1NUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVuZERhdGVFcnJvck1lc3NhZ2UgPSBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gMTI4XHJcbiAgICAgICAvKiAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy50ZWFtcyAmJiB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIgJiYgdGhpcy5pc0NhbXBhaWduUHJvamVjdCkgeyAqL1xyXG4gICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMudGVhbXMgJiYgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm93bmVyICYmICh0aGlzLmlzQ2FtcGFpZ25Qcm9qZWN0IHx8IHRoaXMuaXNDYW1wYWlnbk1hbmFnZXIgJiYgIXRoaXMuaGFzVGVhbXMpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy50ZWFtcy52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHRlYW0gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRlYW0pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVGVhbSA9IHRoaXMub3ZlclZpZXdDb25maWcudGVhbXNPcHRpb25zLmZpbmQoZGF0YSA9PiBkYXRhWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGVhbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRUZWFtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0ZvclRlYW1Sb2xlKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5vd25lci5lbmFibGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5jYXRlZ29yeU1ldGFkYXRhLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0TWV0YURhdGEoTVBNX0xFVkVMUy5QUk9KRUNULCBkYXRhLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8qIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMuY2F0ZWdvcnkpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLmNhdGVnb3J5LnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoY2F0ZWdvcnkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNhdGVnb3J5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeURldGFpbHMoY2F0ZWdvcnkpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q2F0ZWdvcnlEZXRhaWxzKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY3VycmVudENhdGVnb3J5TGV2ZWwgPSB0aGlzLmlzQ2FtcGFpZ24gPyBNUE1fTEVWRUxTLkNBTVBBSUdOIDogTVBNX0xFVkVMUy5QUk9KRUNUO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yeUxldmVscyA9IHJlc3BvbnNlICYmIHJlc3BvbnNlLmNhdGVnb3J5TGV2ZWxzICYmIHJlc3BvbnNlLmNhdGVnb3J5TGV2ZWxzLmNhdGVnb3J5TGV2ZWwgPyByZXNwb25zZS5jYXRlZ29yeUxldmVscy5jYXRlZ29yeUxldmVsIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQ2F0ZWdvcnlMZXZlbCA9IGNhdGVnb3J5TGV2ZWxzLmZpbmQoY2F0ZWdvcnlMZXZlbCA9PiBjYXRlZ29yeUxldmVsLmNhdGVnb3J5TGV2ZWxUeXBlID09PSBjdXJyZW50Q2F0ZWdvcnlMZXZlbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByaW9yaXR5TGlzdCA9IHNlbGVjdGVkQ2F0ZWdvcnlMZXZlbC5sZXZlbFByaW9yaXR5LlByaW9yaXR5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9ICovXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdE9UTU1Bc3NldChjYXRlZ29yeUxldmVsKSB7XHJcbiAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbiAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkQ2FtcGFpZ24uT1RNTV9GT0xERVJfSUQgJiYgdHlwZW9mIHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbi5PVE1NX0ZPTERFUl9JRCA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5nZXRPVE1NRm9kbGVyQnlJZCh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkQ2FtcGFpZ24uT1RNTV9GT0xERVJfSUQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGZvbGRlckRldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmb2xkZXJEZXRhaWxzICYmIGZvbGRlckRldGFpbHMubWV0YWRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5tZXRhRGF0YVZhbHVlcyA9IGZvbGRlckRldGFpbHMubWV0YWRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkQ2FtcGFpZ24gJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduLlJfUE9fQ0FURUdPUllcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduLlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRDYW1wYWlnbi5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldE1ldGFEYXRhKGNhdGVnb3J5TGV2ZWwsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuT1RNTV9GT0xERVJfSUQgJiYgdHlwZW9mIHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0Lk9UTU1fRk9MREVSX0lEID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldE9UTU1Gb2RsZXJCeUlkKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0Lk9UTU1fRk9MREVSX0lEKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShmb2xkZXJEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZm9sZGVyRGV0YWlscyAmJiBmb2xkZXJEZXRhaWxzLm1ldGFkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlclZpZXdDb25maWcubWV0YURhdGFWYWx1ZXMgPSBmb2xkZXJEZXRhaWxzLm1ldGFkYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQVRFR09SWVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yeU1ldGFkYXRhSUQgPSAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQVRFR09SWV9NRVRBREFUQSkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQVRFR09SWV9NRVRBREFUQVsnTVBNX0NhdGVnb3J5X01ldGFkYXRhLWlkJ10uSWQgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldE1ldGFEYXRhKGNhdGVnb3J5TGV2ZWwsIGNhdGVnb3J5TWV0YWRhdGFJRCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRNZXRhRGF0YShjYXRlZ29yeUxldmVsLCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25DdXN0b21NZXRhZGF0YUZpZWxkQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmdldCgnQ3VzdG9tRmllbGRHcm91cCcpWydjb250cm9scyddLmZvckVhY2goZm9ybUdyb3VwID0+IHtcclxuICAgICAgICAgICAgZm9ybUdyb3VwWydjb250cm9scyddWydmaWVsZHNldCddLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0RGV0YWlsKHByb2plY3RJZCkge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIHByb2plY3RJRDogcHJvamVjdElkXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgZm9ya0pvaW4oW3RoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfUFJPSkVDVF9ERVRBSUxTX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KS8qLCB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3REYXRhVmFsdWVzKHByb2plY3RJZCkqL10pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlWzBdLlByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9qZWN0ID0gcmVzcG9uc2VbMF0uUHJvamVjdDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCA9IHByb2plY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvamVjdFN0YXR1c2VzID0gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0J5Y2F0ZWdvcnlOYW1lKE1QTV9MRVZFTFMuUFJPSkVDVCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvamVjdFN0YXR1czogYW55ID0gcHJvamVjdFN0YXR1c2VzICYmIHByb2plY3RTdGF0dXNlcy5sZW5ndGggPiAwID8gcHJvamVjdFN0YXR1c2VzWzBdIDogcHJvamVjdFN0YXR1c2VzO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICBjb25zdCBpbml0aWFsUHJvamVjdFN0YXR1cyA9IHByb2plY3RTdGF0dXNlcy5maW5kKHN0YXR1cyA9PiBzdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGluaXRpYWxQcm9qZWN0U3RhdHVzID0gcHJvamVjdFN0YXR1cy5maW5kKHN0YXR1cyA9PiBzdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpbml0aWFsUHJvamVjdFN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkICE9PSB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkICYmIHRoaXMuZW5hYmxlUHJvamVjdFJlc3RhcnRDb25maWcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbmFibGVSZXN0YXJ0UHJvamVjdCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdFNlcnZpY2Uuc2V0VXBkYXRlZFByb2plY3RJbmZvKHByb2plY3QpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vdGhpcy5vdmVyVmlld0NvbmZpZy5kYXRlVmFsaWRhdGlvbiA9IHJlc3BvbnNlWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0RmlsdGVycygpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBwcm9qZWN0IGRldGFpbHMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2FtcGFpZ25EZXRhaWwoY2FtcGFpZ25JZCkge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIENhbXBhaWduSWQ6IGNhbXBhaWduSWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICBmb3JrSm9pbihbdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQU1QQUlHTl9ERVRBSUxTX01FVEhPRF9OUywgdGhpcy5HRVRfQ0FNUEFJR05fV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpLyogLCB0aGlzLnByb2plY3RTZXJ2aWNlLmdldFByb2plY3REYXRhVmFsdWVzKHByb2plY3RJZCkgKi9cclxuICAgICAgICBdKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVswXS5DYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduID0gcmVzcG9uc2VbMF0uQ2FtcGFpZ247XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZENhbXBhaWduID0gY2FtcGFpZ247XHJcbiAgICAgICAgICAgICAgICAgICAgLyogdGhpcy5wcm9qZWN0U2VydmljZS5zZXRVcGRhdGVkUHJvamVjdEluZm8oY2FtcGFpZ24pOyBcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmRhdGVWYWxpZGF0aW9uID0gcmVzcG9uc2VbMV07Ki9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEZpbHRlcnMoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgY2FtcGFpZ24gZGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAyMDFcclxuICAgIGdldENhbXBhaWduQnlJZChjYW1wYWlnbklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgQ2FtcGFpZ25JZDogY2FtcGFpZ25JZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNBTVBBSUdOX0RFVEFJTFNfTUVUSE9EX05TLCB0aGlzLkdFVF9DQU1QQUlHTl9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5DYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuQ2FtcGFpZ24pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5DYW1wYWlnbiA9IFtyZXNwb25zZS5DYW1wYWlnbl07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW1wYWlnbkxpc3QgPSByZXNwb25zZS5DYW1wYWlnbjtcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY2FtcGFpZ25GaWx0ZXJMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FtcGFpZ25MaXN0Lm1hcChjYW1wYWlnbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZm9ybUNhbXBhaWduVmFsdWUoY2FtcGFpZ24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNhbXBhaWduLkNBTVBBSUdOX05BTUUgJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10gJiYgY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjYW1wYWlnbi5DQU1QQUlHTl9FTkRfREFURSwgJ3l5eXktTU0tZGQnKSA+PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FtcGFpZ25GaWx0ZXJMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBjYW1wYWlnbi5DQU1QQUlHTl9OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogY2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbkRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyA9IGNhbXBhaWduRmlsdGVyTGlzdDtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbXBhaWduTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnLmNhbXBhaWduRGF0YUNvbmZpZy5maWx0ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5vdmVyVmlld0NvbmZpZy5jYW1wYWlnbkRhdGFDb25maWcuZmlsdGVyT3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlcnNSb2xlKGdldFJlcXVlc3RPYmplY3QpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy51dGlsU2VydmljZS5jdXJyZW50VXNlckhhc1JvbGUoZ2V0UmVxdWVzdE9iamVjdCk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVVzZXJEZXRhaWxzKHVzZXJPYmo6IGFueSwgdXNlcklkPzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHVzZXJPYmogJiYgdXNlck9iai5Vc2VyRGlzcGxheU5hbWUgJiYgdXNlcklkICYmIHVzZXJJZCAhPT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHVzZXJPYmouVXNlckRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHVzZXJJZCxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiB1c2VyT2JqLlVzZXJEaXNwbGF5TmFtZSArICcgKCcgKyB1c2VySWQgKyAnKSdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2UgaWYgKHVzZXJPYmogJiYgdXNlck9iai5Vc2VySWQgJiYgdXNlck9iai5Vc2VySWQgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB1c2VyT2JqLkRlc2NyaXB0aW9uIHx8IHVzZXJPYmouRnVsbE5hbWUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdXNlck9iai5Vc2VySWQsXHJcbiAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogKHVzZXJPYmouRGVzY3JpcHRpb24gfHwgdXNlck9iai5GdWxsTmFtZSkgKyAnICgnICsgdXNlck9iai5Vc2VySWQgKyAnKSdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUNhdGVnb3J5TWV0YWRhdGEobWV0YWRhdGFJZDogYW55KSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRDYXRlZ29yeU1ldGFkYXRhID0gdGhpcy5vdmVyVmlld0NvbmZpZy5jYXRlZ29yeU1ldGFkYXRhQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChjYXRlZ29yeU1ldGFkYXRhID0+IGNhdGVnb3J5TWV0YWRhdGEudmFsdWUgPT09IG1ldGFkYXRhSWQpO1xyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgPyBzZWxlY3RlZENhdGVnb3J5TWV0YWRhdGEgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRpYWxpc2VPdmVydmlld0NvbmZpZygpIHtcclxuICAgICAgICB0aGlzLm92ZXJWaWV3Q29uZmlnID0ge1xyXG4gICAgICAgICAgICBpc1Byb2plY3Q6IHRydWUsXHJcbiAgICAgICAgICAgIGZvcm1UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICBzdGF0dXNPcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICBwcmlvcml0eU9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHRlYW1zT3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgLy8gY2F0ZWdvcnlPcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICBjdXJyZW50VXNlckluZm86IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJPYmplY3QoKSxcclxuICAgICAgICAgICAgaGFzUHJvamVjdEFkbWluUm9sZTogZmFsc2UsXHJcbiAgICAgICAgICAgIGN1cnJlbnRVc2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJRCgpLFxyXG4gICAgICAgICAgICBjdXJyZW50VXNlckl0ZW1JZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpLFxyXG4gICAgICAgICAgICBwcm9qZWN0T3duZXJzOiBudWxsLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFByb2plY3Q6IG51bGwsXHJcbiAgICAgICAgICAgIG1ldGFEYXRhVmFsdWVzOiBudWxsLFxyXG4gICAgICAgICAgICBpc1JlYWRPbmx5OiB0cnVlLFxyXG4gICAgICAgICAgICBzaG93UmVxdWVzdGVyRmllbGQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBvd25lckZpZWxkQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ093bmVyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2FtcGFpZ25Pd25lckZpZWxkQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NhbXBhaWduIE93bmVyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2FtcGFpZ25EYXRhQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NhbXBhaWduIE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjYXRlZ29yeU1ldGFkYXRhQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ2F0ZWdvcnkgTWV0YWRhdGEnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW11cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGF0ZVZhbGlkYXRpb246IG51bGwsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjYW5jZWxQcm9qZWN0Q3JlYXRpb24oKSB7XHJcbiAgICAgICAgdGhpcy5jb25maXJtQ2FuY2VsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlUHJvamVjdCgpIHtcclxuICAgICAgICAvLyBpZiBleGlzdGluZyBwcm9qZWN0XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZCh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uZ2V0UmF3VmFsdWUoKS5zdGF0dXMpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocHJvamVjdFN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvamVjdFN0YXR1cy5TVEFUVVNfVFlQRSA9PT0gUHJvamVjdENvbnN0YW50LlNUQVRVU19UWVBFX0ZJTkFMX0NBTkNFTExFRCB8fCBwcm9qZWN0U3RhdHVzLlRZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9PTkhPTEQgfHwgKHRoaXMub2xkU3RhdHVzSW5mbyAmJiB0aGlzLm9sZFN0YXR1c0luZm8uVFlQRSA9PT0gJ09OSE9MRCcgJiYgcHJvamVjdFN0YXR1cy5UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfSU5fUFJPR1JFU1MpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAvKiBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNhbmNlbCB0aGUgcHJvamVjdD8nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzQ29uZmlybWF0aW9uQ29tbWVudDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1lbnRUZXh0OiAnQ29tbWVudHMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNDb21tZW50UmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvck1lc3NhZ2U6ICdLaW5kbHkgZmlsbCB0aGUgY29tbWVudHMgZmllbGQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY29uZmlybWF0aW9uTWVzc2FnZSA9IHByb2plY3RTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQgPyAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNhbmNlbCB0aGUgcHJvamVjdD8nIDogcHJvamVjdFN0YXR1cy5UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfT05IT0xEID8gJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBwdXQgdGhlIHByb2plY3Qgb25Ib2xkPycgOiAodGhpcy5vbGRTdGF0dXNJbmZvICYmIHRoaXMub2xkU3RhdHVzSW5mby5UWVBFID09PSAnT05IT0xEJyAmJiBwcm9qZWN0U3RhdHVzLlRZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9JTl9QUk9HUkVTUykgPyAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIHJlc3VtZSB0aGUgcHJvamVjdD8nIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0dXNTZXJ2aWNlLkdldFN0YXR1c1JlYXNvbkJ5U3RhdHVzSWQocHJvamVjdFN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKS5zdWJzY3JpYmUoc3RhdHVzUmVhc29uID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coc3RhdHVzUmVhc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdFN0YXR1c1snTVBNX1N0YXR1c19SZWFzb24nXSA9IHN0YXR1c1JlYXNvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihCdWxrQ29tbWVudHNDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbWVzc2FnZTogJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjYW5jZWwgdGhlIHByb2plY3Q/JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBjb25maXJtYXRpb25NZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc0NvbmZpcm1hdGlvbkNvbW1lbnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudFRleHQ6ICdDb21tZW50cycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNDb21tZW50UmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JNZXNzYWdlOiAnS2luZGx5IGZpbGwgdGhlIGNvbW1lbnRzIGZpZWxkJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNEYXRhOiBwcm9qZWN0U3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c01lc3NhZ2U6ICdQcm9qZWN0IFN0YXR1cydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpc1N0YXR1c1JlYXNvbjogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKiAgY29uc29sZS5sb2cocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0ICE9PSAndHJ1ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbmNlbENvbW1lbnQgPSByZXN1bHQuY29tbWVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWFzb25zID0gcmVzdWx0LlNlbGVjdGVkUmVhc29ucztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3VjY2Vzc01lc3NhZ2UgPSBwcm9qZWN0U3RhdHVzLlNUQVRVU19UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEID8gJ1Byb2plY3QgY2FuY2VsbGF0aW9uIGlzIGluIHByb2dyZXNzIGFuZCBpdCBtaWdodCB0YWtlIHNvbWUgdGltZSB0byBjb21wbGV0ZScgOiBwcm9qZWN0U3RhdHVzLk5BTUUgPyAnUHJvamVjdCBpcyBtb3ZlZCB0byAnICsgcHJvamVjdFN0YXR1cy5OQU1FICsgJyBzdGF0ZScgOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdQcm9qZWN0ICcgKyBwcm9qZWN0U3RhdHVzLlNUQVRVU19UWVBFID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEID8gJ2NhbmNlbGxhdGlvbicgOiBwcm9qZWN0U3RhdHVzLk5BTUUgKyAnIHByb2Nlc3MgaXMgaW4gcHJvZ3Jlc3MgYW5kIGl0IG1pZ2h0IHRha2Ugc29tZSB0aW1lIHRvIGNvbXBsZXRlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKHN1Y2Nlc3NNZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5zdGF0dXMgPT09ICdWQUxJRCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVByb2plY3RTYXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMubmFtZS52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGV2ZW50RGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc0NhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgY2FtcGFpZ24gbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIHRlbXBsYXRlIG5hbWUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnUGxlYXNlIHByb3ZpZGUgYSB2YWxpZCBwcm9qZWN0IG5hbWUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlUHJvamVjdFNhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUuc2V0VmFsdWUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUudmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gVE9ETzogRm9yIEVkaXQgUHJvamVjdCBjaGVja1Byb2plY3RPd25lckNoYW5nZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdE93bmVyRGV0YWlscyh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIudmFsdWUudmFsdWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdLaW5kbHkgZmlsbCBhbGwgbWFuZGF0b3J5IGZpZWxkcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdCAhPT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgIHRoaXMuY2FuY2VsQ29tbWVudCA9IHJlc3VsdC5jb21tZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgdGhpcy5yZWFzb25zID0gcmVzdWx0LlNlbGVjdGVkUmVhc29ucztcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZWFzb24gPSAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuU2VsZWN0ZWRSZWFzb25zLmZvckVhY2goKHNlbGVjdGVkUmVhc29uLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFN0YXR1c1JlYXNvbiA9IHN0YXR1c1JlYXNvbi5NUE1fU3RhdHVzX1JlYXNvbi5maW5kKHN0YXR1c1JlYXNvbiA9PiBzdGF0dXNSZWFzb25bJ01QTV9TdGF0dXNfUmVhc29uLWlkJ10uSWQgPT09IHNlbGVjdGVkUmVhc29uLklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVhc29uID0gcmVhc29uICsgc2VsZWN0ZWRTdGF0dXNSZWFzb24uTkFNRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGluZGV4IDwgKHJlc3VsdC5TZWxlY3RlZFJlYXNvbnMubGVuZ3RoIC0gMSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlYXNvbiA9IHJlYXNvbiArICcsJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBuZXdDb21tZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZWFzb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3Q29tbWVudCA9IHJlYXNvbiArICcgOiAnICsgcmVzdWx0LmNvbW1lbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3Q29tbWVudCA9IHJlc3VsdC5jb21tZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbmNlbENvbW1lbnQgPSBuZXdDb21tZW50O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzdWNjZXNzTWVzc2FnZSA9IHByb2plY3RTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQgPyAnUHJvamVjdCBjYW5jZWxsYXRpb24gaXMgaW4gcHJvZ3Jlc3MgYW5kIGl0IG1pZ2h0IHRha2Ugc29tZSB0aW1lIHRvIGNvbXBsZXRlJyA6IHByb2plY3RTdGF0dXMuTkFNRSA/ICdQcm9qZWN0IGlzIG1vdmVkIHRvICcgKyBwcm9qZWN0U3RhdHVzLk5BTUUgKyAnIHN0YXRlJyA6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnUHJvamVjdCAnICsgcHJvamVjdFN0YXR1cy5TVEFUVVNfVFlQRSA9PT0gUHJvamVjdENvbnN0YW50LlNUQVRVU19UWVBFX0ZJTkFMX0NBTkNFTExFRCA/ICdjYW5jZWxsYXRpb24nIDogcHJvamVjdFN0YXR1cy5OQU1FICsgJyBwcm9jZXNzIGlzIGluIHByb2dyZXNzIGFuZCBpdCBtaWdodCB0YWtlIHNvbWUgdGltZSB0byBjb21wbGV0ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKHN1Y2Nlc3NNZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLnN0YXR1cyA9PT0gJ1ZBTElEJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMubmFtZS52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgZXZlbnREYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgY2FtcGFpZ24gbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnUGxlYXNlIHByb3ZpZGUgYSB2YWxpZCB0ZW1wbGF0ZSBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIHByb2plY3QgbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUuc2V0VmFsdWUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUudmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IEZvciBFZGl0IFByb2plY3QgY2hlY2tQcm9qZWN0T3duZXJDaGFuZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdE93bmVyRGV0YWlscyh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIudmFsdWUudmFsdWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdLaW5kbHkgZmlsbCBhbGwgbWFuZGF0b3J5IGZpZWxkcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlUHJvamVjdFNhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uc3RhdHVzID09PSAnVkFMSUQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVByb2plY3RTYXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5uYW1lLnZhbHVlLnRyaW0oKSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBldmVudERhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc0NhbXBhaWduKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnUGxlYXNlIHByb3ZpZGUgYSB2YWxpZCBjYW1wYWlnbiBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIHRlbXBsYXRlIG5hbWUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgcHJvamVjdCBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVByb2plY3RTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMubmFtZS5zZXRWYWx1ZSh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMubmFtZS52YWx1ZS50cmltKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gVE9ETzogRm9yIEVkaXQgUHJvamVjdCBjaGVja1Byb2plY3RPd25lckNoYW5nZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUHJvamVjdERldGFpbHMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0T3duZXJEZXRhaWxzKHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5jb250cm9scy5vd25lci52YWx1ZS52YWx1ZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUHJvamVjdERldGFpbHMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ0tpbmRseSBmaWxsIGFsbCBtYW5kYXRvcnkgZmllbGRzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0sIHByb2plY3RTdGF0dXNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcm9qZWN0U3RhdHVzRXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAvKiBpZiAodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLnN0YXR1cyA9PT0gJ1ZBTElEJykge1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMubmFtZS52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZXZlbnREYXRhO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgY2FtcGFpZ24gbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnUGxlYXNlIHByb3ZpZGUgYSB2YWxpZCB0ZW1wbGF0ZSBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIHByb2plY3QgbmFtZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUuc2V0VmFsdWUodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmNvbnRyb2xzLm5hbWUudmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IEZvciBFZGl0IFByb2plY3QgY2hlY2tQcm9qZWN0T3duZXJDaGFuZ2VcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0UHJvamVjdE93bmVyRGV0YWlscyh0aGlzLnByb2plY3RPdmVydmlld0Zvcm0uY29udHJvbHMub3duZXIudmFsdWUudmFsdWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVByb2plY3REZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdLaW5kbHkgZmlsbCBhbGwgbWFuZGF0b3J5IGZpZWxkcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLklORk8gfTtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlUHJvamVjdFNhdmUgPSBmYWxzZTtcclxuICAgICAgICB9ICovXHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlUHJvamVjdERldGFpbHMoKSB7XHJcbiAgICAgICAgY29uc3QgUHJvamVjdE9iamVjdDogUHJvamVjdFVwZGF0ZU9iaiB8IENhbXBhaWduVXBkYXRlT2JqID0gdGhpcy5jcmVhdGVOZXdQcm9qZWN0VXBkYXRlKCk7XHJcbiAgICAgICAgdGhpcy5zYXZlUHJvamVjdChQcm9qZWN0T2JqZWN0KTtcclxuICAgIH1cclxuXHJcbiAgICBzYXZlUHJvamVjdChQcm9qZWN0T2JqZWN0KSB7XHJcblxyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICBsZXQgc3RhbmRhcmRTdGF0dXNWYWx1ZTtcclxuICAgICAgICBpZiAoIXRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0KSB7XHJcbiAgICAgICAgICAgIHN0YW5kYXJkU3RhdHVzVmFsdWUgPSB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0udmFsdWUuc3RhbmRhcmRTdGF0dXM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIFByb2plY3RPYmplY3QgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jb21wYXJlVHdvUHJvamVjdERldGFpbHMoUHJvamVjdE9iamVjdCwgdGhpcy5vbGRQcm9qZWN0TWV0YWRhdGEpO1xyXG4gICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMoUHJvamVjdE9iamVjdCkubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdE9iamVjdC5CdWxrT3BlcmF0aW9uID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS51cGRhdGVQcm9qZWN0RGV0YWlscyhQcm9qZWN0T2JqZWN0LCB0aGlzLmNhbmNlbENvbW1lbnQsIHRoaXMucmVhc29ucykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6IHRoaXMub3ZlclZpZXdDb25maWcuZm9ybVR5cGUgKyAnIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5JywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlUHJvamVjdE5vdGlmaWNhdGlvbihyZXNwb25zZSwgZXZlbnREYXRhLCBmYWxzZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwZGF0aW5nIHByb2plY3QnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlUHJvamVjdE5vdGlmaWNhdGlvbihudWxsLCBldmVudERhdGEsIGZhbHNlLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVQcm9qZWN0U2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzQ2FtcGFpZ24pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RPYmogPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgQ2FtcGFpZ25PYmplY3Q6IFByb2plY3RPYmplY3RcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLnNldFByb2plY3RDdXN0b21NZXRhZGF0YShQcm9qZWN0T2JqZWN0LkN1c3RvbU1ldGFkYXRhKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9CUE1fTUVUSE9EX05TLCB0aGlzLkNSRUFURV9DQU1QQUlHTl9XU19NRVRIT0RfTkFNRSwgcmVxdWVzdE9iailcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiB0aGlzLm92ZXJWaWV3Q29uZmlnLmZvcm1UeXBlICsgJyBjcmVhdGVkIHN1Y2Nlc3NmdWxseScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLlNVQ0NFU1MgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kbGVQcm9qZWN0Tm90aWZpY2F0aW9uKHJlc3BvbnNlLCBldmVudERhdGEsIHRydWUsIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIGNyZWF0aW5nIGNhbXBhaWduJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kbGVQcm9qZWN0Tm90aWZpY2F0aW9uKG51bGwsIGV2ZW50RGF0YSwgdHJ1ZSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVxdWVzdE9iaiA9IHtcclxuICAgICAgICAgICAgICAgICAgICBQcm9qZWN0T2JqZWN0OiBQcm9qZWN0T2JqZWN0XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZS5zZXRQcm9qZWN0Q3VzdG9tTWV0YWRhdGEoUHJvamVjdE9iamVjdC5DdXN0b21NZXRhZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQlBNX01FVEhPRF9OUywgdGhpcy5DUkVBVEVfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSwgcmVxdWVzdE9iailcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiB0aGlzLm92ZXJWaWV3Q29uZmlnLmZvcm1UeXBlICsgJyBjcmVhdGVkIHN1Y2Nlc3NmdWxseScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLlNVQ0NFU1MgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kbGVQcm9qZWN0Tm90aWZpY2F0aW9uKHJlc3BvbnNlLCBldmVudERhdGEsIHRydWUsIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIGNyZWF0aW5nIHByb2plY3QnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZVByb2plY3ROb3RpZmljYXRpb24obnVsbCwgZXZlbnREYXRhLCB0cnVlLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzdGFydFByb2plY3QoKSB7XHJcbiAgICAgICAgdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c1JlYXNvbnMoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBub3RlTWVzc2FnZSA9ICgodGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtICYmIHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5kaXJ0eSkgfHwgKHRoaXMuY3VzdG9tRmllbGRzR3JvdXAgJiYgdGhpcy5jdXN0b21GaWVsZHNHcm91cC5kaXJ0eSkpID9cclxuICAgICAgICAgICAgICAgICdPbiByZXN0YXJ0aW5nIHByb2plY3QsIGNoYW5nZXMgd2lsbCBiZSBkaXNjYXJkZWQuJyA6IG51bGw7XHJcbiAgICAgICAgICAgIGNvbnN0IHByb2plY3RTdGF0dXMgPSB7XHJcbiAgICAgICAgICAgICAgICAvL1JFUVVJUkVfUkVBU09OOiAndHJ1ZScsXHJcbiAgICAgICAgICAgICAgICAvL1JFUVVJUkVfQ09NTUVOVFM6ICd0cnVlJyxcclxuICAgICAgICAgICAgICAgIE1QTV9TdGF0dXNfUmVhc29uOiAnJ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBwcm9qZWN0U3RhdHVzLk1QTV9TdGF0dXNfUmVhc29uID0gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQnVsa0NvbW1lbnRzQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOlxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdSZXN0YXJ0IHByb2plY3QnLFxyXG4gICAgICAgICAgICAgICAgICAgIGhhc0NvbmZpcm1hdGlvbkNvbW1lbnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgY29tbWVudFRleHQ6ICdDb21tZW50cycsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNDb21tZW50UmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JNZXNzYWdlOiAnS2luZGx5IGZpbGwgdGhlIGNvbW1lbnRzIGZpZWxkJyxcclxuICAgICAgICAgICAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJyxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXNEYXRhOiBwcm9qZWN0U3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgbm90ZTogbm90ZU1lc3NhZ2VcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQgIT09ICd0cnVlJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByZWFzb24gPSAnJztcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQuU2VsZWN0ZWRSZWFzb25zLmZvckVhY2goKHNlbGVjdGVkUmVhc29uLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFN0YXR1c1JlYXNvbiA9IHJlc3BvbnNlLk1QTV9TdGF0dXNfUmVhc29uLmZpbmQoc3RhdHVzUmVhc29uID0+IHN0YXR1c1JlYXNvblsnTVBNX1N0YXR1c19SZWFzb24taWQnXS5JZCA9PT0gc2VsZWN0ZWRSZWFzb24uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFzb24gPSByZWFzb24gKyBzZWxlY3RlZFN0YXR1c1JlYXNvbi5OQU1FO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggPCAocmVzdWx0LlNlbGVjdGVkUmVhc29ucy5sZW5ndGggLSAxKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVhc29uID0gcmVhc29uICsgJywnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5ld0NvbW1lbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlYXNvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdDb21tZW50ID0gcmVhc29uICsgJyA6ICcgKyByZXN1bHQuY29tbWVudDtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdDb21tZW50ID0gcmVzdWx0LmNvbW1lbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHJpZ2dlclJlc3RhcnRQcm9qZWN0KG5ld0NvbW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0cmlnZ2VyUmVzdGFydFByb2plY3QobmV3Q29tbWVudCkge1xyXG4gICAgICAgIGNvbnN0IGNvbW1lbnQgPSBuZXdDb21tZW50O1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkUHJpb3JpdHkgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnByaW9yaXR5T3B0aW9ucy5maW5kKHByaW9yaXR5ID0+IHByaW9yaXR5WydNUE1fUHJpb3JpdHktaWQnXS5JZCA9PT0gdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQpO1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkVGVhbSA9IHRoaXMub3ZlclZpZXdDb25maWcudGVhbXNPcHRpb25zLmZpbmQodGVhbSA9PiB0ZWFtWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCk7XHJcbiAgICAgICAgY29uc3QgaW50ZXJtZWRpYXRlU3RhdHVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxTdGF0dXNDb25maWcoKS5maW5kKHN0YXR1cyA9PiBzdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOVEVSTUVESUFURSk7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFVzZXJPYmplY3QgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyT2JqZWN0KCk7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdE1ldGFEYXRhID0ge1xyXG4gICAgICAgICAgICBQUk9KRUNUX0lTX0FDVElWRTogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuSVNfQUNUSVZFLFxyXG4gICAgICAgICAgICBQUk9KRUNUX0lTX0RFTEVURUQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBQUk9KRUNUX0NBVEVHT1JZOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNlbGVjdGVkQ2F0ZWdvcnkuQ0FURUdPUllfTkFNRSxcclxuICAgICAgICAgICAgUFJPSkVDVF9ERVNDUklQVElPTjogdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkRFU0NSSVBUSU9OKSxcclxuICAgICAgICAgICAgUFJPSkVDVF9EVUVfREFURTogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEUsXHJcbiAgICAgICAgICAgIFBST0pFQ1RfRU5EX0RBVEU6IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkVORF9EQVRFLFxyXG4gICAgICAgICAgICBQUk9KRUNUX05BTUU6IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlBST0pFQ1RfTkFNRSxcclxuICAgICAgICAgICAgUFJPSkVDVF9PV05FUl9OQU1FOiBjdXJyZW50VXNlck9iamVjdC5Vc2VyRGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgIFBST0pFQ1RfUFJJT1JJVFk6IHNlbGVjdGVkUHJpb3JpdHkuREVTQ1JJUFRJT04sXHJcbiAgICAgICAgICAgIFBST0pFQ1RfU1RBUlRfREFURTogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuU1RBUlRfREFURSxcclxuICAgICAgICAgICAgUFJPSkVDVF9TVEFUVVM6IGludGVybWVkaWF0ZVN0YXR1cy5OQU1FLFxyXG4gICAgICAgICAgICBQUk9KRUNUX1RFQU06IHNlbGVjdGVkVGVhbS5OQU1FLFxyXG4gICAgICAgICAgICBEQVRBX1RZUEU6IE9UTU1NUE1EYXRhVHlwZXMuUFJPSkVDVCxcclxuICAgICAgICAgICAgUFJPSkVDVF9JVEVNX0lEOiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdFsnUHJvamVjdC1pZCddLkl0ZW1JZCxcclxuICAgICAgICAgICAgUFJPSkVDVF9QUk9HUkVTUzogMCxcclxuICAgICAgICAgICAgUFJPSkVDVF9UWVBFOiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5QUk9KRUNUX1RZUEUsXHJcbiAgICAgICAgICAgIElTX1NUQU5EQVJEX1NUQVRVUzogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuSVNfU1RBTkRBUkRfU1RBVFVTLFxyXG4gICAgICAgICAgICBTVEFUVVNfSUQ6IGludGVybWVkaWF0ZVN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkLFxyXG4gICAgICAgICAgICBQUk9KRUNUX1NUQVRVU19UWVBFOiBpbnRlcm1lZGlhdGVTdGF0dXMuU1RBVFVTX1RZUEUsXHJcbiAgICAgICAgICAgIFBST0pFQ1RfSUQ6IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0WydQcm9qZWN0LWlkJ10uSWQsXHJcbiAgICAgICAgICAgIE9SSUdJTkFMX1BST0pFQ1RfU1RBUlRfREFURTogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuU1RBUlRfREFURSxcclxuICAgICAgICAgICAgT1JJR0lOQUxfUFJPSkVDVF9EVUVfREFURTogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEUsXHJcbiAgICAgICAgICAgIEVYUEVDVEVEX1BST0pFQ1RfRFVSQVRJT046IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5FWFBFQ1RFRF9EVVJBVElPTiksXHJcbiAgICAgICAgICAgIFBST0pFQ1RfVElNRV9TUEVOVDogMCxcclxuICAgICAgICAgICAgUFJPSkVDVF9DQU1QQUlHTl9JRDogdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQU1QQUlHTiAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX0NBTVBBSUdOWydDYW1wYWlnbi1pZCddLklkXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBjdXN0b21NZXRhZGF0YSA9IHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuZ2V0Q3VzdG9tTWV0YWRhdGEodGhpcy5jdXN0b21GaWVsZHNHcm91cCk7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5yZXN0YXJ0UHJvamVjdChwcm9qZWN0TWV0YURhdGEsIGN1c3RvbU1ldGFkYXRhLCBjb21tZW50KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdSZXN0YXJ0IHByb2plY3QgaW5pdGlhdGVkIHN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uZmlybUV4aXRPbkVkaXQoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKFxyXG4gICAgICAgICAgICBvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGlzY2FyZCB0aGUgY2hhbmdlcyBhbmQgRXhpdCA/JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uZmlybUNhbmNlbCgpIHtcclxuICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIERpc2NhcmQgdGhlIGNoYW5nZXM/JyxcclxuICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaFZpZXcoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZU5ld1Byb2plY3RVcGRhdGUoKTogUHJvamVjdFVwZGF0ZU9iaiB8IENhbXBhaWduVXBkYXRlT2JqIHtcclxuICAgICAgICBsZXQgUHJvamVjdE9iamVjdDogUHJvamVjdFVwZGF0ZU9iaiB8IENhbXBhaWduVXBkYXRlT2JqO1xyXG4gICAgICAgIGNvbnN0IHByb2plY3RWYWx1ZSA9IHRoaXMucHJvamVjdE92ZXJ2aWV3Rm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgIC8qICAgaWYocHJvamVjdFZhbHVlICYmIHByb2plY3RWYWx1ZS5jYW1wYWlnbi52YWx1ZSAmJiBwcm9qZWN0VmFsdWUuY2FtcGFpZ24uc3BsaXQoJy0nKVswXSkgKi9cclxuICAgICAgICBwcm9qZWN0VmFsdWUuY2FtcGFpZ24gPSB0aGlzLmNhbXBhaWduSWQgPyB0aGlzLmNhbXBhaWduSWQgOiB0aGlzLnNlbGVjdGVkQ2FtcGFpZ25JZCA/IHRoaXMuc2VsZWN0ZWRDYW1wYWlnbklkIDogJyc7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdFR5cGUgPSAodGhpcy5pc1RlbXBsYXRlKSA/IFByb2plY3RDb25zdGFudC5QUk9KRUNUX1RZUEVfVEVNUExBVEUgOiBQcm9qZWN0Q29uc3RhbnQuUFJPSkVDVF9UWVBFX1BST0pFQ1Q7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDYW1wYWlnbikge1xyXG4gICAgICAgICAgICBQcm9qZWN0T2JqZWN0ID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuY3JlYXRlTmV3Q2FtcGFpZ25VcGRhdGVPYmplY3QocHJvamVjdFZhbHVlLCB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlcklkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBQcm9qZWN0T2JqZWN0ID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuY3JlYXRlTmV3UHJvamVjdFVwZGF0ZU9iamVjdChwcm9qZWN0VmFsdWUsIHByb2plY3RUeXBlLCB0aGlzLnByb2plY3RPd25lckRldGFpbHMudXNlcklkKTtcclxuICAgICAgICAgICAgLy93ZSBjYW4gcmVtb3ZlIHRoaXMgYmxvY2sgb2YgY29kZVxyXG4gICAgICAgICAgICBpZiAodGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3RbJ1Byb2plY3QtaWQnXVxyXG4gICAgICAgICAgICAgICAgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3RbJ1Byb2plY3QtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdE9iamVjdC5Qcm9qZWN0SWQuSWQgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdFsnUHJvamVjdC1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTSAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddXHJcbiAgICAgICAgICAgICAgICAgICAgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIFByb2plY3RPYmplY3QuUlBPVGVhbS5UZWFtSUQuSWQgPSB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fQ0FURUdPUlkgJiYgdGhpcy5vdmVyVmlld0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ11cclxuICAgICAgICAgICAgICAgICAgICAmJiB0aGlzLm92ZXJWaWV3Q29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIFByb2plY3RPYmplY3QuUlBPQ2F0ZWdvcnkuQ2F0ZWdvcnlJRC5JZCA9IHRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMub3ZlclZpZXdDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmICF0aGlzLm92ZXJWaWV3Q29uZmlnLmlzUHJvamVjdCkge1xyXG4gICAgICAgICAgICBQcm9qZWN0T2JqZWN0LlJQT1N0YXR1cy5TdGF0dXNJRC5JZCA9IHRoaXMub3ZlclZpZXdDb25maWcuc3RhdHVzT3B0aW9uc1swXVsnTVBNX1N0YXR1cy1pZCddLklkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgUHJvamVjdE9iamVjdC5DdXN0b21NZXRhZGF0YSA9IHRoaXMucHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2UuZ2V0Q3VzdG9tTWV0YWRhdGEodGhpcy5jdXN0b21GaWVsZHNHcm91cCk7XHJcbiAgICAgICAgcmV0dXJuIFByb2plY3RPYmplY3Q7XHJcbiAgICB9XHJcbiAgICBoYW5kbGVQcm9qZWN0Tm90aWZpY2F0aW9uKHJlc3BvbnNlOiBhbnksIGV2ZW50RGF0YTogYW55LCBpc0NyZWF0aW9uOiBib29sZWFuLCBpc0Vycm9yOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICBpZiAoIWlzRXJyb3IpIHtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yTWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVByb2plY3RTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcgJiZcclxuICAgICAgICAgICAgICAgICAgICAoKChyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuUHJvamVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlByb2plY3RbJ1Byb2plY3QtaWQnXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlByb2plY3RbJ1Byb2plY3QtaWQnXS5JZCkgfHwgIWlzQ3JlYXRpb24pIHx8IChyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuQ2FtcGFpZ25cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuQ2FtcGFpZ25bJ0NhbXBhaWduLWlkJ11cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuQ2FtcGFpZ25bJ0NhbXBhaWduLWlkJ10uSWQpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ3JlYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLnJlc2V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VzdG9tRmllbGRzR3JvdXA/LnJlc2V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLlByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNhbGxCYWNrSGFuZGxlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuUHJvamVjdFsnUHJvamVjdC1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzVGVtcGxhdGU6ICh0aGlzLm92ZXJWaWV3Q29uZmlnLmlzVGVtcGxhdGUgPyB0cnVlIDogZmFsc2UpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNhbGxCYWNrSGFuZGxlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYW1wYWlnbklkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkNhbXBhaWduWydDYW1wYWlnbi1pZCddLklkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLnByb2plY3RPdmVydmlld0Zvcm0ucmVzZXQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5jdXN0b21GaWVsZHNHcm91cD8ucmVzZXQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlUHJvamVjdFNhdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nSGFuZGxlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2FtcCgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcImtva29rb1wiKVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmIChuYXZpZ2F0b3IubGFuZ3VhZ2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFkYXB0ZXIuc2V0TG9jYWxlKG5hdmlnYXRvci5sYW5ndWFnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHByb2plY3RDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFByb2plY3RDb25maWcoKTtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5lbmFibGVXb3JrV2VlayA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHByb2plY3RDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX1BST0pFQ1RfQ09ORklHLkVOQUJMRV9XT1JLX1dFRUtdKTtcclxuICAgICAgICB0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dKSA/ICcuKicgOlxyXG4gICAgICAgICAgICB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5OQU1FX1NUUklOR19QQVRURVJOXTtcclxuICAgICAgICB0aGlzLmVuYWJsZUNhbXBhaWduID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX0NBTVBBSUdOXSk7XHJcbiAgICAgICAgdGhpcy5lbmFibGVDdXN0b21Xb3JrZmxvdyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVOQUJMRV9DVVNUT01fV09SS0ZMT1ddKTtcclxuICAgICAgICB0aGlzLmVuYWJsZVByb2plY3RSZXN0YXJ0Q29uZmlnID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX1BST0pFQ1RfUkVTVEFSVF0pO1xyXG4gICAgICAgIHRoaXMuaXNDYW1wYWlnbk1hbmFnZXIgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhbXBhaWduUm9sZSgpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucXVlcnlQYXJhbXMuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwYXJhbXMuY2FtcGFpZ25JZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduSWQgPSBwYXJhbXMuY2FtcGFpZ25JZDtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNDYW1wYWlnblByb2plY3QgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZENhbXBhaWduSWQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0NhbXBhaWduUHJvamVjdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoVmlldygpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYXNVbnNhdmVkRGF0YSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9qZWN0T3ZlcnZpZXdGb3JtLmRpcnR5O1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpiZWZvcmV1bmxvYWQnLCBbJyRldmVudCddKVxyXG4gICAgdW5sb2FkTm90aWZpY2F0aW9uKCRldmVudDogYW55KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaGFzVW5zYXZlZERhdGEoKSkge1xyXG4gICAgICAgICAgICAkZXZlbnQucmV0dXJuVmFsdWUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb21wYXJlT2JqZWN0cyhvMTogYW55LCBvMjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYobzI9PSBvMSl7XHJcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICBkYXRlRmlsdGVyOiAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IGJvb2xlYW4gPVxyXG4gICAgICAgIChkYXRlOiBEYXRlIHwgbnVsbCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5lbmFibGVXb3JrV2Vlaykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXkgPSBkYXRlLmdldERheSgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRheSAhPT0gMCAmJiBkYXkgIT09IDY7XHJcbiAgICAgICAgICAgICAgICAvLzAgbWVhbnMgc3VuZGF5XHJcbiAgICAgICAgICAgICAgICAvLzYgbWVhbnMgc2F0dXJkYXlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxufVxyXG4iXX0=