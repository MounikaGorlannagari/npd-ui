import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { OTMMMPMDataTypes } from '../../mpm-utils/objects/OTMMMPMDataTypes';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
export var ProjectConstant = {
    PROJECT_TYPE_PROJECT: 'PROJECT',
    PROJECT_TYPE_TEMPLATE: 'TEMPLATE',
    CATEGORY_LEVEL_PROJECT: 'PROJECT',
    CATEGORY_LEVEL_CAMPAIGN: 'CAMPAIGN',
    STATUS_TYPE_INITIAL: 'INITIAL',
    STATUS_TYPE_FINAL_CANCELLED: 'FINAL-CANCELLED',
    STATUS_TYPE_FINAL_COMPLETED: 'FINAL-COMPLETED',
    STATUS_TYPE_ONHOLD: 'ONHOLD',
    STATUS_TYPE_IN_PROGRESS: 'INPROGRESS',
    NOTIFICATION_LABELS: {
        ERROR: 'ERROR',
        SUCCESS: 'SUCCESS',
        INFO: 'INFO',
        WARN: 'WARN'
    },
    NAME_STRING_PATTERN: '^[a-zA-Z0-9 _().-]*$',
    MPM_PROJECT_METADATA_GROUP: 'MPM.PROJECT.GROUP',
    MPM_CAMPAIGN_METADATA_GROUP: 'MPM.CAMPAIGN.GROUP',
    MPM_PROJECT_CUSTOM_METADATA_GROUP: 'MPM.CUSTOM.PROJECT.GROUP',
    MPM_CAMPAIGN_CUSTOM_METADATA_GROUP: 'MPM.CUSTOM.CAMPAIGN.GROUP',
    METADATA_EDIT_TYPES: {
        SIMPLE: 'SIMPLE',
        DATE: 'DATE',
        TEXTAREA: 'TEXTAREA',
        COMBO: 'COMBO',
        NUMBER: 'NUMBER',
    },
    DEFAULT_CARD_FIELDS: [
        MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME, MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY,
        MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS
    ],
    GET_ALL_PROJECT_SEARCH_CONDITION_LIST: [{
            type: IndexerDataTypes.STRING,
            field_id: MPMFieldConstants.MPM_PROJECT_FEILDS.IS_PROJECT_ACTIVE,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            value: 'true'
        },
        {
            type: IndexerDataTypes.STRING,
            field_id: MPMFieldConstants.CONTENT_TYPE,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            value: OTMMMPMDataTypes.PROJECT,
            relational_operator: MPMSearchOperators.AND
        }],
    GET_ALL_CAMPAIGN_SEARCH_CONDITION_LIST: [{
            type: IndexerDataTypes.STRING,
            field_id: MPMFieldConstants.MPM_CAMPAIGN_FEILDS.CAMPAIGN_IS_ACTIVE,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            value: 'true'
        },
        {
            type: IndexerDataTypes.STRING,
            field_id: MPMFieldConstants.CONTENT_TYPE,
            relational_operator_id: MPMSearchOperators.IS,
            relational_operator_name: MPMSearchOperatorNames.IS,
            value: OTMMMPMDataTypes.CAMPAIGN,
            relational_operator: MPMSearchOperators.AND
        }],
    PROJECT_START_DATE_ERROR_MESSAGE: 'Selected date is less than project start date',
    CAMPAIGN_START_DATE_ERROR_MESSAGE: 'Selected date is less than campaign start date',
    PROJECT_CAMPAIGN_DATE_ERROR_MESSAGE: 'Selected date is not in the range of campaign start date/end date',
    START_DATE_ERROR_MESSAGE: 'Selected date cannot be of past dates',
    TASK_DATE_ERROR_MESSAGE: 'Selected date is not in the range of project start date/end date',
    TASK_START_DATE_ERROR_MESSAGE: 'Selected date is less than task start date',
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5jb25zdGFudHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDMUYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUVBQXFFLENBQUM7QUFDekcsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDNUUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0seUVBQXlFLENBQUM7QUFFakgsTUFBTSxDQUFDLElBQU0sZUFBZSxHQUFHO0lBQzNCLG9CQUFvQixFQUFFLFNBQVM7SUFDL0IscUJBQXFCLEVBQUUsVUFBVTtJQUNqQyxzQkFBc0IsRUFBRSxTQUFTO0lBQ2pDLHVCQUF1QixFQUFFLFVBQVU7SUFDbkMsbUJBQW1CLEVBQUUsU0FBUztJQUM5QiwyQkFBMkIsRUFBRSxpQkFBaUI7SUFDOUMsMkJBQTJCLEVBQUUsaUJBQWlCO0lBQzlDLGtCQUFrQixFQUFFLFFBQVE7SUFDNUIsdUJBQXVCLEVBQUUsWUFBWTtJQUNyQyxtQkFBbUIsRUFBRTtRQUNqQixLQUFLLEVBQUUsT0FBTztRQUNkLE9BQU8sRUFBRSxTQUFTO1FBQ2xCLElBQUksRUFBRSxNQUFNO1FBQ1osSUFBSSxFQUFFLE1BQU07S0FDZjtJQUNELG1CQUFtQixFQUFFLHNCQUFzQjtJQUMzQywwQkFBMEIsRUFBRSxtQkFBbUI7SUFDL0MsMkJBQTJCLEVBQUUsb0JBQW9CO0lBQ2pELGlDQUFpQyxFQUFFLDBCQUEwQjtJQUM3RCxrQ0FBa0MsRUFBRSwyQkFBMkI7SUFDL0QsbUJBQW1CLEVBQUU7UUFDakIsTUFBTSxFQUFFLFFBQVE7UUFDaEIsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUUsVUFBVTtRQUNwQixLQUFLLEVBQUUsT0FBTztRQUNkLE1BQU0sRUFBRSxRQUFRO0tBQ25CO0lBQ0QsbUJBQW1CLEVBQUU7UUFDakIsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLGdCQUFnQjtRQUN4RyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjO0tBQ3REO0lBQ0QscUNBQXFDLEVBQUUsQ0FBQztZQUNwQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsTUFBTTtZQUM3QixRQUFRLEVBQUUsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCO1lBQ2hFLHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLEVBQUU7WUFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtZQUNuRCxLQUFLLEVBQUUsTUFBTTtTQUNoQjtRQUNEO1lBQ0ksSUFBSSxFQUFFLGdCQUFnQixDQUFDLE1BQU07WUFDN0IsUUFBUSxFQUFFLGlCQUFpQixDQUFDLFlBQVk7WUFDeEMsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsRUFBRTtZQUM3Qyx3QkFBd0IsRUFBRSxzQkFBc0IsQ0FBQyxFQUFFO1lBQ25ELEtBQUssRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPO1lBQy9CLG1CQUFtQixFQUFFLGtCQUFrQixDQUFDLEdBQUc7U0FDOUMsQ0FBQztJQUNGLHNDQUFzQyxFQUFFLENBQUM7WUFDckMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLE1BQU07WUFDN0IsUUFBUSxFQUFFLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQjtZQUNsRSxzQkFBc0IsRUFBRSxrQkFBa0IsQ0FBQyxFQUFFO1lBQzdDLHdCQUF3QixFQUFFLHNCQUFzQixDQUFDLEVBQUU7WUFDbkQsS0FBSyxFQUFFLE1BQU07U0FDaEI7UUFDRDtZQUNJLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxNQUFNO1lBQzdCLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxZQUFZO1lBQ3hDLHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLEVBQUU7WUFDN0Msd0JBQXdCLEVBQUUsc0JBQXNCLENBQUMsRUFBRTtZQUNuRCxLQUFLLEVBQUUsZ0JBQWdCLENBQUMsUUFBUTtZQUNoQyxtQkFBbUIsRUFBRSxrQkFBa0IsQ0FBQyxHQUFHO1NBQzlDLENBQUM7SUFDRixnQ0FBZ0MsRUFBRSwrQ0FBK0M7SUFDakYsaUNBQWlDLEVBQUUsZ0RBQWdEO0lBQ25GLG1DQUFtQyxFQUFFLG1FQUFtRTtJQUN4Ryx3QkFBd0IsRUFBRSx1Q0FBdUM7SUFDakUsdUJBQXVCLEVBQUUsa0VBQWtFO0lBQzNGLDZCQUE2QixFQUFFLDRDQUE0QztDQUM5RSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBJbmRleGVyRGF0YVR5cGVzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9JbmRleGVyRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvY29uc3RhbnRzL01QTVNlYXJjaE9wZXJhdG9yLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9UTU1NUE1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzJztcclxuXHJcbmV4cG9ydCBjb25zdCBQcm9qZWN0Q29uc3RhbnQgPSB7XHJcbiAgICBQUk9KRUNUX1RZUEVfUFJPSkVDVDogJ1BST0pFQ1QnLFxyXG4gICAgUFJPSkVDVF9UWVBFX1RFTVBMQVRFOiAnVEVNUExBVEUnLFxyXG4gICAgQ0FURUdPUllfTEVWRUxfUFJPSkVDVDogJ1BST0pFQ1QnLFxyXG4gICAgQ0FURUdPUllfTEVWRUxfQ0FNUEFJR046ICdDQU1QQUlHTicsXHJcbiAgICBTVEFUVVNfVFlQRV9JTklUSUFMOiAnSU5JVElBTCcsXHJcbiAgICBTVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQ6ICdGSU5BTC1DQU5DRUxMRUQnLFxyXG4gICAgU1RBVFVTX1RZUEVfRklOQUxfQ09NUExFVEVEOiAnRklOQUwtQ09NUExFVEVEJyxcclxuICAgIFNUQVRVU19UWVBFX09OSE9MRDogJ09OSE9MRCcsXHJcbiAgICBTVEFUVVNfVFlQRV9JTl9QUk9HUkVTUzogJ0lOUFJPR1JFU1MnLFxyXG4gICAgTk9USUZJQ0FUSU9OX0xBQkVMUzoge1xyXG4gICAgICAgIEVSUk9SOiAnRVJST1InLFxyXG4gICAgICAgIFNVQ0NFU1M6ICdTVUNDRVNTJyxcclxuICAgICAgICBJTkZPOiAnSU5GTycsXHJcbiAgICAgICAgV0FSTjogJ1dBUk4nXHJcbiAgICB9LFxyXG4gICAgTkFNRV9TVFJJTkdfUEFUVEVSTjogJ15bYS16QS1aMC05IF8oKS4tXSokJyxcclxuICAgIE1QTV9QUk9KRUNUX01FVEFEQVRBX0dST1VQOiAnTVBNLlBST0pFQ1QuR1JPVVAnLFxyXG4gICAgTVBNX0NBTVBBSUdOX01FVEFEQVRBX0dST1VQOiAnTVBNLkNBTVBBSUdOLkdST1VQJyxcclxuICAgIE1QTV9QUk9KRUNUX0NVU1RPTV9NRVRBREFUQV9HUk9VUDogJ01QTS5DVVNUT00uUFJPSkVDVC5HUk9VUCcsXHJcbiAgICBNUE1fQ0FNUEFJR05fQ1VTVE9NX01FVEFEQVRBX0dST1VQOiAnTVBNLkNVU1RPTS5DQU1QQUlHTi5HUk9VUCcsXHJcbiAgICBNRVRBREFUQV9FRElUX1RZUEVTOiB7XHJcbiAgICAgICAgU0lNUExFOiAnU0lNUExFJyxcclxuICAgICAgICBEQVRFOiAnREFURScsXHJcbiAgICAgICAgVEVYVEFSRUE6ICdURVhUQVJFQScsXHJcbiAgICAgICAgQ09NQk86ICdDT01CTycsXHJcbiAgICAgICAgTlVNQkVSOiAnTlVNQkVSJyxcclxuICAgIH0sXHJcbiAgICBERUZBVUxUX0NBUkRfRklFTERTOiBbXHJcbiAgICAgICAgTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfTkFNRSwgTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfUFJJT1JJVFksXHJcbiAgICAgICAgTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLlBST0pFQ1RfU1RBVFVTXHJcbiAgICBdLFxyXG4gICAgR0VUX0FMTF9QUk9KRUNUX1NFQVJDSF9DT05ESVRJT05fTElTVDogW3tcclxuICAgICAgICB0eXBlOiBJbmRleGVyRGF0YVR5cGVzLlNUUklORyxcclxuICAgICAgICBmaWVsZF9pZDogTVBNRmllbGRDb25zdGFudHMuTVBNX1BST0pFQ1RfRkVJTERTLklTX1BST0pFQ1RfQUNUSVZFLFxyXG4gICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IE1QTVNlYXJjaE9wZXJhdG9ycy5JUyxcclxuICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICB0eXBlOiBJbmRleGVyRGF0YVR5cGVzLlNUUklORyxcclxuICAgICAgICBmaWVsZF9pZDogTVBNRmllbGRDb25zdGFudHMuQ09OVEVOVF9UWVBFLFxyXG4gICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IE1QTVNlYXJjaE9wZXJhdG9ycy5JUyxcclxuICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMuSVMsXHJcbiAgICAgICAgdmFsdWU6IE9UTU1NUE1EYXRhVHlwZXMuUFJPSkVDVCxcclxuICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yOiBNUE1TZWFyY2hPcGVyYXRvcnMuQU5EXHJcbiAgICB9XSxcclxuICAgIEdFVF9BTExfQ0FNUEFJR05fU0VBUkNIX0NPTkRJVElPTl9MSVNUOiBbe1xyXG4gICAgICAgIHR5cGU6IEluZGV4ZXJEYXRhVHlwZXMuU1RSSU5HLFxyXG4gICAgICAgIGZpZWxkX2lkOiBNUE1GaWVsZENvbnN0YW50cy5NUE1fQ0FNUEFJR05fRkVJTERTLkNBTVBBSUdOX0lTX0FDVElWRSxcclxuICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgdHlwZTogSW5kZXhlckRhdGFUeXBlcy5TVFJJTkcsXHJcbiAgICAgICAgZmllbGRfaWQ6IE1QTUZpZWxkQ29uc3RhbnRzLkNPTlRFTlRfVFlQRSxcclxuICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgIHZhbHVlOiBPVE1NTVBNRGF0YVR5cGVzLkNBTVBBSUdOLFxyXG4gICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3I6IE1QTVNlYXJjaE9wZXJhdG9ycy5BTkRcclxuICAgIH1dLFxyXG4gICAgUFJPSkVDVF9TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U6ICdTZWxlY3RlZCBkYXRlIGlzIGxlc3MgdGhhbiBwcm9qZWN0IHN0YXJ0IGRhdGUnLFxyXG4gICAgQ0FNUEFJR05fU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFOiAnU2VsZWN0ZWQgZGF0ZSBpcyBsZXNzIHRoYW4gY2FtcGFpZ24gc3RhcnQgZGF0ZScsXHJcbiAgICBQUk9KRUNUX0NBTVBBSUdOX0RBVEVfRVJST1JfTUVTU0FHRTogJ1NlbGVjdGVkIGRhdGUgaXMgbm90IGluIHRoZSByYW5nZSBvZiBjYW1wYWlnbiBzdGFydCBkYXRlL2VuZCBkYXRlJyxcclxuICAgIFNUQVJUX0RBVEVfRVJST1JfTUVTU0FHRTogJ1NlbGVjdGVkIGRhdGUgY2Fubm90IGJlIG9mIHBhc3QgZGF0ZXMnLFxyXG4gICAgVEFTS19EQVRFX0VSUk9SX01FU1NBR0U6ICdTZWxlY3RlZCBkYXRlIGlzIG5vdCBpbiB0aGUgcmFuZ2Ugb2YgcHJvamVjdCBzdGFydCBkYXRlL2VuZCBkYXRlJyxcclxuICAgIFRBU0tfU1RBUlRfREFURV9FUlJPUl9NRVNTQUdFOiAnU2VsZWN0ZWQgZGF0ZSBpcyBsZXNzIHRoYW4gdGFzayBzdGFydCBkYXRlJyxcclxufTtcclxuIl19