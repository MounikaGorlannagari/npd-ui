import { __decorate } from "tslib";
import { Component, Input, isDevMode, EventEmitter, Output } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AssetStatusConstants } from './asset_status_constants';
import { AssetService } from '../shared/services/asset.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { TaskService } from '../tasks/task.service';
import { DeliverableConstants } from '../tasks/deliverable/deliverable.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { QdsService } from '../../upload/services/qds.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import * as acronui from '../../../lib/mpm-utils/auth/utility';
import { MPM_ROLES } from '../../../lib/mpm-utils/objects/Role';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { forkJoin, Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { Router } from '@angular/router';
import { TaskConstants } from '../tasks/task.constants';
import { DeliverableService } from '../tasks/deliverable/deliverable.service';
import { ProjectConstant } from '../project-overview/project.constants';
var AssetCardComponent = /** @class */ (function () {
    function AssetCardComponent(otmmService, assetService, sharingService, taskService, utilService, qdsService, entityAppdefService, loaderService, notificationService, appService, router, deliverableService) {
        this.otmmService = otmmService;
        this.assetService = assetService;
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.utilService = utilService;
        this.qdsService = qdsService;
        this.entityAppdefService = entityAppdefService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.appService = appService;
        this.router = router;
        this.deliverableService = deliverableService;
        this.assetVersionsCallBackHandler = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.selectedAssetsCallBackHandler = new EventEmitter();
        this.unSelectedAssetsCallBackHandler = new EventEmitter();
        this.shareAssetsCallbackHandler = new EventEmitter();
        this.assetPreviewCallBackHandler = new EventEmitter();
        this.downloadCallBackHandler = new EventEmitter();
        // @Output() crHandler = new EventEmitter<any>();
        this.openCreativeReview = new EventEmitter();
        this.downloadHandler = new EventEmitter();
        this.copyToolTip = 'Click to copy deliverable name';
        this.assetDescription = "";
        this.assetStatusConstants = AssetStatusConstants;
        this.deliverableConstants = DeliverableConstants;
        this.skip = 0;
        this.top = 10;
    }
    AssetCardComponent.prototype.findDocType = function (fileName) {
        return this.assetService.findIconByName(fileName);
    };
    AssetCardComponent.prototype.fileFormat = function (fileName) {
        return this.assetService.getfileFormatObject(fileName);
    };
    AssetCardComponent.prototype.checkDeliverable = function (asset) {
        if (this.checked) {
            this.selectedAssetsCallBackHandler.emit(asset);
            // this.crHandler.emit(asset);
        }
        else {
            this.unSelectedAssetsCallBackHandler.emit(asset);
        }
    };
    AssetCardComponent.prototype.download = function (asset) {
        this.downloadHandler.next(asset);
    };
    AssetCardComponent.prototype.getAssetVersions = function (asset) {
        var _this = this;
        this.otmmService.getAssetVersionsByAssetId(asset.asset_id).subscribe(function (response) {
            _this.assetVersionsCallBackHandler.next(response[0]);
        });
    };
    AssetCardComponent.prototype.getAssetDetails = function (asset) {
        this.assetDetailsCallBackHandler.next(asset);
    };
    AssetCardComponent.prototype.getAssetPreview = function (asset) {
        this.assetPreviewCallBackHandler.next(asset);
    };
    AssetCardComponent.prototype.updateDescription = function () {
        var _this = this;
        this.loadingHandler(true);
        var assetId = this.asset.asset_id;
        var body = {
            "edited_asset": {
                "data": {
                    "asset_identifier": "string",
                    "metadata": [
                        {
                            "id": "ARTESIA.FIELD.ASSET DESCRIPTION",
                            "name": "Description",
                            "type": "com.artesia.metadata.MetadataField",
                            "value": {
                                "value": {
                                    "type": "string",
                                    "value": this.assetDescription
                                }
                            }
                        }
                    ]
                }
            }
        };
        this.otmmService.updateMetadata(assetId, 'Asset', body).subscribe(function (res) {
            _this.notificationService.success('metadata Successfully updated');
        });
        this.loadingHandler(false);
    };
    AssetCardComponent.prototype.notificationHandler = function (event) {
        if (event.message) {
            this.notificationService.error(event.message);
        }
    };
    AssetCardComponent.prototype.loadingHandler = function (event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    };
    AssetCardComponent.prototype.getColumnValue = function (column) {
        var data = this.asset.allMetadata;
        for (var row = 0; row < data.length; row++) {
            if (column.OTMM_FIELD_ID === data[row].id) {
                if (data[row].value && data[row].value.value && data[row].value.value.value) {
                    return data[row].value.value.value;
                }
                else if (data[row].metadata_element && data[row].metadata_element.value && data[row].metadata_element.value.value && data[row].metadata_element.value.value.value) {
                    return data[row].metadata_element.value.value.value;
                }
            }
        }
        return " :NA";
    };
    AssetCardComponent.prototype.getProperty = function (asset, property) {
        return this.taskService.getProperty(asset, property);
    };
    AssetCardComponent.prototype.shareAsset = function () {
        this.shareAssetsCallbackHandler.next([this.asset]);
    };
    AssetCardComponent.prototype.getDeliverableDetails = function (deliverableId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.deliverableService.readDeliverable(deliverableId)
                .subscribe(function (response) {
                observer.next(response.Deliverable);
                observer.complete();
            }, function (error) {
                observer.error();
            });
        });
    };
    AssetCardComponent.prototype.onClickEdit = function () {
        this.assetDescription = "";
    };
    AssetCardComponent.prototype.openCR = function () {
        this.openCreativeReview.next(this.asset);
    };
    AssetCardComponent.prototype.ngOnChanges = function (changes) {
        if (changes && changes.selectAllDeliverables && typeof changes.selectAllDeliverables.currentValue === 'boolean' && !changes.isAssetListView) {
            if (changes.selectAllDeliverables.currentValue) {
                this.checked = true;
                this.selectedAssetsCallBackHandler.emit(this.asset);
            }
            else {
                this.checked = false;
            }
        }
        if (changes.isAssetListView) {
            this.checked = false;
            this.checkDeliverable(this.asset);
        }
    };
    AssetCardComponent.prototype.ngOnInit = function () {
        var _this = this;
        var _a;
        this.assetCardFields = ((_a = this.assetCardFields) === null || _a === void 0 ? void 0 : _a.length) > 10 ? this.assetCardFields.splice(0, 10) : this.assetCardFields;
        if (this.asset.inherited_metadata_collections && this.asset.inherited_metadata_collections[0]) {
            this.asset.allMetadata = this.asset.metadata.metadata_element_list[0].metadata_element_list.concat(this.asset.inherited_metadata_collections[0].inherited_metadata_values);
        }
        else {
            this.asset.allMetadata = this.asset.metadata.metadata_element_list[0].metadata_element_list;
        }
        this.otmmBaseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        this.appConfig = this.sharingService.getAppConfig();
        this.canDownload = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ENABLE_ASSET_DOWNLOAD]);
        this.crActionData = this.sharingService.getCRActions();
        this.checked = this.selectedAssetData === undefined ? false : this.selectedAssetData.find(function (assetId) { return assetId === _this.asset.asset_id; }) ? true : false;
        if (this.selectAllDeliverables) {
            this.selectAllDeliverables.subscribe(function (data) {
                if (data) {
                    _this.checked = true;
                }
                else {
                    _this.checked = false;
                }
                _this.checkDeliverable(_this.asset);
            });
        }
        if (this.asset && this.asset.metadata && this.asset.metadata.metadata_element_list) {
            this.asset.metadata.metadata_element_list.forEach(function (metadataElement) {
                if (metadataElement.id === 'MPM.DELIVERABLE.GROUP') {
                    metadataElement.metadata_element_list.forEach(function (metadataElementList) {
                        if (metadataElementList.id === 'MPM.DELIVERABLE.STATUS') {
                            _this.assetStatus = metadataElementList.value.value.value;
                        }
                    });
                }
                else if (metadataElement.id === 'MPM.ASSET.GROUP') {
                    metadataElement.metadata_element_list.forEach(function (metadataElementList) {
                        if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                            _this.isReferenceAsset = (metadataElementList && metadataElementList.value && metadataElementList.value.value && metadataElementList.value.value.value === 'true') ? true : false;
                        }
                    });
                }
            });
        }
        /*  console.log(this.router.url.split('/apps/mpm/project/')[1].split('/')[0]);
         this.currProjectId = this.router.url.split('/apps/mpm/project/')[1].split('/')[0];
 
         this.assetService.getProjectDetails(this.currProjectId).subscribe(projectResponse => {
             this.projectData = projectResponse.Project;
             this.currentUserCRData = this.crActionData.find(data => data['MPM_Teams-id'].Id === this.projectData.R_PO_TEAM['MPM_Teams-id'].Id);
             if (this.currentUserCRData) {
                 const userRoles = acronui.findObjectsByProp(this.currentUserCRData, 'MPM_Team_Role_Mapping');
                 let currentRole;
                 currentRole = MPM_ROLES.MANAGER;
                 userRoles.find(role => {
                     const appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                     const appRoleObj = appRoles.find(appRole => {
                         return appRole.ROLE_NAME === currentRole;
                     });
                     if (appRoleObj) {
                         this.currentUserTeamRoleMapping = role;
                     }
                 });
             }
         }); */
        this.taskId = this.taskService.getProperty(this.asset, TaskConstants.TASK_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        /*
                this.taskService.getTaskById(this.taskId).subscribe(taskData => {
                    this.taskName = taskData.NAME;
                    console.log(taskData);
                }); */
        this.deliverableId = this.taskService.getProperty(this.asset, DeliverableConstants.DELIVERABLE_ITEM_ID_METADATAFIELD_ID).split('.')[1];
        this.getDeliverableDetails(this.deliverableId)
            .subscribe(function (deliverableResponse) {
            _this.currProjectId = deliverableResponse.R_PO_PROJECT['Project-id'].Id;
            forkJoin([_this.assetService.getProjectDetails(_this.currProjectId), _this.taskService.getTaskById(_this.taskId)])
                .subscribe(function (responseList) {
                var projectResponse = null;
                _this.taskData = responseList[1];
                projectResponse = responseList[0] ? responseList[0] : null;
                _this.projectData = projectResponse.Project;
                _this.isOnHoldProject = (_this.projectData && _this.projectData.IS_ON_HOLD === 'true') ? true : false;
                _this.entityAppdefService.getStatusById(_this.projectData.R_PO_STATUS['MPM_Status-id'].Id).subscribe(function (status) {
                    _this.projectCompletedStatus = status.STATUS_TYPE === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
                    _this.currentUserCRData = _this.crActionData.find(function (data) { return data['MPM_Teams-id'].Id === _this.projectData.R_PO_TEAM['MPM_Teams-id'].Id; });
                    if (_this.currentUserCRData) {
                        var userRoles = acronui.findObjectsByProp(_this.currentUserCRData, 'MPM_Team_Role_Mapping');
                        var currentRole_1;
                        currentRole_1 = MPM_ROLES.MANAGER;
                        userRoles.forEach(function (role) {
                            var appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                            var appRoleObj = appRoles.find(function (appRole) {
                                return appRole.ROLE_NAME === currentRole_1;
                            });
                            if (appRoleObj) {
                                _this.currentUserTeamRoleMapping = role;
                            }
                        });
                    }
                });
                // deliverable status
                _this.entityAppdefService.getStatusById(deliverableResponse.R_PO_STATUS['MPM_Status-id'].Id).subscribe(function (status) {
                    _this.deliverableStatus = status.NAME;
                });
            });
        });
    };
    AssetCardComponent.ctorParameters = function () { return [
        { type: OTMMService },
        { type: AssetService },
        { type: SharingService },
        { type: TaskService },
        { type: UtilService },
        { type: QdsService },
        { type: EntityAppDefService },
        { type: LoaderService },
        { type: NotificationService },
        { type: AppService },
        { type: Router },
        { type: DeliverableService }
    ]; };
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "asset", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "selectedAssetData", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "isVersion", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "selectAllDeliverables", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "filterType", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "IsNotPMView", void 0);
    __decorate([
        Input()
    ], AssetCardComponent.prototype, "assetCardFields", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "assetVersionsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "assetDetailsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "selectedAssetsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "unSelectedAssetsCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "shareAssetsCallbackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "assetPreviewCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "downloadCallBackHandler", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "openCreativeReview", void 0);
    __decorate([
        Output()
    ], AssetCardComponent.prototype, "downloadHandler", void 0);
    AssetCardComponent = __decorate([
        Component({
            selector: 'mpm-asset-card',
            template: "<mat-card class=\"asset-card\" *ngIf=\"asset && (asset.latest_version || isVersion)\">\r\n    <mat-checkbox *ngIf=\"!isVersion\" color=\"primary\" class=\"flex-row-item select-asset\" (click)=\"$event.stopPropagation()\" (change)=\"checkDeliverable(asset)\" [(ngModel)]=\"checked\">\r\n    </mat-checkbox>\r\n    <div class=\"asset-content-area\" *ngIf=\"asset.rendition_content && asset.rendition_content.thumbnail_content && asset.rendition_content.thumbnail_content.url\">\r\n        <div class=\"asset-holder\">\r\n            <img src=\"{{otmmBaseUrl+asset.rendition_content.thumbnail_content.url.slice(1)}}\" alt=\"asset\" matTooltip=\"{{asset.name}}\">\r\n        </div>\r\n    </div>\r\n    <div class=\"asset-content-area\" *ngIf=\"!asset.rendition_content\">\r\n        <div class=\"asset-holder\">\r\n            <mat-icon class=\"preview-file-icon\" matTooltip=\"{{asset.name}}\">\r\n                {{findDocType(asset.name)}}\r\n            </mat-icon>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-content\">\r\n        <div class=\"flex-row asset-actions\">\r\n            <button class=\"green status-icon\" *ngIf=\"assetStatus === assetStatusConstants.APPROVED && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>check_circle</mat-icon>\r\n            </button>\r\n            <button class=\"red status-icon\" *ngIf=\"assetStatus === assetStatusConstants.REQUESTED_CHANGES || !asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>highlight_off</mat-icon>\r\n            </button>\r\n            <button class=\"orange status-icon\" *ngIf=\"assetStatus === assetStatusConstants.IN_PROGRESS && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>fiber_manual_record</mat-icon>\r\n            </button>\r\n            <!-- Need to remove asset status - defined, if not needed-->\r\n            <button class=\"gray status-icon\" *ngIf=\"assetStatus === assetStatusConstants.DEFINED && asset.latest_version\" mat-icon-button matTooltip=\"{{assetStatus}}\">\r\n                <mat-icon>fiber_manual_record</mat-icon>\r\n            </button>\r\n            <!-- open creative review -->\r\n            <button mat-icon-button matTooltip=\"Open Creative Review\" (click)=\"openCR()\" *ngIf=\"!isReferenceAsset && !isVersion\">\r\n                <mat-icon>rate_review</mat-icon>\r\n                <!--  *ngIf=\"!isTemplate && deliverableData.deliverableType !== 'COMPLETE'\"\r\n                 [disabled]=\"(isPMView && !isProjectOwner) || (!deliverableData.asset) || \r\n                (!isPMView && getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_TYPE) === 'REVIEW' && (deliverableData.deliverableReviewId ? deliverableData.deliverableReviewId !== loggedInUserId : (getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_APPROVER_ID) !== loggedInUserId && getProperty(deliverableData, MPMFeildConstantMapper.DELIVERABLE_ACTIVE_USER_NAME) === 'NA'))) || projectCompletedStatus || isOnHoldProject\" -->\r\n            </button>\r\n            <button mat-icon-button matTooltip=\"Download Asset\" [disabled]=\"isOnHoldProject || projectCompletedStatus\" (click)=\"download(asset)\" *ngIf=\"canDownload\">\r\n                <mat-icon>get_app</mat-icon>\r\n            </button>\r\n            <button mat-icon-button matTooltip=\"Share Asset\" [disabled]=\"isOnHoldProject || projectCompletedStatus\" (click)=\"shareAsset()\">\r\n                <mat-icon>share</mat-icon>\r\n            </button>\r\n            <span class=\"action-icon flex-row-item\">\r\n                <button mat-icon-button color=\"primary\" matTooltip=\"More Actions\" [matMenuTriggerFor]=\"AssetMenu\"\r\n                    *ngIf=\"!isVersion\">\r\n                    <mat-icon matSuffix>more_vert</mat-icon>\r\n                </button>\r\n            </span>\r\n        </div>\r\n        <div class=\"flex-row asset-details\">\r\n            <div class=\"flex-row-item\" *ngIf=\"asset.name\">\r\n                <mat-icon class=\"asset-type-icon\" matTooltip=\"{{fileFormat(asset.name)}}\">\r\n                    {{findDocType(asset.name)}}\r\n                </mat-icon>\r\n            </div>\r\n            <!-- <div class=\"flex-row-item\">\r\n                <span class=\"asset-name\"\r\n                    matTooltip=\"Deliverable: {{getProperty(asset, deliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID) || 'NA'}}\"\r\n                    matTooltipPosition=\"below\">{{getProperty(asset, deliverableConstants.DELIVERABLE_NAME_METADATAFIELD_ID) || 'NA'}}</span>\r\n            </div> -->\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"asset-name\" matTooltip=\"Name: {{asset.name || 'NA' }}\" matTooltipPosition=\"below\">{{asset.name || 'NA'}}</span>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-icon>call_merge</mat-icon>\r\n                <span matTooltip=\"Version: {{asset.version}}\">\r\n                    V{{asset.version}}\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</mat-card>\r\n<mat-menu #AssetMenu=\"matMenu\">\r\n    <button mat-menu-item (click)=\"getAssetDetails(asset)\">\r\n        <mat-icon>info</mat-icon>\r\n        <span>View Details</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetVersions(asset)\">\r\n        <mat-icon>collections</mat-icon>\r\n        <span>View Versions</span>\r\n    </button>\r\n    <button mat-menu-item (click)=\"getAssetPreview(asset)\">\r\n        <mat-icon>remove_red_eye</mat-icon>\r\n        <span>Preview</span>\r\n    </button>\r\n</mat-menu>",
            styles: [".asset-menu{color:#000}.version-chip{min-height:18px!important;padding:4px!important;margin-top:14px!important}.mat-checkbox-checked.select-asset{display:inline-block}.asset-card{padding:0}.asset-card .select-asset{position:absolute;top:10px;left:12px;display:inline-block;z-index:100}.asset-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.asset-card div.asset-content-area{height:14rem;width:100%;position:relative;display:flex;align-items:flex-end;justify-content:center;outline:0}.asset-card div.asset-content-area .asset-holder{display:inline-block;width:100%;height:100%;position:relative;border:1px solid #ddd}.asset-card div.asset-content-area .asset-holder img{display:block;position:absolute;left:0;right:0;top:0;bottom:0;margin:auto;width:auto;height:auto;max-width:100%;max-height:100%;min-width:190px;min-height:1px}.asset-card div.asset-content-area .preview-file-icon{font-size:70px;margin-left:80px;margin-top:80px}.asset-card div.card-content .asset-actions .action-icon{justify-content:flex-end}.asset-card div.card-content .asset-actions .red{color:red}.asset-card div.card-content .asset-actions .orange{color:orange}.asset-card div.card-content .asset-actions .green{color:green}.asset-card div.card-content .asset-actions .gray{color:gray}.asset-card div.card-content .asset-actions .status-icon{cursor:default}.asset-card div.card-content .asset-details{padding:8px}.asset-card div.card-content .asset-details .asset-name{text-overflow:ellipsis;overflow:hidden;width:140px;white-space:nowrap}"]
        })
    ], AssetCardComponent);
    return AssetCardComponent;
}());
export { AssetCardComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtY2FyZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L2Fzc2V0LWNhcmQvYXNzZXQtY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFFbEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBRWpHLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEtBQUssT0FBTyxNQUFNLHFDQUFxQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBT3hFO0lBK0NJLDRCQUNXLFdBQXdCLEVBQ3hCLFlBQTBCLEVBQzFCLGNBQThCLEVBQzlCLFdBQXdCLEVBQ3hCLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3RCLG1CQUF3QyxFQUN4QyxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsVUFBc0IsRUFDdEIsTUFBYyxFQUNkLGtCQUFzQztRQVh0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWpEdkMsaUNBQTRCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2RCxnQ0FBMkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3RELGtDQUE2QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDeEQsb0NBQStCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxRCwrQkFBMEIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3JELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM1RCxpREFBaUQ7UUFDdkMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3QyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJcEQsZ0JBQVcsR0FBRyxnQ0FBZ0MsQ0FBQztRQUUvQyxxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMsU0FBSSxHQUFHLENBQUMsQ0FBQztRQUNULFFBQUcsR0FBRyxFQUFFLENBQUM7SUErQkwsQ0FBQztJQUVMLHdDQUFXLEdBQVgsVUFBWSxRQUFRO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELHVDQUFVLEdBQVYsVUFBVyxRQUFRO1FBQ2YsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFRCw2Q0FBZ0IsR0FBaEIsVUFBaUIsS0FBSztRQUNsQixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9DLDhCQUE4QjtTQUNqQzthQUFNO1lBQ0gsSUFBSSxDQUFDLCtCQUErQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7SUFFRCxxQ0FBUSxHQUFSLFVBQVMsS0FBSztRQUNWLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCw2Q0FBZ0IsR0FBaEIsVUFBaUIsS0FBSztRQUF0QixpQkFJQztRQUhHLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDekUsS0FBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDakIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsNENBQWUsR0FBZixVQUFnQixLQUFLO1FBQ2pCLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDhDQUFpQixHQUFqQjtRQUFBLGlCQTJCRztRQTFCQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1FBQ2xDLElBQUksSUFBSSxHQUFHO1lBQ1QsY0FBYyxFQUFDO2dCQUNaLE1BQU0sRUFBQztvQkFDSixrQkFBa0IsRUFBQyxRQUFRO29CQUMzQixVQUFVLEVBQUM7d0JBQ1I7NEJBQ0csSUFBSSxFQUFDLGlDQUFpQzs0QkFDdEMsTUFBTSxFQUFDLGFBQWE7NEJBQ3BCLE1BQU0sRUFBQyxvQ0FBb0M7NEJBQzNDLE9BQU8sRUFBQztnQ0FDTCxPQUFPLEVBQUM7b0NBQ0wsTUFBTSxFQUFDLFFBQVE7b0NBQ2YsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7aUNBQ2hDOzZCQUNIO3lCQUNIO3FCQUNIO2lCQUNIO2FBQ0g7U0FDSCxDQUFBO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFDLE9BQU8sRUFBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxHQUFHO1lBQ25FLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUNuRSxDQUFDLENBQUMsQ0FBQztRQUNBLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELGdEQUFtQixHQUFuQixVQUFvQixLQUFLO1FBQ3ZCLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2pEO0lBQ0gsQ0FBQztJQUVQLDJDQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2hCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbEUsQ0FBQztJQUVDLDJDQUFjLEdBQWQsVUFBZSxNQUFNO1FBQ2pCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1FBQ3BDLEtBQUksSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFDO1lBQ3RDLElBQUcsTUFBTSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFDO2dCQUN2QyxJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFDO29CQUN6RSxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztpQkFDcEM7cUJBQ0ksSUFBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUM7b0JBQy9KLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO2lCQUNyRDthQUNGO1NBQ0Y7UUFDSCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBRUQsd0NBQVcsR0FBWCxVQUFZLEtBQUssRUFBRSxRQUFRO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCx1Q0FBVSxHQUFWO1FBQ0ksSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFHRCxrREFBcUIsR0FBckIsVUFBc0IsYUFBYTtRQUFuQyxpQkFVQztRQVRHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDO2lCQUNqRCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixHQUFDLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsbUNBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdDLENBQUM7SUFFRCx3Q0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLHFCQUFxQixJQUFJLE9BQU8sT0FBTyxDQUFDLHFCQUFxQixDQUFDLFlBQVksS0FBSyxTQUFTLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO1lBQ3pJLElBQUksT0FBTyxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRTtnQkFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3ZEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1NBQ0o7UUFDRCxJQUFHLE9BQU8sQ0FBQyxlQUFlLEVBQUM7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNyQztJQUNMLENBQUM7SUFFRCxxQ0FBUSxHQUFSO1FBQUEsaUJBd0dDOztRQXZHRyxJQUFJLENBQUMsZUFBZSxHQUFHLE9BQUEsSUFBSSxDQUFDLGVBQWUsMENBQUUsTUFBTSxJQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ3BILElBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxFQUFDO1lBQ3pGLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7U0FDOUs7YUFBSTtZQUNELElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDO1NBQy9GO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztRQUNySSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQS9CLENBQStCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDckosSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDNUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQWE7Z0JBQy9DLElBQUksSUFBSSxFQUFFO29CQUNOLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztpQkFDeEI7Z0JBQ0QsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixFQUFFO1lBQ2hGLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGVBQWU7Z0JBQzdELElBQUksZUFBZSxDQUFDLEVBQUUsS0FBSyx1QkFBdUIsRUFBRTtvQkFDaEQsZUFBZSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFBLG1CQUFtQjt3QkFDN0QsSUFBSSxtQkFBbUIsQ0FBQyxFQUFFLEtBQUssd0JBQXdCLEVBQUU7NEJBQ3JELEtBQUksQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7eUJBQzVEO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNLElBQUksZUFBZSxDQUFDLEVBQUUsS0FBSyxpQkFBaUIsRUFBRTtvQkFDakQsZUFBZSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFBLG1CQUFtQjt3QkFDN0QsSUFBSSxtQkFBbUIsQ0FBQyxFQUFFLEtBQUssOEJBQThCLEVBQUU7NEJBQzNELEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLEtBQUssSUFBSSxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt5QkFDcEw7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBb0JPO1FBQ1AsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVsSDs7OztzQkFJYztRQUVkLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxvQkFBb0IsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2SSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQzthQUN6QyxTQUFTLENBQUMsVUFBQyxtQkFBbUI7WUFDM0IsS0FBSSxDQUFDLGFBQWEsR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3ZFLFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2lCQUN6RyxTQUFTLENBQUMsVUFBQSxZQUFZO2dCQUNuQixJQUFJLGVBQWUsR0FBUSxJQUFJLENBQUM7Z0JBQ2hDLEtBQUksQ0FBQyxRQUFRLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxlQUFlLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDM0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDO2dCQUMzQyxLQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ25HLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtvQkFDckcsS0FBSSxDQUFDLHNCQUFzQixHQUFHLE1BQU0sQ0FBQyxXQUFXLEtBQUssZUFBZSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDaEgsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLEVBQXpFLENBQXlFLENBQUMsQ0FBQztvQkFDbkksSUFBSSxLQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQ3hCLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQzt3QkFDN0YsSUFBSSxhQUFXLENBQUM7d0JBQ2hCLGFBQVcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDO3dCQUNoQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs0QkFDbEIsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQzs0QkFDbEUsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87Z0NBQ3BDLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxhQUFXLENBQUM7NEJBQzdDLENBQUMsQ0FBQyxDQUFDOzRCQUNILElBQUksVUFBVSxFQUFFO2dDQUNaLEtBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUM7NkJBQzFDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILHFCQUFxQjtnQkFDckIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtvQkFDeEcsS0FBSSxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ3pDLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7O2dCQTNQdUIsV0FBVztnQkFDVixZQUFZO2dCQUNWLGNBQWM7Z0JBQ2pCLFdBQVc7Z0JBQ1gsV0FBVztnQkFDWixVQUFVO2dCQUNELG1CQUFtQjtnQkFDekIsYUFBYTtnQkFDUCxtQkFBbUI7Z0JBQzVCLFVBQVU7Z0JBQ2QsTUFBTTtnQkFDTSxrQkFBa0I7O0lBekR4QztRQUFSLEtBQUssRUFBRTtxREFBTztJQUNOO1FBQVIsS0FBSyxFQUFFO2lFQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTt5REFBVztJQUNWO1FBQVIsS0FBSyxFQUFFO3FFQUF1QjtJQUN0QjtRQUFSLEtBQUssRUFBRTswREFBWTtJQUNYO1FBQVIsS0FBSyxFQUFFOzJEQUFhO0lBQ1o7UUFBUixLQUFLLEVBQUU7K0RBQWlCO0lBRWY7UUFBVCxNQUFNLEVBQUU7NEVBQXdEO0lBQ3ZEO1FBQVQsTUFBTSxFQUFFOzJFQUF1RDtJQUN0RDtRQUFULE1BQU0sRUFBRTs2RUFBeUQ7SUFDeEQ7UUFBVCxNQUFNLEVBQUU7K0VBQTJEO0lBQzFEO1FBQVQsTUFBTSxFQUFFOzBFQUFzRDtJQUNyRDtRQUFULE1BQU0sRUFBRTsyRUFBdUQ7SUFDdEQ7UUFBVCxNQUFNLEVBQUU7dUVBQW1EO0lBRWxEO1FBQVQsTUFBTSxFQUFFO2tFQUE4QztJQUM3QztRQUFULE1BQU0sRUFBRTsrREFBMkM7SUFuQjNDLGtCQUFrQjtRQUw5QixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLGluTEFBMEM7O1NBRTdDLENBQUM7T0FDVyxrQkFBa0IsQ0E2UzlCO0lBQUQseUJBQUM7Q0FBQSxBQTdTRCxJQTZTQztTQTdTWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIGlzRGV2TW9kZSwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBc3NldFN0YXR1c0NvbnN0YW50cyB9IGZyb20gJy4vYXNzZXRfc3RhdHVzX2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFzc2V0U2VydmljZSB9IGZyb20gJy4uL3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrcy90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9UTU1NUE1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9saWIvbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IE1QTV9ST0xFUyB9IGZyb20gJy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvb2JqZWN0cy9Sb2xlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBmb3JrSm9pbiwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgVGFza0NvbnN0YW50cyB9IGZyb20gJy4uL3Rhc2tzL3Rhc2suY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RDb25zdGFudCB9IGZyb20gJy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC5jb25zdGFudHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1hc3NldC1jYXJkJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hc3NldC1jYXJkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Fzc2V0LWNhcmQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXNzZXRDYXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIGFzc2V0O1xyXG4gICAgQElucHV0KCkgc2VsZWN0ZWRBc3NldERhdGE7XHJcbiAgICBASW5wdXQoKSBpc1ZlcnNpb247XHJcbiAgICBASW5wdXQoKSBzZWxlY3RBbGxEZWxpdmVyYWJsZXM7XHJcbiAgICBASW5wdXQoKSBmaWx0ZXJUeXBlO1xyXG4gICAgQElucHV0KCkgSXNOb3RQTVZpZXc7XHJcbiAgICBASW5wdXQoKSBhc3NldENhcmRGaWVsZHM7XHJcblxyXG4gICAgQE91dHB1dCgpIGFzc2V0VmVyc2lvbnNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHVuU2VsZWN0ZWRBc3NldHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzaGFyZUFzc2V0c0NhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGFzc2V0UHJldmlld0NhbGxCYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRvd25sb2FkQ2FsbEJhY2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICAvLyBAT3V0cHV0KCkgY3JIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3BlbkNyZWF0aXZlUmV2aWV3ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZG93bmxvYWRIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgb3RtbUJhc2VVcmw7XHJcbiAgICBjaGVja2VkOiBib29sZWFuO1xyXG4gICAgY29weVRvb2xUaXAgPSAnQ2xpY2sgdG8gY29weSBkZWxpdmVyYWJsZSBuYW1lJztcclxuICAgIGFzc2V0U3RhdHVzO1xyXG4gICAgYXNzZXREZXNjcmlwdGlvbiA9IFwiXCI7XHJcbiAgICBhc3NldFN0YXR1c0NvbnN0YW50cyA9IEFzc2V0U3RhdHVzQ29uc3RhbnRzO1xyXG4gICAgZGVsaXZlcmFibGVDb25zdGFudHMgPSBEZWxpdmVyYWJsZUNvbnN0YW50cztcclxuICAgIHNraXAgPSAwO1xyXG4gICAgdG9wID0gMTA7XHJcbiAgICBhcHBDb25maWc6IGFueTtcclxuICAgIGNhbkRvd25sb2FkOiBib29sZWFuO1xyXG4gICAgaXNSZWZlcmVuY2VBc3NldDtcclxuICAgIGNyQWN0aW9uRGF0YTogYW55O1xyXG4gICAgcHJvamVjdERhdGE6IGFueTtcclxuICAgIGN1cnJlbnRVc2VyQ1JEYXRhOiBhbnk7XHJcbiAgICBjdXJyZW50VXNlclRlYW1Sb2xlTWFwcGluZzogYW55O1xyXG4gICAgY3JSb2xlOiBhbnk7XHJcbiAgICBjdXJyUHJvamVjdElkOiBhbnk7XHJcbiAgICB0YXNrSWQ6IGFueTtcclxuICAgIHRhc2tOYW1lOiBhbnk7XHJcbiAgICBkZWxpdmVyYWJsZUlkOiBhbnk7XHJcbiAgICB0YXNrRGF0YTogYW55O1xyXG4gICAgcHJvamVjdENvbXBsZXRlZFN0YXR1czogYW55O1xyXG4gICAgZGVsaXZlcmFibGVTdGF0dXM6IGFueTtcclxuICAgIGlzT25Ib2xkUHJvamVjdDogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBkZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwdWJsaWMgZGVsaXZlcmFibGVTZXJ2aWNlOiBEZWxpdmVyYWJsZVNlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgZmluZERvY1R5cGUoZmlsZU5hbWUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hc3NldFNlcnZpY2UuZmluZEljb25CeU5hbWUoZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGZpbGVGb3JtYXQoZmlsZU5hbWUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hc3NldFNlcnZpY2UuZ2V0ZmlsZUZvcm1hdE9iamVjdChmaWxlTmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tEZWxpdmVyYWJsZShhc3NldCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNoZWNrZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEFzc2V0c0NhbGxCYWNrSGFuZGxlci5lbWl0KGFzc2V0KTtcclxuICAgICAgICAgICAgLy8gdGhpcy5jckhhbmRsZXIuZW1pdChhc3NldCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy51blNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyLmVtaXQoYXNzZXQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkb3dubG9hZChhc3NldCl7XHJcbiAgICAgICAgdGhpcy5kb3dubG9hZEhhbmRsZXIubmV4dChhc3NldCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXNzZXRWZXJzaW9ucyhhc3NldCkge1xyXG4gICAgICAgIHRoaXMub3RtbVNlcnZpY2UuZ2V0QXNzZXRWZXJzaW9uc0J5QXNzZXRJZChhc3NldC5hc3NldF9pZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hc3NldFZlcnNpb25zQ2FsbEJhY2tIYW5kbGVyLm5leHQocmVzcG9uc2VbMF0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0RGV0YWlscyhhc3NldCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXREZXRhaWxzQ2FsbEJhY2tIYW5kbGVyLm5leHQoYXNzZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0UHJldmlldyhhc3NldCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRQcmV2aWV3Q2FsbEJhY2tIYW5kbGVyLm5leHQoYXNzZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZURlc2NyaXB0aW9uKCkge1xyXG4gICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIodHJ1ZSk7XHJcbiAgICAgICAgbGV0IGFzc2V0SWQgPSB0aGlzLmFzc2V0LmFzc2V0X2lkO1xyXG4gICAgICAgIGxldCBib2R5ID0ge1xyXG4gICAgICAgICAgXCJlZGl0ZWRfYXNzZXRcIjp7XHJcbiAgICAgICAgICAgICBcImRhdGFcIjp7XHJcbiAgICAgICAgICAgICAgICBcImFzc2V0X2lkZW50aWZpZXJcIjpcInN0cmluZ1wiLFxyXG4gICAgICAgICAgICAgICAgXCJtZXRhZGF0YVwiOltcclxuICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgIFwiaWRcIjpcIkFSVEVTSUEuRklFTEQuQVNTRVQgREVTQ1JJUFRJT05cIixcclxuICAgICAgICAgICAgICAgICAgICAgIFwibmFtZVwiOlwiRGVzY3JpcHRpb25cIixcclxuICAgICAgICAgICAgICAgICAgICAgIFwidHlwZVwiOlwiY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFGaWVsZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgXCJ2YWx1ZVwiOntcclxuICAgICAgICAgICAgICAgICAgICAgICAgIFwidmFsdWVcIjp7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInR5cGVcIjpcInN0cmluZ1wiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJ2YWx1ZVwiOiB0aGlzLmFzc2V0RGVzY3JpcHRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgIH1cclxuICAgICAgIHRoaXMub3RtbVNlcnZpY2UudXBkYXRlTWV0YWRhdGEoYXNzZXRJZCwnQXNzZXQnLGJvZHkpLnN1YnNjcmliZSgocmVzKSA9PiB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ21ldGFkYXRhIFN1Y2Nlc3NmdWxseSB1cGRhdGVkJyk7XHJcbiAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMubG9hZGluZ0hhbmRsZXIoZmFsc2UpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBub3RpZmljYXRpb25IYW5kbGVyKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50Lm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGV2ZW50Lm1lc3NhZ2UpO1xyXG4gICAgICAgIH0gXHJcbiAgICAgIH1cclxuICAgICAgXHJcbmxvYWRpbmdIYW5kbGVyKGV2ZW50KSB7XHJcbiAgICAoZXZlbnQpID8gdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKSA6IHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgfVxyXG5cclxuICAgIGdldENvbHVtblZhbHVlKGNvbHVtbil7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuYXNzZXQuYWxsTWV0YWRhdGE7XHJcbiAgICAgICAgZm9yKGxldCByb3cgPSAwOyByb3cgPCBkYXRhLmxlbmd0aDsgcm93Kyspe1xyXG4gICAgICAgICAgICBpZihjb2x1bW4uT1RNTV9GSUVMRF9JRCA9PT0gZGF0YVtyb3ddLmlkKXtcclxuICAgICAgICAgICAgICBpZihkYXRhW3Jvd10udmFsdWUgJiYgZGF0YVtyb3ddLnZhbHVlLnZhbHVlICYmIGRhdGFbcm93XS52YWx1ZS52YWx1ZS52YWx1ZSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YVtyb3ddLnZhbHVlLnZhbHVlLnZhbHVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBlbHNlIGlmKGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50ICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlICYmIGRhdGFbcm93XS5tZXRhZGF0YV9lbGVtZW50LnZhbHVlLnZhbHVlLnZhbHVlKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhW3Jvd10ubWV0YWRhdGFfZWxlbWVudC52YWx1ZS52YWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICByZXR1cm4gXCIgOk5BXCI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGdldFByb3BlcnR5KGFzc2V0LCBwcm9wZXJ0eSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRhc2tTZXJ2aWNlLmdldFByb3BlcnR5KGFzc2V0LCBwcm9wZXJ0eSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hhcmVBc3NldCgpIHtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRzQ2FsbGJhY2tIYW5kbGVyLm5leHQoW3RoaXMuYXNzZXRdKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgZ2V0RGVsaXZlcmFibGVEZXRhaWxzKGRlbGl2ZXJhYmxlSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVTZXJ2aWNlLnJlYWREZWxpdmVyYWJsZShkZWxpdmVyYWJsZUlkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5EZWxpdmVyYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGlja0VkaXQoKXtcclxuICAgICAgICB0aGlzLmFzc2V0RGVzY3JpcHRpb249XCJcIjtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ1IoKXtcclxuICAgICAgICB0aGlzLm9wZW5DcmVhdGl2ZVJldmlldy5uZXh0KHRoaXMuYXNzZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoY2hhbmdlcyAmJiBjaGFuZ2VzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcyAmJiB0eXBlb2YgY2hhbmdlcy5zZWxlY3RBbGxEZWxpdmVyYWJsZXMuY3VycmVudFZhbHVlID09PSAnYm9vbGVhbicgJiYgIWNoYW5nZXMuaXNBc3NldExpc3RWaWV3KSB7XHJcbiAgICAgICAgICAgIGlmIChjaGFuZ2VzLnNlbGVjdEFsbERlbGl2ZXJhYmxlcy5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkQXNzZXRzQ2FsbEJhY2tIYW5kbGVyLmVtaXQodGhpcy5hc3NldCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZihjaGFuZ2VzLmlzQXNzZXRMaXN0Vmlldyl7XHJcbiAgICAgICAgICAgIHRoaXMuY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmNoZWNrRGVsaXZlcmFibGUodGhpcy5hc3NldCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRDYXJkRmllbGRzID0gdGhpcy5hc3NldENhcmRGaWVsZHM/Lmxlbmd0aCA+IDEwID8gdGhpcy5hc3NldENhcmRGaWVsZHMuc3BsaWNlKDAsMTApIDogdGhpcy5hc3NldENhcmRGaWVsZHM7XHJcbiAgICAgICAgaWYodGhpcy5hc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnMgJiYgdGhpcy5hc3NldC5pbmhlcml0ZWRfbWV0YWRhdGFfY29sbGVjdGlvbnNbMF0pe1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0LmFsbE1ldGFkYXRhID0gdGhpcy5hc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3RbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0LmNvbmNhdCh0aGlzLmFzc2V0LmluaGVyaXRlZF9tZXRhZGF0YV9jb2xsZWN0aW9uc1swXS5pbmhlcml0ZWRfbWV0YWRhdGFfdmFsdWVzKTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgdGhpcy5hc3NldC5hbGxNZXRhZGF0YSA9IHRoaXMuYXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0WzBdLm1ldGFkYXRhX2VsZW1lbnRfbGlzdDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5vdG1tQmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJy4vJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICB0aGlzLmNhbkRvd25sb2FkID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRU5BQkxFX0FTU0VUX0RPV05MT0FEXSk7XHJcbiAgICAgICAgdGhpcy5jckFjdGlvbkRhdGEgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENSQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMuY2hlY2tlZCA9IHRoaXMuc2VsZWN0ZWRBc3NldERhdGEgPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogdGhpcy5zZWxlY3RlZEFzc2V0RGF0YS5maW5kKGFzc2V0SWQgPT4gYXNzZXRJZCA9PT0gdGhpcy5hc3NldC5hc3NldF9pZCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0QWxsRGVsaXZlcmFibGVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0QWxsRGVsaXZlcmFibGVzLnN1YnNjcmliZSgoZGF0YTogYm9vbGVhbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuY2hlY2tEZWxpdmVyYWJsZSh0aGlzLmFzc2V0KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5hc3NldCAmJiB0aGlzLmFzc2V0Lm1ldGFkYXRhICYmIHRoaXMuYXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0LmZvckVhY2gobWV0YWRhdGFFbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUVsZW1lbnQuaWQgPT09ICdNUE0uREVMSVZFUkFCTEUuR1JPVVAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFFbGVtZW50Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5mb3JFYWNoKG1ldGFkYXRhRWxlbWVudExpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50TGlzdC5pZCA9PT0gJ01QTS5ERUxJVkVSQUJMRS5TVEFUVVMnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFzc2V0U3RhdHVzID0gbWV0YWRhdGFFbGVtZW50TGlzdC52YWx1ZS52YWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChtZXRhZGF0YUVsZW1lbnQuaWQgPT09ICdNUE0uQVNTRVQuR1JPVVAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFFbGVtZW50Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5mb3JFYWNoKG1ldGFkYXRhRWxlbWVudExpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50TGlzdC5pZCA9PT0gJ01QTS5BU1NFVC5JU19SRUZFUkVOQ0VfQVNTRVQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzUmVmZXJlbmNlQXNzZXQgPSAobWV0YWRhdGFFbGVtZW50TGlzdCAmJiBtZXRhZGF0YUVsZW1lbnRMaXN0LnZhbHVlICYmIG1ldGFkYXRhRWxlbWVudExpc3QudmFsdWUudmFsdWUgJiYgbWV0YWRhdGFFbGVtZW50TGlzdC52YWx1ZS52YWx1ZS52YWx1ZSA9PT0gJ3RydWUnKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyogIGNvbnNvbGUubG9nKHRoaXMucm91dGVyLnVybC5zcGxpdCgnL2FwcHMvbXBtL3Byb2plY3QvJylbMV0uc3BsaXQoJy8nKVswXSk7XHJcbiAgICAgICAgIHRoaXMuY3VyclByb2plY3RJZCA9IHRoaXMucm91dGVyLnVybC5zcGxpdCgnL2FwcHMvbXBtL3Byb2plY3QvJylbMV0uc3BsaXQoJy8nKVswXTtcclxuIFxyXG4gICAgICAgICB0aGlzLmFzc2V0U2VydmljZS5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLmN1cnJQcm9qZWN0SWQpLnN1YnNjcmliZShwcm9qZWN0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgdGhpcy5wcm9qZWN0RGF0YSA9IHByb2plY3RSZXNwb25zZS5Qcm9qZWN0O1xyXG4gICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlckNSRGF0YSA9IHRoaXMuY3JBY3Rpb25EYXRhLmZpbmQoZGF0YSA9PiBkYXRhWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGhpcy5wcm9qZWN0RGF0YS5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkKTtcclxuICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRVc2VyQ1JEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgY29uc3QgdXNlclJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcCh0aGlzLmN1cnJlbnRVc2VyQ1JEYXRhLCAnTVBNX1RlYW1fUm9sZV9NYXBwaW5nJyk7XHJcbiAgICAgICAgICAgICAgICAgbGV0IGN1cnJlbnRSb2xlO1xyXG4gICAgICAgICAgICAgICAgIGN1cnJlbnRSb2xlID0gTVBNX1JPTEVTLk1BTkFHRVI7XHJcbiAgICAgICAgICAgICAgICAgdXNlclJvbGVzLmZpbmQocm9sZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFwcFJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyb2xlLCAnTVBNX0FQUF9Sb2xlcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBSb2xlT2JqID0gYXBwUm9sZXMuZmluZChhcHBSb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhcHBSb2xlLlJPTEVfTkFNRSA9PT0gY3VycmVudFJvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICBpZiAoYXBwUm9sZU9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclRlYW1Sb2xlTWFwcGluZyA9IHJvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgdGhpcy50YXNrSWQgPSB0aGlzLnRhc2tTZXJ2aWNlLmdldFByb3BlcnR5KHRoaXMuYXNzZXQsIFRhc2tDb25zdGFudHMuVEFTS19JVEVNX0lEX01FVEFEQVRBRklFTERfSUQpLnNwbGl0KCcuJylbMV07XHJcblxyXG4gICAgICAgIC8qIFxyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5nZXRUYXNrQnlJZCh0aGlzLnRhc2tJZCkuc3Vic2NyaWJlKHRhc2tEYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tOYW1lID0gdGFza0RhdGEuTkFNRTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0YXNrRGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9KTsgKi9cclxuXHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUlkID0gdGhpcy50YXNrU2VydmljZS5nZXRQcm9wZXJ0eSh0aGlzLmFzc2V0LCBEZWxpdmVyYWJsZUNvbnN0YW50cy5ERUxJVkVSQUJMRV9JVEVNX0lEX01FVEFEQVRBRklFTERfSUQpLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgdGhpcy5nZXREZWxpdmVyYWJsZURldGFpbHModGhpcy5kZWxpdmVyYWJsZUlkKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChkZWxpdmVyYWJsZVJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJQcm9qZWN0SWQgPSBkZWxpdmVyYWJsZVJlc3BvbnNlLlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuYXNzZXRTZXJ2aWNlLmdldFByb2plY3REZXRhaWxzKHRoaXMuY3VyclByb2plY3RJZCksIHRoaXMudGFza1NlcnZpY2UuZ2V0VGFza0J5SWQodGhpcy50YXNrSWQpXSlcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlTGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBwcm9qZWN0UmVzcG9uc2U6IGFueSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0RhdGEgPSByZXNwb25zZUxpc3RbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RSZXNwb25zZSA9IHJlc3BvbnNlTGlzdFswXSA/IHJlc3BvbnNlTGlzdFswXSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvamVjdERhdGEgPSBwcm9qZWN0UmVzcG9uc2UuUHJvamVjdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc09uSG9sZFByb2plY3QgPSAodGhpcy5wcm9qZWN0RGF0YSAmJiB0aGlzLnByb2plY3REYXRhLklTX09OX0hPTEQgPT09ICd0cnVlJykgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRTdGF0dXNCeUlkKHRoaXMucHJvamVjdERhdGEuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCkuc3Vic2NyaWJlKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RDb21wbGV0ZWRTdGF0dXMgPSBzdGF0dXMuU1RBVFVTX1RZUEUgPT09IFByb2plY3RDb25zdGFudC5TVEFUVVNfVFlQRV9GSU5BTF9DQU5DRUxMRUQgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyQ1JEYXRhID0gdGhpcy5jckFjdGlvbkRhdGEuZmluZChkYXRhID0+IGRhdGFbJ01QTV9UZWFtcy1pZCddLklkID09PSB0aGlzLnByb2plY3REYXRhLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuY3VycmVudFVzZXJDUkRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VyUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHRoaXMuY3VycmVudFVzZXJDUkRhdGEsICdNUE1fVGVhbV9Sb2xlX01hcHBpbmcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgY3VycmVudFJvbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudFJvbGUgPSBNUE1fUk9MRVMuTUFOQUdFUjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyUm9sZXMuZm9yRWFjaChyb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJvbGUsICdNUE1fQVBQX1JvbGVzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFwcFJvbGVPYmogPSBhcHBSb2xlcy5maW5kKGFwcFJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFwcFJvbGUuUk9MRV9OQU1FID09PSBjdXJyZW50Um9sZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHBSb2xlT2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyVGVhbVJvbGVNYXBwaW5nID0gcm9sZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZGVsaXZlcmFibGUgc3RhdHVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRTdGF0dXNCeUlkKGRlbGl2ZXJhYmxlUmVzcG9uc2UuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCkuc3Vic2NyaWJlKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlU3RhdHVzID0gc3RhdHVzLk5BTUU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19