import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as acronui from '../../../mpm-utils/auth/utility';
import { AppService } from '../../../mpm-utils/services/app.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { NotificationService } from '../../../notification/notification.service';
import { SessionStorageConstants } from '../../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/sharing.service";
import * as i3 from "../../../notification/notification.service";
var CategoryService = /** @class */ (function () {
    function CategoryService(appService, sharingService, notificationService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.CATEGORY_NS = 'http://schemas/AcheronMPMCore/MPM_Category/operations';
        this.CATEGORY_LEVEL_NS = 'http://schemas/AcheronMPMCore/MPM_Category_Level/operations';
        this.CATEGORY_WSAPP_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.GET_ALL_CATEGORIES = 'GetAllCategories';
        this.GET_ALL_CATEGORY_LEVELS_DETAILS_BY_NAME = 'GetLevelDetailsByName';
        this.GET_ALL_CATEGORY_LEVELS_DETAILS_BY_CATEGORY_ID = 'GetAllLevelsByCategoryID';
        this.GET_CATEGORY_DETAILS = 'GetCategoryDetails';
    }
    /* getLevelDetailsByName(categoryId: string, categoryLevelName: MPM_LEVEL): Observable<any> {
        return new Observable(observer => {
            const param = {
                categoryID: categoryId,
                categoryLevelName: categoryLevelName
            };
            this.appService.invokeRequest(this.CATEGORY_LEVEL_NS, this.GET_ALL_CATEGORY_LEVELS_DETAILS_BY_NAME, param)
                .subscribe(response => {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Category_Level'));
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
        });
    } */
    CategoryService.prototype.getCategoryLevelDetailsByType = function (type) {
        var categoryLevels = this.sharingService.getCategoryLevels();
        return categoryLevels.find(function (cl) { return cl.CATEGORY_LEVEL_TYPE === type; });
    };
    /* getAllCategoryLevelDetailsByCategoryId(categoryId: string): Observable<any> {
        return new Observable(observer => {
            const param = {
                categoryID: categoryId,
            };
            this.appService.invokeRequest(this.CATEGORY_LEVEL_NS, this.GET_ALL_CATEGORY_LEVELS_DETAILS_BY_CATEGORY_ID, param)
                .subscribe(response => {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Category_Level'));
                    observer.complete();
                }, error => {
                    this.notificationService.error('Something went wrong while getting category level details');
                    observer.error(error);
                });
        });
    } */
    CategoryService.prototype.getAllCategoryLevelDetailsByCategoryId = function (categoryId) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                categoryID: categoryId,
            };
            if (sessionStorage.getItem(SessionStorageConstants.ALL_LEVELS_BY_CATEGORY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_LEVELS_BY_CATEGORY_ID)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.CATEGORY_LEVEL_NS, _this.GET_ALL_CATEGORY_LEVELS_DETAILS_BY_CATEGORY_ID, param).subscribe(function (response) {
                    var categoryLevel = acronui.findObjectsByProp(response, 'MPM_Category_Level');
                    sessionStorage.setItem(SessionStorageConstants.ALL_LEVELS_BY_CATEGORY_ID, JSON.stringify(categoryLevel));
                    observer.next(categoryLevel);
                    observer.complete();
                }, function (error) {
                    _this.notificationService.error('Something went wrong while getting category level details');
                    observer.error(error);
                });
            }
        });
    };
    /* getAllCategories(): Observable<any> {
        return new Observable(observer => {
            this.appService.invokeRequest(this.CATEGORY_NS, this.GET_ALL_CATEGORIES, null)
                .subscribe(response => {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Category'));
                    observer.complete();
                }, error => {
                    this.notificationService.error('Something went wrong while getting categories');
                    observer.error(error);
                });
        });
    } */
    CategoryService.prototype.getAllCategories = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_CATEGORY) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_CATEGORY)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.CATEGORY_NS, _this.GET_ALL_CATEGORIES, null).subscribe(function (response) {
                    var category = acronui.findObjectsByProp(response, 'MPM_Category');
                    sessionStorage.setItem(SessionStorageConstants.ALL_CATEGORY, JSON.stringify(category));
                    observer.next(category);
                    observer.complete();
                });
            }
        });
    };
    CategoryService.prototype.getCategoryDetails = function (categoryId) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                categoryId: categoryId ? categoryId : '',
            };
            _this.appService.invokeRequest(_this.CATEGORY_WSAPP_NS, _this.GET_CATEGORY_DETAILS, param)
                .subscribe(function (response) {
                if (response && response.tuple && response.tuple.old && response.tuple.old.Category) {
                    observer.next(response.tuple.old.Category);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, function (error) {
                _this.notificationService.error('Something went wrong while getting category');
                observer.error(error);
            });
        });
    };
    CategoryService.ctorParameters = function () { return [
        { type: AppService },
        { type: SharingService },
        { type: NotificationService }
    ]; };
    CategoryService.ɵprov = i0.ɵɵdefineInjectable({ factory: function CategoryService_Factory() { return new CategoryService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.NotificationService)); }, token: CategoryService, providedIn: "root" });
    CategoryService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CategoryService);
    return CategoryService;
}());
export { CategoryService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcnkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL2NhdGVnb3J5LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLGlDQUFpQyxDQUFDO0FBQzNELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFFN0UsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDakYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0scURBQXFELENBQUM7Ozs7O0FBTTlGO0lBRUkseUJBQ1csVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsbUJBQXdDO1FBRnhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFHbkQsZ0JBQVcsR0FBRyx1REFBdUQsQ0FBQztRQUN0RSxzQkFBaUIsR0FBRyw2REFBNkQsQ0FBQztRQUNsRixzQkFBaUIsR0FBRywrQ0FBK0MsQ0FBQztRQUVwRSx1QkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztRQUN4Qyw0Q0FBdUMsR0FBRyx1QkFBdUIsQ0FBQztRQUNsRSxtREFBOEMsR0FBRywwQkFBMEIsQ0FBQztRQUM1RSx5QkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztJQVR4QyxDQUFDO0lBV0w7Ozs7Ozs7Ozs7Ozs7O1FBY0k7SUFHSix1REFBNkIsR0FBN0IsVUFBOEIsSUFBZTtRQUN6QyxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDL0QsT0FBTyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsRUFBRSxDQUFDLG1CQUFtQixLQUFLLElBQUksRUFBL0IsQ0FBK0IsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7UUFjSTtJQUVKLGdFQUFzQyxHQUF0QyxVQUF1QyxVQUFrQjtRQUF6RCxpQkFvQkM7UUFuQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxLQUFLLEdBQUc7Z0JBQ1YsVUFBVSxFQUFFLFVBQVU7YUFDekIsQ0FBQztZQUNGLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDcEYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JHLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLDhDQUE4QyxFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2hJLElBQU0sYUFBYSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztvQkFDaEYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7b0JBQ3pHLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzdCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDJEQUEyRCxDQUFDLENBQUM7b0JBQzVGLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRDs7Ozs7Ozs7Ozs7UUFXSTtJQUVKLDBDQUFnQixHQUFoQjtRQUFBLGlCQWNDO1FBYkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDdkUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDN0YsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDckUsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN2RixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBa0IsR0FBbEIsVUFBbUIsVUFBVztRQUE5QixpQkFtQkM7UUFsQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxLQUFLLEdBQUc7Z0JBQ1YsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQzNDLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQztpQkFDbEYsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRTtvQkFDakYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDM0MsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7Z0JBQzlFLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQTNIc0IsVUFBVTtnQkFDTixjQUFjO2dCQUNULG1CQUFtQjs7O0lBTDFDLGVBQWU7UUFKM0IsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUVXLGVBQWUsQ0FnSTNCOzBCQTlJRDtDQThJQyxBQWhJRCxJQWdJQztTQWhJWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IENhdGVnb3J5TGV2ZWwgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9DYXRlZ29yeUxldmVsJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENhdGVnb3J5U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBDQVRFR09SWV9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQ2F0ZWdvcnkvb3BlcmF0aW9ucyc7XHJcbiAgICBDQVRFR09SWV9MRVZFTF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQ2F0ZWdvcnlfTGV2ZWwvb3BlcmF0aW9ucyc7XHJcbiAgICBDQVRFR09SWV9XU0FQUF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vd3NhcHAvY29yZS8xLjAnO1xyXG5cclxuICAgIEdFVF9BTExfQ0FURUdPUklFUyA9ICdHZXRBbGxDYXRlZ29yaWVzJztcclxuICAgIEdFVF9BTExfQ0FURUdPUllfTEVWRUxTX0RFVEFJTFNfQllfTkFNRSA9ICdHZXRMZXZlbERldGFpbHNCeU5hbWUnO1xyXG4gICAgR0VUX0FMTF9DQVRFR09SWV9MRVZFTFNfREVUQUlMU19CWV9DQVRFR09SWV9JRCA9ICdHZXRBbGxMZXZlbHNCeUNhdGVnb3J5SUQnO1xyXG4gICAgR0VUX0NBVEVHT1JZX0RFVEFJTFMgPSAnR2V0Q2F0ZWdvcnlEZXRhaWxzJztcclxuXHJcbiAgICAvKiBnZXRMZXZlbERldGFpbHNCeU5hbWUoY2F0ZWdvcnlJZDogc3RyaW5nLCBjYXRlZ29yeUxldmVsTmFtZTogTVBNX0xFVkVMKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICAgICAgICAgIGNhdGVnb3J5SUQ6IGNhdGVnb3J5SWQsXHJcbiAgICAgICAgICAgICAgICBjYXRlZ29yeUxldmVsTmFtZTogY2F0ZWdvcnlMZXZlbE5hbWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQVRFR09SWV9MRVZFTF9OUywgdGhpcy5HRVRfQUxMX0NBVEVHT1JZX0xFVkVMU19ERVRBSUxTX0JZX05BTUUsIHBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX0NhdGVnb3J5X0xldmVsJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG5cclxuICAgIGdldENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlUeXBlKHR5cGU6IE1QTV9MRVZFTCk6IENhdGVnb3J5TGV2ZWwge1xyXG4gICAgICAgIGNvbnN0IGNhdGVnb3J5TGV2ZWxzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDYXRlZ29yeUxldmVscygpO1xyXG4gICAgICAgIHJldHVybiBjYXRlZ29yeUxldmVscy5maW5kKGNsID0+IGNsLkNBVEVHT1JZX0xFVkVMX1RZUEUgPT09IHR5cGUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEFsbENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlDYXRlZ29yeUlkKGNhdGVnb3J5SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW0gPSB7XHJcbiAgICAgICAgICAgICAgICBjYXRlZ29yeUlEOiBjYXRlZ29yeUlkLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNBVEVHT1JZX0xFVkVMX05TLCB0aGlzLkdFVF9BTExfQ0FURUdPUllfTEVWRUxTX0RFVEFJTFNfQllfQ0FURUdPUllfSUQsIHBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX0NhdGVnb3J5X0xldmVsJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIGNhdGVnb3J5IGxldmVsIGRldGFpbHMnKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxDYXRlZ29yeUxldmVsRGV0YWlsc0J5Q2F0ZWdvcnlJZChjYXRlZ29yeUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlJRDogY2F0ZWdvcnlJZCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0xFVkVMU19CWV9DQVRFR09SWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9MRVZFTFNfQllfQ0FURUdPUllfSUQpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQVRFR09SWV9MRVZFTF9OUywgdGhpcy5HRVRfQUxMX0NBVEVHT1JZX0xFVkVMU19ERVRBSUxTX0JZX0NBVEVHT1JZX0lELCBwYXJhbSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yeUxldmVsID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9DYXRlZ29yeV9MZXZlbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0xFVkVMU19CWV9DQVRFR09SWV9JRCwgSlNPTi5zdHJpbmdpZnkoY2F0ZWdvcnlMZXZlbCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoY2F0ZWdvcnlMZXZlbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgY2F0ZWdvcnkgbGV2ZWwgZGV0YWlscycpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8qIGdldEFsbENhdGVnb3JpZXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNBVEVHT1JZX05TLCB0aGlzLkdFVF9BTExfQ0FURUdPUklFUywgbnVsbClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9DYXRlZ29yeScpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBjYXRlZ29yaWVzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgZ2V0QWxsQ2F0ZWdvcmllcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQVRFR09SWSkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQVRFR09SWSkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkNBVEVHT1JZX05TLCB0aGlzLkdFVF9BTExfQ0FURUdPUklFUywgbnVsbCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yeSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fQ2F0ZWdvcnknKTtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9DQVRFR09SWSwgSlNPTi5zdHJpbmdpZnkoY2F0ZWdvcnkpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGNhdGVnb3J5KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDYXRlZ29yeURldGFpbHMoY2F0ZWdvcnlJZD8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlJZDogY2F0ZWdvcnlJZCA/IGNhdGVnb3J5SWQgOiAnJyxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQVRFR09SWV9XU0FQUF9OUywgdGhpcy5HRVRfQ0FURUdPUllfREVUQUlMUywgcGFyYW0pXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudHVwbGUgJiYgcmVzcG9uc2UudHVwbGUub2xkICYmIHJlc3BvbnNlLnR1cGxlLm9sZC5DYXRlZ29yeSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLnR1cGxlLm9sZC5DYXRlZ29yeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBjYXRlZ29yeScpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=