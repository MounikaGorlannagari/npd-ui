import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import * as acronui from '../../../mpm-utils/auth/utility';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/otmm.service";
import * as i3 from "../../../mpm-utils/services/util.service";
import * as i4 from "../../../shared/services/otmm-metadata.service";
import * as i5 from "../../../mpm-utils/services/sharing.service";
import * as i6 from "../../../comments/services/comments.util.service";
var ProjectService = /** @class */ (function () {
    function ProjectService(appService, otmmService, utilService, otmmMetadataService, sharingService, commentUtilService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.utilService = utilService;
        this.otmmMetadataService = otmmMetadataService;
        this.sharingService = sharingService;
        this.commentUtilService = commentUtilService;
        this.PROJECT_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.PROJECT_FILTER_NS = 'http://schemas/AcheronMPMCore/MPM_Project_Filter/operations';
        this.PROJECT_BPM = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_ALL_CATEGORY_METADATA_NS = 'http://schemas/AcheronMPMCore/MPM_Category_Metadata/operations';
        this.DOWNLOAD_EXCEL_NS = 'http://schemas.acheron.com/mpm/wsapp/excel/1.0';
        this.DELETE_PROJECT_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_TASK_BY_PROJECT_ID_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.DOWNLOAD_EXCEL_TEMPLATE = 'DownloadExcelTemplate ';
        // CREATE_BULK_PROJECT_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_BULK_PROJECT = 'BulkProjectCreation';
        this.CREATE_DOCUMENT_NS = 'http://schemas.cordys.com/documentstore/default/1.0';
        this.CREATE_DOCUMENT = 'CreateDocument';
        this.GET_ALL_PROJECTS_WS = 'GetAllProjects';
        this.GET_ALL_PROJECT_FILTERS = 'GetAllProjectFilters';
        this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_WS = 'GetMilestoneTasksByProject';
        this.GET_PROJECTS_BY_ID = 'ReadProject';
        this.RESTART_PROJECT_WS = 'HandleRestartProject';
        this.GET_ALL_CATEGORY_METADATA = 'GetAllCategoryMetadata';
        this.DELETE_PROJECT = 'DeleteProject';
        this.GET_TASK_BY_PROJECT_ID = 'GetTaskByProjectID ';
        this.updateProjectInfo = new BehaviorSubject(null);
        this.onUpdateProjectInfo = this.updateProjectInfo.asObservable();
    }
    ProjectService.prototype.setSelectedProjectDetails = function (projectObj) {
        this.selectedProject = projectObj;
    };
    ProjectService.prototype.getSelectedProjectDetails = function () {
        return this.selectedProject;
    };
    ProjectService.prototype.setUpdatedProjectInfo = function (currMetadata) {
        this.updateProjectInfo.next(currMetadata);
    };
    ProjectService.prototype.getProperty = function (obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    };
    ProjectService.prototype.converToLocalDate = function (obj, path) {
        var dateObj = this.otmmMetadataService.getFieldValueById(obj.metadata, path);
        if (dateObj != null) {
            return dateObj;
        }
        return;
    };
    ProjectService.prototype.getProjects = function (parameters) {
        var _this = this;
        var searchConditionList = JSON.parse(parameters.defaultSearchConditionList);
        if (parameters.searchConditionList && parameters.searchConditionList.length > 0) {
            searchConditionList.search_condition_list.search_condition = searchConditionList.search_condition_list.search_condition.concat(parameters.searchConditionList);
        }
        var skip = parameters.skip || 0;
        var top = parameters.top || 10;
        var folderFilter = searchConditionList.search_condition_list.search_condition.some(function (sc) { return sc.metadata_field_id === 'MPM.PROJECT.PROJECT_TYPE' && sc.value === 'PROJECT'; }) ?
            this.sharingService.getSelectedCategory().PROJECT_FOLDER_ID : this.sharingService.getSelectedCategory().TEMPLATE_FOLDER_ID;
        var facetRestrictionList = parameters.facetRestrictionList || '';
        var otmmProjectsSearchConfig = {
            load_type: 'metadata',
            level_of_detail: 'slim',
            child_count_load_type: 'folders',
            search_condition_list: searchConditionList,
            folder_filter_type: 'direct',
            facet_restriction_list: facetRestrictionList,
            folder_filter: folderFilter,
            sortString: parameters.sortString,
            search_config_id: parameters.search_config_id,
        };
        return new Observable(function (observer) {
            _this.otmmService.searchCustomText(otmmProjectsSearchConfig, skip, top, otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()).
                subscribe(function (response) {
                if (response.search_result_resource && response.search_result_resource.search_result
                    && response.search_result_resource.search_result.total_hit_count > 0
                    && response.search_result_resource.asset_list) {
                    response.search_result_resource.asset_list = _this.otmmMetadataService.setReferenceValueForMetadataFeilds(response.search_result_resource.asset_list, _this.otmmMetadataService.getDefaultProjectMetadataFields());
                    response.search_result_resource.asset_list = _this.otmmMetadataService.setReferenceValueForMetadataFeilds(response.search_result_resource.asset_list, _this.otmmMetadataService.getCustomProjectMetadataFields());
                }
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.getProjectById = function (projectId) {
        var _this = this;
        var parameter = {
            'Project-id': {
                Id: projectId
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_NS, _this.GET_PROJECTS_BY_ID, parameter)
                .subscribe(function (response) {
                observer.next(response.Project);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.getCompletedMilestonesByProjectId = function (projectId) {
        var _this = this;
        var parameter = {
            'projectId': projectId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_NS, _this.GET_COMPLETED_MILESTONES_BY_PROJECT_ID_WS, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.getProjectDataValues = function (projectId) {
        var _this = this;
        var parameter = {
            ProjectID: projectId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_BPM, 'EditProjectDateValidation', parameter)
                .subscribe(function (response) {
                observer.next({
                    startMaxDate: response && response['Date']['StartDate'] && typeof response['Date']['StartDate'] === 'string' ? new Date(response['Date']['StartDate']) : null,
                    endMinDate: response && response['Date']['EndDate'] && typeof response['Date']['EndDate'] === 'string' ? new Date(response['Date']['EndDate']) : null,
                });
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.updateProjectDetails = function (updatedProject, cancelComment, reasons) {
        var _this = this;
        var parameter = {
            ProjectObject: updatedProject,
            CancelComment: cancelComment,
            Reasons: {
                'MPM_Status_Reason-id': reasons
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_BPM, 'EditProject', parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.getCurrProjectStatus = function (statusId) {
        if (statusId) {
            return this.commentUtilService.getStatusLevelByStatusId(statusId);
        }
        return null;
    };
    ProjectService.prototype.bulkProjectCreation = function (documentURL, templateID) {
        var _this = this;
        var parameter = {
            documentURL: documentURL,
            templateID: templateID,
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_BPM, 'BulkProjectCreation', parameter).
                subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.restartProject = function (projectMetadata, customMetadata, comments) {
        var _this = this;
        var parameter = {
            ProjectMetadata: projectMetadata,
            CustomMetadata: customMetadata,
            Comments: comments
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_BPM, _this.RESTART_PROJECT_WS, parameter).
                subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.createDocument = function (documentName, documentContent) {
        var _this = this;
        var parameter = {
            DocumentName: documentName,
            Desctiption: "Test From Angular",
            DocumentContent: documentContent,
            Folder: "/opt/opentext/AppWorksPlatform/devbpm/content/uploadcontent",
            'Properties': {
                MimeType: "application/octet-stream"
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.CREATE_DOCUMENT_NS, _this.CREATE_DOCUMENT, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.downloadEncodedExcelFile = function (userID, categoryID) {
        var _this = this;
        var parameter = {
            userId: userID,
            categoryId: categoryID
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DOWNLOAD_EXCEL_NS, _this.DOWNLOAD_EXCEL_TEMPLATE, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.prototype.getAllCategoryMetadata = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.sharingService.getCategoryMetadata().length > 0) {
                observer.next(_this.sharingService.getCategoryMetadata());
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.GET_ALL_CATEGORY_METADATA_NS, _this.GET_ALL_CATEGORY_METADATA, {})
                    .subscribe(function (response) {
                    _this.sharingService.setCategoryMetadata(acronui.findObjectsByProp(response, 'MPM_Category_Metadata'));
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Category_Metadata'));
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    //  MPMV3-2094
    ProjectService.prototype.deleteProject = function (id, itemId, statusId) {
        var _this = this;
        var parameter = {
            ProjectObject: {
                ProjectId: {
                    Id: id,
                    ItemId: itemId
                },
                RPOStatus: {
                    StatusID: {
                        Id: statusId
                    }
                }
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELETE_PROJECT_NS, _this.DELETE_PROJECT, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    //  MPMV3-2094
    ProjectService.prototype.getTaskByProjectID = function (projectId) {
        var _this = this;
        var parameter = {
            projectID: projectId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.GET_TASK_BY_PROJECT_ID_NS, _this.GET_TASK_BY_PROJECT_ID, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectService.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService },
        { type: UtilService },
        { type: OtmmMetadataService },
        { type: SharingService },
        { type: CommentsUtilService }
    ]; };
    ProjectService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectService_Factory() { return new ProjectService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.UtilService), i0.ɵɵinject(i4.OtmmMetadataService), i0.ɵɵinject(i5.SharingService), i0.ɵɵinject(i6.CommentsUtilService)); }, token: ProjectService, providedIn: "root" });
    ProjectService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ProjectService);
    return ProjectService;
}());
export { ProjectService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxLQUFLLE9BQU8sTUFBTSxpQ0FBaUMsQ0FBQztBQUMzRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRTdFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOzs7Ozs7OztBQVN2RjtJQWlDSSx3QkFDVyxVQUFzQixFQUN0QixXQUF3QixFQUN4QixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsa0JBQXVDO1FBTHZDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQXJDbEQsZUFBVSxHQUFHLGtEQUFrRCxDQUFDO1FBQ2hFLDhDQUF5QyxHQUFHLCtDQUErQyxDQUFDO1FBQzVGLHNCQUFpQixHQUFHLDZEQUE2RCxDQUFDO1FBQ2xGLGdCQUFXLEdBQUcsc0RBQXNELENBQUM7UUFDckUsaUNBQTRCLEdBQUcsZ0VBQWdFLENBQUM7UUFDaEcsc0JBQWlCLEdBQUcsZ0RBQWdELENBQUM7UUFDckUsc0JBQWlCLEdBQUcsc0RBQXNELENBQUM7UUFDM0UsOEJBQXlCLEdBQUcsK0NBQStDLENBQUM7UUFFNUUsNEJBQXVCLEdBQUcsd0JBQXdCLENBQUM7UUFFbkQsbUZBQW1GO1FBQ25GLHdCQUFtQixHQUFHLHFCQUFxQixDQUFDO1FBRTVDLHVCQUFrQixHQUFHLHFEQUFxRCxDQUFDO1FBQzNFLG9CQUFlLEdBQUcsZ0JBQWdCLENBQUM7UUFFbkMsd0JBQW1CLEdBQUcsZ0JBQWdCLENBQUM7UUFDdkMsNEJBQXVCLEdBQUcsc0JBQXNCLENBQUM7UUFDakQsOENBQXlDLEdBQUcsNEJBQTRCLENBQUM7UUFDekUsdUJBQWtCLEdBQUcsYUFBYSxDQUFDO1FBQ25DLHVCQUFrQixHQUFHLHNCQUFzQixDQUFDO1FBQzVDLDhCQUF5QixHQUFHLHdCQUF3QixDQUFDO1FBQ3JELG1CQUFjLEdBQUcsZUFBZSxDQUFDO1FBQ2pDLDJCQUFzQixHQUFHLHFCQUFxQixDQUFDO1FBSXhDLHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELHdCQUFtQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQVN4RCxDQUFDO0lBRUwsa0RBQXlCLEdBQXpCLFVBQTBCLFVBQW1CO1FBQ3pDLElBQUksQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDO0lBQ3RDLENBQUM7SUFFRCxrREFBeUIsR0FBekI7UUFDSSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7SUFDaEMsQ0FBQztJQUVELDhDQUFxQixHQUFyQixVQUFzQixZQUFxQjtRQUN2QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksR0FBRyxFQUFFLElBQUk7UUFDakIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7WUFDaEMsT0FBTztTQUNWO1FBQ0QsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELDBDQUFpQixHQUFqQixVQUFrQixHQUFHLEVBQUUsSUFBSTtRQUN2QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvRSxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7WUFDakIsT0FBTyxPQUFPLENBQUM7U0FDbEI7UUFDRCxPQUFPO0lBQ1gsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxVQUFlO1FBQTNCLGlCQTRDQztRQTNDRyxJQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFFOUUsSUFBSSxVQUFVLENBQUMsbUJBQW1CLElBQUksVUFBVSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0UsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLEdBQUcsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ2xLO1FBRUQsSUFBTSxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUM7UUFDbEMsSUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUM7UUFFakMsSUFBTSxZQUFZLEdBQUcsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsRUFBRSxDQUFDLGlCQUFpQixLQUFLLDBCQUEwQixJQUFJLEVBQUUsQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUE3RSxDQUE2RSxDQUFDLENBQUMsQ0FBQztZQUN2SyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztRQUUvSCxJQUFNLG9CQUFvQixHQUFHLFVBQVUsQ0FBQyxvQkFBb0IsSUFBSSxFQUFFLENBQUM7UUFFbkUsSUFBTSx3QkFBd0IsR0FBRztZQUM3QixTQUFTLEVBQUUsVUFBVTtZQUNyQixlQUFlLEVBQUUsTUFBTTtZQUN2QixxQkFBcUIsRUFBRSxTQUFTO1lBQ2hDLHFCQUFxQixFQUFFLG1CQUFtQjtZQUMxQyxrQkFBa0IsRUFBRSxRQUFRO1lBQzVCLHNCQUFzQixFQUFFLG9CQUFvQjtZQUM1QyxhQUFhLEVBQUUsWUFBWTtZQUMzQixVQUFVLEVBQUUsVUFBVSxDQUFDLFVBQVU7WUFDakMsZ0JBQWdCLEVBQUUsVUFBVSxDQUFDLGdCQUFnQjtTQUNoRCxDQUFDO1FBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDekksU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZCxJQUFJLFFBQVEsQ0FBQyxzQkFBc0IsSUFBSSxRQUFRLENBQUMsc0JBQXNCLENBQUMsYUFBYTt1QkFDN0UsUUFBUSxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxlQUFlLEdBQUcsQ0FBQzt1QkFDakUsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRTtvQkFDL0MsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsa0NBQWtDLENBQ3BHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLCtCQUErQixFQUFFLENBQUMsQ0FBQztvQkFDNUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsa0NBQWtDLENBQ3BHLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLDhCQUE4QixFQUFFLENBQUMsQ0FBQztpQkFDOUc7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxTQUFpQjtRQUFoQyxpQkFlQztRQWRHLElBQU0sU0FBUyxHQUFHO1lBQ2QsWUFBWSxFQUFFO2dCQUNWLEVBQUUsRUFBRSxTQUFTO2FBQ2hCO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLGtCQUFrQixFQUFFLFNBQVMsQ0FBQztpQkFDN0UsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDaEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDBEQUFpQyxHQUFqQyxVQUFrQyxTQUFTO1FBQTNDLGlCQWFDO1FBWkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxXQUFXLEVBQUUsU0FBUztTQUN6QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHlDQUF5QyxFQUFFLEtBQUksQ0FBQyx5Q0FBeUMsRUFBRSxTQUFTLENBQUM7aUJBQ25JLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZDQUFvQixHQUFwQixVQUFxQixTQUFpQjtRQUF0QyxpQkFnQkM7UUFmRyxJQUFNLFNBQVMsR0FBRztZQUNkLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFLDJCQUEyQixFQUFFLFNBQVMsQ0FBQztpQkFDbEYsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDO29CQUNWLFlBQVksRUFBRSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7b0JBQzdKLFVBQVUsRUFBRSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7aUJBQ3hKLENBQUMsQ0FBQztnQkFDSCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQW9CLEdBQXBCLFVBQXFCLGNBQWdDLEVBQUUsYUFBYSxFQUFFLE9BQWE7UUFBbkYsaUJBaUJDO1FBaEJHLElBQU0sU0FBUyxHQUFHO1lBQ2QsYUFBYSxFQUFFLGNBQWM7WUFDN0IsYUFBYSxFQUFFLGFBQWE7WUFDNUIsT0FBTyxFQUFFO2dCQUNMLHNCQUFzQixFQUFFLE9BQU87YUFDbEM7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxhQUFhLEVBQUUsU0FBUyxDQUFDO2lCQUNwRSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCw2Q0FBb0IsR0FBcEIsVUFBcUIsUUFBUTtRQUN6QixJQUFJLFFBQVEsRUFBRTtZQUNWLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3JFO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELDRDQUFtQixHQUFuQixVQUFvQixXQUFtQixFQUFFLFVBQWtCO1FBQTNELGlCQWNDO1FBYkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxXQUFXLEVBQUUsV0FBVztZQUN4QixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsRUFBRSxTQUFTLENBQUM7Z0JBQzdFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxlQUFlLEVBQUUsY0FBYyxFQUFFLFFBQVE7UUFBeEQsaUJBZUM7UUFkRyxJQUFNLFNBQVMsR0FBRztZQUNkLGVBQWUsRUFBRSxlQUFlO1lBQ2hDLGNBQWMsRUFBRSxjQUFjO1lBQzlCLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsV0FBVyxFQUFFLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLENBQUM7Z0JBQy9FLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxZQUFvQixFQUFFLGVBQXVCO1FBQTVELGlCQW1CQztRQWxCRyxJQUFNLFNBQVMsR0FBRztZQUNkLFlBQVksRUFBRSxZQUFZO1lBQzFCLFdBQVcsRUFBRSxtQkFBbUI7WUFDaEMsZUFBZSxFQUFFLGVBQWU7WUFDaEMsTUFBTSxFQUFFLDZEQUE2RDtZQUNyRSxZQUFZLEVBQUU7Z0JBQ1YsUUFBUSxFQUFFLDBCQUEwQjthQUN2QztTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLGVBQWUsRUFBRSxTQUFTLENBQUM7aUJBQ2xGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlEQUF3QixHQUF4QixVQUF5QixNQUFjLEVBQUUsVUFBa0I7UUFBM0QsaUJBZUM7UUFkRyxJQUFNLFNBQVMsR0FBRztZQUNkLE1BQU0sRUFBRSxNQUFNO1lBQ2QsVUFBVSxFQUFFLFVBQVU7U0FFekIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFJLENBQUMsdUJBQXVCLEVBQUUsU0FBUyxDQUFDO2lCQUN6RixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwrQ0FBc0IsR0FBdEI7UUFBQSxpQkFnQkM7UUFmRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDO2dCQUN6RCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLDRCQUE0QixFQUFFLEtBQUksQ0FBQyx5QkFBeUIsRUFBRSxFQUFFLENBQUM7cUJBQy9GLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHVCQUF1QixDQUFDLENBQUMsQ0FBQztvQkFDdEcsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHVCQUF1QixDQUFDLENBQUMsQ0FBQztvQkFDNUUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjO0lBQ2Qsc0NBQWEsR0FBYixVQUFjLEVBQUUsRUFBRSxNQUFNLEVBQUUsUUFBUTtRQUFsQyxpQkF1QkM7UUF0QkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxhQUFhLEVBQUU7Z0JBQ1gsU0FBUyxFQUFFO29CQUNQLEVBQUUsRUFBRSxFQUFFO29CQUNOLE1BQU0sRUFBRSxNQUFNO2lCQUNqQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1AsUUFBUSxFQUFFO3dCQUNOLEVBQUUsRUFBRSxRQUFRO3FCQUNmO2lCQUNKO2FBQ0o7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxjQUFjLEVBQUUsU0FBUyxDQUFDO2lCQUNoRixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjO0lBQ2QsMkNBQWtCLEdBQWxCLFVBQW1CLFNBQVM7UUFBNUIsaUJBYUM7UUFaRyxJQUFNLFNBQVMsR0FBRztZQUNkLFNBQVMsRUFBRSxTQUFTO1NBQ3ZCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMseUJBQXlCLEVBQUUsS0FBSSxDQUFDLHNCQUFzQixFQUFFLFNBQVMsQ0FBQztpQkFDaEcsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkE3UnNCLFVBQVU7Z0JBQ1QsV0FBVztnQkFDWCxXQUFXO2dCQUNILG1CQUFtQjtnQkFDeEIsY0FBYztnQkFDVixtQkFBbUI7OztJQXZDekMsY0FBYztRQUoxQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsY0FBYyxDQWlVMUI7eUJBcFZEO0NBb1ZDLEFBalVELElBaVVDO1NBalVZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IE90bW1NZXRhZGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvb3RtbS1tZXRhZGF0YS5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdCB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1Byb2plY3QnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29tbWVudHMvc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFVwZGF0ZU9iaiB9IGZyb20gJy4uLy4uL3Byb2plY3Qtb3ZlcnZpZXcvcHJvamVjdC51cGRhdGUnO1xyXG5pbXBvcnQgeyBTdGF0dXNMZXZlbCB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBpZGVudGlmaWVyTW9kdWxlVXJsIH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXInO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvamVjdFNlcnZpY2Uge1xyXG5cclxuICAgIFBST0pFQ1RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvUHJvamVjdC9vcGVyYXRpb25zJztcclxuICAgIEdFVF9DT01QTEVURURfTUlMRVNUT05FU19CWV9QUk9KRUNUX0lEX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcbiAgICBQUk9KRUNUX0ZJTFRFUl9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fUHJvamVjdF9GaWx0ZXIvb3BlcmF0aW9ucyc7XHJcbiAgICBQUk9KRUNUX0JQTSA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIEdFVF9BTExfQ0FURUdPUllfTUVUQURBVEFfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0NhdGVnb3J5X01ldGFkYXRhL29wZXJhdGlvbnMnO1xyXG4gICAgRE9XTkxPQURfRVhDRUxfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2V4Y2VsLzEuMCc7XHJcbiAgICBERUxFVEVfUFJPSkVDVF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIEdFVF9UQVNLX0JZX1BST0pFQ1RfSURfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvVGFzay9vcGVyYXRpb25zJztcclxuXHJcbiAgICBET1dOTE9BRF9FWENFTF9URU1QTEFURSA9ICdEb3dubG9hZEV4Y2VsVGVtcGxhdGUgJztcclxuICAgIFxyXG4gICAgLy8gQ1JFQVRFX0JVTEtfUFJPSkVDVF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIENSRUFURV9CVUxLX1BST0pFQ1QgPSAnQnVsa1Byb2plY3RDcmVhdGlvbic7XHJcblxyXG4gICAgQ1JFQVRFX0RPQ1VNRU5UX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmNvcmR5cy5jb20vZG9jdW1lbnRzdG9yZS9kZWZhdWx0LzEuMCc7XHJcbiAgICBDUkVBVEVfRE9DVU1FTlQgPSAnQ3JlYXRlRG9jdW1lbnQnO1xyXG5cclxuICAgIEdFVF9BTExfUFJPSkVDVFNfV1MgPSAnR2V0QWxsUHJvamVjdHMnO1xyXG4gICAgR0VUX0FMTF9QUk9KRUNUX0ZJTFRFUlMgPSAnR2V0QWxsUHJvamVjdEZpbHRlcnMnO1xyXG4gICAgR0VUX0NPTVBMRVRFRF9NSUxFU1RPTkVTX0JZX1BST0pFQ1RfSURfV1MgPSAnR2V0TWlsZXN0b25lVGFza3NCeVByb2plY3QnO1xyXG4gICAgR0VUX1BST0pFQ1RTX0JZX0lEID0gJ1JlYWRQcm9qZWN0JztcclxuICAgIFJFU1RBUlRfUFJPSkVDVF9XUyA9ICdIYW5kbGVSZXN0YXJ0UHJvamVjdCc7XHJcbiAgICBHRVRfQUxMX0NBVEVHT1JZX01FVEFEQVRBID0gJ0dldEFsbENhdGVnb3J5TWV0YWRhdGEnO1xyXG4gICAgREVMRVRFX1BST0pFQ1QgPSAnRGVsZXRlUHJvamVjdCc7XHJcbiAgICBHRVRfVEFTS19CWV9QUk9KRUNUX0lEID0gJ0dldFRhc2tCeVByb2plY3RJRCAnO1xyXG5cclxuICAgIHNlbGVjdGVkUHJvamVjdDogUHJvamVjdDtcclxuXHJcbiAgICBwdWJsaWMgdXBkYXRlUHJvamVjdEluZm8gPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gICAgb25VcGRhdGVQcm9qZWN0SW5mbyA9IHRoaXMudXBkYXRlUHJvamVjdEluZm8uYXNPYnNlcnZhYmxlKCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNvbW1lbnRVdGlsU2VydmljZTogQ29tbWVudHNVdGlsU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBzZXRTZWxlY3RlZFByb2plY3REZXRhaWxzKHByb2plY3RPYmo6IFByb2plY3QpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUHJvamVjdCA9IHByb2plY3RPYmo7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2VsZWN0ZWRQcm9qZWN0RGV0YWlscygpOiBQcm9qZWN0IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZFByb2plY3Q7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VXBkYXRlZFByb2plY3RJbmZvKGN1cnJNZXRhZGF0YTogUHJvamVjdCkge1xyXG4gICAgICAgIHRoaXMudXBkYXRlUHJvamVjdEluZm8ubmV4dChjdXJyTWV0YWRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KG9iaiwgcGF0aCkge1xyXG4gICAgICAgIGlmICghb2JqIHx8ICFwYXRoIHx8ICFvYmoubWV0YWRhdGEpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKG9iai5tZXRhZGF0YSwgcGF0aCwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgY29uc3QgZGF0ZU9iaiA9IHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXRGaWVsZFZhbHVlQnlJZChvYmoubWV0YWRhdGEsIHBhdGgpO1xyXG4gICAgICAgIGlmIChkYXRlT2JqICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGVPYmo7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0cyhwYXJhbWV0ZXJzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaENvbmRpdGlvbkxpc3QgPSBKU09OLnBhcnNlKHBhcmFtZXRlcnMuZGVmYXVsdFNlYXJjaENvbmRpdGlvbkxpc3QpO1xyXG5cclxuICAgICAgICBpZiAocGFyYW1ldGVycy5zZWFyY2hDb25kaXRpb25MaXN0ICYmIHBhcmFtZXRlcnMuc2VhcmNoQ29uZGl0aW9uTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbkxpc3Quc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24gPSBzZWFyY2hDb25kaXRpb25MaXN0LnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uLmNvbmNhdChwYXJhbWV0ZXJzLnNlYXJjaENvbmRpdGlvbkxpc3QpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgc2tpcCA9IHBhcmFtZXRlcnMuc2tpcCB8fCAwO1xyXG4gICAgICAgIGNvbnN0IHRvcCA9IHBhcmFtZXRlcnMudG9wIHx8IDEwO1xyXG5cclxuICAgICAgICBjb25zdCBmb2xkZXJGaWx0ZXIgPSBzZWFyY2hDb25kaXRpb25MaXN0LnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uLnNvbWUoc2MgPT4gc2MubWV0YWRhdGFfZmllbGRfaWQgPT09ICdNUE0uUFJPSkVDVC5QUk9KRUNUX1RZUEUnICYmIHNjLnZhbHVlID09PSAnUFJPSkVDVCcpID9cclxuICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRTZWxlY3RlZENhdGVnb3J5KCkuUFJPSkVDVF9GT0xERVJfSUQgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFNlbGVjdGVkQ2F0ZWdvcnkoKS5URU1QTEFURV9GT0xERVJfSUQ7XHJcblxyXG4gICAgICAgIGNvbnN0IGZhY2V0UmVzdHJpY3Rpb25MaXN0ID0gcGFyYW1ldGVycy5mYWNldFJlc3RyaWN0aW9uTGlzdCB8fCAnJztcclxuXHJcbiAgICAgICAgY29uc3Qgb3RtbVByb2plY3RzU2VhcmNoQ29uZmlnID0ge1xyXG4gICAgICAgICAgICBsb2FkX3R5cGU6ICdtZXRhZGF0YScsXHJcbiAgICAgICAgICAgIGxldmVsX29mX2RldGFpbDogJ3NsaW0nLFxyXG4gICAgICAgICAgICBjaGlsZF9jb3VudF9sb2FkX3R5cGU6ICdmb2xkZXJzJyxcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiBzZWFyY2hDb25kaXRpb25MaXN0LFxyXG4gICAgICAgICAgICBmb2xkZXJfZmlsdGVyX3R5cGU6ICdkaXJlY3QnLFxyXG4gICAgICAgICAgICBmYWNldF9yZXN0cmljdGlvbl9saXN0OiBmYWNldFJlc3RyaWN0aW9uTGlzdCxcclxuICAgICAgICAgICAgZm9sZGVyX2ZpbHRlcjogZm9sZGVyRmlsdGVyLFxyXG4gICAgICAgICAgICBzb3J0U3RyaW5nOiBwYXJhbWV0ZXJzLnNvcnRTdHJpbmcsXHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IHBhcmFtZXRlcnMuc2VhcmNoX2NvbmZpZ19pZCxcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLnNlYXJjaEN1c3RvbVRleHQob3RtbVByb2plY3RzU2VhcmNoQ29uZmlnLCBza2lwLCB0b3AsIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSkuXHJcbiAgICAgICAgICAgICAgICBzdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlICYmIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2Uuc2VhcmNoX3Jlc3VsdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLnNlYXJjaF9yZXN1bHQudG90YWxfaGl0X2NvdW50ID4gMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLmFzc2V0X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZS5hc3NldF9saXN0ID0gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLnNldFJlZmVyZW5jZVZhbHVlRm9yTWV0YWRhdGFGZWlsZHMoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLmFzc2V0X2xpc3QsIHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXREZWZhdWx0UHJvamVjdE1ldGFkYXRhRmllbGRzKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLmFzc2V0X2xpc3QgPSB0aGlzLm90bW1NZXRhZGF0YVNlcnZpY2Uuc2V0UmVmZXJlbmNlVmFsdWVGb3JNZXRhZGF0YUZlaWxkcyhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UuYXNzZXRfbGlzdCwgdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEN1c3RvbVByb2plY3RNZXRhZGF0YUZpZWxkcygpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0QnlJZChwcm9qZWN0SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8UHJvamVjdD4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgJ1Byb2plY3QtaWQnOiB7XHJcbiAgICAgICAgICAgICAgICBJZDogcHJvamVjdElkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9OUywgdGhpcy5HRVRfUFJPSkVDVFNfQllfSUQsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuUHJvamVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb21wbGV0ZWRNaWxlc3RvbmVzQnlQcm9qZWN0SWQocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICdwcm9qZWN0SWQnOiBwcm9qZWN0SWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0NPTVBMRVRFRF9NSUxFU1RPTkVTX0JZX1BST0pFQ1RfSURfTlMsIHRoaXMuR0VUX0NPTVBMRVRFRF9NSUxFU1RPTkVTX0JZX1BST0pFQ1RfSURfV1MsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdERhdGFWYWx1ZXMocHJvamVjdElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgUHJvamVjdElEOiBwcm9qZWN0SWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJPSkVDVF9CUE0sICdFZGl0UHJvamVjdERhdGVWYWxpZGF0aW9uJywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0TWF4RGF0ZTogcmVzcG9uc2UgJiYgcmVzcG9uc2VbJ0RhdGUnXVsnU3RhcnREYXRlJ10gJiYgdHlwZW9mIHJlc3BvbnNlWydEYXRlJ11bJ1N0YXJ0RGF0ZSddID09PSAnc3RyaW5nJyA/IG5ldyBEYXRlKHJlc3BvbnNlWydEYXRlJ11bJ1N0YXJ0RGF0ZSddKSA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuZE1pbkRhdGU6IHJlc3BvbnNlICYmIHJlc3BvbnNlWydEYXRlJ11bJ0VuZERhdGUnXSAmJiB0eXBlb2YgcmVzcG9uc2VbJ0RhdGUnXVsnRW5kRGF0ZSddID09PSAnc3RyaW5nJyA/IG5ldyBEYXRlKHJlc3BvbnNlWydEYXRlJ11bJ0VuZERhdGUnXSkgOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlUHJvamVjdERldGFpbHModXBkYXRlZFByb2plY3Q6IFByb2plY3RVcGRhdGVPYmosIGNhbmNlbENvbW1lbnQsIHJlYXNvbnM/OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgUHJvamVjdE9iamVjdDogdXBkYXRlZFByb2plY3QsXHJcbiAgICAgICAgICAgIENhbmNlbENvbW1lbnQ6IGNhbmNlbENvbW1lbnQsXHJcbiAgICAgICAgICAgIFJlYXNvbnM6IHtcclxuICAgICAgICAgICAgICAgICdNUE1fU3RhdHVzX1JlYXNvbi1pZCc6IHJlYXNvbnNcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0JQTSwgJ0VkaXRQcm9qZWN0JywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGdldEN1cnJQcm9qZWN0U3RhdHVzKHN0YXR1c0lkKTogU3RhdHVzTGV2ZWwge1xyXG4gICAgICAgIGlmIChzdGF0dXNJZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb21tZW50VXRpbFNlcnZpY2UuZ2V0U3RhdHVzTGV2ZWxCeVN0YXR1c0lkKHN0YXR1c0lkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgYnVsa1Byb2plY3RDcmVhdGlvbihkb2N1bWVudFVSTDogc3RyaW5nLCB0ZW1wbGF0ZUlEOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgZG9jdW1lbnRVUkw6IGRvY3VtZW50VVJMLFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZUlEOiB0ZW1wbGF0ZUlELFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0JQTSwgJ0J1bGtQcm9qZWN0Q3JlYXRpb24nLCBwYXJhbWV0ZXIpLlxyXG4gICAgICAgICAgICAgICAgc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc3RhcnRQcm9qZWN0KHByb2plY3RNZXRhZGF0YSwgY3VzdG9tTWV0YWRhdGEsIGNvbW1lbnRzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIFByb2plY3RNZXRhZGF0YTogcHJvamVjdE1ldGFkYXRhLFxyXG4gICAgICAgICAgICBDdXN0b21NZXRhZGF0YTogY3VzdG9tTWV0YWRhdGEsXHJcbiAgICAgICAgICAgIENvbW1lbnRzOiBjb21tZW50c1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0JQTSwgdGhpcy5SRVNUQVJUX1BST0pFQ1RfV1MsIHBhcmFtZXRlcikuXHJcbiAgICAgICAgICAgICAgICBzdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlRG9jdW1lbnQoZG9jdW1lbnROYW1lOiBzdHJpbmcsIGRvY3VtZW50Q29udGVudDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIERvY3VtZW50TmFtZTogZG9jdW1lbnROYW1lLFxyXG4gICAgICAgICAgICBEZXNjdGlwdGlvbjogXCJUZXN0IEZyb20gQW5ndWxhclwiLFxyXG4gICAgICAgICAgICBEb2N1bWVudENvbnRlbnQ6IGRvY3VtZW50Q29udGVudCxcclxuICAgICAgICAgICAgRm9sZGVyOiBcIi9vcHQvb3BlbnRleHQvQXBwV29ya3NQbGF0Zm9ybS9kZXZicG0vY29udGVudC91cGxvYWRjb250ZW50XCIsXHJcbiAgICAgICAgICAgICdQcm9wZXJ0aWVzJzoge1xyXG4gICAgICAgICAgICAgICAgTWltZVR5cGU6IFwiYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtXCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DUkVBVEVfRE9DVU1FTlRfTlMsIHRoaXMuQ1JFQVRFX0RPQ1VNRU5ULCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGRvd25sb2FkRW5jb2RlZEV4Y2VsRmlsZSh1c2VySUQ6IHN0cmluZywgY2F0ZWdvcnlJRDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIHVzZXJJZDogdXNlcklELFxyXG4gICAgICAgICAgICBjYXRlZ29yeUlkOiBjYXRlZ29yeUlEXHJcblxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ET1dOTE9BRF9FWENFTF9OUywgdGhpcy5ET1dOTE9BRF9FWENFTF9URU1QTEFURSwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxDYXRlZ29yeU1ldGFkYXRhKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q2F0ZWdvcnlNZXRhZGF0YSgpLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5zaGFyaW5nU2VydmljZS5nZXRDYXRlZ29yeU1ldGFkYXRhKCkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9DQVRFR09SWV9NRVRBREFUQV9OUywgdGhpcy5HRVRfQUxMX0NBVEVHT1JZX01FVEFEQVRBLCB7fSlcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDYXRlZ29yeU1ldGFkYXRhKGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fQ2F0ZWdvcnlfTWV0YWRhdGEnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9DYXRlZ29yeV9NZXRhZGF0YScpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vICBNUE1WMy0yMDk0XHJcbiAgICBkZWxldGVQcm9qZWN0KGlkLCBpdGVtSWQsIHN0YXR1c0lkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIFByb2plY3RPYmplY3Q6IHtcclxuICAgICAgICAgICAgICAgIFByb2plY3RJZDoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBpZCxcclxuICAgICAgICAgICAgICAgICAgICBJdGVtSWQ6IGl0ZW1JZFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIFJQT1N0YXR1czoge1xyXG4gICAgICAgICAgICAgICAgICAgIFN0YXR1c0lEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiBzdGF0dXNJZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfUFJPSkVDVF9OUywgdGhpcy5ERUxFVEVfUFJPSkVDVCwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgZ2V0VGFza0J5UHJvamVjdElEKHByb2plY3RJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBwcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfVEFTS19CWV9QUk9KRUNUX0lEX05TLCB0aGlzLkdFVF9UQVNLX0JZX1BST0pFQ1RfSUQsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==