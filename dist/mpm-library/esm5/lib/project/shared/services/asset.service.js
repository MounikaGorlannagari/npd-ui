import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { IndexerService } from '../../../shared/services/indexer/indexer.service';
import { QdsService } from '../../../upload/services/qds.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/otmm.service";
import * as i3 from "../../../shared/services/indexer/indexer.service";
import * as i4 from "../../../upload/services/qds.service";
var AssetService = /** @class */ (function () {
    function AssetService(appService, otmmService, indexerService, qdsService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.indexerService = indexerService;
        this.qdsService = qdsService;
        this.PROJECT_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
        this.CAMPAIGN_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';
        this.GET_CAMPAIGN_DETAILS_WS_METHOD_NAME = 'GetCampaignById';
        this.OTMM_METHOD_NS = 'http://schemas.acheron.com/mpm/otmm/bpm/1.0';
        this.FORM_METADATA_WS_METHOD_NAME = 'FormMetadata';
        this.GET_CR_IS_VIEWER_NS = 'http://schemas/AcheronMPMCore/MPM_CR_Role/operations';
        this.GET_CR_IS_VIEWER_WS_METHOD_NAME = 'GetCRIsViewer';
    }
    AssetService.prototype.getProjectDetails = function (projectId) {
        var _this = this;
        var getRequestObject = {
            projectID: projectId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_METHOD_NS, _this.GET_PROJECT_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetService.prototype.getCampaignDetails = function (campaignId) {
        var _this = this;
        var getRequestObject = {
            CampaignId: campaignId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.CAMPAIGN_METHOD_NS, _this.GET_CAMPAIGN_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetService.prototype.getMetadataFields = function (referenceId) {
        var _this = this;
        var getRequestObject = {
            eventReferenceID: referenceId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.OTMM_METHOD_NS, _this.FORM_METADATA_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.APIResponse && response.APIResponse.data && response.APIResponse.data.GetR_PM_FIELDSResponse) {
                    observer.next(response.APIResponse.data.GetR_PM_FIELDSResponse);
                    observer.complete();
                }
                else {
                    observer.error('Unable to get metadata fields');
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetService.prototype.getAssetDetails = function (searchCondition) {
        var _this = this;
        var parametersReq = {
            search_config_id: null,
            keyword: '',
            search_condition_list: searchCondition.search_condition_list,
            facet_condition_list: {
                facet_condition: []
            },
            sorting_list: {
                sort: [{
                        field_id: 'CONTENT_TYPE',
                        order: 'ASC'
                    }]
            },
            cursor: {
                page_index: 0,
                page_size: 100
            }
        };
        return new Observable(function (observer) {
            _this.indexerService.search(parametersReq).subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error('Something went wrong while getting projects.');
            });
        });
    };
    AssetService.prototype.getAssets = function (searchCondition, folderId, skip, top, userSessionId) {
        var _this = this;
        if (folderId) {
            var otmmAssetSearchConfig_1 = {
                search_condition_list: searchCondition,
                load_type: 'full',
                keyword_query: null,
                metadata_to_return: null,
                sortString: null,
                searchKey: null,
                child_count_load_type: 'folders',
                level_of_detail: 'slim',
                folder_filter: folderId,
                load_multilingual_values: 'true',
                multilingual_language_code: 'en_US'
            };
            return new Observable(function (observer) {
                _this.otmmService.searchCustomText(otmmAssetSearchConfig_1, skip, top, userSessionId).
                    subscribe(function (res) {
                    observer.next(res);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            });
        }
    };
    AssetService.prototype.initiateDownload = function (fileURL, fileName) {
        // for non-IE
        if (!window['ActiveXObject']) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', fileURL, true);
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.withCredentials = true;
            xhr.responseType = 'blob';
            xhr.onload = function () {
                var urlCreator = window.URL || window['webkitURL'];
                var imageUrl = urlCreator.createObjectURL(this.response);
                var tag = document.createElement('a');
                tag.href = imageUrl;
                tag.download = fileName;
                document.body.appendChild(tag);
                tag.click();
                document.body.removeChild(tag);
            };
            xhr.send();
        }
        else if (window['ActiveXObject'] && document.execCommand) {
            var customWindow = window.open(fileURL, '_blank');
            customWindow.document.close();
            customWindow.document.execCommand('SaveAs', true, fileName || fileURL);
            customWindow.close();
        }
    };
    AssetService.prototype.initQDSDownload = function (downloadParams) {
        this.qdsService.inlineDownload(downloadParams);
    };
    AssetService.prototype.findIconByName = function (fileName) {
        var unknownIcon = 'insert_drive_file';
        var documentIcon = 'description';
        var imageIcon = 'image';
        var videoIcon = 'movie_creation';
        var pdfIcon = 'picture_as_pdf';
        var gifIcon = 'gif';
        var zipIcon = 'archive';
        var audioIcon = 'graphic_eq';
        if (!fileName || fileName != null && fileName !== '') {
            var fileRegx = /(?:\.([^.]+))?$/;
            var fileExt = fileRegx.exec(fileName)[1];
            if (fileExt && fileExt != null && fileExt !== '') {
                fileExt = fileExt.toUpperCase();
            }
            if (fileExt === 'PDF') {
                return pdfIcon;
            }
            else if (fileExt === 'DOC' || fileExt === 'DOCX') {
                return documentIcon;
            }
            else if (fileExt === 'XLS' || fileExt === 'XLSX') {
                return documentIcon;
            }
            else if (fileExt === 'JPG' || fileExt === 'JPEG' || fileExt === 'PNG') {
                return imageIcon;
            }
            else if (fileExt === 'GIF') {
                return gifIcon;
            }
            else if (fileExt === 'MP4' || fileExt === 'AVI' || fileExt === 'MKV'
                || fileExt === 'FLV' || fileExt === 'WMV' || fileExt === 'MPEG' || fileExt === '3GP') {
                return videoIcon;
            }
            else if (fileExt === 'TXT') {
                return documentIcon;
            }
            else if (fileExt === 'MP3' || fileExt === 'AAC' || fileExt === 'FLAC'
                || fileExt === 'M4A' || fileExt === 'WAV' || fileExt === 'WMA') {
                return videoIcon;
            }
            else if (fileExt === 'EPS') {
                return documentIcon;
            }
            else if (fileExt === 'ZIP') {
                return zipIcon;
            }
            else if (fileExt === 'SND' || fileExt === 'AIF' || fileExt === 'AIFF') {
                return audioIcon;
            }
            else {
                return unknownIcon;
            }
        }
        else {
            return unknownIcon;
        }
    };
    AssetService.prototype.getfileFormatObject = function (filename) {
        var fileFormatObject;
        if (filename != null && filename !== '') {
            var fileRegx = /(?:\.([^.]+))?$/;
            var fileExt = fileRegx.exec(filename)[1];
            if (fileExt != null && fileExt !== '') {
                fileExt = fileExt.toUpperCase();
            }
            if (fileExt === 'PDF') {
                fileFormatObject = 'Acrobat';
                return fileFormatObject;
            }
            else if (fileExt === 'DOC' || fileExt === 'DOCX' || fileExt === 'XLS' || fileExt === 'XLSX' ||
                fileExt === 'PPT' || fileExt === 'PPTX') {
                fileFormatObject = 'Microsoft Office';
                return fileFormatObject;
            }
            else if (fileExt === 'JPG' || fileExt === 'JPEG' || fileExt === 'JPE' || fileExt === 'TIFF' ||
                fileExt === 'TIF' || fileExt === 'EPS' || fileExt === 'AI' || fileExt === 'PSD') {
                fileFormatObject = 'Image';
                return fileFormatObject;
            }
            else if (fileExt === 'GIF') {
                fileFormatObject = 'GIF';
                return fileFormatObject;
            }
            else if (fileExt === 'PNG') {
                fileFormatObject = 'PNG';
                return fileFormatObject;
            }
            else if (fileExt === 'BMP') {
                fileFormatObject = 'BMP';
                return fileFormatObject;
            }
            else if (fileExt === 'MP4' || fileExt === 'AVI' || fileExt === 'MKV' || fileExt === 'FLV' ||
                fileExt === 'WMV' || fileExt === 'MPEG' || fileExt === '3GP' || fileExt === 'MOV') {
                fileFormatObject = 'VIDEO';
                return fileFormatObject;
            }
            else if (fileExt === 'TXT') {
                fileFormatObject = 'TXT';
                return fileFormatObject;
            }
            else if (fileExt === 'EML') {
                fileFormatObject = 'EML';
                return fileFormatObject;
            }
            else if (fileExt === 'RTX') {
                fileFormatObject = 'RTX';
                return fileFormatObject;
            }
            else if (fileExt === 'MDB') {
                fileFormatObject = 'MDB';
                return fileFormatObject;
            }
            else if (fileExt === 'ZIP') {
                fileFormatObject = 'ZIP';
                return fileFormatObject;
            }
            else if (fileExt === 'SND' || fileExt === 'AIF' || fileExt === 'AIFF') {
                fileFormatObject = 'AUDIO';
                return fileFormatObject;
            }
            else {
                fileFormatObject = 'Unsupported Format';
                return fileFormatObject;
            }
        }
    };
    ;
    AssetService.prototype.GetCRIsViewer = function (isViewer) {
        var _this = this;
        var getRequestObject = {
            IsViewer: isViewer
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.GET_CR_IS_VIEWER_NS, _this.GET_CR_IS_VIEWER_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AssetService.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService },
        { type: IndexerService },
        { type: QdsService }
    ]; };
    AssetService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AssetService_Factory() { return new AssetService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.IndexerService), i0.ɵɵinject(i4.QdsService)); }, token: AssetService, providedIn: "root" });
    AssetService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AssetService);
    return AssetService;
}());
export { AssetService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL2Fzc2V0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUdsRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7Ozs7OztBQUtsRTtJQUVJLHNCQUNXLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLFVBQXNCO1FBSHRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFHakMsc0JBQWlCLEdBQUcsa0RBQWtELENBQUM7UUFDdkUsdUNBQWtDLEdBQUcsZ0JBQWdCLENBQUM7UUFDdEQsdUJBQWtCLEdBQUcsbURBQW1ELENBQUM7UUFDekUsd0NBQW1DLEdBQUcsaUJBQWlCLENBQUM7UUFFeEQsbUJBQWMsR0FBRyw2Q0FBNkMsQ0FBQztRQUMvRCxpQ0FBNEIsR0FBRyxjQUFjLENBQUM7UUFFOUMsd0JBQW1CLEdBQUcsc0RBQXNELENBQUM7UUFDN0Usb0NBQStCLEdBQUcsZUFBZSxDQUFDO0lBWDlDLENBQUM7SUFhTCx3Q0FBaUIsR0FBakIsVUFBa0IsU0FBUztRQUEzQixpQkFhQztRQVpHLElBQU0sZ0JBQWdCLEdBQUc7WUFDckIsU0FBUyxFQUFFLFNBQVM7U0FDdkIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFJLENBQUMsa0NBQWtDLEVBQUUsZ0JBQWdCLENBQUM7aUJBQzNHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFrQixHQUFsQixVQUFtQixVQUFVO1FBQTdCLGlCQWFDO1FBWkcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLEtBQUksQ0FBQyxtQ0FBbUMsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDN0csU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0NBQWlCLEdBQWpCLFVBQWtCLFdBQVc7UUFBN0IsaUJBaUJDO1FBaEJHLElBQU0sZ0JBQWdCLEdBQUc7WUFDckIsZ0JBQWdCLEVBQUUsV0FBVztTQUNoQyxDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxLQUFJLENBQUMsNEJBQTRCLEVBQUUsZ0JBQWdCLENBQUM7aUJBQ2xHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtvQkFDbkgsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUNoRSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztpQkFDbkQ7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxzQ0FBZSxHQUFmLFVBQWdCLGVBQWU7UUFBL0IsaUJBMkJDO1FBMUJHLElBQU0sYUFBYSxHQUFrQjtZQUNqQyxnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxFQUFFO1lBQ1gscUJBQXFCLEVBQUUsZUFBZSxDQUFDLHFCQUFxQjtZQUM1RCxvQkFBb0IsRUFBRTtnQkFDbEIsZUFBZSxFQUFFLEVBQUU7YUFDdEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsSUFBSSxFQUFFLENBQUM7d0JBQ0gsUUFBUSxFQUFFLGNBQWM7d0JBQ3hCLEtBQUssRUFBRSxLQUFLO3FCQUNmLENBQUM7YUFDTDtZQUNELE1BQU0sRUFBRTtnQkFDSixVQUFVLEVBQUUsQ0FBQztnQkFDYixTQUFTLEVBQUUsR0FBRzthQUNqQjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxRQUF3QjtnQkFDekUsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO1lBQ25FLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZ0NBQVMsR0FBVCxVQUFVLGVBQWUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxhQUFhO1FBQTdELGlCQTBCQztRQXpCRyxJQUFJLFFBQVEsRUFBRTtZQUNWLElBQU0sdUJBQXFCLEdBQUc7Z0JBQzFCLHFCQUFxQixFQUFFLGVBQWU7Z0JBQ3RDLFNBQVMsRUFBRSxNQUFNO2dCQUNqQixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsa0JBQWtCLEVBQUUsSUFBSTtnQkFDeEIsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLFNBQVMsRUFBRSxJQUFJO2dCQUNmLHFCQUFxQixFQUFFLFNBQVM7Z0JBQ2hDLGVBQWUsRUFBRSxNQUFNO2dCQUN2QixhQUFhLEVBQUUsUUFBUTtnQkFDdkIsd0JBQXdCLEVBQUUsTUFBTTtnQkFDaEMsMEJBQTBCLEVBQUUsT0FBTzthQUN0QyxDQUFDO1lBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7Z0JBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsdUJBQXFCLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxhQUFhLENBQUM7b0JBQzlFLFNBQVMsQ0FBQyxVQUFBLEdBQUc7b0JBQ1QsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDbkIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCx1Q0FBZ0IsR0FBaEIsVUFBaUIsT0FBTyxFQUFFLFFBQVE7UUFDOUIsYUFBYTtRQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDMUIsSUFBTSxHQUFHLEdBQUcsSUFBSSxjQUFjLEVBQUUsQ0FBQztZQUNqQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDL0IsR0FBRyxDQUFDLGdCQUFnQixDQUFDLDZCQUE2QixFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ3pELEdBQUcsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1lBQzNCLEdBQUcsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDO1lBQzFCLEdBQUcsQ0FBQyxNQUFNLEdBQUc7Z0JBQ1QsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLEdBQUcsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JELElBQU0sUUFBUSxHQUFHLFVBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMzRCxJQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN4QyxHQUFHLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQztnQkFDcEIsR0FBRyxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMvQixHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ1osUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkMsQ0FBQyxDQUFDO1lBQ0YsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2Q7YUFBTSxJQUFJLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO1lBQ3hELElBQU0sWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3BELFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDOUIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxRQUFRLElBQUksT0FBTyxDQUFDLENBQUM7WUFDdkUsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELHNDQUFlLEdBQWYsVUFBZ0IsY0FBYztRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQscUNBQWMsR0FBZCxVQUFlLFFBQVE7UUFDbkIsSUFBTSxXQUFXLEdBQUcsbUJBQW1CLENBQUM7UUFDeEMsSUFBTSxZQUFZLEdBQUcsYUFBYSxDQUFDO1FBQ25DLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQztRQUMxQixJQUFNLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztRQUNuQyxJQUFNLE9BQU8sR0FBRyxnQkFBZ0IsQ0FBQztRQUNqQyxJQUFNLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDO1FBQzFCLElBQU0sU0FBUyxHQUFHLFlBQVksQ0FBQztRQUUvQixJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsSUFBSSxJQUFJLElBQUksUUFBUSxLQUFLLEVBQUUsRUFBRTtZQUNsRCxJQUFNLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQztZQUNuQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXpDLElBQUksT0FBTyxJQUFJLE9BQU8sSUFBSSxJQUFJLElBQUksT0FBTyxLQUFLLEVBQUUsRUFBRTtnQkFDOUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNuQztZQUVELElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDbkIsT0FBTyxPQUFPLENBQUM7YUFDbEI7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLEVBQUU7Z0JBQ2hELE9BQU8sWUFBWSxDQUFDO2FBQ3ZCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxFQUFFO2dCQUNoRCxPQUFPLFlBQVksQ0FBQzthQUN2QjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNyRSxPQUFPLFNBQVMsQ0FBQzthQUNwQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLE9BQU8sT0FBTyxDQUFDO2FBQ2xCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLO21CQUMvRCxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUN0RixPQUFPLFNBQVMsQ0FBQzthQUNwQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLE9BQU8sWUFBWSxDQUFDO2FBQ3ZCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNO21CQUNoRSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDaEUsT0FBTyxTQUFTLENBQUM7YUFDcEI7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUMxQixPQUFPLFlBQVksQ0FBQzthQUN2QjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLE9BQU8sT0FBTyxDQUFDO2FBQ2xCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLEVBQUU7Z0JBQ3JFLE9BQU8sU0FBUyxDQUFDO2FBQ3BCO2lCQUFNO2dCQUNILE9BQU8sV0FBVyxDQUFDO2FBQ3RCO1NBQ0o7YUFBTTtZQUNILE9BQU8sV0FBVyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQztJQUVELDBDQUFtQixHQUFuQixVQUFvQixRQUFRO1FBQ3hCLElBQUksZ0JBQWdCLENBQUM7UUFDckIsSUFBSSxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsS0FBSyxFQUFFLEVBQUU7WUFDckMsSUFBTSxRQUFRLEdBQUcsaUJBQWlCLENBQUM7WUFDbkMsSUFBSSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV6QyxJQUFJLE9BQU8sSUFBSSxJQUFJLElBQUksT0FBTyxLQUFLLEVBQUUsRUFBRTtnQkFDbkMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNuQztZQUVELElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDbkIsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO2dCQUM3QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU07Z0JBQ3pGLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sRUFBRTtnQkFDekMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUM7Z0JBQ3RDLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxNQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTTtnQkFDekYsT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxJQUFJLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDakYsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDO2dCQUMzQixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDMUIsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLGdCQUFnQixDQUFDO2FBQzNCO2lCQUFNLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUs7Z0JBQ3ZGLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLE1BQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQ25GLGdCQUFnQixHQUFHLE9BQU8sQ0FBQztnQkFDM0IsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQzFCLGdCQUFnQixHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxnQkFBZ0IsQ0FBQzthQUMzQjtpQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLEtBQUssTUFBTSxFQUFFO2dCQUNyRSxnQkFBZ0IsR0FBRyxPQUFPLENBQUM7Z0JBQzNCLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0gsZ0JBQWdCLEdBQUcsb0JBQW9CLENBQUM7Z0JBQ3hDLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7U0FDSjtJQUNMLENBQUM7SUFBQSxDQUFDO0lBRUYsb0NBQWEsR0FBYixVQUFjLFFBQVE7UUFBdEIsaUJBYUM7UUFaRyxJQUFNLGdCQUFnQixHQUFHO1lBQ3JCLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLEVBQUUsS0FBSSxDQUFDLCtCQUErQixFQUFFLGdCQUFnQixDQUFDO2lCQUMxRyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXJSc0IsVUFBVTtnQkFDVCxXQUFXO2dCQUNSLGNBQWM7Z0JBQ2xCLFVBQVU7OztJQU54QixZQUFZO1FBSHhCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FDVyxZQUFZLENBeVJ4Qjt1QkFyU0Q7Q0FxU0MsQUF6UkQsSUF5UkM7U0F6UlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kZXhlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9pbmRleGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXF1ZXN0IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9TZWFyY2hSZXF1ZXN0JztcclxuaW1wb3J0IHsgU2VhcmNoUmVzcG9uc2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlc3BvbnNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEFzc2V0U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgaW5kZXhlclNlcnZpY2U6IEluZGV4ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIFBST0pFQ1RfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL1Byb2plY3Qvb3BlcmF0aW9ucyc7XHJcbiAgICBHRVRfUFJPSkVDVF9ERVRBSUxTX1dTX01FVEhPRF9OQU1FID0gJ0dldFByb2plY3RCeUlEJztcclxuICAgIENBTVBBSUdOX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9DYW1wYWlnbi9vcGVyYXRpb25zJztcclxuICAgIEdFVF9DQU1QQUlHTl9ERVRBSUxTX1dTX01FVEhPRF9OQU1FID0gJ0dldENhbXBhaWduQnlJZCc7XHJcblxyXG4gICAgT1RNTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL290bW0vYnBtLzEuMCc7XHJcbiAgICBGT1JNX01FVEFEQVRBX1dTX01FVEhPRF9OQU1FID0gJ0Zvcm1NZXRhZGF0YSc7XHJcblxyXG4gICAgR0VUX0NSX0lTX1ZJRVdFUl9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQ1JfUm9sZS9vcGVyYXRpb25zJztcclxuICAgIEdFVF9DUl9JU19WSUVXRVJfV1NfTUVUSE9EX05BTUUgPSAnR2V0Q1JJc1ZpZXdlcic7XHJcblxyXG4gICAgZ2V0UHJvamVjdERldGFpbHMocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICBwcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX01FVEhPRF9OUywgdGhpcy5HRVRfUFJPSkVDVF9ERVRBSUxTX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDYW1wYWlnbkRldGFpbHMoY2FtcGFpZ25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgQ2FtcGFpZ25JZDogY2FtcGFpZ25JZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5DQU1QQUlHTl9NRVRIT0RfTlMsIHRoaXMuR0VUX0NBTVBBSUdOX0RFVEFJTFNfV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1ldGFkYXRhRmllbGRzKHJlZmVyZW5jZUlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICBldmVudFJlZmVyZW5jZUlEOiByZWZlcmVuY2VJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5PVE1NX01FVEhPRF9OUywgdGhpcy5GT1JNX01FVEFEQVRBX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5HZXRSX1BNX0ZJRUxEU1Jlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5HZXRSX1BNX0ZJRUxEU1Jlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignVW5hYmxlIHRvIGdldCBtZXRhZGF0YSBmaWVsZHMnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldEFzc2V0RGV0YWlscyhzZWFyY2hDb25kaXRpb24pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnNSZXE6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgIHNlYXJjaF9jb25maWdfaWQ6IG51bGwsXHJcbiAgICAgICAgICAgIGtleXdvcmQ6ICcnLFxyXG4gICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHNlYXJjaENvbmRpdGlvbi5zZWFyY2hfY29uZGl0aW9uX2xpc3QsXHJcbiAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgc29ydDogW3tcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogJ0NPTlRFTlRfVFlQRScsXHJcbiAgICAgICAgICAgICAgICAgICAgb3JkZXI6ICdBU0MnXHJcbiAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICAgICAgICBwYWdlX3NpemU6IDEwMFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmluZGV4ZXJTZXJ2aWNlLnNlYXJjaChwYXJhbWV0ZXJzUmVxKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBwcm9qZWN0cy4nKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXNzZXRzKHNlYXJjaENvbmRpdGlvbiwgZm9sZGVySWQsIHNraXAsIHRvcCwgdXNlclNlc3Npb25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgaWYgKGZvbGRlcklkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG90bW1Bc3NldFNlYXJjaENvbmZpZyA9IHtcclxuICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDogc2VhcmNoQ29uZGl0aW9uLFxyXG4gICAgICAgICAgICAgICAgbG9hZF90eXBlOiAnZnVsbCcsXHJcbiAgICAgICAgICAgICAgICBrZXl3b3JkX3F1ZXJ5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFfdG9fcmV0dXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgc29ydFN0cmluZzogbnVsbCxcclxuICAgICAgICAgICAgICAgIHNlYXJjaEtleTogbnVsbCxcclxuICAgICAgICAgICAgICAgIGNoaWxkX2NvdW50X2xvYWRfdHlwZTogJ2ZvbGRlcnMnLFxyXG4gICAgICAgICAgICAgICAgbGV2ZWxfb2ZfZGV0YWlsOiAnc2xpbScsXHJcbiAgICAgICAgICAgICAgICBmb2xkZXJfZmlsdGVyOiBmb2xkZXJJZCxcclxuICAgICAgICAgICAgICAgIGxvYWRfbXVsdGlsaW5ndWFsX3ZhbHVlczogJ3RydWUnLFxyXG4gICAgICAgICAgICAgICAgbXVsdGlsaW5ndWFsX2xhbmd1YWdlX2NvZGU6ICdlbl9VUydcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLnNlYXJjaEN1c3RvbVRleHQob3RtbUFzc2V0U2VhcmNoQ29uZmlnLCBza2lwLCB0b3AsIHVzZXJTZXNzaW9uSWQpLlxyXG4gICAgICAgICAgICAgICAgICAgIHN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWF0ZURvd25sb2FkKGZpbGVVUkwsIGZpbGVOYW1lKSB7XHJcbiAgICAgICAgLy8gZm9yIG5vbi1JRVxyXG4gICAgICAgIGlmICghd2luZG93WydBY3RpdmVYT2JqZWN0J10pIHtcclxuICAgICAgICAgICAgY29uc3QgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICAgICAgICAgIHhoci5vcGVuKCdHRVQnLCBmaWxlVVJMLCB0cnVlKTtcclxuICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoJ0FjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpbicsICcqJyk7XHJcbiAgICAgICAgICAgIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xyXG4gICAgICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gJ2Jsb2InO1xyXG4gICAgICAgICAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXJsQ3JlYXRvciA9IHdpbmRvdy5VUkwgfHwgd2luZG93Wyd3ZWJraXRVUkwnXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGltYWdlVXJsID0gdXJsQ3JlYXRvci5jcmVhdGVPYmplY3RVUkwodGhpcy5yZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XHJcbiAgICAgICAgICAgICAgICB0YWcuaHJlZiA9IGltYWdlVXJsO1xyXG4gICAgICAgICAgICAgICAgdGFnLmRvd25sb2FkID0gZmlsZU5hbWU7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRhZyk7XHJcbiAgICAgICAgICAgICAgICB0YWcuY2xpY2soKTtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGFnKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgeGhyLnNlbmQoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHdpbmRvd1snQWN0aXZlWE9iamVjdCddICYmIGRvY3VtZW50LmV4ZWNDb21tYW5kKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1c3RvbVdpbmRvdyA9IHdpbmRvdy5vcGVuKGZpbGVVUkwsICdfYmxhbmsnKTtcclxuICAgICAgICAgICAgY3VzdG9tV2luZG93LmRvY3VtZW50LmNsb3NlKCk7XHJcbiAgICAgICAgICAgIGN1c3RvbVdpbmRvdy5kb2N1bWVudC5leGVjQ29tbWFuZCgnU2F2ZUFzJywgdHJ1ZSwgZmlsZU5hbWUgfHwgZmlsZVVSTCk7XHJcbiAgICAgICAgICAgIGN1c3RvbVdpbmRvdy5jbG9zZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0UURTRG93bmxvYWQoZG93bmxvYWRQYXJhbXMpIHtcclxuICAgICAgICB0aGlzLnFkc1NlcnZpY2UuaW5saW5lRG93bmxvYWQoZG93bmxvYWRQYXJhbXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGZpbmRJY29uQnlOYW1lKGZpbGVOYW1lKSB7XHJcbiAgICAgICAgY29uc3QgdW5rbm93bkljb24gPSAnaW5zZXJ0X2RyaXZlX2ZpbGUnO1xyXG4gICAgICAgIGNvbnN0IGRvY3VtZW50SWNvbiA9ICdkZXNjcmlwdGlvbic7XHJcbiAgICAgICAgY29uc3QgaW1hZ2VJY29uID0gJ2ltYWdlJztcclxuICAgICAgICBjb25zdCB2aWRlb0ljb24gPSAnbW92aWVfY3JlYXRpb24nO1xyXG4gICAgICAgIGNvbnN0IHBkZkljb24gPSAncGljdHVyZV9hc19wZGYnO1xyXG4gICAgICAgIGNvbnN0IGdpZkljb24gPSAnZ2lmJztcclxuICAgICAgICBjb25zdCB6aXBJY29uID0gJ2FyY2hpdmUnO1xyXG4gICAgICAgIGNvbnN0IGF1ZGlvSWNvbiA9ICdncmFwaGljX2VxJztcclxuXHJcbiAgICAgICAgaWYgKCFmaWxlTmFtZSB8fCBmaWxlTmFtZSAhPSBudWxsICYmIGZpbGVOYW1lICE9PSAnJykge1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlUmVneCA9IC8oPzpcXC4oW14uXSspKT8kLztcclxuICAgICAgICAgICAgbGV0IGZpbGVFeHQgPSBmaWxlUmVneC5leGVjKGZpbGVOYW1lKVsxXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChmaWxlRXh0ICYmIGZpbGVFeHQgIT0gbnVsbCAmJiBmaWxlRXh0ICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUV4dCA9IGZpbGVFeHQudG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpbGVFeHQgPT09ICdQREYnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcGRmSWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnRE9DJyB8fCBmaWxlRXh0ID09PSAnRE9DWCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudEljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1hMUycgfHwgZmlsZUV4dCA9PT0gJ1hMU1gnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnRJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdKUEcnIHx8IGZpbGVFeHQgPT09ICdKUEVHJyB8fCBmaWxlRXh0ID09PSAnUE5HJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGltYWdlSWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnR0lGJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGdpZkljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ01QNCcgfHwgZmlsZUV4dCA9PT0gJ0FWSScgfHwgZmlsZUV4dCA9PT0gJ01LVidcclxuICAgICAgICAgICAgICAgIHx8IGZpbGVFeHQgPT09ICdGTFYnIHx8IGZpbGVFeHQgPT09ICdXTVYnIHx8IGZpbGVFeHQgPT09ICdNUEVHJyB8fCBmaWxlRXh0ID09PSAnM0dQJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZGVvSWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnVFhUJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50SWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnTVAzJyB8fCBmaWxlRXh0ID09PSAnQUFDJyB8fCBmaWxlRXh0ID09PSAnRkxBQydcclxuICAgICAgICAgICAgICAgIHx8IGZpbGVFeHQgPT09ICdNNEEnIHx8IGZpbGVFeHQgPT09ICdXQVYnIHx8IGZpbGVFeHQgPT09ICdXTUEnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmlkZW9JY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdFUFMnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnRJY29uO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdaSVAnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gemlwSWNvbjtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnU05EJyB8fCBmaWxlRXh0ID09PSAnQUlGJyB8fCBmaWxlRXh0ID09PSAnQUlGRicpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhdWRpb0ljb247XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdW5rbm93bkljb247XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdW5rbm93bkljb247XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldGZpbGVGb3JtYXRPYmplY3QoZmlsZW5hbWUpIHtcclxuICAgICAgICBsZXQgZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICBpZiAoZmlsZW5hbWUgIT0gbnVsbCAmJiBmaWxlbmFtZSAhPT0gJycpIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsZVJlZ3ggPSAvKD86XFwuKFteLl0rKSk/JC87XHJcbiAgICAgICAgICAgIGxldCBmaWxlRXh0ID0gZmlsZVJlZ3guZXhlYyhmaWxlbmFtZSlbMV07XHJcblxyXG4gICAgICAgICAgICBpZiAoZmlsZUV4dCAhPSBudWxsICYmIGZpbGVFeHQgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRXh0ID0gZmlsZUV4dC50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmlsZUV4dCA9PT0gJ1BERicpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnQWNyb2JhdCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnRE9DJyB8fCBmaWxlRXh0ID09PSAnRE9DWCcgfHwgZmlsZUV4dCA9PT0gJ1hMUycgfHwgZmlsZUV4dCA9PT0gJ1hMU1gnIHx8XHJcbiAgICAgICAgICAgICAgICBmaWxlRXh0ID09PSAnUFBUJyB8fCBmaWxlRXh0ID09PSAnUFBUWCcpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnTWljcm9zb2Z0IE9mZmljZSc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnSlBHJyB8fCBmaWxlRXh0ID09PSAnSlBFRycgfHwgZmlsZUV4dCA9PT0gJ0pQRScgfHwgZmlsZUV4dCA9PT0gJ1RJRkYnIHx8XHJcbiAgICAgICAgICAgICAgICBmaWxlRXh0ID09PSAnVElGJyB8fCBmaWxlRXh0ID09PSAnRVBTJyB8fCBmaWxlRXh0ID09PSAnQUknIHx8IGZpbGVFeHQgPT09ICdQU0QnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0ltYWdlJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdHSUYnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0dJRic7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnUE5HJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdQTkcnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ0JNUCcpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnQk1QJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdNUDQnIHx8IGZpbGVFeHQgPT09ICdBVkknIHx8IGZpbGVFeHQgPT09ICdNS1YnIHx8IGZpbGVFeHQgPT09ICdGTFYnIHx8XHJcbiAgICAgICAgICAgICAgICBmaWxlRXh0ID09PSAnV01WJyB8fCBmaWxlRXh0ID09PSAnTVBFRycgfHwgZmlsZUV4dCA9PT0gJzNHUCcgfHwgZmlsZUV4dCA9PT0gJ01PVicpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnVklERU8nO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ1RYVCcpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnVFhUJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdFTUwnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ0VNTCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnUlRYJykge1xyXG4gICAgICAgICAgICAgICAgZmlsZUZvcm1hdE9iamVjdCA9ICdSVFgnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlsZUV4dCA9PT0gJ01EQicpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnTURCJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWxlRm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGZpbGVFeHQgPT09ICdaSVAnKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ1pJUCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChmaWxlRXh0ID09PSAnU05EJyB8fCBmaWxlRXh0ID09PSAnQUlGJyB8fCBmaWxlRXh0ID09PSAnQUlGRicpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVGb3JtYXRPYmplY3QgPSAnQVVESU8nO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpbGVGb3JtYXRPYmplY3Q7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlRm9ybWF0T2JqZWN0ID0gJ1Vuc3VwcG9ydGVkIEZvcm1hdCc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmlsZUZvcm1hdE9iamVjdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgR2V0Q1JJc1ZpZXdlcihpc1ZpZXdlcik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgSXNWaWV3ZXI6IGlzVmlld2VyXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9DUl9JU19WSUVXRVJfTlMsIHRoaXMuR0VUX0NSX0lTX1ZJRVdFUl9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ==