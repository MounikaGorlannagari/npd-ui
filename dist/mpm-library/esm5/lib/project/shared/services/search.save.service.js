import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "../../../mpm-utils/services/sharing.service";
var SearchSaveService = /** @class */ (function () {
    function SearchSaveService(appService, sharingService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.CreateMPMSavedSearchesOperationNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.CreateMPMSavedSearchesWS = 'CreateMPM_Saved_Searches';
        this.DeleteMPMSavedSearchesWS = 'DeleteMPM_Saved_Searches';
        this.DeleteMPMSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.UpdateMPMSavedSearchesWS = 'UpdateMPM_Saved_Searches';
        this.UpdateMPMSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
    }
    SearchSaveService.prototype.saveSearch = function (name, desc, searchConditionList, isPublic, viewName) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                'MPM_Saved_Searches-create': {
                    NAME: name,
                    DESCRIPTION: desc,
                    IS_PUBLIC: isPublic,
                    IS_ROLE_SPECIFIC: false,
                    ROLE_ID: '',
                    TYPE: '',
                    IS_SYSTEM_DEFAULT_SEARCH: false,
                    DEFAULT_OPERATOR: '',
                    SEARCH_DATA: searchConditionList ? JSON.stringify(searchConditionList) : '',
                    VIEW_NAME: viewName,
                    R_PO_CREATED_USER: {
                        'Identity-id': {
                            Id: _this.sharingService.getcurrentUserItemID()
                        }
                    }
                }
            };
            _this.appService.invokeRequest(_this.CreateMPMSavedSearchesOperationNS, _this.CreateMPMSavedSearchesWS, param).subscribe(function (savedSearch) {
                observer.next(savedSearch);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    SearchSaveService.prototype.deleteSavedSearch = function (savedSearchId) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                MPM_Saved_Searches: {
                    'MPM_Saved_Searches-id': {
                        Id: savedSearchId
                    }
                }
            };
            _this.appService.invokeRequest(_this.DeleteMPMSavedSearchesNS, _this.DeleteMPMSavedSearchesWS, param).subscribe(function (success) {
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    SearchSaveService.prototype.updateSavedSearch = function (seachId, name, desc, searchConditionList, isPublic, viewName) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                'MPM_Saved_Searches-id': {
                    Id: seachId
                },
                'MPM_Saved_Searches-update': {
                    NAME: name,
                    DESCRIPTION: desc,
                    IS_PUBLIC: isPublic,
                    IS_ROLE_SPECIFIC: false,
                    ROLE_ID: '',
                    TYPE: '',
                    IS_SYSTEM_DEFAULT_SEARCH: false,
                    DEFAULT_OPERATOR: '',
                    SEARCH_DATA: searchConditionList ? JSON.stringify(searchConditionList) : '',
                    VIEW_NAME: viewName,
                    R_PO_CREATED_USER: {
                        'Identity-id': {
                            Id: _this.sharingService.getcurrentUserItemID()
                        }
                    }
                }
            };
            _this.appService.invokeRequest(_this.UpdateMPMSavedSearchesNS, _this.UpdateMPMSavedSearchesWS, param).subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    SearchSaveService.ctorParameters = function () { return [
        { type: AppService },
        { type: SharingService }
    ]; };
    SearchSaveService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchSaveService_Factory() { return new SearchSaveService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService)); }, token: SearchSaveService, providedIn: "root" });
    SearchSaveService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], SearchSaveService);
    return SearchSaveService;
}());
export { SearchSaveService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNhdmUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC5zYXZlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUVsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDOzs7O0FBVTdFO0lBRUksMkJBQ1csVUFBc0IsRUFDdEIsY0FBOEI7UUFEOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFHekMsc0NBQWlDLEdBQUcsNkRBQTZELENBQUM7UUFDbEcsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFFdEQsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFDdEQsNkJBQXdCLEdBQUcsNkRBQTZELENBQUM7UUFFekYsNkJBQXdCLEdBQUcsMEJBQTBCLENBQUM7UUFDdEQsNkJBQXdCLEdBQUcsNkRBQTZELENBQUM7SUFUckYsQ0FBQztJQVdMLHNDQUFVLEdBQVYsVUFBVyxJQUFZLEVBQUUsSUFBWSxFQUFFLG1CQUF3QyxFQUFFLFFBQWlCLEVBQUUsUUFBUTtRQUE1RyxpQkE2QkM7UUE1QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxLQUFLLEdBQXlCO2dCQUNoQywyQkFBMkIsRUFBRTtvQkFDekIsSUFBSSxFQUFFLElBQUk7b0JBQ1YsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLFNBQVMsRUFBRSxRQUFRO29CQUNuQixnQkFBZ0IsRUFBRSxLQUFLO29CQUN2QixPQUFPLEVBQUUsRUFBRTtvQkFDWCxJQUFJLEVBQUUsRUFBRTtvQkFDUix3QkFBd0IsRUFBRSxLQUFLO29CQUMvQixnQkFBZ0IsRUFBRSxFQUFFO29CQUNwQixXQUFXLEVBQUUsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDM0UsU0FBUyxFQUFFLFFBQVE7b0JBQ25CLGlCQUFpQixFQUFFO3dCQUNmLGFBQWEsRUFBRTs0QkFDWCxFQUFFLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRTt5QkFDakQ7cUJBQ0o7aUJBQ0o7YUFDSixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGlDQUFpQyxFQUFFLEtBQUksQ0FBQyx3QkFBd0IsRUFBRSxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXO2dCQUM3SCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZDQUFpQixHQUFqQixVQUFrQixhQUFxQjtRQUF2QyxpQkFpQkM7UUFoQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxLQUFLLEdBQTRCO2dCQUNuQyxrQkFBa0IsRUFBRTtvQkFDaEIsdUJBQXVCLEVBQUU7d0JBQ3JCLEVBQUUsRUFBRSxhQUFhO3FCQUNwQjtpQkFDSjthQUNKLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSSxDQUFDLHdCQUF3QixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLE9BQU87Z0JBQ2hILFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQWlCLEdBQWpCLFVBQWtCLE9BQWUsRUFBRSxJQUFZLEVBQUUsSUFBWSxFQUFFLG1CQUF3QyxFQUFFLFFBQWlCLEVBQUUsUUFBUTtRQUFwSSxpQkFnQ0M7UUEvQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxLQUFLLEdBQTZCO2dCQUNwQyx1QkFBdUIsRUFBRTtvQkFDckIsRUFBRSxFQUFFLE9BQU87aUJBQ2Q7Z0JBQ0QsMkJBQTJCLEVBQUU7b0JBQ3pCLElBQUksRUFBRSxJQUFJO29CQUNWLFdBQVcsRUFBRSxJQUFJO29CQUNqQixTQUFTLEVBQUUsUUFBUTtvQkFDbkIsZ0JBQWdCLEVBQUUsS0FBSztvQkFDdkIsT0FBTyxFQUFFLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLEVBQUU7b0JBQ1Isd0JBQXdCLEVBQUUsS0FBSztvQkFDL0IsZ0JBQWdCLEVBQUUsRUFBRTtvQkFDcEIsV0FBVyxFQUFFLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQzNFLFNBQVMsRUFBRSxRQUFRO29CQUNuQixpQkFBaUIsRUFBRTt3QkFDZixhQUFhLEVBQUU7NEJBQ1gsRUFBRSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7eUJBQ2pEO3FCQUNKO2lCQUNKO2FBQ0osQ0FBQztZQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyx3QkFBd0IsRUFBRSxLQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDakgsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQS9Gc0IsVUFBVTtnQkFDTixjQUFjOzs7SUFKaEMsaUJBQWlCO1FBSjdCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxpQkFBaUIsQ0FvRzdCOzRCQWxIRDtDQWtIQyxBQXBHRCxJQW9HQztTQXBHWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTVBNU2F2ZWRTZWFyY2hDcmVhdGUsIFNlYXJjaENvbmRpdGlvbkxpc3QgfSBmcm9tICcuLi8uLi8uLi9zZWFyY2gvb2JqZWN0cy9NUE1TYXZlZFNlYXJjaENyZWF0ZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb25kaXRpb25MaXN0IH0gZnJvbSAnLi4vLi4vLi4vc2VhcmNoL29iamVjdHMvQ29uZGl0aW9uTGlzdCc7XHJcbmltcG9ydCB7IE1QTURlbGV0ZVNhdmVkU2VhcmNoUmVxIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL29iamVjdHMvTVBNRGVsZXRlU2F2ZWRTZWFyY2hSZXEnO1xyXG5pbXBvcnQgeyBNUE1TYXZlZFNlYXJjaCB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vYmplY3RzL01QTVNhdmVkU2VhcmNoJztcclxuaW1wb3J0IHsgVXBkYXRlTVBNU2F2ZWRTZWFyY2hlUmVxIH0gZnJvbSAnLi4vLi4vLi4vc2VhcmNoL29iamVjdHMvVXBkYXRlTVBNU2F2ZWRTZWFyY2hlUmVxJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNlYXJjaFNhdmVTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIENyZWF0ZU1QTVNhdmVkU2VhcmNoZXNPcGVyYXRpb25OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU2F2ZWRfU2VhcmNoZXMvb3BlcmF0aW9ucyc7XHJcbiAgICBDcmVhdGVNUE1TYXZlZFNlYXJjaGVzV1MgPSAnQ3JlYXRlTVBNX1NhdmVkX1NlYXJjaGVzJztcclxuXHJcbiAgICBEZWxldGVNUE1TYXZlZFNlYXJjaGVzV1MgPSAnRGVsZXRlTVBNX1NhdmVkX1NlYXJjaGVzJztcclxuICAgIERlbGV0ZU1QTVNhdmVkU2VhcmNoZXNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU2F2ZWRfU2VhcmNoZXMvb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgVXBkYXRlTVBNU2F2ZWRTZWFyY2hlc1dTID0gJ1VwZGF0ZU1QTV9TYXZlZF9TZWFyY2hlcyc7XHJcbiAgICBVcGRhdGVNUE1TYXZlZFNlYXJjaGVzTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1NhdmVkX1NlYXJjaGVzL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIHNhdmVTZWFyY2gobmFtZTogc3RyaW5nLCBkZXNjOiBzdHJpbmcsIHNlYXJjaENvbmRpdGlvbkxpc3Q6IFNlYXJjaENvbmRpdGlvbkxpc3QsIGlzUHVibGljOiBib29sZWFuLCB2aWV3TmFtZSk6IE9ic2VydmFibGU8TVBNU2F2ZWRTZWFyY2hDcmVhdGU+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbTogTVBNU2F2ZWRTZWFyY2hDcmVhdGUgPSB7XHJcbiAgICAgICAgICAgICAgICAnTVBNX1NhdmVkX1NlYXJjaGVzLWNyZWF0ZSc6IHtcclxuICAgICAgICAgICAgICAgICAgICBOQU1FOiBuYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIERFU0NSSVBUSU9OOiBkZXNjLFxyXG4gICAgICAgICAgICAgICAgICAgIElTX1BVQkxJQzogaXNQdWJsaWMsXHJcbiAgICAgICAgICAgICAgICAgICAgSVNfUk9MRV9TUEVDSUZJQzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgUk9MRV9JRDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgVFlQRTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgSVNfU1lTVEVNX0RFRkFVTFRfU0VBUkNIOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBERUZBVUxUX09QRVJBVE9SOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBTRUFSQ0hfREFUQTogc2VhcmNoQ29uZGl0aW9uTGlzdCA/IEpTT04uc3RyaW5naWZ5KHNlYXJjaENvbmRpdGlvbkxpc3QpIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgVklFV19OQU1FOiB2aWV3TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBSX1BPX0NSRUFURURfVVNFUjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnSWRlbnRpdHktaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRjdXJyZW50VXNlckl0ZW1JRCgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuQ3JlYXRlTVBNU2F2ZWRTZWFyY2hlc09wZXJhdGlvbk5TLCB0aGlzLkNyZWF0ZU1QTVNhdmVkU2VhcmNoZXNXUywgcGFyYW0pLnN1YnNjcmliZShzYXZlZFNlYXJjaCA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNhdmVkU2VhcmNoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVTYXZlZFNlYXJjaChzYXZlZFNlYXJjaElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbTogTVBNRGVsZXRlU2F2ZWRTZWFyY2hSZXEgPSB7XHJcbiAgICAgICAgICAgICAgICBNUE1fU2F2ZWRfU2VhcmNoZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogc2F2ZWRTZWFyY2hJZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5EZWxldGVNUE1TYXZlZFNlYXJjaGVzTlMsIHRoaXMuRGVsZXRlTVBNU2F2ZWRTZWFyY2hlc1dTLCBwYXJhbSkuc3Vic2NyaWJlKHN1Y2Nlc3MgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVTYXZlZFNlYXJjaChzZWFjaElkOiBzdHJpbmcsIG5hbWU6IHN0cmluZywgZGVzYzogc3RyaW5nLCBzZWFyY2hDb25kaXRpb25MaXN0OiBTZWFyY2hDb25kaXRpb25MaXN0LCBpc1B1YmxpYzogYm9vbGVhbiwgdmlld05hbWUpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbTogVXBkYXRlTVBNU2F2ZWRTZWFyY2hlUmVxID0ge1xyXG4gICAgICAgICAgICAgICAgJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogc2VhY2hJZFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICdNUE1fU2F2ZWRfU2VhcmNoZXMtdXBkYXRlJzoge1xyXG4gICAgICAgICAgICAgICAgICAgIE5BTUU6IG5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgREVTQ1JJUFRJT046IGRlc2MsXHJcbiAgICAgICAgICAgICAgICAgICAgSVNfUFVCTElDOiBpc1B1YmxpYyxcclxuICAgICAgICAgICAgICAgICAgICBJU19ST0xFX1NQRUNJRklDOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBST0xFX0lEOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBUWVBFOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBJU19TWVNURU1fREVGQVVMVF9TRUFSQ0g6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIERFRkFVTFRfT1BFUkFUT1I6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIFNFQVJDSF9EQVRBOiBzZWFyY2hDb25kaXRpb25MaXN0ID8gSlNPTi5zdHJpbmdpZnkoc2VhcmNoQ29uZGl0aW9uTGlzdCkgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBWSUVXX05BTUU6IHZpZXdOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIFJfUE9fQ1JFQVRFRF9VU0VSOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdJZGVudGl0eS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIElkOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldGN1cnJlbnRVc2VySXRlbUlEKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5VcGRhdGVNUE1TYXZlZFNlYXJjaGVzTlMsIHRoaXMuVXBkYXRlTVBNU2F2ZWRTZWFyY2hlc1dTLCBwYXJhbSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=