import { __decorate, __read, __spread } from "tslib";
import { Injectable } from '@angular/core';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { OTMMDataTypes } from '../../../mpm-utils/objects/OTMMDataTypes';
import { AssetConstants } from '../../assets/asset_constants';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../../shared/pipe/format-to-locale.pipe";
import * as i2 from "../../../mpm-utils/services/sharing.service";
var AssetMetadataHelper = /** @class */ (function () {
    function AssetMetadataHelper(formatToLocalePipe, sharingService) {
        this.formatToLocalePipe = formatToLocalePipe;
        this.sharingService = sharingService;
    }
    AssetMetadataHelper.prototype.checkandFormValues = function (valueObject) {
        if (valueObject.value && valueObject.value.value !== 'NA') {
            valueObject.value.value = valueObject.value.type === OTMMDataTypes.DATE_TIME ? this.formatToLocalePipe.transform(valueObject.value.value) : valueObject.value.value;
        }
        return valueObject;
    };
    AssetMetadataHelper.prototype.getDisplayMetadataFields = function (metadataFieldList, metadataFilterList, isDefaultMetadataField, isRenameRequired) {
        var _this = this;
        var displayableMetadataFields = [];
        metadataFieldList.map(function (field) {
            var fieldObj = metadataFilterList.find(function (e) { return (e.id === field.FIELD_ID); });
            if (fieldObj) {
                fieldObj.sequence = Number(field.DISPLAY_SEQUENCE);
                fieldObj.referenceValue = (field && field.REFERENCE_VALUE && typeof field.REFERENCE_VALUE === 'string')
                    ? field.REFERENCE_VALUE : null;
                fieldObj.isDefaultMetadataField = isDefaultMetadataField;
                if ((fieldObj.value && fieldObj.value.value) || (fieldObj.metadata_element && fieldObj.metadata_element.value && fieldObj.metadata_element.value.value) || true) {
                    var currValue = _this.checkandFormValues(fieldObj.value || fieldObj.metadata_element.value);
                    if (currValue) {
                        if (fieldObj.value) {
                            fieldObj.value = currValue;
                        }
                        else {
                            fieldObj.metadata_element.value = currValue;
                        }
                        if (isRenameRequired) {
                            fieldObj.metadata_element.name = _this.replaceTheString(fieldObj.metadata_element.name, isRenameRequired.findString, isRenameRequired.replaceString);
                            console.log(fieldObj);
                        }
                        displayableMetadataFields.push(fieldObj);
                    }
                }
            }
        });
        return displayableMetadataFields;
    };
    AssetMetadataHelper.prototype.getListDisplayColumnConfig = function (defaultMetadataFields, metaDataFields, inheritedMetadataFields, isTemplate) {
        var _this = this;
        var data = {
            displayableMetadataFields: [],
            displayableInheritedMetadataFields: []
        };
        var displayableMetadataFields = [];
        var displayableInheritedMetadataFields = [];
        if (metaDataFields && metaDataFields.metadata_element_list) {
            metaDataFields.metadata_element_list.forEach(function (metadataElement, index) {
                var _a;
                displayableMetadataFields.push({
                    MetadataFieldGroup: metadataElement.name,
                    MetadataFields: []
                });
                var metadataField = metadataElement.metadata_element_list;
                if (metadataField && Array.isArray(metadataField)) {
                    (_a = displayableMetadataFields[index].MetadataFields).push.apply(_a, __spread(_this.getDisplayMetadataFields(defaultMetadataFields, metadataField, true, null)));
                }
            });
        }
        if (inheritedMetadataFields) {
            inheritedMetadataFields.forEach(function (metadataElement, index) {
                var _a;
                var isRenameRequired = null;
                if (isTemplate && metadataElement.container_type_name && metadataElement.container_type_name.indexOf('MPM Project Folder') >= 0) {
                    isRenameRequired = {
                        findString: 'Project',
                        replaceString: 'Template'
                    };
                    metadataElement.container_type_name = _this.replaceTheString(metadataElement.container_type_name, isRenameRequired.findString, isRenameRequired.replaceString);
                }
                displayableInheritedMetadataFields.push({
                    MetadataFieldGroup: metadataElement.container_type_name,
                    MetadataFields: []
                });
                var inheritedMetadataField = metadataElement.inherited_metadata_values;
                if (inheritedMetadataField && Array.isArray(inheritedMetadataField)) {
                    (_a = displayableInheritedMetadataFields[index].MetadataFields).push.apply(_a, __spread(_this.getDisplayMetadataFields(defaultMetadataFields, inheritedMetadataField, true, isRenameRequired)));
                }
            });
        }
        displayableMetadataFields.forEach(function (element) {
            element.MetadataFields = element.MetadataFields.sort(function (a, b) {
                return a.sequence - b.sequence;
            });
        });
        displayableInheritedMetadataFields.forEach(function (element) {
            element.MetadataFields = element.MetadataFields.sort(function (a, b) {
                return a.sequence - b.sequence;
            });
        });
        data.displayableMetadataFields = displayableMetadataFields;
        data.displayableInheritedMetadataFields = displayableInheritedMetadataFields;
        return data;
    };
    AssetMetadataHelper.prototype.getAssetDetailsMetadata = function (asset_details_data, asset_details_fields, isTemplate) {
        var _this = this;
        var data = {
            displayableMetadataFields: [],
            displayableInheritedMetadataFields: []
        };
        var displayableMetadataFields = [];
        var displayableInheritedMetadataFields = [];
        var assetDetailsViewConfigs = (asset_details_data && asset_details_data.length === 1) ? AssetConstants.REFERENCE_ASSET_DETAILS_VIEW_CONFIG : AssetConstants.ASSET_DETAILS_VIEW_CONFIG;
        var categoryLelvels = this.sharingService.getCategoryLevelsById();
        if (asset_details_data && asset_details_data.length > 0 && asset_details_fields && asset_details_fields.length > 0) {
            assetDetailsViewConfigs.forEach(function (assetDetailsViewConfig) {
                var category_level_view = categoryLelvels.find(function (category_level) { return assetDetailsViewConfig.type == category_level.categoryLevelType; });
                var category_details_data = asset_details_data.find(function (details_data) { return details_data.CONTENT_TYPE == assetDetailsViewConfig.content_type; });
                var category_detials_fields = asset_details_fields.filter(function (details_field) { return details_field.R_PO_MPM_CATEGORY_LEVEL['MPM_Category_Level-id'].Id == category_level_view.id; });
                displayableMetadataFields.push({
                    MetadataFieldGroup: assetDetailsViewConfig.group_display_name,
                    MetadataFields: _this.getAssetDetailsDataMapping(category_details_data, category_detials_fields)
                });
            });
        }
        data.displayableMetadataFields = displayableMetadataFields;
        return data;
    };
    AssetMetadataHelper.prototype.getAssetDetailsDataMapping = function (asset_details_data, view_details_fields) {
        var _this = this;
        var assetMetadata = [];
        view_details_fields.forEach(function (view_details_field) {
            assetMetadata.push({
                name: view_details_field.DISPLAY_NAME,
                value: (view_details_field.DATA_TYPE === OTMMDataTypes.DATE_TIME) ? asset_details_data ? _this.formatToLocalePipe.transform(asset_details_data[view_details_field.INDEXER_FIELD_ID]) : '' : asset_details_data[view_details_field.INDEXER_FIELD_ID]
            });
        });
        return assetMetadata;
    };
    AssetMetadataHelper.prototype.replaceTheString = function (originalString, findString, replaceString) {
        return originalString.replace(new RegExp(findString, 'gi'), replaceString);
    };
    AssetMetadataHelper.ctorParameters = function () { return [
        { type: FormatToLocalePipe },
        { type: SharingService }
    ]; };
    AssetMetadataHelper.ɵprov = i0.ɵɵdefineInjectable({ factory: function AssetMetadataHelper_Factory() { return new AssetMetadataHelper(i0.ɵɵinject(i1.FormatToLocalePipe), i0.ɵɵinject(i2.SharingService)); }, token: AssetMetadataHelper, providedIn: "root" });
    AssetMetadataHelper = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AssetMetadataHelper);
    return AssetMetadataHelper;
}());
export { AssetMetadataHelper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQubWV0YWRhdGEuaGVscGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC9zaGFyZWQvc2VydmljZXMvYXNzZXQubWV0YWRhdGEuaGVscGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDOzs7O0FBTTdFO0lBQ0ksNkJBQ1csa0JBQXNDLEVBQ3RDLGNBQThCO1FBRDlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO0lBQ3JDLENBQUM7SUFFTCxnREFBa0IsR0FBbEIsVUFBbUIsV0FBZ0I7UUFDL0IsSUFBSSxXQUFXLENBQUMsS0FBSyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLElBQUksRUFBRTtZQUN2RCxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1NBQ3ZLO1FBQ0QsT0FBTyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQUVELHNEQUF3QixHQUF4QixVQUF5QixpQkFBNkIsRUFBRSxrQkFBOEIsRUFBRSxzQkFBK0IsRUFBRSxnQkFBcUI7UUFBOUksaUJBMkJDO1FBMUJHLElBQU0seUJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxVQUFBLEtBQUs7WUFDdkIsSUFBTSxRQUFRLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO1lBQ3pFLElBQUksUUFBUSxFQUFFO2dCQUNWLFFBQVEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNuRCxRQUFRLENBQUMsY0FBYyxHQUFHLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxlQUFlLElBQUksT0FBTyxLQUFLLENBQUMsZUFBZSxLQUFLLFFBQVEsQ0FBQztvQkFDbkcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkMsUUFBUSxDQUFDLHNCQUFzQixHQUFHLHNCQUFzQixDQUFDO2dCQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLEVBQUU7b0JBQzdKLElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDN0YsSUFBSSxTQUFTLEVBQUU7d0JBQ1gsSUFBSSxRQUFRLENBQUMsS0FBSyxFQUFFOzRCQUNoQixRQUFRLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQzt5QkFDOUI7NkJBQU07NEJBQ0gsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7eUJBQy9DO3dCQUNELElBQUksZ0JBQWdCLEVBQUU7NEJBQ2xCLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUNwSixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUN6Qjt3QkFDRCx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQzVDO2lCQUNKO2FBQ0o7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8seUJBQXlCLENBQUM7SUFDckMsQ0FBQztJQUVELHdEQUEwQixHQUExQixVQUEyQixxQkFBcUIsRUFBRSxjQUFjLEVBQUUsdUJBQXVCLEVBQUUsVUFBbUI7UUFBOUcsaUJBMERDO1FBeERHLElBQU0sSUFBSSxHQUFHO1lBQ1QseUJBQXlCLEVBQUUsRUFBRTtZQUM3QixrQ0FBa0MsRUFBRSxFQUFFO1NBQ3pDLENBQUM7UUFDRixJQUFNLHlCQUF5QixHQUFHLEVBQUUsQ0FBQztRQUNyQyxJQUFNLGtDQUFrQyxHQUFHLEVBQUUsQ0FBQztRQUU5QyxJQUFJLGNBQWMsSUFBSSxjQUFjLENBQUMscUJBQXFCLEVBQUU7WUFDeEQsY0FBYyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFDLGVBQWUsRUFBRSxLQUFLOztnQkFDaEUseUJBQXlCLENBQUMsSUFBSSxDQUFDO29CQUMzQixrQkFBa0IsRUFBRSxlQUFlLENBQUMsSUFBSTtvQkFDeEMsY0FBYyxFQUFFLEVBQUU7aUJBQ3JCLENBQUMsQ0FBQztnQkFDSCxJQUFNLGFBQWEsR0FBRyxlQUFlLENBQUMscUJBQXFCLENBQUM7Z0JBQzVELElBQUksYUFBYSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7b0JBQy9DLENBQUEsS0FBQSx5QkFBeUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxjQUFjLENBQUEsQ0FBQyxJQUFJLG9CQUFJLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxxQkFBcUIsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFFO2lCQUM1STtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLHVCQUF1QixFQUFFO1lBQ3pCLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxVQUFDLGVBQWUsRUFBRSxLQUFLOztnQkFDbkQsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQzVCLElBQUksVUFBVSxJQUFJLGVBQWUsQ0FBQyxtQkFBbUIsSUFBSSxlQUFlLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM3SCxnQkFBZ0IsR0FBRzt3QkFDZixVQUFVLEVBQUUsU0FBUzt3QkFDckIsYUFBYSxFQUFFLFVBQVU7cUJBQzVCLENBQUM7b0JBQ0YsZUFBZSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLEVBQUUsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDO2lCQUNqSztnQkFDRCxrQ0FBa0MsQ0FBQyxJQUFJLENBQUM7b0JBQ3BDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxtQkFBbUI7b0JBQ3ZELGNBQWMsRUFBRSxFQUFFO2lCQUNyQixDQUFDLENBQUM7Z0JBQ0gsSUFBTSxzQkFBc0IsR0FBRyxlQUFlLENBQUMseUJBQXlCLENBQUM7Z0JBQ3pFLElBQUksc0JBQXNCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFO29CQUNqRSxDQUFBLEtBQUEsa0NBQWtDLENBQUMsS0FBSyxDQUFDLENBQUMsY0FBYyxDQUFBLENBQUMsSUFBSSxvQkFBSSxLQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixDQUFDLEdBQUU7aUJBQzFLO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELHlCQUF5QixDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDckMsT0FBTyxDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN0RCxPQUFPLENBQUMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUNuQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBRUgsa0NBQWtDLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUM5QyxPQUFPLENBQUMsY0FBYyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3RELE9BQU8sQ0FBQyxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDO1lBQ25DLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMseUJBQXlCLEdBQUcseUJBQXlCLENBQUM7UUFDM0QsSUFBSSxDQUFDLGtDQUFrQyxHQUFHLGtDQUFrQyxDQUFDO1FBRTdFLE9BQU8sSUFBSSxDQUFDO0lBRWhCLENBQUM7SUFFRCxxREFBdUIsR0FBdkIsVUFBd0Isa0JBQWtCLEVBQUUsb0JBQW9CLEVBQUUsVUFBbUI7UUFBckYsaUJBdUJDO1FBdEJHLElBQU0sSUFBSSxHQUFHO1lBQ1QseUJBQXlCLEVBQUUsRUFBRTtZQUM3QixrQ0FBa0MsRUFBRSxFQUFFO1NBQ3pDLENBQUM7UUFDRixJQUFNLHlCQUF5QixHQUFHLEVBQUUsQ0FBQztRQUNyQyxJQUFNLGtDQUFrQyxHQUFHLEVBQUUsQ0FBQztRQUM5QyxJQUFNLHVCQUF1QixHQUFHLENBQUMsa0JBQWtCLElBQUksa0JBQWtCLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsQ0FBQztRQUN4TCxJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFFcEUsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDaEgsdUJBQXVCLENBQUMsT0FBTyxDQUFDLFVBQUEsc0JBQXNCO2dCQUNsRCxJQUFNLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBQSxjQUFjLElBQU0sT0FBTyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksY0FBYyxDQUFDLGlCQUFpQixDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9JLElBQU0scUJBQXFCLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQUEsWUFBWSxJQUFNLE9BQU8sWUFBWSxDQUFDLFlBQVksSUFBSSxzQkFBc0IsQ0FBQyxZQUFZLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkosSUFBTSx1QkFBdUIsR0FBRyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxhQUFhLElBQU0sT0FBTyxhQUFhLENBQUMsdUJBQXVCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLElBQUksbUJBQW1CLENBQUMsRUFBRSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JMLHlCQUF5QixDQUFDLElBQUksQ0FBQztvQkFDM0Isa0JBQWtCLEVBQUUsc0JBQXNCLENBQUMsa0JBQWtCO29CQUM3RCxjQUFjLEVBQUUsS0FBSSxDQUFDLDBCQUEwQixDQUFDLHFCQUFxQixFQUFFLHVCQUF1QixDQUFDO2lCQUNsRyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLHlCQUF5QixHQUFHLHlCQUF5QixDQUFDO1FBQzNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCx3REFBMEIsR0FBMUIsVUFBMkIsa0JBQXVCLEVBQUUsbUJBQXdCO1FBQTVFLGlCQVNDO1FBUkcsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGtCQUFrQjtZQUMxQyxhQUFhLENBQUMsSUFBSSxDQUFDO2dCQUNmLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxZQUFZO2dCQUNyQyxLQUFLLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEtBQUssYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7YUFDclAsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLGFBQWEsQ0FBQztJQUN6QixDQUFDO0lBRUQsOENBQWdCLEdBQWhCLFVBQWlCLGNBQXNCLEVBQUUsVUFBa0IsRUFBRSxhQUFxQjtRQUM5RSxPQUFPLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQy9FLENBQUM7O2dCQTFJOEIsa0JBQWtCO2dCQUN0QixjQUFjOzs7SUFIaEMsbUJBQW1CO1FBSi9CLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxtQkFBbUIsQ0E2SS9COzhCQXZKRDtDQXVKQyxBQTdJRCxJQTZJQztTQTdJWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1hdFRvTG9jYWxlUGlwZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUucGlwZSc7XHJcbmltcG9ydCB7IE9UTU1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgQXNzZXRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9hc3NldHMvYXNzZXRfY29uc3RhbnRzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFzc2V0TWV0YWRhdGFIZWxwZXIge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGZvcm1hdFRvTG9jYWxlUGlwZTogRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgY2hlY2thbmRGb3JtVmFsdWVzKHZhbHVlT2JqZWN0OiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGlmICh2YWx1ZU9iamVjdC52YWx1ZSAmJiB2YWx1ZU9iamVjdC52YWx1ZS52YWx1ZSAhPT0gJ05BJykge1xyXG4gICAgICAgICAgICB2YWx1ZU9iamVjdC52YWx1ZS52YWx1ZSA9IHZhbHVlT2JqZWN0LnZhbHVlLnR5cGUgPT09IE9UTU1EYXRhVHlwZXMuREFURV9USU1FID8gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKHZhbHVlT2JqZWN0LnZhbHVlLnZhbHVlKSA6IHZhbHVlT2JqZWN0LnZhbHVlLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWVPYmplY3Q7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGlzcGxheU1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRMaXN0OiBBcnJheTxhbnk+LCBtZXRhZGF0YUZpbHRlckxpc3Q6IEFycmF5PGFueT4sIGlzRGVmYXVsdE1ldGFkYXRhRmllbGQ6IGJvb2xlYW4sIGlzUmVuYW1lUmVxdWlyZWQ6IGFueSk6IEFycmF5PGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuICAgICAgICBtZXRhZGF0YUZpZWxkTGlzdC5tYXAoZmllbGQgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IG1ldGFkYXRhRmlsdGVyTGlzdC5maW5kKGUgPT4gKGUuaWQgPT09IGZpZWxkLkZJRUxEX0lEKSk7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgZmllbGRPYmouc2VxdWVuY2UgPSBOdW1iZXIoZmllbGQuRElTUExBWV9TRVFVRU5DRSk7XHJcbiAgICAgICAgICAgICAgICBmaWVsZE9iai5yZWZlcmVuY2VWYWx1ZSA9IChmaWVsZCAmJiBmaWVsZC5SRUZFUkVOQ0VfVkFMVUUgJiYgdHlwZW9mIGZpZWxkLlJFRkVSRU5DRV9WQUxVRSA9PT0gJ3N0cmluZycpXHJcbiAgICAgICAgICAgICAgICAgICAgPyBmaWVsZC5SRUZFUkVOQ0VfVkFMVUUgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgZmllbGRPYmouaXNEZWZhdWx0TWV0YWRhdGFGaWVsZCA9IGlzRGVmYXVsdE1ldGFkYXRhRmllbGQ7XHJcbiAgICAgICAgICAgICAgICBpZiAoKGZpZWxkT2JqLnZhbHVlICYmIGZpZWxkT2JqLnZhbHVlLnZhbHVlKSB8fCAoZmllbGRPYmoubWV0YWRhdGFfZWxlbWVudCAmJiBmaWVsZE9iai5tZXRhZGF0YV9lbGVtZW50LnZhbHVlICYmIGZpZWxkT2JqLm1ldGFkYXRhX2VsZW1lbnQudmFsdWUudmFsdWUpIHx8IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjdXJyVmFsdWUgPSB0aGlzLmNoZWNrYW5kRm9ybVZhbHVlcyhmaWVsZE9iai52YWx1ZSB8fCBmaWVsZE9iai5tZXRhZGF0YV9lbGVtZW50LnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY3VyclZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWVsZE9iai52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRPYmoudmFsdWUgPSBjdXJyVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZE9iai5tZXRhZGF0YV9lbGVtZW50LnZhbHVlID0gY3VyclZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc1JlbmFtZVJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZE9iai5tZXRhZGF0YV9lbGVtZW50Lm5hbWUgPSB0aGlzLnJlcGxhY2VUaGVTdHJpbmcoZmllbGRPYmoubWV0YWRhdGFfZWxlbWVudC5uYW1lLCBpc1JlbmFtZVJlcXVpcmVkLmZpbmRTdHJpbmcsIGlzUmVuYW1lUmVxdWlyZWQucmVwbGFjZVN0cmluZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhmaWVsZE9iaik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5wdXNoKGZpZWxkT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRMaXN0RGlzcGxheUNvbHVtbkNvbmZpZyhkZWZhdWx0TWV0YWRhdGFGaWVsZHMsIG1ldGFEYXRhRmllbGRzLCBpbmhlcml0ZWRNZXRhZGF0YUZpZWxkcywgaXNUZW1wbGF0ZTogYm9vbGVhbikge1xyXG5cclxuICAgICAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICAgICAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzOiBbXSxcclxuICAgICAgICAgICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkczogW11cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuICAgICAgICBjb25zdCBkaXNwbGF5YWJsZUluaGVyaXRlZE1ldGFkYXRhRmllbGRzID0gW107XHJcblxyXG4gICAgICAgIGlmIChtZXRhRGF0YUZpZWxkcyAmJiBtZXRhRGF0YUZpZWxkcy5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgbWV0YURhdGFGaWVsZHMubWV0YWRhdGFfZWxlbWVudF9saXN0LmZvckVhY2goKG1ldGFkYXRhRWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgTWV0YWRhdGFGaWVsZEdyb3VwOiBtZXRhZGF0YUVsZW1lbnQubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBNZXRhZGF0YUZpZWxkczogW11cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWV0YWRhdGFGaWVsZCA9IG1ldGFkYXRhRWxlbWVudC5tZXRhZGF0YV9lbGVtZW50X2xpc3Q7XHJcbiAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZCAmJiBBcnJheS5pc0FycmF5KG1ldGFkYXRhRmllbGQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkc1tpbmRleF0uTWV0YWRhdGFGaWVsZHMucHVzaCguLi50aGlzLmdldERpc3BsYXlNZXRhZGF0YUZpZWxkcyhkZWZhdWx0TWV0YWRhdGFGaWVsZHMsIG1ldGFkYXRhRmllbGQsIHRydWUsIG51bGwpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpbmhlcml0ZWRNZXRhZGF0YUZpZWxkcykge1xyXG4gICAgICAgICAgICBpbmhlcml0ZWRNZXRhZGF0YUZpZWxkcy5mb3JFYWNoKChtZXRhZGF0YUVsZW1lbnQsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgaXNSZW5hbWVSZXF1aXJlZCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXNUZW1wbGF0ZSAmJiBtZXRhZGF0YUVsZW1lbnQuY29udGFpbmVyX3R5cGVfbmFtZSAmJiBtZXRhZGF0YUVsZW1lbnQuY29udGFpbmVyX3R5cGVfbmFtZS5pbmRleE9mKCdNUE0gUHJvamVjdCBGb2xkZXInKSA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNSZW5hbWVSZXF1aXJlZCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmluZFN0cmluZzogJ1Byb2plY3QnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlU3RyaW5nOiAnVGVtcGxhdGUnXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUVsZW1lbnQuY29udGFpbmVyX3R5cGVfbmFtZSA9IHRoaXMucmVwbGFjZVRoZVN0cmluZyhtZXRhZGF0YUVsZW1lbnQuY29udGFpbmVyX3R5cGVfbmFtZSwgaXNSZW5hbWVSZXF1aXJlZC5maW5kU3RyaW5nLCBpc1JlbmFtZVJlcXVpcmVkLnJlcGxhY2VTdHJpbmcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBNZXRhZGF0YUZpZWxkR3JvdXA6IG1ldGFkYXRhRWxlbWVudC5jb250YWluZXJfdHlwZV9uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIE1ldGFkYXRhRmllbGRzOiBbXVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbmhlcml0ZWRNZXRhZGF0YUZpZWxkID0gbWV0YWRhdGFFbGVtZW50LmluaGVyaXRlZF9tZXRhZGF0YV92YWx1ZXM7XHJcbiAgICAgICAgICAgICAgICBpZiAoaW5oZXJpdGVkTWV0YWRhdGFGaWVsZCAmJiBBcnJheS5pc0FycmF5KGluaGVyaXRlZE1ldGFkYXRhRmllbGQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkc1tpbmRleF0uTWV0YWRhdGFGaWVsZHMucHVzaCguLi50aGlzLmdldERpc3BsYXlNZXRhZGF0YUZpZWxkcyhkZWZhdWx0TWV0YWRhdGFGaWVsZHMsIGluaGVyaXRlZE1ldGFkYXRhRmllbGQsIHRydWUsIGlzUmVuYW1lUmVxdWlyZWQpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgZWxlbWVudC5NZXRhZGF0YUZpZWxkcyA9IGVsZW1lbnQuTWV0YWRhdGFGaWVsZHMuc29ydCgoYSwgYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGEuc2VxdWVuY2UgLSBiLnNlcXVlbmNlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkcy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICBlbGVtZW50Lk1ldGFkYXRhRmllbGRzID0gZWxlbWVudC5NZXRhZGF0YUZpZWxkcy5zb3J0KChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYS5zZXF1ZW5jZSAtIGIuc2VxdWVuY2U7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBkYXRhLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gICAgICAgIGRhdGEuZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkcyA9IGRpc3BsYXlhYmxlSW5oZXJpdGVkTWV0YWRhdGFGaWVsZHM7XHJcblxyXG4gICAgICAgIHJldHVybiBkYXRhO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldERldGFpbHNNZXRhZGF0YShhc3NldF9kZXRhaWxzX2RhdGEsIGFzc2V0X2RldGFpbHNfZmllbGRzLCBpc1RlbXBsYXRlOiBib29sZWFuKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgICAgICAgZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkczogW10sXHJcbiAgICAgICAgICAgIGRpc3BsYXlhYmxlSW5oZXJpdGVkTWV0YWRhdGFGaWVsZHM6IFtdXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgY29uc3QgZGlzcGxheWFibGVJbmhlcml0ZWRNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGFzc2V0RGV0YWlsc1ZpZXdDb25maWdzID0gKGFzc2V0X2RldGFpbHNfZGF0YSAmJiBhc3NldF9kZXRhaWxzX2RhdGEubGVuZ3RoID09PSAxKSA/IEFzc2V0Q29uc3RhbnRzLlJFRkVSRU5DRV9BU1NFVF9ERVRBSUxTX1ZJRVdfQ09ORklHIDogQXNzZXRDb25zdGFudHMuQVNTRVRfREVUQUlMU19WSUVXX0NPTkZJRztcclxuICAgICAgICBjb25zdCBjYXRlZ29yeUxlbHZlbHMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxzQnlJZCgpO1xyXG5cclxuICAgICAgICBpZiAoYXNzZXRfZGV0YWlsc19kYXRhICYmIGFzc2V0X2RldGFpbHNfZGF0YS5sZW5ndGggPiAwICYmIGFzc2V0X2RldGFpbHNfZmllbGRzICYmIGFzc2V0X2RldGFpbHNfZmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgYXNzZXREZXRhaWxzVmlld0NvbmZpZ3MuZm9yRWFjaChhc3NldERldGFpbHNWaWV3Q29uZmlnID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhdGVnb3J5X2xldmVsX3ZpZXcgPSBjYXRlZ29yeUxlbHZlbHMuZmluZChjYXRlZ29yeV9sZXZlbCA9PiB7IHJldHVybiBhc3NldERldGFpbHNWaWV3Q29uZmlnLnR5cGUgPT0gY2F0ZWdvcnlfbGV2ZWwuY2F0ZWdvcnlMZXZlbFR5cGUgfSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yeV9kZXRhaWxzX2RhdGEgPSBhc3NldF9kZXRhaWxzX2RhdGEuZmluZChkZXRhaWxzX2RhdGEgPT4geyByZXR1cm4gZGV0YWlsc19kYXRhLkNPTlRFTlRfVFlQRSA9PSBhc3NldERldGFpbHNWaWV3Q29uZmlnLmNvbnRlbnRfdHlwZSB9KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNhdGVnb3J5X2RldGlhbHNfZmllbGRzID0gYXNzZXRfZGV0YWlsc19maWVsZHMuZmlsdGVyKGRldGFpbHNfZmllbGQgPT4geyByZXR1cm4gZGV0YWlsc19maWVsZC5SX1BPX01QTV9DQVRFR09SWV9MRVZFTFsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQgPT0gY2F0ZWdvcnlfbGV2ZWxfdmlldy5pZCB9KTtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgTWV0YWRhdGFGaWVsZEdyb3VwOiBhc3NldERldGFpbHNWaWV3Q29uZmlnLmdyb3VwX2Rpc3BsYXlfbmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBNZXRhZGF0YUZpZWxkczogdGhpcy5nZXRBc3NldERldGFpbHNEYXRhTWFwcGluZyhjYXRlZ29yeV9kZXRhaWxzX2RhdGEsIGNhdGVnb3J5X2RldGlhbHNfZmllbGRzKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkYXRhLmRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0RGV0YWlsc0RhdGFNYXBwaW5nKGFzc2V0X2RldGFpbHNfZGF0YTogYW55LCB2aWV3X2RldGFpbHNfZmllbGRzOiBhbnkpIHtcclxuICAgICAgICBsZXQgYXNzZXRNZXRhZGF0YSA9IFtdO1xyXG4gICAgICAgIHZpZXdfZGV0YWlsc19maWVsZHMuZm9yRWFjaCh2aWV3X2RldGFpbHNfZmllbGQgPT4ge1xyXG4gICAgICAgICAgICBhc3NldE1ldGFkYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogdmlld19kZXRhaWxzX2ZpZWxkLkRJU1BMQVlfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiAodmlld19kZXRhaWxzX2ZpZWxkLkRBVEFfVFlQRSA9PT0gT1RNTURhdGFUeXBlcy5EQVRFX1RJTUUpID8gYXNzZXRfZGV0YWlsc19kYXRhID8gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKGFzc2V0X2RldGFpbHNfZGF0YVt2aWV3X2RldGFpbHNfZmllbGQuSU5ERVhFUl9GSUVMRF9JRF0pIDogJycgOiBhc3NldF9kZXRhaWxzX2RhdGFbdmlld19kZXRhaWxzX2ZpZWxkLklOREVYRVJfRklFTERfSURdXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBhc3NldE1ldGFkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIHJlcGxhY2VUaGVTdHJpbmcob3JpZ2luYWxTdHJpbmc6IHN0cmluZywgZmluZFN0cmluZzogc3RyaW5nLCByZXBsYWNlU3RyaW5nOiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gb3JpZ2luYWxTdHJpbmcucmVwbGFjZShuZXcgUmVnRXhwKGZpbmRTdHJpbmcsICdnaScpLCByZXBsYWNlU3RyaW5nKTtcclxuICAgIH1cclxufVxyXG4iXX0=