import { __decorate, __values } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../../mpm-utils/services/app.service';
import { CategoryService } from './category.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { ProjectConstant } from '../../project-overview/project.constants';
import { FormControl, Validators, FormArray } from '@angular/forms';
import { DateValidators } from '../../../shared/validators/date.validator';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ProjectService } from './project.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import { DeliverableConstants } from '../../tasks/deliverable/deliverable.constants';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { MPM_ROLES } from '../../../mpm-utils/objects/Role';
import * as acronui from '../../../mpm-utils/auth/utility';
import { LoaderService } from '../../../loader/loader.service';
import { IndexerService } from '../../../shared/services/indexer/indexer.service';
import { SearchConfigConstants } from '../../../mpm-utils/objects/SearchConfigConstants';
import { ViewConfigService } from '../../../shared/services/view-config.service';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { MPMSearchOperators } from '../../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { newArray } from '@angular/compiler/src/util';
import { MPMSearchOperatorNames } from '../../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
import * as i2 from "./category.service";
import * as i3 from "../../../mpm-utils/services/otmm.service";
import * as i4 from "../../../mpm-utils/services/util.service";
import * as i5 from "./project.service";
import * as i6 from "../../../shared/services/otmm-metadata.service";
import * as i7 from "../../../mpm-utils/services/sharing.service";
import * as i8 from "../../../loader/loader.service";
import * as i9 from "../../../shared/services/indexer/indexer.service";
import * as i10 from "../../../shared/services/view-config.service";
import * as i11 from "../../../shared/services/field-config.service";
var ProjectFromTemplateService = /** @class */ (function () {
    function ProjectFromTemplateService(appService, categoryService, otmmService, utilService, projectService, otmmMetadataService, sharingService, loaderService, indexerService, viewConfigService, fieldConfigService) {
        this.appService = appService;
        this.categoryService = categoryService;
        this.otmmService = otmmService;
        this.utilService = utilService;
        this.projectService = projectService;
        this.otmmMetadataService = otmmMetadataService;
        this.sharingService = sharingService;
        this.loaderService = loaderService;
        this.indexerService = indexerService;
        this.viewConfigService = viewConfigService;
        this.fieldConfigService = fieldConfigService;
        this.PROJECT_NAME_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.GET_PROJECT_BY_NAME_WS_METHOD_NAME = 'GetAllProjectsByNameType';
        this.PROJECT_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_PROJECT_FROM_TEMPLATE_METHOD_NAME = 'CreateProjectFromTemplate';
        //TEMPLATE_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.TEMPLATE_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.CREATE_TEMPLATE_FROM_TEMPLATE_METHOD_NAME = 'CreateTemplateFromSource';
        this.TEAM_BPM_METHOD_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
        this.GET_TEAM_WS_METHOD_NAME = 'GetTeamsForUser';
        this.TEAM_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Teams/operations';
        this.GET_ALL_TEAMS_WS_METHOD_NAME = 'GetAllTeams';
        this.DELIVERABLE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable/operations';
        this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME = 'GetAllDeliverablesByProject';
        this.GET_TASK_BY_PROJECT_WS_METHOD_NAME = 'GetTaskByProjectID';
        this.TASK_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.metadataConstants = MPMFieldConstants;
    }
    ProjectFromTemplateService.prototype.getProperty = function (obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    };
    ProjectFromTemplateService.prototype.getUserDetailsByUserCn = function (userCn) {
        var _this = this;
        return new Observable(function (observer) {
            var userIdentyID = '';
            var parameters = {
                userCN: userCn
            };
            _this.appService.getPersonDetailsByUserCN(parameters)
                .subscribe(function (response) {
                if (response && response.Person && response.Person.PersonToUser['Identity-id'] &&
                    response.Person.PersonToUser['Identity-id'].Id) {
                    userIdentyID = response.Person.PersonToUser['Identity-id'].Id;
                }
                observer.next(userIdentyID);
                observer.complete();
            });
        });
    };
    ProjectFromTemplateService.prototype.getProjectOwners = function (teamId) {
        var _this = this;
        var roleDetailObj = this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
        return new Observable(function (observer) {
            var getUserByRoleRequest = {
                allocationType: 'ROLE',
                roleDN: roleDetailObj.ROLE_DN,
                teamID: teamId,
                isApproveTask: false,
                isUploadTask: true
            };
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            });
        });
    };
    ProjectFromTemplateService.prototype.getTeams = function (isCampaignProject) {
        var _this = this;
        return new Observable(function (observer) {
            if (isCampaignProject) {
                _this.appService.invokeRequest(_this.TEAM_METHOD_NS, _this.GET_ALL_TEAMS_WS_METHOD_NAME, null)
                    .subscribe(function (response) {
                    var teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    observer.error(eventData);
                });
            }
            else {
                var roleDetailObj = _this.sharingService.getRoleByName(MPM_ROLES.MANAGER);
                var param = {
                    roleDN: roleDetailObj.ROLE_DN
                };
                _this.appService.invokeRequest(_this.TEAM_BPM_METHOD_NS, _this.GET_TEAM_WS_METHOD_NAME, param)
                    .subscribe(function (response) {
                    var teams = acronui.findObjectsByProp(response, 'MPM_Teams');
                    observer.next(teams);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: 'Something went wrong while fetching Teams', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    observer.error(eventData);
                });
            }
        });
    };
    ProjectFromTemplateService.prototype.getTeam = function (teamId) {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                TeamId: teamId
            };
            _this.appService.invokeRequest(_this.TEAM_BPM_METHOD_NS, _this.GET_TEAM_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.Team) {
                    observer.next(response.Team);
                    observer.complete();
                }
            }, function (error) {
                observer.error(error);
                var eventData = {
                    message: 'Something went wrong while fetching Team',
                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                };
            });
        });
    };
    ProjectFromTemplateService.prototype.getTasks = function (projectId) {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                projectID: projectId
            };
            _this.appService.invokeRequest(_this.TASK_DETAILS_METHOD_NS, _this.GET_TASK_BY_PROJECT_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.Task) {
                    if (!Array.isArray(response.Task)) {
                        response.Task = [response.Task];
                    }
                    observer.next(response.Task);
                    observer.complete();
                }
                observer.next(false);
                observer.complete();
            }, function (error) {
                observer.error(error);
                var eventData = { message: 'Something went wrong while fetching Tasks', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            });
        });
    };
    ProjectFromTemplateService.prototype.getDeliverables = function (projectId) {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                ProjectID: projectId
            };
            _this.appService.invokeRequest(_this.DELIVERABLE_DETAILS_METHOD_NS, _this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.Deliverable) {
                    if (!Array.isArray(response.Deliverable)) {
                        response.Deliverable = [response.Deliverable];
                    }
                    observer.next(response.Deliverable);
                    observer.complete();
                }
                observer.next(false);
                observer.complete();
            }, function (error) {
                observer.error(error);
                var eventData = { message: 'Something went wrong while fetching Deliverables', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            });
        });
    };
    ProjectFromTemplateService.prototype.getDeliverableByProjectFolderId = function (projectFolderId) {
        var searchConditionList = JSON.parse(JSON.stringify(DeliverableConstants.DELIVERABLE_SEARCH_CONDITION));
        var skip = 0;
        var top = 100;
        var otmmAllActiveTasksSearchConfig = {
            search_condition_list: searchConditionList,
            load_type: 'metadata',
            child_count_load_type: 'folders',
            level_of_detail: 'full',
            folder_filter: projectFolderId
        };
        return this.otmmService.searchCustomText(otmmAllActiveTasksSearchConfig, skip, top, otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString());
    };
    ProjectFromTemplateService.prototype.checkProjectName = function (projectSearchCondition, searchKeyword, viewConfigName) {
        var _this = this;
        return new Observable(function (observer) {
            var viewConfig;
            if (viewConfigName) {
                viewConfig = viewConfigName; //this.sharingService.getViewConfigById(viewConfigId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.PROJECT;
            }
            _this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetails) {
                var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                var parameters = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: searchKeyword,
                    search_condition_list: {
                        search_condition: projectSearchCondition
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: []
                    },
                    cursor: {
                        page_index: 0,
                        page_size: 999
                    }
                };
                _this.indexerService.search(parameters).subscribe(function (response) {
                    if (response && response.data && response.data.length) {
                        var hasSameProjectName_1 = false;
                        var indexerField_1 = _this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                        response.data.forEach(function (project) {
                            if (project[indexerField_1] === searchKeyword) {
                                hasSameProjectName_1 = true;
                            }
                        });
                        if (hasSameProjectName_1) {
                            observer.next(true);
                        }
                        else {
                            observer.next(false);
                        }
                    }
                    else {
                        observer.next(false);
                    }
                    observer.complete();
                }, function (error) {
                    _this.loaderService.hide();
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    };
    ProjectFromTemplateService.prototype.createProjectFromTemplate = function (CreateProjectFromSourceObject) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.PROJECT_BPM_METHOD_NS, _this.CREATE_PROJECT_FROM_TEMPLATE_METHOD_NAME, CreateProjectFromSourceObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectFromTemplateService.prototype.createTemplateFromTemplate = function (CreateTemplateFromSourceObject) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TEMPLATE_BPM_METHOD_NS, _this.CREATE_TEMPLATE_FROM_TEMPLATE_METHOD_NAME, CreateTemplateFromSourceObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    ProjectFromTemplateService.prototype.getOwnerListByTeam = function (teamName, isCampaignProject) {
        var _this = this;
        return new Observable(function (observer) {
            var projectOwnerList = [];
            _this.getTeams(isCampaignProject)
                .subscribe(function (teamList) {
                var hasTeam = false;
                teamList.map(function (team) {
                    if (team.NAME === teamName) {
                        hasTeam = true;
                        var teamId = team['MPM_Teams-id'].Id;
                        _this.getProjectOwners(teamId)
                            .subscribe(function (response) {
                            if (response && response['users'] && response['users'].user) {
                                response['users'].user.map(function (user) {
                                    user.displayName = user.name;
                                    user.value = user.cn;
                                    user.id = user.cn;
                                });
                                projectOwnerList = response['users'].user;
                            }
                            observer.next(projectOwnerList);
                            observer.complete();
                        }, function (error) {
                            observer.next(projectOwnerList);
                            observer.complete();
                        });
                    }
                });
                if (!hasTeam) {
                    observer.next(projectOwnerList);
                    observer.complete();
                }
            }, function (error) {
                observer.next(projectOwnerList);
                observer.complete();
            });
        });
    };
    ProjectFromTemplateService.prototype.validateProjectTaskandDeliverable = function (templateObj, templateform, projectDependency, deliverableViewName) {
        var _this = this;
        if (templateObj) {
            var viewConfig_1;
            var templateProjectItemId_1 = this.fieldConfigService.getFeildValueByMapperName(templateObj, this.metadataConstants.MPM_PROJECT_FEILDS.PROJECT_ITEM_ID);
            if (templateProjectItemId_1) {
                var templateProjectId = templateProjectItemId_1.split('.')[1];
                this.getTasks(templateProjectId)
                    .subscribe(function (response) {
                    var tasks = response;
                    if (!tasks || !tasks.length || tasks.length === 0) {
                        templateform.controls.copyTask.patchValue(false);
                        templateform.controls.copyDeliverable.patchValue(false);
                        templateform.controls.copyTask.disable();
                        templateform.controls.copyDeliverable.disable();
                        projectDependency.isNoTasks = true;
                        projectDependency.isNoDeliverables = true;
                        projectDependency.isTaskWithTaskWithDeliverableDependency = false;
                        return;
                    }
                    else {
                        templateform.controls.copyTask.patchValue(true);
                        templateform.controls.copyDeliverable.patchValue(true);
                        templateform.controls.copyTask.enable();
                        projectDependency.isNoTasks = false;
                        projectDependency.isNoDeliverables = false;
                        projectDependency.isTaskWithTaskWithDeliverableDependency = false;
                    }
                    if (deliverableViewName) {
                        viewConfig_1 = deliverableViewName;
                    }
                    else {
                        viewConfig_1 = SearchConfigConstants.SEARCH_NAME.DELIVERABLE;
                    }
                    _this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig_1).subscribe(function (viewDetails) {
                        var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                            ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                        var searchConditionList = [];
                        var displayField = _this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_PROJECT_ID, MPM_LEVELS.DELIVERABLE);
                        searchConditionList.push({
                            type: displayField ? displayField.DATA_TYPE : 'string',
                            field_id: displayField.INDEXER_FIELD_ID,
                            relational_operator_id: MPMSearchOperators.IS,
                            relational_operator_name: MPMSearchOperatorNames.IS,
                            value: templateProjectItemId_1
                        });
                        var indexerField = _this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                        var parametersReq = {
                            search_config_id: searchConfig ? searchConfig : null,
                            keyword: '',
                            search_condition_list: {
                                search_condition: searchConditionList
                            },
                            facet_condition_list: {
                                facet_condition: []
                            },
                            sorting_list: {
                                sort: [{
                                        field_id: indexerField,
                                        order: 'ASC'
                                    }]
                            },
                            cursor: {
                                page_index: 0,
                                page_size: 2
                            }
                        };
                        _this.indexerService.search(parametersReq).subscribe(function (responseData) {
                            if (responseData.data.length > 0) {
                                templateform.controls.copyDeliverable.enable();
                            }
                            else {
                                templateform.controls.copyDeliverable.patchValue(false);
                                templateform.controls.copyDeliverable.disable();
                                projectDependency.isNoDeliverables = true;
                            }
                        });
                    });
                }, function (error) {
                });
            }
        }
        else {
            var eventData = { message: 'Invalid project information supplied to validate task and deliverables', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
        }
    };
    ProjectFromTemplateService.prototype.copyTemplate = function (sourceId, templateform, templateSearchCondition, customMetadata, projectOwner, viewConfigName, enableDuplicateNames) {
        var _this = this;
        return new Observable(function (observer) {
            if (sourceId) {
                var ProjectObject_1;
                if (projectOwner) {
                    var userId = projectOwner.value;
                    ProjectObject_1 = {
                        SourceID: sourceId.split('.')[1],
                        ProjectName: templateform.value.projectName,
                        ProjectOwner: userId,
                        ProjectDescription: templateform.value.description,
                        IsCopyTask: templateform.value.copyTask ? templateform.value.copyTask : false,
                        IsCopyDeliverable: templateform.value.copyDeliverable ? templateform.value.copyDeliverable : false,
                        CustomMetadata: customMetadata,
                        CampaignID: templateform.getRawValue().campaign.value,
                        CategoryMetadataID: templateform.getRawValue().categoryMetadata.value,
                        isCopyWorkflowRulesNeeded: templateform.controls.copyWorflowRules.value
                    };
                    _this.checkProjectName(templateSearchCondition, ProjectObject_1.ProjectName, viewConfigName)
                        .subscribe(function (response) {
                        if (response && enableDuplicateNames === true) {
                            var eventData = {
                                message: 'Template already exists',
                                type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                            };
                            observer.next(eventData);
                            observer.complete();
                        }
                        else {
                            var requestObject = {
                                CreateProjectFromSourceObject: ProjectObject_1
                            };
                            var eventData_1 = {
                                message: 'Create Template has been initiated sucessfully',
                                type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS
                            };
                            _this.createTemplateFromTemplate(ProjectObject_1)
                                .subscribe(function (createProjectResponse) {
                                if (createProjectResponse && createProjectResponse['APIResponse']) {
                                    if (createProjectResponse['APIResponse'].statusCode === '500') {
                                        eventData_1 = {
                                            message: createProjectResponse['APIResponse'].error.errorMessage,
                                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                        };
                                        observer.next(eventData_1);
                                        observer.complete();
                                    }
                                    else if (createProjectResponse['APIResponse'].statusCode === '200') {
                                        observer.next(eventData_1);
                                        observer.complete();
                                    }
                                }
                            }, function (error) {
                                var eventData = {
                                    message: 'Something went wrong while copying template',
                                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                };
                                observer.next(eventData);
                                observer.complete();
                            });
                        }
                    });
                    // });
                }
            }
        });
    };
    ProjectFromTemplateService.prototype.createProject = function (sourceId, templateform, projectSearchCondition, customMetadata, projectOwner, viewConfigName, enableDuplicateNames) {
        var _this = this;
        return new Observable(function (observer) {
            if (sourceId) {
                var ProjectObject_2;
                if (projectOwner) {
                    var userId = projectOwner.value;
                    // templateform.value.projectOwner ? templateform.value.projectOwner.cn : 
                    // this.getUserDetailsByUserCn(ownerCn)
                    //     .subscribe(userId => {
                    ProjectObject_2 = {
                        TemplateID: sourceId.split('.')[1],
                        ProjectName: templateform.value.projectName,
                        ProjectOwner: userId,
                        ProjectDescription: templateform.value.description,
                        ExpectedProjectDuration: templateform.value.expectedDuration,
                        ProjectStartDate: templateform.value.projectStartDate,
                        ProjectDueDate: templateform.value.projectEndDate,
                        TaskStartDate: templateform.value.taskStartDate,
                        TaskDueDate: templateform.controls.taskEndDate.value,
                        IsCopyTask: templateform.value.copyTask || false,
                        IsCopyDeliverable: templateform.value.copyDeliverable || false,
                        CustomMetadata: customMetadata,
                        CampaignID: templateform.getRawValue().campaign ?
                            (templateform.getRawValue().campaign.value ? templateform.getRawValue().campaign.value : templateform.getRawValue().campaign.split(/-(.+)/)[0]) : '',
                        CategoryMetadataID: templateform.getRawValue().categoryMetadata ?
                            (templateform.getRawValue().categoryMetadata.value ? templateform.getRawValue().categoryMetadata.value : templateform.getRawValue().categoryMetadata) : ''
                    };
                    _this.checkProjectName(projectSearchCondition, ProjectObject_2.ProjectName, viewConfigName)
                        .subscribe(function (response) {
                        if (response && enableDuplicateNames == 'false') {
                            var eventData = {
                                message: 'Project already exists',
                                type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                            };
                            observer.next(eventData);
                            observer.complete();
                        }
                        else {
                            var requestObject = {
                                CreateProjectFromSourceObject: ProjectObject_2
                            };
                            var eventData_2 = {
                                message: 'Create Project has been initiated sucessfully',
                                type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS
                            };
                            _this.createProjectFromTemplate(ProjectObject_2)
                                .subscribe(function (createProjectResponse) {
                                if (createProjectResponse && createProjectResponse['APIResponse']) {
                                    if (createProjectResponse['APIResponse'].statusCode === '500') {
                                        eventData_2 = {
                                            message: createProjectResponse['APIResponse'].error.errorMessage,
                                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                        };
                                        observer.next(eventData_2);
                                        observer.complete();
                                    }
                                    else if (createProjectResponse['APIResponse'].statusCode === '200') {
                                        observer.next(eventData_2);
                                        observer.complete();
                                    }
                                }
                            }, function (error) {
                                var eventData = {
                                    message: 'Something went wrong while copying project',
                                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                                };
                                observer.next(eventData);
                                observer.complete();
                            });
                        }
                    });
                    // });
                }
            }
        });
    };
    ProjectFromTemplateService.prototype.putCustomMetadata = function (customFieldsGroup, templateObj) {
        var e_1, _a;
        var costomMetadataValues = [];
        if (customFieldsGroup && customFieldsGroup['controls'] && customFieldsGroup['controls']['fieldset']
            && customFieldsGroup['controls']['fieldset'].controls
            && customFieldsGroup['controls']['fieldset'].controls.length > 0) {
            try {
                for (var _b = __values(customFieldsGroup['controls']['fieldset'].controls), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var control = _c.value;
                    if (this.getProperty(templateObj, control.fieldset.id)) {
                        control.value = this.getProperty(templateObj, control.fieldset.id);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return customFieldsGroup;
    };
    ProjectFromTemplateService.prototype.getCustomMetadata = function (customFieldsGroup) {
        var e_2, _a;
        var _b, _c;
        var costomMetadataValues = [];
        if (customFieldsGroup && customFieldsGroup['controls'] && customFieldsGroup['controls']['fieldset']
            && customFieldsGroup['controls']['fieldset'].controls
            && customFieldsGroup['controls']['fieldset'].controls.length > 0) {
            var controls = customFieldsGroup['controls']['fieldset'].controls;
            var _loop_1 = function (control) {
                if ((((_b = control) === null || _b === void 0 ? void 0 : _b.value) !== null && ((_c = control) === null || _c === void 0 ? void 0 : _c.value) !== '')) {
                    var metadataFieldObj_1 = {};
                    metadataFieldObj_1['dataType'] = this_1.otmmMetadataService.getMetadataFieldType(control.fieldset.data_type);
                    /*metadataFieldObj['defaultValue'] = control.fieldset.data_type === ProjectConstant.METADATA_EDIT_TYPES.DATE ?
                        this.utilService.convertToOTMMDateFormat(control.value) : control.value;*/
                    metadataFieldObj_1['defaultValue'] = control.value;
                    metadataFieldObj_1['fieldId'] = control.fieldset.id;
                    metadataFieldObj_1['fieldType'] = control.fieldset.type;
                    metadataFieldObj_1['referenceValue'] = 'defaultReference';
                    metadataFieldObj_1['domainFieldValue'] = null;
                    if (control.fieldset && control.fieldset.edit_type && control.fieldset.values
                        && Array.isArray(control.fieldset.values) && control.fieldset.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
                        control.fieldset.values.forEach(function (data) {
                            if (data.field_value && data.field_value.value && data.field_value.value === metadataFieldObj_1['defaultValue']) {
                                metadataFieldObj_1['domainFieldValue'] = data.display_value;
                            }
                        });
                    }
                    costomMetadataValues.push(metadataFieldObj_1);
                }
            };
            var this_1 = this;
            try {
                for (var controls_1 = __values(controls), controls_1_1 = controls_1.next(); !controls_1_1.done; controls_1_1 = controls_1.next()) {
                    var control = controls_1_1.value;
                    _loop_1(control);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (controls_1_1 && !controls_1_1.done && (_a = controls_1.return)) _a.call(controls_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        return { Metadata: { metadataFields: costomMetadataValues } };
    };
    ProjectFromTemplateService.prototype.getMaxMinValueForCustomMetadata = function (dataLength, scale) {
        var data = Math.abs(dataLength - scale);
        var numericValue = newArray(data).fill('9').join('');
        if (scale) {
            numericValue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericValue) : parseInt(numericValue);
    };
    ProjectFromTemplateService.prototype.mapCustomMetadataForm = function (metadataField, templateObj) {
        var validators = [];
        var value = (templateObj) ?
            this.otmmMetadataService.getFieldValueById(templateObj.metadata, metadataField.id) : '';
        var formControlObj = new FormControl({
            value: value ? value : '',
            disabled: !metadataField.editable,
        });
        if (metadataField.required) {
            validators.push(Validators.required);
        }
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
            validators.push(DateValidators.dateFormat);
        }
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
            metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
            validators.push(Validators.maxLength(metadataField.data_length));
        }
        formControlObj.setValidators(validators);
        formControlObj.updateValueAndValidity();
        formControlObj['fieldset'] = metadataField;
        formControlObj['name'] = metadataField.name;
        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
            metadataField.values = this.utilService.getLookupDomainValuesById(metadataField.domain_id);
        }
        return formControlObj;
    };
    ProjectFromTemplateService.prototype.getOTMMCustomMetaData = function (customMetadataObj, formGroup, templateObj, categoryLevelDetail) {
        var _this = this;
        return new Observable(function (observer) {
            var categoryLevelDetails = _this.categoryService.getCategoryLevelDetailsByType(MPM_LEVELS.PROJECT);
            if (categoryLevelDetail) {
                categoryLevelDetails = categoryLevelDetail;
            }
            _this.loaderService.show();
            _this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
                .subscribe(function (metaDataModelDetails) {
                var e_3, _a, e_4, _b;
                customMetadataObj.fieldsetGroup = [];
                customMetadataObj.fieldGroups = [];
                customMetadataObj.customMetadataFields = [];
                if (metaDataModelDetails && metaDataModelDetails.metadata_element_list &&
                    Array.isArray(metaDataModelDetails.metadata_element_list)) {
                    try {
                        for (var _c = __values(metaDataModelDetails.metadata_element_list), _d = _c.next(); !_d.done; _d = _c.next()) {
                            var metadataFieldGroup = _d.value;
                            var formControlArray = [];
                            if (metadataFieldGroup.id === ProjectConstant.MPM_PROJECT_METADATA_GROUP) {
                                continue;
                            }
                            if (metadataFieldGroup.metadata_element_list &&
                                Array.isArray(metadataFieldGroup.metadata_element_list)) {
                                try {
                                    for (var _e = (e_4 = void 0, __values(metadataFieldGroup.metadata_element_list)), _f = _e.next(); !_f.done; _f = _e.next()) {
                                        var metadataField = _f.value;
                                        customMetadataObj.customMetadataFields.push(metadataField);
                                        var validators = [];
                                        var value = (templateObj) ?
                                            _this.fieldConfigService.getIndexerIdByOTMMId(metadataField.id) : '';
                                        value = templateObj[value] ? templateObj[value] : '';
                                        var formControlObj = new FormControl({
                                            value: typeof value !== 'undefined' ? value : '',
                                            disabled: !metadataField.editable,
                                        });
                                        if (metadataField.required) {
                                            validators.push(Validators.required);
                                        }
                                        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.DATE) {
                                            validators.push(DateValidators.dateFormat);
                                        }
                                        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.SIMPLE ||
                                            metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.TEXTAREA) {
                                            validators.push(Validators.maxLength(metadataField.data_length));
                                        }
                                        if (metadataField.data_type === ProjectConstant.METADATA_EDIT_TYPES.NUMBER) {
                                            var numericValue = _this.getMaxMinValueForCustomMetadata(metadataField.data_length, metadataField.scale);
                                            validators.push(Validators.max(numericValue), Validators.min(-(numericValue)));
                                        }
                                        formControlObj.setValidators(validators);
                                        formControlObj.updateValueAndValidity();
                                        formControlObj['fieldset'] = metadataField;
                                        formControlObj['name'] = metadataField.name;
                                        if (metadataField.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO) {
                                            metadataField.values = _this.utilService.getLookupDomainValuesById(metadataField.domain_id);
                                        }
                                        customMetadataObj.fieldsetGroup.push(formControlObj);
                                        // new code
                                        formControlArray.push(formControlObj);
                                    }
                                }
                                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                                finally {
                                    try {
                                        if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                                    }
                                    finally { if (e_4) throw e_4.error; }
                                }
                            }
                            var fieldGroup = formGroup.group({ fieldset: new FormArray(formControlArray) });
                            fieldGroup['name'] = metadataFieldGroup.name;
                            customMetadataObj.fieldGroups.push(fieldGroup);
                        }
                    }
                    catch (e_3_1) { e_3 = { error: e_3_1 }; }
                    finally {
                        try {
                            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                        }
                        finally { if (e_3) throw e_3.error; }
                    }
                }
                _this.loaderService.hide();
                observer.next(customMetadataObj);
                observer.complete();
            }, function (error) {
                _this.loaderService.hide();
                observer.error(customMetadataObj);
            });
        });
    };
    ProjectFromTemplateService.prototype.checkTaskAccess = function (projectDependency, templateform) {
        if (!projectDependency.selectedObject && templateform.controls.copyTask.value) {
            templateform.controls.copyTask.patchValue(false);
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy task option. Please select a template.';
        }
        if (projectDependency.isNoTasks && templateform.controls.copyTask.value) {
            templateform.controls.copyTask.patchValue(false);
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy task option. No tasks are present.';
        }
        if (templateform.controls.copyTask.value && projectDependency.isTaskWithTaskWithDeliverableDependency) {
            templateform.controls.copyDeliverable.patchValue(true);
            return '';
        }
        if (!templateform.controls.copyTask.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return '';
        }
    };
    ProjectFromTemplateService.prototype.checkDeliverableAccess = function (projectDependency, templateform) {
        if (!projectDependency.selectedObject && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. Please select a template.';
        }
        if (projectDependency.isNoDeliverables && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. No Deliverables are present.';
        }
        if (!templateform.controls.copyTask.value && templateform.controls.copyDeliverable.value) {
            templateform.controls.copyDeliverable.patchValue(false);
            return 'Cannot enable copy deliverable option. Enable copy task option to enable copy deliverble.';
        }
        if (!templateform.controls.copyDeliverable.value && projectDependency.isTaskWithTaskWithDeliverableDependency) {
            templateform.controls.copyDeliverable.patchValue(true);
            return 'Cannot disable copy deliverable option. Selected project contains Approval Tasks, which has deliverable dependencies.';
        }
        if (templateform.controls.copyDeliverable.value && !projectDependency.isNoDeliverables) {
            templateform.controls.copyTask.patchValue(true);
            return '';
        }
    };
    ProjectFromTemplateService.prototype.getAllTemplates = function (templateSearchCondition, viewConfigName) {
        var _this = this;
        return new Observable(function (observer) {
            var viewConfig;
            if (viewConfigName) {
                viewConfig = viewConfigName; //this.sharingService.getViewConfigById(viewConfigId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.PROJECT;
            }
            _this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetails) {
                var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                var indexerField = _this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_NAME);
                var parametersReq = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: '',
                    search_condition_list: {
                        search_condition: templateSearchCondition
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: [{
                                field_id: indexerField,
                                order: 'ASC'
                            }]
                    },
                    cursor: {
                        page_index: 0,
                        page_size: 100
                    }
                };
                _this.indexerService.search(parametersReq).subscribe(function (response) {
                    var templateObj = {
                        allTemplateCount: response.cursor.total_records,
                        allTemplateList: response.data
                    };
                    observer.next(templateObj);
                    observer.complete();
                }, function (error) {
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    };
    ProjectFromTemplateService.ctorParameters = function () { return [
        { type: AppService },
        { type: CategoryService },
        { type: OTMMService },
        { type: UtilService },
        { type: ProjectService },
        { type: OtmmMetadataService },
        { type: SharingService },
        { type: LoaderService },
        { type: IndexerService },
        { type: ViewConfigService },
        { type: FieldConfigService }
    ]; };
    ProjectFromTemplateService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectFromTemplateService_Factory() { return new ProjectFromTemplateService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.CategoryService), i0.ɵɵinject(i3.OTMMService), i0.ɵɵinject(i4.UtilService), i0.ɵɵinject(i5.ProjectService), i0.ɵɵinject(i6.OtmmMetadataService), i0.ɵɵinject(i7.SharingService), i0.ɵɵinject(i8.LoaderService), i0.ɵɵinject(i9.IndexerService), i0.ɵɵinject(i10.ViewConfigService), i0.ɵɵinject(i11.FieldConfigService)); }, token: ProjectFromTemplateService, providedIn: "root" });
    ProjectFromTemplateService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ProjectFromTemplateService);
    return ProjectFromTemplateService;
}());
export { ProjectFromTemplateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1mcm9tLXRlbXBsYXRlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LWZyb20tdGVtcGxhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUV2RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDckYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRzdFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUM1RCxPQUFPLEtBQUssT0FBTyxNQUFNLGlDQUFpQyxDQUFDO0FBQzNELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUcvRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDbEYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFFekYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDakYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdFQUF3RSxDQUFDO0FBQzVHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQzs7Ozs7Ozs7Ozs7OztBQU9wSDtJQXlCSSxvQ0FDVyxVQUFzQixFQUN0QixlQUFnQyxFQUNoQyxXQUF3QixFQUN4QixXQUF3QixFQUN4QixjQUE4QixFQUM5QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsaUJBQW9DLEVBQ3BDLGtCQUFzQztRQVZ0QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWxDakQsMkJBQXNCLEdBQUcsa0RBQWtELENBQUM7UUFDNUUsdUNBQWtDLEdBQUcsMEJBQTBCLENBQUM7UUFFaEUsMEJBQXFCLEdBQUcsc0RBQXNELENBQUM7UUFDL0UsNkNBQXdDLEdBQUcsMkJBQTJCLENBQUM7UUFFdkUsa0ZBQWtGO1FBQ2xGLDJCQUFzQixHQUFHLHNEQUFzRCxDQUFDO1FBQ2hGLDhDQUF5QyxHQUFHLDBCQUEwQixDQUFDO1FBRXZFLHVCQUFrQixHQUFHLDhDQUE4QyxDQUFDO1FBQ3BFLDRCQUF1QixHQUFHLGlCQUFpQixDQUFDO1FBQzVDLG1CQUFjLEdBQUcsb0RBQW9ELENBQUM7UUFDdEUsaUNBQTRCLEdBQUcsYUFBYSxDQUFDO1FBRTdDLGtDQUE2QixHQUFHLHNEQUFzRCxDQUFDO1FBQ3ZGLCtDQUEwQyxHQUFHLDZCQUE2QixDQUFDO1FBRTNFLHVDQUFrQyxHQUFHLG9CQUFvQixDQUFDO1FBQzFELDJCQUFzQixHQUFHLCtDQUErQyxDQUFDO1FBRXpFLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDO0lBY2xDLENBQUM7SUFFTCxnREFBVyxHQUFYLFVBQVksR0FBRyxFQUFFLElBQUk7UUFDakIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7WUFDaEMsT0FBTztTQUNWO1FBQ0QsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELDJEQUFzQixHQUF0QixVQUF1QixNQUFNO1FBQTdCLGlCQWdCQztRQWZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFNLFVBQVUsR0FBRztnQkFDZixNQUFNLEVBQUUsTUFBTTthQUNqQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUM7aUJBQy9DLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7b0JBQzFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDaEQsWUFBWSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDakU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDNUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscURBQWdCLEdBQWhCLFVBQWlCLE1BQU07UUFBdkIsaUJBZ0JDO1FBZkcsSUFBTSxhQUFhLEdBQVUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2xGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sb0JBQW9CLEdBQUc7Z0JBQ3pCLGNBQWMsRUFBRSxNQUFNO2dCQUN0QixNQUFNLEVBQUUsYUFBYSxDQUFDLE9BQU87Z0JBQzdCLE1BQU0sRUFBRSxNQUFNO2dCQUNkLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixZQUFZLEVBQUUsSUFBSTthQUNyQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2hELFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQVEsR0FBUixVQUFTLGlCQUFpQjtRQUExQixpQkE0QkM7UUEzQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxpQkFBaUIsRUFBRTtnQkFDbkIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxLQUFJLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDO3FCQUN0RixTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNmLElBQU0sS0FBSyxHQUFnQixPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDNUgsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxJQUFNLGFBQWEsR0FBVSxLQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ2xGLElBQU0sS0FBSyxHQUFHO29CQUNWLE1BQU0sRUFBRSxhQUFhLENBQUMsT0FBTztpQkFDaEMsQ0FBQztnQkFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLHVCQUF1QixFQUFFLEtBQUssQ0FBQztxQkFDdEYsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixJQUFNLEtBQUssR0FBZ0IsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDNUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzVILFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCw0Q0FBTyxHQUFQLFVBQVEsTUFBTTtRQUFkLGlCQW1CQztRQWxCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLGdCQUFnQixHQUFHO2dCQUNyQixNQUFNLEVBQUUsTUFBTTthQUNqQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLEtBQUksQ0FBQyx1QkFBdUIsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDakcsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxFQUFFO29CQUMzQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsSUFBTSxTQUFTLEdBQUc7b0JBQ2QsT0FBTyxFQUFFLDBDQUEwQztvQkFDbkQsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO2lCQUNsRCxDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBUSxHQUFSLFVBQVMsU0FBUztRQUFsQixpQkFxQkM7UUFwQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxnQkFBZ0IsR0FBRztnQkFDckIsU0FBUyxFQUFFLFNBQVM7YUFDdkIsQ0FBQztZQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxLQUFJLENBQUMsa0NBQWtDLEVBQUUsZ0JBQWdCLENBQUM7aUJBQ2hILFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtvQkFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUMvQixRQUFRLENBQUMsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNuQztvQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hJLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0Qsb0RBQWUsR0FBZixVQUFnQixTQUFTO1FBQXpCLGlCQXFCQztRQXBCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLGdCQUFnQixHQUFHO2dCQUNyQixTQUFTLEVBQUUsU0FBUzthQUN2QixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLDZCQUE2QixFQUFFLEtBQUksQ0FBQywwQ0FBMEMsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDL0gsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO29CQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7d0JBQ3RDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7cUJBQ2pEO29CQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGtEQUFrRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkksQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvRUFBK0IsR0FBL0IsVUFBZ0MsZUFBZTtRQUMzQyxJQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7UUFFMUcsSUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ2YsSUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBRWhCLElBQU0sOEJBQThCLEdBQUc7WUFDbkMscUJBQXFCLEVBQUUsbUJBQW1CO1lBQzFDLFNBQVMsRUFBRSxVQUFVO1lBQ3JCLHFCQUFxQixFQUFFLFNBQVM7WUFDaEMsZUFBZSxFQUFFLE1BQU07WUFDdkIsYUFBYSxFQUFFLGVBQWU7U0FDakMsQ0FBQztRQUdGLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyw4QkFBOEIsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUM5RSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRUQscURBQWdCLEdBQWhCLFVBQWlCLHNCQUFzQixFQUFFLGFBQWEsRUFBRSxjQUFjO1FBQXRFLGlCQXFEQztRQXBERyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLFVBQVUsQ0FBQztZQUNmLElBQUksY0FBYyxFQUFFO2dCQUNoQixVQUFVLEdBQUcsY0FBYyxDQUFDLENBQUEsMkRBQTJEO2FBQzFGO2lCQUFNO2dCQUNILFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO2FBQzFEO1lBQ0QsS0FBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFdBQXVCO2dCQUNqRyxJQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsMkJBQTJCLElBQUksT0FBTyxXQUFXLENBQUMsMkJBQTJCLEtBQUssUUFBUTtvQkFDdkgsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsMkJBQTJCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkUsSUFBTSxVQUFVLEdBQWtCO29CQUM5QixnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtvQkFDcEQsT0FBTyxFQUFFLGFBQWE7b0JBQ3RCLHFCQUFxQixFQUFFO3dCQUNuQixnQkFBZ0IsRUFBRSxzQkFBc0I7cUJBQzNDO29CQUNELG9CQUFvQixFQUFFO3dCQUNsQixlQUFlLEVBQUUsRUFBRTtxQkFDdEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLElBQUksRUFBRSxFQUFFO3FCQUNYO29CQUNELE1BQU0sRUFBRTt3QkFDSixVQUFVLEVBQUUsQ0FBQzt3QkFDYixTQUFTLEVBQUUsR0FBRztxQkFDakI7aUJBQ0osQ0FBQztnQkFDRixLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxRQUF3QjtvQkFDdEUsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTt3QkFDbkQsSUFBSSxvQkFBa0IsR0FBRyxLQUFLLENBQUM7d0JBQy9CLElBQU0sY0FBWSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDMUgsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPOzRCQUN6QixJQUFJLE9BQU8sQ0FBQyxjQUFZLENBQUMsS0FBSyxhQUFhLEVBQUU7Z0NBQ3pDLG9CQUFrQixHQUFHLElBQUksQ0FBQzs2QkFDN0I7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBQ0gsSUFBSSxvQkFBa0IsRUFBRTs0QkFDcEIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDdkI7NkJBQU07NEJBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDeEI7cUJBQ0o7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDeEI7b0JBQ0QsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FBQztnQkFDbkUsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUVQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhEQUF5QixHQUF6QixVQUEwQiw2QkFBNkI7UUFBdkQsaUJBVUM7UUFURyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMscUJBQXFCLEVBQUUsS0FBSSxDQUFDLHdDQUF3QyxFQUFFLDZCQUE2QixDQUFDO2lCQUNsSSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwrREFBMEIsR0FBMUIsVUFBMkIsOEJBQThCO1FBQXpELGlCQVVDO1FBVEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLEtBQUksQ0FBQyx5Q0FBeUMsRUFBRSw4QkFBOEIsQ0FBQztpQkFDckksU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdURBQWtCLEdBQWxCLFVBQW1CLFFBQVEsRUFBRSxpQkFBaUI7UUFBOUMsaUJBcUNDO1FBcENHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUM7aUJBQzNCLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNwQixRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtvQkFDYixJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO3dCQUN4QixPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ3ZDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7NkJBQ3hCLFNBQVMsQ0FBQyxVQUFBLFFBQVE7NEJBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUU7Z0NBQ3pELFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtvQ0FDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29DQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7b0NBQ3JCLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztnQ0FDdEIsQ0FBQyxDQUFDLENBQUM7Z0NBQ0gsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQzs2QkFDN0M7NEJBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUNoQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7NEJBQ0osUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUNoQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ3hCLENBQUMsQ0FBQyxDQUFDO3FCQUNWO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUNoQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ2hDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNFQUFpQyxHQUFqQyxVQUFrQyxXQUFXLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLG1CQUFtQjtRQUFuRyxpQkFpRkM7UUFoRkcsSUFBSSxXQUFXLEVBQUU7WUFDYixJQUFJLFlBQVUsQ0FBQztZQUNmLElBQU0sdUJBQXFCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDeEosSUFBSSx1QkFBcUIsRUFBRTtnQkFDdkIsSUFBTSxpQkFBaUIsR0FBRyx1QkFBcUIsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUM7cUJBQzNCLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBTSxLQUFLLEdBQUcsUUFBUSxDQUFDO29CQUN2QixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDL0MsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNqRCxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3hELFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUN6QyxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDaEQsaUJBQWlCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzt3QkFDbkMsaUJBQWlCLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3dCQUMxQyxpQkFBaUIsQ0FBQyx1Q0FBdUMsR0FBRyxLQUFLLENBQUM7d0JBQ2xFLE9BQU87cUJBQ1Y7eUJBQU07d0JBQ0gsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNoRCxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3ZELFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUN4QyxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO3dCQUNwQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7d0JBQzNDLGlCQUFpQixDQUFDLHVDQUF1QyxHQUFHLEtBQUssQ0FBQztxQkFDckU7b0JBQ0QsSUFBSSxtQkFBbUIsRUFBRTt3QkFDckIsWUFBVSxHQUFHLG1CQUFtQixDQUFDO3FCQUNwQzt5QkFBTTt3QkFDSCxZQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQztxQkFDOUQ7b0JBQ0QsS0FBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFlBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFdBQXVCO3dCQUNqRyxJQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsMkJBQTJCLElBQUksT0FBTyxXQUFXLENBQUMsMkJBQTJCLEtBQUssUUFBUTs0QkFDdkgsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsMkJBQTJCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDbkUsSUFBTSxtQkFBbUIsR0FBRyxFQUFFLENBQUM7d0JBQy9CLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUNwRixpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsRUFBRSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQzdGLG1CQUFtQixDQUFDLElBQUksQ0FBQzs0QkFDckIsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUTs0QkFDdEQsUUFBUSxFQUFFLFlBQVksQ0FBQyxnQkFBZ0I7NEJBQ3ZDLHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLEVBQUU7NEJBQzdDLHdCQUF3QixFQUFFLHNCQUFzQixDQUFDLEVBQUU7NEJBQ25ELEtBQUssRUFBRSx1QkFBcUI7eUJBQy9CLENBQUMsQ0FBQzt3QkFDSCxJQUFNLFlBQVksR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQzFILElBQU0sYUFBYSxHQUFrQjs0QkFDakMsZ0JBQWdCLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUk7NEJBQ3BELE9BQU8sRUFBRSxFQUFFOzRCQUNYLHFCQUFxQixFQUFFO2dDQUNuQixnQkFBZ0IsRUFBRSxtQkFBbUI7NkJBQ3hDOzRCQUNELG9CQUFvQixFQUFFO2dDQUNsQixlQUFlLEVBQUUsRUFBRTs2QkFDdEI7NEJBQ0QsWUFBWSxFQUFFO2dDQUNWLElBQUksRUFBRSxDQUFDO3dDQUNILFFBQVEsRUFBRSxZQUFZO3dDQUN0QixLQUFLLEVBQUUsS0FBSztxQ0FDZixDQUFDOzZCQUNMOzRCQUNELE1BQU0sRUFBRTtnQ0FDSixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsQ0FBQzs2QkFDZjt5QkFDSixDQUFDO3dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFlBQTRCOzRCQUM3RSxJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQ0FDOUIsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUM7NkJBQ2xEO2lDQUFNO2dDQUNILFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDeEQsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7Z0NBQ2hELGlCQUFpQixDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzs2QkFDN0M7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDUixDQUFDLENBQUMsQ0FBQzthQUNWO1NBQ0o7YUFBTTtZQUNILElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHdFQUF3RSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDNUo7SUFDTCxDQUFDO0lBRUQsaURBQVksR0FBWixVQUFhLFFBQVEsRUFBRSxZQUFZLEVBQUUsdUJBQXVCLEVBQUUsY0FBYyxFQUFFLFlBQVksRUFBRSxjQUFjLEVBQUUsb0JBQW9CO1FBQWhJLGlCQWdFQztRQS9ERyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLGVBQWEsQ0FBQztnQkFDbEIsSUFBSSxZQUFZLEVBQUU7b0JBQ2QsSUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztvQkFDbEMsZUFBYSxHQUFHO3dCQUNaLFFBQVEsRUFBRSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDM0MsWUFBWSxFQUFFLE1BQU07d0JBQ3BCLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsV0FBVzt3QkFDbEQsVUFBVSxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFBLENBQUMsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSzt3QkFDNUUsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLO3dCQUNqRyxjQUFjLEVBQUUsY0FBYzt3QkFDOUIsVUFBVSxFQUFFLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSzt3QkFDckQsa0JBQWtCLEVBQUUsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLGdCQUFnQixDQUFDLEtBQUs7d0JBQ3JFLHlCQUF5QixFQUFFLFlBQVksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSztxQkFDMUUsQ0FBQztvQkFDRixLQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQXVCLEVBQUUsZUFBYSxDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUM7eUJBQ3BGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7d0JBQ2YsSUFBSSxRQUFRLElBQUksb0JBQW9CLEtBQUssSUFBSSxFQUFFOzRCQUMzQyxJQUFNLFNBQVMsR0FBRztnQ0FDZCxPQUFPLEVBQUUseUJBQXlCO2dDQUNsQyxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7NkJBQ2xELENBQUM7NEJBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzs0QkFDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3lCQUN2Qjs2QkFBTTs0QkFDSCxJQUFNLGFBQWEsR0FBRztnQ0FDbEIsNkJBQTZCLEVBQUUsZUFBYTs2QkFDL0MsQ0FBQzs0QkFDRixJQUFJLFdBQVMsR0FBRztnQ0FDWixPQUFPLEVBQUUsZ0RBQWdEO2dDQUN6RCxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU87NkJBQ3BELENBQUM7NEJBQ0YsS0FBSSxDQUFDLDBCQUEwQixDQUFDLGVBQWEsQ0FBQztpQ0FDekMsU0FBUyxDQUFDLFVBQUEscUJBQXFCO2dDQUM1QixJQUFJLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxFQUFFO29DQUMvRCxJQUFJLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7d0NBQzNELFdBQVMsR0FBRzs0Q0FDUixPQUFPLEVBQUUscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLFlBQVk7NENBQ2hFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSzt5Q0FDbEQsQ0FBQzt3Q0FDRixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVMsQ0FBQyxDQUFDO3dDQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUNBQ3ZCO3lDQUFNLElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3Q0FDbEUsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFTLENBQUMsQ0FBQzt3Q0FDekIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FDQUN2QjtpQ0FDSjs0QkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dDQUNKLElBQU0sU0FBUyxHQUFHO29DQUNkLE9BQU8sRUFBRSw2Q0FBNkM7b0NBQ3RELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSztpQ0FDbEQsQ0FBQztnQ0FDRixRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dDQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NEJBQ3hCLENBQUMsQ0FBQyxDQUFDO3lCQUNWO29CQUNMLENBQUMsQ0FBQyxDQUFDO29CQUNQLE1BQU07aUJBQ1Q7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtEQUFhLEdBQWIsVUFBYyxRQUFRLEVBQUUsWUFBWSxFQUFFLHNCQUFzQixFQUFFLGNBQWMsRUFBRSxZQUFZLEVBQUUsY0FBYyxFQUFFLG9CQUFvQjtRQUFoSSxpQkF5RUM7UUF4RUcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsSUFBSSxlQUFhLENBQUM7Z0JBQ2xCLElBQUksWUFBWSxFQUFFO29CQUNkLElBQU0sTUFBTSxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7b0JBQ2xDLDBFQUEwRTtvQkFDMUUsdUNBQXVDO29CQUN2Qyw2QkFBNkI7b0JBQzdCLGVBQWEsR0FBRzt3QkFDWixVQUFVLEVBQUUsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xDLFdBQVcsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLFdBQVc7d0JBQzNDLFlBQVksRUFBRSxNQUFNO3dCQUNwQixrQkFBa0IsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLFdBQVc7d0JBQ2xELHVCQUF1QixFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCO3dCQUM1RCxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLGdCQUFnQjt3QkFDckQsY0FBYyxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsY0FBYzt3QkFDakQsYUFBYSxFQUFFLFlBQVksQ0FBQyxLQUFLLENBQUMsYUFBYTt3QkFDL0MsV0FBVyxFQUFFLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7d0JBQ3BELFVBQVUsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLO3dCQUNoRCxpQkFBaUIsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLGVBQWUsSUFBSSxLQUFLO3dCQUM5RCxjQUFjLEVBQUUsY0FBYzt3QkFDOUIsVUFBVSxFQUFFLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDN0MsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3hKLGtCQUFrQixFQUFFLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUM3RCxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3FCQUNqSyxDQUFDO29CQUNGLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxlQUFhLENBQUMsV0FBVyxFQUFFLGNBQWMsQ0FBQzt5QkFDbkYsU0FBUyxDQUFDLFVBQUEsUUFBUTt3QkFDZixJQUFJLFFBQVEsSUFBSSxvQkFBb0IsSUFBSSxPQUFPLEVBQUU7NEJBQzdDLElBQU0sU0FBUyxHQUFHO2dDQUNkLE9BQU8sRUFBRSx3QkFBd0I7Z0NBQ2pDLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSzs2QkFDbEQsQ0FBQzs0QkFDRixRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDOzRCQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQ3ZCOzZCQUFNOzRCQUNILElBQU0sYUFBYSxHQUFHO2dDQUNsQiw2QkFBNkIsRUFBRSxlQUFhOzZCQUMvQyxDQUFDOzRCQUNGLElBQUksV0FBUyxHQUFHO2dDQUNaLE9BQU8sRUFBRSwrQ0FBK0M7Z0NBQ3hELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsT0FBTzs2QkFDcEQsQ0FBQzs0QkFDRixLQUFJLENBQUMseUJBQXlCLENBQUMsZUFBYSxDQUFDO2lDQUN4QyxTQUFTLENBQUMsVUFBQSxxQkFBcUI7Z0NBQzVCLElBQUkscUJBQXFCLElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLEVBQUU7b0NBQy9ELElBQUkscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3Q0FDM0QsV0FBUyxHQUFHOzRDQUNSLE9BQU8sRUFBRSxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWTs0Q0FDaEUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO3lDQUNsRCxDQUFDO3dDQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBUyxDQUFDLENBQUM7d0NBQ3pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQ0FDdkI7eUNBQU0sSUFBSSxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFFO3dDQUNsRSxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVMsQ0FBQyxDQUFDO3dDQUN6QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUNBQ3ZCO2lDQUNKOzRCQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0NBQ0osSUFBTSxTQUFTLEdBQUc7b0NBQ2QsT0FBTyxFQUFFLDRDQUE0QztvQ0FDckQsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO2lDQUNsRCxDQUFDO2dDQUNGLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0NBQ3pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs0QkFDeEIsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ1AsTUFBTTtpQkFDVDthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0RBQWlCLEdBQWpCLFVBQWtCLGlCQUFpQixFQUFFLFdBQVc7O1FBQzVDLElBQU0sb0JBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksaUJBQWlCLElBQUksaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDO2VBQzVGLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFFBQVE7ZUFDbEQsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O2dCQUNsRSxLQUFzQixJQUFBLEtBQUEsU0FBQSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUEsZ0JBQUEsNEJBQUU7b0JBQXJFLElBQU0sT0FBTyxXQUFBO29CQUNkLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRTt3QkFDcEQsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3FCQUN0RTtpQkFDSjs7Ozs7Ozs7O1NBQ0o7UUFDRCxPQUFPLGlCQUFpQixDQUFDO0lBQzdCLENBQUM7SUFDRCxzREFBaUIsR0FBakIsVUFBa0IsaUJBQWlCOzs7UUFDL0IsSUFBTSxvQkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDaEMsSUFBSSxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUM7ZUFDNUYsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUTtlQUNsRCxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNsRSxJQUFNLFFBQVEsR0FBZSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxRQUFRLENBQUM7b0NBQ3JFLE9BQU87Z0JBQ2QsSUFBSSxDQUFDLE9BQUEsT0FBTywwQ0FBRSxLQUFLLE1BQUssSUFBSSxJQUFJLE9BQUEsT0FBTywwQ0FBRSxLQUFLLE1BQUssRUFBRSxDQUFDLEVBQUU7b0JBQ3BELElBQU0sa0JBQWdCLEdBQUcsRUFBRSxDQUFDO29CQUM1QixrQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxPQUFLLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pHO2tHQUM4RTtvQkFDOUUsa0JBQWdCLENBQUMsY0FBYyxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztvQkFDakQsa0JBQWdCLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7b0JBQ2xELGtCQUFnQixDQUFDLFdBQVcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUN0RCxrQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLGtCQUFrQixDQUFDO29CQUN4RCxrQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFDNUMsSUFBSSxPQUFPLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTTsyQkFDdEUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUU7d0JBQ3ZILE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ2hDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxrQkFBZ0IsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQ0FDM0csa0JBQWdCLENBQUMsa0JBQWtCLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDOzZCQUM3RDt3QkFDTCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFDRCxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsa0JBQWdCLENBQUMsQ0FBQztpQkFDL0M7Ozs7Z0JBcEJMLEtBQXNCLElBQUEsYUFBQSxTQUFBLFFBQVEsQ0FBQSxrQ0FBQTtvQkFBekIsSUFBTSxPQUFPLHFCQUFBOzRCQUFQLE9BQU87aUJBcUJqQjs7Ozs7Ozs7O1NBQ0o7UUFDRCxPQUFPLEVBQUUsUUFBUSxFQUFFLEVBQUUsY0FBYyxFQUFFLG9CQUFvQixFQUFFLEVBQUUsQ0FBQztJQUNsRSxDQUFDO0lBRUQsb0VBQStCLEdBQS9CLFVBQWdDLFVBQVUsRUFBRSxLQUFLO1FBQzdDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQ3hDLElBQUksWUFBWSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELElBQUksS0FBSyxFQUFFO1lBQ1AsWUFBWSxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM1RDtRQUNELGtDQUFrQztRQUNsQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVELDBEQUFxQixHQUFyQixVQUFzQixhQUFhLEVBQUUsV0FBVztRQUM1QyxJQUFNLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBTSxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBRTVGLElBQU0sY0FBYyxHQUFHLElBQUksV0FBVyxDQUFDO1lBQ25DLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN6QixRQUFRLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUTtTQUNwQyxDQUFDLENBQUM7UUFFSCxJQUFJLGFBQWEsQ0FBQyxRQUFRLEVBQUU7WUFDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDeEM7UUFFRCxJQUFJLGFBQWEsQ0FBQyxTQUFTLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRTtZQUN0RSxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM5QztRQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsTUFBTTtZQUN0RSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUU7WUFDMUUsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQ3BFO1FBRUQsY0FBYyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN6QyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUV4QyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsYUFBYSxDQUFDO1FBQzNDLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO1FBRTVDLElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFO1lBQ3ZFLGFBQWEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUY7UUFDRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBQ0QsMERBQXFCLEdBQXJCLFVBQXNCLGlCQUFvQyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsbUJBQW1CO1FBQXZHLGlCQThFQztRQTdFRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsNkJBQTZCLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2xHLElBQUksbUJBQW1CLEVBQUU7Z0JBQ3JCLG9CQUFvQixHQUFHLG1CQUFtQixDQUFDO2FBQzlDO1lBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDLGlCQUFpQixDQUFDO2lCQUN2RSxTQUFTLENBQUMsVUFBQSxvQkFBb0I7O2dCQUMzQixpQkFBaUIsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2dCQUNyQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO2dCQUNuQyxpQkFBaUIsQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7Z0JBQzVDLElBQUksb0JBQW9CLElBQUksb0JBQW9CLENBQUMscUJBQXFCO29CQUNsRSxLQUFLLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLEVBQUU7O3dCQUMzRCxLQUFpQyxJQUFBLEtBQUEsU0FBQSxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQSxnQkFBQSw0QkFBRTs0QkFBeEUsSUFBTSxrQkFBa0IsV0FBQTs0QkFDekIsSUFBTSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7NEJBQzVCLElBQUksa0JBQWtCLENBQUMsRUFBRSxLQUFLLGVBQWUsQ0FBQywwQkFBMEIsRUFBRTtnQ0FDdEUsU0FBUzs2QkFDWjs0QkFDRCxJQUFJLGtCQUFrQixDQUFDLHFCQUFxQjtnQ0FDeEMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFOztvQ0FDekQsS0FBNEIsSUFBQSxvQkFBQSxTQUFBLGtCQUFrQixDQUFDLHFCQUFxQixDQUFBLENBQUEsZ0JBQUEsNEJBQUU7d0NBQWpFLElBQU0sYUFBYSxXQUFBO3dDQUNwQixpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7d0NBRTNELElBQU0sVUFBVSxHQUFHLEVBQUUsQ0FBQzt3Q0FDdEIsSUFBSSxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDOzRDQUN2QixLQUFJLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7d0NBQ3hFLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dDQUNyRCxJQUFNLGNBQWMsR0FBRyxJQUFJLFdBQVcsQ0FBQzs0Q0FDbkMsS0FBSyxFQUFFLE9BQU8sS0FBSyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFOzRDQUNoRCxRQUFRLEVBQUUsQ0FBQyxhQUFhLENBQUMsUUFBUTt5Q0FDcEMsQ0FBQyxDQUFDO3dDQUVILElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTs0Q0FDeEIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7eUNBQ3hDO3dDQUVELElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFOzRDQUN0RSxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQzt5Q0FDOUM7d0NBRUQsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNOzRDQUN0RSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUU7NENBQzFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzt5Q0FDcEU7d0NBRUQsSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUU7NENBQ3hFLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQywrQkFBK0IsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0Q0FDMUcsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzt5Q0FDbEY7d0NBRUQsY0FBYyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQzt3Q0FDekMsY0FBYyxDQUFDLHNCQUFzQixFQUFFLENBQUM7d0NBRXhDLGNBQWMsQ0FBQyxVQUFVLENBQUMsR0FBRyxhQUFhLENBQUM7d0NBQzNDLGNBQWMsQ0FBQyxNQUFNLENBQUMsR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDO3dDQUU1QyxJQUFJLGFBQWEsQ0FBQyxTQUFTLEtBQUssZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRTs0Q0FDdkUsYUFBYSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQzt5Q0FDOUY7d0NBQ0QsaUJBQWlCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3Q0FDckQsV0FBVzt3Q0FDWCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7cUNBQ3pDOzs7Ozs7Ozs7NkJBQ0o7NEJBQ0QsSUFBTSxVQUFVLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQzs0QkFDbEYsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQzs0QkFDN0MsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDbEQ7Ozs7Ozs7OztpQkFDSjtnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ2pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9EQUFlLEdBQWYsVUFBZ0IsaUJBQWlCLEVBQUUsWUFBWTtRQUMzQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUMzRSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakQsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hELE9BQU8sMkRBQTJELENBQUM7U0FDdEU7UUFDRCxJQUFJLGlCQUFpQixDQUFDLFNBQVMsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7WUFDckUsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2pELFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RCxPQUFPLHVEQUF1RCxDQUFDO1NBQ2xFO1FBQ0QsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksaUJBQWlCLENBQUMsdUNBQXVDLEVBQUU7WUFDbkcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sRUFBRSxDQUFDO1NBQ2I7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQ3ZDLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0wsQ0FBQztJQUNELDJEQUFzQixHQUF0QixVQUF1QixpQkFBaUIsRUFBRSxZQUFZO1FBQ2xELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLElBQUksWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFO1lBQ2xGLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4RCxPQUFPLGtFQUFrRSxDQUFDO1NBQzdFO1FBQ0QsSUFBSSxpQkFBaUIsQ0FBQyxnQkFBZ0IsSUFBSSxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUU7WUFDbkYsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hELE9BQU8scUVBQXFFLENBQUM7U0FDaEY7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRTtZQUN0RixZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEQsT0FBTywyRkFBMkYsQ0FBQztTQUN0RztRQUVELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLElBQUksaUJBQWlCLENBQUMsdUNBQXVDLEVBQUU7WUFDM0csWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZELE9BQU8sdUhBQXVILENBQUM7U0FDbEk7UUFDRCxJQUFJLFlBQVksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFO1lBQ3BGLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoRCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0wsQ0FBQztJQUVELG9EQUFlLEdBQWYsVUFBZ0IsdUJBQXVCLEVBQUUsY0FBYztRQUF2RCxpQkE2Q0M7UUE1Q0csT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxVQUFVLENBQUM7WUFDZixJQUFJLGNBQWMsRUFBRTtnQkFDaEIsVUFBVSxHQUFHLGNBQWMsQ0FBQyxDQUFBLDJEQUEyRDthQUMxRjtpQkFBTTtnQkFDSCxVQUFVLEdBQUcscUJBQXFCLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQTthQUN6RDtZQUNELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxXQUF1QjtnQkFDakcsSUFBTSxZQUFZLEdBQUcsV0FBVyxDQUFDLDJCQUEyQixJQUFJLE9BQU8sV0FBVyxDQUFDLDJCQUEyQixLQUFLLFFBQVE7b0JBQ3ZILENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDJCQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25FLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDMUgsSUFBTSxhQUFhLEdBQWtCO29CQUNqQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtvQkFDcEQsT0FBTyxFQUFFLEVBQUU7b0JBQ1gscUJBQXFCLEVBQUU7d0JBQ25CLGdCQUFnQixFQUFFLHVCQUF1QjtxQkFDNUM7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ2xCLGVBQWUsRUFBRSxFQUFFO3FCQUN0QjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsSUFBSSxFQUFFLENBQUM7Z0NBQ0gsUUFBUSxFQUFFLFlBQVk7Z0NBQ3RCLEtBQUssRUFBRSxLQUFLOzZCQUNmLENBQUM7cUJBQ0w7b0JBQ0QsTUFBTSxFQUFFO3dCQUNKLFVBQVUsRUFBRSxDQUFDO3dCQUNiLFNBQVMsRUFBRSxHQUFHO3FCQUNqQjtpQkFDSixDQUFDO2dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQXdCO29CQUN6RSxJQUFNLFdBQVcsR0FBRzt3QkFDaEIsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhO3dCQUMvQyxlQUFlLEVBQUUsUUFBUSxDQUFDLElBQUk7cUJBQ2pDLENBQUM7b0JBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUV4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FBQztnQkFDbkUsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBbndCc0IsVUFBVTtnQkFDTCxlQUFlO2dCQUNuQixXQUFXO2dCQUNYLFdBQVc7Z0JBQ1IsY0FBYztnQkFDVCxtQkFBbUI7Z0JBQ3hCLGNBQWM7Z0JBQ2YsYUFBYTtnQkFDWixjQUFjO2dCQUNYLGlCQUFpQjtnQkFDaEIsa0JBQWtCOzs7SUFwQ3hDLDBCQUEwQjtRQUp0QyxVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsMEJBQTBCLENBOHhCdEM7cUNBcjBCRDtDQXEwQkMsQUE5eEJELElBOHhCQztTQTl4QlksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICcuL2NhdGVnb3J5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMsIEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgRGF0ZVZhbGlkYXRvcnMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvdmFsaWRhdG9ycy9kYXRlLnZhbGlkYXRvcic7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEN1c3RvbU1ldGFkYXRhT2JqIH0gZnJvbSAnLi4vLi4vcHJvamVjdC1mcm9tLXRlbXBsYXRlcy9wcm9qZWN0LmZyb20udGVtcGxhdGUnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4vcHJvamVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT3RtbU1ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vdG1tLW1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRlYW0gfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9UZWFtJztcclxuaW1wb3J0IHsgUm9sZXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9Sb2xlcyc7XHJcbmltcG9ydCB7IE1QTV9ST0xFUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1JvbGUnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoUmVxdWVzdCB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVxdWVzdCc7XHJcbmltcG9ydCB7IFNlYXJjaFJlc3BvbnNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvb2JqZWN0cy9TZWFyY2hSZXNwb25zZSc7XHJcbmltcG9ydCB7IEluZGV4ZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvaW5kZXhlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvcnMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3IuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgbmV3QXJyYXkgfSBmcm9tICdAYW5ndWxhci9jb21waWxlci9zcmMvdXRpbCc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvamVjdEZyb21UZW1wbGF0ZVNlcnZpY2Uge1xyXG5cclxuICAgIFBST0pFQ1RfTkFNRV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvUHJvamVjdC9vcGVyYXRpb25zJztcclxuICAgIEdFVF9QUk9KRUNUX0JZX05BTUVfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsUHJvamVjdHNCeU5hbWVUeXBlJztcclxuXHJcbiAgICBQUk9KRUNUX0JQTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcbiAgICBDUkVBVEVfUFJPSkVDVF9GUk9NX1RFTVBMQVRFX01FVEhPRF9OQU1FID0gJ0NyZWF0ZVByb2plY3RGcm9tVGVtcGxhdGUnO1xyXG5cclxuICAgIC8vVEVNUExBVEVfQlBNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIFRFTVBMQVRFX0JQTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcbiAgICBDUkVBVEVfVEVNUExBVEVfRlJPTV9URU1QTEFURV9NRVRIT0RfTkFNRSA9ICdDcmVhdGVUZW1wbGF0ZUZyb21Tb3VyY2UnO1xyXG5cclxuICAgIFRFQU1fQlBNX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vdGVhbXMvYnBtLzEuMCc7XHJcbiAgICBHRVRfVEVBTV9XU19NRVRIT0RfTkFNRSA9ICdHZXRUZWFtc0ZvclVzZXInO1xyXG4gICAgVEVBTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1RlYW1zL29wZXJhdGlvbnMnO1xyXG4gICAgR0VUX0FMTF9URUFNU19XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxUZWFtcyc7XHJcblxyXG4gICAgREVMSVZFUkFCTEVfREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRGVsaXZlcmFibGUvb3BlcmF0aW9ucyc7XHJcbiAgICBHRVRfREVMSVZFUkFCTEVTX0JZX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsRGVsaXZlcmFibGVzQnlQcm9qZWN0JztcclxuXHJcbiAgICBHRVRfVEFTS19CWV9QUk9KRUNUX1dTX01FVEhPRF9OQU1FID0gJ0dldFRhc2tCeVByb2plY3RJRCc7XHJcbiAgICBUQVNLX0RFVEFJTFNfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL1Rhc2svb3BlcmF0aW9ucyc7XHJcblxyXG4gICAgbWV0YWRhdGFDb25zdGFudHMgPSBNUE1GaWVsZENvbnN0YW50cztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1NZXRhZGF0YVNlcnZpY2U6IE90bW1NZXRhZGF0YVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgaW5kZXhlclNlcnZpY2U6IEluZGV4ZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBnZXRQcm9wZXJ0eShvYmosIHBhdGgpIHtcclxuICAgICAgICBpZiAoIW9iaiB8fCAhcGF0aCB8fCAhb2JqLm1ldGFkYXRhKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXRGaWVsZFZhbHVlQnlJZChvYmoubWV0YWRhdGEsIHBhdGgsIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJEZXRhaWxzQnlVc2VyQ24odXNlckNuKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IHVzZXJJZGVudHlJRCA9ICcnO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgdXNlckNOOiB1c2VyQ25cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkRldGFpbHNCeVVzZXJDTihwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLlBlcnNvbiAmJiByZXNwb25zZS5QZXJzb24uUGVyc29uVG9Vc2VyWydJZGVudGl0eS1pZCddICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLlBlcnNvbi5QZXJzb25Ub1VzZXJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcklkZW50eUlEID0gcmVzcG9uc2UuUGVyc29uLlBlcnNvblRvVXNlclsnSWRlbnRpdHktaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VySWRlbnR5SUQpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0T3duZXJzKHRlYW1JZCkge1xyXG4gICAgICAgIGNvbnN0IHJvbGVEZXRhaWxPYmo6IFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRSb2xlQnlOYW1lKE1QTV9ST0xFUy5NQU5BR0VSKTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiAnUk9MRScsXHJcbiAgICAgICAgICAgICAgICByb2xlRE46IHJvbGVEZXRhaWxPYmouUk9MRV9ETixcclxuICAgICAgICAgICAgICAgIHRlYW1JRDogdGVhbUlkLFxyXG4gICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6IHRydWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUZWFtcyhpc0NhbXBhaWduUHJvamVjdCk6IE9ic2VydmFibGU8QXJyYXk8VGVhbT4+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoaXNDYW1wYWlnblByb2plY3QpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTV9NRVRIT0RfTlMsIHRoaXMuR0VUX0FMTF9URUFNU19XU19NRVRIT0RfTkFNRSwgbnVsbClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVhbXM6IEFycmF5PFRlYW0+ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9UZWFtcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRlYW1zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRlYW1zJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvbGVEZXRhaWxPYmo6IFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRSb2xlQnlOYW1lKE1QTV9ST0xFUy5NQU5BR0VSKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogcm9sZURldGFpbE9iai5ST0xFX0ROXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5URUFNX0JQTV9NRVRIT0RfTlMsIHRoaXMuR0VUX1RFQU1fV1NfTUVUSE9EX05BTUUsIHBhcmFtKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZWFtczogQXJyYXk8VGVhbT4gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1RlYW1zJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGVhbXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgVGVhbXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBnZXRUZWFtKHRlYW1JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgIFRlYW1JZDogdGVhbUlkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTV9CUE1fTUVUSE9EX05TLCB0aGlzLkdFVF9URUFNX1dTX01FVEhPRF9OQU1FLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLlRlYW0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5UZWFtKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRlYW0nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUlxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrcyhwcm9qZWN0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgICAgICBwcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfREVUQUlMU19NRVRIT0RfTlMsIHRoaXMuR0VUX1RBU0tfQllfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5UYXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZS5UYXNrKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuVGFzayA9IFtyZXNwb25zZS5UYXNrXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLlRhc2spO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBUYXNrcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGdldERlbGl2ZXJhYmxlcyhwcm9qZWN0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9qZWN0SUQ6IHByb2plY3RJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTElWRVJBQkxFX0RFVEFJTFNfTUVUSE9EX05TLCB0aGlzLkdFVF9ERUxJVkVSQUJMRVNfQllfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5EZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuRGVsaXZlcmFibGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5EZWxpdmVyYWJsZSA9IFtyZXNwb25zZS5EZWxpdmVyYWJsZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5EZWxpdmVyYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIERlbGl2ZXJhYmxlcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWxpdmVyYWJsZUJ5UHJvamVjdEZvbGRlcklkKHByb2plY3RGb2xkZXJJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgc2VhcmNoQ29uZGl0aW9uTGlzdCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoRGVsaXZlcmFibGVDb25zdGFudHMuREVMSVZFUkFCTEVfU0VBUkNIX0NPTkRJVElPTikpO1xyXG5cclxuICAgICAgICBjb25zdCBza2lwID0gMDtcclxuICAgICAgICBjb25zdCB0b3AgPSAxMDA7XHJcblxyXG4gICAgICAgIGNvbnN0IG90bW1BbGxBY3RpdmVUYXNrc1NlYXJjaENvbmZpZyA9IHtcclxuICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiBzZWFyY2hDb25kaXRpb25MaXN0LFxyXG4gICAgICAgICAgICBsb2FkX3R5cGU6ICdtZXRhZGF0YScsXHJcbiAgICAgICAgICAgIGNoaWxkX2NvdW50X2xvYWRfdHlwZTogJ2ZvbGRlcnMnLFxyXG4gICAgICAgICAgICBsZXZlbF9vZl9kZXRhaWw6ICdmdWxsJyxcclxuICAgICAgICAgICAgZm9sZGVyX2ZpbHRlcjogcHJvamVjdEZvbGRlcklkXHJcbiAgICAgICAgfTtcclxuXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLm90bW1TZXJ2aWNlLnNlYXJjaEN1c3RvbVRleHQob3RtbUFsbEFjdGl2ZVRhc2tzU2VhcmNoQ29uZmlnLCBza2lwLCB0b3AsXHJcbiAgICAgICAgICAgIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tQcm9qZWN0TmFtZShwcm9qZWN0U2VhcmNoQ29uZGl0aW9uLCBzZWFyY2hLZXl3b3JkLCB2aWV3Q29uZmlnTmFtZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IHZpZXdDb25maWc7XHJcbiAgICAgICAgICAgIGlmICh2aWV3Q29uZmlnTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IHZpZXdDb25maWdOYW1lOy8vdGhpcy5zaGFyaW5nU2VydmljZS5nZXRWaWV3Q29uZmlnQnlJZCh2aWV3Q29uZmlnSWQpLlZJRVc7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlBST0pFQ1Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHM6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgICAgICAgICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyczogU2VhcmNoUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZmlnX2lkOiBzZWFyY2hDb25maWcgPyBzZWFyY2hDb25maWcgOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIGtleXdvcmQ6IHNlYXJjaEtleXdvcmQsXHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb246IHByb2plY3RTZWFyY2hDb25kaXRpb25cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0OiBbXVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2Vfc2l6ZTogOTk5XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmRhdGEgJiYgcmVzcG9uc2UuZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGhhc1NhbWVQcm9qZWN0TmFtZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpbmRleGVyRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRJbmRleGVySWRCeU1hcHBlclZhbHVlKE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0ZFSUxEUy5QUk9KRUNUX05BTUUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5kYXRhLmZvckVhY2gocHJvamVjdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvamVjdFtpbmRleGVyRmllbGRdID09PSBzZWFyY2hLZXl3b3JkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzU2FtZVByb2plY3ROYW1lID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChoYXNTYW1lUHJvamVjdE5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgcHJvamVjdHMuJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVByb2plY3RGcm9tVGVtcGxhdGUoQ3JlYXRlUHJvamVjdEZyb21Tb3VyY2VPYmplY3QpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQlBNX01FVEhPRF9OUywgdGhpcy5DUkVBVEVfUFJPSkVDVF9GUk9NX1RFTVBMQVRFX01FVEhPRF9OQU1FLCBDcmVhdGVQcm9qZWN0RnJvbVNvdXJjZU9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVGVtcGxhdGVGcm9tVGVtcGxhdGUoQ3JlYXRlVGVtcGxhdGVGcm9tU291cmNlT2JqZWN0KSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5URU1QTEFURV9CUE1fTUVUSE9EX05TLCB0aGlzLkNSRUFURV9URU1QTEFURV9GUk9NX1RFTVBMQVRFX01FVEhPRF9OQU1FLCBDcmVhdGVUZW1wbGF0ZUZyb21Tb3VyY2VPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE93bmVyTGlzdEJ5VGVhbSh0ZWFtTmFtZSwgaXNDYW1wYWlnblByb2plY3QpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBsZXQgcHJvamVjdE93bmVyTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmdldFRlYW1zKGlzQ2FtcGFpZ25Qcm9qZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSh0ZWFtTGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGhhc1RlYW0gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0ZWFtTGlzdC5tYXAodGVhbSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0ZWFtLk5BTUUgPT09IHRlYW1OYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXNUZWFtID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRlYW1JZCA9IHRlYW1bJ01QTV9UZWFtcy1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRQcm9qZWN0T3duZXJzKHRlYW1JZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlWyd1c2VycyddICYmIHJlc3BvbnNlWyd1c2VycyddLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlWyd1c2VycyddLnVzZXIubWFwKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXIuZGlzcGxheU5hbWUgPSB1c2VyLm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlci52YWx1ZSA9IHVzZXIuY247XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlci5pZCA9IHVzZXIuY247XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RPd25lckxpc3QgPSByZXNwb25zZVsndXNlcnMnXS51c2VyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocHJvamVjdE93bmVyTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHByb2plY3RPd25lckxpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFoYXNUZWFtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocHJvamVjdE93bmVyTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocHJvamVjdE93bmVyTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlUHJvamVjdFRhc2thbmREZWxpdmVyYWJsZSh0ZW1wbGF0ZU9iaiwgdGVtcGxhdGVmb3JtLCBwcm9qZWN0RGVwZW5kZW5jeSwgZGVsaXZlcmFibGVWaWV3TmFtZSkge1xyXG4gICAgICAgIGlmICh0ZW1wbGF0ZU9iaikge1xyXG4gICAgICAgICAgICBsZXQgdmlld0NvbmZpZztcclxuICAgICAgICAgICAgY29uc3QgdGVtcGxhdGVQcm9qZWN0SXRlbUlkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmVpbGRWYWx1ZUJ5TWFwcGVyTmFtZSh0ZW1wbGF0ZU9iaiwgdGhpcy5tZXRhZGF0YUNvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9JVEVNX0lEKTtcclxuICAgICAgICAgICAgaWYgKHRlbXBsYXRlUHJvamVjdEl0ZW1JZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGVtcGxhdGVQcm9qZWN0SWQgPSB0ZW1wbGF0ZVByb2plY3RJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGFza3ModGVtcGxhdGVQcm9qZWN0SWQpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tzID0gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdGFza3MgfHwgIXRhc2tzLmxlbmd0aCB8fCB0YXNrcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2suZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0RGVwZW5kZW5jeS5pc05vVGFza3MgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNOb0RlbGl2ZXJhYmxlcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0RGVwZW5kZW5jeS5pc1Rhc2tXaXRoVGFza1dpdGhEZWxpdmVyYWJsZURlcGVuZGVuY3kgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdERlcGVuZGVuY3kuaXNOb1Rhc2tzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0RGVwZW5kZW5jeS5pc05vRGVsaXZlcmFibGVzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0RGVwZW5kZW5jeS5pc1Rhc2tXaXRoVGFza1dpdGhEZWxpdmVyYWJsZURlcGVuZGVuY3kgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGVWaWV3TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IGRlbGl2ZXJhYmxlVmlld05hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLkRFTElWRVJBQkxFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hDb25maWcgPSB2aWV3RGV0YWlscy5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcgJiYgdHlwZW9mIHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyA9PT0gJ3N0cmluZydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IHBhcnNlSW50KHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRywgMTApIDogbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaENvbmRpdGlvbkxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRpc3BsYXlGaWVsZCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTVBNRmllbGRDb25zdGFudHMuREVMSVZFUkFCTEVfTVBNX0ZJRUxEUy5ERUxJVkVSQUJMRV9QUk9KRUNUX0lELCBNUE1fTEVWRUxTLkRFTElWRVJBQkxFKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbkxpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogZGlzcGxheUZpZWxkID8gZGlzcGxheUZpZWxkLkRBVEFfVFlQRSA6ICdzdHJpbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkX2lkOiBkaXNwbGF5RmllbGQuSU5ERVhFUl9GSUVMRF9JRCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX2lkOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0ZW1wbGF0ZVByb2plY3RJdGVtSWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaW5kZXhlckZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlcklkQnlNYXBwZXJWYWx1ZShNUE1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9OQU1FKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnNSZXE6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogc2VhcmNoQ29uZmlnID8gc2VhcmNoQ29uZmlnIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXl3b3JkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogc2VhcmNoQ29uZGl0aW9uTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFjZXRfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFjZXRfY29uZGl0aW9uOiBbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc29ydGluZ19saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNvcnQ6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZF9pZDogaW5kZXhlckZpZWxkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3JkZXI6ICdBU0MnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZV9pbmRleDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZV9zaXplOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnNSZXEpLnN1YnNjcmliZSgocmVzcG9uc2VEYXRhOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZURhdGEuZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3REZXBlbmRlbmN5LmlzTm9EZWxpdmVyYWJsZXMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdJbnZhbGlkIHByb2plY3QgaW5mb3JtYXRpb24gc3VwcGxpZWQgdG8gdmFsaWRhdGUgdGFzayBhbmQgZGVsaXZlcmFibGVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29weVRlbXBsYXRlKHNvdXJjZUlkLCB0ZW1wbGF0ZWZvcm0sIHRlbXBsYXRlU2VhcmNoQ29uZGl0aW9uLCBjdXN0b21NZXRhZGF0YSwgcHJvamVjdE93bmVyLCB2aWV3Q29uZmlnTmFtZSwgZW5hYmxlRHVwbGljYXRlTmFtZXMpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc291cmNlSWQpIHtcclxuICAgICAgICAgICAgICAgIGxldCBQcm9qZWN0T2JqZWN0O1xyXG4gICAgICAgICAgICAgICAgaWYgKHByb2plY3RPd25lcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJJZCA9IHByb2plY3RPd25lci52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBQcm9qZWN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBTb3VyY2VJRDogc291cmNlSWQuc3BsaXQoJy4nKVsxXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdE5hbWU6IHRlbXBsYXRlZm9ybS52YWx1ZS5wcm9qZWN0TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdE93bmVyOiB1c2VySWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3REZXNjcmlwdGlvbjogdGVtcGxhdGVmb3JtLnZhbHVlLmRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJc0NvcHlUYXNrOiB0ZW1wbGF0ZWZvcm0udmFsdWUuY29weVRhc2s/IHRlbXBsYXRlZm9ybS52YWx1ZS5jb3B5VGFzayA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJc0NvcHlEZWxpdmVyYWJsZTogdGVtcGxhdGVmb3JtLnZhbHVlLmNvcHlEZWxpdmVyYWJsZT8gdGVtcGxhdGVmb3JtLnZhbHVlLmNvcHlEZWxpdmVyYWJsZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDdXN0b21NZXRhZGF0YTogY3VzdG9tTWV0YWRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENhbXBhaWduSUQ6IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhbXBhaWduLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDYXRlZ29yeU1ldGFkYXRhSUQ6IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhdGVnb3J5TWV0YWRhdGEudmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzQ29weVdvcmtmbG93UnVsZXNOZWVkZWQ6IHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5V29yZmxvd1J1bGVzLnZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrUHJvamVjdE5hbWUodGVtcGxhdGVTZWFyY2hDb25kaXRpb24sIFByb2plY3RPYmplY3QuUHJvamVjdE5hbWUsIHZpZXdDb25maWdOYW1lKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiBlbmFibGVEdXBsaWNhdGVOYW1lcyA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1RlbXBsYXRlIGFscmVhZHkgZXhpc3RzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDcmVhdGVQcm9qZWN0RnJvbVNvdXJjZU9iamVjdDogUHJvamVjdE9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ0NyZWF0ZSBUZW1wbGF0ZSBoYXMgYmVlbiBpbml0aWF0ZWQgc3VjZXNzZnVsbHknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNyZWF0ZVRlbXBsYXRlRnJvbVRlbXBsYXRlKFByb2plY3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JlYXRlUHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjcmVhdGVQcm9qZWN0UmVzcG9uc2UgJiYgY3JlYXRlUHJvamVjdFJlc3BvbnNlWydBUElSZXNwb25zZSddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXS5zdGF0dXNDb2RlID09PSAnNTAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ0FQSVJlc3BvbnNlJ10uZXJyb3IuZXJyb3JNZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY3JlYXRlUHJvamVjdFJlc3BvbnNlWydBUElSZXNwb25zZSddLnN0YXR1c0NvZGUgPT09ICcyMDAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgY29weWluZyB0ZW1wbGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVQcm9qZWN0KHNvdXJjZUlkLCB0ZW1wbGF0ZWZvcm0sIHByb2plY3RTZWFyY2hDb25kaXRpb24sIGN1c3RvbU1ldGFkYXRhLCBwcm9qZWN0T3duZXIsIHZpZXdDb25maWdOYW1lLCBlbmFibGVEdXBsaWNhdGVOYW1lcykge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzb3VyY2VJZCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IFByb2plY3RPYmplY3Q7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvamVjdE93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlcklkID0gcHJvamVjdE93bmVyLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRlbXBsYXRlZm9ybS52YWx1ZS5wcm9qZWN0T3duZXIgPyB0ZW1wbGF0ZWZvcm0udmFsdWUucHJvamVjdE93bmVyLmNuIDogXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5nZXRVc2VyRGV0YWlsc0J5VXNlckNuKG93bmVyQ24pXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC5zdWJzY3JpYmUodXNlcklkID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBQcm9qZWN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBUZW1wbGF0ZUlEOiBzb3VyY2VJZC5zcGxpdCgnLicpWzFdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9qZWN0TmFtZTogdGVtcGxhdGVmb3JtLnZhbHVlLnByb2plY3ROYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9qZWN0T3duZXI6IHVzZXJJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdERlc2NyaXB0aW9uOiB0ZW1wbGF0ZWZvcm0udmFsdWUuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEV4cGVjdGVkUHJvamVjdER1cmF0aW9uOiB0ZW1wbGF0ZWZvcm0udmFsdWUuZXhwZWN0ZWREdXJhdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvamVjdFN0YXJ0RGF0ZTogdGVtcGxhdGVmb3JtLnZhbHVlLnByb2plY3RTdGFydERhdGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3REdWVEYXRlOiB0ZW1wbGF0ZWZvcm0udmFsdWUucHJvamVjdEVuZERhdGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFRhc2tTdGFydERhdGU6IHRlbXBsYXRlZm9ybS52YWx1ZS50YXNrU3RhcnREYXRlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBUYXNrRHVlRGF0ZTogdGVtcGxhdGVmb3JtLmNvbnRyb2xzLnRhc2tFbmREYXRlLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJc0NvcHlUYXNrOiB0ZW1wbGF0ZWZvcm0udmFsdWUuY29weVRhc2sgfHwgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElzQ29weURlbGl2ZXJhYmxlOiB0ZW1wbGF0ZWZvcm0udmFsdWUuY29weURlbGl2ZXJhYmxlIHx8IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBDdXN0b21NZXRhZGF0YTogY3VzdG9tTWV0YWRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENhbXBhaWduSUQ6IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhbXBhaWduID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYW1wYWlnbi52YWx1ZSA/IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhbXBhaWduLnZhbHVlIDogdGVtcGxhdGVmb3JtLmdldFJhd1ZhbHVlKCkuY2FtcGFpZ24uc3BsaXQoLy0oLispLylbMF0pIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENhdGVnb3J5TWV0YWRhdGFJRDogdGVtcGxhdGVmb3JtLmdldFJhd1ZhbHVlKCkuY2F0ZWdvcnlNZXRhZGF0YSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAodGVtcGxhdGVmb3JtLmdldFJhd1ZhbHVlKCkuY2F0ZWdvcnlNZXRhZGF0YS52YWx1ZSA/IHRlbXBsYXRlZm9ybS5nZXRSYXdWYWx1ZSgpLmNhdGVnb3J5TWV0YWRhdGEudmFsdWUgOiB0ZW1wbGF0ZWZvcm0uZ2V0UmF3VmFsdWUoKS5jYXRlZ29yeU1ldGFkYXRhKSA6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrUHJvamVjdE5hbWUocHJvamVjdFNlYXJjaENvbmRpdGlvbiwgUHJvamVjdE9iamVjdC5Qcm9qZWN0TmFtZSwgdmlld0NvbmZpZ05hbWUpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIGVuYWJsZUR1cGxpY2F0ZU5hbWVzID09ICdmYWxzZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdQcm9qZWN0IGFscmVhZHkgZXhpc3RzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDcmVhdGVQcm9qZWN0RnJvbVNvdXJjZU9iamVjdDogUHJvamVjdE9iamVjdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ0NyZWF0ZSBQcm9qZWN0IGhhcyBiZWVuIGluaXRpYXRlZCBzdWNlc3NmdWxseScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLlNVQ0NFU1NcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlUHJvamVjdEZyb21UZW1wbGF0ZShQcm9qZWN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyZWF0ZVByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY3JlYXRlUHJvamVjdFJlc3BvbnNlICYmIGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjcmVhdGVQcm9qZWN0UmVzcG9uc2VbJ0FQSVJlc3BvbnNlJ10uc3RhdHVzQ29kZSA9PT0gJzUwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnREYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogY3JlYXRlUHJvamVjdFJlc3BvbnNlWydBUElSZXNwb25zZSddLmVycm9yLmVycm9yTWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGNyZWF0ZVByb2plY3RSZXNwb25zZVsnQVBJUmVzcG9uc2UnXS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGNvcHlpbmcgcHJvamVjdCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdXRDdXN0b21NZXRhZGF0YShjdXN0b21GaWVsZHNHcm91cCwgdGVtcGxhdGVPYmopIHtcclxuICAgICAgICBjb25zdCBjb3N0b21NZXRhZGF0YVZhbHVlcyA9IFtdO1xyXG4gICAgICAgIGlmIChjdXN0b21GaWVsZHNHcm91cCAmJiBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXSAmJiBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXVsnZmllbGRzZXQnXVxyXG4gICAgICAgICAgICAmJiBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXVsnZmllbGRzZXQnXS5jb250cm9sc1xyXG4gICAgICAgICAgICAmJiBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXVsnZmllbGRzZXQnXS5jb250cm9scy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgY29udHJvbCBvZiBjdXN0b21GaWVsZHNHcm91cFsnY29udHJvbHMnXVsnZmllbGRzZXQnXS5jb250cm9scykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZ2V0UHJvcGVydHkodGVtcGxhdGVPYmosIGNvbnRyb2wuZmllbGRzZXQuaWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udHJvbC52YWx1ZSA9IHRoaXMuZ2V0UHJvcGVydHkodGVtcGxhdGVPYmosIGNvbnRyb2wuZmllbGRzZXQuaWQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjdXN0b21GaWVsZHNHcm91cDtcclxuICAgIH1cclxuICAgIGdldEN1c3RvbU1ldGFkYXRhKGN1c3RvbUZpZWxkc0dyb3VwKSB7XHJcbiAgICAgICAgY29uc3QgY29zdG9tTWV0YWRhdGFWYWx1ZXMgPSBbXTtcclxuICAgICAgICBpZiAoY3VzdG9tRmllbGRzR3JvdXAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ10gJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J11cclxuICAgICAgICAgICAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10uY29udHJvbHNcclxuICAgICAgICAgICAgJiYgY3VzdG9tRmllbGRzR3JvdXBbJ2NvbnRyb2xzJ11bJ2ZpZWxkc2V0J10uY29udHJvbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBjb250cm9sczogQXJyYXk8YW55PiA9IGN1c3RvbUZpZWxkc0dyb3VwWydjb250cm9scyddWydmaWVsZHNldCddLmNvbnRyb2xzO1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNvbnRyb2wgb2YgY29udHJvbHMpIHtcclxuICAgICAgICAgICAgICAgIGlmICgoY29udHJvbD8udmFsdWUgIT09IG51bGwgJiYgY29udHJvbD8udmFsdWUgIT09ICcnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1ldGFkYXRhRmllbGRPYmogPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydkYXRhVHlwZSddID0gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldE1ldGFkYXRhRmllbGRUeXBlKGNvbnRyb2wuZmllbGRzZXQuZGF0YV90eXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAvKm1ldGFkYXRhRmllbGRPYmpbJ2RlZmF1bHRWYWx1ZSddID0gY29udHJvbC5maWVsZHNldC5kYXRhX3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkRBVEUgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmNvbnZlcnRUb09UTU1EYXRlRm9ybWF0KGNvbnRyb2wudmFsdWUpIDogY29udHJvbC52YWx1ZTsqL1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRPYmpbJ2RlZmF1bHRWYWx1ZSddID0gY29udHJvbC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydmaWVsZElkJ10gPSBjb250cm9sLmZpZWxkc2V0LmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRPYmpbJ2ZpZWxkVHlwZSddID0gY29udHJvbC5maWVsZHNldC50eXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRPYmpbJ3JlZmVyZW5jZVZhbHVlJ10gPSAnZGVmYXVsdFJlZmVyZW5jZSc7XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZE9ialsnZG9tYWluRmllbGRWYWx1ZSddID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29udHJvbC5maWVsZHNldCAmJiBjb250cm9sLmZpZWxkc2V0LmVkaXRfdHlwZSAmJiBjb250cm9sLmZpZWxkc2V0LnZhbHVlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiBBcnJheS5pc0FycmF5KGNvbnRyb2wuZmllbGRzZXQudmFsdWVzKSAmJiBjb250cm9sLmZpZWxkc2V0LmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuQ09NQk8pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udHJvbC5maWVsZHNldC52YWx1ZXMuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmZpZWxkX3ZhbHVlICYmIGRhdGEuZmllbGRfdmFsdWUudmFsdWUgJiYgZGF0YS5maWVsZF92YWx1ZS52YWx1ZSA9PT0gbWV0YWRhdGFGaWVsZE9ialsnZGVmYXVsdFZhbHVlJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkT2JqWydkb21haW5GaWVsZFZhbHVlJ10gPSBkYXRhLmRpc3BsYXlfdmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb3N0b21NZXRhZGF0YVZhbHVlcy5wdXNoKG1ldGFkYXRhRmllbGRPYmopO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB7IE1ldGFkYXRhOiB7IG1ldGFkYXRhRmllbGRzOiBjb3N0b21NZXRhZGF0YVZhbHVlcyB9IH07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWF4TWluVmFsdWVGb3JDdXN0b21NZXRhZGF0YShkYXRhTGVuZ3RoLCBzY2FsZSk6IGFueSB7XHJcbiAgICAgICAgbGV0IGRhdGEgPSBNYXRoLmFicyhkYXRhTGVuZ3RoIC0gc2NhbGUpO1xyXG4gICAgICAgIGxldCBudW1lcmljVmFsdWUgPSBuZXdBcnJheShkYXRhKS5maWxsKCc5Jykuam9pbignJyk7XHJcbiAgICAgICAgaWYgKHNjYWxlKSB7XHJcbiAgICAgICAgICAgIG51bWVyaWNWYWx1ZSArPSAnLicgKyBuZXdBcnJheShzY2FsZSkuZmlsbCgnOScpLmpvaW4oJycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHJhZGl4XHJcbiAgICAgICAgcmV0dXJuIHNjYWxlID8gcGFyc2VGbG9hdChudW1lcmljVmFsdWUpIDogcGFyc2VJbnQobnVtZXJpY1ZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBtYXBDdXN0b21NZXRhZGF0YUZvcm0obWV0YWRhdGFGaWVsZCwgdGVtcGxhdGVPYmopIHtcclxuICAgICAgICBjb25zdCB2YWxpZGF0b3JzID0gW107XHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSAodGVtcGxhdGVPYmopID9cclxuICAgICAgICAgICAgdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKHRlbXBsYXRlT2JqLm1ldGFkYXRhLCBtZXRhZGF0YUZpZWxkLmlkKSA6ICcnO1xyXG5cclxuICAgICAgICBjb25zdCBmb3JtQ29udHJvbE9iaiA9IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgIHZhbHVlOiB2YWx1ZSA/IHZhbHVlIDogJycsXHJcbiAgICAgICAgICAgIGRpc2FibGVkOiAhbWV0YWRhdGFGaWVsZC5lZGl0YWJsZSxcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQucmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5EQVRFKSB7XHJcbiAgICAgICAgICAgIHZhbGlkYXRvcnMucHVzaChEYXRlVmFsaWRhdG9ycy5kYXRlRm9ybWF0KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuU0lNUExFIHx8XHJcbiAgICAgICAgICAgIG1ldGFkYXRhRmllbGQuZWRpdF90eXBlID09PSBQcm9qZWN0Q29uc3RhbnQuTUVUQURBVEFfRURJVF9UWVBFUy5URVhUQVJFQSkge1xyXG4gICAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5tYXhMZW5ndGgobWV0YWRhdGFGaWVsZC5kYXRhX2xlbmd0aCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9ybUNvbnRyb2xPYmouc2V0VmFsaWRhdG9ycyh2YWxpZGF0b3JzKTtcclxuICAgICAgICBmb3JtQ29udHJvbE9iai51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcblxyXG4gICAgICAgIGZvcm1Db250cm9sT2JqWydmaWVsZHNldCddID0gbWV0YWRhdGFGaWVsZDtcclxuICAgICAgICBmb3JtQ29udHJvbE9ialsnbmFtZSddID0gbWV0YWRhdGFGaWVsZC5uYW1lO1xyXG5cclxuICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkNPTUJPKSB7XHJcbiAgICAgICAgICAgIG1ldGFkYXRhRmllbGQudmFsdWVzID0gdGhpcy51dGlsU2VydmljZS5nZXRMb29rdXBEb21haW5WYWx1ZXNCeUlkKG1ldGFkYXRhRmllbGQuZG9tYWluX2lkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZvcm1Db250cm9sT2JqO1xyXG4gICAgfVxyXG4gICAgZ2V0T1RNTUN1c3RvbU1ldGFEYXRhKGN1c3RvbU1ldGFkYXRhT2JqOiBDdXN0b21NZXRhZGF0YU9iaiwgZm9ybUdyb3VwLCB0ZW1wbGF0ZU9iaiwgY2F0ZWdvcnlMZXZlbERldGFpbCk6IE9ic2VydmFibGU8Q3VzdG9tTWV0YWRhdGFPYmo+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBsZXQgY2F0ZWdvcnlMZXZlbERldGFpbHMgPSB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeUxldmVsRGV0YWlsc0J5VHlwZShNUE1fTEVWRUxTLlBST0pFQ1QpO1xyXG4gICAgICAgICAgICBpZiAoY2F0ZWdvcnlMZXZlbERldGFpbCkge1xyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbERldGFpbHMgPSBjYXRlZ29yeUxldmVsRGV0YWlsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuZ2V0TWV0YWRhdE1vZGVsQnlJZChjYXRlZ29yeUxldmVsRGV0YWlscy5NRVRBREFUQV9NT0RFTF9JRClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobWV0YURhdGFNb2RlbERldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmZpZWxkc2V0R3JvdXAgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBjdXN0b21NZXRhZGF0YU9iai5maWVsZEdyb3VwcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmN1c3RvbU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFEYXRhTW9kZWxEZXRhaWxzICYmIG1ldGFEYXRhTW9kZWxEZXRhaWxzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBBcnJheS5pc0FycmF5KG1ldGFEYXRhTW9kZWxEZXRhaWxzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBtZXRhZGF0YUZpZWxkR3JvdXAgb2YgbWV0YURhdGFNb2RlbERldGFpbHMubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmb3JtQ29udHJvbEFycmF5ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZEdyb3VwLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX1BST0pFQ1RfTUVUQURBVEFfR1JPVVApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkR3JvdXAubWV0YWRhdGFfZWxlbWVudF9saXN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQXJyYXkuaXNBcnJheShtZXRhZGF0YUZpZWxkR3JvdXAubWV0YWRhdGFfZWxlbWVudF9saXN0KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgbWV0YWRhdGFGaWVsZCBvZiBtZXRhZGF0YUZpZWxkR3JvdXAubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmN1c3RvbU1ldGFkYXRhRmllbGRzLnB1c2gobWV0YWRhdGFGaWVsZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWxpZGF0b3JzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2YWx1ZSA9ICh0ZW1wbGF0ZU9iaikgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlcklkQnlPVE1NSWQobWV0YWRhdGFGaWVsZC5pZCkgOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSB0ZW1wbGF0ZU9ialt2YWx1ZV0gPyB0ZW1wbGF0ZU9ialt2YWx1ZV0gOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZm9ybUNvbnRyb2xPYmogPSBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcgPyB2YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6ICFtZXRhZGF0YUZpZWxkLmVkaXRhYmxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5yZXF1aXJlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YUZpZWxkLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuREFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKERhdGVWYWxpZGF0b3JzLmRhdGVGb3JtYXQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLlNJTVBMRSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLlRFWFRBUkVBKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5tYXhMZW5ndGgobWV0YWRhdGFGaWVsZC5kYXRhX2xlbmd0aCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5kYXRhX3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLk5VTUJFUikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbnVtZXJpY1ZhbHVlID0gdGhpcy5nZXRNYXhNaW5WYWx1ZUZvckN1c3RvbU1ldGFkYXRhKG1ldGFkYXRhRmllbGQuZGF0YV9sZW5ndGgsIG1ldGFkYXRhRmllbGQuc2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMubWF4KG51bWVyaWNWYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY1ZhbHVlKSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE9iai5zZXRWYWxpZGF0b3JzKHZhbGlkYXRvcnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE9iai51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE9ialsnZmllbGRzZXQnXSA9IG1ldGFkYXRhRmllbGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1Db250cm9sT2JqWyduYW1lJ10gPSBtZXRhZGF0YUZpZWxkLm5hbWU7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZC5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkNPTUJPKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkLnZhbHVlcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChtZXRhZGF0YUZpZWxkLmRvbWFpbl9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VzdG9tTWV0YWRhdGFPYmouZmllbGRzZXRHcm91cC5wdXNoKGZvcm1Db250cm9sT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gbmV3IGNvZGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xBcnJheS5wdXNoKGZvcm1Db250cm9sT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZEdyb3VwID0gZm9ybUdyb3VwLmdyb3VwKHsgZmllbGRzZXQ6IG5ldyBGb3JtQXJyYXkoZm9ybUNvbnRyb2xBcnJheSkgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWVsZEdyb3VwWyduYW1lJ10gPSBtZXRhZGF0YUZpZWxkR3JvdXAubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbU1ldGFkYXRhT2JqLmZpZWxkR3JvdXBzLnB1c2goZmllbGRHcm91cCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGN1c3RvbU1ldGFkYXRhT2JqKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoY3VzdG9tTWV0YWRhdGFPYmopO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tUYXNrQWNjZXNzKHByb2plY3REZXBlbmRlbmN5LCB0ZW1wbGF0ZWZvcm0pIHtcclxuICAgICAgICBpZiAoIXByb2plY3REZXBlbmRlbmN5LnNlbGVjdGVkT2JqZWN0ICYmIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay52YWx1ZSkge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiAnQ2Fubm90IGVuYWJsZSBjb3B5IHRhc2sgb3B0aW9uLiBQbGVhc2Ugc2VsZWN0IGEgdGVtcGxhdGUuJztcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHByb2plY3REZXBlbmRlbmN5LmlzTm9UYXNrcyAmJiB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sudmFsdWUpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gJ0Nhbm5vdCBlbmFibGUgY29weSB0YXNrIG9wdGlvbi4gTm8gdGFza3MgYXJlIHByZXNlbnQuJztcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay52YWx1ZSAmJiBwcm9qZWN0RGVwZW5kZW5jeS5pc1Rhc2tXaXRoVGFza1dpdGhEZWxpdmVyYWJsZURlcGVuZGVuY3kpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlUYXNrLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUucGF0Y2hWYWx1ZShmYWxzZSk7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBjaGVja0RlbGl2ZXJhYmxlQWNjZXNzKHByb2plY3REZXBlbmRlbmN5LCB0ZW1wbGF0ZWZvcm0pIHtcclxuICAgICAgICBpZiAoIXByb2plY3REZXBlbmRlbmN5LnNlbGVjdGVkT2JqZWN0ICYmIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5RGVsaXZlcmFibGUudmFsdWUpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKGZhbHNlKTtcclxuICAgICAgICAgICAgcmV0dXJuICdDYW5ub3QgZW5hYmxlIGNvcHkgZGVsaXZlcmFibGUgb3B0aW9uLiBQbGVhc2Ugc2VsZWN0IGEgdGVtcGxhdGUuJztcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHByb2plY3REZXBlbmRlbmN5LmlzTm9EZWxpdmVyYWJsZXMgJiYgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS52YWx1ZSkge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gJ0Nhbm5vdCBlbmFibGUgY29weSBkZWxpdmVyYWJsZSBvcHRpb24uIE5vIERlbGl2ZXJhYmxlcyBhcmUgcHJlc2VudC4nO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weVRhc2sudmFsdWUgJiYgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS52YWx1ZSkge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnBhdGNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gJ0Nhbm5vdCBlbmFibGUgY29weSBkZWxpdmVyYWJsZSBvcHRpb24uIEVuYWJsZSBjb3B5IHRhc2sgb3B0aW9uIHRvIGVuYWJsZSBjb3B5IGRlbGl2ZXJibGUuJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS52YWx1ZSAmJiBwcm9qZWN0RGVwZW5kZW5jeS5pc1Rhc2tXaXRoVGFza1dpdGhEZWxpdmVyYWJsZURlcGVuZGVuY3kpIHtcclxuICAgICAgICAgICAgdGVtcGxhdGVmb3JtLmNvbnRyb2xzLmNvcHlEZWxpdmVyYWJsZS5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICByZXR1cm4gJ0Nhbm5vdCBkaXNhYmxlIGNvcHkgZGVsaXZlcmFibGUgb3B0aW9uLiBTZWxlY3RlZCBwcm9qZWN0IGNvbnRhaW5zIEFwcHJvdmFsIFRhc2tzLCB3aGljaCBoYXMgZGVsaXZlcmFibGUgZGVwZW5kZW5jaWVzLic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0ZW1wbGF0ZWZvcm0uY29udHJvbHMuY29weURlbGl2ZXJhYmxlLnZhbHVlICYmICFwcm9qZWN0RGVwZW5kZW5jeS5pc05vRGVsaXZlcmFibGVzKSB7XHJcbiAgICAgICAgICAgIHRlbXBsYXRlZm9ybS5jb250cm9scy5jb3B5VGFzay5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFRlbXBsYXRlcyh0ZW1wbGF0ZVNlYXJjaENvbmRpdGlvbiwgdmlld0NvbmZpZ05hbWUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgICAgICAgICBpZiAodmlld0NvbmZpZ05hbWUpIHtcclxuICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSB2aWV3Q29uZmlnTmFtZTsvL3RoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Vmlld0NvbmZpZ0J5SWQodmlld0NvbmZpZ0lkKS5WSUVXO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmlld0NvbmZpZyA9IFNlYXJjaENvbmZpZ0NvbnN0YW50cy5TRUFSQ0hfTkFNRS5QUk9KRUNUXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHM6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgICAgICAgICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaW5kZXhlckZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlcklkQnlNYXBwZXJWYWx1ZShNUE1GaWVsZENvbnN0YW50cy5NUE1fUFJPSkVDVF9GRUlMRFMuUFJPSkVDVF9OQU1FKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnNSZXE6IFNlYXJjaFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmZpZ19pZDogc2VhcmNoQ29uZmlnID8gc2VhcmNoQ29uZmlnIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICBrZXl3b3JkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VhcmNoX2NvbmRpdGlvbjogdGVtcGxhdGVTZWFyY2hDb25kaXRpb25cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbl9saXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhY2V0X2NvbmRpdGlvbjogW11cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHNvcnRpbmdfbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0OiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRfaWQ6IGluZGV4ZXJGaWVsZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yZGVyOiAnQVNDJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2VfaW5kZXg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2Vfc2l6ZTogMTAwXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnNSZXEpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVtcGxhdGVPYmogPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsbFRlbXBsYXRlQ291bnQ6IHJlc3BvbnNlLmN1cnNvci50b3RhbF9yZWNvcmRzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxUZW1wbGF0ZUxpc3Q6IHJlc3BvbnNlLmRhdGFcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGVtcGxhdGVPYmopO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIHByb2plY3RzLicpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==