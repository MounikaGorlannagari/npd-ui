import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Renderer2, SimpleChanges, OnChanges } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { TaskConstants } from '../task.constants';
import { AssetStatusConstants } from '../../asset-card/asset_status_constants';
import { TaskService } from '../task.service';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ProjectService } from '../../../project/shared/services/project.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { DataTypeConstants } from '../../shared/constants/data-type.constants';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { AssetService } from '../../shared/services/asset.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { moveItemInArray } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
var TaskListViewComponent = /** @class */ (function () {
    function TaskListViewComponent(sharingService, taskService, formatToLocalePipe, utilService, fieldConfigService, assetService, loaderService, notificationService, projectService, dialog, renderer) {
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.utilService = utilService;
        this.fieldConfigService = fieldConfigService;
        this.assetService = assetService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.projectService = projectService;
        this.dialog = dialog;
        this.renderer = renderer;
        this.refreshTaskListData = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.taskListDataSource = new MatTableDataSource([]);
        this.metaDataDataTypes = DataTypeConstants;
        this.displayableMPMFields = [];
        this.isProjectOwner = false;
        this.displayColumns = [];
        this.assetStatusConstants = AssetStatusConstants;
        this.MPMFieldConstants = MPMFieldConstants.MPM_TASK_FIELDS;
        this.taskTypeConstants = TaskConstants.TASK_TYPE;
        this.page = 1;
        this.skip = 0;
        this.forceDeletion = false;
        this.affectedTask = [];
    }
    TaskListViewComponent.prototype.editTask = function (selectedTask) {
        var taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        var isApprovalTask = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_IS_APPROVAL);
        this.taskConfig.isApprovalTask = isApprovalTask === 'true' ? true : false;
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.editTaskHandler.next(this.taskConfig);
            this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.editTaskHandler.next(this.taskConfig);
            this.refresh();
        }
    };
    TaskListViewComponent.prototype.openCustomWorkflow = function (selectedTask) {
        this.customWorkflowHandler.next(selectedTask);
    };
    TaskListViewComponent.prototype.tableDrop = function (event) {
        var _this = this;
        var count = 0;
        if (this.displayColumns.find(function (column) { return column === 'Active'; })) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        var orderedFields = [];
        this.displayColumns.forEach(function (column) {
            if (!(column === 'Active' || column === 'Actions')) {
                orderedFields.push(_this.displayableFields.find(function (displayableField) { return displayableField.INDEXER_FIELD_ID === column; }));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    };
    TaskListViewComponent.prototype.onResizeMouseDown = function (event, column) {
        event.stopPropagation();
        event.preventDefault();
        var start = event.target;
        var pressed = true;
        var startX = event.x;
        var startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    };
    TaskListViewComponent.prototype.initResizableColumns = function (start, pressed, startX, startWidth, column) {
        var _this = this;
        this.renderer.listen('body', 'mousemove', function (event) {
            if (pressed) {
                var width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', function (event) {
            if (pressed) {
                pressed = false;
                _this.resizeDisplayableFields.next();
            }
        });
    };
    TaskListViewComponent.prototype.openComment = function (selectedTask) {
    };
    TaskListViewComponent.prototype.openTaskDetails = function (selectedTask, isNewDeliverable) {
        this.taskConfig.isNewDeliverable = isNewDeliverable;
        var taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.taskDetailsHandler.next(this.taskConfig);
            this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.taskDetailsHandler.next(this.taskConfig);
            this.refresh();
        }
    };
    TaskListViewComponent.prototype.isCancelledTask = function (selectedTask) {
    };
    TaskListViewComponent.prototype.deleteTask = function (selectedTask, isForceDelete) {
        var _this = this;
        var taskId = selectedTask.ID;
        var msg;
        if (this.isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        }
        else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }
        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(function (approvalTask) {
            console.log(approvalTask);
            if (approvalTask.tuple != undefined) {
                _this.forceDeletion = true;
                _this.affectedTask = [];
                var tasks = void 0;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                }
                else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(function (task) {
                    if (task.old.TaskView.isDeleted === 'false') {
                        _this.affectedTask.push(task.old.TaskView.name);
                    }
                });
            }
            var dialogRef = _this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    name: _this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result) {
                    if (isForceDelete) {
                        _this.taskService.forceDeleteTask(taskId, _this.isCustomWorkflow).subscribe(function (response) {
                            _this.notificationService.success('Task has been deleted successfully');
                            _this.refresh();
                        }, function (error) {
                            _this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                    else {
                        _this.taskService.softDeleteTask(taskId, _this.isCustomWorkflow).subscribe(function (response) {
                            _this.notificationService.success('Task has been deleted successfully');
                            _this.refresh();
                        }, function (error) {
                            _this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                }
            });
        });
    };
    TaskListViewComponent.prototype.openRequestDetails = function (selectedTask) {
    };
    TaskListViewComponent.prototype.showReminder = function (selectedTask) {
    };
    TaskListViewComponent.prototype.changeTaskStatus = function (selectedTask, status) {
    };
    TaskListViewComponent.prototype.loadingHandler = function (event) {
        (event) ? this.loaderService.show() : this.loaderService.hide();
    };
    TaskListViewComponent.prototype.refreshData = function (taskConfig) {
        this.displayableMPMFields = this.taskConfig.viewConfig ? this.utilService.getDisplayOrderData(this.displayableFields, this.taskConfig.viewConfig.listFieldOrder) : [];
        this.displayColumns = this.displayableMPMFields.map(function (column) { return column.INDEXER_FIELD_ID; });
        this.displayColumns = ['Active'].concat(this.displayColumns);
        if (this.isProjectOwner || (this.currentUserId && this.taskConfig.selectedProject &&
            this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id)) {
            this.displayColumns.push('Actions');
        }
        else if (!this.taskConfig.selectedProject) {
            this.displayColumns.push('Actions');
        }
        if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW) {
            this.isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
        }
        // this.taskConfig = taskConfig;
        this.taskListDataSource.data = [];
        this.taskListDataSource.data = this.taskConfig.taskList;
        if (this.sort && this.taskConfig.taskListSortFieldName) {
            this.sort.active = this.taskConfig.taskListSortFieldName;
            this.sort.direction = this.taskConfig.taskListSortOrder.toLowerCase();
        }
        this.taskListDataSource.sort = this.sort;
    };
    TaskListViewComponent.prototype.refresh = function () {
        this.refreshTaskListData.next(true);
    };
    TaskListViewComponent.prototype.getProperty = function (displayColum, taskData) {
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, taskData);
    };
    TaskListViewComponent.prototype.getPropertyByMapper = function (taskData, mapperName) {
        var displayColum = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.TASK);
        return this.getProperty(displayColum, taskData);
    };
    TaskListViewComponent.prototype.getPriority = function (taskData, displayColum) {
        var priorityObj = this.utilService.getPriorityIcon(this.getProperty(displayColum, taskData), MPM_LEVELS.TASK);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    };
    TaskListViewComponent.prototype.pagination = function (pagination) {
        this.pageSize = pagination.pageSize;
        this.top = pagination.pageSize;
        this.skip = (pagination.pageIndex * pagination.pageSize);
        this.page = 1 + pagination.pageIndex;
        this.pageNumber = this.page;
        this.loadingHandler(true);
        /*if (this.isCampaignAssetView) {
            this.routeService.gotoCampaignAssets(this.urlParams.isTemplate, this.campaignId, this.pageNumber, this.pageSize);
        } else {
            this.routeService.gotoProjectAssets(this.urlParams.isTemplate, this.projectId, this.urlParams.campaignId, this.pageNumber, this.pageSize);
        }
        this.getAssets();*/
    };
    TaskListViewComponent.prototype.converToLocalDate = function (obj, path) {
        return this.formatToLocalePipe.transform(this.taskService.converToLocalDate(obj, path));
    };
    /**
     * @since MPMV3-946
     * @param taskType
     */
    TaskListViewComponent.prototype.getTaskTypeIcon = function (taskType) {
        return this.taskService.getTaskIconByTaskType(taskType);
    };
    TaskListViewComponent.prototype.getStatusCheck = function (selectedTask) {
        return this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_REJECTED;
    };
    TaskListViewComponent.prototype.getFinalStatusCheck = function (selectedTask) {
        return this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED ||
            this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_STATUS_TYPE) === StatusTypes.FINAL_REJECTED;
    };
    TaskListViewComponent.prototype.hasCurrentWorkflowActions = function (selectedTask) {
        if (this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            var taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
            var taskId_1;
            if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
                taskId_1 = taskItemId.split('.')[1];
            }
            return this.taskConfig.workflowActionRules.find(function (workflowActionRule) { return workflowActionRule.TaskId === taskId_1; }) ? true : false;
        }
        else {
            return false;
        }
    };
    TaskListViewComponent.prototype.getCurrentWorkflowActions = function (selectedTask) {
        if (this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            var taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
            var taskId_2;
            if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
                taskId_2 = taskItemId.split('.')[1];
            }
            var currentWorkflowActionRules = this.taskConfig.workflowActionRules.filter(function (workflowActionRule) { return workflowActionRule.TaskId === taskId_2; });
            var currentActions_1 = [];
            currentWorkflowActionRules.forEach(function (rule) {
                if (rule && rule.Action) {
                    if (rule.Action.length > 0) {
                        rule.Action.forEach(function (action) {
                            var selectedAction = currentActions_1.find(function (currentAction) { return currentAction.Id === action.Id; });
                            if (!selectedAction) {
                                currentActions_1.push(action);
                            }
                        });
                    }
                    else {
                        currentActions_1.push(rule.Action);
                    }
                }
            });
            return currentActions_1;
        }
        else {
            return [];
        }
    };
    TaskListViewComponent.prototype.triggerActionRule = function (selectedTask, actionId) {
        var _this = this;
        var taskItemId = this.getPropertyByMapper(selectedTask, this.MPMFieldConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            var taskId = taskItemId.split('.')[1];
            this.loaderService.show();
            this.taskService.triggerRuleOnAction(taskId, actionId).subscribe(function (response) {
                _this.loaderService.hide();
                _this.notificationService.success('Triggering action has been initiated');
            });
        }
    };
    TaskListViewComponent.prototype.ngOnChanges = function (changes) {
        if (changes.displayableFields && !changes.displayableFields.firstChange &&
            (JSON.stringify(changes.displayableFields.currentValue) !== JSON.stringify(changes.displayableFields.previousValue))) {
            this.refreshData(this.taskConfig);
        }
    };
    TaskListViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        var appConfig = this.sharingService.getAppConfig();
        this.top = this.utilService.convertStringToNumber(appConfig["DEFAULT_PAGINATION"]);
        this.pageSize = this.utilService.convertStringToNumber(appConfig["DEFAULT_PAGINATION"]);
        this.pageNumber = this.page;
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(function (projectResponse) {
                _this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD === 'true' ? true : false;
                _this.isTemplate = _this.taskConfig.isTemplate;
                _this.currentUserId = _this.sharingService.getCurrentUserItemID();
                if (_this.currentUserId && _this.taskConfig.selectedProject &&
                    _this.currentUserId === _this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    _this.isProjectOwner = true;
                }
                if (_this.taskConfig.metaDataFields && _this.taskConfig.taskList) {
                    _this.refreshData(_this.taskConfig);
                }
                if (!_this.taskConfig.taskList) {
                    _this.taskConfig.taskList = [];
                }
                if (!_this.taskConfig.metaDataFields) {
                    _this.taskConfig.metaDataFields = [];
                }
                if (_this.sort) {
                    _this.sort.disableClear = true;
                    _this.sort.sortChange.subscribe(function () {
                        if (_this.sort.active && _this.sort.direction) {
                            var selectedSortList = {
                                sortType: _this.sort.direction.toUpperCase(),
                                selectedSortItem: _this.sort.active
                            };
                            _this.sortChange.emit(selectedSortList);
                        }
                    });
                }
            });
        }
        /* this.currentUserId = this.sharingService.getCurrentUserItemID();
        if (this.currentUserId && this.taskConfig.selectedProject &&
            this.currentUserId === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.isProjectOwner = true;
        }
        if (this.taskConfig.metaDataFields && this.taskConfig.taskList) {
            this.refreshData(this.taskConfig);
        }

        if (!this.taskConfig.taskList) {
            this.taskConfig.taskList = [];
        }

        if (!this.taskConfig.metaDataFields) {
            this.taskConfig.metaDataFields = [];
        }

        if (this.sort) {
            this.sort.disableClear = true;
            this.sort.sortChange.subscribe(() => {
                if (this.sort.active && this.sort.direction) {
                    const selectedSortList = {
                        sortType: this.sort.direction.toUpperCase(),
                        selectedSortItem: this.sort.active
                    };

                    this.sortChange.emit(selectedSortList);
                }
            });
        } */
    };
    TaskListViewComponent.ctorParameters = function () { return [
        { type: SharingService },
        { type: TaskService },
        { type: FormatToLocalePipe },
        { type: UtilService },
        { type: FieldConfigService },
        { type: AssetService },
        { type: LoaderService },
        { type: NotificationService },
        { type: ProjectService },
        { type: MatDialog },
        { type: Renderer2 }
    ]; };
    __decorate([
        Input()
    ], TaskListViewComponent.prototype, "taskConfig", void 0);
    __decorate([
        Input()
    ], TaskListViewComponent.prototype, "displayableFields", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "refreshTaskListData", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "editTaskHandler", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "taskDetailsHandler", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "sortChange", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "customWorkflowHandler", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "orderedDisplayableFields", void 0);
    __decorate([
        Output()
    ], TaskListViewComponent.prototype, "resizeDisplayableFields", void 0);
    __decorate([
        ViewChild(MatSort, { static: true })
    ], TaskListViewComponent.prototype, "sort", void 0);
    TaskListViewComponent = __decorate([
        Component({
            selector: 'mpm-task-list-view',
            template: "<table class=\"task-list\" mat-table [dataSource]=\"taskListDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Active\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-icon [ngClass]=\"[ getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE)==='true' ? 'active-task-icon' : 'inactive-task-icon']\"\r\n                \r\n                matTooltip=\"{{getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE) === 'true' ? 'Active Task' : 'Inactive Task'}}\"><!-- [style.color]=\"getActiveColor(getPropertyByMapper(row, MPMFieldConstants.IS_TASK_ACTIVE))\" -->\r\n                {{getTaskTypeIcon(getPropertyByMapper(row, MPMFieldConstants.TASK_TYPE))}}</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMPMFields\">y\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':column.DATA_TYPE === 'string'}\">\r\n            <span>\r\n                <div *ngIf=\"(column.MAPPER_NAME !== MPMFieldConstants.TASK_PRIORITY)\">\r\n                    <a class=\"pointer\" matTooltip=\"{{getProperty(column, row)}}\"\r\n                        (click)=\"(column.MAPPER_NAME === MPMFieldConstants.TASK_ID || column.MAPPER_NAME === MPMFieldConstants.TASK_ITEM_ID || column.MAPPER_NAME === MPMFieldConstants.TASK_NAME) && openTaskDetails(row, false)\">{{getProperty(column, row) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === MPMFieldConstants.TASK_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">\r\n                        {{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <div>\r\n                <button mat-icon-button matTooltip=\"More\" [matMenuTriggerFor]=\"TaskActions\"\r\n                    [matMenuTriggerData]=\"{selectedTask: row}\"\r\n                    [disabled]=\"!isProjectOwner && (!((row.REFERENCE_LINK !== null) && (row.REFERENCE_LINK_ID !== null)))\">\r\n                    <mat-icon>more_vert</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row.asset_id}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"project-element-row\"></tr>\r\n</table>\r\n<mat-menu #TaskActions=\"matMenu\">\r\n    <ng-template matMenuContent let-selectedTask=\"selectedTask\">\r\n        <button mat-menu-item matTooltip=\"Add New Deliverable\" (click)=\"openTaskDetails(selectedTask, true)\"\r\n            [disabled]=\"getStatusCheck(selectedTask) || !taskConfig.isProjectOwner || taskConfig.isReadOnly || (getPropertyByMapper(selectedTask, MPMFieldConstants.TASK_TYPE) !== taskTypeConstants.NORMAL_TASK)\">\r\n            <mat-icon>add</mat-icon>\r\n            <span>Add New Deliverable</span>\r\n        </button>\r\n        <button mat-menu-item matTooltip=\"Edit Task\" (click)=\"editTask(selectedTask)\"\r\n            [disabled]=\"getFinalStatusCheck(selectedTask) || !taskConfig.isProjectOwner || taskConfig.isReadOnly\">\r\n            <mat-icon>edit</mat-icon>\r\n            <span>Edit Task</span>\r\n        </button>\r\n    </ng-template>\r\n</mat-menu>\r\n<mat-menu #taskStatusMenu=\"matMenu\">\r\n    <ng-template matMenuContent let-selectedTask=\"selectedTask\">\r\n        <span *ngFor=\"let status of taskConfig.taskStatus\">\r\n            <button mat-menu-item matTooltip=\"Task Status\" (click)=\"changeTaskStatus(selectedTask, status)\"\r\n                *ngIf=\"status.DISPLAY_NAME != selectedTask.STATUS_DISPLAY_NAME\">\r\n                <span>{{status.DISPLAY_NAME}}</span>\r\n            </button>\r\n        </span>\r\n    </ng-template>\r\n</mat-menu>\r\n\r\n<!--<div class=\"flex-row\" *ngIf=\"totalAssetsCount > 0\">\r\n    <div class=\"flex-row-item right\">\r\n        <mpm-pagination [length]=\"totalAssetsCount\" [pageSize]=\"pageSize\" [pageIndex]=\"page\"\r\n            (paginator)=\"pagination($event)\"></mpm-pagination>\r\n    </div>\r\n</div>--->\r\n\r\n<div *ngIf=\"taskConfig.taskTotalCount === 0\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No task is available.</button>\r\n</div>",
            styles: ["table.task-list{width:100%}table.task-list tr{height:40px}table.task-list tr th:last-child{padding-left:12px}table.task-list tr td a.mat-button{padding:0;text-align:left}table.task-list tr td mat-icon{font-size:16px}.circle{height:10px;width:10px;background-color:#e4e4e4;border-radius:10px}.duedate-current,.duedate-extended{opacity:.9}.active-refrenced-task{background-color:#73736f}.text-summary{text-overflow:ellipsis;overflow:hidden;white-space:nowrap;width:105px;cursor:pointer;-webkit-user-select:all!important;-moz-user-select:all!important;user-select:all!important}.task-title{display:inline-block;text-overflow:ellipsis;overflow:hidden;white-space:nowrap;min-width:1rem;max-width:250px}.task-id{cursor:pointer}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}.badge-info{min-height:22px!important;font-size:12px!important}span div .pointer{cursor:pointer}td.wrap-text-custom span>div{max-width:14em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
        })
    ], TaskListViewComponent);
    return TaskListViewComponent;
}());
export { TaskListViewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1saXN0LXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLWxpc3Qtdmlldy90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvSCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFLaEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNsRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFHL0UsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFZLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBRW5GLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDbkUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDRFQUE0RSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQWUsZUFBZSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdEUsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFPNUI7SUFtQ0ksK0JBQ1csY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsa0JBQXNDLEVBQ3RDLFdBQXdCLEVBQ3hCLGtCQUFzQyxFQUN0QyxZQUEwQixFQUMxQixhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsY0FBOEIsRUFDOUIsTUFBaUIsRUFDakIsUUFBbUI7UUFWbkIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLGFBQVEsR0FBUixRQUFRLENBQVc7UUExQ3BCLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNoRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFJNUQsdUJBQWtCLEdBQUcsSUFBSSxrQkFBa0IsQ0FBTSxFQUFFLENBQUMsQ0FBQztRQUNyRCxzQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUN0Qyx5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBQzVDLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUN0RCxzQkFBaUIsR0FBRyxhQUFhLENBQUMsU0FBUyxDQUFDO1FBQzVDLFNBQUksR0FBRyxDQUFDLENBQUM7UUFDVCxTQUFJLEdBQUcsQ0FBQyxDQUFDO1FBU1Qsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsaUJBQVksR0FBRyxFQUFFLENBQUM7SUFjZCxDQUFDO0lBRUwsd0NBQVEsR0FBUixVQUFTLFlBQVk7UUFDakIsSUFBTSxVQUFVLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkcsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUN2RyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxjQUFjLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMxRSxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDcEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNsRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7WUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFRCxrREFBa0IsR0FBbEIsVUFBbUIsWUFBWTtRQUMzQixJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCx5Q0FBUyxHQUFULFVBQVUsS0FBNEI7UUFBdEMsaUJBYUM7UUFaRyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxLQUFLLFFBQVEsRUFBbkIsQ0FBbUIsQ0FBQyxFQUFFO1lBQ3pELEtBQUssR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1NBQ3JCO1FBQ0QsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLGFBQWEsR0FBRyxLQUFLLEVBQUUsS0FBSyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsQ0FBQztRQUM5RixJQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO1lBQzlCLElBQUksQ0FBQyxDQUFDLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxLQUFLLFNBQVMsQ0FBQyxFQUFFO2dCQUNoRCxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxnQkFBZ0IsSUFBSSxPQUFBLGdCQUFnQixDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBNUMsQ0FBNEMsQ0FBQyxDQUFDLENBQUM7YUFDckg7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGlEQUFpQixHQUFqQixVQUFrQixLQUFLLEVBQUUsTUFBTTtRQUMzQixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDM0IsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQU0sTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDdkIsSUFBTSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELG9EQUFvQixHQUFwQixVQUFxQixLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTTtRQUEvRCxpQkFhQztRQVpHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUUsVUFBQyxLQUFLO1lBQzVDLElBQUksT0FBTyxFQUFFO2dCQUNULElBQU0sS0FBSyxHQUFHLFVBQVUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLFVBQUMsS0FBSztZQUMxQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNoQixLQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDdkM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCwyQ0FBVyxHQUFYLFVBQVksWUFBWTtJQUN4QixDQUFDO0lBRUQsK0NBQWUsR0FBZixVQUFnQixZQUFZLEVBQUUsZ0JBQWdCO1FBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDcEQsSUFBTSxVQUFVLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkcsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3BFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO2FBQU0sSUFBSSxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNsRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7WUFDcEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO0lBQ0wsQ0FBQztJQUVELCtDQUFlLEdBQWYsVUFBZ0IsWUFBWTtJQUM1QixDQUFDO0lBRUQsMENBQVUsR0FBVixVQUFXLFlBQVksRUFBQyxhQUFxQjtRQUE3QyxpQkEwREM7UUF6REcsSUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLEVBQUUsQ0FBQztRQUMvQixJQUFJLEdBQUcsQ0FBQztRQUNSLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3ZCLEdBQUcsR0FBRyxxRkFBcUYsQ0FBQztTQUMvRjthQUFNO1lBQ0gsR0FBRyxHQUFHLHFEQUFxRCxDQUFDO1NBQy9EO1FBQ0QsY0FBYztRQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsWUFBWTtZQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO1lBQ3pCLElBQUksWUFBWSxDQUFDLEtBQUssSUFBSSxTQUFTLEVBQUU7Z0JBQ2pDLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2dCQUMxQixLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxLQUFLLFNBQUEsQ0FBQztnQkFDVixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3BDLEtBQUssR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDaEM7cUJBQU07b0JBQ0gsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7aUJBQzlCO2dCQUNELEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUNkLElBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsU0FBUyxLQUFLLE9BQU8sRUFBRTt3QkFDeEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2xEO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFDRCxJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsR0FBRztvQkFDWixJQUFJLEVBQUUsS0FBSSxDQUFDLFlBQVk7b0JBQ3ZCLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDckI7YUFDSixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtnQkFDcEMsSUFBSSxNQUFNLEVBQUU7b0JBQ1IsSUFBRyxhQUFhLEVBQUU7d0JBQ2QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsQ0FDckUsVUFBQSxRQUFROzRCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQzs0QkFDdkUsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUNuQixDQUFDLEVBQUUsVUFBQSxLQUFLOzRCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQzt3QkFDcEYsQ0FBQyxDQUFDLENBQUM7cUJBQ1Y7eUJBQU07d0JBQ0MsS0FBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsQ0FDcEUsVUFBQSxRQUFROzRCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsb0NBQW9DLENBQUMsQ0FBQzs0QkFDdkUsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUNuQixDQUFDLEVBQUUsVUFBQSxLQUFLOzRCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQzt3QkFDcEYsQ0FBQyxDQUFDLENBQUM7cUJBQ2Q7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtEQUFrQixHQUFsQixVQUFtQixZQUFZO0lBQy9CLENBQUM7SUFFRCw0Q0FBWSxHQUFaLFVBQWEsWUFBWTtJQUN6QixDQUFDO0lBRUQsZ0RBQWdCLEdBQWhCLFVBQWlCLFlBQVksRUFBRSxNQUFNO0lBRXJDLENBQUM7SUFFRCw4Q0FBYyxHQUFkLFVBQWUsS0FBSztRQUNoQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3BFLENBQUM7SUFFRCwyQ0FBVyxHQUFYLFVBQVksVUFBVTtRQUNsQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDdEssSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLFVBQUMsTUFBZ0IsSUFBSyxPQUFBLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO1FBQ25HLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBRTdELElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlO1lBQzdFLElBQUksQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDOUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDdkM7YUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUU7WUFDekMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDdkM7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQ2hIO1FBQ0QsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7UUFDeEQsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEVBQUU7WUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQztZQUN6RCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3pFO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQzdDLENBQUM7SUFFRCx1Q0FBTyxHQUFQO1FBQ0ksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsMkNBQVcsR0FBWCxVQUFZLFlBQXNCLEVBQUUsUUFBYTtRQUM3QyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDeEYsQ0FBQztJQUNELG1EQUFtQixHQUFuQixVQUFvQixRQUFRLEVBQUUsVUFBVTtRQUNwQyxJQUFNLFlBQVksR0FBYSxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUNELDJDQUFXLEdBQVgsVUFBWSxRQUFRLEVBQUUsWUFBWTtRQUM5QixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsRUFDekYsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXJCLE9BQU87WUFDSCxLQUFLLEVBQUUsV0FBVyxDQUFDLEtBQUs7WUFDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJO1lBQ3RCLE9BQU8sRUFBRSxXQUFXLENBQUMsT0FBTztTQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVELDBDQUFVLEdBQVYsVUFBVyxVQUFlO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztRQUNwQyxJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7UUFDL0IsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBRTVCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUI7Ozs7OzJCQUttQjtJQUN2QixDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLEdBQUcsRUFBRSxJQUFJO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFDRDs7O09BR0c7SUFDSCwrQ0FBZSxHQUFmLFVBQWdCLFFBQVE7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRCw4Q0FBYyxHQUFkLFVBQWUsWUFBWTtRQUN2QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEtBQUssV0FBVyxDQUFDLGNBQWM7WUFDakgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxXQUFXLENBQUMsZUFBZTtZQUMvRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUM7SUFDdkgsQ0FBQztJQUNELG1EQUFtQixHQUFuQixVQUFvQixZQUFZO1FBQzVCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxXQUFXLENBQUMsY0FBYztZQUNqSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxlQUFlO1lBQy9HLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQztJQUN2SCxDQUFDO0lBRUQseURBQXlCLEdBQXpCLFVBQTBCLFlBQVk7UUFDbEMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN2RixJQUFNLFVBQVUsR0FBVyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2RyxJQUFJLFFBQU0sQ0FBQztZQUNYLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDcEUsUUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckM7WUFDRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQUEsa0JBQWtCLElBQUksT0FBQSxrQkFBa0IsQ0FBQyxNQUFNLEtBQUssUUFBTSxFQUFwQyxDQUFvQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1NBQzlIO2FBQU07WUFDSCxPQUFPLEtBQUssQ0FBQztTQUNoQjtJQUNMLENBQUM7SUFFRCx5REFBeUIsR0FBekIsVUFBMEIsWUFBWTtRQUNsQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZGLElBQU0sVUFBVSxHQUFXLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZHLElBQUksUUFBTSxDQUFDO1lBQ1gsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNwRSxRQUFNLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNyQztZQUNELElBQU0sMEJBQTBCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxrQkFBa0IsSUFBSSxPQUFBLGtCQUFrQixDQUFDLE1BQU0sS0FBSyxRQUFNLEVBQXBDLENBQW9DLENBQUMsQ0FBQztZQUMxSSxJQUFNLGdCQUFjLEdBQUcsRUFBRSxDQUFDO1lBQzFCLDBCQUEwQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ25DLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ3JCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07NEJBQ3RCLElBQU0sY0FBYyxHQUFHLGdCQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsYUFBYSxJQUFJLE9BQUEsYUFBYSxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsRUFBRSxFQUE5QixDQUE4QixDQUFDLENBQUM7NEJBQzVGLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0NBQ2pCLGdCQUFjLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzZCQUMvQjt3QkFDTCxDQUFDLENBQUMsQ0FBQztxQkFDTjt5QkFBTTt3QkFDSCxnQkFBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQ3BDO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLGdCQUFjLENBQUM7U0FDekI7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLFlBQVksRUFBRSxRQUFRO1FBQXhDLGlCQVVDO1FBVEcsSUFBTSxVQUFVLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDdkcsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3BFLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNyRSxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCwyQ0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsaUJBQWlCLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsV0FBVztZQUNuRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7WUFDdEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDckM7SUFDTCxDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUFBLGlCQTBFQztRQXpFRyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUM1QixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxlQUFlO2dCQUNwRixLQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBRXBGLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBRTdDLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNoRSxJQUFJLEtBQUksQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlO29CQUNyRCxLQUFJLENBQUMsYUFBYSxLQUFLLEtBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0YsS0FBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7aUJBQzlCO2dCQUNELElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7b0JBQzVELEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNyQztnQkFFRCxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7b0JBQzNCLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztpQkFDakM7Z0JBRUQsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO29CQUNqQyxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7aUJBQ3ZDO2dCQUVELElBQUksS0FBSSxDQUFDLElBQUksRUFBRTtvQkFDWCxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQzt3QkFDM0IsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTs0QkFDekMsSUFBTSxnQkFBZ0IsR0FBRztnQ0FDckIsUUFBUSxFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRTtnQ0FDM0MsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxNQUFNOzZCQUNyQyxDQUFDOzRCQUVGLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7eUJBQzFDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNOO1lBRUwsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQTZCSTtJQUNSLENBQUM7O2dCQW5ZMEIsY0FBYztnQkFDakIsV0FBVztnQkFDSixrQkFBa0I7Z0JBQ3pCLFdBQVc7Z0JBQ0osa0JBQWtCO2dCQUN4QixZQUFZO2dCQUNYLGFBQWE7Z0JBQ1AsbUJBQW1CO2dCQUN4QixjQUFjO2dCQUN0QixTQUFTO2dCQUNQLFNBQVM7O0lBNUNyQjtRQUFSLEtBQUssRUFBRTs2REFBaUM7SUFDaEM7UUFBUixLQUFLLEVBQUU7b0VBQW1CO0lBQ2pCO1FBQVQsTUFBTSxFQUFFO3NFQUErQztJQUM5QztRQUFULE1BQU0sRUFBRTtrRUFBMkM7SUFDMUM7UUFBVCxNQUFNLEVBQUU7cUVBQThDO0lBQzdDO1FBQVQsTUFBTSxFQUFFOzZEQUFzQztJQUNyQztRQUFULE1BQU0sRUFBRTt3RUFBaUQ7SUFDaEQ7UUFBVCxNQUFNLEVBQUU7MkVBQW9EO0lBQ25EO1FBQVQsTUFBTSxFQUFFOzBFQUFtRDtJQUV0QjtRQUFyQyxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO3VEQUFlO0lBWjNDLHFCQUFxQjtRQUxqQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLDZ5SkFBOEM7O1NBRWpELENBQUM7T0FDVyxxQkFBcUIsQ0F3YWpDO0lBQUQsNEJBQUM7Q0FBQSxBQXhhRCxJQXdhQztTQXhhWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIFJlbmRlcmVyMiwgU2ltcGxlQ2hhbmdlcywgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0JztcclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBc3NldFN0YXR1c0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL2Fzc2V0LWNhcmQvYXNzZXRfc3RhdHVzX2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybWF0VG9Mb2NhbGVQaXBlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS5waXBlJztcclxuaW1wb3J0IHsgUGFnaW5hdGlvbkNvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL3BhZ2luYXRpb24vcGFnaW5hdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3JNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InO1xyXG5cclxuXHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2plY3RTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvcHJvamVjdC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgRGF0YVR5cGVDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL2RhdGEtdHlwZS5jb25zdGFudHMnO1xyXG5cclxuaW1wb3J0IHsgT3RtbU1ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vdG1tLW1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkLCBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25maWdJbnRlcmZhY2UgfSBmcm9tICcuLi90YXNrLmNvbmZpZy5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBBc3NldFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wLCBtb3ZlSXRlbUluQXJyYXkgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS10YXNrLWxpc3QtdmlldycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdGFzay1saXN0LXZpZXcuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdGFzay1saXN0LXZpZXcuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFza0xpc3RWaWV3Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIHRhc2tDb25maWc6IFRhc2tDb25maWdJbnRlcmZhY2U7XHJcbiAgICBASW5wdXQoKSBkaXNwbGF5YWJsZUZpZWxkcztcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoVGFza0xpc3REYXRhID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZWRpdFRhc2tIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgdGFza0RldGFpbHNIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc29ydENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGN1c3RvbVdvcmtmbG93SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG9yZGVyZWREaXNwbGF5YWJsZUZpZWxkcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHJlc2l6ZURpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBzb3J0OiBNYXRTb3J0O1xyXG5cclxuICAgIHRhc2tMaXN0RGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2U8YW55PihbXSk7XHJcbiAgICBtZXRhRGF0YURhdGFUeXBlcyA9IERhdGFUeXBlQ29uc3RhbnRzO1xyXG4gICAgZGlzcGxheWFibGVNUE1GaWVsZHM6IEFycmF5PE1QTUZpZWxkPiA9IFtdO1xyXG4gICAgaXNQcm9qZWN0T3duZXIgPSBmYWxzZTtcclxuICAgIGRpc3BsYXlDb2x1bW5zID0gW107XHJcbiAgICBhc3NldFN0YXR1c0NvbnN0YW50cyA9IEFzc2V0U3RhdHVzQ29uc3RhbnRzO1xyXG4gICAgTVBNRmllbGRDb25zdGFudHMgPSBNUE1GaWVsZENvbnN0YW50cy5NUE1fVEFTS19GSUVMRFM7XHJcbiAgICB0YXNrVHlwZUNvbnN0YW50cyA9IFRhc2tDb25zdGFudHMuVEFTS19UWVBFO1xyXG4gICAgcGFnZSA9IDE7XHJcbiAgICBza2lwID0gMDtcclxuICAgIHBhZ2VTaXplOiBudW1iZXI7XHJcbiAgICB0b3A6IG51bWJlcjtcclxuICAgIHBhZ2VOdW1iZXI6IG51bWJlcjtcclxuXHJcbiAgICBjdXJyZW50VXNlcklkO1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBib29sZWFuO1xyXG4gICAgaXNDdXN0b21Xb3JrZmxvdztcclxuICAgIGlzVGVtcGxhdGU6IGJvb2xlYW47XHJcbiAgICBmb3JjZURlbGV0aW9uID0gZmFsc2U7XHJcbiAgICBhZmZlY3RlZFRhc2sgPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZvcm1hdFRvTG9jYWxlUGlwZTogRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBlZGl0VGFzayhzZWxlY3RlZFRhc2spIHtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkOiBzdHJpbmcgPSB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgY29uc3QgaXNBcHByb3ZhbFRhc2sgPSB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfSVNfQVBQUk9WQUwpO1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayA9IGlzQXBwcm92YWxUYXNrID09PSAndHJ1ZScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5pbmNsdWRlcygnLicpICYmIHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0lkID0gdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRUYXNrSGFuZGxlci5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGFza0l0ZW1JZCAmJiAhKHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSkpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tJZCA9IHRhc2tJdGVtSWQ7XHJcbiAgICAgICAgICAgIHRoaXMuZWRpdFRhc2tIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5DdXN0b21Xb3JrZmxvdyhzZWxlY3RlZFRhc2spIHtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93SGFuZGxlci5uZXh0KHNlbGVjdGVkVGFzayk7XHJcbiAgICB9XHJcblxyXG4gICAgdGFibGVEcm9wKGV2ZW50OiBDZGtEcmFnRHJvcDxzdHJpbmdbXT4pIHtcclxuICAgICAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgICAgIGlmICh0aGlzLmRpc3BsYXlDb2x1bW5zLmZpbmQoY29sdW1uID0+IGNvbHVtbiA9PT0gJ0FjdGl2ZScpKSB7XHJcbiAgICAgICAgICAgIGNvdW50ID0gY291bnQgKyAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBtb3ZlSXRlbUluQXJyYXkodGhpcy5kaXNwbGF5Q29sdW1ucywgZXZlbnQucHJldmlvdXNJbmRleCArIGNvdW50LCBldmVudC5jdXJyZW50SW5kZXggKyBjb3VudCk7XHJcbiAgICAgICAgY29uc3Qgb3JkZXJlZEZpZWxkcyA9IFtdO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICAgICAgICBpZiAoIShjb2x1bW4gPT09ICdBY3RpdmUnIHx8IGNvbHVtbiA9PT0gJ0FjdGlvbnMnKSkge1xyXG4gICAgICAgICAgICAgICAgb3JkZXJlZEZpZWxkcy5wdXNoKHRoaXMuZGlzcGxheWFibGVGaWVsZHMuZmluZChkaXNwbGF5YWJsZUZpZWxkID0+IGRpc3BsYXlhYmxlRmllbGQuSU5ERVhFUl9GSUVMRF9JRCA9PT0gY29sdW1uKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLm9yZGVyZWREaXNwbGF5YWJsZUZpZWxkcy5uZXh0KG9yZGVyZWRGaWVsZHMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uUmVzaXplTW91c2VEb3duKGV2ZW50LCBjb2x1bW4pIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0ID0gZXZlbnQudGFyZ2V0O1xyXG4gICAgICAgIGNvbnN0IHByZXNzZWQgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0WCA9IGV2ZW50Lng7XHJcbiAgICAgICAgY29uc3Qgc3RhcnRXaWR0aCA9ICQoc3RhcnQpLnBhcmVudCgpLndpZHRoKCk7XHJcbiAgICAgICAgdGhpcy5pbml0UmVzaXphYmxlQ29sdW1ucyhzdGFydCwgcHJlc3NlZCwgc3RhcnRYLCBzdGFydFdpZHRoLCBjb2x1bW4pO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRSZXNpemFibGVDb2x1bW5zKHN0YXJ0LCBwcmVzc2VkLCBzdGFydFgsIHN0YXJ0V2lkdGgsIGNvbHVtbikge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIubGlzdGVuKCdib2R5JywgJ21vdXNlbW92ZScsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAocHJlc3NlZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgd2lkdGggPSBzdGFydFdpZHRoICsgKGV2ZW50LnggLSBzdGFydFgpO1xyXG4gICAgICAgICAgICAgICAgY29sdW1uLndpZHRoID0gd2lkdGg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLmxpc3RlbignYm9keScsICdtb3VzZXVwJywgKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICBwcmVzc2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2l6ZURpc3BsYXlhYmxlRmllbGRzLm5leHQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBvcGVuQ29tbWVudChzZWxlY3RlZFRhc2spIHtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuVGFza0RldGFpbHMoc2VsZWN0ZWRUYXNrLCBpc05ld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzTmV3RGVsaXZlcmFibGUgPSBpc05ld0RlbGl2ZXJhYmxlO1xyXG4gICAgICAgIGNvbnN0IHRhc2tJdGVtSWQ6IHN0cmluZyA9IHRoaXMuZ2V0UHJvcGVydHlCeU1hcHBlcihzZWxlY3RlZFRhc2ssIHRoaXMuTVBNRmllbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMudGFza0RldGFpbHNIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXNrSXRlbUlkICYmICEodGFza0l0ZW1JZC5pbmNsdWRlcygnLicpKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0lkID0gdGFza0l0ZW1JZDtcclxuICAgICAgICAgICAgdGhpcy50YXNrRGV0YWlsc0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNDYW5jZWxsZWRUYXNrKHNlbGVjdGVkVGFzaykge1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVRhc2soc2VsZWN0ZWRUYXNrLGlzRm9yY2VEZWxldGU6Ym9vbGVhbikge1xyXG4gICAgICAgIGNvbnN0IHRhc2tJZCA9IHNlbGVjdGVkVGFzay5JRDtcclxuICAgICAgICBsZXQgbXNnO1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ3VzdG9tV29ya2Zsb3cpIHtcclxuICAgICAgICAgICAgbXNnID0gJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIHRhc2tzIGFuZCByZWNvbmZpZ3VyZSB0aGUgd29ya2Zsb3cgcnVsZSBlbmdpbmU/JztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtc2cgPSAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGUgdGFza3MgYW5kIEV4aXQ/JztcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gIE1QTVYzLTIwOTRcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmdldEFsbEFwcHJvdmFsVGFza3ModGFza0lkKS5zdWJzY3JpYmUoYXBwcm92YWxUYXNrID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coYXBwcm92YWxUYXNrKVxyXG4gICAgICAgICAgICBpZiAoYXBwcm92YWxUYXNrLnR1cGxlICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZURlbGV0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrID0gW107XHJcbiAgICAgICAgICAgICAgICBsZXQgdGFza3M7XHJcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoYXBwcm92YWxUYXNrLnR1cGxlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tzID0gW2FwcHJvdmFsVGFzay50dXBsZV07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tzID0gYXBwcm92YWxUYXNrLnR1cGxlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGFza3MuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZih0YXNrLm9sZC5UYXNrVmlldy5pc0RlbGV0ZWQgPT09ICdmYWxzZScpIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrLnB1c2godGFzay5vbGQuVGFza1ZpZXcubmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogbXNnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHRoaXMuYWZmZWN0ZWRUYXNrLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZihpc0ZvcmNlRGVsZXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuZm9yY2VEZWxldGVUYXNrKHRhc2tJZCwgdGhpcy5pc0N1c3RvbVdvcmtmbG93KS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1Rhc2sgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0RlbGV0ZSBUYXNrIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2Uuc29mdERlbGV0ZVRhc2sodGFza0lkLCB0aGlzLmlzQ3VzdG9tV29ya2Zsb3cpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdUYXNrIGhhcyBiZWVuIGRlbGV0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdEZWxldGUgVGFzayBvcGVyYXRpb24gZmFpbGVkLCB0cnkgYWdhaW4gbGF0ZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblJlcXVlc3REZXRhaWxzKHNlbGVjdGVkVGFzaykge1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dSZW1pbmRlcihzZWxlY3RlZFRhc2spIHtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VUYXNrU3RhdHVzKHNlbGVjdGVkVGFzaywgc3RhdHVzKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGxvYWRpbmdIYW5kbGVyKGV2ZW50KSB7XHJcbiAgICAgICAgKGV2ZW50KSA/IHRoaXMubG9hZGVyU2VydmljZS5zaG93KCkgOiB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hEYXRhKHRhc2tDb25maWcpIHtcclxuICAgICAgICB0aGlzLmRpc3BsYXlhYmxlTVBNRmllbGRzID0gdGhpcy50YXNrQ29uZmlnLnZpZXdDb25maWcgPyB0aGlzLnV0aWxTZXJ2aWNlLmdldERpc3BsYXlPcmRlckRhdGEodGhpcy5kaXNwbGF5YWJsZUZpZWxkcywgdGhpcy50YXNrQ29uZmlnLnZpZXdDb25maWcubGlzdEZpZWxkT3JkZXIpIDogW107XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucyA9IHRoaXMuZGlzcGxheWFibGVNUE1GaWVsZHMubWFwKChjb2x1bW46IE1QTUZpZWxkKSA9PiBjb2x1bW4uSU5ERVhFUl9GSUVMRF9JRCk7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucyA9IFsnQWN0aXZlJ10uY29uY2F0KHRoaXMuZGlzcGxheUNvbHVtbnMpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc1Byb2plY3RPd25lciB8fCAodGhpcy5jdXJyZW50VXNlcklkICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiZcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlcklkID09PSB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fUFJPSkVDVF9PV05FUlsnSWRlbnRpdHktaWQnXS5JZCkpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5Q29sdW1ucy5wdXNoKCdBY3Rpb25zJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zLnB1c2goJ0FjdGlvbnMnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuSVNfQ1VTVE9NX1dPUktGTE9XKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNDdXN0b21Xb3JrZmxvdyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuSVNfQ1VTVE9NX1dPUktGTE9XKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gdGhpcy50YXNrQ29uZmlnID0gdGFza0NvbmZpZztcclxuICAgICAgICB0aGlzLnRhc2tMaXN0RGF0YVNvdXJjZS5kYXRhID0gW107XHJcbiAgICAgICAgdGhpcy50YXNrTGlzdERhdGFTb3VyY2UuZGF0YSA9IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdDtcclxuICAgICAgICBpZiAodGhpcy5zb3J0ICYmIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRGaWVsZE5hbWUpIHtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LmFjdGl2ZSA9IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRGaWVsZE5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydC5kaXJlY3Rpb24gPSB0aGlzLnRhc2tDb25maWcudGFza0xpc3RTb3J0T3JkZXIudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMudGFza0xpc3REYXRhU291cmNlLnNvcnQgPSB0aGlzLnNvcnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hUYXNrTGlzdERhdGEubmV4dCh0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9wZXJ0eShkaXNwbGF5Q29sdW06IE1QTUZpZWxkLCB0YXNrRGF0YTogYW55KTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihkaXNwbGF5Q29sdW0sIHRhc2tEYXRhKTtcclxuICAgIH1cclxuICAgIGdldFByb3BlcnR5QnlNYXBwZXIodGFza0RhdGEsIG1hcHBlck5hbWUpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlDb2x1bTogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBtYXBwZXJOYW1lLCBNUE1fTEVWRUxTLlRBU0spO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5KGRpc3BsYXlDb2x1bSwgdGFza0RhdGEpO1xyXG4gICAgfVxyXG4gICAgZ2V0UHJpb3JpdHkodGFza0RhdGEsIGRpc3BsYXlDb2x1bSkge1xyXG4gICAgICAgIGNvbnN0IHByaW9yaXR5T2JqID0gdGhpcy51dGlsU2VydmljZS5nZXRQcmlvcml0eUljb24odGhpcy5nZXRQcm9wZXJ0eShkaXNwbGF5Q29sdW0sIHRhc2tEYXRhKSxcclxuICAgICAgICAgICAgTVBNX0xFVkVMUy5UQVNLKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgY29sb3I6IHByaW9yaXR5T2JqLmNvbG9yLFxyXG4gICAgICAgICAgICBpY29uOiBwcmlvcml0eU9iai5pY29uLFxyXG4gICAgICAgICAgICB0b29sdGlwOiBwcmlvcml0eU9iai50b29sdGlwXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwYWdpbmF0aW9uKHBhZ2luYXRpb246IGFueSkge1xyXG4gICAgICAgIHRoaXMucGFnZVNpemUgPSBwYWdpbmF0aW9uLnBhZ2VTaXplO1xyXG4gICAgICAgIHRoaXMudG9wID0gcGFnaW5hdGlvbi5wYWdlU2l6ZTtcclxuICAgICAgICB0aGlzLnNraXAgPSAocGFnaW5hdGlvbi5wYWdlSW5kZXggKiBwYWdpbmF0aW9uLnBhZ2VTaXplKTtcclxuICAgICAgICB0aGlzLnBhZ2UgPSAxICsgcGFnaW5hdGlvbi5wYWdlSW5kZXg7XHJcbiAgICAgICAgdGhpcy5wYWdlTnVtYmVyID0gdGhpcy5wYWdlO1xyXG5cclxuICAgICAgICB0aGlzLmxvYWRpbmdIYW5kbGVyKHRydWUpO1xyXG4gICAgICAgIC8qaWYgKHRoaXMuaXNDYW1wYWlnbkFzc2V0Vmlldykge1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRlU2VydmljZS5nb3RvQ2FtcGFpZ25Bc3NldHModGhpcy51cmxQYXJhbXMuaXNUZW1wbGF0ZSwgdGhpcy5jYW1wYWlnbklkLCB0aGlzLnBhZ2VOdW1iZXIsIHRoaXMucGFnZVNpemUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVTZXJ2aWNlLmdvdG9Qcm9qZWN0QXNzZXRzKHRoaXMudXJsUGFyYW1zLmlzVGVtcGxhdGUsIHRoaXMucHJvamVjdElkLCB0aGlzLnVybFBhcmFtcy5jYW1wYWlnbklkLCB0aGlzLnBhZ2VOdW1iZXIsIHRoaXMucGFnZVNpemUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmdldEFzc2V0cygpOyovXHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZm9ybWF0VG9Mb2NhbGVQaXBlLnRyYW5zZm9ybSh0aGlzLnRhc2tTZXJ2aWNlLmNvbnZlclRvTG9jYWxEYXRlKG9iaiwgcGF0aCkpO1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBAc2luY2UgTVBNVjMtOTQ2XHJcbiAgICAgKiBAcGFyYW0gdGFza1R5cGVcclxuICAgICAqL1xyXG4gICAgZ2V0VGFza1R5cGVJY29uKHRhc2tUeXBlKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGFza1NlcnZpY2UuZ2V0VGFza0ljb25CeVRhc2tUeXBlKHRhc2tUeXBlKTtcclxuICAgIH1cclxuICAgIGdldFN0YXR1c0NoZWNrKHNlbGVjdGVkVGFzayk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCB8fFxyXG4gICAgICAgICAgICB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQgfHxcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9wZXJ0eUJ5TWFwcGVyKHNlbGVjdGVkVGFzaywgdGhpcy5NUE1GaWVsZENvbnN0YW50cy5UQVNLX1NUQVRVU19UWVBFKSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQ7XHJcbiAgICB9XHJcbiAgICBnZXRGaW5hbFN0YXR1c0NoZWNrKHNlbGVjdGVkVGFzayk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCB8fFxyXG4gICAgICAgICAgICB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQgfHxcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9wZXJ0eUJ5TWFwcGVyKHNlbGVjdGVkVGFzaywgdGhpcy5NUE1GaWVsZENvbnN0YW50cy5UQVNLX1NUQVRVU19UWVBFKSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQ7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzQ3VycmVudFdvcmtmbG93QWN0aW9ucyhzZWxlY3RlZFRhc2spOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMgJiYgdGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXNrSXRlbUlkOiBzdHJpbmcgPSB0aGlzLmdldFByb3BlcnR5QnlNYXBwZXIoc2VsZWN0ZWRUYXNrLCB0aGlzLk1QTUZpZWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgICAgIGxldCB0YXNrSWQ7XHJcbiAgICAgICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgICAgIHRhc2tJZCA9IHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50YXNrQ29uZmlnLndvcmtmbG93QWN0aW9uUnVsZXMuZmluZCh3b3JrZmxvd0FjdGlvblJ1bGUgPT4gd29ya2Zsb3dBY3Rpb25SdWxlLlRhc2tJZCA9PT0gdGFza0lkKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRXb3JrZmxvd0FjdGlvbnMoc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzICYmIHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgdGFza0l0ZW1JZDogc3RyaW5nID0gdGhpcy5nZXRQcm9wZXJ0eUJ5TWFwcGVyKHNlbGVjdGVkVGFzaywgdGhpcy5NUE1GaWVsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgICAgICBsZXQgdGFza0lkO1xyXG4gICAgICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgY3VycmVudFdvcmtmbG93QWN0aW9uUnVsZXMgPSB0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcy5maWx0ZXIod29ya2Zsb3dBY3Rpb25SdWxlID0+IHdvcmtmbG93QWN0aW9uUnVsZS5UYXNrSWQgPT09IHRhc2tJZCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRBY3Rpb25zID0gW107XHJcbiAgICAgICAgICAgIGN1cnJlbnRXb3JrZmxvd0FjdGlvblJ1bGVzLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZSAmJiBydWxlLkFjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChydWxlLkFjdGlvbi5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGUuQWN0aW9uLmZvckVhY2goYWN0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQWN0aW9uID0gY3VycmVudEFjdGlvbnMuZmluZChjdXJyZW50QWN0aW9uID0+IGN1cnJlbnRBY3Rpb24uSWQgPT09IGFjdGlvbi5JZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXNlbGVjdGVkQWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudEFjdGlvbnMucHVzaChhY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50QWN0aW9ucy5wdXNoKHJ1bGUuQWN0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gY3VycmVudEFjdGlvbnM7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0cmlnZ2VyQWN0aW9uUnVsZShzZWxlY3RlZFRhc2ssIGFjdGlvbklkKSB7XHJcbiAgICAgICAgY29uc3QgdGFza0l0ZW1JZDogc3RyaW5nID0gdGhpcy5nZXRQcm9wZXJ0eUJ5TWFwcGVyKHNlbGVjdGVkVGFzaywgdGhpcy5NUE1GaWVsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuaW5jbHVkZXMoJy4nKSAmJiB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV0pIHtcclxuICAgICAgICAgICAgY29uc3QgdGFza0lkID0gdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRyaWdnZXJSdWxlT25BY3Rpb24odGFza0lkLCBhY3Rpb25JZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVHJpZ2dlcmluZyBhY3Rpb24gaGFzIGJlZW4gaW5pdGlhdGVkJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMgJiYgIWNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMuZmlyc3RDaGFuZ2UgJiZcclxuICAgICAgICAgICAgKEpTT04uc3RyaW5naWZ5KGNoYW5nZXMuZGlzcGxheWFibGVGaWVsZHMuY3VycmVudFZhbHVlKSAhPT0gSlNPTi5zdHJpbmdpZnkoY2hhbmdlcy5kaXNwbGF5YWJsZUZpZWxkcy5wcmV2aW91c1ZhbHVlKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWZyZXNoRGF0YSh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zdCBhcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMudG9wID0gdGhpcy51dGlsU2VydmljZS5jb252ZXJ0U3RyaW5nVG9OdW1iZXIoYXBwQ29uZmlnW1wiREVGQVVMVF9QQUdJTkFUSU9OXCJdKTtcclxuICAgICAgICB0aGlzLnBhZ2VTaXplID0gdGhpcy51dGlsU2VydmljZS5jb252ZXJ0U3RyaW5nVG9OdW1iZXIoYXBwQ29uZmlnW1wiREVGQVVMVF9QQUdJTkFUSU9OXCJdKTtcclxuICAgICAgICB0aGlzLnBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQpIHtcclxuICAgICAgICAgICAgdGhpcy5hc3NldFNlcnZpY2UuZ2V0UHJvamVjdERldGFpbHModGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkuc3Vic2NyaWJlKHByb2plY3RSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzT25Ib2xkUHJvamVjdCA9IHByb2plY3RSZXNwb25zZS5Qcm9qZWN0LklTX09OX0hPTEQgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzVGVtcGxhdGUgPSB0aGlzLnRhc2tDb25maWcuaXNUZW1wbGF0ZTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50VXNlcklkICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiZcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VySWQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzICYmIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVmcmVzaERhdGEodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMudGFza0NvbmZpZy50YXNrTGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzID0gW107XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc29ydCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc29ydC5kaXNhYmxlQ2xlYXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc29ydC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnNvcnQuYWN0aXZlICYmIHRoaXMuc29ydC5kaXJlY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkU29ydExpc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc29ydFR5cGU6IHRoaXMuc29ydC5kaXJlY3Rpb24udG9VcHBlckNhc2UoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFNvcnRJdGVtOiB0aGlzLnNvcnQuYWN0aXZlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc29ydENoYW5nZS5lbWl0KHNlbGVjdGVkU29ydExpc3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLyogdGhpcy5jdXJyZW50VXNlcklkID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRVc2VySWQgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VySWQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQcm9qZWN0T3duZXIgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzICYmIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hEYXRhKHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMudGFza0NvbmZpZy50YXNrTGlzdCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0xpc3QgPSBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy50YXNrQ29uZmlnLm1ldGFEYXRhRmllbGRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5tZXRhRGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc29ydCkge1xyXG4gICAgICAgICAgICB0aGlzLnNvcnQuZGlzYWJsZUNsZWFyID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNvcnQuYWN0aXZlICYmIHRoaXMuc29ydC5kaXJlY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFNvcnRMaXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0VHlwZTogdGhpcy5zb3J0LmRpcmVjdGlvbi50b1VwcGVyQ2FzZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFNvcnRJdGVtOiB0aGlzLnNvcnQuYWN0aXZlXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zb3J0Q2hhbmdlLmVtaXQoc2VsZWN0ZWRTb3J0TGlzdCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gKi9cclxuICAgIH1cclxufVxyXG4iXX0=