import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TaskService } from '../task.service';
import { TaskConstants } from '../task.constants';
import { SharingService } from '../../../../lib/mpm-utils/services/sharing.service';
import { OtmmMetadataService } from '../../../shared/services/otmm-metadata.service';
import { FormatToLocalePipe } from '../../../shared/pipe/format-to-locale.pipe';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { AssetStatusConstants } from '../../asset-card/asset_status_constants';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { ViewConfigService } from '../../../shared/services/view-config.service';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { SearchConfigConstants } from '../../../mpm-utils/objects/SearchConfigConstants';
import { ProjectConstant } from '../../project-overview/project.constants';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
var TaskCardViewComponent = /** @class */ (function () {
    function TaskCardViewComponent(taskService, sharingService, otmmMetadataService, formatToLocalePipe, utilService, viewConfigService, fieldConfigService, loaderService, notificationService, dialog) {
        this.taskService = taskService;
        this.sharingService = sharingService;
        this.otmmMetadataService = otmmMetadataService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.utilService = utilService;
        this.viewConfigService = viewConfigService;
        this.fieldConfigService = fieldConfigService;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.dialog = dialog;
        this.refreshTask = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.taskConstants = TaskConstants;
        this.assetStatusConstants = AssetStatusConstants;
        this.isProjectOwner = false;
        this.isFinalStatus = false;
        this.isCancelledTask = false;
        this.isFinalTask = false;
        this.copyToolTip = 'Click to copy task name';
        this.view = 'ProjectManagement';
        this.displayableMPMFields = [];
        this.MPMFeildConstants = MPMFieldConstants.MPM_TASK_FIELDS;
        this.priorityObj = {
            color: '',
            icon: '',
            tooltip: ''
        };
        this.isNormalTask = false;
        this.forceDeletion = false;
        this.affectedTask = [];
    }
    TaskCardViewComponent.prototype.openTaskDetails = function (isNewDeliverable) {
        this.taskConfig.isNewDeliverable = isNewDeliverable;
        var taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.taskDetailsHandler.emit(this.taskConfig);
            // this.refresh();
        }
    };
    TaskCardViewComponent.prototype.openCustomWorkflow = function () {
        var taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId) {
            var selectedTask = this.taskConfig.taskList.find(function (task) { return task.ITEM_ID === taskItemId; });
            this.customWorkflowHandler.next(selectedTask);
        }
    };
    TaskCardViewComponent.prototype.editTask = function () {
        var taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        var isApprovalTask = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_IS_APPROVAL);
        this.taskConfig.isApprovalTask = isApprovalTask === 'true' ? true : false;
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            this.taskConfig.taskId = taskItemId.split('.')[1];
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        }
        else if (taskItemId && !(taskItemId.includes('.'))) {
            this.taskConfig.taskId = taskItemId;
            this.editTaskHandler.next(this.taskConfig);
            // this.refresh();
        }
    };
    TaskCardViewComponent.prototype.deleteTask = function (isForceDelete) {
        var _this = this;
        var taskId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ID);
        var msg;
        var isCustomWorkflow = this.utilService.getBooleanValue(this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW);
        if (isCustomWorkflow) {
            msg = 'Are you sure you want to delete the tasks and reconfigure the workflow rule engine?';
        }
        else {
            msg = 'Are you sure you want to delete the tasks and Exit?';
        }
        //  MPMV3-2094
        this.taskService.getAllApprovalTasks(taskId).subscribe(function (approvalTask) {
            if (approvalTask.tuple != undefined) {
                _this.forceDeletion = true;
                _this.affectedTask = [];
                var tasks = void 0;
                if (!Array.isArray(approvalTask.tuple)) {
                    tasks = [approvalTask.tuple];
                }
                else {
                    tasks = approvalTask.tuple;
                }
                tasks.forEach(function (task) {
                    if (task.old.TaskView.isDeleted === 'false') {
                        _this.affectedTask.push(task.old.TaskView.name);
                    }
                });
            }
            var dialogRef = _this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: msg,
                    //name: this.isTemplate ? [] : this.affectedTask,
                    name: _this.affectedTask,
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result) {
                    if (isForceDelete) {
                        _this.taskService.forceDeleteTask(taskId, isCustomWorkflow).subscribe(function (response) {
                            _this.notificationService.success('Task has been deleted successfully');
                            _this.refresh();
                        }, function (error) {
                            _this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                    else {
                        _this.taskService.softDeleteTask(taskId, isCustomWorkflow).subscribe(function (response) {
                            _this.notificationService.success('Task has been deleted successfully');
                            _this.refresh();
                        }, function (error) {
                            _this.notificationService.error('Delete Task operation failed, try again later');
                        });
                    }
                }
            });
        });
    };
    TaskCardViewComponent.prototype.openComment = function () {
    };
    TaskCardViewComponent.prototype.openRequestDetails = function () {
    };
    TaskCardViewComponent.prototype.showReminder = function () {
    };
    TaskCardViewComponent.prototype.refresh = function () {
        this.refreshTask.next(true);
    };
    TaskCardViewComponent.prototype.triggerActionRule = function (actionId) {
        var _this = this;
        var taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
        if (taskItemId && taskItemId.includes('.') && taskItemId.split('.')[1]) {
            var taskId = taskItemId.split('.')[1];
            this.loaderService.show();
            this.taskService.triggerRuleOnAction(taskId, actionId).subscribe(function (response) {
                _this.loaderService.hide();
                _this.notificationService.success('Triggering action has been initiated');
            });
        }
    };
    TaskCardViewComponent.prototype.refreshCurrentTask = function () {
        this.refreshTask.next(this.taskConfig);
    };
    TaskCardViewComponent.prototype.getProperty = function (taskData, mapperName) {
        var displayColum = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName, MPM_LEVELS.TASK);
        return this.fieldConfigService.getFieldValueByDisplayColumn(displayColum, taskData);
    };
    TaskCardViewComponent.prototype.converToLocalDate = function (deliverableData, propertyId) {
        return this.formatToLocalePipe.transform(this.taskService.converToLocalDate(deliverableData, propertyId));
    };
    TaskCardViewComponent.prototype.getPriority = function (taskData, mapperName) {
        var priorityObj = this.utilService.getPriorityIcon(this.getProperty(taskData, mapperName), MPM_LEVELS.TASK);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    };
    /**
     * @since MPMV3-946
     * @param taskType
     */
    TaskCardViewComponent.prototype.getTaskTypeIcon = function (taskType) {
        return this.taskService.getTaskIconByTaskType(taskType);
    };
    /**
     * @since MPMV3-1141
     * @param taskType
     */
    TaskCardViewComponent.prototype.handleActionsOnTaskView = function () {
        var currTaskValue = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_STATUS);
        var currTaskStatusType = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_STATUS_TYPE);
        if (this.assetStatusConstants.REJECTED === currTaskValue || this.assetStatusConstants.APPROVED === currTaskValue ||
            this.assetStatusConstants.CANCELLED === currTaskValue || this.assetStatusConstants.COMPLETED === currTaskValue) {
            this.isCancelledTask = true;
        }
        if (this.projectOwner === this.sharingService.getCurrentUserID() || this.projectOwner === this.sharingService.getCurrentUserItemID()) {
            this.isProjectOwner = true;
        }
        currTaskValue = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_TYPE);
        if (currTaskValue === this.taskConstants.TASK_TYPE.NORMAL_TASK) {
            this.isNormalTask = true;
        }
        if (currTaskStatusType === StatusTypes.FINAL_APPROVED || currTaskStatusType === StatusTypes.FINAL_REJECTED || currTaskStatusType === StatusTypes.FINAL_COMPLETED) {
            this.isFinalTask = true;
        }
    };
    TaskCardViewComponent.prototype.getMetadataTypeValue = function (metadata) {
        if (metadata.value && metadata.value.value) {
            return metadata.value.value.type ? metadata.value.value.type : null;
        }
        return null;
    };
    TaskCardViewComponent.prototype.hasCurrentWorkflowActions = function () {
        return this.currentWorkflowActions && this.currentWorkflowActions.length > 0 ? true : false;
    };
    TaskCardViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        var viewConfig;
        var currentUSerID = this.sharingService.getCurrentUserItemID();
        var customMetadataFields = this.otmmMetadataService.getCustomTaskMetadataFields();
        this.projectCancelledStatus = this.projectStatusType === ProjectConstant.STATUS_TYPE_FINAL_CANCELLED ? true : false;
        if (this.taskData.STATUS && this.taskData.STATUS.TYPE === TaskConstants.StatusConstant.TASK_STATUS.CANCELLED) {
            this.isCancelledTask = true;
        }
        if (currentUSerID && this.taskConfig.selectedProject && this.taskConfig.selectedProject.R_PO_PROJECT_OWNER &&
            this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'] && currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.isProjectOwner = true;
        }
        this.taskStatusType = StatusTypes.FINAL_REJECTED;
        this.handleActionsOnTaskView();
        if (this.taskData && this.taskData.STATUS_STATE === 'FINAL') {
            this.isFinalStatus = true;
        }
        //this.enableDeleteForTask = this.taskData.TASK_STATUS_TYPE === StatusTypes.INITIAL ? true : this.taskData.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE ? true : false;
        if (this.taskConfig.taskViewName) {
            viewConfig = this.taskConfig.taskViewName;
        }
        else {
            viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
        }
        this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetailsParent) {
            var viewDetails = viewDetailsParent ? JSON.parse(JSON.stringify(viewDetailsParent)) : null;
            if (viewDetails && viewDetails.R_PM_CARD_VIEW_MPM_FIELDS) {
                viewDetails.R_PM_CARD_VIEW_MPM_FIELDS.forEach(function (mpmField) {
                    if (_this.taskConstants.DEFAULT_CARD_FIELDS.indexOf(mpmField.MAPPER_NAME) < 0) {
                        mpmField.VALUE = _this.fieldConfigService.getFieldValueByDisplayColumn(mpmField, _this.taskData);
                        _this.displayableMPMFields.push(mpmField);
                    }
                });
            }
        });
        if (this.taskConfig.currentProjectId && this.taskConfig.projectData && this.taskConfig.projectData.length > 0) {
            this.taskConfig.selectedProject = this.taskConfig.projectData.find(function (project) { return project.ID === _this.taskConfig.currentProjectId; });
        }
        if (this.taskConfig.selectedProject && this.taskConfig.selectedProject.IS_CUSTOM_WORKFLOW === 'true' && this.taskConfig.workflowActionRules && this.taskConfig.workflowActionRules.length > 0) {
            var taskItemId = this.getProperty(this.taskData, this.MPMFeildConstants.TASK_ITEM_ID);
            if (taskItemId && taskItemId.split('.')[1]) {
                var taskId_1 = taskItemId.split('.')[1];
                var currentWorkflowActionRules = this.taskConfig.workflowActionRules.filter(function (workflowActionRule) { return workflowActionRule.TaskId === taskId_1; });
                this.currentWorkflowActions = [];
                currentWorkflowActionRules.forEach(function (rule) {
                    if (rule && rule.Action) {
                        if (rule.Action.length > 0) {
                            rule.Action.forEach(function (action) {
                                var selectedAction = _this.currentWorkflowActions.find(function (currentAction) { return currentAction.Id === action.Id; });
                                if (!selectedAction) {
                                    _this.currentWorkflowActions.push(action);
                                }
                            });
                        }
                        else {
                            _this.currentWorkflowActions.push(rule.Action);
                        }
                    }
                });
            }
        }
    };
    TaskCardViewComponent.ctorParameters = function () { return [
        { type: TaskService },
        { type: SharingService },
        { type: OtmmMetadataService },
        { type: FormatToLocalePipe },
        { type: UtilService },
        { type: ViewConfigService },
        { type: FieldConfigService },
        { type: LoaderService },
        { type: NotificationService },
        { type: MatDialog }
    ]; };
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "taskConfig", void 0);
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "taskData", void 0);
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "isTemplate", void 0);
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "projectOwner", void 0);
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "projectStatusType", void 0);
    __decorate([
        Input()
    ], TaskCardViewComponent.prototype, "isOnHoldProject", void 0);
    __decorate([
        Output()
    ], TaskCardViewComponent.prototype, "refreshTask", void 0);
    __decorate([
        Output()
    ], TaskCardViewComponent.prototype, "editTaskHandler", void 0);
    __decorate([
        Output()
    ], TaskCardViewComponent.prototype, "taskDetailsHandler", void 0);
    __decorate([
        Output()
    ], TaskCardViewComponent.prototype, "customWorkflowHandler", void 0);
    TaskCardViewComponent = __decorate([
        Component({
            selector: 'mpm-task-card-view',
            template: "<mat-card class=\"task-card\" *ngIf=\"taskData\">\r\n    <div class=\"flex-col\">\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item copy-icon card-title-wrapper\">\r\n                    <mat-icon class=\"task-card-type-icon \" *ngIf=\"!isTemplate\"\r\n                        [ngClass]=\"[ ((getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)==='true') && (!(projectCancelledStatus) && !(isOnHoldProject))) ? 'active-task-icon' : 'inactive-task-icon']\"\r\n                        matTooltip=\"{{getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)==='true' ? 'Active Task' : 'Inactive Task'}}\">\r\n                        <!-- [style.color]=\"getActiveColor(getProperty(taskData, MPMFeildConstants.IS_TASK_ACTIVE)) === 'true' ? active-task : inactive-task\" -->\r\n                        {{getTaskTypeIcon(getProperty(taskData, MPMFeildConstants.TASK_TYPE))}}</mat-icon>\r\n                    <span class=\"card-title\" (click)=\"openTaskDetails(false)\"\r\n                        matTooltip=\"{{getProperty(taskData, MPMFeildConstants.TASK_NAME) || 'NA'}}\">\r\n                        {{getProperty(taskData, MPMFeildConstants.TASK_NAME) || 'NA'}}\r\n                    </span>\r\n                </div>\r\n                <div class=\"flex-row-item action-button\">\r\n                    <mat-icon color=\"primary\" matTooltip=\"Actions\" matSuffix [matMenuTriggerFor]=\"taskMenu\"\r\n                        *ngIf=\"isProjectOwner\">\r\n                        more_vert\r\n                    </mat-icon>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <span>\r\n            <div class=\"flex-col-item\">\r\n                <div class=\"flex-row\">\r\n                    <div class=\"flex-row-item card-item\">\r\n                        <div class=\"task-percentage\"\r\n                            matTooltip=\"Task Completion : {{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS) || 0}}%\">\r\n                            <span class=\"item-title\">\r\n                                {{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS) || 0}}%</span>\r\n                            <mat-progress-bar color=\"primary\" mode=\"determinate\"\r\n                                value=\"{{getProperty(taskData, MPMFeildConstants.TASK_PROGRESS)}}\">\r\n                            </mat-progress-bar>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </span>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-col-item task-info-wrapper\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row\">\r\n                        <div class=\"flex-row-item card-item task-info\" *ngIf=\"!isTemplate\"\r\n                            matTooltip=\"Due Date: {{getProperty(taskData, MPMFeildConstants.TASK_DUE_DATE)}}\">\r\n                            <mat-icon>event</mat-icon>\r\n                            <span class=\"item-value\">{{getProperty(taskData, MPMFeildConstants.TASK_DUE_DATE)}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <span *ngIf=\"displayableMPMFields && displayableMPMFields.length > 0\">\r\n                    <div class=\"flex-col-item\" *ngFor=\"let column of displayableMPMFields;let i = index;\">\r\n                        <div class=\"flex-row\">\r\n                            <div class=\"flex-row-item card-item task-info\"\r\n                                matTooltip=\"{{column.DISPLAY_NAME}} : {{column.VALUE|| 'NA'}}\">\r\n                                <span class=\"item-title\">{{column.DISPLAY_NAME}}:</span>\r\n                                <span class=\"item-value\">{{column.VALUE || 'NA'}}</span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </span>\r\n            </div>\r\n            <div class=\"flex-row-item priority-info\">\r\n                <mat-icon matTooltip=\"Priority: {{getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).tooltip}}\"\r\n                    [style.color]=\"getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).color\">\r\n                    {{getPriority(taskData, MPMFeildConstants.TASK_PRIORITY).icon}}</mat-icon>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</mat-card>\r\n<mat-menu #taskMenu=\"matMenu\">\r\n    <button mat-menu-item matTooltip=\"Add New Deliverable\" (click)=\"openTaskDetails(true)\"\r\n        [disabled]=\"!(!isFinalTask && isProjectOwner && isNormalTask)\">\r\n        <mat-icon>add</mat-icon>\r\n        <span>Add New Deliverable</span>\r\n    </button>\r\n    <button mat-menu-item matTooltip=\"Edit Task\" (click)=\"editTask()\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>edit</mat-icon>\r\n        <span>Edit Task</span>\r\n    </button>\r\n    <!-- *ngIf=\"isTemplate\" -->\r\n    <button mat-menu-item matTooltip=\"Delete Task\" (click)=\"deleteTask(false)\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>delete</mat-icon>\r\n        <span>Delete Task</span>\r\n    </button>\r\n    <!--*ngIf=\"isTemplate\"-->\r\n    <button mat-menu-item matTooltip=\"Permanent Delete Task\" (click)=\"deleteTask(true)\" [disabled]=\"isFinalTask\">\r\n        <mat-icon>delete</mat-icon>\r\n        <span>Permanent Delete Task</span>\r\n    </button>\r\n    \r\n\r\n</mat-menu>",
            styles: [".circle{height:10px;width:10px;background-color:#e0e0e0;border-radius:10px;margin-top:10px}.circle.active{background-color:#cbc32d}.task-header{display:flex;flex-direction:row}.task-card-image{flex-grow:1;margin-bottom:5px;margin-left:5px}.task-percentage{margin-bottom:10px;text-align:right;width:100%}mat-card{cursor:pointer}.card-title{display:inline-block;text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap;width:150px;margin:5px;font-size:15px}.flex-row-item p{font-size:13px;margin:2px}.task-card{padding:12px}.task-card .action-button{justify-content:flex-end}.task-card:hover{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)!important}.card-heading{font-weight:590}.card-item{margin:4px;font-size:14px;flex-grow:1!important;align-items:center}mat-icon.task-card-type-icon{visibility:visible;margin-top:5px;margin-right:0}.priority-info{justify-content:flex-end}.task-card .task-info-wrapper{width:85%}.task-card .task-info{max-width:180px}.task-card span.item-title{margin-right:4px;font-size:14px;font-weight:600;line-height:16px;white-space:nowrap}.task-card span.item-value{text-overflow:ellipsis;overflow:hidden;display:inline-block;white-space:nowrap}"]
        })
    ], TaskCardViewComponent);
    return TaskCardViewComponent;
}());
export { TaskCardViewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jYXJkLXZpZXcuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLWNhcmQtdmlldy90YXNrLWNhcmQtdmlldy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDcEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQztBQUN4SCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFckQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFZLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRTdFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN6RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUWpGO0lBa0NJLCtCQUNXLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLG1CQUF3QyxFQUN4QyxrQkFBc0MsRUFDdEMsV0FBd0IsRUFDeEIsaUJBQW9DLEVBQ3BDLGtCQUFzQyxFQUN0QyxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsTUFBaUI7UUFUakIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBckNsQixnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUUxRCxrQkFBYSxHQUFHLGFBQWEsQ0FBQztRQUM5Qix5QkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztRQUM1QyxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixnQkFBVyxHQUFHLHlCQUF5QixDQUFDO1FBQ3hDLFNBQUksR0FBRyxtQkFBbUIsQ0FBQztRQUMzQix5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUN0RCxnQkFBVyxHQUFHO1lBQ1YsS0FBSyxFQUFFLEVBQUU7WUFDVCxJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxFQUFFO1NBQ2QsQ0FBQztRQUNGLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSXJCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO0lBYWQsQ0FBQztJQUVMLCtDQUFlLEdBQWYsVUFBZ0IsZ0JBQWdCO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDcEQsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN4RixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsa0JBQWtCO1NBQ3JCO0lBQ0wsQ0FBQztJQUVELGtEQUFrQixHQUFsQjtRQUNJLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEYsSUFBSSxVQUFVLEVBQUU7WUFDWixJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsT0FBTyxLQUFLLFVBQVUsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO1lBQ3hGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsd0NBQVEsR0FBUjtRQUNJLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEYsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2hHLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxHQUFHLGNBQWMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzFFLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNwRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQyxrQkFBa0I7U0FDckI7YUFBTSxJQUFJLFVBQVUsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztZQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDM0Msa0JBQWtCO1NBQ3JCO0lBQ0wsQ0FBQztJQUlELDBDQUFVLEdBQVYsVUFBVyxhQUFzQjtRQUFqQyxpQkE0REM7UUEzREcsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvRSxJQUFJLEdBQUcsQ0FBQztRQUNSLElBQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUM5RyxJQUFJLGdCQUFnQixFQUFFO1lBQ2xCLEdBQUcsR0FBRyxxRkFBcUYsQ0FBQztTQUMvRjthQUFNO1lBQ0gsR0FBRyxHQUFHLHFEQUFxRCxDQUFDO1NBQy9EO1FBRUQsY0FBYztRQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsWUFBWTtZQUMvRCxJQUFJLFlBQVksQ0FBQyxLQUFLLElBQUksU0FBUyxFQUFFO2dCQUNqQyxLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztnQkFDMUIsS0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksS0FBSyxTQUFBLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNwQyxLQUFLLEdBQUcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2hDO3FCQUFNO29CQUNILEtBQUssR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDO2lCQUM5QjtnQkFDRCxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDZCxJQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFNBQVMsS0FBSyxPQUFPLEVBQUU7d0JBQ3hDLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNsRDtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsSUFBTSxTQUFTLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQzNELEtBQUssRUFBRSxLQUFLO2dCQUNaLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxFQUFFLEdBQUc7b0JBQ1osaURBQWlEO29CQUNqRCxJQUFJLEVBQUUsS0FBSSxDQUFDLFlBQVk7b0JBQ3ZCLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDckI7YUFDSixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtnQkFDcEMsSUFBSSxNQUFNLEVBQUU7b0JBQ1IsSUFBRyxhQUFhLEVBQUU7d0JBQ2QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUNoRSxVQUFBLFFBQVE7NEJBQ0osS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDOzRCQUN2RSxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7d0JBQ25CLENBQUMsRUFBRSxVQUFBLEtBQUs7NEJBQ0osS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO3dCQUNwRixDQUFDLENBQUMsQ0FBQztxQkFDVjt5QkFBTTt3QkFDSCxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxTQUFTLENBQy9ELFVBQUEsUUFBUTs0QkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7NEJBQ3ZFLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDbkIsQ0FBQyxFQUFFLFVBQUEsS0FBSzs0QkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUM7d0JBQ3BGLENBQUMsQ0FBQyxDQUFDO3FCQUNWO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwyQ0FBVyxHQUFYO0lBQ0EsQ0FBQztJQUVELGtEQUFrQixHQUFsQjtJQUNBLENBQUM7SUFFRCw0Q0FBWSxHQUFaO0lBQ0EsQ0FBQztJQUVELHVDQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLFFBQVE7UUFBMUIsaUJBVUM7UUFURyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3hGLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNwRSxJQUFNLE1BQU0sR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDckUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDO1lBQzdFLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRUQsa0RBQWtCLEdBQWxCO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCwyQ0FBVyxHQUFYLFVBQVksUUFBYSxFQUFFLFVBQWtCO1FBQ3pDLElBQU0sWUFBWSxHQUFhLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakksT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFFRCxpREFBaUIsR0FBakIsVUFBa0IsZUFBZSxFQUFFLFVBQVU7UUFDekMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsZUFBZSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDOUcsQ0FBQztJQUVELDJDQUFXLEdBQVgsVUFBWSxRQUFhLEVBQUUsVUFBa0I7UUFDekMsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEVBQ3ZGLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVyQixPQUFPO1lBQ0gsS0FBSyxFQUFFLFdBQVcsQ0FBQyxLQUFLO1lBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSTtZQUN0QixPQUFPLEVBQUUsV0FBVyxDQUFDLE9BQU87U0FDL0IsQ0FBQztJQUNOLENBQUM7SUFDRDs7O09BR0c7SUFDSCwrQ0FBZSxHQUFmLFVBQWdCLFFBQVE7UUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFDRDs7O09BR0c7SUFDSCx1REFBdUIsR0FBdkI7UUFDSSxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hGLElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BHLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsS0FBSyxhQUFhLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsS0FBSyxhQUFhO1lBQzVHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEtBQUssYUFBYSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEtBQUssYUFBYSxFQUFFO1lBQ2hILElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsRUFBRTtZQUNsSSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUNELGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksYUFBYSxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRTtZQUM1RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUM1QjtRQUNELElBQUksa0JBQWtCLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxrQkFBa0IsS0FBSyxXQUFXLENBQUMsY0FBYyxJQUFJLGtCQUFrQixLQUFLLFdBQVcsQ0FBQyxlQUFlLEVBQUU7WUFDOUosSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7U0FDM0I7SUFFTCxDQUFDO0lBQ0Qsb0RBQW9CLEdBQXBCLFVBQXFCLFFBQVE7UUFDekIsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ3hDLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUN2RTtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDRCx5REFBeUIsR0FBekI7UUFDSSxPQUFPLElBQUksQ0FBQyxzQkFBc0IsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDaEcsQ0FBQztJQUNELHdDQUFRLEdBQVI7UUFBQSxpQkE4REM7UUE3REcsSUFBSSxVQUFVLENBQUM7UUFDZixJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDakUsSUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztRQUVwRixJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixLQUFLLGVBQWUsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFFcEgsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssYUFBYSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFO1lBQzFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCO1lBQ3RHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxJQUFJLGFBQWEsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDN0osSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDakQsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDL0IsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxLQUFLLE9BQU8sRUFBRTtZQUN6RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUM3QjtRQUNELHdLQUF3SztRQUV4SyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO1lBQzlCLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztTQUM3QzthQUFNO1lBQ0gsVUFBVSxHQUFHLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7U0FDdkQ7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsK0JBQStCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsaUJBQTZCO1lBQ3ZHLElBQU0sV0FBVyxHQUFlLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDekcsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLHlCQUF5QixFQUFFO2dCQUN0RCxXQUFXLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBa0I7b0JBQzdELElBQUksS0FBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDMUUsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDL0YsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztxQkFDNUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0csSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUEvQyxDQUErQyxDQUFDLENBQUM7U0FDbEk7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzTCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3hGLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3hDLElBQU0sUUFBTSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLElBQU0sMEJBQTBCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxrQkFBa0IsSUFBSSxPQUFBLGtCQUFrQixDQUFDLE1BQU0sS0FBSyxRQUFNLEVBQXBDLENBQW9DLENBQUMsQ0FBQztnQkFDMUksSUFBSSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQztnQkFDakMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDbkMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTt3QkFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3hCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTtnQ0FDdEIsSUFBTSxjQUFjLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLGFBQWEsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUFDLEVBQUUsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO2dDQUN6RyxJQUFJLENBQUMsY0FBYyxFQUFFO29DQUNqQixLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lDQUM1Qzs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjs2QkFBTTs0QkFDSCxLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt5QkFDakQ7cUJBQ0o7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNKO0lBQ0wsQ0FBQzs7Z0JBbFF1QixXQUFXO2dCQUNSLGNBQWM7Z0JBQ1QsbUJBQW1CO2dCQUNwQixrQkFBa0I7Z0JBQ3pCLFdBQVc7Z0JBQ0wsaUJBQWlCO2dCQUNoQixrQkFBa0I7Z0JBQ3ZCLGFBQWE7Z0JBQ1AsbUJBQW1CO2dCQUNoQyxTQUFTOztJQTNDbkI7UUFBUixLQUFLLEVBQUU7NkRBQVk7SUFDWDtRQUFSLEtBQUssRUFBRTsyREFBVTtJQUNUO1FBQVIsS0FBSyxFQUFFOzZEQUFZO0lBQ1g7UUFBUixLQUFLLEVBQUU7K0RBQXNCO0lBQ3JCO1FBQVIsS0FBSyxFQUFFO29FQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTtrRUFBaUI7SUFDZjtRQUFULE1BQU0sRUFBRTs4REFBdUM7SUFDdEM7UUFBVCxNQUFNLEVBQUU7a0VBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFO3FFQUE4QztJQUM3QztRQUFULE1BQU0sRUFBRTt3RUFBaUQ7SUFWakQscUJBQXFCO1FBTmpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsZzVLQUE4Qzs7U0FFakQsQ0FBQztPQUVXLHFCQUFxQixDQXVTakM7SUFBRCw0QkFBQztDQUFBLEFBdlNELElBdVNDO1NBdlNZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT3RtbU1ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vdG1tLW1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZVBpcGUgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBc3NldFN0YXR1c0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL2Fzc2V0LWNhcmQvYXNzZXRfc3RhdHVzX2NvbnN0YW50cyc7XHJcblxyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuXHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNRmllbGQsIE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzVHlwZXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tdGFzay1jYXJkLXZpZXcnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Rhc2stY2FyZC12aWV3LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Rhc2stY2FyZC12aWV3LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBUYXNrQ2FyZFZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KCkgdGFza0NvbmZpZztcclxuICAgIEBJbnB1dCgpIHRhc2tEYXRhO1xyXG4gICAgQElucHV0KCkgaXNUZW1wbGF0ZTtcclxuICAgIEBJbnB1dCgpIHByb2plY3RPd25lcjogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgcHJvamVjdFN0YXR1c1R5cGU7XHJcbiAgICBASW5wdXQoKSBpc09uSG9sZFByb2plY3Q7XHJcbiAgICBAT3V0cHV0KCkgcmVmcmVzaFRhc2sgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0VGFza0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB0YXNrRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjdXN0b21Xb3JrZmxvd0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICB0YXNrQ29uc3RhbnRzID0gVGFza0NvbnN0YW50cztcclxuICAgIGFzc2V0U3RhdHVzQ29uc3RhbnRzID0gQXNzZXRTdGF0dXNDb25zdGFudHM7XHJcbiAgICBpc1Byb2plY3RPd25lciA9IGZhbHNlO1xyXG4gICAgaXNGaW5hbFN0YXR1cyA9IGZhbHNlO1xyXG4gICAgaXNDYW5jZWxsZWRUYXNrID0gZmFsc2U7XHJcbiAgICBpc0ZpbmFsVGFzayA9IGZhbHNlO1xyXG4gICAgY29weVRvb2xUaXAgPSAnQ2xpY2sgdG8gY29weSB0YXNrIG5hbWUnO1xyXG4gICAgdmlldyA9ICdQcm9qZWN0TWFuYWdlbWVudCc7XHJcbiAgICBkaXNwbGF5YWJsZU1QTUZpZWxkczogQXJyYXk8TVBNRmllbGQ+ID0gW107XHJcbiAgICBNUE1GZWlsZENvbnN0YW50cyA9IE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9UQVNLX0ZJRUxEUztcclxuICAgIHByaW9yaXR5T2JqID0ge1xyXG4gICAgICAgIGNvbG9yOiAnJyxcclxuICAgICAgICBpY29uOiAnJyxcclxuICAgICAgICB0b29sdGlwOiAnJ1xyXG4gICAgfTtcclxuICAgIGlzTm9ybWFsVGFzayA9IGZhbHNlO1xyXG4gICAgcHJvamVjdENhbmNlbGxlZFN0YXR1cztcclxuICAgIGN1cnJlbnRXb3JrZmxvd0FjdGlvbnM7XHJcbiAgICB0YXNrU3RhdHVzVHlwZTtcclxuICAgIGZvcmNlRGVsZXRpb24gPSBmYWxzZTtcclxuICAgIGFmZmVjdGVkVGFzayA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbU1ldGFkYXRhU2VydmljZTogT3RtbU1ldGFkYXRhU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZm9ybWF0VG9Mb2NhbGVQaXBlOiBGb3JtYXRUb0xvY2FsZVBpcGUsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdmlld0NvbmZpZ1NlcnZpY2U6IFZpZXdDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2dcclxuICAgICkgeyB9XHJcblxyXG4gICAgb3BlblRhc2tEZXRhaWxzKGlzTmV3RGVsaXZlcmFibGUpIHtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNOZXdEZWxpdmVyYWJsZSA9IGlzTmV3RGVsaXZlcmFibGU7XHJcbiAgICAgICAgY29uc3QgdGFza0l0ZW1JZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgIGlmICh0YXNrSXRlbUlkICYmIHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0lkID0gdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tEZXRhaWxzSGFuZGxlci5lbWl0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcGVuQ3VzdG9tV29ya2Zsb3coKSB7XHJcbiAgICAgICAgY29uc3QgdGFza0l0ZW1JZCA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX0lURU1fSUQpO1xyXG4gICAgICAgIGlmICh0YXNrSXRlbUlkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVGFzayA9IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdC5maW5kKHRhc2sgPT4gdGFzay5JVEVNX0lEID09PSB0YXNrSXRlbUlkKTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0hhbmRsZXIubmV4dChzZWxlY3RlZFRhc2spO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0VGFzaygpIHtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgY29uc3QgaXNBcHByb3ZhbFRhc2sgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19JU19BUFBST1ZBTCk7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrID0gaXNBcHByb3ZhbFRhc2sgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICBpZiAodGFza0l0ZW1JZCAmJiB0YXNrSXRlbUlkLmluY2x1ZGVzKCcuJykgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMuZWRpdFRhc2tIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgICAgICAgICAgLy8gdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXNrSXRlbUlkICYmICEodGFza0l0ZW1JZC5pbmNsdWRlcygnLicpKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0lkID0gdGFza0l0ZW1JZDtcclxuICAgICAgICAgICAgdGhpcy5lZGl0VGFza0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICBkZWxldGVUYXNrKGlzRm9yY2VEZWxldGU6IGJvb2xlYW4pIHtcclxuICAgICAgICBjb25zdCB0YXNrSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19JRCk7XHJcbiAgICAgICAgbGV0IG1zZztcclxuICAgICAgICBjb25zdCBpc0N1c3RvbVdvcmtmbG93ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cpO1xyXG4gICAgICAgIGlmIChpc0N1c3RvbVdvcmtmbG93KSB7XHJcbiAgICAgICAgICAgIG1zZyA9ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoZSB0YXNrcyBhbmQgcmVjb25maWd1cmUgdGhlIHdvcmtmbG93IHJ1bGUgZW5naW5lPyc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbXNnID0gJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdGhlIHRhc2tzIGFuZCBFeGl0Pyc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0QWxsQXBwcm92YWxUYXNrcyh0YXNrSWQpLnN1YnNjcmliZShhcHByb3ZhbFRhc2sgPT4ge1xyXG4gICAgICAgICAgICBpZiAoYXBwcm92YWxUYXNrLnR1cGxlICE9IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZURlbGV0aW9uID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWZmZWN0ZWRUYXNrID0gW107XHJcbiAgICAgICAgICAgICAgICBsZXQgdGFza3M7XHJcbiAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoYXBwcm92YWxUYXNrLnR1cGxlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tzID0gW2FwcHJvdmFsVGFzay50dXBsZV07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tzID0gYXBwcm92YWxUYXNrLnR1cGxlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGFza3MuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZih0YXNrLm9sZC5UYXNrVmlldy5pc0RlbGV0ZWQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZmZlY3RlZFRhc2sucHVzaCh0YXNrLm9sZC5UYXNrVmlldy5uYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBtc2csXHJcbiAgICAgICAgICAgICAgICAgICAgLy9uYW1lOiB0aGlzLmlzVGVtcGxhdGUgPyBbXSA6IHRoaXMuYWZmZWN0ZWRUYXNrLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHRoaXMuYWZmZWN0ZWRUYXNrLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZihpc0ZvcmNlRGVsZXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuZm9yY2VEZWxldGVUYXNrKHRhc2tJZCwgaXNDdXN0b21Xb3JrZmxvdykuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdUYXNrIGhhcyBiZWVuIGRlbGV0ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdEZWxldGUgVGFzayBvcGVyYXRpb24gZmFpbGVkLCB0cnkgYWdhaW4gbGF0ZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2Uuc29mdERlbGV0ZVRhc2sodGFza0lkLCBpc0N1c3RvbVdvcmtmbG93KS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1Rhc2sgaGFzIGJlZW4gZGVsZXRlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0RlbGV0ZSBUYXNrIG9wZXJhdGlvbiBmYWlsZWQsIHRyeSBhZ2FpbiBsYXRlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5Db21tZW50KCkge1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5SZXF1ZXN0RGV0YWlscygpIHtcclxuICAgIH1cclxuXHJcbiAgICBzaG93UmVtaW5kZXIoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hUYXNrLm5leHQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdHJpZ2dlckFjdGlvblJ1bGUoYWN0aW9uSWQpIHtcclxuICAgICAgICBjb25zdCB0YXNrSXRlbUlkID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfSVRFTV9JRCk7XHJcbiAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5pbmNsdWRlcygnLicpICYmIHRhc2tJdGVtSWQuc3BsaXQoJy4nKVsxXSkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudHJpZ2dlclJ1bGVPbkFjdGlvbih0YXNrSWQsIGFjdGlvbklkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdUcmlnZ2VyaW5nIGFjdGlvbiBoYXMgYmVlbiBpbml0aWF0ZWQnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hDdXJyZW50VGFzaygpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hUYXNrLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9wZXJ0eSh0YXNrRGF0YTogYW55LCBtYXBwZXJOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGRpc3BsYXlDb2x1bTogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1BUFBFUl9OQU1FLCBtYXBwZXJOYW1lLCBNUE1fTEVWRUxTLlRBU0spO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKGRpc3BsYXlDb2x1bSwgdGFza0RhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlclRvTG9jYWxEYXRlKGRlbGl2ZXJhYmxlRGF0YSwgcHJvcGVydHlJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdFRvTG9jYWxlUGlwZS50cmFuc2Zvcm0odGhpcy50YXNrU2VydmljZS5jb252ZXJUb0xvY2FsRGF0ZShkZWxpdmVyYWJsZURhdGEsIHByb3BlcnR5SWQpKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmlvcml0eSh0YXNrRGF0YTogYW55LCBtYXBwZXJOYW1lOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBwcmlvcml0eU9iaiA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0UHJpb3JpdHlJY29uKHRoaXMuZ2V0UHJvcGVydHkodGFza0RhdGEsIG1hcHBlck5hbWUpLFxyXG4gICAgICAgICAgICBNUE1fTEVWRUxTLlRBU0spO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBjb2xvcjogcHJpb3JpdHlPYmouY29sb3IsXHJcbiAgICAgICAgICAgIGljb246IHByaW9yaXR5T2JqLmljb24sXHJcbiAgICAgICAgICAgIHRvb2x0aXA6IHByaW9yaXR5T2JqLnRvb2x0aXBcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBAc2luY2UgTVBNVjMtOTQ2XHJcbiAgICAgKiBAcGFyYW0gdGFza1R5cGVcclxuICAgICAqL1xyXG4gICAgZ2V0VGFza1R5cGVJY29uKHRhc2tUeXBlKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGFza1NlcnZpY2UuZ2V0VGFza0ljb25CeVRhc2tUeXBlKHRhc2tUeXBlKTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogQHNpbmNlIE1QTVYzLTExNDFcclxuICAgICAqIEBwYXJhbSB0YXNrVHlwZVxyXG4gICAgICovXHJcbiAgICBoYW5kbGVBY3Rpb25zT25UYXNrVmlldygpIHtcclxuICAgICAgICBsZXQgY3VyclRhc2tWYWx1ZSA9IHRoaXMuZ2V0UHJvcGVydHkodGhpcy50YXNrRGF0YSwgdGhpcy5NUE1GZWlsZENvbnN0YW50cy5UQVNLX1NUQVRVUyk7XHJcbiAgICAgICAgY29uc3QgY3VyclRhc2tTdGF0dXNUeXBlID0gdGhpcy5nZXRQcm9wZXJ0eSh0aGlzLnRhc2tEYXRhLCB0aGlzLk1QTUZlaWxkQ29uc3RhbnRzLlRBU0tfU1RBVFVTX1RZUEUpO1xyXG4gICAgICAgIGlmICh0aGlzLmFzc2V0U3RhdHVzQ29uc3RhbnRzLlJFSkVDVEVEID09PSBjdXJyVGFza1ZhbHVlIHx8IHRoaXMuYXNzZXRTdGF0dXNDb25zdGFudHMuQVBQUk9WRUQgPT09IGN1cnJUYXNrVmFsdWUgfHxcclxuICAgICAgICAgICAgdGhpcy5hc3NldFN0YXR1c0NvbnN0YW50cy5DQU5DRUxMRUQgPT09IGN1cnJUYXNrVmFsdWUgfHwgdGhpcy5hc3NldFN0YXR1c0NvbnN0YW50cy5DT01QTEVURUQgPT09IGN1cnJUYXNrVmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0NhbmNlbGxlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5wcm9qZWN0T3duZXIgPT09IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJRCgpIHx8IHRoaXMucHJvamVjdE93bmVyID09PSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGN1cnJUYXNrVmFsdWUgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19UWVBFKTtcclxuICAgICAgICBpZiAoY3VyclRhc2tWYWx1ZSA9PT0gdGhpcy50YXNrQ29uc3RhbnRzLlRBU0tfVFlQRS5OT1JNQUxfVEFTSykge1xyXG4gICAgICAgICAgICB0aGlzLmlzTm9ybWFsVGFzayA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjdXJyVGFza1N0YXR1c1R5cGUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEIHx8IGN1cnJUYXNrU3RhdHVzVHlwZSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQgfHwgY3VyclRhc2tTdGF0dXNUeXBlID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0ZpbmFsVGFzayA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuICAgIGdldE1ldGFkYXRhVHlwZVZhbHVlKG1ldGFkYXRhKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAobWV0YWRhdGEudmFsdWUgJiYgbWV0YWRhdGEudmFsdWUudmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1ldGFkYXRhLnZhbHVlLnZhbHVlLnR5cGUgPyBtZXRhZGF0YS52YWx1ZS52YWx1ZS50eXBlIDogbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbiAgICBoYXNDdXJyZW50V29ya2Zsb3dBY3Rpb25zKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRXb3JrZmxvd0FjdGlvbnMgJiYgdGhpcy5jdXJyZW50V29ya2Zsb3dBY3Rpb25zLmxlbmd0aCA+IDAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBsZXQgdmlld0NvbmZpZztcclxuICAgICAgICBjb25zdCBjdXJyZW50VVNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIGNvbnN0IGN1c3RvbU1ldGFkYXRhRmllbGRzID0gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcygpO1xyXG5cclxuICAgICAgICB0aGlzLnByb2plY3RDYW5jZWxsZWRTdGF0dXMgPSB0aGlzLnByb2plY3RTdGF0dXNUeXBlID09PSBQcm9qZWN0Q29uc3RhbnQuU1RBVFVTX1RZUEVfRklOQUxfQ0FOQ0VMTEVEID8gdHJ1ZSA6IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy50YXNrRGF0YS5TVEFUVVMgJiYgdGhpcy50YXNrRGF0YS5TVEFUVVMuVFlQRSA9PT0gVGFza0NvbnN0YW50cy5TdGF0dXNDb25zdGFudC5UQVNLX1NUQVRVUy5DQU5DRUxMRUQpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0NhbmNlbGxlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY3VycmVudFVTZXJJRCAmJiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSICYmXHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddICYmIGN1cnJlbnRVU2VySUQgPT09IHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19QUk9KRUNUX09XTkVSWydJZGVudGl0eS1pZCddLklkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQcm9qZWN0T3duZXIgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhc2tTdGF0dXNUeXBlID0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQ7XHJcbiAgICAgICAgdGhpcy5oYW5kbGVBY3Rpb25zT25UYXNrVmlldygpO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tEYXRhICYmIHRoaXMudGFza0RhdGEuU1RBVFVTX1NUQVRFID09PSAnRklOQUwnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNGaW5hbFN0YXR1cyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vdGhpcy5lbmFibGVEZWxldGVGb3JUYXNrID0gdGhpcy50YXNrRGF0YS5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMID8gdHJ1ZSA6IHRoaXMudGFza0RhdGEuVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5URVJNRURJQVRFID8gdHJ1ZSA6IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnRhc2tWaWV3TmFtZSkge1xyXG4gICAgICAgICAgICB2aWV3Q29uZmlnID0gdGhpcy50YXNrQ29uZmlnLnRhc2tWaWV3TmFtZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlRBU0s7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3Q29uZmlnKS5zdWJzY3JpYmUoKHZpZXdEZXRhaWxzUGFyZW50OiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZpZXdEZXRhaWxzOiBWaWV3Q29uZmlnID0gdmlld0RldGFpbHNQYXJlbnQgPyBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHZpZXdEZXRhaWxzUGFyZW50KSkgOiBudWxsO1xyXG4gICAgICAgICAgICBpZiAodmlld0RldGFpbHMgJiYgdmlld0RldGFpbHMuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUykge1xyXG4gICAgICAgICAgICAgICAgdmlld0RldGFpbHMuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUy5mb3JFYWNoKChtcG1GaWVsZDogTVBNRmllbGQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uc3RhbnRzLkRFRkFVTFRfQ0FSRF9GSUVMRFMuaW5kZXhPZihtcG1GaWVsZC5NQVBQRVJfTkFNRSkgPCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1wbUZpZWxkLlZBTFVFID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihtcG1GaWVsZCwgdGhpcy50YXNrRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheWFibGVNUE1GaWVsZHMucHVzaChtcG1GaWVsZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmN1cnJlbnRQcm9qZWN0SWQgJiYgdGhpcy50YXNrQ29uZmlnLnByb2plY3REYXRhICYmIHRoaXMudGFza0NvbmZpZy5wcm9qZWN0RGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgPSB0aGlzLnRhc2tDb25maWcucHJvamVjdERhdGEuZmluZChwcm9qZWN0ID0+IHByb2plY3QuSUQgPT09IHRoaXMudGFza0NvbmZpZy5jdXJyZW50UHJvamVjdElkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiYgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5JU19DVVNUT01fV09SS0ZMT1cgPT09ICd0cnVlJyAmJiB0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcyAmJiB0aGlzLnRhc2tDb25maWcud29ya2Zsb3dBY3Rpb25SdWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhc2tJdGVtSWQgPSB0aGlzLmdldFByb3BlcnR5KHRoaXMudGFza0RhdGEsIHRoaXMuTVBNRmVpbGRDb25zdGFudHMuVEFTS19JVEVNX0lEKTtcclxuICAgICAgICAgICAgaWYgKHRhc2tJdGVtSWQgJiYgdGFza0l0ZW1JZC5zcGxpdCgnLicpWzFdKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXNrSWQgPSB0YXNrSXRlbUlkLnNwbGl0KCcuJylbMV07XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50V29ya2Zsb3dBY3Rpb25SdWxlcyA9IHRoaXMudGFza0NvbmZpZy53b3JrZmxvd0FjdGlvblJ1bGVzLmZpbHRlcih3b3JrZmxvd0FjdGlvblJ1bGUgPT4gd29ya2Zsb3dBY3Rpb25SdWxlLlRhc2tJZCA9PT0gdGFza0lkKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFdvcmtmbG93QWN0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgY3VycmVudFdvcmtmbG93QWN0aW9uUnVsZXMuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocnVsZSAmJiBydWxlLkFjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocnVsZS5BY3Rpb24ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZS5BY3Rpb24uZm9yRWFjaChhY3Rpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQWN0aW9uID0gdGhpcy5jdXJyZW50V29ya2Zsb3dBY3Rpb25zLmZpbmQoY3VycmVudEFjdGlvbiA9PiBjdXJyZW50QWN0aW9uLklkID09PSBhY3Rpb24uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghc2VsZWN0ZWRBY3Rpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50V29ya2Zsb3dBY3Rpb25zLnB1c2goYWN0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFdvcmtmbG93QWN0aW9ucy5wdXNoKHJ1bGUuQWN0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=