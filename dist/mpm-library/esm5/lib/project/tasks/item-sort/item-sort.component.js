import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { SortConstants } from '../../../shared/constants/sort.constants';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
var ItemSortComponent = /** @class */ (function () {
    function ItemSortComponent(sharingService) {
        this.sharingService = sharingService;
        this.onSortChange = new EventEmitter();
    }
    ItemSortComponent.prototype.selectSorting = function () {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.onSortChange.next(this.selectedOption);
    };
    ItemSortComponent.prototype.onSelectionChange = function (event, fields) {
        var _this = this;
        if (event && event.currentTarget.value) {
            fields.map(function (option) {
                if (option.name === event.currentTarget.value) {
                    _this.selectedOption.option = option;
                }
            });
        }
        this.onSortChange.next(this.selectedOption);
    };
    ItemSortComponent.prototype.refreshData = function (taskConfig) {
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    };
    ItemSortComponent.prototype.ngOnInit = function () {
        this.appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                //this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    };
    ItemSortComponent.ctorParameters = function () { return [
        { type: SharingService }
    ]; };
    __decorate([
        Input()
    ], ItemSortComponent.prototype, "sortOptions", void 0);
    __decorate([
        Input()
    ], ItemSortComponent.prototype, "selectedOption", void 0);
    __decorate([
        Output()
    ], ItemSortComponent.prototype, "onSortChange", void 0);
    ItemSortComponent = __decorate([
        Component({
            selector: 'mpm-item-sort',
            template: "<div class=\"flex-row-item justify-flex-end\" *ngIf=\"sortOptions && sortOptions.length > 0\">\r\n    <button class=\" sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption.option.displayName}}\"\r\n        [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n        <span>Sort by: {{selectedOption.option.displayName}}</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n    </button>\r\n    <button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n        mat-icon-button>\r\n        <mat-icon *ngIf=\"selectedOption.sortType=='desc'\">arrow_downward</mat-icon>\r\n        <mat-icon *ngIf=\"selectedOption.sortType=='asc'\">arrow_upward</mat-icon>\r\n    </button>\r\n    <mat-menu #sortOrderListMenu=\"matMenu\">\r\n        <span *ngFor=\"let fieldOption of sortOptions\">\r\n            <button *ngIf=\"selectedOption.option.name != fieldOption.name\" mat-menu-item\r\n                matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n                [value]=\"fieldOption.name\">\r\n                <span>{{fieldOption.displayName}}</span>\r\n            </button>\r\n        </span>\r\n    </mat-menu>\r\n</div>",
            styles: [".flex-row-item button{margin:5px}"]
        })
    ], ItemSortComponent);
    return ItemSortComponent;
}());
export { ItemSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1zb3J0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvaXRlbS1zb3J0L2l0ZW0tc29ydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLGNBQWMsRUFBQyxNQUFNLDZDQUE2QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQVNwRztJQU9JLDJCQUNXLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUwvQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFNN0MsQ0FBQztJQUNMLHlDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDckcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw2Q0FBaUIsR0FBakIsVUFBa0IsS0FBSyxFQUFFLE1BQU07UUFBL0IsaUJBU0M7UUFSRyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtZQUNwQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQUEsTUFBTTtnQkFDYixJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztpQkFDdkM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCx1Q0FBVyxHQUFYLFVBQVksVUFBVTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLHlFQUF5RTtnQkFDekUsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMzSztpQkFBTTtnQkFDSCx3REFBd0Q7Z0JBQ3hELElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMxSjtTQUNKO0lBQ0wsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQix5RUFBeUU7Z0JBQ3pFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDM0s7aUJBQU07Z0JBQ0gsd0RBQXdEO2dCQUN4RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUo7U0FDSjtJQUNMLENBQUM7O2dCQTFDMEIsY0FBYzs7SUFQaEM7UUFBUixLQUFLLEVBQUU7MERBQWE7SUFDWjtRQUFSLEtBQUssRUFBRTs2REFBZ0I7SUFDZDtRQUFULE1BQU0sRUFBRTsyREFBd0M7SUFIeEMsaUJBQWlCO1FBTjdCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLHV1Q0FBeUM7O1NBRTVDLENBQUM7T0FFVyxpQkFBaUIsQ0FtRDdCO0lBQUQsd0JBQUM7Q0FBQSxBQW5ERCxJQW1EQztTQW5EWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU29ydENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc29ydC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1pdGVtLXNvcnQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2l0ZW0tc29ydC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9pdGVtLXNvcnQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEl0ZW1Tb3J0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIHNvcnRPcHRpb25zO1xyXG4gICAgQElucHV0KCkgc2VsZWN0ZWRPcHRpb247XHJcbiAgICBAT3V0cHV0KCkgb25Tb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgYXBwQ29uZmlnOiBhbnk7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcbiAgICBzZWxlY3RTb3J0aW5nKCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPSAodGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9PT0gU29ydENvbnN0YW50cy5BU0MpID8gJ2Rlc2MnIDogJ2FzYyc7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5zb3J0VHlwZSA9IHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGU7XHJcbiAgICAgICAgdGhpcy5vblNvcnRDaGFuZ2UubmV4dCh0aGlzLnNlbGVjdGVkT3B0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBvblNlbGVjdGlvbkNoYW5nZShldmVudCwgZmllbGRzKSB7XHJcbiAgICAgICAgaWYgKGV2ZW50ICYmIGV2ZW50LmN1cnJlbnRUYXJnZXQudmFsdWUpIHtcclxuICAgICAgICAgICAgZmllbGRzLm1hcChvcHRpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKG9wdGlvbi5uYW1lID09PSBldmVudC5jdXJyZW50VGFyZ2V0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbi5vcHRpb24gPSBvcHRpb247XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9uU29ydENoYW5nZS5uZXh0KHRoaXMuc2VsZWN0ZWRPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hEYXRhKHRhc2tDb25maWcpIHtcclxuICAgICAgICBpZiAoIXRoaXMuc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc29ydE9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB0aGlzLnNvcnRPcHRpb25zWzBdLCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLnNlbGVjdGVkT3B0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNvcnRPcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICAvL3RoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHRoaXMuc29ydE9wdGlvbnNbMF0sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB7fSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSA9PT0gJ0FTQycgPyAnYXNjJyA6ICdkZXNjJ307XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19