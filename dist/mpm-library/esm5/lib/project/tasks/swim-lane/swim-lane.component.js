import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { TaskService } from '../task.service';
import { MatDialog } from '@angular/material/dialog';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ProjectConstant } from '../../project-overview/project.constants';
import { FieldConfigService } from '../../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../mpm-utils/objects/MPMField';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { EntityAppDefService } from '../../../mpm-utils/services/entity.appdef.service';
import { AssetService } from '../../shared/services/asset.service';
var SwimLaneComponent = /** @class */ (function () {
    function SwimLaneComponent(media, changeDetectorRef, taskService, dialog, fieldConfigService, entityAppDefService, assetService) {
        var _this = this;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.taskService = taskService;
        this.dialog = dialog;
        this.fieldConfigService = fieldConfigService;
        this.entityAppDefService = entityAppDefService;
        this.assetService = assetService;
        this.refreshSwimLine = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.swimListIds = [];
        this.swimList = [];
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = function () { return _this.changeDetectorRef.detectChanges(); };
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    SwimLaneComponent.prototype.editTask = function (eventData) {
        this.editTaskHandler.emit(eventData);
    };
    SwimLaneComponent.prototype.openTaskDetails = function (eventData) {
        this.taskDetailsHandler.emit(eventData);
    };
    SwimLaneComponent.prototype.openCustomWorkflow = function (eventData) {
        this.customWorkflowHandler.emit(eventData);
    };
    SwimLaneComponent.prototype.getConnectedId = function (id) {
        return this.swimListIds.filter(function (ids) { return ids !== id; });
    };
    SwimLaneComponent.prototype.changeSwimLine = function (task, status) {
    };
    SwimLaneComponent.prototype.drop = function (event, list) {
        if (this.taskConfig.isProjectOwner) {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            }
            else {
                if (this.taskConfig.isTemplate === true) {
                    var eventData = { message: 'Status cannot be update for a template\'s task', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                    this.notificationHandler.next(eventData);
                    return;
                }
                transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
                this.changeSwimLine(event.container.data[event.currentIndex], list.listDetail);
            }
        }
    };
    SwimLaneComponent.prototype.dropColumn = function (event) {
        if (this.taskConfig.isProjectOwner) {
            if (this.swimList.length > 1) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
                this.updateSwimLine(event.container.data, event.previousIndex, event.currentIndex);
            }
        }
    };
    SwimLaneComponent.prototype.updateSwimLine = function (containerData, previousIndex, currentIndex) {
        var draggedLane = containerData[currentIndex];
        var objects = [];
        var tempObject = {
            ItemId: Number,
            updateData: {
                Properties: {}
            }
        };
        var draggedIndex = previousIndex;
        var droppedIndex = currentIndex;
        if (droppedIndex > draggedIndex) {
            tempObject.ItemId =
                containerData[currentIndex].listDetail.Identity.ItemId;
            tempObject.updateData = {
                Properties: {
                    SEQUENCE: droppedIndex + 1
                }
            };
            objects.push(tempObject);
            for (var i = droppedIndex; i > draggedIndex; i--) {
                var tempObj = {
                    ItemId: Number,
                    updateData: {
                        Properties: {}
                    }
                };
                tempObj.ItemId = this.swimList[i - 1].listDetail.Identity.ItemId;
                tempObj.updateData = {
                    Properties: {
                        SEQUENCE: i
                    }
                };
                objects.push(tempObj);
            }
        }
        else if (draggedIndex > droppedIndex) {
            tempObject.ItemId =
                draggedLane.listDetail.Identity.ItemId;
            tempObject.updateData = {
                Properties: {
                    SEQUENCE: droppedIndex + 1
                }
            };
            objects.push(tempObject);
            for (var i = droppedIndex; i < draggedIndex; i++) {
                var tempObj = {
                    ItemId: Number,
                    updateData: {
                        Properties: {}
                    }
                };
                tempObj.ItemId = this.swimList[i + 1].listDetail.Identity.ItemId;
                tempObj.updateData = {
                    Properties: {
                        SEQUENCE: i + 2
                    }
                };
                objects.push(tempObj);
            }
        }
        this.swimLineUpdate(objects);
    };
    SwimLaneComponent.prototype.swimLineUpdate = function (objects) {
        var _this = this;
        this.taskService.updateDragandDropLanes(objects)
            .subscribe(function (response) {
            if (response) {
                _this.refresh();
            }
            else {
                _this.refresh();
                var eventData = { message: 'Something went wrong while updating swimlane position', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
            }
        });
    };
    SwimLaneComponent.prototype.refresh = function () {
        this.refreshSwimLine.next(true);
    };
    SwimLaneComponent.prototype.refreshTask = function (event) {
        if (event) {
            this.refresh();
        }
    };
    SwimLaneComponent.prototype.initialiseSwimList = function () {
        var _this = this;
        this.swimList = [];
        this.swimListIds = [];
        if (this.taskConfig) {
            // this.entityAppDefService.getStatusById(this.taskConfig.projectStatusId).subscribe(response => {
            //   console.log(response);
            //  this.projectStatusType = response.STATUS_TYPE;
            // });
            var displayColum_1 = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS_ID, MPM_LEVELS.TASK);
            if (this.taskConfig.taskStatus) {
                this.taskConfig.taskStatus.map(function (status) {
                    var listData = [];
                    if (_this.taskConfig.taskTotalCount > 0) {
                        _this.taskConfig.taskList.map(function (task) {
                            if (_this.fieldConfigService.getFieldValueByDisplayColumn(displayColum_1, task) === status['MPM_Status-id'].Id) {
                                listData.push(task);
                            }
                        });
                    }
                    var swimList = {
                        id: status['MPM_Status-id'].Id,
                        listDetail: status,
                        data: listData,
                        dataCount: listData.length
                    };
                    _this.swimListIds.push(status['MPM_Status-id'].Id);
                    _this.swimList.push(swimList);
                });
            }
            //  });
        }
        if (this.taskConfig && this.taskConfig.taskTotalCount !== 0) {
            this.taskSwimLaneMinHeight = '31rem';
        }
    };
    SwimLaneComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(function (projectResponse) {
                _this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD === 'true' ? true : false;
                _this.entityAppDefService.getStatusById(_this.taskConfig.projectStatusId).subscribe(function (response) {
                    _this.projectStatusType = response.STATUS_TYPE;
                    _this.initialiseSwimList();
                });
                // this.initialiseSwimList();
            });
        }
    };
    SwimLaneComponent.ctorParameters = function () { return [
        { type: MediaMatcher },
        { type: ChangeDetectorRef },
        { type: TaskService },
        { type: MatDialog },
        { type: FieldConfigService },
        { type: EntityAppDefService },
        { type: AssetService }
    ]; };
    __decorate([
        Input()
    ], SwimLaneComponent.prototype, "taskConfig", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "refreshSwimLine", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "loadingHandler", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "notificationHandler", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "editTaskHandler", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "taskDetailsHandler", void 0);
    __decorate([
        Output()
    ], SwimLaneComponent.prototype, "customWorkflowHandler", void 0);
    SwimLaneComponent = __decorate([
        Component({
            selector: 'mpm-swim-lane',
            template: "<div class=\"flex-col swim-layout\">\r\n    <!-- uncomment this once drag and drop position is maintained in backend -->\r\n    <!--<div cdkDropList cdkDropListOrientation=\"horizontal\" class=\"flex-col-item\" [cdkDropListData]=\"swimList\"\r\n        (cdkDropListDropped)=\"dropColumn($event)\">-->\r\n    <div class=\"flex-col-item\">\r\n        <!--remove the above tag once above tag is uncommented-->\r\n        <div class=\"swim-nonDrag-columns\">\r\n            <!-- uncomment this once drag and drop position is maintained in backend -->\r\n            <!--<div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\" cdkDrag>                \r\n                <div cdkDropList class=\"grid-list\" [cdkDropListData]=\"list.data\"\r\n                    (cdkDropListDropped)=\"drop($event, list)\" [cdkDropListConnectedTo]=\"getConnectedId(list.id)\"\r\n                    id=\"{{list.id}}\" [ngStyle]=\"{'min-height': taskSwimLaneMinHeight}\">-->\r\n            <div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\">\r\n                <!--remove the above tag once above tag is uncommented-->\r\n                <div class=\"grid-list\" id=\"{{list.id}}\" [ngStyle]=\"{'min-height': taskSwimLaneMinHeight}\">\r\n                    <!--remove the above tag once above tag is uncommented-->\r\n                    <div class=\"card-wrap\">\r\n                        <div class=\"mpm-card\">\r\n                            <section class=\"card-summary\">{{list.listDetail.NAME}}\r\n                                ({{list.dataCount}})</section>\r\n                        </div>\r\n                    </div>\r\n                    <span>\r\n                        <!-- uncomment this once drag and drop task update is maintained in backend -->\r\n                        <!-- <div class=\"swim-data-card\" cdkDrag [cdkDragData]=\"item\" *ngFor=\"let item of list.data\">\r\n                            <mpm-task-card-view [isTemplate]=\"taskConfig.isTemplate\" [taskData]=\"item\"\r\n                                [taskConfig]=\"taskConfig\" (refreshTask)=\"refreshTask($event)\"\r\n                                (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n                            </mpm-task-card-view>\r\n                        </div> -->\r\n\r\n                        <!--remove the following block once above block is uncommented-->\r\n                        <div class=\"swim-data-card\" *ngFor=\"let item of list.data\">\r\n                            <mpm-task-card-view [isTemplate]=\"taskConfig.isTemplate\" [taskData]=\"item\"\r\n                                [taskConfig]=\"taskConfig\" (refreshTask)=\"refreshTask($event)\" [projectStatusType]=\"projectStatusType\"\r\n                                (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n                            </mpm-task-card-view>\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngIf=\"taskConfig && taskConfig.taskTotalCount==0\" class=\"central-info\">\r\n        <button color=\"primary\" disabled mat-flat-button>No Task is available</button>\r\n    </div>\r\n</div>",
            styles: ["::ng-deep .swim-layout .flex-row-item{flex-grow:.5}.swim-layout{overflow:auto}.swim-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0}.swim-column{box-sizing:border-box;list-style:none;margin:0;position:relative;vertical-align:top;justify-content:center;flex:1 1 0}.swim-data-card{margin-bottom:10px}.swim-nonDrag-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0;display:flex;flex-direction:row}.swim-nonDrag-column{box-sizing:border-box;display:table-cell;list-style:none;margin:0 0 0 12px;position:relative;vertical-align:top;table-layout:fixed;padding-left:5px;padding-right:5px;max-width:15rem;border:1px solid #ddd}::ng-deep .swim-data-card .task-content{margin:5px;cursor:move}::ng-deep app-task-card-view .task-card{min-width:180px}.cdk-drag-preview{box-sizing:border-box;border-radius:4px;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{opacity:0}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.card-box:last-child{border:none}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder){transition:transform 250ms cubic-bezier(0,0,.2,1)}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder) .grid-list{border:1px solid red}.mpm-card{cursor:move;font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.mpm-deliverable{font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.card-summary{display:block;box-sizing:content-box;line-height:1.42857143;max-height:4.28571429em;overflow:hidden}.deliverable-swim{width:240px}.deliverable-data-card{margin:0 0 15px}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}"]
        })
    ], SwimLaneComponent);
    return SwimLaneComponent;
}());
export { SwimLaneComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3dpbS1sYW5lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3Mvc3dpbS1sYW5lL3N3aW0tbGFuZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBZSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUV6RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbkYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUN4RixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFRbkU7SUFtQkksMkJBQ1csS0FBbUIsRUFDbkIsaUJBQW9DLEVBQ3BDLFdBQXdCLEVBQ3hCLE1BQWlCLEVBQ2pCLGtCQUFzQyxFQUN0QyxtQkFBd0MsRUFDeEMsWUFBMEI7UUFQckMsaUJBZUM7UUFkVSxVQUFLLEdBQUwsS0FBSyxDQUFjO1FBQ25CLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFDcEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsaUJBQVksR0FBWixZQUFZLENBQWM7UUF2QjNCLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDekMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5QyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM3QywwQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBSzFELGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFjVixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGNBQU0sT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLEVBQXRDLENBQXNDLENBQUM7UUFDeEUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtZQUMxQix3Q0FBd0M7WUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDMUQ7SUFDTCxDQUFDO0lBRUQsb0NBQVEsR0FBUixVQUFTLFNBQVM7UUFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsMkNBQWUsR0FBZixVQUFnQixTQUFTO1FBQ3JCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELDhDQUFrQixHQUFsQixVQUFtQixTQUFTO1FBQ3hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELDBDQUFjLEdBQWQsVUFBZSxFQUFFO1FBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsS0FBSyxFQUFFLEVBQVYsQ0FBVSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELDBDQUFjLEdBQWQsVUFBZSxJQUFJLEVBQUUsTUFBTTtJQUMzQixDQUFDO0lBRUQsZ0NBQUksR0FBSixVQUFLLEtBQTRCLEVBQUUsSUFBSTtRQUNuQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO1lBQ2hDLElBQUksS0FBSyxDQUFDLGlCQUFpQixLQUFLLEtBQUssQ0FBQyxTQUFTLEVBQUU7Z0JBQzdDLGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNsRjtpQkFBTTtnQkFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxLQUFLLElBQUksRUFBRTtvQkFDckMsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0RBQWdELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDaEksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsT0FBTztpQkFDVjtnQkFDRCxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUMxQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksRUFDcEIsS0FBSyxDQUFDLGFBQWEsRUFDbkIsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDbEY7U0FDSjtJQUNMLENBQUM7SUFFRCxzQ0FBVSxHQUFWLFVBQVcsS0FBNEI7UUFDbkMsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtZQUNoQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDMUIsZUFBZSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvRSxJQUFJLENBQUMsY0FBYyxDQUNmLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUNwQixLQUFLLENBQUMsYUFBYSxFQUNuQixLQUFLLENBQUMsWUFBWSxDQUNyQixDQUFDO2FBQ0w7U0FDSjtJQUNMLENBQUM7SUFFRCwwQ0FBYyxHQUFkLFVBQWUsYUFBYSxFQUFFLGFBQWEsRUFBRSxZQUFZO1FBQ3JELElBQU0sV0FBVyxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNoRCxJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsTUFBTTtZQUNkLFVBQVUsRUFBRTtnQkFDUixVQUFVLEVBQUUsRUFBRTthQUNqQjtTQUNKLENBQUM7UUFDRixJQUFNLFlBQVksR0FBRyxhQUFhLENBQUM7UUFDbkMsSUFBTSxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ2xDLElBQUksWUFBWSxHQUFHLFlBQVksRUFBRTtZQUM3QixVQUFVLENBQUMsTUFBTTtnQkFDYixhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDM0QsVUFBVSxDQUFDLFVBQVUsR0FBRztnQkFDcEIsVUFBVSxFQUFFO29CQUNSLFFBQVEsRUFBRSxZQUFZLEdBQUcsQ0FBQztpQkFDN0I7YUFDSixDQUFDO1lBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLFlBQVksRUFBRSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM5QyxJQUFNLE9BQU8sR0FBRztvQkFDWixNQUFNLEVBQUUsTUFBTTtvQkFDZCxVQUFVLEVBQUU7d0JBQ1IsVUFBVSxFQUFFLEVBQUU7cUJBQ2pCO2lCQUNKLENBQUM7Z0JBQ0YsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDakUsT0FBTyxDQUFDLFVBQVUsR0FBRztvQkFDakIsVUFBVSxFQUFFO3dCQUNSLFFBQVEsRUFBRSxDQUFDO3FCQUNkO2lCQUNKLENBQUM7Z0JBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUN6QjtTQUNKO2FBQU0sSUFBSSxZQUFZLEdBQUcsWUFBWSxFQUFFO1lBQ3BDLFVBQVUsQ0FBQyxNQUFNO2dCQUNiLFdBQVcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUMzQyxVQUFVLENBQUMsVUFBVSxHQUFHO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1IsUUFBUSxFQUFFLFlBQVksR0FBRyxDQUFDO2lCQUM3QjthQUNKLENBQUM7WUFDRixPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsR0FBRyxZQUFZLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzlDLElBQU0sT0FBTyxHQUFHO29CQUNaLE1BQU0sRUFBRSxNQUFNO29CQUNkLFVBQVUsRUFBRTt3QkFDUixVQUFVLEVBQUUsRUFBRTtxQkFDakI7aUJBQ0osQ0FBQztnQkFDRixPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO2dCQUNqRSxPQUFPLENBQUMsVUFBVSxHQUFHO29CQUNqQixVQUFVLEVBQUU7d0JBQ1IsUUFBUSxFQUFFLENBQUMsR0FBRyxDQUFDO3FCQUNsQjtpQkFDSixDQUFDO2dCQUNGLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDekI7U0FDSjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELDBDQUFjLEdBQWQsVUFBZSxPQUFPO1FBQXRCLGlCQVdDO1FBVkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUM7YUFDM0MsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNmLElBQUksUUFBUSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNsQjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2YsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsdURBQXVELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDeEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM1QztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELG1DQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsdUNBQVcsR0FBWCxVQUFZLEtBQUs7UUFDYixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7SUFFRCw4Q0FBa0IsR0FBbEI7UUFBQSxpQkFtQ0M7UUFsQ0csSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLGtHQUFrRztZQUNsRywyQkFBMkI7WUFDM0Isa0RBQWtEO1lBQ2xELE1BQU07WUFDTixJQUFNLGNBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3SixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFO2dCQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO29CQUNqQyxJQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7b0JBQ3BCLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxFQUFFO3dCQUNwQyxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJOzRCQUM3QixJQUFJLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxjQUFZLEVBQUUsSUFBSSxDQUFDLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQ0FDekcsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs2QkFDdkI7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsSUFBTSxRQUFRLEdBQUc7d0JBQ2IsRUFBRSxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3dCQUM5QixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsSUFBSSxFQUFFLFFBQVE7d0JBQ2QsU0FBUyxFQUFFLFFBQVEsQ0FBQyxNQUFNO3FCQUM3QixDQUFDO29CQUNGLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFDRCxPQUFPO1NBRVY7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEtBQUssQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxPQUFPLENBQUM7U0FDeEM7SUFDTCxDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUFBLGlCQVdDO1FBVkcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsZUFBZTtnQkFDcEYsS0FBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNwRixLQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDdEYsS0FBSSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7b0JBQzlDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQztnQkFDSCw2QkFBNkI7WUFDakMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7O2dCQXpNaUIsWUFBWTtnQkFDQSxpQkFBaUI7Z0JBQ3ZCLFdBQVc7Z0JBQ2hCLFNBQVM7Z0JBQ0csa0JBQWtCO2dCQUNqQixtQkFBbUI7Z0JBQzFCLFlBQVk7O0lBeEI1QjtRQUFSLEtBQUssRUFBRTt5REFBbUI7SUFDakI7UUFBVCxNQUFNLEVBQUU7OERBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFOzZEQUEwQztJQUN6QztRQUFULE1BQU0sRUFBRTtrRUFBK0M7SUFDOUM7UUFBVCxNQUFNLEVBQUU7OERBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFO2lFQUE4QztJQUM3QztRQUFULE1BQU0sRUFBRTtvRUFBaUQ7SUFSakQsaUJBQWlCO1FBTjdCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLDJ5R0FBeUM7O1NBRTVDLENBQUM7T0FFVyxpQkFBaUIsQ0ErTjdCO0lBQUQsd0JBQUM7Q0FBQSxBQS9ORCxJQStOQztTQS9OWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZWRpYU1hdGNoZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0JztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBDZGtEcmFnRHJvcCwgbW92ZUl0ZW1JbkFycmF5LCB0cmFuc2ZlckFycmF5SXRlbSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG5pbXBvcnQgeyBUYXNrQ29uc3RhbnRzIH0gZnJvbSAnLi4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNRmllbGRDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL21wbS5maWVsZC5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZEtleXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NUE1GaWVsZCc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXNzZXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2Fzc2V0LnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1zd2ltLWxhbmUnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3N3aW0tbGFuZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9zd2ltLWxhbmUuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFN3aW1MYW5lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBwdWJsaWMgdGFza0NvbmZpZztcclxuICAgIEBPdXRwdXQoKSByZWZyZXNoU3dpbUxpbmUgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBsb2FkaW5nSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIG5vdGlmaWNhdGlvbkhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0VGFza0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB0YXNrRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjdXN0b21Xb3JrZmxvd0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBtb2JpbGVRdWVyeTogTWVkaWFRdWVyeUxpc3Q7XHJcbiAgICBwdWJsaWMgbW9iaWxlUXVlcnlMaXN0ZW5lcjogKCkgPT4gdm9pZDtcclxuXHJcbiAgICBzd2ltTGlzdElkcyA9IFtdO1xyXG4gICAgc3dpbUxpc3QgPSBbXTtcclxuICAgIHRhc2tTd2ltTGFuZU1pbkhlaWdodDtcclxuICAgIHByb2plY3RTdGF0dXNUeXBlO1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBtZWRpYTogTWVkaWFNYXRjaGVyLFxyXG4gICAgICAgIHB1YmxpYyBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZVxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5tb2JpbGVRdWVyeSA9IHRoaXMubWVkaWEubWF0Y2hNZWRpYSgnKG1heC13aWR0aDogNjAwcHgpJyk7XHJcbiAgICAgICAgdGhpcy5tb2JpbGVRdWVyeUxpc3RlbmVyID0gKCkgPT4gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgaWYgKHRoaXMubW9iaWxlUXVlcnkubWF0Y2hlcykge1xyXG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IGRlcHJlY2F0aW9uXHJcbiAgICAgICAgICAgIHRoaXMubW9iaWxlUXVlcnkuYWRkTGlzdGVuZXIodGhpcy5tb2JpbGVRdWVyeUxpc3RlbmVyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdFRhc2soZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0VGFza0hhbmRsZXIuZW1pdChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5UYXNrRGV0YWlscyhldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLnRhc2tEZXRhaWxzSGFuZGxlci5lbWl0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkN1c3RvbVdvcmtmbG93KGV2ZW50RGF0YSkge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dIYW5kbGVyLmVtaXQoZXZlbnREYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb25uZWN0ZWRJZChpZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN3aW1MaXN0SWRzLmZpbHRlcihpZHMgPT4gaWRzICE9PSBpZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlU3dpbUxpbmUodGFzaywgc3RhdHVzKSB7XHJcbiAgICB9XHJcblxyXG4gICAgZHJvcChldmVudDogQ2RrRHJhZ0Ryb3A8c3RyaW5nW10+LCBsaXN0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc1Byb2plY3RPd25lcikge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQucHJldmlvdXNDb250YWluZXIgPT09IGV2ZW50LmNvbnRhaW5lcikge1xyXG4gICAgICAgICAgICAgICAgbW92ZUl0ZW1JbkFycmF5KGV2ZW50LmNvbnRhaW5lci5kYXRhLCBldmVudC5wcmV2aW91c0luZGV4LCBldmVudC5jdXJyZW50SW5kZXgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc1RlbXBsYXRlID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU3RhdHVzIGNhbm5vdCBiZSB1cGRhdGUgZm9yIGEgdGVtcGxhdGVcXCdzIHRhc2snLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0cmFuc2ZlckFycmF5SXRlbShldmVudC5wcmV2aW91c0NvbnRhaW5lci5kYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LmNvbnRhaW5lci5kYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnByZXZpb3VzSW5kZXgsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuY3VycmVudEluZGV4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hhbmdlU3dpbUxpbmUoZXZlbnQuY29udGFpbmVyLmRhdGFbZXZlbnQuY3VycmVudEluZGV4XSwgbGlzdC5saXN0RGV0YWlsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkcm9wQ29sdW1uKGV2ZW50OiBDZGtEcmFnRHJvcDxzdHJpbmdbXT4pIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzUHJvamVjdE93bmVyKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnN3aW1MaXN0Lmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgICAgIG1vdmVJdGVtSW5BcnJheShldmVudC5jb250YWluZXIuZGF0YSwgZXZlbnQucHJldmlvdXNJbmRleCwgZXZlbnQuY3VycmVudEluZGV4KTtcclxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlU3dpbUxpbmUoXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuY29udGFpbmVyLmRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmlvdXNJbmRleCxcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5jdXJyZW50SW5kZXhcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlU3dpbUxpbmUoY29udGFpbmVyRGF0YSwgcHJldmlvdXNJbmRleCwgY3VycmVudEluZGV4KSB7XHJcbiAgICAgICAgY29uc3QgZHJhZ2dlZExhbmUgPSBjb250YWluZXJEYXRhW2N1cnJlbnRJbmRleF07XHJcbiAgICAgICAgY29uc3Qgb2JqZWN0cyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHRlbXBPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIEl0ZW1JZDogTnVtYmVyLFxyXG4gICAgICAgICAgICB1cGRhdGVEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7fVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBkcmFnZ2VkSW5kZXggPSBwcmV2aW91c0luZGV4O1xyXG4gICAgICAgIGNvbnN0IGRyb3BwZWRJbmRleCA9IGN1cnJlbnRJbmRleDtcclxuICAgICAgICBpZiAoZHJvcHBlZEluZGV4ID4gZHJhZ2dlZEluZGV4KSB7XHJcbiAgICAgICAgICAgIHRlbXBPYmplY3QuSXRlbUlkID1cclxuICAgICAgICAgICAgICAgIGNvbnRhaW5lckRhdGFbY3VycmVudEluZGV4XS5saXN0RGV0YWlsLklkZW50aXR5Lkl0ZW1JZDtcclxuICAgICAgICAgICAgdGVtcE9iamVjdC51cGRhdGVEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgUHJvcGVydGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgIFNFUVVFTkNFOiBkcm9wcGVkSW5kZXggKyAxXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIG9iamVjdHMucHVzaCh0ZW1wT2JqZWN0KTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IGRyb3BwZWRJbmRleDsgaSA+IGRyYWdnZWRJbmRleDsgaS0tKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0ZW1wT2JqID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIEl0ZW1JZDogTnVtYmVyLFxyXG4gICAgICAgICAgICAgICAgICAgIHVwZGF0ZURhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvcGVydGllczoge31cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGVtcE9iai5JdGVtSWQgPSB0aGlzLnN3aW1MaXN0W2kgLSAxXS5saXN0RGV0YWlsLklkZW50aXR5Lkl0ZW1JZDtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmoudXBkYXRlRGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFNFUVVFTkNFOiBpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIG9iamVjdHMucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoZHJhZ2dlZEluZGV4ID4gZHJvcHBlZEluZGV4KSB7XHJcbiAgICAgICAgICAgIHRlbXBPYmplY3QuSXRlbUlkID1cclxuICAgICAgICAgICAgICAgIGRyYWdnZWRMYW5lLmxpc3REZXRhaWwuSWRlbnRpdHkuSXRlbUlkO1xyXG4gICAgICAgICAgICB0ZW1wT2JqZWN0LnVwZGF0ZURhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgU0VRVUVOQ0U6IGRyb3BwZWRJbmRleCArIDFcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgb2JqZWN0cy5wdXNoKHRlbXBPYmplY3QpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gZHJvcHBlZEluZGV4OyBpIDwgZHJhZ2dlZEluZGV4OyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBPYmogPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgSXRlbUlkOiBOdW1iZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgdXBkYXRlRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBQcm9wZXJ0aWVzOiB7fVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqLkl0ZW1JZCA9IHRoaXMuc3dpbUxpc3RbaSArIDFdLmxpc3REZXRhaWwuSWRlbnRpdHkuSXRlbUlkO1xyXG4gICAgICAgICAgICAgICAgdGVtcE9iai51cGRhdGVEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIFByb3BlcnRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgU0VRVUVOQ0U6IGkgKyAyXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIG9iamVjdHMucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnN3aW1MaW5lVXBkYXRlKG9iamVjdHMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN3aW1MaW5lVXBkYXRlKG9iamVjdHMpIHtcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnVwZGF0ZURyYWdhbmREcm9wTGFuZXMob2JqZWN0cylcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgdXBkYXRpbmcgc3dpbWxhbmUgcG9zaXRpb24nLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2goKSB7XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoU3dpbUxpbmUubmV4dCh0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoVGFzayhldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGlzZVN3aW1MaXN0KCkge1xyXG4gICAgICAgIHRoaXMuc3dpbUxpc3QgPSBbXTtcclxuICAgICAgICB0aGlzLnN3aW1MaXN0SWRzID0gW107XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZykge1xyXG4gICAgICAgICAgICAvLyB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2UuZ2V0U3RhdHVzQnlJZCh0aGlzLnRhc2tDb25maWcucHJvamVjdFN0YXR1c0lkKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAvLyAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgLy8gIHRoaXMucHJvamVjdFN0YXR1c1R5cGUgPSByZXNwb25zZS5TVEFUVVNfVFlQRTtcclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpc3BsYXlDb2x1bSA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9UQVNLX0ZJRUxEUy5UQVNLX1NUQVRVU19JRCwgTVBNX0xFVkVMUy5UQVNLKTtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy50YXNrU3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza1N0YXR1cy5tYXAoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBsaXN0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcudGFza1RvdGFsQ291bnQgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdC5tYXAodGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihkaXNwbGF5Q29sdW0sIHRhc2spID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3REYXRhLnB1c2godGFzayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzd2ltTGlzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsaXN0RGV0YWlsOiBzdGF0dXMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IGxpc3REYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhQ291bnQ6IGxpc3REYXRhLmxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zd2ltTGlzdElkcy5wdXNoKHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN3aW1MaXN0LnB1c2goc3dpbUxpc3QpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gIH0pO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZyAmJiB0aGlzLnRhc2tDb25maWcudGFza1RvdGFsQ291bnQgIT09IDApIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrU3dpbUxhbmVNaW5IZWlnaHQgPSAnMzFyZW0nO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0U2VydmljZS5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLnRhc2tDb25maWcucHJvamVjdElkKS5zdWJzY3JpYmUocHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNPbkhvbGRQcm9qZWN0ID0gcHJvamVjdFJlc3BvbnNlLlByb2plY3QuSVNfT05fSE9MRCA9PT0gJ3RydWUnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldFN0YXR1c0J5SWQodGhpcy50YXNrQ29uZmlnLnByb2plY3RTdGF0dXNJZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2plY3RTdGF0dXNUeXBlID0gcmVzcG9uc2UuU1RBVFVTX1RZUEU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0aWFsaXNlU3dpbUxpc3QoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5pbml0aWFsaXNlU3dpbUxpc3QoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=