import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OtmmMetadataService } from '../../shared/services/otmm-metadata.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { TaskConstants } from './task.constants';
import { TaskTypeIconForTask } from '../shared/constants/task-type-icon.constants';
import { IndexerService } from '../../shared/services/indexer/indexer.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { SearchConfigConstants } from '../../mpm-utils/objects/SearchConfigConstants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../shared/services/otmm-metadata.service";
import * as i4 from "../../shared/services/indexer/indexer.service";
import * as i5 from "../../shared/services/field-config.service";
import * as i6 from "../../shared/services/view-config.service";
import * as i7 from "../../mpm-utils/services/sharing.service";
var TaskService = /** @class */ (function () {
    function TaskService(appService, otmmService, otmmMetadataService, indexerService, fieldConfigService, viewConfigService, sharingService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.otmmMetadataService = otmmMetadataService;
        this.indexerService = indexerService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.sharingService = sharingService;
        this.STATUS_METHOD_NS = 'http://schemas/AcheronMPMSupportingEntity/Status/operations';
        this.TASK_TYPE_METHOD_NS = 'http://schemas/AcheronMPMAppConfig/MPM_Task_Type/operations';
        this.TASK_BPM_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.TASK_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.WORKFLOW_RULE_NS = 'http://schemas/AcheronMPMCore/Workflow_Rules/operations';
        this.TASK_WSAPP_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.DELETE_WORKFLOW_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_MPM_USER_PREFERENCE_BY_MENU_NS = "http://schemas/AcheronMPMCore/MPM_User_Preference/operations";
        this.GET_MPM_USER_PREFERENCE_BY_MENU = "GetDefaultUserPreferenceByMenu";
        this.DELETE_DELIVERABLE_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_ALL_APPROVAL_TASKS_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.DELETE_TASK_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.ASSIGN_DELIVERABLE_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.DELETE_WORKFLOW = 'DeleteWorkflow';
        this.FORCE_DELETE_TASK = 'ForceDeleteTask';
        this.SOFT_DELETE_TASK = 'SoftDeleteTask';
        this.GET_STATUS_FOR_CATEGORY_WS_METHOD_NAME = 'GetStatusForCategoryLevelName';
        this.GET_TASK_TYPE_WS_METHOD_NAME = 'ReadTask_Type';
        this.GET_STATUS_WS_METHOD_NAME = 'ReadStatus';
        this.GET_DEPENDENT_TASK = 'GetDependentTask';
        this.GET_TASK_BY_ID = 'ReadTask';
        this.UPDATE_TASK = 'UpdateTask';
        this.CREATE_TASK = 'CreateTask';
        this.EDIT_TASK = 'EditTask';
        this.CREATE_WORKFLOW_RULE_WS_METHOD_NAME = 'CreateWorkflowRules';
        this.GET_WORKFLOW_RULES_BY_PROJECT_WS_METHOD_NAME = 'GetWorkflowRulesByProject';
        this.GET_WORKFLOW_RULES_BY_TASK_WS_METHOD_NAME = 'GetWorkflowRulesByTask';
        this.DELETE_WORKFLOW_RULES_BY_ID_WS_METHOD_NAME = 'DeleteWorkflow_Rules';
        this.TRIGGER_TASK_BY_RULE_WS_METHOD_NAME = 'TriggerTaskByRule';
        this.DELETE_WORKFLOW_RULES_WS_METHOD_NAME = 'DeleteWorkflowRules';
        this.UPDATE_DATE_ON_RULE_INITIATE_WS_METHOD_NAME = 'UpdateDateOnRuleInitiate';
        this.GET_ACTION_RULES_BY_PROJECT_ID_WS_METHOD_NAME = 'GetActionRulesByProjectId';
        this.TRIGGER_RULE_ON_ACTION_WS_METHOD_NAME = 'TriggerRuleOnAction';
        this.DELETE_DELIVERABLE = 'DeleteDeliverable';
        this.GET_ALL_APPROVAL_TASKS = 'GetAllApprovalTasks';
        this.DELETE_TASK = 'DeleteTask';
        this.ASSIGN_DELIVERABLE = 'AssignDeliverable';
        this.REMOVE_WORKFLOW_RULES = 'RemoveWorkflowRules';
        this.defaultTaskMetadataFields = [];
        this.customTaskMetadataFields = [];
        this.refreshDataSubject = new BehaviorSubject(100);
        this.refreshData = this.refreshDataSubject.asObservable();
        this.deliverableRefreshDataSubject = new BehaviorSubject(100);
        this.deliverableRefreshData = this.deliverableRefreshDataSubject.asObservable();
    }
    TaskService.prototype.setRefreshData = function (data) {
        if (data !== 100) {
            this.refreshDataSubject.next(data);
        }
    };
    TaskService.prototype.setDeliverableRefreshData = function (data) {
        if (data !== 100) {
            this.deliverableRefreshDataSubject.next(data);
        }
    };
    TaskService.prototype.setDefaultTaskMetadataFields = function (metadataFields) {
        this.defaultTaskMetadataFields = metadataFields;
    };
    TaskService.prototype.getDefaultTaskMetadataFields = function () {
        return this.defaultTaskMetadataFields;
    };
    TaskService.prototype.setCustomTaskMetadataFields = function (metadataFields) {
        this.customTaskMetadataFields = metadataFields;
    };
    TaskService.prototype.getCustomTaskMetadataFields = function () {
        return this.customTaskMetadataFields;
    };
    TaskService.prototype.getStatus = function (categoryLevelName) {
        var _this = this;
        var getRequestObject = {
            CategoryLevelName: categoryLevelName
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_STATUS_FOR_CATEGORY_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (!Array.isArray(response.Status)) {
                    response.Status = [response.Status];
                }
                observer.next(response.Status);
                observer.complete();
            }, function (error) {
                observer.next(error);
                observer.complete();
            });
        });
    };
    TaskService.prototype.getTaskType = function (taskTypeId) {
        var _this = this;
        return new Observable(function (observer) {
            var params = {
                'Task_Type-id': {
                    Id: taskTypeId
                }
            };
            _this.appService.invokeRequest(_this.TASK_TYPE_METHOD_NS, _this.GET_TASK_TYPE_WS_METHOD_NAME, params)
                .subscribe(function (taskTypeDetailResponse) {
                if (taskTypeDetailResponse && taskTypeDetailResponse.Task_Type) {
                    observer.next(taskTypeDetailResponse.Task_Type);
                    observer.complete();
                }
            }, function (taskTypeDetailError) {
                observer.error(taskTypeDetailError);
            });
        });
    };
    TaskService.prototype.getStatusById = function (statusId) {
        var _this = this;
        return new Observable(function (observer) {
            var params = {
                'Status-id': {
                    Id: statusId
                }
            };
            _this.appService.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_STATUS_WS_METHOD_NAME, params)
                .subscribe(function (statusResponse) {
                if (statusResponse && statusResponse.Status) {
                    observer.next(statusResponse.Status);
                    observer.complete();
                }
            }, function (statusError) {
                observer.error(statusError);
            });
        });
    };
    TaskService.prototype.updateDragandDropLanes = function (swimlane) {
        return new Observable(function (observer) {
            observer.next(true);
            observer.complete();
        });
    };
    TaskService.prototype.getProperty = function (obj, path) {
        if (!obj || !path || !obj.metadata) {
            return;
        }
        return this.otmmMetadataService.getFieldValueById(obj.metadata, path, true);
    };
    TaskService.prototype.converToLocalDate = function (obj, path) {
        var dateObj = this.otmmMetadataService.getFieldValueById(obj.metadata, path);
        if (dateObj != null) {
            return dateObj;
        }
        return '';
    };
    TaskService.prototype.getAllActiveTasks = function (parameters, projectViewName) {
        var _this = this;
        return new Observable(function (observer) {
            if (!parameters.projectId) {
                observer.error('Project id is missing.');
            }
            var viewConfig;
            var searchConditionList = _this.fieldConfigService.formIndexerIdFromMapperId(TaskConstants.GET_ALL_TASKS_SEARCH_CONDITION_LIST.search_condition_list.search_condition);
            searchConditionList = searchConditionList.concat(_this.fieldConfigService.formIndexerIdFromMapperId([TaskConstants.ACTIVE_TASK_SEARCH_CONDITION]));
            var projectSearch = _this.fieldConfigService.createConditionObject(MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROJECT_ID, MPMSearchOperators.IS, MPMSearchOperatorNames.IS, MPMSearchOperators.AND, parameters.projectId);
            if (projectSearch) {
                searchConditionList.push(projectSearch);
            }
            if (projectViewName) {
                viewConfig = projectViewName; //this.sharingService.getViewConfigById(projectViewId).VIEW;
            }
            else {
                viewConfig = SearchConfigConstants.SEARCH_NAME.TASK;
            }
            _this.viewConfigService.getAllDisplayFeildForViewConfig(viewConfig).subscribe(function (viewDetails) {
                var searchConfig = viewDetails.R_PO_ADVANCED_SEARCH_CONFIG && typeof viewDetails.R_PO_ADVANCED_SEARCH_CONFIG === 'string'
                    ? parseInt(viewDetails.R_PO_ADVANCED_SEARCH_CONFIG, 10) : null;
                var parametersReq = {
                    search_config_id: searchConfig ? searchConfig : null,
                    keyword: '',
                    search_condition_list: {
                        search_condition: searchConditionList
                    },
                    facet_condition_list: {
                        facet_condition: []
                    },
                    sorting_list: {
                        sort: [{
                                field_id: _this.fieldConfigService.getIndexerIdByMapperValue(MPMFieldConstants.MPM_TASK_FIELDS.TASK_NAME),
                                order: 'ASC'
                            }]
                    },
                    cursor: {
                        page_index: parameters.skip || 0,
                        page_size: parameters.top || 100
                    }
                };
                _this.indexerService.search(parametersReq).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error('Something went wrong while getting projects.');
                });
            });
        });
    };
    TaskService.prototype.getTaskDataValues = function (taskId) {
        var _this = this;
        var parameter = {
            TaskID: taskId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TASK_BPM_NS, 'EditTaskDateValidation', parameter)
                .subscribe(function (response) {
                observer.next({
                    startMaxDate: response && response.Date.StartDate && typeof response.Date.StartDate === 'string' ? new Date(response.Date.StartDate) : null,
                    endMinDate: response && response.Date.EndDate && typeof response.Date.EndDate === 'string' ? new Date(response.Date.EndDate) : null,
                });
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.prototype.getDependentTask = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.GET_DEPENDENT_TASK, parameters)
                .subscribe(function (dependentTaskResponse) {
                if (dependentTaskResponse && dependentTaskResponse.Task) {
                    observer.next(dependentTaskResponse.Task);
                    observer.complete();
                }
                else {
                    observer.next([]);
                    observer.complete();
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.prototype.getTaskById = function (taskId) {
        var _this = this;
        var parameter = {
            'Task-id': {
                Id: taskId
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TASK_NS, _this.GET_TASK_BY_ID, parameter)
                .subscribe(function (response) {
                observer.next(response.Task);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.prototype.createTask = function (parameters) {
        return this.appService.invokeRequest(this.TASK_BPM_NS, this.CREATE_TASK, parameters);
    };
    TaskService.prototype.updateTask = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.EDIT_TASK, parameters)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.prototype.getTaskIconByTaskType = function (taskType) {
        var taskIcon = 'fiber_manual_record';
        TaskConstants.TASK_TYPE_LIST.forEach(function (eachTaskType) {
            if (eachTaskType.NAME === taskType) {
                taskIcon = TaskTypeIconForTask[eachTaskType.NAME];
            }
        });
        return taskIcon;
    };
    TaskService.prototype.createWorkflowRule = function (ruleFormValues, projectId, workflowRuleId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                WorkflowRules: {
                    TriggerType: ruleFormValues.triggerType,
                    TargetType: ruleFormValues.targetType,
                    RPOProject: {
                        ProjectID: {
                            Id: projectId
                        }
                    },
                    RPOTask: {
                        TaskID: {
                            Id: ruleFormValues.currentTask
                        }
                    },
                    RPOStatus: {
                        StatusID: {
                            Id: ruleFormValues.triggerType === 'STATUS' ? ruleFormValues.triggerData : ''
                        }
                    },
                    RPOActions: {
                        ActionID: {
                            Id: ruleFormValues.triggerType === 'ACTION' ? ruleFormValues.triggerData : ''
                        }
                    },
                    RPOEvent: {
                        EventID: {
                            Id: ruleFormValues.targetType === 'EVENT' ? ruleFormValues.targetData : ''
                        }
                    },
                    RPOTargetTask: {
                        TargetTaskID: {
                            Id: ruleFormValues.targetType === 'TASK' ? ruleFormValues.targetData : ''
                        }
                    },
                    WorkflowRuleId: {
                        Id: workflowRuleId
                    },
                    IsInitialRule: ruleFormValues.isInitialRule
                }
            };
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.CREATE_WORKFLOW_RULE_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                    if (response.APIResponse.data && response.APIResponse.data.Workflow_Rules) {
                        observer.next(response.APIResponse.data.Workflow_Rules);
                        observer.complete();
                    }
                    else {
                        observer.next(false);
                        observer.complete();
                    }
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.removeWorkflowRule = function (WorkflowRuleId, RuleData) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                WorkflowRules: {
                    Rule: WorkflowRuleId
                },
                RuleData: RuleData
            };
            _this.appService.invokeRequest(_this.DELETE_WORKFLOW_NS, _this.REMOVE_WORKFLOW_RULES, parameter).subscribe(function (response) {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.deleteWorkflowRules = function (ruleIds) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                WorkflowRules: {
                    Rule: ruleIds
                }
            };
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.DELETE_WORKFLOW_RULES_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.getWorkflowRulesByProject = function (projectId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                projectId: projectId
            };
            _this.appService.invokeRequest(_this.WORKFLOW_RULE_NS, _this.GET_WORKFLOW_RULES_BY_PROJECT_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response && response.Workflow_Rules) {
                    observer.next(response.Workflow_Rules);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.getWorkflowRulesByTask = function (taskId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                taskId: taskId
            };
            _this.appService.invokeRequest(_this.WORKFLOW_RULE_NS, _this.GET_WORKFLOW_RULES_BY_TASK_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response && response.Workflow_Rules) {
                    observer.next(response.Workflow_Rules);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.UpdateDateOnRuleInitiate = function (projectId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                projectId: projectId
            };
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.UPDATE_DATE_ON_RULE_INITIATE_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.triggerWorkflow = function (projectId, isStatusRules) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                ProjectId: projectId,
                IsStatusRules: isStatusRules
            };
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.TRIGGER_TASK_BY_RULE_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response && response.APIResponse && response.APIResponse.statusCode && response.APIResponse.statusCode === '200') {
                    observer.next(response);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.getActionRulesByProjectId = function (projectId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                ProjectId: projectId
            };
            _this.appService.invokeRequest(_this.TASK_WSAPP_NS, _this.GET_ACTION_RULES_BY_PROJECT_ID_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response && response.tuple && response.tuple.old && response.tuple.old.ActionRules && response.tuple.old.ActionRules.ActionRule) {
                    var actionRuleResponse = void 0;
                    if (response.tuple.old.ActionRules.ActionRule.length > 0) {
                        actionRuleResponse = response.tuple.old.ActionRules.ActionRule;
                    }
                    else {
                        actionRuleResponse = [response.tuple.old.ActionRules.ActionRule];
                    }
                    observer.next(actionRuleResponse);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    TaskService.prototype.triggerRuleOnAction = function (taskId, actionId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                TaskId: taskId,
                ActionId: actionId
            };
            _this.appService.invokeRequest(_this.TASK_BPM_NS, _this.TRIGGER_RULE_ON_ACTION_WS_METHOD_NAME, parameter).subscribe(function (response) {
                if (response) {
                    observer.next(response);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            });
        });
    };
    /*deleteTaskDeliverable(type: string, Id: string, isCustomWorkflow: boolean): Observable<any> {
        const parameter = {
            type: type,
            id: Id,
            isCustomWorkflow: isCustomWorkflow
        };
        return new Observable(observer => {
            this.appService.invokeRequest(this.DELETE_WORKFLOW_NS, this.DELETE_WORKFLOW, parameter)
                .subscribe(response => {
                    observer.next(response);
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
        });
    }*/
    TaskService.prototype.forceDeleteTask = function (Id, isCustomWorkflow) {
        var _this = this;
        var parameter = {
            id: Id,
            isCustomWorkflow: isCustomWorkflow,
            isForceUploadDeliverableDelete: false
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELETE_WORKFLOW_NS, _this.FORCE_DELETE_TASK, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.prototype.softDeleteTask = function (Id, isCustomWorkflow) {
        var _this = this;
        var parameter = {
            id: Id,
            isCustomWorkflow: isCustomWorkflow
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELETE_WORKFLOW_NS, _this.SOFT_DELETE_TASK, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    //  MPMV3-2094
    TaskService.prototype.deleteDeliverable = function (deliverableID, forceDelete) {
        var _this = this;
        var parameter = {
            deliverableID: deliverableID,
            forceDelete: forceDelete
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELETE_DELIVERABLE_NS, _this.DELETE_DELIVERABLE, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    //  MPMV3-2094
    TaskService.prototype.getAllApprovalTasks = function (taskID) {
        var _this = this;
        var parameter = {
            taskIds: {
                TaskDetails: {
                    task: {
                        TaskId: taskID
                    }
                }
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.GET_ALL_APPROVAL_TASKS_NS, _this.GET_ALL_APPROVAL_TASKS, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    //  MPMV3-2094
    TaskService.prototype.deleteTask = function (taskID, forceDelete) {
        var _this = this;
        var parameter = {
            taskID: taskID,
            forceDelete: forceDelete
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.DELETE_TASK_NS, _this.DELETE_TASK, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    // MPM_V3-2217
    TaskService.prototype.assignDeliverable = function (deliverableId, userId) {
        var _this = this;
        var parameter = {
            DeliverableId: deliverableId,
            UserId: userId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.ASSIGN_DELIVERABLE_NS, _this.ASSIGN_DELIVERABLE, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    TaskService.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService },
        { type: OtmmMetadataService },
        { type: IndexerService },
        { type: FieldConfigService },
        { type: ViewConfigService },
        { type: SharingService }
    ]; };
    TaskService.ɵprov = i0.ɵɵdefineInjectable({ factory: function TaskService_Factory() { return new TaskService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.OtmmMetadataService), i0.ɵɵinject(i4.IndexerService), i0.ɵɵinject(i5.FieldConfigService), i0.ɵɵinject(i6.ViewConfigService), i0.ɵɵinject(i7.SharingService)); }, token: TaskService, providedIn: "root" });
    TaskService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], TaskService);
    return TaskService;
}());
export { TaskService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUVwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFJakQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFHbkYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBRS9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRTlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7Ozs7Ozs7O0FBTzFFO0lBZ0RJLHFCQUNXLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxjQUE4QixFQUM5QixrQkFBc0MsRUFDdEMsaUJBQW9DLEVBQ3BDLGNBQThCO1FBTjlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQXJEekMscUJBQWdCLEdBQUcsNkRBQTZELENBQUM7UUFDakYsd0JBQW1CLEdBQUcsNkRBQTZELENBQUM7UUFDcEYsZ0JBQVcsR0FBRyxzREFBc0QsQ0FBQztRQUNyRSxZQUFPLEdBQUcsK0NBQStDLENBQUM7UUFDMUQscUJBQWdCLEdBQUcseURBQXlELENBQUM7UUFDN0Usa0JBQWEsR0FBRywrQ0FBK0MsQ0FBQztRQUNoRSx1QkFBa0IsR0FBRyxzREFBc0QsQ0FBQztRQUU1RSx1Q0FBa0MsR0FBRyw4REFBOEQsQ0FBQztRQUNwRyxvQ0FBK0IsR0FBRyxnQ0FBZ0MsQ0FBQztRQUVuRSwwQkFBcUIsR0FBRyxzREFBc0QsQ0FBQztRQUMvRSw4QkFBeUIsR0FBRywrQ0FBK0MsQ0FBQztRQUM1RSxtQkFBYyxHQUFHLHNEQUFzRCxDQUFDO1FBRXhFLDBCQUFxQixHQUFHLHNEQUFzRCxDQUFDO1FBRS9FLG9CQUFlLEdBQUcsZ0JBQWdCLENBQUM7UUFDbkMsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFDdEMscUJBQWdCLEdBQUcsZ0JBQWdCLENBQUM7UUFDcEMsMkNBQXNDLEdBQUcsK0JBQStCLENBQUM7UUFDekUsaUNBQTRCLEdBQUcsZUFBZSxDQUFDO1FBQy9DLDhCQUF5QixHQUFHLFlBQVksQ0FBQztRQUN6Qyx1QkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztRQUN4QyxtQkFBYyxHQUFHLFVBQVUsQ0FBQztRQUM1QixnQkFBVyxHQUFHLFlBQVksQ0FBQztRQUMzQixnQkFBVyxHQUFHLFlBQVksQ0FBQztRQUMzQixjQUFTLEdBQUcsVUFBVSxDQUFDO1FBQ3ZCLHdDQUFtQyxHQUFHLHFCQUFxQixDQUFDO1FBQzVELGlEQUE0QyxHQUFHLDJCQUEyQixDQUFDO1FBQzNFLDhDQUF5QyxHQUFHLHdCQUF3QixDQUFDO1FBQ3JFLCtDQUEwQyxHQUFHLHNCQUFzQixDQUFDO1FBQ3BFLHdDQUFtQyxHQUFHLG1CQUFtQixDQUFDO1FBQzFELHlDQUFvQyxHQUFHLHFCQUFxQixDQUFDO1FBQzdELGdEQUEyQyxHQUFHLDBCQUEwQixDQUFDO1FBQ3pFLGtEQUE2QyxHQUFHLDJCQUEyQixDQUFDO1FBQzVFLDBDQUFxQyxHQUFHLHFCQUFxQixDQUFDO1FBQzlELHVCQUFrQixHQUFHLG1CQUFtQixDQUFDO1FBQ3pDLDJCQUFzQixHQUFHLHFCQUFxQixDQUFDO1FBQy9DLGdCQUFXLEdBQUcsWUFBWSxDQUFDO1FBQzNCLHVCQUFrQixHQUFHLG1CQUFtQixDQUFDO1FBQ3pDLDBCQUFxQixHQUFHLHFCQUFxQixDQUFDO1FBRTlDLDhCQUF5QixHQUFHLEVBQUUsQ0FBQztRQUMvQiw2QkFBd0IsR0FBRyxFQUFFLENBQUM7UUFZdkIsdUJBQWtCLEdBQUcsSUFBSSxlQUFlLENBQU0sR0FBRyxDQUFDLENBQUM7UUFDMUQsZ0JBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFOUMsa0NBQTZCLEdBQUcsSUFBSSxlQUFlLENBQU0sR0FBRyxDQUFDLENBQUM7UUFDckUsMkJBQXNCLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLFlBQVksRUFBRSxDQUFDO0lBTnZFLENBQUM7SUFRTCxvQ0FBYyxHQUFkLFVBQWUsSUFBUztRQUNwQixJQUFJLElBQUksS0FBSyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQztJQUVELCtDQUF5QixHQUF6QixVQUEwQixJQUFTO1FBQy9CLElBQUksSUFBSSxLQUFLLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsa0RBQTRCLEdBQTVCLFVBQTZCLGNBQWM7UUFDdkMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLGNBQWMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsa0RBQTRCLEdBQTVCO1FBQ0ksT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUM7SUFDMUMsQ0FBQztJQUVELGlEQUEyQixHQUEzQixVQUE0QixjQUFjO1FBQ3RDLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxjQUFjLENBQUM7SUFDbkQsQ0FBQztJQUVELGlEQUEyQixHQUEzQjtRQUNJLE9BQU8sSUFBSSxDQUFDLHdCQUF3QixDQUFDO0lBQ3pDLENBQUM7SUFFRCwrQkFBUyxHQUFULFVBQVUsaUJBQWlCO1FBQTNCLGlCQWdCQztRQWZHLElBQU0sZ0JBQWdCLEdBQUc7WUFDckIsaUJBQWlCLEVBQUUsaUJBQWlCO1NBQ3ZDLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLHNDQUFzQyxFQUFFLGdCQUFnQixDQUFDO2lCQUM5RyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUFFO2dCQUM3RSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLFVBQVU7UUFBdEIsaUJBa0JDO1FBakJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sTUFBTSxHQUFHO2dCQUNYLGNBQWMsRUFBRTtvQkFDWixFQUFFLEVBQUUsVUFBVTtpQkFDakI7YUFDSixDQUFDO1lBRUYsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLG1CQUFtQixFQUFFLEtBQUksQ0FBQyw0QkFBNEIsRUFBRSxNQUFNLENBQUM7aUJBQzdGLFNBQVMsQ0FBQyxVQUFBLHNCQUFzQjtnQkFDN0IsSUFBSSxzQkFBc0IsSUFBSSxzQkFBc0IsQ0FBQyxTQUFTLEVBQUU7b0JBQzVELFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ2hELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7WUFDTCxDQUFDLEVBQUUsVUFBQSxtQkFBbUI7Z0JBQ2xCLFFBQVEsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUN4QyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFhLEdBQWIsVUFBYyxRQUFRO1FBQXRCLGlCQW1CQztRQWxCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLE1BQU0sR0FBRztnQkFDWCxXQUFXLEVBQUU7b0JBQ1QsRUFBRSxFQUFFLFFBQVE7aUJBQ2Y7YUFDSixDQUFDO1lBRUYsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyx5QkFBeUIsRUFBRSxNQUFNLENBQUM7aUJBQ3ZGLFNBQVMsQ0FBQyxVQUFBLGNBQWM7Z0JBQ3JCLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7b0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNyQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsV0FBVztnQkFDVixRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxDQUFDO1FBRVgsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQXNCLEdBQXRCLFVBQXVCLFFBQVE7UUFDM0IsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLEdBQUcsRUFBRSxJQUFJO1FBQ2pCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFO1lBQ2hDLE9BQU87U0FDVjtRQUNELE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2hGLENBQUM7SUFFRCx1Q0FBaUIsR0FBakIsVUFBa0IsR0FBRyxFQUFFLElBQUk7UUFDdkIsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0UsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO1lBQ2pCLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsdUNBQWlCLEdBQWpCLFVBQWtCLFVBQVUsRUFBRSxlQUFnQjtRQUE5QyxpQkFvREM7UUFuREcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3ZCLFFBQVEsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQzthQUM1QztZQUNELElBQUksVUFBVSxDQUFDO1lBQ2YsSUFBSSxtQkFBbUIsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUMsYUFBYSxDQUFDLG1DQUFtQyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFFdEssbUJBQW1CLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUVsSixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLGVBQWUsRUFDakgsa0JBQWtCLENBQUMsRUFBRSxFQUFFLHNCQUFzQixDQUFDLEVBQUUsRUFBRSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXBHLElBQUksYUFBYSxFQUFFO2dCQUNmLG1CQUFtQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUMzQztZQUNELElBQUksZUFBZSxFQUFFO2dCQUNqQixVQUFVLEdBQUcsZUFBZSxDQUFDLENBQUEsNERBQTREO2FBQzVGO2lCQUFNO2dCQUNILFVBQVUsR0FBRyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO2FBQ3ZEO1lBQ0QsS0FBSSxDQUFDLGlCQUFpQixDQUFDLCtCQUErQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFdBQXVCO2dCQUNqRyxJQUFNLFlBQVksR0FBRyxXQUFXLENBQUMsMkJBQTJCLElBQUksT0FBTyxXQUFXLENBQUMsMkJBQTJCLEtBQUssUUFBUTtvQkFDdkgsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsMkJBQTJCLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkUsSUFBTSxhQUFhLEdBQWtCO29CQUNqQyxnQkFBZ0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSTtvQkFDcEQsT0FBTyxFQUFFLEVBQUU7b0JBQ1gscUJBQXFCLEVBQUU7d0JBQ25CLGdCQUFnQixFQUFFLG1CQUFtQjtxQkFDeEM7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ2xCLGVBQWUsRUFBRSxFQUFFO3FCQUN0QjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsSUFBSSxFQUFFLENBQUM7Z0NBQ0gsUUFBUSxFQUFFLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDO2dDQUN4RyxLQUFLLEVBQUUsS0FBSzs2QkFDZixDQUFDO3FCQUNMO29CQUNELE1BQU0sRUFBRTt3QkFDSixVQUFVLEVBQUUsVUFBVSxDQUFDLElBQUksSUFBSSxDQUFDO3dCQUNoQyxTQUFTLEVBQUUsVUFBVSxDQUFDLEdBQUcsSUFBSSxHQUFHO3FCQUNuQztpQkFDSixDQUFDO2dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQXdCO29CQUN6RSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO2dCQUNuRSxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUNBQWlCLEdBQWpCLFVBQWtCLE1BQWM7UUFBaEMsaUJBZ0JDO1FBZkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsTUFBTTtTQUNqQixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSx3QkFBd0IsRUFBRSxTQUFTLENBQUM7aUJBQy9FLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQztvQkFDVixZQUFZLEVBQUUsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29CQUMzSSxVQUFVLEVBQUUsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO2lCQUN0SSxDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNDQUFnQixHQUFoQixVQUFpQixVQUFVO1FBQTNCLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDO2lCQUMvRSxTQUFTLENBQUMsVUFBQSxxQkFBcUI7Z0JBQzVCLElBQUkscUJBQXFCLElBQUkscUJBQXFCLENBQUMsSUFBSSxFQUFFO29CQUNyRCxRQUFRLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMxQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBVyxHQUFYLFVBQVksTUFBYztRQUExQixpQkFlQztRQWRHLElBQU0sU0FBUyxHQUFHO1lBQ2QsU0FBUyxFQUFFO2dCQUNQLEVBQUUsRUFBRSxNQUFNO2FBQ2I7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLE9BQU8sRUFBRSxLQUFJLENBQUMsY0FBYyxFQUFFLFNBQVMsQ0FBQztpQkFDdEUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdDQUFVLEdBQVYsVUFBVyxVQUFVO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFFRCxnQ0FBVSxHQUFWLFVBQVcsVUFBVTtRQUFyQixpQkFVQztRQVRHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7aUJBQ3RFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJDQUFxQixHQUFyQixVQUFzQixRQUFnQjtRQUNsQyxJQUFJLFFBQVEsR0FBRyxxQkFBcUIsQ0FBQztRQUNyQyxhQUFhLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVk7WUFDN0MsSUFBSSxZQUFZLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDaEMsUUFBUSxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNyRDtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELHdDQUFrQixHQUFsQixVQUFtQixjQUFjLEVBQUUsU0FBUyxFQUFFLGNBQWM7UUFBNUQsaUJBeURDO1FBeERHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHO2dCQUNkLGFBQWEsRUFBRTtvQkFDWCxXQUFXLEVBQUUsY0FBYyxDQUFDLFdBQVc7b0JBQ3ZDLFVBQVUsRUFBRSxjQUFjLENBQUMsVUFBVTtvQkFDckMsVUFBVSxFQUFFO3dCQUNSLFNBQVMsRUFBRTs0QkFDUCxFQUFFLEVBQUUsU0FBUzt5QkFDaEI7cUJBQ0o7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLE1BQU0sRUFBRTs0QkFDSixFQUFFLEVBQUUsY0FBYyxDQUFDLFdBQVc7eUJBQ2pDO3FCQUNKO29CQUNELFNBQVMsRUFBRTt3QkFDUCxRQUFRLEVBQUU7NEJBQ04sRUFBRSxFQUFFLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFO3lCQUNoRjtxQkFDSjtvQkFDRCxVQUFVLEVBQUU7d0JBQ1IsUUFBUSxFQUFFOzRCQUNOLEVBQUUsRUFBRSxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTt5QkFDaEY7cUJBQ0o7b0JBQ0QsUUFBUSxFQUFFO3dCQUNOLE9BQU8sRUFBRTs0QkFDTCxFQUFFLEVBQUUsY0FBYyxDQUFDLFVBQVUsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUU7eUJBQzdFO3FCQUNKO29CQUNELGFBQWEsRUFBRTt3QkFDWCxZQUFZLEVBQUU7NEJBQ1YsRUFBRSxFQUFFLGNBQWMsQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFO3lCQUM1RTtxQkFDSjtvQkFDRCxjQUFjLEVBQUU7d0JBQ1osRUFBRSxFQUFFLGNBQWM7cUJBQ3JCO29CQUNELGFBQWEsRUFBRSxjQUFjLENBQUMsYUFBYTtpQkFDOUM7YUFDSixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsbUNBQW1DLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDbkgsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7b0JBQy9FLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFO3dCQUN2RSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUN4RCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUJBQ3ZCO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7aUJBQ0o7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0NBQWtCLEdBQWxCLFVBQW1CLGNBQWMsRUFBQyxRQUFRO1FBQTFDLGlCQWtCQztRQWpCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLFNBQVMsR0FBRztnQkFDZCxhQUFhLEVBQUU7b0JBQ1gsSUFBSSxFQUFFLGNBQWM7aUJBQ3ZCO2dCQUNELFFBQVEsVUFBQTthQUNYLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQzVHLElBQUksUUFBUSxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQW1CLEdBQW5CLFVBQW9CLE9BQU87UUFBM0IsaUJBaUJDO1FBaEJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHO2dCQUNkLGFBQWEsRUFBRTtvQkFDWCxJQUFJLEVBQUUsT0FBTztpQkFDaEI7YUFDSixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsb0NBQW9DLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDcEgsSUFBSSxRQUFRLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwrQ0FBeUIsR0FBekIsVUFBMEIsU0FBUztRQUFuQyxpQkFlQztRQWRHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHO2dCQUNkLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLDRDQUE0QyxFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pJLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxjQUFjLEVBQUU7b0JBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUN2QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFzQixHQUF0QixVQUF1QixNQUFNO1FBQTdCLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxTQUFTLEdBQUc7Z0JBQ2QsTUFBTSxFQUFFLE1BQU07YUFDakIsQ0FBQztZQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMseUNBQXlDLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDOUgsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsRUFBRTtvQkFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsOENBQXdCLEdBQXhCLFVBQXlCLFNBQVM7UUFBbEMsaUJBZUM7UUFkRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLFNBQVMsR0FBRztnQkFDZCxTQUFTLEVBQUUsU0FBUzthQUN2QixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsMkNBQTJDLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDM0gsSUFBSSxRQUFRLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQ0FBZSxHQUFmLFVBQWdCLFNBQVMsRUFBRSxhQUFhO1FBQXhDLGlCQWdCQztRQWZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHO2dCQUNkLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixhQUFhLEVBQUUsYUFBYTthQUMvQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMsbUNBQW1DLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDbkgsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLEVBQUU7b0JBQ2xILFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsK0NBQXlCLEdBQXpCLFVBQTBCLFNBQVM7UUFBbkMsaUJBcUJDO1FBcEJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHO2dCQUNkLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyw2Q0FBNkMsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUMvSCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFO29CQUNqSSxJQUFJLGtCQUFrQixTQUFBLENBQUM7b0JBQ3ZCLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN0RCxrQkFBa0IsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDO3FCQUNsRTt5QkFBTTt3QkFDSCxrQkFBa0IsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtxQkFDbkU7b0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUNsQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFtQixHQUFuQixVQUFvQixNQUFNLEVBQUUsUUFBUTtRQUFwQyxpQkFnQkM7UUFmRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLFNBQVMsR0FBRztnQkFDZCxNQUFNLEVBQUUsTUFBTTtnQkFDZCxRQUFRLEVBQUUsUUFBUTthQUNyQixDQUFDO1lBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxLQUFJLENBQUMscUNBQXFDLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDckgsSUFBSSxRQUFRLEVBQUU7b0JBQ1YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7O09BZUc7SUFFSCxxQ0FBZSxHQUFmLFVBQWdCLEVBQVUsRUFBRSxnQkFBeUI7UUFBckQsaUJBZUM7UUFkRyxJQUFNLFNBQVMsR0FBRztZQUNkLEVBQUUsRUFBRSxFQUFFO1lBQ04sZ0JBQWdCLEVBQUUsZ0JBQWdCO1lBQ2xDLDhCQUE4QixFQUFFLEtBQUs7U0FDeEMsQ0FBQTtRQUNELE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsaUJBQWlCLEVBQUUsU0FBUyxDQUFDO2lCQUNwRixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQ0FBYyxHQUFkLFVBQWUsRUFBVSxFQUFFLGdCQUF5QjtRQUFwRCxpQkFjQztRQWJHLElBQU0sU0FBUyxHQUFHO1lBQ2QsRUFBRSxFQUFFLEVBQUU7WUFDTixnQkFBZ0IsRUFBRSxnQkFBZ0I7U0FDckMsQ0FBQTtRQUNELE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsU0FBUyxDQUFDO2lCQUNuRixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxjQUFjO0lBQ2QsdUNBQWlCLEdBQWpCLFVBQWtCLGFBQWEsRUFBRSxXQUFXO1FBQTVDLGlCQWNDO1FBYkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxhQUFhLEVBQUUsYUFBYTtZQUM1QixXQUFXLEVBQUUsV0FBVztTQUMzQixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHFCQUFxQixFQUFFLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxTQUFTLENBQUM7aUJBQ3hGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGNBQWM7SUFDZCx5Q0FBbUIsR0FBbkIsVUFBb0IsTUFBTTtRQUExQixpQkFtQkM7UUFsQkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxPQUFPLEVBQUU7Z0JBQ0wsV0FBVyxFQUFFO29CQUNULElBQUksRUFBRTt3QkFDRixNQUFNLEVBQUUsTUFBTTtxQkFDakI7aUJBQ0o7YUFDSjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMseUJBQXlCLEVBQUUsS0FBSSxDQUFDLHNCQUFzQixFQUFFLFNBQVMsQ0FBQztpQkFDaEcsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsY0FBYztJQUNkLGdDQUFVLEdBQVYsVUFBVyxNQUFNLEVBQUUsV0FBVztRQUE5QixpQkFjQztRQWJHLElBQU0sU0FBUyxHQUFHO1lBQ2QsTUFBTSxFQUFFLE1BQU07WUFDZCxXQUFXLEVBQUUsV0FBVztTQUMzQixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGNBQWMsRUFBRSxLQUFJLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQztpQkFDMUUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBSUQsY0FBYztJQUNkLHVDQUFpQixHQUFqQixVQUFrQixhQUFhLEVBQUUsTUFBTTtRQUF2QyxpQkFjQztRQWJHLElBQU0sU0FBUyxHQUFHO1lBQ2QsYUFBYSxFQUFFLGFBQWE7WUFDNUIsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxLQUFJLENBQUMsa0JBQWtCLEVBQUUsU0FBUyxDQUFDO2lCQUN4RixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQTNrQnNCLFVBQVU7Z0JBQ1QsV0FBVztnQkFDSCxtQkFBbUI7Z0JBQ3hCLGNBQWM7Z0JBQ1Ysa0JBQWtCO2dCQUNuQixpQkFBaUI7Z0JBQ3BCLGNBQWM7OztJQXZEaEMsV0FBVztRQUp2QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsV0FBVyxDQThuQnZCO3NCQTNwQkQ7Q0EycEJDLEFBOW5CRCxJQThuQkM7U0E5bkJZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT3RtbU1ldGFkYXRhU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9vdG1tLW1ldGFkYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBvdG1tU2VydmljZXNDb25zdGFudHMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvY29uZmlnL290bW1TZXJ2aWNlLmNvbnN0YW50JztcclxuaW1wb3J0IHsgVGFza0NvbnN0YW50cyB9IGZyb20gJy4vdGFzay5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBUYXNrIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVGFzayc7XHJcbmltcG9ydCB7IE9ic2VydmVyc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vYnNlcnZlcnMnO1xyXG5pbXBvcnQgeyBUYXNrVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9UYXNrVHlwZSc7XHJcbmltcG9ydCB7IFRhc2tUeXBlSWNvbkZvclRhc2sgfSBmcm9tICcuLi9zaGFyZWQvY29uc3RhbnRzL3Rhc2stdHlwZS1pY29uLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNlYXJjaFJlcXVlc3QgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9vYmplY3RzL1NlYXJjaFJlcXVlc3QnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXNwb25zZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBJbmRleGVyU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2luZGV4ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdFNvcnREaXJlY3Rpb25zIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTWF0U29ydERpcmVjdGlvbic7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9ycyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvci5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFZpZXdDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3ZpZXctY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1NlYXJjaENvbmZpZ0NvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTVNlYXJjaE9wZXJhdG9yTmFtZXMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3JOYW1lLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVGFza1NlcnZpY2Uge1xyXG5cclxuICAgIFNUQVRVU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTVN1cHBvcnRpbmdFbnRpdHkvU3RhdHVzL29wZXJhdGlvbnMnO1xyXG4gICAgVEFTS19UWVBFX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQXBwQ29uZmlnL01QTV9UYXNrX1R5cGUvb3BlcmF0aW9ucyc7XHJcbiAgICBUQVNLX0JQTV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuICAgIFRBU0tfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvVGFzay9vcGVyYXRpb25zJztcclxuICAgIFdPUktGTE9XX1JVTEVfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvV29ya2Zsb3dfUnVsZXMvb3BlcmF0aW9ucyc7XHJcbiAgICBUQVNLX1dTQVBQX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcbiAgICBERUxFVEVfV09SS0ZMT1dfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcblxyXG4gICAgR0VUX01QTV9VU0VSX1BSRUZFUkVOQ0VfQllfTUVOVV9OUyA9IFwiaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1VzZXJfUHJlZmVyZW5jZS9vcGVyYXRpb25zXCI7XHJcbiAgICBHRVRfTVBNX1VTRVJfUFJFRkVSRU5DRV9CWV9NRU5VID0gXCJHZXREZWZhdWx0VXNlclByZWZlcmVuY2VCeU1lbnVcIjtcclxuXHJcbiAgICBERUxFVEVfREVMSVZFUkFCTEVfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcbiAgICBHRVRfQUxMX0FQUFJPVkFMX1RBU0tTX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS93c2FwcC9jb3JlLzEuMCc7XHJcbiAgICBERUxFVEVfVEFTS19OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vY29yZS93b3JrZmxvdy9icG0vMS4wJztcclxuXHJcbiAgICBBU1NJR05fREVMSVZFUkFCTEVfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcblxyXG4gICAgREVMRVRFX1dPUktGTE9XID0gJ0RlbGV0ZVdvcmtmbG93JztcclxuICAgIEZPUkNFX0RFTEVURV9UQVNLID0gJ0ZvcmNlRGVsZXRlVGFzayc7XHJcbiAgICBTT0ZUX0RFTEVURV9UQVNLID0gJ1NvZnREZWxldGVUYXNrJztcclxuICAgIEdFVF9TVEFUVVNfRk9SX0NBVEVHT1JZX1dTX01FVEhPRF9OQU1FID0gJ0dldFN0YXR1c0ZvckNhdGVnb3J5TGV2ZWxOYW1lJztcclxuICAgIEdFVF9UQVNLX1RZUEVfV1NfTUVUSE9EX05BTUUgPSAnUmVhZFRhc2tfVHlwZSc7XHJcbiAgICBHRVRfU1RBVFVTX1dTX01FVEhPRF9OQU1FID0gJ1JlYWRTdGF0dXMnO1xyXG4gICAgR0VUX0RFUEVOREVOVF9UQVNLID0gJ0dldERlcGVuZGVudFRhc2snO1xyXG4gICAgR0VUX1RBU0tfQllfSUQgPSAnUmVhZFRhc2snO1xyXG4gICAgVVBEQVRFX1RBU0sgPSAnVXBkYXRlVGFzayc7XHJcbiAgICBDUkVBVEVfVEFTSyA9ICdDcmVhdGVUYXNrJztcclxuICAgIEVESVRfVEFTSyA9ICdFZGl0VGFzayc7XHJcbiAgICBDUkVBVEVfV09SS0ZMT1dfUlVMRV9XU19NRVRIT0RfTkFNRSA9ICdDcmVhdGVXb3JrZmxvd1J1bGVzJztcclxuICAgIEdFVF9XT1JLRkxPV19SVUxFU19CWV9QUk9KRUNUX1dTX01FVEhPRF9OQU1FID0gJ0dldFdvcmtmbG93UnVsZXNCeVByb2plY3QnO1xyXG4gICAgR0VUX1dPUktGTE9XX1JVTEVTX0JZX1RBU0tfV1NfTUVUSE9EX05BTUUgPSAnR2V0V29ya2Zsb3dSdWxlc0J5VGFzayc7XHJcbiAgICBERUxFVEVfV09SS0ZMT1dfUlVMRVNfQllfSURfV1NfTUVUSE9EX05BTUUgPSAnRGVsZXRlV29ya2Zsb3dfUnVsZXMnO1xyXG4gICAgVFJJR0dFUl9UQVNLX0JZX1JVTEVfV1NfTUVUSE9EX05BTUUgPSAnVHJpZ2dlclRhc2tCeVJ1bGUnO1xyXG4gICAgREVMRVRFX1dPUktGTE9XX1JVTEVTX1dTX01FVEhPRF9OQU1FID0gJ0RlbGV0ZVdvcmtmbG93UnVsZXMnO1xyXG4gICAgVVBEQVRFX0RBVEVfT05fUlVMRV9JTklUSUFURV9XU19NRVRIT0RfTkFNRSA9ICdVcGRhdGVEYXRlT25SdWxlSW5pdGlhdGUnO1xyXG4gICAgR0VUX0FDVElPTl9SVUxFU19CWV9QUk9KRUNUX0lEX1dTX01FVEhPRF9OQU1FID0gJ0dldEFjdGlvblJ1bGVzQnlQcm9qZWN0SWQnO1xyXG4gICAgVFJJR0dFUl9SVUxFX09OX0FDVElPTl9XU19NRVRIT0RfTkFNRSA9ICdUcmlnZ2VyUnVsZU9uQWN0aW9uJztcclxuICAgIERFTEVURV9ERUxJVkVSQUJMRSA9ICdEZWxldGVEZWxpdmVyYWJsZSc7XHJcbiAgICBHRVRfQUxMX0FQUFJPVkFMX1RBU0tTID0gJ0dldEFsbEFwcHJvdmFsVGFza3MnO1xyXG4gICAgREVMRVRFX1RBU0sgPSAnRGVsZXRlVGFzayc7XHJcbiAgICBBU1NJR05fREVMSVZFUkFCTEUgPSAnQXNzaWduRGVsaXZlcmFibGUnO1xyXG4gICAgUkVNT1ZFX1dPUktGTE9XX1JVTEVTID0gJ1JlbW92ZVdvcmtmbG93UnVsZXMnOyBcclxuXHJcbiAgICBkZWZhdWx0VGFza01ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICBjdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tTWV0YWRhdGFTZXJ2aWNlOiBPdG1tTWV0YWRhdGFTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBpbmRleGVyU2VydmljZTogSW5kZXhlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBwdWJsaWMgcmVmcmVzaERhdGFTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KDEwMCk7XHJcbiAgICByZWZyZXNoRGF0YSA9IHRoaXMucmVmcmVzaERhdGFTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICAgIHB1YmxpYyBkZWxpdmVyYWJsZVJlZnJlc2hEYXRhU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PigxMDApO1xyXG4gICAgZGVsaXZlcmFibGVSZWZyZXNoRGF0YSA9IHRoaXMuZGVsaXZlcmFibGVSZWZyZXNoRGF0YVN1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcblxyXG4gICAgc2V0UmVmcmVzaERhdGEoZGF0YTogYW55KSB7XHJcbiAgICAgICAgaWYgKGRhdGEgIT09IDEwMCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hEYXRhU3ViamVjdC5uZXh0KGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXREZWxpdmVyYWJsZVJlZnJlc2hEYXRhKGRhdGE6IGFueSkge1xyXG4gICAgICAgIGlmIChkYXRhICE9PSAxMDApIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVJlZnJlc2hEYXRhU3ViamVjdC5uZXh0KGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXREZWZhdWx0VGFza01ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0VGFza01ldGFkYXRhRmllbGRzID0gbWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVmYXVsdFRhc2tNZXRhZGF0YUZpZWxkcygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kZWZhdWx0VGFza01ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcyhtZXRhZGF0YUZpZWxkcykge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tVGFza01ldGFkYXRhRmllbGRzID0gbWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VzdG9tVGFza01ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1c3RvbVRhc2tNZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdGF0dXMoY2F0ZWdvcnlMZXZlbE5hbWUpIHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICBDYXRlZ29yeUxldmVsTmFtZTogY2F0ZWdvcnlMZXZlbE5hbWVcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlNUQVRVU19NRVRIT0RfTlMsIHRoaXMuR0VUX1NUQVRVU19GT1JfQ0FURUdPUllfV1NfTUVUSE9EX05BTUUsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuU3RhdHVzKSkgeyByZXNwb25zZS5TdGF0dXMgPSBbcmVzcG9uc2UuU3RhdHVzXTsgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuU3RhdHVzKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrVHlwZSh0YXNrVHlwZUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgJ1Rhc2tfVHlwZS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogdGFza1R5cGVJZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX1RZUEVfTUVUSE9EX05TLCB0aGlzLkdFVF9UQVNLX1RZUEVfV1NfTUVUSE9EX05BTUUsIHBhcmFtcylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUodGFza1R5cGVEZXRhaWxSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2tUeXBlRGV0YWlsUmVzcG9uc2UgJiYgdGFza1R5cGVEZXRhaWxSZXNwb25zZS5UYXNrX1R5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0YXNrVHlwZURldGFpbFJlc3BvbnNlLlRhc2tfVHlwZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgdGFza1R5cGVEZXRhaWxFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IodGFza1R5cGVEZXRhaWxFcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdGF0dXNCeUlkKHN0YXR1c0lkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgJ1N0YXR1cy1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogc3RhdHVzSWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU1RBVFVTX01FVEhPRF9OUywgdGhpcy5HRVRfU1RBVFVTX1dTX01FVEhPRF9OQU1FLCBwYXJhbXMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN0YXR1c1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzUmVzcG9uc2UgJiYgc3RhdHVzUmVzcG9uc2UuU3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzUmVzcG9uc2UuU3RhdHVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBzdGF0dXNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3Ioc3RhdHVzRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZURyYWdhbmREcm9wTGFuZXMoc3dpbWxhbmUpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KG9iaiwgcGF0aCkge1xyXG4gICAgICAgIGlmICghb2JqIHx8ICFwYXRoIHx8ICFvYmoubWV0YWRhdGEpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5vdG1tTWV0YWRhdGFTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeUlkKG9iai5tZXRhZGF0YSwgcGF0aCwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVyVG9Mb2NhbERhdGUob2JqLCBwYXRoKSB7XHJcbiAgICAgICAgY29uc3QgZGF0ZU9iaiA9IHRoaXMub3RtbU1ldGFkYXRhU2VydmljZS5nZXRGaWVsZFZhbHVlQnlJZChvYmoubWV0YWRhdGEsIHBhdGgpO1xyXG4gICAgICAgIGlmIChkYXRlT2JqICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGVPYmo7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxBY3RpdmVUYXNrcyhwYXJhbWV0ZXJzLCBwcm9qZWN0Vmlld05hbWU/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXBhcmFtZXRlcnMucHJvamVjdElkKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignUHJvamVjdCBpZCBpcyBtaXNzaW5nLicpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCB2aWV3Q29uZmlnO1xyXG4gICAgICAgICAgICBsZXQgc2VhcmNoQ29uZGl0aW9uTGlzdCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmZvcm1JbmRleGVySWRGcm9tTWFwcGVySWQoVGFza0NvbnN0YW50cy5HRVRfQUxMX1RBU0tTX1NFQVJDSF9DT05ESVRJT05fTElTVC5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbik7XHJcblxyXG4gICAgICAgICAgICBzZWFyY2hDb25kaXRpb25MaXN0ID0gc2VhcmNoQ29uZGl0aW9uTGlzdC5jb25jYXQodGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZm9ybUluZGV4ZXJJZEZyb21NYXBwZXJJZChbVGFza0NvbnN0YW50cy5BQ1RJVkVfVEFTS19TRUFSQ0hfQ09ORElUSU9OXSkpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcHJvamVjdFNlYXJjaCA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmNyZWF0ZUNvbmRpdGlvbk9iamVjdChNUE1GaWVsZENvbnN0YW50cy5NUE1fVEFTS19GSUVMRFMuVEFTS19QUk9KRUNUX0lELFxyXG4gICAgICAgICAgICAgICAgTVBNU2VhcmNoT3BlcmF0b3JzLklTLCBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLCBNUE1TZWFyY2hPcGVyYXRvcnMuQU5ELCBwYXJhbWV0ZXJzLnByb2plY3RJZCk7XHJcblxyXG4gICAgICAgICAgICBpZiAocHJvamVjdFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9uTGlzdC5wdXNoKHByb2plY3RTZWFyY2gpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChwcm9qZWN0Vmlld05hbWUpIHtcclxuICAgICAgICAgICAgICAgIHZpZXdDb25maWcgPSBwcm9qZWN0Vmlld05hbWU7Ly90aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFZpZXdDb25maWdCeUlkKHByb2plY3RWaWV3SWQpLlZJRVc7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3Q29uZmlnID0gU2VhcmNoQ29uZmlnQ29uc3RhbnRzLlNFQVJDSF9OQU1FLlRBU0s7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy52aWV3Q29uZmlnU2VydmljZS5nZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKHZpZXdDb25maWcpLnN1YnNjcmliZSgodmlld0RldGFpbHM6IFZpZXdDb25maWcpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHZpZXdEZXRhaWxzLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyAmJiB0eXBlb2Ygdmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHID09PSAnc3RyaW5nJ1xyXG4gICAgICAgICAgICAgICAgICAgID8gcGFyc2VJbnQodmlld0RldGFpbHMuUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHLCAxMCkgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyc1JlcTogU2VhcmNoUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZmlnX2lkOiBzZWFyY2hDb25maWcgPyBzZWFyY2hDb25maWcgOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIGtleXdvcmQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBzZWFyY2hDb25kaXRpb25MaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb25fbGlzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmYWNldF9jb25kaXRpb246IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzb3J0aW5nX2xpc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydDogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkX2lkOiB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRJbmRleGVySWRCeU1hcHBlclZhbHVlKE1QTUZpZWxkQ29uc3RhbnRzLk1QTV9UQVNLX0ZJRUxEUy5UQVNLX05BTUUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3JkZXI6ICdBU0MnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1dXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZV9pbmRleDogcGFyYW1ldGVycy5za2lwIHx8IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2Vfc2l6ZTogcGFyYW1ldGVycy50b3AgfHwgMTAwXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXhlclNlcnZpY2Uuc2VhcmNoKHBhcmFtZXRlcnNSZXEpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBwcm9qZWN0cy4nKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrRGF0YVZhbHVlcyh0YXNrSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBUYXNrSUQ6IHRhc2tJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgJ0VkaXRUYXNrRGF0ZVZhbGlkYXRpb24nLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRNYXhEYXRlOiByZXNwb25zZSAmJiByZXNwb25zZS5EYXRlLlN0YXJ0RGF0ZSAmJiB0eXBlb2YgcmVzcG9uc2UuRGF0ZS5TdGFydERhdGUgPT09ICdzdHJpbmcnID8gbmV3IERhdGUocmVzcG9uc2UuRGF0ZS5TdGFydERhdGUpIDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kTWluRGF0ZTogcmVzcG9uc2UgJiYgcmVzcG9uc2UuRGF0ZS5FbmREYXRlICYmIHR5cGVvZiByZXNwb25zZS5EYXRlLkVuZERhdGUgPT09ICdzdHJpbmcnID8gbmV3IERhdGUocmVzcG9uc2UuRGF0ZS5FbmREYXRlKSA6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZXBlbmRlbnRUYXNrKHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19CUE1fTlMsIHRoaXMuR0VUX0RFUEVOREVOVF9UQVNLLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShkZXBlbmRlbnRUYXNrUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZXBlbmRlbnRUYXNrUmVzcG9uc2UgJiYgZGVwZW5kZW50VGFza1Jlc3BvbnNlLlRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkZXBlbmRlbnRUYXNrUmVzcG9uc2UuVGFzayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChbXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tCeUlkKHRhc2tJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxUYXNrPiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAnVGFzay1pZCc6IHtcclxuICAgICAgICAgICAgICAgIElkOiB0YXNrSWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX05TLCB0aGlzLkdFVF9UQVNLX0JZX0lELCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLlRhc2spO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVGFzayhwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5DUkVBVEVfVEFTSywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlVGFzayhwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfQlBNX05TLCB0aGlzLkVESVRfVEFTSywgcGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza0ljb25CeVRhc2tUeXBlKHRhc2tUeXBlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCB0YXNrSWNvbiA9ICdmaWJlcl9tYW51YWxfcmVjb3JkJztcclxuICAgICAgICBUYXNrQ29uc3RhbnRzLlRBU0tfVFlQRV9MSVNULmZvckVhY2goZWFjaFRhc2tUeXBlID0+IHtcclxuICAgICAgICAgICAgaWYgKGVhY2hUYXNrVHlwZS5OQU1FID09PSB0YXNrVHlwZSkge1xyXG4gICAgICAgICAgICAgICAgdGFza0ljb24gPSBUYXNrVHlwZUljb25Gb3JUYXNrW2VhY2hUYXNrVHlwZS5OQU1FXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiB0YXNrSWNvbjtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVXb3JrZmxvd1J1bGUocnVsZUZvcm1WYWx1ZXMsIHByb2plY3RJZCwgd29ya2Zsb3dSdWxlSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIFdvcmtmbG93UnVsZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBUcmlnZ2VyVHlwZTogcnVsZUZvcm1WYWx1ZXMudHJpZ2dlclR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgVGFyZ2V0VHlwZTogcnVsZUZvcm1WYWx1ZXMudGFyZ2V0VHlwZSxcclxuICAgICAgICAgICAgICAgICAgICBSUE9Qcm9qZWN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb2plY3RJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHByb2plY3RJZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBSUE9UYXNrOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFRhc2tJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHJ1bGVGb3JtVmFsdWVzLmN1cnJlbnRUYXNrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIFJQT1N0YXR1czoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBTdGF0dXNJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHJ1bGVGb3JtVmFsdWVzLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyA/IHJ1bGVGb3JtVmFsdWVzLnRyaWdnZXJEYXRhIDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgUlBPQWN0aW9uczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBBY3Rpb25JRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHJ1bGVGb3JtVmFsdWVzLnRyaWdnZXJUeXBlID09PSAnQUNUSU9OJyA/IHJ1bGVGb3JtVmFsdWVzLnRyaWdnZXJEYXRhIDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgUlBPRXZlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgRXZlbnRJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHJ1bGVGb3JtVmFsdWVzLnRhcmdldFR5cGUgPT09ICdFVkVOVCcgPyBydWxlRm9ybVZhbHVlcy50YXJnZXREYXRhIDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgUlBPVGFyZ2V0VGFzazoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBUYXJnZXRUYXNrSUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIElkOiBydWxlRm9ybVZhbHVlcy50YXJnZXRUeXBlID09PSAnVEFTSycgPyBydWxlRm9ybVZhbHVlcy50YXJnZXREYXRhIDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgV29ya2Zsb3dSdWxlSWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IHdvcmtmbG93UnVsZUlkXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBJc0luaXRpYWxSdWxlOiBydWxlRm9ybVZhbHVlcy5pc0luaXRpYWxSdWxlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19CUE1fTlMsIHRoaXMuQ1JFQVRFX1dPUktGTE9XX1JVTEVfV1NfTUVUSE9EX05BTUUsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuV29ya2Zsb3dfUnVsZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLldvcmtmbG93X1J1bGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZVdvcmtmbG93UnVsZShXb3JrZmxvd1J1bGVJZCxSdWxlRGF0YSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgV29ya2Zsb3dSdWxlczoge1xyXG4gICAgICAgICAgICAgICAgICAgIFJ1bGU6IFdvcmtmbG93UnVsZUlkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgUnVsZURhdGFcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfV09SS0ZMT1dfTlMsIHRoaXMuUkVNT1ZFX1dPUktGTE9XX1JVTEVTLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBcclxuXHJcbiAgICBkZWxldGVXb3JrZmxvd1J1bGVzKHJ1bGVJZHMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIFdvcmtmbG93UnVsZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBSdWxlOiBydWxlSWRzXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19CUE1fTlMsIHRoaXMuREVMRVRFX1dPUktGTE9XX1JVTEVTX1dTX01FVEhPRF9OQU1FLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFdvcmtmbG93UnVsZXNCeVByb2plY3QocHJvamVjdElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IHByb2plY3RJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLldPUktGTE9XX1JVTEVfTlMsIHRoaXMuR0VUX1dPUktGTE9XX1JVTEVTX0JZX1BST0pFQ1RfV1NfTUVUSE9EX05BTUUsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5Xb3JrZmxvd19SdWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuV29ya2Zsb3dfUnVsZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFdvcmtmbG93UnVsZXNCeVRhc2sodGFza0lkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICB0YXNrSWQ6IHRhc2tJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLldPUktGTE9XX1JVTEVfTlMsIHRoaXMuR0VUX1dPUktGTE9XX1JVTEVTX0JZX1RBU0tfV1NfTUVUSE9EX05BTUUsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5Xb3JrZmxvd19SdWxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuV29ya2Zsb3dfUnVsZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIFVwZGF0ZURhdGVPblJ1bGVJbml0aWF0ZShwcm9qZWN0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgICAgIHByb2plY3RJZDogcHJvamVjdElkXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVEFTS19CUE1fTlMsIHRoaXMuVVBEQVRFX0RBVEVfT05fUlVMRV9JTklUSUFURV9XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0cmlnZ2VyV29ya2Zsb3cocHJvamVjdElkLCBpc1N0YXR1c1J1bGVzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9qZWN0SWQ6IHByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgIElzU3RhdHVzUnVsZXM6IGlzU3RhdHVzUnVsZXNcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5UUklHR0VSX1RBU0tfQllfUlVMRV9XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzIwMCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBY3Rpb25SdWxlc0J5UHJvamVjdElkKHByb2plY3RJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgUHJvamVjdElkOiBwcm9qZWN0SWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX1dTQVBQX05TLCB0aGlzLkdFVF9BQ1RJT05fUlVMRVNfQllfUFJPSkVDVF9JRF9XU19NRVRIT0RfTkFNRSwgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnR1cGxlICYmIHJlc3BvbnNlLnR1cGxlLm9sZCAmJiByZXNwb25zZS50dXBsZS5vbGQuQWN0aW9uUnVsZXMgJiYgcmVzcG9uc2UudHVwbGUub2xkLkFjdGlvblJ1bGVzLkFjdGlvblJ1bGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgYWN0aW9uUnVsZVJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS50dXBsZS5vbGQuQWN0aW9uUnVsZXMuQWN0aW9uUnVsZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvblJ1bGVSZXNwb25zZSA9IHJlc3BvbnNlLnR1cGxlLm9sZC5BY3Rpb25SdWxlcy5BY3Rpb25SdWxlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvblJ1bGVSZXNwb25zZSA9IFtyZXNwb25zZS50dXBsZS5vbGQuQWN0aW9uUnVsZXMuQWN0aW9uUnVsZV1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3Rpb25SdWxlUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRyaWdnZXJSdWxlT25BY3Rpb24odGFza0lkLCBhY3Rpb25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgVGFza0lkOiB0YXNrSWQsXHJcbiAgICAgICAgICAgICAgICBBY3Rpb25JZDogYWN0aW9uSWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5UQVNLX0JQTV9OUywgdGhpcy5UUklHR0VSX1JVTEVfT05fQUNUSU9OX1dTX01FVEhPRF9OQU1FLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKmRlbGV0ZVRhc2tEZWxpdmVyYWJsZSh0eXBlOiBzdHJpbmcsIElkOiBzdHJpbmcsIGlzQ3VzdG9tV29ya2Zsb3c6IGJvb2xlYW4pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgdHlwZTogdHlwZSxcclxuICAgICAgICAgICAgaWQ6IElkLFxyXG4gICAgICAgICAgICBpc0N1c3RvbVdvcmtmbG93OiBpc0N1c3RvbVdvcmtmbG93XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9XT1JLRkxPV19OUywgdGhpcy5ERUxFVEVfV09SS0ZMT1csIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9Ki9cclxuXHJcbiAgICBmb3JjZURlbGV0ZVRhc2soSWQ6IHN0cmluZywgaXNDdXN0b21Xb3JrZmxvdzogYm9vbGVhbikgOk9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBpZDogSWQsXHJcbiAgICAgICAgICAgIGlzQ3VzdG9tV29ya2Zsb3c6IGlzQ3VzdG9tV29ya2Zsb3csXHJcbiAgICAgICAgICAgIGlzRm9yY2VVcGxvYWREZWxpdmVyYWJsZURlbGV0ZTogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfV09SS0ZMT1dfTlMsIHRoaXMuRk9SQ0VfREVMRVRFX1RBU0ssIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc29mdERlbGV0ZVRhc2soSWQ6IHN0cmluZywgaXNDdXN0b21Xb3JrZmxvdzogYm9vbGVhbikgOk9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICBpZDogSWQsXHJcbiAgICAgICAgICAgIGlzQ3VzdG9tV29ya2Zsb3c6IGlzQ3VzdG9tV29ya2Zsb3dcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5ERUxFVEVfV09SS0ZMT1dfTlMsIHRoaXMuU09GVF9ERUxFVEVfVEFTSywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAgTVBNVjMtMjA5NFxyXG4gICAgZGVsZXRlRGVsaXZlcmFibGUoZGVsaXZlcmFibGVJRCwgZm9yY2VEZWxldGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVJRDogZGVsaXZlcmFibGVJRCxcclxuICAgICAgICAgICAgZm9yY2VEZWxldGU6IGZvcmNlRGVsZXRlXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9ERUxJVkVSQUJMRV9OUywgdGhpcy5ERUxFVEVfREVMSVZFUkFCTEUsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gIE1QTVYzLTIwOTRcclxuICAgIGdldEFsbEFwcHJvdmFsVGFza3ModGFza0lEKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIHRhc2tJZHM6IHtcclxuICAgICAgICAgICAgICAgIFRhc2tEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFzazoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBUYXNrSWQ6IHRhc2tJRFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX0FQUFJPVkFMX1RBU0tTX05TLCB0aGlzLkdFVF9BTExfQVBQUk9WQUxfVEFTS1MsIHBhcmFtZXRlcilcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gIE1QTVYzLTIwOTRcclxuICAgIGRlbGV0ZVRhc2sodGFza0lELCBmb3JjZURlbGV0ZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICB0YXNrSUQ6IHRhc2tJRCxcclxuICAgICAgICAgICAgZm9yY2VEZWxldGU6IGZvcmNlRGVsZXRlXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9UQVNLX05TLCB0aGlzLkRFTEVURV9UQVNLLCBwYXJhbWV0ZXIpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG5cclxuICAgIC8vIE1QTV9WMy0yMjE3XHJcbiAgICBhc3NpZ25EZWxpdmVyYWJsZShkZWxpdmVyYWJsZUlkLCB1c2VySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgRGVsaXZlcmFibGVJZDogZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgVXNlcklkOiB1c2VySWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuQVNTSUdOX0RFTElWRVJBQkxFX05TLCB0aGlzLkFTU0lHTl9ERUxJVkVSQUJMRSwgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19