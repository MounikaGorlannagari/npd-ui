import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { ProjectConstant } from '../../project-overview/project.constants';
import { Observable, forkJoin } from 'rxjs';
import { TaskConstants } from '../task.constants';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { UtilService } from '../../../mpm-utils/services/util.service';
import * as acronui from '../../../mpm-utils/auth/utility';
import { TaskService } from '../task.service';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { EntityAppDefService } from '../../../mpm-utils/services/entity.appdef.service';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { LoaderService } from '../../../loader/loader.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { SaveOptionConstant } from '../../../shared/constants/save-options.constants';
import { ProjectUtilService } from '../../shared/services/project-util.service';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { map, startWith, distinctUntilChanged } from 'rxjs/operators';
import { NotificationService } from '../../../notification/notification.service';
var TaskCreationComponent = /** @class */ (function () {
    function TaskCreationComponent(appService, sharingService, adapter, utilService, taskService, entityAppdefService, loaderService, dialog, projectUtilService, commentsUtilService, notificationService) {
        var _this = this;
        this.appService = appService;
        this.sharingService = sharingService;
        this.adapter = adapter;
        this.utilService = utilService;
        this.taskService = taskService;
        this.entityAppdefService = entityAppdefService;
        this.loaderService = loaderService;
        this.dialog = dialog;
        this.projectUtilService = projectUtilService;
        this.commentsUtilService = commentsUtilService;
        this.notificationService = notificationService;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.isInvalidTaskOperation = false;
        this.ownerProjectCount = false;
        this.ownerProjectCountMessage = '';
        this.isZeroOwnerProjectCount = false;
        this.disableTaskSave = false;
        this.existingTaskTypeDetails = null;
        // existingAssignmentTypeDetails = null;
        this.taskConstants = TaskConstants;
        this.savedReviewers = [];
        this.allReviewers = [];
        // reviewers = [];
        this.deliverableReviewers = [];
        // reviewerDetails = [];
        this.reviewerStatusData = [];
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = false;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.newReviewers = [];
        this.removedReviewers = [];
        this.isPM = false;
        this.saveOptions = SaveOptionConstant.taskSaveOptions;
        this.SELECTED_SAVE_OPTION = {
            name: 'Save',
            value: 'SAVE'
        };
        this.DurationType = [
            { value: 'days', viewValue: 'Days' },
            { value: 'weeks', viewValue: 'Weeks' }
        ];
        this.enableDuration = false;
        this.enableWorkWeek = false;
        this.keyRestriction = {
            NUMBER: [46, 69, 186, 187, 188, 107]
        };
        this.dateFilter = function (date) {
            if (_this.enableWorkWeek) {
                return true;
            }
            else {
                var day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
    }
    TaskCreationComponent.prototype.filterReviewers = function (value) {
        if (value && typeof value === 'string' && value.trim() !== '') {
            var filterValue_1 = value.toLowerCase();
            return this.allReviewers.filter(function (reviewer) { return reviewer.value.toLowerCase().indexOf(filterValue_1) === 0; });
        }
    };
    TaskCreationComponent.prototype.restrictKeysOnType = function (event, datatype) {
        if (this.keyRestriction[datatype] && this.keyRestriction[datatype].includes(event.keyCode)) {
            event.preventDefault();
        }
    };
    TaskCreationComponent.prototype.selected = function (event) {
        var reviewerIndex = this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === event.option.value; });
        var reviewer = this.allReviewers.find(function (reviewerData) { return reviewerData.value === event.option.value; });
        if (this.taskForm.value.deliverableApprover &&
            event.option.value === this.taskForm.value.deliverableApprover.value) {
            this.notificationService.error(reviewer.displayName + ' is already the Approver');
        }
        else if (reviewerIndex > -1) {
            this.notificationService.error(reviewer.displayName + ' is already selected');
        }
        else {
            this.savedReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            this.newReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            var removeReviewerIndex = this.removedReviewers.findIndex(function (removedReviewer) { return removedReviewer.value === event.option.value; });
            if (removeReviewerIndex >= 0) {
                this.removedReviewers.splice(removeReviewerIndex, 1);
            }
            this.reviewerStatusData.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName,
                isCompleted: false
            });
            this.reviewerInput.nativeElement.value = '';
            this.taskForm.controls.deliverableReviewers.setValue(null);
        }
    };
    TaskCreationComponent.prototype.addReviewer = function (event) {
        if (!this.matAutocomplete.isOpen) {
            var input = event.input;
            if (input) {
                input.value = '';
            }
            this.taskForm.controls.deliverableReviewers.setValue(null);
        }
    };
    TaskCreationComponent.prototype.onRemoveReviewer = function (reviewerValue) {
        var index = this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === reviewerValue; });
        var newReviewerIndex = this.newReviewers.findIndex(function (newReviewer) { return newReviewer.value === reviewerValue; });
        var reviewerIndex = this.reviewerStatusData.findIndex(function (reviewerData) { return reviewerData.value === reviewerValue; });
        var reviewer = this.allReviewers.find(function (reviewerData) { return reviewerData.value === reviewerValue; });
        this.removedReviewers.push({
            name: reviewer.displayName,
            value: reviewer.value,
            displayName: reviewer.displayName
        });
        if (index >= 0) {
            this.taskForm.markAsDirty();
            this.savedReviewers.splice(index, 1);
        }
        if (reviewerIndex >= 0) {
            this.reviewerStatusData.splice(reviewerIndex, 1);
        }
        if (newReviewerIndex >= 0) {
            this.newReviewers.splice(newReviewerIndex, 1);
        }
    };
    TaskCreationComponent.prototype.getProjectDetails = function (projectObj) {
        var _this = this;
        return new Observable(function (observer) {
            if (projectObj && projectObj['Project-id'] && projectObj['Project-id'].Id) {
                if (projectObj.R_PO_CATEGORY && projectObj.R_PO_CATEGORY['MPM_Category-id']) {
                    _this.taskModalConfig.categoryId = projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
                }
                if (projectObj.R_PO_TEAM && projectObj.R_PO_TEAM['MPM_Teams-id']) {
                    _this.taskModalConfig.teamId = projectObj.R_PO_TEAM['MPM_Teams-id'].Id;
                }
                _this.taskModalConfig.selectedProject = projectObj;
                var currentUSerID = _this.sharingService.getCurrentUserItemID();
                if (currentUSerID && _this.taskModalConfig.selectedProject &&
                    currentUSerID === _this.taskModalConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    _this.taskModalConfig.isProjectOwner = true;
                }
                observer.next(true);
                observer.complete();
            }
        });
    };
    TaskCreationComponent.prototype.getPriorities = function () {
        var _this = this;
        return new Observable(function (observer) {
            /*  this.entityAppdefService.getPriorities(MPM_LEVELS.TASK)
                 .subscribe(priorityResponse => {
                     if (priorityResponse) {
                         if (!Array.isArray(priorityResponse)) {
                             priorityResponse = [priorityResponse];
                         }
                         this.taskModalConfig.taskPriorityList = priorityResponse;
                     } else {
                         this.taskModalConfig.taskPriorityList = [];
                     }
                     observer.next(true);
                     observer.complete();
                 }, () => {
                     const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                     this.notificationHandler.next(eventData);
                     observer.next(false);
                     observer.complete();
                 }); */
            var priority = _this.entityAppdefService.getPriorities(MPM_LEVELS.TASK);
            // .subscribe(priorityResponse => {
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                _this.taskModalConfig.taskPriorityList = priority;
            }
            else {
                _this.taskModalConfig.taskPriorityList = [];
            }
            observer.next(true);
            observer.complete();
            /*  }, () => {
                 const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                 this.notificationHandler.next(eventData);
                 observer.next(false);
                 observer.complete();
             }); */
        });
    };
    TaskCreationComponent.prototype.getStatus = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.entityAppdefService.getStatusByCategoryLevel(MPM_LEVELS.TASK)
                .subscribe(function (statusResponse) {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    _this.taskModalConfig.taskStatusList = statusResponse;
                }
                else {
                    _this.taskModalConfig.taskStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getInitialStatus = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.entityAppdefService.getStatus(MPM_LEVELS.TASK, StatusTypes.INITIAL)
                .subscribe(function (statusResponse) {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    _this.taskModalConfig.taskInitialStatusList = statusResponse;
                }
                else {
                    _this.taskModalConfig.taskInitialStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getAllRoles = function () {
        var _this = this;
        var getRequestObject = {
            teamID: this.taskModalConfig.teamId,
            isApproverTask: this.taskConfig.isApprovalTask,
            isMemberTask: !this.taskConfig.isApprovalTask
        };
        return new Observable(function (observer) {
            _this.loaderService.show();
            _this.appService.getTeamRoles(getRequestObject)
                .subscribe(function (response) {
                if (response && response.Teams && response.Teams.MPM_Teams && response.Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    response = response.Teams.MPM_Teams;
                    if (!Array.isArray(response.MPM_Team_Role_Mapping)) {
                        response.MPM_Team_Role_Mapping = [response.MPM_Team_Role_Mapping];
                    }
                    var roleList = void 0;
                    roleList = response.MPM_Team_Role_Mapping;
                    var roles_1 = [];
                    if (roleList && roleList.length && roleList.length > 0) {
                        roleList.map(function (role) {
                            roles_1.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    _this.teamRolesOptions = roles_1;
                    _this.taskModalConfig.taskRoleConfig.filterOptions = _this.teamRolesOptions;
                }
                else {
                    _this.teamRolesOptions = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getReviewerById = function (taskUserID) {
        var _this = this;
        return new Observable(function (observer) {
            if (!_this.utilService.isNullOrEmpty(taskUserID)) {
                var parameters = {
                    userId: taskUserID
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    };
    TaskCreationComponent.prototype.getDeliverableReviewByTaskId = function (TaskId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.getDeliverableReviewByTaskID(TaskId)
                .subscribe(function (response) {
                if (response && response.DeliverableReview) {
                    var reviewersList = acronui.findObjectsByProp(response, 'DeliverableReview');
                    if (reviewersList && reviewersList.length && reviewersList.length > 0) {
                        reviewersList.forEach(function (reviewer) {
                            if (!_this.utilService.isNullOrEmpty(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id)) {
                                _this.getReviewerById(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id).subscribe(function (reviewerData) {
                                    if (reviewerData) {
                                        var hasReviewerData = _this.savedReviewers.find(function (savedReviewer) { return savedReviewer.value === reviewerData.userCN; });
                                        if (!hasReviewerData) {
                                            _this.savedReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                            _this.deliverableReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                        }
                                    }
                                    _this.entityAppdefService.getStatusById(reviewer.R_PO_REVIEW_STATUS['MPM_Status-id'].Id).subscribe(function (statusResponse) {
                                        var isReviewerCompleted = statusResponse.STATUS_LEVEL === 'FINAL' ? true : false;
                                        var reviewerIndex = _this.reviewerStatusData.findIndex(function (data) { return data.value === reviewerData.userCN; });
                                        if (reviewerIndex >= 0) {
                                            _this.reviewerStatusData[reviewerIndex].isCompleted = isReviewerCompleted;
                                        }
                                        else {
                                            _this.reviewerStatusData.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName,
                                                isCompleted: isReviewerCompleted
                                            });
                                        }
                                    });
                                    observer.next(_this.savedReviewers);
                                    observer.complete();
                                });
                            }
                            else {
                                observer.next('');
                                observer.complete();
                            }
                        });
                    }
                    else {
                        observer.next('');
                        observer.complete();
                    }
                }
                else {
                    observer.next('');
                    observer.complete();
                }
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Deliverable Review Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getReviewers = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.taskConfig.isApprovalTask) {
                var getReviewerUserRequest = {};
                getReviewerUserRequest = {
                    allocationType: TaskConstants.ALLOCATION_TYPE_USER,
                    roleDN: '',
                    teamID: _this.taskModalConfig.teamId,
                    isApproveTask: true,
                    isUploadTask: false,
                    isReview: true
                };
                _this.appService.getUsersForTeam(getReviewerUserRequest)
                    .subscribe(function (response) {
                    if (response && response.users && response.users.user) {
                        var userList = acronui.findObjectsByProp(response, 'user');
                        var users_1 = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(function (user) {
                                var fieldObj = users_1.find(function (e) { return e.value === user.dn; });
                                if (!fieldObj) {
                                    users_1.push({
                                        name: user.dn,
                                        value: user.cn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        _this.allReviewers = users_1;
                    }
                    else {
                        _this.allReviewers = [];
                    }
                    // this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, function () {
                    var eventData = { message: 'Something went wrong while fetching Reviewer Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    };
    TaskCreationComponent.prototype.getUsersByRole = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.loaderService.show();
            var getUserByRoleRequest = {};
            if (_this.taskForm) {
                var taskFormValue = _this.taskForm.getRawValue();
                getUserByRoleRequest = {
                    allocationType: taskFormValue.allocation,
                    roleDN: taskFormValue.allocation === TaskConstants.ALLOCATION_TYPE_ROLE ? taskFormValue.role.value : '',
                    teamID: taskFormValue.allocation === TaskConstants.ALLOCATION_TYPE_USER ? _this.taskModalConfig.teamId : '',
                    isApproveTask: _this.taskConfig.isApprovalTask,
                    isUploadTask: !_this.taskConfig.isApprovalTask
                };
            }
            else if (_this.taskModalConfig.selectedTask) {
                var roleId = _this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE && _this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id']
                    ? _this.taskModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                var role = _this.formRoleValue(roleId);
                getUserByRoleRequest = {
                    allocationType: _this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE,
                    roleDN: _this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE === TaskConstants.ALLOCATION_TYPE_ROLE ? (role ? role.value : '') : '',
                    teamID: _this.taskModalConfig.selectedTask.ASSIGNMENT_TYPE === TaskConstants.ALLOCATION_TYPE_USER ? _this.taskModalConfig.teamId : '',
                    isApproveTask: _this.taskConfig.isApprovalTask,
                    isUploadTask: !_this.taskConfig.isApprovalTask
                };
            }
            else {
                getUserByRoleRequest = {
                    allocationType: TaskConstants.ALLOCATION_TYPE_USER,
                    roleDN: '',
                    teamID: _this.taskModalConfig.teamId,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (response) {
                if (response && response.users && response.users.user) {
                    var userList = acronui.findObjectsByProp(response, 'user');
                    var users_2 = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.map(function (user) {
                            var fieldObj = users_2.find(function (e) { return e.value === user.dn; });
                            if (!fieldObj) {
                                users_2.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    _this.teamRoleUserOptions = users_2;
                    _this.taskModalConfig.taskOwnerConfig.filterOptions = _this.teamRoleUserOptions;
                    _this.taskModalConfig.deliverableApprover.filterOptions = _this.teamRoleUserOptions;
                }
                else {
                    _this.teamRoleUserOptions = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getTaskTypeDetails = function (taskTypeId) {
        var _this = this;
        this.existingTaskTypeDetails = null;
        return new Observable(function (observer) {
            var params = {
                'Task_Type-id': {
                    Id: taskTypeId
                }
            };
            _this.appService.invokeRequest('http://schemas/AcheronMPMCore/Task_Type/operations', 'ReadTask_Type', params).subscribe(function (tasktypeDetailResponse) {
                var taskTypeDetails = acronui.findObjectsByProp(tasktypeDetailResponse, 'Task_Type');
                if (Array.isArray(taskTypeDetails) && taskTypeDetails.length > 0) {
                    _this.existingTaskTypeDetails = taskTypeDetails[0];
                }
                else {
                    var eventData = { message: 'No Task Type Details found', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                }
                observer.next(tasktypeDetailResponse);
                observer.complete();
            }, function (fail) {
                var eventData = { message: 'Something went wrong while fetching Task Type', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.error(fail);
            });
        });
    };
    /*  getAssignmentTypeDetails(assignmentTypeId: string) {
         this.existingAssignmentTypeDetails = null;
         return new Observable(
             observer => {
                 const params = {
                     'Assignment_Type-id': {
                         Id: assignmentTypeId
                     }
                 };
                 this.appService.invokeRequest('http://schemas/AcheronMPMCore/Assignment_Type/operations', 'ReadAssignment_Type', params).subscribe(
                     assignmentTypeDetailsResponse => {
                         const assignmentTypeDetails = acronui.findObjectsByProp(assignmentTypeDetailsResponse, 'Assignment_Type');
                         if (Array.isArray(assignmentTypeDetails) && assignmentTypeDetails.length > 0) {
                             this.existingAssignmentTypeDetails = assignmentTypeDetails[0];
                         } else {
                             const eventData = { message: 'No Assignment Type Details found', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                             this.notificationHandler.next(eventData);
                         }
                         observer.next(assignmentTypeDetailsResponse);
                         observer.complete();
                     },
                     fail => {
                         const eventData = { message: 'Something went wrong while fetching Task Type', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                         this.notificationHandler.next(eventData);
                         observer.error(fail);
                     }
                 );
             }
         );
     } */
    TaskCreationComponent.prototype.getDependentTasks = function (isApprovalTask) {
        var _this = this;
        return new Observable(function (observer) {
            var requestParameters = {
                ProjectID: _this.taskConfig.projectId,
                IsApprovalTask: isApprovalTask,
                AllowActionTask: _this.allowActionTask
            };
            _this.taskService.getDependentTask(requestParameters)
                .subscribe(function (dependentTaskResponse) {
                if (dependentTaskResponse) {
                    if (!Array.isArray(dependentTaskResponse)) {
                        dependentTaskResponse = [dependentTaskResponse];
                    }
                    _this.taskModalConfig.taskList = dependentTaskResponse;
                    if (_this.taskModalConfig.selectedTask) {
                        // removing the selected task from task list
                        _this.taskModalConfig.taskList = _this.taskModalConfig.taskList.filter(function (task) {
                            return task !== _this.taskModalConfig.selectedTask;
                        });
                    }
                    var predecessorFilterList_1 = [];
                    _this.taskModalConfig.taskList.map(function (task) {
                        if (_this.taskModalConfig.taskId && task['Task-id'].Id === _this.taskModalConfig.taskId) {
                            _this.taskModalConfig.selectedTask = task;
                        }
                        var value = _this.formPredecessorValue(task);
                        if (task.NAME && task['Task-id'] && task['Task-id'].Id) {
                            predecessorFilterList_1.push({
                                name: task.NAME,
                                value: task['Task-id'].Id,
                                displayName: value
                            });
                        }
                    });
                    _this.taskModalConfig.deliveryPackageConfig.filterOptions = predecessorFilterList_1;
                    _this.taskModalConfig.predecessorConfig.filterOptions = predecessorFilterList_1;
                }
                else {
                    if (_this.taskForm) {
                        _this.taskForm.controls.predecessor.patchValue('');
                        _this.taskModalConfig.taskList = [];
                        if (_this.taskConfig.isApprovalTask) {
                            _this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
                        }
                        else {
                            _this.taskModalConfig.predecessorConfig.filterOptions = [];
                        }
                    }
                    else {
                        _this.taskModalConfig.taskList = [];
                        _this.taskModalConfig.deliveryPackageConfig.filterOptions = [];
                        _this.taskModalConfig.predecessorConfig.filterOptions = [];
                    }
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            });
        });
    };
    TaskCreationComponent.prototype.getPreDetails = function () {
        var _this = this;
        this.getProjectDetails(this.taskConfig.projectObj)
            .subscribe(function () {
            _this.getDependentTasks(_this.taskConfig.isApprovalTask)
                .subscribe(function () {
                if (_this.taskModalConfig.categoryId && _this.taskModalConfig.teamId) {
                    _this.getAllRoles().subscribe(function () {
                        forkJoin([_this.getPriorities(), _this.getStatus(), _this.getInitialStatus(), _this.getUsersByRole(), _this.getReviewers()])
                            .subscribe(function () {
                            _this.taskModalConfig.taskAssignmentTypes = TaskConstants.ALLOCATION_TYPE;
                            _this.taskModalConfig.taskTypes = TaskConstants.TASK_TYPE_LIST;
                            if (_this.taskModalConfig.isNewTask) {
                                _this.loaderService.hide();
                                _this.initialiseTaskForm();
                            }
                            else {
                                var ownerId = (_this.taskModalConfig.selectedTask && _this.taskModalConfig.selectedTask.R_PO_OWNER_ID
                                    && _this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id']) ? _this.taskModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'].Id : '';
                                forkJoin([
                                    _this.taskService.getTaskDataValues(_this.taskConfig.taskId),
                                    _this.formOwnerValue(ownerId),
                                    _this.getDeliverableReviewByTaskId(_this.taskConfig.taskId)
                                ]).subscribe(function (response) {
                                    _this.loaderService.hide();
                                    _this.dateValidation = response[0];
                                    TaskConstants.TASK_TYPE_LIST.forEach(function (element) {
                                        if (_this.taskModalConfig && _this.taskModalConfig.selectedTask && _this.taskModalConfig.selectedTask.TASK_TYPE && element.NAME === _this.taskModalConfig.selectedTask.TASK_TYPE) {
                                            _this.existingTaskTypeDetails = element;
                                        }
                                    });
                                    TaskConstants.ALLOCATION_TYPE.forEach(function (element) {
                                        if (_this.taskModalConfig && _this.taskModalConfig.selectedTask && _this.taskModalConfig.selectedTask.ALLOCATION_TYPE && element.NAME === _this.taskModalConfig.selectedTask.ALLOCATION_TYPE) {
                                            _this.existingTaskTypeDetails = element;
                                        }
                                    });
                                    _this.initialiseTaskForm();
                                }, function () {
                                    _this.loaderService.hide();
                                });
                            }
                        });
                    });
                }
            }, function () {
                _this.loaderService.hide();
                var eventData = { message: 'Something went wrong while fetching Tasks', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
            });
        }, function () {
            _this.loaderService.hide();
            var eventData = { message: 'Something went wrong while fetching Project Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
            _this.notificationHandler.next(eventData);
        });
    };
    TaskCreationComponent.prototype.getTaskFields = function (task) {
        var taskField = {
            isApprovalTaskSelected: false,
            showOtherComments: false,
            isOtherCommentsSelected: false,
            showPredecessor: false,
            enableDeliverableApprover: false,
            showDeliveryPackage: false
        };
        if (task.IS_APPROVAL_TASK === 'true') {
            if (!this.utilService.isNullOrEmpty(task.PARENT_TASK_ID)) {
                taskField.isOtherCommentsSelected = false;
                taskField.showOtherComments = false;
                taskField.showPredecessor = false;
                taskField.showDeliveryPackage = true;
                taskField.enableDeliverableApprover = false;
                taskField.isApprovalTaskSelected = true;
            }
        }
        else {
            taskField.isApprovalTaskSelected = false;
            taskField.showOtherComments = false;
            taskField.isOtherCommentsSelected = false;
            taskField.showDeliveryPackage = false;
            taskField.showPredecessor = true;
            taskField.enableDeliverableApprover = false;
        }
        return taskField;
    };
    TaskCreationComponent.prototype.getPredecessortask = function (predecessorId) {
        var predecessorTask = null;
        this.taskModalConfig.taskList.map(function (task) {
            if (task['Task-id'].Id === predecessorId) {
                predecessorTask = task;
            }
        });
        return predecessorTask;
    };
    TaskCreationComponent.prototype.initialiseTaskForm = function () {
        var _this = this;
        var disableField = false;
        this.taskForm = null;
        this.taskModalConfig.minDate = this.taskModalConfig.selectedProject.START_DATE;
        this.taskModalConfig.maxDate = this.taskModalConfig.selectedProject.DUE_DATE;
        var pattern = ProjectConstant.NAME_STRING_PATTERN;
        if (!this.taskModalConfig.isTemplate) {
            if (this.taskModalConfig.isNewTask) {
                this.taskStatus = this.taskModalConfig.taskInitialStatusList;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                    status: new FormControl({
                        value: this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                            this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    owner: new FormControl({ value: '', disabled: disableField }),
                    role: new FormControl({ value: '', disabled: disableField }),
                    priority: new FormControl({
                        value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                            this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    startDate: new FormControl({
                        value: this.taskModalConfig.selectedProject.START_DATE, disabled: disableField
                    }, [Validators.required]),
                    endDate: new FormControl({
                        value: this.taskModalConfig.selectedProject.DUE_DATE, disabled: (disableField || this.enableDuration)
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                    isMilestone: new FormControl({ value: false, disabled: disableField }),
                    viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                    predecessor: new FormControl({ value: '', disabled: disableField }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    allocation: new FormControl({ value: '', disabled: disableField })
                });
                this.taskForm.updateValueAndValidity();
            }
            else if (this.taskModalConfig.selectedTask) {
                this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
                this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
                // if (selectedStatusInfo && (selectedStatusInfo['STATUS_LEVEL'] === StatusLevels.FINAL)) {
                // this.overViewConfig.isReadOnly = true;
                // disableFields = true;
                // this.disableStatusOptions = true;
                // }
                var task = this.taskModalConfig.selectedTask;
                var taskField = this.getTaskFields(task);
                var taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                // const taskOwnerId = task.R_PO_OWNER_ID && task.R_PO_OWNER_ID['Identity-id'] ? task.R_PO_OWNER_ID['Identity-id'].Id : '';
                task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: task.NAME, disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || !this.enableDuration)
                    }),
                    status: new FormControl({
                        value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                    }),
                    owner: new FormControl({
                        value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    role: new FormControl({
                        value: this.formRoleValue(taskRoleId), disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    priority: new FormControl({
                        value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                        disabled: disableField
                    }, [Validators.required]),
                    startDate: new FormControl({
                        value: this.commentsUtilService.addUnitsToDate(new Date(task.START_DATE), 0, 'days', this.enableWorkWeek), disabled: (disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL))
                    }, [Validators.required]),
                    endDate: new FormControl({ value: task.DUE_DATE, disabled: (disableField || this.enableDuration) }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                    isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                    viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                    predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                    // disabled: taskField.isApprovalTaskSelected || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL
                    deliverableApprover: new FormControl({
                        value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                    allocation: new FormControl({
                        value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    })
                });
                this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
                this.taskForm.controls.predecessor.disable();
                if (this.taskModalConfig.selectedpredecessor) {
                    var minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                }
                this.getTaskOwnerAssignmentCount();
                // this.onTaskSelection();
                this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
                this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject(this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
                this.taskForm.updateValueAndValidity();
            }
        }
        else {
            if (this.taskModalConfig.isNewTask) {
                this.taskStatus = this.taskModalConfig.taskInitialStatusList;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: '', disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    // 205
                    /*   duration: new FormControl({ value: '', disabled: (disableField || !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                      durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || !this.enableDuration) }),
                      */
                    duration: new FormControl({ value: '', disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({ value: this.DurationType[0].value, disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration) }),
                    status: new FormControl(this.taskModalConfig.taskInitialStatusList && this.taskModalConfig.taskInitialStatusList[0] &&
                        this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'] ? this.taskModalConfig.taskInitialStatusList[0]['MPM_Status-id'].Id : ''),
                    owner: new FormControl({ value: '', disabled: disableField }),
                    role: new FormControl({ value: '', disabled: disableField }),
                    priority: new FormControl({
                        value: this.taskModalConfig.taskPriorityList && this.taskModalConfig.taskPriorityList[0] &&
                            this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'] ? this.taskModalConfig.taskPriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.taskConfig.isApprovalTask ? this.defaultRevisionRequiredStatus : '', disabled: disableField }),
                    isMilestone: new FormControl({ value: false, disabled: disableField }),
                    viewOtherComments: new FormControl({ value: '', disabled: disableField }),
                    predecessor: new FormControl({ value: '', disabled: disableField }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    allocation: new FormControl({ value: '', disabled: disableField })
                });
            }
            else if (this.taskModalConfig.selectedTask) {
                this.taskStatus = this.projectUtilService.filterStatusOptions(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList, MPM_LEVELS.TASK);
                this.selectedStatusInfo = this.projectUtilService.getStatusByStatusId(this.taskModalConfig.selectedTask.R_PO_STATUS['MPM_Status-id'].Id, this.taskModalConfig.taskStatusList);
                var task = this.taskModalConfig.selectedTask;
                var taskField = this.getTaskFields(task);
                var taskRoleId = task.R_PO_TEAM_ROLE && task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'] ? task.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id : '';
                task.predecessor = (task.PARENT_TASK_ID && typeof task.PARENT_TASK_ID !== 'object') ? task.PARENT_TASK_ID : null;
                this.taskForm = new FormGroup({
                    taskName: new FormControl({ value: task.NAME, disabled: disableField }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    // 205
                    /* duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || !this.enableDuration)
                    }), */
                    duration: new FormControl({
                        value: typeof task.TASK_DURATION === 'string' ? task.TASK_DURATION : '',
                        disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                    }, [Validators.required, Validators.max(999), Validators.min(1)]),
                    durationType: new FormControl({
                        value: typeof task.TASK_DURATION_TYPE === 'string' ? task.TASK_DURATION_TYPE : this.DurationType[0].value,
                        disabled: (disableField || this.taskModalConfig.isTemplate ? disableField : !this.enableDuration)
                    }),
                    status: new FormControl({
                        value: task.R_PO_STATUS && task.R_PO_STATUS['MPM_Status-id'] ? task.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: true
                    }),
                    owner: new FormControl({
                        value: taskField.isApprovalTaskSelected ? '' : this.selectedOwner,
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    role: new FormControl({
                        value: this.formRoleValue(taskRoleId), disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    priority: new FormControl({
                        value: (task.R_PO_PRIORITY && task.R_PO_PRIORITY['MPM_Priority-id'] ? task.R_PO_PRIORITY['MPM_Priority-id'].Id : ''),
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: this.utilService.isNullOrEmpty(task.DESCRIPTION) ? '' : task.DESCRIPTION, disabled: disableField }, [Validators.maxLength(1000)]),
                    revisionReviewRequired: new FormControl({ value: this.utilService.getBooleanValue(task.REVISION_REVIEW_REQUIRED), disabled: true }),
                    isMilestone: new FormControl({ value: this.utilService.getBooleanValue(task.IS_MILESTONE), disabled: disableField }),
                    viewOtherComments: new FormControl({ value: taskField.showOtherComments, disabled: disableField }),
                    predecessor: new FormControl(this.formPredecessor(task.predecessor)),
                    deliverableApprover: new FormControl({
                        value: taskField.isApprovalTaskSelected ? this.selectedOwner : '',
                        disabled: disableField || (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers }),
                    allocation: new FormControl({
                        value: task.ASSIGNMENT_TYPE, disabled: disableField ||
                            (this.selectedStatusInfo && this.selectedStatusInfo.STATUS_TYPE !== this.taskConstants.STATUS_TYPE_INITIAL)
                    })
                });
                this.taskModalConfig.selectedpredecessor = this.getPredecessortask(task.predecessor);
                this.taskForm.controls.predecessor.disable();
                if (this.taskModalConfig.selectedpredecessor) {
                    var minDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', this.enableWorkWeek);
                    this.taskModalConfig.minDate = minDateObject;
                }
                this.getTaskOwnerAssignmentCount();
                // this.onTaskSelection();
                this.taskConfig.taskType = this.taskModalConfig.selectedTask.TASK_TYPE;
                this.oldTaskObject = this.projectUtilService.createNewTaskUpdateObject(this.taskForm.getRawValue(), this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.savedReviewers);
            }
        }
        this.filteredReviewers = this.taskForm.get('deliverableReviewers').valueChanges.pipe(startWith(''), map(function (reviewer) { return reviewer ? _this.filterReviewers(reviewer) : _this.allReviewers.slice(); }));
        this.taskModalConfig.selectedStatusItemId = this.taskForm.value.status;
        // this.taskForm.controls.status.disable();
        this.taskModalConfig.predecessorConfig.formControl = this.taskForm.controls.predecessor;
        this.taskModalConfig.taskOwnerConfig.formControl = this.taskForm.controls.owner;
        this.taskModalConfig.taskRoleConfig.formControl = this.taskForm.controls.role;
        this.taskModalConfig.deliverableApprover.formControl = this.taskForm.controls.deliverableApprover;
        this.taskModalConfig.deliverableReviewer.formControl = this.taskForm.controls.deliverableReviewer;
        this.taskModalConfig.deliveryPackageConfig.formControl = this.taskForm.controls.predecessor;
        if (this.taskForm.controls.allocation.value === TaskConstants.ALLOCATION_TYPE_ROLE) {
            if (this.taskForm.controls.owner && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.deliverableApprover.disable();
            }
            if (this.taskForm.controls.deliverableReviewer && !this.taskModalConfig.selectedTask) {
                this.taskForm.controls.deliverableReviewer.disable();
            }
            /* if (this.taskForm.controls.role.value !== '') {
                this.getUsersByRole().subscribe(() => {
                    this.taskModalConfig.taskOwnerConfig.filterOptions = this.teamRoleUserOptions;
                    this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
                });
            } */
        }
        this.taskForm.controls.predecessor.valueChanges.subscribe(function (predecessor) {
            // TODO predecessor change method
            if (!_this.taskModalConfig.selectedTask && typeof predecessor === 'object' && predecessor && predecessor.value) {
                _this.taskModalConfig.taskList.map(function (task) {
                    if (task['Task-id'].Id === predecessor.value) {
                        /*if (task.REVISION_REVIEW_REQUIRED) {
                            this.taskForm.controls.revisionReviewRequired.patchValue(true);
                        }*/
                        var minDateObject = _this.commentsUtilService.addUnitsToDate(new Date(task.DUE_DATE), 1, 'days', _this.enableWorkWeek);
                        _this.taskModalConfig.minDate = minDateObject;
                        _this.taskModalConfig.maxDate = _this.taskModalConfig.selectedProject.DUE_DATE;
                        if (!_this.taskModalConfig.isTemplate) {
                            _this.taskForm.controls.startDate.setValue(minDateObject);
                            _this.taskForm.controls.endDate.setValue(_this.taskModalConfig.selectedProject.DUE_DATE);
                        }
                    }
                });
            }
            else {
                _this.taskModalConfig.minDate = _this.taskModalConfig.selectedpredecessor ?
                    _this.commentsUtilService.addUnitsToDate(new Date(_this.taskModalConfig.selectedpredecessor.DUE_DATE), 1, 'days', _this.enableWorkWeek) : _this.taskModalConfig.selectedProject.START_DATE;
                _this.taskModalConfig.maxDate = _this.taskModalConfig.selectedProject.DUE_DATE;
                var selectedPredecessor = void 0;
                if (_this.taskConfig.isApprovalTask) {
                    selectedPredecessor = _this.taskModalConfig.deliveryPackageConfig.filterOptions.find(function (predecessorValue) { return predecessorValue.displayName === predecessor; });
                }
                else {
                    selectedPredecessor = _this.taskModalConfig.predecessorConfig.filterOptions.find(function (predecessorValue) { return predecessorValue.displayName === predecessor; });
                }
                if (selectedPredecessor) {
                    _this.taskForm.controls.predecessor.setValue(selectedPredecessor);
                }
                // if (!this.taskModalConfig.isTemplate) {
                //     this.taskForm.controls.startDate.setValue(this.taskModalConfig.selectedProject.START_DATE);
                //     this.taskForm.controls.endDate.setValue(this.taskModalConfig.selectedProject.DUE_DATE);
                // }
            }
        });
        if (this.taskForm.controls.owner) {
            this.taskForm.controls.owner.valueChanges.subscribe(function (ownerValue) {
                if (ownerValue && typeof ownerValue === 'object' && !_this.taskModalConfig.isTemplate) {
                    _this.taskModalConfig.taskOwnerConfig.filterOptions.map(function (taskOwnerName) {
                        if (taskOwnerName.value === ownerValue.value) {
                            _this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    _this.ownerProjectCountMessage = '';
                    var selectedOwner = _this.taskModalConfig.taskOwnerConfig.filterOptions.find(function (owner) { return owner.displayName === ownerValue; });
                    if (selectedOwner) {
                        _this.taskForm.controls.owner.setValue(selectedOwner);
                    }
                }
            });
        }
        this.taskForm.controls.allocation.valueChanges.subscribe(function (allocationTypeValue) {
            if (_this.taskForm.controls.role) {
                _this.taskForm.controls.role.setValue('');
            }
            if (_this.taskForm.controls.owner) {
                _this.taskForm.controls.owner.setValue('');
            }
            if (_this.taskForm.controls.deliverableApprover) {
                _this.taskForm.controls.deliverableApprover.setValue('');
            }
            if (_this.taskForm.controls.deliverableReviewers) {
                _this.taskForm.controls.deliverableReviewers.setValue('');
            }
            _this.onChangeofAllocationType(allocationTypeValue, null, null, true);
        });
        /* this.taskForm.controls.deliverableReviewers.valueChanges.subscribe(deliverableReviewers => {
            console.log(deliverableReviewers);
        }); */
        this.taskForm.controls.role.valueChanges.subscribe(function (role) {
            if (role && typeof role === 'object') {
                _this.onChangeofRole(role);
            }
            else {
                var selectedRole = _this.taskModalConfig.taskRoleConfig.filterOptions.find(function (taskRole) { return taskRole.displayName === role; });
                if (selectedRole) {
                    _this.taskForm.controls.role.setValue(selectedRole);
                }
            }
        });
        if (this.taskForm.controls.startDate) {
            this.taskForm.controls.startDate.valueChanges.subscribe(function () {
                if (_this.taskForm.value.owner && _this.taskForm.value.owner.value) {
                    _this.taskModalConfig.taskOwnerConfig.filterOptions.map(function (taskOwnerName) {
                        if (taskOwnerName.value === _this.taskForm.value.owner.value) {
                            _this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    _this.ownerProjectCountMessage = '';
                }
                if (_this.dateValidation && _this.dateValidation.startMaxDate && new Date(_this.taskForm.controls.startDate.value) > new Date(_this.dateValidation.startMaxDate)) {
                    _this.dateErrorMessage = _this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else {
                    _this.dateErrorMessage = _this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
                if (new Date(_this.taskForm.controls.endDate.value) > new Date(_this.taskModalConfig.selectedProject.DUE_DATE)) {
                    _this.durationErrorMessage = _this.taskConstants.DURATION_ERROR_MESSAGE;
                }
                if (_this.enableDuration) {
                    _this.validateDuration();
                }
                if (_this.dateValidation && _this.dateValidation.startMaxDate && new Date(_this.taskForm.controls.startDate.value) > new Date(_this.dateValidation.startMaxDate)) {
                    _this.dateErrorMessage = _this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else {
                    _this.dateErrorMessage = _this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
            });
            this.taskForm.controls.endDate.valueChanges.subscribe(function () {
                if (_this.taskForm.value.owner && _this.taskForm.value.owner.value) {
                    _this.taskModalConfig.taskOwnerConfig.filterOptions.map(function (taskOwnerName) {
                        if (taskOwnerName.value === _this.taskForm.value.owner.value) {
                            _this.getTaskOwnerAssignmentCount();
                        }
                    });
                }
                else {
                    _this.ownerProjectCountMessage = '';
                }
                if (_this.dateValidation && _this.dateValidation.endMinDate && new Date(_this.taskForm.controls.endDate.value) < new Date(_this.dateValidation.endMinDate)) {
                    _this.dateErrorMessage = _this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                }
                else if (new Date(_this.taskForm.controls.endDate.value) < new Date(_this.taskForm.controls.startDate.value)) {
                    _this.dateErrorMessage = _this.taskConstants.START_DATE_ERROR_MESSAGE;
                }
                else {
                    _this.dateErrorMessage = _this.taskConstants.PROJECT_DATE_ERROR_MESSAGE;
                }
            });
        }
        if (this.enableDuration) {
            this.taskForm.controls.duration.valueChanges.pipe(distinctUntilChanged()).subscribe(function () {
                if (_this.taskForm.controls.startDate && _this.taskForm.controls.startDate.value && _this.taskForm.controls.duration && _this.taskForm.controls.duration.value) {
                    _this.validateDuration();
                }
            });
            this.taskForm.controls.durationType.valueChanges.pipe(distinctUntilChanged()).subscribe(function () {
                if (_this.taskForm.controls.startDate && _this.taskForm.controls.startDate.value && _this.taskForm.controls.duration && _this.taskForm.controls.duration.value) {
                    _this.validateDuration();
                }
            });
        }
        this.taskModalConfig.hasAllConfig = true;
    };
    /* onTaskSelection() {
        const roles = [];
        const roleIds = [];
        if (this.taskConfig.isApprovalTask) {
            this.teamRolesOptions.find(element => {
                if (element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_APPROVER) !== -1) {
                    roles.push(element);
                    roleIds.push(element.name);
                }
            });
        } else {
            this.teamRolesOptions.find(element => {
                if (element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.appRoles.ROLE_DN.search(RoleConstants.PROJECT_MEMBER) !== -1) {
                    roles.push(element);
                    roleIds.push(element.name);
                }
            });
        }
        this.taskModalConfig.taskRoleConfig.filterOptions = roles;
        this.getUsersByRole().subscribe(() => {
            this.taskModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
        });
    } */
    TaskCreationComponent.prototype.validateDuration = function () {
        if (this.taskForm.controls.duration && this.taskForm.controls.duration.value) {
            if (this.taskForm.controls.duration.value < 1 || this.taskForm.controls.duration.value > 999) {
                this.durationErrorMessage = this.taskConstants.DURATION_MIN_ERROR_MESSAGE;
            }
            else {
                var endDateObject = this.commentsUtilService.addUnitsToDate(new Date(this.taskForm.controls.startDate.value), this.taskForm.controls.durationType.value === 'weeks' ? (this.taskForm.controls.duration.value) : (this.taskForm.controls.duration.value - 1), this.taskForm.controls.durationType.value, this.enableWorkWeek);
                this.taskForm.controls.endDate.setValue(endDateObject);
                this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(1)]);
                this.taskForm.controls.duration.updateValueAndValidity();
                if (this.taskModalConfig && this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) {
                    if (new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE) >= new Date(this.taskForm.controls.endDate.value)) {
                        var minDuration = this.commentsUtilService.getDaysBetweenTwoDates(new Date(this.taskModalConfig.selectedTask.MAX_DELIVERABLE_DATE), new Date(this.taskForm.controls.endDate.value), this.taskForm.controls.durationType.value);
                        this.taskForm.controls.duration.setValidators([Validators.required, Validators.max(999), Validators.min(minDuration + 1)]);
                        this.taskForm.controls.duration.updateValueAndValidity();
                        this.durationErrorMessage = this.taskConstants.DELIVERABLE_DATE_ERROR_MESSAGE;
                    }
                }
            }
        }
    };
    TaskCreationComponent.prototype.onChangeofAllocationType = function (allocationTypeValue, ownerValue, roleValue, changeDeliverableApprover) {
        var _this = this;
        if (allocationTypeValue === TaskConstants.ALLOCATION_TYPE_ROLE) {
            if (this.taskForm.controls.owner) {
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.disable();
            }
            this.taskForm.controls.role.setValidators([Validators.required]);
            this.taskForm.controls.owner.clearValidators();
            this.taskForm.controls.owner.updateValueAndValidity();
            if (this.taskConfig.isApprovalTask) {
                this.taskForm.controls.deliverableApprover.clearValidators();
                this.taskForm.controls.deliverableApprover.updateValueAndValidity();
            }
            this.taskForm.controls.role.patchValue(roleValue ? roleValue : '');
            this.taskForm.controls.owner.patchValue(ownerValue ? ownerValue : '');
            if (changeDeliverableApprover && this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.patchValue('');
            }
        }
        else if (allocationTypeValue === TaskConstants.ALLOCATION_TYPE_USER) {
            this.getUsersByRole().subscribe(function () {
                _this.taskModalConfig.taskOwnerConfig.filterOptions = _this.teamRoleUserOptions;
                _this.taskModalConfig.deliverableApprover.filterOptions = _this.teamRoleUserOptions;
                _this.taskForm.controls.owner.patchValue(ownerValue ? ownerValue : '');
                if (changeDeliverableApprover && _this.taskForm.controls.deliverableApprover) {
                    _this.taskForm.controls.deliverableApprover.patchValue('');
                }
                _this.taskForm.controls.role.clearValidators();
                _this.taskForm.controls.role.updateValueAndValidity();
                if (_this.taskConfig.isApprovalTask) {
                    _this.taskForm.controls.deliverableApprover.setValidators([Validators.required]);
                    _this.taskForm.controls.deliverableApprover.updateValueAndValidity();
                }
                else {
                    _this.taskForm.controls.owner.setValidators([Validators.required]);
                    _this.taskForm.controls.owner.updateValueAndValidity();
                }
                if (_this.taskForm.controls.owner) {
                    if (_this.taskForm.controls.role && !_this.taskForm.controls.role.disabled) {
                        _this.taskForm.controls.owner.enable();
                    }
                }
                if (_this.taskForm.controls.deliverableApprover) {
                    if (_this.taskForm.controls.role && !_this.taskForm.controls.role.disabled) {
                        _this.taskForm.controls.deliverableApprover.enable();
                    }
                }
            });
        }
    };
    TaskCreationComponent.prototype.onChangeofRole = function (role) {
        var _this = this;
        if (role && role.value) {
            if (this.taskForm.controls.owner) {
                if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                    this.taskForm.controls.owner.enable();
                }
            }
            if (this.taskForm.controls.deliverableApprover) {
                if (this.taskForm.controls.role && !this.taskForm.controls.role.disabled) {
                    this.taskForm.controls.deliverableApprover.enable();
                }
            }
            this.getUsersByRole().subscribe(function () {
                _this.taskModalConfig.taskOwnerConfig.filterOptions = _this.teamRoleUserOptions;
                _this.taskModalConfig.deliverableApprover.filterOptions = _this.teamRoleUserOptions;
                if (_this.taskForm.controls.owner) {
                    _this.taskForm.controls.owner.patchValue('');
                }
                if (_this.taskForm.controls.deliverableApprover) {
                    _this.taskForm.controls.deliverableApprover.patchValue('');
                }
            });
        }
        else {
            if (this.taskForm.controls.owner) {
                this.taskForm.controls.owner.patchValue((role ? role : ''));
                this.taskForm.controls.owner.disable();
            }
            if (this.taskForm.controls.deliverableApprover) {
                this.taskForm.controls.deliverableApprover.patchValue((role ? role : ''));
                this.taskForm.controls.deliverableApprover.disable();
            }
        }
    };
    TaskCreationComponent.prototype.onApprovalStateChange = function () {
        var _this = this;
        this.taskForm.controls.predecessor.setValue('');
        forkJoin([this.getAllRoles(), this.getDependentTasks(this.taskConfig.isApprovalTask)])
            .subscribe(function () {
            _this.taskForm.controls.role.updateValueAndValidity();
            _this.taskForm.controls.predecessor.updateValueAndValidity();
        });
    };
    TaskCreationComponent.prototype.formPredecessorValue = function (task) {
        if (task.NAME && task['Task-id'] && task['Task-id'].Id) {
            return task['Task-id'].Id + '-' + task.NAME;
        }
    };
    TaskCreationComponent.prototype.formPredecessor = function (taskId) {
        if (taskId) {
            var selectedPredecessor = this.taskModalConfig.predecessorConfig.filterOptions.find(function (element) { return element.value === taskId; });
            return selectedPredecessor ? selectedPredecessor : '';
        }
        else {
            return '';
        }
    };
    TaskCreationComponent.prototype.formOwnerValue = function (taskUserID) {
        var _this = this;
        return new Observable(function (observer) {
            if (!_this.utilService.isNullOrEmpty(taskUserID)) {
                var parameters = {
                    userId: taskUserID
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (response) {
                    var selectedUser = _this.teamRoleUserOptions.find(function (element) { return element.value === response.userCN; });
                    if (selectedUser) {
                        _this.selectedOwner = selectedUser;
                    }
                    observer.next(true);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    };
    TaskCreationComponent.prototype.formRoleValue = function (taskRoleID) {
        if (taskRoleID) {
            var selectedRole = this.teamRolesOptions.find(function (element) { return element.name === taskRoleID; });
            return (selectedRole) ? selectedRole : '';
        }
        else {
            return '';
        }
    };
    TaskCreationComponent.prototype.getTaskOwnerAssignmentCount = function () {
    };
    TaskCreationComponent.prototype.cancelTaskCreation = function () {
        var _this = this;
        if (this.taskForm.pristine) {
            this.closeCallbackHandler.next(true);
        }
        else {
            var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result.isTrue) {
                    _this.closeCallbackHandler.next(true);
                }
            });
        }
    };
    TaskCreationComponent.prototype.updateTask = function (saveOption) {
        var _this = this;
        if (this.taskForm.getRawValue().deliverableApprover && this.taskForm.getRawValue().deliverableApprover.value) {
            var data = this.savedReviewers.find(function (reviewer) { return reviewer.value === _this.taskForm.getRawValue().deliverableApprover.value; });
            if (data) {
                var eventData = { message: 'Selected reviewer is already the approver', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                this.disableTaskSave = false;
                return;
            }
        }
        if (this.taskForm.pristine && !(this.chipChanged)) {
            var eventData = { message: 'Kindly make changes to update the task', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            return;
        }
        var nameSplit = this.taskForm.value.taskName.split('_');
        if (nameSplit[0].length === 0) {
            var eventData = { message: 'Name should not start with "_"', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        this.disableTaskSave = true;
        var taskEndDateWarning = false;
        if (taskEndDateWarning) {
            this.taskForm.patchValue({
                endDate: this.taskModalConfig.selectedTask.DUE_DATE
            });
            var eventData = { message: 'Task\'s end date should not be less than the deliverables\' due date', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
            return;
        }
        if (this.taskForm.status === 'VALID') {
            if (this.taskForm.controls.taskName.value.trim() === '') {
                var eventData = { message: 'Please provide a valid task name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                this.notificationHandler.next(eventData);
                this.disableTaskSave = false;
            }
            else {
                this.taskForm.controls.taskName.setValue(this.taskForm.controls.taskName.value.trim());
                this.disableTaskSave = true;
                this.updateTaskDetails(saveOption);
            }
        }
        else {
            var eventData = { message: 'Kindly fill all mandatory fields', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
            this.notificationHandler.next(eventData);
            this.disableTaskSave = false;
        }
    };
    TaskCreationComponent.prototype.formReviewerObj = function () {
        var _this = this;
        var addedReviewers = [];
        var removedReviewers = [];
        if (this.newReviewers && this.deliverableReviewers) {
            this.newReviewers.forEach(function (newReviewer) {
                if (_this.deliverableReviewers.length === 0) {
                    var addedReviewerIndex = addedReviewers.findIndex(function (reviewer) { return reviewer.value === newReviewer.value; });
                    if (addedReviewerIndex === -1) {
                        addedReviewers.push(newReviewer);
                    }
                }
                else {
                    var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (deliverableReviewer) { return deliverableReviewer.value === newReviewer.value; });
                    if (deliverableReviewerIndex === -1) {
                        var addedReviewerIndex = addedReviewers.findIndex(function (reviewer) { return reviewer.value === newReviewer.value; });
                        if (addedReviewerIndex === -1) {
                            addedReviewers.push(newReviewer);
                        }
                    }
                }
            });
            this.newreviewersObj = addedReviewers.map(function (addedReviewer) {
                return {
                    DeliverableId: '',
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: addedReviewer.value,
                        action: 'ADD'
                    },
                    TaskId: _this.taskConfig.taskId,
                    IsPMAssignedReviewer: _this.isPM
                };
            });
        }
        if (this.removedReviewers && this.savedReviewers) {
            this.removedReviewers.forEach(function (removedReviewer) {
                if (_this.savedReviewers.length === 0) {
                    var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (deliverableReviewer) { return deliverableReviewer.value === removedReviewer.value; });
                    if (deliverableReviewerIndex >= 0) {
                        var reviewerIndex = removedReviewers.findIndex(function (reviewer) { return reviewer.value === removedReviewer.value; });
                        if (reviewerIndex === -1) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
                else {
                    var savedReviewerIndex = _this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === removedReviewer.value; });
                    if (savedReviewerIndex === -1) {
                        var removedReviewerIndex = removedReviewers.findIndex(function (reviewer) { return reviewer.value === removedReviewer.value; });
                        var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (data) { return data.value === removedReviewer.value; });
                        if (removedReviewerIndex === -1 && deliverableReviewerIndex >= 0) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
            });
            this.removedreviewersObj = removedReviewers.map(function (reviewer) {
                return {
                    DeliverableId: '',
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: reviewer.value,
                        action: 'REMOVE'
                    },
                    TaskId: _this.taskConfig.taskId,
                    IsPMAssignedReviewer: _this.isPM
                };
            });
        }
        this.selectedReviewersObj = this.savedReviewers.map(function (reviewer) {
            return {
                Id: '',
                ItemId: '',
                userID: reviewer.value
            };
        });
        this.reviewersObj = this.newreviewersObj ? this.newreviewersObj.concat(this.removedreviewersObj) : this.removedreviewersObj.concat(this.newreviewersObj);
    };
    TaskCreationComponent.prototype.updateTaskDetails = function (saveOption) {
        var taskFormValues = this.taskForm.getRawValue();
        this.formReviewerObj();
        var TaskObject = this.projectUtilService.createNewTaskUpdateObject(taskFormValues, this.taskConfig.taskType, this.taskConfig.isApprovalTask, this.taskModalConfig.projectId, this.selectedReviewersObj);
        if (this.taskModalConfig.selectedTask && this.taskModalConfig.selectedTask['Task-id']
            && this.taskModalConfig.selectedTask['Task-id'].Id) {
            TaskObject.TaskId.Id = this.taskModalConfig.selectedTask['Task-id'].Id;
            TaskObject.RevisionReviewRequired = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED)
                ? '' : this.taskModalConfig.selectedTask.REVISION_REVIEW_REQUIRED;
            TaskObject.IsMilestone = this.utilService.isNullOrEmpty(this.taskModalConfig.selectedTask.IS_MILESTONE)
                ? '' : this.taskModalConfig.selectedTask.IS_MILESTONE;
            TaskObject.IsMilestone = this.utilService.isNullOrEmpty(taskFormValues.isMilestone)
                ? '' : taskFormValues.isMilestone;
        }
        this.saveTask(TaskObject, saveOption);
    };
    TaskCreationComponent.prototype.saveTask = function (TaskObject, saveOption) {
        var _this = this;
        this.loaderService.show();
        if (this.taskModalConfig.selectedTask) {
            TaskObject = this.projectUtilService.compareTwoProjectDetails(TaskObject, this.oldTaskObject);
            if (Object.keys(TaskObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
                TaskObject.BulkOperation = false;
                var requestObj = {
                    TaskObject: TaskObject,
                    DeliverableReviewers: {
                        DeliverableReviewer: this.reviewersObj
                    }
                };
                this.taskService.updateTask(requestObj).subscribe(function (response) {
                    _this.loaderService.hide();
                    _this.disableTaskSave = false;
                    if (response && response.APIResponse && response.APIResponse.statusCode === '200') {
                        var eventData = { message: 'Task updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        _this.notificationHandler.next(eventData);
                        _this.saveCallBackHandler.next({
                            taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: _this.taskModalConfig.projectId,
                            isTemplate: _this.taskModalConfig.isTemplate, saveOption: saveOption
                        });
                    }
                    else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                        var eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        _this.notificationHandler.next(eventData);
                    }
                    else {
                        var eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        _this.notificationHandler.next(eventData);
                    }
                }, function (error) {
                    var eventData = { message: 'Something went wrong while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                });
            }
            else {
                this.loaderService.hide();
                this.disableTaskSave = false;
            }
        }
        else {
            var requestObj = {
                TaskObject: TaskObject
            };
            this.taskService.createTask(requestObj)
                .subscribe(function (response) {
                _this.loaderService.hide();
                _this.disableTaskSave = false;
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                    && response.APIResponse.data.Task && response.APIResponse.data.Task['Task-id'] && response.APIResponse.data.Task['Task-id'].Id) {
                    var eventData = { message: 'Task saved successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.notificationHandler.next(eventData);
                    _this.disableTaskSave = false;
                    _this.saveCallBackHandler.next({
                        taskId: response.APIResponse.data.Task['Task-id'].Id, projectId: _this.taskModalConfig.projectId,
                        isTemplate: _this.taskModalConfig.isTemplate, saveOption: saveOption
                    });
                }
                else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                    && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                    var eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                }
                else {
                    var eventData = { message: 'Something went wrong on while creating task', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.notificationHandler.next(eventData);
                }
            }, function () {
                _this.loaderService.hide();
                var eventData = { message: 'Something went wrong on while updating task', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                _this.disableTaskSave = false;
            });
        }
    };
    TaskCreationComponent.prototype.initialiseTaskConfig = function () {
        this.taskModalConfig = {
            hasAllConfig: false,
            taskStatusList: null,
            taskInitialStatusList: null,
            taskPriorityList: null,
            predecessor: null,
            taskList: this.taskConfig.taskList,
            selectedProject: this.taskConfig.selectedProject,
            defaultStatus: null,
            defaultPriority: null,
            selectedStatusItemId: null,
            minDate: '',
            maxDate: '',
            isNewTask: (this.taskConfig.taskId) ? false : true,
            taskId: this.taskConfig.taskId,
            selectedTask: this.taskConfig.selectedTask,
            selectedpredecessor: null,
            isTemplate: this.taskConfig.isTemplate,
            projectId: this.taskConfig.projectId,
            taskOwnerConfig: {
                label: 'Task Owner',
                filterOptions: [],
                formControl: null,
            },
            taskRoleConfig: {
                label: 'Role',
                filterOptions: [],
                formControl: null,
            },
            predecessorConfig: {
                label: 'Predecessor',
                filterOptions: [],
                formControl: null
            },
            deliverableApprover: {
                label: 'Deliverable Approver',
                filterOptions: [],
                formControl: null
            },
            deliverableReviewer: {
                label: 'Deliverable Reviewer',
                filterOptions: [],
                formControl: null
            },
            deliveryPackageConfig: {
                label: 'Delivery Package',
                filterOptions: [],
                formControl: null
            },
            taskAssignmentTypes: null,
        };
    };
    TaskCreationComponent.prototype.ngOnInit = function () {
        var _this = this;
        var projectConfig = this.sharingService.getProjectConfig();
        this.enableDuration = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_DURATION]);
        this.enableWorkWeek = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.showRevisionReviewRequired = this.utilService.getBooleanValue(projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.SHOW_REVISION_REVIEW_REQUIRED]);
        this.projectConfig = this.sharingService.getProjectConfig();
        this.appConfig = this.sharingService.getAppConfig();
        this.defaultRevisionRequiredStatus = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.DEFAULT_REVISION_REQUIRED_STATUS]);
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        this.allowActionTask = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.ALLOW_ACTION_TASK]);
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach(function (user) {
            if (user.Name === 'MPM Project Manager') {
                _this.isPM = true;
            }
        });
        // const statuses = acronui.findObjectsByProp(response, 'Comments');
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        if (this.taskConfig && this.taskConfig.projectId) {
            this.initialiseTaskConfig();
            this.getPreDetails();
        }
    };
    TaskCreationComponent.ctorParameters = function () { return [
        { type: AppService },
        { type: SharingService },
        { type: DateAdapter },
        { type: UtilService },
        { type: TaskService },
        { type: EntityAppDefService },
        { type: LoaderService },
        { type: MatDialog },
        { type: ProjectUtilService },
        { type: CommentsUtilService },
        { type: NotificationService }
    ]; };
    __decorate([
        Input()
    ], TaskCreationComponent.prototype, "taskConfig", void 0);
    __decorate([
        Output()
    ], TaskCreationComponent.prototype, "closeCallbackHandler", void 0);
    __decorate([
        Output()
    ], TaskCreationComponent.prototype, "saveCallBackHandler", void 0);
    __decorate([
        Output()
    ], TaskCreationComponent.prototype, "notificationHandler", void 0);
    __decorate([
        ViewChild('reviewerInput')
    ], TaskCreationComponent.prototype, "reviewerInput", void 0);
    __decorate([
        ViewChild('auto')
    ], TaskCreationComponent.prototype, "matAutocomplete", void 0);
    TaskCreationComponent = __decorate([
        Component({
            selector: 'mpm-task-creation',
            template: "<div class=\"overview-content\">\r\n    <form class=\"task-creation-form\" *ngIf=\"taskForm\" [formGroup]=\"taskForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Task Name</mat-label>\r\n                    <input [appAutoFocus]=\"taskModalConfig && taskModalConfig.hasAllConfig\" matInput\r\n                        placeholder=\"Task Name\" formControlName=\"taskName\" required>\r\n                    <mat-hint align=\"end\"\r\n                        *ngIf=\"taskForm.get('taskName').errors && taskForm.get('taskName').errors.maxlength\">\r\n                        {{taskForm.get('taskName').errors.maxlength.actualLength}}/{{taskForm.get('taskName').errors.maxlength.requiredLength}}\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\"\r\n                *ngIf=\"!(taskConfig.taskType == taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE) && (taskConfig.isApprovalTask) && showRevisionReviewRequired\">\r\n                <div class=\"approval-section-holder\">\r\n\r\n                    <section>\r\n                        <mat-checkbox color=\"primary\" formControlName=\"revisionReviewRequired\"><span>Review Required For\r\n                                Revision</span>\r\n                        </mat-checkbox>\r\n                    </section>\r\n                </div>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Allocation</mat-label>\r\n                    <mat-select formControlName=\"allocation\" placeholder=\"Allocation\" [required]=\"true\"\r\n                        name=\"allocation\">\r\n                        <mat-option *ngFor=\"let allocation of taskModalConfig.taskAssignmentTypes\"\r\n                            value=\"{{allocation.NAME}}\">\r\n                            {{allocation.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE\"\r\n                [searchFieldConfig]=\"taskModalConfig.taskRoleConfig\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"!taskConfig.isApprovalTask && (taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE || taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER)\"\r\n                [searchFieldConfig]=\"taskModalConfig.taskOwnerConfig\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\"\r\n                *ngIf=\"taskConfig.isApprovalTask && (taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_ROLE || taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER)\"\r\n                [searchFieldConfig]=\"taskModalConfig.deliverableApprover\"\r\n                [required]=\"taskForm.getRawValue().allocation === taskConstants.ALLOCATION_TYPE_USER\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\"flex-row-item\" *ngIf=\"!taskConfig.isApprovalTask && this.taskModalConfig.predecessorConfig.filterOptions.length > 0\"\r\n                [searchFieldConfig]=\"taskModalConfig.predecessorConfig\" [required]=\"false\">\r\n            </mpm-custom-search-field>\r\n            <mpm-custom-search-field class=\" flex-row-item\" *ngIf=\"taskConfig.isApprovalTask\"\r\n                [searchFieldConfig]=\"taskModalConfig.deliveryPackageConfig\" [required]=\"true\">\r\n            </mpm-custom-search-field>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Status</mat-label>\r\n                    <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"status\" required>\r\n                        <mat-option *ngFor=\"let status of taskStatus\" value=\"{{status['MPM_Status-id'].Id}}\">\r\n                            {{status.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Priority</mat-label>\r\n                    <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" required>\r\n                        <mat-option *ngFor=\"let priority of taskModalConfig.taskPriorityList\"\r\n                            value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                            {{priority.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"task-owner-task-count\" *ngIf=\"ownerProjectCount\">\r\n            <mat-error *ngIf=\"!isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-error>\r\n            <mat-label *ngIf=\"isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-label>\r\n        </div>\r\n        <div class=\"flex-row\" *ngIf=\"!taskModalConfig.isTemplate\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Task Start Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"startDate\" placeholder=\"Task Start Date (MM/DD/YYYY)\"\r\n                        formControlName=\"startDate\" [min]=\"taskModalConfig.minDate\"\r\n                        [max]=\"dateValidation && dateValidation.startMaxDate ? dateValidation.startMaxDate : taskModalConfig.maxDate\"\r\n                        required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #startDate\r\n                        [disabled]=\"selectedStatusInfo && selectedStatusInfo.STATUS_TYPE !== taskConstants.STATUS_TYPE_INITIAL\">\r\n                    </mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"(taskForm.get('startDate').hasError('matDatepickerMax') || taskForm.get('startDate').hasError('matDatepickerMin'))\">\r\n                        {{dateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\" *ngIf=\"!taskModalConfig.isTemplate\">\r\n                    <mat-label>Task End Date</mat-label>\r\n                    <input matInput [matDatepicker]=\"endDate\"\r\n                        [min]=\"(dateValidation && dateValidation.endMinDate) ? dateValidation.endMinDate : (taskForm.getRawValue().startDate ? taskForm.getRawValue().startDate : taskModalConfig.minDate)\"\r\n                        [max]=\"taskModalConfig.maxDate\" placeholder=\"Task End Date (MM/DD/YYYY)\"\r\n                        formControlName=\"endDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #endDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"taskForm.get('endDate') && (taskForm.get('endDate').hasError('matDatepickerMax') || taskForm.get('endDate').hasError('matDatepickerMin'))\">\r\n                        {{dateErrorMessage}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Description</mat-label>\r\n                    <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\r\n                    <mat-hint align=\"end\"\r\n                        *ngIf=\"taskForm.get('description').errors && taskForm.get('description').errors.maxlength\">\r\n                        {{taskForm.get('description').errors.maxlength.actualLength}}/{{taskForm.get('description').errors.maxlength.requiredLength}}\r\n                    </mat-hint>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item\">\r\n                    <span class=\"form-actions\">\r\n                        <button type=\"button\"  mat-stroked-button (click)=\"cancelTaskCreation()\">Cancel</button>\r\n                        <button id=\"task-save-btn\" color='primary' mat-flat-button\r\n                            (click)=\"updateTask(SELECTED_SAVE_OPTION)\" type=\"submit\" class=\"ang-btn\"\r\n                            matTooltip=\"{{SELECTED_SAVE_OPTION.name}}\"\r\n                            [disabled]=\"(taskForm.pristine || taskForm.invalid)\">{{SELECTED_SAVE_OPTION.name}}</button>\r\n                        <span *ngIf=\"!taskConfig.taskId\">\r\n                            <button id=\"task-save-option\" mat-flat-button color=\"primary\"\r\n                                [matMenuTriggerFor]=\"saveOptionsMenu\"\r\n                                [disabled]=\"taskForm.pristine || taskForm.invalid\">\r\n                                <mat-icon>arrow_drop_down</mat-icon>\r\n                            </button>\r\n                            <mat-menu #saveOptionsMenu=\"matMenu\">\r\n                                <span *ngFor=\"let saveOption of saveOptions\">\r\n                                    <button *ngIf=\"saveOption.value != SELECTED_SAVE_OPTION.value\" mat-menu-item\r\n                                        (click)=\"updateTask(saveOption)\">\r\n                                        <span>{{saveOption.name}}</span>\r\n                                    </button>\r\n                                </span>\r\n                            </mat-menu>\r\n                        </span>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>",
            styles: [".overview-content{padding:0 20px}.flex-row-item{margin:.666667em 15px 0;font-size:14px}.flex-row-item div.approval-section-holder{padding-bottom:1.34375em}.flex-row-item section.approval-task-check{margin-right:90px}.project-overview-form{display:flex;flex-direction:column;flex-wrap:wrap;justify-content:center;padding:10px 50px;width:50vw}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-actions button{margin:10px}.task-owner-task-count{margin-left:16px;margin-bottom:16px;font-size:14px;margin-top:-16px}.task-owner-task-count mat-label{color:green}#task-save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}#task-save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:15px}.form-actions{margin-left:auto}mat-form-field{width:100%}.form-group-panel{background:0 0}.form-group-panel ::ng-deep mpm-custom-metadata-field{width:100%}.form-group-panel ::ng-deep .mat-expansion-panel-header{padding:0 4px}.form-group-panel ::ng-deep .mat-expansion-panel-body{padding:0 16px 16px 0}"]
        })
    ], TaskCreationComponent);
    return TaskCreationComponent;
}());
export { TaskCreationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1jcmVhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL3Rhc2stY3JlYXRpb24vdGFzay1jcmVhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFHckQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sS0FBSyxPQUFPLE1BQU0saUNBQWlDLENBQUM7QUFDM0QsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUN4RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw0RUFBNEUsQ0FBQztBQUN4SCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN0RixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUVoRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUNwRyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBb0IsTUFBTSx1QkFBdUIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBS3RFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBT2pGO0lBaUZJLCtCQUNXLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLE9BQXlCLEVBQ3pCLFdBQXdCLEVBQ3hCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxhQUE0QixFQUM1QixNQUFpQixFQUNqQixrQkFBc0MsRUFDdEMsbUJBQXdDLEVBQ3hDLG1CQUF3QztRQVhuRCxpQkFlQztRQWRVLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLFlBQU8sR0FBUCxPQUFPLENBQWtCO1FBQ3pCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQXpGekMseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMvQyx3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFVeEQsMkJBQXNCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMxQiw2QkFBd0IsR0FBRyxFQUFFLENBQUM7UUFDOUIsNEJBQXVCLEdBQUcsS0FBSyxDQUFDO1FBRWhDLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLDRCQUF1QixHQUFHLElBQUksQ0FBQztRQUMvQix3Q0FBd0M7UUFDeEMsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFpQjlCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLGtCQUFrQjtRQUNsQix5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsd0JBQXdCO1FBQ3hCLHVCQUFrQixHQUFHLEVBQUUsQ0FBQztRQU14QixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsdUJBQWtCLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDcEMsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBR3RCLFNBQUksR0FBRyxLQUFLLENBQUM7UUFFYixnQkFBVyxHQUFHLGtCQUFrQixDQUFDLGVBQWUsQ0FBQztRQUNqRCx5QkFBb0IsR0FBRztZQUNuQixJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxNQUFNO1NBQ2hCLENBQUM7UUFDRixpQkFBWSxHQUFHO1lBQ1gsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUU7WUFDcEMsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUU7U0FDekMsQ0FBQztRQUNGLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBR3ZCLG1CQUFjLEdBQUc7WUFDYixNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQztTQUN2QyxDQUFDO1FBb2xERixlQUFVLEdBQ04sVUFBQyxJQUFpQjtZQUNkLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsT0FBTyxJQUFJLENBQUM7YUFDZjtpQkFBTTtnQkFDSCxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQzFCLE9BQU8sR0FBRyxLQUFLLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUM5QixnQkFBZ0I7Z0JBQ2hCLGtCQUFrQjthQUNyQjtRQUNMLENBQUMsQ0FBQTtJQTNrREwsQ0FBQztJQUVELCtDQUFlLEdBQWYsVUFBZ0IsS0FBVTtRQUN0QixJQUFJLEtBQUssSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLElBQUksS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUMzRCxJQUFNLGFBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7WUFFeEMsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLGFBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkQsQ0FBdUQsQ0FBQyxDQUFDO1NBQ3hHO0lBQ0wsQ0FBQztJQUVELGtEQUFrQixHQUFsQixVQUFtQixLQUFLLEVBQUUsUUFBUTtRQUM5QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3hGLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUMxQjtJQUNMLENBQUM7SUFFRCx3Q0FBUSxHQUFSLFVBQVMsS0FBbUM7UUFDeEMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUExQyxDQUEwQyxDQUFDLENBQUM7UUFDakgsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUF6QyxDQUF5QyxDQUFDLENBQUM7UUFFbkcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxtQkFBbUI7WUFDdkMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFO1lBQ3RFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRywwQkFBMEIsQ0FBQyxDQUFDO1NBQ3JGO2FBQU0sSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLHNCQUFzQixDQUFDLENBQUM7U0FDakY7YUFBTTtZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO2dCQUNyQixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO2FBQ3BDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO2dCQUNuQixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO2FBQ3BDLENBQUMsQ0FBQztZQUNILElBQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFBLGVBQWUsSUFBSSxPQUFBLGVBQWUsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQTVDLENBQTRDLENBQUMsQ0FBQztZQUM3SCxJQUFJLG1CQUFtQixJQUFJLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO2dCQUNyQixXQUFXLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQ2pDLFdBQVcsRUFBRSxLQUFLO2FBQ3JCLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzlEO0lBQ0wsQ0FBQztJQUVELDJDQUFXLEdBQVgsVUFBWSxLQUF3QjtRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7WUFDOUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUUxQixJQUFJLEtBQUssRUFBRTtnQkFDUCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzthQUNwQjtZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5RDtJQUNMLENBQUM7SUFFRCxnREFBZ0IsR0FBaEIsVUFBaUIsYUFBa0I7UUFDL0IsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBckMsQ0FBcUMsQ0FBQyxDQUFDO1FBQ3BHLElBQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXLElBQUksT0FBQSxXQUFXLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBbkMsQ0FBbUMsQ0FBQyxDQUFDO1FBQ3pHLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO1FBQzlHLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsWUFBWSxDQUFDLEtBQUssS0FBSyxhQUFhLEVBQXBDLENBQW9DLENBQUMsQ0FBQztRQUM5RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztZQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7WUFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO1NBQ3BDLENBQUMsQ0FBQztRQUVILElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxhQUFhLElBQUksQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxnQkFBZ0IsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLFVBQVU7UUFBNUIsaUJBbUJDO1FBbEJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUN2RSxJQUFJLFVBQVUsQ0FBQyxhQUFhLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO29CQUN6RSxLQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNwRjtnQkFDRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLElBQUksVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDOUQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ3pFO2dCQUNELEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztnQkFDbEQsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNqRSxJQUFJLGFBQWEsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWU7b0JBQ3JELGFBQWEsS0FBSyxLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQzdGLEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztpQkFDOUM7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQWEsR0FBYjtRQUFBLGlCQXVDQztRQXRDRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQjs7Ozs7Ozs7Ozs7Ozs7Ozs7dUJBaUJXO1lBQ1gsSUFBSSxRQUFRLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDdEUsbUNBQW1DO1lBQ25DLElBQUksUUFBUSxFQUFFO2dCQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMxQixRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDekI7Z0JBQ0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUM7YUFDcEQ7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7YUFDOUM7WUFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNwQjs7Ozs7bUJBS087UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQUEsaUJBcUJDO1FBcEJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2lCQUM3RCxTQUFTLENBQUMsVUFBQSxjQUFjO2dCQUNyQixJQUFJLGNBQWMsRUFBRTtvQkFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7d0JBQ2hDLGNBQWMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO3FCQUNyQztvQkFDRCxLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7aUJBQ3hEO3FCQUFNO29CQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztpQkFDNUM7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRTtnQkFDQyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnREFBZ0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnREFBZ0IsR0FBaEI7UUFBQSxpQkFxQkM7UUFwQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUM7aUJBQ25FLFNBQVMsQ0FBQyxVQUFBLGNBQWM7Z0JBQ3JCLElBQUksY0FBYyxFQUFFO29CQUNoQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTt3QkFDaEMsY0FBYyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7cUJBQ3JDO29CQUNELEtBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLEdBQUcsY0FBYyxDQUFDO2lCQUMvRDtxQkFBTTtvQkFDSCxLQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztpQkFDbkQ7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRTtnQkFDQyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnREFBZ0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwyQ0FBVyxHQUFYO1FBQUEsaUJBMkNDO1FBMUNHLElBQU0sZ0JBQWdCLEdBQUc7WUFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTTtZQUNuQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO1lBQzlDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYztTQUNoRCxDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQztpQkFDekMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixFQUFFO29CQUMxRyxRQUFRLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUNoRCxRQUFRLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQztxQkFDckU7b0JBQ0QsSUFBSSxRQUFRLFNBQUEsQ0FBQztvQkFDYixRQUFRLEdBQUcsUUFBUSxDQUFDLHFCQUFxQixDQUFDO29CQUMxQyxJQUFNLE9BQUssR0FBRyxFQUFFLENBQUM7b0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ3BELFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJOzRCQUNiLE9BQUssQ0FBQyxJQUFJLENBQUM7Z0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTztnQ0FDbkIsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUN0QixRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWE7NkJBQy9CLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFDRCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBSyxDQUFDO29CQUM5QixLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDO2lCQUM3RTtxQkFBTTtvQkFDSCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO2lCQUM5QjtnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFO2dCQUNDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdEQUFnRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtDQUFlLEdBQWYsVUFBZ0IsVUFBVTtRQUExQixpQkFvQkM7UUFuQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUM3QyxJQUFNLFVBQVUsR0FBRztvQkFDZixNQUFNLEVBQUUsVUFBVTtpQkFDckIsQ0FBQztnQkFDRixLQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ3BFLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDbkYsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDREQUE0QixHQUE1QixVQUE2QixNQUFNO1FBQW5DLGlCQStEQztRQTlERyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLDRCQUE0QixDQUFDLE1BQU0sQ0FBQztpQkFDL0MsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3hDLElBQU0sYUFBYSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDL0UsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDbkUsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7NEJBQzFCLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0NBQ2hGLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFlBQVk7b0NBQ3RGLElBQUksWUFBWSxFQUFFO3dDQUNkLElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsYUFBYSxJQUFJLE9BQUEsYUFBYSxDQUFDLEtBQUssS0FBSyxZQUFZLENBQUMsTUFBTSxFQUEzQyxDQUEyQyxDQUFDLENBQUM7d0NBQy9HLElBQUksQ0FBQyxlQUFlLEVBQUU7NENBQ2xCLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO2dEQUNyQixJQUFJLEVBQUUsWUFBWSxDQUFDLFFBQVE7Z0RBQzNCLEtBQUssRUFBRSxZQUFZLENBQUMsTUFBTTtnREFDMUIsV0FBVyxFQUFFLFlBQVksQ0FBQyxRQUFROzZDQUNyQyxDQUFDLENBQUM7NENBQ0gsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQztnREFDM0IsSUFBSSxFQUFFLFlBQVksQ0FBQyxRQUFRO2dEQUMzQixLQUFLLEVBQUUsWUFBWSxDQUFDLE1BQU07Z0RBQzFCLFdBQVcsRUFBRSxZQUFZLENBQUMsUUFBUTs2Q0FDckMsQ0FBQyxDQUFDO3lDQUNOO3FDQUNKO29DQUNELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGNBQWM7d0NBQzVHLElBQU0sbUJBQW1CLEdBQUcsY0FBYyxDQUFDLFlBQVksS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3dDQUNuRixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEtBQUssS0FBSyxZQUFZLENBQUMsTUFBTSxFQUFsQyxDQUFrQyxDQUFDLENBQUM7d0NBQ3BHLElBQUksYUFBYSxJQUFJLENBQUMsRUFBRTs0Q0FDcEIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQzt5Q0FDNUU7NkNBQU07NENBQ0gsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztnREFDekIsSUFBSSxFQUFFLFlBQVksQ0FBQyxRQUFRO2dEQUMzQixLQUFLLEVBQUUsWUFBWSxDQUFDLE1BQU07Z0RBQzFCLFdBQVcsRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDbEMsV0FBVyxFQUFFLG1CQUFtQjs2Q0FDbkMsQ0FBQyxDQUFDO3lDQUNOO29DQUNMLENBQUMsQ0FBQyxDQUFDO29DQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUNuQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0NBQ3hCLENBQUMsQ0FBQyxDQUFDOzZCQUNOO2lDQUFNO2dDQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7Z0NBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs2QkFDdkI7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtpQkFDSjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBRUwsQ0FBQyxFQUFFO2dCQUNDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdFQUFnRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFZLEdBQVo7UUFBQSxpQkFnREM7UUEvQ0csT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtnQkFDaEMsSUFBSSxzQkFBc0IsR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLHNCQUFzQixHQUFHO29CQUNyQixjQUFjLEVBQUUsYUFBYSxDQUFDLG9CQUFvQjtvQkFDbEQsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTTtvQkFDbkMsYUFBYSxFQUFFLElBQUk7b0JBQ25CLFlBQVksRUFBRSxLQUFLO29CQUNuQixRQUFRLEVBQUUsSUFBSTtpQkFDakIsQ0FBQztnQkFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsQ0FBQztxQkFDbEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO3dCQUNuRCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3dCQUM3RCxJQUFNLE9BQUssR0FBRyxFQUFFLENBQUM7d0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3BELFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dDQUNqQixJQUFNLFFBQVEsR0FBRyxPQUFLLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7Z0NBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7b0NBQ1gsT0FBSyxDQUFDLElBQUksQ0FBQzt3Q0FDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7d0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO3dDQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtxQ0FDekIsQ0FBQyxDQUFDO2lDQUNOOzRCQUNMLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUVELEtBQUksQ0FBQyxZQUFZLEdBQUcsT0FBSyxDQUFDO3FCQUM3Qjt5QkFBTTt3QkFDSCxLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztxQkFDMUI7b0JBQ0QsNkJBQTZCO29CQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRTtvQkFDQyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxvREFBb0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNySSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsOENBQWMsR0FBZDtRQUFBLGlCQW1FQztRQWxFRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksb0JBQW9CLEdBQUcsRUFBRSxDQUFDO1lBQzlCLElBQUksS0FBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNsRCxvQkFBb0IsR0FBRztvQkFDbkIsY0FBYyxFQUFFLGFBQWEsQ0FBQyxVQUFVO29CQUN4QyxNQUFNLEVBQUUsYUFBYSxDQUFDLFVBQVUsS0FBSyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN2RyxNQUFNLEVBQUUsYUFBYSxDQUFDLFVBQVUsS0FBSyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUMxRyxhQUFhLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO29CQUM3QyxZQUFZLEVBQUUsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWM7aUJBQ2hELENBQUM7YUFDTDtpQkFBTSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUMxQyxJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxjQUFjLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDO29CQUMzSSxDQUFDLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzNGLElBQU0sSUFBSSxHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLG9CQUFvQixHQUFHO29CQUNuQixjQUFjLEVBQUUsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsZUFBZTtvQkFDakUsTUFBTSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGVBQWUsS0FBSyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDaEksTUFBTSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGVBQWUsS0FBSyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNuSSxhQUFhLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjO29CQUM3QyxZQUFZLEVBQUUsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWM7aUJBQ2hELENBQUM7YUFDTDtpQkFBTTtnQkFDSCxvQkFBb0IsR0FBRztvQkFDbkIsY0FBYyxFQUFFLGFBQWEsQ0FBQyxvQkFBb0I7b0JBQ2xELE1BQU0sRUFBRSxFQUFFO29CQUNWLE1BQU0sRUFBRSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU07b0JBQ25DLGFBQWEsRUFBRSxLQUFLO29CQUNwQixZQUFZLEVBQUUsSUFBSTtpQkFDckIsQ0FBQzthQUNMO1lBRUQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2hELFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtvQkFDbkQsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDN0QsSUFBTSxPQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNwRCxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTs0QkFDYixJQUFNLFFBQVEsR0FBRyxPQUFLLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7NEJBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7Z0NBQ1gsT0FBSyxDQUFDLElBQUksQ0FBQztvQ0FDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO29DQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtpQ0FDekIsQ0FBQyxDQUFDOzZCQUNOO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxPQUFLLENBQUM7b0JBQ2pDLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUM7b0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQztpQkFDckY7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztpQkFDakM7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRTtnQkFDQyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxzREFBc0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUN2SSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBa0IsR0FBbEIsVUFBbUIsVUFBa0I7UUFBckMsaUJBNkJDO1FBNUJHLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDcEMsT0FBTyxJQUFJLFVBQVUsQ0FDakIsVUFBQSxRQUFRO1lBQ0osSUFBTSxNQUFNLEdBQUc7Z0JBQ1gsY0FBYyxFQUFFO29CQUNaLEVBQUUsRUFBRSxVQUFVO2lCQUNqQjthQUNKLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxvREFBb0QsRUFBRSxlQUFlLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUNsSCxVQUFBLHNCQUFzQjtnQkFDbEIsSUFBTSxlQUFlLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQixFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUN2RixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLElBQUksZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzlELEtBQUksQ0FBQyx1QkFBdUIsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3JEO3FCQUFNO29CQUNILElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzdHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQzVDO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDdEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFDRCxVQUFBLElBQUk7Z0JBQ0EsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsK0NBQStDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDaEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QixDQUFDLENBQ0osQ0FBQztRQUNOLENBQUMsQ0FDSixDQUFDO0lBQ04sQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztTQTZCSztJQUVMLGlEQUFpQixHQUFqQixVQUFrQixjQUF1QjtRQUF6QyxpQkF5REM7UUF4REcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxpQkFBaUIsR0FBRztnQkFDdEIsU0FBUyxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUztnQkFDcEMsY0FBYyxFQUFFLGNBQWM7Z0JBQzlCLGVBQWUsRUFBRSxLQUFJLENBQUMsZUFBZTthQUN4QyxDQUFDO1lBQ0YsS0FBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDL0MsU0FBUyxDQUFDLFVBQUEscUJBQXFCO2dCQUM1QixJQUFJLHFCQUFxQixFQUFFO29CQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO3dCQUN2QyxxQkFBcUIsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7cUJBQ25EO29CQUNELEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLHFCQUFxQixDQUFDO29CQUN0RCxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO3dCQUNuQyw0Q0FBNEM7d0JBQzVDLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUk7NEJBQ3JFLE9BQU8sSUFBSSxLQUFLLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDO3dCQUN0RCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFDRCxJQUFNLHVCQUFxQixHQUFHLEVBQUUsQ0FBQztvQkFDakMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTt3QkFDbEMsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFOzRCQUNuRixLQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7eUJBQzVDO3dCQUNELElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDOUMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFOzRCQUNwRCx1QkFBcUIsQ0FBQyxJQUFJLENBQUM7Z0NBQ3ZCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQ0FDZixLQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pCLFdBQVcsRUFBRSxLQUFLOzZCQUNyQixDQUFDLENBQUM7eUJBQ047b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEdBQUcsdUJBQXFCLENBQUM7b0JBQ2pGLEtBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLHVCQUFxQixDQUFDO2lCQUVoRjtxQkFBTTtvQkFDSCxJQUFJLEtBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ2YsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDbEQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFOzRCQUNoQyxLQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7eUJBQ2pFOzZCQUFNOzRCQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQzt5QkFDN0Q7cUJBQ0o7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxLQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7d0JBQzlELEtBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztxQkFDN0Q7aUJBQ0o7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQWEsR0FBYjtRQUFBLGlCQXFEQztRQXBERyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7YUFDN0MsU0FBUyxDQUFDO1lBQ1AsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDO2lCQUNqRCxTQUFTLENBQUM7Z0JBQ1AsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRTtvQkFDaEUsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQzt3QkFDekIsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLGFBQWEsRUFBRSxFQUFFLEtBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxLQUFJLENBQUMsY0FBYyxFQUFFLEVBQUUsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7NkJBQ2xILFNBQVMsQ0FBQzs0QkFDUCxLQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixHQUFHLGFBQWEsQ0FBQyxlQUFlLENBQUM7NEJBQ3pFLEtBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxjQUFjLENBQUM7NEJBQzlELElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUU7Z0NBQ2hDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDOzZCQUM3QjtpQ0FBTTtnQ0FDSCxJQUFNLE9BQU8sR0FBRyxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGFBQWE7dUNBQzlGLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0NBQ2hKLFFBQVEsQ0FBQztvQ0FDTCxLQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO29DQUMxRCxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztvQ0FDNUIsS0FBSSxDQUFDLDRCQUE0QixDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO2lDQUM1RCxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsUUFBUTtvQ0FDbEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQ0FDMUIsS0FBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBQ2xDLGFBQWEsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTzt3Q0FDeEMsSUFBSSxLQUFJLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRTs0Q0FDMUssS0FBSSxDQUFDLHVCQUF1QixHQUFHLE9BQU8sQ0FBQzt5Q0FDMUM7b0NBQ0wsQ0FBQyxDQUFDLENBQUM7b0NBQ0gsYUFBYSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO3dDQUN6QyxJQUFJLEtBQUksQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsZUFBZSxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFOzRDQUN0TCxLQUFJLENBQUMsdUJBQXVCLEdBQUcsT0FBTyxDQUFDO3lDQUMxQztvQ0FDTCxDQUFDLENBQUMsQ0FBQztvQ0FDSCxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQ0FDOUIsQ0FBQyxFQUFFO29DQUNDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzlCLENBQUMsQ0FBQyxDQUFDOzZCQUNOO3dCQUNMLENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsQ0FBQyxDQUFDO2lCQUVOO1lBQ0wsQ0FBQyxFQUFFO2dCQUNDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDJDQUEyQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQzVILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLEVBQUU7WUFDQyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHFEQUFxRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCw2Q0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLElBQU0sU0FBUyxHQUFHO1lBQ2Qsc0JBQXNCLEVBQUUsS0FBSztZQUM3QixpQkFBaUIsRUFBRSxLQUFLO1lBQ3hCLHVCQUF1QixFQUFFLEtBQUs7WUFDOUIsZUFBZSxFQUFFLEtBQUs7WUFDdEIseUJBQXlCLEVBQUUsS0FBSztZQUNoQyxtQkFBbUIsRUFBRSxLQUFLO1NBQzdCLENBQUM7UUFFRixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDdEQsU0FBUyxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztnQkFDMUMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztnQkFDcEMsU0FBUyxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQ2xDLFNBQVMsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7Z0JBQ3JDLFNBQVMsQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7Z0JBQzVDLFNBQVMsQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7YUFDM0M7U0FDSjthQUFNO1lBQ0gsU0FBUyxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUN6QyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLFNBQVMsQ0FBQyx1QkFBdUIsR0FBRyxLQUFLLENBQUM7WUFDMUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztZQUN0QyxTQUFTLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUNqQyxTQUFTLENBQUMseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1NBQy9DO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELGtEQUFrQixHQUFsQixVQUFtQixhQUFhO1FBQzVCLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ2xDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxhQUFhLEVBQUU7Z0JBQ3RDLGVBQWUsR0FBRyxJQUFJLENBQUM7YUFDMUI7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFFRCxrREFBa0IsR0FBbEI7UUFBQSxpQkF1WkM7UUF0WkcsSUFBTSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQztRQUMvRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUM7UUFDN0UsSUFBTSxPQUFPLEdBQUcsZUFBZSxDQUFDLG1CQUFtQixDQUFDO1FBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUM7Z0JBQzdELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxTQUFTLENBQUM7b0JBQzFCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDOUosUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pKLFlBQVksRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQztvQkFDdEgsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNwQixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQzs0QkFDOUYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSTtxQkFDOUosQ0FDQTtvQkFDRCxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDN0QsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzVELFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7NEJBQ3BGLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNoSyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixTQUFTLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ2pGLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDckIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQztxQkFDeEcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2pHLHNCQUFzQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3BKLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUN0RSxpQkFBaUIsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUN6RSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDbkUsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDM0Usb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDNUUsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7aUJBQ3JFLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDMUM7aUJBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUM5SywyRkFBMkY7Z0JBQzNGLHlDQUF5QztnQkFDekMsd0JBQXdCO2dCQUN4QixvQ0FBb0M7Z0JBQ3BDLElBQUk7Z0JBQ0osSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7Z0JBQy9DLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNDLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3BKLDJIQUEySDtnQkFDM0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2pILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxTQUFTLENBQUM7b0JBQzFCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFDbEUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNqRyxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxPQUFPLElBQUksQ0FBQyxhQUFhLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUN2RSxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO3FCQUNuRCxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakUsWUFBWSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUMxQixLQUFLLEVBQUUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSzt3QkFDekcsUUFBUSxFQUFFLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztxQkFDbkQsQ0FBQztvQkFDRixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUN4RyxRQUFRLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUI7cUJBQ3RILENBQUM7b0JBQ0YsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNuQixLQUFLLEVBQUUsU0FBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhO3dCQUNqRSxRQUFRLEVBQUUsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDeEksQ0FBQztvQkFDRixJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2xCLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZOzRCQUN6RCxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7cUJBQ2xILENBQUM7b0JBQ0YsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUNwSCxRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsU0FBUyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsUUFBUSxFQUFFLENBQUMsWUFBWTs0QkFDOUgsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLENBQUM7cUJBQ25ILEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDMUgsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDdkssc0JBQXNCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO29CQUNuSSxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDcEgsaUJBQWlCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDbEcsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUNwRSwySkFBMko7b0JBQzNKLG1CQUFtQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNqQyxLQUFLLEVBQUUsU0FBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUNqRSxRQUFRLEVBQUUsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDeEksQ0FBQztvQkFDRixvQkFBb0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3JFLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDeEIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLEVBQUUsUUFBUSxFQUFFLFlBQVk7NEJBQy9DLENBQUMsSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQztxQkFDbEgsQ0FBQztpQkFDTCxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQzdDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDMUMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUMzSixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRyxhQUFhLENBQUM7aUJBQ2hEO2dCQUNELElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO2dCQUNuQywwQkFBMEI7Z0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDdkUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQ2pFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNqSixJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDMUM7U0FDSjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDO2dCQUM3RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksU0FBUyxDQUFDO29CQUMxQixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQzlKLE1BQU07b0JBQ047O3dCQUVJO29CQUNKLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMxTSxZQUFZLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUM7b0JBQ3ZLLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO3dCQUMvRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUM1SSxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDN0QsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzVELFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7NEJBQ3BGLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNoSyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDakcsc0JBQXNCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDcEosV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3RFLGlCQUFpQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3pFLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUNuRSxtQkFBbUIsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUMzRSxvQkFBb0IsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxDQUFDO29CQUM1RSxVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztpQkFDckUsQ0FBQyxDQUFDO2FBQ047aUJBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3ZMLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUU5SyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQztnQkFDL0MsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0MsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDcEosSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLElBQUksT0FBTyxJQUFJLENBQUMsY0FBYyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ2pILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxTQUFTLENBQUM7b0JBQzFCLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFDbEUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNqRyxNQUFNO29CQUNOOzs7Ozs7OzBCQU9NO29CQUNOLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLE9BQU8sSUFBSSxDQUFDLGFBQWEsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3ZFLFFBQVEsRUFBRSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7cUJBQ3BHLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxZQUFZLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzFCLEtBQUssRUFBRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLO3dCQUN6RyxRQUFRLEVBQUUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO3FCQUNwRyxDQUFDO29CQUNGLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3hHLFFBQVEsRUFBRSxJQUFJO3FCQUNqQixDQUFDO29CQUNGLEtBQUssRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDbkIsS0FBSyxFQUFFLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYTt3QkFDakUsUUFBUSxFQUFFLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUM7cUJBQ3hJLENBQUM7b0JBQ0YsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNsQixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsRUFBRSxRQUFRLEVBQUUsWUFBWTs0QkFDekQsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUNsSCxDQUFDO29CQUNGLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3QkFDcEgsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3ZLLHNCQUFzQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQztvQkFDbkksV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ3BILGlCQUFpQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQ2xHLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDcEUsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2pDLEtBQUssRUFBRSxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ2pFLFFBQVEsRUFBRSxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUN4SSxDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDckUsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN4QixLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxRQUFRLEVBQUUsWUFBWTs0QkFDL0MsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDO3FCQUNsSCxDQUFDO2lCQUVMLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDN0MsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixFQUFFO29CQUMxQyxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQzNKLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQztpQkFDaEQ7Z0JBQ0QsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7Z0JBQ25DLDBCQUEwQjtnQkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FDakUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDcEo7U0FDSjtRQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQ2hGLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixHQUFHLENBQUMsVUFBQyxRQUF1QixJQUFLLE9BQUEsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxFQUFyRSxDQUFxRSxDQUFDLENBQUMsQ0FBQztRQUU3RyxJQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUN2RSwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDaEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUM5RSxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQztRQUNsRyxJQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQztRQUNsRyxJQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFDNUYsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxLQUFLLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtZQUNoRixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUNwRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDMUM7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUU7Z0JBQ2xGLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ3hEO1lBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUNsRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN4RDtZQUNEOzs7OztnQkFLSTtTQUNQO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXO1lBQ2pFLGlDQUFpQztZQUNqQyxJQUFJLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksT0FBTyxXQUFXLEtBQUssUUFBUSxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFO2dCQUMzRyxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUNsQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEtBQUssV0FBVyxDQUFDLEtBQUssRUFBRTt3QkFDMUM7OzJCQUVHO3dCQUNILElBQU0sYUFBYSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUN2SCxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRyxhQUFhLENBQUM7d0JBQzdDLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQzt3QkFDN0UsSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFOzRCQUNsQyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUN6RCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3lCQUMxRjtxQkFDSjtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDckUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7Z0JBQzNMLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQztnQkFDN0UsSUFBSSxtQkFBbUIsU0FBQSxDQUFDO2dCQUN4QixJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO29CQUNoQyxtQkFBbUIsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQSxnQkFBZ0IsSUFBSSxPQUFBLGdCQUFnQixDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQTVDLENBQTRDLENBQUMsQ0FBQztpQkFDeko7cUJBQU07b0JBQ0gsbUJBQW1CLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUEsZ0JBQWdCLElBQUksT0FBQSxnQkFBZ0IsQ0FBQyxXQUFXLEtBQUssV0FBVyxFQUE1QyxDQUE0QyxDQUFDLENBQUM7aUJBQ3JKO2dCQUNELElBQUksbUJBQW1CLEVBQUU7b0JBQ3JCLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztpQkFDcEU7Z0JBQ0QsMENBQTBDO2dCQUMxQyxrR0FBa0c7Z0JBQ2xHLDhGQUE4RjtnQkFDOUYsSUFBSTthQUNQO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFVBQVU7Z0JBQzFELElBQUksVUFBVSxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFO29CQUNsRixLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFVBQUEsYUFBYTt3QkFDaEUsSUFBSSxhQUFhLENBQUMsS0FBSyxLQUFLLFVBQVUsQ0FBQyxLQUFLLEVBQUU7NEJBQzFDLEtBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO3lCQUN0QztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxLQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO29CQUNuQyxJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLFdBQVcsS0FBSyxVQUFVLEVBQWhDLENBQWdDLENBQUMsQ0FBQztvQkFDekgsSUFBSSxhQUFhLEVBQUU7d0JBQ2YsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztxQkFDeEQ7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUI7WUFDeEUsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUU7Z0JBQzdCLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDNUM7WUFDRCxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDOUIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM3QztZQUNELElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMzRDtZQUNELElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUU7Z0JBQzdDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM1RDtZQUNELEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3pFLENBQUMsQ0FBQyxDQUFDO1FBRUg7O2NBRU07UUFFTixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDbkQsSUFBSSxJQUFJLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUNsQyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdCO2lCQUFNO2dCQUNILElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsV0FBVyxLQUFLLElBQUksRUFBN0IsQ0FBNkIsQ0FBQyxDQUFDO2dCQUN2SCxJQUFJLFlBQVksRUFBRTtvQkFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUN0RDthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRTtZQUNsQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDcEQsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQkFDOUQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLGFBQWE7d0JBQ2hFLElBQUksYUFBYSxDQUFDLEtBQUssS0FBSyxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFOzRCQUN6RCxLQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQzt5QkFDdEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU07b0JBQ0gsS0FBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztpQkFDdEM7Z0JBQ0QsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUMxSixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQztpQkFDN0U7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUM7aUJBQ3pFO2dCQUVELElBQUksSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMxRyxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQztpQkFDekU7Z0JBRUQsSUFBSSxLQUFJLENBQUMsY0FBYyxFQUFFO29CQUNyQixLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDM0I7Z0JBRUQsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUMxSixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQztpQkFDN0U7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUM7aUJBQ3pFO1lBR0wsQ0FBQyxDQUFDLENBQUM7WUFLSCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDbEQsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQkFDOUQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLGFBQWE7d0JBQ2hFLElBQUksYUFBYSxDQUFDLEtBQUssS0FBSyxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFOzRCQUN6RCxLQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQzt5QkFDdEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047cUJBQU07b0JBQ0gsS0FBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztpQkFDdEM7Z0JBQ0QsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFO29CQUNwSixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQztpQkFDN0U7cUJBQU0sSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUMxRyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQztpQkFDdkU7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUM7aUJBQ3pFO1lBSUwsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUNoRixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUV4SixLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDM0I7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3BGLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7b0JBRXhKLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2lCQUMzQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFHRCxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDN0MsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBc0JJO0lBRUosZ0RBQWdCLEdBQWhCO1FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUMxRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsR0FBRyxFQUFFO2dCQUMxRixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQzthQUM3RTtpQkFBTTtnQkFDSCxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDN1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0csSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQ3pELElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsRUFBRTtvQkFDckgsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDcEgsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLG9CQUFvQixDQUFDLEVBQ2hJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQy9GLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMzSCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzt3QkFDekQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsOEJBQThCLENBQUM7cUJBQ2pGO2lCQUNKO2FBQ0o7U0FDSjtJQUNMLENBQUM7SUFFRCx3REFBd0IsR0FBeEIsVUFBeUIsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSx5QkFBeUI7UUFBOUYsaUJBeURDO1FBdkRHLElBQUksbUJBQW1CLEtBQUssYUFBYSxDQUFDLG9CQUFvQixFQUFFO1lBQzVELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO2dCQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDMUM7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUM1QyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUN4RDtZQUVELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUVqRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFFdEQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQzdELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixFQUFFLENBQUM7YUFDdkU7WUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN0RSxJQUFJLHlCQUF5QixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUN6RSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDN0Q7U0FDSjthQUFNLElBQUksbUJBQW1CLEtBQUssYUFBYSxDQUFDLG9CQUFvQixFQUFFO1lBQ25FLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxTQUFTLENBQUM7Z0JBQzVCLEtBQUksQ0FBQyxlQUFlLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUM7Z0JBQzlFLEtBQUksQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQztnQkFDbEYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RFLElBQUkseUJBQXlCLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7b0JBQ3pFLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDN0Q7Z0JBRUQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUM5QyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFFckQsSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtvQkFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ2hGLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ3ZFO3FCQUFNO29CQUNILEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDbEUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLHNCQUFzQixFQUFFLENBQUM7aUJBQ3pEO2dCQUVELElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUM5QixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ3RFLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDekM7aUJBQ0o7Z0JBQ0QsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDNUMsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO3dCQUN0RSxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDdkQ7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELDhDQUFjLEdBQWQsVUFBZSxJQUFJO1FBQW5CLGlCQWdDQztRQS9CRyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO2dCQUM5QixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3RFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztpQkFDekM7YUFDSjtZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzVDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDdEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLENBQUM7aUJBQ3ZEO2FBQ0o7WUFDRCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUMsU0FBUyxDQUFDO2dCQUM1QixLQUFJLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDO2dCQUM5RSxLQUFJLENBQUMsZUFBZSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUM7Z0JBQ2xGLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO29CQUM5QixLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUMvQztnQkFDRCxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO29CQUM1QyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzdEO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzFDO1lBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ3hEO1NBQ0o7SUFDTCxDQUFDO0lBRUQscURBQXFCLEdBQXJCO1FBQUEsaUJBT0M7UUFORyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2FBQ2pGLFNBQVMsQ0FBQztZQUNQLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ3JELEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2hFLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELG9EQUFvQixHQUFwQixVQUFxQixJQUFJO1FBQ3JCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNwRCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQsK0NBQWUsR0FBZixVQUFnQixNQUFNO1FBQ2xCLElBQUksTUFBTSxFQUFFO1lBQ1IsSUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsS0FBSyxLQUFLLE1BQU0sRUFBeEIsQ0FBd0IsQ0FBQyxDQUFDO1lBQzNILE9BQU8sbUJBQW1CLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDekQ7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsOENBQWMsR0FBZCxVQUFlLFVBQVU7UUFBekIsaUJBd0JDO1FBdkJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDN0MsSUFBTSxVQUFVLEdBQUc7b0JBQ2YsTUFBTSxFQUFFLFVBQVU7aUJBQ3JCLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNwRSxJQUFNLFlBQVksR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsTUFBTSxFQUFqQyxDQUFpQyxDQUFDLENBQUM7b0JBQ2pHLElBQUksWUFBWSxFQUFFO3dCQUNkLEtBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO3FCQUNyQztvQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ25GLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBYSxHQUFiLFVBQWMsVUFBVTtRQUNwQixJQUFJLFVBQVUsRUFBRTtZQUNaLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO1lBQ3hGLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDN0M7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsMkRBQTJCLEdBQTNCO0lBQ0EsQ0FBQztJQUVELGtEQUFrQixHQUFsQjtRQUFBLGlCQW1CQztRQWxCRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEM7YUFBTTtZQUNILElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO2dCQUMzRCxLQUFLLEVBQUUsS0FBSztnQkFDWixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsSUFBSSxFQUFFO29CQUNGLE9BQU8sRUFBRSxpQ0FBaUM7b0JBQzFDLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDckI7YUFDSixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtnQkFDcEMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtvQkFDekIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDeEM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUVELDBDQUFVLEdBQVYsVUFBVyxVQUFVO1FBQXJCLGlCQW9EQztRQW5ERyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUU7WUFDMUcsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxLQUFLLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUF4RSxDQUF3RSxDQUFDLENBQUM7WUFDNUgsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkNBQTJDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDNUgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQzdCLE9BQU87YUFDVjtTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQy9DLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDeEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPO1NBQ1Y7UUFFRCxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQzFELElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDM0IsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNoSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pDLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQU0sa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBRWpDLElBQUksa0JBQWtCLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxRQUFRO2FBQ3RELENBQUMsQ0FBQztZQUNILElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNFQUFzRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdEosSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPO1NBQ1Y7UUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUNyRCxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNsSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUNoQztpQkFBTTtnQkFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDdkYsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUV0QztTQUNKO2FBQU07WUFDSCxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2xILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7U0FDaEM7SUFDTCxDQUFDO0lBRUQsK0NBQWUsR0FBZjtRQUFBLGlCQW1GQztRQWxGRyxJQUFNLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBTSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFNUIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUNoRCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFdBQVc7Z0JBQ2pDLElBQUksS0FBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ3hDLElBQU0sa0JBQWtCLEdBQUcsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxLQUFLLEtBQUssV0FBVyxDQUFDLEtBQUssRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO29CQUN0RyxJQUFJLGtCQUFrQixLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUMzQixjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUNwQztpQkFDSjtxQkFBTTtvQkFDSCxJQUFNLHdCQUF3QixHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUIsSUFBSSxPQUFBLG1CQUFtQixDQUFDLEtBQUssS0FBSyxXQUFXLENBQUMsS0FBSyxFQUEvQyxDQUErQyxDQUFDLENBQUM7b0JBQzdJLElBQUksd0JBQXdCLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ2pDLElBQU0sa0JBQWtCLEdBQUcsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxLQUFLLEtBQUssV0FBVyxDQUFDLEtBQUssRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO3dCQUN0RyxJQUFJLGtCQUFrQixLQUFLLENBQUMsQ0FBQyxFQUFFOzRCQUMzQixjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3lCQUNwQztxQkFDSjtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQUMsYUFBYTtnQkFDcEQsT0FBTztvQkFDSCxhQUFhLEVBQUUsRUFBRTtvQkFDakIsUUFBUSxFQUFFO3dCQUNOLEVBQUUsRUFBRSxFQUFFO3dCQUNOLE1BQU0sRUFBRSxFQUFFO3dCQUNWLE1BQU0sRUFBRSxhQUFhLENBQUMsS0FBSzt3QkFDM0IsTUFBTSxFQUFFLEtBQUs7cUJBQ2hCO29CQUNELE1BQU0sRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07b0JBQzlCLG9CQUFvQixFQUFFLEtBQUksQ0FBQyxJQUFJO2lCQUVsQyxDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGVBQWU7Z0JBQ3pDLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUNsQyxJQUFNLHdCQUF3QixHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUIsSUFBSSxPQUFBLG1CQUFtQixDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxFQUFuRCxDQUFtRCxDQUFDLENBQUM7b0JBQ2pKLElBQUksd0JBQXdCLElBQUksQ0FBQyxFQUFFO3dCQUMvQixJQUFNLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLEVBQXhDLENBQXdDLENBQUMsQ0FBQzt3QkFDdkcsSUFBSSxhQUFhLEtBQUssQ0FBQyxDQUFDLEVBQUU7NEJBQ3RCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzt5QkFDMUM7cUJBQ0o7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBTSxrQkFBa0IsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLGFBQWEsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssRUFBN0MsQ0FBNkMsQ0FBQyxDQUFDO29CQUN6SCxJQUFJLGtCQUFrQixLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUMzQixJQUFNLG9CQUFvQixHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssRUFBeEMsQ0FBd0MsQ0FBQyxDQUFDO3dCQUM5RyxJQUFNLHdCQUF3QixHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLEVBQXBDLENBQW9DLENBQUMsQ0FBQzt3QkFDbkgsSUFBSSxvQkFBb0IsS0FBSyxDQUFDLENBQUMsSUFBSSx3QkFBd0IsSUFBSSxDQUFDLEVBQUU7NEJBQzlELGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzt5QkFDMUM7cUJBQ0o7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBQyxRQUFRO2dCQUNyRCxPQUFPO29CQUNILGFBQWEsRUFBRSxFQUFFO29CQUNqQixRQUFRLEVBQUU7d0JBQ04sRUFBRSxFQUFFLEVBQUU7d0JBQ04sTUFBTSxFQUFFLEVBQUU7d0JBQ1YsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLO3dCQUN0QixNQUFNLEVBQUUsUUFBUTtxQkFDbkI7b0JBQ0QsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTTtvQkFDOUIsb0JBQW9CLEVBQUUsS0FBSSxDQUFDLElBQUk7aUJBRWxDLENBQUM7WUFDTixDQUFDLENBQUMsQ0FBQztTQUVOO1FBRUQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBUTtZQUN6RCxPQUFPO2dCQUNILEVBQUUsRUFBRSxFQUFFO2dCQUNOLE1BQU0sRUFBRSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxRQUFRLENBQUMsS0FBSzthQUN6QixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM3SixDQUFDO0lBQ0QsaURBQWlCLEdBQWpCLFVBQWtCLFVBQVU7UUFDeEIsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBTSxVQUFVLEdBQWtCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx5QkFBeUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQ3hKLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQy9ELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2VBQzlFLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNwRCxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDdkUsVUFBVSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLHdCQUF3QixDQUFDO2dCQUMxSCxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyx3QkFBd0IsQ0FBQztZQUN0RSxVQUFVLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQztnQkFDbkcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDO1lBQzFELFVBQVUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztnQkFDL0UsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztTQUN6QztRQUVELElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx3Q0FBUSxHQUFSLFVBQVMsVUFBVSxFQUFFLFVBQVU7UUFBL0IsaUJBc0VDO1FBckVHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRTtZQUNuQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDOUYsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzNILFVBQVUsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUNqQyxJQUFNLFVBQVUsR0FBRztvQkFDZixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsb0JBQW9CLEVBQUU7d0JBQ2xCLG1CQUFtQixFQUFFLElBQUksQ0FBQyxZQUFZO3FCQUN6QztpQkFDSixDQUFDO2dCQUNGLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ3RELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO29CQUM3QixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssRUFBRTt3QkFDL0UsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDOUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDekMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzs0QkFDMUIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsU0FBUyxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsU0FBUzs0QkFDL0YsVUFBVSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxVQUFVO3lCQUN0RSxDQUFDLENBQUM7cUJBQ047eUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLOzJCQUMvRyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO3dCQUNwRixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDeEgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7eUJBQU07d0JBQ0gsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzt3QkFDaEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7Z0JBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwwQ0FBMEMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUMzSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO2FBQ2hDO1NBQ0o7YUFBTTtZQUNILElBQU0sVUFBVSxHQUFHO2dCQUNmLFVBQVUsRUFBRSxVQUFVO2FBQ3pCLENBQUM7WUFDRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7aUJBQ2xDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7Z0JBQzdCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssS0FBSyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSTt1QkFDdkcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUNoSSxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUM1RyxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzt3QkFDMUIsTUFBTSxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsU0FBUyxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsU0FBUzt3QkFDL0YsVUFBVSxFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxVQUFVO3FCQUN0RSxDQUFDLENBQUM7aUJBQ047cUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLO3VCQUMvRyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO29CQUNwRixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDeEgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDNUM7cUJBQU07b0JBQ0gsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDaEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDNUM7WUFDTCxDQUFDLEVBQUU7Z0JBQ0MsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDOUgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDakMsQ0FBQyxDQUFDLENBQUM7U0FDVjtJQUNMLENBQUM7SUFFRCxvREFBb0IsR0FBcEI7UUFDSSxJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ25CLFlBQVksRUFBRSxLQUFLO1lBQ25CLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLHFCQUFxQixFQUFFLElBQUk7WUFDM0IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixXQUFXLEVBQUUsSUFBSTtZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRO1lBQ2xDLGVBQWUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWU7WUFDaEQsYUFBYSxFQUFFLElBQUk7WUFDbkIsZUFBZSxFQUFFLElBQUk7WUFDckIsb0JBQW9CLEVBQUUsSUFBSTtZQUMxQixPQUFPLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gsU0FBUyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ2xELE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU07WUFDOUIsWUFBWSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWTtZQUMxQyxtQkFBbUIsRUFBRSxJQUFJO1lBQ3pCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVU7WUFDdEMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUztZQUNwQyxlQUFlLEVBQUU7Z0JBQ2IsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELGNBQWMsRUFBRTtnQkFDWixLQUFLLEVBQUUsTUFBTTtnQkFDYixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxpQkFBaUIsRUFBRTtnQkFDZixLQUFLLEVBQUUsYUFBYTtnQkFDcEIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsbUJBQW1CLEVBQUU7Z0JBQ2pCLEtBQUssRUFBRSxzQkFBc0I7Z0JBQzdCLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELG1CQUFtQixFQUFFO2dCQUNqQixLQUFLLEVBQUUsc0JBQXNCO2dCQUM3QixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCxxQkFBcUIsRUFBRTtnQkFDbkIsS0FBSyxFQUFFLGtCQUFrQjtnQkFDekIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsbUJBQW1CLEVBQUUsSUFBSTtTQUM1QixDQUFDO0lBQ04sQ0FBQztJQUVELHdDQUFRLEdBQVI7UUFBQSxpQkF5QkM7UUF4QkcsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDckksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3RJLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQy9KLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwRCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDMUssSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0ksSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUNySSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUM1QyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUsscUJBQXFCLEVBQUU7Z0JBQ3JDLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ3BCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxvRUFBb0U7UUFDcEUsSUFBSSxTQUFTLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDOUM7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7WUFDOUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQzs7Z0JBN2tEc0IsVUFBVTtnQkFDTixjQUFjO2dCQUNyQixXQUFXO2dCQUNQLFdBQVc7Z0JBQ1gsV0FBVztnQkFDSCxtQkFBbUI7Z0JBQ3pCLGFBQWE7Z0JBQ3BCLFNBQVM7Z0JBQ0csa0JBQWtCO2dCQUNqQixtQkFBbUI7Z0JBQ25CLG1CQUFtQjs7SUExRjFDO1FBQVIsS0FBSyxFQUFFOzZEQUFpQztJQUMvQjtRQUFULE1BQU0sRUFBRTt1RUFBZ0Q7SUFDL0M7UUFBVCxNQUFNLEVBQUU7c0VBQStDO0lBQzlDO1FBQVQsTUFBTSxFQUFFO3NFQUErQztJQWtDNUI7UUFBM0IsU0FBUyxDQUFDLGVBQWUsQ0FBQztnRUFBMkI7SUF3Q25DO1FBQWxCLFNBQVMsQ0FBQyxNQUFNLENBQUM7a0VBQWtDO0lBL0UzQyxxQkFBcUI7UUFMakMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1CQUFtQjtZQUM3Qixvc1ZBQTZDOztTQUVoRCxDQUFDO09BQ1cscUJBQXFCLENBNHFEakM7SUFBRCw0QkFBQztDQUFBLEFBNXFERCxJQTRxREM7U0E1cURZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMsIEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jb3JlJztcclxuaW1wb3J0IHsgTWF0QXV0b2NvbXBsZXRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlJztcclxuaW1wb3J0IHsgVGFza1VwZGF0ZU9iaiB9IGZyb20gJy4vdGFzay51cGRhdGUnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTFMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzVHlwZXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTYXZlT3B0aW9uQ29uc3RhbnQgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL3NhdmUtb3B0aW9ucy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBQcm9qZWN0VXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC11dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrQ29uZmlnSW50ZXJmYWNlIH0gZnJvbSAnLi4vdGFzay5jb25maWcuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBDb21tZW50c1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vY29tbWVudHMvc2VydmljZXMvY29tbWVudHMudXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRU5URVIsIENPTU1BLCBTRU1JQ09MT04sIFNQQUNFIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcclxuaW1wb3J0IHsgbWFwLCBzdGFydFdpdGgsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlJztcclxuaW1wb3J0IHsgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRDaGlwSW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NoaXBzJztcclxuaW1wb3J0IHsgTm9kZVdpdGhJMThuIH0gZnJvbSAnQGFuZ3VsYXIvY29tcGlsZXInO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tdGFzay1jcmVhdGlvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdGFzay1jcmVhdGlvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi90YXNrLWNyZWF0aW9uLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhc2tDcmVhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgdGFza0NvbmZpZzogVGFza0NvbmZpZ0ludGVyZmFjZTtcclxuICAgIEBPdXRwdXQoKSBjbG9zZUNhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNhdmVDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBub3RpZmljYXRpb25IYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG5cclxuICAgIHRhc2tGb3JtOiBGb3JtR3JvdXA7XHJcblxyXG4gICAgdGFza01vZGFsQ29uZmlnO1xyXG4gICAgdGVhbVJvbGVzT3B0aW9ucztcclxuICAgIHRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICB0YXNrU3RhdHVzO1xyXG4gICAgc2VsZWN0ZWRPd25lcjtcclxuICAgIGlzSW52YWxpZFRhc2tPcGVyYXRpb24gPSBmYWxzZTtcclxuICAgIG93bmVyUHJvamVjdENvdW50ID0gZmFsc2U7XHJcbiAgICBvd25lclByb2plY3RDb3VudE1lc3NhZ2UgPSAnJztcclxuICAgIGlzWmVyb093bmVyUHJvamVjdENvdW50ID0gZmFsc2U7XHJcbiAgICBzaG93UmV2aXNpb25SZXZpZXdSZXF1aXJlZDogYm9vbGVhbjtcclxuICAgIGRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgZXhpc3RpbmdUYXNrVHlwZURldGFpbHMgPSBudWxsO1xyXG4gICAgLy8gZXhpc3RpbmdBc3NpZ25tZW50VHlwZURldGFpbHMgPSBudWxsO1xyXG4gICAgdGFza0NvbnN0YW50cyA9IFRhc2tDb25zdGFudHM7XHJcbiAgICBzZWxlY3RlZFN0YXR1c0luZm87XHJcbiAgICBvbGRUYXNrT2JqZWN0OiBUYXNrVXBkYXRlT2JqO1xyXG4gICAgZGF0ZVZhbGlkYXRpb246IHtcclxuICAgICAgICBzdGFydE1heERhdGU6IERhdGU7XHJcbiAgICAgICAgZW5kTWluRGF0ZTogRGF0ZTtcclxuICAgIH07XHJcbiAgICBtYXhEdXJhdGlvbjtcclxuICAgIGR1cmF0aW9uRXJyb3JNZXNzYWdlO1xyXG5cclxuICAgIGRlZmF1bHRSZXZpc2lvblJlcXVpcmVkU3RhdHVzOiBib29sZWFuO1xyXG4gICAgcHJvamVjdENvbmZpZzogYW55O1xyXG4gICAgYXBwQ29uZmlnOiBhbnk7XHJcbiAgICBuYW1lU3RyaW5nUGF0dGVybjtcclxuICAgIGFsbG93QWN0aW9uVGFzaztcclxuXHJcbiAgICBAVmlld0NoaWxkKCdyZXZpZXdlcklucHV0JykgcmV2aWV3ZXJJbnB1dDogRWxlbWVudFJlZjtcclxuICAgIHNhdmVkUmV2aWV3ZXJzID0gW107XHJcbiAgICBhbGxSZXZpZXdlcnMgPSBbXTtcclxuICAgIC8vIHJldmlld2VycyA9IFtdO1xyXG4gICAgZGVsaXZlcmFibGVSZXZpZXdlcnMgPSBbXTtcclxuICAgIC8vIHJldmlld2VyRGV0YWlscyA9IFtdO1xyXG4gICAgcmV2aWV3ZXJTdGF0dXNEYXRhID0gW107XHJcbiAgICBmaWx0ZXJlZFJldmlld2VyczogT2JzZXJ2YWJsZTxhbnlbXT47XHJcbiAgICBuZXdyZXZpZXdlcnNPYmo7XHJcbiAgICByZW1vdmVkcmV2aWV3ZXJzT2JqO1xyXG4gICAgcmV2aWV3ZXJzT2JqO1xyXG4gICAgc2VsZWN0ZWRSZXZpZXdlcnNPYmo7XHJcbiAgICB2aXNpYmxlID0gdHJ1ZTtcclxuICAgIHNlbGVjdGFibGUgPSB0cnVlO1xyXG4gICAgcmVtb3ZhYmxlID0gdHJ1ZTtcclxuICAgIGFkZE9uQmx1ciA9IGZhbHNlO1xyXG4gICAgc2VwYXJhdG9yS2V5c0NvZGVzID0gW0VOVEVSLCBDT01NQV07XHJcbiAgICBuZXdSZXZpZXdlcnMgPSBbXTtcclxuICAgIHJlbW92ZWRSZXZpZXdlcnMgPSBbXTtcclxuICAgIGxvZ2dlZEluVXNlcjtcclxuICAgIGRhdGVFcnJvck1lc3NhZ2U7XHJcbiAgICBpc1BNID0gZmFsc2U7XHJcbiAgICBjaGlwQ2hhbmdlZDtcclxuICAgIHNhdmVPcHRpb25zID0gU2F2ZU9wdGlvbkNvbnN0YW50LnRhc2tTYXZlT3B0aW9ucztcclxuICAgIFNFTEVDVEVEX1NBVkVfT1BUSU9OID0ge1xyXG4gICAgICAgIG5hbWU6ICdTYXZlJyxcclxuICAgICAgICB2YWx1ZTogJ1NBVkUnXHJcbiAgICB9O1xyXG4gICAgRHVyYXRpb25UeXBlID0gW1xyXG4gICAgICAgIHsgdmFsdWU6ICdkYXlzJywgdmlld1ZhbHVlOiAnRGF5cycgfSxcclxuICAgICAgICB7IHZhbHVlOiAnd2Vla3MnLCB2aWV3VmFsdWU6ICdXZWVrcycgfVxyXG4gICAgXTtcclxuICAgIGVuYWJsZUR1cmF0aW9uID0gZmFsc2U7XHJcbiAgICBlbmFibGVXb3JrV2VlayA9IGZhbHNlO1xyXG4gICAgcmV2aWV3ZXJDb21wbGV0ZWQ7XHJcblxyXG4gICAga2V5UmVzdHJpY3Rpb24gPSB7XHJcbiAgICAgICAgTlVNQkVSOiBbNDYsIDY5LCAxODYsIDE4NywgMTg4LCAxMDddXHJcbiAgICB9O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ2F1dG8nKSBtYXRBdXRvY29tcGxldGU6IE1hdEF1dG9jb21wbGV0ZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+LFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwZGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIHByb2plY3RVdGlsU2VydmljZTogUHJvamVjdFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBjb21tZW50c1V0aWxTZXJ2aWNlOiBDb21tZW50c1V0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHtcclxuXHJcblxyXG4gICAgfVxyXG5cclxuICAgIGZpbHRlclJldmlld2Vycyh2YWx1ZTogYW55KTogYW55W10ge1xyXG4gICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnICYmIHZhbHVlLnRyaW0oKSAhPT0gJycpIHtcclxuICAgICAgICAgICAgY29uc3QgZmlsdGVyVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYWxsUmV2aWV3ZXJzLmZpbHRlcihyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID09PSAwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzdHJpY3RLZXlzT25UeXBlKGV2ZW50LCBkYXRhdHlwZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmtleVJlc3RyaWN0aW9uW2RhdGF0eXBlXSAmJiB0aGlzLmtleVJlc3RyaWN0aW9uW2RhdGF0eXBlXS5pbmNsdWRlcyhldmVudC5rZXlDb2RlKSkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RlZChldmVudDogTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChzYXZlZFJldmlld2VyID0+IHNhdmVkUmV2aWV3ZXIudmFsdWUgPT09IGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcbiAgICAgICAgY29uc3QgcmV2aWV3ZXIgPSB0aGlzLmFsbFJldmlld2Vycy5maW5kKHJldmlld2VyRGF0YSA9PiByZXZpZXdlckRhdGEudmFsdWUgPT09IGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLnZhbHVlLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiZcclxuICAgICAgICAgICAgZXZlbnQub3B0aW9uLnZhbHVlID09PSB0aGlzLnRhc2tGb3JtLnZhbHVlLmRlbGl2ZXJhYmxlQXBwcm92ZXIudmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHJldmlld2VyLmRpc3BsYXlOYW1lICsgJyBpcyBhbHJlYWR5IHRoZSBBcHByb3ZlcicpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmV2aWV3ZXJJbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihyZXZpZXdlci5kaXNwbGF5TmFtZSArICcgaXMgYWxyZWFkeSBzZWxlY3RlZCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5uZXdSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgcmVtb3ZlUmV2aWV3ZXJJbmRleCA9IHRoaXMucmVtb3ZlZFJldmlld2Vycy5maW5kSW5kZXgocmVtb3ZlZFJldmlld2VyID0+IHJlbW92ZWRSZXZpZXdlci52YWx1ZSA9PT0gZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKHJlbW92ZVJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLnNwbGljZShyZW1vdmVSZXZpZXdlckluZGV4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6IGZhbHNlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VySW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLnNldFZhbHVlKG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRSZXZpZXdlcihldmVudDogTWF0Q2hpcElucHV0RXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMubWF0QXV0b2NvbXBsZXRlLmlzT3Blbikge1xyXG4gICAgICAgICAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG5cclxuICAgICAgICAgICAgaWYgKGlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICBpbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLnNldFZhbHVlKG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblJlbW92ZVJldmlld2VyKHJldmlld2VyVmFsdWU6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kSW5kZXgoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSByZXZpZXdlclZhbHVlKTtcclxuICAgICAgICBjb25zdCBuZXdSZXZpZXdlckluZGV4ID0gdGhpcy5uZXdSZXZpZXdlcnMuZmluZEluZGV4KG5ld1Jldmlld2VyID0+IG5ld1Jldmlld2VyLnZhbHVlID09PSByZXZpZXdlclZhbHVlKTtcclxuICAgICAgICBjb25zdCByZXZpZXdlckluZGV4ID0gdGhpcy5yZXZpZXdlclN0YXR1c0RhdGEuZmluZEluZGV4KHJldmlld2VyRGF0YSA9PiByZXZpZXdlckRhdGEudmFsdWUgPT09IHJldmlld2VyVmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IHJldmlld2VyID0gdGhpcy5hbGxSZXZpZXdlcnMuZmluZChyZXZpZXdlckRhdGEgPT4gcmV2aWV3ZXJEYXRhLnZhbHVlID09PSByZXZpZXdlclZhbHVlKTtcclxuICAgICAgICB0aGlzLnJlbW92ZWRSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgIG5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXIudmFsdWUsXHJcbiAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLm1hcmtBc0RpcnR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRSZXZpZXdlcnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5zcGxpY2UocmV2aWV3ZXJJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChuZXdSZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5uZXdSZXZpZXdlcnMuc3BsaWNlKG5ld1Jldmlld2VySW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRQcm9qZWN0RGV0YWlscyhwcm9qZWN0T2JqKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAocHJvamVjdE9iaiAmJiBwcm9qZWN0T2JqWydQcm9qZWN0LWlkJ10gJiYgcHJvamVjdE9ialsnUHJvamVjdC1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvamVjdE9iai5SX1BPX0NBVEVHT1JZICYmIHByb2plY3RPYmouUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5jYXRlZ29yeUlkID0gcHJvamVjdE9iai5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChwcm9qZWN0T2JqLlJfUE9fVEVBTSAmJiBwcm9qZWN0T2JqLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50ZWFtSWQgPSBwcm9qZWN0T2JqLlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QgPSBwcm9qZWN0T2JqO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudFVTZXJJRCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTtcclxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50VVNlcklEICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRVU2VySUQgPT09IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1Byb2plY3RPd25lciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByaW9yaXRpZXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAvKiAgdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFByaW9yaXRpZXMoTVBNX0xFVkVMUy5UQVNLKVxyXG4gICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocHJpb3JpdHlSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGlmIChwcmlvcml0eVJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocHJpb3JpdHlSZXNwb25zZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmlvcml0eVJlc3BvbnNlID0gW3ByaW9yaXR5UmVzcG9uc2VdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdCA9IHByaW9yaXR5UmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICAgICAgbGV0IHByaW9yaXR5ID0gdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFByaW9yaXRpZXMoTVBNX0xFVkVMUy5UQVNLKVxyXG4gICAgICAgICAgICAvLyAuc3Vic2NyaWJlKHByaW9yaXR5UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBpZiAocHJpb3JpdHkpIHtcclxuICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShwcmlvcml0eSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eSA9IFtwcmlvcml0eV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0ID0gcHJpb3JpdHk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0ID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgLyogIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBQcmlvcml0aWVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdGF0dXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsKE1QTV9MRVZFTFMuVEFTSylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3RhdHVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXNSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoc3RhdHVzUmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNSZXNwb25zZSA9IFtzdGF0dXNSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QgPSBzdGF0dXNSZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrU3RhdHVzTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUHJpb3JpdGllcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXR1cygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRTdGF0dXMoTVBNX0xFVkVMUy5UQVNLLCBTdGF0dXNUeXBlcy5JTklUSUFMKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShzdGF0dXNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXR1c1Jlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShzdGF0dXNSZXNwb25zZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c1Jlc3BvbnNlID0gW3N0YXR1c1Jlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3QgPSBzdGF0dXNSZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFJvbGVzKCkge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIHRlYW1JRDogdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkLFxyXG4gICAgICAgICAgICBpc0FwcHJvdmVyVGFzazogdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrLFxyXG4gICAgICAgICAgICBpc01lbWJlclRhc2s6ICF0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2tcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRUZWFtUm9sZXMoZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5UZWFtcyAmJiByZXNwb25zZS5UZWFtcy5NUE1fVGVhbXMgJiYgcmVzcG9uc2UuVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9IHJlc3BvbnNlLlRlYW1zLk1QTV9UZWFtcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLk1QTV9UZWFtX1JvbGVfTWFwcGluZykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLk1QTV9UZWFtX1JvbGVfTWFwcGluZyA9IFtyZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByb2xlTGlzdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm9sZUxpc3QgPSByZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvbGVzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyb2xlTGlzdCAmJiByb2xlTGlzdC5sZW5ndGggJiYgcm9sZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZUxpc3QubWFwKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvbGVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiByb2xlWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJvbGUuUk9MRV9ETixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJvbGUuTkFNRSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwUm9sZXM6IHJvbGUuTVBNX0FQUF9Sb2xlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZXNPcHRpb25zID0gcm9sZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tSb2xlQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlc09wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZXNPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJldmlld2VyQnlJZCh0YXNrVXNlcklEKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0YXNrVXNlcklEKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHRhc2tVc2VySURcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlbGl2ZXJhYmxlUmV2aWV3QnlUYXNrSWQoVGFza0lkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldERlbGl2ZXJhYmxlUmV2aWV3QnlUYXNrSUQoVGFza0lkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkRlbGl2ZXJhYmxlUmV2aWV3KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2Vyc0xpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnRGVsaXZlcmFibGVSZXZpZXcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJldmlld2Vyc0xpc3QgJiYgcmV2aWV3ZXJzTGlzdC5sZW5ndGggJiYgcmV2aWV3ZXJzTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXZpZXdlcnNMaXN0LmZvckVhY2gocmV2aWV3ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHJldmlld2VyLlJfUE9fUkVWSUVXRVJfVVNFUlsnSWRlbnRpdHktaWQnXS5JZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRSZXZpZXdlckJ5SWQocmV2aWV3ZXIuUl9QT19SRVZJRVdFUl9VU0VSWydJZGVudGl0eS1pZCddLklkKS5zdWJzY3JpYmUocmV2aWV3ZXJEYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXZpZXdlckRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBoYXNSZXZpZXdlckRhdGEgPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLmZpbmQoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSByZXZpZXdlckRhdGEudXNlckNOKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWhhc1Jldmlld2VyRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkUmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyRGF0YS51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyRGF0YS51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRTdGF0dXNCeUlkKHJldmlld2VyLlJfUE9fUkVWSUVXX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkKS5zdWJzY3JpYmUoc3RhdHVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGlzUmV2aWV3ZXJDb21wbGV0ZWQgPSBzdGF0dXNSZXNwb25zZS5TVEFUVVNfTEVWRUwgPT09ICdGSU5BTCcgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmV2aWV3ZXJJbmRleCA9IHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLmZpbmRJbmRleChkYXRhID0+IGRhdGEudmFsdWUgPT09IHJldmlld2VyRGF0YS51c2VyQ04pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXZpZXdlclN0YXR1c0RhdGFbcmV2aWV3ZXJJbmRleF0uaXNDb21wbGV0ZWQgPSBpc1Jldmlld2VyQ29tcGxldGVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmV2aWV3ZXJTdGF0dXNEYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyRGF0YS51c2VyQ04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcmV2aWV3ZXJEYXRhLmZ1bGxOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6IGlzUmV2aWV3ZXJDb21wbGV0ZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc2F2ZWRSZXZpZXdlcnMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIERlbGl2ZXJhYmxlIFJldmlldyBEZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZXZpZXdlcnMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdCA9IHt9O1xyXG4gICAgICAgICAgICAgICAgZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUixcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBpc1JldmlldzogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyTGlzdC5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdXNlci5uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsUmV2aWV3ZXJzID0gdXNlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbFJldmlld2VycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBSZXZpZXdlciBVc2VycycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBnZXRVc2Vyc0J5Um9sZSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIGxldCBnZXRVc2VyQnlSb2xlUmVxdWVzdCA9IHt9O1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFza0Zvcm1WYWx1ZSA9IHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKTtcclxuICAgICAgICAgICAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiB0YXNrRm9ybVZhbHVlLmFsbG9jYXRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUROOiB0YXNrRm9ybVZhbHVlLmFsbG9jYXRpb24gPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUgPyB0YXNrRm9ybVZhbHVlLnJvbGUudmFsdWUgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICB0ZWFtSUQ6IHRhc2tGb3JtVmFsdWUuYWxsb2NhdGlvbiA9PT0gVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUiA/IHRoaXMudGFza01vZGFsQ29uZmlnLnRlYW1JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayxcclxuICAgICAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6ICF0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2tcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb2xlSWQgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19URUFNX1JPTEUgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXVxyXG4gICAgICAgICAgICAgICAgICAgID8gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm9sZSA9IHRoaXMuZm9ybVJvbGVWYWx1ZShyb2xlSWQpO1xyXG4gICAgICAgICAgICAgICAgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BU1NJR05NRU5UX1RZUEUsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUROOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQVNTSUdOTUVOVF9UWVBFID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFID8gKHJvbGUgPyByb2xlLnZhbHVlIDogJycpIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQVNTSUdOTUVOVF9UWVBFID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9VU0VSID8gdGhpcy50YXNrTW9kYWxDb25maWcudGVhbUlkIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogIXRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFza1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb25UeXBlOiBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9VU0VSLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVhbUlEOiB0aGlzLnRhc2tNb2RhbENvbmZpZy50ZWFtSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0VXNlcnNGb3JUZWFtKGdldFVzZXJCeVJvbGVSZXF1ZXN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJMaXN0Lm1hcCh1c2VyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWZpZWxkT2JqKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB1c2VyLmNuLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHVzZXIubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMgPSB1c2VycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgSW5kaXZpZHVhbCBVc2VycycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza1R5cGVEZXRhaWxzKHRhc2tUeXBlSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuZXhpc3RpbmdUYXNrVHlwZURldGFpbHMgPSBudWxsO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShcclxuICAgICAgICAgICAgb2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICdUYXNrX1R5cGUtaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiB0YXNrVHlwZUlkXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KCdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9UYXNrX1R5cGUvb3BlcmF0aW9ucycsICdSZWFkVGFza19UeXBlJywgcGFyYW1zKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgdGFza3R5cGVEZXRhaWxSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tUeXBlRGV0YWlscyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AodGFza3R5cGVEZXRhaWxSZXNwb25zZSwgJ1Rhc2tfVHlwZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh0YXNrVHlwZURldGFpbHMpICYmIHRhc2tUeXBlRGV0YWlscy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmV4aXN0aW5nVGFza1R5cGVEZXRhaWxzID0gdGFza1R5cGVEZXRhaWxzWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnTm8gVGFzayBUeXBlIERldGFpbHMgZm91bmQnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRhc2t0eXBlRGV0YWlsUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZmFpbCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRhc2sgVHlwZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGZhaWwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qICBnZXRBc3NpZ25tZW50VHlwZURldGFpbHMoYXNzaWdubWVudFR5cGVJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgIHRoaXMuZXhpc3RpbmdBc3NpZ25tZW50VHlwZURldGFpbHMgPSBudWxsO1xyXG4gICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoXHJcbiAgICAgICAgICAgICBvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAnQXNzaWdubWVudF9UeXBlLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IGFzc2lnbm1lbnRUeXBlSWRcclxuICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QoJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0Fzc2lnbm1lbnRfVHlwZS9vcGVyYXRpb25zJywgJ1JlYWRBc3NpZ25tZW50X1R5cGUnLCBwYXJhbXMpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgYXNzaWdubWVudFR5cGVEZXRhaWxzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXNzaWdubWVudFR5cGVEZXRhaWxzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChhc3NpZ25tZW50VHlwZURldGFpbHNSZXNwb25zZSwgJ0Fzc2lnbm1lbnRfVHlwZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoYXNzaWdubWVudFR5cGVEZXRhaWxzKSAmJiBhc3NpZ25tZW50VHlwZURldGFpbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXhpc3RpbmdBc3NpZ25tZW50VHlwZURldGFpbHMgPSBhc3NpZ25tZW50VHlwZURldGFpbHNbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ05vIEFzc2lnbm1lbnQgVHlwZSBEZXRhaWxzIGZvdW5kJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhc3NpZ25tZW50VHlwZURldGFpbHNSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICBmYWlsID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFRhc2sgVHlwZScsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZmFpbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgfVxyXG4gICAgICAgICApO1xyXG4gICAgIH0gKi9cclxuXHJcbiAgICBnZXREZXBlbmRlbnRUYXNrcyhpc0FwcHJvdmFsVGFzazogYm9vbGVhbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcmVxdWVzdFBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICBQcm9qZWN0SUQ6IHRoaXMudGFza0NvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICBJc0FwcHJvdmFsVGFzazogaXNBcHByb3ZhbFRhc2ssXHJcbiAgICAgICAgICAgICAgICBBbGxvd0FjdGlvblRhc2s6IHRoaXMuYWxsb3dBY3Rpb25UYXNrXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuZ2V0RGVwZW5kZW50VGFzayhyZXF1ZXN0UGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGVwZW5kZW50VGFza1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGVwZW5kZW50VGFza1Jlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShkZXBlbmRlbnRUYXNrUmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXBlbmRlbnRUYXNrUmVzcG9uc2UgPSBbZGVwZW5kZW50VGFza1Jlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdCA9IGRlcGVuZGVudFRhc2tSZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZpbmcgdGhlIHNlbGVjdGVkIHRhc2sgZnJvbSB0YXNrIGxpc3RcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tMaXN0ID0gdGhpcy50YXNrTW9kYWxDb25maWcudGFza0xpc3QuZmlsdGVyKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0YXNrICE9PSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwcmVkZWNlc3NvckZpbHRlckxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0xpc3QubWFwKHRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJZCAmJiB0YXNrWydUYXNrLWlkJ10uSWQgPT09IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzayA9IHRhc2s7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZm9ybVByZWRlY2Vzc29yVmFsdWUodGFzayk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFzay5OQU1FICYmIHRhc2tbJ1Rhc2staWQnXSAmJiB0YXNrWydUYXNrLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVkZWNlc3NvckZpbHRlckxpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHRhc2suTkFNRSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRhc2tbJ1Rhc2staWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyeVBhY2thZ2VDb25maWcuZmlsdGVyT3B0aW9ucyA9IHByZWRlY2Vzc29yRmlsdGVyTGlzdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcucHJlZGVjZXNzb3JDb25maWcuZmlsdGVyT3B0aW9ucyA9IHByZWRlY2Vzc29yRmlsdGVyTGlzdDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3IucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJ5UGFja2FnZUNvbmZpZy5maWx0ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyeVBhY2thZ2VDb25maWcuZmlsdGVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcucHJlZGVjZXNzb3JDb25maWcuZmlsdGVyT3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJlRGV0YWlscygpIHtcclxuICAgICAgICB0aGlzLmdldFByb2plY3REZXRhaWxzKHRoaXMudGFza0NvbmZpZy5wcm9qZWN0T2JqKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0RGVwZW5kZW50VGFza3ModGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuY2F0ZWdvcnlJZCAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy50ZWFtSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWxsUm9sZXMoKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmdldFByaW9yaXRpZXMoKSwgdGhpcy5nZXRTdGF0dXMoKSwgdGhpcy5nZXRJbml0aWFsU3RhdHVzKCksIHRoaXMuZ2V0VXNlcnNCeVJvbGUoKSwgdGhpcy5nZXRSZXZpZXdlcnMoKV0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0Fzc2lnbm1lbnRUeXBlcyA9IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1R5cGVzID0gVGFza0NvbnN0YW50cy5UQVNLX1RZUEVfTElTVDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5pc05ld1Rhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdGlhbGlzZVRhc2tGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVySWQgPSAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddKSA/IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ya0pvaW4oW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmdldFRhc2tEYXRhVmFsdWVzKHRoaXMudGFza0NvbmZpZy50YXNrSWQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Pd25lclZhbHVlKG93bmVySWQpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldERlbGl2ZXJhYmxlUmV2aWV3QnlUYXNrSWQodGhpcy50YXNrQ29uZmlnLnRhc2tJZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKS5zdWJzY3JpYmUoKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZVZhbGlkYXRpb24gPSByZXNwb25zZVswXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVGFza0NvbnN0YW50cy5UQVNLX1RZUEVfTElTVC5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzayAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suVEFTS19UWVBFICYmIGVsZW1lbnQuTkFNRSA9PT0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlRBU0tfVFlQRSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXhpc3RpbmdUYXNrVHlwZURldGFpbHMgPSBlbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVGFza0NvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEUuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZyAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLkFMTE9DQVRJT05fVFlQRSAmJiBlbGVtZW50Lk5BTUUgPT09IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BTExPQ0FUSU9OX1RZUEUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmV4aXN0aW5nVGFza1R5cGVEZXRhaWxzID0gZWxlbWVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdGlhbGlzZVRhc2tGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgVGFza3MnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgUHJvamVjdCBEZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tGaWVsZHModGFzaykge1xyXG4gICAgICAgIGNvbnN0IHRhc2tGaWVsZCA9IHtcclxuICAgICAgICAgICAgaXNBcHByb3ZhbFRhc2tTZWxlY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHNob3dPdGhlckNvbW1lbnRzOiBmYWxzZSxcclxuICAgICAgICAgICAgaXNPdGhlckNvbW1lbnRzU2VsZWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBzaG93UHJlZGVjZXNzb3I6IGZhbHNlLFxyXG4gICAgICAgICAgICBlbmFibGVEZWxpdmVyYWJsZUFwcHJvdmVyOiBmYWxzZSxcclxuICAgICAgICAgICAgc2hvd0RlbGl2ZXJ5UGFja2FnZTogZmFsc2VcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAodGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGFzay5QQVJFTlRfVEFTS19JRCkpIHtcclxuICAgICAgICAgICAgICAgIHRhc2tGaWVsZC5pc090aGVyQ29tbWVudHNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dPdGhlckNvbW1lbnRzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0YXNrRmllbGQuc2hvd1ByZWRlY2Vzc29yID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0YXNrRmllbGQuc2hvd0RlbGl2ZXJ5UGFja2FnZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0YXNrRmllbGQuZW5hYmxlRGVsaXZlcmFibGVBcHByb3ZlciA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGFza0ZpZWxkLnNob3dPdGhlckNvbW1lbnRzID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRhc2tGaWVsZC5pc090aGVyQ29tbWVudHNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0YXNrRmllbGQuc2hvd0RlbGl2ZXJ5UGFja2FnZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0YXNrRmllbGQuc2hvd1ByZWRlY2Vzc29yID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGFza0ZpZWxkLmVuYWJsZURlbGl2ZXJhYmxlQXBwcm92ZXIgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRhc2tGaWVsZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmVkZWNlc3NvcnRhc2socHJlZGVjZXNzb3JJZCkge1xyXG4gICAgICAgIGxldCBwcmVkZWNlc3NvclRhc2sgPSBudWxsO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tMaXN0Lm1hcCh0YXNrID0+IHtcclxuICAgICAgICAgICAgaWYgKHRhc2tbJ1Rhc2staWQnXS5JZCA9PT0gcHJlZGVjZXNzb3JJZCkge1xyXG4gICAgICAgICAgICAgICAgcHJlZGVjZXNzb3JUYXNrID0gdGFzaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBwcmVkZWNlc3NvclRhc2s7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGlzZVRhc2tGb3JtKCkge1xyXG4gICAgICAgIGNvbnN0IGRpc2FibGVGaWVsZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMudGFza0Zvcm0gPSBudWxsO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1pbkRhdGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuU1RBUlRfREFURTtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5tYXhEYXRlID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkRVRV9EQVRFO1xyXG4gICAgICAgIGNvbnN0IHBhdHRlcm4gPSBQcm9qZWN0Q29uc3RhbnQuTkFNRV9TVFJJTkdfUEFUVEVSTjtcclxuICAgICAgICBpZiAoIXRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLmlzTmV3VGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU3RhdHVzID0gdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0O1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tOYW1lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvblR5cGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLkR1cmF0aW9uVHlwZVswXS52YWx1ZSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgIXRoaXMuZW5hYmxlRHVyYXRpb24pIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdCAmJiB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddID8gdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0WzBdWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgICAgICBvd25lcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3QgJiYgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdFswXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10gPyB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0WzBdWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGFydERhdGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuU1RBUlRfREFURSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZW5kRGF0ZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5EVUVfREFURSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcmV2aXNpb25SZXZpZXdSZXF1aXJlZDogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzayA/IHRoaXMuZGVmYXVsdFJldmlzaW9uUmVxdWlyZWRTdGF0dXMgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBpc01pbGVzdG9uZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IGZhbHNlLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZXdPdGhlckNvbW1lbnRzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJlZGVjZXNzb3I6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tTdGF0dXMgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5maWx0ZXJTdGF0dXNPcHRpb25zKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkLCB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrU3RhdHVzTGlzdCwgTVBNX0xFVkVMUy5UQVNLKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuZ2V0U3RhdHVzQnlTdGF0dXNJZCh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCwgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QpO1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgKHNlbGVjdGVkU3RhdHVzSW5mbyAmJiAoc2VsZWN0ZWRTdGF0dXNJbmZvWydTVEFUVVNfTEVWRUwnXSA9PT0gU3RhdHVzTGV2ZWxzLkZJTkFMKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5vdmVyVmlld0NvbmZpZy5pc1JlYWRPbmx5ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIC8vIGRpc2FibGVGaWVsZHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5kaXNhYmxlU3RhdHVzT3B0aW9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXNrID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFza0ZpZWxkID0gdGhpcy5nZXRUYXNrRmllbGRzKHRhc2spO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFza1JvbGVJZCA9IHRhc2suUl9QT19URUFNX1JPTEUgJiYgdGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10gPyB0YXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgLy8gY29uc3QgdGFza093bmVySWQgPSB0YXNrLlJfUE9fT1dORVJfSUQgJiYgdGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddID8gdGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICB0YXNrLnByZWRlY2Vzc29yID0gKHRhc2suUEFSRU5UX1RBU0tfSUQgJiYgdHlwZW9mIHRhc2suUEFSRU5UX1RBU0tfSUQgIT09ICdvYmplY3QnKSA/IHRhc2suUEFSRU5UX1RBU0tfSUQgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tOYW1lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGFzay5OQU1FLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB0YXNrLlRBU0tfRFVSQVRJT04gPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heCg5OTkpLCBWYWxpZGF0b3JzLm1pbigxKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uVHlwZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB0YXNrLlRBU0tfRFVSQVRJT05fVFlQRSA9PT0gJ3N0cmluZycgPyB0YXNrLlRBU0tfRFVSQVRJT05fVFlQRSA6IHRoaXMuRHVyYXRpb25UeXBlWzBdLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLlJfUE9fU1RBVFVTICYmIHRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXSA/IHRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8gJiYgdGhpcy5zZWxlY3RlZFN0YXR1c0luZm8uU1RBVFVTX1RZUEUgIT09IHRoaXMudGFza0NvbnN0YW50cy5TVEFUVVNfVFlQRV9JTklUSUFMXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrRmllbGQuaXNBcHByb3ZhbFRhc2tTZWxlY3RlZCA/ICcnIDogdGhpcy5zZWxlY3RlZE93bmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8ICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZm9ybVJvbGVWYWx1ZSh0YXNrUm9sZUlkKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTClcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICh0YXNrLlJfUE9fUFJJT1JJVFkgJiYgdGFzay5SX1BPX1BSSU9SSVRZWydNUE1fUHJpb3JpdHktaWQnXSA/IHRhc2suUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuY29tbWVudHNVdGlsU2VydmljZS5hZGRVbml0c1RvRGF0ZShuZXcgRGF0ZSh0YXNrLlNUQVJUX0RBVEUpLCAwLCAnZGF5cycsIHRoaXMuZW5hYmxlV29ya1dlZWspLCBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTCkpXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBlbmREYXRlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGFzay5EVUVfREFURSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgdGhpcy5lbmFibGVEdXJhdGlvbikgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0YXNrLkRFU0NSSVBUSU9OKSA/ICcnIDogdGFzay5ERVNDUklQVElPTiwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICByZXZpc2lvblJldmlld1JlcXVpcmVkOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGFzay5SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQpLCBkaXNhYmxlZDogdHJ1ZSB9KSxcclxuICAgICAgICAgICAgICAgICAgICBpc01pbGVzdG9uZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRhc2suSVNfTUlMRVNUT05FKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICB2aWV3T3RoZXJDb21tZW50czogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRhc2tGaWVsZC5zaG93T3RoZXJDb21tZW50cywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmVkZWNlc3NvcjogbmV3IEZvcm1Db250cm9sKHRoaXMuZm9ybVByZWRlY2Vzc29yKHRhc2sucHJlZGVjZXNzb3IpKSxcclxuICAgICAgICAgICAgICAgICAgICAvLyBkaXNhYmxlZDogdGFza0ZpZWxkLmlzQXBwcm92YWxUYXNrU2VsZWN0ZWQgfHwgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrRmllbGQuaXNBcHByb3ZhbFRhc2tTZWxlY3RlZCA/IHRoaXMuc2VsZWN0ZWRPd25lciA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8ICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNhdmVkUmV2aWV3ZXJzIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLkFTU0lHTk1FTlRfVFlQRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZHByZWRlY2Vzc29yID0gdGhpcy5nZXRQcmVkZWNlc3NvcnRhc2sodGFzay5wcmVkZWNlc3Nvcik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnByZWRlY2Vzc29yLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZHByZWRlY2Vzc29yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWluRGF0ZU9iamVjdCA9IHRoaXMuY29tbWVudHNVdGlsU2VydmljZS5hZGRVbml0c1RvRGF0ZShuZXcgRGF0ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZHByZWRlY2Vzc29yLkRVRV9EQVRFKSwgMSwgJ2RheXMnLCB0aGlzLmVuYWJsZVdvcmtXZWVrKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5taW5EYXRlID0gbWluRGF0ZU9iamVjdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGFza093bmVyQXNzaWdubWVudENvdW50KCk7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLm9uVGFza1NlbGVjdGlvbigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tUeXBlID0gdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlRBU0tfVFlQRTtcclxuICAgICAgICAgICAgICAgIHRoaXMub2xkVGFza09iamVjdCA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmNyZWF0ZU5ld1Rhc2tVcGRhdGVPYmplY3RcclxuICAgICAgICAgICAgICAgICAgICAodGhpcy50YXNrRm9ybS5nZXRSYXdWYWx1ZSgpLCB0aGlzLnRhc2tDb25maWcudGFza1R5cGUsIHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaywgdGhpcy50YXNrTW9kYWxDb25maWcucHJvamVjdElkLCB0aGlzLnNhdmVkUmV2aWV3ZXJzKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza01vZGFsQ29uZmlnLmlzTmV3VGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU3RhdHVzID0gdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0O1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tOYW1lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIDIwNVxyXG4gICAgICAgICAgICAgICAgICAgIC8qICAgZHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgIXRoaXMuZW5hYmxlRHVyYXRpb24pIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heCg5OTkpLCBWYWxpZGF0b3JzLm1pbigxKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb25UeXBlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5EdXJhdGlvblR5cGVbMF0udmFsdWUsIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSA/IGRpc2FibGVGaWVsZCA6ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvblR5cGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLkR1cmF0aW9uVHlwZVswXS52YWx1ZSwgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSA/IGRpc2FibGVGaWVsZCA6ICF0aGlzLmVuYWJsZUR1cmF0aW9uKSB9KSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrSW5pdGlhbFN0YXR1c0xpc3QgJiYgdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0WzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tJbml0aWFsU3RhdHVzTGlzdFswXVsnTVBNX1N0YXR1cy1pZCddID8gdGhpcy50YXNrTW9kYWxDb25maWcudGFza0luaXRpYWxTdGF0dXNMaXN0WzBdWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICByb2xlOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUHJpb3JpdHlMaXN0ICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tQcmlvcml0eUxpc3RbMF1bJ01QTV9Qcmlvcml0eS1pZCddID8gdGhpcy50YXNrTW9kYWxDb25maWcudGFza1ByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICByZXZpc2lvblJldmlld1JlcXVpcmVkOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrID8gdGhpcy5kZWZhdWx0UmV2aXNpb25SZXF1aXJlZFN0YXR1cyA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzTWlsZXN0b25lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogZmFsc2UsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgdmlld090aGVyQ29tbWVudHM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmVkZWNlc3NvcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZVJldmlld2VyczogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tTdGF0dXMgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5maWx0ZXJTdGF0dXNPcHRpb25zKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkLCB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrU3RhdHVzTGlzdCwgTVBNX0xFVkVMUy5UQVNLKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvID0gdGhpcy5wcm9qZWN0VXRpbFNlcnZpY2UuZ2V0U3RhdHVzQnlTdGF0dXNJZCh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCwgdGhpcy50YXNrTW9kYWxDb25maWcudGFza1N0YXR1c0xpc3QpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhc2sgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2s7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXNrRmllbGQgPSB0aGlzLmdldFRhc2tGaWVsZHModGFzayk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YXNrUm9sZUlkID0gdGFzay5SX1BPX1RFQU1fUk9MRSAmJiB0YXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXSA/IHRhc2suUl9QT19URUFNX1JPTEVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICB0YXNrLnByZWRlY2Vzc29yID0gKHRhc2suUEFSRU5UX1RBU0tfSUQgJiYgdHlwZW9mIHRhc2suUEFSRU5UX1RBU0tfSUQgIT09ICdvYmplY3QnKSA/IHRhc2suUEFSRU5UX1RBU0tfSUQgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRhc2tOYW1lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGFzay5OQU1FLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICAvLyAyMDVcclxuICAgICAgICAgICAgICAgICAgICAvKiBkdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB0YXNrLlRBU0tfRFVSQVRJT04gPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8ICF0aGlzLmVuYWJsZUR1cmF0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heCg5OTkpLCBWYWxpZGF0b3JzLm1pbigxKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uVHlwZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB0YXNrLlRBU0tfRFVSQVRJT05fVFlQRSA9PT0gJ3N0cmluZycgPyB0YXNrLlRBU0tfRFVSQVRJT05fVFlQRSA6IHRoaXMuRHVyYXRpb25UeXBlWzBdLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogKGRpc2FibGVGaWVsZCB8fCAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9KSwgKi9cclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHR5cGVvZiB0YXNrLlRBU0tfRFVSQVRJT04gPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAoZGlzYWJsZUZpZWxkIHx8IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyBkaXNhYmxlRmllbGQgOiAhdGhpcy5lbmFibGVEdXJhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXgoOTk5KSwgVmFsaWRhdG9ycy5taW4oMSldKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvblR5cGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2YgdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgPT09ICdzdHJpbmcnID8gdGFzay5UQVNLX0RVUkFUSU9OX1RZUEUgOiB0aGlzLkR1cmF0aW9uVHlwZVswXS52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IChkaXNhYmxlRmllbGQgfHwgdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSA/IGRpc2FibGVGaWVsZCA6ICF0aGlzLmVuYWJsZUR1cmF0aW9uKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suUl9QT19TVEFUVVMgJiYgdGFzay5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddID8gdGFzay5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrRmllbGQuaXNBcHByb3ZhbFRhc2tTZWxlY3RlZCA/ICcnIDogdGhpcy5zZWxlY3RlZE93bmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8ICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZm9ybVJvbGVWYWx1ZSh0YXNrUm9sZUlkKSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTClcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICh0YXNrLlJfUE9fUFJJT1JJVFkgJiYgdGFzay5SX1BPX1BSSU9SSVRZWydNUE1fUHJpb3JpdHktaWQnXSA/IHRhc2suUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRhc2suREVTQ1JJUFRJT04pID8gJycgOiB0YXNrLkRFU0NSSVBUSU9OLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0sIFtWYWxpZGF0b3JzLm1heExlbmd0aCgxMDAwKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHJldmlzaW9uUmV2aWV3UmVxdWlyZWQ6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0YXNrLlJFVklTSU9OX1JFVklFV19SRVFVSVJFRCksIGRpc2FibGVkOiB0cnVlIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzTWlsZXN0b25lOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGFzay5JU19NSUxFU1RPTkUpLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpZXdPdGhlckNvbW1lbnRzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGFza0ZpZWxkLnNob3dPdGhlckNvbW1lbnRzLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHByZWRlY2Vzc29yOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5mb3JtUHJlZGVjZXNzb3IodGFzay5wcmVkZWNlc3NvcikpLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrRmllbGQuaXNBcHByb3ZhbFRhc2tTZWxlY3RlZCA/IHRoaXMuc2VsZWN0ZWRPd25lciA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8ICh0aGlzLnNlbGVjdGVkU3RhdHVzSW5mbyAmJiB0aGlzLnNlbGVjdGVkU3RhdHVzSW5mby5TVEFUVVNfVFlQRSAhPT0gdGhpcy50YXNrQ29uc3RhbnRzLlNUQVRVU19UWVBFX0lOSVRJQUwpXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNhdmVkUmV2aWV3ZXJzIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLkFTU0lHTk1FTlRfVFlQRSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvICYmIHRoaXMuc2VsZWN0ZWRTdGF0dXNJbmZvLlNUQVRVU19UWVBFICE9PSB0aGlzLnRhc2tDb25zdGFudHMuU1RBVFVTX1RZUEVfSU5JVElBTClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3NvciA9IHRoaXMuZ2V0UHJlZGVjZXNzb3J0YXNrKHRhc2sucHJlZGVjZXNzb3IpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3Nvci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3Nvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1pbkRhdGVPYmplY3QgPSB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuYWRkVW5pdHNUb0RhdGUobmV3IERhdGUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRwcmVkZWNlc3Nvci5EVUVfREFURSksIDEsICdkYXlzJywgdGhpcy5lbmFibGVXb3JrV2Vlayk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWluRGF0ZSA9IG1pbkRhdGVPYmplY3Q7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFRhc2tPd25lckFzc2lnbm1lbnRDb3VudCgpO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5vblRhc2tTZWxlY3Rpb24oKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrVHlwZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5UQVNLX1RZUEU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9sZFRhc2tPYmplY3QgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jcmVhdGVOZXdUYXNrVXBkYXRlT2JqZWN0XHJcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKSwgdGhpcy50YXNrQ29uZmlnLnRhc2tUeXBlLCB0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2ssIHRoaXMudGFza01vZGFsQ29uZmlnLnByb2plY3RJZCwgdGhpcy5zYXZlZFJldmlld2Vycyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZmlsdGVyZWRSZXZpZXdlcnMgPSB0aGlzLnRhc2tGb3JtLmdldCgnZGVsaXZlcmFibGVSZXZpZXdlcnMnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgICAgICAgbWFwKChyZXZpZXdlcjogc3RyaW5nIHwgbnVsbCkgPT4gcmV2aWV3ZXIgPyB0aGlzLmZpbHRlclJldmlld2VycyhyZXZpZXdlcikgOiB0aGlzLmFsbFJldmlld2Vycy5zbGljZSgpKSk7XHJcblxyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkU3RhdHVzSXRlbUlkID0gdGhpcy50YXNrRm9ybS52YWx1ZS5zdGF0dXM7XHJcbiAgICAgICAgLy8gdGhpcy50YXNrRm9ybS5jb250cm9scy5zdGF0dXMuZGlzYWJsZSgpO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3NvcjtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrT3duZXJDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tSb2xlQ29uZmlnLmZvcm1Db250cm9sID0gdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlO1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZm9ybUNvbnRyb2wgPSB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXI7XHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcmFibGVSZXZpZXdlci5mb3JtQ29udHJvbCA9IHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcjtcclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyeVBhY2thZ2VDb25maWcuZm9ybUNvbnRyb2wgPSB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnByZWRlY2Vzc29yO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmFsbG9jYXRpb24udmFsdWUgPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1JPTEUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIgJiYgIXRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlciAmJiAhdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlUmV2aWV3ZXIgJiYgIXRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzaykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2VyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvKiBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnZhbHVlICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgpLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3Zlci5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gKi9cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucHJlZGVjZXNzb3IudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShwcmVkZWNlc3NvciA9PiB7XHJcbiAgICAgICAgICAgIC8vIFRPRE8gcHJlZGVjZXNzb3IgY2hhbmdlIG1ldGhvZFxyXG4gICAgICAgICAgICBpZiAoIXRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzayAmJiB0eXBlb2YgcHJlZGVjZXNzb3IgPT09ICdvYmplY3QnICYmIHByZWRlY2Vzc29yICYmIHByZWRlY2Vzc29yLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrTGlzdC5tYXAodGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2tbJ1Rhc2staWQnXS5JZCA9PT0gcHJlZGVjZXNzb3IudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLyppZiAodGFzay5SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMucmV2aXNpb25SZXZpZXdSZXF1aXJlZC5wYXRjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbWluRGF0ZU9iamVjdCA9IHRoaXMuY29tbWVudHNVdGlsU2VydmljZS5hZGRVbml0c1RvRGF0ZShuZXcgRGF0ZSh0YXNrLkRVRV9EQVRFKSwgMSwgJ2RheXMnLCB0aGlzLmVuYWJsZVdvcmtXZWVrKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWluRGF0ZSA9IG1pbkRhdGVPYmplY3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLm1heERhdGUgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUuc2V0VmFsdWUobWluRGF0ZU9iamVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmVuZERhdGUuc2V0VmFsdWUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LkRVRV9EQVRFKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWluRGF0ZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkcHJlZGVjZXNzb3IgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29tbWVudHNVdGlsU2VydmljZS5hZGRVbml0c1RvRGF0ZShuZXcgRGF0ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZHByZWRlY2Vzc29yLkRVRV9EQVRFKSwgMSwgJ2RheXMnLCB0aGlzLmVuYWJsZVdvcmtXZWVrKSA6IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5TVEFSVF9EQVRFO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcubWF4RGF0ZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5EVUVfREFURTtcclxuICAgICAgICAgICAgICAgIGxldCBzZWxlY3RlZFByZWRlY2Vzc29yO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaykge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkUHJlZGVjZXNzb3IgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyeVBhY2thZ2VDb25maWcuZmlsdGVyT3B0aW9ucy5maW5kKHByZWRlY2Vzc29yVmFsdWUgPT4gcHJlZGVjZXNzb3JWYWx1ZS5kaXNwbGF5TmFtZSA9PT0gcHJlZGVjZXNzb3IpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFByZWRlY2Vzc29yID0gdGhpcy50YXNrTW9kYWxDb25maWcucHJlZGVjZXNzb3JDb25maWcuZmlsdGVyT3B0aW9ucy5maW5kKHByZWRlY2Vzc29yVmFsdWUgPT4gcHJlZGVjZXNzb3JWYWx1ZS5kaXNwbGF5TmFtZSA9PT0gcHJlZGVjZXNzb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUHJlZGVjZXNzb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnByZWRlY2Vzc29yLnNldFZhbHVlKHNlbGVjdGVkUHJlZGVjZXNzb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gaWYgKCF0aGlzLnRhc2tNb2RhbENvbmZpZy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5zdGFydERhdGUuc2V0VmFsdWUodGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlNUQVJUX0RBVEUpO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS5zZXRWYWx1ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuRFVFX0RBVEUpO1xyXG4gICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShvd25lclZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChvd25lclZhbHVlICYmIHR5cGVvZiBvd25lclZhbHVlID09PSAnb2JqZWN0JyAmJiAhdGhpcy50YXNrTW9kYWxDb25maWcuaXNUZW1wbGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zLm1hcCh0YXNrT3duZXJOYW1lID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2tPd25lck5hbWUudmFsdWUgPT09IG93bmVyVmFsdWUudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGFza093bmVyQXNzaWdubWVudENvdW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vd25lclByb2plY3RDb3VudE1lc3NhZ2UgPSAnJztcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZE93bmVyID0gdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChvd25lciA9PiBvd25lci5kaXNwbGF5TmFtZSA9PT0gb3duZXJWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkT3duZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5zZXRWYWx1ZShzZWxlY3RlZE93bmVyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmFsbG9jYXRpb24udmFsdWVDaGFuZ2VzLnN1YnNjcmliZShhbGxvY2F0aW9uVHlwZVZhbHVlID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mQWxsb2NhdGlvblR5cGUoYWxsb2NhdGlvblR5cGVWYWx1ZSwgbnVsbCwgbnVsbCwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8qIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcnMudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShkZWxpdmVyYWJsZVJldmlld2VycyA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRlbGl2ZXJhYmxlUmV2aWV3ZXJzKTtcclxuICAgICAgICB9KTsgKi9cclxuXHJcbiAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUocm9sZSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyb2xlICYmIHR5cGVvZiByb2xlID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mUm9sZShyb2xlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZSA9IHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tSb2xlQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZCh0YXNrUm9sZSA9PiB0YXNrUm9sZS5kaXNwbGF5TmFtZSA9PT0gcm9sZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLnNldFZhbHVlKHNlbGVjdGVkUm9sZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0udmFsdWUub3duZXIgJiYgdGhpcy50YXNrRm9ybS52YWx1ZS5vd25lci52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLnRhc2tPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zLm1hcCh0YXNrT3duZXJOYW1lID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRhc2tPd25lck5hbWUudmFsdWUgPT09IHRoaXMudGFza0Zvcm0udmFsdWUub3duZXIudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0VGFza093bmVyQXNzaWdubWVudENvdW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vd25lclByb2plY3RDb3VudE1lc3NhZ2UgPSAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRhdGVWYWxpZGF0aW9uICYmIHRoaXMuZGF0ZVZhbGlkYXRpb24uc3RhcnRNYXhEYXRlICYmIG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMuZGF0ZVZhbGlkYXRpb24uc3RhcnRNYXhEYXRlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZUVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5ERUxJVkVSQUJMRV9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZUVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5QUk9KRUNUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAobmV3IERhdGUodGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlKSA+IG5ldyBEYXRlKHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5EVUVfREFURSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmR1cmF0aW9uRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLkRVUkFUSU9OX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZW5hYmxlRHVyYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRlRHVyYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRlVmFsaWRhdGlvbiAmJiB0aGlzLmRhdGVWYWxpZGF0aW9uLnN0YXJ0TWF4RGF0ZSAmJiBuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSkgPiBuZXcgRGF0ZSh0aGlzLmRhdGVWYWxpZGF0aW9uLnN0YXJ0TWF4RGF0ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGVFcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuUFJPSkVDVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgfSk7XHJcblxyXG5cclxuXHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmVuZERhdGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS52YWx1ZS5vd25lciAmJiB0aGlzLnRhc2tGb3JtLnZhbHVlLm93bmVyLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMubWFwKHRhc2tPd25lck5hbWUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGFza093bmVyTmFtZS52YWx1ZSA9PT0gdGhpcy50YXNrRm9ybS52YWx1ZS5vd25lci52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRUYXNrT3duZXJBc3NpZ25tZW50Q291bnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm93bmVyUHJvamVjdENvdW50TWVzc2FnZSA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZGF0ZVZhbGlkYXRpb24gJiYgdGhpcy5kYXRlVmFsaWRhdGlvbi5lbmRNaW5EYXRlICYmIG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkgPCBuZXcgRGF0ZSh0aGlzLmRhdGVWYWxpZGF0aW9uLmVuZE1pbkRhdGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRlRXJyb3JNZXNzYWdlID0gdGhpcy50YXNrQ29uc3RhbnRzLkRFTElWRVJBQkxFX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobmV3IERhdGUodGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlKSA8IG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZUVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5TVEFSVF9EQVRFX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZUVycm9yTWVzc2FnZSA9IHRoaXMudGFza0NvbnN0YW50cy5QUk9KRUNUX0RBVEVfRVJST1JfTUVTU0FHRTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmVuYWJsZUR1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWVDaGFuZ2VzLnBpcGUoZGlzdGluY3RVbnRpbENoYW5nZWQoKSkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZSAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSAmJiB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52YWxpZGF0ZUR1cmF0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uVHlwZS52YWx1ZUNoYW5nZXMucGlwZShkaXN0aW5jdFVudGlsQ2hhbmdlZCgpKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuc3RhcnREYXRlLnZhbHVlICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24gJiYgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbi52YWx1ZSkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRlRHVyYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcuaGFzQWxsQ29uZmlnID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBvblRhc2tTZWxlY3Rpb24oKSB7XHJcbiAgICAgICAgY29uc3Qgcm9sZXMgPSBbXTtcclxuICAgICAgICBjb25zdCByb2xlSWRzID0gW107XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc0FwcHJvdmFsVGFzaykge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LmFwcFJvbGVzLlJPTEVfRE4uc2VhcmNoKFJvbGVDb25zdGFudHMuUFJPSkVDVF9NQU5BR0VSKSAhPT0gLTEgfHwgZWxlbWVudC5hcHBSb2xlcy5ST0xFX0ROLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfQVBQUk9WRVIpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUlkcy5wdXNoKGVsZW1lbnQubmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVhbVJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuYXBwUm9sZXMuUk9MRV9ETi5zZWFyY2goUm9sZUNvbnN0YW50cy5QUk9KRUNUX01BTkFHRVIpICE9PSAtMSB8fCBlbGVtZW50LmFwcFJvbGVzLlJPTEVfRE4uc2VhcmNoKFJvbGVDb25zdGFudHMuUFJPSkVDVF9NRU1CRVIpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUlkcy5wdXNoKGVsZW1lbnQubmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zID0gcm9sZXM7XHJcbiAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgpLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICB2YWxpZGF0ZUR1cmF0aW9uKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUgPCAxIHx8IHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUgPiA5OTkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHVyYXRpb25FcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuRFVSQVRJT05fTUlOX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZW5kRGF0ZU9iamVjdCA9IHRoaXMuY29tbWVudHNVdGlsU2VydmljZS5hZGRVbml0c1RvRGF0ZShuZXcgRGF0ZSh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnN0YXJ0RGF0ZS52YWx1ZSksIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb25UeXBlLnZhbHVlID09PSAnd2Vla3MnID8gKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUpIDogKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZHVyYXRpb24udmFsdWUgLSAxKSwgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvblR5cGUudmFsdWUsIHRoaXMuZW5hYmxlV29ya1dlZWspO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnNldFZhbHVlKGVuZERhdGVPYmplY3QpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbi5zZXRWYWxpZGF0b3JzKFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heCg5OTkpLCBWYWxpZGF0b3JzLm1pbigxKV0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbi51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrTW9kYWxDb25maWcgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrICYmIHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5NQVhfREVMSVZFUkFCTEVfREFURSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChuZXcgRGF0ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suTUFYX0RFTElWRVJBQkxFX0RBVEUpID49IG5ldyBEYXRlKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZW5kRGF0ZS52YWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbWluRHVyYXRpb24gPSB0aGlzLmNvbW1lbnRzVXRpbFNlcnZpY2UuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suTUFYX0RFTElWRVJBQkxFX0RBVEUpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3IERhdGUodGhpcy50YXNrRm9ybS5jb250cm9scy5lbmREYXRlLnZhbHVlKSwgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvblR5cGUudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmR1cmF0aW9uLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4KDk5OSksIFZhbGlkYXRvcnMubWluKG1pbkR1cmF0aW9uICsgMSldKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kdXJhdGlvbi51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZHVyYXRpb25FcnJvck1lc3NhZ2UgPSB0aGlzLnRhc2tDb25zdGFudHMuREVMSVZFUkFCTEVfREFURV9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mQWxsb2NhdGlvblR5cGUoYWxsb2NhdGlvblR5cGVWYWx1ZSwgb3duZXJWYWx1ZSwgcm9sZVZhbHVlLCBjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcblxyXG4gICAgICAgIGlmIChhbGxvY2F0aW9uVHlwZVZhbHVlID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUuc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUucGF0Y2hWYWx1ZShyb2xlVmFsdWUgPyByb2xlVmFsdWUgOiAnJyk7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIucGF0Y2hWYWx1ZShvd25lclZhbHVlID8gb3duZXJWYWx1ZSA6ICcnKTtcclxuICAgICAgICAgICAgaWYgKGNoYW5nZURlbGl2ZXJhYmxlQXBwcm92ZXIgJiYgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKGFsbG9jYXRpb25UeXBlVmFsdWUgPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1VTRVIpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgpLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy50YXNrT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIucGF0Y2hWYWx1ZShvd25lclZhbHVlID8gb3duZXJWYWx1ZSA6ICcnKTtcclxuICAgICAgICAgICAgICAgIGlmIChjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyICYmIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2spIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZSAmJiAhdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUgJiYgIXRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25DaGFuZ2VvZlJvbGUocm9sZSkge1xyXG4gICAgICAgIGlmIChyb2xlICYmIHJvbGUudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUgJiYgIXRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZS5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIuZW5hYmxlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uY29udHJvbHMucm9sZSAmJiAhdGhpcy50YXNrRm9ybS5jb250cm9scy5yb2xlLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kYWxDb25maWcudGFza093bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza0Zvcm0uY29udHJvbHMub3duZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5vd25lci5wYXRjaFZhbHVlKChyb2xlID8gcm9sZSA6ICcnKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLm93bmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgocm9sZSA/IHJvbGUgOiAnJykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkFwcHJvdmFsU3RhdGVDaGFuZ2UoKSB7XHJcbiAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3Nvci5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgZm9ya0pvaW4oW3RoaXMuZ2V0QWxsUm9sZXMoKSwgdGhpcy5nZXREZXBlbmRlbnRUYXNrcyh0aGlzLnRhc2tDb25maWcuaXNBcHByb3ZhbFRhc2spXSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrRm9ybS5jb250cm9scy5wcmVkZWNlc3Nvci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1QcmVkZWNlc3NvclZhbHVlKHRhc2spIHtcclxuICAgICAgICBpZiAodGFzay5OQU1FICYmIHRhc2tbJ1Rhc2staWQnXSAmJiB0YXNrWydUYXNrLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRhc2tbJ1Rhc2staWQnXS5JZCArICctJyArIHRhc2suTkFNRTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVByZWRlY2Vzc29yKHRhc2tJZCkge1xyXG4gICAgICAgIGlmICh0YXNrSWQpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRQcmVkZWNlc3NvciA9IHRoaXMudGFza01vZGFsQ29uZmlnLnByZWRlY2Vzc29yQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQudmFsdWUgPT09IHRhc2tJZCk7XHJcbiAgICAgICAgICAgIHJldHVybiBzZWxlY3RlZFByZWRlY2Vzc29yID8gc2VsZWN0ZWRQcmVkZWNlc3NvciA6ICcnO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybU93bmVyVmFsdWUodGFza1VzZXJJRCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGFza1VzZXJJRCkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklkOiB0YXNrVXNlcklEXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFVzZXIgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQudmFsdWUgPT09IHJlc3BvbnNlLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkVXNlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3duZXIgPSBzZWxlY3RlZFVzZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICcnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCgnJyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVJvbGVWYWx1ZSh0YXNrUm9sZUlEKSB7XHJcbiAgICAgICAgaWYgKHRhc2tSb2xlSUQpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb2xlID0gdGhpcy50ZWFtUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IHRhc2tSb2xlSUQpO1xyXG4gICAgICAgICAgICByZXR1cm4gKHNlbGVjdGVkUm9sZSkgPyBzZWxlY3RlZFJvbGUgOiAnJztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tPd25lckFzc2lnbm1lbnRDb3VudCgpIHtcclxuICAgIH1cclxuXHJcbiAgICBjYW5jZWxUYXNrQ3JlYXRpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNsb3NlPycsXHJcbiAgICAgICAgICAgICAgICAgICAgc3VibWl0QnV0dG9uOiAnWWVzJyxcclxuICAgICAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlVGFzayhzYXZlT3B0aW9uKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyICYmIHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLmZpbmQocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IHRoaXMudGFza0Zvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyLnZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NlbGVjdGVkIHJldmlld2VyIGlzIGFscmVhZHkgdGhlIGFwcHJvdmVyJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy50YXNrRm9ybS5wcmlzdGluZSAmJiAhKHRoaXMuY2hpcENoYW5nZWQpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ0tpbmRseSBtYWtlIGNoYW5nZXMgdG8gdXBkYXRlIHRoZSB0YXNrJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuV0FSTiB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBuYW1lU3BsaXQgPSB0aGlzLnRhc2tGb3JtLnZhbHVlLnRhc2tOYW1lLnNwbGl0KCdfJyk7XHJcbiAgICAgICAgaWYgKG5hbWVTcGxpdFswXS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnTmFtZSBzaG91bGQgbm90IHN0YXJ0IHdpdGggXCJfXCInLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5XQVJOIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlVGFza1NhdmUgPSB0cnVlO1xyXG4gICAgICAgIGNvbnN0IHRhc2tFbmREYXRlV2FybmluZyA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAodGFza0VuZERhdGVXYXJuaW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0Zvcm0ucGF0Y2hWYWx1ZSh7XHJcbiAgICAgICAgICAgICAgICBlbmREYXRlOiB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suRFVFX0RBVEVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1Rhc2tcXCdzIGVuZCBkYXRlIHNob3VsZCBub3QgYmUgbGVzcyB0aGFuIHRoZSBkZWxpdmVyYWJsZXNcXCcgZHVlIGRhdGUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5XQVJOIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0Zvcm0uc3RhdHVzID09PSAnVkFMSUQnKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnRhc2tOYW1lLnZhbHVlLnRyaW0oKSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1BsZWFzZSBwcm92aWRlIGEgdmFsaWQgdGFzayBuYW1lJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GTyB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tGb3JtLmNvbnRyb2xzLnRhc2tOYW1lLnNldFZhbHVlKHRoaXMudGFza0Zvcm0uY29udHJvbHMudGFza05hbWUudmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlVGFza0RldGFpbHMoc2F2ZU9wdGlvbik7XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnS2luZGx5IGZpbGwgYWxsIG1hbmRhdG9yeSBmaWVsZHMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvcm1SZXZpZXdlck9iaigpIHtcclxuICAgICAgICBjb25zdCBhZGRlZFJldmlld2VycyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IHJlbW92ZWRSZXZpZXdlcnMgPSBbXTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMubmV3UmV2aWV3ZXJzICYmIHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMpIHtcclxuICAgICAgICAgICAgdGhpcy5uZXdSZXZpZXdlcnMuZm9yRWFjaChuZXdSZXZpZXdlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhZGRlZFJldmlld2VySW5kZXggPSBhZGRlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IG5ld1Jldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoYWRkZWRSZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhZGRlZFJldmlld2Vycy5wdXNoKG5ld1Jldmlld2VyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA9IHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMuZmluZEluZGV4KGRlbGl2ZXJhYmxlUmV2aWV3ZXIgPT4gZGVsaXZlcmFibGVSZXZpZXdlci52YWx1ZSA9PT0gbmV3UmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFkZGVkUmV2aWV3ZXJJbmRleCA9IGFkZGVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gbmV3UmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYWRkZWRSZXZpZXdlckluZGV4ID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkZWRSZXZpZXdlcnMucHVzaChuZXdSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLm5ld3Jldmlld2Vyc09iaiA9IGFkZGVkUmV2aWV3ZXJzLm1hcCgoYWRkZWRSZXZpZXdlcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICBEZWxpdmVyYWJsZUlkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICBSZXZpZXdlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEl0ZW1JZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZDogYWRkZWRSZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnQUREJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgVGFza0lkOiB0aGlzLnRhc2tDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgIElzUE1Bc3NpZ25lZFJldmlld2VyOiB0aGlzLmlzUE1cclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucmVtb3ZlZFJldmlld2VycyAmJiB0aGlzLnNhdmVkUmV2aWV3ZXJzKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlZFJldmlld2Vycy5mb3JFYWNoKHJlbW92ZWRSZXZpZXdlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zYXZlZFJldmlld2Vycy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmZpbmRJbmRleChkZWxpdmVyYWJsZVJldmlld2VyID0+IGRlbGl2ZXJhYmxlUmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSByZW1vdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1vdmVkUmV2aWV3ZXJzLnB1c2gocmVtb3ZlZFJldmlld2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2F2ZWRSZXZpZXdlckluZGV4ID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kSW5kZXgoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzYXZlZFJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlbW92ZWRSZXZpZXdlckluZGV4ID0gcmVtb3ZlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA9IHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMuZmluZEluZGV4KGRhdGEgPT4gZGF0YS52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlbW92ZWRSZXZpZXdlckluZGV4ID09PSAtMSAmJiBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlZFJldmlld2Vycy5wdXNoKHJlbW92ZWRSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZWRyZXZpZXdlcnNPYmogPSByZW1vdmVkUmV2aWV3ZXJzLm1hcCgocmV2aWV3ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVJZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgUmV2aWV3ZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBJdGVtSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb246ICdSRU1PVkUnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBUYXNrSWQ6IHRoaXMudGFza0NvbmZpZy50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgSXNQTUFzc2lnbmVkUmV2aWV3ZXI6IHRoaXMuaXNQTVxyXG5cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRSZXZpZXdlcnNPYmogPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLm1hcCgocmV2aWV3ZXIpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIElkOiAnJyxcclxuICAgICAgICAgICAgICAgIEl0ZW1JZDogJycsXHJcbiAgICAgICAgICAgICAgICB1c2VySUQ6IHJldmlld2VyLnZhbHVlXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMucmV2aWV3ZXJzT2JqID0gdGhpcy5uZXdyZXZpZXdlcnNPYmogPyB0aGlzLm5ld3Jldmlld2Vyc09iai5jb25jYXQodGhpcy5yZW1vdmVkcmV2aWV3ZXJzT2JqKSA6IHRoaXMucmVtb3ZlZHJldmlld2Vyc09iai5jb25jYXQodGhpcy5uZXdyZXZpZXdlcnNPYmopO1xyXG4gICAgfVxyXG4gICAgdXBkYXRlVGFza0RldGFpbHMoc2F2ZU9wdGlvbikge1xyXG4gICAgICAgIGNvbnN0IHRhc2tGb3JtVmFsdWVzID0gdGhpcy50YXNrRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgIHRoaXMuZm9ybVJldmlld2VyT2JqKCk7XHJcbiAgICAgICAgY29uc3QgVGFza09iamVjdDogVGFza1VwZGF0ZU9iaiA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmNyZWF0ZU5ld1Rhc2tVcGRhdGVPYmplY3QodGFza0Zvcm1WYWx1ZXMsIHRoaXMudGFza0NvbmZpZy50YXNrVHlwZSwgdGhpcy50YXNrQ29uZmlnLmlzQXBwcm92YWxUYXNrLFxyXG4gICAgICAgICAgICB0aGlzLnRhc2tNb2RhbENvbmZpZy5wcm9qZWN0SWQsIHRoaXMuc2VsZWN0ZWRSZXZpZXdlcnNPYmopO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2sgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrWydUYXNrLWlkJ11cclxuICAgICAgICAgICAgJiYgdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrWydUYXNrLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgVGFza09iamVjdC5UYXNrSWQuSWQgPSB0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2tbJ1Rhc2staWQnXS5JZDtcclxuICAgICAgICAgICAgVGFza09iamVjdC5SZXZpc2lvblJldmlld1JlcXVpcmVkID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQpXHJcbiAgICAgICAgICAgICAgICA/ICcnIDogdGhpcy50YXNrTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJFVklTSU9OX1JFVklFV19SRVFVSVJFRDtcclxuICAgICAgICAgICAgVGFza09iamVjdC5Jc01pbGVzdG9uZSA9IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suSVNfTUlMRVNUT05FKVxyXG4gICAgICAgICAgICAgICAgPyAnJyA6IHRoaXMudGFza01vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5JU19NSUxFU1RPTkU7XHJcbiAgICAgICAgICAgIFRhc2tPYmplY3QuSXNNaWxlc3RvbmUgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGFza0Zvcm1WYWx1ZXMuaXNNaWxlc3RvbmUpXHJcbiAgICAgICAgICAgICAgICA/ICcnIDogdGFza0Zvcm1WYWx1ZXMuaXNNaWxlc3RvbmU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNhdmVUYXNrKFRhc2tPYmplY3QsIHNhdmVPcHRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIHNhdmVUYXNrKFRhc2tPYmplY3QsIHNhdmVPcHRpb24pIHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgIGlmICh0aGlzLnRhc2tNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2spIHtcclxuICAgICAgICAgICAgVGFza09iamVjdCA9IHRoaXMucHJvamVjdFV0aWxTZXJ2aWNlLmNvbXBhcmVUd29Qcm9qZWN0RGV0YWlscyhUYXNrT2JqZWN0LCB0aGlzLm9sZFRhc2tPYmplY3QpO1xyXG4gICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMoVGFza09iamVjdCkubGVuZ3RoID4gMSB8fCAoSlNPTi5zdHJpbmdpZnkodGhpcy5kZWxpdmVyYWJsZVJldmlld2VycykgIT09IEpTT04uc3RyaW5naWZ5KHRoaXMuc2F2ZWRSZXZpZXdlcnMpKSkge1xyXG4gICAgICAgICAgICAgICAgVGFza09iamVjdC5CdWxrT3BlcmF0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXF1ZXN0T2JqID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIFRhc2tPYmplY3Q6IFRhc2tPYmplY3QsXHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVSZXZpZXdlcnM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVSZXZpZXdlcjogdGhpcy5yZXZpZXdlcnNPYmpcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS51cGRhdGVUYXNrKHJlcXVlc3RPYmopLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdUYXNrIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5JywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVDYWxsQmFja0hhbmRsZXIubmV4dCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXNrSWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuVGFza1snVGFzay1pZCddLklkLCBwcm9qZWN0SWQ6IHRoaXMudGFza01vZGFsQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzVGVtcGxhdGU6IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUsIHNhdmVPcHRpb246IHNhdmVPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnNTAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvckNvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIHRhc2snLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHVwZGF0aW5nIHRhc2snLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgcmVxdWVzdE9iaiA9IHtcclxuICAgICAgICAgICAgICAgIFRhc2tPYmplY3Q6IFRhc2tPYmplY3RcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5jcmVhdGVUYXNrKHJlcXVlc3RPYmopXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZVRhc2tTYXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5UYXNrICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuVGFza1snVGFzay1pZCddICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuVGFza1snVGFzay1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1Rhc2sgc2F2ZWQgc3VjY2Vzc2Z1bGx5JywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVDYWxsQmFja0hhbmRsZXIubmV4dCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YXNrSWQ6IHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuVGFza1snVGFzay1pZCddLklkLCBwcm9qZWN0SWQ6IHRoaXMudGFza01vZGFsQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzVGVtcGxhdGU6IHRoaXMudGFza01vZGFsQ29uZmlnLmlzVGVtcGxhdGUsIHNhdmVPcHRpb246IHNhdmVPcHRpb25cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnNTAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvckNvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIGNyZWF0aW5nIHRhc2snLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5TVUNDRVNTIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgb24gd2hpbGUgdXBkYXRpbmcgdGFzaycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVUYXNrU2F2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGluaXRpYWxpc2VUYXNrQ29uZmlnKCkge1xyXG4gICAgICAgIHRoaXMudGFza01vZGFsQ29uZmlnID0ge1xyXG4gICAgICAgICAgICBoYXNBbGxDb25maWc6IGZhbHNlLFxyXG4gICAgICAgICAgICB0YXNrU3RhdHVzTGlzdDogbnVsbCxcclxuICAgICAgICAgICAgdGFza0luaXRpYWxTdGF0dXNMaXN0OiBudWxsLFxyXG4gICAgICAgICAgICB0YXNrUHJpb3JpdHlMaXN0OiBudWxsLFxyXG4gICAgICAgICAgICBwcmVkZWNlc3NvcjogbnVsbCxcclxuICAgICAgICAgICAgdGFza0xpc3Q6IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdCxcclxuICAgICAgICAgICAgc2VsZWN0ZWRQcm9qZWN0OiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0LFxyXG4gICAgICAgICAgICBkZWZhdWx0U3RhdHVzOiBudWxsLFxyXG4gICAgICAgICAgICBkZWZhdWx0UHJpb3JpdHk6IG51bGwsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkU3RhdHVzSXRlbUlkOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5EYXRlOiAnJyxcclxuICAgICAgICAgICAgbWF4RGF0ZTogJycsXHJcbiAgICAgICAgICAgIGlzTmV3VGFzazogKHRoaXMudGFza0NvbmZpZy50YXNrSWQpID8gZmFsc2UgOiB0cnVlLFxyXG4gICAgICAgICAgICB0YXNrSWQ6IHRoaXMudGFza0NvbmZpZy50YXNrSWQsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkVGFzazogdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkVGFzayxcclxuICAgICAgICAgICAgc2VsZWN0ZWRwcmVkZWNlc3NvcjogbnVsbCxcclxuICAgICAgICAgICAgaXNUZW1wbGF0ZTogdGhpcy50YXNrQ29uZmlnLmlzVGVtcGxhdGUsXHJcbiAgICAgICAgICAgIHByb2plY3RJZDogdGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgdGFza093bmVyQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1Rhc2sgT3duZXInLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGFza1JvbGVDb25maWc6IHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnUm9sZScsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwcmVkZWNlc3NvckNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdQcmVkZWNlc3NvcicsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnRGVsaXZlcmFibGUgQXBwcm92ZXInLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZVJldmlld2VyOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJhYmxlIFJldmlld2VyJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsaXZlcnlQYWNrYWdlQ29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJ5IFBhY2thZ2UnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB0YXNrQXNzaWdubWVudFR5cGVzOiBudWxsLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgcHJvamVjdENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UHJvamVjdENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlRHVyYXRpb24gPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5FTkFCTEVfRFVSQVRJT05dKTtcclxuICAgICAgICB0aGlzLmVuYWJsZVdvcmtXZWVrID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUocHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX1dPUktfV0VFS10pO1xyXG4gICAgICAgIHRoaXMuc2hvd1JldmlzaW9uUmV2aWV3UmVxdWlyZWQgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZShwcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5TSE9XX1JFVklTSU9OX1JFVklFV19SRVFVSVJFRF0pO1xyXG4gICAgICAgIHRoaXMucHJvamVjdENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UHJvamVjdENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICB0aGlzLmRlZmF1bHRSZXZpc2lvblJlcXVpcmVkU3RhdHVzID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5wcm9qZWN0Q29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9QUk9KRUNUX0NPTkZJRy5ERUZBVUxUX1JFVklTSU9OX1JFUVVJUkVEX1NUQVRVU10pO1xyXG4gICAgICAgIHRoaXMubmFtZVN0cmluZ1BhdHRlcm4gPSB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuTkFNRV9TVFJJTkdfUEFUVEVSTl0pID8gJy4qJyA6XHJcbiAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dO1xyXG4gICAgICAgIHRoaXMuYWxsb3dBY3Rpb25UYXNrID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuQUxMT1dfQUNUSU9OX1RBU0tdKTtcclxuICAgICAgICB0aGlzLmxvZ2dlZEluVXNlciA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJPYmplY3QoKTtcclxuICAgICAgICB0aGlzLmxvZ2dlZEluVXNlci5NYW5hZ2VyRm9yLlRhcmdldC5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodXNlci5OYW1lID09PSAnTVBNIFByb2plY3QgTWFuYWdlcicpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNQTSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBjb25zdCBzdGF0dXNlcyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdDb21tZW50cycpO1xyXG4gICAgICAgIGlmIChuYXZpZ2F0b3IubGFuZ3VhZ2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFkYXB0ZXIuc2V0TG9jYWxlKG5hdmlnYXRvci5sYW5ndWFnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tDb25maWcgJiYgdGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VUYXNrQ29uZmlnKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJlRGV0YWlscygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkYXRlRmlsdGVyOiAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IGJvb2xlYW4gPVxyXG4gICAgICAgIChkYXRlOiBEYXRlIHwgbnVsbCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5lbmFibGVXb3JrV2Vlaykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXkgPSBkYXRlLmdldERheSgpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRheSAhPT0gMCAmJiBkYXkgIT09IDY7XHJcbiAgICAgICAgICAgICAgICAvLzAgbWVhbnMgc3VuZGF5XHJcbiAgICAgICAgICAgICAgICAvLzYgbWVhbnMgc2F0dXJkYXlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxufVxyXG4iXX0=