import { __decorate, __read, __spread } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { StatusTypes } from '../../../mpm-utils/objects/StatusType';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { NotificationService } from '../../../notification/notification.service';
import { ConfirmationModalComponent } from '../../../shared/components/confirmation-modal/confirmation-modal.component';
import { StatusService } from '../../../shared/services/status.service';
import { ProjectUtilService } from '../../shared/services/project-util.service';
import { TaskService } from '../task.service';
var CustomWorkflowFieldComponent = /** @class */ (function () {
    function CustomWorkflowFieldComponent(statusService, sharingService, taskService, notificationService, projectUtilService, dialog) {
        this.statusService = statusService;
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.notificationService = notificationService;
        this.projectUtilService = projectUtilService;
        this.dialog = dialog;
        this.closeCallbackHandler = new EventEmitter();
        this.removeWorkflowRuleHandler = new EventEmitter();
        this.enableInitiateWorkflowHandler = new EventEmitter();
        //@Output() isTaskLevelRuleEdit = new EventEmitter<any>();
        this.istaskRuleEdit = false;
        this.ruleGroupHandler = new EventEmitter();
        this.ruleDependencyErrorHandler = new EventEmitter();
        //@Input() allRuleListGroup; MVSS-322
        this.isChildDependecies = false;
        this.triggerTypes = [{
                name: 'Status',
                value: 'STATUS'
            }, {
                name: 'Action',
                value: 'ACTION'
            }];
        this.targetTypes = [{
                name: 'Task',
                value: 'TASK'
            }, {
                name: 'Event',
                value: 'EVENT'
            }];
        this.triggerData = [];
        this.targetData = [];
        this.taskList = [];
        this.allRuleList = [];
        this.currentSourceTasks = [];
        this.noPredecessorRuleList = [];
        this.httpRequestArray = [];
    }
    //for Editing workflow Rules...
    CustomWorkflowFieldComponent.prototype.updateWorkflowRule = function () {
        var _this = this;
        if (this.ruleData.currentTask.length > this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            //Removing a rule from the source tasks list
            var newTasks_1 = [];
            this.ruleData.currentTask.forEach(function (task) {
                var index = _this.customWorkflowFieldForm.getRawValue().currentTask.indexOf(task);
                if (index < 0) {
                    newTasks_1.push(task);
                }
            });
            var deletedRules_1 = [];
            newTasks_1.forEach(function (task) {
                var rule = {
                    currentTask: task,
                    isInitialRule: _this.ruleData.isInitialRule,
                    targetData: _this.ruleData.targetData,
                    targetType: _this.ruleData.targetType,
                    triggerData: _this.ruleData.triggerData,
                    triggerType: _this.ruleData.triggerType
                };
                deletedRules_1.push(rule);
            });
            var originalRules_1 = [];
            for (var i = 0; i < this.ruleData.currentTask.length; i++) {
                var rule = {
                    currentTask: this.ruleData.currentTask[i],
                    isInitialRule: this.ruleData.isInitialRule,
                    targetData: this.ruleData.targetData,
                    targetType: this.ruleData.targetType,
                    triggerData: this.ruleData.triggerData,
                    triggerType: this.ruleData.triggerType,
                    workflowRuleId: this.ruleData.workflowRuleId[i]
                };
                originalRules_1.push(rule);
            }
            var deletedRuleIds_1 = [];
            deletedRules_1.forEach(function (rule) {
                originalRules_1.forEach(function (eachRule) {
                    if (rule.currentTask == eachRule.currentTask) {
                        deletedRuleIds_1.push(eachRule.workflowRuleId);
                    }
                });
            });
            var otherParamsChanged = ((this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType)
                || (this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData)
                || (this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType)
                || (this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData)
                || (this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule));
            if (otherParamsChanged) {
                for (var i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                    var rule = this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                    this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i]));
                }
            }
            var approvalTasks = this.taskData.filter(function (task) { return task.IS_APPROVAL_TASK === 'true'; });
            var removedParentTasks_1 = [];
            approvalTasks.forEach(function (approvalTask) {
                var index = newTasks_1.indexOf(String(approvalTask.TASK_PARENT_ID));
                if (index >= 0) {
                    removedParentTasks_1.push(newTasks_1[index]);
                }
            });
            //checking if the removed task is a parent task hence checking dependencies...
            /************************************************************************************************* */
            var approvalTasksIdList_1 = [];
            removedParentTasks_1.forEach(function (eachTask) {
                var approvalTaskIds = _this.taskData.filter(function (task) { return String(task.TASK_PARENT_ID) == eachTask; });
                approvalTasksIdList_1.push.apply(approvalTasksIdList_1, __spread(approvalTaskIds));
            });
            var allTasksinRules_1 = [];
            this.ruleListGroup.forEach(function (rule) {
                allTasksinRules_1.push.apply(allTasksinRules_1, __spread(rule.currentTask));
                allTasksinRules_1.push(rule.targetData);
            });
            var affectedApprovalTasks_1 = [];
            approvalTasksIdList_1.forEach(function (eachTask) {
                var index = allTasksinRules_1.indexOf(eachTask.ID);
                if (index >= 0) {
                    affectedApprovalTasks_1.push(eachTask);
                }
            });
            this.ruleListGroup.forEach(function (rule) { return rule.hasPredecessor = true; });
            this.ruleListGroup.forEach(function (rule) {
                var ruleTasks = [];
                ruleTasks.push.apply(ruleTasks, __spread(rule.currentTask));
                ruleTasks.push(rule.targetData);
                affectedApprovalTasks_1.forEach(function (eachTask) {
                    var index = ruleTasks.indexOf(eachTask.ID);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                    }
                });
            });
            /**************************************************************************************************************** */
            if (!(affectedApprovalTasks_1 && affectedApprovalTasks_1.length > 0)) {
                var ruleIds_1 = [];
                deletedRuleIds_1.forEach(function (workflowId) {
                    var Rule = {
                        Id: workflowId
                    };
                    ruleIds_1.push(Rule);
                });
                this.taskService.removeWorkflowRule(ruleIds_1, {}).subscribe(function (response) {
                    if (response) {
                        // this.removeWorkflowRuleHandler.next(this.ruleData);
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (task) { return _this.currentSourceTasks.push(task); });
                        _this.ruleData.currentTask = _this.customWorkflowFieldForm.controls.currentTask.value;
                        _this.isRuleSaved = true;
                        _this.enableInitiateWorkflowHandler.next();
                        _this.notificationService.info('Workflow Rule has been removed');
                    }
                    else {
                        _this.notificationService.error('Something went wrong while removing workflow rule');
                    }
                });
            }
            else {
                this.notificationService.error('Edit Operation could not be Performed because It is a Parent Task and it Will affect the highlighted rules');
            }
        }
        else if (this.ruleData.currentTask.length < this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            //Adding a rule to the source tasks list
            var newTasks_2 = [];
            this.customWorkflowFieldForm.getRawValue().currentTask.forEach(function (task) {
                var index = _this.ruleData.currentTask.indexOf(task);
                if (index < 0) {
                    newTasks_2.push(task);
                }
            });
            this.httpRequestArray = [];
            if ((this.customWorkflowFieldForm.getRawValue().triggerType !== this.ruleData.triggerType)
                || (this.customWorkflowFieldForm.getRawValue().triggerData !== this.ruleData.triggerData)
                || (this.customWorkflowFieldForm.getRawValue().targetType !== this.ruleData.targetType)
                || (this.customWorkflowFieldForm.getRawValue().targetData !== this.ruleData.targetData)
                || (this.customWorkflowFieldForm.getRawValue().isInitialRule !== this.ruleData.isInitialRule)) {
                for (var i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                    var rule = this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                    this.httpRequestArray.push(this.taskService.createWorkflowRule(rule, this.projectId, this.ruleData.workflowRuleId[i]));
                }
            }
            else {
                newTasks_2.forEach(function (task) {
                    var rule = _this.customWorkflowFieldForm.getRawValue();
                    rule.currentTask = task;
                    _this.httpRequestArray.push(_this.taskService.createWorkflowRule(rule, _this.projectId, ''));
                });
            }
        }
    };
    CustomWorkflowFieldComponent.prototype.updateValidation = function () {
        var allSourceTasks = [];
        if (!this.isTaskLevelcalled) {
            this.ruleList.forEach(function (rule) {
                if (!rule.isInitialRule) {
                    allSourceTasks.push.apply(allSourceTasks, __spread(rule.currentTask));
                }
            });
        }
        else {
            this.taskruleListGroup.forEach(function (rule) {
                if (!rule.isInitialRule) {
                    allSourceTasks.push.apply(allSourceTasks, __spread(rule.currentTask));
                }
            });
        }
        var index = allSourceTasks.indexOf(this.ruleData.targetData);
        if (index >= 0) {
            this.customWorkflowFieldForm.controls.currentTask.setValue(this.ruleData.currentTask);
            this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            this.isRuleSaved = true;
            this.notificationService.error("Cannot edit targetTask as it has dependencies");
        }
        else if (JSON.stringify(this.ruleData) !== JSON.stringify(this.customWorkflowFieldForm.getRawValue() && !this.isTaskLevelcalled)
            && this.ruleData.isExist
            && this.ruleData.currentTask.length != this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            this.updateWorkflowRule();
        }
        else if (!this.isTaskLevelcalled) {
            this.httpRequestArray = [];
            for (var i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                var workflowRule = this.customWorkflowFieldForm.getRawValue();
                workflowRule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                this.httpRequestArray.push(this.taskService.createWorkflowRule(workflowRule, this.projectId, this.ruleData.workflowRuleId[i]));
            }
        }
    };
    CustomWorkflowFieldComponent.prototype.checkTaskDependencies = function () {
        var allSourceTasks = [];
        this.taskruleListGroup.forEach(function (rule) {
            if (!rule.isInitialRule) {
                allSourceTasks.push.apply(allSourceTasks, __spread(rule.currentTask));
            }
        });
        var index = allSourceTasks.indexOf(this.ruleData.targetData);
        if (index >= 0) {
            this.customWorkflowFieldForm.controls.currentTask.setValue(this.ruleData.currentTask);
            this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            this.isRuleSaved = true;
            this.notificationService.error("Cannot edit targetTask as it has dependencies");
            this.isChildDependecies = true;
        }
        return this.isChildDependecies;
    };
    CustomWorkflowFieldComponent.prototype.addWorkflowRule = function () {
        var _this = this;
        if (this.isTaskLevelcalled && !this.checkTaskDependencies()) {
            this.httpRequestArray = [];
            for (var i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                var workflowFlowObject = [];
                workflowFlowObject.push({
                    currentTask: this.customWorkflowFieldForm.controls.currentTask.value[i],
                    triggerType: this.customWorkflowFieldForm.controls.triggerType.value,
                    targetType: this.customWorkflowFieldForm.controls.targetType.value,
                    triggerData: this.customWorkflowFieldForm.controls.triggerData.value,
                    targetData: this.customWorkflowFieldForm.controls.targetData.value,
                    isInitialRule: this.customWorkflowFieldForm.controls.isInitialRule.value,
                });
                this.httpRequestArray.push(this.taskService.createWorkflowRule(JSON.parse(JSON.stringify(workflowFlowObject[0])), this.projectId, this.ruleData.workflowRuleId[i]));
                // this.isRuleSaved= true
                // this.istaskRuleEdit=true
                // this.enableInitiateWorkflowHandler.next(this.istaskRuleEdit);
            }
        }
        else if (this.ruleData.targetData !== this.customWorkflowFieldForm.getRawValue().targetData && this.ruleData.isExist && !this.isTaskLevelcalled) {
            this.updateValidation();
        }
        else if (JSON.stringify(this.ruleData) !== JSON.stringify(this.customWorkflowFieldForm.getRawValue() && !this.isTaskLevelcalled)
            && this.ruleData.isExist
            && this.ruleData.currentTask.length != this.customWorkflowFieldForm.getRawValue().currentTask.length) {
            this.updateWorkflowRule();
        }
        else if (!this.isTaskLevelcalled) {
            this.httpRequestArray = [];
            for (var i = 0; i < this.customWorkflowFieldForm.getRawValue().currentTask.length; i++) {
                var workflowRule = this.customWorkflowFieldForm.getRawValue();
                workflowRule.currentTask = this.customWorkflowFieldForm.getRawValue().currentTask[i];
                this.httpRequestArray.push(this.taskService.createWorkflowRule(workflowRule, this.projectId, this.ruleData.workflowRuleId[i]));
            }
        }
        forkJoin(this.httpRequestArray).subscribe(function (response) {
            if (response) {
                _this.ruleData.currentTask = [_this.customWorkflowFieldForm.getRawValue().currentTask];
                _this.ruleData.triggerType = _this.customWorkflowFieldForm.getRawValue().triggerType;
                _this.ruleData.triggerData = _this.customWorkflowFieldForm.getRawValue().triggerData;
                _this.ruleData.targetType = _this.customWorkflowFieldForm.getRawValue().targetType;
                _this.ruleData.targetData = _this.customWorkflowFieldForm.getRawValue().targetData;
                _this.ruleData.isInitialRule = _this.customWorkflowFieldForm.getRawValue().isInitialRule;
                _this.ruleData.isRuleAdded = true;
                _this.ruleData.workflowRuleId = [];
                for (var i = 0; i < response.length; i++) {
                    _this.ruleData.workflowRuleId.push(response[i]['Workflow_Rules-id'] && response[i]['Workflow_Rules-id'].Id ? response[i]['Workflow_Rules-id'].Id : null);
                }
                _this.ruleGroupHandler.next();
                _this.isRuleSaved = true;
                if (!_this.isTaskLevelcalled) {
                    _this.enableInitiateWorkflowHandler.next(_this.istaskRuleEdit);
                }
                else {
                    _this.istaskRuleEdit = true;
                    _this.enableInitiateWorkflowHandler.next(_this.istaskRuleEdit);
                }
                //   else if(this.isTaskLevelcalled){
                //     this.istaskRuleEdit= true
                //     this.isTaskLevelRuleEdit.emit(this.istaskRuleEdit)
                //   }
                _this.notificationService.info('Workflow Rule has been saved');
            }
            else {
                _this.notificationService.error('Something went wrong while creating workflow rule');
            }
        });
    };
    CustomWorkflowFieldComponent.prototype.checkRuleDependencies1 = function () {
        var removedRuleTargetTask = this.ruleData.targetData;
        if (this.ruleData.targetType === 'TASK' && !this.isTaskLevelcalled) {
            var sourceTasks_1 = new Set();
            var sourceTasksArray = [];
            //this.allRuleListGroup.forEach(rule => {
            this.ruleListGroup.forEach(function (rule) {
                if (!rule.isInitialRule) {
                    rule.currentTask.forEach(function (task) { return sourceTasks_1.add(task); });
                }
            });
            sourceTasksArray = Array.from(sourceTasks_1);
            var noPredecessorRuleListArray_1 = [];
            var count_1 = 0;
            // this.allRuleListGroup.forEach(rule => rule.hasPredecessor = true);
            this.ruleListGroup.forEach(function (rule) { return rule.hasPredecessor = true; });
            var targetIndex = sourceTasksArray.indexOf(removedRuleTargetTask);
            if (targetIndex >= 0) {
                // this.allRuleListGroup.forEach(rule => {
                this.ruleListGroup.forEach(function (rule) {
                    var index = rule.currentTask.indexOf(removedRuleTargetTask);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                        noPredecessorRuleListArray_1.push(rule);
                        count_1 += 1;
                    }
                });
            }
            this.noPredecessorRuleList = count_1 <= 0 ? [] : noPredecessorRuleListArray_1;
            if (this.ruleData.targetTask !== "") {
                // this.ruleDependencyErrorHandler.next(this.allRuleListGroup);
                this.ruleDependencyErrorHandler.next(this.ruleListGroup);
            }
        }
        else if (this.ruleData.targetType === 'TASK' && this.isTaskLevelcalled) {
            var sourceTasks_2 = new Set();
            var sourceTasksArray = [];
            this.taskruleListGroup.forEach(function (rule) {
                if (!rule.isInitialRule) {
                    rule.currentTask.forEach(function (task) { return sourceTasks_2.add(task); });
                }
            });
            sourceTasksArray = Array.from(sourceTasks_2);
            var noPredecessorRuleListArray_2 = [];
            var count_2 = 0;
            this.taskruleListGroup.forEach(function (rule) { return rule.hasPredecessor = true; });
            var targetIndex = sourceTasksArray.indexOf(removedRuleTargetTask);
            if (targetIndex >= 0) {
                this.taskruleListGroup.forEach(function (rule) {
                    var index = rule.currentTask.indexOf(removedRuleTargetTask);
                    if (index >= 0) {
                        rule.hasPredecessor = false;
                        noPredecessorRuleListArray_2.push(rule);
                        count_2 += 1;
                    }
                });
            }
            this.noPredecessorRuleList = count_2 <= 0 ? [] : noPredecessorRuleListArray_2;
            if (this.ruleData.targetTask !== "") {
                //  this.ruleDependencyErrorHandler.next(this.allRuleListGroup);
                this.ruleDependencyErrorHandler.next(this.ruleListGroup);
            }
        }
    };
    CustomWorkflowFieldComponent.prototype.validateRemoveWorkflowRule = function () {
        var _this = this;
        // let ruleCountWithSameTargetTask = 0;
        // this.ruleListGroup.forEach(rule => {
        //   if(rule.targetData == this.ruleData.targetData){
        //     ruleCountWithSameTargetTask ++;
        //   }
        // })
        this.checkRuleDependencies1();
        if ((this.noPredecessorRuleList.length <= 0)) {
            if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
                this.ruleListGroup.forEach(function (rule) { return rule.hasPredecessor = true; });
                this.removeWorkflowRule();
            }
            else {
                var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                    width: '40%',
                    disableClose: true,
                    data: {
                        message: 'Are you sure you want to remove the Rule?',
                        submitButton: 'Yes',
                        cancelButton: 'No'
                    }
                });
                dialogRef.afterClosed().subscribe(function (result) {
                    if (result && result.isTrue) {
                        _this.removeWorkflowRule();
                    }
                });
            }
        }
        else {
            if (!this.isTaskLevelcalled) {
                // this.notificationService.error("Please Delete Highlighted Rule First!! If Rule not visible change the page size and try again!!");
                this.notificationService.error("Please Delete Highlighted Child Rules First");
            }
            else {
                this.notificationService.error("Please open global configure rule and then edit and remove this task");
            }
        }
    };
    CustomWorkflowFieldComponent.prototype.removeWorkflowRule = function () {
        var _this = this;
        var isInitialRule1 = this.ruleData.isInitialRule ? 'true' : 'false';
        if (this.ruleData.workflowRuleId) {
            this.ruleData.workflowRuleId = Array.isArray(this.ruleData.workflowRuleId) ? this.ruleData.workflowRuleId : [this.ruleData.workflowRuleId];
            //let httpRequestArray = []
            var ruleIds_2 = [];
            this.ruleData.workflowRuleId.forEach(function (workflowId) {
                var Rule = {
                    Id: workflowId
                };
                ruleIds_2.push(Rule);
                //httpRequestArray.push(this.taskService.removeWorkflowRule(workflowId));
            });
            var currentTask_1 = [];
            this.ruleData.currentTask.forEach(function (eachTask) {
                var sourceTask = {
                    Id: eachTask
                };
                currentTask_1.push(sourceTask);
            });
            var rData = {
                currentTask: currentTask_1,
                isInitialRule: isInitialRule1
            };
            this.taskService.removeWorkflowRule(ruleIds_2, rData).subscribe(function (response) {
                if (response) {
                    _this.removeWorkflowRuleHandler.next(_this.ruleData);
                    _this.enableInitiateWorkflowHandler.next();
                    _this.notificationService.info('Workflow Rule has been removed');
                }
                else {
                    _this.notificationService.error('Something went wrong while removing workflow rule');
                }
            });
        }
        else {
            this.removeWorkflowRuleHandler.next(null);
        }
    };
    CustomWorkflowFieldComponent.prototype.closeDialog = function () {
        var _this = this;
        if (this.customWorkflowFieldForm && this.customWorkflowFieldForm.pristine) {
            this.closeCallbackHandler.next();
        }
        else {
            var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result.isTrue) {
                    _this.closeCallbackHandler.next();
                }
            });
        }
    };
    CustomWorkflowFieldComponent.prototype.initializeForm = function () {
        var _this = this;
        var _a, _b, _c, _d, _e, _f;
        // this.allRuleList = [...new Set([...this.ruleList, ...this.projectRuleList])]; --- Update Standard code for all rule list - Menaka
        this.ruleList.forEach(function (rule) {
            var existingRule = _this.allRuleList.find(function (selectedRule) { return JSON.stringify(selectedRule) === JSON.stringify(rule); });
            if (!existingRule) {
                _this.allRuleList.push(rule);
            }
        });
        this.projectRuleList.forEach(function (rule) {
            var existingRule = _this.allRuleList.find(function (selectedRule) { return JSON.stringify(selectedRule) === JSON.stringify(rule); });
            if (!existingRule) {
                _this.allRuleList.push(rule);
            }
        });
        this.currentRuleList = [];
        this.allRuleList.forEach(function (rule) {
            if (JSON.stringify(_this.ruleData) !== JSON.stringify(rule)) {
                _this.currentRuleList.push(rule);
            }
            if (_this.isTaskLevelcalled) {
                _this.currentRuleList.push(rule);
            }
        });
        if (this.taskData[0].PROJECT_ITEM_ID) {
            var projectItemId = this.taskData[0].PROJECT_ITEM_ID;
            this.projectId = projectItemId.split('.')[1];
        }
        this.taskData.forEach(function (task) {
            if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL || task.TASK_STATUS_TYPE === StatusTypes.INTERMEDIATE) || (_this.ruleData.isExist && _this.isRulesInitiated)) {
                if (task.IS_APPROVAL_TASK === 'true') {
                    /*if (this.currentRuleList) {
                      const hasParentTaskRule = this.currentRuleList.find(rule => rule.currentTask === String(task.TASK_PARENT_ID) || this.currentRuleList.find(rule => rule.targetData === task.ID));
                      if (hasParentTaskRule) {
                        this.taskList.push({
                          name: task.TASK_NAME,
                          value: task.ID
                        });
                      }
                    }
                    */
                    if (_this.currentRuleList && _this.currentRuleList.length > 0 && !_this.isTaskLevelcalled) {
                        var hasRule = _this.currentRuleList.find(function (rule) { return rule.targetData === task.ID; });
                        if (hasRule) {
                            _this.taskList.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                            });
                        }
                    }
                    else if (_this.isTaskLevelcalled) {
                        _this.taskList.push({
                            name: task.TASK_NAME,
                            value: task.ID,
                            isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                        });
                    }
                }
                else {
                    _this.taskList.push({
                        name: task.TASK_NAME,
                        value: task.ID,
                        isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED)
                    });
                }
            }
        });
        var taskStatus = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.TASK);
        // this.taskStatuses = this.statusService.getAllStatusBycategoryName(MPM_LEVELS.TASK);
        this.taskStatuses = taskStatus && taskStatus.length > 0 && Array.isArray(taskStatus) ? taskStatus[0] : taskStatus;
        var currentRuleTriggerData = this.taskStatuses.find(function (status) { return status['MPM_Status-id'].Id == _this.ruleData.targetData; });
        this.targetTask = this.taskData.find(function (task) { return task.ID === _this.ruleData.targetData; });
        this.customWorkflowFieldForm = new FormGroup({
            currentTask: new FormControl({
                value: this.ruleData.currentTask,
                disabled: (this.isCurrentTaskRule ? true : (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_a = this.targetTask) === null || _a === void 0 ? void 0 : _a.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false))
            }, [Validators.required]),
            triggerType: new FormControl({ value: this.ruleData.triggerType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_b = this.targetTask) === null || _b === void 0 ? void 0 : _b.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
            triggerData: new FormControl({ value: '', disabled: this.ruleData.triggerType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_c = this.targetTask) === null || _c === void 0 ? void 0 : _c.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
            targetType: new FormControl({ value: this.ruleData.targetType, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_d = this.targetTask) === null || _d === void 0 ? void 0 : _d.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }, [Validators.required]),
            targetData: new FormControl({ value: '', disabled: this.ruleData.targetType ? (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_e = this.targetTask) === null || _e === void 0 ? void 0 : _e.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) : true }, [Validators.required]),
            isInitialRule: new FormControl({ value: this.ruleData.isInitialRule, disabled: (this.ruleData.isExist ? (this.ruleData.targetType === 'TASK' ? (((_f = this.targetTask) === null || _f === void 0 ? void 0 : _f.IS_ACTIVE_TASK) === 'true' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-ACCEPTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-CANCELLED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REJECTED' || this.targetTask.TASK_STATUS_TYPE === 'FINAL-REVIEW-REJECTED' ? true : false) : false) : false) }),
        });
        if (this.customWorkflowFieldForm.controls.currentTask.value) {
            var currentTaskList_1 = this.customWorkflowFieldForm.controls.currentTask.value;
            var _loop_1 = function (i) {
                var currentTask = this_1.taskData.find(function (task) { return task.ID === currentTaskList_1[i]; });
                if (currentTask && currentTask.IS_APPROVAL_TASK === 'true') {
                    this_1.approvalTask = true;
                    return "break";
                }
                else {
                    this_1.approvalTask = false;
                }
            };
            var this_1 = this;
            for (var i = 0; i < currentTaskList_1.length; i++) {
                var state_1 = _loop_1(i);
                if (state_1 === "break")
                    break;
            }
            var isTargetTask_1 = false;
            this.allRuleList.forEach(function (rule) {
                // if (rule.targetData === this.customWorkflowFieldForm.getRawValue().currentTask) {
                //   const status = this.taskStatuses.find(taskStatus => taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED);
                //   if (!(rule.triggerType === 'STATUS' && status['MPM_Status-id'].Id === rule.triggerData)) {
                //     isTargetTask = true;
                //   }
                // }
                _this.customWorkflowFieldForm.getRawValue().currentTask.forEach(function (eachTask) {
                    if (rule.targetData === eachTask) {
                        var status_1 = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                        if (!(rule.triggerType === 'STATUS' && status_1['MPM_Status-id'].Id === rule.triggerData)) {
                            isTargetTask_1 = true;
                        }
                    }
                });
            });
            if (isTargetTask_1 || this.approvalTask) {
                this.customWorkflowFieldForm.controls.isInitialRule.disable();
            }
        }
        if (this.customWorkflowFieldForm.getRawValue().triggerType) {
            if (this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS') {
                this.taskStatuses.forEach(function (status) {
                    if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                        !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !_this.approvalTask)) {
                        _this.triggerData.push({
                            name: status.NAME,
                            value: status['MPM_Status-id'].Id
                        });
                    }
                });
                this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            }
            else {
                var taskWorkflowActions = this.sharingService.getTaskWorkflowActions();
                taskWorkflowActions.forEach(function (action) {
                    _this.triggerData.push({
                        name: action.NAME,
                        value: action['MPM_Workflow_Actions-id'].Id
                    });
                });
                this.customWorkflowFieldForm.controls.triggerData.setValue(this.ruleData.triggerData);
            }
        }
        var isSourceTask = false;
        if (this.customWorkflowFieldForm.getRawValue().targetType) {
            if (this.customWorkflowFieldForm.getRawValue().targetType === 'TASK') {
                this.taskData.forEach(function (task) {
                    var isSelectedTask = false;
                    var isParentTaskCompleted = true;
                    var isCycleTask = false;
                    var currentTaskRuleList = [];
                    if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                        _this.currentRuleList.forEach(function (rule) {
                            _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                if (eachTask === rule.currentTask[0]) {
                                    currentTaskRuleList.push(rule);
                                }
                            });
                        });
                        // currentTaskRuleList.forEach(rule => {
                        //   if (rule.targetData === task.ID && rule.triggerData === this.customWorkflowFieldForm.controls.triggerData.value) {
                        //     isSelectedTask = true;
                        //   }
                        // });
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value) {
                            var targetTaskRuleList_1 = [];
                            _this.currentRuleList.forEach(function (rule) {
                                _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList_1.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList_1.forEach(function (rule) {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === _this.customWorkflowFieldForm.controls.triggerData.value) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    var status = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
                    var statusAsRequestedChanges = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                    if (task.IS_APPROVAL_TASK === 'true') {
                        var isParentTaskInCurrentTask_1 = false;
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask_1 = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask_1 && ((_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                            (_this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            var parentTaskRuleList = _this.allRuleList.filter(function (rule) { return rule.currentTask.includes(String(task.TASK_PARENT_ID)) || rule.targetData === String(task.TASK_PARENT_ID); });
                            if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                parentTaskRuleList.forEach(function (parentTaskRule) {
                                    if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                        isParentTaskCompleted = false;
                                    }
                                });
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                        if (task.ID === eachTask) {
                            isSourceTask = true;
                        }
                    });
                    if ((!isSelectedTask && !isCycleTask && isParentTaskCompleted /*&& !this.isTaskLevelcalled*/) /*|| (this.ruleData.isExist && this.isRulesInitiated)*/) {
                        if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL) || (_this.ruleData.isExist && _this.isRulesInitiated)) {
                            _this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                    // else if(this.isTaskLevelcalled){
                    //   if ((task.TASK_STATUS_TYPE === StatusTypes.INITIAL) || (this.ruleData.isExist && this.isRulesInitiated)) {
                    //     this.targetData.push({
                    //       name: task.TASK_NAME,
                    //       value: task.ID,
                    //       isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                    //       isApprovalTask: task.IS_APPROVAL_TASK==='true' ?true:false
                    //     });
                    //   }
                    // }
                });
                this.groupTargetTask();
                this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            }
            else {
                var taskEvents = this.sharingService.getTaskEvents();
                taskEvents.forEach(function (event) {
                    _this.targetData.push({
                        name: event.DISPLAY_NAME,
                        value: event['MPM_Events-id'].Id
                    });
                });
                this.customWorkflowFieldForm.controls.targetData.setValue(this.ruleData.targetData);
            }
        }
        this.customWorkflowFieldForm.controls.currentTask.valueChanges.subscribe(function (currentTask) {
            var _a;
            /***********************Checking For Approval Task in Current Source task List*********************/
            /*************************We shouldn't allow Approval task to be grouped with other rules*************/
            var sourceTasks = [];
            // this.customWorkflowFieldForm.controls.isInitialRule.setValue(false);
            _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                var currentTask = _this.taskData.find(function (task) { return task.ID === eachTask; });
                sourceTasks.push(currentTask);
            });
            sourceTasks.forEach(function (eachTask) {
                var _a;
                if (((_a = eachTask) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true' && sourceTasks.length > 1) {
                    _this.customWorkflowFieldForm.controls.currentTask.setValue([]);
                    _this.notificationService.error('Approval Task cant be Grouped with Other tasks');
                }
            });
            //Checking if there only completed tasks in source tasks of a Rule.
            //This comes handy while updating tasks
            var onlyCompletedTasksInSourceTaskList = true;
            sourceTasks.forEach(function (eachTask) {
                var _a, _b, _c;
                if (!(((_a = eachTask) === null || _a === void 0 ? void 0 : _a.TASK_STATUS_TYPE) === StatusTypes.FINAL_APPROVED || ((_b = eachTask) === null || _b === void 0 ? void 0 : _b.TASK_STATUS_TYPE) === StatusTypes.FINAL_CANCELLED || ((_c = eachTask) === null || _c === void 0 ? void 0 : _c.TASK_STATUS_TYPE) === StatusTypes.FINAL_COMPLETED)) {
                    onlyCompletedTasksInSourceTaskList = false;
                }
            });
            if (onlyCompletedTasksInSourceTaskList && _this.customWorkflowFieldForm.controls.currentTask.value.length > 0) {
                _this.customWorkflowFieldForm.controls.currentTask.setValue(_this.ruleData.currentTask);
                _this.notificationService.error('There should be atleast one Task that is not Completed');
            }
            //checking if target type is "TASK/EVENT"
            if (currentTask && _this.customWorkflowFieldForm.controls.targetType && _this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                _this.targetData = [];
                _this.customWorkflowFieldForm.controls.targetData.setValue(_this.ruleData.targetData);
                _this.taskData.forEach(function (task) {
                    var isSelectedTask = false;
                    var isParentTaskCompleted = true;
                    var isCycleTask = false;
                    if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                        var currentTaskRuleList_1 = [];
                        _this.currentRuleList.forEach(function (rule) {
                            currentTask.forEach(function (eachTask) {
                                if (rule.currentTask[0] === eachTask) {
                                    currentTaskRuleList_1.push(rule);
                                }
                            });
                        });
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value) {
                            var targetTaskRuleList_2 = [];
                            _this.currentRuleList.forEach(function (rule) {
                                _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList_2.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList_2.forEach(function (rule) {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === _this.customWorkflowFieldForm.controls.triggerData.value) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    var status = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
                    var statusAsRequestedChanges = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                    if (task.IS_APPROVAL_TASK === 'true') {
                        var isParentTaskInCurrentTask_2 = false;
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask_2 = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask_2 && ((_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                            (_this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                                var parentTaskRuleList = _this.currentRuleList.filter(function (rule) { return rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID); });
                                if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                    parentTaskRuleList.forEach(function (parentTaskRule) {
                                        if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                            isParentTaskCompleted = false;
                                        }
                                    });
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    if (!isSelectedTask && !isCycleTask && isParentTaskCompleted && !_this.isTaskLevelcalled) {
                        if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                            _this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                });
                _this.groupTargetTask();
            }
            //checking with Trigger type 
            if (currentTask && _this.customWorkflowFieldForm.controls.triggerType) {
                var approvalTaskInSource_1 = false;
                for (var i = 0; i < sourceTasks.length; i++) {
                    if (((_a = sourceTasks[i]) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true') {
                        approvalTaskInSource_1 = true;
                        break;
                    }
                }
                if (_this.customWorkflowFieldForm.getRawValue().triggerType === 'STATUS') {
                    _this.triggerData = [];
                    _this.taskStatuses.forEach(function (status) {
                        if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                            !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !approvalTaskInSource_1)) {
                            _this.triggerData.push({
                                name: status.NAME,
                                value: status['MPM_Status-id'].Id
                            });
                        }
                    });
                    _this.selectedTriggerData = _this.triggerData[0];
                }
            }
            var isTargetTask = false;
            if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                _this.currentRuleList.forEach(function (rule) {
                    // if (rule.targetData === currentTask) {
                    _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                        if (rule.targetData === eachTask) {
                            var status_2 = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                            if (!(rule.triggerType === 'STATUS' && status_2['MPM_Status-id'].Id === rule.triggerData)) {
                                isTargetTask = true;
                            }
                        }
                    });
                });
            }
            if (_this.customWorkflowFieldForm.controls.currentTask.value) {
                var currentTaskList_2 = _this.customWorkflowFieldForm.controls.currentTask.value;
                var _loop_2 = function (i) {
                    var currentTask_2 = _this.taskData.find(function (task) { return task.ID === currentTaskList_2[i]; });
                    if (currentTask_2 && currentTask_2.IS_APPROVAL_TASK === 'true') {
                        _this.approvalTask = true;
                        return "break";
                    }
                    else {
                        _this.approvalTask = false;
                    }
                };
                for (var i = 0; i < currentTaskList_2.length; i++) {
                    var state_2 = _loop_2(i);
                    if (state_2 === "break")
                        break;
                }
            }
            if (isTargetTask || _this.approvalTask) {
                _this.customWorkflowFieldForm.controls.isInitialRule.disable();
            }
            else {
                _this.customWorkflowFieldForm.controls.isInitialRule.enable();
            }
            _this.customWorkflowFieldForm.controls.targetData.setValue('');
            _this.groupTargetTask();
        });
        this.customWorkflowFieldForm.controls.triggerType.valueChanges.subscribe(function (triggerType) {
            if (triggerType) {
                if (_this.customWorkflowFieldForm.controls.currentTask.value) {
                    var currentTaskList_3 = _this.customWorkflowFieldForm.controls.currentTask.value;
                    var _loop_3 = function (i) {
                        var currentTask = _this.taskData.find(function (task) { return task.ID === currentTaskList_3[i]; });
                        if (currentTask && currentTask.IS_APPROVAL_TASK === 'true') {
                            _this.approvalTask = true;
                            return "break";
                        }
                        else {
                            _this.approvalTask = false;
                        }
                    };
                    for (var i = 0; i < currentTaskList_3.length; i++) {
                        var state_3 = _loop_3(i);
                        if (state_3 === "break")
                            break;
                    }
                }
                _this.triggerData = [];
                _this.customWorkflowFieldForm.controls.triggerData.setValue('');
                if (triggerType === 'STATUS') {
                    _this.taskStatuses.forEach(function (status) {
                        if (status.STATUS_TYPE !== StatusTypes.INITIAL && status.STATUS_TYPE !== StatusTypes.FINAL_CANCELLED &&
                            !(status.STATUS_TYPE === StatusTypes.FINAL_REJECTED && !_this.approvalTask)) {
                            _this.triggerData.push({
                                name: status.NAME,
                                value: status['MPM_Status-id'].Id
                            });
                        }
                    });
                }
                else if (triggerType === 'ACTION') {
                    var taskWorkflowActions = _this.sharingService.getTaskWorkflowActions();
                    taskWorkflowActions.forEach(function (action) {
                        _this.triggerData.push({
                            name: action.NAME,
                            value: action['MPM_Workflow_Actions-id'].Id
                        });
                    });
                }
                if (_this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                    _this.targetData = [];
                    _this.customWorkflowFieldForm.controls.targetData.setValue('');
                    _this.taskData.forEach(function (task) {
                        var isParentTaskCompleted = true;
                        var status = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
                        var statusAsRequestedChanges = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                        if (task.IS_APPROVAL_TASK === 'true') {
                            var isParentTaskInCurrentTask_3 = false;
                            _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                if (eachTask === String(task.TASK_PARENT_ID)) {
                                    isParentTaskInCurrentTask_3 = true;
                                }
                            });
                            if (!(isParentTaskInCurrentTask_3 && ((triggerType === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) || (triggerType === 'ACTION')))) {
                                if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                                    var parentTaskRuleList = _this.currentRuleList.filter(function (rule) { return rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID); });
                                    if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                        parentTaskRuleList.forEach(function (parentTaskRule) {
                                            if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id))) || (parentTaskRule.triggerType === 'ACTION')) {
                                                isParentTaskCompleted = false;
                                            }
                                        });
                                    }
                                    else {
                                        isParentTaskCompleted = false;
                                    }
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                        }
                        if (isParentTaskCompleted) {
                            if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL && task.ID !== _this.customWorkflowFieldForm.controls.currentTask.value) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                                _this.targetData.push({
                                    name: task.TASK_NAME,
                                    value: task.ID,
                                    isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                    isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                                });
                            }
                        }
                    });
                }
                _this.groupTargetTask();
                _this.customWorkflowFieldForm.controls.triggerData.enable();
            }
        });
        this.customWorkflowFieldForm.controls.triggerData.valueChanges.subscribe(function (triggerData) {
            if (triggerData && _this.customWorkflowFieldForm.controls.targetType.value === 'TASK') {
                var removedTasks_1 = [];
                _this.ruleData.currentTask.forEach(function (eachTask) {
                    var index = _this.customWorkflowFieldForm.controls.currentTask.value.indexOf(eachTask);
                    if (index < 0) {
                        removedTasks_1.push(eachTask);
                    }
                });
                _this.targetData = [];
                _this.customWorkflowFieldForm.controls.targetData.setValue('');
                _this.taskData.forEach(function (task) {
                    var isSelectedTask = false;
                    var isCycleTask = false;
                    var isParentTaskCompleted = true;
                    if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                        var currentTaskRuleList_2 = [];
                        _this.currentRuleList.forEach(function (rule) {
                            _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                if (rule.currentTask[0] === eachTask) {
                                    currentTaskRuleList_2.push(rule);
                                }
                            });
                        });
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS') {
                            var targetTaskRuleList_3 = [];
                            _this.currentRuleList.forEach(function (rule) {
                                _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                    if (rule.targetData === eachTask) {
                                        targetTaskRuleList_3.push(rule);
                                    }
                                });
                            });
                            targetTaskRuleList_3.forEach(function (rule) {
                                if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === triggerData) {
                                    isCycleTask = true;
                                }
                            });
                        }
                    }
                    var status = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
                    var statusAsRequestedChanges = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                    var isParentTaskInCurrentTask = false;
                    if (task.IS_APPROVAL_TASK === 'true') {
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (eachTask === String(task.TASK_PARENT_ID)) {
                                isParentTaskInCurrentTask = true;
                            }
                        });
                        if (!(isParentTaskInCurrentTask
                            && ((_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && triggerData === status['MPM_Status-id'].Id) ||
                                (_this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                            if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                                var parentTaskRuleList_1 = _this.currentRuleList.filter(function (rule) { return rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID); });
                                var _loop_4 = function (i) {
                                    removedTasks_1.forEach(function (eachTask) {
                                        if (eachTask === parentTaskRuleList_1[i].currentTask[0]) {
                                            parentTaskRuleList_1.splice(i, 1);
                                        }
                                    });
                                };
                                //while editing if a remove a parent task from source task list then we are removing that rule from parentTaskRuleList
                                for (var i = 0; i < parentTaskRuleList_1.length; i++) {
                                    _loop_4(i);
                                }
                                if (parentTaskRuleList_1 && parentTaskRuleList_1.length > 0) {
                                    parentTaskRuleList_1.forEach(function (parentTaskRule) {
                                        if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                            isParentTaskCompleted = false;
                                        }
                                    });
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                            else {
                                isParentTaskCompleted = false;
                            }
                        }
                    }
                    if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
                        if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL && task.ID !== _this.customWorkflowFieldForm.controls.currentTask.value) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                            _this.targetData.push({
                                name: task.TASK_NAME,
                                value: task.ID,
                                isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                            });
                        }
                    }
                });
            }
            _this.groupTargetTask();
        });
        this.customWorkflowFieldForm.controls.targetType.valueChanges.subscribe(function (targetType) {
            if (targetType) {
                _this.targetData = [];
                _this.customWorkflowFieldForm.controls.targetData.setValue('');
                if (targetType === 'EVENT') {
                    var taskEvents = _this.sharingService.getTaskEvents();
                    taskEvents.forEach(function (event) {
                        _this.targetData.push({
                            name: event.DISPLAY_NAME,
                            value: event['MPM_Events-id'].Id
                        });
                    });
                }
                else if (targetType === 'TASK') {
                    _this.taskData.forEach(function (task) {
                        var isSelectedTask = false;
                        var isParentTaskCompleted = true;
                        var isCycleTask = false;
                        _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                            if (task.ID === eachTask) {
                                isSelectedTask = true;
                            }
                        });
                        if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                            if (_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value) {
                                var targetTaskRuleList_4 = [];
                                _this.currentRuleList.forEach(function (rule) {
                                    _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                        if (rule.targetData === eachTask) {
                                            targetTaskRuleList_4.push(rule);
                                        }
                                    });
                                });
                                targetTaskRuleList_4.forEach(function (rule) {
                                    if (rule.currentTask[0] === task.ID && rule.triggerType === 'STATUS' && rule.triggerData === _this.customWorkflowFieldForm.controls.triggerData.value) {
                                        isCycleTask = true;
                                    }
                                });
                            }
                        }
                        var status = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
                        var statusAsRequestedChanges = _this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
                        if (task.IS_APPROVAL_TASK === 'true') {
                            var isParentTaskInCurrentTask_4 = false;
                            _this.customWorkflowFieldForm.controls.currentTask.value.forEach(function (eachTask) {
                                if (eachTask === String(task.TASK_PARENT_ID)) {
                                    isParentTaskInCurrentTask_4 = true;
                                }
                            });
                            if (!(isParentTaskInCurrentTask_4
                                && ((_this.customWorkflowFieldForm.controls.triggerType.value === 'STATUS' && _this.customWorkflowFieldForm.controls.triggerData.value === status['MPM_Status-id'].Id) ||
                                    (_this.customWorkflowFieldForm.controls.triggerType.value === 'ACTION')))) {
                                if (_this.currentRuleList && _this.currentRuleList.length > 0) {
                                    var parentTaskRuleList = _this.currentRuleList.filter(function (rule) { return rule.currentTask[0] === String(task.TASK_PARENT_ID) || rule.targetData === String(task.TASK_PARENT_ID); });
                                    if (parentTaskRuleList && parentTaskRuleList.length > 0) {
                                        parentTaskRuleList.forEach(function (parentTaskRule) {
                                            if (!(parentTaskRule && (parentTaskRule.triggerType === 'STATUS' && (parentTaskRule.triggerData === status['MPM_Status-id'].Id || parentTaskRule.triggerData === statusAsRequestedChanges['MPM_Status-id'].Id)) || (parentTaskRule.triggerType === 'ACTION'))) {
                                                isParentTaskCompleted = false;
                                            }
                                        });
                                    }
                                    else {
                                        isParentTaskCompleted = false;
                                    }
                                }
                                else {
                                    isParentTaskCompleted = false;
                                }
                            }
                        }
                        if (!isSelectedTask && !isCycleTask && isParentTaskCompleted) {
                            if (task.TASK_STATUS_TYPE === StatusTypes.INITIAL) { //  && task.TASK_START_DATE > currentTask.TASK_DUE_DATE
                                _this.targetData.push({
                                    name: task.TASK_NAME,
                                    value: task.ID,
                                    isCompleted: (task.TASK_STATUS_TYPE === StatusTypes.FINAL_APPROVED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_COMPLETED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_CANCELLED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REJECTED) || (task.TASK_STATUS_TYPE === StatusTypes.FINAL_REVIEW_REJECTED),
                                    isApprovalTask: task.IS_APPROVAL_TASK === 'true' ? true : false
                                });
                            }
                        }
                    });
                }
                _this.groupTargetTask();
                _this.customWorkflowFieldForm.controls.targetData.enable();
            }
        });
        this.customWorkflowFieldForm.controls.targetData.valueChanges.subscribe(function (response) {
            _this.isRuleSaved = false;
        });
        this.customWorkflowFieldForm.valueChanges.subscribe(function (response) {
            _this.isRuleChanged = true;
        });
    };
    CustomWorkflowFieldComponent.prototype.ngOnInit = function () {
        var _this = this;
        var sourceTasksSet = new Set();
        this.ruleListGroup.forEach(function (rule) {
            rule.hasPredecessor = true;
        });
        this.taskData.forEach(function (task) {
            _this.ruleData.currentTask.forEach(function (eachTask) {
                if (task.ID === eachTask) {
                    sourceTasksSet.add(task.TASK_NAME);
                }
            });
        });
        sourceTasksSet.forEach(function (task) { return _this.currentSourceTasks.push(task); });
        this.ruleData.currentTask = this.ruleData.currentTask.length > 0 && Array.isArray(this.ruleData.currentTask) ? this.ruleData.currentTask : [this.ruleData.currentTask];
        this.ruleList.forEach(function (rule) {
            rule.currentTask = rule.currentTask.length > 0 && Array.isArray(rule.currentTask) ? rule.currentTask : [rule.currentTask];
        });
        this.initializeForm();
    };
    CustomWorkflowFieldComponent.prototype.groupTargetTask = function () {
        var _this = this;
        var _a, _b, _c;
        var status = this.taskStatuses.find(function (taskStatus) { return taskStatus.STATUS_TYPE === StatusTypes.FINAL_APPROVED; });
        // if(this.isTaskLevelcalled){
        if (((_a = this.taskWorkFlowRuleToDisplay) === null || _a === void 0 ? void 0 : _a.IS_APPROVAL_TASK) === 'true' && !(this.customWorkflowFieldForm.controls.triggerData.value == status['MPM_Status-id'].Id)) {
            var newtargetData_1 = this.targetData.filter(function (targetTask) { return !_this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value); }); /*.filter(t1=> t1.isApprovalTask==false)*/
            this.targetData = newtargetData_1;
        }
        else if (((_b = this.taskWorkFlowRuleToDisplay) === null || _b === void 0 ? void 0 : _b.IS_APPROVAL_TASK) === 'true' && (this.customWorkflowFieldForm.controls.triggerData.value == status['MPM_Status-id'].Id)) {
            var parentId_1 = ((_c = this.taskWorkFlowRuleToDisplay) === null || _c === void 0 ? void 0 : _c.TASK_PARENT_ID) + '';
            var removeTargetTask_1 = new Set();
            this.taskruleListGroup.forEach(function (task) {
                if (task.currentTask.includes(parentId_1)) {
                    task.currentTask.forEach(function (taskId) { return removeTargetTask_1.add(taskId); });
                }
                // const newtargetData= this.targetData.filter(targetTask=> targetTask.value!=this.taskWorkFlowRuleToDisplay?.TASK_PARENT_ID && !this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value) && !removeTargetTask.has(targetTask.value))
                var newtargetData = _this.targetData.filter(function (targetTask) { return !_this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value); });
                _this.targetData = newtargetData;
            });
        }
        else if (this.customWorkflowFieldForm.controls.triggerData.value != status['MPM_Status-id'].Id) {
            var newtargetData = this.targetData.filter(function (targetTask) { return !_this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value); }); /*.filter(t1=> t1.isApprovalTask==false)*/
            this.targetData = newtargetData;
        }
        else {
            var newtargetData_2 = this.targetData.filter(function (targetTask) { /*targetTask.value!=this.taskWorkFlowRuleToDisplay?.TASK_PARENT_ID &&*/ return !_this.customWorkflowFieldForm.controls.currentTask.value.includes(targetTask.value); });
            this.targetData = newtargetData_2;
        }
    };
    CustomWorkflowFieldComponent.ctorParameters = function () { return [
        { type: StatusService },
        { type: SharingService },
        { type: TaskService },
        { type: NotificationService },
        { type: ProjectUtilService },
        { type: MatDialog }
    ]; };
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "ruleData", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "ruleList", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "projectRuleList", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "taskData", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "isRulesInitiated", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "isCurrentTaskRule", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "ruleListGroup", void 0);
    __decorate([
        Output()
    ], CustomWorkflowFieldComponent.prototype, "closeCallbackHandler", void 0);
    __decorate([
        Output()
    ], CustomWorkflowFieldComponent.prototype, "removeWorkflowRuleHandler", void 0);
    __decorate([
        Output()
    ], CustomWorkflowFieldComponent.prototype, "enableInitiateWorkflowHandler", void 0);
    __decorate([
        Output()
    ], CustomWorkflowFieldComponent.prototype, "ruleGroupHandler", void 0);
    __decorate([
        Output()
    ], CustomWorkflowFieldComponent.prototype, "ruleDependencyErrorHandler", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "isTaskLevelcalled", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "taskWorkFlowRuleToDisplay", void 0);
    __decorate([
        Input()
    ], CustomWorkflowFieldComponent.prototype, "taskruleListGroup", void 0);
    CustomWorkflowFieldComponent = __decorate([
        Component({
            selector: 'mpm-custom-workflow-field',
            template: "<div [ngClass]=\"{'dependency-error': !ruleData.hasPredecessor}\">\r\n    <form [formGroup]=\"customWorkflowFieldForm\">\r\n        <mat-form-field appearance=\"outline\" matTooltip=\"{{currentSourceTasks}}\">\r\n            <mat-label>Task Name</mat-label>\r\n            <mat-select formControlName=\"currentTask\" multiple>\r\n                <mat-option *ngFor=\"let task of taskList\" value=\"{{task.value}}\" [disabled]=\"task?.isCompleted\">\r\n                    <span matTooltip=\"{{task.name}}\" [style.color]=\"task?.isCompleted ? 'grey':''\">{{task.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Trigger Type</mat-label>\r\n            <mat-select formControlName=\"triggerType\">\r\n                <mat-option *ngFor=\"let triggerType of triggerTypes\" value=\"{{triggerType.value}}\">\r\n                    <span matTooltip=\"{{triggerType.name}}\">{{triggerType.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Status/Action</mat-label>\r\n            <mat-select formControlName=\"triggerData\">\r\n                <mat-option *ngFor=\"let data of triggerData\" value=\"{{data.value}}\">\r\n                    <span matTooltip=\"{{data.name}}\">{{data.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Target Type</mat-label>\r\n            <mat-select formControlName=\"targetType\">\r\n                <mat-option *ngFor=\"let targetType of targetTypes\" value=\"{{targetType.value}}\">\r\n                    <span matTooltip=\"{{targetType.name}}\">{{targetType.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Task/Event</mat-label>\r\n            <mat-select formControlName=\"targetData\">\r\n                <mat-option *ngFor=\"let data of targetData\" value=\"{{data.value}}\" [disabled]=\"data?.isCompleted\">\r\n                    <span matTooltip=\"{{data.name}}\" [style.color]=\"data?.isCompleted ? 'grey':''\">{{data.name}}</span>\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-checkbox color=\"primary\" formControlName=\"isInitialRule\">\r\n            <span>Is Initial</span>\r\n        </mat-checkbox>\r\n\r\n        <button class=\"field-row-action\" *ngIf=\"isRuleChanged && (!ruleData.isExist || customWorkflowFieldForm.dirty) && customWorkflowFieldForm.valid && !isRuleSaved\" mat-icon-button color=\"primary\" type=\"button\" matTooltip=\"Save this Rule\" (click)=\"addWorkflowRule()\">\r\n            <mat-icon>done</mat-icon>\r\n        </button>\r\n        <button class=\"field-row-action\" type=\"button\" *ngIf=\"(!ruleData.isExist || (targetTask?.IS_ACTIVE_TASK==='false' && targetTask?.TASK_STATUS_TYPE !=='FINAL-ACCEPTED')) || !ruleData.hasPredecessor || ruleData.targetType=='EVENT'\" (click)=\"validateRemoveWorkflowRule()\"\r\n            mat-icon-button color=\"primary\" matTooltip=\"Remove this Rule\">\r\n            <mat-icon>close</mat-icon>\r\n        </button>\r\n    </form>\r\n</div>",
            styles: ["mat-form-field{width:17%;margin-left:8px;font-size:12px}"]
        })
    ], CustomWorkflowFieldComponent);
    return CustomWorkflowFieldComponent;
}());
export { CustomWorkflowFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXdvcmtmbG93LWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvY3VzdG9tLXdvcmtmbG93LWZpZWxkL2N1c3RvbS13b3JrZmxvdy1maWVsZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDRFQUE0RSxDQUFDO0FBQ3hILE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFPOUM7SUFvREUsc0NBQ1MsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGtCQUFzQyxFQUN0QyxNQUFpQjtRQUxqQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLFdBQU0sR0FBTixNQUFNLENBQVc7UUFqRGhCLHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDL0MsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRCxrQ0FBNkIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ2xFLDBEQUEwRDtRQUMxRCxtQkFBYyxHQUFDLEtBQUssQ0FBQztRQUNYLHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDM0MsK0JBQTBCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUkvRCxxQ0FBcUM7UUFDcEMsdUJBQWtCLEdBQUUsS0FBSyxDQUFDO1FBRTNCLGlCQUFZLEdBQUcsQ0FBQztnQkFDZCxJQUFJLEVBQUUsUUFBUTtnQkFDZCxLQUFLLEVBQUUsUUFBUTthQUNoQixFQUFFO2dCQUNELElBQUksRUFBRSxRQUFRO2dCQUNkLEtBQUssRUFBRSxRQUFRO2FBQ2hCLENBQUMsQ0FBQztRQUNILGdCQUFXLEdBQUcsQ0FBQztnQkFDYixJQUFJLEVBQUUsTUFBTTtnQkFDWixLQUFLLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0QsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsS0FBSyxFQUFFLE9BQU87YUFDZixDQUFDLENBQUM7UUFDSCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixlQUFVLEdBQU8sRUFBRSxDQUFDO1FBQ3BCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFLZCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUdqQix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFFeEIsMEJBQXFCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztJQVVsQixDQUFDO0lBRUwsK0JBQStCO0lBQy9CLHlEQUFrQixHQUFsQjtRQUFBLGlCQXdKQztRQXZKQyxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBQztZQUNsRyw0Q0FBNEM7WUFDNUMsSUFBTSxVQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ3BDLElBQU0sS0FBSyxHQUFFLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsRixJQUFHLEtBQUssR0FBRyxDQUFDLEVBQUM7b0JBQ1gsVUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckI7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUksY0FBWSxHQUFHLEVBQUUsQ0FBQTtZQUNyQixVQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDbkIsSUFBTSxJQUFJLEdBQUU7b0JBQ1osV0FBVyxFQUFFLElBQUk7b0JBQ2pCLGFBQWEsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLGFBQWE7b0JBQzFDLFVBQVUsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVU7b0JBQ3BDLFVBQVUsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVU7b0JBQ3BDLFdBQVcsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVc7b0JBQ3RDLFdBQVcsRUFBRSxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVc7aUJBQ3JDLENBQUE7Z0JBQ0QsY0FBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUksZUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN2QixLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUNyRCxJQUFNLElBQUksR0FBRTtvQkFDVixXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxhQUFhLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhO29CQUMxQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVO29CQUNwQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXO29CQUN0QyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXO29CQUN0QyxjQUFjLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2lCQUM5QyxDQUFBO2dCQUNILGVBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDMUI7WUFDRCxJQUFJLGdCQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLGNBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUN2QixlQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtvQkFDNUIsSUFBRyxJQUFJLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUM7d0JBQzFDLGdCQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDOUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDLENBQUMsQ0FBQTtZQUNGLElBQU0sa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7bUJBQy9HLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQzttQkFDdEYsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO21CQUNwRixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7bUJBQ3BGLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDL0YsSUFBRyxrQkFBa0IsRUFDcEI7Z0JBQ0csS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNoRixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEg7YUFDSDtZQUNGLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO1lBQ25GLElBQUksb0JBQWtCLEdBQUcsRUFBRSxDQUFDO1lBQzVCLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxZQUFZO2dCQUNoQyxJQUFNLEtBQUssR0FBRyxVQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDcEUsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO29CQUNaLG9CQUFrQixDQUFDLElBQUksQ0FBQyxVQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztpQkFDMUM7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLDhFQUE4RTtZQUM5RSxxR0FBcUc7WUFDckcsSUFBSSxxQkFBbUIsR0FBRyxFQUFFLENBQUM7WUFDN0Isb0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtnQkFDakMsSUFBTSxlQUFlLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLFFBQVEsRUFBdkMsQ0FBdUMsQ0FBRSxDQUFDO2dCQUMvRixxQkFBbUIsQ0FBQyxJQUFJLE9BQXhCLHFCQUFtQixXQUFTLGVBQWUsR0FBRTtZQUMvQyxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksaUJBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUM3QixpQkFBZSxDQUFDLElBQUksT0FBcEIsaUJBQWUsV0FBUyxJQUFJLENBQUMsV0FBVyxHQUFFO2dCQUMxQyxpQkFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEMsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLHVCQUFxQixHQUFHLEVBQUUsQ0FBQztZQUMvQixxQkFBbUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO2dCQUNsQyxJQUFNLEtBQUssR0FBRyxpQkFBZSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ25ELElBQUcsS0FBSyxJQUFJLENBQUMsRUFBQztvQkFDWix1QkFBcUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ3RDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxFQUExQixDQUEwQixDQUFDLENBQUM7WUFDN0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUM3QixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ25CLFNBQVMsQ0FBQyxJQUFJLE9BQWQsU0FBUyxXQUFTLElBQUksQ0FBQyxXQUFXLEdBQUU7Z0JBQ3BDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNoQyx1QkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO29CQUNwQyxJQUFNLEtBQUssR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDN0MsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO3dCQUNaLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO3FCQUM3QjtnQkFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUMsQ0FBQyxDQUFBO1lBQ0gsb0hBQW9IO1lBQ25ILElBQUcsQ0FBQyxDQUFDLHVCQUFxQixJQUFJLHVCQUFxQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBQztnQkFDaEUsSUFBSSxTQUFPLEdBQUcsRUFBRSxDQUFBO2dCQUNoQixnQkFBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFVBQVU7b0JBQ2pDLElBQU0sSUFBSSxHQUFHO3dCQUNYLEVBQUUsRUFBRSxVQUFVO3FCQUNmLENBQUE7b0JBQ0QsU0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkIsQ0FBQyxDQUFDLENBQUE7Z0JBQ0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFPLEVBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDaEUsSUFBSSxRQUFRLEVBQUU7d0JBQ1osc0RBQXNEO3dCQUN0RCxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDO3dCQUM1RyxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7d0JBQ3BGLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO3dCQUN4QixLQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztxQkFDakU7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDO3FCQUNyRjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFJO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsNEdBQTRHLENBQUMsQ0FBQzthQUM5STtTQUVGO2FBQ0ksSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUM7WUFDdkcsd0NBQXdDO1lBQ3hDLElBQU0sVUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ2pFLElBQU0sS0FBSyxHQUFFLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckQsSUFBRyxLQUFLLEdBQUcsQ0FBQyxFQUFDO29CQUNYLFVBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3JCO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLElBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO21CQUNyRixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7bUJBQ3RGLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQzttQkFDcEYsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO21CQUNwRixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFDN0Y7Z0JBQ0csS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNoRixJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBQyxJQUFJLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEg7YUFDSDtpQkFBSTtnQkFDSixVQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDbkIsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN4RCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztvQkFDeEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxLQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVGLENBQUMsQ0FBQyxDQUFBO2FBQ0Y7U0FDSDtJQUNILENBQUM7SUFFRCx1REFBZ0IsR0FBaEI7UUFDRSxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztZQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ3hCLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDO29CQUNyQixjQUFjLENBQUMsSUFBSSxPQUFuQixjQUFjLFdBQVMsSUFBSSxDQUFDLFdBQVcsR0FBQztpQkFDekM7WUFDSCxDQUFDLENBQUMsQ0FBQTtTQUNIO2FBQ0c7WUFDRixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDakMsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7b0JBQ3JCLGNBQWMsQ0FBQyxJQUFJLE9BQW5CLGNBQWMsV0FBUyxJQUFJLENBQUMsV0FBVyxHQUFDO2lCQUN6QztZQUNILENBQUMsQ0FBQyxDQUFBO1NBQ0g7UUFDQSxJQUFNLEtBQUssR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0QsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO1lBQ1gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1NBQ2pGO2FBQUssSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztlQUMzSCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87ZUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFDO1lBQ25HLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBRTdCO2FBRUksSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztZQUMvQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkYsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNoRSxZQUFZLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEk7U0FDRjtJQUNILENBQUM7SUFFRCw0REFBcUIsR0FBckI7UUFFRSxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDakMsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQ3JCLGNBQWMsQ0FBQyxJQUFJLE9BQW5CLGNBQWMsV0FBUyxJQUFJLENBQUMsV0FBVyxHQUFDO2FBQ3pDO1FBQ0gsQ0FBQyxDQUFDLENBQUE7UUFDSCxJQUFNLEtBQUssR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0QsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO1lBQ1gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxrQkFBa0IsR0FBQyxJQUFJLENBQUM7U0FDOUI7UUFDRCxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQTtJQUNsQyxDQUFDO0lBRUQsc0RBQWUsR0FBZjtRQUFBLGlCQWtFQztRQWpFQyxJQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFDO1lBQ3pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRSxFQUFFLENBQUE7WUFDMUIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRixJQUFJLGtCQUFrQixHQUFFLEVBQUUsQ0FBQztnQkFDM0Isa0JBQWtCLENBQUMsSUFBSSxDQUFDO29CQUN0QixXQUFXLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDdkUsV0FBVyxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7b0JBQ3BFLFVBQVUsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLO29CQUNsRSxXQUFXLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSztvQkFDcEUsVUFBVSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUs7b0JBQ2xFLGFBQWEsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLO2lCQUMxRSxDQUFDLENBQUE7Z0JBRUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BLLHlCQUF5QjtnQkFDekIsMkJBQTJCO2dCQUMzQixnRUFBZ0U7YUFDakU7U0FDQTthQUNHLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUssSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBQztZQUMzSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUN6QjthQUFLLElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7ZUFDN0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPO2VBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBQztZQUNuRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMzQjthQUFLLElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7WUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUMzQixLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ25GLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDaEUsWUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hJO1NBQ0Y7UUFFRCxRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNoRCxJQUFHLFFBQVEsRUFBQztnQkFDVixLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDbkYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsQ0FBQztnQkFDbkYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDakYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDakYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsQ0FBQztnQkFDdkYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUNqQyxLQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7Z0JBQ2xDLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO29CQUNwQyxLQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUN6SjtnQkFDRCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixJQUFHLENBQUMsS0FBSSxDQUFDLGlCQUFpQixFQUFDO29CQUMzQixLQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDNUQ7cUJBQ0c7b0JBQ0YsS0FBSSxDQUFDLGNBQWMsR0FBQyxJQUFJLENBQUM7b0JBQ3pCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lCQUM5RDtnQkFDSCxxQ0FBcUM7Z0JBQ3JDLGdDQUFnQztnQkFDaEMseURBQXlEO2dCQUN6RCxNQUFNO2dCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUMvRDtpQkFBSTtnQkFDSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1EQUFtRCxDQUFDLENBQUM7YUFDckY7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFQSw2REFBc0IsR0FBdEI7UUFDQyxJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDO1FBQ3BELElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUssTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFDO1lBQ2pFLElBQUksYUFBVyxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7WUFDekIseUNBQXlDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDL0IsSUFBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUM7b0JBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsYUFBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxDQUFDO2lCQUN6RDtZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0osZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFXLENBQUMsQ0FBQztZQUMzQyxJQUFJLDRCQUEwQixHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLE9BQUssR0FBRyxDQUFDLENBQUM7WUFDZixxRUFBcUU7WUFDckUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO1lBRTlELElBQU0sV0FBVyxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3BFLElBQUcsV0FBVyxJQUFJLENBQUMsRUFBQztnQkFDbkIsMENBQTBDO2dCQUN6QyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQzdCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQzlELElBQUcsS0FBSyxJQUFJLENBQUMsRUFBQzt3QkFDWixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQzt3QkFDNUIsNEJBQTBCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN0QyxPQUFLLElBQUksQ0FBQyxDQUFDO3FCQUNaO2dCQUNILENBQUMsQ0FBQyxDQUFBO2FBQ0g7WUFDRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsT0FBSyxJQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyw0QkFBMEIsQ0FBQztZQUN6RSxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFLLEVBQUUsRUFBQztnQkFDbEMsK0RBQStEO2dCQUMvRCxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUV4RDtTQUNEO2FBQ0csSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFDO1lBQ25FLElBQUksYUFBVyxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7WUFDNUIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7WUFDekIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ2pDLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDO29CQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLGFBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQXJCLENBQXFCLENBQUMsQ0FBQztpQkFDekQ7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBVyxDQUFDLENBQUM7WUFDM0MsSUFBSSw0QkFBMEIsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxPQUFLLEdBQUcsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxFQUExQixDQUEwQixDQUFDLENBQUM7WUFDbkUsSUFBTSxXQUFXLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDcEUsSUFBRyxXQUFXLElBQUksQ0FBQyxFQUFDO2dCQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDakMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDOUQsSUFBRyxLQUFLLElBQUksQ0FBQyxFQUFDO3dCQUNaLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO3dCQUM1Qiw0QkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3RDLE9BQUssSUFBSSxDQUFDLENBQUM7cUJBQ1o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7YUFDSDtZQUNELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxPQUFLLElBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLDRCQUEwQixDQUFDO1lBQ3pFLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUssRUFBRSxFQUFDO2dCQUNuQyxnRUFBZ0U7Z0JBQ2hFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBRXZEO1NBQ0Q7SUFDRixDQUFDO0lBRUYsaUVBQTBCLEdBQTFCO1FBQUEsaUJBcUNDO1FBcENDLHVDQUF1QztRQUN2Qyx1Q0FBdUM7UUFDdkMscURBQXFEO1FBQ3JELHNDQUFzQztRQUN0QyxNQUFNO1FBQ04sS0FBSztRQUNMLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxFQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEVBQTFCLENBQTBCLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7b0JBQzdELEtBQUssRUFBRSxLQUFLO29CQUNaLFlBQVksRUFBRSxJQUFJO29CQUNsQixJQUFJLEVBQUU7d0JBQ0osT0FBTyxFQUFFLDJDQUEyQzt3QkFDcEQsWUFBWSxFQUFFLEtBQUs7d0JBQ25CLFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRixDQUFDLENBQUM7Z0JBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07b0JBQ3RDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7d0JBQzNCLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO3FCQUMzQjtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1NBQ0Y7YUFBSTtZQUNILElBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUM7Z0JBQzFCLHFJQUFxSTtnQkFDckksSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO2FBQzlFO2lCQUNHO2dCQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsc0VBQXNFLENBQUMsQ0FBQzthQUN4RztTQUNGO0lBQ0gsQ0FBQztJQUVELHlEQUFrQixHQUFsQjtRQUFBLGlCQW9DQztRQW5DQyxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFDdEUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBRTtZQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDM0ksMkJBQTJCO1lBQzNCLElBQUksU0FBTyxHQUFHLEVBQUUsQ0FBQTtZQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO2dCQUM3QyxJQUFNLElBQUksR0FBRztvQkFDWCxFQUFFLEVBQUUsVUFBVTtpQkFDZixDQUFBO2dCQUNELFNBQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25CLHlFQUF5RTtZQUMzRSxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksYUFBVyxHQUFHLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO2dCQUN4QyxJQUFNLFVBQVUsR0FBRztvQkFDakIsRUFBRSxFQUFFLFFBQVE7aUJBQ2IsQ0FBQTtnQkFDRCxhQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQy9CLENBQUMsQ0FBQyxDQUFBO1lBQ0YsSUFBSSxLQUFLLEdBQUc7Z0JBQ1YsV0FBVyxlQUFBO2dCQUNYLGFBQWEsRUFBRSxjQUFjO2FBQzlCLENBQUE7WUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLFNBQU8sRUFBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNuRSxJQUFJLFFBQVEsRUFBRTtvQkFDWixLQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkQsS0FBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7aUJBQ2pFO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsbURBQW1ELENBQUMsQ0FBQztpQkFDckY7WUFDSCxDQUFDLENBQUMsQ0FBQTtTQUNIO2FBQU07WUFDTCxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNDO0lBQ0gsQ0FBQztJQUVELGtEQUFXLEdBQVg7UUFBQSxpQkFtQkM7UUFsQkMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRTtZQUN6RSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDbEM7YUFBTTtZQUNMLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO2dCQUM3RCxLQUFLLEVBQUUsS0FBSztnQkFDWixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsSUFBSSxFQUFFO29CQUNKLE9BQU8sRUFBRSxpQ0FBaUM7b0JBQzFDLFlBQVksRUFBRSxLQUFLO29CQUNuQixZQUFZLEVBQUUsSUFBSTtpQkFDbkI7YUFDRixDQUFDLENBQUM7WUFDSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtnQkFDdEMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtvQkFDM0IsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxDQUFDO2lCQUNsQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQscURBQWMsR0FBZDtRQUFBLGlCQWl0Qkc7O1FBL3NCRCxvSUFBb0k7UUFDcEksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ3hCLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7WUFDbEgsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDakIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUMvQixJQUFNLFlBQVksR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBckQsQ0FBcUQsQ0FBQyxDQUFDO1lBQ2xILElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ2pCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFHSCxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDMUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMzRCxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqQztZQUNELElBQUcsS0FBSSxDQUFDLGlCQUFpQixFQUFDO2dCQUN4QixLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUNoQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBR0gsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsRUFBRTtZQUNwQyxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQztZQUN2RCxJQUFJLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDOUM7UUFHRCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sSUFBSSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtnQkFDN0osSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFFO29CQUNwQzs7Ozs7Ozs7O3NCQVNFO29CQUNGLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsaUJBQWlCLEVBQUU7d0JBQ3RGLElBQU0sT0FBTyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUEzQixDQUEyQixDQUFDLENBQUM7d0JBQzlFLElBQUksT0FBTyxFQUFFOzRCQUNaLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dDQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtnQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDOzZCQUMxTCxDQUFDLENBQUM7eUJBQ0w7cUJBRUY7eUJBQ0ksSUFBRyxLQUFJLENBQUMsaUJBQWlCLEVBQUM7d0JBQzlCLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDOzRCQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7NEJBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTs0QkFDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDO3lCQUMzTCxDQUFDLENBQUM7cUJBQ0o7aUJBQ0E7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7d0JBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO3dCQUNkLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFLLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUM7cUJBQzNMLENBQUMsQ0FBQztpQkFDSjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsRixzRkFBc0Y7UUFDdEYsSUFBSSxDQUFDLFlBQVksR0FBRyxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7UUFDbEgsSUFBSSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEVBQXRELENBQXNELENBQUMsQ0FBQztRQUd0SCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUMzQyxXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQzNCLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVc7Z0JBQ2hDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzFaLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL2MsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQUEsSUFBSSxDQUFDLFVBQVUsMENBQUUsY0FBYyxNQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsaUJBQWlCLElBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdkLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsS0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBQSxJQUFJLENBQUMsVUFBVSwwQ0FBRSxjQUFjLE1BQUksTUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxpQkFBaUIsSUFBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9jLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFBLElBQUksQ0FBQyxVQUFVLDBDQUFFLGNBQWMsTUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGlCQUFpQixJQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsZ0JBQWdCLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxZCxhQUFhLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLEtBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQUEsSUFBSSxDQUFDLFVBQVUsMENBQUUsY0FBYyxNQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEtBQUcsaUJBQWlCLElBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsS0FBRyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixLQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztTQUM1YixDQUFDLENBQUM7UUFJTCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtZQUMzRCxJQUFNLGlCQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29DQUN4RSxDQUFDO2dCQUNQLElBQUksV0FBVyxHQUFHLE9BQUssUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxFQUFFLEtBQUssaUJBQWUsQ0FBQyxDQUFDLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDO2dCQUM3RSxJQUFHLFdBQVcsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDO29CQUN4RCxPQUFLLFlBQVksR0FBRyxJQUFJLENBQUM7O2lCQUUxQjtxQkFBSTtvQkFDSCxPQUFLLFlBQVksR0FBRyxLQUFLLENBQUM7aUJBQzNCOzs7WUFQSCxLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsaUJBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO3NDQUFwQyxDQUFDOzs7YUFRUjtZQUNELElBQUksY0FBWSxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQzNCLG9GQUFvRjtnQkFDcEYsZ0hBQWdIO2dCQUNoSCwrRkFBK0Y7Z0JBQy9GLDJCQUEyQjtnQkFDM0IsTUFBTTtnQkFDTixJQUFJO2dCQUNKLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtvQkFDckUsSUFBRyxJQUFJLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBQzt3QkFDOUIsSUFBTSxRQUFNLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUksT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLEVBQXJELENBQXFELENBQUMsQ0FBQzt3QkFDM0csSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksUUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7NEJBQ25GLGNBQVksR0FBRyxJQUFJLENBQUM7eUJBQ3JCO3FCQUNOO2dCQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGNBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNyQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMvRDtTQUNGO1FBR0QsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsV0FBVyxFQUFFO1lBQzFELElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsS0FBSyxRQUFRLEVBQUU7Z0JBQ3ZFLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTtvQkFDOUIsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsZUFBZTt3QkFDbEcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUUsRUFBRTt3QkFDN0UsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7NEJBQ3BCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTs0QkFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3lCQUNsQyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0QsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkY7aUJBQU07Z0JBQ0wsSUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQ3pFLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07b0JBQ2hDLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUNwQixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7d0JBQ2pCLEtBQUssRUFBRSxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFO3FCQUM1QyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkY7U0FDRjtRQUdELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEVBQUU7WUFDekQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxLQUFLLE1BQU0sRUFBRTtnQkFDcEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUN4QixJQUFJLGNBQWMsR0FBRyxLQUFLLENBQUM7b0JBQzNCLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQU0sbUJBQW1CLEdBQUcsRUFBRSxDQUFDO29CQUMvQixJQUFJLEtBQUksQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMzRCxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQy9CLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFRO2dDQUN2RSxJQUFHLFFBQVEsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFDO29DQUNsQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUNBQ2hDOzRCQUNILENBQUMsQ0FBQyxDQUFBO3dCQUNKLENBQUMsQ0FBQyxDQUFBO3dCQUNGLHdDQUF3Qzt3QkFDeEMsdUhBQXVIO3dCQUN2SCw2QkFBNkI7d0JBQzdCLE1BQU07d0JBQ04sTUFBTTt3QkFDTixLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBUTs0QkFDdkUsSUFBRyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBQztnQ0FDdEIsY0FBYyxHQUFHLElBQUksQ0FBQzs2QkFDdkI7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTs0QkFDbkksSUFBTSxvQkFBa0IsR0FBRyxFQUFFLENBQUM7NEJBQzVCLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDL0IsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7b0NBQ3RFLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUM7d0NBQzlCLG9CQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQ0FDL0I7Z0NBQ0gsQ0FBQyxDQUFDLENBQUE7NEJBQ0osQ0FBQyxDQUFDLENBQUM7NEJBQ0wsb0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0NBQ3BKLFdBQVcsR0FBRyxJQUFJLENBQUM7aUNBQ3BCOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKO3FCQUNGO29CQUNELElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7b0JBQzNHLElBQU0sd0JBQXdCLEdBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUksT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLEVBQXJELENBQXFELENBQUMsQ0FBQztvQkFDNUgsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDO3dCQUNsQyxJQUFJLDJCQUF5QixHQUFHLEtBQUssQ0FBQzt3QkFDdEMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7NEJBQ3RFLElBQUcsUUFBUSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUM7Z0NBQzFDLDJCQUF5QixHQUFHLElBQUksQ0FBQzs2QkFDbEM7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osSUFBSSxDQUFDLENBQUMsMkJBQXlCLElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQ2hNLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTs0QkFDNUUsSUFBTSxrQkFBa0IsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQXpHLENBQXlHLENBQUMsQ0FBQzs0QkFDdEssSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUN2RCxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjO29DQUN2QyxJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxjQUFjLENBQUMsV0FBVyxLQUFJLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLEVBQUU7d0NBQzVQLHFCQUFxQixHQUFHLEtBQUssQ0FBQztxQ0FDL0I7Z0NBQ0gsQ0FBQyxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDOzZCQUMvQjt5QkFDRjtxQkFFRjtvQkFFQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUMsUUFBUTt3QkFDdkUsSUFBRyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBQzs0QkFDdEIsWUFBWSxHQUFHLElBQUksQ0FBQzt5QkFDckI7b0JBQ0gsQ0FBQyxDQUFDLENBQUE7b0JBQ0YsSUFBSSxDQUFDLENBQUMsY0FBYyxJQUFJLENBQUMsV0FBVyxJQUFJLHFCQUFxQixDQUFDLDhCQUE4QixDQUFDLENBQUMsdURBQXVELEVBQUU7d0JBQ3JKLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksS0FBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7NEJBQ3ZHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO2dDQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtnQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztnQ0FDcFQsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsS0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSzs2QkFDM0QsQ0FBQyxDQUFDO3lCQUNKO3FCQUNGO29CQUNELG1DQUFtQztvQkFDbkMsK0dBQStHO29CQUMvRyw2QkFBNkI7b0JBQzdCLDhCQUE4QjtvQkFDOUIsd0JBQXdCO29CQUN4Qiw4VEFBOFQ7b0JBQzlULG1FQUFtRTtvQkFDbkUsVUFBVTtvQkFDVixNQUFNO29CQUNOLElBQUk7Z0JBQ04sQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO2dCQUN0QixJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNyRjtpQkFBTTtnQkFDTCxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO2dCQUN2RCxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztvQkFDdEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7d0JBQ25CLElBQUksRUFBRSxLQUFLLENBQUMsWUFBWTt3QkFDeEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3FCQUNqQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDckY7U0FDRjtRQUdELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXOztZQUNsRixvR0FBb0c7WUFDcEcsdUdBQXVHO1lBQ3ZHLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0Qix1RUFBdUU7WUFDdEUsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVE7Z0JBQ3ZFLElBQUksV0FBVyxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxRQUFRLEVBQXBCLENBQW9CLENBQUMsQ0FBQztnQkFDbkUsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNoQyxDQUFDLENBQUMsQ0FBQztZQUNMLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFROztnQkFDMUIsSUFBRyxPQUFBLFFBQVEsMENBQUUsZ0JBQWdCLE1BQUssTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO29CQUNqRSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQy9ELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsZ0RBQWdELENBQUMsQ0FBQztpQkFDbEY7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLG1FQUFtRTtZQUNuRSx1Q0FBdUM7WUFDdkMsSUFBSSxrQ0FBa0MsR0FBRyxJQUFJLENBQUM7WUFDOUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7O2dCQUMxQixJQUFHLENBQUMsQ0FBQyxPQUFBLFFBQVEsMENBQUUsZ0JBQWdCLE1BQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxPQUFBLFFBQVEsMENBQUUsZ0JBQWdCLE1BQUssV0FBVyxDQUFDLGVBQWUsSUFBSSxPQUFBLFFBQVEsMENBQUUsZ0JBQWdCLE1BQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxFQUFDO29CQUMxTCxrQ0FBa0MsR0FBRyxLQUFLLENBQUM7aUJBQzVDO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDRixJQUFHLGtDQUFrQyxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFDO2dCQUMxRyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDdEYsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyx3REFBd0QsQ0FBQyxDQUFDO2FBQzFGO1lBRUMseUNBQXlDO1lBQ3pDLElBQUksV0FBVyxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxNQUFNLEVBQUU7Z0JBQ3hJLEtBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dCQUNyQixLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEYsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUN4QixJQUFJLGNBQWMsR0FBRyxLQUFLLENBQUM7b0JBQzNCLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzVELElBQU0scUJBQW1CLEdBQUcsRUFBRSxDQUFDO3dCQUM5QixLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQy9CLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO2dDQUMxQixJQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFDO29DQUNsQyxxQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUNBQ2hDOzRCQUNILENBQUMsQ0FBQyxDQUFBO3dCQUNKLENBQUMsQ0FBQyxDQUFBO3dCQUNGLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFROzRCQUN2RSxJQUFHLElBQUksQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFDO2dDQUN0QixjQUFjLEdBQUcsSUFBSSxDQUFDOzZCQUN2Qjt3QkFDSCxDQUFDLENBQUMsQ0FBQTt3QkFDRixJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFOzRCQUNuSSxJQUFNLG9CQUFrQixHQUFHLEVBQUUsQ0FBQzs0QkFDNUIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dDQUMvQixLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtvQ0FDdEUsSUFBRyxJQUFJLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBQzt3Q0FDOUIsb0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FDQUMvQjtnQ0FDSCxDQUFDLENBQUMsQ0FBQTs0QkFDSixDQUFDLENBQUMsQ0FBQzs0QkFDTCxvQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dDQUM3QixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRTtvQ0FDcEosV0FBVyxHQUFHLElBQUksQ0FBQztpQ0FDcEI7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7eUJBQ0o7cUJBQ0Y7b0JBRUQsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUksT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLEVBQXJELENBQXFELENBQUMsQ0FBQztvQkFDM0csSUFBTSx3QkFBd0IsR0FBRSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLFVBQVUsSUFBRyxPQUFBLFVBQVUsQ0FBQyxXQUFXLEtBQUksV0FBVyxDQUFDLGNBQWMsRUFBcEQsQ0FBb0QsQ0FBQyxDQUFBO29CQUN6SCxJQUFHLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQUM7d0JBQ2xDLElBQUksMkJBQXlCLEdBQUcsS0FBSyxDQUFDO3dCQUN0QyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTs0QkFDdEUsSUFBRyxRQUFRLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBQztnQ0FDMUMsMkJBQXlCLEdBQUcsSUFBSSxDQUFDOzZCQUNsQzt3QkFDSCxDQUFDLENBQUMsQ0FBQTt3QkFDSixJQUFJLENBQUMsQ0FBQywyQkFBeUIsSUFBSSxDQUFDLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDaE0sQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUM1RSxJQUFJLEtBQUksQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUMzRCxJQUFNLGtCQUFrQixHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBdEcsQ0FBc0csQ0FBQyxDQUFDO2dDQUN2SyxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQ3ZELGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGNBQWM7d0NBQ3ZDLElBQUksQ0FBQyxDQUFDLGNBQWMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxJQUFJLGNBQWMsQ0FBQyxXQUFXLEtBQUksd0JBQXdCLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUMsRUFBRTs0Q0FDNVAscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3lDQUMvQjtvQ0FDSCxDQUFDLENBQUMsQ0FBQztpQ0FDSjtxQ0FBTTtvQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7aUNBQy9COzZCQUNGO2lDQUFNO2dDQUNMLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2QkFDL0I7eUJBQ0Y7cUJBQ0Y7b0JBQ0QsSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLFdBQVcsSUFBSSxxQkFBcUIsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRTt3QkFDckYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFFLHVEQUF1RDs0QkFDMUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7Z0NBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUztnQ0FDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO2dDQUNkLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLHFCQUFxQixDQUFDO2dDQUNwVCxjQUFjLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixLQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUEsSUFBSSxDQUFBLENBQUMsQ0FBQSxLQUFLOzZCQUMzRCxDQUFDLENBQUM7eUJBQ0o7cUJBQ0Y7Z0JBRUgsQ0FBQyxDQUFDLENBQUM7Z0JBQ0osS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3ZCO1lBR0QsNkJBQTZCO1lBQzdCLElBQUksV0FBVyxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFO2dCQUNwRSxJQUFJLHNCQUFvQixHQUFHLEtBQUssQ0FBQztnQkFDakMsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7b0JBQ3ZDLElBQUcsT0FBQSxXQUFXLENBQUMsQ0FBQyxDQUFDLDBDQUFFLGdCQUFnQixNQUFLLE1BQU0sRUFBQzt3QkFDN0Msc0JBQW9CLEdBQUcsSUFBSSxDQUFDO3dCQUM1QixNQUFNO3FCQUNQO2lCQUNGO2dCQUNELElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDLFdBQVcsS0FBSyxRQUFRLEVBQUU7b0JBQ3ZFLEtBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO29CQUV0QixLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07d0JBQzlCLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGVBQWU7NEJBQ2xHLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLElBQUksQ0FBQyxzQkFBb0IsQ0FBQyxFQUFFOzRCQUUvRSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQ0FDcEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO2dDQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUU7NkJBQ2xDLENBQUMsQ0FBQzt5QkFFSjtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxLQUFJLENBQUMsbUJBQW1CLEdBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDL0M7YUFDRjtZQUdELElBQUksWUFBWSxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLEtBQUksQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUMzRCxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQy9CLHlDQUF5QztvQkFDekMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7d0JBQ3RFLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUM7NEJBQzlCLElBQU0sUUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7NEJBQzNHLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLFFBQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dDQUN2RixZQUFZLEdBQUcsSUFBSSxDQUFDOzZCQUNyQjt5QkFDRjtvQkFDSCxDQUFDLENBQUMsQ0FBQTtnQkFDSixDQUFDLENBQUMsQ0FBQTthQUNIO1lBQ0QsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7Z0JBQzNELElBQU0saUJBQWUsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7d0NBQ3hFLENBQUM7b0JBQ1AsSUFBSSxhQUFXLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsRUFBRSxLQUFLLGlCQUFlLENBQUMsQ0FBQyxDQUFDLEVBQTlCLENBQThCLENBQUMsQ0FBQztvQkFDN0UsSUFBRyxhQUFXLElBQUksYUFBVyxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQzt3QkFDeEQsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O3FCQUUxQjt5QkFBSTt3QkFDSCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztxQkFDM0I7O2dCQVBILEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7MENBQXBDLENBQUM7OztpQkFRUjthQUNGO1lBQ0QsSUFBSSxZQUFZLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTtnQkFDckMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDL0Q7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDOUQ7WUFDRCxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDOUQsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBU0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFdBQVc7WUFDbEYsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0JBQzNELElBQU0saUJBQWUsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7NENBQ3hFLENBQUM7d0JBQ1AsSUFBSSxXQUFXLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsRUFBRSxLQUFLLGlCQUFlLENBQUMsQ0FBQyxDQUFDLEVBQTlCLENBQThCLENBQUMsQ0FBQzt3QkFDN0UsSUFBRyxXQUFXLElBQUksV0FBVyxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQzs0QkFDeEQsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7O3lCQUUxQjs2QkFBSTs0QkFDSCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzt5QkFDM0I7O29CQVBILEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxpQkFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7OENBQXBDLENBQUM7OztxQkFRUjtpQkFDRjtnQkFDRCxLQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUMvRCxJQUFJLFdBQVcsS0FBSyxRQUFRLEVBQUU7b0JBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTt3QkFDOUIsSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsZUFBZTs0QkFDbEcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTs0QkFDMUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7Z0NBQ3RCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTtnQ0FDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFOzZCQUNsQyxDQUFDLENBQUM7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU0sSUFBSSxXQUFXLEtBQUssUUFBUSxFQUFFO29CQUNyQyxJQUFNLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDekUsbUJBQW1CLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTt3QkFDaEMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7NEJBQ3BCLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSTs0QkFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUU7eUJBQzVDLENBQUMsQ0FBQztvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssS0FBSyxNQUFNLEVBQUU7b0JBQ3JFLEtBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO29CQUNyQixLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzlELEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTt3QkFDeEIsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUM7d0JBQ2pDLElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7d0JBQzNHLElBQU0sd0JBQXdCLEdBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUcsT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQXBELENBQW9ELENBQUMsQ0FBQTt3QkFDekgsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxFQUFDOzRCQUNsQyxJQUFJLDJCQUF5QixHQUFHLEtBQUssQ0FBQzs0QkFDdEMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7Z0NBQ3RFLElBQUcsUUFBUSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUM7b0NBQzFDLDJCQUF5QixHQUFHLElBQUksQ0FBQztpQ0FDbEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7NEJBQ0osSUFBSSxDQUFDLENBQUMsMkJBQXlCLElBQUksQ0FBQyxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3hMLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQzNELElBQU0sa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUF0RyxDQUFzRyxDQUFDLENBQUM7b0NBQ3ZLLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDdkQsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsY0FBYzs0Q0FDdkMsSUFBSSxDQUFDLENBQUMsY0FBYyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksY0FBYyxDQUFDLFdBQVcsS0FBSSx3QkFBd0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxFQUFFO2dEQUM1UCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7NkNBQy9CO3dDQUNILENBQUMsQ0FBQyxDQUFDO3FDQUNKO3lDQUFNO3dDQUNMLHFCQUFxQixHQUFHLEtBQUssQ0FBQztxQ0FDL0I7aUNBQ0Y7cUNBQU07b0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDO2lDQUMvQjs2QkFDRjt5QkFDRjt3QkFDQyxJQUFJLHFCQUFxQixFQUFFOzRCQUN6QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEVBQUUsdURBQXVEO2dDQUNqTCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQ0FDbkIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTO29DQUNwQixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2QsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMscUJBQXFCLENBQUM7b0NBQ25ULGNBQWMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEtBQUcsTUFBTSxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUEsQ0FBQyxDQUFBLEtBQUs7aUNBQzNELENBQUMsQ0FBQzs2QkFDSjt5QkFDRjtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBRTVEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFHSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsV0FBVztZQUNsRixJQUFJLFdBQVcsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUssTUFBTSxFQUFFO2dCQUNwRixJQUFJLGNBQVksR0FBRyxFQUFFLENBQUE7Z0JBQ3JCLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7b0JBQ3hDLElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hGLElBQUcsS0FBSyxHQUFHLENBQUMsRUFBQzt3QkFDWCxjQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3FCQUM3QjtnQkFDSCxDQUFDLENBQUMsQ0FBQTtnQkFDRixLQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDckIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUM5RCxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ3hCLElBQUksY0FBYyxHQUFHLEtBQUssQ0FBQztvQkFDM0IsSUFBSSxXQUFXLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQztvQkFDakMsSUFBSSxLQUFJLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDM0QsSUFBTSxxQkFBbUIsR0FBRyxFQUFFLENBQUM7d0JBQy9CLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTs0QkFDL0IsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7Z0NBQ3RFLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLEVBQUM7b0NBQ2xDLHFCQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDaEM7NEJBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osQ0FBQyxDQUFDLENBQUE7d0JBQ0YsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVE7NEJBQ3ZFLElBQUcsSUFBSSxDQUFDLEVBQUUsS0FBSyxRQUFRLEVBQUM7Z0NBQ3RCLGNBQWMsR0FBRyxJQUFJLENBQUM7NkJBQ3ZCO3dCQUNILENBQUMsQ0FBQyxDQUFBO3dCQUNGLElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsRUFBRTs0QkFDeEUsSUFBTSxvQkFBa0IsR0FBRyxFQUFFLENBQUM7NEJBQzVCLEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDL0IsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7b0NBQ3RFLElBQUcsSUFBSSxDQUFDLFVBQVUsS0FBSyxRQUFRLEVBQUM7d0NBQzlCLG9CQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztxQ0FDL0I7Z0NBQ0gsQ0FBQyxDQUFDLENBQUE7NEJBQ0osQ0FBQyxDQUFDLENBQUM7NEJBQ0wsb0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQUU7b0NBQ3hHLFdBQVcsR0FBRyxJQUFJLENBQUM7aUNBQ3BCOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKO3FCQUNGO29CQUNELElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsVUFBVSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsY0FBYyxFQUFyRCxDQUFxRCxDQUFDLENBQUM7b0JBQzNHLElBQU0sd0JBQXdCLEdBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUcsT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQXBELENBQW9ELENBQUMsQ0FBQTtvQkFDekgsSUFBSSx5QkFBeUIsR0FBRyxLQUFLLENBQUM7b0JBQ3RDLElBQUcsSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQzt3QkFDbEMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7NEJBQ3RFLElBQUcsUUFBUSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUM7Z0NBQzFDLHlCQUF5QixHQUFHLElBQUksQ0FBQzs2QkFDbEM7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0osSUFBSSxDQUFDLENBQUMseUJBQXlCOytCQUMxQixDQUFDLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQ0FDdEgsQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFOzRCQUM1RSxJQUFJLEtBQUksQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUMzRCxJQUFNLG9CQUFrQixHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBdEcsQ0FBc0csQ0FBQyxDQUFDO3dEQUcvSixDQUFDO29DQUNQLGNBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO3dDQUMzQixJQUFHLFFBQVEsS0FBSyxvQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUM7NENBQ25ELG9CQUFrQixDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7eUNBQ2hDO29DQUNILENBQUMsQ0FBQyxDQUFBOztnQ0FOSixzSEFBc0g7Z0NBQ3RILEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxvQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFOzRDQUFyQyxDQUFDO2lDQU1SO2dDQUNELElBQUksb0JBQWtCLElBQUksb0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQ0FDdkQsb0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsY0FBYzt3Q0FDdkMsSUFBSSxDQUFDLENBQUMsY0FBYyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLElBQUksY0FBYyxDQUFDLFdBQVcsS0FBSSx3QkFBd0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsQ0FBQyxFQUFFOzRDQUM1UCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7eUNBQy9CO29DQUNILENBQUMsQ0FBQyxDQUFDO2lDQUNKO3FDQUFNO29DQUNMLHFCQUFxQixHQUFHLEtBQUssQ0FBQztpQ0FDL0I7NkJBQ0Y7aUNBQU07Z0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDOzZCQUMvQjt5QkFDRjtxQkFDRjtvQkFDQyxJQUFJLENBQUMsY0FBYyxJQUFJLENBQUMsV0FBVyxJQUFJLHFCQUFxQixFQUFFO3dCQUM1RCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEVBQUUsdURBQXVEOzRCQUNqTCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztnQ0FDbkIsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTO2dDQUNwQixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0NBQ2QsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMscUJBQXFCLENBQUM7Z0NBQ3BULGNBQWMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEtBQUksTUFBTSxDQUFFLENBQUMsQ0FBQSxJQUFJLENBQUEsQ0FBQyxDQUFBLEtBQUs7NkJBQzlELENBQUMsQ0FBQzt5QkFDSDtxQkFDRjtnQkFDRCxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBR0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFVBQVU7WUFDaEYsSUFBSSxVQUFVLEVBQUU7Z0JBQ2QsS0FBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxVQUFVLEtBQUssT0FBTyxFQUFFO29CQUMxQixJQUFNLFVBQVUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUN2RCxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSzt3QkFDdEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7NEJBQ25CLElBQUksRUFBRSxLQUFLLENBQUMsWUFBWTs0QkFDeEIsS0FBSyxFQUFFLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3lCQUNqQyxDQUFDLENBQUM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU0sSUFBSSxVQUFVLEtBQUssTUFBTSxFQUFFO29CQUNoQyxLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7d0JBQ3hCLElBQUksY0FBYyxHQUFHLEtBQUssQ0FBQzt3QkFDM0IsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUM7d0JBQ2pDLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQzt3QkFDeEIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVE7NEJBQ3ZFLElBQUcsSUFBSSxDQUFDLEVBQUUsS0FBSyxRQUFRLEVBQUM7Z0NBQ3RCLGNBQWMsR0FBRyxJQUFJLENBQUM7NkJBQ3ZCO3dCQUNILENBQUMsQ0FBQyxDQUFBO3dCQUNGLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQzVELElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsSUFBSSxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7Z0NBQ2xJLElBQU0sb0JBQWtCLEdBQUcsRUFBRSxDQUFDO2dDQUM5QixLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0NBQy9CLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO3dDQUN0RSxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFDOzRDQUM5QixvQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7eUNBQy9CO29DQUNILENBQUMsQ0FBQyxDQUFBO2dDQUNKLENBQUMsQ0FBQyxDQUFDO2dDQUNILG9CQUFrQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0NBQzdCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsS0FBSyxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFO3dDQUNwSixXQUFXLEdBQUcsSUFBSSxDQUFDO3FDQUNwQjtnQ0FDSCxDQUFDLENBQUMsQ0FBQzs2QkFDSjt5QkFDRjt3QkFDRCxJQUFNLE1BQU0sR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxVQUFBLFVBQVUsSUFBSSxPQUFBLFVBQVUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsRUFBckQsQ0FBcUQsQ0FBQyxDQUFDO3dCQUMzRyxJQUFNLHdCQUF3QixHQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFHLE9BQUEsVUFBVSxDQUFDLFdBQVcsS0FBSSxXQUFXLENBQUMsY0FBYyxFQUFwRCxDQUFvRCxDQUFDLENBQUE7d0JBQ3pILElBQUcsSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sRUFBQzs0QkFDbEMsSUFBSSwyQkFBeUIsR0FBRyxLQUFLLENBQUM7NEJBQ3RDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO2dDQUN0RSxJQUFHLFFBQVEsS0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxFQUFDO29DQUMxQywyQkFBeUIsR0FBRyxJQUFJLENBQUM7aUNBQ2xDOzRCQUNILENBQUMsQ0FBQyxDQUFBOzRCQUNGLElBQUksQ0FBQyxDQUFDLDJCQUF5QjttQ0FDMUIsQ0FBQyxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7b0NBQ2xLLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQ0FDNUUsSUFBSSxLQUFJLENBQUMsZUFBZSxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQ0FDM0QsSUFBTSxrQkFBa0IsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQXRHLENBQXNHLENBQUMsQ0FBQztvQ0FDdkssSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dDQUN2RCxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjOzRDQUN2QyxJQUFJLENBQUMsQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEtBQUssTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxjQUFjLENBQUMsV0FBVyxLQUFJLHdCQUF3QixDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDLEVBQUU7Z0RBQzVQLHFCQUFxQixHQUFHLEtBQUssQ0FBQzs2Q0FDL0I7d0NBQ0gsQ0FBQyxDQUFDLENBQUM7cUNBQ0o7eUNBQU07d0NBQ0wscUJBQXFCLEdBQUcsS0FBSyxDQUFDO3FDQUMvQjtpQ0FDRjtxQ0FBTTtvQ0FDTCxxQkFBcUIsR0FBRyxLQUFLLENBQUM7aUNBQy9COzZCQUNGO3lCQUNKO3dCQUNDLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxXQUFXLElBQUkscUJBQXFCLEVBQUU7NEJBQzVELElBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxPQUFPLEVBQUcsRUFBRSx1REFBdUQ7Z0NBQzNHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO29DQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0NBQ3BCLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDZCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztvQ0FDcFQsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsS0FBSSxNQUFNLENBQUMsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSztpQ0FDNUQsQ0FBQyxDQUFDOzZCQUNKO3lCQUNGO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDM0Q7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQzlFLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUMsQ0FBQyxDQUFDO1FBR0gsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQzFELEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQUVILCtDQUFRLEdBQVI7UUFBQSxpQkFrQkM7UUFqQkMsSUFBTSxjQUFjLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDN0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7WUFDekIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtnQkFDeEMsSUFBRyxJQUFJLENBQUMsRUFBRSxLQUFLLFFBQVEsRUFBQztvQkFDdEIsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3BDO1lBQ0gsQ0FBQyxDQUFDLENBQUE7UUFDSixDQUFDLENBQUMsQ0FBQTtRQUNGLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFsQyxDQUFrQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdkssSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO1lBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1SCxDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRUQsc0RBQWUsR0FBZjtRQUFBLGlCQTZCRDs7UUE1QkcsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxVQUFVLElBQUksT0FBQSxVQUFVLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLEVBQXJELENBQXFELENBQUMsQ0FBQztRQUM3Ryw4QkFBOEI7UUFDM0IsSUFBRyxPQUFBLElBQUksQ0FBQyx5QkFBeUIsMENBQUUsZ0JBQWdCLE1BQUksTUFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFDO1lBQ3ZKLElBQU0sZUFBYSxHQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQUEsVUFBVSxJQUFHLE9BQUEsQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBbkYsQ0FBbUYsQ0FBQyxDQUFBLENBQUEsMENBQTBDO1lBQ3ZMLElBQUksQ0FBQyxVQUFVLEdBQUUsZUFBYSxDQUFBO1NBQzlCO2FBRUcsSUFBRyxPQUFBLElBQUksQ0FBQyx5QkFBeUIsMENBQUUsZ0JBQWdCLE1BQUksTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBQztZQUMzSixJQUFNLFVBQVEsR0FBRSxPQUFBLElBQUksQ0FBQyx5QkFBeUIsMENBQUUsY0FBYyxJQUFDLEVBQUUsQ0FBQztZQUNsRSxJQUFNLGtCQUFnQixHQUFFLElBQUksR0FBRyxFQUFFLENBQUM7WUFDakMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ2pDLElBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsVUFBUSxDQUFDLEVBQUM7b0JBQ3JDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTSxJQUFFLE9BQUEsa0JBQWdCLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUE7aUJBQy9EO2dCQUNGLCtQQUErUDtnQkFDaFEsSUFBTSxhQUFhLEdBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBQSxVQUFVLElBQUUsT0FBQSxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFuRixDQUFtRixDQUFDLENBQUE7Z0JBQzNJLEtBQUksQ0FBQyxVQUFVLEdBQUUsYUFBYSxDQUFBO1lBQy9CLENBQUMsQ0FBQyxDQUFBO1NBQ0g7YUFDSyxJQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFDO1lBQzNGLElBQUksYUFBYSxHQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQUEsVUFBVSxJQUFHLE9BQUEsQ0FBQyxLQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBbkYsQ0FBbUYsQ0FBQyxDQUFBLENBQUEsMENBQTBDO1lBRXJMLElBQUksQ0FBQyxVQUFVLEdBQUUsYUFBYSxDQUFBO1NBQzlCO2FBQ0c7WUFDTCxJQUFNLGVBQWEsR0FBRSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFBLFVBQVUsSUFBRyx1RUFBdUUsQ0FBQyxPQUFBLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEVBQW5GLENBQW1GLENBQUMsQ0FBQTtZQUNyTixJQUFJLENBQUMsVUFBVSxHQUFFLGVBQWEsQ0FBQTtTQUM3QjtJQUNMLENBQUM7O2dCQTdzQ3lCLGFBQWE7Z0JBQ1osY0FBYztnQkFDakIsV0FBVztnQkFDSCxtQkFBbUI7Z0JBQ3BCLGtCQUFrQjtnQkFDOUIsU0FBUzs7SUF4RGpCO1FBQVIsS0FBSyxFQUFFO2tFQUFVO0lBQ1Q7UUFBUixLQUFLLEVBQUU7a0VBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTt5RUFBaUI7SUFDaEI7UUFBUixLQUFLLEVBQUU7a0VBQWlCO0lBQ2hCO1FBQVIsS0FBSyxFQUFFOzBFQUFrQjtJQUNqQjtRQUFSLEtBQUssRUFBRTsyRUFBbUI7SUFDbEI7UUFBUixLQUFLLEVBQUU7dUVBQWU7SUFDYjtRQUFULE1BQU0sRUFBRTs4RUFBZ0Q7SUFDL0M7UUFBVCxNQUFNLEVBQUU7bUZBQXFEO0lBQ3BEO1FBQVQsTUFBTSxFQUFFO3VGQUF5RDtJQUd4RDtRQUFULE1BQU0sRUFBRTswRUFBNEM7SUFDM0M7UUFBVCxNQUFNLEVBQUU7b0ZBQXNEO0lBQ3REO1FBQVIsS0FBSyxFQUFFOzJFQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTttRkFBMEI7SUFDekI7UUFBUixLQUFLLEVBQUU7MkVBQW1CO0lBbEJoQiw0QkFBNEI7UUFMeEMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxvNEdBQXFEOztTQUV0RCxDQUFDO09BQ1csNEJBQTRCLENBbXdDeEM7SUFBRCxtQ0FBQztDQUFBLEFBbndDRCxJQW13Q0M7U0Fud0NZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgZm9ya0pvaW4gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgU3RhdHVzVHlwZXMgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdGF0dXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3N0YXR1cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3Byb2plY3QtdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGFza1NlcnZpY2UgfSBmcm9tICcuLi90YXNrLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tY3VzdG9tLXdvcmtmbG93LWZpZWxkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY3VzdG9tLXdvcmtmbG93LWZpZWxkLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jdXN0b20td29ya2Zsb3ctZmllbGQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tV29ya2Zsb3dGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHJ1bGVEYXRhO1xyXG4gIEBJbnB1dCgpIHJ1bGVMaXN0O1xyXG4gIEBJbnB1dCgpIHByb2plY3RSdWxlTGlzdDtcclxuICBASW5wdXQoKSB0YXNrRGF0YTogYW55W107XHJcbiAgQElucHV0KCkgaXNSdWxlc0luaXRpYXRlZDtcclxuICBASW5wdXQoKSBpc0N1cnJlbnRUYXNrUnVsZTtcclxuICBASW5wdXQoKSBydWxlTGlzdEdyb3VwO1xyXG4gIEBPdXRwdXQoKSBjbG9zZUNhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSByZW1vdmVXb3JrZmxvd1J1bGVIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGVuYWJsZUluaXRpYXRlV29ya2Zsb3dIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgLy9AT3V0cHV0KCkgaXNUYXNrTGV2ZWxSdWxlRWRpdCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIGlzdGFza1J1bGVFZGl0PWZhbHNlO1xyXG4gIEBPdXRwdXQoKSBydWxlR3JvdXBIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIHJ1bGVEZXBlbmRlbmN5RXJyb3JIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgaXNUYXNrTGV2ZWxjYWxsZWQ7XHJcbiAgQElucHV0KCkgdGFza1dvcmtGbG93UnVsZVRvRGlzcGxheVxyXG4gIEBJbnB1dCgpIHRhc2tydWxlTGlzdEdyb3VwO1xyXG4gIC8vQElucHV0KCkgYWxsUnVsZUxpc3RHcm91cDsgTVZTUy0zMjJcclxuICAgaXNDaGlsZERlcGVuZGVjaWVzPSBmYWxzZTtcclxuICBjdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybTogRm9ybUdyb3VwO1xyXG4gIHRyaWdnZXJUeXBlcyA9IFt7XHJcbiAgICBuYW1lOiAnU3RhdHVzJyxcclxuICAgIHZhbHVlOiAnU1RBVFVTJ1xyXG4gIH0sIHtcclxuICAgIG5hbWU6ICdBY3Rpb24nLFxyXG4gICAgdmFsdWU6ICdBQ1RJT04nXHJcbiAgfV07XHJcbiAgdGFyZ2V0VHlwZXMgPSBbe1xyXG4gICAgbmFtZTogJ1Rhc2snLFxyXG4gICAgdmFsdWU6ICdUQVNLJ1xyXG4gIH0sIHtcclxuICAgIG5hbWU6ICdFdmVudCcsXHJcbiAgICB2YWx1ZTogJ0VWRU5UJ1xyXG4gIH1dO1xyXG4gIHRyaWdnZXJEYXRhID0gW107XHJcbiAgdGFyZ2V0RGF0YTphbnkgPSBbXTtcclxuICB0YXNrTGlzdCA9IFtdO1xyXG4gIHByb2plY3RJZDtcclxuICBpc1J1bGVDaGFuZ2VkO1xyXG4gIHRhc2tTdGF0dXNlcztcclxuICBjdXJyZW50UnVsZUxpc3Q7XHJcbiAgYWxsUnVsZUxpc3QgPSBbXTtcclxuICBpc1J1bGVTYXZlZDtcclxuICBhcHByb3ZhbFRhc2s7XHJcbiAgY3VycmVudFNvdXJjZVRhc2tzID0gW107XHJcbiAgdGFyZ2V0VGFzazogYW55O1xyXG4gIG5vUHJlZGVjZXNzb3JSdWxlTGlzdCA9IFtdO1xyXG4gIGh0dHBSZXF1ZXN0QXJyYXkgPSBbXTtcclxuICBzZWxlY3RlZFRyaWdnZXJEYXRhO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHB1YmxpYyBwcm9qZWN0VXRpbFNlcnZpY2U6IFByb2plY3RVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZ1xyXG4gICkgeyB9XHJcbiAgXHJcbiAgLy9mb3IgRWRpdGluZyB3b3JrZmxvdyBSdWxlcy4uLlxyXG4gIHVwZGF0ZVdvcmtmbG93UnVsZSgpe1xyXG4gICAgaWYodGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5sZW5ndGggPiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoKXtcclxuICAgICAgLy9SZW1vdmluZyBhIHJ1bGUgZnJvbSB0aGUgc291cmNlIHRhc2tzIGxpc3RcclxuICAgICAgY29uc3QgbmV3VGFza3MgPSBbXTtcclxuICAgICAgdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID10aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2suaW5kZXhPZih0YXNrKTtcclxuICAgICAgICBpZihpbmRleCA8IDApe1xyXG4gICAgICAgICAgbmV3VGFza3MucHVzaCh0YXNrKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIGxldCBkZWxldGVkUnVsZXMgPSBbXVxyXG4gICAgICBuZXdUYXNrcy5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgIGNvbnN0IHJ1bGUgPXtcclxuICAgICAgICBjdXJyZW50VGFzazogdGFzayxcclxuICAgICAgICBpc0luaXRpYWxSdWxlOiB0aGlzLnJ1bGVEYXRhLmlzSW5pdGlhbFJ1bGUsXHJcbiAgICAgICAgdGFyZ2V0RGF0YTogdGhpcy5ydWxlRGF0YS50YXJnZXREYXRhLFxyXG4gICAgICAgIHRhcmdldFR5cGU6IHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSxcclxuICAgICAgICB0cmlnZ2VyRGF0YTogdGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSxcclxuICAgICAgICB0cmlnZ2VyVHlwZTogdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZVxyXG4gICAgICAgIH1cclxuICAgICAgICBkZWxldGVkUnVsZXMucHVzaChydWxlKTtcclxuICAgICAgfSlcclxuICAgICAgbGV0IG9yaWdpbmFsUnVsZXMgPSBbXTtcclxuICAgICAgZm9yKGxldCBpPTA7IGkgPCB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICBjb25zdCBydWxlID17XHJcbiAgICAgICAgICBjdXJyZW50VGFzazogdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFza1tpXSxcclxuICAgICAgICAgIGlzSW5pdGlhbFJ1bGU6IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSxcclxuICAgICAgICAgIHRhcmdldERhdGE6IHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSxcclxuICAgICAgICAgIHRhcmdldFR5cGU6IHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSxcclxuICAgICAgICAgIHRyaWdnZXJEYXRhOiB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhLFxyXG4gICAgICAgICAgdHJpZ2dlclR5cGU6IHRoaXMucnVsZURhdGEudHJpZ2dlclR5cGUsXHJcbiAgICAgICAgICB3b3JrZmxvd1J1bGVJZDogdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZFtpXVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIG9yaWdpbmFsUnVsZXMucHVzaChydWxlKTtcclxuICAgICAgfVxyXG4gICAgICBsZXQgZGVsZXRlZFJ1bGVJZHMgPSBbXTtcclxuICAgICAgZGVsZXRlZFJ1bGVzLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgb3JpZ2luYWxSdWxlcy5mb3JFYWNoKGVhY2hSdWxlID0+IHtcclxuICAgICAgICAgIGlmKHJ1bGUuY3VycmVudFRhc2sgPT0gZWFjaFJ1bGUuY3VycmVudFRhc2spe1xyXG4gICAgICAgICAgICBkZWxldGVkUnVsZUlkcy5wdXNoKGVhY2hSdWxlLndvcmtmbG93UnVsZUlkKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICB9KVxyXG4gICAgICBjb25zdCBvdGhlclBhcmFtc0NoYW5nZWQgPSAoKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyVHlwZSAhPT0gdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZSlcclxuICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyRGF0YSAhPT0gdGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSlcclxuICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXRUeXBlICE9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGUpXHJcbiAgICAgIHx8ICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudGFyZ2V0RGF0YSAhPT0gdGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKVxyXG4gICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmlzSW5pdGlhbFJ1bGUgIT09IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSkpO1xyXG4gICAgICBpZihvdGhlclBhcmFtc0NoYW5nZWQpXHJcbiAgICAgICB7XHJcbiAgICAgICAgICBmb3IobGV0IGk9MDsgaTx0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICBjb25zdCBydWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgICAgICBydWxlLmN1cnJlbnRUYXNrID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrW2ldO1xyXG4gICAgICAgICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZShydWxlLHRoaXMucHJvamVjdElkLHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWRbaV0pKTtcclxuICAgICAgICAgIH1cclxuICAgICAgIH1cclxuICAgICAgbGV0IGFwcHJvdmFsVGFza3MgPSB0aGlzLnRhc2tEYXRhLmZpbHRlcih0YXNrID0+IHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKTtcclxuICAgICAgbGV0IHJlbW92ZWRQYXJlbnRUYXNrcyA9IFtdO1xyXG4gICAgICBhcHByb3ZhbFRhc2tzLmZvckVhY2goYXBwcm92YWxUYXNrID0+IHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IG5ld1Rhc2tzLmluZGV4T2YoU3RyaW5nKGFwcHJvdmFsVGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG4gICAgICAgIGlmKGluZGV4ID49IDApe1xyXG4gICAgICAgICAgcmVtb3ZlZFBhcmVudFRhc2tzLnB1c2gobmV3VGFza3NbaW5kZXhdKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIC8vY2hlY2tpbmcgaWYgdGhlIHJlbW92ZWQgdGFzayBpcyBhIHBhcmVudCB0YXNrIGhlbmNlIGNoZWNraW5nIGRlcGVuZGVuY2llcy4uLlxyXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG4gICAgICBsZXQgYXBwcm92YWxUYXNrc0lkTGlzdCA9IFtdO1xyXG4gICAgICByZW1vdmVkUGFyZW50VGFza3MuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgY29uc3QgYXBwcm92YWxUYXNrSWRzID0gdGhpcy50YXNrRGF0YS5maWx0ZXIodGFzayA9PiBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkgPT0gZWFjaFRhc2sgKTtcclxuICAgICAgICBhcHByb3ZhbFRhc2tzSWRMaXN0LnB1c2goLi4uYXBwcm92YWxUYXNrSWRzKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGxldCBhbGxUYXNrc2luUnVsZXMgPSBbXTtcclxuICAgICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgYWxsVGFza3NpblJ1bGVzLnB1c2goLi4ucnVsZS5jdXJyZW50VGFzayk7XHJcbiAgICAgICAgYWxsVGFza3NpblJ1bGVzLnB1c2gocnVsZS50YXJnZXREYXRhKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICBsZXQgYWZmZWN0ZWRBcHByb3ZhbFRhc2tzID0gW107XHJcbiAgICAgIGFwcHJvdmFsVGFza3NJZExpc3QuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSBhbGxUYXNrc2luUnVsZXMuaW5kZXhPZihlYWNoVGFzay5JRCk7XHJcbiAgICAgICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgICAgICBhZmZlY3RlZEFwcHJvdmFsVGFza3MucHVzaChlYWNoVGFzayk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgXHJcbiAgICAgIHRoaXMucnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4gcnVsZS5oYXNQcmVkZWNlc3NvciA9IHRydWUpO1xyXG4gICAgICAgIHRoaXMucnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT57XHJcbiAgICAgICAgICBsZXQgcnVsZVRhc2tzID0gW107XHJcbiAgICAgICAgICBydWxlVGFza3MucHVzaCguLi5ydWxlLmN1cnJlbnRUYXNrKTtcclxuICAgICAgICAgIHJ1bGVUYXNrcy5wdXNoKHJ1bGUudGFyZ2V0RGF0YSk7XHJcbiAgICAgICAgICBhZmZlY3RlZEFwcHJvdmFsVGFza3MuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gcnVsZVRhc2tzLmluZGV4T2YoZWFjaFRhc2suSUQpO1xyXG4gICAgICAgICAgICBpZihpbmRleCA+PSAwKXtcclxuICAgICAgICAgICAgICBydWxlLmhhc1ByZWRlY2Vzc29yID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfSlcclxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbiAgICAgICAgaWYoIShhZmZlY3RlZEFwcHJvdmFsVGFza3MgJiYgYWZmZWN0ZWRBcHByb3ZhbFRhc2tzLmxlbmd0aCA+IDApKXtcclxuICAgICAgICBsZXQgcnVsZUlkcyA9IFtdXHJcbiAgICAgICAgZGVsZXRlZFJ1bGVJZHMuZm9yRWFjaCh3b3JrZmxvd0lkID0+IHtcclxuICAgICAgICBjb25zdCBSdWxlID0ge1xyXG4gICAgICAgICAgSWQ6IHdvcmtmbG93SWRcclxuICAgICAgICB9XHJcbiAgICAgICAgcnVsZUlkcy5wdXNoKFJ1bGUpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgdGhpcy50YXNrU2VydmljZS5yZW1vdmVXb3JrZmxvd1J1bGUocnVsZUlkcyx7fSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHsgXHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgLy8gdGhpcy5yZW1vdmVXb3JrZmxvd1J1bGVIYW5kbGVyLm5leHQodGhpcy5ydWxlRGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaCh0YXNrID0+IHRoaXMuY3VycmVudFNvdXJjZVRhc2tzLnB1c2godGFzaykpO1xyXG4gICAgICAgICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5pc1J1bGVTYXZlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlSW5pdGlhdGVXb3JrZmxvd0hhbmRsZXIubmV4dCgpO1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnV29ya2Zsb3cgUnVsZSBoYXMgYmVlbiByZW1vdmVkJyk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIHJlbW92aW5nIHdvcmtmbG93IHJ1bGUnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdFZGl0IE9wZXJhdGlvbiBjb3VsZCBub3QgYmUgUGVyZm9ybWVkIGJlY2F1c2UgSXQgaXMgYSBQYXJlbnQgVGFzayBhbmQgaXQgV2lsbCBhZmZlY3QgdGhlIGhpZ2hsaWdodGVkIHJ1bGVzJyk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICB9XHJcbiAgICBlbHNlIGlmKHRoaXMucnVsZURhdGEuY3VycmVudFRhc2subGVuZ3RoIDwgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrLmxlbmd0aCl7XHJcbiAgICAgIC8vQWRkaW5nIGEgcnVsZSB0byB0aGUgc291cmNlIHRhc2tzIGxpc3RcclxuICAgICAgY29uc3QgbmV3VGFza3MgPSBbXTtcclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPXRoaXMucnVsZURhdGEuY3VycmVudFRhc2suaW5kZXhPZih0YXNrKTtcclxuICAgICAgICBpZihpbmRleCA8IDApe1xyXG4gICAgICAgICAgbmV3VGFza3MucHVzaCh0YXNrKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheSA9IFtdO1xyXG4gICAgICBpZigodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJUeXBlICE9PSB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJUeXBlKVxyXG4gICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyRGF0YSAhPT0gdGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSlcclxuICAgICAgIHx8ICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudGFyZ2V0VHlwZSAhPT0gdGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlKVxyXG4gICAgICAgfHwgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXREYXRhICE9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpXHJcbiAgICAgICB8fCAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmlzSW5pdGlhbFJ1bGUgIT09IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSkpXHJcbiAgICAgICB7XHJcbiAgICAgICAgICBmb3IobGV0IGk9MDsgaTx0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICBjb25zdCBydWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgICAgICBydWxlLmN1cnJlbnRUYXNrID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrW2ldO1xyXG4gICAgICAgICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZShydWxlLHRoaXMucHJvamVjdElkLHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWRbaV0pKTtcclxuICAgICAgICAgIH1cclxuICAgICAgIH1lbHNle1xyXG4gICAgICAgIG5ld1Rhc2tzLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgICBjb25zdCBydWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgICAgcnVsZS5jdXJyZW50VGFzayA9IHRhc2s7XHJcbiAgICAgICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZShydWxlLCB0aGlzLnByb2plY3RJZCwgJycpKTtcclxuICAgICAgICB9KVxyXG4gICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBcclxuICB1cGRhdGVWYWxpZGF0aW9uKCl7XHJcbiAgICBsZXQgYWxsU291cmNlVGFza3MgPSBbXTtcclxuICAgIGlmKCF0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgIHRoaXMucnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgaWYoIXJ1bGUuaXNJbml0aWFsUnVsZSl7XHJcbiAgICAgICAgYWxsU291cmNlVGFza3MucHVzaCguLi5ydWxlLmN1cnJlbnRUYXNrKVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxuICBlbHNle1xyXG4gICAgdGhpcy50YXNrcnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICBpZighcnVsZS5pc0luaXRpYWxSdWxlKXtcclxuICAgICAgICBhbGxTb3VyY2VUYXNrcy5wdXNoKC4uLnJ1bGUuY3VycmVudFRhc2spXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfVxyXG4gICBjb25zdCBpbmRleCA9IGFsbFNvdXJjZVRhc2tzLmluZGV4T2YodGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKTtcclxuICAgaWYoaW5kZXggPj0gMCl7XHJcbiAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2suc2V0VmFsdWUodGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzayk7XHJcbiAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50cmlnZ2VyRGF0YSk7XHJcbiAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgICB0aGlzLmlzUnVsZVNhdmVkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKFwiQ2Fubm90IGVkaXQgdGFyZ2V0VGFzayBhcyBpdCBoYXMgZGVwZW5kZW5jaWVzXCIpO1xyXG4gICAgfWVsc2UgaWYoSlNPTi5zdHJpbmdpZnkodGhpcy5ydWxlRGF0YSkgIT09IEpTT04uc3RyaW5naWZ5KHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKSAmJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCkgXHJcbiAgICAgICYmIHRoaXMucnVsZURhdGEuaXNFeGlzdFxyXG4gICAgICAmJiB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmxlbmd0aCAhPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoKXtcclxuICAgICAgICB0aGlzLnVwZGF0ZVdvcmtmbG93UnVsZSgpO1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBlbHNlIGlmICghdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgIHRoaXMuaHR0cFJlcXVlc3RBcnJheSA9IFtdO1xyXG4gICAgICBmb3IobGV0IGk9MDsgaSA8IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFzay5sZW5ndGg7IGkrKyApe1xyXG4gICAgICAgIGNvbnN0IHdvcmtmbG93UnVsZSA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKTtcclxuICAgICAgICB3b3JrZmxvd1J1bGUuY3VycmVudFRhc2sgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2tbaV07XHJcbiAgICAgICAgdGhpcy5odHRwUmVxdWVzdEFycmF5LnB1c2godGhpcy50YXNrU2VydmljZS5jcmVhdGVXb3JrZmxvd1J1bGUod29ya2Zsb3dSdWxlLCB0aGlzLnByb2plY3RJZCwgdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZFtpXSkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjaGVja1Rhc2tEZXBlbmRlbmNpZXMoKTphbnl7XHJcbiAgICBcclxuICAgIGxldCBhbGxTb3VyY2VUYXNrcyA9IFtdO1xyXG4gICAgICB0aGlzLnRhc2tydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgaWYoIXJ1bGUuaXNJbml0aWFsUnVsZSl7XHJcbiAgICAgICAgICBhbGxTb3VyY2VUYXNrcy5wdXNoKC4uLnJ1bGUuY3VycmVudFRhc2spXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgIGNvbnN0IGluZGV4ID0gYWxsU291cmNlVGFza3MuaW5kZXhPZih0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgIGlmKGluZGV4ID49IDApe1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2suc2V0VmFsdWUodGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzayk7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhKTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKTtcclxuICAgICAgICB0aGlzLmlzUnVsZVNhdmVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoXCJDYW5ub3QgZWRpdCB0YXJnZXRUYXNrIGFzIGl0IGhhcyBkZXBlbmRlbmNpZXNcIik7XHJcbiAgICAgICAgdGhpcy5pc0NoaWxkRGVwZW5kZWNpZXM9dHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcy5pc0NoaWxkRGVwZW5kZWNpZXNcclxuICB9XHJcblxyXG4gIGFkZFdvcmtmbG93UnVsZSgpIHtcclxuICAgIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQgJiYgIXRoaXMuY2hlY2tUYXNrRGVwZW5kZW5jaWVzKCkpe1xyXG4gICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXk9IFtdXHJcbiAgICAgZm9yKGxldCBpPTA7IGkgPCB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoOyBpKysgKXtcclxuICAgICAgdmFyIHdvcmtmbG93Rmxvd09iamVjdD0gW107XHJcbiAgICAgIHdvcmtmbG93Rmxvd09iamVjdC5wdXNoKHtcclxuICAgICAgICBjdXJyZW50VGFzazogdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZVtpXSxcclxuICAgICAgICB0cmlnZ2VyVHlwZTogdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSxcclxuICAgICAgICB0YXJnZXRUeXBlOiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldFR5cGUudmFsdWUsXHJcbiAgICAgICAgdHJpZ2dlckRhdGE6IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUsXHJcbiAgICAgICAgdGFyZ2V0RGF0YTogdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnZhbHVlLFxyXG4gICAgICAgIGlzSW5pdGlhbFJ1bGU6IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuaXNJbml0aWFsUnVsZS52YWx1ZSxcclxuICAgICB9KSBcclxuICAgICBcclxuICAgICAgdGhpcy5odHRwUmVxdWVzdEFycmF5LnB1c2godGhpcy50YXNrU2VydmljZS5jcmVhdGVXb3JrZmxvd1J1bGUoSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh3b3JrZmxvd0Zsb3dPYmplY3RbMF0pKSwgdGhpcy5wcm9qZWN0SWQsIHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWRbaV0pKTtcclxuICAgICAgLy8gdGhpcy5pc1J1bGVTYXZlZD0gdHJ1ZVxyXG4gICAgICAvLyB0aGlzLmlzdGFza1J1bGVFZGl0PXRydWVcclxuICAgICAgLy8gdGhpcy5lbmFibGVJbml0aWF0ZVdvcmtmbG93SGFuZGxlci5uZXh0KHRoaXMuaXN0YXNrUnVsZUVkaXQpO1xyXG4gICAgfVxyXG4gICAgfVxyXG4gICBlbHNlIGlmKHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSAhPT0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldERhdGEgJiYgdGhpcy5ydWxlRGF0YS5pc0V4aXN0ICYmIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICB0aGlzLnVwZGF0ZVZhbGlkYXRpb24oKTtcclxuICAgIH1lbHNlIGlmKEpTT04uc3RyaW5naWZ5KHRoaXMucnVsZURhdGEpICE9PSBKU09OLnN0cmluZ2lmeSh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkgJiYgIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpIFxyXG4gICAgJiYgdGhpcy5ydWxlRGF0YS5pc0V4aXN0XHJcbiAgICAmJiB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmxlbmd0aCAhPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2subGVuZ3RoKXtcclxuICAgICAgdGhpcy51cGRhdGVXb3JrZmxvd1J1bGUoKTtcclxuICAgIH1lbHNlIGlmKCF0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgICAgdGhpcy5odHRwUmVxdWVzdEFycmF5ID0gW107XHJcbiAgICAgIGZvcihsZXQgaT0wOyBpIDwgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrLmxlbmd0aDsgaSsrICl7XHJcbiAgICAgICAgY29uc3Qgd29ya2Zsb3dSdWxlID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpO1xyXG4gICAgICAgIHdvcmtmbG93UnVsZS5jdXJyZW50VGFzayA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFza1tpXTtcclxuICAgICAgICB0aGlzLmh0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLmNyZWF0ZVdvcmtmbG93UnVsZSh3b3JrZmxvd1J1bGUsIHRoaXMucHJvamVjdElkLCB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkW2ldKSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JrSm9pbih0aGlzLmh0dHBSZXF1ZXN0QXJyYXkpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIGlmKHJlc3BvbnNlKXtcclxuICAgICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrID0gW3RoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5jdXJyZW50VGFza107XHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZSA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyVHlwZTtcclxuICAgICAgICB0aGlzLnJ1bGVEYXRhLnRyaWdnZXJEYXRhID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJEYXRhO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXRUeXBlO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50YXJnZXREYXRhO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSA9IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS5pc0luaXRpYWxSdWxlO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEuaXNSdWxlQWRkZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWQgPSBbXTtcclxuICAgICAgICBmb3IobGV0IGk9MDsgaSA8IHJlc3BvbnNlLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgIHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWQucHVzaChyZXNwb25zZVtpXVsnV29ya2Zsb3dfUnVsZXMtaWQnXSAmJiByZXNwb25zZVtpXVsnV29ya2Zsb3dfUnVsZXMtaWQnXS5JZCA/IHJlc3BvbnNlW2ldWydXb3JrZmxvd19SdWxlcy1pZCddLklkIDogbnVsbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucnVsZUdyb3VwSGFuZGxlci5uZXh0KCk7XHJcbiAgICAgICAgdGhpcy5pc1J1bGVTYXZlZCA9IHRydWU7XHJcbiAgICAgICAgaWYoIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICAgIHRoaXMuZW5hYmxlSW5pdGlhdGVXb3JrZmxvd0hhbmRsZXIubmV4dCh0aGlzLmlzdGFza1J1bGVFZGl0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgIHRoaXMuaXN0YXNrUnVsZUVkaXQ9dHJ1ZTtcclxuICAgICAgICAgIHRoaXMuZW5hYmxlSW5pdGlhdGVXb3JrZmxvd0hhbmRsZXIubmV4dCh0aGlzLmlzdGFza1J1bGVFZGl0KTtcclxuICAgICAgICB9XHJcbiAgICAgIC8vICAgZWxzZSBpZih0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgICAgLy8gICAgIHRoaXMuaXN0YXNrUnVsZUVkaXQ9IHRydWVcclxuICAgICAgLy8gICAgIHRoaXMuaXNUYXNrTGV2ZWxSdWxlRWRpdC5lbWl0KHRoaXMuaXN0YXNrUnVsZUVkaXQpXHJcbiAgICAgIC8vICAgfVxyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdXb3JrZmxvdyBSdWxlIGhhcyBiZWVuIHNhdmVkJyk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgY3JlYXRpbmcgd29ya2Zsb3cgcnVsZScpO1xyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgIGNoZWNrUnVsZURlcGVuZGVuY2llczEoKXtcclxuICAgIGxldCByZW1vdmVkUnVsZVRhcmdldFRhc2sgPSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGE7XHJcbiAgICAgaWYodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlID09PSAnVEFTSycgJiYgIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICBsZXQgc291cmNlVGFza3MgPSBuZXcgU2V0KCk7XHJcbiAgICAgIGxldCBzb3VyY2VUYXNrc0FycmF5ID0gW11cclxuICAgICAgLy90aGlzLmFsbFJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBpZighcnVsZS5pc0luaXRpYWxSdWxlKXtcclxuICAgICAgICAgIHJ1bGUuY3VycmVudFRhc2suZm9yRWFjaCh0YXNrID0+IHNvdXJjZVRhc2tzLmFkZCh0YXNrKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgc291cmNlVGFza3NBcnJheSA9IEFycmF5LmZyb20oc291cmNlVGFza3MpO1xyXG4gICAgbGV0IG5vUHJlZGVjZXNzb3JSdWxlTGlzdEFycmF5ID0gW107XHJcbiAgICBsZXQgY291bnQgPSAwO1xyXG4gICAvLyB0aGlzLmFsbFJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSB0cnVlKTtcclxuICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiBydWxlLmhhc1ByZWRlY2Vzc29yID0gdHJ1ZSk7XHJcblxyXG4gICAgY29uc3QgdGFyZ2V0SW5kZXggPSBzb3VyY2VUYXNrc0FycmF5LmluZGV4T2YocmVtb3ZlZFJ1bGVUYXJnZXRUYXNrKTtcclxuICAgIGlmKHRhcmdldEluZGV4ID49IDApe1xyXG4gICAgIC8vIHRoaXMuYWxsUnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICB0aGlzLnJ1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHJ1bGUuY3VycmVudFRhc2suaW5kZXhPZihyZW1vdmVkUnVsZVRhcmdldFRhc2spO1xyXG4gICAgICAgIGlmKGluZGV4ID49IDApe1xyXG4gICAgICAgICAgcnVsZS5oYXNQcmVkZWNlc3NvciA9IGZhbHNlO1xyXG4gICAgICAgICAgbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXkucHVzaChydWxlKTtcclxuICAgICAgICAgIGNvdW50ICs9IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gICAgdGhpcy5ub1ByZWRlY2Vzc29yUnVsZUxpc3QgPSBjb3VudCA8PTAgPyBbXSA6IG5vUHJlZGVjZXNzb3JSdWxlTGlzdEFycmF5O1xyXG4gICAgaWYodGhpcy5ydWxlRGF0YS50YXJnZXRUYXNrICE9PSBcIlwiKXtcclxuICAgICAvLyB0aGlzLnJ1bGVEZXBlbmRlbmN5RXJyb3JIYW5kbGVyLm5leHQodGhpcy5hbGxSdWxlTGlzdEdyb3VwKTtcclxuICAgICB0aGlzLnJ1bGVEZXBlbmRlbmN5RXJyb3JIYW5kbGVyLm5leHQodGhpcy5ydWxlTGlzdEdyb3VwKTtcclxuXHJcbiAgICAgfVxyXG4gICAgfVxyXG4gICBlbHNlIGlmKHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSA9PT0gJ1RBU0snICYmIHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICBsZXQgc291cmNlVGFza3MgPSBuZXcgU2V0KCk7XHJcbiAgICAgIGxldCBzb3VyY2VUYXNrc0FycmF5ID0gW11cclxuICAgICAgdGhpcy50YXNrcnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgIGlmKCFydWxlLmlzSW5pdGlhbFJ1bGUpe1xyXG4gICAgICAgICAgcnVsZS5jdXJyZW50VGFzay5mb3JFYWNoKHRhc2sgPT4gc291cmNlVGFza3MuYWRkKHRhc2spKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICBzb3VyY2VUYXNrc0FycmF5ID0gQXJyYXkuZnJvbShzb3VyY2VUYXNrcyk7XHJcbiAgICBsZXQgbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXkgPSBbXTtcclxuICAgIGxldCBjb3VudCA9IDA7XHJcbiAgICB0aGlzLnRhc2tydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiBydWxlLmhhc1ByZWRlY2Vzc29yID0gdHJ1ZSk7XHJcbiAgICBjb25zdCB0YXJnZXRJbmRleCA9IHNvdXJjZVRhc2tzQXJyYXkuaW5kZXhPZihyZW1vdmVkUnVsZVRhcmdldFRhc2spO1xyXG4gICAgaWYodGFyZ2V0SW5kZXggPj0gMCl7XHJcbiAgICAgIHRoaXMudGFza3J1bGVMaXN0R3JvdXAuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHJ1bGUuY3VycmVudFRhc2suaW5kZXhPZihyZW1vdmVkUnVsZVRhcmdldFRhc2spO1xyXG4gICAgICAgIGlmKGluZGV4ID49IDApe1xyXG4gICAgICAgICAgcnVsZS5oYXNQcmVkZWNlc3NvciA9IGZhbHNlO1xyXG4gICAgICAgICAgbm9QcmVkZWNlc3NvclJ1bGVMaXN0QXJyYXkucHVzaChydWxlKTtcclxuICAgICAgICAgIGNvdW50ICs9IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gICAgdGhpcy5ub1ByZWRlY2Vzc29yUnVsZUxpc3QgPSBjb3VudCA8PTAgPyBbXSA6IG5vUHJlZGVjZXNzb3JSdWxlTGlzdEFycmF5O1xyXG4gICAgaWYodGhpcy5ydWxlRGF0YS50YXJnZXRUYXNrICE9PSBcIlwiKXtcclxuICAgIC8vICB0aGlzLnJ1bGVEZXBlbmRlbmN5RXJyb3JIYW5kbGVyLm5leHQodGhpcy5hbGxSdWxlTGlzdEdyb3VwKTtcclxuICAgIHRoaXMucnVsZURlcGVuZGVuY3lFcnJvckhhbmRsZXIubmV4dCh0aGlzLnJ1bGVMaXN0R3JvdXApO1xyXG5cclxuICAgICB9XHJcbiAgICB9XHJcbiAgIH1cclxuXHJcbiAgdmFsaWRhdGVSZW1vdmVXb3JrZmxvd1J1bGUoKSB7XHJcbiAgICAvLyBsZXQgcnVsZUNvdW50V2l0aFNhbWVUYXJnZXRUYXNrID0gMDtcclxuICAgIC8vIHRoaXMucnVsZUxpc3RHcm91cC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgLy8gICBpZihydWxlLnRhcmdldERhdGEgPT0gdGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKXtcclxuICAgIC8vICAgICBydWxlQ291bnRXaXRoU2FtZVRhcmdldFRhc2sgKys7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH0pXHJcbiAgICB0aGlzLmNoZWNrUnVsZURlcGVuZGVuY2llczEoKTtcclxuICAgIGlmKCh0aGlzLm5vUHJlZGVjZXNzb3JSdWxlTGlzdC5sZW5ndGggPD0gMCkpe1xyXG4gICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybSAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLnByaXN0aW5lKSB7XHJcbiAgICAgICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiBydWxlLmhhc1ByZWRlY2Vzc29yID0gdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5yZW1vdmVXb3JrZmxvd1J1bGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gcmVtb3ZlIHRoZSBSdWxlPycsXHJcbiAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvbjogJ05vJ1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlV29ya2Zsb3dSdWxlKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1lbHNle1xyXG4gICAgICBpZighdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCl7XHJcbiAgICAgICAvLyB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoXCJQbGVhc2UgRGVsZXRlIEhpZ2hsaWdodGVkIFJ1bGUgRmlyc3QhISBJZiBSdWxlIG5vdCB2aXNpYmxlIGNoYW5nZSB0aGUgcGFnZSBzaXplIGFuZCB0cnkgYWdhaW4hIVwiKTtcclxuICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihcIlBsZWFzZSBEZWxldGUgSGlnaGxpZ2h0ZWQgQ2hpbGQgUnVsZXMgRmlyc3RcIik7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZXtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoXCJQbGVhc2Ugb3BlbiBnbG9iYWwgY29uZmlndXJlIHJ1bGUgYW5kIHRoZW4gZWRpdCBhbmQgcmVtb3ZlIHRoaXMgdGFza1wiKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlV29ya2Zsb3dSdWxlKCkge1xyXG4gICAgY29uc3QgaXNJbml0aWFsUnVsZTEgPSB0aGlzLnJ1bGVEYXRhLmlzSW5pdGlhbFJ1bGUgPyAndHJ1ZScgOiAnZmFsc2UnO1xyXG4gICAgaWYgKHRoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWQpIHtcclxuICAgICAgdGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZCA9IEFycmF5LmlzQXJyYXkodGhpcy5ydWxlRGF0YS53b3JrZmxvd1J1bGVJZCkgPyB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkIDogW3RoaXMucnVsZURhdGEud29ya2Zsb3dSdWxlSWRdO1xyXG4gICAgICAvL2xldCBodHRwUmVxdWVzdEFycmF5ID0gW11cclxuICAgICAgbGV0IHJ1bGVJZHMgPSBbXVxyXG4gICAgICB0aGlzLnJ1bGVEYXRhLndvcmtmbG93UnVsZUlkLmZvckVhY2god29ya2Zsb3dJZCA9PiB7XHJcbiAgICAgICAgY29uc3QgUnVsZSA9IHtcclxuICAgICAgICAgIElkOiB3b3JrZmxvd0lkXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJ1bGVJZHMucHVzaChSdWxlKTtcclxuICAgICAgICAvL2h0dHBSZXF1ZXN0QXJyYXkucHVzaCh0aGlzLnRhc2tTZXJ2aWNlLnJlbW92ZVdvcmtmbG93UnVsZSh3b3JrZmxvd0lkKSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICBsZXQgY3VycmVudFRhc2sgPSBbXTtcclxuICAgICAgdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICBjb25zdCBzb3VyY2VUYXNrID0ge1xyXG4gICAgICAgICAgSWQ6IGVhY2hUYXNrXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGN1cnJlbnRUYXNrLnB1c2goc291cmNlVGFzayk7XHJcbiAgICAgIH0pXHJcbiAgICAgIGxldCByRGF0YSA9IHtcclxuICAgICAgICBjdXJyZW50VGFzayxcclxuICAgICAgICBpc0luaXRpYWxSdWxlOiBpc0luaXRpYWxSdWxlMVxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMudGFza1NlcnZpY2UucmVtb3ZlV29ya2Zsb3dSdWxlKHJ1bGVJZHMsckRhdGEpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7IFxyXG4gICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgdGhpcy5yZW1vdmVXb3JrZmxvd1J1bGVIYW5kbGVyLm5leHQodGhpcy5ydWxlRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmVuYWJsZUluaXRpYXRlV29ya2Zsb3dIYW5kbGVyLm5leHQoKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdXb3JrZmxvdyBSdWxlIGhhcyBiZWVuIHJlbW92ZWQnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSByZW1vdmluZyB3b3JrZmxvdyBydWxlJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yZW1vdmVXb3JrZmxvd1J1bGVIYW5kbGVyLm5leHQobnVsbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjbG9zZURpYWxvZygpIHtcclxuICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgdGhpcy5jbG9zZUNhbGxiYWNrSGFuZGxlci5uZXh0KCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNsb3NlPycsXHJcbiAgICAgICAgICBzdWJtaXRCdXR0b246ICdZZXMnLFxyXG4gICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuaXNUcnVlKSB7XHJcbiAgICAgICAgICB0aGlzLmNsb3NlQ2FsbGJhY2tIYW5kbGVyLm5leHQoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaW5pdGlhbGl6ZUZvcm0oKSB7XHJcblxyXG4gICAgLy8gdGhpcy5hbGxSdWxlTGlzdCA9IFsuLi5uZXcgU2V0KFsuLi50aGlzLnJ1bGVMaXN0LCAuLi50aGlzLnByb2plY3RSdWxlTGlzdF0pXTsgLS0tIFVwZGF0ZSBTdGFuZGFyZCBjb2RlIGZvciBhbGwgcnVsZSBsaXN0IC0gTWVuYWthXHJcbiAgICB0aGlzLnJ1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgIGNvbnN0IGV4aXN0aW5nUnVsZSA9IHRoaXMuYWxsUnVsZUxpc3QuZmluZChzZWxlY3RlZFJ1bGUgPT4gSlNPTi5zdHJpbmdpZnkoc2VsZWN0ZWRSdWxlKSA9PT0gSlNPTi5zdHJpbmdpZnkocnVsZSkpO1xyXG4gICAgICBpZiAoIWV4aXN0aW5nUnVsZSkge1xyXG4gICAgICAgIHRoaXMuYWxsUnVsZUxpc3QucHVzaChydWxlKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5wcm9qZWN0UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgY29uc3QgZXhpc3RpbmdSdWxlID0gdGhpcy5hbGxSdWxlTGlzdC5maW5kKHNlbGVjdGVkUnVsZSA9PiBKU09OLnN0cmluZ2lmeShzZWxlY3RlZFJ1bGUpID09PSBKU09OLnN0cmluZ2lmeShydWxlKSk7XHJcbiAgICAgIGlmICghZXhpc3RpbmdSdWxlKSB7XHJcbiAgICAgICAgdGhpcy5hbGxSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgdGhpcy5jdXJyZW50UnVsZUxpc3QgPSBbXTtcclxuICAgIHRoaXMuYWxsUnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgIGlmIChKU09OLnN0cmluZ2lmeSh0aGlzLnJ1bGVEYXRhKSAhPT0gSlNPTi5zdHJpbmdpZnkocnVsZSkpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICAgIHRoaXMuY3VycmVudFJ1bGVMaXN0LnB1c2gocnVsZSlcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIGlmICh0aGlzLnRhc2tEYXRhWzBdLlBST0pFQ1RfSVRFTV9JRCkge1xyXG4gICAgICBjb25zdCBwcm9qZWN0SXRlbUlkID0gdGhpcy50YXNrRGF0YVswXS5QUk9KRUNUX0lURU1fSUQ7XHJcbiAgICAgIHRoaXMucHJvamVjdElkID0gcHJvamVjdEl0ZW1JZC5zcGxpdCgnLicpWzFdO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICB0aGlzLnRhc2tEYXRhLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgIGlmICgodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMIHx8IHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5URVJNRURJQVRFKSB8fCAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ICYmIHRoaXMuaXNSdWxlc0luaXRpYXRlZCkpIHtcclxuICAgICAgICBpZiAodGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpIHtcclxuICAgICAgICAgIC8qaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGhhc1BhcmVudFRhc2tSdWxlID0gdGhpcy5jdXJyZW50UnVsZUxpc3QuZmluZChydWxlID0+IHJ1bGUuY3VycmVudFRhc2sgPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSB8fCB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maW5kKHJ1bGUgPT4gcnVsZS50YXJnZXREYXRhID09PSB0YXNrLklEKSk7XHJcbiAgICAgICAgICAgIGlmIChoYXNQYXJlbnRUYXNrUnVsZSkge1xyXG4gICAgICAgICAgICAgIHRoaXMudGFza0xpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklEXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0gXHJcbiAgICAgICAgICAqL1xyXG4gICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDAgJiYgIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpIHtcclxuICAgICAgICAgICAgY29uc3QgaGFzUnVsZSA9IHRoaXMuY3VycmVudFJ1bGVMaXN0LmZpbmQocnVsZSA9PiBydWxlLnRhcmdldERhdGEgPT09IHRhc2suSUQpO1xyXG4gICAgICAgICAgICAgaWYgKGhhc1J1bGUpIHtcclxuICAgICAgICAgICAgICB0aGlzLnRhc2tMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5JRCxcclxuICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgfVxyXG4gICAgICAgICBlbHNlIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICAgICAgdGhpcy50YXNrTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRClcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy50YXNrTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRClcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgY29uc3QgdGFza1N0YXR1cyA9IHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxTdGF0dXNCeWNhdGVnb3J5TmFtZShNUE1fTEVWRUxTLlRBU0spO1xyXG4gICAgLy8gdGhpcy50YXNrU3RhdHVzZXMgPSB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0QWxsU3RhdHVzQnljYXRlZ29yeU5hbWUoTVBNX0xFVkVMUy5UQVNLKTtcclxuICAgIHRoaXMudGFza1N0YXR1c2VzID0gdGFza1N0YXR1cyAmJiB0YXNrU3RhdHVzLmxlbmd0aCA+IDAgJiYgQXJyYXkuaXNBcnJheSh0YXNrU3RhdHVzKSA/IHRhc2tTdGF0dXNbMF0gOiB0YXNrU3RhdHVzO1xyXG4gICAgbGV0IGN1cnJlbnRSdWxlVHJpZ2dlckRhdGEgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHN0YXR1cyA9PiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCA9PSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG5cclxuXHJcbiAgICB0aGlzLnRhcmdldFRhc2sgPSB0aGlzLnRhc2tEYXRhLmZpbmQodGFzayA9PiB0YXNrLklEID09PSB0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgICBjdXJyZW50VGFzazogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICB2YWx1ZTogdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzaywgXHJcbiAgICAgICAgZGlzYWJsZWQ6ICh0aGlzLmlzQ3VycmVudFRhc2tSdWxlID8gdHJ1ZSA6ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyAodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlPT09J1RBU0snID8gKHRoaXMudGFyZ2V0VGFzaz8uSVNfQUNUSVZFX1RBU0sgPT09J3RydWUnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUFDQ0VQVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1DQU5DRUxMRUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFSkVDVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRVZJRVctUkVKRUNURUQnID8gdHJ1ZSA6IGZhbHNlKTogZmFsc2UpIDogZmFsc2UpKVxyXG4gICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICB0cmlnZ2VyVHlwZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMucnVsZURhdGEudHJpZ2dlclR5cGUsIGRpc2FibGVkOiAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ID8odGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlPT09J1RBU0snID8gKHRoaXMudGFyZ2V0VGFzaz8uSVNfQUNUSVZFX1RBU0sgPT09J3RydWUnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUFDQ0VQVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1DQU5DRUxMRUQnfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVKRUNURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFVklFVy1SRUpFQ1RFRCcgPyB0cnVlIDogZmFsc2UpOmZhbHNlKSA6IGZhbHNlKSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pLFxyXG4gICAgICB0cmlnZ2VyRGF0YTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogdGhpcy5ydWxlRGF0YS50cmlnZ2VyVHlwZSA/ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyh0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGU9PT0nVEFTSycgPyAodGhpcy50YXJnZXRUYXNrPy5JU19BQ1RJVkVfVEFTSyA9PT0ndHJ1ZScgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQUNDRVBURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUNBTkNFTExFRCd8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRUpFQ1RFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVWSUVXLVJFSkVDVEVEJyA/IHRydWUgOiBmYWxzZSkgOiBmYWxzZSkgOiBmYWxzZSkgOiB0cnVlIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgIHRhcmdldFR5cGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnJ1bGVEYXRhLnRhcmdldFR5cGUsIGRpc2FibGVkOiAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ID8gKHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZT09PSdUQVNLJyA/ICh0aGlzLnRhcmdldFRhc2s/LklTX0FDVElWRV9UQVNLID09PSd0cnVlJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1BQ0NFUFRFRCcgfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtQ0FOQ0VMTEVEJ3x8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFSkVDVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1SRVZJRVctUkVKRUNURUQnID8gdHJ1ZSA6IGZhbHNlKTogZmFsc2UpIDogZmFsc2UpIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgIHRhcmdldERhdGE6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IHRoaXMucnVsZURhdGEudGFyZ2V0VHlwZSA/ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyAodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlPT09J1RBU0snID8gKHRoaXMudGFyZ2V0VGFzaz8uSVNfQUNUSVZFX1RBU0sgPT09J3RydWUnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUFDQ0VQVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1DQU5DRUxMRUQnfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVKRUNURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFVklFVy1SRUpFQ1RFRCcgPyB0cnVlIDogZmFsc2UpOmZhbHNlKSA6IGZhbHNlKSA6IHRydWUgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgaXNJbml0aWFsUnVsZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMucnVsZURhdGEuaXNJbml0aWFsUnVsZSwgZGlzYWJsZWQ6ICh0aGlzLnJ1bGVEYXRhLmlzRXhpc3QgPyAodGhpcy5ydWxlRGF0YS50YXJnZXRUeXBlPT09J1RBU0snID8gKHRoaXMudGFyZ2V0VGFzaz8uSVNfQUNUSVZFX1RBU0sgPT09J3RydWUnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLUFDQ0VQVEVEJyB8fCB0aGlzLnRhcmdldFRhc2suVEFTS19TVEFUVVNfVFlQRT09PSdGSU5BTC1DQU5DRUxMRUQnfHwgdGhpcy50YXJnZXRUYXNrLlRBU0tfU1RBVFVTX1RZUEU9PT0nRklOQUwtUkVKRUNURUQnIHx8IHRoaXMudGFyZ2V0VGFzay5UQVNLX1NUQVRVU19UWVBFPT09J0ZJTkFMLVJFVklFVy1SRUpFQ1RFRCcgPyB0cnVlIDogZmFsc2UpOmZhbHNlKSA6IGZhbHNlKSB9KSxcclxuICAgICAgfSk7XHJcblxyXG5cclxuICAgXHJcbiAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZSkge1xyXG4gICAgICBjb25zdCBjdXJyZW50VGFza0xpc3QgPSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlO1xyXG4gICAgICBmb3IobGV0IGk9MDsgaSA8IGN1cnJlbnRUYXNrTGlzdC5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRUYXNrID0gdGhpcy50YXNrRGF0YS5maW5kKHRhc2sgPT4gdGFzay5JRCA9PT0gY3VycmVudFRhc2tMaXN0W2ldKTtcclxuICAgICAgICBpZihjdXJyZW50VGFzayAmJiBjdXJyZW50VGFzay5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScpe1xyXG4gICAgICAgICAgdGhpcy5hcHByb3ZhbFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICB0aGlzLmFwcHJvdmFsVGFzayA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBsZXQgaXNUYXJnZXRUYXNrID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuYWxsUnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAvLyBpZiAocnVsZS50YXJnZXREYXRhID09PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkuY3VycmVudFRhc2spIHtcclxuICAgICAgICAvLyAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCk7XHJcbiAgICAgICAgLy8gICBpZiAoIShydWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCA9PT0gcnVsZS50cmlnZ2VyRGF0YSkpIHtcclxuICAgICAgICAvLyAgICAgaXNUYXJnZXRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLmN1cnJlbnRUYXNrLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgaWYocnVsZS50YXJnZXREYXRhID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCk7XHJcbiAgICAgICAgICAgIGlmICghKHJ1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkID09PSBydWxlLnRyaWdnZXJEYXRhKSkge1xyXG4gICAgICAgICAgICAgICAgICBpc1RhcmdldFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pO1xyXG4gICAgICBpZiAoaXNUYXJnZXRUYXNrIHx8IHRoaXMuYXBwcm92YWxUYXNrKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5pc0luaXRpYWxSdWxlLmRpc2FibGUoKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRyaWdnZXJUeXBlKSB7XHJcbiAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnKSB7XHJcbiAgICAgICAgdGhpcy50YXNrU3RhdHVzZXMuZm9yRWFjaChzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgaWYgKHN0YXR1cy5TVEFUVVNfVFlQRSAhPT0gU3RhdHVzVHlwZXMuSU5JVElBTCAmJiBzdGF0dXMuU1RBVFVTX1RZUEUgIT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCAmJlxyXG4gICAgICAgICAgICAhKHN0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQgJiYgIXRoaXMuYXBwcm92YWxUYXNrICkpIHtcclxuICAgICAgICAgICAgdGhpcy50cmlnZ2VyRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuTkFNRSxcclxuICAgICAgICAgICAgICB2YWx1ZTogc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWRcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudHJpZ2dlckRhdGEpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IHRhc2tXb3JrZmxvd0FjdGlvbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFRhc2tXb3JrZmxvd0FjdGlvbnMoKTtcclxuICAgICAgICB0YXNrV29ya2Zsb3dBY3Rpb25zLmZvckVhY2goYWN0aW9uID0+IHtcclxuICAgICAgICAgIHRoaXMudHJpZ2dlckRhdGEucHVzaCh7XHJcbiAgICAgICAgICAgIG5hbWU6IGFjdGlvbi5OQU1FLFxyXG4gICAgICAgICAgICB2YWx1ZTogYWN0aW9uWydNUE1fV29ya2Zsb3dfQWN0aW9ucy1pZCddLklkXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudHJpZ2dlckRhdGEpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGxldCBpc1NvdXJjZVRhc2sgPSBmYWxzZTtcclxuICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmdldFJhd1ZhbHVlKCkudGFyZ2V0VHlwZSkge1xyXG4gICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5nZXRSYXdWYWx1ZSgpLnRhcmdldFR5cGUgPT09ICdUQVNLJykge1xyXG4gICAgICAgIHRoaXMudGFza0RhdGEuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgIGxldCBpc1NlbGVjdGVkVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgbGV0IGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IHRydWU7XHJcbiAgICAgICAgICBsZXQgaXNDeWNsZVRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgIGNvbnN0IGN1cnJlbnRUYXNrUnVsZUxpc3QgPSBbXTtcclxuICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFJ1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYoZWFjaFRhc2sgPT09IHJ1bGUuY3VycmVudFRhc2tbMF0pe1xyXG4gICAgICAgICAgICAgICAgICBjdXJyZW50VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLy8gY3VycmVudFRhc2tSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAvLyAgIGlmIChydWxlLnRhcmdldERhdGEgPT09IHRhc2suSUQgJiYgcnVsZS50cmlnZ2VyRGF0YSA9PT0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSkge1xyXG4gICAgICAgICAgICAvLyAgICAgaXNTZWxlY3RlZFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAvLyAgIH1cclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaCgoZWFjaFRhc2spID0+IHtcclxuICAgICAgICAgICAgICBpZih0YXNrLklEID09PSBlYWNoVGFzayl7XHJcbiAgICAgICAgICAgICAgICBpc1NlbGVjdGVkVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ1NUQVRVUycgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHRhcmdldFRhc2tSdWxlTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZihydWxlLnRhcmdldERhdGEgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFRhc2tSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIHRhcmdldFRhc2tSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJ1bGUuY3VycmVudFRhc2tbMF0gPT09IHRhc2suSUQgJiYgcnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgcnVsZS50cmlnZ2VyRGF0YSA9PT0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICBpc0N5Y2xlVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCk7XHJcbiAgICAgICAgICBjb25zdCBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXM9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCk7XHJcbiAgICAgICAgICBpZih0YXNrLklTX0FQUFJPVkFMX1RBU0sgPT09ICd0cnVlJyl7XHJcbiAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgaWYoZWFjaFRhc2sgPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSl7XHJcbiAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICBpZiAoIShpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrICYmICgodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ1NUQVRVUycgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpIHx8XHJcbiAgICAgICAgICAgICAgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdBQ1RJT04nKSkpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudFRhc2tSdWxlTGlzdCA9IHRoaXMuYWxsUnVsZUxpc3QuZmlsdGVyKHJ1bGUgPT4gcnVsZS5jdXJyZW50VGFzay5pbmNsdWRlcyhTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpIHx8IHJ1bGUudGFyZ2V0RGF0YSA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKTtcclxuICAgICAgICAgICAgaWYgKHBhcmVudFRhc2tSdWxlTGlzdCAmJiBwYXJlbnRUYXNrUnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHBhcmVudFRhc2tSdWxlTGlzdC5mb3JFYWNoKHBhcmVudFRhc2tSdWxlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghKHBhcmVudFRhc2tSdWxlICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCB8fCBwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YT09PSBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkpIHx8IChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ0FDVElPTicpKSkge1xyXG4gICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0gXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaCgoZWFjaFRhc2spID0+IHtcclxuICAgICAgICAgICAgaWYodGFzay5JRCA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgIGlzU291cmNlVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICBpZiAoKCFpc1NlbGVjdGVkVGFzayAmJiAhaXNDeWNsZVRhc2sgJiYgaXNQYXJlbnRUYXNrQ29tcGxldGVkIC8qJiYgIXRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQqLykgLyp8fCAodGhpcy5ydWxlRGF0YS5pc0V4aXN0ICYmIHRoaXMuaXNSdWxlc0luaXRpYXRlZCkqLykge1xyXG4gICAgICAgICAgICBpZiAoKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5JVElBTCkgfHwgKHRoaXMucnVsZURhdGEuaXNFeGlzdCAmJiB0aGlzLmlzUnVsZXNJbml0aWF0ZWQpKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGFzay5JRCxcclxuICAgICAgICAgICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRVZJRVdfUkVKRUNURUQpLFxyXG4gICAgICAgICAgICAgICAgaXNBcHByb3ZhbFRhc2s6IHRhc2suSVNfQVBQUk9WQUxfVEFTSz09PSd0cnVlJyA/dHJ1ZTpmYWxzZVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAvLyBlbHNlIGlmKHRoaXMuaXNUYXNrTGV2ZWxjYWxsZWQpe1xyXG4gICAgICAgICAgLy8gICBpZiAoKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5JVElBTCkgfHwgKHRoaXMucnVsZURhdGEuaXNFeGlzdCAmJiB0aGlzLmlzUnVsZXNJbml0aWF0ZWQpKSB7XHJcbiAgICAgICAgICAvLyAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgLy8gICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAvLyAgICAgICB2YWx1ZTogdGFzay5JRCxcclxuICAgICAgICAgIC8vICAgICAgIGlzQ29tcGxldGVkOiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRVZJRVdfUkVKRUNURUQpLFxyXG4gICAgICAgICAgLy8gICAgICAgaXNBcHByb3ZhbFRhc2s6IHRhc2suSVNfQVBQUk9WQUxfVEFTSz09PSd0cnVlJyA/dHJ1ZTpmYWxzZVxyXG4gICAgICAgICAgLy8gICAgIH0pO1xyXG4gICAgICAgICAgLy8gICB9XHJcbiAgICAgICAgICAvLyB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5ncm91cFRhcmdldFRhc2soKVxyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSh0aGlzLnJ1bGVEYXRhLnRhcmdldERhdGEpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IHRhc2tFdmVudHMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFRhc2tFdmVudHMoKTtcclxuICAgICAgICB0YXNrRXZlbnRzLmZvckVhY2goZXZlbnQgPT4ge1xyXG4gICAgICAgICAgdGhpcy50YXJnZXREYXRhLnB1c2goe1xyXG4gICAgICAgICAgICBuYW1lOiBldmVudC5ESVNQTEFZX05BTUUsXHJcbiAgICAgICAgICAgIHZhbHVlOiBldmVudFsnTVBNX0V2ZW50cy1pZCddLklkXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuc2V0VmFsdWUodGhpcy5ydWxlRGF0YS50YXJnZXREYXRhKTtcclxuICAgICAgfSBcclxuICAgIH1cclxuXHJcblxyXG4gICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGN1cnJlbnRUYXNrID0+IHtcclxuICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqQ2hlY2tpbmcgRm9yIEFwcHJvdmFsIFRhc2sgaW4gQ3VycmVudCBTb3VyY2UgdGFzayBMaXN0KioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKldlIHNob3VsZG4ndCBhbGxvdyBBcHByb3ZhbCB0YXNrIHRvIGJlIGdyb3VwZWQgd2l0aCBvdGhlciBydWxlcyoqKioqKioqKioqKiovXHJcbiAgICAgIGxldCBzb3VyY2VUYXNrcyA9IFtdO1xyXG4gICAgIC8vIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuaXNJbml0aWFsUnVsZS5zZXRWYWx1ZShmYWxzZSk7XHJcbiAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaCgoZWFjaFRhc2spID0+IHtcclxuICAgICAgICBsZXQgY3VycmVudFRhc2sgPSB0aGlzLnRhc2tEYXRhLmZpbmQodGFzayA9PiB0YXNrLklEID09PSBlYWNoVGFzayk7XHJcbiAgICAgICAgc291cmNlVGFza3MucHVzaChjdXJyZW50VGFzayk7XHJcbiAgICAgIH0pO1xyXG4gICAgc291cmNlVGFza3MuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgIGlmKGVhY2hUYXNrPy5JU19BUFBST1ZBTF9UQVNLID09PSAndHJ1ZScgJiYgc291cmNlVGFza3MubGVuZ3RoID4gMSl7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay5zZXRWYWx1ZShbXSk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdBcHByb3ZhbCBUYXNrIGNhbnQgYmUgR3JvdXBlZCB3aXRoIE90aGVyIHRhc2tzJyk7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgICAvL0NoZWNraW5nIGlmIHRoZXJlIG9ubHkgY29tcGxldGVkIHRhc2tzIGluIHNvdXJjZSB0YXNrcyBvZiBhIFJ1bGUuXHJcbiAgICAvL1RoaXMgY29tZXMgaGFuZHkgd2hpbGUgdXBkYXRpbmcgdGFza3NcclxuICAgIGxldCBvbmx5Q29tcGxldGVkVGFza3NJblNvdXJjZVRhc2tMaXN0ID0gdHJ1ZTtcclxuICAgIHNvdXJjZVRhc2tzLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICBpZighKGVhY2hUYXNrPy5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCB8fCBlYWNoVGFzaz8uVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEIHx8IGVhY2hUYXNrPy5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpKXtcclxuICAgICAgICBvbmx5Q29tcGxldGVkVGFza3NJblNvdXJjZVRhc2tMaXN0ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgICBpZihvbmx5Q29tcGxldGVkVGFza3NJblNvdXJjZVRhc2tMaXN0ICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUubGVuZ3RoID4gMCl7XHJcbiAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2suc2V0VmFsdWUodGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzayk7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVGhlcmUgc2hvdWxkIGJlIGF0bGVhc3Qgb25lIFRhc2sgdGhhdCBpcyBub3QgQ29tcGxldGVkJyk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgICAgLy9jaGVja2luZyBpZiB0YXJnZXQgdHlwZSBpcyBcIlRBU0svRVZFTlRcIlxyXG4gICAgICBpZiAoY3VycmVudFRhc2sgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXRUeXBlICYmIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0VHlwZS52YWx1ZSA9PT0gJ1RBU0snKSB7XHJcbiAgICAgICAgdGhpcy50YXJnZXREYXRhID0gW107XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKHRoaXMucnVsZURhdGEudGFyZ2V0RGF0YSk7XHJcbiAgICAgICAgdGhpcy50YXNrRGF0YS5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgbGV0IGlzU2VsZWN0ZWRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICBsZXQgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gdHJ1ZTtcclxuICAgICAgICAgIGxldCBpc0N5Y2xlVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICBjb25zdCBjdXJyZW50VGFza1J1bGVMaXN0ID0gW107IFxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgIGN1cnJlbnRUYXNrLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYocnVsZS5jdXJyZW50VGFza1swXSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICBjdXJyZW50VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmKHRhc2suSUQgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0VGFza1J1bGVMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKHJ1bGUudGFyZ2V0RGF0YSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocnVsZS5jdXJyZW50VGFza1swXSA9PT0gdGFzay5JRCAmJiBydWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiBydWxlLnRyaWdnZXJEYXRhID09PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlzQ3ljbGVUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cyA9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCk7XHJcbiAgICAgICAgICBjb25zdCBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXM9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cz0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEU9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpXHJcbiAgICAgICAgICBpZih0YXNrLklTX0FQUFJPVkFMX1RBU0sgPT09ICd0cnVlJyl7XHJcbiAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgaWYoZWFjaFRhc2sgPT09IFN0cmluZyh0YXNrLlRBU0tfUEFSRU5UX0lEKSl7XHJcbiAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICBpZiAoIShpc1BhcmVudFRhc2tJbkN1cnJlbnRUYXNrICYmICgodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ1NUQVRVUycgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpIHx8XHJcbiAgICAgICAgICAgICAgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdBQ1RJT04nKSkpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgcGFyZW50VGFza1J1bGVMaXN0ID0gdGhpcy5jdXJyZW50UnVsZUxpc3QuZmlsdGVyKHJ1bGUgPT4gcnVsZS5jdXJyZW50VGFza1swXSA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpIHx8IHJ1bGUudGFyZ2V0RGF0YSA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKTtcclxuICAgICAgICAgICAgICBpZiAocGFyZW50VGFza1J1bGVMaXN0ICYmIHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJlbnRUYXNrUnVsZUxpc3QuZm9yRWFjaChwYXJlbnRUYXNrUnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICghKHBhcmVudFRhc2tSdWxlICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCB8fCBwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YT09PSBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkpIHx8IChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ0FDVElPTicpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghaXNTZWxlY3RlZFRhc2sgJiYgIWlzQ3ljbGVUYXNrICYmIGlzUGFyZW50VGFza0NvbXBsZXRlZCAmJiAhdGhpcy5pc1Rhc2tMZXZlbGNhbGxlZCkge1xyXG4gICAgICAgICAgICBpZiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMKSB7IC8vICAmJiB0YXNrLlRBU0tfU1RBUlRfREFURSA+IGN1cnJlbnRUYXNrLlRBU0tfRFVFX0RBVEVcclxuICAgICAgICAgICAgICB0aGlzLnRhcmdldERhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiB0YXNrLlRBU0tfTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFVklFV19SRUpFQ1RFRCksXHJcbiAgICAgICAgICAgICAgICBpc0FwcHJvdmFsVGFzazogdGFzay5JU19BUFBST1ZBTF9UQVNLPT09J3RydWUnID90cnVlOmZhbHNlXHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgdGhpcy5ncm91cFRhcmdldFRhc2soKTtcclxuICAgICAgfVxyXG4gICAgICBcclxuXHJcbiAgICAgIC8vY2hlY2tpbmcgd2l0aCBUcmlnZ2VyIHR5cGUgXHJcbiAgICAgIGlmIChjdXJyZW50VGFzayAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlKSB7XHJcbiAgICAgICAgbGV0IGFwcHJvdmFsVGFza0luU291cmNlID0gZmFsc2U7XHJcbiAgICAgICAgZm9yKGxldCBpPTA7IGkgPCBzb3VyY2VUYXNrcy5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICBpZihzb3VyY2VUYXNrc1tpXT8uSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgYXBwcm92YWxUYXNrSW5Tb3VyY2UgPSB0cnVlO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uZ2V0UmF3VmFsdWUoKS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycpIHtcclxuICAgICAgICAgIHRoaXMudHJpZ2dlckRhdGEgPSBbXTtcclxuICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnRhc2tTdGF0dXNlcy5mb3JFYWNoKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzdGF0dXMuU1RBVFVTX1RZUEUgIT09IFN0YXR1c1R5cGVzLklOSVRJQUwgJiYgc3RhdHVzLlNUQVRVU19UWVBFICE9PSBTdGF0dXNUeXBlcy5GSU5BTF9DQU5DRUxMRUQgJiZcclxuICAgICAgICAgICAgICAhKHN0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQgJiYgIWFwcHJvdmFsVGFza0luU291cmNlKSkge1xyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIHRoaXMudHJpZ2dlckRhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuTkFNRSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZFxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRUcmlnZ2VyRGF0YT0gdGhpcy50cmlnZ2VyRGF0YVswXTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIFxyXG4gICAgICBsZXQgaXNUYXJnZXRUYXNrID0gZmFsc2U7XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgIC8vIGlmIChydWxlLnRhcmdldERhdGEgPT09IGN1cnJlbnRUYXNrKSB7XHJcbiAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICBpZihydWxlLnRhcmdldERhdGEgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXMgPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpO1xyXG4gICAgICAgICAgICAgIGlmICghKHJ1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkID09PSBydWxlLnRyaWdnZXJEYXRhKSkge1xyXG4gICAgICAgICAgICAgICAgaXNUYXJnZXRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pICBcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlKSB7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFRhc2tMaXN0ID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZTtcclxuICAgICAgICBmb3IobGV0IGk9MDsgaSA8IGN1cnJlbnRUYXNrTGlzdC5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICBsZXQgY3VycmVudFRhc2sgPSB0aGlzLnRhc2tEYXRhLmZpbmQodGFzayA9PiB0YXNrLklEID09PSBjdXJyZW50VGFza0xpc3RbaV0pO1xyXG4gICAgICAgICAgaWYoY3VycmVudFRhc2sgJiYgY3VycmVudFRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgdGhpcy5hcHByb3ZhbFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICB0aGlzLmFwcHJvdmFsVGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoaXNUYXJnZXRUYXNrIHx8IHRoaXMuYXBwcm92YWxUYXNrKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5pc0luaXRpYWxSdWxlLmRpc2FibGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmlzSW5pdGlhbFJ1bGUuZW5hYmxlKCk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnNldFZhbHVlKCcnKTtcclxuICAgICAgdGhpcy5ncm91cFRhcmdldFRhc2soKTtcclxuICAgIH0pO1xyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUodHJpZ2dlclR5cGUgPT4ge1xyXG4gICAgICBpZiAodHJpZ2dlclR5cGUpIHtcclxuICAgICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZSkge1xyXG4gICAgICAgICAgY29uc3QgY3VycmVudFRhc2tMaXN0ID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZTtcclxuICAgICAgICAgIGZvcihsZXQgaT0wOyBpIDwgY3VycmVudFRhc2tMaXN0Lmxlbmd0aDsgaSsrKXtcclxuICAgICAgICAgICAgbGV0IGN1cnJlbnRUYXNrID0gdGhpcy50YXNrRGF0YS5maW5kKHRhc2sgPT4gdGFzay5JRCA9PT0gY3VycmVudFRhc2tMaXN0W2ldKTtcclxuICAgICAgICAgICAgaWYoY3VycmVudFRhc2sgJiYgY3VycmVudFRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgICB0aGlzLmFwcHJvdmFsVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgIHRoaXMuYXBwcm92YWxUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy50cmlnZ2VyRGF0YSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgIGlmICh0cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycpIHtcclxuICAgICAgICAgIHRoaXMudGFza1N0YXR1c2VzLmZvckVhY2goc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgaWYgKHN0YXR1cy5TVEFUVVNfVFlQRSAhPT0gU3RhdHVzVHlwZXMuSU5JVElBTCAmJiBzdGF0dXMuU1RBVFVTX1RZUEUgIT09IFN0YXR1c1R5cGVzLkZJTkFMX0NBTkNFTExFRCAmJlxyXG4gICAgICAgICAgICAgICEoc3RhdHVzLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCAmJiAhdGhpcy5hcHByb3ZhbFRhc2spKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRyaWdnZXJEYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZTogc3RhdHVzLk5BTUUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWRcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0cmlnZ2VyVHlwZSA9PT0gJ0FDVElPTicpIHtcclxuICAgICAgICAgIGNvbnN0IHRhc2tXb3JrZmxvd0FjdGlvbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFRhc2tXb3JrZmxvd0FjdGlvbnMoKTtcclxuICAgICAgICAgIHRhc2tXb3JrZmxvd0FjdGlvbnMuZm9yRWFjaChhY3Rpb24gPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnRyaWdnZXJEYXRhLnB1c2goe1xyXG4gICAgICAgICAgICAgIG5hbWU6IGFjdGlvbi5OQU1FLFxyXG4gICAgICAgICAgICAgIHZhbHVlOiBhY3Rpb25bJ01QTV9Xb3JrZmxvd19BY3Rpb25zLWlkJ10uSWRcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0VHlwZS52YWx1ZSA9PT0gJ1RBU0snKSB7XHJcbiAgICAgICAgICB0aGlzLnRhcmdldERhdGEgPSBbXTtcclxuICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICB0aGlzLnRhc2tEYXRhLmZvckVhY2godGFzayA9PiB7XHJcbiAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXMgPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpO1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXM9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cz0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEU9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpXHJcbiAgICAgICAgICAgIGlmKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgICBsZXQgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKXtcclxuICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgaWYgKCEoaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayAmJiAoKHRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgfHwgKHRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykpKSkge1xyXG4gICAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJlbnRUYXNrUnVsZUxpc3QgPSB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maWx0ZXIocnVsZSA9PiBydWxlLmN1cnJlbnRUYXNrWzBdID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkgfHwgcnVsZS50YXJnZXREYXRhID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHBhcmVudFRhc2tSdWxlTGlzdCAmJiBwYXJlbnRUYXNrUnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICBwYXJlbnRUYXNrUnVsZUxpc3QuZm9yRWFjaChwYXJlbnRUYXNrUnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEocGFyZW50VGFza1J1bGUgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGEgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkIHx8IHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhPT09IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlc1snTVBNX1N0YXR1cy1pZCddLklkKSkpIHx8IChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyVHlwZSA9PT0gJ0FDVElPTicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrQ29tcGxldGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoaXNQYXJlbnRUYXNrQ29tcGxldGVkKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuSU5JVElBTCAmJiB0YXNrLklEICE9PSB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlKSB7IC8vICAmJiB0YXNrLlRBU0tfU1RBUlRfREFURSA+IGN1cnJlbnRUYXNrLlRBU0tfRFVFX0RBVEVcclxuICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVWSUVXX1JFSkVDVEVEKVxyXG4gICAgICAgICAgICAgICAgICAsaXNBcHByb3ZhbFRhc2s6IHRhc2suSVNfQVBQUk9WQUxfVEFTSz09PSd0cnVlJz90cnVlOmZhbHNlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmdyb3VwVGFyZ2V0VGFzaygpO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEuZW5hYmxlKCk7XHJcbiAgICAgICAgXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUodHJpZ2dlckRhdGEgPT4ge1xyXG4gICAgICBpZiAodHJpZ2dlckRhdGEgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXRUeXBlLnZhbHVlID09PSAnVEFTSycpIHsgXHJcbiAgICAgICAgbGV0IHJlbW92ZWRUYXNrcyA9IFtdXHJcbiAgICAgICAgdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFzay5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5pbmRleE9mKGVhY2hUYXNrKTtcclxuICAgICAgICAgIGlmKGluZGV4IDwgMCl7XHJcbiAgICAgICAgICAgIHJlbW92ZWRUYXNrcy5wdXNoKGVhY2hUYXNrKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICAgIHRoaXMudGFyZ2V0RGF0YSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudGFyZ2V0RGF0YS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgdGhpcy50YXNrRGF0YS5mb3JFYWNoKHRhc2sgPT4ge1xyXG4gICAgICAgICAgbGV0IGlzU2VsZWN0ZWRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICBsZXQgaXNDeWNsZVRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgaWYgKHRoaXMuY3VycmVudFJ1bGVMaXN0ICYmIHRoaXMuY3VycmVudFJ1bGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgY3VycmVudFRhc2tSdWxlTGlzdCA9IFtdOyBcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYocnVsZS5jdXJyZW50VGFza1swXSA9PT0gZWFjaFRhc2spe1xyXG4gICAgICAgICAgICAgICAgICBjdXJyZW50VGFza1J1bGVMaXN0LnB1c2gocnVsZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmKHRhc2suSUQgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJykge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHRhcmdldFRhc2tSdWxlTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZihydWxlLnRhcmdldERhdGEgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFRhc2tSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIHRhcmdldFRhc2tSdWxlTGlzdC5mb3JFYWNoKHJ1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJ1bGUuY3VycmVudFRhc2tbMF0gPT09IHRhc2suSUQgJiYgcnVsZS50cmlnZ2VyVHlwZSA9PT0gJ1NUQVRVUycgJiYgcnVsZS50cmlnZ2VyRGF0YSA9PT0gdHJpZ2dlckRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgaXNDeWNsZVRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXMgPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpO1xyXG4gICAgICAgICAgY29uc3Qgc3RhdHVzQXNSZXF1ZXN0ZWRDaGFuZ2VzPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXM9PiB0YXNrU3RhdHVzLlNUQVRVU19UWVBFPT09IFN0YXR1c1R5cGVzLkZJTkFMX1JFSkVDVEVEKVxyXG4gICAgICAgICAgbGV0IGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgIGlmKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKXtcclxuICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0luQ3VycmVudFRhc2sgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIGlmICghKGlzUGFyZW50VGFza0luQ3VycmVudFRhc2tcclxuICAgICAgICAgICAgJiYgKCh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0cmlnZ2VyRGF0YSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpIHx8XHJcbiAgICAgICAgICAgICAgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdBQ1RJT04nKSkpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgcGFyZW50VGFza1J1bGVMaXN0ID0gdGhpcy5jdXJyZW50UnVsZUxpc3QuZmlsdGVyKHJ1bGUgPT4gcnVsZS5jdXJyZW50VGFza1swXSA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpIHx8IHJ1bGUudGFyZ2V0RGF0YSA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKTtcclxuXHJcbiAgICAgICAgICAgICAgLy93aGlsZSBlZGl0aW5nIGlmIGEgcmVtb3ZlIGEgcGFyZW50IHRhc2sgZnJvbSBzb3VyY2UgdGFzayBsaXN0IHRoZW4gd2UgYXJlIHJlbW92aW5nIHRoYXQgcnVsZSBmcm9tIHBhcmVudFRhc2tSdWxlTGlzdFxyXG4gICAgICAgICAgICAgIGZvcihsZXQgaT0wOyBpPHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICByZW1vdmVkVGFza3MuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmKGVhY2hUYXNrID09PSBwYXJlbnRUYXNrUnVsZUxpc3RbaV0uY3VycmVudFRhc2tbMF0pe1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmVudFRhc2tSdWxlTGlzdC5zcGxpY2UoaSwxKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaWYgKHBhcmVudFRhc2tSdWxlTGlzdCAmJiBwYXJlbnRUYXNrUnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgcGFyZW50VGFza1J1bGVMaXN0LmZvckVhY2gocGFyZW50VGFza1J1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICBpZiAoIShwYXJlbnRUYXNrUnVsZSAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIChwYXJlbnRUYXNrUnVsZS50cmlnZ2VyRGF0YSA9PT0gc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQgfHwgcGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGE9PT0gc3RhdHVzQXNSZXF1ZXN0ZWRDaGFuZ2VzWydNUE1fU3RhdHVzLWlkJ10uSWQpKSB8fCAocGFyZW50VGFza1J1bGUudHJpZ2dlclR5cGUgPT09ICdBQ1RJT04nKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAgIGlmICghaXNTZWxlY3RlZFRhc2sgJiYgIWlzQ3ljbGVUYXNrICYmIGlzUGFyZW50VGFza0NvbXBsZXRlZCkge1xyXG4gICAgICAgICAgICBpZiAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5JTklUSUFMICYmIHRhc2suSUQgIT09IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUpIHsgLy8gICYmIHRhc2suVEFTS19TVEFSVF9EQVRFID4gY3VycmVudFRhc2suVEFTS19EVUVfREFURVxyXG4gICAgICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHRhc2suVEFTS19OQU1FLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHRhc2suSUQsXHJcbiAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVWSUVXX1JFSkVDVEVEKVxyXG4gICAgICAgICAgICAgICAsaXNBcHByb3ZhbFRhc2s6IHRhc2suSVNfQVBQUk9WQUxfVEFTSz09PSAndHJ1ZScgID90cnVlOmZhbHNlXHJcbiAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5ncm91cFRhcmdldFRhc2soKTtcclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldFR5cGUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSh0YXJnZXRUeXBlID0+IHtcclxuICAgICAgaWYgKHRhcmdldFR5cGUpIHtcclxuICAgICAgICB0aGlzLnRhcmdldERhdGEgPSBbXTtcclxuICAgICAgICB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRhcmdldERhdGEuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgIGlmICh0YXJnZXRUeXBlID09PSAnRVZFTlQnKSB7XHJcbiAgICAgICAgICBjb25zdCB0YXNrRXZlbnRzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRUYXNrRXZlbnRzKCk7XHJcbiAgICAgICAgICB0YXNrRXZlbnRzLmZvckVhY2goZXZlbnQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnRhcmdldERhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgbmFtZTogZXZlbnQuRElTUExBWV9OQU1FLFxyXG4gICAgICAgICAgICAgIHZhbHVlOiBldmVudFsnTVBNX0V2ZW50cy1pZCddLklkXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0YXJnZXRUeXBlID09PSAnVEFTSycpIHtcclxuICAgICAgICAgIHRoaXMudGFza0RhdGEuZm9yRWFjaCh0YXNrID0+IHtcclxuICAgICAgICAgICAgbGV0IGlzU2VsZWN0ZWRUYXNrID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBsZXQgaXNDeWNsZVRhc2sgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKChlYWNoVGFzaykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmKHRhc2suSUQgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWRUYXNrID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmN1cnJlbnRSdWxlTGlzdCAmJiB0aGlzLmN1cnJlbnRSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICBpZiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyVHlwZS52YWx1ZSA9PT0gJ1NUQVRVUycgJiYgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFyZ2V0VGFza1J1bGVMaXN0ID0gW107IFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50UnVsZUxpc3QuZm9yRWFjaChydWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5mb3JFYWNoKGVhY2hUYXNrID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZihydWxlLnRhcmdldERhdGEgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgICAgICAgICAgICAgIHRhcmdldFRhc2tSdWxlTGlzdC5wdXNoKHJ1bGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGFyZ2V0VGFza1J1bGVMaXN0LmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChydWxlLmN1cnJlbnRUYXNrWzBdID09PSB0YXNrLklEICYmIHJ1bGUudHJpZ2dlclR5cGUgPT09ICdTVEFUVVMnICYmIHJ1bGUudHJpZ2dlckRhdGEgPT09IHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlckRhdGEudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpc0N5Y2xlVGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnRhc2tTdGF0dXNlcy5maW5kKHRhc2tTdGF0dXMgPT4gdGFza1N0YXR1cy5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpO1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXNBc1JlcXVlc3RlZENoYW5nZXM9IHRoaXMudGFza1N0YXR1c2VzLmZpbmQodGFza1N0YXR1cz0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEU9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpXHJcbiAgICAgICAgICAgIGlmKHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0gJ3RydWUnKXtcclxuICAgICAgICAgICAgICBsZXQgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuZm9yRWFjaChlYWNoVGFzayA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZihlYWNoVGFzayA9PT0gU3RyaW5nKHRhc2suVEFTS19QQVJFTlRfSUQpKXtcclxuICAgICAgICAgICAgICAgICAgaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFzayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSkgXHJcbiAgICAgICAgICAgICAgaWYgKCEoaXNQYXJlbnRUYXNrSW5DdXJyZW50VGFza1xyXG4gICAgICAgICAgICAgICAgJiYgKCh0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJUeXBlLnZhbHVlID09PSAnU1RBVFVTJyAmJiB0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLnRyaWdnZXJEYXRhLnZhbHVlID09PSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgfHxcclxuICAgICAgICAgICAgICAgICAgKHRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMudHJpZ2dlclR5cGUudmFsdWUgPT09ICdBQ1RJT04nKSkpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UnVsZUxpc3QgJiYgdGhpcy5jdXJyZW50UnVsZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICBjb25zdCBwYXJlbnRUYXNrUnVsZUxpc3QgPSB0aGlzLmN1cnJlbnRSdWxlTGlzdC5maWx0ZXIocnVsZSA9PiBydWxlLmN1cnJlbnRUYXNrWzBdID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkgfHwgcnVsZS50YXJnZXREYXRhID09PSBTdHJpbmcodGFzay5UQVNLX1BBUkVOVF9JRCkpO1xyXG4gICAgICAgICAgICAgICAgICBpZiAocGFyZW50VGFza1J1bGVMaXN0ICYmIHBhcmVudFRhc2tSdWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50VGFza1J1bGVMaXN0LmZvckVhY2gocGFyZW50VGFza1J1bGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgaWYgKCEocGFyZW50VGFza1J1bGUgJiYgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnU1RBVFVTJyAmJiAocGFyZW50VGFza1J1bGUudHJpZ2dlckRhdGEgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkIHx8IHBhcmVudFRhc2tSdWxlLnRyaWdnZXJEYXRhPT09IHN0YXR1c0FzUmVxdWVzdGVkQ2hhbmdlc1snTVBNX1N0YXR1cy1pZCddLklkKSkgfHwgKHBhcmVudFRhc2tSdWxlLnRyaWdnZXJUeXBlID09PSAnQUNUSU9OJykpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzUGFyZW50VGFza0NvbXBsZXRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBpc1BhcmVudFRhc2tDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICghaXNTZWxlY3RlZFRhc2sgJiYgIWlzQ3ljbGVUYXNrICYmIGlzUGFyZW50VGFza0NvbXBsZXRlZCkge1xyXG4gICAgICAgICAgICAgIGlmICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLklOSVRJQUwgKSB7IC8vICAmJiB0YXNrLlRBU0tfU1RBUlRfREFURSA+IGN1cnJlbnRUYXNrLlRBU0tfRFVFX0RBVEVcclxuICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0RGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgbmFtZTogdGFzay5UQVNLX05BTUUsXHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiB0YXNrLklELFxyXG4gICAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQVBQUk9WRUQpIHx8ICh0YXNrLlRBU0tfU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0NPTVBMRVRFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ0FOQ0VMTEVEKSB8fCAodGFzay5UQVNLX1NUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRCkgfHwgKHRhc2suVEFTS19TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVWSUVXX1JFSkVDVEVEKSxcclxuICAgICAgICAgICAgICAgICAgaXNBcHByb3ZhbFRhc2s6IHRhc2suSVNfQVBQUk9WQUxfVEFTSyA9PT0ndHJ1ZScgP3RydWU6ZmFsc2VcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuZ3JvdXBUYXJnZXRUYXNrKCk7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLmVuYWJsZSgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50YXJnZXREYXRhLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLmlzUnVsZVNhdmVkID0gZmFsc2U7XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5pc1J1bGVDaGFuZ2VkID0gdHJ1ZTtcclxuICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGNvbnN0IHNvdXJjZVRhc2tzU2V0ID0gbmV3IFNldCgpO1xyXG4gICAgdGhpcy5ydWxlTGlzdEdyb3VwLmZvckVhY2gocnVsZSA9PiB7XHJcbiAgICAgIHJ1bGUuaGFzUHJlZGVjZXNzb3IgPSB0cnVlO1xyXG4gICAgfSlcclxuICAgIHRoaXMudGFza0RhdGEuZm9yRWFjaCgodGFzaykgPT4ge1xyXG4gICAgICB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmZvckVhY2goZWFjaFRhc2sgPT4ge1xyXG4gICAgICAgIGlmKHRhc2suSUQgPT09IGVhY2hUYXNrKXtcclxuICAgICAgICAgIHNvdXJjZVRhc2tzU2V0LmFkZCh0YXNrLlRBU0tfTkFNRSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICAgIHNvdXJjZVRhc2tzU2V0LmZvckVhY2godGFzayA9PiB0aGlzLmN1cnJlbnRTb3VyY2VUYXNrcy5wdXNoKHRhc2spKTtcclxuICAgIHRoaXMucnVsZURhdGEuY3VycmVudFRhc2sgPSB0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrLmxlbmd0aCA+IDAgJiYgQXJyYXkuaXNBcnJheSh0aGlzLnJ1bGVEYXRhLmN1cnJlbnRUYXNrKSA/IHRoaXMucnVsZURhdGEuY3VycmVudFRhc2sgOiBbdGhpcy5ydWxlRGF0YS5jdXJyZW50VGFza107XHJcbiAgICB0aGlzLnJ1bGVMaXN0LmZvckVhY2goKHJ1bGUpID0+IHtcclxuICAgICAgcnVsZS5jdXJyZW50VGFzayA9IHJ1bGUuY3VycmVudFRhc2subGVuZ3RoID4gMCAmJiBBcnJheS5pc0FycmF5KHJ1bGUuY3VycmVudFRhc2spID8gcnVsZS5jdXJyZW50VGFzayA6IFtydWxlLmN1cnJlbnRUYXNrXTtcclxuICAgIH0pXHJcbiAgICB0aGlzLmluaXRpYWxpemVGb3JtKCk7XHJcbiAgfVxyXG4gIFxyXG4gIGdyb3VwVGFyZ2V0VGFzaygpe1xyXG4gICAgY29uc3Qgc3RhdHVzID0gdGhpcy50YXNrU3RhdHVzZXMuZmluZCh0YXNrU3RhdHVzID0+IHRhc2tTdGF0dXMuU1RBVFVTX1RZUEUgPT09IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEKTtcclxuICAvLyBpZih0aGlzLmlzVGFza0xldmVsY2FsbGVkKXtcclxuICAgICBpZih0aGlzLnRhc2tXb3JrRmxvd1J1bGVUb0Rpc3BsYXk/LklTX0FQUFJPVkFMX1RBU0s9PT0gJ3RydWUnICYmICEodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZT09c3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpKXtcclxuICAgICAgY29uc3QgbmV3dGFyZ2V0RGF0YT0gdGhpcy50YXJnZXREYXRhLmZpbHRlcih0YXJnZXRUYXNrPT4gIXRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuaW5jbHVkZXModGFyZ2V0VGFzay52YWx1ZSkpLyouZmlsdGVyKHQxPT4gdDEuaXNBcHByb3ZhbFRhc2s9PWZhbHNlKSovXHJcbiAgICAgIHRoaXMudGFyZ2V0RGF0YT0gbmV3dGFyZ2V0RGF0YVxyXG4gICAgIH1cclxuICAgICBcclxuICAgIGVsc2UgaWYodGhpcy50YXNrV29ya0Zsb3dSdWxlVG9EaXNwbGF5Py5JU19BUFBST1ZBTF9UQVNLPT09ICd0cnVlJyAmJiAodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZT09c3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpKXtcclxuICAgICBjb25zdCBwYXJlbnRJZD0gdGhpcy50YXNrV29ya0Zsb3dSdWxlVG9EaXNwbGF5Py5UQVNLX1BBUkVOVF9JRCsnJztcclxuICAgICBjb25zdCByZW1vdmVUYXJnZXRUYXNrPSBuZXcgU2V0KCk7XHJcbiAgICAgIHRoaXMudGFza3J1bGVMaXN0R3JvdXAuZm9yRWFjaCh0YXNrPT57XHJcbiAgICAgICAgaWYodGFzay5jdXJyZW50VGFzay5pbmNsdWRlcyhwYXJlbnRJZCkpe1xyXG4gICAgICAgICAgdGFzay5jdXJyZW50VGFzay5mb3JFYWNoKHRhc2tJZD0+cmVtb3ZlVGFyZ2V0VGFzay5hZGQodGFza0lkKSlcclxuICAgICAgICB9XHJcbiAgICAgICAvLyBjb25zdCBuZXd0YXJnZXREYXRhPSB0aGlzLnRhcmdldERhdGEuZmlsdGVyKHRhcmdldFRhc2s9PiB0YXJnZXRUYXNrLnZhbHVlIT10aGlzLnRhc2tXb3JrRmxvd1J1bGVUb0Rpc3BsYXk/LlRBU0tfUEFSRU5UX0lEICYmICF0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmluY2x1ZGVzKHRhcmdldFRhc2sudmFsdWUpICYmICFyZW1vdmVUYXJnZXRUYXNrLmhhcyh0YXJnZXRUYXNrLnZhbHVlKSlcclxuICAgICAgY29uc3QgbmV3dGFyZ2V0RGF0YT0gdGhpcy50YXJnZXREYXRhLmZpbHRlcih0YXJnZXRUYXNrPT4hdGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy5jdXJyZW50VGFzay52YWx1ZS5pbmNsdWRlcyh0YXJnZXRUYXNrLnZhbHVlKSlcclxuICAgICAgIHRoaXMudGFyZ2V0RGF0YT0gbmV3dGFyZ2V0RGF0YVxyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gICAgIGVsc2UgaWYodGhpcy5jdXN0b21Xb3JrZmxvd0ZpZWxkRm9ybS5jb250cm9scy50cmlnZ2VyRGF0YS52YWx1ZSE9c3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpe1xyXG4gICAgICB2YXIgbmV3dGFyZ2V0RGF0YT0gdGhpcy50YXJnZXREYXRhLmZpbHRlcih0YXJnZXRUYXNrPT4gIXRoaXMuY3VzdG9tV29ya2Zsb3dGaWVsZEZvcm0uY29udHJvbHMuY3VycmVudFRhc2sudmFsdWUuaW5jbHVkZXModGFyZ2V0VGFzay52YWx1ZSkpLyouZmlsdGVyKHQxPT4gdDEuaXNBcHByb3ZhbFRhc2s9PWZhbHNlKSovXHJcbiAgICAgIFxyXG4gICAgICB0aGlzLnRhcmdldERhdGE9IG5ld3RhcmdldERhdGFcclxuICAgICB9XHJcbiAgICAgZWxzZXtcclxuICAgIGNvbnN0IG5ld3RhcmdldERhdGE9IHRoaXMudGFyZ2V0RGF0YS5maWx0ZXIodGFyZ2V0VGFzaz0+IC8qdGFyZ2V0VGFzay52YWx1ZSE9dGhpcy50YXNrV29ya0Zsb3dSdWxlVG9EaXNwbGF5Py5UQVNLX1BBUkVOVF9JRCAmJiovICF0aGlzLmN1c3RvbVdvcmtmbG93RmllbGRGb3JtLmNvbnRyb2xzLmN1cnJlbnRUYXNrLnZhbHVlLmluY2x1ZGVzKHRhcmdldFRhc2sudmFsdWUpKVxyXG4gICAgdGhpcy50YXJnZXREYXRhPSBuZXd0YXJnZXREYXRhXHJcbiAgICB9XHJcbn1cclxufVxyXG4iXX0=