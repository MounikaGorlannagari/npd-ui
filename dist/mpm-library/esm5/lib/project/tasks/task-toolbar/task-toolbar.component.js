import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl, FormArray } from '@angular/forms';
import { ItemSortComponent } from '../item-sort/item-sort.component';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { TaskConstants } from '../task.constants';
import { CommentsUtilService } from '../../../comments/services/comments.util.service';
import { TaskTypeIconForTask } from '../../shared/constants/task-type-icon.constants';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { ApplicationConfigConstants } from '../../../shared/constants/application.config.constants';
import { AssetService } from '../../shared/services/asset.service';
var TaskToolbarComponent = /** @class */ (function () {
    function TaskToolbarComponent(sharingService, commentUtilService, utilService, assetService) {
        this.sharingService = sharingService;
        this.commentUtilService = commentUtilService;
        this.utilService = utilService;
        this.assetService = assetService;
        this.taskConfigChange = new EventEmitter();
        this.createNewTaskHandler = new EventEmitter();
        this.createNewTaskByTypeHandler = new EventEmitter();
        this.viewChange = new EventEmitter();
        this.customWorkflowHandler = new EventEmitter();
        this.columnChooserHandler = new EventEmitter();
        this.saveColumnChooserHandler = new EventEmitter();
        this.resetColumnChooserHandler = new EventEmitter();
        this.isColumnsFreezable = false;
        this.showTaskViewButton = true;
        this.resources = new FormArray([]);
        this.viewByResources = new FormControl();
        this.taskConstants = TaskConstants;
        this.disableNewTask = false;
        this.taskTypeIcon = TaskTypeIconForTask;
    }
    TaskToolbarComponent.prototype.createNewTask = function () {
        this.createNewTaskHandler.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.createNewTaskByType = function (type) {
        this.taskConfig.taskCreationType = type;
        this.createNewTaskByTypeHandler.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.refresh = function () {
        this.taskConfigChange.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.openCustomWorkflow = function () {
        this.customWorkflowHandler.next();
    };
    TaskToolbarComponent.prototype.updateDisplayColumns = function (displayColumns) {
        this.columnChooserHandler.next(displayColumns);
    };
    TaskToolbarComponent.prototype.saveDisplayColumns = function (displayColumns) {
        this.saveColumnChooserHandler.next(displayColumns);
    };
    TaskToolbarComponent.prototype.resetDisplayColumns = function () {
        this.resetColumnChooserHandler.next();
    };
    TaskToolbarComponent.prototype.changeView = function () {
        if (this.taskConfig.isTaskListView) {
            this.taskConfig.isTaskListView = false;
        }
        else {
            this.taskConfig.isTaskListView = true;
        }
        this.viewChange.next(this.taskConfig.isTaskListView);
        this.refresh();
    };
    TaskToolbarComponent.prototype.onSortChange = function (eventData) {
        var _this = this;
        if (eventData && eventData.option) {
            if (this.taskConfig.isTaskListView) {
                this.taskConfig.taskListSortFieldName = eventData.option.name;
                this.taskConfig.taskListSortOrder = eventData.sortType;
                this.taskConfig.selectedTaskListSortOption = {
                    option: this.taskConfig.taskListSortOptions.find(function (data) { return data.name === _this.taskConfig.taskListSortFieldName; }),
                    sortType: this.taskConfig.taskListSortOrder
                };
            }
            else {
                this.taskConfig.taskCardSortFieldName = eventData.option.name;
                this.taskConfig.taskCardSortOrder = eventData.sortType;
                this.taskConfig.selectedTaskCardSortOption = {
                    option: this.taskConfig.taskCardSortOptions.find(function (data) { return data.name === _this.taskConfig.taskCardSortFieldName; }),
                    sortType: this.taskConfig.taskCardSortOrder
                };
            }
        }
        this.taskConfig.isSortChange = true;
        this.taskConfigChange.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.refreshChild = function (taskConfig) {
        this.taskConfig = taskConfig;
        if (this.itemSortComponent) {
            this.itemSortComponent.refreshData(this.taskConfig);
        }
    };
    TaskToolbarComponent.prototype.onResourceSelect = function () {
        this.taskConfig.selectedResources = this.viewByResources.value;
        this.taskConfigChange.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.onResourceRemoved = function (resource) {
        var index = this.taskConfig.selectedResources.indexOf(resource);
        this.taskConfig.selectedResources.splice(index, 1);
        this.viewByResources.patchValue(this.taskConfig.selectedResources);
        this.taskConfigChange.next(this.taskConfig);
    };
    TaskToolbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.taskConfig.projectId) {
            this.assetService.getProjectDetails(this.taskConfig.projectId).subscribe(function (projectResponse) {
                _this.isOnHoldProject = projectResponse.Project.IS_ON_HOLD;
                var currentUSerID = _this.sharingService.getCurrentUserItemID();
                if (currentUSerID && _this.taskConfig.selectedProject &&
                    currentUSerID === _this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
                    _this.taskConfig.isProjectOwner = true;
                }
                _this.isProjectCompleted = _this.taskConfig.isReadOnly;
                console.log(_this.taskConfig.IS_ON_HOLD);
                _this.appConfig = _this.sharingService.getAppConfig();
                _this.disableGridView = _this.utilService.getBooleanValue(_this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]);
            });
        }
        /* const currentUSerID = this.sharingService.getCurrentUserItemID();
        if (currentUSerID && this.taskConfig.selectedProject &&
            currentUSerID === this.taskConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.taskConfig.isProjectOwner = true;
        }
        this.isProjectCompleted = this.taskConfig.isReadOnly;
        console.log(this.taskConfig.IS_ON_HOLD);
        this.appConfig = this.sharingService.getAppConfig();
        this.disableGridView = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DISABLE_GRID_VIEW]);
    */ 
    };
    TaskToolbarComponent.ctorParameters = function () { return [
        { type: SharingService },
        { type: CommentsUtilService },
        { type: UtilService },
        { type: AssetService }
    ]; };
    __decorate([
        Input()
    ], TaskToolbarComponent.prototype, "taskConfig", void 0);
    __decorate([
        Input()
    ], TaskToolbarComponent.prototype, "columnChooserFields", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "taskConfigChange", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "createNewTaskHandler", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "createNewTaskByTypeHandler", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "viewChange", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "customWorkflowHandler", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "columnChooserHandler", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "saveColumnChooserHandler", void 0);
    __decorate([
        Output()
    ], TaskToolbarComponent.prototype, "resetColumnChooserHandler", void 0);
    __decorate([
        ViewChild(ItemSortComponent)
    ], TaskToolbarComponent.prototype, "itemSortComponent", void 0);
    TaskToolbarComponent = __decorate([
        Component({
            selector: 'mpm-task-toolbar',
            template: "<mat-toolbar class=\"app-task-toolbar\">\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-row-item task-info-holder\">\r\n            <mpm-item-count *ngIf=\"taskConfig\" [name]=\"'Tasks'\" [count]=\"taskConfig.taskTotalCount\"></mpm-item-count>\r\n            <button mat-icon-button (click)=\"refresh()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\" *ngIf=\"!taskConfig.isTaskListView\">\r\n            <mpm-item-sort [sortOptions]=\"taskConfig.taskSortOptions\" [selectedOption]=\"taskConfig.selectedSortOption\"\r\n                (onSortChange)=\"onSortChange($event)\"></mpm-item-sort>\r\n        </div>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mat-form-field appearance=\"outline\" class=\"resource-select\">\r\n                <mat-label>View by resource</mat-label>\r\n                <mat-select #resourceSelect [formControl]=\"viewByResources\" multiple\r\n                    [disabled]=\"!taskConfig.taskOwnerResources.length\">\r\n                    <mat-select-trigger>\r\n                        {{viewByResources.value ? viewByResources.value[0] : ''}}\r\n                        <span *ngIf=\"viewByResources.value?.length > 1\" class=\"example-additional-selection\">\r\n                            (+{{viewByResources.value.length - 1}}\r\n                            {{viewByResources.value?.length === 2 ? 'other' : 'others'}})\r\n                        </span>\r\n                    </mat-select-trigger>\r\n                    <mat-option *ngFor=\"let resource of taskConfig.taskOwnerResources\" [value]=\"resource\">{{resource}}\r\n                    </mat-option>\r\n                    <button class=\"resource-apply-btn\" (click)=\"onResourceSelect(); resourceSelect.close()\"\r\n                        mat-button>Apply</button>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <button [matMenuTriggerFor]=\"addnewMenu\" mat-raised-button color=\"accent\"\r\n                matTooltip=\"Click to view more options\" [disabled]=\"taskConfig.isReadOnly\">\r\n                <mat-icon>add</mat-icon>\r\n                <span> New</span>\r\n                <mat-icon>arrow_drop_down</mat-icon>\r\n            </button>\r\n            <mat-menu #addnewMenu=\"matMenu\">\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.TASK_WITHOUT_DELIVERABLE}}</mat-icon>\r\n                    <span>Action Task</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.NORMAL_TASK)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.NORMAL_TASK}}</mat-icon>\r\n                    <span>Upload Task</span>\r\n                </button>\r\n                <button mat-menu-item (click)=\"createNewTaskByType(taskConstants.TASK_TYPE.APPROVAL_TASK)\"\r\n                    [disabled]=\"!taskConfig.isProjectOwner || isProjectCompleted\">\r\n                    <mat-icon>{{taskTypeIcon.APPROVAL_TASK}}</mat-icon>\r\n                    <span>Approval Task</span>\r\n                </button>\r\n            </mat-menu>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\" *ngIf=\"!disableGridView\">\r\n            <button matTooltip=\"Switch to {{taskConfig.isTaskListView ? 'Card' : 'List'}} View\" mat-button\r\n                (click)=\"changeView()\" *ngIf=\"showTaskViewButton\">\r\n                <mat-icon *ngIf=\"taskConfig.isTaskListView\" class=\"task-action-btn\">view_module\r\n                </mat-icon>\r\n                <mat-icon *ngIf=\"!taskConfig.isTaskListView\" class=\"task-action-btn\">\r\n                    format_list_bulleted\r\n                </mat-icon>\r\n            </button>\r\n        </div>\r\n        <span class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-icon-button>\r\n                <mat-icon class=\"dashboard-action-btn\">settings\r\n                </mat-icon>\r\n            </button>\r\n        </span>\r\n    </mat-toolbar-row>\r\n    <mat-toolbar-row *ngIf=\"taskConfig.selectedResources && taskConfig.selectedResources.length > 0\">\r\n        <span class=\"flex-row\">\r\n            <span class=\"resources-heading\">Resources: </span>\r\n            <mat-chip-list aria-label=\"resource selection\">\r\n                <mat-chip [removable]=\"true\" (removed)=\"onResourceRemoved(resource)\"\r\n                    *ngFor=\"let resource of taskConfig.selectedResources\">{{resource}}\r\n                    <mat-icon matChipRemove>cancel</mat-icon>\r\n                </mat-chip>\r\n            </mat-chip-list>\r\n        </span>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>",
            styles: ["mat-toolbar.app-task-toolbar{background-color:transparent}mat-toolbar .spacer{flex:1 1 auto}.flex-row-item button{margin:5px}.resources-row,.task-resource-icon{font-size:14px}.resourceCheckbox{margin-right:5px;margin-left:5px}.task-icon-btn{margin-top:18px}.task-info-holder{flex-grow:0;align-items:center}.align-items-center{align-items:center}.right{justify-content:flex-end}mat-form-field.resource-select{font-size:11px}::ng-deep .resource-select .mat-form-field-wrapper{padding:0}.flex-grow-0{flex-grow:0}.resources-heading{font-size:16px;margin-right:8px}.resource-apply-btn{width:100%}"]
        })
    ], TaskToolbarComponent);
    return TaskToolbarComponent;
}());
export { TaskToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay10b29sYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvdGFzay10b29sYmFyL3Rhc2stdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN2RixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUN0RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFDcEcsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBT25FO0lBMkJJLDhCQUNXLGNBQThCLEVBQzlCLGtCQUF1QyxFQUN2QyxXQUF3QixFQUN4QixZQUEwQjtRQUgxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFxQjtRQUN2QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQTNCM0IscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMzQyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLCtCQUEwQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckQsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckMsMEJBQXFCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNoRCx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLDZCQUF3QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkQsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5RCx1QkFBa0IsR0FBUyxLQUFLLENBQUE7UUFFaEMsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBRTFCLGNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5QixvQkFBZSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDcEMsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFDOUIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFDdkIsaUJBQVksR0FBRyxtQkFBbUIsQ0FBQztJQVkvQixDQUFDO0lBRUwsNENBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxrREFBbUIsR0FBbkIsVUFBb0IsSUFBSTtRQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsc0NBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxpREFBa0IsR0FBbEI7UUFDSSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELG1EQUFvQixHQUFwQixVQUFxQixjQUFjO1FBQy9CLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELGlEQUFrQixHQUFsQixVQUFtQixjQUFjO1FBQzdCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELGtEQUFtQixHQUFuQjtRQUNJLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQseUNBQVUsR0FBVjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1NBQzFDO2FBQU07WUFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDekM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsMkNBQVksR0FBWixVQUFhLFNBQVM7UUFBdEIsaUJBb0JDO1FBbkJHLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDOUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDO2dCQUN2RCxJQUFJLENBQUMsVUFBVSxDQUFDLDBCQUEwQixHQUFHO29CQUN6QyxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEVBQW5ELENBQW1ELENBQUM7b0JBQzdHLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtpQkFDOUMsQ0FBQzthQUNMO2lCQUFNO2dCQUNILElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQzlELElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLFVBQVUsQ0FBQywwQkFBMEIsR0FBRztvQkFDekMsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxLQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFuRCxDQUFtRCxDQUFDO29CQUM3RyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7aUJBQzlDLENBQUM7YUFDTDtTQUNKO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCwyQ0FBWSxHQUFaLFVBQWEsVUFBVTtRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN2RDtJQUNMLENBQUM7SUFFRCwrQ0FBZ0IsR0FBaEI7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDO1FBQy9ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxnREFBaUIsR0FBakIsVUFBa0IsUUFBUTtRQUN0QixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQUEsaUJBeUJJO1FBeEJBLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7WUFDM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGVBQWU7Z0JBQ3BGLEtBQUksQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7Z0JBQzFELElBQU0sYUFBYSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztnQkFDakUsSUFBSSxhQUFhLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlO29CQUNoRCxhQUFhLEtBQUssS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUN4RixLQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7aUJBQ3pDO2dCQUNELEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztnQkFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN4QyxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBRXpJLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRDs7Ozs7Ozs7O01BU0Y7SUFBQyxDQUFDOztnQkFoSHVCLGNBQWM7Z0JBQ1YsbUJBQW1CO2dCQUMxQixXQUFXO2dCQUNWLFlBQVk7O0lBN0I1QjtRQUFSLEtBQUssRUFBRTs0REFBWTtJQUNYO1FBQVIsS0FBSyxFQUFFO3FFQUFxQjtJQUNuQjtRQUFULE1BQU0sRUFBRTtrRUFBNEM7SUFDM0M7UUFBVCxNQUFNLEVBQUU7c0VBQWdEO0lBQy9DO1FBQVQsTUFBTSxFQUFFOzRFQUFzRDtJQUNyRDtRQUFULE1BQU0sRUFBRTs0REFBc0M7SUFDckM7UUFBVCxNQUFNLEVBQUU7dUVBQWlEO0lBQ2hEO1FBQVQsTUFBTSxFQUFFO3NFQUFnRDtJQUMvQztRQUFULE1BQU0sRUFBRTswRUFBb0Q7SUFDbkQ7UUFBVCxNQUFNLEVBQUU7MkVBQXFEO0lBY2hDO1FBQTdCLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQzttRUFBc0M7SUF6QjFELG9CQUFvQjtRQU5oQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLDZwTEFBNEM7O1NBRS9DLENBQUM7T0FFVyxvQkFBb0IsQ0E2SWhDO0lBQUQsMkJBQUM7Q0FBQSxBQTdJRCxJQTZJQztTQTdJWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIEZvcm1BcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgSXRlbVNvcnRDb21wb25lbnQgfSBmcm9tICcuLi9pdGVtLXNvcnQvaXRlbS1zb3J0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IENvbW1lbnRzVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9jb21tZW50cy9zZXJ2aWNlcy9jb21tZW50cy51dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrVHlwZUljb25Gb3JUYXNrIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy90YXNrLXR5cGUtaWNvbi5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBVdGlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFzc2V0U2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9hc3NldC5zZXJ2aWNlJztcclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS10YXNrLXRvb2xiYXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Rhc2stdG9vbGJhci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi90YXNrLXRvb2xiYXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRhc2tUb29sYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSB0YXNrQ29uZmlnO1xyXG4gICAgQElucHV0KCkgY29sdW1uQ2hvb3NlckZpZWxkcztcclxuICAgIEBPdXRwdXQoKSB0YXNrQ29uZmlnQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgY3JlYXRlTmV3VGFza0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjcmVhdGVOZXdUYXNrQnlUeXBlSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHZpZXdDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjdXN0b21Xb3JrZmxvd0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBjb2x1bW5DaG9vc2VySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNhdmVDb2x1bW5DaG9vc2VySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHJlc2V0Q29sdW1uQ2hvb3NlckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIGlzQ29sdW1uc0ZyZWV6YWJsZTpib29sZWFuPWZhbHNlXHJcbiAgICBpc1Byb2plY3RDb21wbGV0ZWQ6IGJvb2xlYW47XHJcbiAgICBzaG93VGFza1ZpZXdCdXR0b24gPSB0cnVlO1xyXG4gICAgc2VsZWN0ZWRPcHRpb247XHJcbiAgICByZXNvdXJjZXMgPSBuZXcgRm9ybUFycmF5KFtdKTtcclxuICAgIHZpZXdCeVJlc291cmNlcyA9IG5ldyBGb3JtQ29udHJvbCgpO1xyXG4gICAgdGFza0NvbnN0YW50cyA9IFRhc2tDb25zdGFudHM7XHJcbiAgICBkaXNhYmxlTmV3VGFzayA9IGZhbHNlO1xyXG4gICAgdGFza1R5cGVJY29uID0gVGFza1R5cGVJY29uRm9yVGFzaztcclxuICAgIGRpc2FibGVHcmlkVmlldzogYm9vbGVhbjtcclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgaXNPbkhvbGRQcm9qZWN0OiBib29sZWFuO1xyXG4gICAgZW5hYmxlQXBwcm92YWxUYXNrOiBib29sZWFuO1xyXG4gICAgQFZpZXdDaGlsZChJdGVtU29ydENvbXBvbmVudCkgaXRlbVNvcnRDb21wb25lbnQ6IEl0ZW1Tb3J0Q29tcG9uZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGNvbW1lbnRVdGlsU2VydmljZTogQ29tbWVudHNVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhc3NldFNlcnZpY2U6IEFzc2V0U2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjcmVhdGVOZXdUYXNrKCkge1xyXG4gICAgICAgIHRoaXMuY3JlYXRlTmV3VGFza0hhbmRsZXIubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZU5ld1Rhc2tCeVR5cGUodHlwZSkge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrQ3JlYXRpb25UeXBlID0gdHlwZTtcclxuICAgICAgICB0aGlzLmNyZWF0ZU5ld1Rhc2tCeVR5cGVIYW5kbGVyLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoKCkge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZ0NoYW5nZS5uZXh0KHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkN1c3RvbVdvcmtmbG93KCkge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tV29ya2Zsb3dIYW5kbGVyLm5leHQoKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVEaXNwbGF5Q29sdW1ucyhkaXNwbGF5Q29sdW1ucykge1xyXG4gICAgICAgIHRoaXMuY29sdW1uQ2hvb3NlckhhbmRsZXIubmV4dChkaXNwbGF5Q29sdW1ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2F2ZURpc3BsYXlDb2x1bW5zKGRpc3BsYXlDb2x1bW5zKSB7XHJcbiAgICAgICAgdGhpcy5zYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXIubmV4dChkaXNwbGF5Q29sdW1ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXREaXNwbGF5Q29sdW1ucygpIHtcclxuICAgICAgICB0aGlzLnJlc2V0Q29sdW1uQ2hvb3NlckhhbmRsZXIubmV4dCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVZpZXcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc1Rhc2tMaXN0Vmlldykge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcgPSBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnZpZXdDaGFuZ2UubmV4dCh0aGlzLnRhc2tDb25maWcuaXNUYXNrTGlzdFZpZXcpO1xyXG4gICAgICAgIHRoaXMucmVmcmVzaCgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU29ydENoYW5nZShldmVudERhdGEpIHtcclxuICAgICAgICBpZiAoZXZlbnREYXRhICYmIGV2ZW50RGF0YS5vcHRpb24pIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc1Rhc2tMaXN0Vmlldykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tMaXN0U29ydEZpZWxkTmFtZSA9IGV2ZW50RGF0YS5vcHRpb24ubmFtZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRPcmRlciA9IGV2ZW50RGF0YS5zb3J0VHlwZTtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFRhc2tMaXN0U29ydE9wdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgICAgICBvcHRpb246IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRPcHRpb25zLmZpbmQoZGF0YSA9PiBkYXRhLm5hbWUgPT09IHRoaXMudGFza0NvbmZpZy50YXNrTGlzdFNvcnRGaWVsZE5hbWUpLFxyXG4gICAgICAgICAgICAgICAgICAgIHNvcnRUeXBlOiB0aGlzLnRhc2tDb25maWcudGFza0xpc3RTb3J0T3JkZXJcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcudGFza0NhcmRTb3J0RmllbGROYW1lID0gZXZlbnREYXRhLm9wdGlvbi5uYW1lO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnRhc2tDYXJkU29ydE9yZGVyID0gZXZlbnREYXRhLnNvcnRUeXBlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkVGFza0NhcmRTb3J0T3B0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9wdGlvbjogdGhpcy50YXNrQ29uZmlnLnRhc2tDYXJkU29ydE9wdGlvbnMuZmluZChkYXRhID0+IGRhdGEubmFtZSA9PT0gdGhpcy50YXNrQ29uZmlnLnRhc2tDYXJkU29ydEZpZWxkTmFtZSksXHJcbiAgICAgICAgICAgICAgICAgICAgc29ydFR5cGU6IHRoaXMudGFza0NvbmZpZy50YXNrQ2FyZFNvcnRPcmRlclxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNTb3J0Q2hhbmdlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnRhc2tDb25maWdDaGFuZ2UubmV4dCh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hDaGlsZCh0YXNrQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnID0gdGFza0NvbmZpZztcclxuICAgICAgICBpZiAodGhpcy5pdGVtU29ydENvbXBvbmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLml0ZW1Tb3J0Q29tcG9uZW50LnJlZnJlc2hEYXRhKHRoaXMudGFza0NvbmZpZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmVzb3VyY2VTZWxlY3QoKSB7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzID0gdGhpcy52aWV3QnlSZXNvdXJjZXMudmFsdWU7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnQ2hhbmdlLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJlc291cmNlUmVtb3ZlZChyZXNvdXJjZSkge1xyXG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzLmluZGV4T2YocmVzb3VyY2UpO1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFJlc291cmNlcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cclxuICAgICAgICB0aGlzLnZpZXdCeVJlc291cmNlcy5wYXRjaFZhbHVlKHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFJlc291cmNlcyk7XHJcbiAgICAgICAgdGhpcy50YXNrQ29uZmlnQ2hhbmdlLm5leHQodGhpcy50YXNrQ29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy50YXNrQ29uZmlnLnByb2plY3RJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFzc2V0U2VydmljZS5nZXRQcm9qZWN0RGV0YWlscyh0aGlzLnRhc2tDb25maWcucHJvamVjdElkKS5zdWJzY3JpYmUocHJvamVjdFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNPbkhvbGRQcm9qZWN0ID0gcHJvamVjdFJlc3BvbnNlLlByb2plY3QuSVNfT05fSE9MRDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRVU2VySUQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFVTZXJJRCAmJiB0aGlzLnRhc2tDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFVTZXJJRCA9PT0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tDb25maWcuaXNQcm9qZWN0T3duZXIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5pc1Byb2plY3RDb21wbGV0ZWQgPSB0aGlzLnRhc2tDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMudGFza0NvbmZpZy5JU19PTl9IT0xEKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlzYWJsZUdyaWRWaWV3ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRElTQUJMRV9HUklEX1ZJRVddKTtcclxuXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvKiBjb25zdCBjdXJyZW50VVNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIGlmIChjdXJyZW50VVNlcklEICYmIHRoaXMudGFza0NvbmZpZy5zZWxlY3RlZFByb2plY3QgJiZcclxuICAgICAgICAgICAgY3VycmVudFVTZXJJRCA9PT0gdGhpcy50YXNrQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1BST0pFQ1RfT1dORVJbJ0lkZW50aXR5LWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrQ29uZmlnLmlzUHJvamVjdE93bmVyID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5pc1Byb2plY3RDb21wbGV0ZWQgPSB0aGlzLnRhc2tDb25maWcuaXNSZWFkT25seTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRhc2tDb25maWcuSVNfT05fSE9MRCk7XHJcbiAgICAgICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZUdyaWRWaWV3ID0gdGhpcy51dGlsU2VydmljZS5nZXRCb29sZWFuVmFsdWUodGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuRElTQUJMRV9HUklEX1ZJRVddKTtcclxuICAgICovIH1cclxufVxyXG4iXX0=