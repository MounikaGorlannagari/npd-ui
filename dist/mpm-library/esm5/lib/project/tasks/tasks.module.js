import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material.module';
import { MpmLibraryModule } from '../../mpm-library.module';
import { ItemCountComponent } from './item-count/item-count.component';
import { ItemSortComponent } from './item-sort/item-sort.component';
import { ResourceViewComponent } from './resource-view/resource-view.component';
import { SwimLaneComponent } from './swim-lane/swim-lane.component';
import { TaskGridComponent } from './task-grid/task-grid.component';
import { TaskListViewComponent } from './task-list-view/task-list-view.component';
import { TaskToolbarComponent } from './task-toolbar/task-toolbar.component';
import { CustomWorkflowFieldComponent } from './custom-workflow-field/custom-workflow-field.component';
var TasksModule = /** @class */ (function () {
    function TasksModule() {
    }
    TasksModule = __decorate([
        NgModule({
            declarations: [
                ItemCountComponent,
                ItemSortComponent,
                ResourceViewComponent,
                SwimLaneComponent,
                TaskGridComponent,
                TaskListViewComponent,
                TaskToolbarComponent,
                CustomWorkflowFieldComponent
            ],
            imports: [
                CommonModule,
                MaterialModule,
                MpmLibraryModule
            ],
            exports: [
                ItemCountComponent,
                ItemSortComponent,
                ResourceViewComponent,
                SwimLaneComponent,
                TaskGridComponent,
                TaskListViewComponent,
                TaskToolbarComponent,
                CustomWorkflowFieldComponent
            ]
        })
    ], TasksModule);
    return TasksModule;
}());
export { TasksModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFza3MubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy90YXNrcy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUU1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNoRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQThCdkc7SUFBQTtJQUEyQixDQUFDO0lBQWYsV0FBVztRQTVCdkIsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFO2dCQUNWLGtCQUFrQjtnQkFDbEIsaUJBQWlCO2dCQUNqQixxQkFBcUI7Z0JBQ3JCLGlCQUFpQjtnQkFDakIsaUJBQWlCO2dCQUNqQixxQkFBcUI7Z0JBQ3JCLG9CQUFvQjtnQkFDcEIsNEJBQTRCO2FBQy9CO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osY0FBYztnQkFDZCxnQkFBZ0I7YUFDbkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsa0JBQWtCO2dCQUNsQixpQkFBaUI7Z0JBQ2pCLHFCQUFxQjtnQkFDckIsaUJBQWlCO2dCQUNqQixpQkFBaUI7Z0JBQ2pCLHFCQUFxQjtnQkFDckIsb0JBQW9CO2dCQUNwQiw0QkFBNEI7YUFDL0I7U0FDSixDQUFDO09BRVcsV0FBVyxDQUFJO0lBQUQsa0JBQUM7Q0FBQSxBQUE1QixJQUE0QjtTQUFmLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uLy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IE1wbUxpYnJhcnlNb2R1bGUgfSBmcm9tICcuLi8uLi9tcG0tbGlicmFyeS5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgSXRlbUNvdW50Q29tcG9uZW50IH0gZnJvbSAnLi9pdGVtLWNvdW50L2l0ZW0tY291bnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSXRlbVNvcnRDb21wb25lbnQgfSBmcm9tICcuL2l0ZW0tc29ydC9pdGVtLXNvcnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVzb3VyY2VWaWV3Q29tcG9uZW50IH0gZnJvbSAnLi9yZXNvdXJjZS12aWV3L3Jlc291cmNlLXZpZXcuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3dpbUxhbmVDb21wb25lbnQgfSBmcm9tICcuL3N3aW0tbGFuZS9zd2ltLWxhbmUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFza0dyaWRDb21wb25lbnQgfSBmcm9tICcuL3Rhc2stZ3JpZC90YXNrLWdyaWQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVGFza0xpc3RWaWV3Q29tcG9uZW50IH0gZnJvbSAnLi90YXNrLWxpc3Qtdmlldy90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYXNrVG9vbGJhckNvbXBvbmVudCB9IGZyb20gJy4vdGFzay10b29sYmFyL3Rhc2stdG9vbGJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDdXN0b21Xb3JrZmxvd0ZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9jdXN0b20td29ya2Zsb3ctZmllbGQvY3VzdG9tLXdvcmtmbG93LWZpZWxkLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgSXRlbUNvdW50Q29tcG9uZW50LFxyXG4gICAgICAgIEl0ZW1Tb3J0Q29tcG9uZW50LFxyXG4gICAgICAgIFJlc291cmNlVmlld0NvbXBvbmVudCxcclxuICAgICAgICBTd2ltTGFuZUNvbXBvbmVudCxcclxuICAgICAgICBUYXNrR3JpZENvbXBvbmVudCxcclxuICAgICAgICBUYXNrTGlzdFZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgVGFza1Rvb2xiYXJDb21wb25lbnQsXHJcbiAgICAgICAgQ3VzdG9tV29ya2Zsb3dGaWVsZENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgTXBtTGlicmFyeU1vZHVsZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBJdGVtQ291bnRDb21wb25lbnQsXHJcbiAgICAgICAgSXRlbVNvcnRDb21wb25lbnQsXHJcbiAgICAgICAgUmVzb3VyY2VWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIFN3aW1MYW5lQ29tcG9uZW50LFxyXG4gICAgICAgIFRhc2tHcmlkQ29tcG9uZW50LFxyXG4gICAgICAgIFRhc2tMaXN0Vmlld0NvbXBvbmVudCxcclxuICAgICAgICBUYXNrVG9vbGJhckNvbXBvbmVudCxcclxuICAgICAgICBDdXN0b21Xb3JrZmxvd0ZpZWxkQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVGFza3NNb2R1bGUgeyB9XHJcbiJdfQ==