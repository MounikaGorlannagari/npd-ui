import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SharingService } from '../../../../../lib/mpm-utils/services/sharing.service';
import { SortConstants } from '../../../../../lib/shared/constants/sort.constants';
import { ApplicationConfigConstants } from '../../../../../lib/shared/constants/application.config.constants';
var DeliverableSortComponent = /** @class */ (function () {
    function DeliverableSortComponent(sharingService) {
        this.sharingService = sharingService;
        this.onSortChange = new EventEmitter();
    }
    DeliverableSortComponent.prototype.selectSorting = function () {
        this.selectedOption.sortType = (this.selectedOption.sortType === SortConstants.ASC) ? 'desc' : 'asc';
        this.selectedOption.sortType = this.selectedOption.sortType;
        this.onSortChange.next(this.selectedOption);
    };
    DeliverableSortComponent.prototype.onSelectionChange = function (event, fields) {
        var _this = this;
        if (event && event.currentTarget.value) {
            fields.map(function (option) {
                if (option.name === event.currentTarget.value) {
                    _this.selectedOption.option = option;
                }
            });
        }
        this.onSortChange.next(this.selectedOption);
    };
    DeliverableSortComponent.prototype.refreshData = function (taskConfig) {
        if (!this.selectedOption) {
            if (this.sortOptions) {
                // this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    };
    DeliverableSortComponent.prototype.ngOnInit = function () {
        this.appConfig = this.sharingService.getAppConfig();
        if (!this.selectedOption) {
            if (this.sortOptions) {
                // this.selectedOption = { option: this.sortOptions[0], sortType: 'asc' };
                this.selectedOption = { option: this.sortOptions[0], sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
            else {
                //this.selectedOption = { option: {}, sortType: 'asc' };
                this.selectedOption = { option: {}, sortType: this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_SORT_ORDER] === 'ASC' ? 'asc' : 'desc' };
            }
        }
    };
    DeliverableSortComponent.ctorParameters = function () { return [
        { type: SharingService }
    ]; };
    __decorate([
        Input()
    ], DeliverableSortComponent.prototype, "sortOptions", void 0);
    __decorate([
        Input()
    ], DeliverableSortComponent.prototype, "selectedOption", void 0);
    __decorate([
        Output()
    ], DeliverableSortComponent.prototype, "onSortChange", void 0);
    DeliverableSortComponent = __decorate([
        Component({
            selector: 'mpm-deliverable-sort',
            template: "<div class=\"flex-row-item justify-flex-end\" *ngIf=\"sortOptions && sortOptions.length > 0\">\r\n    <button class=\"sort-btn\" mat-stroked-button matTooltip=\"Sorted by {{selectedOption.option.displayName}}\"\r\n        [matMenuTriggerFor]=\"sortOrderListMenu\">\r\n        <span>Sort by: {{selectedOption.option.displayName}}</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n    </button>\r\n    <button class=\"dashboard-icon-btn sort-dir-icon\" (click)=\"selectSorting()\" matTooltip=\"Reverse sort direction\"\r\n        mat-icon-button>\r\n        <mat-icon *ngIf=\"selectedOption.sortType === 'desc'\">arrow_downward</mat-icon>\r\n        <mat-icon *ngIf=\"selectedOption.sortType === 'asc'\">arrow_upward</mat-icon>\r\n    </button>\r\n    <mat-menu #sortOrderListMenu=\"matMenu\">\r\n        <span *ngFor=\"let fieldOption of sortOptions\">\r\n            <button *ngIf=\"selectedOption.option.name !== fieldOption.name\" mat-menu-item\r\n                matTooltip=\"{{fieldOption.displayName}}\" (click)=\"onSelectionChange($event, sortOptions)\"\r\n                [value]=\"fieldOption.name\">\r\n                <span>{{fieldOption.displayName}}</span>\r\n            </button>\r\n        </span>\r\n    </mat-menu>\r\n</div>",
            styles: [".flex-row-item button{margin:5px}"]
        })
    ], DeliverableSortComponent);
    return DeliverableSortComponent;
}());
export { DeliverableSortComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLXNvcnQvZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUNuRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQVM5RztJQU9JLGtDQUNXLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUwvQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFNN0MsQ0FBQztJQUNMLGdEQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxLQUFLLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDckcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDNUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxvREFBaUIsR0FBakIsVUFBa0IsS0FBSyxFQUFFLE1BQU07UUFBL0IsaUJBU0M7UUFSRyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRTtZQUNwQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQUEsTUFBTTtnQkFDYixJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztpQkFDdkM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw4Q0FBVyxHQUFYLFVBQVksVUFBVTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ25CLDBFQUEwRTtnQkFDekUsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMzSztpQkFBTTtnQkFDSCx3REFBd0Q7Z0JBQ3hELElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUMsQ0FBQzthQUMxSjtTQUNKO0lBQ0wsQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFDRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNuQiwwRUFBMEU7Z0JBQzFFLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUs7aUJBQU07Z0JBQ0gsd0RBQXdEO2dCQUN4RCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFDLENBQUM7YUFDMUo7U0FDSjtJQUNMLENBQUM7O2dCQTFDMEIsY0FBYzs7SUFQaEM7UUFBUixLQUFLLEVBQUU7aUVBQWE7SUFDWjtRQUFSLEtBQUssRUFBRTtvRUFBZ0I7SUFDZDtRQUFULE1BQU0sRUFBRTtrRUFBd0M7SUFIeEMsd0JBQXdCO1FBTnBDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsNnVDQUFnRDs7U0FFbkQsQ0FBQztPQUVXLHdCQUF3QixDQW1EcEM7SUFBRCwrQkFBQztDQUFBLEFBbkRELElBbURDO1NBbkRZLHdCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFNvcnRDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvc2hhcmVkL2NvbnN0YW50cy9zb3J0LmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1kZWxpdmVyYWJsZS1zb3J0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kZWxpdmVyYWJsZS1zb3J0LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RlbGl2ZXJhYmxlLXNvcnQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIERlbGl2ZXJhYmxlU29ydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBASW5wdXQoKSBzb3J0T3B0aW9ucztcclxuICAgIEBJbnB1dCgpIHNlbGVjdGVkT3B0aW9uO1xyXG4gICAgQE91dHB1dCgpIG9uU29ydENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIGFwcENvbmZpZzogYW55O1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG4gICAgc2VsZWN0U29ydGluZygpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlID0gKHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPT09IFNvcnRDb25zdGFudHMuQVNDKSA/ICdkZXNjJyA6ICdhc2MnO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24uc29ydFR5cGUgPSB0aGlzLnNlbGVjdGVkT3B0aW9uLnNvcnRUeXBlO1xyXG4gICAgICAgIHRoaXMub25Tb3J0Q2hhbmdlLm5leHQodGhpcy5zZWxlY3RlZE9wdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgb25TZWxlY3Rpb25DaGFuZ2UoZXZlbnQsIGZpZWxkcykge1xyXG4gICAgICAgIGlmIChldmVudCAmJiBldmVudC5jdXJyZW50VGFyZ2V0LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGZpZWxkcy5tYXAob3B0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChvcHRpb24ubmFtZSA9PT0gZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24ub3B0aW9uID0gb3B0aW9uO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5vblNvcnRDaGFuZ2UubmV4dCh0aGlzLnNlbGVjdGVkT3B0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoRGF0YSh0YXNrQ29uZmlnKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnNlbGVjdGVkT3B0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNvcnRPcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgIC8vIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHRoaXMuc29ydE9wdGlvbnNbMF0sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHsgb3B0aW9uOiB7fSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogdGhpcy5hcHBDb25maWdbQXBwbGljYXRpb25Db25maWdDb25zdGFudHMuTVBNX0FQUF9DT05GSUcuREVGQVVMVF9TT1JUX09SREVSXSA9PT0gJ0FTQycgPyAnYXNjJyA6ICdkZXNjJ307XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICB0aGlzLmFwcENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwQ29uZmlnKCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLnNlbGVjdGVkT3B0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNvcnRPcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgIC8vIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6ICdhc2MnIH07XHJcbiAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjogdGhpcy5zb3J0T3B0aW9uc1swXSwgc29ydFR5cGU6IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkRFRkFVTFRfU09SVF9PUkRFUl0gPT09ICdBU0MnID8gJ2FzYycgOiAnZGVzYyd9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnNlbGVjdGVkT3B0aW9uID0geyBvcHRpb246IHt9LCBzb3J0VHlwZTogJ2FzYycgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSB7IG9wdGlvbjoge30sIHNvcnRUeXBlOiB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX1NPUlRfT1JERVJdID09PSAnQVNDJyA/ICdhc2MnIDogJ2Rlc2MnfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=