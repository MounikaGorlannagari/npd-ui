import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliverableToolbarComponent } from './deliverable-toolbar/deliverable-toolbar.component';
import { MaterialModule } from '../../../material.module';
import { DeliverableSwinLaneComponent } from './deliverable-swin-lane/deliverable-swin-lane.component';
import { DeliverableCardComponent } from './deliverable-card/deliverable-card.component';
import { DeliverableSortComponent } from './deliverable-sort/deliverable-sort.component';
import { MpmLibraryModule } from '../../../mpm-library.module';
import { BulkActionsComponent } from './bulk-actions/bulk-actions.component';
var DeliverableModule = /** @class */ (function () {
    function DeliverableModule() {
    }
    DeliverableModule = __decorate([
        NgModule({
            declarations: [
                DeliverableToolbarComponent,
                DeliverableSwinLaneComponent,
                DeliverableCardComponent,
                DeliverableSortComponent,
                BulkActionsComponent
            ],
            imports: [
                CommonModule,
                MaterialModule,
                MpmLibraryModule
            ],
            exports: [
                DeliverableToolbarComponent,
                DeliverableSwinLaneComponent,
                DeliverableCardComponent,
                DeliverableSortComponent,
                BulkActionsComponent
            ]
        })
    ], DeliverableModule);
    return DeliverableModule;
}());
export { DeliverableModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUN2RyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN6RixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQXdCN0U7SUFBQTtJQUFpQyxDQUFDO0lBQXJCLGlCQUFpQjtRQXRCN0IsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFO2dCQUNWLDJCQUEyQjtnQkFDM0IsNEJBQTRCO2dCQUM1Qix3QkFBd0I7Z0JBQ3hCLHdCQUF3QjtnQkFDeEIsb0JBQW9CO2FBQ3ZCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osY0FBYztnQkFDZCxnQkFBZ0I7YUFDbkI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsMkJBQTJCO2dCQUMzQiw0QkFBNEI7Z0JBQzVCLHdCQUF3QjtnQkFDeEIsd0JBQXdCO2dCQUN4QixvQkFBb0I7YUFDdkI7U0FDSixDQUFDO09BRVcsaUJBQWlCLENBQUk7SUFBRCx3QkFBQztDQUFBLEFBQWxDLElBQWtDO1NBQXJCLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlVG9vbGJhckNvbXBvbmVudCB9IGZyb20gJy4vZGVsaXZlcmFibGUtdG9vbGJhci9kZWxpdmVyYWJsZS10b29sYmFyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTd2luTGFuZUNvbXBvbmVudCB9IGZyb20gJy4vZGVsaXZlcmFibGUtc3dpbi1sYW5lL2RlbGl2ZXJhYmxlLXN3aW4tbGFuZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNhcmRDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLWNhcmQvZGVsaXZlcmFibGUtY2FyZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLXNvcnQvZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNcG1MaWJyYXJ5TW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLWxpYnJhcnkubW9kdWxlJztcclxuaW1wb3J0IHsgQnVsa0FjdGlvbnNDb21wb25lbnQgfSBmcm9tICcuL2J1bGstYWN0aW9ucy9idWxrLWFjdGlvbnMuY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBEZWxpdmVyYWJsZVRvb2xiYXJDb21wb25lbnQsXHJcbiAgICAgICAgRGVsaXZlcmFibGVTd2luTGFuZUNvbXBvbmVudCxcclxuICAgICAgICBEZWxpdmVyYWJsZUNhcmRDb21wb25lbnQsXHJcbiAgICAgICAgRGVsaXZlcmFibGVTb3J0Q29tcG9uZW50LFxyXG4gICAgICAgIEJ1bGtBY3Rpb25zQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgICAgICBNcG1MaWJyYXJ5TW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIERlbGl2ZXJhYmxlVG9vbGJhckNvbXBvbmVudCxcclxuICAgICAgICBEZWxpdmVyYWJsZVN3aW5MYW5lQ29tcG9uZW50LFxyXG4gICAgICAgIERlbGl2ZXJhYmxlQ2FyZENvbXBvbmVudCxcclxuICAgICAgICBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQsXHJcbiAgICAgICAgQnVsa0FjdGlvbnNDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEZWxpdmVyYWJsZU1vZHVsZSB7IH1cclxuIl19