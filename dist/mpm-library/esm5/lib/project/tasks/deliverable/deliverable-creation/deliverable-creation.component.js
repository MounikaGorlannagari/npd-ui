import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { DeliverableConstants } from '../deliverable.constants';
import { AppService } from '../../../../mpm-utils/services/app.service';
import { ProjectConstant } from '../../../project-overview/project.constants';
import { EntityAppDefService } from '../../../../mpm-utils/services/entity.appdef.service';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
import { AllocationTypes } from '../../../../mpm-utils/objects/AllocationType';
import { LoaderService } from '../../../../../lib/loader/loader.service';
import { UtilService } from '../../../../../lib/mpm-utils/services/util.service';
import * as acronui from '../../../../../lib/mpm-utils/auth/utility';
import { DeliverableService } from '../deliverable.service';
import { ConfirmationModalComponent } from '../../../../shared/components/confirmation-modal/confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { SaveOptionConstant } from '../../../../shared/constants/save-options.constants';
import { StatusService } from '../../../../shared/services/status.service';
import { StatusLevels } from '../../../../mpm-utils/objects/StatusType';
import { ProjectUtilService } from '../../../shared/services/project-util.service';
import { DeliverableTypes } from '../../../../../lib/mpm-utils/objects/DeliverableTypes';
import { CustomSearchFieldComponent } from '../../../../shared/components/custom-search-field/custom-search-field.component';
import { SharingService } from '../../../../../lib/mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../../../../lib/shared/constants/application.config.constants';
import { startWith, map } from 'rxjs/operators';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { newArray } from '@angular/compiler/src/util';
import { TaskTypes } from '../../../../mpm-utils/objects/TaskType';
import { NotificationService } from '../../../../notification/notification.service';
var DeliverableCreationComponent = /** @class */ (function () {
    function DeliverableCreationComponent(adapter, appService, utilService, entityAppdefService, deliverableService, loaderService, dialog, statusService, projectUtilService, sharingService, notificationService) {
        var _this = this;
        this.adapter = adapter;
        this.appService = appService;
        this.utilService = utilService;
        this.entityAppdefService = entityAppdefService;
        this.deliverableService = deliverableService;
        this.loaderService = loaderService;
        this.dialog = dialog;
        this.statusService = statusService;
        this.projectUtilService = projectUtilService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.closeCallbackHandler = new EventEmitter();
        this.saveCallBackHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.selectedOwner = null;
        this.selectedApprover = null;
        this.deliverableConstants = DeliverableConstants;
        this.saveOptions = JSON.parse(JSON.stringify(SaveOptionConstant.deliverableSaveOptions));
        this.SELECTED_SAVE_OPTION = {
            name: 'Save',
            value: 'SAVE'
        };
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.separatorKeysCodes = [ENTER, COMMA];
        this.isPM = false;
        this.newReviewers = [];
        this.removedReviewers = [];
        // reviewers: any[] = [];
        this.savedReviewers = [];
        this.allReviewers = [];
        // availableReviewers: any[];
        // reviewerDetails;
        this.reviewerStatusData = [];
        this.deliverableReviewers = [];
        this.PROJECT_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Project/operations';
        this.TASK_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Task/operations';
        this.DELIVERABLE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable/operations';
        this.DELIVERABLE_TYPE_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Deliverable_Type/operations';
        this.USER_TEAM_METHOD_NS = 'http://schemas.acheron-tech.com/mpm/teams/bpm/1.0';
        this.ROLE_TEAM_METHOD_NS = 'http://schemas/AcheronMPMTeams/Team_Role_Mapping/operations';
        this.ASSIGNMENT_TYPE_METHOD_NS = 'http://schemas/AcheronMPMCore/Assignment_Type/operations';
        this.DELIVERABLE_METHOD_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.GET_PROJECT_DETAILS_WS_METHOD_NAME = 'GetProjectByID';
        this.GET_TASK_DETAILS_WS_METHOD_NAME = 'ReadTask';
        this.GET_DELIVERABLE_DETAILS_WS_METHOD_NAME = 'ReadDeliverable';
        this.GET_USERS_BY_TEAM_WS_METHOD_NAME = 'GetTeamUserDetails';
        this.GET_DELIVERABLES_BY_PROJECT_WS_METHOD_NAME = 'GetAllDeliverablesByProject';
        this.GET_ASSIGNMENT_TYPE_WS_METHOD_NAME = 'GetAllAssignmentType';
        this.CREATE_DELIVERABLE_WS_METHOD_NAME = 'CreateDeliverable';
        this.UPDATE_DELIVERABLE_WS_METHOD_NAME = 'EditDeliverable';
        this.GET_ALL_DELIVERABLES_BY_NAME_WS_METHOD_NAME = 'GetAllDeliverablesByDeliverableName';
        this.GET_USERS_BY_TEAM_ROLE_WS_METHOD_NAME = 'GetTeamRoleUsers ';
        this.GET_DELIVERABLE_TYPE_BY_NAME_WS_METHOD_NAME = 'GetDeliverableTypeByName';
        this.dateFilter = function (date) {
            if (_this.enableWorkWeek) {
                return true;
            }
            else {
                var day = date.getDay();
                return day !== 0 && day !== 6;
                //0 means sunday
                //6 means saturday
            }
        };
    }
    DeliverableCreationComponent.prototype.filterReviewers = function (value) {
        if (value && typeof value === 'string' && value.trim() !== '') {
            var filterValue_1 = value.toLowerCase();
            return this.allReviewers.filter(function (reviewer) { return reviewer.value.toLowerCase().indexOf(filterValue_1) === 0; });
        }
    };
    DeliverableCreationComponent.prototype.selected = function (event) {
        var reviewerIndex = this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === event.option.value; });
        var reviewer = this.allReviewers.find(function (reviewerData) { return reviewerData.value === event.option.value; });
        if (this.deliverableForm.value.deliverableApprover &&
            event.option.value === this.deliverableForm.value.deliverableApprover.name) {
            this.notificationService.error(reviewer.displayName + ' is already the Approver');
        }
        else if (reviewerIndex > -1) {
            this.notificationService.error(reviewer.displayName + ' is already selected');
        }
        else {
            this.savedReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            this.newReviewers.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName
            });
            var removeReviewerIndex = this.removedReviewers.findIndex(function (removedReviewer) { return removedReviewer.value === event.option.value; });
            if (removeReviewerIndex >= 0) {
                this.removedReviewers.splice(removeReviewerIndex, 1);
            }
            this.reviewerStatusData.push({
                name: reviewer.displayName,
                value: reviewer.value,
                displayName: reviewer.displayName,
                isCompleted: false
            });
            this.reviewerInput.nativeElement.value = '';
            this.deliverableForm.controls.deliverableReviewers.setValue(null);
        }
    };
    DeliverableCreationComponent.prototype.addReviewer = function (event) {
        if (!this.matAutocomplete.isOpen) {
            var input = event.input;
            if (input) {
                input.value = '';
            }
            this.deliverableForm.controls.deliverableReviewers.setValue(null);
        }
    };
    DeliverableCreationComponent.prototype.onRemoveReviewer = function (reviewerValue) {
        var index = this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === reviewerValue; });
        var newReviewerIndex = this.newReviewers.findIndex(function (newReviewer) { return newReviewer.value === reviewerValue; });
        var reviewerIndex = this.reviewerStatusData.findIndex(function (reviewerData) { return reviewerData.value === reviewerValue; });
        var reviewer = this.allReviewers.find(function (reviewerData) { return reviewerData.value === reviewerValue; });
        this.removedReviewers.push({
            name: reviewer.displayName,
            value: reviewer.value,
            displayName: reviewer.displayName
        });
        if (index >= 0) {
            this.deliverableForm.markAsDirty();
            this.savedReviewers.splice(index, 1);
        }
        if (reviewerIndex >= 0) {
            this.reviewerStatusData.splice(reviewerIndex, 1);
        }
        if (newReviewerIndex >= 0) {
            this.newReviewers.splice(newReviewerIndex, 1);
        }
    };
    DeliverableCreationComponent.prototype.onChangeofSkipReviewValue = function (isNeedReviewValue) {
        if (!isNeedReviewValue) {
            this.deliverableForm.controls.approverAllocation.clearValidators();
            this.deliverableForm.controls.deliverableApprover.clearValidators();
            this.deliverableForm.controls.deliverableApproverRole.clearValidators();
            this.deliverableForm.controls.approverAllocation.patchValue('');
            this.deliverableForm.controls.deliverableApprover.patchValue('');
            this.deliverableForm.controls.deliverableApproverRole.patchValue('');
            this.deliverableForm.controls.approverAllocation.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
        }
        else {
            this.onChangeofApproverAllocationType(this.deliverableForm.getRawValue().approverAllocation, this.deliverableForm.getRawValue().deliverableApprover, false);
        }
    };
    DeliverableCreationComponent.prototype.onChangeofOwnerAllocationType = function (allocationTypeValue, ownerValue, changeDeliverableApprover) {
        var _this = this;
        var roles = [];
        var roleIds = [];
        this.teamOwnerRolesOptions.find(function (element) {
            roles.push(element);
            roleIds.push(element.name);
        });
        if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableOwner) {
                this.deliverableForm.controls.deliverableOwner.disable();
            }
            this.deliverableForm.controls.deliverableOwnerRole.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableOwner.clearValidators();
            this.deliverableForm.controls.deliverableOwner.updateValueAndValidity();
            this.deliverableForm.controls.deliverableOwnerRole.updateValueAndValidity();
            this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = roles;
            if (this.deliverableForm.getRawValue().deliverableOwnerRole) {
                this.getUsersByRole('OWNER').subscribe(function (response) {
                    _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = _this.teamRoleOwnerOptions;
                });
            }
            else {
                this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleOwnerOptions;
            }
            this.deliverableForm.controls.deliverableOwner.patchValue(ownerValue ? ownerValue : '');
        }
        else if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_USER) {
            this.deliverableForm.controls.deliverableOwnerRole.clearValidators();
            this.deliverableForm.controls.deliverableOwner.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableOwnerRole.updateValueAndValidity();
            this.deliverableForm.controls.deliverableOwner.updateValueAndValidity();
            this.getUsersByRole('OWNER').subscribe(function (response) {
                _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = _this.teamRoleOwnerOptions;
                _this.deliverableForm.controls.deliverableOwner.patchValue(ownerValue ? ownerValue : '');
            });
            if (this.deliverableForm.controls.deliverableOwner && !this.deliverableForm.controls.ownerAllocation.disabled) {
                this.deliverableForm.controls.deliverableOwner.enable();
            }
        }
    };
    DeliverableCreationComponent.prototype.onChangeofApproverAllocationType = function (allocationTypeValue, ownerValue, changeDeliverableApprover) {
        var _this = this;
        var roles = [];
        var roleIds = [];
        this.teamApproverRolesOptions.find(function (element) {
            roles.push(element);
            roleIds.push(element.name);
        });
        if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.disable();
            }
            this.deliverableForm.controls.deliverableApproverRole.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableApprover.clearValidators();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
            this.deliverableModalConfig.deliverableApproverRole.filterOptions = roles;
            if (this.deliverableForm.getRawValue().deliverableApproverRole) {
                this.getUsersByRole('APPROVER').subscribe(function (response) {
                    _this.deliverableModalConfig.deliverableApprover.filterOptions = _this.teamRoleApproverOptions;
                });
            }
            else {
                this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleApproverOptions;
            }
            if (changeDeliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.patchValue('');
            }
        }
        else if (allocationTypeValue === DeliverableConstants.ALLOCATION_TYPE_USER) {
            this.deliverableForm.controls.deliverableApprover.setValidators([Validators.required]);
            this.deliverableForm.controls.deliverableApproverRole.clearValidators();
            this.deliverableForm.controls.deliverableApproverRole.updateValueAndValidity();
            this.deliverableForm.controls.deliverableApprover.updateValueAndValidity();
            this.getUsersByRole('APPROVER').subscribe(function (response) {
                _this.deliverableModalConfig.deliverableApprover.filterOptions = _this.teamRoleApproverOptions;
                if (changeDeliverableApprover) {
                    _this.deliverableForm.controls.deliverableApprover.patchValue('');
                }
            });
            if (this.deliverableForm.controls.deliverableApprover && !this.deliverableForm.controls.approverAllocation.disabled) {
                this.deliverableForm.controls.deliverableApprover.enable();
            }
        }
    };
    DeliverableCreationComponent.prototype.onChangeofOwnerRole = function (ownerRole) {
        var _this = this;
        if (ownerRole && ownerRole.name) {
            if (this.deliverableForm.controls.deliverableOwner && !this.deliverableForm.controls.deliverableOwnerRole.disabled) {
                this.deliverableForm.controls.deliverableOwner.enable();
            }
            this.getUsersByRole('OWNER').subscribe(function (response) {
                _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = _this.teamRoleOwnerOptions;
                _this.deliverableForm.controls.deliverableOwner.patchValue('');
            });
        }
        else {
            if (this.deliverableForm.controls.deliverableOwner) {
                this.deliverableForm.controls.deliverableOwner.patchValue('');
                this.deliverableForm.controls.deliverableOwner.disable();
            }
        }
    };
    DeliverableCreationComponent.prototype.onChangeofApproverRole = function (approverRole) {
        var _this = this;
        if (approverRole && approverRole.name) {
            if (this.deliverableForm.controls.deliverableApprover && !this.deliverableForm.controls.deliverableApproverRole.disabled) {
                this.deliverableForm.controls.deliverableApprover.enable();
            }
            this.getUsersByRole('APPROVER').subscribe(function (response) {
                _this.deliverableModalConfig.deliverableApprover.filterOptions = _this.teamRoleApproverOptions;
                _this.deliverableForm.controls.deliverableApprover.patchValue('');
            });
        }
        else {
            if (this.deliverableForm.controls.deliverableApprover) {
                this.deliverableForm.controls.deliverableApprover.patchValue('');
                this.deliverableForm.controls.deliverableApprover.disable();
            }
        }
    };
    /* onDeliverableSelection() {
        const ownerRoles = [];
        const ownerRoleIds = [];
        const approverRoles = [];
        const approverRoleIds = [];
        this.teamOwnerRolesOptions.find(element => {
            if (element.value.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.value.search(RoleConstants.PROJECT_MEMBER) !== -1) {
                ownerRoles.push(element);
                ownerRoleIds.push(element.name);
            }
        });
        this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = ownerRoles;
        this.getUsersByRole('OWNER').subscribe(response => {
            this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = this.teamRoleUserOptions;
        });
        this.teamApproverRolesOptions.find(element => {
            if (element.value.search(RoleConstants.PROJECT_MANAGER) !== -1 || element.value.search(RoleConstants.PROJECT_APPROVER) !== -1) {
                approverRoles.push(element);
                approverRoleIds.push(element.name);
            }
        });
        this.deliverableModalConfig.deliverableApproverRole.filterOptions = approverRoles;
        this.getUsersByRole('APPROVER').subscribe(response => {
            this.deliverableModalConfig.deliverableApprover.filterOptions = this.teamRoleUserOptions;
        });
    } */
    DeliverableCreationComponent.prototype.updateDeliverable = function (saveOption) {
        var _this = this;
        if (this.deliverableForm.getRawValue().deliverableApprover && this.deliverableForm.getRawValue().deliverableApprover.name) {
            var data = this.savedReviewers.find(function (reviewer) { return reviewer.value === _this.deliverableForm.getRawValue().deliverableApprover.name; });
            if (data) {
                var eventData = { message: 'Selected reviewer is already the approver', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                return;
            }
        }
        if (this.deliverableForm.pristine) {
            var eventData = { message: 'Kindly make changes to update the deliverable', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        var nameSplit = this.deliverableForm.getRawValue().deliverableName.split('_');
        if (nameSplit[0].length === 0) {
            var eventData = { message: 'Name should not start with "_"', type: ProjectConstant.NOTIFICATION_LABELS.WARN };
            this.notificationHandler.next(eventData);
            return;
        }
        if (this.deliverableForm.status === 'VALID') {
            if (this.deliverableForm.controls.deliverableName.value.trim() === '') {
                var eventData = { message: 'Please provide a valid deliverable name', type: ProjectConstant.NOTIFICATION_LABELS.INFO };
                this.notificationHandler.next(eventData);
            }
            else {
                this.updateDeliverableDetails(saveOption);
            }
        }
        else {
            var eventData = {
                message: 'Kindly fill all mandatory fields',
                type: ProjectConstant.NOTIFICATION_LABELS.INFO
            };
            this.notificationHandler.next(eventData);
        }
    };
    DeliverableCreationComponent.prototype.getAssignementTypeId = function (typeName) {
        var assignementId = null;
        this.deliverableModalConfig.deliverableOwnerAssignmentType.map(function (type) {
            if (type.NAME === typeName) {
                assignementId = type['Assignment_Type-id'].Id;
            }
        });
        return assignementId;
    };
    DeliverableCreationComponent.prototype.getUpdatedDeliverableDetails = function () {
        var _a, _b, _c, _d;
        var deliverableObject;
        var deliverableFormValues = this.deliverableForm.getRawValue();
        deliverableObject = {
            DeliverableId: {
                Id: this.deliverableModalConfig.isNewDeliverable ? null : this.deliverableModalConfig.deliverableId,
            },
            DeliverableName: deliverableFormValues.deliverableName.trim() ? deliverableFormValues.deliverableName.trim().replace(/\s+/g, ' ') : deliverableFormValues.deliverableName.trim(),
            Description: deliverableFormValues.description,
            StartDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(this.deliverableModalConfig.selectedTask.START_DATE) ? '' :
                this.deliverableModalConfig.selectedTask.START_DATE),
            EndDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(deliverableFormValues.dueDate) ? '' :
                deliverableFormValues.dueDate),
            DueDate: this.deliverableModalConfig.isTemplate ? '' : (this.utilService.isNullOrEmpty(deliverableFormValues.dueDate) ? '' : deliverableFormValues.dueDate),
            IsActive: true,
            IsDeleted: false,
            DeliverableType: deliverableFormValues.needReview ? 'UPLOAD_REVIEW' : 'UPLOAD',
            OwnerAssignmentType: deliverableFormValues.ownerAllocation,
            ApproverAssignmentType: deliverableFormValues.approverAllocation,
            ExpectedDuration: deliverableFormValues.expectedDuration,
            RPOStatus: {
                MPMStatusID: {
                    Id: deliverableFormValues.status
                }
            },
            RPOPriority: {
                MPMPriorityID: {
                    Id: deliverableFormValues.priority
                }
            },
            RPOTask: {
                TaskID: {
                    Id: this.deliverableModalConfig.selectedTask['Task-id'].Id
                }
            },
            RPOProject: {
                ProjectID: {
                    Id: this.deliverableModalConfig.selectedProject['Project-id'].Id
                }
            },
            RPOOwnerRoleID: {
                MPMTeamRoleMappingID: {
                    Id: (_a = deliverableFormValues.deliverableOwnerRole) === null || _a === void 0 ? void 0 : _a.name
                }
            },
            RPOApproverRoleID: {
                MPMTeamRoleMappingID: {
                    Id: (_b = deliverableFormValues.deliverableApproverRole) === null || _b === void 0 ? void 0 : _b.name
                }
            },
            RPODeliverableOwnerID: {
                IdentityID: {
                    userID: (_c = deliverableFormValues.deliverableOwner) === null || _c === void 0 ? void 0 : _c.name
                }
            },
            RPODeliverableApproverID: {
                IdentityID: {
                    userID: (_d = deliverableFormValues.deliverableApprover) === null || _d === void 0 ? void 0 : _d.name
                }
            }
        };
        return deliverableObject;
    };
    DeliverableCreationComponent.prototype.formReviewerObj = function () {
        var _this = this;
        var addedReviewers = [];
        var removedReviewers = [];
        if (this.newReviewers && this.deliverableReviewers) {
            this.newReviewers.forEach(function (newReviewer) {
                if (_this.deliverableReviewers.length === 0) {
                    var addedReviewerIndex = addedReviewers.findIndex(function (reviewer) { return reviewer.value === newReviewer.value; });
                    if (addedReviewerIndex === -1) {
                        addedReviewers.push(newReviewer);
                    }
                }
                else {
                    var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (deliverableReviewer) { return deliverableReviewer.value === newReviewer.value; });
                    if (deliverableReviewerIndex === -1) {
                        var addedReviewerIndex = addedReviewers.findIndex(function (reviewer) { return reviewer.value === newReviewer.value; });
                        if (addedReviewerIndex === -1) {
                            addedReviewers.push(newReviewer);
                        }
                    }
                }
            });
            this.newreviewersObj = addedReviewers.map(function (addedReviewer) {
                return {
                    DeliverableId: _this.deliverableConfig.deliverableId,
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: addedReviewer.value,
                        action: 'ADD'
                    },
                    TaskId: _this.deliverableModalConfig.taskId,
                    IsPMAssignedReviewer: _this.isPM
                };
            });
        }
        if (this.removedReviewers && this.savedReviewers) {
            this.removedReviewers.forEach(function (removedReviewer) {
                if (_this.savedReviewers.length === 0) {
                    var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (deliverableReviewer) { return deliverableReviewer.value === removedReviewer.value; });
                    if (deliverableReviewerIndex >= 0) {
                        var reviewerIndex = removedReviewers.findIndex(function (reviewer) { return reviewer.value === removedReviewer.value; });
                        if (reviewerIndex === -1) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
                else {
                    var savedReviewerIndex = _this.savedReviewers.findIndex(function (savedReviewer) { return savedReviewer.value === removedReviewer.value; });
                    if (savedReviewerIndex === -1) {
                        var removedReviewerIndex = removedReviewers.findIndex(function (reviewer) { return reviewer.value === removedReviewer.value; });
                        var deliverableReviewerIndex = _this.deliverableReviewers.findIndex(function (data) { return data.value === removedReviewer.value; });
                        if (removedReviewerIndex === -1 && deliverableReviewerIndex >= 0) {
                            removedReviewers.push(removedReviewer);
                        }
                    }
                }
            });
            this.removedreviewersObj = removedReviewers.map(function (reviewer) {
                return {
                    DeliverableId: _this.deliverableConfig.deliverableId,
                    Reviewer: {
                        Id: '',
                        ItemId: '',
                        userId: reviewer.value,
                        action: 'REMOVE'
                    },
                    TaskId: _this.deliverableModalConfig.taskId,
                    IsPMAssignedReviewer: _this.isPM
                };
            });
        }
        this.reviewersObj = this.newreviewersObj ? this.newreviewersObj.concat(this.removedreviewersObj) : this.removedreviewersObj.concat(this.newreviewersObj);
    };
    DeliverableCreationComponent.prototype.updateDeliverableDetails = function (saveOption) {
        var deliverableObject = this.getUpdatedDeliverableDetails();
        this.formReviewerObj();
        this.saveDeliverable(deliverableObject, saveOption);
    };
    DeliverableCreationComponent.prototype.saveDeliverable = function (deliverableObject, saveOption) {
        var _this = this;
        this.loaderService.show();
        if (this.deliverableModalConfig.selectedDeliverable) {
            deliverableObject = this.projectUtilService.compareTwoProjectDetails(deliverableObject, this.oldFormValue);
            if (Object.keys(deliverableObject).length > 1 || (JSON.stringify(this.deliverableReviewers) !== JSON.stringify(this.savedReviewers))) {
                deliverableObject.BulkOperation = false;
                var requestObj = {
                    DeliverableObject: deliverableObject,
                    DeliverableReviewers: {
                        DeliverableReviewer: this.reviewersObj
                    }
                };
                this.appService.invokeRequest(this.DELIVERABLE_METHOD_NS, this.UPDATE_DELIVERABLE_WS_METHOD_NAME, requestObj)
                    .subscribe(function (response) {
                    _this.loaderService.hide();
                    if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                        && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                        var eventData = { message: ' Deliverable updated successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                        _this.notificationHandler.next(eventData);
                        _this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: _this.deliverableModalConfig.taskId,
                            projectId: _this.deliverableModalConfig.projectId,
                            isTemplate: _this.deliverableModalConfig.isTemplate ? true : false,
                            saveOption: saveOption
                        });
                    }
                    else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                        && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                        var eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                        _this.notificationHandler.next(eventData);
                    }
                    else {
                        _this.loaderService.hide();
                        var eventData = {
                            message: 'Something went wrong on while updating deliverable',
                            type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                        };
                        _this.notificationHandler.next(eventData);
                    }
                }, function (error) {
                    _this.loaderService.hide();
                    var eventData = { message: 'Something went wrong on while updating deliverable', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                });
            }
            else {
                this.loaderService.hide();
            }
        }
        else {
            var request = {
                DeliverableObject: deliverableObject
            };
            this.appService.invokeRequest(this.DELIVERABLE_METHOD_NS, this.CREATE_DELIVERABLE_WS_METHOD_NAME, request)
                .subscribe(function (response) {
                _this.loaderService.hide();
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data
                    && response.APIResponse.data.Deliverable && response.APIResponse.data.Deliverable['Deliverable-id'] && response.APIResponse.data.Deliverable['Deliverable-id'].Id) {
                    var eventData = { message: 'Deliverable saved successfully', type: ProjectConstant.NOTIFICATION_LABELS.SUCCESS };
                    _this.notificationHandler.next(eventData);
                    if (_this.deliverableModalConfig.isTemplate) {
                        _this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: _this.deliverableModalConfig.taskId,
                            projectId: _this.deliverableModalConfig.projectId,
                            isTemplate: true,
                            saveOption: saveOption
                        });
                    }
                    else {
                        _this.saveCallBackHandler.next({
                            deliverableId: response.APIResponse.data.Deliverable['Deliverable-id'].Id,
                            taskId: _this.deliverableModalConfig.taskId,
                            projectId: _this.deliverableModalConfig.projectId,
                            isTemplate: false,
                            saveOption: saveOption
                        });
                    }
                }
                else if (response && response.APIResponse && response.APIResponse.statusCode === '500' && response.APIResponse.error
                    && response.APIResponse.error.errorCode && response.APIResponse.error.errorMessage) {
                    var eventData = { message: response.APIResponse.error.errorMessage, type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                }
                else {
                    _this.loaderService.hide();
                    var eventData = {
                        message: 'Something went wrong on while creating deliverable',
                        type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                    };
                    _this.notificationHandler.next(eventData);
                }
            }, function (error) {
                _this.loaderService.hide();
                var eventData = {
                    message: 'Something went wrong on while creating deliverable',
                    type: ProjectConstant.NOTIFICATION_LABELS.ERROR
                };
                _this.notificationHandler.next(eventData);
            });
        }
    };
    /* formUserValue(user) {
        const parameters = {
            userId: user
        };
        this.appService.getPersonByIdentityUserId(parameters).subscribe(response => {
            const selectedUser = this.teamRoleUserOptions.find(element => element.name === response.userCN);
            if (selectedUser) {
                this.selectedOwner =  selectedUser;
            }
        });
    } */
    DeliverableCreationComponent.prototype.formRoleValue = function (role, isReviewDeliverable) {
        if (role) {
            if (isReviewDeliverable) {
                var selectedRole = this.teamApproverRolesOptions.find(function (element) { return element.name === role; });
                return (selectedRole) ? selectedRole : '';
            }
            else {
                var selectedRole = this.teamOwnerRolesOptions.find(function (element) { return element.name === role; });
                return (selectedRole) ? selectedRole : '';
            }
        }
        else {
            return '';
        }
    };
    DeliverableCreationComponent.prototype.formOwnerValue = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            if (userId) {
                var parameters = {
                    userId: userId
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (response) {
                    var selectedUser = _this.teamRoleOwnerOptions.find(function (element) { return element.name === response.userCN; });
                    _this.selectedOwner = selectedUser;
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                observer.next(true);
                observer.complete();
            }
        });
    };
    DeliverableCreationComponent.prototype.formApproverValue = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            if (userId) {
                var parameters = {
                    userId: userId
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (response) {
                    var selectedUser = _this.teamRoleApproverOptions.find(function (element) { return element.name === response.userCN; });
                    _this.selectedApprover = selectedUser;
                    observer.next(true);
                    observer.complete();
                });
            }
            else {
                observer.next(true);
                observer.complete();
            }
        });
    };
    DeliverableCreationComponent.prototype.formOwnerRoleValue = function (deliverable) {
        if (deliverable.R_PO_OWNER_ROLE_ID && deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'] && typeof deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id !== 'object') {
            var selectedRole = this.teamOwnerRolesOptions.find(function (element) { return element.name === deliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id; });
            return (selectedRole) ? selectedRole : '';
        }
        return '';
    };
    DeliverableCreationComponent.prototype.formApproverRoleValue = function (deliverable) {
        if (deliverable && deliverable.R_PO_APPROVER_ROLE_ID && typeof deliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id !== 'object') {
            var selectedRole = this.teamApproverRolesOptions.find(function (element) { return element.name === deliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id; });
            return (selectedRole) ? selectedRole : '';
        }
        return '';
    };
    DeliverableCreationComponent.prototype.formOwnerAllocationValue = function (deliverable) {
        if (deliverable) {
            return (deliverable.OWNER_ASSIGNMENT_TYPE) ? deliverable.OWNER_ASSIGNMENT_TYPE : '';
        }
        return '';
    };
    DeliverableCreationComponent.prototype.formApproverAllocationValue = function (deliverable) {
        if (deliverable) {
            return (deliverable.APPROVER_ASSIGNMENT_TYPE) ? deliverable.APPROVER_ASSIGNMENT_TYPE : '';
        }
        return '';
    };
    DeliverableCreationComponent.prototype.initialiseDeliverableForm = function () {
        var _this = this;
        var isTemplate = this.deliverableModalConfig.isTemplate;
        var isNewDeliverable = this.deliverableModalConfig.isNewDeliverable;
        var numericeVlalue = this.getMaxMinValueForNumber(0);
        var disableField = false;
        var selectedDeliverable = this.deliverableModalConfig.selectedDeliverable;
        this.showReviewer = (this.deliverableModalConfig && this.deliverableModalConfig.selectedDeliverable &&
            this.deliverableModalConfig.selectedDeliverable.DELIVERABLE_TYPE !== DeliverableTypes.UPLOAD) ? true : false;
        this.deliverableModalConfig.minDate = this.deliverableModalConfig.selectedTask.START_DATE;
        this.deliverableModalConfig.maxDate = this.deliverableModalConfig.selectedTask.DUE_DATE;
        var pattern = '^[a-zA-Z0-9 _().-]*$';
        if (this.deliverableConfig.title) {
            this.modalLabel = this.deliverableConfig.title;
        }
        if (!isTemplate) {
            if (isNewDeliverable) {
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: '', disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    priority: new FormControl({
                        value: this.deliverableModalConfig.deliverablePriorityList && this.deliverableModalConfig.deliverablePriorityList[0] &&
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'] ?
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    status: new FormControl({
                        value: this.deliverableModalConfig.deliverableStatusList && this.deliverableModalConfig.deliverableStatusList[0] &&
                            this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'] ? this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    dueDate: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.DUE_DATE, disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({ value: false, disabled: !this.enableNeedReview }),
                    approverAllocation: new FormControl({
                        value: '', disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE, disabled: disableField
                    }),
                    deliverableOwner: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID ?
                            this.selectedOwner : '', disabled: disableField
                    }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableOwnerRole: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE === DeliverableConstants.ALLOCATION_TYPE_ROLE ?
                            this.formRoleValue(this.deliverableModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id, false) : '', disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: '', disabled: disableField }),
                    expectedDuration: new FormControl('', [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
            else {
                disableField = this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.FINAL || !this.isUploaded ||
                    (selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW) ? true : disableField;
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: selectedDeliverable.NAME,
                        disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    deliverableOwner: new FormControl({
                        value: this.selectedOwner,
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableOwnerRole: new FormControl({
                        value: this.formOwnerRoleValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableApprover: new FormControl({
                        value: this.selectedApprover,
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableApproverRole: new FormControl({
                        value: this.formApproverRoleValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    status: new FormControl({
                        value: selectedDeliverable.R_PO_STATUS && selectedDeliverable.R_PO_STATUS['MPM_Status-id'] ? selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    priority: new FormControl({
                        value: selectedDeliverable.R_PO_PRIORITY && selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'] ? selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'].Id : '',
                        disabled: disableField
                    }, [Validators.required]),
                    dueDate: new FormControl({
                        value: selectedDeliverable.DUE_DATE,
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({
                        value: typeof selectedDeliverable.DESCRIPTION === 'string' ?
                            selectedDeliverable.DESCRIPTION : '',
                        disabled: disableField
                    }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({
                        value: this.utilService.isNullOrEmpty(selectedDeliverable.APPROVER_ASSIGNMENT_TYPE) ? false : true,
                        disabled: true
                    }),
                    // !disableField ? selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW ? disableField : true : disableField
                    approverAllocation: new FormControl({
                        value: this.formApproverAllocationValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    ownerAllocation: new FormControl({
                        value: this.formOwnerAllocationValue(selectedDeliverable),
                        disabled: disableField || this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl(this.utilService.isNullOrEmpty(selectedDeliverable.EXPECTED_DURATION) ? '' : selectedDeliverable.EXPECTED_DURATION, [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
                // this.deliverableForm.controls.needReview.disable();
                // this.onDeliverableSelection();
            }
        }
        else {
            if (isNewDeliverable) {
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: '', disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    priority: new FormControl({
                        value: this.deliverableModalConfig.deliverablePriorityList && this.deliverableModalConfig.deliverablePriorityList[0] &&
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'] ?
                            this.deliverableModalConfig.deliverablePriorityList[0]['MPM_Priority-id'].Id : '', disabled: disableField
                    }, [Validators.required]),
                    status: new FormControl({
                        value: this.deliverableModalConfig.deliverableStatusList && this.deliverableModalConfig.deliverableStatusList[0] &&
                            this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'] ? this.deliverableModalConfig.deliverableStatusList[0]['MPM_Status-id'].Id : '', disabled: true
                    }),
                    description: new FormControl({ value: '', disabled: disableField }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({ value: false, disabled: !this.enableNeedReview }),
                    approverAllocation: new FormControl({
                        value: '', disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE, disabled: disableField
                    }),
                    deliverableOwner: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID ?
                            this.selectedOwner : '', disabled: disableField
                    }),
                    deliverableApprover: new FormControl({ value: '', disabled: disableField }),
                    deliverableOwnerRole: new FormControl({
                        value: this.deliverableModalConfig.selectedTask.ASSIGNMENT_TYPE === DeliverableConstants.ALLOCATION_TYPE_ROLE ?
                            this.formRoleValue(this.deliverableModalConfig.selectedTask.R_PO_TEAM_ROLE['MPM_Team_Role_Mapping-id'].Id, false) : '', disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({ value: '', disabled: disableField }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl('', [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
            else {
                disableField = this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE ||
                    (selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW) ? true : disableField;
                this.deliverableForm = new FormGroup({
                    deliverableName: new FormControl({
                        value: selectedDeliverable.NAME,
                        disabled: disableField
                    }, [Validators.required, Validators.maxLength(120), Validators.pattern(this.nameStringPattern)]),
                    deliverableOwner: new FormControl({
                        value: this.selectedOwner,
                        disabled: disableField
                    }),
                    deliverableOwnerRole: new FormControl({
                        value: this.formOwnerRoleValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    deliverableApprover: new FormControl({
                        value: this.selectedApprover,
                        disabled: disableField
                    }),
                    deliverableApproverRole: new FormControl({
                        value: this.formApproverRoleValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    status: new FormControl({
                        value: selectedDeliverable.R_PO_STATUS && selectedDeliverable.R_PO_STATUS['MPM_Status-id'] ? selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id : '',
                        disabled: true
                    }),
                    priority: new FormControl({
                        value: selectedDeliverable.R_PO_PRIORITY && selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'] ? selectedDeliverable.R_PO_PRIORITY['MPM_Priority-id'].Id : '',
                        disabled: disableField
                    }, [Validators.required]),
                    description: new FormControl({
                        value: typeof selectedDeliverable.DESCRIPTION === 'string' ?
                            selectedDeliverable.DESCRIPTION : '',
                        disabled: disableField
                    }, [Validators.maxLength(1000)]),
                    needReview: new FormControl({
                        value: this.utilService.isNullOrEmpty(selectedDeliverable.APPROVER_ASSIGNMENT_TYPE) ? false : true,
                        disabled: true
                    }),
                    // !disableField ? selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW ? disableField : true : disableField
                    approverAllocation: new FormControl({
                        value: this.formApproverAllocationValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    ownerAllocation: new FormControl({
                        value: this.formOwnerAllocationValue(selectedDeliverable),
                        disabled: disableField
                    }),
                    deliverableReviewers: new FormControl({ value: this.savedReviewers, disabled: disableField }),
                    expectedDuration: new FormControl(this.utilService.isNullOrEmpty(selectedDeliverable.EXPECTED_DURATION) ? '' : selectedDeliverable.EXPECTED_DURATION, [Validators.max(numericeVlalue), Validators.min(-(numericeVlalue))])
                });
            }
        }
        this.filteredReviewers = this.deliverableForm.get('deliverableReviewers').valueChanges.pipe(startWith(''), map(function (reviewer) { return reviewer ? _this.filterReviewers(reviewer) : _this.allReviewers.slice(); }));
        this.deliverableModalConfig.deliverableOwnerConfig.formControl = this.deliverableForm.controls.deliverableOwner;
        this.deliverableModalConfig.deliverableApprover.formControl = this.deliverableForm.controls.deliverableApprover;
        this.deliverableModalConfig.deliverableOwnerRoleConfig.formControl = this.deliverableForm.controls.deliverableOwnerRole;
        this.deliverableModalConfig.deliverableApproverRole.formControl = this.deliverableForm.controls.deliverableApproverRole;
        if (this.deliverableForm.controls.ownerAllocation.value === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableOwner && this.deliverableForm.controls.deliverableOwnerRole.value === '') {
                this.deliverableForm.controls.deliverableOwner.disable();
            }
            if (this.deliverableForm.controls.deliverableOwnerRole.value !== '' && isNewDeliverable) {
                this.getUsersByRole('OWNER').subscribe(function (response) {
                    _this.deliverableModalConfig.deliverableOwnerConfig.formControl.updateValueAndValidity();
                    _this.deliverableForm.get('deliverableOwner').updateValueAndValidity();
                });
            }
        }
        if (this.deliverableForm.controls.approverAllocation.value === DeliverableConstants.ALLOCATION_TYPE_ROLE) {
            if (this.deliverableForm.controls.deliverableApprover && this.deliverableForm.controls.deliverableApproverRole.value === '') {
                this.deliverableForm.controls.deliverableApprover.disable();
            }
            if (this.deliverableForm.controls.deliverableApproverRole.value !== '' && isNewDeliverable) {
                this.getUsersByRole('APPROVER').subscribe(function (response) {
                    _this.deliverableModalConfig.deliverableApprover.formControl.updateValueAndValidity();
                    _this.deliverableForm.get('deliverableApprover').updateValueAndValidity();
                });
            }
        }
        this.deliverableForm.controls.needReview.valueChanges.subscribe(function (isNeedReviewValue) {
            _this.onChangeofSkipReviewValue(isNeedReviewValue);
        });
        // this.onChangeofOwnerAllocationType(this.deliverableForm.value.ownerAllocation, this.deliverableForm.value.deliverableOwner, false);
        this.deliverableForm.controls.ownerAllocation.valueChanges.subscribe(function (allocationTypeValue) {
            if (_this.deliverableForm.controls.deliverableOwnerRole) {
                _this.deliverableForm.controls.deliverableOwnerRole.setValue('');
            }
            if (_this.deliverableForm.controls.deliverableOwner) {
                _this.deliverableForm.controls.deliverableOwner.setValue('');
            }
            _this.onChangeofOwnerAllocationType(allocationTypeValue, null, true);
        });
        // this.onChangeofApproverAllocationType(this.deliverableForm.value.approverAllocation, this.deliverableForm.value.deliverableApprover, false);
        this.deliverableForm.controls.approverAllocation.valueChanges.subscribe(function (allocationTypeValue) {
            if (_this.deliverableForm.controls.deliverableApproverRole) {
                _this.deliverableForm.controls.deliverableApproverRole.setValue('');
            }
            if (_this.deliverableForm.controls.deliverableApprover) {
                _this.deliverableForm.controls.deliverableApprover.setValue('');
            }
            if (_this.deliverableForm.controls.deliverableReviewers) {
                _this.deliverableForm.controls.deliverableReviewers.setValue('');
            }
            _this.onChangeofApproverAllocationType(allocationTypeValue, null, true);
        });
        this.deliverableForm.controls.deliverableOwnerRole.valueChanges.subscribe(function (ownerRole) {
            if (ownerRole && typeof ownerRole === 'object') {
                if (!(selectedDeliverable && selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW)) {
                    _this.onChangeofOwnerRole(ownerRole);
                }
            }
            else {
                var selectedOwnerRole = _this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions.find(function (role) { return role.displayName === ownerRole; });
                if (selectedOwnerRole) {
                    _this.deliverableForm.controls.deliverableOwnerRole.setValue(selectedOwnerRole);
                }
            }
        });
        this.deliverableForm.controls.deliverableApproverRole.valueChanges.subscribe(function (approverRole) {
            if (approverRole && typeof approverRole === 'object') {
                if (!(selectedDeliverable && selectedDeliverable.DELIVERABLE_TYPE === DeliverableTypes.REVIEW)) {
                    _this.onChangeofApproverRole(approverRole);
                }
            }
            else {
                var selectedApproverRole = _this.deliverableModalConfig.deliverableApproverRole.filterOptions.find(function (role) { return role.displayName === approverRole; });
                if (selectedApproverRole) {
                    _this.deliverableForm.controls.deliverableApproverRole.setValue(selectedApproverRole);
                }
            }
        });
        if (this.deliverableForm.controls.deliverableOwner) {
            this.deliverableForm.controls.deliverableOwner.valueChanges.subscribe(function (ownerValue) {
                if (!(ownerValue && typeof ownerValue === 'object')) {
                    var selectedOwner = _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions.find(function (owner) { return owner.displayName === ownerValue; });
                    if (selectedOwner) {
                        _this.deliverableForm.controls.deliverableOwner.setValue(selectedOwner);
                    }
                }
            });
        }
        if (this.deliverableForm.controls.deliverableApprover) {
            this.deliverableForm.controls.deliverableApprover.valueChanges.subscribe(function (approverValue) {
                if (!(approverValue && typeof approverValue === 'object')) {
                    var selectedApprover = _this.deliverableModalConfig.deliverableApprover.filterOptions.find(function (approver) { return approver.displayName === approverValue; });
                    if (selectedApprover) {
                        _this.deliverableForm.controls.deliverableApprover.setValue(selectedApprover);
                    }
                }
            });
        }
        this.oldFormValue = this.getUpdatedDeliverableDetails();
    };
    DeliverableCreationComponent.prototype.getProjectDetails = function () {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                ProjectID: _this.deliverableModalConfig.projectId
            };
            _this.loaderService.show();
            _this.appService.invokeRequest(_this.PROJECT_DETAILS_METHOD_NS, _this.GET_PROJECT_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.Project) {
                    var projectObj = response.Project;
                    if (projectObj.R_PO_CATEGORY && projectObj.R_PO_CATEGORY['MPM_Category-id']) {
                        _this.deliverableModalConfig.categoryId = projectObj.R_PO_CATEGORY['MPM_Category-id'].Id;
                    }
                    if (projectObj.R_PO_TEAM && projectObj.R_PO_TEAM['MPM_Teams-id']) {
                        _this.deliverableModalConfig.teamId = projectObj.R_PO_TEAM['MPM_Teams-id'].Id;
                    }
                    _this.deliverableModalConfig.selectedProject = response.Project;
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.error(error);
                var eventData = { message: 'Something went wrong while fetching project details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                _this.loaderService.hide();
            });
        });
    };
    DeliverableCreationComponent.prototype.getTaskDetails = function () {
        var _this = this;
        return new Observable(function (observer) {
            var getRequestObject = {
                'Task-id': {
                    Id: _this.deliverableModalConfig.taskId
                }
            };
            _this.loaderService.show();
            _this.appService.invokeRequest(_this.TASK_DETAILS_METHOD_NS, _this.GET_TASK_DETAILS_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (response) {
                if (response && response.Task) {
                    var taskObj = response.Task;
                    _this.deliverableModalConfig.selectedTask = response.Task;
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.error(error);
                var eventData = { message: 'Something went wrong while fetching task details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                _this.loaderService.hide();
            });
        });
    };
    DeliverableCreationComponent.prototype.getDeliverableDetails = function () {
        var _this = this;
        this.loaderService.show();
        return new Observable(function (observer) {
            _this.deliverableService.readDeliverable(_this.deliverableModalConfig.deliverableId)
                .subscribe(function (response) {
                if (response && response.Deliverable) {
                    _this.deliverableModalConfig.selectedDeliverable = response.Deliverable;
                    _this.deliverableModalConfig.currStatus = _this.deliverableModalConfig.selectedDeliverable.R_PO_STATUS ?
                        _this.statusService.getStatusByStatusId(_this.deliverableModalConfig.selectedDeliverable.R_PO_STATUS['MPM_Status-id'].Id) : null;
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.error(error);
                var eventData = { message: 'Something went wrong while fetching deliverable details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                _this.loaderService.hide();
            });
        });
    };
    DeliverableCreationComponent.prototype.getPriorities = function () {
        var _this = this;
        return new Observable(function (observer) {
            /* this.entityAppdefService.getPriorities(MPM_LEVELS.DELIVERABLE)
                .subscribe(priorityResponse => {
                    if (priorityResponse) {
                        if (!Array.isArray(priorityResponse)) {
                            priorityResponse = [priorityResponse];
                        }
                        this.deliverableModalConfig.deliverablePriorityList = priorityResponse;
                    } else {
                        this.deliverableModalConfig.deliverablePriorityList = [];
                    }
                    observer.next(true);
                    observer.complete();
                }, error => {
                    const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                }); */
            var priority = _this.entityAppdefService.getPriorities(MPM_LEVELS.DELIVERABLE);
            // .subscribe(priorityResponse => {
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                _this.deliverableModalConfig.deliverablePriorityList = priority;
            }
            else {
                _this.deliverableModalConfig.deliverablePriorityList = [];
            }
            observer.next(true);
            observer.complete();
            /* }, error => {
                const eventData = { message: 'Something went wrong while fetching Priorities', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            }); */
        });
    };
    DeliverableCreationComponent.prototype.getStatus = function (statusLeveLList) {
        var _this = this;
        return new Observable(function (observer) {
            _this.entityAppdefService.getStatusByCategoryLevel(MPM_LEVELS.DELIVERABLE)
                .subscribe(function (statusResponse) {
                if (statusResponse) {
                    if (!Array.isArray(statusResponse)) {
                        statusResponse = [statusResponse];
                    }
                    _this.deliverableModalConfig.deliverableStatusList = statusResponse.filter(function (data) {
                        return statusLeveLList.indexOf(data.STATUS_LEVEL) >= 0 ? true : false;
                    });
                }
                else {
                    _this.deliverableModalConfig.deliverableStatusList = [];
                }
                observer.next(true);
                observer.complete();
            }, function (error) {
                var eventData = { message: 'Something went wrong while fetching Statuses', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    DeliverableCreationComponent.prototype.getAllRoles = function () {
        var _this = this;
        var ownerRolesParams = {
            teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
            isApproverTask: false,
            isMemberTask: true
        };
        var approverRolesParams = {
            teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
            isApproverTask: true,
            isMemberTask: false
        };
        return new Observable(function (observer) {
            forkJoin([_this.appService.getTeamRoles(ownerRolesParams), _this.appService.getTeamRoles(approverRolesParams)])
                .subscribe(function (response) {
                if (response && response[0].Teams && response[0].Teams.MPM_Teams &&
                    response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping && response[1].Teams &&
                    response[1].Teams.MPM_Teams && response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    var ownerRoleList = [];
                    var approverRoleList = [];
                    var ownerRoles_1 = [];
                    var approverRoles_1 = [];
                    if (!Array.isArray(response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping)) {
                        ownerRoleList = [response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping];
                    }
                    if (!Array.isArray(response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping)) {
                        approverRoleList = [response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping];
                    }
                    ownerRoleList = response[0].Teams.MPM_Teams.MPM_Team_Role_Mapping;
                    approverRoleList = response[1].Teams.MPM_Teams.MPM_Team_Role_Mapping;
                    if (ownerRoleList && ownerRoleList.length && ownerRoleList.length > 0) {
                        ownerRoleList.map(function (role) {
                            ownerRoles_1.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    if (approverRoleList && approverRoleList.length && approverRoleList.length > 0) {
                        approverRoleList.map(function (role) {
                            approverRoles_1.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    _this.teamOwnerRolesOptions = ownerRoles_1;
                    _this.teamApproverRolesOptions = approverRoles_1;
                    _this.deliverableModalConfig.deliverableOwnerRoleConfig.filterOptions = _this.teamOwnerRolesOptions;
                    _this.deliverableModalConfig.deliverableApproverRole.filterOptions = _this.teamApproverRolesOptions;
                }
                else {
                    _this.teamOwnerRolesOptions = [];
                    _this.teamApproverRolesOptions = [];
                }
                observer.next(true);
                observer.complete();
            }, function (error) {
                var eventData = { message: 'Something went wrong while fetching Roles', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    DeliverableCreationComponent.prototype.getReviewerById = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            if (!_this.utilService.isNullOrEmpty(userId)) {
                var parameters = {
                    userId: userId
                };
                _this.appService.getPersonByIdentityUserId(parameters).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: '', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    };
    DeliverableCreationComponent.prototype.getDeliverableReviewByDeliverableId = function (deliverableId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.getDeliverableReviewByDeliverableID(deliverableId)
                .subscribe(function (response) {
                if (response && response.DeliverableReview) {
                    var reviewersList = acronui.findObjectsByProp(response, 'DeliverableReview');
                    if (reviewersList && reviewersList.length && reviewersList.length > 0) {
                        reviewersList.forEach(function (reviewer) {
                            if (!_this.utilService.isNullOrEmpty(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id)) {
                                _this.getReviewerById(reviewer.R_PO_REVIEWER_USER['Identity-id'].Id).subscribe(function (reviewerData) {
                                    if (reviewerData) {
                                        var hasReviewerData = _this.savedReviewers.find(function (savedReviewer) { return savedReviewer.value === reviewerData.userCN; });
                                        if (!hasReviewerData) {
                                            _this.savedReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                            _this.deliverableReviewers.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName
                                            });
                                        }
                                    }
                                    _this.entityAppdefService.getStatusById(reviewer.R_PO_REVIEW_STATUS['MPM_Status-id'].Id).subscribe(function (statusResponse) {
                                        var isReviewerCompleted = statusResponse.STATUS_LEVEL === 'FINAL' ? true : false;
                                        var reviewerIndex = _this.reviewerStatusData.findIndex(function (data) { return data.value === reviewerData.userCN; });
                                        if (reviewerIndex >= 0) {
                                            _this.reviewerStatusData[reviewerIndex].isCompleted = isReviewerCompleted;
                                        }
                                        else {
                                            _this.reviewerStatusData.push({
                                                name: reviewerData.fullName,
                                                value: reviewerData.userCN,
                                                displayName: reviewerData.fullName,
                                                isCompleted: isReviewerCompleted
                                            });
                                        }
                                    });
                                    observer.next(_this.savedReviewers);
                                    observer.complete();
                                });
                            }
                            else {
                                observer.next('');
                                observer.complete();
                            }
                        });
                    }
                    else {
                        observer.next('');
                        observer.complete();
                    }
                }
                else {
                    observer.next('');
                    observer.complete();
                }
            }, function () {
                var eventData = { message: 'Something went wrong while fetching Deliverable Review Details', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    DeliverableCreationComponent.prototype.getReviewers = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.deliverableConfig.selectedTask.TASK_TYPE === TaskTypes.APPROVAL_TASK) {
                var getReviewerUserRequest = {};
                getReviewerUserRequest = {
                    allocationType: 'USER',
                    roleDN: '',
                    teamID: _this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: true,
                    isUploadTask: false,
                    isReview: true
                };
                _this.appService.getUsersForTeam(getReviewerUserRequest)
                    .subscribe(function (response) {
                    if (response && response.users && response.users.user) {
                        var userList = acronui.findObjectsByProp(response, 'user');
                        var users_1 = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(function (user) {
                                var fieldObj = users_1.find(function (e) { return e.value === user.dn; });
                                if (!fieldObj) {
                                    users_1.push({
                                        name: user.dn,
                                        value: user.cn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        _this.allReviewers = users_1;
                    }
                    else {
                        _this.allReviewers = [];
                    }
                    _this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, function () {
                    var eventData = { message: 'Something went wrong while fetching Reviewer Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                observer.next('');
                observer.complete();
            }
        });
    };
    DeliverableCreationComponent.prototype.getUsersByRole = function (field, currAllocationType) {
        var _this = this;
        this.loaderService.show();
        var getOwnerByRoleRequest = {};
        var getApproverByRoleRequest = {};
        var getUserByRoleRequest = null;
        var userType;
        if (this.deliverableForm) {
            var allocationType = (field === 'OWNER') ? this.deliverableForm.getRawValue().ownerAllocation : this.deliverableForm.getRawValue().approverAllocation;
            userType = field;
            getUserByRoleRequest = {
                allocationType: allocationType,
                roleDN: allocationType === AllocationTypes.ROLE.toUpperCase() ?
                    (field === 'OWNER' ? this.deliverableForm.getRawValue().deliverableOwnerRole.value : this.deliverableForm.getRawValue().deliverableApproverRole.value) : '',
                teamID: allocationType === AllocationTypes.USER.toUpperCase() ? this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id : '',
                isApproveTask: (field === 'OWNER') ? false : true,
                isUploadTask: (field === 'OWNER') ? true : false
            };
        }
        else if (this.deliverableModalConfig.selectedDeliverable) {
            var selectedField = this.utilService.isNullOrEmpty(this.deliverableModalConfig.selectedDeliverable.DELIVERABLE_PARENT_ID) ? 'OWNER' : 'APPROVER';
            if (selectedField === 'OWNER') {
                var allocationType = this.deliverableModalConfig.selectedDeliverable.OWNER_ASSIGNMENT_TYPE;
                var selectedRoleId = allocationType === AllocationTypes.ROLE.toUpperCase() ? this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                userType = selectedField;
                getUserByRoleRequest = {
                    allocationType: allocationType,
                    roleDN: allocationType === AllocationTypes.ROLE.toUpperCase() ? this.formRoleValue(selectedRoleId, false).value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            else {
                var ownerAllocationType = this.deliverableModalConfig.selectedDeliverable.OWNER_ASSIGNMENT_TYPE;
                var approverAllocationType = this.deliverableModalConfig.selectedDeliverable.APPROVER_ASSIGNMENT_TYPE;
                var selectedOwnerRoleId = ownerAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                    this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                var selectedApproverRoleId = approverAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                    this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_ROLE_ID['MPM_Team_Role_Mapping-id'].Id : '';
                getOwnerByRoleRequest = {
                    allocationType: ownerAllocationType,
                    roleDN: ownerAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                        this.formRoleValue(selectedOwnerRoleId, false).value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: false,
                    isUploadTask: true
                };
                getApproverByRoleRequest = {
                    allocationType: approverAllocationType,
                    roleDN: approverAllocationType === AllocationTypes.ROLE.toUpperCase() ?
                        this.formRoleValue(selectedApproverRoleId, selectedField === 'APPROVER').value : '',
                    teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                    isApproveTask: true,
                    isUploadTask: false
                };
            }
        }
        else {
            getOwnerByRoleRequest = {
                allocationType: 'USER',
                roleDN: '',
                teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                isApproveTask: false,
                isUploadTask: true
            };
            getApproverByRoleRequest = {
                allocationType: 'USER',
                roleDN: '',
                teamID: this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id,
                isApproveTask: true,
                isUploadTask: false
            };
        }
        return new Observable(function (observer) {
            if (getUserByRoleRequest) {
                _this.appService.getUsersForTeam(getUserByRoleRequest)
                    .subscribe(function (response) {
                    if (response && response.users && response.users.user) {
                        var userList = acronui.findObjectsByProp(response, 'user');
                        var users_2 = [];
                        if (userList && userList.length && userList.length > 0) {
                            userList.forEach(function (user) {
                                var fieldObj = users_2.find(function (e) { return e.value === user.dn; });
                                if (!fieldObj) {
                                    users_2.push({
                                        name: user.cn,
                                        value: user.dn,
                                        displayName: user.name
                                    });
                                }
                            });
                        }
                        if (userType === 'APPROVER') {
                            _this.teamRoleApproverOptions = users_2;
                            _this.deliverableModalConfig.deliverableApprover.filterOptions = _this.teamRoleApproverOptions;
                        }
                        else {
                            _this.teamRoleOwnerOptions = users_2;
                            _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = _this.teamRoleOwnerOptions;
                        }
                    }
                    else {
                        if (userType === 'APPROVER') {
                            _this.teamRoleApproverOptions = [];
                        }
                        else {
                            _this.teamRoleOwnerOptions = [];
                        }
                    }
                    _this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    _this.loaderService.hide();
                    observer.next(false);
                    observer.complete();
                });
            }
            else {
                forkJoin([_this.appService.getUsersForTeam(getOwnerByRoleRequest), _this.appService.getUsersForTeam(getApproverByRoleRequest)])
                    .subscribe(function (response) {
                    if (response && response[0].users && response[0].users.user &&
                        response[1].users && response[1].users.user) {
                        var ownerList = acronui.findObjectsByProp(response[0], 'user');
                        var approverList = acronui.findObjectsByProp(response[1], 'user');
                        var owners_1 = [];
                        var approvers_1 = [];
                        if (ownerList && ownerList.length && ownerList.length > 0) {
                            ownerList.forEach(function (owner) {
                                var fieldObj = owners_1.find(function (e) { return e.value === owner.dn; });
                                if (!fieldObj) {
                                    owners_1.push({
                                        name: owner.cn,
                                        value: owner.dn,
                                        displayName: owner.name
                                    });
                                }
                            });
                        }
                        if (approverList && approverList.length && approverList.length > 0) {
                            approverList.forEach(function (approver) {
                                var fieldObj = approvers_1.find(function (e) { return e.value === approver.dn; });
                                if (!fieldObj) {
                                    approvers_1.push({
                                        name: approver.cn,
                                        value: approver.dn,
                                        displayName: approver.name
                                    });
                                }
                            });
                        }
                        _this.teamRoleOwnerOptions = owners_1;
                        _this.deliverableModalConfig.deliverableOwnerConfig.filterOptions = _this.teamRoleOwnerOptions;
                        _this.teamRoleApproverOptions = approvers_1;
                        _this.deliverableModalConfig.deliverableApprover.filterOptions = _this.teamRoleApproverOptions;
                    }
                    else {
                        _this.teamRoleOwnerOptions = [];
                        _this.teamRoleApproverOptions = [];
                    }
                    _this.loaderService.hide();
                    observer.next(true);
                    observer.complete();
                }, function (error) {
                    var eventData = { message: 'Something went wrong while fetching Individual Users', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                    _this.notificationHandler.next(eventData);
                    _this.loaderService.hide();
                    observer.next(false);
                    observer.complete();
                });
            }
        });
    };
    DeliverableCreationComponent.prototype.getAssignmentTypes = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.ASSIGNMENT_TYPE_METHOD_NS, _this.GET_ASSIGNMENT_TYPE_WS_METHOD_NAME, null)
                .subscribe(function (response) {
                if (response.Assignment_Type) {
                    if (!Array.isArray(response.Assignment_Type)) {
                        response.Assignment_Type = [response.Assignment_Type];
                    }
                    _this.deliverableModalConfig.deliverableOwnerAssignmentType = response.Assignment_Type;
                    _this.deliverableModalConfig.deliverableApproverAssignmentType = response.Assignment_Type;
                }
                else {
                    _this.deliverableModalConfig.deliverableOwnerAssignmentType = [];
                    _this.deliverableModalConfig.deliverableApproverAssignmentType = [];
                }
                // TODO: check for defaultPriority
                observer.next(true);
                observer.complete();
            }, function (error) {
                var eventData = { message: 'Something went wrong while fetching Assignment types', type: ProjectConstant.NOTIFICATION_LABELS.ERROR };
                _this.notificationHandler.next(eventData);
                observer.next(false);
                observer.complete();
            });
        });
    };
    DeliverableCreationComponent.prototype.getPreDetails = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.loaderService.show();
            if (_this.deliverableConfig.deliverableId) {
                forkJoin([_this.getDeliverableDetails(), _this.getAllRoles()]).subscribe(function () {
                    forkJoin([_this.getPriorities(), _this.getUsersByRole(), _this.getReviewers(), _this.getDeliverableReviewByDeliverableId(_this.deliverableConfig.deliverableId)])
                        .subscribe(function (delResponse) {
                        if (_this.deliverableModalConfig.selectedProject.R_PO_CATEGORY['MPM_Category-id'].Id && _this.deliverableModalConfig.selectedProject.R_PO_TEAM['MPM_Teams-id'].Id) {
                            var statusList = [StatusLevels.INTERMEDIATE, StatusLevels.INITIAL];
                            if (_this.deliverableModalConfig.currStatus.STATUS_LEVEL === StatusLevels.INTERMEDIATE) {
                                statusList.pop();
                            }
                            var approverValue = _this.deliverableModalConfig.selectedDeliverable && _this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID &&
                                _this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID['Identity-id'] ?
                                _this.deliverableModalConfig.selectedDeliverable.R_PO_APPROVER_USER_ID['Identity-id'].Id : '';
                            var ownerValue = _this.deliverableModalConfig.selectedDeliverable && _this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID &&
                                _this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID['Identity-id'] ?
                                _this.deliverableModalConfig.selectedDeliverable.R_PO_OWNER_USER_ID['Identity-id'].Id : '';
                            forkJoin([_this.getStatus(statusList), _this.formApproverValue(approverValue),
                                _this.formOwnerValue(ownerValue)])
                                .subscribe(function (responseList) {
                                _this.loaderService.hide();
                                observer.next();
                                observer.complete();
                            }, function (err) {
                                _this.loaderService.hide();
                            });
                        }
                    }, function (delError) {
                        _this.loaderService.hide();
                        observer.error(delError);
                    });
                });
            }
            else {
                forkJoin([_this.getPriorities(), _this.getStatus([StatusLevels.INITIAL]), _this.getAllRoles(), _this.getUsersByRole()])
                    .subscribe(function (responseList) {
                    var ownerValue = _this.deliverableModalConfig.selectedTask && _this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID &&
                        _this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'] ?
                        _this.deliverableModalConfig.selectedTask.R_PO_OWNER_ID['Identity-id'].Id : '';
                    forkJoin([_this.formOwnerValue(ownerValue)])
                        .subscribe(function (response) {
                        _this.loaderService.hide();
                        observer.next();
                        observer.complete();
                    }, function (err) {
                        _this.loaderService.hide();
                    });
                }, function (err) {
                    _this.loaderService.hide();
                    observer.error(err);
                });
            }
        });
    };
    DeliverableCreationComponent.prototype.cancelDeliverableCreation = function () {
        var _this = this;
        if (this.deliverableForm.pristine) {
            this.closeCallbackHandler.next(true);
        }
        else {
            var dialogRef = this.dialog.open(ConfirmationModalComponent, {
                width: '40%',
                disableClose: true,
                data: {
                    message: 'Are you sure you want to close?',
                    submitButton: 'Yes',
                    cancelButton: 'No'
                }
            });
            dialogRef.afterClosed().subscribe(function (result) {
                if (result && result.isTrue) {
                    _this.closeCallbackHandler.next(true);
                }
            });
        }
    };
    DeliverableCreationComponent.prototype.getMaxMinValueForNumber = function (scale) {
        var numericeVlalue = '99999';
        if (scale) {
            numericeVlalue += '.' + newArray(scale).fill('9').join('');
        }
        // tslint:disable-next-line: radix
        return scale ? parseFloat(numericeVlalue) : parseInt(numericeVlalue);
    };
    DeliverableCreationComponent.prototype.initialiseDeliverableConfig = function () {
        this.deliverableModalConfig = {
            hasAllConfig: true,
            deliverablePriorityList: null,
            deliverableStatusList: null,
            defaultStatus: null,
            defaultPriority: null,
            deliverableOwnerConfig: {
                label: 'Deliverable Owner',
                filterOptions: [],
                formControl: null
            },
            deliverableOwnerRoleConfig: {
                label: 'Deliverable Owner Role',
                filterOptions: [],
                formControl: null
            },
            deliverableApprover: {
                label: 'Deliverable Approver',
                filterOptions: [],
                formControl: null
            },
            deliverableApproverRole: {
                label: 'Deliverable Approver Role',
                filterOptions: [],
                formControl: null
            },
            selectedDeliverable: this.deliverableConfig.selectedDeliverable,
            deliverableOwnerAssignmentType: DeliverableConstants.ALLOCATION_TYPE,
            deliverableApproverAssignmentType: DeliverableConstants.ALLOCATION_TYPE,
            isNewDeliverable: (this.deliverableConfig.deliverableId) ? false : true,
            isTemplate: this.deliverableConfig.isTemplate,
            projectId: this.deliverableConfig.projectId,
            taskId: this.deliverableConfig.taskId,
            deliverableId: this.deliverableConfig.deliverableId,
            selectedProject: this.deliverableConfig.selectedProject,
            selectedTask: this.deliverableConfig.selectedTask,
            minDate: '',
            maxDate: '',
            deliverableAllocationList: [],
            currStatus: null
        };
        if (this.deliverableConfig.deliverableId) {
            this.saveOptions = [this.saveOptions[0]];
        }
    };
    DeliverableCreationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach(function (user) {
            if (user.Name === 'MPM Project Manager') {
                _this.isPM = true;
            }
        });
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        this.isApprover = this.sharingService.getIsApprover();
        this.isUploaded = this.sharingService.getIsUploaded();
        this.projectConfig = this.sharingService.getProjectConfig();
        this.enableNeedReview = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_NEED_REVIEW]);
        this.enableWorkWeek = this.utilService.getBooleanValue(this.projectConfig[ApplicationConfigConstants.MPM_PROJECT_CONFIG.ENABLE_WORK_WEEK]);
        this.appConfig = this.sharingService.getAppConfig();
        this.nameStringPattern = this.utilService.isNullOrEmpty(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN]) ? '.*' :
            this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.NAME_STRING_PATTERN];
        if (this.deliverableConfig && this.deliverableConfig.taskId) {
            this.initialiseDeliverableConfig();
            this.getPreDetails().subscribe(function (response) {
                _this.initialiseDeliverableForm();
            });
        }
    };
    DeliverableCreationComponent.ctorParameters = function () { return [
        { type: DateAdapter },
        { type: AppService },
        { type: UtilService },
        { type: EntityAppDefService },
        { type: DeliverableService },
        { type: LoaderService },
        { type: MatDialog },
        { type: StatusService },
        { type: ProjectUtilService },
        { type: SharingService },
        { type: NotificationService }
    ]; };
    __decorate([
        Input()
    ], DeliverableCreationComponent.prototype, "deliverableConfig", void 0);
    __decorate([
        Output()
    ], DeliverableCreationComponent.prototype, "closeCallbackHandler", void 0);
    __decorate([
        Output()
    ], DeliverableCreationComponent.prototype, "saveCallBackHandler", void 0);
    __decorate([
        Output()
    ], DeliverableCreationComponent.prototype, "notificationHandler", void 0);
    __decorate([
        ViewChild(CustomSearchFieldComponent)
    ], DeliverableCreationComponent.prototype, "customSearchFieldComponent", void 0);
    __decorate([
        ViewChild('reviewerInput', { static: false })
    ], DeliverableCreationComponent.prototype, "reviewerInput", void 0);
    __decorate([
        ViewChild('auto', { static: false })
    ], DeliverableCreationComponent.prototype, "matAutocomplete", void 0);
    DeliverableCreationComponent = __decorate([
        Component({
            selector: 'mpm-deliverable-creation',
            template: "<div class=\"overview-content\">\r\n    <form class=\"deliverable-creation-form\" *ngIf=\"deliverableForm\" [formGroup]=\"deliverableForm\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Name</mat-label>\r\n                    <input [appAutoFocus]=\"deliverableModalConfig && deliverableModalConfig.hasAllConfig\" matInput\r\n                        placeholder=\"Deliverable Name\" formControlName=\"deliverableName\" required>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item\" *ngIf=\"!deliverableConfig.isTemplate\">\r\n                <mat-form-field id=\"dueDate\" appearance=\"outline\">\r\n                    <mat-label>Due On</mat-label>\r\n                    <input matInput [min]=\"deliverableModalConfig.minDate\" [max]=\"deliverableModalConfig.maxDate\"\r\n                        [matDatepicker]=\"dueDate\" placeholder=\"Deliverable Due Date (MM/DD/YYYY)\"\r\n                        formControlName=\"dueDate\" required>\r\n                    <mat-datepicker-toggle matSuffix [for]=\"dueDate\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #dueDate></mat-datepicker>\r\n                    <mat-error\r\n                        *ngIf=\"(deliverableForm.get('dueDate').hasError('matDatepickerMax') || deliverableForm.get('dueDate').hasError('matDatepickerMin'))\">\r\n                        {{deliverableConstants.DATE_ERROR_MESSAGE}}\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item skip-review-section-holder\">\r\n                <section class=\"skip-review-check\">\r\n                    <mat-checkbox color=\"primary\" labelPosition=\"before\" formControlName=\"needReview\">\r\n                        <span class=\"skipReview\"> Need Review? </span>\r\n                    </mat-checkbox>\r\n                </section>\r\n            </div>\r\n            <div class=\"flex-row-item flex\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Priority</mat-label>\r\n                    <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" required>\r\n                        <mat-option *ngFor=\"let priority of deliverableModalConfig.deliverablePriorityList\"\r\n                            value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                            {{priority.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item max-width\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Status</mat-label>\r\n                    <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"status\" required>\r\n                        <mat-option *ngFor=\"let status of deliverableModalConfig.deliverableStatusList\"\r\n                            value=\"{{status['MPM_Status-id'].Id}}\">\r\n                            {{status.NAME}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Owner Allocation</mat-label>\r\n                    <mat-select formControlName=\"ownerAllocation\" placeholder=\"Allocation\" required\r\n                        name=\"ownerAllocation\" disable>\r\n                        <mat-option *ngFor=\"let allocationType of deliverableModalConfig.deliverableOwnerAssignmentType\"\r\n                            [value]=\"allocationType.NAME\">\r\n                            {{allocationType.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableOwnerRoleConfig\"\r\n                    [required]=\"deliverableForm.getRawValue().ownerAllocation !== deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE || deliverableForm.getRawValue().ownerAllocation === deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableOwnerConfig\"\r\n                    [required]=\"deliverableForm.getRawValue().ownerAllocation !== deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\" *ngIf=\"deliverableForm.getRawValue().needReview\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Deliverable Approver Allocation</mat-label>\r\n                    <mat-select formControlName=\"approverAllocation\" placeholder=\"Allocation\"\r\n                        [required]=\"deliverableForm.getRawValue().needReview\" name=\"approverAllocation\" disable>\r\n                        <mat-option\r\n                            *ngFor=\"let allocationType of deliverableModalConfig.deliverableApproverAssignmentType\"\r\n                            [value]=\"allocationType.NAME\">\r\n                            {{allocationType.DESCRIPTION}}\r\n                        </mat-option>\r\n                    </mat-select>\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().needReview  && deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableApproverRole\"\r\n                    [required]=\"deliverableForm.getRawValue().needReview  && !(deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_USER)\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item mat-form-field\"\r\n                *ngIf=\"deliverableForm.getRawValue().needReview && deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE || deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_USER\">\r\n                <mpm-custom-search-field class=\"flex-row-item\"\r\n                    [searchFieldConfig]=\"deliverableModalConfig.deliverableApprover\"\r\n                    [required]=\"deliverableForm.getRawValue().needReview && !(deliverableForm.getRawValue().approverAllocation === deliverableConstants.ALLOCATION_TYPE_ROLE)\">\r\n                </mpm-custom-search-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"task-owner-task-count\" *ngIf=\"hasDeliverableCount\">\r\n            <mat-error *ngIf=\"!isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-error>\r\n            <mat-label *ngIf=\"isZeroOwnerProjectCount\">{{ownerProjectCountMessage}}</mat-label>\r\n        </div>\r\n        <!--         <div class=\"flex-row\" *ngIf=\"deliverableForm.getRawValue().needReview && showViewOrEditReviewers\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Reviewers</mat-label>\r\n                    <mat-chip-list #reviewerChipList aria-label=\"Reviewer selection\">\r\n                        <mat-chip *ngFor=\"let reviewer of reviewers\" [selectable]=\"false\" [removable]=\"true\"\r\n                            [ngClass]=\"{'has-reviewed': reviewer.hasReviewed}\">\r\n                            {{reviewer.displayName}}\r\n                            <mat-icon matChipRemove>cancel</mat-icon>\r\n                        </mat-chip>\r\n                        <input #reviewerInput formControlName=\"deliverableReviewers\">\r\n                    </mat-chip-list>\r\n                    <mat-autocomplete #auto=\"matAutocomplete\">\r\n                        <mat-option *ngFor=\"let reviewer of filteredReviewers | async\" [value]=\"reviewer\">\r\n                            {{reviewer.displayName}}\r\n                        </mat-option>\r\n                    </mat-autocomplete>\r\n                </mat-form-field>\r\n            </div>\r\n        </div> -->\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Expected Duration (Days)</mat-label>\r\n                    <input matInput placeholder=\"Expected Duration (Days)\" formControlName=\"expectedDuration\"\r\n                        type=\"number\">\r\n                    <!-- <mat-hint align=\"end\" *ngIf=\"projectOverviewForm.get('expectedDuration').errors\">\r\n                        Please enter the number lesser than 99999\r\n                    </mat-hint> -->\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <mat-form-field appearance=\"outline\">\r\n                    <mat-label>Description</mat-label>\r\n                    <textarea matInput placeholder=\"Description\" formControlName=\"description\"></textarea>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n        <div class=\"flex-col-item\">\r\n            <div class=\"flex-row\">\r\n                <div class=\"flex-row-item\">\r\n                    <span class=\"form-actions\">\r\n                        <button type=\"button\" mat-stroked-button (click)=\"cancelDeliverableCreation()\">Cancel</button>\r\n                        <button id=\"deliverable-save-btn\" color='primary' mat-flat-button\r\n                            (click)=\"updateDeliverable(SELECTED_SAVE_OPTION)\" type=\"submit\" class=\"ang-btn\"\r\n                            matTooltip=\"{{SELECTED_SAVE_OPTION.name}}\"\r\n                            [disabled]=\"deliverableForm.pristine || deliverableForm.invalid\">{{SELECTED_SAVE_OPTION.name}}</button>\r\n                        <span *ngIf=\"!deliverableConfig.deliverableId\">\r\n                            <button id=\"deliverable-save-option\" mat-flat-button color=\"primary\"\r\n                                [matMenuTriggerFor]=\"saveOptionsMenu\"\r\n                                [disabled]=\"deliverableForm.pristine || deliverableForm.invalid\">\r\n                                <mat-icon>arrow_drop_down</mat-icon>\r\n                            </button>\r\n                            <mat-menu #saveOptionsMenu=\"matMenu\">\r\n                                <span *ngFor=\"let saveOption of saveOptions\">\r\n                                    <button *ngIf=\"saveOption.value != SELECTED_SAVE_OPTION.value\" mat-menu-item\r\n                                        (click)=\"updateDeliverable(saveOption)\">\r\n                                        <span>{{saveOption.name}}</span>\r\n                                    </button>\r\n                                </span>\r\n                            </mat-menu>\r\n                        </span>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>",
            styles: [".project-overview-form{display:flex;flex-direction:column;flex-wrap:wrap;justify-content:center;padding:10px 50px;width:50vw}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-actions button{margin:10px}.task-owner-task-count{margin-left:16px;margin-bottom:16px;font-size:12px;margin-top:-16px}.task-owner-task-count mat-label{color:green}#deliverable-save-option{margin:0;padding:0;min-width:32px;border-radius:0 4px 4px 0}#deliverable-save-btn{margin-right:0;border-radius:4px 0 0 4px;margin-left:15px}.form-actions{margin-left:auto}mat-form-field{width:100%}.flex-row-item{margin:.666667em 15px 0;font-size:12px}.skip-review-section-holder{flex-grow:3}.flex-row .mat-form-field mpm-custom-search-field{margin:0}.form-group-panel{background:0 0}.form-group-panel ::ng-deep mpm-custom-metadata-field{width:100%}.form-group-panel ::ng-deep .mat-expansion-panel-header{padding:0 4px}.form-group-panel ::ng-deep .mat-expansion-panel-body{padding:0 16px 16px 0}.flex-row-item.flex{width:10.5rem}"]
        })
    ], DeliverableCreationComponent);
    return DeliverableCreationComponent;
}());
export { DeliverableCreationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtY3JlYXRpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvcHJvamVjdC90YXNrcy9kZWxpdmVyYWJsZS9kZWxpdmVyYWJsZS1jcmVhdGlvbi9kZWxpdmVyYWJsZS1jcmVhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQWdCLFNBQVMsRUFBYyxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUU5RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDakYsT0FBTyxLQUFLLE9BQU8sTUFBTSwyQ0FBMkMsQ0FBQztBQUdyRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSwrRUFBK0UsQ0FBQztBQUMzSCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDekYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQzNFLE9BQU8sRUFBZSxZQUFZLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNyRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNuRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx1REFBdUQsQ0FBQztBQUN6RixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUM3SCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sdURBQXVELENBQUM7QUFDdkYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDOUcsT0FBTyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUdoRCxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDbkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFRcEY7SUErRkksc0NBQ1csT0FBeUIsRUFDekIsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsbUJBQXdDLEVBQ3hDLGtCQUFzQyxFQUN0QyxhQUE0QixFQUM1QixNQUFpQixFQUNqQixhQUE0QixFQUM1QixrQkFBc0MsRUFDdEMsY0FBOEIsRUFDOUIsbUJBQXdDO1FBWG5ELGlCQVlLO1FBWE0sWUFBTyxHQUFQLE9BQU8sQ0FBa0I7UUFDekIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBdkd6Qyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQy9DLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDOUMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQVV4RCxrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFVeEIseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFRNUMsZ0JBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ3BGLHlCQUFvQixHQUFHO1lBQ25CLElBQUksRUFBRSxNQUFNO1lBQ1osS0FBSyxFQUFFLE1BQU07U0FDaEIsQ0FBQztRQUlGLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQix1QkFBa0IsR0FBYSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUc5QyxTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2IsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFFbEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBU3RCLHlCQUF5QjtRQUN6QixtQkFBYyxHQUFVLEVBQUUsQ0FBQztRQUMzQixpQkFBWSxHQUFVLEVBQUUsQ0FBQztRQUN6Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO1FBQ25CLHVCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUV4Qix5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFLMUIsOEJBQXlCLEdBQUcsa0RBQWtELENBQUM7UUFDL0UsMkJBQXNCLEdBQUcsK0NBQStDLENBQUM7UUFDekUsa0NBQTZCLEdBQUcsc0RBQXNELENBQUM7UUFDdkYsdUNBQWtDLEdBQUcsMkRBQTJELENBQUM7UUFFakcsd0JBQW1CLEdBQUcsbURBQW1ELENBQUM7UUFDMUUsd0JBQW1CLEdBQUcsNkRBQTZELENBQUM7UUFDcEYsOEJBQXlCLEdBQUcsMERBQTBELENBQUM7UUFDdkYsMEJBQXFCLEdBQUcsc0RBQXNELENBQUM7UUFFL0UsdUNBQWtDLEdBQUcsZ0JBQWdCLENBQUM7UUFDdEQsb0NBQStCLEdBQUcsVUFBVSxDQUFDO1FBQzdDLDJDQUFzQyxHQUFHLGlCQUFpQixDQUFDO1FBQzNELHFDQUFnQyxHQUFHLG9CQUFvQixDQUFDO1FBQ3hELCtDQUEwQyxHQUFHLDZCQUE2QixDQUFDO1FBQzNFLHVDQUFrQyxHQUFHLHNCQUFzQixDQUFDO1FBQzVELHNDQUFpQyxHQUFHLG1CQUFtQixDQUFDO1FBQ3hELHNDQUFpQyxHQUFHLGlCQUFpQixDQUFDO1FBQ3RELGdEQUEyQyxHQUFHLHFDQUFxQyxDQUFDO1FBQ3BGLDBDQUFxQyxHQUFHLG1CQUFtQixDQUFDO1FBQzVELGdEQUEyQyxHQUFHLDBCQUEwQixDQUFDO1FBbW9EekUsZUFBVSxHQUNWLFVBQUMsSUFBaUI7WUFDZCxJQUFJLEtBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0gsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQztnQkFDOUIsZ0JBQWdCO2dCQUNoQixrQkFBa0I7YUFDckI7UUFDTCxDQUFDLENBQUE7SUEvbkRHLENBQUM7SUFFTCxzREFBZSxHQUFmLFVBQWdCLEtBQVU7UUFDdEIsSUFBSSxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDM0QsSUFBTSxhQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBRXhDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxhQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZELENBQXVELENBQUMsQ0FBQztTQUN4RztJQUNMLENBQUM7SUFFRCwrQ0FBUSxHQUFSLFVBQVMsS0FBbUM7UUFDeEMsSUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUExQyxDQUEwQyxDQUFDLENBQUM7UUFDakgsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUF6QyxDQUF5QyxDQUFDLENBQUM7UUFFbkcsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxtQkFBbUI7WUFDOUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFO1lBQzVFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRywwQkFBMEIsQ0FBQyxDQUFDO1NBQ3JGO2FBQU0sSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLHNCQUFzQixDQUFDLENBQUM7U0FDakY7YUFBTTtZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO2dCQUNyQixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO2FBQ3BDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO2dCQUNuQixJQUFJLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQzFCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO2FBQ3BDLENBQUMsQ0FBQztZQUNILElBQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFBLGVBQWUsSUFBSSxPQUFBLGVBQWUsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQTVDLENBQTRDLENBQUMsQ0FBQztZQUM3SCxJQUFJLG1CQUFtQixJQUFJLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN4RDtZQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztnQkFDMUIsS0FBSyxFQUFFLFFBQVEsQ0FBQyxLQUFLO2dCQUNyQixXQUFXLEVBQUUsUUFBUSxDQUFDLFdBQVc7Z0JBQ2pDLFdBQVcsRUFBRSxLQUFLO2FBQ3JCLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3JFO0lBQ0wsQ0FBQztJQUVELGtEQUFXLEdBQVgsVUFBWSxLQUF3QjtRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7WUFDOUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUUxQixJQUFJLEtBQUssRUFBRTtnQkFDUCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzthQUNwQjtZQUVELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyRTtJQUNMLENBQUM7SUFFRCx1REFBZ0IsR0FBaEIsVUFBaUIsYUFBa0I7UUFDL0IsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBckMsQ0FBcUMsQ0FBQyxDQUFDO1FBQ3BHLElBQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXLElBQUksT0FBQSxXQUFXLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBbkMsQ0FBbUMsQ0FBQyxDQUFDO1FBQ3pHLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxZQUFZLElBQUksT0FBQSxZQUFZLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO1FBQzlHLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsWUFBWSxDQUFDLEtBQUssS0FBSyxhQUFhLEVBQXBDLENBQW9DLENBQUMsQ0FBQztRQUM5RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO1lBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsV0FBVztZQUMxQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7WUFDckIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxXQUFXO1NBQ3BDLENBQUMsQ0FBQztRQUVILElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNaLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxhQUFhLElBQUksQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxnQkFBZ0IsSUFBSSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsZ0VBQXlCLEdBQXpCLFVBQTBCLGlCQUFpQjtRQUN2QyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDcEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDeEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUMxRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDbEY7YUFBTTtZQUNILElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDL0o7SUFDTCxDQUFDO0lBRUQsb0VBQTZCLEdBQTdCLFVBQThCLG1CQUFtQixFQUFFLFVBQVUsRUFBRSx5QkFBeUI7UUFBeEYsaUJBc0NDO1FBckNHLElBQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87WUFDbkMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwQixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDbkUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDNUQ7WUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4RixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDNUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLDBCQUEwQixDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0UsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLG9CQUFvQixFQUFFO2dCQUN6RCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzNDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDO2dCQUNqRyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDO2FBQ2hHO1lBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUMzRjthQUFNLElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDMUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDckUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDcEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUM1RSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDM0MsS0FBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUM7Z0JBQzdGLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDNUYsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTtnQkFDM0csSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDM0Q7U0FDSjtJQUNMLENBQUM7SUFFRCx1RUFBZ0MsR0FBaEMsVUFBaUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLHlCQUF5QjtRQUEzRixpQkF5Q0M7UUF4Q0csSUFBTSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxtQkFBbUIsS0FBSyxvQkFBb0IsQ0FBQyxvQkFBb0IsRUFBRTtZQUNuRSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dCQUNuRCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMvRDtZQUNELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLGFBQWEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzNGLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3BFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDM0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUMvRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUMxRSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsdUJBQXVCLEVBQUU7Z0JBQzVELElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDOUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUM7Z0JBQ2pHLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7YUFDaEc7WUFDRCxJQUFJLHlCQUF5QixFQUFFO2dCQUMzQixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDcEU7U0FDSjthQUFNLElBQUksbUJBQW1CLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDMUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDeEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUMvRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1lBQzNFLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDOUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUM7Z0JBQzdGLElBQUkseUJBQXlCLEVBQUU7b0JBQzNCLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDcEU7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzlEO1NBQ0o7SUFDTCxDQUFDO0lBRUQsMERBQW1CLEdBQW5CLFVBQW9CLFNBQVM7UUFBN0IsaUJBZUM7UUFkRyxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO1lBQzdCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLEVBQUU7Z0JBQ2hILElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQzNEO1lBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUMzQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQztnQkFDN0YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2xFLENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDNUQ7U0FDSjtJQUNMLENBQUM7SUFFRCw2REFBc0IsR0FBdEIsVUFBdUIsWUFBWTtRQUFuQyxpQkFlQztRQWRHLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxJQUFJLEVBQUU7WUFDbkMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRTtnQkFDdEgsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDOUQ7WUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQzlDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDO2dCQUM3RixLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckUsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMvRDtTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBeUJJO0lBRUosd0RBQWlCLEdBQWpCLFVBQWtCLFVBQVU7UUFBNUIsaUJBbUNDO1FBbENHLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRTtZQUN2SCxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxLQUFLLEtBQUssS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQTlFLENBQThFLENBQUMsQ0FBQztZQUNsSSxJQUFJLElBQUksRUFBRTtnQkFDTixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUM1SCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxPQUFPO2FBQ1Y7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7WUFDL0IsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsK0NBQStDLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMvSCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pDLE9BQU87U0FDVjtRQUNELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoRixJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzNCLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDaEgsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN6QyxPQUFPO1NBQ1Y7UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTtZQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUNuRSxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSx5Q0FBeUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO2dCQUN6SCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzVDO2lCQUFNO2dCQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM3QztTQUNKO2FBQU07WUFDSCxJQUFNLFNBQVMsR0FBRztnQkFDZCxPQUFPLEVBQUUsa0NBQWtDO2dCQUMzQyxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLElBQUk7YUFDakQsQ0FBQztZQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsMkRBQW9CLEdBQXBCLFVBQXFCLFFBQWdCO1FBQ2pDLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTtZQUMvRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUN4QixhQUFhLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ2pEO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLGFBQWEsQ0FBQztJQUN6QixDQUFDO0lBQ0QsbUVBQTRCLEdBQTVCOztRQUNJLElBQUksaUJBQTBDLENBQUM7UUFDL0MsSUFBTSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ2pFLGlCQUFpQixHQUFHO1lBQ2hCLGFBQWEsRUFBRTtnQkFDWCxFQUFFLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO2FBQ3RHO1lBQ0QsZUFBZSxFQUFFLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUU7WUFDaEwsV0FBVyxFQUFFLHFCQUFxQixDQUFDLFdBQVc7WUFDOUMsU0FBUyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEosSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7WUFDeEQsT0FBTyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hILHFCQUFxQixDQUFDLE9BQU8sQ0FBQztZQUNsQyxPQUFPLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQztZQUMzSixRQUFRLEVBQUUsSUFBSTtZQUNkLFNBQVMsRUFBRSxLQUFLO1lBQ2hCLGVBQWUsRUFBRSxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsUUFBUTtZQUM5RSxtQkFBbUIsRUFBRSxxQkFBcUIsQ0FBQyxlQUFlO1lBQzFELHNCQUFzQixFQUFFLHFCQUFxQixDQUFDLGtCQUFrQjtZQUNoRSxnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxnQkFBZ0I7WUFDeEQsU0FBUyxFQUFFO2dCQUNQLFdBQVcsRUFBRTtvQkFDVCxFQUFFLEVBQUUscUJBQXFCLENBQUMsTUFBTTtpQkFDbkM7YUFDSjtZQUNELFdBQVcsRUFBRTtnQkFDVCxhQUFhLEVBQUU7b0JBQ1gsRUFBRSxFQUFFLHFCQUFxQixDQUFDLFFBQVE7aUJBQ3JDO2FBQ0o7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsTUFBTSxFQUFFO29CQUNKLEVBQUUsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7aUJBQzdEO2FBQ0o7WUFDRCxVQUFVLEVBQUU7Z0JBQ1IsU0FBUyxFQUFFO29CQUNQLEVBQUUsRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUU7aUJBQ25FO2FBQ0o7WUFDRCxjQUFjLEVBQUU7Z0JBQ1osb0JBQW9CLEVBQUU7b0JBQ2xCLEVBQUUsUUFBRSxxQkFBcUIsQ0FBQyxvQkFBb0IsMENBQUUsSUFBSTtpQkFDdkQ7YUFDSjtZQUNELGlCQUFpQixFQUFFO2dCQUNmLG9CQUFvQixFQUFFO29CQUNsQixFQUFFLFFBQUUscUJBQXFCLENBQUMsdUJBQXVCLDBDQUFFLElBQUk7aUJBQzFEO2FBQ0o7WUFDRCxxQkFBcUIsRUFBRTtnQkFDbkIsVUFBVSxFQUFFO29CQUNSLE1BQU0sUUFBRSxxQkFBcUIsQ0FBQyxnQkFBZ0IsMENBQUUsSUFBSTtpQkFDdkQ7YUFDSjtZQUNELHdCQUF3QixFQUFFO2dCQUN0QixVQUFVLEVBQUU7b0JBQ1IsTUFBTSxRQUFFLHFCQUFxQixDQUFDLG1CQUFtQiwwQ0FBRSxJQUFJO2lCQUMxRDthQUNKO1NBQ0osQ0FBQztRQUNGLE9BQU8saUJBQWlCLENBQUM7SUFDN0IsQ0FBQztJQUVELHNEQUFlLEdBQWY7UUFBQSxpQkEwRUM7UUF6RUcsSUFBTSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQU0sZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTVCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDaEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxXQUFXO2dCQUNqQyxJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN4QyxJQUFNLGtCQUFrQixHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxLQUFLLEVBQXBDLENBQW9DLENBQUMsQ0FBQztvQkFDdEcsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDM0IsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDcEM7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBTSx3QkFBd0IsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFVBQUEsbUJBQW1CLElBQUksT0FBQSxtQkFBbUIsQ0FBQyxLQUFLLEtBQUssV0FBVyxDQUFDLEtBQUssRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDO29CQUM3SSxJQUFJLHdCQUF3QixLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUNqQyxJQUFNLGtCQUFrQixHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxLQUFLLEVBQXBDLENBQW9DLENBQUMsQ0FBQzt3QkFDdEcsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTs0QkFDM0IsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt5QkFDcEM7cUJBQ0o7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDLEdBQUcsQ0FBQyxVQUFDLGFBQWE7Z0JBQ3BELE9BQU87b0JBQ0gsYUFBYSxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhO29CQUNuRCxRQUFRLEVBQUU7d0JBQ04sRUFBRSxFQUFFLEVBQUU7d0JBQ04sTUFBTSxFQUFFLEVBQUU7d0JBQ1YsTUFBTSxFQUFFLGFBQWEsQ0FBQyxLQUFLO3dCQUMzQixNQUFNLEVBQUUsS0FBSztxQkFDaEI7b0JBQ0QsTUFBTSxFQUFFLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNO29CQUMxQyxvQkFBb0IsRUFBRSxLQUFJLENBQUMsSUFBSTtpQkFFbEMsQ0FBQztZQUNOLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQzlDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxlQUFlO2dCQUN6QyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDbEMsSUFBTSx3QkFBd0IsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFVBQUEsbUJBQW1CLElBQUksT0FBQSxtQkFBbUIsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLEtBQUssRUFBbkQsQ0FBbUQsQ0FBQyxDQUFDO29CQUNqSixJQUFJLHdCQUF3QixJQUFJLENBQUMsRUFBRTt3QkFDL0IsSUFBTSxhQUFhLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxFQUF4QyxDQUF3QyxDQUFDLENBQUM7d0JBQ3ZHLElBQUksYUFBYSxLQUFLLENBQUMsQ0FBQyxFQUFFOzRCQUN0QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7eUJBQzFDO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILElBQU0sa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLEVBQTdDLENBQTZDLENBQUMsQ0FBQztvQkFDekgsSUFBSSxrQkFBa0IsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDM0IsSUFBTSxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsS0FBSyxLQUFLLGVBQWUsQ0FBQyxLQUFLLEVBQXhDLENBQXdDLENBQUMsQ0FBQzt3QkFDOUcsSUFBTSx3QkFBd0IsR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEtBQUssS0FBSyxlQUFlLENBQUMsS0FBSyxFQUFwQyxDQUFvQyxDQUFDLENBQUM7d0JBQ25ILElBQUksb0JBQW9CLEtBQUssQ0FBQyxDQUFDLElBQUksd0JBQXdCLElBQUksQ0FBQyxFQUFFOzRCQUM5RCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7eUJBQzFDO3FCQUNKO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFVBQUMsUUFBUTtnQkFDckQsT0FBTztvQkFDSCxhQUFhLEVBQUUsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWE7b0JBQ25ELFFBQVEsRUFBRTt3QkFDTixFQUFFLEVBQUUsRUFBRTt3QkFDTixNQUFNLEVBQUUsRUFBRTt3QkFDVixNQUFNLEVBQUUsUUFBUSxDQUFDLEtBQUs7d0JBQ3RCLE1BQU0sRUFBRSxRQUFRO3FCQUNuQjtvQkFDRCxNQUFNLEVBQUUsS0FBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07b0JBQzFDLG9CQUFvQixFQUFFLEtBQUksQ0FBQyxJQUFJO2lCQUVsQyxDQUFDO1lBQ04sQ0FBQyxDQUFDLENBQUM7U0FFTjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzdKLENBQUM7SUFFRCwrREFBd0IsR0FBeEIsVUFBeUIsVUFBVTtRQUMvQixJQUFNLGlCQUFpQixHQUE0QixJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztRQUN2RixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQsc0RBQWUsR0FBZixVQUFnQixpQkFBaUIsRUFBRSxVQUFVO1FBQTdDLGlCQWdHQztRQS9GRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixFQUFFO1lBQ2pELGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx3QkFBd0IsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0csSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtnQkFDbEksaUJBQWlCLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDeEMsSUFBTSxVQUFVLEdBQUc7b0JBQ2YsaUJBQWlCLEVBQUUsaUJBQWlCO29CQUNwQyxvQkFBb0IsRUFBRTt3QkFDbEIsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLFlBQVk7cUJBQ3pDO2lCQUNKLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxVQUFVLENBQUM7cUJBQ3hHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJOzJCQUN2RyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUNuSyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO3dCQUN0SCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUN6QyxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDOzRCQUMxQixhQUFhLEVBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBRTs0QkFDekUsTUFBTSxFQUFFLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNOzRCQUMxQyxTQUFTLEVBQUUsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7NEJBQ2hELFVBQVUsRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUs7NEJBQ2pFLFVBQVUsRUFBRSxVQUFVO3lCQUN6QixDQUFDLENBQUM7cUJBQ047eUJBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLOzJCQUMvRyxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFO3dCQUNwRixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDeEgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDNUM7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsSUFBTSxTQUFTLEdBQUc7NEJBQ2QsT0FBTyxFQUFFLG9EQUFvRDs0QkFDN0QsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO3lCQUNsRCxDQUFDO3dCQUNGLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7cUJBQzVDO2dCQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsb0RBQW9ELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDckksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDN0MsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQzdCO1NBQ0o7YUFBTTtZQUNILElBQU0sT0FBTyxHQUFHO2dCQUNaLGlCQUFpQixFQUFFLGlCQUFpQjthQUN2QyxDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxPQUFPLENBQUM7aUJBQ3JHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJO3VCQUN2RyxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBRSxFQUFFO29CQUNuSyxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUNuSCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxJQUFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUU7d0JBQ3hDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7NEJBQzFCLGFBQWEsRUFBRSxRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxFQUFFOzRCQUN6RSxNQUFNLEVBQUUsS0FBSSxDQUFDLHNCQUFzQixDQUFDLE1BQU07NEJBQzFDLFNBQVMsRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUzs0QkFDaEQsVUFBVSxFQUFFLElBQUk7NEJBQ2hCLFVBQVUsRUFBRSxVQUFVO3lCQUN6QixDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQzs0QkFDMUIsYUFBYSxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEVBQUU7NEJBQ3pFLE1BQU0sRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTTs0QkFDMUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTOzRCQUNoRCxVQUFVLEVBQUUsS0FBSzs0QkFDakIsVUFBVSxFQUFFLFVBQVU7eUJBQ3pCLENBQUMsQ0FBQztxQkFDTjtpQkFFSjtxQkFBTSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLEtBQUssSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUs7dUJBQy9HLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7b0JBQ3BGLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUN4SCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM1QztxQkFBTTtvQkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixJQUFNLFNBQVMsR0FBRzt3QkFDZCxPQUFPLEVBQUUsb0RBQW9EO3dCQUM3RCxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7cUJBQ2xELENBQUM7b0JBQ0YsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDNUM7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQU0sU0FBUyxHQUFHO29CQUNkLE9BQU8sRUFBRSxvREFBb0Q7b0JBQzdELElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSztpQkFDbEQsQ0FBQztnQkFDRixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7UUFVSTtJQUVKLG9EQUFhLEdBQWIsVUFBYyxJQUFJLEVBQUUsbUJBQW1CO1FBQ25DLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxtQkFBbUIsRUFBRTtnQkFDckIsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFyQixDQUFxQixDQUFDLENBQUM7Z0JBQzFGLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDN0M7aUJBQU07Z0JBQ0gsSUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFyQixDQUFxQixDQUFDLENBQUM7Z0JBQ3ZGLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDN0M7U0FDSjthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFFRCxxREFBYyxHQUFkLFVBQWUsTUFBTTtRQUFyQixpQkFpQkM7UUFoQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxNQUFNLEVBQUU7Z0JBQ1IsSUFBTSxVQUFVLEdBQUc7b0JBQ2YsTUFBTSxFQUFFLE1BQU07aUJBQ2pCLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNwRSxJQUFNLFlBQVksR0FBRyxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsTUFBTSxFQUFoQyxDQUFnQyxDQUFDLENBQUM7b0JBQ2pHLEtBQUksQ0FBQyxhQUFhLEdBQUcsWUFBWSxDQUFDO29CQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0RBQWlCLEdBQWpCLFVBQWtCLE1BQU07UUFBeEIsaUJBaUJDO1FBaEJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksTUFBTSxFQUFFO2dCQUNSLElBQU0sVUFBVSxHQUFHO29CQUNmLE1BQU0sRUFBRSxNQUFNO2lCQUNqQixDQUFDO2dCQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDcEUsSUFBTSxZQUFZLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEtBQUssUUFBUSxDQUFDLE1BQU0sRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDO29CQUNwRyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsWUFBWSxDQUFDO29CQUNyQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseURBQWtCLEdBQWxCLFVBQW1CLFdBQVc7UUFDMUIsSUFBSSxXQUFXLENBQUMsa0JBQWtCLElBQUksV0FBVyxDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDLElBQUksT0FBTyxXQUFXLENBQUMsa0JBQWtCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFFO1lBQ25MLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsRUFBOUUsQ0FBOEUsQ0FBQyxDQUFDO1lBQ2hKLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDN0M7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCw0REFBcUIsR0FBckIsVUFBc0IsV0FBVztRQUM3QixJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMscUJBQXFCLElBQUksT0FBTyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUFFO1lBQzFJLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsRUFBakYsQ0FBaUYsQ0FBQyxDQUFDO1lBQ3RKLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDN0M7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCwrREFBd0IsR0FBeEIsVUFBeUIsV0FBVztRQUNoQyxJQUFJLFdBQVcsRUFBRTtZQUNiLE9BQU8sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDdkY7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxrRUFBMkIsR0FBM0IsVUFBNEIsV0FBVztRQUNuQyxJQUFJLFdBQVcsRUFBRTtZQUNiLE9BQU8sQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDN0Y7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxnRUFBeUIsR0FBekI7UUFBQSxpQkE4VEM7UUE3VEcsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQztRQUMxRCxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN0RSxJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQU0sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDO1FBQzVFLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQjtZQUMvRixJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ2pILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7UUFDMUYsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUN4RixJQUFNLE9BQU8sR0FBRyxzQkFBc0IsQ0FBQztRQUN2QyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDO1NBQ2xEO1FBQ0QsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNiLElBQUksZ0JBQWdCLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxTQUFTLENBQUM7b0JBQ2pDLGVBQWUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWTtxQkFDcEMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2hHLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDOzRCQUNoSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDOzRCQUMzRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsWUFBWTtxQkFDaEgsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNwQixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUM7NEJBQzVHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJO3FCQUM1SyxDQUFDO29CQUNGLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDckIsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxZQUFZO3FCQUNuRixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDakcsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDL0Usa0JBQWtCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2hDLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3BDLENBQUM7b0JBQ0YsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQzFGLENBQUM7b0JBQ0YsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUMzRCxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3RELENBQUM7b0JBQ0YsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDM0Usb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGVBQWUsS0FBSyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOzRCQUMzRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3JKLENBQUM7b0JBQ0YsdUJBQXVCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDL0Usb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDNUUsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzdHLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFlBQVksR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxZQUFZLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVU7b0JBQ3pHLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO2dCQUM3RixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksU0FBUyxDQUFDO29CQUNqQyxlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxJQUFJO3dCQUMvQixRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2hHLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7d0JBQ3pCLFFBQVEsRUFBRSxZQUFZLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7cUJBQzlHLENBQUM7b0JBQ0Ysb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUM7d0JBQ25ELFFBQVEsRUFBRSxZQUFZLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7cUJBQzlHLENBQUM7b0JBQ0YsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2pDLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCO3dCQUM1QixRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLHVCQUF1QixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNyQyxLQUFLLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDO3dCQUN0RCxRQUFRLEVBQUUsWUFBWSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZO3FCQUM5RyxDQUFDO29CQUNGLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDcEIsS0FBSyxFQUFFLG1CQUFtQixDQUFDLFdBQVcsSUFBSSxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3JKLFFBQVEsRUFBRSxZQUFZLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7cUJBQzlHLENBQUM7b0JBQ0YsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN0QixLQUFLLEVBQUUsbUJBQW1CLENBQUMsYUFBYSxJQUFJLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQy9KLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN6QixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3JCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxRQUFRO3dCQUNuQyxRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsV0FBVyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUN6QixLQUFLLEVBQUUsT0FBTyxtQkFBbUIsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLENBQUM7NEJBQ3hELG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDeEMsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2hDLFVBQVUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDeEIsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSTt3QkFDbEcsUUFBUSxFQUFFLElBQUk7cUJBQ2pCLENBQUM7b0JBQ0Ysd0hBQXdIO29CQUN4SCxrQkFBa0IsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDaEMsS0FBSyxFQUFFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDNUQsUUFBUSxFQUFFLFlBQVksSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLFlBQVksS0FBSyxZQUFZLENBQUMsWUFBWTtxQkFDOUcsQ0FBQztvQkFDRixlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUM7d0JBQ3pELFFBQVEsRUFBRSxZQUFZLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7cUJBQzlHLENBQUM7b0JBQ0Ysb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzdGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLEVBQ2hKLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzNFLENBQUMsQ0FBQztnQkFDSCxzREFBc0Q7Z0JBQ3RELGlDQUFpQzthQUNwQztTQUNKO2FBQU07WUFDSCxJQUFJLGdCQUFnQixFQUFFO2dCQUNsQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksU0FBUyxDQUFDO29CQUNqQyxlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3BDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO29CQUNoRyxRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQzs0QkFDaEgsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzs0QkFDM0UsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ2hILEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDcEIsS0FBSyxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDOzRCQUM1RyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSTtxQkFDNUssQ0FBQztvQkFDRixXQUFXLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDakcsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDL0Usa0JBQWtCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2hDLEtBQUssRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3BDLENBQUM7b0JBQ0YsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQzFGLENBQUM7b0JBQ0YsZ0JBQWdCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDOzRCQUMzRCxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3RELENBQUM7b0JBQ0YsbUJBQW1CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDM0Usb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLGVBQWUsS0FBSyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOzRCQUMzRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLFlBQVk7cUJBQ3JKLENBQUM7b0JBQ0YsdUJBQXVCLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsQ0FBQztvQkFDL0Usb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzdGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUM3RyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxZQUFZLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQyxZQUFZLEtBQUssWUFBWSxDQUFDLFlBQVk7b0JBQzVGLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO2dCQUM3RixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksU0FBUyxDQUFDO29CQUNqQyxlQUFlLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQzdCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxJQUFJO3dCQUMvQixRQUFRLEVBQUUsWUFBWTtxQkFDekIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7b0JBQ2hHLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGFBQWE7d0JBQ3pCLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixDQUFDO29CQUNGLG9CQUFvQixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNsQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDO3dCQUNuRCxRQUFRLEVBQUUsWUFBWTtxQkFDekIsQ0FBQztvQkFDRixtQkFBbUIsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDakMsS0FBSyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7d0JBQzVCLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixDQUFDO29CQUNGLHVCQUF1QixFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNyQyxLQUFLLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDO3dCQUN0RCxRQUFRLEVBQUUsWUFBWTtxQkFDekIsQ0FBQztvQkFDRixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3BCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxXQUFXLElBQUksbUJBQW1CLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUNySixRQUFRLEVBQUUsSUFBSTtxQkFDakIsQ0FBQztvQkFDRixRQUFRLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3RCLEtBQUssRUFBRSxtQkFBbUIsQ0FBQyxhQUFhLElBQUksbUJBQW1CLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDL0osUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLFdBQVcsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDekIsS0FBSyxFQUFFLE9BQU8sbUJBQW1CLENBQUMsV0FBVyxLQUFLLFFBQVEsQ0FBQyxDQUFDOzRCQUN4RCxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3hDLFFBQVEsRUFBRSxZQUFZO3FCQUN6QixFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7d0JBQ2xHLFFBQVEsRUFBRSxJQUFJO3FCQUNqQixDQUFDO29CQUNGLHdIQUF3SDtvQkFDeEgsa0JBQWtCLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsMkJBQTJCLENBQUMsbUJBQW1CLENBQUM7d0JBQzVELFFBQVEsRUFBRSxZQUFZO3FCQUN6QixDQUFDO29CQUNGLGVBQWUsRUFBRSxJQUFJLFdBQVcsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxtQkFBbUIsQ0FBQzt3QkFDekQsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCLENBQUM7b0JBQ0Ysb0JBQW9CLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFFLENBQUM7b0JBQzdGLGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLEVBQ2hKLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzNFLENBQUMsQ0FBQzthQUNOO1NBQ0o7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUN2RixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUMsUUFBdUIsSUFBSyxPQUFBLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsRUFBckUsQ0FBcUUsQ0FBQyxDQUFDLENBQUM7UUFFN0csSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUNoSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDO1FBQ2hILElBQUksQ0FBQyxzQkFBc0IsQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUM7UUFDeEgsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQztRQUV4SCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDbkcsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUNuSCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM1RDtZQUNELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDckYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMzQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQ3hGLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDMUUsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEtBQUssb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7WUFDdEcsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUN6SCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUMvRDtZQUNELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDeEYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUM5QyxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQ3JGLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDN0UsQ0FBQyxDQUFDLENBQUM7YUFDTjtTQUNKO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxpQkFBaUI7WUFDN0UsS0FBSSxDQUFDLHlCQUF5QixDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxzSUFBc0k7UUFFdEksSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxtQkFBbUI7WUFDcEYsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDcEQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ25FO1lBQ0QsSUFBSSxLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQy9EO1lBQ0QsS0FBSSxDQUFDLDZCQUE2QixDQUFDLG1CQUFtQixFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQztRQUVILCtJQUErSTtRQUUvSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsbUJBQW1CO1lBQ3ZGLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLEVBQUU7Z0JBQ3ZELEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN0RTtZQUNELElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ25ELEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNsRTtZQUNELElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3BELEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNuRTtZQUNELEtBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0UsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsU0FBUztZQUMvRSxJQUFJLFNBQVMsSUFBSSxPQUFPLFNBQVMsS0FBSyxRQUFRLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxDQUFDLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUM1RixLQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBTSxpQkFBaUIsR0FBRyxLQUFJLENBQUMsc0JBQXNCLENBQUMsMEJBQTBCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUE5QixDQUE4QixDQUFDLENBQUM7Z0JBQzVJLElBQUksaUJBQWlCLEVBQUU7b0JBQ25CLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2lCQUNsRjthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsWUFBWTtZQUNyRixJQUFJLFlBQVksSUFBSSxPQUFPLFlBQVksS0FBSyxRQUFRLEVBQUU7Z0JBQ2xELElBQUksQ0FBQyxDQUFDLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUM1RixLQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQzdDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBTSxvQkFBb0IsR0FBRyxLQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEtBQUssWUFBWSxFQUFqQyxDQUFpQyxDQUFDLENBQUM7Z0JBQy9JLElBQUksb0JBQW9CLEVBQUU7b0JBQ3RCLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2lCQUN4RjthQUNKO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFO1lBQ2hELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxVQUFVO2dCQUM1RSxJQUFJLENBQUMsQ0FBQyxVQUFVLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxDQUFDLEVBQUU7b0JBQ2pELElBQU0sYUFBYSxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLFdBQVcsS0FBSyxVQUFVLEVBQWhDLENBQWdDLENBQUMsQ0FBQztvQkFDdkksSUFBSSxhQUFhLEVBQUU7d0JBQ2YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUMxRTtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFO1lBQ25ELElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhO2dCQUNsRixJQUFJLENBQUMsQ0FBQyxhQUFhLElBQUksT0FBTyxhQUFhLEtBQUssUUFBUSxDQUFDLEVBQUU7b0JBQ3ZELElBQU0sZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQSxRQUFRLElBQUksT0FBQSxRQUFRLENBQUMsV0FBVyxLQUFLLGFBQWEsRUFBdEMsQ0FBc0MsQ0FBQyxDQUFDO29CQUNoSixJQUFJLGdCQUFnQixFQUFFO3dCQUNsQixLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztxQkFDaEY7aUJBQ0o7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsNEJBQTRCLEVBQUUsQ0FBQztJQUM1RCxDQUFDO0lBRUQsd0RBQWlCLEdBQWpCO1FBQUEsaUJBNEJDO1FBM0JHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sZ0JBQWdCLEdBQUc7Z0JBQ3JCLFNBQVMsRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUzthQUNuRCxDQUFDO1lBQ0YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMseUJBQXlCLEVBQUUsS0FBSSxDQUFDLGtDQUFrQyxFQUFFLGdCQUFnQixDQUFDO2lCQUNuSCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7b0JBQzlCLElBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7b0JBQ3BDLElBQUksVUFBVSxDQUFDLGFBQWEsSUFBSSxVQUFVLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7d0JBQ3pFLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQkFDM0Y7b0JBQ0QsSUFBSSxVQUFVLENBQUMsU0FBUyxJQUFJLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7d0JBQzlELEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7cUJBQ2hGO29CQUNELEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQztpQkFDbEU7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUscURBQXFELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDdEksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFEQUFjLEdBQWQ7UUFBQSxpQkF3QkM7UUF2QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxnQkFBZ0IsR0FBRztnQkFDckIsU0FBUyxFQUFFO29CQUNQLEVBQUUsRUFBRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTTtpQkFDekM7YUFDSixDQUFDO1lBQ0YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsc0JBQXNCLEVBQUUsS0FBSSxDQUFDLCtCQUErQixFQUFFLGdCQUFnQixDQUFDO2lCQUM3RyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7b0JBQzNCLElBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDNUQ7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsa0RBQWtELEVBQUUsSUFBSSxFQUFFLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDbkksS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDREQUFxQixHQUFyQjtRQUFBLGlCQXFCQztRQXBCRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRTFCLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQztpQkFDN0UsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO29CQUNsQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztvQkFDdkUsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ2xHLEtBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2lCQUN0STtnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSx5REFBeUQsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUMxSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0RBQWEsR0FBYjtRQUFBLGlCQXdDQztRQXZDRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQjs7Ozs7Ozs7Ozs7Ozs7Ozs7c0JBaUJVO1lBRU4sSUFBSSxRQUFRLEdBQUksS0FBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUE7WUFDL0UsbUNBQW1DO1lBQzlCLElBQUksUUFBUSxFQUFFO2dCQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMxQixRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDekI7Z0JBQ0QsS0FBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixHQUFHLFFBQVEsQ0FBQzthQUNsRTtpQkFBTTtnQkFDSCxLQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLEdBQUcsRUFBRSxDQUFDO2FBQzVEO1lBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEI7Ozs7O2tCQUtNO1FBQ2QsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZ0RBQVMsR0FBVCxVQUFVLGVBQW1DO1FBQTdDLGlCQXVCQztRQXRCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztpQkFDcEUsU0FBUyxDQUFDLFVBQUEsY0FBYztnQkFDckIsSUFBSSxjQUFjLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxFQUFFO3dCQUNoQyxjQUFjLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDckM7b0JBQ0QsS0FBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJO3dCQUMxRSxPQUFPLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQzFFLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNO29CQUNILEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUM7aUJBQzFEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLDhDQUE4QyxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQy9ILEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtEQUFXLEdBQVg7UUFBQSxpQkF3RUM7UUF2RUcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtZQUNoRixjQUFjLEVBQUUsS0FBSztZQUNyQixZQUFZLEVBQUUsSUFBSTtTQUNyQixDQUFDO1FBRUYsSUFBTSxtQkFBbUIsR0FBRztZQUN4QixNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtZQUNoRixjQUFjLEVBQUUsSUFBSTtZQUNwQixZQUFZLEVBQUUsS0FBSztTQUN0QixDQUFDO1FBRUYsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7aUJBQ3hHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQzVELFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLO29CQUN0RSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRTtvQkFFbEYsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO29CQUN2QixJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztvQkFDMUIsSUFBTSxZQUFVLEdBQUcsRUFBRSxDQUFDO29CQUN0QixJQUFNLGVBQWEsR0FBRyxFQUFFLENBQUM7b0JBRXpCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ25FLGFBQWEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQUM7cUJBQ3ZFO29CQUNELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ25FLGdCQUFnQixHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQztxQkFDMUU7b0JBQ0QsYUFBYSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDO29CQUNsRSxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQztvQkFFckUsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDbkUsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7NEJBQ2xCLFlBQVUsQ0FBQyxJQUFJLENBQUM7Z0NBQ1osSUFBSSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3pDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTztnQ0FDbkIsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUN0QixRQUFRLEVBQUUsSUFBSSxDQUFDLGFBQWE7NkJBQy9CLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjtvQkFFRCxJQUFJLGdCQUFnQixJQUFJLGdCQUFnQixDQUFDLE1BQU0sSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUM1RSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJOzRCQUNyQixlQUFhLENBQUMsSUFBSSxDQUFDO2dDQUNmLElBQUksRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFO2dDQUN6QyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU87Z0NBQ25CLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtnQ0FDdEIsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhOzZCQUMvQixDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsS0FBSSxDQUFDLHFCQUFxQixHQUFHLFlBQVUsQ0FBQztvQkFDeEMsS0FBSSxDQUFDLHdCQUF3QixHQUFHLGVBQWEsQ0FBQztvQkFDOUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLDBCQUEwQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUM7b0JBQ2xHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLHdCQUF3QixDQUFDO2lCQUNyRztxQkFBTTtvQkFDSCxLQUFJLENBQUMscUJBQXFCLEdBQUcsRUFBRSxDQUFDO29CQUNoQyxLQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO2lCQUN0QztnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUM1SCxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzREFBZSxHQUFmLFVBQWdCLE1BQU07UUFBdEIsaUJBb0JDO1FBbkJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBRTtnQkFDekMsSUFBTSxVQUFVLEdBQUc7b0JBQ2YsTUFBTSxFQUFFLE1BQU07aUJBQ2pCLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNwRSxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osSUFBTSxTQUFTLEdBQUcsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ25GLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwRUFBbUMsR0FBbkMsVUFBb0MsYUFBYTtRQUFqRCxpQkErREM7UUE5REcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxtQ0FBbUMsQ0FBQyxhQUFhLENBQUM7aUJBQzdELFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGlCQUFpQixFQUFFO29CQUN4QyxJQUFNLGFBQWEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLENBQUM7b0JBQy9FLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQ25FLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFROzRCQUMxQixJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dDQUNoRixLQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxZQUFZO29DQUN0RixJQUFJLFlBQVksRUFBRTt3Q0FDZCxJQUFNLGVBQWUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLGFBQWEsQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLE1BQU0sRUFBM0MsQ0FBMkMsQ0FBQyxDQUFDO3dDQUMvRyxJQUFJLENBQUMsZUFBZSxFQUFFOzRDQUNsQixLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnREFDckIsSUFBSSxFQUFFLFlBQVksQ0FBQyxRQUFRO2dEQUMzQixLQUFLLEVBQUUsWUFBWSxDQUFDLE1BQU07Z0RBQzFCLFdBQVcsRUFBRSxZQUFZLENBQUMsUUFBUTs2Q0FDckMsQ0FBQyxDQUFDOzRDQUNILEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUM7Z0RBQzNCLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDM0IsS0FBSyxFQUFFLFlBQVksQ0FBQyxNQUFNO2dEQUMxQixXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVE7NkNBQ3JDLENBQUMsQ0FBQzt5Q0FDTjtxQ0FDSjtvQ0FDRCxLQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO3dDQUM1RyxJQUFNLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxZQUFZLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt3Q0FDbkYsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxLQUFLLEtBQUssWUFBWSxDQUFDLE1BQU0sRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDO3dDQUNwRyxJQUFJLGFBQWEsSUFBSSxDQUFDLEVBQUU7NENBQ3BCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUM7eUNBQzVFOzZDQUFNOzRDQUNILEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7Z0RBQ3pCLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUTtnREFDM0IsS0FBSyxFQUFFLFlBQVksQ0FBQyxNQUFNO2dEQUMxQixXQUFXLEVBQUUsWUFBWSxDQUFDLFFBQVE7Z0RBQ2xDLFdBQVcsRUFBRSxtQkFBbUI7NkNBQ25DLENBQUMsQ0FBQzt5Q0FDTjtvQ0FDTCxDQUFDLENBQUMsQ0FBQztvQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztvQ0FDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dDQUN4QixDQUFDLENBQUMsQ0FBQzs2QkFDTjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NkJBQ3ZCO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7d0JBQ2xCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7aUJBQ0o7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFDRztnQkFDSSxJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxnRUFBZ0UsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqSixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDZixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtREFBWSxHQUFaO1FBQUEsaUJBK0NDO1FBOUNHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksS0FBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxTQUFTLEtBQUssU0FBUyxDQUFDLGFBQWEsRUFBRTtnQkFDM0UsSUFBSSxzQkFBc0IsR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLHNCQUFzQixHQUFHO29CQUNyQixjQUFjLEVBQUUsTUFBTTtvQkFDdEIsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsTUFBTSxFQUFFLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7b0JBQ2hGLGFBQWEsRUFBRSxJQUFJO29CQUNuQixZQUFZLEVBQUUsS0FBSztvQkFDbkIsUUFBUSxFQUFFLElBQUk7aUJBQ2pCLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsc0JBQXNCLENBQUM7cUJBQ2xELFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTt3QkFDbkQsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQzt3QkFDN0QsSUFBTSxPQUFLLEdBQUcsRUFBRSxDQUFDO3dCQUNqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUNwRCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDakIsSUFBTSxRQUFRLEdBQUcsT0FBSyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQUUsRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDO2dDQUN0RCxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLE9BQUssQ0FBQyxJQUFJLENBQUM7d0NBQ1AsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFFO3dDQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRTt3Q0FDZCxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7cUNBQ3pCLENBQUMsQ0FBQztpQ0FDTjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFDRCxLQUFJLENBQUMsWUFBWSxHQUFHLE9BQUssQ0FBQztxQkFDN0I7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7cUJBQzFCO29CQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFO29CQUNDLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLG9EQUFvRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3JJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNsQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxxREFBYyxHQUFkLFVBQWUsS0FBTSxFQUFFLGtCQUFtQjtRQUExQyxpQkF1S0M7UUF0S0csSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFJLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLG9CQUFvQixHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLFFBQVEsQ0FBQztRQUNiLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixJQUFNLGNBQWMsR0FBRyxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLENBQUM7WUFDeEosUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNqQixvQkFBb0IsR0FBRztnQkFDbkIsY0FBYyxFQUFFLGNBQWM7Z0JBQzlCLE1BQU0sRUFBRSxjQUFjLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUMzRCxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUMvSixNQUFNLEVBQUUsY0FBYyxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDN0ksYUFBYSxFQUFFLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7Z0JBQ2pELFlBQVksRUFBRSxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLO2FBQ25ELENBQUM7U0FDTDthQUFNLElBQUksSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixFQUFFO1lBQ3hELElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztZQUNuSixJQUFJLGFBQWEsS0FBSyxPQUFPLEVBQUU7Z0JBQzNCLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDN0YsSUFBTSxjQUFjLEdBQUcsY0FBYyxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUN0TCxRQUFRLEdBQUcsYUFBYSxDQUFDO2dCQUN6QixvQkFBb0IsR0FBRztvQkFDbkIsY0FBYyxFQUFFLGNBQWM7b0JBQzlCLE1BQU0sRUFBRSxjQUFjLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNwSCxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtvQkFDaEYsYUFBYSxFQUFFLEtBQUs7b0JBQ3BCLFlBQVksRUFBRSxJQUFJO2lCQUNyQixDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsSUFBTSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUM7Z0JBQ2xHLElBQU0sc0JBQXNCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLHdCQUF3QixDQUFDO2dCQUN4RyxJQUFNLG1CQUFtQixHQUFHLG1CQUFtQixLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztvQkFDcEYsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzNHLElBQU0sc0JBQXNCLEdBQUcsc0JBQXNCLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO29CQUMxRixJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDOUcscUJBQXFCLEdBQUc7b0JBQ3BCLGNBQWMsRUFBRSxtQkFBbUI7b0JBQ25DLE1BQU0sRUFBRSxtQkFBbUIsS0FBSyxlQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7d0JBQ2hFLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUM3RCxNQUFNLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRTtvQkFDaEYsYUFBYSxFQUFFLEtBQUs7b0JBQ3BCLFlBQVksRUFBRSxJQUFJO2lCQUNyQixDQUFDO2dCQUNGLHdCQUF3QixHQUFHO29CQUN2QixjQUFjLEVBQUUsc0JBQXNCO29CQUN0QyxNQUFNLEVBQUUsc0JBQXNCLEtBQUssZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLHNCQUFzQixFQUFFLGFBQWEsS0FBSyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3ZGLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO29CQUNoRixhQUFhLEVBQUUsSUFBSTtvQkFDbkIsWUFBWSxFQUFFLEtBQUs7aUJBQ3RCLENBQUM7YUFDTDtTQUNKO2FBQU07WUFDSCxxQkFBcUIsR0FBRztnQkFDcEIsY0FBYyxFQUFFLE1BQU07Z0JBQ3RCLE1BQU0sRUFBRSxFQUFFO2dCQUNWLE1BQU0sRUFBRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFO2dCQUNoRixhQUFhLEVBQUUsS0FBSztnQkFDcEIsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQztZQUNGLHdCQUF3QixHQUFHO2dCQUN2QixjQUFjLEVBQUUsTUFBTTtnQkFDdEIsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsTUFBTSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hGLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixZQUFZLEVBQUUsS0FBSzthQUN0QixDQUFDO1NBQ0w7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLG9CQUFvQixFQUFFO2dCQUN0QixLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQztxQkFDaEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO3dCQUNuRCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO3dCQUM3RCxJQUFNLE9BQUssR0FBRyxFQUFFLENBQUM7d0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3BELFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dDQUNqQixJQUFNLFFBQVEsR0FBRyxPQUFLLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFuQixDQUFtQixDQUFDLENBQUM7Z0NBQ3RELElBQUksQ0FBQyxRQUFRLEVBQUU7b0NBQ1gsT0FBSyxDQUFDLElBQUksQ0FBQzt3Q0FDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUU7d0NBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFO3dDQUNkLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtxQ0FDekIsQ0FBQyxDQUFDO2lDQUNOOzRCQUNMLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUNELElBQUksUUFBUSxLQUFLLFVBQVUsRUFBRTs0QkFDekIsS0FBSSxDQUFDLHVCQUF1QixHQUFHLE9BQUssQ0FBQzs0QkFDckMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsdUJBQXVCLENBQUM7eUJBQ2hHOzZCQUFNOzRCQUNILEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxPQUFLLENBQUM7NEJBQ2xDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDO3lCQUNoRztxQkFDSjt5QkFBTTt3QkFDSCxJQUFJLFFBQVEsS0FBSyxVQUFVLEVBQUU7NEJBQ3pCLEtBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7eUJBQ3JDOzZCQUFNOzRCQUNILEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLENBQUM7eUJBQ2xDO3FCQUNKO29CQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixJQUFNLFNBQVMsR0FBRyxFQUFFLE9BQU8sRUFBRSxzREFBc0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO29CQUN2SSxLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUN6QyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7cUJBQ3hILFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUk7d0JBQ3ZELFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7d0JBRTdDLElBQU0sU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ2pFLElBQU0sWUFBWSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ3BFLElBQU0sUUFBTSxHQUFHLEVBQUUsQ0FBQzt3QkFDbEIsSUFBTSxXQUFTLEdBQUcsRUFBRSxDQUFDO3dCQUNyQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUN2RCxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztnQ0FDbkIsSUFBTSxRQUFRLEdBQUcsUUFBTSxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLEVBQUUsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO2dDQUN4RCxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLFFBQU0sQ0FBQyxJQUFJLENBQUM7d0NBQ1IsSUFBSSxFQUFFLEtBQUssQ0FBQyxFQUFFO3dDQUNkLEtBQUssRUFBRSxLQUFLLENBQUMsRUFBRTt3Q0FDZixXQUFXLEVBQUUsS0FBSyxDQUFDLElBQUk7cUNBQzFCLENBQUMsQ0FBQztpQ0FDTjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt5QkFDTjt3QkFDRCxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUNoRSxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtnQ0FDekIsSUFBTSxRQUFRLEdBQUcsV0FBUyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLEVBQUUsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO2dDQUM5RCxJQUFJLENBQUMsUUFBUSxFQUFFO29DQUNYLFdBQVMsQ0FBQyxJQUFJLENBQUM7d0NBQ1gsSUFBSSxFQUFFLFFBQVEsQ0FBQyxFQUFFO3dDQUNqQixLQUFLLEVBQUUsUUFBUSxDQUFDLEVBQUU7d0NBQ2xCLFdBQVcsRUFBRSxRQUFRLENBQUMsSUFBSTtxQ0FDN0IsQ0FBQyxDQUFDO2lDQUNOOzRCQUNMLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUNELEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxRQUFNLENBQUM7d0JBQ25DLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLG9CQUFvQixDQUFDO3dCQUM3RixLQUFJLENBQUMsdUJBQXVCLEdBQUcsV0FBUyxDQUFDO3dCQUN6QyxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQztxQkFDaEc7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQzt3QkFDL0IsS0FBSSxDQUFDLHVCQUF1QixHQUFHLEVBQUUsQ0FBQztxQkFDckM7b0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3ZJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3pDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDVjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlEQUFrQixHQUFsQjtRQUFBLGlCQXdCQztRQXZCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMseUJBQXlCLEVBQUUsS0FBSSxDQUFDLGtDQUFrQyxFQUFFLElBQUksQ0FBQztpQkFDdkcsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsQ0FBQyxlQUFlLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsRUFBRTt3QkFDMUMsUUFBUSxDQUFDLGVBQWUsR0FBRyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQztxQkFDekQ7b0JBQ0QsS0FBSSxDQUFDLHNCQUFzQixDQUFDLDhCQUE4QixHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7b0JBQ3RGLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxpQ0FBaUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDO2lCQUM1RjtxQkFBTTtvQkFDSCxLQUFJLENBQUMsc0JBQXNCLENBQUMsOEJBQThCLEdBQUcsRUFBRSxDQUFDO29CQUNoRSxLQUFJLENBQUMsc0JBQXNCLENBQUMsaUNBQWlDLEdBQUcsRUFBRSxDQUFDO2lCQUN0RTtnQkFDRCxrQ0FBa0M7Z0JBQ2xDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLElBQU0sU0FBUyxHQUFHLEVBQUUsT0FBTyxFQUFFLHNEQUFzRCxFQUFFLElBQUksRUFBRSxlQUFlLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3ZJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9EQUFhLEdBQWI7UUFBQSxpQkFxREM7UUFwREcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixJQUFJLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7Z0JBQ3RDLFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO29CQUNuRSxRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYSxFQUFFLEVBQUUsS0FBSSxDQUFDLGNBQWMsRUFBRSxFQUFFLEtBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRSxLQUFJLENBQUMsbUNBQW1DLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7eUJBQ3ZKLFNBQVMsQ0FBQyxVQUFBLFdBQVc7d0JBQ2xCLElBQUksS0FBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLElBQUksS0FBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxFQUFFOzRCQUM3SixJQUFNLFVBQVUsR0FBRyxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDOzRCQUNyRSxJQUFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsWUFBWSxLQUFLLFlBQVksQ0FBQyxZQUFZLEVBQUU7Z0NBQ25GLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQzs2QkFDcEI7NEJBQ0QsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixJQUFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUI7Z0NBQzFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO2dDQUN0RixLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQ2pHLElBQU0sVUFBVSxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsSUFBSSxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCO2dDQUNwSSxLQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQ0FDbkYsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUM5RixRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUM7Z0NBQzNFLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztpQ0FDNUIsU0FBUyxDQUFDLFVBQUEsWUFBWTtnQ0FDbkIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUIsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO2dDQUNoQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NEJBQ3hCLENBQUMsRUFBRSxVQUFBLEdBQUc7Z0NBQ0YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDOUIsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxFQUFFLFVBQUEsUUFBUTt3QkFDUCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUM3QixDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsS0FBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO3FCQUM5RyxTQUFTLENBQUMsVUFBQSxZQUFZO29CQUNuQixJQUFNLFVBQVUsR0FBRyxLQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxJQUFJLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsYUFBYTt3QkFDakgsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQzt3QkFDdkUsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xGLFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt5QkFDdEMsU0FBUyxDQUFDLFVBQUEsUUFBUTt3QkFDZixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLFVBQUEsR0FBRzt3QkFDRixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM5QixDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDLEVBQUUsVUFBQSxHQUFHO29CQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnRUFBeUIsR0FBekI7UUFBQSxpQkFtQkM7UUFsQkcsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRTtZQUMvQixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hDO2FBQU07WUFDSCxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtnQkFDM0QsS0FBSyxFQUFFLEtBQUs7Z0JBQ1osWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLElBQUksRUFBRTtvQkFDRixPQUFPLEVBQUUsaUNBQWlDO29CQUMxQyxZQUFZLEVBQUUsS0FBSztvQkFDbkIsWUFBWSxFQUFFLElBQUk7aUJBQ3JCO2FBQ0osQ0FBQyxDQUFDO1lBQ0gsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07Z0JBQ3BDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ3pCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3hDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCw4REFBdUIsR0FBdkIsVUFBd0IsS0FBSztRQUN6QixJQUFJLGNBQWMsR0FBRyxPQUFPLENBQUM7UUFDN0IsSUFBSSxLQUFLLEVBQUU7WUFDUCxjQUFjLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzlEO1FBQ0Qsa0NBQWtDO1FBQ2xDLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsa0VBQTJCLEdBQTNCO1FBQ0ksSUFBSSxDQUFDLHNCQUFzQixHQUFHO1lBQzFCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLHVCQUF1QixFQUFFLElBQUk7WUFDN0IscUJBQXFCLEVBQUUsSUFBSTtZQUMzQixhQUFhLEVBQUUsSUFBSTtZQUNuQixlQUFlLEVBQUUsSUFBSTtZQUNyQixzQkFBc0IsRUFBRTtnQkFDcEIsS0FBSyxFQUFFLG1CQUFtQjtnQkFDMUIsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsMEJBQTBCLEVBQUU7Z0JBQ3hCLEtBQUssRUFBRSx3QkFBd0I7Z0JBQy9CLGFBQWEsRUFBRSxFQUFFO2dCQUNqQixXQUFXLEVBQUUsSUFBSTthQUNwQjtZQUNELG1CQUFtQixFQUFFO2dCQUNqQixLQUFLLEVBQUUsc0JBQXNCO2dCQUM3QixhQUFhLEVBQUUsRUFBRTtnQkFDakIsV0FBVyxFQUFFLElBQUk7YUFDcEI7WUFDRCx1QkFBdUIsRUFBRTtnQkFDckIsS0FBSyxFQUFFLDJCQUEyQjtnQkFDbEMsYUFBYSxFQUFFLEVBQUU7Z0JBQ2pCLFdBQVcsRUFBRSxJQUFJO2FBQ3BCO1lBQ0QsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQjtZQUMvRCw4QkFBOEIsRUFBRSxvQkFBb0IsQ0FBQyxlQUFlO1lBQ3BFLGlDQUFpQyxFQUFFLG9CQUFvQixDQUFDLGVBQWU7WUFDdkUsZ0JBQWdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUN2RSxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVU7WUFDN0MsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTO1lBQzNDLE1BQU0sRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTTtZQUNyQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWE7WUFDbkQsZUFBZSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlO1lBQ3ZELFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWTtZQUNqRCxPQUFPLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxFQUFFO1lBQ1gseUJBQXlCLEVBQUUsRUFBRTtZQUM3QixVQUFVLEVBQUUsSUFBSTtTQUNuQixDQUFDO1FBQ0YsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsK0NBQVEsR0FBUjtRQUFBLGlCQXlCQztRQXhCRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUM1QyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUsscUJBQXFCLEVBQUU7Z0JBQ3JDLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ3BCO1FBQ0wsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLFNBQVMsQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM5QztRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN0RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDNUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBQy9JLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFDM0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNJLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDbEYsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRTtZQUN6RCxJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztZQUNuQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDbkMsS0FBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7O2dCQTluRG1CLFdBQVc7Z0JBQ1IsVUFBVTtnQkFDVCxXQUFXO2dCQUNILG1CQUFtQjtnQkFDcEIsa0JBQWtCO2dCQUN2QixhQUFhO2dCQUNwQixTQUFTO2dCQUNGLGFBQWE7Z0JBQ1Isa0JBQWtCO2dCQUN0QixjQUFjO2dCQUNULG1CQUFtQjs7SUF4RzFDO1FBQVIsS0FBSyxFQUFFOzJFQUFtQjtJQUNqQjtRQUFULE1BQU0sRUFBRTs4RUFBZ0Q7SUFDL0M7UUFBVCxNQUFNLEVBQUU7NkVBQStDO0lBQzlDO1FBQVQsTUFBTSxFQUFFOzZFQUErQztJQUVqQjtRQUF0QyxTQUFTLENBQUMsMEJBQTBCLENBQUM7b0ZBQXdEO0lBK0QvQztRQUE5QyxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO3VFQUE2QztJQUNyRDtRQUFyQyxTQUFTLENBQUMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO3lFQUFrQztJQXZFOUQsNEJBQTRCO1FBTnhDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSwwQkFBMEI7WUFDcEMsZ3FZQUFvRDs7U0FFdkQsQ0FBQztPQUVXLDRCQUE0QixDQTJ1RHhDO0lBQUQsbUNBQUM7Q0FBQSxBQTN1REQsSUEydURDO1NBM3VEWSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGRyZW4sIFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZUNvbnN0YW50cyB9IGZyb20gJy4uL2RlbGl2ZXJhYmxlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0Q29uc3RhbnQgfSBmcm9tICcuLi8uLi8uLi9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVVcGRhdGVPYmplY3QgfSBmcm9tICcuL2RlbGl2ZXJhYmxlLnVwZGF0ZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgQWxsb2NhdGlvblR5cGVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvQWxsb2NhdGlvblR5cGUnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL21wbS11dGlscy9zZXJ2aWNlcy91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuXHJcbmltcG9ydCB7IFJvbGVDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL3JvbGUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVTZXJ2aWNlIH0gZnJvbSAnLi4vZGVsaXZlcmFibGUuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBTYXZlT3B0aW9uQ29uc3RhbnQgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvY29uc3RhbnRzL3NhdmUtb3B0aW9ucy5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBTdGF0dXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3N0YXR1cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzTGV2ZWwsIFN0YXR1c0xldmVscyB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBQcm9qZWN0VXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC11dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVR5cGVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vbGliL21wbS11dGlscy9vYmplY3RzL0RlbGl2ZXJhYmxlVHlwZXMnO1xyXG5pbXBvcnQgeyBDdXN0b21TZWFyY2hGaWVsZENvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1zZWFyY2gtZmllbGQvY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb25Db25maWdDb25zdGFudHMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvc2hhcmVkL2NvbnN0YW50cy9hcHBsaWNhdGlvbi5jb25maWcuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgc3RhcnRXaXRoLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZSwgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xyXG5pbXBvcnQgeyBFTlRFUiwgQ09NTUEgfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xyXG5pbXBvcnQgeyBuZXdBcnJheSB9IGZyb20gJ0Bhbmd1bGFyL2NvbXBpbGVyL3NyYy91dGlsJztcclxuaW1wb3J0IHsgVGFza1R5cGVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVGFza1R5cGUnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tZGVsaXZlcmFibGUtY3JlYXRpb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RlbGl2ZXJhYmxlLWNyZWF0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RlbGl2ZXJhYmxlLWNyZWF0aW9uLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEZWxpdmVyYWJsZUNyZWF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBkZWxpdmVyYWJsZUNvbmZpZztcclxuICAgIEBPdXRwdXQoKSBjbG9zZUNhbGxiYWNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNhdmVDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBub3RpZmljYXRpb25IYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQFZpZXdDaGlsZChDdXN0b21TZWFyY2hGaWVsZENvbXBvbmVudCkgY3VzdG9tU2VhcmNoRmllbGRDb21wb25lbnQ6IEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50O1xyXG5cclxuICAgIGRlbGl2ZXJhYmxlTW9kYWxDb25maWc7XHJcbiAgICB0ZWFtT3duZXJSb2xlc09wdGlvbnM7XHJcbiAgICB0ZWFtQXBwcm92ZXJSb2xlc09wdGlvbnM7XHJcbiAgICB0ZWFtUm9sZU93bmVyT3B0aW9ucztcclxuICAgIHRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zO1xyXG5cclxuICAgIHNlbGVjdGVkT3duZXIgPSBudWxsO1xyXG4gICAgc2VsZWN0ZWRBcHByb3ZlciA9IG51bGw7XHJcblxyXG4gICAgZGVsaXZlcmFibGVUeXBlSWQ7XHJcblxyXG4gICAgZGVsaXZlcmFibGVGb3JtOiBGb3JtR3JvdXA7XHJcblxyXG4gICAgaGFzRGVsaXZlcmFibGVDb3VudDtcclxuICAgIG93bmVyUHJvamVjdENvdW50TWVzc2FnZTtcclxuICAgIGlzWmVyb093bmVyUHJvamVjdENvdW50O1xyXG4gICAgbW9kYWxMYWJlbDtcclxuICAgIGRlbGl2ZXJhYmxlQ29uc3RhbnRzID0gRGVsaXZlcmFibGVDb25zdGFudHM7XHJcblxyXG4gICAgZW5hYmxlTmVlZFJldmlldztcclxuICAgIGVuYWJsZVdvcmtXZWVrO1xyXG4gICAgcHJvamVjdENvbmZpZztcclxuICAgIGFwcENvbmZpZztcclxuICAgIG5hbWVTdHJpbmdQYXR0ZXJuO1xyXG5cclxuICAgIHNhdmVPcHRpb25zID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShTYXZlT3B0aW9uQ29uc3RhbnQuZGVsaXZlcmFibGVTYXZlT3B0aW9ucykpO1xyXG4gICAgU0VMRUNURURfU0FWRV9PUFRJT04gPSB7XHJcbiAgICAgICAgbmFtZTogJ1NhdmUnLFxyXG4gICAgICAgIHZhbHVlOiAnU0FWRSdcclxuICAgIH07XHJcblxyXG4gICAgb2xkRm9ybVZhbHVlOiBEZWxpdmVyYWJsZVVwZGF0ZU9iamVjdDtcclxuXHJcbiAgICB2aXNpYmxlID0gdHJ1ZTtcclxuICAgIHNlbGVjdGFibGUgPSB0cnVlO1xyXG4gICAgcmVtb3ZhYmxlID0gdHJ1ZTtcclxuICAgIGFkZE9uQmx1ciA9IHRydWU7XHJcbiAgICBzZXBhcmF0b3JLZXlzQ29kZXM6IG51bWJlcltdID0gW0VOVEVSLCBDT01NQV07XHJcbiAgICBpc0FwcHJvdmVyO1xyXG4gICAgbG9nZ2VkSW5Vc2VyO1xyXG4gICAgaXNQTSA9IGZhbHNlO1xyXG4gICAgbmV3UmV2aWV3ZXJzID0gW107XHJcbiAgICBuZXdyZXZpZXdlcnNPYmo7XHJcbiAgICByZW1vdmVkUmV2aWV3ZXJzID0gW107XHJcbiAgICByZW1vdmVkcmV2aWV3ZXJzT2JqO1xyXG4gICAgcmV2aWV3ZXJzT2JqO1xyXG4gICAgY2hpcENoYW5nZWQ7XHJcbiAgICBpc1VwbG9hZGVkO1xyXG4gICAgcmV2aWV3ZXJDb21wbGV0ZWQ7XHJcbiAgICBzaG93UmV2aWV3ZXI7XHJcblxyXG4gICAgZmlsdGVyZWRSZXZpZXdlcnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gICAgLy8gcmV2aWV3ZXJzOiBhbnlbXSA9IFtdO1xyXG4gICAgc2F2ZWRSZXZpZXdlcnM6IGFueVtdID0gW107XHJcbiAgICBhbGxSZXZpZXdlcnM6IGFueVtdID0gW107XHJcbiAgICAvLyBhdmFpbGFibGVSZXZpZXdlcnM6IGFueVtdO1xyXG4gICAgLy8gcmV2aWV3ZXJEZXRhaWxzO1xyXG4gICAgcmV2aWV3ZXJTdGF0dXNEYXRhID0gW107XHJcblxyXG4gICAgZGVsaXZlcmFibGVSZXZpZXdlcnMgPSBbXTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdyZXZpZXdlcklucHV0JywgeyBzdGF0aWM6IGZhbHNlIH0pIHJldmlld2VySW5wdXQ6IEVsZW1lbnRSZWY8SFRNTElucHV0RWxlbWVudD47XHJcbiAgICBAVmlld0NoaWxkKCdhdXRvJywgeyBzdGF0aWM6IGZhbHNlIH0pIG1hdEF1dG9jb21wbGV0ZTogTWF0QXV0b2NvbXBsZXRlO1xyXG5cclxuICAgIFBST0pFQ1RfREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvUHJvamVjdC9vcGVyYXRpb25zJztcclxuICAgIFRBU0tfREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvVGFzay9vcGVyYXRpb25zJztcclxuICAgIERFTElWRVJBQkxFX0RFVEFJTFNfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0RlbGl2ZXJhYmxlL29wZXJhdGlvbnMnO1xyXG4gICAgREVMSVZFUkFCTEVfVFlQRV9ERVRBSUxTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9EZWxpdmVyYWJsZV9UeXBlL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIFVTRVJfVEVBTV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi10ZWNoLmNvbS9tcG0vdGVhbXMvYnBtLzEuMCc7XHJcbiAgICBST0xFX1RFQU1fTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1UZWFtcy9UZWFtX1JvbGVfTWFwcGluZy9vcGVyYXRpb25zJztcclxuICAgIEFTU0lHTk1FTlRfVFlQRV9NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvQXNzaWdubWVudF9UeXBlL29wZXJhdGlvbnMnO1xyXG4gICAgREVMSVZFUkFCTEVfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9jb3JlL3dvcmtmbG93L2JwbS8xLjAnO1xyXG5cclxuICAgIEdFVF9QUk9KRUNUX0RFVEFJTFNfV1NfTUVUSE9EX05BTUUgPSAnR2V0UHJvamVjdEJ5SUQnO1xyXG4gICAgR0VUX1RBU0tfREVUQUlMU19XU19NRVRIT0RfTkFNRSA9ICdSZWFkVGFzayc7XHJcbiAgICBHRVRfREVMSVZFUkFCTEVfREVUQUlMU19XU19NRVRIT0RfTkFNRSA9ICdSZWFkRGVsaXZlcmFibGUnO1xyXG4gICAgR0VUX1VTRVJTX0JZX1RFQU1fV1NfTUVUSE9EX05BTUUgPSAnR2V0VGVhbVVzZXJEZXRhaWxzJztcclxuICAgIEdFVF9ERUxJVkVSQUJMRVNfQllfUFJPSkVDVF9XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxEZWxpdmVyYWJsZXNCeVByb2plY3QnO1xyXG4gICAgR0VUX0FTU0lHTk1FTlRfVFlQRV9XU19NRVRIT0RfTkFNRSA9ICdHZXRBbGxBc3NpZ25tZW50VHlwZSc7XHJcbiAgICBDUkVBVEVfREVMSVZFUkFCTEVfV1NfTUVUSE9EX05BTUUgPSAnQ3JlYXRlRGVsaXZlcmFibGUnO1xyXG4gICAgVVBEQVRFX0RFTElWRVJBQkxFX1dTX01FVEhPRF9OQU1FID0gJ0VkaXREZWxpdmVyYWJsZSc7XHJcbiAgICBHRVRfQUxMX0RFTElWRVJBQkxFU19CWV9OQU1FX1dTX01FVEhPRF9OQU1FID0gJ0dldEFsbERlbGl2ZXJhYmxlc0J5RGVsaXZlcmFibGVOYW1lJztcclxuICAgIEdFVF9VU0VSU19CWV9URUFNX1JPTEVfV1NfTUVUSE9EX05BTUUgPSAnR2V0VGVhbVJvbGVVc2VycyAnO1xyXG4gICAgR0VUX0RFTElWRVJBQkxFX1RZUEVfQllfTkFNRV9XU19NRVRIT0RfTkFNRSA9ICdHZXREZWxpdmVyYWJsZVR5cGVCeU5hbWUnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+LFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcGRlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRlbGl2ZXJhYmxlU2VydmljZTogRGVsaXZlcmFibGVTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgc3RhdHVzU2VydmljZTogU3RhdHVzU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgcHJvamVjdFV0aWxTZXJ2aWNlOiBQcm9qZWN0VXRpbFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBmaWx0ZXJSZXZpZXdlcnModmFsdWU6IGFueSk6IGFueVtdIHtcclxuICAgICAgICBpZiAodmFsdWUgJiYgdHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJyAmJiB2YWx1ZS50cmltKCkgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlclZhbHVlID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFsbFJldmlld2Vycy5maWx0ZXIocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKGZpbHRlclZhbHVlKSA9PT0gMCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdGVkKGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50KTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgcmV2aWV3ZXJJbmRleCA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZEluZGV4KHNhdmVkUmV2aWV3ZXIgPT4gc2F2ZWRSZXZpZXdlci52YWx1ZSA9PT0gZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgICAgICBjb25zdCByZXZpZXdlciA9IHRoaXMuYWxsUmV2aWV3ZXJzLmZpbmQocmV2aWV3ZXJEYXRhID0+IHJldmlld2VyRGF0YS52YWx1ZSA9PT0gZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLnZhbHVlLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiZcclxuICAgICAgICAgICAgZXZlbnQub3B0aW9uLnZhbHVlID09PSB0aGlzLmRlbGl2ZXJhYmxlRm9ybS52YWx1ZS5kZWxpdmVyYWJsZUFwcHJvdmVyLm5hbWUpIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHJldmlld2VyLmRpc3BsYXlOYW1lICsgJyBpcyBhbHJlYWR5IHRoZSBBcHByb3ZlcicpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmV2aWV3ZXJJbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihyZXZpZXdlci5kaXNwbGF5TmFtZSArICcgaXMgYWxyZWFkeSBzZWxlY3RlZCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5uZXdSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlci5kaXNwbGF5TmFtZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc3QgcmVtb3ZlUmV2aWV3ZXJJbmRleCA9IHRoaXMucmVtb3ZlZFJldmlld2Vycy5maW5kSW5kZXgocmVtb3ZlZFJldmlld2VyID0+IHJlbW92ZWRSZXZpZXdlci52YWx1ZSA9PT0gZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKHJlbW92ZVJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW1vdmVkUmV2aWV3ZXJzLnNwbGljZShyZW1vdmVSZXZpZXdlckluZGV4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHJldmlld2VyLnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lLFxyXG4gICAgICAgICAgICAgICAgaXNDb21wbGV0ZWQ6IGZhbHNlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VySW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycy5zZXRWYWx1ZShudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYWRkUmV2aWV3ZXIoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLm1hdEF1dG9jb21wbGV0ZS5pc09wZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgaW5wdXQgPSBldmVudC5pbnB1dDtcclxuXHJcbiAgICAgICAgICAgIGlmIChpbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgaW5wdXQudmFsdWUgPSAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcnMuc2V0VmFsdWUobnVsbCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uUmVtb3ZlUmV2aWV3ZXIocmV2aWV3ZXJWYWx1ZTogYW55KTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLnNhdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChzYXZlZFJldmlld2VyID0+IHNhdmVkUmV2aWV3ZXIudmFsdWUgPT09IHJldmlld2VyVmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IG5ld1Jldmlld2VySW5kZXggPSB0aGlzLm5ld1Jldmlld2Vycy5maW5kSW5kZXgobmV3UmV2aWV3ZXIgPT4gbmV3UmV2aWV3ZXIudmFsdWUgPT09IHJldmlld2VyVmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5maW5kSW5kZXgocmV2aWV3ZXJEYXRhID0+IHJldmlld2VyRGF0YS52YWx1ZSA9PT0gcmV2aWV3ZXJWYWx1ZSk7XHJcbiAgICAgICAgY29uc3QgcmV2aWV3ZXIgPSB0aGlzLmFsbFJldmlld2Vycy5maW5kKHJldmlld2VyRGF0YSA9PiByZXZpZXdlckRhdGEudmFsdWUgPT09IHJldmlld2VyVmFsdWUpO1xyXG4gICAgICAgIHRoaXMucmVtb3ZlZFJldmlld2Vycy5wdXNoKHtcclxuICAgICAgICAgICAgbmFtZTogcmV2aWV3ZXIuZGlzcGxheU5hbWUsXHJcbiAgICAgICAgICAgIHZhbHVlOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJldmlld2VyLmRpc3BsYXlOYW1lXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLm1hcmtBc0RpcnR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRSZXZpZXdlcnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YS5zcGxpY2UocmV2aWV3ZXJJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChuZXdSZXZpZXdlckluZGV4ID49IDApIHtcclxuICAgICAgICAgICAgdGhpcy5uZXdSZXZpZXdlcnMuc3BsaWNlKG5ld1Jldmlld2VySW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mU2tpcFJldmlld1ZhbHVlKGlzTmVlZFJldmlld1ZhbHVlKSB7XHJcbiAgICAgICAgaWYgKCFpc05lZWRSZXZpZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5hcHByb3ZlckFsbG9jYXRpb24uY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmNsZWFyVmFsaWRhdG9ycygpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5hcHByb3ZlckFsbG9jYXRpb24ucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5hcHByb3ZlckFsbG9jYXRpb24udXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZkFwcHJvdmVyQWxsb2NhdGlvblR5cGUodGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5hcHByb3ZlckFsbG9jYXRpb24sIHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVBcHByb3ZlciwgZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mT3duZXJBbGxvY2F0aW9uVHlwZShhbGxvY2F0aW9uVHlwZVZhbHVlLCBvd25lclZhbHVlLCBjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgY29uc3Qgcm9sZXMgPSBbXTtcclxuICAgICAgICBjb25zdCByb2xlSWRzID0gW107XHJcbiAgICAgICAgdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgcm9sZXMucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgICAgcm9sZUlkcy5wdXNoKGVsZW1lbnQubmFtZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGFsbG9jYXRpb25UeXBlVmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUuc2V0VmFsaWRhdG9ycyhbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLmNsZWFyVmFsaWRhdG9ycygpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lclJvbGVDb25maWcuZmlsdGVyT3B0aW9ucyA9IHJvbGVzO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZU93bmVyUm9sZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnT1dORVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtUm9sZU93bmVyT3B0aW9ucztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnBhdGNoVmFsdWUob3duZXJWYWx1ZSA/IG93bmVyVmFsdWUgOiAnJyk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChhbGxvY2F0aW9uVHlwZVZhbHVlID09PSBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUikge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS5jbGVhclZhbGlkYXRvcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5zZXRWYWxpZGF0b3JzKFtWYWxpZGF0b3JzLnJlcXVpcmVkXSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ09XTkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5wYXRjaFZhbHVlKG93bmVyVmFsdWUgPyBvd25lclZhbHVlIDogJycpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIgJiYgIXRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLm93bmVyQWxsb2NhdGlvbi5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5lbmFibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mQXBwcm92ZXJBbGxvY2F0aW9uVHlwZShhbGxvY2F0aW9uVHlwZVZhbHVlLCBvd25lclZhbHVlLCBjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgY29uc3Qgcm9sZXMgPSBbXTtcclxuICAgICAgICBjb25zdCByb2xlSWRzID0gW107XHJcbiAgICAgICAgdGhpcy50ZWFtQXBwcm92ZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgcm9sZXMucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgICAgcm9sZUlkcy5wdXNoKGVsZW1lbnQubmFtZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGFsbG9jYXRpb25UeXBlVmFsdWUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5zZXRWYWxpZGF0b3JzKFtWYWxpZGF0b3JzLnJlcXVpcmVkXSk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5maWx0ZXJPcHRpb25zID0gcm9sZXM7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCdBUFBST1ZFUicpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChjaGFuZ2VEZWxpdmVyYWJsZUFwcHJvdmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChhbGxvY2F0aW9uVHlwZVZhbHVlID09PSBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfVVNFUikge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnNldFZhbGlkYXRvcnMoW1ZhbGlkYXRvcnMucmVxdWlyZWRdKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlclJvbGUuY2xlYXJWYWxpZGF0b3JzKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ0FQUFJPVkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgaWYgKGNoYW5nZURlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiYgIXRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmFwcHJvdmVyQWxsb2NhdGlvbi5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5lbmFibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mT3duZXJSb2xlKG93bmVyUm9sZSkge1xyXG4gICAgICAgIGlmIChvd25lclJvbGUgJiYgb3duZXJSb2xlLm5hbWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIgJiYgIXRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ09XTkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZW9mQXBwcm92ZXJSb2xlKGFwcHJvdmVyUm9sZSkge1xyXG4gICAgICAgIGlmIChhcHByb3ZlclJvbGUgJiYgYXBwcm92ZXJSb2xlLm5hbWUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiYgIXRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ0FQUFJPVkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlci5wYXRjaFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIucGF0Y2hWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLmRpc2FibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKiBvbkRlbGl2ZXJhYmxlU2VsZWN0aW9uKCkge1xyXG4gICAgICAgIGNvbnN0IG93bmVyUm9sZXMgPSBbXTtcclxuICAgICAgICBjb25zdCBvd25lclJvbGVJZHMgPSBbXTtcclxuICAgICAgICBjb25zdCBhcHByb3ZlclJvbGVzID0gW107XHJcbiAgICAgICAgY29uc3QgYXBwcm92ZXJSb2xlSWRzID0gW107XHJcbiAgICAgICAgdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgaWYgKGVsZW1lbnQudmFsdWUuc2VhcmNoKFJvbGVDb25zdGFudHMuUFJPSkVDVF9NQU5BR0VSKSAhPT0gLTEgfHwgZWxlbWVudC52YWx1ZS5zZWFyY2goUm9sZUNvbnN0YW50cy5QUk9KRUNUX01FTUJFUikgIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICBvd25lclJvbGVzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICBvd25lclJvbGVJZHMucHVzaChlbGVtZW50Lm5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJSb2xlQ29uZmlnLmZpbHRlck9wdGlvbnMgPSBvd25lclJvbGVzO1xyXG4gICAgICAgIHRoaXMuZ2V0VXNlcnNCeVJvbGUoJ09XTkVSJykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZWxlbWVudC52YWx1ZS5zZWFyY2goUm9sZUNvbnN0YW50cy5QUk9KRUNUX01BTkFHRVIpICE9PSAtMSB8fCBlbGVtZW50LnZhbHVlLnNlYXJjaChSb2xlQ29uc3RhbnRzLlBST0pFQ1RfQVBQUk9WRVIpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgYXBwcm92ZXJSb2xlcy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgYXBwcm92ZXJSb2xlSWRzLnB1c2goZWxlbWVudC5uYW1lKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5maWx0ZXJPcHRpb25zID0gYXBwcm92ZXJSb2xlcztcclxuICAgICAgICB0aGlzLmdldFVzZXJzQnlSb2xlKCdBUFBST1ZFUicpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnM7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgdXBkYXRlRGVsaXZlcmFibGUoc2F2ZU9wdGlvbikge1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlQXBwcm92ZXIgJiYgdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyLm5hbWUpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyLm5hbWUpO1xyXG4gICAgICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU2VsZWN0ZWQgcmV2aWV3ZXIgaXMgYWxyZWFkeSB0aGUgYXBwcm92ZXInLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0ucHJpc3RpbmUpIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnS2luZGx5IG1ha2UgY2hhbmdlcyB0byB1cGRhdGUgdGhlIGRlbGl2ZXJhYmxlJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuV0FSTiB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG5hbWVTcGxpdCA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCkuZGVsaXZlcmFibGVOYW1lLnNwbGl0KCdfJyk7XHJcbiAgICAgICAgaWYgKG5hbWVTcGxpdFswXS5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnTmFtZSBzaG91bGQgbm90IHN0YXJ0IHdpdGggXCJfXCInLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5XQVJOIH07XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5zdGF0dXMgPT09ICdWQUxJRCcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlTmFtZS52YWx1ZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdQbGVhc2UgcHJvdmlkZSBhIHZhbGlkIGRlbGl2ZXJhYmxlIG5hbWUnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5JTkZPIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGRhdGVEZWxpdmVyYWJsZURldGFpbHMoc2F2ZU9wdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnS2luZGx5IGZpbGwgYWxsIG1hbmRhdG9yeSBmaWVsZHMnLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuSU5GT1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NpZ25lbWVudFR5cGVJZCh0eXBlTmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgbGV0IGFzc2lnbmVtZW50SWQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQXNzaWdubWVudFR5cGUubWFwKHR5cGUgPT4ge1xyXG4gICAgICAgICAgICBpZiAodHlwZS5OQU1FID09PSB0eXBlTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgYXNzaWduZW1lbnRJZCA9IHR5cGVbJ0Fzc2lnbm1lbnRfVHlwZS1pZCddLklkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGFzc2lnbmVtZW50SWQ7XHJcbiAgICB9XHJcbiAgICBnZXRVcGRhdGVkRGVsaXZlcmFibGVEZXRhaWxzKCk6IERlbGl2ZXJhYmxlVXBkYXRlT2JqZWN0IHtcclxuICAgICAgICBsZXQgZGVsaXZlcmFibGVPYmplY3Q6IERlbGl2ZXJhYmxlVXBkYXRlT2JqZWN0O1xyXG4gICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcyA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmdldFJhd1ZhbHVlKCk7XHJcbiAgICAgICAgZGVsaXZlcmFibGVPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIERlbGl2ZXJhYmxlSWQ6IHtcclxuICAgICAgICAgICAgICAgIElkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuaXNOZXdEZWxpdmVyYWJsZSA/IG51bGwgOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgRGVsaXZlcmFibGVOYW1lOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZGVsaXZlcmFibGVOYW1lLnRyaW0oKSA/IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kZWxpdmVyYWJsZU5hbWUudHJpbSgpLnJlcGxhY2UoL1xccysvZywgJyAnKSA6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kZWxpdmVyYWJsZU5hbWUudHJpbSgpLFxyXG4gICAgICAgICAgICBEZXNjcmlwdGlvbjogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICBTdGFydERhdGU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc1RlbXBsYXRlID8gJycgOiAodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suU1RBUlRfREFURSkgPyAnJyA6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlNUQVJUX0RBVEUpLFxyXG4gICAgICAgICAgICBFbmREYXRlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuaXNUZW1wbGF0ZSA/ICcnIDogKHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZHVlRGF0ZSkgPyAnJyA6XHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZHVlRGF0ZSksXHJcbiAgICAgICAgICAgIER1ZURhdGU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc1RlbXBsYXRlID8gJycgOiAodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kdWVEYXRlKSA/ICcnIDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmR1ZURhdGUpLFxyXG4gICAgICAgICAgICBJc0FjdGl2ZTogdHJ1ZSxcclxuICAgICAgICAgICAgSXNEZWxldGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgRGVsaXZlcmFibGVUeXBlOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMubmVlZFJldmlldyA/ICdVUExPQURfUkVWSUVXJyA6ICdVUExPQUQnLFxyXG4gICAgICAgICAgICBPd25lckFzc2lnbm1lbnRUeXBlOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMub3duZXJBbGxvY2F0aW9uLFxyXG4gICAgICAgICAgICBBcHByb3ZlckFzc2lnbm1lbnRUeXBlOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuYXBwcm92ZXJBbGxvY2F0aW9uLFxyXG4gICAgICAgICAgICBFeHBlY3RlZER1cmF0aW9uOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZXhwZWN0ZWREdXJhdGlvbixcclxuICAgICAgICAgICAgUlBPU3RhdHVzOiB7XHJcbiAgICAgICAgICAgICAgICBNUE1TdGF0dXNJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuc3RhdHVzXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFJQT1ByaW9yaXR5OiB7XHJcbiAgICAgICAgICAgICAgICBNUE1Qcmlvcml0eUlEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5wcmlvcml0eVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBSUE9UYXNrOiB7XHJcbiAgICAgICAgICAgICAgICBUYXNrSUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFza1snVGFzay1pZCddLklkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFJQT1Byb2plY3Q6IHtcclxuICAgICAgICAgICAgICAgIFByb2plY3RJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0WydQcm9qZWN0LWlkJ10uSWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgUlBPT3duZXJSb2xlSUQ6IHtcclxuICAgICAgICAgICAgICAgIE1QTVRlYW1Sb2xlTWFwcGluZ0lEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgSWQ6IGRlbGl2ZXJhYmxlRm9ybVZhbHVlcy5kZWxpdmVyYWJsZU93bmVyUm9sZT8ubmFtZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBSUE9BcHByb3ZlclJvbGVJRDoge1xyXG4gICAgICAgICAgICAgICAgTVBNVGVhbVJvbGVNYXBwaW5nSUQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlPy5uYW1lXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIFJQT0RlbGl2ZXJhYmxlT3duZXJJRDoge1xyXG4gICAgICAgICAgICAgICAgSWRlbnRpdHlJRDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJJRDogZGVsaXZlcmFibGVGb3JtVmFsdWVzLmRlbGl2ZXJhYmxlT3duZXI/Lm5hbWVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgUlBPRGVsaXZlcmFibGVBcHByb3ZlcklEOiB7XHJcbiAgICAgICAgICAgICAgICBJZGVudGl0eUlEOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlcklEOiBkZWxpdmVyYWJsZUZvcm1WYWx1ZXMuZGVsaXZlcmFibGVBcHByb3Zlcj8ubmFtZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gZGVsaXZlcmFibGVPYmplY3Q7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVJldmlld2VyT2JqKCkge1xyXG4gICAgICAgIGNvbnN0IGFkZGVkUmV2aWV3ZXJzID0gW107XHJcbiAgICAgICAgY29uc3QgcmVtb3ZlZFJldmlld2VycyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5uZXdSZXZpZXdlcnMgJiYgdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycykge1xyXG4gICAgICAgICAgICB0aGlzLm5ld1Jldmlld2Vycy5mb3JFYWNoKG5ld1Jldmlld2VyID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFkZGVkUmV2aWV3ZXJJbmRleCA9IGFkZGVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gbmV3UmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhZGRlZFJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFkZGVkUmV2aWV3ZXJzLnB1c2gobmV3UmV2aWV3ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVsaXZlcmFibGVSZXZpZXdlckluZGV4ID0gdGhpcy5kZWxpdmVyYWJsZVJldmlld2Vycy5maW5kSW5kZXgoZGVsaXZlcmFibGVSZXZpZXdlciA9PiBkZWxpdmVyYWJsZVJldmlld2VyLnZhbHVlID09PSBuZXdSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYWRkZWRSZXZpZXdlckluZGV4ID0gYWRkZWRSZXZpZXdlcnMuZmluZEluZGV4KHJldmlld2VyID0+IHJldmlld2VyLnZhbHVlID09PSBuZXdSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhZGRlZFJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZGRlZFJldmlld2Vycy5wdXNoKG5ld1Jldmlld2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMubmV3cmV2aWV3ZXJzT2JqID0gYWRkZWRSZXZpZXdlcnMubWFwKChhZGRlZFJldmlld2VyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlSWQ6IHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgICAgICAgICBSZXZpZXdlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEl0ZW1JZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZDogYWRkZWRSZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnQUREJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgVGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgIElzUE1Bc3NpZ25lZFJldmlld2VyOiB0aGlzLmlzUE1cclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucmVtb3ZlZFJldmlld2VycyAmJiB0aGlzLnNhdmVkUmV2aWV3ZXJzKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlZFJldmlld2Vycy5mb3JFYWNoKHJlbW92ZWRSZXZpZXdlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zYXZlZFJldmlld2Vycy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzLmZpbmRJbmRleChkZWxpdmVyYWJsZVJldmlld2VyID0+IGRlbGl2ZXJhYmxlUmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VySW5kZXggPSByZW1vdmVkUmV2aWV3ZXJzLmZpbmRJbmRleChyZXZpZXdlciA9PiByZXZpZXdlci52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1vdmVkUmV2aWV3ZXJzLnB1c2gocmVtb3ZlZFJldmlld2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2F2ZWRSZXZpZXdlckluZGV4ID0gdGhpcy5zYXZlZFJldmlld2Vycy5maW5kSW5kZXgoc2F2ZWRSZXZpZXdlciA9PiBzYXZlZFJldmlld2VyLnZhbHVlID09PSByZW1vdmVkUmV2aWV3ZXIudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzYXZlZFJldmlld2VySW5kZXggPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlbW92ZWRSZXZpZXdlckluZGV4ID0gcmVtb3ZlZFJldmlld2Vycy5maW5kSW5kZXgocmV2aWV3ZXIgPT4gcmV2aWV3ZXIudmFsdWUgPT09IHJlbW92ZWRSZXZpZXdlci52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlUmV2aWV3ZXJJbmRleCA9IHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMuZmluZEluZGV4KGRhdGEgPT4gZGF0YS52YWx1ZSA9PT0gcmVtb3ZlZFJldmlld2VyLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlbW92ZWRSZXZpZXdlckluZGV4ID09PSAtMSAmJiBkZWxpdmVyYWJsZVJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlZFJldmlld2Vycy5wdXNoKHJlbW92ZWRSZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnJlbW92ZWRyZXZpZXdlcnNPYmogPSByZW1vdmVkUmV2aWV3ZXJzLm1hcCgocmV2aWV3ZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgRGVsaXZlcmFibGVJZDogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICAgICAgICAgIFJldmlld2VyOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgSXRlbUlkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlcklkOiByZXZpZXdlci52YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiAnUkVNT1ZFJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgVGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgICAgIElzUE1Bc3NpZ25lZFJldmlld2VyOiB0aGlzLmlzUE1cclxuXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmV2aWV3ZXJzT2JqID0gdGhpcy5uZXdyZXZpZXdlcnNPYmogPyB0aGlzLm5ld3Jldmlld2Vyc09iai5jb25jYXQodGhpcy5yZW1vdmVkcmV2aWV3ZXJzT2JqKSA6IHRoaXMucmVtb3ZlZHJldmlld2Vyc09iai5jb25jYXQodGhpcy5uZXdyZXZpZXdlcnNPYmopO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZURlbGl2ZXJhYmxlRGV0YWlscyhzYXZlT3B0aW9uKSB7XHJcbiAgICAgICAgY29uc3QgZGVsaXZlcmFibGVPYmplY3Q6IERlbGl2ZXJhYmxlVXBkYXRlT2JqZWN0ID0gdGhpcy5nZXRVcGRhdGVkRGVsaXZlcmFibGVEZXRhaWxzKCk7XHJcbiAgICAgICAgdGhpcy5mb3JtUmV2aWV3ZXJPYmooKTtcclxuICAgICAgICB0aGlzLnNhdmVEZWxpdmVyYWJsZShkZWxpdmVyYWJsZU9iamVjdCwgc2F2ZU9wdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgc2F2ZURlbGl2ZXJhYmxlKGRlbGl2ZXJhYmxlT2JqZWN0LCBzYXZlT3B0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVPYmplY3QgPSB0aGlzLnByb2plY3RVdGlsU2VydmljZS5jb21wYXJlVHdvUHJvamVjdERldGFpbHMoZGVsaXZlcmFibGVPYmplY3QsIHRoaXMub2xkRm9ybVZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKE9iamVjdC5rZXlzKGRlbGl2ZXJhYmxlT2JqZWN0KS5sZW5ndGggPiAxIHx8IChKU09OLnN0cmluZ2lmeSh0aGlzLmRlbGl2ZXJhYmxlUmV2aWV3ZXJzKSAhPT0gSlNPTi5zdHJpbmdpZnkodGhpcy5zYXZlZFJldmlld2VycykpKSB7XHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU9iamVjdC5CdWxrT3BlcmF0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXF1ZXN0T2JqID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIERlbGl2ZXJhYmxlT2JqZWN0OiBkZWxpdmVyYWJsZU9iamVjdCxcclxuICAgICAgICAgICAgICAgICAgICBEZWxpdmVyYWJsZVJldmlld2Vyczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBEZWxpdmVyYWJsZVJldmlld2VyOiB0aGlzLnJldmlld2Vyc09ialxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkRFTElWRVJBQkxFX01FVEhPRF9OUywgdGhpcy5VUERBVEVfREVMSVZFUkFCTEVfV1NfTUVUSE9EX05BTUUsIHJlcXVlc3RPYmopXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10gJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZVsnRGVsaXZlcmFibGUtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnIERlbGl2ZXJhYmxlIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5JywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNhbGxCYWNrSGFuZGxlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUlkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhc2tJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNUZW1wbGF0ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmlzVGVtcGxhdGUgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZU9wdGlvbjogc2F2ZU9wdGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2Uuc3RhdHVzQ29kZSA9PT0gJzUwMCcgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmVycm9yLmVycm9yQ29kZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvck1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIGRlbGl2ZXJhYmxlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIHVwZGF0aW5nIGRlbGl2ZXJhYmxlJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCByZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgRGVsaXZlcmFibGVPYmplY3Q6IGRlbGl2ZXJhYmxlT2JqZWN0XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuREVMSVZFUkFCTEVfTUVUSE9EX05TLCB0aGlzLkNSRUFURV9ERUxJVkVSQUJMRV9XU19NRVRIT0RfTkFNRSwgcmVxdWVzdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLnN0YXR1c0NvZGUgPT09ICcyMDAnICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGFcclxuICAgICAgICAgICAgICAgICAgICAgICAgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZGF0YS5EZWxpdmVyYWJsZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuRGVsaXZlcmFibGVbJ0RlbGl2ZXJhYmxlLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnRGVsaXZlcmFibGUgc2F2ZWQgc3VjY2Vzc2Z1bGx5JywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuU1VDQ0VTUyB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNhbGxCYWNrSGFuZGxlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUlkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhc2tJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNUZW1wbGF0ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYXZlT3B0aW9uOiBzYXZlT3B0aW9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNhbGxCYWNrSGFuZGxlci5uZXh0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUlkOiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhLkRlbGl2ZXJhYmxlWydEZWxpdmVyYWJsZS1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhc2tJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5wcm9qZWN0SWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNUZW1wbGF0ZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZU9wdGlvbjogc2F2ZU9wdGlvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnNTAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvclxyXG4gICAgICAgICAgICAgICAgICAgICAgICAmJiByZXNwb25zZS5BUElSZXNwb25zZS5lcnJvci5lcnJvckNvZGUgJiYgcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogcmVzcG9uc2UuQVBJUmVzcG9uc2UuZXJyb3IuZXJyb3JNZXNzYWdlLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyBvbiB3aGlsZSBjcmVhdGluZyBkZWxpdmVyYWJsZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIG9uIHdoaWxlIGNyZWF0aW5nIGRlbGl2ZXJhYmxlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1JcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyogZm9ybVVzZXJWYWx1ZSh1c2VyKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgdXNlcklkOiB1c2VyXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZFVzZXIgPSB0aGlzLnRlYW1Sb2xlVXNlck9wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQubmFtZSA9PT0gcmVzcG9uc2UudXNlckNOKTtcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkVXNlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE93bmVyID0gIHNlbGVjdGVkVXNlcjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGZvcm1Sb2xlVmFsdWUocm9sZSwgaXNSZXZpZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIGlmIChyb2xlKSB7XHJcbiAgICAgICAgICAgIGlmIChpc1Jldmlld0RlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFJvbGUgPSB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSByb2xlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoc2VsZWN0ZWRSb2xlKSA/IHNlbGVjdGVkUm9sZSA6ICcnO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb2xlID0gdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMuZmluZChlbGVtZW50ID0+IGVsZW1lbnQubmFtZSA9PT0gcm9sZSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKHNlbGVjdGVkUm9sZSkgPyBzZWxlY3RlZFJvbGUgOiAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybU93bmVyVmFsdWUodXNlcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHVzZXJJZCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHVzZXJJZFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQZXJzb25CeUlkZW50aXR5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyID0gdGhpcy50ZWFtUm9sZU93bmVyT3B0aW9ucy5maW5kKGVsZW1lbnQgPT4gZWxlbWVudC5uYW1lID09PSByZXNwb25zZS51c2VyQ04pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPd25lciA9IHNlbGVjdGVkVXNlcjtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybUFwcHJvdmVyVmFsdWUodXNlcklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodXNlcklkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFVzZXIgPSB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IHJlc3BvbnNlLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZEFwcHJvdmVyID0gc2VsZWN0ZWRVc2VyO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtT3duZXJSb2xlVmFsdWUoZGVsaXZlcmFibGUpIHtcclxuICAgICAgICBpZiAoZGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEICYmIGRlbGl2ZXJhYmxlLlJfUE9fT1dORVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10gJiYgdHlwZW9mIGRlbGl2ZXJhYmxlLlJfUE9fT1dORVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZSA9IHRoaXMudGVhbU93bmVyUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IGRlbGl2ZXJhYmxlLlJfUE9fT1dORVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICByZXR1cm4gKHNlbGVjdGVkUm9sZSkgPyBzZWxlY3RlZFJvbGUgOiAnJztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1BcHByb3ZlclJvbGVWYWx1ZShkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIGlmIChkZWxpdmVyYWJsZSAmJiBkZWxpdmVyYWJsZS5SX1BPX0FQUFJPVkVSX1JPTEVfSUQgJiYgdHlwZW9mIGRlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkUm9sZSA9IHRoaXMudGVhbUFwcHJvdmVyUm9sZXNPcHRpb25zLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lm5hbWUgPT09IGRlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICByZXR1cm4gKHNlbGVjdGVkUm9sZSkgPyBzZWxlY3RlZFJvbGUgOiAnJztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1Pd25lckFsbG9jYXRpb25WYWx1ZShkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIGlmIChkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gKGRlbGl2ZXJhYmxlLk9XTkVSX0FTU0lHTk1FTlRfVFlQRSkgPyBkZWxpdmVyYWJsZS5PV05FUl9BU1NJR05NRU5UX1RZUEUgOiAnJztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1BcHByb3ZlckFsbG9jYXRpb25WYWx1ZShkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIGlmIChkZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gKGRlbGl2ZXJhYmxlLkFQUFJPVkVSX0FTU0lHTk1FTlRfVFlQRSkgPyBkZWxpdmVyYWJsZS5BUFBST1ZFUl9BU1NJR05NRU5UX1RZUEUgOiAnJztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRpYWxpc2VEZWxpdmVyYWJsZUZvcm0oKSB7XHJcbiAgICAgICAgY29uc3QgaXNUZW1wbGF0ZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5pc1RlbXBsYXRlO1xyXG4gICAgICAgIGNvbnN0IGlzTmV3RGVsaXZlcmFibGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuaXNOZXdEZWxpdmVyYWJsZTtcclxuICAgICAgICBjb25zdCBudW1lcmljZVZsYWx1ZSA9IHRoaXMuZ2V0TWF4TWluVmFsdWVGb3JOdW1iZXIoMCk7XHJcbiAgICAgICAgbGV0IGRpc2FibGVGaWVsZCA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IHNlbGVjdGVkRGVsaXZlcmFibGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZTtcclxuICAgICAgICB0aGlzLnNob3dSZXZpZXdlciA9ICh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUgJiZcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSAhPT0gRGVsaXZlcmFibGVUeXBlcy5VUExPQUQpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5taW5EYXRlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5TVEFSVF9EQVRFO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5tYXhEYXRlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5EVUVfREFURTtcclxuICAgICAgICBjb25zdCBwYXR0ZXJuID0gJ15bYS16QS1aMC05IF8oKS4tXSokJztcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy50aXRsZSkge1xyXG4gICAgICAgICAgICB0aGlzLm1vZGFsTGFiZWwgPSB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnRpdGxlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIWlzVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgaWYgKGlzTmV3RGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVOYW1lOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3QgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0WzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3RbMF1bJ01QTV9Qcmlvcml0eS1pZCddID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlU3RhdHVzTGlzdCAmJiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVTdGF0dXNMaXN0WzBdICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVTdGF0dXNMaXN0WzBdWydNUE1fU3RhdHVzLWlkJ10gPyB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVTdGF0dXNMaXN0WzBdWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJywgZGlzYWJsZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkdWVEYXRlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5EVUVfREFURSwgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9LCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwMCldKSxcclxuICAgICAgICAgICAgICAgICAgICBuZWVkUmV2aWV3OiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogZmFsc2UsIGRpc2FibGVkOiAhdGhpcy5lbmFibGVOZWVkUmV2aWV3IH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwcHJvdmVyQWxsb2NhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BU1NJR05NRU5UX1RZUEUsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPd25lciA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlcjogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJSb2xlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5BU1NJR05NRU5UX1RZUEUgPT09IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybVJvbGVWYWx1ZSh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrLlJfUE9fVEVBTV9ST0xFWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCwgZmFsc2UpIDogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyUm9sZTogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5tYXgobnVtZXJpY2VWbGFsdWUpLCBWYWxpZGF0b3JzLm1pbigtKG51bWVyaWNlVmxhbHVlKSldKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlRmllbGQgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5GSU5BTCB8fCAhdGhpcy5pc1VwbG9hZGVkIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSA9PT0gRGVsaXZlcmFibGVUeXBlcy5SRVZJRVcpID8gdHJ1ZSA6IGRpc2FibGVGaWVsZDtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVOYW1lOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWREZWxpdmVyYWJsZS5OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEyMCksIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuKV0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXI6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkT3duZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHwgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMuU1RBVFVTX0xFVkVMID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Pd25lclJvbGVWYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5zZWxlY3RlZEFwcHJvdmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIHx8IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzLlNUQVRVU19MRVZFTCA9PT0gU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtQXBwcm92ZXJSb2xlVmFsdWUoc2VsZWN0ZWREZWxpdmVyYWJsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHwgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMuU1RBVFVTX0xFVkVMID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVUyAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10gPyBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB8fCB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEVcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19QUklPUklUWSAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fUFJJT1JJVFlbJ01QTV9Qcmlvcml0eS1pZCddID8gc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1BSSU9SSVRZWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBkdWVEYXRlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogc2VsZWN0ZWREZWxpdmVyYWJsZS5EVUVfREFURSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2Ygc2VsZWN0ZWREZWxpdmVyYWJsZS5ERVNDUklQVElPTiA9PT0gJ3N0cmluZycgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWREZWxpdmVyYWJsZS5ERVNDUklQVElPTiA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgbmVlZFJldmlldzogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShzZWxlY3RlZERlbGl2ZXJhYmxlLkFQUFJPVkVSX0FTU0lHTk1FTlRfVFlQRSkgPyBmYWxzZSA6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIWRpc2FibGVGaWVsZCA/IHNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSA9PT0gRGVsaXZlcmFibGVUeXBlcy5SRVZJRVcgPyBkaXNhYmxlRmllbGQgOiB0cnVlIDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtQXBwcm92ZXJBbGxvY2F0aW9uVmFsdWUoc2VsZWN0ZWREZWxpdmVyYWJsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHwgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMuU1RBVFVTX0xFVkVMID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgb3duZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtT3duZXJBbGxvY2F0aW9uVmFsdWUoc2VsZWN0ZWREZWxpdmVyYWJsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfHwgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmN1cnJTdGF0dXMuU1RBVFVTX0xFVkVMID09PSBTdGF0dXNMZXZlbHMuSU5URVJNRURJQVRFXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVSZXZpZXdlcnM6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiB0aGlzLnNhdmVkUmV2aWV3ZXJzLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGV4cGVjdGVkRHVyYXRpb246IG5ldyBGb3JtQ29udHJvbCh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoc2VsZWN0ZWREZWxpdmVyYWJsZS5FWFBFQ1RFRF9EVVJBVElPTikgPyAnJyA6IHNlbGVjdGVkRGVsaXZlcmFibGUuRVhQRUNURURfRFVSQVRJT04sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFtWYWxpZGF0b3JzLm1heChudW1lcmljZVZsYWx1ZSksIFZhbGlkYXRvcnMubWluKC0obnVtZXJpY2VWbGFsdWUpKV0pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLm5lZWRSZXZpZXcuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5vbkRlbGl2ZXJhYmxlU2VsZWN0aW9uKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU5hbWU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1heExlbmd0aCgxMjApLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5uYW1lU3RyaW5nUGF0dGVybildKSxcclxuICAgICAgICAgICAgICAgICAgICBwcmlvcml0eTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdCAmJiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVByaW9yaXR5TGlzdFswXVsnTVBNX1ByaW9yaXR5LWlkJ10gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0WzBdWydNUE1fUHJpb3JpdHktaWQnXS5JZCA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVTdGF0dXNMaXN0ICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXSA/IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3RbMF1bJ01QTV9TdGF0dXMtaWQnXS5JZCA6ICcnLCBkaXNhYmxlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogJycsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgbmVlZFJldmlldzogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IGZhbHNlLCBkaXNhYmxlZDogIXRoaXMuZW5hYmxlTmVlZFJldmlldyB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhcHByb3ZlckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG93bmVyQWxsb2NhdGlvbjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQVNTSUdOTUVOVF9UWVBFLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lcjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkT3duZXIgOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXI6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyUm9sZTogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suQVNTSUdOTUVOVF9UWVBFID09PSBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Sb2xlVmFsdWUodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX1RFQU1fUk9MRVsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQsIGZhbHNlKSA6ICcnLCBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7IHZhbHVlOiAnJywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZVJldmlld2VyczogbmV3IEZvcm1Db250cm9sKHsgdmFsdWU6IHRoaXMuc2F2ZWRSZXZpZXdlcnMsIGRpc2FibGVkOiBkaXNhYmxlRmllbGQgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZXhwZWN0ZWREdXJhdGlvbjogbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5tYXgobnVtZXJpY2VWbGFsdWUpLCBWYWxpZGF0b3JzLm1pbigtKG51bWVyaWNlVmxhbHVlKSldKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlRmllbGQgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEUgfHxcclxuICAgICAgICAgICAgICAgICAgICAoc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlJFVklFVykgPyB0cnVlIDogZGlzYWJsZUZpZWxkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZU5hbWU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZERlbGl2ZXJhYmxlLk5BTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKSwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMubmFtZVN0cmluZ1BhdHRlcm4pXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVPd25lcjogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWRPd25lcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJSb2xlOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtT3duZXJSb2xlVmFsdWUoc2VsZWN0ZWREZWxpdmVyYWJsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5zZWxlY3RlZEFwcHJvdmVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3ZlclJvbGU6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1BcHByb3ZlclJvbGVWYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19TVEFUVVMgJiYgc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddID8gc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1NUQVRVU1snTVBNX1N0YXR1cy1pZCddLklkIDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpb3JpdHk6IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBzZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fUFJJT1JJVFkgJiYgc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX1BSSU9SSVRZWydNUE1fUHJpb3JpdHktaWQnXSA/IHNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19QUklPUklUWVsnTVBNX1ByaW9yaXR5LWlkJ10uSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0eXBlb2Ygc2VsZWN0ZWREZWxpdmVyYWJsZS5ERVNDUklQVElPTiA9PT0gJ3N0cmluZycgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWREZWxpdmVyYWJsZS5ERVNDUklQVElPTiA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgfSwgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMDApXSksXHJcbiAgICAgICAgICAgICAgICAgICAgbmVlZFJldmlldzogbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eShzZWxlY3RlZERlbGl2ZXJhYmxlLkFQUFJPVkVSX0FTU0lHTk1FTlRfVFlQRSkgPyBmYWxzZSA6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gIWRpc2FibGVGaWVsZCA/IHNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfVFlQRSA9PT0gRGVsaXZlcmFibGVUeXBlcy5SRVZJRVcgPyBkaXNhYmxlRmllbGQgOiB0cnVlIDogZGlzYWJsZUZpZWxkXHJcbiAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJBbGxvY2F0aW9uOiBuZXcgRm9ybUNvbnRyb2woe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5mb3JtQXBwcm92ZXJBbGxvY2F0aW9uVmFsdWUoc2VsZWN0ZWREZWxpdmVyYWJsZSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlRmllbGRcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBvd25lckFsbG9jYXRpb246IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmZvcm1Pd25lckFsbG9jYXRpb25WYWx1ZShzZWxlY3RlZERlbGl2ZXJhYmxlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlUmV2aWV3ZXJzOiBuZXcgRm9ybUNvbnRyb2woeyB2YWx1ZTogdGhpcy5zYXZlZFJldmlld2VycywgZGlzYWJsZWQ6IGRpc2FibGVGaWVsZCB9KSxcclxuICAgICAgICAgICAgICAgICAgICBleHBlY3RlZER1cmF0aW9uOiBuZXcgRm9ybUNvbnRyb2wodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHNlbGVjdGVkRGVsaXZlcmFibGUuRVhQRUNURURfRFVSQVRJT04pID8gJycgOiBzZWxlY3RlZERlbGl2ZXJhYmxlLkVYUEVDVEVEX0RVUkFUSU9OLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBbVmFsaWRhdG9ycy5tYXgobnVtZXJpY2VWbGFsdWUpLCBWYWxpZGF0b3JzLm1pbigtKG51bWVyaWNlVmxhbHVlKSldKVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZmlsdGVyZWRSZXZpZXdlcnMgPSB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXQoJ2RlbGl2ZXJhYmxlUmV2aWV3ZXJzJykudmFsdWVDaGFuZ2VzLnBpcGUoXHJcbiAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgIG1hcCgocmV2aWV3ZXI6IHN0cmluZyB8IG51bGwpID0+IHJldmlld2VyID8gdGhpcy5maWx0ZXJSZXZpZXdlcnMocmV2aWV3ZXIpIDogdGhpcy5hbGxSZXZpZXdlcnMuc2xpY2UoKSkpO1xyXG5cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lckNvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXI7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZm9ybUNvbnRyb2wgPSB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyUm9sZUNvbmZpZy5mb3JtQ29udHJvbCA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5mb3JtQ29udHJvbCA9IHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMub3duZXJBbGxvY2F0aW9uLnZhbHVlID09PSBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lciAmJiB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS52YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZS52YWx1ZSAhPT0gJycgJiYgaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnT1dORVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZvcm1Db250cm9sLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXQoJ2RlbGl2ZXJhYmxlT3duZXInKS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuYXBwcm92ZXJBbGxvY2F0aW9uLnZhbHVlID09PSBEZWxpdmVyYWJsZUNvbnN0YW50cy5BTExPQ0FUSU9OX1RZUEVfUk9MRSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3ZlciAmJiB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS52YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZGlzYWJsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS52YWx1ZSAhPT0gJycgJiYgaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRVc2Vyc0J5Um9sZSgnQVBQUk9WRVInKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZvcm1Db250cm9sLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXQoJ2RlbGl2ZXJhYmxlQXBwcm92ZXInKS51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMubmVlZFJldmlldy52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGlzTmVlZFJldmlld1ZhbHVlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vbkNoYW5nZW9mU2tpcFJldmlld1ZhbHVlKGlzTmVlZFJldmlld1ZhbHVlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gdGhpcy5vbkNoYW5nZW9mT3duZXJBbGxvY2F0aW9uVHlwZSh0aGlzLmRlbGl2ZXJhYmxlRm9ybS52YWx1ZS5vd25lckFsbG9jYXRpb24sIHRoaXMuZGVsaXZlcmFibGVGb3JtLnZhbHVlLmRlbGl2ZXJhYmxlT3duZXIsIGZhbHNlKTtcclxuXHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMub3duZXJBbGxvY2F0aW9uLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoYWxsb2NhdGlvblR5cGVWYWx1ZSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyUm9sZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lclJvbGUuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZU93bmVyLnNldFZhbHVlKCcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLm9uQ2hhbmdlb2ZPd25lckFsbG9jYXRpb25UeXBlKGFsbG9jYXRpb25UeXBlVmFsdWUsIG51bGwsIHRydWUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyB0aGlzLm9uQ2hhbmdlb2ZBcHByb3ZlckFsbG9jYXRpb25UeXBlKHRoaXMuZGVsaXZlcmFibGVGb3JtLnZhbHVlLmFwcHJvdmVyQWxsb2NhdGlvbiwgdGhpcy5kZWxpdmVyYWJsZUZvcm0udmFsdWUuZGVsaXZlcmFibGVBcHByb3ZlciwgZmFsc2UpO1xyXG5cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5hcHByb3ZlckFsbG9jYXRpb24udmFsdWVDaGFuZ2VzLnN1YnNjcmliZShhbGxvY2F0aW9uVHlwZVZhbHVlID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlQXBwcm92ZXIuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZVJldmlld2Vycykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVSZXZpZXdlcnMuc2V0VmFsdWUoJycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZkFwcHJvdmVyQWxsb2NhdGlvblR5cGUoYWxsb2NhdGlvblR5cGVWYWx1ZSwgbnVsbCwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUob3duZXJSb2xlID0+IHtcclxuICAgICAgICAgICAgaWYgKG93bmVyUm9sZSAmJiB0eXBlb2Ygb3duZXJSb2xlID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEoc2VsZWN0ZWREZWxpdmVyYWJsZSAmJiBzZWxlY3RlZERlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1RZUEUgPT09IERlbGl2ZXJhYmxlVHlwZXMuUkVWSUVXKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25DaGFuZ2VvZk93bmVyUm9sZShvd25lclJvbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRPd25lclJvbGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVPd25lclJvbGVDb25maWcuZmlsdGVyT3B0aW9ucy5maW5kKHJvbGUgPT4gcm9sZS5kaXNwbGF5TmFtZSA9PT0gb3duZXJSb2xlKTtcclxuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZE93bmVyUm9sZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnNldFZhbHVlKHNlbGVjdGVkT3duZXJSb2xlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKGFwcHJvdmVyUm9sZSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcHByb3ZlclJvbGUgJiYgdHlwZW9mIGFwcHJvdmVyUm9sZSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIGlmICghKHNlbGVjdGVkRGVsaXZlcmFibGUgJiYgc2VsZWN0ZWREZWxpdmVyYWJsZS5ERUxJVkVSQUJMRV9UWVBFID09PSBEZWxpdmVyYWJsZVR5cGVzLlJFVklFVykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uQ2hhbmdlb2ZBcHByb3ZlclJvbGUoYXBwcm92ZXJSb2xlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXBwcm92ZXJSb2xlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlLmZpbHRlck9wdGlvbnMuZmluZChyb2xlID0+IHJvbGUuZGlzcGxheU5hbWUgPT09IGFwcHJvdmVyUm9sZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRBcHByb3ZlclJvbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5zZXRWYWx1ZShzZWxlY3RlZEFwcHJvdmVyUm9sZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLmNvbnRyb2xzLmRlbGl2ZXJhYmxlT3duZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKG93bmVyVmFsdWUgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCEob3duZXJWYWx1ZSAmJiB0eXBlb2Ygb3duZXJWYWx1ZSA9PT0gJ29iamVjdCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRPd25lciA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQ29uZmlnLmZpbHRlck9wdGlvbnMuZmluZChvd25lciA9PiBvd25lci5kaXNwbGF5TmFtZSA9PT0gb3duZXJWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkT3duZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVPd25lci5zZXRWYWx1ZShzZWxlY3RlZE93bmVyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0uY29udHJvbHMuZGVsaXZlcmFibGVBcHByb3Zlcikge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoYXBwcm92ZXJWYWx1ZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIShhcHByb3ZlclZhbHVlICYmIHR5cGVvZiBhcHByb3ZlclZhbHVlID09PSAnb2JqZWN0JykpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZEFwcHJvdmVyID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucy5maW5kKGFwcHJvdmVyID0+IGFwcHJvdmVyLmRpc3BsYXlOYW1lID09PSBhcHByb3ZlclZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRBcHByb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5jb250cm9scy5kZWxpdmVyYWJsZUFwcHJvdmVyLnNldFZhbHVlKHNlbGVjdGVkQXBwcm92ZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm9sZEZvcm1WYWx1ZSA9IHRoaXMuZ2V0VXBkYXRlZERlbGl2ZXJhYmxlRGV0YWlscygpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3REZXRhaWxzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgICAgIFByb2plY3RJRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnByb2plY3RJZFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfREVUQUlMU19NRVRIT0RfTlMsIHRoaXMuR0VUX1BST0pFQ1RfREVUQUlMU19XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5Qcm9qZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb2plY3RPYmogPSByZXNwb25zZS5Qcm9qZWN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvamVjdE9iai5SX1BPX0NBVEVHT1JZICYmIHByb2plY3RPYmouUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jYXRlZ29yeUlkID0gcHJvamVjdE9iai5SX1BPX0NBVEVHT1JZWydNUE1fQ2F0ZWdvcnktaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvamVjdE9iai5SX1BPX1RFQU0gJiYgcHJvamVjdE9iai5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcudGVhbUlkID0gcHJvamVjdE9iai5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QgPSByZXNwb25zZS5Qcm9qZWN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgcHJvamVjdCBkZXRhaWxzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrRGV0YWlscygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgICAgICAnVGFzay1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnRhc2tJZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfREVUQUlMU19NRVRIT0RfTlMsIHRoaXMuR0VUX1RBU0tfREVUQUlMU19XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5UYXNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tPYmogPSByZXNwb25zZS5UYXNrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrID0gcmVzcG9uc2UuVGFzaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIHRhc2sgZGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVsaXZlcmFibGVEZXRhaWxzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVNlcnZpY2UucmVhZERlbGl2ZXJhYmxlKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUlkKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkRlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlID0gcmVzcG9uc2UuRGVsaXZlcmFibGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5jdXJyU3RhdHVzID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19TVEFUVVMgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5U3RhdHVzSWQodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19TVEFUVVNbJ01QTV9TdGF0dXMtaWQnXS5JZCkgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgZGVsaXZlcmFibGUgZGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJpb3JpdGllcygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAvKiB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0UHJpb3JpdGllcyhNUE1fTEVWRUxTLkRFTElWRVJBQkxFKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShwcmlvcml0eVJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocHJpb3JpdHlSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocHJpb3JpdHlSZXNwb25zZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaW9yaXR5UmVzcG9uc2UgPSBbcHJpb3JpdHlSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0ID0gcHJpb3JpdHlSZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVQcmlvcml0eUxpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFByaW9yaXRpZXMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pOyAqL1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBwcmlvcml0eSA9ICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0UHJpb3JpdGllcyhNUE1fTEVWRUxTLkRFTElWRVJBQkxFKVxyXG4gICAgICAgICAgICAgICAvLyAuc3Vic2NyaWJlKHByaW9yaXR5UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmlvcml0eSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocHJpb3JpdHkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmlvcml0eSA9IFtwcmlvcml0eV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0ID0gcHJpb3JpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlUHJpb3JpdHlMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIC8qIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBQcmlvcml0aWVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTsgKi9cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTdGF0dXMoc3RhdHVzTGV2ZUxMaXN0OiBBcnJheTxTdGF0dXNMZXZlbD4pIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsKE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHN0YXR1c1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHN0YXR1c1Jlc3BvbnNlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzUmVzcG9uc2UgPSBbc3RhdHVzUmVzcG9uc2VdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3QgPSBzdGF0dXNSZXNwb25zZS5maWx0ZXIoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3RhdHVzTGV2ZUxMaXN0LmluZGV4T2YoZGF0YS5TVEFUVVNfTEVWRUwpID49IDAgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZVN0YXR1c0xpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFN0YXR1c2VzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxSb2xlcygpIHtcclxuICAgICAgICBjb25zdCBvd25lclJvbGVzUGFyYW1zID0ge1xyXG4gICAgICAgICAgICB0ZWFtSUQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgICAgICAgaXNBcHByb3ZlclRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICBpc01lbWJlclRhc2s6IHRydWVcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBhcHByb3ZlclJvbGVzUGFyYW1zID0ge1xyXG4gICAgICAgICAgICB0ZWFtSUQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgICAgICAgaXNBcHByb3ZlclRhc2s6IHRydWUsXHJcbiAgICAgICAgICAgIGlzTWVtYmVyVGFzazogZmFsc2VcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBmb3JrSm9pbihbdGhpcy5hcHBTZXJ2aWNlLmdldFRlYW1Sb2xlcyhvd25lclJvbGVzUGFyYW1zKSwgdGhpcy5hcHBTZXJ2aWNlLmdldFRlYW1Sb2xlcyhhcHByb3ZlclJvbGVzUGFyYW1zKV0pXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2VbMF0uVGVhbXMgJiYgcmVzcG9uc2VbMF0uVGVhbXMuTVBNX1RlYW1zICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlWzBdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcgJiYgcmVzcG9uc2VbMV0uVGVhbXMgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbMV0uVGVhbXMuTVBNX1RlYW1zICYmIHJlc3BvbnNlWzFdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvd25lclJvbGVMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBhcHByb3ZlclJvbGVMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVyUm9sZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXJSb2xlcyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlWzBdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvd25lclJvbGVMaXN0ID0gW3Jlc3BvbnNlWzBdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghQXJyYXkuaXNBcnJheShyZXNwb25zZVsxXS5UZWFtcy5NUE1fVGVhbXMuTVBNX1RlYW1fUm9sZV9NYXBwaW5nKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJSb2xlTGlzdCA9IFtyZXNwb25zZVsxXS5UZWFtcy5NUE1fVGVhbXMuTVBNX1RlYW1fUm9sZV9NYXBwaW5nXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvd25lclJvbGVMaXN0ID0gcmVzcG9uc2VbMF0uVGVhbXMuTVBNX1RlYW1zLk1QTV9UZWFtX1JvbGVfTWFwcGluZztcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXBwcm92ZXJSb2xlTGlzdCA9IHJlc3BvbnNlWzFdLlRlYW1zLk1QTV9UZWFtcy5NUE1fVGVhbV9Sb2xlX01hcHBpbmc7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob3duZXJSb2xlTGlzdCAmJiBvd25lclJvbGVMaXN0Lmxlbmd0aCAmJiBvd25lclJvbGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG93bmVyUm9sZUxpc3QubWFwKHJvbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG93bmVyUm9sZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJvbGVbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogcm9sZS5ST0xFX0ROLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogcm9sZS5OQU1FLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBSb2xlczogcm9sZS5NUE1fQVBQX1JvbGVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcHJvdmVyUm9sZUxpc3QgJiYgYXBwcm92ZXJSb2xlTGlzdC5sZW5ndGggJiYgYXBwcm92ZXJSb2xlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVMaXN0Lm1hcChyb2xlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHByb3ZlclJvbGVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiByb2xlWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHJvbGUuUk9MRV9ETixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJvbGUuTkFNRSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwUm9sZXM6IHJvbGUuTVBNX0FQUF9Sb2xlc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMgPSBvd25lclJvbGVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1BcHByb3ZlclJvbGVzT3B0aW9ucyA9IGFwcHJvdmVyUm9sZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyUm9sZUNvbmZpZy5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS5maWx0ZXJPcHRpb25zID0gdGhpcy50ZWFtQXBwcm92ZXJSb2xlc09wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtT3duZXJSb2xlc09wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtQXBwcm92ZXJSb2xlc09wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFJvbGVzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZXZpZXdlckJ5SWQodXNlcklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMudXRpbFNlcnZpY2UuaXNOdWxsT3JFbXB0eSh1c2VySWQpKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWxpdmVyYWJsZVJldmlld0J5RGVsaXZlcmFibGVJZChkZWxpdmVyYWJsZUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldERlbGl2ZXJhYmxlUmV2aWV3QnlEZWxpdmVyYWJsZUlEKGRlbGl2ZXJhYmxlSWQpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuRGVsaXZlcmFibGVSZXZpZXcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcmV2aWV3ZXJzTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdEZWxpdmVyYWJsZVJldmlldycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmV2aWV3ZXJzTGlzdCAmJiByZXZpZXdlcnNMaXN0Lmxlbmd0aCAmJiByZXZpZXdlcnNMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmlld2Vyc0xpc3QuZm9yRWFjaChyZXZpZXdlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCF0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkocmV2aWV3ZXIuUl9QT19SRVZJRVdFUl9VU0VSWydJZGVudGl0eS1pZCddLklkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldFJldmlld2VyQnlJZChyZXZpZXdlci5SX1BPX1JFVklFV0VSX1VTRVJbJ0lkZW50aXR5LWlkJ10uSWQpLnN1YnNjcmliZShyZXZpZXdlckRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJldmlld2VyRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGhhc1Jldmlld2VyRGF0YSA9IHRoaXMuc2F2ZWRSZXZpZXdlcnMuZmluZChzYXZlZFJldmlld2VyID0+IHNhdmVkUmV2aWV3ZXIudmFsdWUgPT09IHJldmlld2VyRGF0YS51c2VyQ04pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghaGFzUmV2aWV3ZXJEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZWRSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXJEYXRhLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVSZXZpZXdlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXJEYXRhLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBkZWZTZXJ2aWNlLmdldFN0YXR1c0J5SWQocmV2aWV3ZXIuUl9QT19SRVZJRVdfU1RBVFVTWydNUE1fU3RhdHVzLWlkJ10uSWQpLnN1YnNjcmliZShzdGF0dXNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaXNSZXZpZXdlckNvbXBsZXRlZCA9IHN0YXR1c1Jlc3BvbnNlLlNUQVRVU19MRVZFTCA9PT0gJ0ZJTkFMJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXZpZXdlckluZGV4ID0gdGhpcy5yZXZpZXdlclN0YXR1c0RhdGEuZmluZEluZGV4KGRhdGEgPT4gZGF0YS52YWx1ZSA9PT0gcmV2aWV3ZXJEYXRhLnVzZXJDTik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJldmlld2VySW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJldmlld2VyU3RhdHVzRGF0YVtyZXZpZXdlckluZGV4XS5pc0NvbXBsZXRlZCA9IGlzUmV2aWV3ZXJDb21wbGV0ZWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXZpZXdlclN0YXR1c0RhdGEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogcmV2aWV3ZXJEYXRhLnVzZXJDTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiByZXZpZXdlckRhdGEuZnVsbE5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0NvbXBsZXRlZDogaXNSZXZpZXdlckNvbXBsZXRlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5zYXZlZFJldmlld2Vycyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBldmVudERhdGEgPSB7IG1lc3NhZ2U6ICdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBmZXRjaGluZyBEZWxpdmVyYWJsZSBSZXZpZXcgRGV0YWlscycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJldmlld2VycygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFRhc2suVEFTS19UWVBFID09PSBUYXNrVHlwZXMuQVBQUk9WQUxfVEFTSykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGdldFJldmlld2VyVXNlclJlcXVlc3QgPSB7fTtcclxuICAgICAgICAgICAgICAgIGdldFJldmlld2VyVXNlclJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6ICdVU0VSJyxcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNVcGxvYWRUYXNrOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBpc1JldmlldzogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0UmV2aWV3ZXJVc2VyUmVxdWVzdClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyTGlzdC5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdXNlci5uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxSZXZpZXdlcnMgPSB1c2VycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsUmV2aWV3ZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIFJldmlld2VyIFVzZXJzJywgdHlwZTogUHJvamVjdENvbnN0YW50Lk5PVElGSUNBVElPTl9MQUJFTFMuRVJST1IgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KCcnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGdldFVzZXJzQnlSb2xlKGZpZWxkPywgY3VyckFsbG9jYXRpb25UeXBlPykge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgbGV0IGdldE93bmVyQnlSb2xlUmVxdWVzdCA9IHt9O1xyXG4gICAgICAgIGxldCBnZXRBcHByb3ZlckJ5Um9sZVJlcXVlc3QgPSB7fTtcclxuICAgICAgICBsZXQgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSBudWxsO1xyXG4gICAgICAgIGxldCB1c2VyVHlwZTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUZvcm0pIHtcclxuICAgICAgICAgICAgY29uc3QgYWxsb2NhdGlvblR5cGUgPSAoZmllbGQgPT09ICdPV05FUicpID8gdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5vd25lckFsbG9jYXRpb24gOiB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmFwcHJvdmVyQWxsb2NhdGlvbjtcclxuICAgICAgICAgICAgdXNlclR5cGUgPSBmaWVsZDtcclxuICAgICAgICAgICAgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogYWxsb2NhdGlvblR5cGUsXHJcbiAgICAgICAgICAgICAgICByb2xlRE46IGFsbG9jYXRpb25UeXBlID09PSBBbGxvY2F0aW9uVHlwZXMuUk9MRS50b1VwcGVyQ2FzZSgpID9cclxuICAgICAgICAgICAgICAgICAgICAoZmllbGQgPT09ICdPV05FUicgPyB0aGlzLmRlbGl2ZXJhYmxlRm9ybS5nZXRSYXdWYWx1ZSgpLmRlbGl2ZXJhYmxlT3duZXJSb2xlLnZhbHVlIDogdGhpcy5kZWxpdmVyYWJsZUZvcm0uZ2V0UmF3VmFsdWUoKS5kZWxpdmVyYWJsZUFwcHJvdmVyUm9sZS52YWx1ZSkgOiAnJyxcclxuICAgICAgICAgICAgICAgIHRlYW1JRDogYWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5VU0VSLnRvVXBwZXJDYXNlKCkgPyB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fVEVBTVsnTVBNX1RlYW1zLWlkJ10uSWQgOiAnJyxcclxuICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IChmaWVsZCA9PT0gJ09XTkVSJykgPyBmYWxzZSA6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6IChmaWVsZCA9PT0gJ09XTkVSJykgPyB0cnVlIDogZmFsc2VcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkRmllbGQgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkodGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuREVMSVZFUkFCTEVfUEFSRU5UX0lEKSA/ICdPV05FUicgOiAnQVBQUk9WRVInO1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRGaWVsZCA9PT0gJ09XTkVSJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYWxsb2NhdGlvblR5cGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5PV05FUl9BU1NJR05NRU5UX1RZUEU7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZFJvbGVJZCA9IGFsbG9jYXRpb25UeXBlID09PSBBbGxvY2F0aW9uVHlwZXMuUk9MRS50b1VwcGVyQ2FzZSgpID8gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19PV05FUl9ST0xFX0lEWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgdXNlclR5cGUgPSBzZWxlY3RlZEZpZWxkO1xyXG4gICAgICAgICAgICAgICAgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6IGFsbG9jYXRpb25UeXBlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVETjogYWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgPyB0aGlzLmZvcm1Sb2xlVmFsdWUoc2VsZWN0ZWRSb2xlSWQsIGZhbHNlKS52YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG93bmVyQWxsb2NhdGlvblR5cGUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5PV05FUl9BU1NJR05NRU5UX1RZUEU7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhcHByb3ZlckFsbG9jYXRpb25UeXBlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuQVBQUk9WRVJfQVNTSUdOTUVOVF9UWVBFO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRPd25lclJvbGVJZCA9IG93bmVyQWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fT1dORVJfUk9MRV9JRFsnTVBNX1RlYW1fUm9sZV9NYXBwaW5nLWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkQXBwcm92ZXJSb2xlSWQgPSBhcHByb3ZlckFsbG9jYXRpb25UeXBlID09PSBBbGxvY2F0aW9uVHlwZXMuUk9MRS50b1VwcGVyQ2FzZSgpID9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX0FQUFJPVkVSX1JPTEVfSURbJ01QTV9UZWFtX1JvbGVfTWFwcGluZy1pZCddLklkIDogJyc7XHJcbiAgICAgICAgICAgICAgICBnZXRPd25lckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6IG93bmVyQWxsb2NhdGlvblR5cGUsXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZUROOiBvd25lckFsbG9jYXRpb25UeXBlID09PSBBbGxvY2F0aW9uVHlwZXMuUk9MRS50b1VwcGVyQ2FzZSgpID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtUm9sZVZhbHVlKHNlbGVjdGVkT3duZXJSb2xlSWQsIGZhbHNlKS52YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIGdldEFwcHJvdmVyQnlSb2xlUmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogYXBwcm92ZXJBbGxvY2F0aW9uVHlwZSxcclxuICAgICAgICAgICAgICAgICAgICByb2xlRE46IGFwcHJvdmVyQWxsb2NhdGlvblR5cGUgPT09IEFsbG9jYXRpb25UeXBlcy5ST0xFLnRvVXBwZXJDYXNlKCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Sb2xlVmFsdWUoc2VsZWN0ZWRBcHByb3ZlclJvbGVJZCwgc2VsZWN0ZWRGaWVsZCA9PT0gJ0FQUFJPVkVSJykudmFsdWUgOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICB0ZWFtSUQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICBpc0FwcHJvdmVUYXNrOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzVXBsb2FkVGFzazogZmFsc2VcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBnZXRPd25lckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogJ1VTRVInLFxyXG4gICAgICAgICAgICAgICAgcm9sZUROOiAnJyxcclxuICAgICAgICAgICAgICAgIHRlYW1JRDogdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5SX1BPX1RFQU1bJ01QTV9UZWFtcy1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgaXNBcHByb3ZlVGFzazogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6IHRydWVcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgZ2V0QXBwcm92ZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgYWxsb2NhdGlvblR5cGU6ICdVU0VSJyxcclxuICAgICAgICAgICAgICAgIHJvbGVETjogJycsXHJcbiAgICAgICAgICAgICAgICB0ZWFtSUQ6IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgIGlzQXBwcm92ZVRhc2s6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBpc1VwbG9hZFRhc2s6IGZhbHNlXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChnZXRVc2VyQnlSb2xlUmVxdWVzdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRVc2VyQnlSb2xlUmVxdWVzdClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnVzZXJzICYmIHJlc3BvbnNlLnVzZXJzLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlckxpc3QgJiYgdXNlckxpc3QubGVuZ3RoICYmIHVzZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VyTGlzdC5mb3JFYWNoKHVzZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdXNlci5uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHVzZXJUeXBlID09PSAnQVBQUk9WRVInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50ZWFtUm9sZUFwcHJvdmVyT3B0aW9ucyA9IHVzZXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyLmZpbHRlck9wdGlvbnMgPSB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zID0gdXNlcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlclR5cGUgPT09ICdBUFBST1ZFUicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgSW5kaXZpZHVhbCBVc2VycycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0T3duZXJCeVJvbGVSZXF1ZXN0KSwgdGhpcy5hcHBTZXJ2aWNlLmdldFVzZXJzRm9yVGVhbShnZXRBcHByb3ZlckJ5Um9sZVJlcXVlc3QpXSlcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlWzBdLnVzZXJzICYmIHJlc3BvbnNlWzBdLnVzZXJzLnVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlWzFdLnVzZXJzICYmIHJlc3BvbnNlWzFdLnVzZXJzLnVzZXIpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBvd25lckxpc3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlWzBdLCAndXNlcicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXJMaXN0ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZVsxXSwgJ3VzZXInKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXJzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAob3duZXJMaXN0ICYmIG93bmVyTGlzdC5sZW5ndGggJiYgb3duZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvd25lckxpc3QuZm9yRWFjaChvd25lciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gb3duZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSBvd25lci5kbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG93bmVycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBvd25lci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogb3duZXIuZG4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IG93bmVyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXBwcm92ZXJMaXN0ICYmIGFwcHJvdmVyTGlzdC5sZW5ndGggJiYgYXBwcm92ZXJMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHByb3Zlckxpc3QuZm9yRWFjaChhcHByb3ZlciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkT2JqID0gYXBwcm92ZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSBhcHByb3Zlci5kbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcHJvdmVycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBhcHByb3Zlci5jbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogYXBwcm92ZXIuZG4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IGFwcHJvdmVyLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zID0gb3duZXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJDb25maWcuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVPd25lck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zID0gYXBwcm92ZXJzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlQXBwcm92ZXIuZmlsdGVyT3B0aW9ucyA9IHRoaXMudGVhbVJvbGVBcHByb3Zlck9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlT3duZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlYW1Sb2xlQXBwcm92ZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50RGF0YSA9IHsgbWVzc2FnZTogJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGZldGNoaW5nIEluZGl2aWR1YWwgVXNlcnMnLCB0eXBlOiBQcm9qZWN0Q29uc3RhbnQuTk9USUZJQ0FUSU9OX0xBQkVMUy5FUlJPUiB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkhhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2lnbm1lbnRUeXBlcygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkFTU0lHTk1FTlRfVFlQRV9NRVRIT0RfTlMsIHRoaXMuR0VUX0FTU0lHTk1FTlRfVFlQRV9XU19NRVRIT0RfTkFNRSwgbnVsbClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5Bc3NpZ25tZW50X1R5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHJlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLkFzc2lnbm1lbnRfVHlwZSA9IFtyZXNwb25zZS5Bc3NpZ25tZW50X1R5cGVdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZU93bmVyQXNzaWdubWVudFR5cGUgPSByZXNwb25zZS5Bc3NpZ25tZW50X1R5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5kZWxpdmVyYWJsZUFwcHJvdmVyQXNzaWdubWVudFR5cGUgPSByZXNwb25zZS5Bc3NpZ25tZW50X1R5cGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLmRlbGl2ZXJhYmxlT3duZXJBc3NpZ25tZW50VHlwZSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuZGVsaXZlcmFibGVBcHByb3ZlckFzc2lnbm1lbnRUeXBlID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IGNoZWNrIGZvciBkZWZhdWx0UHJpb3JpdHlcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZXZlbnREYXRhID0geyBtZXNzYWdlOiAnU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZmV0Y2hpbmcgQXNzaWdubWVudCB0eXBlcycsIHR5cGU6IFByb2plY3RDb25zdGFudC5OT1RJRklDQVRJT05fTEFCRUxTLkVSUk9SIH07XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25IYW5kbGVyLm5leHQoZXZlbnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJlRGV0YWlscygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlSWQpIHtcclxuICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmdldERlbGl2ZXJhYmxlRGV0YWlscygpLCB0aGlzLmdldEFsbFJvbGVzKCldKS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmdldFByaW9yaXRpZXMoKSwgdGhpcy5nZXRVc2Vyc0J5Um9sZSgpLCB0aGlzLmdldFJldmlld2VycygpLCB0aGlzLmdldERlbGl2ZXJhYmxlUmV2aWV3QnlEZWxpdmVyYWJsZUlkKHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVJZCldKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRlbFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fQ0FURUdPUllbJ01QTV9DYXRlZ29yeS1pZCddLklkICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFByb2plY3QuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHN0YXR1c0xpc3QgPSBbU3RhdHVzTGV2ZWxzLklOVEVSTUVESUFURSwgU3RhdHVzTGV2ZWxzLklOSVRJQUxdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuY3VyclN0YXR1cy5TVEFUVVNfTEVWRUwgPT09IFN0YXR1c0xldmVscy5JTlRFUk1FRElBVEUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzTGlzdC5wb3AoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXJWYWx1ZSA9IHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fQVBQUk9WRVJfVVNFUl9JRCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX0FQUFJPVkVSX1VTRVJfSURbJ0lkZW50aXR5LWlkJ10gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWREZWxpdmVyYWJsZS5SX1BPX0FQUFJPVkVSX1VTRVJfSURbJ0lkZW50aXR5LWlkJ10uSWQgOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBvd25lclZhbHVlID0gdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUgJiYgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUuUl9QT19PV05FUl9VU0VSX0lEICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fT1dORVJfVVNFUl9JRFsnSWRlbnRpdHktaWQnXSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZERlbGl2ZXJhYmxlLlJfUE9fT1dORVJfVVNFUl9JRFsnSWRlbnRpdHktaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmdldFN0YXR1cyhzdGF0dXNMaXN0KSwgdGhpcy5mb3JtQXBwcm92ZXJWYWx1ZShhcHByb3ZlclZhbHVlKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Pd25lclZhbHVlKG93bmVyVmFsdWUpXSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZUxpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgZGVsRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGRlbEVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvcmtKb2luKFt0aGlzLmdldFByaW9yaXRpZXMoKSwgdGhpcy5nZXRTdGF0dXMoW1N0YXR1c0xldmVscy5JTklUSUFMXSksIHRoaXMuZ2V0QWxsUm9sZXMoKSwgdGhpcy5nZXRVc2Vyc0J5Um9sZSgpXSlcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlTGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG93bmVyVmFsdWUgPSB0aGlzLmRlbGl2ZXJhYmxlTW9kYWxDb25maWcuc2VsZWN0ZWRUYXNrICYmIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZU1vZGFsQ29uZmlnLnNlbGVjdGVkVGFzay5SX1BPX09XTkVSX0lEWydJZGVudGl0eS1pZCddID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZy5zZWxlY3RlZFRhc2suUl9QT19PV05FUl9JRFsnSWRlbnRpdHktaWQnXS5JZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JrSm9pbihbdGhpcy5mb3JtT3duZXJWYWx1ZShvd25lclZhbHVlKV0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbmNlbERlbGl2ZXJhYmxlQ3JlYXRpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVGb3JtLnByaXN0aW5lKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogJ0FyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjbG9zZT8nLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0LmlzVHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VDYWxsYmFja0hhbmRsZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE1heE1pblZhbHVlRm9yTnVtYmVyKHNjYWxlKTogYW55IHtcclxuICAgICAgICBsZXQgbnVtZXJpY2VWbGFsdWUgPSAnOTk5OTknO1xyXG4gICAgICAgIGlmIChzY2FsZSkge1xyXG4gICAgICAgICAgICBudW1lcmljZVZsYWx1ZSArPSAnLicgKyBuZXdBcnJheShzY2FsZSkuZmlsbCgnOScpLmpvaW4oJycpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHJhZGl4XHJcbiAgICAgICAgcmV0dXJuIHNjYWxlID8gcGFyc2VGbG9hdChudW1lcmljZVZsYWx1ZSkgOiBwYXJzZUludChudW1lcmljZVZsYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGlzZURlbGl2ZXJhYmxlQ29uZmlnKCkge1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVNb2RhbENvbmZpZyA9IHtcclxuICAgICAgICAgICAgaGFzQWxsQ29uZmlnOiB0cnVlLFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZVByaW9yaXR5TGlzdDogbnVsbCxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVTdGF0dXNMaXN0OiBudWxsLFxyXG4gICAgICAgICAgICBkZWZhdWx0U3RhdHVzOiBudWxsLFxyXG4gICAgICAgICAgICBkZWZhdWx0UHJpb3JpdHk6IG51bGwsXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJDb25maWc6IHtcclxuICAgICAgICAgICAgICAgIGxhYmVsOiAnRGVsaXZlcmFibGUgT3duZXInLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZU93bmVyUm9sZUNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZWxpdmVyYWJsZSBPd25lciBSb2xlJyxcclxuICAgICAgICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2w6IG51bGxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVBcHByb3Zlcjoge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdEZWxpdmVyYWJsZSBBcHByb3ZlcicsXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXSxcclxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sOiBudWxsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlQXBwcm92ZXJSb2xlOiB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0RlbGl2ZXJhYmxlIEFwcHJvdmVyIFJvbGUnLFxyXG4gICAgICAgICAgICAgICAgZmlsdGVyT3B0aW9uczogW10sXHJcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbDogbnVsbFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzZWxlY3RlZERlbGl2ZXJhYmxlOiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkRGVsaXZlcmFibGUsXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlT3duZXJBc3NpZ25tZW50VHlwZTogRGVsaXZlcmFibGVDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFLFxyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZUFwcHJvdmVyQXNzaWdubWVudFR5cGU6IERlbGl2ZXJhYmxlQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRSxcclxuICAgICAgICAgICAgaXNOZXdEZWxpdmVyYWJsZTogKHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVJZCkgPyBmYWxzZSA6IHRydWUsXHJcbiAgICAgICAgICAgIGlzVGVtcGxhdGU6IHRoaXMuZGVsaXZlcmFibGVDb25maWcuaXNUZW1wbGF0ZSxcclxuICAgICAgICAgICAgcHJvamVjdElkOiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgdGFza0lkOiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnRhc2tJZCxcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVJZDogdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFByb2plY3Q6IHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRQcm9qZWN0LFxyXG4gICAgICAgICAgICBzZWxlY3RlZFRhc2s6IHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRUYXNrLFxyXG4gICAgICAgICAgICBtaW5EYXRlOiAnJyxcclxuICAgICAgICAgICAgbWF4RGF0ZTogJycsXHJcbiAgICAgICAgICAgIGRlbGl2ZXJhYmxlQWxsb2NhdGlvbkxpc3Q6IFtdLFxyXG4gICAgICAgICAgICBjdXJyU3RhdHVzOiBudWxsXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZU9wdGlvbnMgPSBbdGhpcy5zYXZlT3B0aW9uc1swXV07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlck9iamVjdCgpO1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyLk1hbmFnZXJGb3IuVGFyZ2V0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh1c2VyLk5hbWUgPT09ICdNUE0gUHJvamVjdCBNYW5hZ2VyJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc1BNID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcblxyXG4gICAgICAgIGlmIChuYXZpZ2F0b3IubGFuZ3VhZ2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLmFkYXB0ZXIuc2V0TG9jYWxlKG5hdmlnYXRvci5sYW5ndWFnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaXNBcHByb3ZlciA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0SXNBcHByb3ZlcigpO1xyXG4gICAgICAgIHRoaXMuaXNVcGxvYWRlZCA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0SXNVcGxvYWRlZCgpO1xyXG4gICAgICAgIHRoaXMucHJvamVjdENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UHJvamVjdENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmVlZFJldmlldyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMucHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX05FRURfUkVWSUVXXSk7XHJcbiAgICAgICAgdGhpcy5lbmFibGVXb3JrV2VlayA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0Qm9vbGVhblZhbHVlKHRoaXMucHJvamVjdENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fUFJPSkVDVF9DT05GSUcuRU5BQkxFX1dPUktfV0VFS10pO1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBcHBDb25maWcoKTtcclxuICAgICAgICB0aGlzLm5hbWVTdHJpbmdQYXR0ZXJuID0gdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLk5BTUVfU1RSSU5HX1BBVFRFUk5dKSA/ICcuKicgOlxyXG4gICAgICAgICAgICB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5OQU1FX1NUUklOR19QQVRURVJOXTtcclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyAmJiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnRhc2tJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmluaXRpYWxpc2VEZWxpdmVyYWJsZUNvbmZpZygpO1xyXG4gICAgICAgICAgICB0aGlzLmdldFByZURldGFpbHMoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbml0aWFsaXNlRGVsaXZlcmFibGVGb3JtKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkYXRlRmlsdGVyOiAoZGF0ZTogRGF0ZSB8IG51bGwpID0+IGJvb2xlYW4gPVxyXG4gICAgKGRhdGU6IERhdGUgfCBudWxsKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlV29ya1dlZWspIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgZGF5ID0gZGF0ZS5nZXREYXkoKTtcclxuICAgICAgICAgICAgcmV0dXJuIGRheSAhPT0gMCAmJiBkYXkgIT09IDY7XHJcbiAgICAgICAgICAgIC8vMCBtZWFucyBzdW5kYXlcclxuICAgICAgICAgICAgLy82IG1lYW5zIHNhdHVyZGF5XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==