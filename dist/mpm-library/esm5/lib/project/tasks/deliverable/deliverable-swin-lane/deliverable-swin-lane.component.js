import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { TaskService } from '../../task.service';
import { FieldConfigService } from '../../../../shared/services/field-config.service';
import { MPMFieldConstants } from '../../../../shared/constants/mpm.field.constants';
import { MPMFieldKeys } from '../../../../mpm-utils/objects/MPMField';
import { DeliverableBulkCountService } from '../../../../deliverable-task/service/deliverable-bulk-count.service';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
var DeliverableSwinLaneComponent = /** @class */ (function () {
    function DeliverableSwinLaneComponent(media, changeDetectorRef, taskService, fieldConfigService, deliverableBulkCountService) {
        var _this = this;
        this.media = media;
        this.changeDetectorRef = changeDetectorRef;
        this.taskService = taskService;
        this.fieldConfigService = fieldConfigService;
        this.deliverableBulkCountService = deliverableBulkCountService;
        this.refreshSwimLine = new EventEmitter();
        this.loadingHandler = new EventEmitter();
        this.notificationHandler = new EventEmitter();
        this.editDeliverableHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.deliverableDetailsHandler = new EventEmitter();
        this.versionsHandler = new EventEmitter();
        this.shareAssetHandler = new EventEmitter();
        this.deliverableBulkEditCount = new EventEmitter();
        this.selectedDeliverableCount = new EventEmitter();
        this.selectedDeliverableDatas = new EventEmitter();
        this.selectedDeliverableData = new EventEmitter();
        this.swimListIds = [];
        this.swimList = [];
        this.deliverableId = [];
        this.masterSelected = false;
        this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = function () { return _this.changeDetectorRef.detectChanges(); };
        if (this.mobileQuery.matches) {
            // tslint:disable-next-line: deprecation
            this.mobileQuery.addListener(this.mobileQueryListener);
        }
    }
    DeliverableSwinLaneComponent.prototype.editDeliverable = function (eventData) {
        this.editDeliverableHandler.emit(eventData);
    };
    DeliverableSwinLaneComponent.prototype.masterToogle = function (event) {
        this.masterToogleDeliverable.emit(event);
    };
    DeliverableSwinLaneComponent.prototype.openCR = function (event) {
        this.crHandler.emit(event);
    };
    DeliverableSwinLaneComponent.prototype.deliverableDetails = function (event) {
        this.deliverableDetailsHandler.emit(event);
    };
    DeliverableSwinLaneComponent.prototype.deliverableBulkCheckCount = function (deliverableData) {
        var id;
        var deliverableid;
        if (this.masterSelectedDeliverable === true) {
            this.masterSelected = false;
        }
        if (this.masterSelectedDeliverable === false && !this.masterSelected) {
            id = [];
            deliverableid = true;
            this.masterSelected = true;
        }
        id = this.deliverableBulkCountService.getBulkDeliverableId(deliverableData, deliverableid);
        /*  if (this.deliverableId && !this.deliverableId.includes(deliverableData)) {
             this.deliverableId.push(deliverableData);
         } else {
             const index = this.deliverableId.indexOf(deliverableData);
             if (index > -1) {
                 this.deliverableId.splice(index, 1);
             }
         }
  */
        if (id.length > 1) {
            this.deliverableBulkEditCount.emit(true);
        }
        else {
            this.deliverableBulkEditCount.emit(false);
        }
        this.selectedDeliverableCount.emit(id.length);
        this.selectedDeliverableDatas.next(id);
    };
    DeliverableSwinLaneComponent.prototype.selectedBulkDeliverableData = function (data) {
        this.selectedDeliverableData.next(data);
    };
    DeliverableSwinLaneComponent.prototype.viewVersions = function (event) {
        this.versionsHandler.emit(event);
    };
    DeliverableSwinLaneComponent.prototype.shareAsset = function (event) {
        this.shareAssetHandler.emit(event);
    };
    DeliverableSwinLaneComponent.prototype.getConnectedId = function (id) {
        return this.swimListIds.filter(function (ids) { return ids !== id; });
    };
    DeliverableSwinLaneComponent.prototype.refresh = function () {
        this.refreshSwimLine.next(true);
    };
    DeliverableSwinLaneComponent.prototype.refreshDeliverable = function (event) {
        if (event) {
            this.refresh();
        }
    };
    DeliverableSwinLaneComponent.prototype.initialiseSwimList = function () {
        var _this = this;
        this.swimList = [];
        this.swimListIds = [];
        if (this.deliverableConfig) {
            if (this.deliverableConfig.deliverableStatus) {
                var mpmStatusField_1 = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, MPMFieldConstants.DELIVERABLE_MPM_FIELDS.DELIVERABLE_STATUS_ID, MPM_LEVELS.DELIVERABLE);
                this.deliverableConfig.deliverableStatus.map(function (status) {
                    var listData = [];
                    if (_this.deliverableConfig.deliverableTotalCount > 0) {
                        _this.deliverableConfig.deliverableList.map(function (deliverable) {
                            if (_this.fieldConfigService.getFieldValueByDisplayColumn(mpmStatusField_1, deliverable) === status['MPM_Status-id'].Id) {
                                if (deliverable.DELIVERABLE_TYPE !== 'COMPLETE' && deliverable.DELIVERABLE_STATUS_VALUE !== 'Approved' && deliverable.DELIVERABLE_STATUS_VALUE !== 'Cancelled'
                                    && deliverable.DELIVERABLE_STATUS_VALUE !== 'Rejected' && deliverable.DELIVERABLE_TYPE === 'REVIEW') {
                                    deliverable.bulkEdit = true;
                                }
                                else {
                                    deliverable.bulkEdit = false;
                                }
                                if (deliverable.IS_BULK_UPDATE_DELIVERABLE !== 'true' && deliverable.BULK_ACTION !== 'true') {
                                    listData.push(deliverable);
                                }
                            }
                        });
                    }
                    var swimList = {
                        id: status['MPM_Status-id'].Id,
                        listDetail: status,
                        data: listData,
                        dataCount: listData.length
                    };
                    _this.swimListIds.push(status['MPM_Status-id'].Id);
                    _this.swimList.push(swimList);
                });
            }
        }
        if (this.deliverableConfig && this.deliverableConfig.deliverableTotalCount !== 0) {
            this.deliverableSwimLaneMinHeight = '31rem';
        }
    };
    DeliverableSwinLaneComponent.prototype.ngOnInit = function () {
        this.initialiseSwimList();
    };
    DeliverableSwinLaneComponent.ctorParameters = function () { return [
        { type: MediaMatcher },
        { type: ChangeDetectorRef },
        { type: TaskService },
        { type: FieldConfigService },
        { type: DeliverableBulkCountService }
    ]; };
    __decorate([
        Input()
    ], DeliverableSwinLaneComponent.prototype, "deliverableConfig", void 0);
    __decorate([
        Input()
    ], DeliverableSwinLaneComponent.prototype, "isViewByResourceFilterEnabled", void 0);
    __decorate([
        Input()
    ], DeliverableSwinLaneComponent.prototype, "deliverableBulkResponse", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "refreshSwimLine", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "loadingHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "notificationHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "editDeliverableHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "crHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "deliverableDetailsHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "versionsHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "shareAssetHandler", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "deliverableBulkEditCount", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "selectedDeliverableCount", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "selectedDeliverableDatas", void 0);
    __decorate([
        Output()
    ], DeliverableSwinLaneComponent.prototype, "selectedDeliverableData", void 0);
    __decorate([
        Input()
    ], DeliverableSwinLaneComponent.prototype, "masterSelectedDeliverable", void 0);
    DeliverableSwinLaneComponent = __decorate([
        Component({
            selector: 'mpm-deliverable-swin-lane',
            template: "<div class=\"flex-col \"\r\n    [ngClass]=\"{'swim-layout':isViewByResourceFilterEnabled,'swim-layout-one':!isViewByResourceFilterEnabled}\">\r\n    <div class=\"flex-col-item\">\r\n        <div class=\"swim-nonDrag-columns\">\r\n            <div class=\"flex-row-item swim-nonDrag-column deliverable-swim\" *ngFor=\"let list of swimList\">\r\n                <div class=\"grid-list\">\r\n                    <div class=\"card-wrap\">\r\n                        <div class=\"mpm-card\">\r\n                            <section class=\"card-summary\">{{list.listDetail.NAME}}\r\n                                ({{list.dataCount}})</section>\r\n                        </div>\r\n                    </div>\r\n                    <span>\r\n                        <div class=\"swim-data-card\" *ngFor=\"let item of list.data\">\r\n                            <mpm-deliverable-card [isPMView]=\"true\" [deliverableData]=\"item\"\r\n                                [deliverableBulkResponse]=\"deliverableBulkResponse\"\r\n                                (deliverableBulkCheckCount)=\"deliverableBulkCheckCount($event)\"\r\n                                (selectedDeliverableData)=\"selectedBulkDeliverableData($event)\"\r\n                                [isTemplate]=\"deliverableConfig.isTemplate\"\r\n                                (refreshDeliverable)=\"refreshDeliverable($event)\"\r\n                                (editDeliverableHandler)=\"editDeliverable($event)\" (crHandler)=\"openCR($event)\"\r\n                                (deliverableDetailsHandler)=\"deliverableDetails($event)\"\r\n                                [masterSelectedDeliverable]=\"masterSelectedDeliverable\"\r\n                                (versionsHandler)=\"viewVersions($event)\" (shareAssetHandler)=\"shareAsset($event)\">\r\n                            </mpm-deliverable-card><!-- [projectId]=\"deliverableConfig.projectId\" -->\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>",
            styles: [".swim-layout{height:calc(100vh - 154px);overflow:auto}.swim-layout-one{height:calc(100vh - 200px);overflow:auto}.swim-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0}.swim-column{box-sizing:border-box;list-style:none;margin:0;position:relative;vertical-align:top;justify-content:center;flex:1 1 0}.swim-data-card{margin-bottom:10px}.swim-nonDrag-columns{box-sizing:border-box;border-spacing:10px 0;list-style:none;margin:0;width:100%;padding:0;display:flex;flex-direction:row}.swim-nonDrag-column{box-sizing:border-box;display:table-cell;list-style:none;margin:0 10px 0 0;position:relative;vertical-align:top;table-layout:fixed;padding-left:5px;padding-right:5px;max-width:15rem;border:1px solid #ddd}::ng-deep .swim-data-card .task-content{margin:5px}::ng-deep app-task-card-view .task-card{min-width:180px}.cdk-drag-preview{box-sizing:border-box;border-radius:4px;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.cdk-drag-placeholder{opacity:0}.cdk-drag-animating{transition:transform 250ms cubic-bezier(0,0,.2,1)}.card-box:last-child{border:none}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder){transition:transform 250ms cubic-bezier(0,0,.2,1)}.grid-list.cdk-drop-list-dragging .card-box:not(.cdk-drag-placeholder) .grid-list{border:1px solid red}.mpm-card,.mpm-deliverable{font-size:14px;margin-top:5px;margin-bottom:5px;padding:10px;position:relative;transition:background-color 140ms ease-in-out,border-color 75ms ease-in-out}.card-summary{display:block;box-sizing:content-box;line-height:1.42857143;max-height:4.28571429em;overflow:hidden}.deliverable-swim{width:240px}.deliverable-data-card{margin:0 0 15px}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}"]
        })
    ], DeliverableSwinLaneComponent);
    return DeliverableSwinLaneComponent;
}());
export { DeliverableSwinLaneComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtc3dpbi1sYW5lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUtc3dpbi1sYW5lL2RlbGl2ZXJhYmxlLXN3aW4tbGFuZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFakQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDdEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDckYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFFQUFxRSxDQUFBO0FBQ2pILE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQVNqRTtJQStCSSxzQ0FDVyxLQUFtQixFQUNuQixpQkFBb0MsRUFDcEMsV0FBd0IsRUFDeEIsa0JBQXNDLEVBQ3RDLDJCQUF3RDtRQUxuRSxpQkFhQztRQVpVLFVBQUssR0FBTCxLQUFLLENBQWM7UUFDbkIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGdDQUEyQixHQUEzQiwyQkFBMkIsQ0FBNkI7UUFoQ3pELG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUMxQyxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDekMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM5QywyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ2pELGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BDLDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDcEQsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFFNUMsNkJBQXdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNuRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRW5ELDZCQUF3QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbkQsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQU01RCxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBRWQsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFJbkIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFRbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxFQUF0QyxDQUFzQyxDQUFDO1FBQ3hFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUU7WUFDMUIsd0NBQXdDO1lBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQzFEO0lBQ0wsQ0FBQztJQUVELHNEQUFlLEdBQWYsVUFBZ0IsU0FBUztRQUNyQixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxtREFBWSxHQUFaLFVBQWEsS0FBSztRQUNkLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7SUFDNUMsQ0FBQztJQUVELDZDQUFNLEdBQU4sVUFBTyxLQUFLO1FBQ1IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELHlEQUFrQixHQUFsQixVQUFtQixLQUFLO1FBQ3BCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELGdFQUF5QixHQUF6QixVQUEwQixlQUFlO1FBQ3JDLElBQUksRUFBRSxDQUFDO1FBQ1AsSUFBSSxhQUFhLENBQUM7UUFDbEIsSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssSUFBSSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1NBQy9CO1FBQ0QsSUFBSSxJQUFJLENBQUMseUJBQXlCLEtBQUssS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNsRSxFQUFFLEdBQUcsRUFBRSxDQUFDO1lBQ1IsYUFBYSxHQUFHLElBQUksQ0FBQztZQUNyQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtRQUVELEVBQUUsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsb0JBQW9CLENBQUMsZUFBZSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQzNGOzs7Ozs7OztJQVFKO1FBQ0ksSUFBSSxFQUFFLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNmLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUM7YUFBTTtZQUNILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0M7UUFDRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBRTNDLENBQUM7SUFFRCxrRUFBMkIsR0FBM0IsVUFBNEIsSUFBSTtRQUM1QixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxtREFBWSxHQUFaLFVBQWEsS0FBSztRQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxpREFBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELHFEQUFjLEdBQWQsVUFBZSxFQUFFO1FBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsS0FBSyxFQUFFLEVBQVYsQ0FBVSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELDhDQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQseURBQWtCLEdBQWxCLFVBQW1CLEtBQUs7UUFDcEIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDbEI7SUFDTCxDQUFDO0lBRUQseURBQWtCLEdBQWxCO1FBQUEsaUJBcUNDO1FBcENHLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3hCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixFQUFFO2dCQUMxQyxJQUFNLGdCQUFjLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNwTCxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFVBQUEsTUFBTTtvQkFDL0MsSUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNwQixJQUFJLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxDQUFDLEVBQUU7d0JBQ2xELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFVBQUEsV0FBVzs0QkFDbEQsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsZ0JBQWMsRUFBRSxXQUFXLENBQUMsS0FBSyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxFQUFFO2dDQUNsSCxJQUFJLFdBQVcsQ0FBQyxnQkFBZ0IsS0FBSyxVQUFVLElBQUksV0FBVyxDQUFDLHdCQUF3QixLQUFLLFVBQVUsSUFBSSxXQUFXLENBQUMsd0JBQXdCLEtBQUssV0FBVzt1Q0FDdkosV0FBVyxDQUFDLHdCQUF3QixLQUFLLFVBQVUsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEtBQUssUUFBUSxFQUFFO29DQUNyRyxXQUFXLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztpQ0FDL0I7cUNBQU07b0NBQ0gsV0FBVyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7aUNBQ2hDO2dDQUNELElBQUksV0FBVyxDQUFDLDBCQUEwQixLQUFLLE1BQU0sSUFBSSxXQUFXLENBQUMsV0FBVyxLQUFLLE1BQU0sRUFBRTtvQ0FDekYsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztpQ0FDOUI7NkJBQ0o7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsSUFBTSxRQUFRLEdBQUc7d0JBQ2IsRUFBRSxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFO3dCQUM5QixVQUFVLEVBQUUsTUFBTTt3QkFDbEIsSUFBSSxFQUFFLFFBQVE7d0JBQ2QsU0FBUyxFQUFFLFFBQVEsQ0FBQyxNQUFNO3FCQUM3QixDQUFDO29CQUNGLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDbEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxDQUFDO2FBQ047U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsS0FBSyxDQUFDLEVBQUU7WUFDOUUsSUFBSSxDQUFDLDRCQUE0QixHQUFHLE9BQU8sQ0FBQztTQUMvQztJQUNMLENBQUM7SUFFRCwrQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDOUIsQ0FBQzs7Z0JBaklpQixZQUFZO2dCQUNBLGlCQUFpQjtnQkFDdkIsV0FBVztnQkFDSixrQkFBa0I7Z0JBQ1QsMkJBQTJCOztJQW5DMUQ7UUFBUixLQUFLLEVBQUU7MkVBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO3VGQUErQjtJQUM5QjtRQUFSLEtBQUssRUFBRTtpRkFBeUI7SUFDdkI7UUFBVCxNQUFNLEVBQUU7eUVBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFO3dFQUEwQztJQUN6QztRQUFULE1BQU0sRUFBRTs2RUFBK0M7SUFDOUM7UUFBVCxNQUFNLEVBQUU7Z0ZBQWtEO0lBQ2pEO1FBQVQsTUFBTSxFQUFFO21FQUFxQztJQUNwQztRQUFULE1BQU0sRUFBRTttRkFBcUQ7SUFDcEQ7UUFBVCxNQUFNLEVBQUU7eUVBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFOzJFQUE2QztJQUU1QztRQUFULE1BQU0sRUFBRTtrRkFBb0Q7SUFDbkQ7UUFBVCxNQUFNLEVBQUU7a0ZBQW9EO0lBRW5EO1FBQVQsTUFBTSxFQUFFO2tGQUFvRDtJQUNuRDtRQUFULE1BQU0sRUFBRTtpRkFBbUQ7SUFDbkQ7UUFBUixLQUFLLEVBQUU7bUZBQTJCO0lBbEIxQiw0QkFBNEI7UUFOeEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxzaUVBQXFEOztTQUV4RCxDQUFDO09BRVcsNEJBQTRCLENBbUt4QztJQUFELG1DQUFDO0NBQUEsQUFuS0QsSUFtS0M7U0FuS1ksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWVkaWFNYXRjaGVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCc7XHJcbmltcG9ydCB7IFRhc2tTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vdGFzay5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVDb25zdGFudHMgfSBmcm9tICcuLi9kZWxpdmVyYWJsZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zaGFyZWQvc2VydmljZXMvZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZENvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uLy4uL3NoYXJlZC9jb25zdGFudHMvbXBtLmZpZWxkLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL2RlbGl2ZXJhYmxlLWJ1bGstY291bnQuc2VydmljZSdcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWRlbGl2ZXJhYmxlLXN3aW4tbGFuZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGVsaXZlcmFibGUtc3dpbi1sYW5lLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RlbGl2ZXJhYmxlLXN3aW4tbGFuZS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVTd2luTGFuZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBASW5wdXQoKSBkZWxpdmVyYWJsZUNvbmZpZztcclxuICAgIEBJbnB1dCgpIGlzVmlld0J5UmVzb3VyY2VGaWx0ZXJFbmFibGVkO1xyXG4gICAgQElucHV0KCkgZGVsaXZlcmFibGVCdWxrUmVzcG9uc2U7XHJcbiAgICBAT3V0cHV0KCkgcmVmcmVzaFN3aW1MaW5lID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgbG9hZGluZ0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBub3RpZmljYXRpb25IYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgZWRpdERlbGl2ZXJhYmxlSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRlbGl2ZXJhYmxlRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB2ZXJzaW9uc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzaGFyZUFzc2V0SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBPdXRwdXQoKSBkZWxpdmVyYWJsZUJ1bGtFZGl0Q291bnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlQ291bnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWREZWxpdmVyYWJsZURhdGFzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWREZWxpdmVyYWJsZURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBJbnB1dCgpIG1hc3RlclNlbGVjdGVkRGVsaXZlcmFibGU7XHJcbiAgICBtYXN0ZXJUb29nbGVEZWxpdmVyYWJsZTtcclxuICAgIG1vYmlsZVF1ZXJ5OiBNZWRpYVF1ZXJ5TGlzdDtcclxuICAgIHB1YmxpYyBtb2JpbGVRdWVyeUxpc3RlbmVyOiAoKSA9PiB2b2lkO1xyXG5cclxuICAgIHN3aW1MaXN0SWRzID0gW107XHJcbiAgICBzd2ltTGlzdCA9IFtdO1xyXG4gICAgZGVsaXZlcmFibGVTd2ltTGFuZU1pbkhlaWdodDtcclxuICAgIGRlbGl2ZXJhYmxlSWQgPSBbXTtcclxuICAgIC8vc3RhdHVzID0gWydDT01QTEVURScsICdBJywgJ0FyY2hpdmVkJ107XHJcblxyXG4gICAgdW5zZWxlY3RlZERlbGl2ZXJhYmxlO1xyXG4gICAgbWFzdGVyU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBtZWRpYTogTWVkaWFNYXRjaGVyLFxyXG4gICAgICAgIHB1YmxpYyBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGRlbGl2ZXJhYmxlQnVsa0NvdW50U2VydmljZTogRGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlXHJcbiAgICApIHtcclxuICAgICAgICB0aGlzLm1vYmlsZVF1ZXJ5ID0gdGhpcy5tZWRpYS5tYXRjaE1lZGlhKCcobWF4LXdpZHRoOiA2MDBweCknKTtcclxuICAgICAgICB0aGlzLm1vYmlsZVF1ZXJ5TGlzdGVuZXIgPSAoKSA9PiB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICBpZiAodGhpcy5tb2JpbGVRdWVyeS5tYXRjaGVzKSB7XHJcbiAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogZGVwcmVjYXRpb25cclxuICAgICAgICAgICAgdGhpcy5tb2JpbGVRdWVyeS5hZGRMaXN0ZW5lcih0aGlzLm1vYmlsZVF1ZXJ5TGlzdGVuZXIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlZGl0RGVsaXZlcmFibGUoZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0RGVsaXZlcmFibGVIYW5kbGVyLmVtaXQoZXZlbnREYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBtYXN0ZXJUb29nbGUoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLm1hc3RlclRvb2dsZURlbGl2ZXJhYmxlLmVtaXQoZXZlbnQpXHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkNSKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5jckhhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVsaXZlcmFibGVEZXRhaWxzKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZURldGFpbHNIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGl2ZXJhYmxlQnVsa0NoZWNrQ291bnQoZGVsaXZlcmFibGVEYXRhKSB7XHJcbiAgICAgICAgbGV0IGlkO1xyXG4gICAgICAgIGxldCBkZWxpdmVyYWJsZWlkO1xyXG4gICAgICAgIGlmICh0aGlzLm1hc3RlclNlbGVjdGVkRGVsaXZlcmFibGUgPT09IHRydWUpIHtcclxuICAgICAgICAgICAgdGhpcy5tYXN0ZXJTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5tYXN0ZXJTZWxlY3RlZERlbGl2ZXJhYmxlID09PSBmYWxzZSAmJiAhdGhpcy5tYXN0ZXJTZWxlY3RlZCkge1xyXG4gICAgICAgICAgICBpZCA9IFtdO1xyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZWlkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5tYXN0ZXJTZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZCA9IHRoaXMuZGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlLmdldEJ1bGtEZWxpdmVyYWJsZUlkKGRlbGl2ZXJhYmxlRGF0YSwgZGVsaXZlcmFibGVpZCk7XHJcbiAgICAgICAgLyogIGlmICh0aGlzLmRlbGl2ZXJhYmxlSWQgJiYgIXRoaXMuZGVsaXZlcmFibGVJZC5pbmNsdWRlcyhkZWxpdmVyYWJsZURhdGEpKSB7XHJcbiAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlSWQucHVzaChkZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlSWQuaW5kZXhPZihkZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgICAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlSWQuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgfVxyXG4gICovXHJcbiAgICAgICAgaWYgKGlkLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUJ1bGtFZGl0Q291bnQuZW1pdCh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQnVsa0VkaXRDb3VudC5lbWl0KGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlQ291bnQuZW1pdChpZC5sZW5ndGgpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZURhdGFzLm5leHQoaWQpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RlZEJ1bGtEZWxpdmVyYWJsZURhdGEoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREZWxpdmVyYWJsZURhdGEubmV4dChkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICB2aWV3VmVyc2lvbnMoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnZlcnNpb25zSGFuZGxlci5lbWl0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBzaGFyZUFzc2V0KGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5zaGFyZUFzc2V0SGFuZGxlci5lbWl0KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb25uZWN0ZWRJZChpZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnN3aW1MaXN0SWRzLmZpbHRlcihpZHMgPT4gaWRzICE9PSBpZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaCgpIHtcclxuICAgICAgICB0aGlzLnJlZnJlc2hTd2ltTGluZS5uZXh0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hEZWxpdmVyYWJsZShldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2goKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGlzZVN3aW1MaXN0KCkge1xyXG4gICAgICAgIHRoaXMuc3dpbUxpc3QgPSBbXTtcclxuICAgICAgICB0aGlzLnN3aW1MaXN0SWRzID0gW107XHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVDb25maWcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVDb25maWcuZGVsaXZlcmFibGVTdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1wbVN0YXR1c0ZpZWxkID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRCeUtleVZhbHVlKE1QTUZpZWxkS2V5cy5NQVBQRVJfTkFNRSwgTVBNRmllbGRDb25zdGFudHMuREVMSVZFUkFCTEVfTVBNX0ZJRUxEUy5ERUxJVkVSQUJMRV9TVEFUVVNfSUQsIE1QTV9MRVZFTFMuREVMSVZFUkFCTEUpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZVN0YXR1cy5tYXAoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBsaXN0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlVG90YWxDb3VudCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5kZWxpdmVyYWJsZUxpc3QubWFwKGRlbGl2ZXJhYmxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZFZhbHVlQnlEaXNwbGF5Q29sdW1uKG1wbVN0YXR1c0ZpZWxkLCBkZWxpdmVyYWJsZSkgPT09IHN0YXR1c1snTVBNX1N0YXR1cy1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1RZUEUgIT09ICdDT01QTEVURScgJiYgZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfU1RBVFVTX1ZBTFVFICE9PSAnQXBwcm92ZWQnICYmIGRlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1NUQVRVU19WQUxVRSAhPT0gJ0NhbmNlbGxlZCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJiYgZGVsaXZlcmFibGUuREVMSVZFUkFCTEVfU1RBVFVTX1ZBTFVFICE9PSAnUmVqZWN0ZWQnICYmIGRlbGl2ZXJhYmxlLkRFTElWRVJBQkxFX1RZUEUgPT09ICdSRVZJRVcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlLmJ1bGtFZGl0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZS5idWxrRWRpdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGVsaXZlcmFibGUuSVNfQlVMS19VUERBVEVfREVMSVZFUkFCTEUgIT09ICd0cnVlJyAmJiBkZWxpdmVyYWJsZS5CVUxLX0FDVElPTiAhPT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpc3REYXRhLnB1c2goZGVsaXZlcmFibGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHN3aW1MaXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxpc3REZXRhaWw6IHN0YXR1cyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogbGlzdERhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFDb3VudDogbGlzdERhdGEubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN3aW1MaXN0SWRzLnB1c2goc3RhdHVzWydNUE1fU3RhdHVzLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3dpbUxpc3QucHVzaChzd2ltTGlzdCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyAmJiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlVG90YWxDb3VudCAhPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlU3dpbUxhbmVNaW5IZWlnaHQgPSAnMzFyZW0nO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmluaXRpYWxpc2VTd2ltTGlzdCgpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=