import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BulkCommentsComponent } from '../../../../../lib/comments/bulk-comments/bulk-comments.component';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../../../../notification/notification.service';
import { LoaderService } from '../../../../loader/loader.service';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { EntityAppDefService } from '../../../../mpm-utils/services/entity.appdef.service';
import { Observable } from 'rxjs';
import { AppService } from '../../../../mpm-utils/services/app.service';
import { StatusTypes } from '../../../../mpm-utils/objects/StatusType';
import { MPM_LEVELS } from '../../../../mpm-utils/objects/Level';
import { StatusService } from '../../../../shared/services/status.service';
import { ProjectConstant } from '../../../../project/project-overview/project.constants';
import { TaskConstants } from '../../task.constants';
import * as acronui from '../../../../mpm-utils/auth/utility';
import { MPM_ROLES } from '../../../../mpm-utils/objects/Role';
var BulkActionsComponent = /** @class */ (function () {
    function BulkActionsComponent(dialog, notificationService, loaderService, sharingService, entityAppdefService, appService, statusService) {
        this.dialog = dialog;
        this.notificationService = notificationService;
        this.loaderService = loaderService;
        this.sharingService = sharingService;
        this.entityAppdefService = entityAppdefService;
        this.appService = appService;
        this.statusService = statusService;
        this.unselectDeliverable = new EventEmitter();
        this.bulkResponse = new EventEmitter();
        this.bulkResponseData = new EventEmitter();
        this.bulkActions = true;
        this.projectStatus = {
            REQUIRE_REASON: 'true',
            REQUIRE_COMMENTS: 'true',
            MPM_Status_Reason: ''
        };
    }
    BulkActionsComponent.prototype.bulkAction = function (data, bulkAction) {
        var _this = this;
        this.bulkStatus = data;
        this.bulkStatusAction = bulkAction;
        var dialogRef = this.dialog.open(BulkCommentsComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: 'Bulk Approval',
                hasConfirmationComment: true,
                status: this.bulkStatus,
                deliverableData: this.deliverableData,
                projectData: this.projectData,
                statusMessage: 'Approval State',
                commentText: 'Comments',
                isCommentRequired: true,
                errorMessage: 'Kindly fill all the required fields',
                submitButton: 'Yes',
                cancelButton: 'No',
                statusData: this.projectStatus,
            }
        });
        dialogRef.afterClosed().subscribe(function (bulkComment) {
            if (bulkComment) {
                var appId = _this.entityAppdefService.getApplicationID();
                var crActions = _this.sharingService.getCRActions();
                var teamItemId_1 = _this.projectData.R_PO_TEAM['MPM_Teams-id'].Id;
                var crAction = crActions.find(function (el) { return el['MPM_Teams-id'].Id === teamItemId_1; });
                var selectedReasonsData = bulkComment.SelectedReasons;
                var assetId = '';
                var versioning = true;
                var isBulkApprove = true;
                var taskName = '';
                var teamRole_1;
                var userRoles = acronui.findObjectsByProp(crAction, 'MPM_Team_Role_Mapping');
                userRoles.forEach(function (role) {
                    var appRoles = acronui.findObjectsByProp(role, 'MPM_APP_Roles');
                    var roleObj = appRoles.find(function (appRole) {
                        return appRole.ROLE_NAME === MPM_ROLES.MANAGER;
                    });
                    if (roleObj) {
                        teamRole_1 = role;
                    }
                });
                if (teamRole_1 && teamRole_1.Approver_CR_Role && teamRole_1.Approver_CR_Role.ROLE_ID) {
                    _this.crRole = teamRole_1.Approver_CR_Role;
                }
                _this.performBulkAction(_this.bulkStatusAction, appId, _this.deliverableData, assetId, _this.crRole.ROLE_ID, versioning, taskName, bulkComment.comment, isBulkApprove, selectedReasonsData);
            }
        });
    };
    BulkActionsComponent.prototype.getBulkActionData = function (appId, deliverableId, assetId, crRoleId, versioning, taskName, bulkComment, isBulkApprove, statusId, selectedReasonsData) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.handleCRApproval(appId, deliverableId, assetId, taskName, crRoleId, versioning, bulkComment, isBulkApprove, statusId, selectedReasonsData, '')
                .subscribe(function (response) {
                if (response) {
                    _this.notificationService.info('Bulk Approval process initiated');
                    // this.reload(true)
                    _this.bulkResponse.next(true);
                    _this.bulkResponseData.next(deliverableId);
                    _this.loaderService.hide();
                }
                else {
                    observer.error('Something went wrong in Bulk Approval process');
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    BulkActionsComponent.prototype.performBulkAction = function (action, appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, selectedReasonsData) {
        var _this = this;
        var statusType;
        this.loaderService.show();
        if (action === 'Approve') {
            statusType = StatusTypes.FINAL_APPROVED;
        }
        else if (action === 'Reject') {
            statusType = StatusTypes.FINAL_REJECTED;
        }
        else if (action === 'Complete') {
            statusType = StatusTypes.FINAL_COMPLETED;
        }
        this.statusService.getStatusByCategoryLevelAndStatusType(MPM_LEVELS.DELIVERABLE, statusType)
            .subscribe(function (response) {
            var status = response.find(function (el) { return el.STATUS_TYPE === StatusTypes.FINAL_COMPLETED || el.STATUS_TYPE === StatusTypes.FINAL_APPROVED || el.STATUS_TYPE === StatusTypes.FINAL_REJECTED; });
            if (status) {
                _this.statusId = status['MPM_Status-id'].Id;
                _this.getBulkActionData(appId, deliverableData, assetId, crRole, versioning, taskName, bulkComment, isBulkApprove, _this.statusId, selectedReasonsData)
                    .subscribe(function (crData) {
                    _this.loaderService.hide();
                    //this.crHandler.next(crData);
                    console.log(crData);
                }, function (error) {
                    _this.loaderService.hide();
                    _this.notificationService.error('Something went wrong in Bulk Approval process');
                });
            }
            else {
                _this.loaderService.hide();
                _this.notificationService.error('Something went wrong while performing the deliverable action');
            }
        }, function (error) {
            _this.loaderService.hide();
            _this.notificationService.error('Something went wrong while performing the deliverable action');
        });
    };
    BulkActionsComponent.prototype.masterToogle = function (event) {
        this.bulkActions = event.checked;
        this.unselectDeliverable.next(event.checked);
    };
    BulkActionsComponent.prototype.getPriorities = function () {
        var _this = this;
        return new Observable(function (observer) {
            var priority = _this.entityAppdefService.getPriorities(_this.categoryLevel);
            if (priority) {
                if (!Array.isArray(priority)) {
                    priority = [priority];
                }
                _this.priorityList = priority;
            }
            else {
                _this.priorityList = [];
            }
            /*  this.entityAppdefService.getPriorities(this.categoryLevel)
               .subscribe(priorityResponse => {
                 if (priorityResponse) {
                   if (!Array.isArray(priorityResponse)) {
                     priorityResponse = [priorityResponse];
                   }
                   this.priorityList = priorityResponse;
                 } else {
                   this.priorityList = [];
                 }
                 observer.next(true);
                 observer.complete();
               }, () => {
                 observer.next(false);
                 observer.complete();
               }); */
        });
    };
    BulkActionsComponent.prototype.getAllRoles = function () {
        var _this = this;
        var getRequestObject = {
            teamID: this.teamId,
            isApproverTask: this.isApprovalTask,
            isMemberTask: !this.isApprovalTask
        };
        return new Observable(function (observer) {
            _this.loaderService.show();
            _this.appService.getTeamRoles(getRequestObject)
                .subscribe(function (response) {
                if (response && response.Teams && response.Teams.MPM_Teams && response.Teams.MPM_Teams.MPM_Team_Role_Mapping) {
                    response = response.Teams.MPM_Teams;
                    if (!Array.isArray(response.MPM_Team_Role_Mapping)) {
                        response.MPM_Team_Role_Mapping = [response.MPM_Team_Role_Mapping];
                    }
                    var roleList = void 0;
                    roleList = response.MPM_Team_Role_Mapping;
                    var roles_1 = [];
                    if (roleList && roleList.length && roleList.length > 0) {
                        roleList.map(function (role) {
                            roles_1.push({
                                name: role['MPM_Team_Role_Mapping-id'].Id,
                                value: role.ROLE_DN,
                                displayName: role.NAME,
                                appRoles: role.MPM_APP_Roles
                            });
                        });
                    }
                    _this.teamRolesOptions = roles_1;
                }
                else {
                    _this.teamRolesOptions = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                observer.next(false);
                observer.complete();
            });
        });
    };
    BulkActionsComponent.prototype.getUsersByRole = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.loaderService.show();
            var getUserByRoleRequest = {};
            if (_this.selectedRole) {
                getUserByRoleRequest = {
                    allocationType: _this.selectedAllocation,
                    roleDN: _this.selectedAllocation === TaskConstants.ALLOCATION_TYPE_ROLE ? _this.selectedRole.value : '',
                    teamID: _this.selectedAllocation === TaskConstants.ALLOCATION_TYPE_USER ? _this.teamId : '',
                    isApproveTask: _this.isApprovalTask,
                    isUploadTask: !_this.isApprovalTask
                };
            }
            else {
                getUserByRoleRequest = {
                    allocationType: _this.selectedAllocation,
                    roleDN: '',
                    teamID: _this.teamId,
                    isApproveTask: false,
                    isUploadTask: true
                };
            }
            _this.appService.getUsersForTeam(getUserByRoleRequest)
                .subscribe(function (response) {
                if (response && response.users && response.users.user) {
                    var userList = acronui.findObjectsByProp(response, 'user');
                    var users_1 = [];
                    if (userList && userList.length && userList.length > 0) {
                        userList.forEach(function (user) {
                            var fieldObj = users_1.find(function (e) { return e.value === user.dn; });
                            if (!fieldObj) {
                                users_1.push({
                                    name: user.dn,
                                    value: user.cn,
                                    displayName: user.name
                                });
                            }
                        });
                    }
                    _this.teamRoleUserOptions = users_1;
                    // TODO: getuser entities by userId
                }
                else {
                    _this.teamRoleUserOptions = [];
                }
                _this.loaderService.hide();
                observer.next(true);
                observer.complete();
            }, function () {
                observer.next(false);
                observer.complete();
            });
        });
    };
    BulkActionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.statusService.getStatusByCategoryLevelAndStatusType(ProjectConstant.CATEGORY_LEVEL_PROJECT, StatusTypes.INITIAL).subscribe(function (response) {
            console.log(response);
        });
        this.statusService.getAllCRStatusReasons().subscribe(function (response) {
            _this.projectStatus.MPM_Status_Reason = response;
        });
    };
    BulkActionsComponent.ctorParameters = function () { return [
        { type: MatDialog },
        { type: NotificationService },
        { type: LoaderService },
        { type: SharingService },
        { type: EntityAppDefService },
        { type: AppService },
        { type: StatusService }
    ]; };
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "enableAction", void 0);
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "delCount", void 0);
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "deliverableData", void 0);
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "projectData", void 0);
    __decorate([
        Output()
    ], BulkActionsComponent.prototype, "unselectDeliverable", void 0);
    __decorate([
        Output()
    ], BulkActionsComponent.prototype, "bulkResponse", void 0);
    __decorate([
        Output()
    ], BulkActionsComponent.prototype, "bulkResponseData", void 0);
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "selectedProjectData", void 0);
    __decorate([
        Input()
    ], BulkActionsComponent.prototype, "categoryLevel", void 0);
    BulkActionsComponent = __decorate([
        Component({
            selector: 'mpm-bulk-actions',
            template: "<div class=\"flex-container\" *ngIf=\"bulkActions && delCount > 0\">\r\n    <div class=\"flex-container-item\">\r\n        <mat-checkbox class=\"mat-checkbox-color\" [checked]='true' (change)=\"$event ? masterToogle($event) : null\">\r\n        </mat-checkbox>\r\n        <span>{{delCount}} Items Selected </span>\r\n    </div>\r\n    <div>\r\n        <span>\r\n            <!-- *ngIf=\"!enableAction\" -->\r\n            <button class=\"new-project-btn\" [disabled]=\"!enableAction\" mat-raised-button matTooltip=\"Actions\"\r\n                [matMenuTriggerFor]=\"actionTypeMenu\">\r\n                <span> Actions</span>\r\n        <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n        </span>\r\n        <mat-menu #actionTypeMenu=\"matMenu\">\r\n            <button mat-menu-item matTooltip=\"Approved\" (click)=\"bulkAction('Approved','Approve')\">\r\n                <span>Approved</span>\r\n            </button>\r\n            <button mat-menu-item matTooltip=\"Requested Changes\" (click)=\"bulkAction('Requested Changes','Reject')\">\r\n                <span>Requested Changes</span>\r\n            </button>\r\n        </mat-menu>\r\n\r\n        <!--  <mat-form-field appearance=\"outline\">\r\n            <mat-label>Status</mat-label>\r\n            <mat-select formControlName=\"status\" placeholder=\"Status\" name=\"projectStatus\"\r\n                (selectionChange)=\"validateTaskStatusUpdate(selectedStatus)\" [required]=\"true\"\r\n                [disabled]=\"projectOverviewForm.get('status').status === 'DISABLED' ? true : false\">\r\n                <span *ngFor=\"let status of overViewConfig.statusOptions\">\r\n                    <mat-option value=\"{{status['MPM_Status-id'].Id}}\">\r\n                        {{status.NAME}}\r\n                    </mat-option>\r\n                </span>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Priority</mat-label>\r\n            <mat-select formControlName=\"priority\" placeholder=\"Priority\" name=\"priority\" [required]=\"true\"\r\n                [disabled]=\"projectOverviewForm.get('priority').status === 'DISABLED' ? true : false\">\r\n                <mat-option *ngFor=\"let priority of overViewConfig.priorityOptions\"\r\n                    value=\"{{priority['MPM_Priority-id'].Id}}\">\r\n                    {{priority.DESCRIPTION}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"overViewConfig.isProject\">\r\n            <mat-label>Start Date</mat-label>\r\n            <input formControlName=\"startDate\" matInput [matDatepicker]=\"startDate\"\r\n                placeholder=\"Start Date (MM/DD/YYYY)\" required\r\n                [min]=\"this.overViewConfig && this.overViewConfig.selectedProject && this.overViewConfig.selectedProject.START_DATE ? this.overViewConfig.selectedProject.START_DATE : minStartDate\"\r\n                [(value)]=\"selectedStartDate\" (dateChange)=\"validateProjectStartDate($event)\"\r\n                [max]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.startMaxDate ? overViewConfig.dateValidation.startMaxDate : null\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDate [disabled]=\"disableStartDate\"></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"overViewConfig.isProject\">\r\n            <mat-label>End Date</mat-label>\r\n            <input formControlName=\"endDate\"\r\n                [min]=\"overViewConfig.dateValidation && overViewConfig.dateValidation.endMinDate ? overViewConfig.dateValidation.endMinDate : (projectOverviewForm.value.startDate ? projectOverviewForm.value.startDate : selectedStartDate)\"\r\n                matInput [matDatepicker]=\"endDate\" placeholder=\"End Date (MM/DD/YYYY)\" required\r\n                [(value)]=\"selectedEndDate\" (dateChange)=\"validateProjectEndDate($event)\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"endDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #endDate [disabled]=\"disableEndDate\"></mat-datepicker>\r\n        </mat-form-field>-->\r\n    </div>\r\n\r\n</div>",
            styles: [".flex-container{display:flex}.flex-container>div{margin:10px;padding:10px}.flex-container-item{padding:18px!important}.mat-checkbox-color{background-color:#fff}"]
        })
    ], BulkActionsComponent);
    return BulkActionsComponent;
}());
export { BulkActionsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVsay1hY3Rpb25zLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvYnVsay1hY3Rpb25zL2J1bGstYWN0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sbUVBQW1FLENBQUM7QUFDMUcsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDaEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDM0YsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdEQUF3RCxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRCxPQUFPLEtBQUssT0FBTyxNQUFNLG9DQUFvQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQU0vRDtJQStCRSw4QkFDUyxNQUFpQixFQUNqQixtQkFBd0MsRUFDeEMsYUFBNEIsRUFDNUIsY0FBOEIsRUFDOUIsbUJBQXdDLEVBQ3hDLFVBQXNCLEVBQ3RCLGFBQTRCO1FBTjVCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBaEMzQix3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN2QyxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBV3JELGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBT25CLGtCQUFhLEdBQUc7WUFDZCxjQUFjLEVBQUUsTUFBTTtZQUN0QixnQkFBZ0IsRUFBRSxNQUFNO1lBQ3hCLGlCQUFpQixFQUFFLEVBQUU7U0FDdEIsQ0FBQztJQVNFLENBQUM7SUFFTCx5Q0FBVSxHQUFWLFVBQVcsSUFBSSxFQUFFLFVBQVU7UUFBM0IsaUJBc0RDO1FBcERDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLENBQUM7UUFDbkMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDeEQsS0FBSyxFQUFFLEtBQUs7WUFDWixZQUFZLEVBQUUsSUFBSTtZQUNsQixJQUFJLEVBQ0o7Z0JBQ0UsT0FBTyxFQUFFLGVBQWU7Z0JBQ3hCLHNCQUFzQixFQUFFLElBQUk7Z0JBQzVCLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDdkIsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO2dCQUNyQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7Z0JBQzdCLGFBQWEsRUFBRSxnQkFBZ0I7Z0JBQy9CLFdBQVcsRUFBRSxVQUFVO2dCQUN2QixpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixZQUFZLEVBQUUscUNBQXFDO2dCQUNuRCxZQUFZLEVBQUUsS0FBSztnQkFDbkIsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLFVBQVUsRUFBRSxJQUFJLENBQUMsYUFBYTthQUMvQjtTQUNGLENBQUMsQ0FBQztRQUVILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXO1lBQzNDLElBQUksV0FBVyxFQUFFO2dCQUNmLElBQU0sS0FBSyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUMxRCxJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNyRCxJQUFNLFlBQVUsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2pFLElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxLQUFLLFlBQVUsRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO2dCQUM1RSxJQUFNLG1CQUFtQixHQUFHLFdBQVcsQ0FBQyxlQUFlLENBQUM7Z0JBQ3hELElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFDbkIsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQzNCLElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztnQkFDcEIsSUFBSSxVQUFRLENBQUM7Z0JBQ2IsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUMvRSxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDcEIsSUFBTSxRQUFRLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztvQkFDbEUsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87d0JBQ25DLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDO29CQUNqRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLE9BQU8sRUFBRTt3QkFDWCxVQUFRLEdBQUcsSUFBSSxDQUFDO3FCQUNqQjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLFVBQVEsSUFBSSxVQUFRLENBQUMsZ0JBQWdCLElBQUksVUFBUSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtvQkFDOUUsS0FBSSxDQUFDLE1BQU0sR0FBRyxVQUFRLENBQUMsZ0JBQWdCLENBQUM7aUJBQ3pDO2dCQUVELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLEtBQUksQ0FBQyxlQUFlLEVBQUUsT0FBTyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsV0FBVyxDQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsbUJBQW1CLENBQUMsQ0FBQzthQUN6TDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdEQUFpQixHQUFqQixVQUFrQixLQUFLLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxtQkFBbUI7UUFBMUksaUJBa0JDO1FBakJDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLEtBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsbUJBQW1CLEVBQUUsRUFBRSxDQUFDO2lCQUMzSixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNqQixJQUFJLFFBQVEsRUFBRTtvQkFDWixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLENBQUM7b0JBQ2pFLG9CQUFvQjtvQkFDcEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBRTNCO3FCQUFNO29CQUNMLFFBQVEsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQztpQkFDakU7WUFDSCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnREFBaUIsR0FBakIsVUFBa0IsTUFBeUMsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLG1CQUFtQjtRQUEzSyxpQkFrQ0M7UUFqQ0MsSUFBSSxVQUFVLENBQUM7UUFDZixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRTFCLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUN4QixVQUFVLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQztTQUN6QzthQUFNLElBQUksTUFBTSxLQUFLLFFBQVEsRUFBRTtZQUM5QixVQUFVLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQztTQUN6QzthQUFNLElBQUksTUFBTSxLQUFLLFVBQVUsRUFBRTtZQUNoQyxVQUFVLEdBQUcsV0FBVyxDQUFDLGVBQWUsQ0FBQztTQUMxQztRQUVELElBQUksQ0FBQyxhQUFhLENBQUMscUNBQXFDLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUM7YUFDekYsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNqQixJQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsRUFBRSxJQUFJLE9BQUEsRUFBRSxDQUFDLFdBQVcsS0FBSyxXQUFXLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEtBQUssV0FBVyxDQUFDLGNBQWMsSUFBSSxFQUFFLENBQUMsV0FBVyxLQUFLLFdBQVcsQ0FBQyxjQUFjLEVBQWhKLENBQWdKLENBQUMsQ0FBQztZQUNyTCxJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQzNDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFFLEtBQUksQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUM7cUJBQ2xKLFNBQVMsQ0FBQyxVQUFBLE1BQU07b0JBQ2YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsOEJBQThCO29CQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQztnQkFDbEYsQ0FBQyxDQUFDLENBQUM7YUFDTjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDhEQUE4RCxDQUFDLENBQUM7YUFDaEc7UUFDSCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDhEQUE4RCxDQUFDLENBQUM7UUFDakcsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMkNBQVksR0FBWixVQUFhLEtBQUs7UUFDaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCw0Q0FBYSxHQUFiO1FBQUEsaUJBZ0NDO1FBL0JDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBRTVCLElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBRXpFLElBQUksUUFBUSxFQUFFO2dCQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM1QixRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDdkI7Z0JBQ0QsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7YUFDeEI7WUFHRDs7Ozs7Ozs7Ozs7Ozs7O3FCQWVTO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsMENBQVcsR0FBWDtRQUFBLGlCQXdDQztRQXZDQyxJQUFNLGdCQUFnQixHQUFHO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7WUFDbkMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWM7U0FDbkMsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMscUJBQXFCLEVBQUU7b0JBQzVHLFFBQVEsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ2xELFFBQVEsQ0FBQyxxQkFBcUIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3FCQUNuRTtvQkFDRCxJQUFJLFFBQVEsU0FBQSxDQUFDO29CQUNiLFFBQVEsR0FBRyxRQUFRLENBQUMscUJBQXFCLENBQUM7b0JBQzFDLElBQU0sT0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDdEQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUk7NEJBQ2YsT0FBSyxDQUFDLElBQUksQ0FBQztnQ0FDVCxJQUFJLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRTtnQ0FDekMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPO2dDQUNuQixXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0NBQ3RCLFFBQVEsRUFBRSxJQUFJLENBQUMsYUFBYTs2QkFDN0IsQ0FBQyxDQUFDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNKO29CQUNELEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxPQUFLLENBQUM7aUJBQy9CO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7aUJBQzVCO2dCQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNkNBQWMsR0FBZDtRQUFBLGlCQW9EQztRQW5EQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLElBQUksb0JBQW9CLEdBQUcsRUFBRSxDQUFDO1lBQzlCLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTtnQkFDckIsb0JBQW9CLEdBQUc7b0JBQ3JCLGNBQWMsRUFBRSxLQUFJLENBQUMsa0JBQWtCO29CQUN2QyxNQUFNLEVBQUUsS0FBSSxDQUFDLGtCQUFrQixLQUFLLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3JHLE1BQU0sRUFBRSxLQUFJLENBQUMsa0JBQWtCLEtBQUssYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN6RixhQUFhLEVBQUUsS0FBSSxDQUFDLGNBQWM7b0JBQ2xDLFlBQVksRUFBRSxDQUFDLEtBQUksQ0FBQyxjQUFjO2lCQUNuQyxDQUFDO2FBQ0g7aUJBQU07Z0JBQ0wsb0JBQW9CLEdBQUc7b0JBQ3JCLGNBQWMsRUFBRSxLQUFJLENBQUMsa0JBQWtCO29CQUN2QyxNQUFNLEVBQUUsRUFBRTtvQkFDVixNQUFNLEVBQUUsS0FBSSxDQUFDLE1BQU07b0JBQ25CLGFBQWEsRUFBRSxLQUFLO29CQUNwQixZQUFZLEVBQUUsSUFBSTtpQkFDbkIsQ0FBQzthQUNIO1lBRUQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7b0JBQ3JELElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQzdELElBQU0sT0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDdEQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7NEJBQ25CLElBQU0sUUFBUSxHQUFHLE9BQUssQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxFQUFFLEVBQW5CLENBQW1CLENBQUMsQ0FBQzs0QkFDdEQsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQ0FDYixPQUFLLENBQUMsSUFBSSxDQUFDO29DQUNULElBQUksRUFBRSxJQUFJLENBQUMsRUFBRTtvQ0FDYixLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQUU7b0NBQ2QsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJO2lDQUN2QixDQUFDLENBQUM7NkJBQ0o7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsS0FBSSxDQUFDLG1CQUFtQixHQUFHLE9BQUssQ0FBQztvQkFDakMsbUNBQW1DO2lCQUNwQztxQkFBTTtvQkFDTCxLQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO2lCQUMvQjtnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHVDQUFRLEdBQVI7UUFBQSxpQkFTQztRQVJDLElBQUksQ0FBQyxhQUFhLENBQUMscUNBQXFDLENBQUMsZUFBZSxDQUFDLHNCQUFzQixFQUFFLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ3RJLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUMzRCxLQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLFFBQVEsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7O2dCQXpRZ0IsU0FBUztnQkFDSSxtQkFBbUI7Z0JBQ3pCLGFBQWE7Z0JBQ1osY0FBYztnQkFDVCxtQkFBbUI7Z0JBQzVCLFVBQVU7Z0JBQ1AsYUFBYTs7SUFwQzVCO1FBQVIsS0FBSyxFQUFFOzhEQUFjO0lBQ2I7UUFBUixLQUFLLEVBQUU7MERBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTtpRUFBaUI7SUFDaEI7UUFBUixLQUFLLEVBQUU7NkRBQWE7SUFDWDtRQUFULE1BQU0sRUFBRTtxRUFBK0M7SUFDOUM7UUFBVCxNQUFNLEVBQUU7OERBQXdDO0lBQ3ZDO1FBQVQsTUFBTSxFQUFFO2tFQUE0QztJQUM1QztRQUFSLEtBQUssRUFBRTtxRUFBcUI7SUFDcEI7UUFBUixLQUFLLEVBQUU7K0RBQWU7SUFWWixvQkFBb0I7UUFMaEMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGtCQUFrQjtZQUM1Qix3eElBQTRDOztTQUU3QyxDQUFDO09BQ1csb0JBQW9CLENBMFNoQztJQUFELDJCQUFDO0NBQUEsQUExU0QsSUEwU0M7U0ExU1ksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBCdWxrQ29tbWVudHNDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvY29tbWVudHMvYnVsay1jb21tZW50cy9idWxrLWNvbW1lbnRzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlcyB9IGZyb20gJy4uLy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL1N0YXR1c1R5cGUnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBTdGF0dXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3N0YXR1cy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vLi4vLi4vcHJvamVjdC9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVGFza0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3Rhc2suY29uc3RhbnRzJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgTVBNX1JPTEVTIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvUm9sZSc7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWJ1bGstYWN0aW9ucycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2J1bGstYWN0aW9ucy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vYnVsay1hY3Rpb25zLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnVsa0FjdGlvbnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBlbmFibGVBY3Rpb247XHJcbiAgQElucHV0KCkgZGVsQ291bnQ7XHJcbiAgQElucHV0KCkgZGVsaXZlcmFibGVEYXRhO1xyXG4gIEBJbnB1dCgpIHByb2plY3REYXRhO1xyXG4gIEBPdXRwdXQoKSB1bnNlbGVjdERlbGl2ZXJhYmxlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGJ1bGtSZXNwb25zZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSBidWxrUmVzcG9uc2VEYXRhID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQElucHV0KCkgc2VsZWN0ZWRQcm9qZWN0RGF0YTtcclxuICBASW5wdXQoKSBjYXRlZ29yeUxldmVsO1xyXG5cclxuICBwcmlvcml0eUxpc3Q7XHJcbiAgaXNBcHByb3ZhbFRhc2s7XHJcbiAgdGVhbUlkO1xyXG4gIHRlYW1Sb2xlc09wdGlvbnM7XHJcbiAgdGVhbVJvbGVVc2VyT3B0aW9ucztcclxuICBzZWxlY3RlZFJvbGU7XHJcbiAgc2VsZWN0ZWRBbGxvY2F0aW9uO1xyXG4gIGJ1bGtBY3Rpb25zID0gdHJ1ZTtcclxuICBjclJvbGU7XHJcbiAgYnVsa1N0YXR1cztcclxuICBzdGF0dXNJZDtcclxuICBzdGF0dXNPcHRpb25zO1xyXG4gIGJ1bGtTdGF0dXNBY3Rpb247XHJcblxyXG4gIHByb2plY3RTdGF0dXMgPSB7XHJcbiAgICBSRVFVSVJFX1JFQVNPTjogJ3RydWUnLFxyXG4gICAgUkVRVUlSRV9DT01NRU5UUzogJ3RydWUnLFxyXG4gICAgTVBNX1N0YXR1c19SZWFzb246ICcnXHJcbiAgfTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGVudGl0eUFwcGRlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHB1YmxpYyBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICkgeyB9XHJcblxyXG4gIGJ1bGtBY3Rpb24oZGF0YSwgYnVsa0FjdGlvbikge1xyXG5cclxuICAgIHRoaXMuYnVsa1N0YXR1cyA9IGRhdGE7XHJcbiAgICB0aGlzLmJ1bGtTdGF0dXNBY3Rpb24gPSBidWxrQWN0aW9uO1xyXG4gICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihCdWxrQ29tbWVudHNDb21wb25lbnQsIHtcclxuICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgIGRhdGE6XHJcbiAgICAgIHtcclxuICAgICAgICBtZXNzYWdlOiAnQnVsayBBcHByb3ZhbCcsXHJcbiAgICAgICAgaGFzQ29uZmlybWF0aW9uQ29tbWVudDogdHJ1ZSxcclxuICAgICAgICBzdGF0dXM6IHRoaXMuYnVsa1N0YXR1cyxcclxuICAgICAgICBkZWxpdmVyYWJsZURhdGE6IHRoaXMuZGVsaXZlcmFibGVEYXRhLFxyXG4gICAgICAgIHByb2plY3REYXRhOiB0aGlzLnByb2plY3REYXRhLFxyXG4gICAgICAgIHN0YXR1c01lc3NhZ2U6ICdBcHByb3ZhbCBTdGF0ZScsXHJcbiAgICAgICAgY29tbWVudFRleHQ6ICdDb21tZW50cycsXHJcbiAgICAgICAgaXNDb21tZW50UmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgZXJyb3JNZXNzYWdlOiAnS2luZGx5IGZpbGwgYWxsIHRoZSByZXF1aXJlZCBmaWVsZHMnLFxyXG4gICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgY2FuY2VsQnV0dG9uOiAnTm8nLFxyXG4gICAgICAgIHN0YXR1c0RhdGE6IHRoaXMucHJvamVjdFN0YXR1cyxcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKGJ1bGtDb21tZW50ID0+IHtcclxuICAgICAgaWYgKGJ1bGtDb21tZW50KSB7XHJcbiAgICAgICAgY29uc3QgYXBwSWQgPSB0aGlzLmVudGl0eUFwcGRlZlNlcnZpY2UuZ2V0QXBwbGljYXRpb25JRCgpO1xyXG4gICAgICAgIGNvbnN0IGNyQWN0aW9ucyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q1JBY3Rpb25zKCk7XHJcbiAgICAgICAgY29uc3QgdGVhbUl0ZW1JZCA9IHRoaXMucHJvamVjdERhdGEuUl9QT19URUFNWydNUE1fVGVhbXMtaWQnXS5JZDtcclxuICAgICAgICBjb25zdCBjckFjdGlvbiA9IGNyQWN0aW9ucy5maW5kKGVsID0+IGVsWydNUE1fVGVhbXMtaWQnXS5JZCA9PT0gdGVhbUl0ZW1JZCk7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRSZWFzb25zRGF0YSA9IGJ1bGtDb21tZW50LlNlbGVjdGVkUmVhc29ucztcclxuICAgICAgICBjb25zdCBhc3NldElkID0gJyc7XHJcbiAgICAgICAgY29uc3QgdmVyc2lvbmluZyA9IHRydWU7XHJcbiAgICAgICAgY29uc3QgaXNCdWxrQXBwcm92ZSA9IHRydWU7XHJcbiAgICAgICAgY29uc3QgdGFza05hbWUgPSAnJztcclxuICAgICAgICBsZXQgdGVhbVJvbGU7XHJcbiAgICAgICAgY29uc3QgdXNlclJvbGVzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChjckFjdGlvbiwgJ01QTV9UZWFtX1JvbGVfTWFwcGluZycpO1xyXG4gICAgICAgIHVzZXJSb2xlcy5mb3JFYWNoKHJvbGUgPT4ge1xyXG4gICAgICAgICAgY29uc3QgYXBwUm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJvbGUsICdNUE1fQVBQX1JvbGVzJyk7XHJcbiAgICAgICAgICBjb25zdCByb2xlT2JqID0gYXBwUm9sZXMuZmluZChhcHBSb2xlID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGFwcFJvbGUuUk9MRV9OQU1FID09PSBNUE1fUk9MRVMuTUFOQUdFUjtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaWYgKHJvbGVPYmopIHtcclxuICAgICAgICAgICAgdGVhbVJvbGUgPSByb2xlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGVhbVJvbGUgJiYgdGVhbVJvbGUuQXBwcm92ZXJfQ1JfUm9sZSAmJiB0ZWFtUm9sZS5BcHByb3Zlcl9DUl9Sb2xlLlJPTEVfSUQpIHtcclxuICAgICAgICAgIHRoaXMuY3JSb2xlID0gdGVhbVJvbGUuQXBwcm92ZXJfQ1JfUm9sZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMucGVyZm9ybUJ1bGtBY3Rpb24odGhpcy5idWxrU3RhdHVzQWN0aW9uLCBhcHBJZCwgdGhpcy5kZWxpdmVyYWJsZURhdGEsIGFzc2V0SWQsIHRoaXMuY3JSb2xlLlJPTEVfSUQsIHZlcnNpb25pbmcsIHRhc2tOYW1lLCBidWxrQ29tbWVudC5jb21tZW50LCBpc0J1bGtBcHByb3ZlLCBzZWxlY3RlZFJlYXNvbnNEYXRhKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRCdWxrQWN0aW9uRGF0YShhcHBJZCwgZGVsaXZlcmFibGVJZCwgYXNzZXRJZCwgY3JSb2xlSWQsIHZlcnNpb25pbmcsIHRhc2tOYW1lLCBidWxrQ29tbWVudCwgaXNCdWxrQXBwcm92ZSwgc3RhdHVzSWQsIHNlbGVjdGVkUmVhc29uc0RhdGEpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmhhbmRsZUNSQXBwcm92YWwoYXBwSWQsIGRlbGl2ZXJhYmxlSWQsIGFzc2V0SWQsIHRhc2tOYW1lLCBjclJvbGVJZCwgdmVyc2lvbmluZywgYnVsa0NvbW1lbnQsIGlzQnVsa0FwcHJvdmUsIHN0YXR1c0lkLCBzZWxlY3RlZFJlYXNvbnNEYXRhLCAnJylcclxuICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnQnVsayBBcHByb3ZhbCBwcm9jZXNzIGluaXRpYXRlZCcpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnJlbG9hZCh0cnVlKVxyXG4gICAgICAgICAgICB0aGlzLmJ1bGtSZXNwb25zZS5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICB0aGlzLmJ1bGtSZXNwb25zZURhdGEubmV4dChkZWxpdmVyYWJsZUlkKTtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBvYnNlcnZlci5lcnJvcignU29tZXRoaW5nIHdlbnQgd3JvbmcgaW4gQnVsayBBcHByb3ZhbCBwcm9jZXNzJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwZXJmb3JtQnVsa0FjdGlvbihhY3Rpb246ICdBcHByb3ZlJyB8ICdSZWplY3QnIHwgJ0NvbXBsZXRlJywgYXBwSWQsIGRlbGl2ZXJhYmxlRGF0YSwgYXNzZXRJZCwgY3JSb2xlLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSwgYnVsa0NvbW1lbnQsIGlzQnVsa0FwcHJvdmUsIHNlbGVjdGVkUmVhc29uc0RhdGEpIHtcclxuICAgIGxldCBzdGF0dXNUeXBlO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuXHJcbiAgICBpZiAoYWN0aW9uID09PSAnQXBwcm92ZScpIHtcclxuICAgICAgc3RhdHVzVHlwZSA9IFN0YXR1c1R5cGVzLkZJTkFMX0FQUFJPVkVEO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT09ICdSZWplY3QnKSB7XHJcbiAgICAgIHN0YXR1c1R5cGUgPSBTdGF0dXNUeXBlcy5GSU5BTF9SRUpFQ1RFRDtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSAnQ29tcGxldGUnKSB7XHJcbiAgICAgIHN0YXR1c1R5cGUgPSBTdGF0dXNUeXBlcy5GSU5BTF9DT01QTEVURUQ7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zdGF0dXNTZXJ2aWNlLmdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbEFuZFN0YXR1c1R5cGUoTVBNX0xFVkVMUy5ERUxJVkVSQUJMRSwgc3RhdHVzVHlwZSlcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgY29uc3Qgc3RhdHVzID0gcmVzcG9uc2UuZmluZChlbCA9PiBlbC5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfQ09NUExFVEVEIHx8IGVsLlNUQVRVU19UWVBFID09PSBTdGF0dXNUeXBlcy5GSU5BTF9BUFBST1ZFRCB8fCBlbC5TVEFUVVNfVFlQRSA9PT0gU3RhdHVzVHlwZXMuRklOQUxfUkVKRUNURUQpO1xyXG4gICAgICAgIGlmIChzdGF0dXMpIHtcclxuICAgICAgICAgIHRoaXMuc3RhdHVzSWQgPSBzdGF0dXNbJ01QTV9TdGF0dXMtaWQnXS5JZDtcclxuICAgICAgICAgIHRoaXMuZ2V0QnVsa0FjdGlvbkRhdGEoYXBwSWQsIGRlbGl2ZXJhYmxlRGF0YSwgYXNzZXRJZCwgY3JSb2xlLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSwgYnVsa0NvbW1lbnQsIGlzQnVsa0FwcHJvdmUsIHRoaXMuc3RhdHVzSWQsIHNlbGVjdGVkUmVhc29uc0RhdGEpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoY3JEYXRhID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgIC8vdGhpcy5jckhhbmRsZXIubmV4dChjckRhdGEpO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNyRGF0YSk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3JvbmcgaW4gQnVsayBBcHByb3ZhbCBwcm9jZXNzJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBwZXJmb3JtaW5nIHRoZSBkZWxpdmVyYWJsZSBhY3Rpb24nKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgcGVyZm9ybWluZyB0aGUgZGVsaXZlcmFibGUgYWN0aW9uJyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbWFzdGVyVG9vZ2xlKGV2ZW50KSB7XHJcbiAgICB0aGlzLmJ1bGtBY3Rpb25zID0gZXZlbnQuY2hlY2tlZDtcclxuICAgIHRoaXMudW5zZWxlY3REZWxpdmVyYWJsZS5uZXh0KGV2ZW50LmNoZWNrZWQpO1xyXG4gIH1cclxuXHJcbiAgZ2V0UHJpb3JpdGllcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuXHJcbiAgICAgIGxldCBwcmlvcml0eSA9IHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKHRoaXMuY2F0ZWdvcnlMZXZlbClcclxuXHJcbiAgICAgIGlmIChwcmlvcml0eSkge1xyXG4gICAgICAgIGlmICghQXJyYXkuaXNBcnJheShwcmlvcml0eSkpIHtcclxuICAgICAgICAgIHByaW9yaXR5ID0gW3ByaW9yaXR5XTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcmlvcml0eUxpc3QgPSBwcmlvcml0eTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnByaW9yaXR5TGlzdCA9IFtdO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgICAgLyogIHRoaXMuZW50aXR5QXBwZGVmU2VydmljZS5nZXRQcmlvcml0aWVzKHRoaXMuY2F0ZWdvcnlMZXZlbClcclxuICAgICAgICAgLnN1YnNjcmliZShwcmlvcml0eVJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICBpZiAocHJpb3JpdHlSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHByaW9yaXR5UmVzcG9uc2UpKSB7XHJcbiAgICAgICAgICAgICAgIHByaW9yaXR5UmVzcG9uc2UgPSBbcHJpb3JpdHlSZXNwb25zZV07XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICB0aGlzLnByaW9yaXR5TGlzdCA9IHByaW9yaXR5UmVzcG9uc2U7XHJcbiAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgIHRoaXMucHJpb3JpdHlMaXN0ID0gW107XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgIH0pOyAqL1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRBbGxSb2xlcygpIHtcclxuICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgIHRlYW1JRDogdGhpcy50ZWFtSWQsXHJcbiAgICAgIGlzQXBwcm92ZXJUYXNrOiB0aGlzLmlzQXBwcm92YWxUYXNrLFxyXG4gICAgICBpc01lbWJlclRhc2s6ICF0aGlzLmlzQXBwcm92YWxUYXNrXHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFRlYW1Sb2xlcyhnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLlRlYW1zICYmIHJlc3BvbnNlLlRlYW1zLk1QTV9UZWFtcyAmJiByZXNwb25zZS5UZWFtcy5NUE1fVGVhbXMuTVBNX1RlYW1fUm9sZV9NYXBwaW5nKSB7XHJcbiAgICAgICAgICAgIHJlc3BvbnNlID0gcmVzcG9uc2UuVGVhbXMuTVBNX1RlYW1zO1xyXG4gICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuTVBNX1RlYW1fUm9sZV9NYXBwaW5nKSkge1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlLk1QTV9UZWFtX1JvbGVfTWFwcGluZyA9IFtyZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmddO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCByb2xlTGlzdDtcclxuICAgICAgICAgICAgcm9sZUxpc3QgPSByZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmc7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvbGVzID0gW107XHJcbiAgICAgICAgICAgIGlmIChyb2xlTGlzdCAmJiByb2xlTGlzdC5sZW5ndGggJiYgcm9sZUxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHJvbGVMaXN0Lm1hcChyb2xlID0+IHtcclxuICAgICAgICAgICAgICAgIHJvbGVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICBuYW1lOiByb2xlWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgdmFsdWU6IHJvbGUuUk9MRV9ETixcclxuICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6IHJvbGUuTkFNRSxcclxuICAgICAgICAgICAgICAgICAgYXBwUm9sZXM6IHJvbGUuTVBNX0FQUF9Sb2xlc1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy50ZWFtUm9sZXNPcHRpb25zID0gcm9sZXM7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRlYW1Sb2xlc09wdGlvbnMgPSBbXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldFVzZXJzQnlSb2xlKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICBsZXQgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7fTtcclxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRSb2xlKSB7XHJcbiAgICAgICAgZ2V0VXNlckJ5Um9sZVJlcXVlc3QgPSB7XHJcbiAgICAgICAgICBhbGxvY2F0aW9uVHlwZTogdGhpcy5zZWxlY3RlZEFsbG9jYXRpb24sXHJcbiAgICAgICAgICByb2xlRE46IHRoaXMuc2VsZWN0ZWRBbGxvY2F0aW9uID09PSBUYXNrQ29uc3RhbnRzLkFMTE9DQVRJT05fVFlQRV9ST0xFID8gdGhpcy5zZWxlY3RlZFJvbGUudmFsdWUgOiAnJyxcclxuICAgICAgICAgIHRlYW1JRDogdGhpcy5zZWxlY3RlZEFsbG9jYXRpb24gPT09IFRhc2tDb25zdGFudHMuQUxMT0NBVElPTl9UWVBFX1VTRVIgPyB0aGlzLnRlYW1JZCA6ICcnLFxyXG4gICAgICAgICAgaXNBcHByb3ZlVGFzazogdGhpcy5pc0FwcHJvdmFsVGFzayxcclxuICAgICAgICAgIGlzVXBsb2FkVGFzazogIXRoaXMuaXNBcHByb3ZhbFRhc2tcclxuICAgICAgICB9O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGdldFVzZXJCeVJvbGVSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgYWxsb2NhdGlvblR5cGU6IHRoaXMuc2VsZWN0ZWRBbGxvY2F0aW9uLFxyXG4gICAgICAgICAgcm9sZUROOiAnJyxcclxuICAgICAgICAgIHRlYW1JRDogdGhpcy50ZWFtSWQsXHJcbiAgICAgICAgICBpc0FwcHJvdmVUYXNrOiBmYWxzZSxcclxuICAgICAgICAgIGlzVXBsb2FkVGFzazogdHJ1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRVc2Vyc0ZvclRlYW0oZ2V0VXNlckJ5Um9sZVJlcXVlc3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcnMgJiYgcmVzcG9uc2UudXNlcnMudXNlcikge1xyXG4gICAgICAgICAgICBjb25zdCB1c2VyTGlzdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICd1c2VyJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gW107XHJcbiAgICAgICAgICAgIGlmICh1c2VyTGlzdCAmJiB1c2VyTGlzdC5sZW5ndGggJiYgdXNlckxpc3QubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHVzZXJMaXN0LmZvckVhY2godXNlciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE9iaiA9IHVzZXJzLmZpbmQoZSA9PiBlLnZhbHVlID09PSB1c2VyLmRuKTtcclxuICAgICAgICAgICAgICAgIGlmICghZmllbGRPYmopIHtcclxuICAgICAgICAgICAgICAgICAgdXNlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdXNlci5kbixcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdXNlci5jbixcclxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogdXNlci5uYW1lXHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudGVhbVJvbGVVc2VyT3B0aW9ucyA9IHVzZXJzO1xyXG4gICAgICAgICAgICAvLyBUT0RPOiBnZXR1c2VyIGVudGl0aWVzIGJ5IHVzZXJJZFxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50ZWFtUm9sZVVzZXJPcHRpb25zID0gW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuc3RhdHVzU2VydmljZS5nZXRTdGF0dXNCeUNhdGVnb3J5TGV2ZWxBbmRTdGF0dXNUeXBlKFByb2plY3RDb25zdGFudC5DQVRFR09SWV9MRVZFTF9QUk9KRUNULCBTdGF0dXNUeXBlcy5JTklUSUFMKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0QWxsQ1JTdGF0dXNSZWFzb25zKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgdGhpcy5wcm9qZWN0U3RhdHVzLk1QTV9TdGF0dXNfUmVhc29uID0gcmVzcG9uc2U7XHJcbiAgICB9KTtcclxuXHJcbiAgfVxyXG59XHJcbiJdfQ==