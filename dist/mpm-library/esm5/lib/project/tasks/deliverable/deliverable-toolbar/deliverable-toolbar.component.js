import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SharingService } from '../../../../mpm-utils/services/sharing.service';
import { DeliverableSortComponent } from '../deliverable-sort/deliverable-sort.component';
import { TaskService } from '../../task.service';
import { TaskConstants } from '../../task.constants';
import { AssetStatusConstants } from '../../../asset-card/asset_status_constants';
var DeliverableToolbarComponent = /** @class */ (function () {
    function DeliverableToolbarComponent(sharingService, taskService) {
        this.sharingService = sharingService;
        this.taskService = taskService;
        this.resouceFilterChange = new EventEmitter();
        this.deliverableConfigChange = new EventEmitter();
        this.createNewDeliverableHandler = new EventEmitter();
        this.showTaskViewButton = true;
        this.viewByResources = new FormControl();
        this.taskConstants = TaskConstants;
        this.assetStatusConstants = AssetStatusConstants;
        this.disableAction = true;
    }
    DeliverableToolbarComponent.prototype.createNewDeliverable = function () {
        this.createNewDeliverableHandler.next(this.deliverableConfig);
    };
    DeliverableToolbarComponent.prototype.selectResource = function (resourceName) {
    };
    DeliverableToolbarComponent.prototype.refresh = function () {
        this.deliverableConfigChange.next(this.deliverableConfig);
    };
    DeliverableToolbarComponent.prototype.changeView = function () {
        if (this.deliverableConfig.isTaskListView) {
            this.deliverableConfig.isTaskListView = false;
            localStorage.setItem('isTaskListView', 'false');
        }
        else {
            this.deliverableConfig.isTaskListView = true;
            localStorage.setItem('isTaskListView', 'true');
        }
        this.refresh();
    };
    DeliverableToolbarComponent.prototype.onSortChange = function (eventData) {
        if (eventData && eventData.option) {
            this.deliverableConfig.sortFieldName = eventData.option.name;
            this.deliverableConfig.sortOrder = eventData.sortType;
        }
        this.deliverableConfigChange.next(this.deliverableConfig);
    };
    DeliverableToolbarComponent.prototype.refreshChild = function (deliverableConfig) {
        this.deliverableConfig = deliverableConfig;
        if (this.deliverableSortComponent) {
            this.deliverableSortComponent.refreshData(this.deliverableConfig);
        }
    };
    DeliverableToolbarComponent.prototype.onResourceSelect = function () {
        this.deliverableConfig.selectedResources = this.viewByResources.value;
        this.deliverableConfigChange.next(this.deliverableConfig);
    };
    DeliverableToolbarComponent.prototype.onResourceRemoved = function (resource) {
        var index = this.deliverableConfig.selectedResources.indexOf(resource);
        this.deliverableConfig.selectedResources.splice(index, 1);
        this.isViewByResourceFilterEnabled = false;
        this.viewByResources.patchValue(this.deliverableConfig.selectedResources);
        this.deliverableConfigChange.next(this.deliverableConfig);
        this.resouceFilterChange.next(this.isViewByResourceFilterEnabled);
    };
    DeliverableToolbarComponent.prototype.ngOnInit = function () {
        var currentUSerID = this.sharingService.getCurrentUserItemID();
        if (currentUSerID && this.deliverableConfig.selectedProject &&
            currentUSerID === this.deliverableConfig.selectedProject.R_PO_PROJECT_OWNER['Identity-id'].Id) {
            this.deliverableConfig.isProjectOwner = true;
        }
        this.isProjectCompleted = false;
        this.isOnHoldProject = (this.deliverableConfig.selectedProject && this.deliverableConfig.selectedProject.IS_ON_HOLD === 'true') ? true : false;
        if (this.deliverableConfig.isNewDeliverable) {
            this.createNewDeliverable();
        }
    };
    DeliverableToolbarComponent.ctorParameters = function () { return [
        { type: SharingService },
        { type: TaskService }
    ]; };
    __decorate([
        Input()
    ], DeliverableToolbarComponent.prototype, "deliverableConfig", void 0);
    __decorate([
        Input()
    ], DeliverableToolbarComponent.prototype, "isViewByResourceFilterEnabled", void 0);
    __decorate([
        Output()
    ], DeliverableToolbarComponent.prototype, "resouceFilterChange", void 0);
    __decorate([
        Output()
    ], DeliverableToolbarComponent.prototype, "deliverableConfigChange", void 0);
    __decorate([
        Output()
    ], DeliverableToolbarComponent.prototype, "createNewDeliverableHandler", void 0);
    __decorate([
        Input()
    ], DeliverableToolbarComponent.prototype, "enableAction", void 0);
    __decorate([
        Input()
    ], DeliverableToolbarComponent.prototype, "delCount", void 0);
    __decorate([
        Input()
    ], DeliverableToolbarComponent.prototype, "crBulkResponse", void 0);
    __decorate([
        ViewChild(DeliverableSortComponent)
    ], DeliverableToolbarComponent.prototype, "deliverableSortComponent", void 0);
    DeliverableToolbarComponent = __decorate([
        Component({
            selector: 'mpm-deliverable-toolbar',
            template: "<mat-toolbar class=\"app-task-toolbar\">\r\n    <mat-toolbar-row>\r\n        <div class=\"flex-row-item task-info-holder\">\r\n            <span class=\"flex-row-item deliverable-count\">Deliverables\r\n                ({{deliverableConfig.deliverableTotalCount ? deliverableConfig.deliverableTotalCount : '0'}})</span>\r\n            <span class=\"flex-row-item task-details\"\r\n                *ngIf=\"deliverableConfig.selectedTask && deliverableConfig.selectedTask.NAME\">\r\n                <mat-icon>assignment_turned_in</mat-icon>\r\n                <div class=\"task-name\" matTooltip=\"Task Name: {{deliverableConfig.selectedTask.NAME}}\">\r\n                    {{deliverableConfig.selectedTask.NAME}}</div>\r\n            </span>\r\n            <button mat-icon-button (click)=\"refresh()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <span class=\"spacer\"></span>\r\n        <!--   <span *ngIf=\"delCount\" matTooltip=\"Selected Deliverable\"> ({{delCount}})</span> -->\r\n        <!--   <span>\r\n            <button [disabled]=\"!enableAction\" class=\"new-project-btn\" mat-raised-button  matTooltip=\"Actions\"\r\n                [matMenuTriggerFor]=\"actionTypeMenu\">\r\n                <span> Actions</span>\r\n                <mat-icon>arrow_drop_down</mat-icon>\r\n            </button>\r\n        </span>\r\n        <mat-menu #actionTypeMenu=\"matMenu\">\r\n            <button mat-menu-item matTooltip=\"Approved\">\r\n                <span>Approved</span>\r\n            </button>\r\n            <button mat-menu-item matTooltip=\"Rejected\">\r\n                <span>Rejected</span>\r\n            </button>\r\n        </mat-menu> -->\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mpm-deliverable-sort [sortOptions]=\"deliverableConfig.deliverableSortOptions\"\r\n                [selectedOption]=\"deliverableConfig.selectedSortOption\" (onSortChange)=\"onSortChange($event)\">\r\n            </mpm-deliverable-sort>\r\n        </div>\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <mat-form-field appearance=\"outline\" class=\"resource-select\">\r\n                <mat-label>View by resource</mat-label>\r\n                <mat-select #resourceSelect [formControl]=\"viewByResources\" multiple\r\n                    [disabled]=\"!deliverableConfig.deliverableResources.length\">\r\n                    <mat-select-trigger>\r\n                        {{viewByResources.value ? viewByResources.value[0] : ''}}\r\n                        <span *ngIf=\"viewByResources.value?.length > 1\" class=\"example-additional-selection\">\r\n                            (+{{viewByResources.value.length - 1}}\r\n                            {{viewByResources.value?.length === 2 ? 'other' : 'others'}})\r\n                        </span>\r\n                    </mat-select-trigger>\r\n                    <mat-option *ngFor=\"let resource of deliverableConfig.deliverableResources\" [value]=\"resource\">\r\n                        {{resource}}\r\n                    </mat-option>\r\n\r\n                    <button class=\"resource-apply-btn\" (click)=\"onResourceSelect(); resourceSelect.close()\"\r\n                        mat-button>Apply</button>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-stroked-button (click)=\"createNewDeliverable()\"\r\n                [disabled]=\"!deliverableConfig.isProjectOwner || isProjectCompleted || (deliverableConfig.selectedTask && deliverableConfig.selectedTask.TASK_TYPE === taskConstants.TASK_TYPE.TASK_WITHOUT_DELIVERABLE) || deliverableConfig.isReadOnly ||\r\n            (deliverableConfig.selectedTask && deliverableConfig.selectedTask.statusDetails && deliverableConfig.selectedTask.statusDetails.NAME === assetStatusConstants.APPROVED) || (deliverableConfig.selectedTask && deliverableConfig.selectedTask.IS_APPROVAL_TASK === 'true')\">\r\n                <mat-icon>add</mat-icon>\r\n                <span> New Deliverable</span>\r\n            </button>\r\n        </div>\r\n\r\n        <span class=\"flex-row-item right flex-grow-0\">\r\n            <button mat-icon-button>\r\n                <mat-icon class=\"dashboard-action-btn\">settings\r\n                </mat-icon>\r\n            </button>\r\n        </span>\r\n    </mat-toolbar-row>\r\n\r\n    <mat-toolbar-row *ngIf=\"deliverableConfig.selectedResources && deliverableConfig.selectedResources.length > 0\">\r\n        <span class=\"flex-row\">\r\n            <span class=\"resource-heading\">Resources: </span>\r\n            <mat-chip-list aria-label=\"resource selection\">\r\n                <mat-chip [removable]=\"true\" (removed)=\"onResourceRemoved(resource)\"\r\n                    *ngFor=\"let resource of deliverableConfig.selectedResources\">{{resource}}\r\n                    <mat-icon matChipRemove>cancel</mat-icon>\r\n                </mat-chip>\r\n            </mat-chip-list>\r\n        </span>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>",
            styles: ["mat-toolbar.app-task-toolbar{background-color:transparent}.app-task-toolbar .spacer{flex:1 1 auto}.flex-row-item button{margin:5px}.resources-row,.task-resource-icon{font-size:14px}.resourceCheckbox{margin-right:5px;margin-left:5px}.task-icon-btn{margin-top:18px}.task-info-holder{flex-grow:0;align-items:center}.task-info-holder .deliverable-count{padding:8px}.task-info-holder .task-details{align-items:center;max-width:150px}.task-info-holder .task-details .task-name{text-overflow:ellipsis;word-break:break-all;overflow:hidden;white-space:nowrap}.align-items-center{align-items:center}.right{justify-content:flex-end}mat-form-field.resource-select{font-size:11px}::ng-deep .resource-select .mat-form-field-wrapper{padding:0}.flex-grow-0{flex-grow:0}.resources-heading{font-size:16px;margin-right:8px}.resource-apply-btn{width:100%}"]
        })
    ], DeliverableToolbarComponent);
    return DeliverableToolbarComponent;
}());
export { DeliverableToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLXRvb2xiYXIvZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUEyQyxNQUFNLGVBQWUsQ0FBQztBQUNuSSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ2hGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQzFGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDckQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFRbEY7SUEyQkkscUNBQ1csY0FBOEIsRUFDOUIsV0FBd0I7UUFEeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBeEJ6Qix3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzlDLDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDbEQsZ0NBQTJCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQVFoRSx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFFMUIsb0JBQWUsR0FBRyxJQUFJLFdBQVcsRUFBRSxDQUFDO1FBRXBDLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBQzlCLHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBQzVDLGtCQUFhLEdBQUcsSUFBSSxDQUFDO0lBU2pCLENBQUM7SUFFTCwwREFBb0IsR0FBcEI7UUFDSSxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxvREFBYyxHQUFkLFVBQWUsWUFBWTtJQUUzQixDQUFDO0lBRUQsNkNBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELGdEQUFVLEdBQVY7UUFDSSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDOUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNuRDthQUFNO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDN0MsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsRDtRQUNELElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsa0RBQVksR0FBWixVQUFhLFNBQVM7UUFDbEIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQzdELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQztTQUN6RDtRQUNELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELGtEQUFZLEdBQVosVUFBYSxpQkFBaUI7UUFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDO1FBQzNDLElBQUksSUFBSSxDQUFDLHdCQUF3QixFQUFFO1lBQy9CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDckU7SUFDTCxDQUFDO0lBRUQsc0RBQWdCLEdBQWhCO1FBQ0ksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDO1FBQ3RFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHVEQUFpQixHQUFqQixVQUFrQixRQUFRO1FBQ3RCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLDZCQUE2QixHQUFDLEtBQUssQ0FBQztRQUV6QyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELDhDQUFRLEdBQVI7UUFDSSxJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDakUsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWU7WUFDdkQsYUFBYSxLQUFLLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQy9GLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsZUFBZSxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDL0ksSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUU7WUFDekMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FDL0I7SUFDTCxDQUFDOztnQkFwRTBCLGNBQWM7Z0JBQ2pCLFdBQVc7O0lBM0IxQjtRQUFSLEtBQUssRUFBRTswRUFBbUI7SUFDbEI7UUFBUixLQUFLLEVBQUU7c0ZBQStCO0lBRTdCO1FBQVQsTUFBTSxFQUFFOzRFQUErQztJQUM5QztRQUFULE1BQU0sRUFBRTtnRkFBbUQ7SUFDbEQ7UUFBVCxNQUFNLEVBQUU7b0ZBQXVEO0lBRXZEO1FBQVIsS0FBSyxFQUFFO3FFQUFjO0lBQ2I7UUFBUixLQUFLLEVBQUU7aUVBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTt1RUFBZ0I7SUFjYTtRQUFwQyxTQUFTLENBQUMsd0JBQXdCLENBQUM7aUZBQW9EO0lBekIvRSwyQkFBMkI7UUFOdkMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHlCQUF5QjtZQUNuQyx1b0tBQW1EOztTQUV0RCxDQUFDO09BRVcsMkJBQTJCLENBa0d2QztJQUFELGtDQUFDO0NBQUEsQUFsR0QsSUFrR0M7U0FsR1ksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgVmlld0NoaWxkLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIEFmdGVyVmlld0luaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlU29ydENvbXBvbmVudCB9IGZyb20gJy4uL2RlbGl2ZXJhYmxlLXNvcnQvZGVsaXZlcmFibGUtc29ydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYXNrU2VydmljZSB9IGZyb20gJy4uLy4uL3Rhc2suc2VydmljZSc7XHJcbmltcG9ydCB7IFRhc2tDb25zdGFudHMgfSBmcm9tICcuLi8uLi90YXNrLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IEFzc2V0U3RhdHVzQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vLi4vYXNzZXQtY2FyZC9hc3NldF9zdGF0dXNfY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tZGVsaXZlcmFibGUtdG9vbGJhcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGVsaXZlcmFibGUtdG9vbGJhci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kZWxpdmVyYWJsZS10b29sYmFyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEZWxpdmVyYWJsZVRvb2xiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGRlbGl2ZXJhYmxlQ29uZmlnO1xyXG4gICAgQElucHV0KCkgaXNWaWV3QnlSZXNvdXJjZUZpbHRlckVuYWJsZWQ7XHJcblxyXG4gICAgQE91dHB1dCgpIHJlc291Y2VGaWx0ZXJDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBkZWxpdmVyYWJsZUNvbmZpZ0NoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNyZWF0ZU5ld0RlbGl2ZXJhYmxlSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBJbnB1dCgpIGVuYWJsZUFjdGlvbjtcclxuICAgIEBJbnB1dCgpIGRlbENvdW50O1xyXG4gICAgQElucHV0KCkgY3JCdWxrUmVzcG9uc2U7XHJcblxyXG5cclxuICAgIGlzUHJvamVjdENvbXBsZXRlZDogYm9vbGVhbjtcclxuICAgIHNob3dUYXNrVmlld0J1dHRvbiA9IHRydWU7XHJcbiAgICBzZWxlY3RlZE9wdGlvbjtcclxuICAgIHZpZXdCeVJlc291cmNlcyA9IG5ldyBGb3JtQ29udHJvbCgpO1xyXG5cclxuICAgIHRhc2tDb25zdGFudHMgPSBUYXNrQ29uc3RhbnRzO1xyXG4gICAgYXNzZXRTdGF0dXNDb25zdGFudHMgPSBBc3NldFN0YXR1c0NvbnN0YW50cztcclxuICAgIGRpc2FibGVBY3Rpb24gPSB0cnVlO1xyXG5cclxuICAgIGlzT25Ib2xkUHJvamVjdDogYm9vbGVhbjtcclxuXHJcbiAgICBAVmlld0NoaWxkKERlbGl2ZXJhYmxlU29ydENvbXBvbmVudCkgZGVsaXZlcmFibGVTb3J0Q29tcG9uZW50OiBEZWxpdmVyYWJsZVNvcnRDb21wb25lbnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgdGFza1NlcnZpY2U6IFRhc2tTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGNyZWF0ZU5ld0RlbGl2ZXJhYmxlKCkge1xyXG4gICAgICAgIHRoaXMuY3JlYXRlTmV3RGVsaXZlcmFibGVIYW5kbGVyLm5leHQodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2VsZWN0UmVzb3VyY2UocmVzb3VyY2VOYW1lKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2goKSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZ0NoYW5nZS5uZXh0KHRoaXMuZGVsaXZlcmFibGVDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVZpZXcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGVsaXZlcmFibGVDb25maWcuaXNUYXNrTGlzdFZpZXcpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZy5pc1Rhc2tMaXN0VmlldyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnaXNUYXNrTGlzdFZpZXcnLCAnZmFsc2UnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmlzVGFza0xpc3RWaWV3ID0gdHJ1ZTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2lzVGFza0xpc3RWaWV3JywgJ3RydWUnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Tb3J0Q2hhbmdlKGV2ZW50RGF0YSkge1xyXG4gICAgICAgIGlmIChldmVudERhdGEgJiYgZXZlbnREYXRhLm9wdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNvcnRGaWVsZE5hbWUgPSBldmVudERhdGEub3B0aW9uLm5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWcuc29ydE9yZGVyID0gZXZlbnREYXRhLnNvcnRUeXBlO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnQ2hhbmdlLm5leHQodGhpcy5kZWxpdmVyYWJsZUNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaENoaWxkKGRlbGl2ZXJhYmxlQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZyA9IGRlbGl2ZXJhYmxlQ29uZmlnO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlU29ydENvbXBvbmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlU29ydENvbXBvbmVudC5yZWZyZXNoRGF0YSh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNvdXJjZVNlbGVjdCgpIHtcclxuICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzID0gdGhpcy52aWV3QnlSZXNvdXJjZXMudmFsdWU7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZUNvbmZpZ0NoYW5nZS5uZXh0KHRoaXMuZGVsaXZlcmFibGVDb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uUmVzb3VyY2VSZW1vdmVkKHJlc291cmNlKSB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUmVzb3VyY2VzLmluZGV4T2YocmVzb3VyY2UpO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRSZXNvdXJjZXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB0aGlzLmlzVmlld0J5UmVzb3VyY2VGaWx0ZXJFbmFibGVkPWZhbHNlO1xyXG5cclxuICAgICAgICB0aGlzLnZpZXdCeVJlc291cmNlcy5wYXRjaFZhbHVlKHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRSZXNvdXJjZXMpO1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVDb25maWdDaGFuZ2UubmV4dCh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnKTtcclxuICAgICAgICB0aGlzLnJlc291Y2VGaWx0ZXJDaGFuZ2UubmV4dCh0aGlzLmlzVmlld0J5UmVzb3VyY2VGaWx0ZXJFbmFibGVkKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50VVNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpO1xyXG4gICAgICAgIGlmIChjdXJyZW50VVNlcklEICYmIHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRQcm9qZWN0ICYmXHJcbiAgICAgICAgICAgIGN1cnJlbnRVU2VySUQgPT09IHRoaXMuZGVsaXZlcmFibGVDb25maWcuc2VsZWN0ZWRQcm9qZWN0LlJfUE9fUFJPSkVDVF9PV05FUlsnSWRlbnRpdHktaWQnXS5JZCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmlzUHJvamVjdE93bmVyID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5pc1Byb2plY3RDb21wbGV0ZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmlzT25Ib2xkUHJvamVjdCA9ICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUHJvamVjdCAmJiB0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLnNlbGVjdGVkUHJvamVjdC5JU19PTl9IT0xEID09PSAndHJ1ZScpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmRlbGl2ZXJhYmxlQ29uZmlnLmlzTmV3RGVsaXZlcmFibGUpIHtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVOZXdEZWxpdmVyYWJsZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19