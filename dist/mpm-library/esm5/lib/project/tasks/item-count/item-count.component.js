import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var ItemCountComponent = /** @class */ (function () {
    function ItemCountComponent() {
    }
    ItemCountComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input()
    ], ItemCountComponent.prototype, "name", void 0);
    __decorate([
        Input()
    ], ItemCountComponent.prototype, "count", void 0);
    ItemCountComponent = __decorate([
        Component({
            selector: 'mpm-item-count',
            template: "<span class=\"task-count flex-row-item\">{{name}} ({{count}})</span>",
            styles: [""]
        })
    ], ItemCountComponent);
    return ItemCountComponent;
}());
export { ItemCountComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS1jb3VudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wcm9qZWN0L3Rhc2tzL2l0ZW0tY291bnQvaXRlbS1jb3VudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBT3pEO0lBSUU7SUFBZ0IsQ0FBQztJQUVqQixxQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQU5RO1FBQVIsS0FBSyxFQUFFO29EQUFNO0lBQ0w7UUFBUixLQUFLLEVBQUU7cURBQU87SUFGSixrQkFBa0I7UUFMOUIsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixnRkFBMEM7O1NBRTNDLENBQUM7T0FDVyxrQkFBa0IsQ0FTOUI7SUFBRCx5QkFBQztDQUFBLEFBVEQsSUFTQztTQVRZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0taXRlbS1jb3VudCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2l0ZW0tY291bnQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2l0ZW0tY291bnQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSXRlbUNvdW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBuYW1lO1xyXG4gIEBJbnB1dCgpIGNvdW50O1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==