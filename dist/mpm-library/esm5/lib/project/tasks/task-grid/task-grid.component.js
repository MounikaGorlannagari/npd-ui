import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { SwimLaneComponent } from '../swim-lane/swim-lane.component';
import { TaskListViewComponent } from '../task-list-view/task-list-view.component';
var TaskGridComponent = /** @class */ (function () {
    function TaskGridComponent() {
        this.refreshTaskGrid = new EventEmitter();
        this.editTaskHandler = new EventEmitter();
        this.taskDetailsHandler = new EventEmitter();
        this.sortChange = new EventEmitter();
    }
    TaskGridComponent.prototype.refreshChild = function (taskConfig) {
        this.taskConfig = taskConfig;
        if (this.taskConfig.isTaskListView) {
            this.taskListViewComponent.refreshData(this.taskConfig);
        }
        else {
            this.swimLaneComponent.initialiseSwimList();
        }
    };
    TaskGridComponent.prototype.refreshSwimLine = function (event) {
        if (event) {
            this.refreshTaskGrid.next(true);
        }
    };
    TaskGridComponent.prototype.refreshTaskList = function (event) {
        this.refreshTaskGrid.next(event);
    };
    TaskGridComponent.prototype.editTask = function (eventData) {
        this.editTaskHandler.next(eventData);
    };
    TaskGridComponent.prototype.openTaskDetails = function (eventData) {
        this.taskDetailsHandler.next(eventData);
    };
    TaskGridComponent.prototype.onSortChange = function (eventData) {
        this.sortChange.emit(eventData);
    };
    TaskGridComponent.prototype.ngOnInit = function () {
        if (this.taskConfig) {
        }
    };
    __decorate([
        Input()
    ], TaskGridComponent.prototype, "taskConfig", void 0);
    __decorate([
        Output()
    ], TaskGridComponent.prototype, "refreshTaskGrid", void 0);
    __decorate([
        Output()
    ], TaskGridComponent.prototype, "editTaskHandler", void 0);
    __decorate([
        Output()
    ], TaskGridComponent.prototype, "taskDetailsHandler", void 0);
    __decorate([
        Output()
    ], TaskGridComponent.prototype, "sortChange", void 0);
    __decorate([
        ViewChild(SwimLaneComponent)
    ], TaskGridComponent.prototype, "swimLaneComponent", void 0);
    __decorate([
        ViewChild(TaskListViewComponent)
    ], TaskGridComponent.prototype, "taskListViewComponent", void 0);
    TaskGridComponent = __decorate([
        Component({
            selector: 'mpm-task-grid',
            template: "<mpm-swim-lane *ngIf=\"!taskConfig.isTaskListView\" [taskConfig]=\"taskConfig\" (refreshSwimLine)=\"refreshSwimLine($event)\"\r\n    (editTaskHandler)=\"editTask($event)\" (taskDetailsHandler)=\"openTaskDetails($event)\">\r\n</mpm-swim-lane>\r\n<mpm-task-list-view *ngIf=\"taskConfig.isTaskListView\" [taskConfig]=\"taskConfig\"\r\n    (editTaskHandler)=\"editTask($event)\"  (taskDetailsHandler)=\"openTaskDetails($event)\" (sortChange)=\"onSortChange($event)\"\r\n    (refreshTaskListData)=\"refreshTaskList($event)\">\r\n</mpm-task-list-view>",
            styles: ["app-swim-lane{width:100%}"]
        })
    ], TaskGridComponent);
    return TaskGridComponent;
}());
export { TaskGridComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFzay1ncmlkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3Byb2plY3QvdGFza3MvdGFzay1ncmlkL3Rhc2stZ3JpZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUW5GO0lBV0k7UUFSVSxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQzFDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDN0MsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFLL0IsQ0FBQztJQUVqQix3Q0FBWSxHQUFaLFVBQWEsVUFBVTtRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzNEO2FBQU07WUFDSCxJQUFJLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUMvQztJQUNMLENBQUM7SUFFRCwyQ0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDakIsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7SUFFRCwyQ0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDakIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVILG9DQUFRLEdBQVIsVUFBUyxTQUFTO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELDJDQUFlLEdBQWYsVUFBZ0IsU0FBUztRQUNyQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCx3Q0FBWSxHQUFaLFVBQWEsU0FBUztRQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtTQUVwQjtJQUNMLENBQUM7SUE5Q1E7UUFBUixLQUFLLEVBQUU7eURBQVk7SUFDVjtRQUFULE1BQU0sRUFBRTs4REFBMkM7SUFDMUM7UUFBVCxNQUFNLEVBQUU7OERBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFO2lFQUE4QztJQUM3QztRQUFULE1BQU0sRUFBRTt5REFBc0M7SUFFakI7UUFBN0IsU0FBUyxDQUFDLGlCQUFpQixDQUFDO2dFQUFzQztJQUNqQztRQUFqQyxTQUFTLENBQUMscUJBQXFCLENBQUM7b0VBQThDO0lBVHRFLGlCQUFpQjtRQU43QixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsZUFBZTtZQUN6Qiw2aUJBQXlDOztTQUU1QyxDQUFDO09BRVcsaUJBQWlCLENBa0Q3QjtJQUFELHdCQUFDO0NBQUEsQUFsREQsSUFrREM7U0FsRFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN3aW1MYW5lQ29tcG9uZW50IH0gZnJvbSAnLi4vc3dpbS1sYW5lL3N3aW0tbGFuZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYXNrTGlzdFZpZXdDb21wb25lbnQgfSBmcm9tICcuLi90YXNrLWxpc3Qtdmlldy90YXNrLWxpc3Qtdmlldy5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS10YXNrLWdyaWQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3Rhc2stZ3JpZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi90YXNrLWdyaWQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRhc2tHcmlkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSB0YXNrQ29uZmlnO1xyXG4gICAgQE91dHB1dCgpIHJlZnJlc2hUYXNrR3JpZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGVkaXRUYXNrSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHRhc2tEZXRhaWxzSGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHNvcnRDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAVmlld0NoaWxkKFN3aW1MYW5lQ29tcG9uZW50KSBzd2ltTGFuZUNvbXBvbmVudDogU3dpbUxhbmVDb21wb25lbnQ7XHJcbiAgICBAVmlld0NoaWxkKFRhc2tMaXN0Vmlld0NvbXBvbmVudCkgdGFza0xpc3RWaWV3Q29tcG9uZW50OiBUYXNrTGlzdFZpZXdDb21wb25lbnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICByZWZyZXNoQ2hpbGQodGFza0NvbmZpZykge1xyXG4gICAgICAgIHRoaXMudGFza0NvbmZpZyA9IHRhc2tDb25maWc7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZy5pc1Rhc2tMaXN0Vmlldykge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tMaXN0Vmlld0NvbXBvbmVudC5yZWZyZXNoRGF0YSh0aGlzLnRhc2tDb25maWcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3dpbUxhbmVDb21wb25lbnQuaW5pdGlhbGlzZVN3aW1MaXN0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlZnJlc2hTd2ltTGluZShldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnJlc2hUYXNrR3JpZC5uZXh0KHRydWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoVGFza0xpc3QoZXZlbnQpe1xyXG4gICAgICAgIHRoaXMucmVmcmVzaFRhc2tHcmlkLm5leHQoZXZlbnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgZWRpdFRhc2soZXZlbnREYXRhKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0VGFza0hhbmRsZXIubmV4dChldmVudERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5UYXNrRGV0YWlscyhldmVudERhdGEpIHtcclxuICAgICAgICB0aGlzLnRhc2tEZXRhaWxzSGFuZGxlci5uZXh0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Tb3J0Q2hhbmdlKGV2ZW50RGF0YSkge1xyXG4gICAgICAgIHRoaXMuc29ydENoYW5nZS5lbWl0KGV2ZW50RGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudGFza0NvbmZpZykge1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==