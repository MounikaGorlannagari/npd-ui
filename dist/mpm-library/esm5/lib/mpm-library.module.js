import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { MaterialModule } from './material.module';
import { LoginModule } from './login/login.module';
import { SearchModule } from './search/search.module';
import { PaginationModule } from './pagination/pagination.module';
import { ShareAssetsModule } from './share-assets/share-assets.module';
import { MpmLibraryComponent } from './mpm-library.component';
import { NotificationComponent } from './notification/notification.component';
import { LoaderComponent } from './loader/loader.component';
import { ProjectOverviewComponent } from './project/project-overview/project-overview.component';
import { TaskCreationComponent } from './project/tasks/task-creation/task-creation.component';
import { CustomSearchFieldComponent } from './shared/components/custom-search-field/custom-search-field.component';
import { ConfirmationModalComponent } from './shared/components/confirmation-modal/confirmation-modal.component';
import { AutofocusDirective } from './shared/directive/AutofocusDirective';
import { CopyClipboardDirective } from './shared/directive/CopyClipboardDirective';
import { DisableControlDirective } from './shared/directive/DisableControlDirective';
import { DeliverableCreationComponent } from './project/tasks/deliverable/deliverable-creation/deliverable-creation.component';
import { AssetsComponent } from './project/assets/assets.component';
import { AssetCardComponent } from './project/asset-card/asset-card.component';
import { FileUploadComponent } from './upload/file-upload/file-upload.component';
import { UploadComponent } from './upload/upload/upload.component';
import { TaskCardViewComponent } from './project/tasks/task-card-view/task-card-view.component';
import { IframeComponent } from './shared/components/iframe/iframe.component';
import { CustomMetadataFieldComponent } from './shared/components/custom-metadata-field/custom-metadata-field.component';
import { FormatToLocalePipe } from './shared/pipe/format-to-locale.pipe';
import { FormatToLocaleDateTimePipe } from './shared/pipe/format-to-locale-date-time.pipe';
import { ProjectFromTemplatesComponent } from './project/project-from-templates/project-from-templates.component';
import { MpmAutoCompleteComponent } from './shared/components/mpm-auto-complete/mpm-auto-complete.component';
import { AssetDetailsComponent } from './project/asset-details/asset-details.component';
import { QdsTransferTrayComponent } from './shared/components/qds-transfer-tray/qds-transfer-tray.component';
import { QdsTransferTrayModalComponent } from './shared/components/qds-transfer-tray-modal/qds-transfer-tray-modal.component';
import { DownloadsTransferTrayComponent } from './shared/components/downloads-transfer-tray/downloads-transfer-tray.component';
import { DownloadsTransferTrayModalComponent } from './shared/components/downloads-transfer-tray-modal/downloads-transfer-tray-modal.component';
import { AssetPreviewComponent } from './shared/components/asset-preview/asset-preview.component';
import { EmailNotificationPreferenceComponent } from './shared/components/email-notification-preference/email-notification-preference.component';
import { ResourceManagementToolbarComponent } from './resource-management/resource-management-toolbar/resource-management-toolbar.component';
import { ExportDataComponent } from './shared/components/export-data/export-data.component';
import { ExportDataTrayComponent } from './shared/components/export-data-tray/export-data-tray.component';
import { ExportDataTrayModalComponent } from './shared/components/export-data-tray-modal/export-data-tray-modal.component';
import { ResourceManagementComponent } from './resource-management/resource-management.component';
import { SynchronizeTaskFiltersComponent } from './resource-management/synchronize-task-filters/synchronize-task-filters.component';
import { ResourceManagementResourceAllocationComponent } from './resource-management/resource-management-resource-allocation/resource-management-resource-allocation.component';
import { AssetListComponent } from './project/asset-list/asset-list.component';
import { GetPropertyPipe } from './shared/pipe/get-property.pipe';
var MpmLibraryModule = /** @class */ (function () {
    function MpmLibraryModule() {
    }
    MpmLibraryModule = __decorate([
        NgModule({
            declarations: [
                MpmLibraryComponent,
                NotificationComponent,
                LoaderComponent,
                ProjectOverviewComponent,
                CustomSearchFieldComponent,
                CustomMetadataFieldComponent,
                ConfirmationModalComponent,
                AutofocusDirective,
                CopyClipboardDirective,
                DisableControlDirective,
                TaskCreationComponent,
                DeliverableCreationComponent,
                AssetsComponent,
                AssetCardComponent,
                AssetDetailsComponent,
                FileUploadComponent,
                UploadComponent,
                TaskCardViewComponent,
                IframeComponent,
                FormatToLocalePipe,
                FormatToLocaleDateTimePipe,
                ProjectFromTemplatesComponent,
                MpmAutoCompleteComponent,
                QdsTransferTrayComponent,
                QdsTransferTrayModalComponent,
                DownloadsTransferTrayComponent,
                DownloadsTransferTrayModalComponent,
                AssetPreviewComponent,
                EmailNotificationPreferenceComponent,
                ResourceManagementToolbarComponent,
                ExportDataComponent,
                ExportDataTrayComponent,
                ExportDataTrayModalComponent,
                ResourceManagementComponent,
                SynchronizeTaskFiltersComponent,
                ResourceManagementResourceAllocationComponent,
                AssetListComponent,
                GetPropertyPipe
            ],
            imports: [
                CommonModule,
                MaterialModule,
                PaginationModule,
                LoginModule,
                SearchModule,
                ShareAssetsModule
            ],
            exports: [
                MpmLibraryComponent,
                NotificationComponent,
                LoaderComponent,
                ProjectOverviewComponent,
                AutofocusDirective,
                CopyClipboardDirective,
                DisableControlDirective,
                TaskCreationComponent,
                DeliverableCreationComponent,
                ConfirmationModalComponent,
                AssetsComponent,
                AssetCardComponent,
                AssetDetailsComponent,
                TaskCardViewComponent,
                IframeComponent,
                ProjectFromTemplatesComponent,
                QdsTransferTrayModalComponent,
                DownloadsTransferTrayModalComponent,
                AssetPreviewComponent,
                CustomSearchFieldComponent,
                CustomMetadataFieldComponent,
                MpmAutoCompleteComponent,
                EmailNotificationPreferenceComponent,
                ExportDataComponent,
                ExportDataTrayComponent,
                ExportDataTrayModalComponent,
                AssetListComponent,
                GetPropertyPipe
            ],
            providers: [
                FormatToLocalePipe,
                FormatToLocaleDateTimePipe,
                TitleCasePipe,
                GetPropertyPipe
            ],
            entryComponents: []
        })
    ], MpmLibraryModule);
    return MpmLibraryModule;
}());
export { MpmLibraryModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWxpYnJhcnkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLWxpYnJhcnkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsYUFBYSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFFdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBRTlGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHVFQUF1RSxDQUFDO0FBQ25ILE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBRWpILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLGlGQUFpRixDQUFDO0FBQy9ILE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDbkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seURBQXlELENBQUM7QUFDaEcsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDJFQUEyRSxDQUFDO0FBQ3pILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzNGLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG1FQUFtRSxDQUFDO0FBQ2xILE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG1FQUFtRSxDQUFDO0FBQzdHLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG1FQUFtRSxDQUFDO0FBQzdHLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLCtFQUErRSxDQUFDO0FBQzlILE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLCtFQUErRSxDQUFDO0FBQy9ILE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLDJGQUEyRixDQUFDO0FBQ2hKLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJEQUEyRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxvQ0FBb0MsRUFBRSxNQUFNLDJGQUEyRixDQUFDO0FBQ2pKLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxNQUFNLHlGQUF5RixDQUFDO0FBQzdJLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHVEQUF1RCxDQUFDO0FBQzVGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGlFQUFpRSxDQUFDO0FBQzFHLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDZFQUE2RSxDQUFDO0FBQzNILE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHFEQUFxRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSwrQkFBK0IsRUFBRSxNQUFNLG1GQUFtRixDQUFDO0FBQ3BJLE9BQU8sRUFBRSw2Q0FBNkMsRUFBRSxNQUFNLGlIQUFpSCxDQUFDO0FBQ2hMLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQTZGbEU7SUFBQTtJQUFnQyxDQUFDO0lBQXBCLGdCQUFnQjtRQXhGNUIsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFO2dCQUNWLG1CQUFtQjtnQkFDbkIscUJBQXFCO2dCQUNyQixlQUFlO2dCQUNmLHdCQUF3QjtnQkFDeEIsMEJBQTBCO2dCQUMxQiw0QkFBNEI7Z0JBQzVCLDBCQUEwQjtnQkFDMUIsa0JBQWtCO2dCQUNsQixzQkFBc0I7Z0JBQ3RCLHVCQUF1QjtnQkFDdkIscUJBQXFCO2dCQUNyQiw0QkFBNEI7Z0JBQzVCLGVBQWU7Z0JBQ2Ysa0JBQWtCO2dCQUNsQixxQkFBcUI7Z0JBQ3JCLG1CQUFtQjtnQkFDbkIsZUFBZTtnQkFDZixxQkFBcUI7Z0JBQ3JCLGVBQWU7Z0JBQ2Ysa0JBQWtCO2dCQUNsQiwwQkFBMEI7Z0JBQzFCLDZCQUE2QjtnQkFDN0Isd0JBQXdCO2dCQUN4Qix3QkFBd0I7Z0JBQ3hCLDZCQUE2QjtnQkFDN0IsOEJBQThCO2dCQUM5QixtQ0FBbUM7Z0JBQ25DLHFCQUFxQjtnQkFDckIsb0NBQW9DO2dCQUNwQyxrQ0FBa0M7Z0JBQ2xDLG1CQUFtQjtnQkFDbkIsdUJBQXVCO2dCQUN2Qiw0QkFBNEI7Z0JBQzVCLDJCQUEyQjtnQkFDM0IsK0JBQStCO2dCQUMvQiw2Q0FBNkM7Z0JBQzdDLGtCQUFrQjtnQkFDbEIsZUFBZTthQUNsQjtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLGNBQWM7Z0JBQ2QsZ0JBQWdCO2dCQUNoQixXQUFXO2dCQUNYLFlBQVk7Z0JBQ1osaUJBQWlCO2FBQ3BCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLG1CQUFtQjtnQkFDbkIscUJBQXFCO2dCQUNyQixlQUFlO2dCQUNmLHdCQUF3QjtnQkFDeEIsa0JBQWtCO2dCQUNsQixzQkFBc0I7Z0JBQ3RCLHVCQUF1QjtnQkFDdkIscUJBQXFCO2dCQUNyQiw0QkFBNEI7Z0JBQzVCLDBCQUEwQjtnQkFDMUIsZUFBZTtnQkFDZixrQkFBa0I7Z0JBQ2xCLHFCQUFxQjtnQkFDckIscUJBQXFCO2dCQUNyQixlQUFlO2dCQUNmLDZCQUE2QjtnQkFDN0IsNkJBQTZCO2dCQUM3QixtQ0FBbUM7Z0JBQ25DLHFCQUFxQjtnQkFDckIsMEJBQTBCO2dCQUMxQiw0QkFBNEI7Z0JBQzVCLHdCQUF3QjtnQkFDeEIsb0NBQW9DO2dCQUNwQyxtQkFBbUI7Z0JBQ25CLHVCQUF1QjtnQkFDdkIsNEJBQTRCO2dCQUM1QixrQkFBa0I7Z0JBQ2xCLGVBQWU7YUFDbEI7WUFDRCxTQUFTLEVBQUU7Z0JBQ1Asa0JBQWtCO2dCQUNsQiwwQkFBMEI7Z0JBQzFCLGFBQWE7Z0JBQ2IsZUFBZTthQUNsQjtZQUNELGVBQWUsRUFBRSxFQUFFO1NBQ3RCLENBQUM7T0FFVyxnQkFBZ0IsQ0FBSTtJQUFELHVCQUFDO0NBQUEsQUFBakMsSUFBaUM7U0FBcEIsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlLCBUaXRsZUNhc2VQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuL21hdGVyaWFsLm1vZHVsZSc7XHJcblxyXG5pbXBvcnQgeyBMb2dpbk1vZHVsZSB9IGZyb20gJy4vbG9naW4vbG9naW4ubW9kdWxlJztcclxuaW1wb3J0IHsgU2VhcmNoTW9kdWxlIH0gZnJvbSAnLi9zZWFyY2gvc2VhcmNoLm1vZHVsZSc7XHJcbmltcG9ydCB7IFBhZ2luYXRpb25Nb2R1bGUgfSBmcm9tICcuL3BhZ2luYXRpb24vcGFnaW5hdGlvbi5tb2R1bGUnO1xyXG5pbXBvcnQgeyBTaGFyZUFzc2V0c01vZHVsZSB9IGZyb20gJy4vc2hhcmUtYXNzZXRzL3NoYXJlLWFzc2V0cy5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgTXBtTGlicmFyeUNvbXBvbmVudCB9IGZyb20gJy4vbXBtLWxpYnJhcnkuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvYWRlckNvbXBvbmVudCB9IGZyb20gJy4vbG9hZGVyL2xvYWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBQcm9qZWN0T3ZlcnZpZXdDb21wb25lbnQgfSBmcm9tICcuL3Byb2plY3QvcHJvamVjdC1vdmVydmlldy9wcm9qZWN0LW92ZXJ2aWV3LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRhc2tDcmVhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vcHJvamVjdC90YXNrcy90YXNrLWNyZWF0aW9uL3Rhc2stY3JlYXRpb24uY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tc2VhcmNoLWZpZWxkL2N1c3RvbS1zZWFyY2gtZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7IEF1dG9mb2N1c0RpcmVjdGl2ZSB9IGZyb20gJy4vc2hhcmVkL2RpcmVjdGl2ZS9BdXRvZm9jdXNEaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBDb3B5Q2xpcGJvYXJkRGlyZWN0aXZlIH0gZnJvbSAnLi9zaGFyZWQvZGlyZWN0aXZlL0NvcHlDbGlwYm9hcmREaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBEaXNhYmxlQ29udHJvbERpcmVjdGl2ZSB9IGZyb20gJy4vc2hhcmVkL2RpcmVjdGl2ZS9EaXNhYmxlQ29udHJvbERpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlQ3JlYXRpb25Db21wb25lbnQgfSBmcm9tICcuL3Byb2plY3QvdGFza3MvZGVsaXZlcmFibGUvZGVsaXZlcmFibGUtY3JlYXRpb24vZGVsaXZlcmFibGUtY3JlYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXNzZXRzQ29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0L2Fzc2V0cy9hc3NldHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXNzZXRDYXJkQ29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0L2Fzc2V0LWNhcmQvYXNzZXQtY2FyZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGaWxlVXBsb2FkQ29tcG9uZW50IH0gZnJvbSAnLi91cGxvYWQvZmlsZS11cGxvYWQvZmlsZS11cGxvYWQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVXBsb2FkQ29tcG9uZW50IH0gZnJvbSAnLi91cGxvYWQvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUYXNrQ2FyZFZpZXdDb21wb25lbnQgfSBmcm9tICcuL3Byb2plY3QvdGFza3MvdGFzay1jYXJkLXZpZXcvdGFzay1jYXJkLXZpZXcuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSWZyYW1lQ29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9pZnJhbWUvaWZyYW1lLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEN1c3RvbU1ldGFkYXRhRmllbGRDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1tZXRhZGF0YS1maWVsZC9jdXN0b20tbWV0YWRhdGEtZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9ybWF0VG9Mb2NhbGVQaXBlIH0gZnJvbSAnLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZURhdGVUaW1lUGlwZSB9IGZyb20gJy4vc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS1kYXRlLXRpbWUucGlwZSc7XHJcbmltcG9ydCB7IFByb2plY3RGcm9tVGVtcGxhdGVzQ29tcG9uZW50IH0gZnJvbSAnLi9wcm9qZWN0L3Byb2plY3QtZnJvbS10ZW1wbGF0ZXMvcHJvamVjdC1mcm9tLXRlbXBsYXRlcy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNcG1BdXRvQ29tcGxldGVDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlZC9jb21wb25lbnRzL21wbS1hdXRvLWNvbXBsZXRlL21wbS1hdXRvLWNvbXBsZXRlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFzc2V0RGV0YWlsc0NvbXBvbmVudCB9IGZyb20gJy4vcHJvamVjdC9hc3NldC1kZXRhaWxzL2Fzc2V0LWRldGFpbHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUWRzVHJhbnNmZXJUcmF5Q29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9xZHMtdHJhbnNmZXItdHJheS9xZHMtdHJhbnNmZXItdHJheS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vc2hhcmVkL2NvbXBvbmVudHMvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRG93bmxvYWRzVHJhbnNmZXJUcmF5Q29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9kb3dubG9hZHMtdHJhbnNmZXItdHJheS9kb3dubG9hZHMtdHJhbnNmZXItdHJheS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBEb3dubG9hZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vc2hhcmVkL2NvbXBvbmVudHMvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXNzZXRQcmV2aWV3Q29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9hc3NldC1wcmV2aWV3L2Fzc2V0LXByZXZpZXcuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9lbWFpbC1ub3RpZmljYXRpb24tcHJlZmVyZW5jZS9lbWFpbC1ub3RpZmljYXRpb24tcHJlZmVyZW5jZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBSZXNvdXJjZU1hbmFnZW1lbnRUb29sYmFyQ29tcG9uZW50IH0gZnJvbSAnLi9yZXNvdXJjZS1tYW5hZ2VtZW50L3Jlc291cmNlLW1hbmFnZW1lbnQtdG9vbGJhci9yZXNvdXJjZS1tYW5hZ2VtZW50LXRvb2xiYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRXhwb3J0RGF0YUNvbXBvbmVudCB9IGZyb20gJy4vc2hhcmVkL2NvbXBvbmVudHMvZXhwb3J0LWRhdGEvZXhwb3J0LWRhdGEuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRXhwb3J0RGF0YVRyYXlDb21wb25lbnQgfSBmcm9tICcuL3NoYXJlZC9jb21wb25lbnRzL2V4cG9ydC1kYXRhLXRyYXkvZXhwb3J0LWRhdGEtdHJheS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBFeHBvcnREYXRhVHJheU1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9zaGFyZWQvY29tcG9uZW50cy9leHBvcnQtZGF0YS10cmF5LW1vZGFsL2V4cG9ydC1kYXRhLXRyYXktbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50IH0gZnJvbSAnLi9yZXNvdXJjZS1tYW5hZ2VtZW50L3Jlc291cmNlLW1hbmFnZW1lbnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3luY2hyb25pemVUYXNrRmlsdGVyc0NvbXBvbmVudCB9IGZyb20gJy4vcmVzb3VyY2UtbWFuYWdlbWVudC9zeW5jaHJvbml6ZS10YXNrLWZpbHRlcnMvc3luY2hyb25pemUtdGFzay1maWx0ZXJzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFJlc291cmNlTWFuYWdlbWVudFJlc291cmNlQWxsb2NhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vcmVzb3VyY2UtbWFuYWdlbWVudC9yZXNvdXJjZS1tYW5hZ2VtZW50LXJlc291cmNlLWFsbG9jYXRpb24vcmVzb3VyY2UtbWFuYWdlbWVudC1yZXNvdXJjZS1hbGxvY2F0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFzc2V0TGlzdENvbXBvbmVudCB9IGZyb20gJy4vcHJvamVjdC9hc3NldC1saXN0L2Fzc2V0LWxpc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgR2V0UHJvcGVydHlQaXBlIH0gZnJvbSAnLi9zaGFyZWQvcGlwZS9nZXQtcHJvcGVydHkucGlwZSc7XHJcblxyXG5cclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgTXBtTGlicmFyeUNvbXBvbmVudCxcclxuICAgICAgICBOb3RpZmljYXRpb25Db21wb25lbnQsXHJcbiAgICAgICAgTG9hZGVyQ29tcG9uZW50LFxyXG4gICAgICAgIFByb2plY3RPdmVydmlld0NvbXBvbmVudCxcclxuICAgICAgICBDdXN0b21TZWFyY2hGaWVsZENvbXBvbmVudCxcclxuICAgICAgICBDdXN0b21NZXRhZGF0YUZpZWxkQ29tcG9uZW50LFxyXG4gICAgICAgIENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LFxyXG4gICAgICAgIEF1dG9mb2N1c0RpcmVjdGl2ZSxcclxuICAgICAgICBDb3B5Q2xpcGJvYXJkRGlyZWN0aXZlLFxyXG4gICAgICAgIERpc2FibGVDb250cm9sRGlyZWN0aXZlLFxyXG4gICAgICAgIFRhc2tDcmVhdGlvbkNvbXBvbmVudCxcclxuICAgICAgICBEZWxpdmVyYWJsZUNyZWF0aW9uQ29tcG9uZW50LFxyXG4gICAgICAgIEFzc2V0c0NvbXBvbmVudCxcclxuICAgICAgICBBc3NldENhcmRDb21wb25lbnQsXHJcbiAgICAgICAgQXNzZXREZXRhaWxzQ29tcG9uZW50LFxyXG4gICAgICAgIEZpbGVVcGxvYWRDb21wb25lbnQsXHJcbiAgICAgICAgVXBsb2FkQ29tcG9uZW50LFxyXG4gICAgICAgIFRhc2tDYXJkVmlld0NvbXBvbmVudCxcclxuICAgICAgICBJZnJhbWVDb21wb25lbnQsXHJcbiAgICAgICAgRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIEZvcm1hdFRvTG9jYWxlRGF0ZVRpbWVQaXBlLFxyXG4gICAgICAgIFByb2plY3RGcm9tVGVtcGxhdGVzQ29tcG9uZW50LFxyXG4gICAgICAgIE1wbUF1dG9Db21wbGV0ZUNvbXBvbmVudCxcclxuICAgICAgICBRZHNUcmFuc2ZlclRyYXlDb21wb25lbnQsXHJcbiAgICAgICAgUWRzVHJhbnNmZXJUcmF5TW9kYWxDb21wb25lbnQsXHJcbiAgICAgICAgRG93bmxvYWRzVHJhbnNmZXJUcmF5Q29tcG9uZW50LFxyXG4gICAgICAgIERvd25sb2Fkc1RyYW5zZmVyVHJheU1vZGFsQ29tcG9uZW50LFxyXG4gICAgICAgIEFzc2V0UHJldmlld0NvbXBvbmVudCxcclxuICAgICAgICBFbWFpbE5vdGlmaWNhdGlvblByZWZlcmVuY2VDb21wb25lbnQsXHJcbiAgICAgICAgUmVzb3VyY2VNYW5hZ2VtZW50VG9vbGJhckNvbXBvbmVudCxcclxuICAgICAgICBFeHBvcnREYXRhQ29tcG9uZW50LFxyXG4gICAgICAgIEV4cG9ydERhdGFUcmF5Q29tcG9uZW50LFxyXG4gICAgICAgIEV4cG9ydERhdGFUcmF5TW9kYWxDb21wb25lbnQsXHJcbiAgICAgICAgUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50LFxyXG4gICAgICAgIFN5bmNocm9uaXplVGFza0ZpbHRlcnNDb21wb25lbnQsXHJcbiAgICAgICAgUmVzb3VyY2VNYW5hZ2VtZW50UmVzb3VyY2VBbGxvY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgICAgIEFzc2V0TGlzdENvbXBvbmVudCxcclxuICAgICAgICBHZXRQcm9wZXJ0eVBpcGVcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIFBhZ2luYXRpb25Nb2R1bGUsXHJcbiAgICAgICAgTG9naW5Nb2R1bGUsXHJcbiAgICAgICAgU2VhcmNoTW9kdWxlLFxyXG4gICAgICAgIFNoYXJlQXNzZXRzTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIE1wbUxpYnJhcnlDb21wb25lbnQsXHJcbiAgICAgICAgTm90aWZpY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgICAgIExvYWRlckNvbXBvbmVudCxcclxuICAgICAgICBQcm9qZWN0T3ZlcnZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgQXV0b2ZvY3VzRGlyZWN0aXZlLFxyXG4gICAgICAgIENvcHlDbGlwYm9hcmREaXJlY3RpdmUsXHJcbiAgICAgICAgRGlzYWJsZUNvbnRyb2xEaXJlY3RpdmUsXHJcbiAgICAgICAgVGFza0NyZWF0aW9uQ29tcG9uZW50LFxyXG4gICAgICAgIERlbGl2ZXJhYmxlQ3JlYXRpb25Db21wb25lbnQsXHJcbiAgICAgICAgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsXHJcbiAgICAgICAgQXNzZXRzQ29tcG9uZW50LFxyXG4gICAgICAgIEFzc2V0Q2FyZENvbXBvbmVudCxcclxuICAgICAgICBBc3NldERldGFpbHNDb21wb25lbnQsXHJcbiAgICAgICAgVGFza0NhcmRWaWV3Q29tcG9uZW50LFxyXG4gICAgICAgIElmcmFtZUNvbXBvbmVudCxcclxuICAgICAgICBQcm9qZWN0RnJvbVRlbXBsYXRlc0NvbXBvbmVudCxcclxuICAgICAgICBRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCxcclxuICAgICAgICBEb3dubG9hZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCxcclxuICAgICAgICBBc3NldFByZXZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgQ3VzdG9tU2VhcmNoRmllbGRDb21wb25lbnQsXHJcbiAgICAgICAgQ3VzdG9tTWV0YWRhdGFGaWVsZENvbXBvbmVudCxcclxuICAgICAgICBNcG1BdXRvQ29tcGxldGVDb21wb25lbnQsXHJcbiAgICAgICAgRW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50LFxyXG4gICAgICAgIEV4cG9ydERhdGFDb21wb25lbnQsXHJcbiAgICAgICAgRXhwb3J0RGF0YVRyYXlDb21wb25lbnQsXHJcbiAgICAgICAgRXhwb3J0RGF0YVRyYXlNb2RhbENvbXBvbmVudCxcclxuICAgICAgICBBc3NldExpc3RDb21wb25lbnQsXHJcbiAgICAgICAgR2V0UHJvcGVydHlQaXBlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIEZvcm1hdFRvTG9jYWxlRGF0ZVRpbWVQaXBlLFxyXG4gICAgICAgIFRpdGxlQ2FzZVBpcGUsXHJcbiAgICAgICAgR2V0UHJvcGVydHlQaXBlXHJcbiAgICBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE1wbUxpYnJhcnlNb2R1bGUgeyB9XHJcbiJdfQ==