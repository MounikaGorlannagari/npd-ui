import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
var SearchChangeEventService = /** @class */ (function () {
    function SearchChangeEventService() {
        this.currentSearchViewConfig = new BehaviorSubject(null);
        this.onSearchViewChange = this.currentSearchViewConfig.asObservable();
        this.viewToggleSearch = new BehaviorSubject(null);
        this.onViewToggleSearch = this.viewToggleSearch.asObservable();
    }
    SearchChangeEventService.prototype.update = function (viewInfo) {
        this.activeSearchId = viewInfo ? viewInfo.R_PO_ADVANCED_SEARCH_CONFIG : null;
        this.currentSearchViewConfig.next(viewInfo);
    };
    SearchChangeEventService.prototype.setAdvancedSearchConfig = function (advancedSearchConfig) {
        this.advancedSearchConfig = advancedSearchConfig;
    };
    SearchChangeEventService.prototype.getAdvancedSearchConfigs = function () {
        return this.advancedSearchConfig ? this.advancedSearchConfig : [];
    };
    SearchChangeEventService.prototype.getAdvancedSearchConfigByName = function (searchIdentifier) {
        if (!searchIdentifier) {
            return;
        }
        // check this
        var currConfig = this.getAdvancedSearchConfigs().find(function (serachConfig) {
            return serachConfig['MPM_Advanced_Search_Config-id'].Id === searchIdentifier;
        });
        if (currConfig) {
            var configId = 0;
            try {
                configId = parseInt(currConfig['MPM_Advanced_Search_Config-id'].Id, 0);
            }
            catch (e) {
                configId = -1;
            }
            return configId;
        }
        return -1;
    };
    SearchChangeEventService.prototype.getFacetConfigIdForActiveSearch = function () {
        var _this = this;
        if (!this.activeSearchId) {
            return;
        }
        // Change it for facet
        var advConfig = this.getAdvancedSearchConfigs().find(function (serachConfig) {
            return serachConfig['MPM_Advanced_Search_Config-id'].Id === _this.activeSearchId;
        });
        if (!advConfig) {
            return;
        }
        if (advConfig.facetConfigId) {
            return advConfig.facetConfigId;
        }
        return;
    };
    SearchChangeEventService.prototype.updateOnViewChange = function (updateAction) {
        this.viewToggleSearch.next(updateAction);
    };
    SearchChangeEventService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchChangeEventService_Factory() { return new SearchChangeEventService(); }, token: SearchChangeEventService, providedIn: "root" });
    SearchChangeEventService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], SearchChangeEventService);
    return SearchChangeEventService;
}());
export { SearchChangeEventService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNoYW5nZS1ldmVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2VhcmNoL3NlcnZpY2VzL3NlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBd0IsTUFBTSxlQUFlLENBQUM7QUFDakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFPdkM7SUFVRTtRQVBPLDRCQUF1QixHQUFHLElBQUksZUFBZSxDQUFhLElBQUksQ0FBQyxDQUFDO1FBQ3ZFLHVCQUFrQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUUxRCxxQkFBZ0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRCx1QkFBa0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFHMUMsQ0FBQztJQUVqQix5Q0FBTSxHQUFOLFVBQU8sUUFBb0I7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzdFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELDBEQUF1QixHQUF2QixVQUF3QixvQkFBb0I7UUFDMUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLG9CQUFvQixDQUFDO0lBQ25ELENBQUM7SUFFRCwyREFBd0IsR0FBeEI7UUFDRSxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDcEUsQ0FBQztJQUVELGdFQUE2QixHQUE3QixVQUE4QixnQkFBZ0I7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3JCLE9BQU87U0FDUjtRQUNELGFBQWE7UUFDYixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxZQUFZO1lBQ2xFLE9BQU8sWUFBWSxDQUFDLCtCQUErQixDQUFDLENBQUMsRUFBRSxLQUFLLGdCQUFnQixDQUFDO1FBQy9FLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxVQUFVLEVBQUU7WUFDZCxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7WUFDakIsSUFBSTtnQkFDRixRQUFRLEdBQUcsUUFBUSxDQUFDLFVBQVUsQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUN4RTtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQzthQUNmO1lBQ0QsT0FBTyxRQUFRLENBQUM7U0FDakI7UUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVELGtFQUErQixHQUEvQjtRQUFBLGlCQWVDO1FBZEMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDeEIsT0FBTztTQUNSO1FBQ0Qsc0JBQXNCO1FBQ3RCLElBQU0sU0FBUyxHQUE0QixJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxZQUFZO1lBQzFGLE9BQU8sWUFBWSxDQUFDLCtCQUErQixDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxjQUFjLENBQUM7UUFDbEYsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsT0FBTztTQUNSO1FBQ0QsSUFBSSxTQUFTLENBQUMsYUFBYSxFQUFFO1lBQzNCLE9BQU8sU0FBUyxDQUFDLGFBQWEsQ0FBQztTQUNoQztRQUNELE9BQU87SUFDVCxDQUFDO0lBRUQscURBQWtCLEdBQWxCLFVBQW1CLFlBQThCO1FBQy9DLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7SUFoRVUsd0JBQXdCO1FBSHBDLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyx3QkFBd0IsQ0FpRXBDO21DQXpFRDtDQXlFQyxBQWpFRCxJQWlFQztTQWpFWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2VhcmNoRXZlbnRNb2RhbCB9IGZyb20gJy4uL29iamVjdHMvc2VhcmNoLWV2ZW50LW1vZGFsJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUFkdmFuY2VkU2VhcmNoQ29uZmlnJztcclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlIHtcclxuXHJcbiAgcHVibGljIGFjdGl2ZVNlYXJjaElkO1xyXG4gIHB1YmxpYyBjdXJyZW50U2VhcmNoVmlld0NvbmZpZyA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Vmlld0NvbmZpZz4obnVsbCk7XHJcbiAgb25TZWFyY2hWaWV3Q2hhbmdlID0gdGhpcy5jdXJyZW50U2VhcmNoVmlld0NvbmZpZy5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgcHVibGljIHZpZXdUb2dnbGVTZWFyY2ggPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gIG9uVmlld1RvZ2dsZVNlYXJjaCA9IHRoaXMudmlld1RvZ2dsZVNlYXJjaC5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgcHVibGljIGFkdmFuY2VkU2VhcmNoQ29uZmlnOiBBcnJheTxhbnk+O1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHVwZGF0ZSh2aWV3SW5mbzogVmlld0NvbmZpZykge1xyXG4gICAgdGhpcy5hY3RpdmVTZWFyY2hJZCA9IHZpZXdJbmZvID8gdmlld0luZm8uUl9QT19BRFZBTkNFRF9TRUFSQ0hfQ09ORklHIDogbnVsbDtcclxuICAgIHRoaXMuY3VycmVudFNlYXJjaFZpZXdDb25maWcubmV4dCh2aWV3SW5mbyk7XHJcbiAgfVxyXG5cclxuICBzZXRBZHZhbmNlZFNlYXJjaENvbmZpZyhhZHZhbmNlZFNlYXJjaENvbmZpZykge1xyXG4gICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IGFkdmFuY2VkU2VhcmNoQ29uZmlnO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdzKCk6IEFycmF5PE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnPiB7XHJcbiAgICByZXR1cm4gdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA/IHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWcgOiBbXTtcclxuICB9XHJcblxyXG4gIGdldEFkdmFuY2VkU2VhcmNoQ29uZmlnQnlOYW1lKHNlYXJjaElkZW50aWZpZXIpOiBudW1iZXIge1xyXG4gICAgaWYgKCFzZWFyY2hJZGVudGlmaWVyKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIC8vIGNoZWNrIHRoaXNcclxuICAgIGNvbnN0IGN1cnJDb25maWcgPSB0aGlzLmdldEFkdmFuY2VkU2VhcmNoQ29uZmlncygpLmZpbmQoc2VyYWNoQ29uZmlnID0+IHtcclxuICAgICAgcmV0dXJuIHNlcmFjaENvbmZpZ1snTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZCA9PT0gc2VhcmNoSWRlbnRpZmllcjtcclxuICAgIH0pO1xyXG4gICAgaWYgKGN1cnJDb25maWcpIHtcclxuICAgICAgbGV0IGNvbmZpZ0lkID0gMDtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICBjb25maWdJZCA9IHBhcnNlSW50KGN1cnJDb25maWdbJ01QTV9BZHZhbmNlZF9TZWFyY2hfQ29uZmlnLWlkJ10uSWQsIDApO1xyXG4gICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgY29uZmlnSWQgPSAtMTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gY29uZmlnSWQ7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gLTE7XHJcbiAgfVxyXG5cclxuICBnZXRGYWNldENvbmZpZ0lkRm9yQWN0aXZlU2VhcmNoKCk6IHN0cmluZyB7XHJcbiAgICBpZiAoIXRoaXMuYWN0aXZlU2VhcmNoSWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gQ2hhbmdlIGl0IGZvciBmYWNldFxyXG4gICAgY29uc3QgYWR2Q29uZmlnOiBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyA9IHRoaXMuZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdzKCkuZmluZChzZXJhY2hDb25maWcgPT4ge1xyXG4gICAgICByZXR1cm4gc2VyYWNoQ29uZmlnWydNUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy1pZCddLklkID09PSB0aGlzLmFjdGl2ZVNlYXJjaElkO1xyXG4gICAgfSk7XHJcbiAgICBpZiAoIWFkdkNvbmZpZykge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoYWR2Q29uZmlnLmZhY2V0Q29uZmlnSWQpIHtcclxuICAgICAgcmV0dXJuIGFkdkNvbmZpZy5mYWNldENvbmZpZ0lkO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlT25WaWV3Q2hhbmdlKHVwZGF0ZUFjdGlvbjogU2VhcmNoRXZlbnRNb2RhbCkge1xyXG4gICAgdGhpcy52aWV3VG9nZ2xlU2VhcmNoLm5leHQodXBkYXRlQWN0aW9uKTtcclxuICB9XHJcbn1cclxuIl19