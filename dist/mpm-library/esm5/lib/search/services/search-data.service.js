import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
var SearchDataService = /** @class */ (function () {
    function SearchDataService() {
        this.searchDataSubject = new BehaviorSubject([]);
        this.searchData = this.searchDataSubject.asObservable();
        this.KEYWORD_MAPPER = 'MPM.FIELD.ALL_KEYWORD';
    }
    SearchDataService.prototype.setSearchData = function (data) {
        this.searchDataSubject.next(data);
    };
    SearchDataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchDataService_Factory() { return new SearchDataService(); }, token: SearchDataService, providedIn: "root" });
    SearchDataService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], SearchDataService);
    return SearchDataService;
}());
export { SearchDataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWRhdGEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZXJ2aWNlcy9zZWFyY2gtZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7O0FBTXZDO0lBS0k7UUFITyxzQkFBaUIsR0FBRyxJQUFJLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNuRCxlQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzVDLG1CQUFjLEdBQUcsdUJBQXVCLENBQUM7SUFDaEMsQ0FBQztJQUVqQix5Q0FBYSxHQUFiLFVBQWMsSUFBVztRQUNyQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3RDLENBQUM7O0lBVFEsaUJBQWlCO1FBSjdCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxpQkFBaUIsQ0FXN0I7NEJBbEJEO0NBa0JDLEFBWEQsSUFXQztTQVhZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hEYXRhU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHNlYXJjaERhdGFTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdChbXSk7XHJcbiAgICBzZWFyY2hEYXRhID0gdGhpcy5zZWFyY2hEYXRhU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAgIHB1YmxpYyBLRVlXT1JEX01BUFBFUiA9ICdNUE0uRklFTEQuQUxMX0tFWVdPUkQnO1xyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICBzZXRTZWFyY2hEYXRhKGRhdGE6IGFueVtdKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hEYXRhU3ViamVjdC5uZXh0KGRhdGEpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=