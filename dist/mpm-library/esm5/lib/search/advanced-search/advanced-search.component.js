import { __decorate, __param, __values } from "tslib";
import { Component, Inject, HostListener } from '@angular/core';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SearchSaveService } from '../../project/shared/services/search.save.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { DatePipe } from '@angular/common';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { MPMSearchOperators } from '../../../lib/shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../../lib/shared/services/indexer/constants/MPMSearchOperatorName.constants';
var AdvancedSearchComponent = /** @class */ (function () {
    function AdvancedSearchComponent(dialogRef, data, loaderService, notificationService, searchSaveService, formatToLocalePipe, sharingService, searchConfigService, fieldConfigService, datePipe) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.searchSaveService = searchSaveService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.sharingService = sharingService;
        this.searchConfigService = searchConfigService;
        this.fieldConfigService = fieldConfigService;
        this.datePipe = datePipe;
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        this.filterRowList = [];
        this.searchTobeSavedName = '';
        this.keywords = [];
        this.allSearchFields = [];
        this.availableSearchFields = [];
    }
    AdvancedSearchComponent.prototype.keyEvent = function (event) {
        if (event.key === 'Enter') {
            this.close(false);
        }
    };
    AdvancedSearchComponent.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add the keyword
        value = (value || '').trim();
        if (value) {
            this.keywords.push({ name: value });
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
    };
    AdvancedSearchComponent.prototype.remove = function (keyword) {
        var index = this.keywords.indexOf(keyword);
        if (index >= 0) {
            this.keywords.splice(index, 1);
        }
    };
    AdvancedSearchComponent.prototype.close = function (data) {
        if (data && data !== true) {
            this.sharingService.setRecentSearch(data);
        }
        if (data === true && this.data && this.data.recentSearch) {
            this.data.recentSearch.map(function (rowData) {
                if (rowData.isEdit) {
                    rowData.isEdit = false;
                }
            });
        }
        this.dialogRef.close(data);
    };
    AdvancedSearchComponent.prototype.filterSelectionChange = function (event) {
        this.setAvialbleSearchFiels();
    };
    AdvancedSearchComponent.prototype.getFieldDetails = function (fieldId) {
        return this.data.advancedSearchConfigData.metadataFields.filter(function (field) {
            return field['MPM_Fields_Config-id'].Id === fieldId;
        });
    };
    // to remove all fields
    AdvancedSearchComponent.prototype.clearFilter = function () {
        this.filterRowList = [];
        // this.addFilter();
        this.setAvialbleSearchFiels();
    };
    // to remove selected field
    AdvancedSearchComponent.prototype.removeFilter = function (event) {
        if (event && event.rowId) {
            this.filterRowList = this.filterRowList.filter(function (rowData) {
                return rowData.rowId !== event.rowId;
            });
        }
        this.setAvialbleSearchFiels();
    };
    // to add new field
    AdvancedSearchComponent.prototype.addFilter = function () {
        var canAddNewFilter = true;
        this.filterRowList.forEach(function (rowData) {
            if (rowData && !rowData.isFilterSelected) {
                canAddNewFilter = false;
            }
        });
        if (canAddNewFilter) {
            if (this.availableSearchFields.length > 0) {
                var count = this.filterRowList.length + 1;
                this.filterRowList.push({
                    rowId: count,
                    searchField: '',
                    searchOperatorId: '',
                    searchOperatorName: '',
                    searchValue: '',
                    searchSecondValue: '',
                    issearchValueRequired: true,
                    isFilterSelected: false,
                    hasTwoValues: '',
                    availableSearchFields: this.availableSearchFields
                });
            }
            else {
                this.notificationService.info('No more filters to add');
                return;
            }
        }
        else {
            this.notificationService.info('Kindly select the previous filter first');
        }
    };
    AdvancedSearchComponent.prototype.validateForSearch = function () {
        var allFilterFieldSelected = true;
        var isValid = true;
        var emptyValue = false;
        this.filterRowList.forEach(function (filter) {
            var searchField = filter.availableSearchFields.find(function (data) { return (filter.searchField === data.MAPPER_NAME || filter.searchField === data.MPM_FIELD_CONFIG_ID); });
            if (!searchField.IS_DROP_DOWN) {
                if (filter.searchField) {
                    if (filter.searchOperatorId) {
                        if (filter.issearchValueRequired) {
                            if (filter.searchValue) {
                                if (filter.hasTwoValues && !filter.searchSecondValue) {
                                    isValid = false;
                                }
                            }
                            else {
                                isValid = false;
                            }
                        }
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    isValid = false;
                }
            }
            if (typeof (filter.searchValue) === 'string' && filter.searchValue.trim() === '' && filter.issearchValueRequired) {
                isValid = false;
                emptyValue = true;
            }
            if (!isValid || !filter.isFilterSelected) {
                allFilterFieldSelected = false;
            }
        });
        if (emptyValue) {
            this.notificationService.info('Kindly enter the value to be searched');
            return false;
        }
        else if (!allFilterFieldSelected) {
            this.notificationService.info('Kindly select all field');
            return false;
        }
        else {
            return true;
        }
    };
    AdvancedSearchComponent.prototype.converToLocalDate = function (date) {
        var isoStringToLocale = acronui.getDateWithLocaleTimeZone(date);
        return this.formatToLocalePipe.transform(isoStringToLocale, 'MM/dd/yyyy');
    };
    AdvancedSearchComponent.prototype.saveSearch = function (searchConditions, viewName) {
        var _this = this;
        if (!this.searchTobeSavedName) {
            this.notificationService.info('Kindly add search name');
            return;
        }
        if (this.searchTobeSavedName.length > 64) {
            this.notificationService.info('Search name cannot be more than 64 characters');
            return;
        }
        var hasDuplicateSearchName = false;
        this.data.availableSavedSearch.forEach(function (savedSearch) {
            if ((savedSearch.NAME).toLowerCase() === (_this.searchTobeSavedName).toLowerCase()) {
                hasDuplicateSearchName = true;
            }
        });
        if (hasDuplicateSearchName && (!this.data.savedSearch || (typeof (this.data.savedSearch) === 'object') && this.data.savedSearch.NAME !== this.searchTobeSavedName)) { // || typeof (this.data.savedSearch) === 'object')
            this.notificationService.error('Search Name already exists, Kindly change the name');
            return;
        }
        var searchConditionList = {
            search_condition_list: {
                search_condition: searchConditions
            }
        };
        var savedSearchId = (this.data.savedSearch && this.data.savedSearch['MPM_Saved_Searches-id'] &&
            this.data.savedSearch['MPM_Saved_Searches-id'].Id) ? this.data.savedSearch['MPM_Saved_Searches-id'].Id : '';
        this.loaderService.show();
        if (savedSearchId) {
            this.searchSaveService.updateSavedSearch(savedSearchId, this.searchTobeSavedName, this.searchTobeSavedName, searchConditionList, false, viewName).subscribe(function (response) {
                _this.loaderService.hide();
                var data = searchConditions;
                data[0].NAME = response['MPM_Saved_Searches'].NAME;
                _this.close(data);
            }, function (error) {
                _this.loaderService.hide();
                _this.notificationService.error('Error while saving search');
            });
        }
        else {
            this.searchSaveService.saveSearch(this.searchTobeSavedName, this.searchTobeSavedName, searchConditionList, false, viewName).subscribe(function (response) {
                _this.loaderService.hide();
                var data = searchConditions;
                data[0].NAME = response['MPM_Saved_Searches'].NAME;
                _this.close(data);
            }, function (error) {
                _this.loaderService.hide();
                _this.notificationService.error('Error while saving search');
            });
        }
    };
    AdvancedSearchComponent.prototype.search = function (isSavedSearch) {
        var _this = this;
        if (this.validateForSearch()) {
            var searchConditions_1 = [];
            this.filterRowList.forEach(function (rowData) {
                if (rowData.searchValue && rowData.searchValue instanceof Date) {
                    rowData.searchValue = _this.datePipe.transform(new Date(rowData.searchValue), 'yyyy-MM-dd').concat('T00:00:00Z');
                    if (rowData.searchSecondValue && rowData.searchSecondValue instanceof Date) {
                        rowData.searchSecondValue = _this.datePipe.transform(new Date(rowData.searchSecondValue), 'yyyy-MM-dd').concat('T00:00:00Z');
                    }
                }
                // rowData.availableSearchFields = [];
                var currMPMField = _this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MPM_FIELD_CONFIG_ID, rowData.searchField);
                var fieldId = currMPMField ? _this.fieldConfigService.formIndexerIdFromField(currMPMField) : rowData.searchField;
                if (rowData.isComboType) {
                    var values = rowData.searchValue;
                    var condidtion = {
                        isAdvancedSearch: true,
                        searchIdentifier: _this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG,
                        field_id: fieldId,
                        relational_operator_id: rowData.searchOperatorId ? rowData.searchOperatorId : MPMSearchOperators.IS,
                        relational_operator_name: rowData.searchOperatorName ? rowData.searchOperatorName : MPMSearchOperatorNames.IS,
                        type: currMPMField ? currMPMField.DATA_TYPE : 'string',
                        mapper_field_id: rowData.searchField,
                        mapper_name: currMPMField.MAPPER_NAME,
                        view: _this.data.advancedSearchConfigData.searchIdentifier.VIEW
                    };
                    if (values.length === 1) {
                        var conditionCopy = Object.assign({}, condidtion);
                        conditionCopy.filterRowData = rowData;
                        conditionCopy.left_paren = '(';
                        conditionCopy.right_paren = ')';
                        conditionCopy.display_value = values[0].display_value;
                        conditionCopy.value = values[0].field_value.value;
                        conditionCopy.relational_operator = 'and';
                        searchConditions_1.push(conditionCopy);
                    }
                    else {
                        var lastValueIndex = values.length - 1;
                        for (var i = 0; i < values.length; i++) {
                            var conditionCopy = Object.assign({}, condidtion);
                            conditionCopy.display_value = values[i].display_value;
                            conditionCopy.value = values[i].field_value.value;
                            if (i === 0) {
                                conditionCopy.left_paren = '(';
                                conditionCopy.filterRowData = rowData;
                            }
                            else if (i === lastValueIndex) {
                                conditionCopy.right_paren = ')';
                                conditionCopy.relational_operator = 'or';
                            }
                            else {
                                conditionCopy.relational_operator = 'or';
                            }
                            searchConditions_1.push(conditionCopy);
                        }
                    }
                }
                else {
                    var value = rowData.searchValue;
                    if (rowData.searchSecondValue) {
                        // if (rowData.searchSecondValue instanceof Date && rowData.issearchValueRequired) {
                        //   rowData.searchSecondValue = this.converToLocalDate(rowData.searchSecondValue);
                        // }
                        /* if (rowData.searchSecondValue instanceof Date) {
                          // value = value.toISOString() + ' and ' + rowData.searchSecondValue.toISOString();
                          value = this.datePipe.transform(new Date(rowData.searchValue), 'yyyy-MM-dd').concat('T00:00:00Z') + ' and ' +
                                  this.datePipe.transform(new Date(rowData.searchSecondValue), 'yyyy-MM-dd').concat('T00:00:00Z')
                        } else { */
                        value = value + ' and ' + rowData.searchSecondValue;
                        // }
                    }
                    var condition = {
                        isAdvancedSearch: true,
                        searchIdentifier: _this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG,
                        filterRowData: rowData,
                        field_id: fieldId,
                        type: currMPMField ? currMPMField.DATA_TYPE : 'string',
                        value: value,
                        relational_operator_id: rowData.searchOperatorId ? rowData.searchOperatorId : MPMSearchOperators.IS,
                        relational_operator_name: rowData.searchOperatorName ? rowData.searchOperatorName : MPMSearchOperatorNames.IS,
                        relational_operator: 'and',
                        mapper_field_id: rowData.searchField,
                        mapper_name: currMPMField.MAPPER_NAME,
                        view: _this.data.advancedSearchConfigData.searchIdentifier.VIEW
                    };
                    searchConditions_1.push(condition);
                }
            });
            if (isSavedSearch) {
                if (this.data && this.data.recentSearch) {
                    this.data.recentSearch.map(function (rowData) {
                        if (rowData.isEdit) {
                            rowData.isEdit = false;
                        }
                    });
                }
                this.saveSearch(searchConditions_1, this.data.advancedSearchConfigData.searchIdentifier.VIEW);
            }
            else {
                this.close(searchConditions_1);
            }
        }
    };
    AdvancedSearchComponent.prototype.isFieldAlreadySelected = function (fieldId) {
        if (!fieldId) {
            return true;
        }
        return this.filterRowList.find(function (row) {
            return row.searchField === fieldId;
        });
    };
    AdvancedSearchComponent.prototype.setAvialbleSearchFiels = function () {
        var e_1, _a;
        this.availableSearchFields = [];
        if (!Array.isArray(this.allSearchFields)) {
            return;
        }
        try {
            for (var _b = __values(this.allSearchFields), _c = _b.next(); !_c.done; _c = _b.next()) {
                var field = _c.value;
                if (field && field.MPM_FIELD_CONFIG_ID &&
                    !this.isFieldAlreadySelected(field.MPM_FIELD_CONFIG_ID)) {
                    this.availableSearchFields.push(field);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        // this.advancedSearchConfigData.availableSearchFields = this.availableSearchFields;
    };
    AdvancedSearchComponent.prototype.mapSeachConfig = function () {
        var _this = this;
        if (this.data.savedSearch) {
            this.searchTobeSavedName = this.data.savedSearch.NAME;
            var conditions = [];
            if (this.data.savedSearch.SEARCH_DATA &&
                this.data.savedSearch.SEARCH_DATA.search_condition_list &&
                this.data.savedSearch.SEARCH_DATA.search_condition_list.search_condition) {
                conditions = this.data.savedSearch.SEARCH_DATA.search_condition_list.search_condition;
            }
            if (conditions && conditions[0].searchIdentifier) {
                var advConfig = this.searchConfigService.getAdvancedSearchConfigById(conditions[0].searchIdentifier);
                this.allSearchFields = advConfig.R_PM_Search_Fields;
                conditions.forEach(function (condition) {
                    var count = _this.filterRowList.length + 1;
                    _this.setAvialbleSearchFiels();
                    var value = condition.value != null ? condition.value : '';
                    /* this.filterRowList.push({
                      rowId: count,
                      searchField: condition.mapper_field_id,
                      searchOperatorId: condition.relational_operator_id,
                      searchOperatorName: condition.relational_operator_name,
                      searchValue: condition.value,
                      searchSecondValue: condition.value.split('and ').length > 0 ? condition.value.split('and ')[1] : '',
                      issearchValueRequired: condition.value ? true : false,
                      isFilterSelected: true,
                      hasTwoValues: '',
                      availableSearchFields: this.allSearchFields.filter(searchFieldItem => {
                        return searchFieldItem.MPM_FIELD_CONFIG_ID === condition.mapper_field_id;
                      })
                    }); */
                    _this.filterRowList.push({
                        rowId: count,
                        searchField: condition.mapper_field_id,
                        searchOperatorId: condition.relational_operator_id,
                        searchOperatorName: condition.relational_operator_name,
                        searchValue: value,
                        searchSecondValue: value.split('and ').length > 0 ? value.split('and ')[1] !== undefined ? value.split('and ')[1] : '' : '',
                        issearchValueRequired: value ? true : false,
                        isFilterSelected: true,
                        hasTwoValues: '',
                        availableSearchFields: _this.allSearchFields.filter(function (searchFieldItem) {
                            return searchFieldItem.MPM_FIELD_CONFIG_ID === condition.mapper_field_id;
                        })
                    });
                    _this.setAvialbleSearchFiels();
                });
                this.advancedSearchConfigData.availableSearchFields = this.allSearchFields;
                this.advancedSearchConfigData.searchOperatorList = this.data.advancedSearchConfigData.searchOperatorList;
            }
        }
        else {
            if (this.data.advancedSearchConfigData && this.data.advancedSearchConfigData.searchIdentifier) {
                var searchConfig = this.data.advancedSearchConfigData.advancedSearchConfiguration.filter(function (serachConfig) {
                    return serachConfig['MPM_Advanced_Search_Config-id'].Id === _this.data.advancedSearchConfigData.searchIdentifier.R_PO_ADVANCED_SEARCH_CONFIG;
                });
                // this.data.advancedSearchConfigData.searchIdentifier = searchConfig[0]['MPM_Advanced_Search_Config-id'].Id;
                this.allSearchFields = searchConfig[0].R_PM_Search_Fields;
                this.advancedSearchConfigData.availableSearchFields = searchConfig[0].R_PM_Search_Fields;
                this.advancedSearchConfigData.searchOperatorList = this.data.advancedSearchConfigData.searchOperatorList;
                this.setAvialbleSearchFiels();
                // this.addFilter();
            }
        }
    };
    AdvancedSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.advancedSearchConfigData = {
            availableSearchFields: [],
            searchOperatorList: []
        };
        this.mapSeachConfig();
        if (this.data.recentSearch) {
            this.filterRowList = [];
            this.data.recentSearch.map(function (rowData) {
                if (rowData.filterRowData) {
                    rowData.filterRowData.availableSearchFields = _this.allSearchFields.filter(function (searchFieldItem) {
                        return searchFieldItem.MPM_FIELD_CONFIG_ID === rowData.filterRowData.searchField;
                    });
                    _this.filterRowList.push(rowData.filterRowData);
                    _this.setAvialbleSearchFiels();
                }
            });
        }
    };
    AdvancedSearchComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: LoaderService },
        { type: NotificationService },
        { type: SearchSaveService },
        { type: DatePipe },
        { type: SharingService },
        { type: SearchConfigService },
        { type: FieldConfigService },
        { type: DatePipe }
    ]; };
    __decorate([
        HostListener('window:keyup', ['$event'])
    ], AdvancedSearchComponent.prototype, "keyEvent", null);
    AdvancedSearchComponent = __decorate([
        Component({
            selector: 'mpm-advanced-search',
            template: "<div class=\"advanced-search-modal\">\r\n\t<div class=\"search-header\">\r\n\t\t<button (click)=\"close(true)\" mat-icon-button color=\"primary\" matTooltip=\"Navigate back to simple search\">\r\n\t\t\t<mat-icon>keyboard_backspace</mat-icon>\r\n\t\t</button>\r\n\t\t<span>Back To Normal Search</span>\r\n\t\t<button (click)=\"close(true)\" class=\"right-align\" mat-icon-button color=\"primary\" matTooltip=\"Exit Search\">\r\n\t\t\t<mat-icon>close</mat-icon>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"field-action\">\r\n\t\t<button (click)=\"addFilter()\" mat-flat-button class=\"trySpace\" matTooltip=\"Add Filter\"\r\n\t\t\t[disabled]=\"!availableSearchFields.length\">\r\n\t\t\t<mat-icon>add</mat-icon>\r\n\t\t\tAdd Filter\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"field-options\">\r\n\t\t<span *ngFor=\"let row of filterRowList\">\r\n\t\t\t<mpm-advanced-search-field [filterRowData]=\"row\" [advancedSearchConfigData]=\"advancedSearchConfigData\"\r\n\t\t\t\t(removeFilterField)=\"removeFilter($event)\" (filterSelected)=\"filterSelectionChange($event)\">\r\n\t\t\t</mpm-advanced-search-field>\r\n\t\t</span>\r\n\t\t<div class=\"info\" *ngIf=\"!filterRowList.length\">\r\n\t\t\t<p>Click on Add Filter to start adding filters</p>\r\n\t\t</div>\r\n\t</div>\r\n\t<div>\r\n\t\t<mat-form-field appearance=\"outline\" class=\"search-name\">\r\n\t\t\t<mat-label>Search Name</mat-label>\r\n\t\t\t<input [(ngModel)]=\"searchTobeSavedName\" matInput placeholder=\"Provide a name to the Search\">\r\n\t\t</mat-form-field>\r\n\t\t<button (click)=\"search(true)\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Save and Perform Search\">Save and Search</button>\r\n\t\t<button (click)=\"search(false)\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Search\">Search</button>\r\n\t\t<button (click)=\"clearFilter()\" mat-flat-button color=\"primary\" class=\"trySpace\"\r\n\t\t\t[disabled]=\"!filterRowList.length\" matTooltip=\"Clear All\">Clear All</button>\r\n\t</div>\r\n</div>",
            styles: [".advanced-search-modal{font-size:12px!important;min-width:900px}.example-chip-list{width:100%}.trySpace{margin-right:10px}.filter-row{margin:10px;width:95%!important}.filter-field{max-height:55vh;min-height:21vh}button,mat-form-field{font-size:12px!important;margin:5px}.search-header{margin-bottom:20px}.field-action{margin-left:-20px}.search-name{width:435px}.field-options{max-height:40vh;overflow-y:auto;min-height:310px}.info{width:100%;text-align:center}.searchName{display:inline-block}.right-align{float:right}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], AdvancedSearchComponent);
    return AdvancedSearchComponent;
}());
export { AdvancedSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9hZHZhbmNlZC1zZWFyY2gvYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXhFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUczQyxPQUFPLEVBQVksWUFBWSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFFbEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNEVBQTRFLENBQUM7QUFDaEgsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZ0ZBQWdGLENBQUM7QUFXeEg7SUFhRSxpQ0FDUyxTQUFnRCxFQUN2QixJQUF3QixFQUNqRCxhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGtCQUE0QixFQUM1QixjQUE4QixFQUM5QixtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLFFBQWtCO1FBVGxCLGNBQVMsR0FBVCxTQUFTLENBQXVDO1FBQ3ZCLFNBQUksR0FBSixJQUFJLENBQW9CO1FBQ2pELGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQVU7UUFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBckIzQixZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2YsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIsd0JBQW1CLEdBQUcsRUFBRSxDQUFDO1FBRXpCLGFBQVEsR0FBYyxFQUFFLENBQUM7UUFDekIsb0JBQWUsR0FBZSxFQUFFLENBQUM7UUFDakMsMEJBQXFCLEdBQWUsRUFBRSxDQUFDO0lBYW5DLENBQUM7SUFHTCwwQ0FBUSxHQUFSLFVBQVMsS0FBb0I7UUFDM0IsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLE9BQU8sRUFBRTtZQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELHFDQUFHLEdBQUgsVUFBSSxLQUF3QjtRQUMxQixJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFFeEIsa0JBQWtCO1FBQ2xCLEtBQUssR0FBRyxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM3QixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7U0FDckM7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxLQUFLLEVBQUU7WUFDVCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUNsQjtJQUNILENBQUM7SUFFRCx3Q0FBTSxHQUFOLFVBQU8sT0FBZ0I7UUFDckIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFN0MsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQztJQUVELHVDQUFLLEdBQUwsVUFBTSxJQUFVO1FBQ2QsSUFBSSxJQUFJLElBQUksSUFBSSxLQUFLLElBQUksRUFBRTtZQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxVQUFBLE9BQU87Z0JBQ2hDLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtvQkFDbEIsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7aUJBQ3hCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRCx1REFBcUIsR0FBckIsVUFBc0IsS0FBSztRQUN6QixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsaURBQWUsR0FBZixVQUFnQixPQUFPO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSztZQUNuRSxPQUFPLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxPQUFPLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsdUJBQXVCO0lBQ3ZCLDZDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELDJCQUEyQjtJQUMzQiw4Q0FBWSxHQUFaLFVBQWEsS0FBSztRQUNoQixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBQSxPQUFPO2dCQUNwRCxPQUFPLE9BQU8sQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBQztZQUN2QyxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELG1CQUFtQjtJQUNuQiwyQ0FBUyxHQUFUO1FBQ0UsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUNoQyxJQUFJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDeEMsZUFBZSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxlQUFlLEVBQUU7WUFDbkIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDekMsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztvQkFDdEIsS0FBSyxFQUFFLEtBQUs7b0JBQ1osV0FBVyxFQUFFLEVBQUU7b0JBQ2YsZ0JBQWdCLEVBQUUsRUFBRTtvQkFDcEIsa0JBQWtCLEVBQUUsRUFBRTtvQkFDdEIsV0FBVyxFQUFFLEVBQUU7b0JBQ2YsaUJBQWlCLEVBQUUsRUFBRTtvQkFDckIscUJBQXFCLEVBQUUsSUFBSTtvQkFDM0IsZ0JBQWdCLEVBQUUsS0FBSztvQkFDdkIsWUFBWSxFQUFFLEVBQUU7b0JBQ2hCLHFCQUFxQixFQUFFLElBQUksQ0FBQyxxQkFBcUI7aUJBQ2xELENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEQsT0FBTzthQUNSO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMseUNBQXlDLENBQUMsQ0FBQztTQUMxRTtJQUNILENBQUM7SUFFRCxtREFBaUIsR0FBakI7UUFDRSxJQUFJLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTtZQUMvQixJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxNQUFNLENBQUMsV0FBVyxLQUFLLElBQUksQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxJQUFJLENBQUMsbUJBQW1CLENBQUMsRUFBNUYsQ0FBNEYsQ0FBQyxDQUFDO1lBQzVKLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFO2dCQUM3QixJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUU7b0JBQ3RCLElBQUksTUFBTSxDQUFDLGdCQUFnQixFQUFFO3dCQUMzQixJQUFJLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRTs0QkFDaEMsSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFO2dDQUN0QixJQUFJLE1BQU0sQ0FBQyxZQUFZLElBQUksQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUU7b0NBQ3BELE9BQU8sR0FBRyxLQUFLLENBQUM7aUNBQ2pCOzZCQUNGO2lDQUFNO2dDQUNMLE9BQU8sR0FBRyxLQUFLLENBQUM7NkJBQ2pCO3lCQUNGO3FCQUNGO3lCQUFNO3dCQUNMLE9BQU8sR0FBRyxLQUFLLENBQUM7cUJBQ2pCO2lCQUNGO3FCQUFNO29CQUNMLE9BQU8sR0FBRyxLQUFLLENBQUM7aUJBQ2pCO2FBQ0Y7WUFFRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssUUFBUSxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRTtnQkFDaEgsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsVUFBVSxHQUFHLElBQUksQ0FBQzthQUNuQjtZQUdELElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3hDLHNCQUFzQixHQUFHLEtBQUssQ0FBQzthQUNoQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxVQUFVLEVBQUU7WUFDZCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHVDQUF1QyxDQUFDLENBQUM7WUFDdkUsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUNsQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDekQsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxtREFBaUIsR0FBakIsVUFBa0IsSUFBSTtRQUNwQixJQUFNLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELDRDQUFVLEdBQVYsVUFBVyxnQkFBaUMsRUFBRSxRQUFnQjtRQUE5RCxpQkFrREM7UUFqREMsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM3QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDeEQsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLCtDQUErQyxDQUFDLENBQUM7WUFDL0UsT0FBTztTQUNSO1FBQ0QsSUFBSSxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxXQUFXO1lBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDakYsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO2FBQy9CO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLHNCQUFzQixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsbUJBQW1CLENBQUMsRUFBRSxFQUFFLGtEQUFrRDtZQUN0TixJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUM7WUFDckYsT0FBTztTQUNSO1FBQ0QsSUFBTSxtQkFBbUIsR0FBRztZQUMxQixxQkFBcUIsRUFBRTtnQkFDckIsZ0JBQWdCLEVBQUUsZ0JBQWdCO2FBQ25DO1NBQ0YsQ0FBQztRQUNGLElBQU0sYUFBYSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUM7WUFDNUYsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM5RyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksYUFBYSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsRUFDeEcsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ3RELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLElBQU0sSUFBSSxHQUFRLGdCQUFnQixDQUFDO2dCQUNuQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbkQsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuQixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUM5RCxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLEVBQ2xGLG1CQUFtQixFQUFFLEtBQUssRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUN0RCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixJQUFNLElBQUksR0FBUSxnQkFBZ0IsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDTixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7WUFDOUQsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRCx3Q0FBTSxHQUFOLFVBQU8sYUFBYTtRQUFwQixpQkFpR0M7UUFoR0MsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRTtZQUM1QixJQUFNLGtCQUFnQixHQUFvQixFQUFFLENBQUM7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUNoQyxJQUFJLE9BQU8sQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLFdBQVcsWUFBWSxJQUFJLEVBQUU7b0JBQzlELE9BQU8sQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDaEgsSUFBSSxPQUFPLENBQUMsaUJBQWlCLElBQUksT0FBTyxDQUFDLGlCQUFpQixZQUFZLElBQUksRUFBRTt3QkFDMUUsT0FBTyxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztxQkFDN0g7aUJBQ0Y7Z0JBQ0Qsc0NBQXNDO2dCQUN0QyxJQUFNLFlBQVksR0FBYSxLQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDakksSUFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsc0JBQXNCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7Z0JBQ2xILElBQUksT0FBTyxDQUFDLFdBQVcsRUFBRTtvQkFDdkIsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQkFDbkMsSUFBTSxVQUFVLEdBQWtCO3dCQUNoQyxnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixnQkFBZ0IsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLDJCQUEyQjt3QkFDakcsUUFBUSxFQUFFLE9BQU87d0JBQ2pCLHNCQUFzQixFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO3dCQUNuRyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsRUFBRTt3QkFDN0csSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUTt3QkFDdEQsZUFBZSxFQUFFLE9BQU8sQ0FBQyxXQUFXO3dCQUNwQyxXQUFXLEVBQUUsWUFBWSxDQUFDLFdBQVc7d0JBQ3JDLElBQUksRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLElBQUk7cUJBQy9ELENBQUM7b0JBQ0YsSUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTt3QkFDdkIsSUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUM7d0JBQ3BELGFBQWEsQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDO3dCQUN0QyxhQUFhLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQzt3QkFDL0IsYUFBYSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7d0JBQ2hDLGFBQWEsQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQzt3QkFDdEQsYUFBYSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzt3QkFDbEQsYUFBYSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQzt3QkFDMUMsa0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUN0Qzt5QkFBTTt3QkFDTCxJQUFNLGNBQWMsR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzt3QkFDekMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3RDLElBQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDOzRCQUNwRCxhQUFhLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7NEJBQ3RELGFBQWEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7NEJBQ2xELElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtnQ0FDWCxhQUFhLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQztnQ0FDL0IsYUFBYSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7NkJBQ3ZDO2lDQUFNLElBQUksQ0FBQyxLQUFLLGNBQWMsRUFBRTtnQ0FDL0IsYUFBYSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7Z0NBQ2hDLGFBQWEsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7NkJBQzFDO2lDQUFNO2dDQUNMLGFBQWEsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7NkJBQzFDOzRCQUNELGtCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzt5QkFDdEM7cUJBQ0Y7aUJBQ0Y7cUJBQU07b0JBQ0wsSUFBSSxLQUFLLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQkFDaEMsSUFBSSxPQUFPLENBQUMsaUJBQWlCLEVBQUU7d0JBQzdCLG9GQUFvRjt3QkFDcEYsbUZBQW1GO3dCQUNuRixJQUFJO3dCQUNKOzs7O21DQUlXO3dCQUNYLEtBQUssR0FBRyxLQUFLLEdBQUcsT0FBTyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDcEQsSUFBSTtxQkFDTDtvQkFDRCxJQUFNLFNBQVMsR0FBa0I7d0JBQy9CLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsMkJBQTJCO3dCQUNqRyxhQUFhLEVBQUUsT0FBTzt3QkFDdEIsUUFBUSxFQUFFLE9BQU87d0JBQ2pCLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVE7d0JBQ3RELEtBQUssRUFBRSxLQUFLO3dCQUNaLHNCQUFzQixFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO3dCQUNuRyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUMsRUFBRTt3QkFDN0csbUJBQW1CLEVBQUUsS0FBSzt3QkFDMUIsZUFBZSxFQUFFLE9BQU8sQ0FBQyxXQUFXO3dCQUNwQyxXQUFXLEVBQUUsWUFBWSxDQUFDLFdBQVc7d0JBQ3JDLElBQUksRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLElBQUk7cUJBQy9ELENBQUM7b0JBQ0Ysa0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUNsQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxhQUFhLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtvQkFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQUEsT0FBTzt3QkFDaEMsSUFBSSxPQUFPLENBQUMsTUFBTSxFQUFFOzRCQUNsQixPQUFPLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzt5QkFDeEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7Z0JBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBZ0IsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWdCLENBQUMsQ0FBQzthQUM5QjtTQUNGO0lBQ0gsQ0FBQztJQUVELHdEQUFzQixHQUF0QixVQUF1QixPQUFlO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLElBQUksQ0FBQztTQUNiO1FBQ0QsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDaEMsT0FBTyxHQUFHLENBQUMsV0FBVyxLQUFLLE9BQU8sQ0FBQztRQUNyQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx3REFBc0IsR0FBdEI7O1FBQ0UsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDeEMsT0FBTztTQUNSOztZQUNELEtBQW9CLElBQUEsS0FBQSxTQUFBLElBQUksQ0FBQyxlQUFlLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQXJDLElBQU0sS0FBSyxXQUFBO2dCQUNkLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxtQkFBbUI7b0JBQ3BDLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFO29CQUN6RCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN4QzthQUNGOzs7Ozs7Ozs7UUFDRCxvRkFBb0Y7SUFDdEYsQ0FBQztJQUVELGdEQUFjLEdBQWQ7UUFBQSxpQkFnRUM7UUEvREMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUN6QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQ3RELElBQUksVUFBVSxHQUFvQixFQUFFLENBQUM7WUFDckMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXO2dCQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMscUJBQXFCO2dCQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQzFFLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUM7YUFDdkY7WUFDRCxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2hELElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQywyQkFBMkIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdkcsSUFBSSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUMsa0JBQWtCLENBQUM7Z0JBQ3BELFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxTQUFTO29CQUMxQixJQUFNLEtBQUssR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUM5QixJQUFNLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUM3RDs7Ozs7Ozs7Ozs7OzswQkFhTTtvQkFDTixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQzt3QkFDdEIsS0FBSyxFQUFFLEtBQUs7d0JBQ1osV0FBVyxFQUFFLFNBQVMsQ0FBQyxlQUFlO3dCQUN0QyxnQkFBZ0IsRUFBRSxTQUFTLENBQUMsc0JBQXNCO3dCQUNsRCxrQkFBa0IsRUFBRSxTQUFTLENBQUMsd0JBQXdCO3dCQUN0RCxXQUFXLEVBQUUsS0FBSzt3QkFDbEIsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUMzSCxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSzt3QkFDM0MsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsWUFBWSxFQUFFLEVBQUU7d0JBQ2hCLHFCQUFxQixFQUFFLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLFVBQUEsZUFBZTs0QkFDaEUsT0FBTyxlQUFlLENBQUMsbUJBQW1CLEtBQUssU0FBUyxDQUFDLGVBQWUsQ0FBQzt3QkFDM0UsQ0FBQyxDQUFDO3FCQUNILENBQUMsQ0FBQztvQkFDSCxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDaEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7Z0JBQzNFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixDQUFDO2FBQzFHO1NBQ0Y7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGdCQUFnQixFQUFFO2dCQUM3RixJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLDJCQUEyQixDQUFDLE1BQU0sQ0FBQyxVQUFBLFlBQVk7b0JBQ3JHLE9BQU8sWUFBWSxDQUFDLCtCQUErQixDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsZ0JBQWdCLENBQUMsMkJBQTJCLENBQUM7Z0JBQzlJLENBQUMsQ0FBQyxDQUFDO2dCQUVILDZHQUE2RztnQkFFN0csSUFBSSxDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUM7Z0JBQzFELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUM7Z0JBQ3pGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixDQUFDO2dCQUN6RyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsb0JBQW9CO2FBQ3JCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQWtCQztRQWpCQyxJQUFJLENBQUMsd0JBQXdCLEdBQUc7WUFDOUIscUJBQXFCLEVBQUUsRUFBRTtZQUN6QixrQkFBa0IsRUFBRSxFQUFFO1NBQ3ZCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQSxPQUFPO2dCQUNoQyxJQUFJLE9BQU8sQ0FBQyxhQUFhLEVBQUU7b0JBQ3pCLE9BQU8sQ0FBQyxhQUFhLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsVUFBQSxlQUFlO3dCQUN2RixPQUFPLGVBQWUsQ0FBQyxtQkFBbUIsS0FBSyxPQUFPLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztvQkFDbkYsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUMvQyxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztpQkFDL0I7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Z0JBM2FtQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTtnQkFDRCxhQUFhO2dCQUNQLG1CQUFtQjtnQkFDckIsaUJBQWlCO2dCQUNoQixRQUFRO2dCQUNaLGNBQWM7Z0JBQ1QsbUJBQW1CO2dCQUNwQixrQkFBa0I7Z0JBQzVCLFFBQVE7O0lBSTNCO1FBREMsWUFBWSxDQUFDLGNBQWMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzJEQUt4QztJQS9CVSx1QkFBdUI7UUFMbkMsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQiwya0VBQStDOztTQUVoRCxDQUFDO1FBZ0JHLFdBQUEsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO09BZmYsdUJBQXVCLENBMGJuQztJQUFELDhCQUFDO0NBQUEsQUExYkQsSUEwYkM7U0ExYlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgU2VhcmNoU2F2ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9zZWFyY2guc2F2ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBBZHZhbmNlZFNlYXJjaERhdGEgfSBmcm9tICcuLi9vYmplY3RzL0FkdmFuY2VkU2VhcmNoRGF0YSc7XHJcbmltcG9ydCB7IEFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSB9IGZyb20gJy4uL29iamVjdHMvQWR2YW5jZWRTZWFyY2hDb25maWdEYXRhJztcclxuaW1wb3J0IHsgTVBNRmllbGQsIE1QTUZpZWxkS2V5cyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUZpZWxkJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zZWFyY2gtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb25kaXRpb25MaXN0IH0gZnJvbSAnLi4vb2JqZWN0cy9Db25kaXRpb25MaXN0JztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JzIH0gZnJvbSAnLi4vLi4vLi4vbGliL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvci5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvck5hbWVzIH0gZnJvbSAnLi4vLi4vLi4vbGliL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgS2V5d29yZCB7XHJcbiAgbmFtZTogc3RyaW5nO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1hZHZhbmNlZC1zZWFyY2gnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9hZHZhbmNlZC1zZWFyY2guY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2FkdmFuY2VkLXNlYXJjaC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBZHZhbmNlZFNlYXJjaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIHZpc2libGUgPSB0cnVlO1xyXG4gIHNlbGVjdGFibGUgPSB0cnVlO1xyXG4gIHJlbW92YWJsZSA9IHRydWU7XHJcbiAgYWRkT25CbHVyID0gdHJ1ZTtcclxuICBmaWx0ZXJSb3dMaXN0ID0gW107XHJcbiAgc2VhcmNoVG9iZVNhdmVkTmFtZSA9ICcnO1xyXG4gIGFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YTogQWR2YW5jZWRTZWFyY2hDb25maWdEYXRhO1xyXG4gIGtleXdvcmRzOiBLZXl3b3JkW10gPSBbXTtcclxuICBhbGxTZWFyY2hGaWVsZHM6IE1QTUZpZWxkW10gPSBbXTtcclxuICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IE1QTUZpZWxkW10gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QWR2YW5jZWRTZWFyY2hDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBBZHZhbmNlZFNlYXJjaERhdGEsXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIHNlYXJjaFNhdmVTZXJ2aWNlOiBTZWFyY2hTYXZlU2VydmljZSxcclxuICAgIHB1YmxpYyBmb3JtYXRUb0xvY2FsZVBpcGU6IERhdGVQaXBlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBzZWFyY2hDb25maWdTZXJ2aWNlOiBTZWFyY2hDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRhdGVQaXBlOiBEYXRlUGlwZVxyXG4gICkgeyB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzprZXl1cCcsIFsnJGV2ZW50J10pXHJcbiAga2V5RXZlbnQoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgIGlmIChldmVudC5rZXkgPT09ICdFbnRlcicpIHtcclxuICAgICAgdGhpcy5jbG9zZShmYWxzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBhZGQoZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7XHJcbiAgICBjb25zdCBpbnB1dCA9IGV2ZW50LmlucHV0O1xyXG4gICAgbGV0IHZhbHVlID0gZXZlbnQudmFsdWU7XHJcblxyXG4gICAgLy8gQWRkIHRoZSBrZXl3b3JkXHJcbiAgICB2YWx1ZSA9ICh2YWx1ZSB8fCAnJykudHJpbSgpO1xyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIHRoaXMua2V5d29yZHMucHVzaCh7IG5hbWU6IHZhbHVlIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlc2V0IHRoZSBpbnB1dCB2YWx1ZVxyXG4gICAgaWYgKGlucHV0KSB7XHJcbiAgICAgIGlucHV0LnZhbHVlID0gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW1vdmUoa2V5d29yZDogS2V5d29yZCk6IHZvaWQge1xyXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLmtleXdvcmRzLmluZGV4T2Yoa2V5d29yZCk7XHJcblxyXG4gICAgaWYgKGluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy5rZXl3b3Jkcy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY2xvc2UoZGF0YT86IGFueSkge1xyXG4gICAgaWYgKGRhdGEgJiYgZGF0YSAhPT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFJlY2VudFNlYXJjaChkYXRhKTtcclxuICAgIH1cclxuICAgIGlmIChkYXRhID09PSB0cnVlICYmIHRoaXMuZGF0YSAmJiB0aGlzLmRhdGEucmVjZW50U2VhcmNoKSB7XHJcbiAgICAgIHRoaXMuZGF0YS5yZWNlbnRTZWFyY2gubWFwKHJvd0RhdGEgPT4ge1xyXG4gICAgICAgIGlmIChyb3dEYXRhLmlzRWRpdCkge1xyXG4gICAgICAgICAgcm93RGF0YS5pc0VkaXQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoZGF0YSk7XHJcbiAgfVxyXG5cclxuICBmaWx0ZXJTZWxlY3Rpb25DaGFuZ2UoZXZlbnQpIHtcclxuICAgIHRoaXMuc2V0QXZpYWxibGVTZWFyY2hGaWVscygpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RmllbGREZXRhaWxzKGZpZWxkSWQpIHtcclxuICAgIHJldHVybiB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLm1ldGFkYXRhRmllbGRzLmZpbHRlcihmaWVsZCA9PiB7XHJcbiAgICAgIHJldHVybiBmaWVsZFsnTVBNX0ZpZWxkc19Db25maWctaWQnXS5JZCA9PT0gZmllbGRJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gdG8gcmVtb3ZlIGFsbCBmaWVsZHNcclxuICBjbGVhckZpbHRlcigpIHtcclxuICAgIHRoaXMuZmlsdGVyUm93TGlzdCA9IFtdO1xyXG4gICAgLy8gdGhpcy5hZGRGaWx0ZXIoKTtcclxuICAgIHRoaXMuc2V0QXZpYWxibGVTZWFyY2hGaWVscygpO1xyXG4gIH1cclxuXHJcbiAgLy8gdG8gcmVtb3ZlIHNlbGVjdGVkIGZpZWxkXHJcbiAgcmVtb3ZlRmlsdGVyKGV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQgJiYgZXZlbnQucm93SWQpIHtcclxuICAgICAgdGhpcy5maWx0ZXJSb3dMaXN0ID0gdGhpcy5maWx0ZXJSb3dMaXN0LmZpbHRlcihyb3dEYXRhID0+IHtcclxuICAgICAgICByZXR1cm4gcm93RGF0YS5yb3dJZCAhPT0gZXZlbnQucm93SWQ7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk7XHJcbiAgfVxyXG5cclxuICAvLyB0byBhZGQgbmV3IGZpZWxkXHJcbiAgYWRkRmlsdGVyKCkge1xyXG4gICAgbGV0IGNhbkFkZE5ld0ZpbHRlciA9IHRydWU7XHJcbiAgICB0aGlzLmZpbHRlclJvd0xpc3QuZm9yRWFjaChyb3dEYXRhID0+IHtcclxuICAgICAgaWYgKHJvd0RhdGEgJiYgIXJvd0RhdGEuaXNGaWx0ZXJTZWxlY3RlZCkge1xyXG4gICAgICAgIGNhbkFkZE5ld0ZpbHRlciA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGlmIChjYW5BZGROZXdGaWx0ZXIpIHtcclxuICAgICAgaWYgKHRoaXMuYXZhaWxhYmxlU2VhcmNoRmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICBjb25zdCBjb3VudCA9IHRoaXMuZmlsdGVyUm93TGlzdC5sZW5ndGggKyAxO1xyXG4gICAgICAgIHRoaXMuZmlsdGVyUm93TGlzdC5wdXNoKHtcclxuICAgICAgICAgIHJvd0lkOiBjb3VudCxcclxuICAgICAgICAgIHNlYXJjaEZpZWxkOiAnJyxcclxuICAgICAgICAgIHNlYXJjaE9wZXJhdG9ySWQ6ICcnLFxyXG4gICAgICAgICAgc2VhcmNoT3BlcmF0b3JOYW1lOiAnJyxcclxuICAgICAgICAgIHNlYXJjaFZhbHVlOiAnJyxcclxuICAgICAgICAgIHNlYXJjaFNlY29uZFZhbHVlOiAnJyxcclxuICAgICAgICAgIGlzc2VhcmNoVmFsdWVSZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgIGlzRmlsdGVyU2VsZWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgaGFzVHdvVmFsdWVzOiAnJyxcclxuICAgICAgICAgIGF2YWlsYWJsZVNlYXJjaEZpZWxkczogdGhpcy5hdmFpbGFibGVTZWFyY2hGaWVsZHNcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnTm8gbW9yZSBmaWx0ZXJzIHRvIGFkZCcpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ0tpbmRseSBzZWxlY3QgdGhlIHByZXZpb3VzIGZpbHRlciBmaXJzdCcpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVGb3JTZWFyY2goKSB7XHJcbiAgICBsZXQgYWxsRmlsdGVyRmllbGRTZWxlY3RlZCA9IHRydWU7XHJcbiAgICBsZXQgaXNWYWxpZCA9IHRydWU7XHJcbiAgICBsZXQgZW1wdHlWYWx1ZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5maWx0ZXJSb3dMaXN0LmZvckVhY2goZmlsdGVyID0+IHtcclxuICAgICAgY29uc3Qgc2VhcmNoRmllbGQgPSBmaWx0ZXIuYXZhaWxhYmxlU2VhcmNoRmllbGRzLmZpbmQoZGF0YSA9PiAoZmlsdGVyLnNlYXJjaEZpZWxkID09PSBkYXRhLk1BUFBFUl9OQU1FIHx8IGZpbHRlci5zZWFyY2hGaWVsZCA9PT0gZGF0YS5NUE1fRklFTERfQ09ORklHX0lEKSk7XHJcbiAgICAgIGlmICghc2VhcmNoRmllbGQuSVNfRFJPUF9ET1dOKSB7XHJcbiAgICAgICAgaWYgKGZpbHRlci5zZWFyY2hGaWVsZCkge1xyXG4gICAgICAgICAgaWYgKGZpbHRlci5zZWFyY2hPcGVyYXRvcklkKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWx0ZXIuaXNzZWFyY2hWYWx1ZVJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgaWYgKGZpbHRlci5zZWFyY2hWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGZpbHRlci5oYXNUd29WYWx1ZXMgJiYgIWZpbHRlci5zZWFyY2hTZWNvbmRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiAoZmlsdGVyLnNlYXJjaFZhbHVlKSA9PT0gJ3N0cmluZycgJiYgZmlsdGVyLnNlYXJjaFZhbHVlLnRyaW0oKSA9PT0gJycgJiYgZmlsdGVyLmlzc2VhcmNoVmFsdWVSZXF1aXJlZCkge1xyXG4gICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICBlbXB0eVZhbHVlID0gdHJ1ZTtcclxuICAgICAgfVxyXG5cclxuXHJcbiAgICAgIGlmICghaXNWYWxpZCB8fCAhZmlsdGVyLmlzRmlsdGVyU2VsZWN0ZWQpIHtcclxuICAgICAgICBhbGxGaWx0ZXJGaWVsZFNlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGlmIChlbXB0eVZhbHVlKSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdLaW5kbHkgZW50ZXIgdGhlIHZhbHVlIHRvIGJlIHNlYXJjaGVkJyk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0gZWxzZSBpZiAoIWFsbEZpbHRlckZpZWxkU2VsZWN0ZWQpIHtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ0tpbmRseSBzZWxlY3QgYWxsIGZpZWxkJyk7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29udmVyVG9Mb2NhbERhdGUoZGF0ZSkge1xyXG4gICAgY29uc3QgaXNvU3RyaW5nVG9Mb2NhbGUgPSBhY3JvbnVpLmdldERhdGVXaXRoTG9jYWxlVGltZVpvbmUoZGF0ZSk7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKGlzb1N0cmluZ1RvTG9jYWxlLCAnTU0vZGQveXl5eScpO1xyXG4gIH1cclxuXHJcbiAgc2F2ZVNlYXJjaChzZWFyY2hDb25kaXRpb25zOiBDb25kaXRpb25MaXN0W10sIHZpZXdOYW1lOiBzdHJpbmcpIHtcclxuICAgIGlmICghdGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lKSB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdLaW5kbHkgYWRkIHNlYXJjaCBuYW1lJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNlYXJjaFRvYmVTYXZlZE5hbWUubGVuZ3RoID4gNjQpIHtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1NlYXJjaCBuYW1lIGNhbm5vdCBiZSBtb3JlIHRoYW4gNjQgY2hhcmFjdGVycycpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBsZXQgaGFzRHVwbGljYXRlU2VhcmNoTmFtZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXRhLmF2YWlsYWJsZVNhdmVkU2VhcmNoLmZvckVhY2goc2F2ZWRTZWFyY2ggPT4ge1xyXG4gICAgICBpZiAoKHNhdmVkU2VhcmNoLk5BTUUpLnRvTG93ZXJDYXNlKCkgPT09ICh0aGlzLnNlYXJjaFRvYmVTYXZlZE5hbWUpLnRvTG93ZXJDYXNlKCkpIHtcclxuICAgICAgICBoYXNEdXBsaWNhdGVTZWFyY2hOYW1lID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBpZiAoaGFzRHVwbGljYXRlU2VhcmNoTmFtZSAmJiAoIXRoaXMuZGF0YS5zYXZlZFNlYXJjaCB8fCAodHlwZW9mICh0aGlzLmRhdGEuc2F2ZWRTZWFyY2gpID09PSAnb2JqZWN0JykgJiYgdGhpcy5kYXRhLnNhdmVkU2VhcmNoLk5BTUUgIT09IHRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSkpIHsgLy8gfHwgdHlwZW9mICh0aGlzLmRhdGEuc2F2ZWRTZWFyY2gpID09PSAnb2JqZWN0JylcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTZWFyY2ggTmFtZSBhbHJlYWR5IGV4aXN0cywgS2luZGx5IGNoYW5nZSB0aGUgbmFtZScpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBzZWFyY2hDb25kaXRpb25MaXN0ID0ge1xyXG4gICAgICBzZWFyY2hfY29uZGl0aW9uX2xpc3Q6IHtcclxuICAgICAgICBzZWFyY2hfY29uZGl0aW9uOiBzZWFyY2hDb25kaXRpb25zXHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICBjb25zdCBzYXZlZFNlYXJjaElkID0gKHRoaXMuZGF0YS5zYXZlZFNlYXJjaCAmJiB0aGlzLmRhdGEuc2F2ZWRTZWFyY2hbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddICYmXHJcbiAgICAgIHRoaXMuZGF0YS5zYXZlZFNlYXJjaFsnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10uSWQpID8gdGhpcy5kYXRhLnNhdmVkU2VhcmNoWydNUE1fU2F2ZWRfU2VhcmNoZXMtaWQnXS5JZCA6ICcnO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgIGlmIChzYXZlZFNlYXJjaElkKSB7XHJcbiAgICAgIHRoaXMuc2VhcmNoU2F2ZVNlcnZpY2UudXBkYXRlU2F2ZWRTZWFyY2goc2F2ZWRTZWFyY2hJZCwgdGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lLCB0aGlzLnNlYXJjaFRvYmVTYXZlZE5hbWUsXHJcbiAgICAgICAgc2VhcmNoQ29uZGl0aW9uTGlzdCwgZmFsc2UsIHZpZXdOYW1lKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIGNvbnN0IGRhdGE6IGFueSA9IHNlYXJjaENvbmRpdGlvbnM7XHJcbiAgICAgICAgICBkYXRhWzBdLk5BTUUgPSByZXNwb25zZVsnTVBNX1NhdmVkX1NlYXJjaGVzJ10uTkFNRTtcclxuICAgICAgICAgIHRoaXMuY2xvc2UoZGF0YSk7XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgc2F2aW5nIHNlYXJjaCcpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWFyY2hTYXZlU2VydmljZS5zYXZlU2VhcmNoKHRoaXMuc2VhcmNoVG9iZVNhdmVkTmFtZSwgdGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lLFxyXG4gICAgICAgIHNlYXJjaENvbmRpdGlvbkxpc3QsIGZhbHNlLCB2aWV3TmFtZSkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICBjb25zdCBkYXRhOiBhbnkgPSBzZWFyY2hDb25kaXRpb25zO1xyXG4gICAgICAgICAgZGF0YVswXS5OQU1FID0gcmVzcG9uc2VbJ01QTV9TYXZlZF9TZWFyY2hlcyddLk5BTUU7XHJcbiAgICAgICAgICB0aGlzLmNsb3NlKGRhdGEpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0Vycm9yIHdoaWxlIHNhdmluZyBzZWFyY2gnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNlYXJjaChpc1NhdmVkU2VhcmNoKSB7XHJcbiAgICBpZiAodGhpcy52YWxpZGF0ZUZvclNlYXJjaCgpKSB7XHJcbiAgICAgIGNvbnN0IHNlYXJjaENvbmRpdGlvbnM6IENvbmRpdGlvbkxpc3RbXSA9IFtdO1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0xpc3QuZm9yRWFjaChyb3dEYXRhID0+IHtcclxuICAgICAgICBpZiAocm93RGF0YS5zZWFyY2hWYWx1ZSAmJiByb3dEYXRhLnNlYXJjaFZhbHVlIGluc3RhbmNlb2YgRGF0ZSkge1xyXG4gICAgICAgICAgcm93RGF0YS5zZWFyY2hWYWx1ZSA9IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKHJvd0RhdGEuc2VhcmNoVmFsdWUpLCAneXl5eS1NTS1kZCcpLmNvbmNhdCgnVDAwOjAwOjAwWicpO1xyXG4gICAgICAgICAgaWYgKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgJiYgcm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgICAgICAgICAgcm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSA9IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUpLCAneXl5eS1NTS1kZCcpLmNvbmNhdCgnVDAwOjAwOjAwWicpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyByb3dEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGN1cnJNUE1GaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoTVBNRmllbGRLZXlzLk1QTV9GSUVMRF9DT05GSUdfSUQsIHJvd0RhdGEuc2VhcmNoRmllbGQpO1xyXG4gICAgICAgIGNvbnN0IGZpZWxkSWQgPSBjdXJyTVBNRmllbGQgPyB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5mb3JtSW5kZXhlcklkRnJvbUZpZWxkKGN1cnJNUE1GaWVsZCkgOiByb3dEYXRhLnNlYXJjaEZpZWxkO1xyXG4gICAgICAgIGlmIChyb3dEYXRhLmlzQ29tYm9UeXBlKSB7XHJcbiAgICAgICAgICBjb25zdCB2YWx1ZXMgPSByb3dEYXRhLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgY29uc3QgY29uZGlkdGlvbjogQ29uZGl0aW9uTGlzdCA9IHtcclxuICAgICAgICAgICAgaXNBZHZhbmNlZFNlYXJjaDogdHJ1ZSxcclxuICAgICAgICAgICAgc2VhcmNoSWRlbnRpZmllcjogdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hJZGVudGlmaWVyLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRyxcclxuICAgICAgICAgICAgZmllbGRfaWQ6IGZpZWxkSWQsXHJcbiAgICAgICAgICAgIHJlbGF0aW9uYWxfb3BlcmF0b3JfaWQ6IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCA/IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCA6IE1QTVNlYXJjaE9wZXJhdG9ycy5JUyxcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9uYW1lOiByb3dEYXRhLnNlYXJjaE9wZXJhdG9yTmFtZSA/IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JOYW1lIDogTVBNU2VhcmNoT3BlcmF0b3JOYW1lcy5JUyxcclxuICAgICAgICAgICAgdHlwZTogY3Vyck1QTUZpZWxkID8gY3Vyck1QTUZpZWxkLkRBVEFfVFlQRSA6ICdzdHJpbmcnLFxyXG4gICAgICAgICAgICBtYXBwZXJfZmllbGRfaWQ6IHJvd0RhdGEuc2VhcmNoRmllbGQsXHJcbiAgICAgICAgICAgIG1hcHBlcl9uYW1lOiBjdXJyTVBNRmllbGQuTUFQUEVSX05BTUUsXHJcbiAgICAgICAgICAgIHZpZXc6IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllci5WSUVXXHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgICAgaWYgKHZhbHVlcy5sZW5ndGggPT09IDEpIHtcclxuICAgICAgICAgICAgY29uc3QgY29uZGl0aW9uQ29weSA9IE9iamVjdC5hc3NpZ24oe30sIGNvbmRpZHRpb24pO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LmZpbHRlclJvd0RhdGEgPSByb3dEYXRhO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LmxlZnRfcGFyZW4gPSAnKCc7XHJcbiAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkucmlnaHRfcGFyZW4gPSAnKSc7XHJcbiAgICAgICAgICAgIGNvbmRpdGlvbkNvcHkuZGlzcGxheV92YWx1ZSA9IHZhbHVlc1swXS5kaXNwbGF5X3ZhbHVlO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LnZhbHVlID0gdmFsdWVzWzBdLmZpZWxkX3ZhbHVlLnZhbHVlO1xyXG4gICAgICAgICAgICBjb25kaXRpb25Db3B5LnJlbGF0aW9uYWxfb3BlcmF0b3IgPSAnYW5kJztcclxuICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9ucy5wdXNoKGNvbmRpdGlvbkNvcHkpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgbGFzdFZhbHVlSW5kZXggPSB2YWx1ZXMubGVuZ3RoIC0gMTtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB2YWx1ZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICBjb25zdCBjb25kaXRpb25Db3B5ID0gT2JqZWN0LmFzc2lnbih7fSwgY29uZGlkdGlvbik7XHJcbiAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5kaXNwbGF5X3ZhbHVlID0gdmFsdWVzW2ldLmRpc3BsYXlfdmFsdWU7XHJcbiAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS52YWx1ZSA9IHZhbHVlc1tpXS5maWVsZF92YWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgICBpZiAoaSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5sZWZ0X3BhcmVuID0gJygnO1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5maWx0ZXJSb3dEYXRhID0gcm93RGF0YTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKGkgPT09IGxhc3RWYWx1ZUluZGV4KSB7XHJcbiAgICAgICAgICAgICAgICBjb25kaXRpb25Db3B5LnJpZ2h0X3BhcmVuID0gJyknO1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5yZWxhdGlvbmFsX29wZXJhdG9yID0gJ29yJztcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uQ29weS5yZWxhdGlvbmFsX29wZXJhdG9yID0gJ29yJztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgc2VhcmNoQ29uZGl0aW9ucy5wdXNoKGNvbmRpdGlvbkNvcHkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGxldCB2YWx1ZSA9IHJvd0RhdGEuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICBpZiAocm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSkge1xyXG4gICAgICAgICAgICAvLyBpZiAocm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSBpbnN0YW5jZW9mIERhdGUgJiYgcm93RGF0YS5pc3NlYXJjaFZhbHVlUmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgLy8gICByb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlID0gdGhpcy5jb252ZXJUb0xvY2FsRGF0ZShyb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAvKiBpZiAocm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcclxuICAgICAgICAgICAgICAvLyB2YWx1ZSA9IHZhbHVlLnRvSVNPU3RyaW5nKCkgKyAnIGFuZCAnICsgcm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZS50b0lTT1N0cmluZygpO1xyXG4gICAgICAgICAgICAgIHZhbHVlID0gdGhpcy5kYXRlUGlwZS50cmFuc2Zvcm0obmV3IERhdGUocm93RGF0YS5zZWFyY2hWYWx1ZSksICd5eXl5LU1NLWRkJykuY29uY2F0KCdUMDA6MDA6MDBaJykgKyAnIGFuZCAnICtcclxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKHJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUpLCAneXl5eS1NTS1kZCcpLmNvbmNhdCgnVDAwOjAwOjAwWicpICBcclxuICAgICAgICAgICAgfSBlbHNlIHsgKi9cclxuICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZSArICcgYW5kICcgKyByb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjb25zdCBjb25kaXRpb246IENvbmRpdGlvbkxpc3QgPSB7XHJcbiAgICAgICAgICAgIGlzQWR2YW5jZWRTZWFyY2g6IHRydWUsXHJcbiAgICAgICAgICAgIHNlYXJjaElkZW50aWZpZXI6IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllci5SX1BPX0FEVkFOQ0VEX1NFQVJDSF9DT05GSUcsXHJcbiAgICAgICAgICAgIGZpbHRlclJvd0RhdGE6IHJvd0RhdGEsXHJcbiAgICAgICAgICAgIGZpZWxkX2lkOiBmaWVsZElkLFxyXG4gICAgICAgICAgICB0eXBlOiBjdXJyTVBNRmllbGQgPyBjdXJyTVBNRmllbGQuREFUQV9UWVBFIDogJ3N0cmluZycsXHJcbiAgICAgICAgICAgIHZhbHVlOiB2YWx1ZSxcclxuICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogcm93RGF0YS5zZWFyY2hPcGVyYXRvcklkID8gcm93RGF0YS5zZWFyY2hPcGVyYXRvcklkIDogTVBNU2VhcmNoT3BlcmF0b3JzLklTLFxyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IHJvd0RhdGEuc2VhcmNoT3BlcmF0b3JOYW1lID8gcm93RGF0YS5zZWFyY2hPcGVyYXRvck5hbWUgOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yOiAnYW5kJyxcclxuICAgICAgICAgICAgbWFwcGVyX2ZpZWxkX2lkOiByb3dEYXRhLnNlYXJjaEZpZWxkLFxyXG4gICAgICAgICAgICBtYXBwZXJfbmFtZTogY3Vyck1QTUZpZWxkLk1BUFBFUl9OQU1FLFxyXG4gICAgICAgICAgICB2aWV3OiB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaElkZW50aWZpZXIuVklFV1xyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnMucHVzaChjb25kaXRpb24pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGlmIChpc1NhdmVkU2VhcmNoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSAmJiB0aGlzLmRhdGEucmVjZW50U2VhcmNoKSB7XHJcbiAgICAgICAgICB0aGlzLmRhdGEucmVjZW50U2VhcmNoLm1hcChyb3dEYXRhID0+IHtcclxuICAgICAgICAgICAgaWYgKHJvd0RhdGEuaXNFZGl0KSB7XHJcbiAgICAgICAgICAgICAgcm93RGF0YS5pc0VkaXQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2F2ZVNlYXJjaChzZWFyY2hDb25kaXRpb25zLCB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaElkZW50aWZpZXIuVklFVyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jbG9zZShzZWFyY2hDb25kaXRpb25zKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaXNGaWVsZEFscmVhZHlTZWxlY3RlZChmaWVsZElkOiBzdHJpbmcpIHtcclxuICAgIGlmICghZmllbGRJZCkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiB0aGlzLmZpbHRlclJvd0xpc3QuZmluZChyb3cgPT4ge1xyXG4gICAgICByZXR1cm4gcm93LnNlYXJjaEZpZWxkID09PSBmaWVsZElkO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCkge1xyXG4gICAgdGhpcy5hdmFpbGFibGVTZWFyY2hGaWVsZHMgPSBbXTtcclxuICAgIGlmICghQXJyYXkuaXNBcnJheSh0aGlzLmFsbFNlYXJjaEZpZWxkcykpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBmaWVsZCBvZiB0aGlzLmFsbFNlYXJjaEZpZWxkcykge1xyXG4gICAgICBpZiAoZmllbGQgJiYgZmllbGQuTVBNX0ZJRUxEX0NPTkZJR19JRCAmJlxyXG4gICAgICAgICF0aGlzLmlzRmllbGRBbHJlYWR5U2VsZWN0ZWQoZmllbGQuTVBNX0ZJRUxEX0NPTkZJR19JRCkpIHtcclxuICAgICAgICB0aGlzLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5wdXNoKGZpZWxkKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzID0gdGhpcy5hdmFpbGFibGVTZWFyY2hGaWVsZHM7XHJcbiAgfVxyXG5cclxuICBtYXBTZWFjaENvbmZpZygpIHtcclxuICAgIGlmICh0aGlzLmRhdGEuc2F2ZWRTZWFyY2gpIHtcclxuICAgICAgdGhpcy5zZWFyY2hUb2JlU2F2ZWROYW1lID0gdGhpcy5kYXRhLnNhdmVkU2VhcmNoLk5BTUU7XHJcbiAgICAgIGxldCBjb25kaXRpb25zOiBDb25kaXRpb25MaXN0W10gPSBbXTtcclxuICAgICAgaWYgKHRoaXMuZGF0YS5zYXZlZFNlYXJjaC5TRUFSQ0hfREFUQSAmJlxyXG4gICAgICAgIHRoaXMuZGF0YS5zYXZlZFNlYXJjaC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3QgJiZcclxuICAgICAgICB0aGlzLmRhdGEuc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24pIHtcclxuICAgICAgICBjb25kaXRpb25zID0gdGhpcy5kYXRhLnNhdmVkU2VhcmNoLlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChjb25kaXRpb25zICYmIGNvbmRpdGlvbnNbMF0uc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgIGNvbnN0IGFkdkNvbmZpZyA9IHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5nZXRBZHZhbmNlZFNlYXJjaENvbmZpZ0J5SWQoY29uZGl0aW9uc1swXS5zZWFyY2hJZGVudGlmaWVyKTtcclxuICAgICAgICB0aGlzLmFsbFNlYXJjaEZpZWxkcyA9IGFkdkNvbmZpZy5SX1BNX1NlYXJjaF9GaWVsZHM7XHJcbiAgICAgICAgY29uZGl0aW9ucy5mb3JFYWNoKGNvbmRpdGlvbiA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjb3VudCA9IHRoaXMuZmlsdGVyUm93TGlzdC5sZW5ndGggKyAxO1xyXG4gICAgICAgICAgdGhpcy5zZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk7XHJcbiAgICAgICAgICBjb25zdCB2YWx1ZSA9IGNvbmRpdGlvbi52YWx1ZSAhPSBudWxsID8gY29uZGl0aW9uLnZhbHVlIDogJyc7XHJcbiAgICAgICAgICAvKiB0aGlzLmZpbHRlclJvd0xpc3QucHVzaCh7XHJcbiAgICAgICAgICAgIHJvd0lkOiBjb3VudCxcclxuICAgICAgICAgICAgc2VhcmNoRmllbGQ6IGNvbmRpdGlvbi5tYXBwZXJfZmllbGRfaWQsXHJcbiAgICAgICAgICAgIHNlYXJjaE9wZXJhdG9ySWQ6IGNvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yX2lkLFxyXG4gICAgICAgICAgICBzZWFyY2hPcGVyYXRvck5hbWU6IGNvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yX25hbWUsXHJcbiAgICAgICAgICAgIHNlYXJjaFZhbHVlOiBjb25kaXRpb24udmFsdWUsXHJcbiAgICAgICAgICAgIHNlYXJjaFNlY29uZFZhbHVlOiBjb25kaXRpb24udmFsdWUuc3BsaXQoJ2FuZCAnKS5sZW5ndGggPiAwID8gY29uZGl0aW9uLnZhbHVlLnNwbGl0KCdhbmQgJylbMV0gOiAnJyxcclxuICAgICAgICAgICAgaXNzZWFyY2hWYWx1ZVJlcXVpcmVkOiBjb25kaXRpb24udmFsdWUgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgICAgIGlzRmlsdGVyU2VsZWN0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgIGhhc1R3b1ZhbHVlczogJycsXHJcbiAgICAgICAgICAgIGF2YWlsYWJsZVNlYXJjaEZpZWxkczogdGhpcy5hbGxTZWFyY2hGaWVsZHMuZmlsdGVyKHNlYXJjaEZpZWxkSXRlbSA9PiB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHNlYXJjaEZpZWxkSXRlbS5NUE1fRklFTERfQ09ORklHX0lEID09PSBjb25kaXRpb24ubWFwcGVyX2ZpZWxkX2lkO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSk7ICovXHJcbiAgICAgICAgICB0aGlzLmZpbHRlclJvd0xpc3QucHVzaCh7XHJcbiAgICAgICAgICAgIHJvd0lkOiBjb3VudCxcclxuICAgICAgICAgICAgc2VhcmNoRmllbGQ6IGNvbmRpdGlvbi5tYXBwZXJfZmllbGRfaWQsXHJcbiAgICAgICAgICAgIHNlYXJjaE9wZXJhdG9ySWQ6IGNvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yX2lkLFxyXG4gICAgICAgICAgICBzZWFyY2hPcGVyYXRvck5hbWU6IGNvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yX25hbWUsXHJcbiAgICAgICAgICAgIHNlYXJjaFZhbHVlOiB2YWx1ZSxcclxuICAgICAgICAgICAgc2VhcmNoU2Vjb25kVmFsdWU6IHZhbHVlLnNwbGl0KCdhbmQgJykubGVuZ3RoID4gMCA/IHZhbHVlLnNwbGl0KCdhbmQgJylbMV0gIT09IHVuZGVmaW5lZCA/IHZhbHVlLnNwbGl0KCdhbmQgJylbMV0gOiAnJyA6ICcnLFxyXG4gICAgICAgICAgICBpc3NlYXJjaFZhbHVlUmVxdWlyZWQ6IHZhbHVlID8gdHJ1ZSA6IGZhbHNlLFxyXG4gICAgICAgICAgICBpc0ZpbHRlclNlbGVjdGVkOiB0cnVlLFxyXG4gICAgICAgICAgICBoYXNUd29WYWx1ZXM6ICcnLFxyXG4gICAgICAgICAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IHRoaXMuYWxsU2VhcmNoRmllbGRzLmZpbHRlcihzZWFyY2hGaWVsZEl0ZW0gPT4ge1xyXG4gICAgICAgICAgICAgIHJldHVybiBzZWFyY2hGaWVsZEl0ZW0uTVBNX0ZJRUxEX0NPTkZJR19JRCA9PT0gY29uZGl0aW9uLm1hcHBlcl9maWVsZF9pZDtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5zZXRBdmlhbGJsZVNlYXJjaEZpZWxzKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzID0gdGhpcy5hbGxTZWFyY2hGaWVsZHM7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoT3BlcmF0b3JMaXN0ID0gdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hPcGVyYXRvckxpc3Q7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhICYmIHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZyA9IHRoaXMuZGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuYWR2YW5jZWRTZWFyY2hDb25maWd1cmF0aW9uLmZpbHRlcihzZXJhY2hDb25maWcgPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHNlcmFjaENvbmZpZ1snTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZCA9PT0gdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hJZGVudGlmaWVyLlJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRztcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgLy8gdGhpcy5kYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hJZGVudGlmaWVyID0gc2VhcmNoQ29uZmlnWzBdWydNUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy1pZCddLklkO1xyXG5cclxuICAgICAgICB0aGlzLmFsbFNlYXJjaEZpZWxkcyA9IHNlYXJjaENvbmZpZ1swXS5SX1BNX1NlYXJjaF9GaWVsZHM7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzID0gc2VhcmNoQ29uZmlnWzBdLlJfUE1fU2VhcmNoX0ZpZWxkcztcclxuICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hPcGVyYXRvckxpc3QgPSB0aGlzLmRhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLnNlYXJjaE9wZXJhdG9yTGlzdDtcclxuICAgICAgICB0aGlzLnNldEF2aWFsYmxlU2VhcmNoRmllbHMoKTtcclxuICAgICAgICAvLyB0aGlzLmFkZEZpbHRlcigpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhID0ge1xyXG4gICAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IFtdLFxyXG4gICAgICBzZWFyY2hPcGVyYXRvckxpc3Q6IFtdXHJcbiAgICB9O1xyXG4gICAgdGhpcy5tYXBTZWFjaENvbmZpZygpO1xyXG4gICAgaWYgKHRoaXMuZGF0YS5yZWNlbnRTZWFyY2gpIHtcclxuICAgICAgdGhpcy5maWx0ZXJSb3dMaXN0ID0gW107XHJcbiAgICAgIHRoaXMuZGF0YS5yZWNlbnRTZWFyY2gubWFwKHJvd0RhdGEgPT4ge1xyXG4gICAgICAgIGlmIChyb3dEYXRhLmZpbHRlclJvd0RhdGEpIHtcclxuICAgICAgICAgIHJvd0RhdGEuZmlsdGVyUm93RGF0YS5hdmFpbGFibGVTZWFyY2hGaWVsZHMgPSB0aGlzLmFsbFNlYXJjaEZpZWxkcy5maWx0ZXIoc2VhcmNoRmllbGRJdGVtID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHNlYXJjaEZpZWxkSXRlbS5NUE1fRklFTERfQ09ORklHX0lEID09PSByb3dEYXRhLmZpbHRlclJvd0RhdGEuc2VhcmNoRmllbGQ7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuZmlsdGVyUm93TGlzdC5wdXNoKHJvd0RhdGEuZmlsdGVyUm93RGF0YSk7XHJcbiAgICAgICAgICB0aGlzLnNldEF2aWFsYmxlU2VhcmNoRmllbHMoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=