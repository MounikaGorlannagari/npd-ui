import { __assign, __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { NotificationService } from '../../notification/notification.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { IndexerDataTypes } from '../../shared/services/indexer/objects/IndexerDataTypes';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusService } from '../../shared/services/status.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { AppService } from '../../mpm-utils/services/app.service';
import { ProjectService } from '../../project/shared/services/project.service';
import { map, startWith } from 'rxjs/operators';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { ProjectConstant } from '../../project/project-overview/project.constants';
import { AdvancedSearchStandardValue, StandardCampaignValue, StandardDeliverableValue, StandardProjectValue, StandardTaskValue } from '../objects/AdvancedSearchStandardValue';
var AdvancedSearchFieldComponent = /** @class */ (function () {
    function AdvancedSearchFieldComponent(adapter, utilService, notificationService, sharingService, statusService, entityAppDefService, categoryService, otmmService, appService, projectService) {
        this.adapter = adapter;
        this.utilService = utilService;
        this.notificationService = notificationService;
        this.sharingService = sharingService;
        this.statusService = statusService;
        this.entityAppDefService = entityAppDefService;
        this.categoryService = categoryService;
        this.otmmService = otmmService;
        this.appService = appService;
        this.projectService = projectService;
        this.filterSelected = new EventEmitter();
        this.removeFilterField = new EventEmitter();
        this.requireFieldValue = true;
        this.charDataType = true;
        this.dateDataType = false;
        this.numberDataType = false;
        this.comboType = false;
        this.comboValues = [];
        this.options = [];
    }
    AdvancedSearchFieldComponent.prototype.convertToDate = function (dateString) {
        var dateArray = dateString.split('/');
        if (dateArray.length > 0) {
            return new Date(dateArray[2], dateArray[1] - 1, dateArray[0]);
        }
        else {
            return dateString;
        }
    };
    AdvancedSearchFieldComponent.prototype.loadFieldValueType = function (selectedOperatorId, isInitial) {
        var _this = this;
        this.filterRowData.searchOperatorId = selectedOperatorId;
        this.filterRowData.searchOperatorName = this.availableFieldOperators.find(function (element) { return element.OPERATOR_ID === selectedOperatorId; }).NAME;
        var selectedField = this.filterRowData.availableSearchFields.find(function (field) {
            return (field.MPM_FIELD_CONFIG_ID === _this.filterRowData.searchField);
        });
        if (!isInitial) {
            this.filterRowData.searchSecondValue = '';
            this.filterRowData.searchValue = '';
            if (this.advancedSearchFieldForm && this.advancedSearchFieldForm.controls &&
                this.advancedSearchFieldForm.controls.fieldValue) {
                this.advancedSearchFieldForm.controls.fieldValue.patchValue('');
            }
            if (this.advancedSearchFieldForm && this.advancedSearchFieldForm.controls && this.advancedSearchFieldForm.controls.fieldSecondValue) {
                this.advancedSearchFieldForm.controls.fieldSecondValue.patchValue('');
            }
        }
        var selectedOperator = this.advancedSearchConfigData.searchOperatorList.filter(function (operator) {
            return operator.OPERATOR_ID === selectedOperatorId && operator.DATA_TYPE === selectedField.DATA_TYPE;
        });
        if (selectedOperator[0] && selectedOperator[0].VALUE_COUNT) {
            this.requireFieldValue = true;
            this.filterRowData.issearchValueRequired = true;
        }
        else {
            this.requireFieldValue = false;
            this.filterRowData.issearchValueRequired = false;
        }
        if (selectedOperator[0].VALUE_COUNT === 2) {
            this.filterRowData.hasTwoValues = true;
            if (typeof this.filterRowData.searchValue === 'string' && this.filterRowData.searchValue.includes('and')
                && this.filterRowData.searchValue.split(' and ').length > 1) {
                var searchValues = this.filterRowData.searchValue.split(' and ');
                this.filterRowData.searchValue = searchValues[0];
                this.filterRowData.searchSecondValue = searchValues[1];
            }
        }
        else {
            this.filterRowData.hasTwoValues = false;
        }
        if (!this.comboType) {
            if (selectedOperator[0].DATA_TYPE === IndexerDataTypes.STRING
                || selectedOperator[0].DATA_TYPE === IndexerDataTypes.BOOLEAN) {
                this.charDataType = true;
                this.dateDataType = false;
                this.numberDataType = false;
            }
            else if (selectedOperator[0].DATA_TYPE === IndexerDataTypes.DECIMAL
                || selectedOperator[0].DATA_TYPE === IndexerDataTypes.NUMBER) {
                this.charDataType = false;
                this.dateDataType = false;
                this.numberDataType = true;
            }
            else if (selectedOperator[0].DATA_TYPE === IndexerDataTypes.DATETIME) {
                this.charDataType = false;
                this.dateDataType = true;
                this.numberDataType = false;
                if (isInitial) {
                    if (typeof this.filterRowData.searchValue === 'string') {
                        // this.filterRowData.searchValue = new Date(this.filterRowData.searchValue.split('/').reverse().join('/'));
                        this.filterRowData.searchValue = new Date(this.filterRowData.searchValue);
                    }
                    if (this.filterRowData.hasTwoValues && typeof this.filterRowData.searchSecondValue === 'string') {
                        // this.filterRowData.searchSecondValue = new Date(this.filterRowData.searchSecondValue.split('/').reverse().join('/'));
                        this.filterRowData.searchSecondValue = new Date(this.filterRowData.searchSecondValue);
                    }
                }
            }
            else if (selectedOperator[0].DATA_TYPE === IndexerDataTypes.NUMBER) {
                this.charDataType = false;
                this.dateDataType = false;
                this.numberDataType = true;
            }
        }
    };
    AdvancedSearchFieldComponent.prototype.validateSelectedField = function () {
        var _this = this;
        // to load default field
        if (!this.filterRowData.searchField) {
            this.filterRowData.searchField = this.filterRowData.availableSearchFields[0].MPM_FIELD_CONFIG_ID;
        }
        // to load selected field
        if (this.filterRowData.searchField) {
            var selectedField_1 = this.filterRowData.availableSearchFields.filter(function (field) {
                return field.MPM_FIELD_CONFIG_ID === _this.filterRowData.searchField;
            });
            // to load selected field operators
            if (selectedField_1 && selectedField_1[0] && selectedField_1[0].DATA_TYPE) {
                this.availableFieldOperators = [];
                this.availableFieldOperators = this.advancedSearchConfigData.searchOperatorList.filter(function (operator) {
                    return selectedField_1[0].DATA_TYPE === operator.DATA_TYPE;
                });
            }
            if (selectedField_1[0].EDIT_TYPE === 'COMBO') {
                this.comboType = true;
                this.comboValues = this.utilService.getLookupDomainValuesById(selectedField_1[0].MPM_FIELD_CONFIG_ID);
                // get combo values;
            }
            if (selectedField_1 && selectedField_1[0] && selectedField_1[0].IS_DROP_DOWN === true) {
                this.isDropDown = true;
                this.formAutoSuggestValues(selectedField_1[0].MAPPER_NAME);
            }
            if (this.filterRowData.searchOperatorId) {
                this.loadFieldValueType(this.filterRowData.searchOperatorId, true);
            }
        }
    };
    AdvancedSearchFieldComponent.prototype.formAutoSuggestValues = function (event) {
        var _this = this;
        // custom-fields
        this.advancedSearchConfigData.availableSearchFields.forEach(function (searchFields) {
            if (searchFields.MAPPER_NAME === event || searchFields.MPM_FIELD_CONFIG_ID === event) {
                _this.level = searchFields.MAPPER_NAME.includes(MPM_LEVELS.PROJECT) ? MPM_LEVELS.PROJECT :
                    searchFields.MAPPER_NAME.includes(MPM_LEVELS.TASK) ? MPM_LEVELS.TASK : searchFields.MAPPER_NAME.includes(MPM_LEVELS.DELIVERABLE) ?
                        MPM_LEVELS.DELIVERABLE : searchFields.MAPPER_NAME.includes(MPM_LEVELS.CAMPAIGN) ? MPM_LEVELS.CAMPAIGN : '';
                searchFields.IS_DROP_DOWN = _this.isDropDown ? true : false;
            }
            if (searchFields.MPM_FIELD_CONFIG_ID === event && searchFields.IS_CUSTOM_METADATA === 'true') {
                // this.isDropDown = true;
                var categoryLevelDetails = _this.categoryService.getCategoryLevelDetailsByType(_this.level);
                _this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
                    .subscribe(function (metaDataModelDetails) {
                    console.log(metaDataModelDetails);
                    metaDataModelDetails.metadata_element_list.forEach(function (metadata) {
                        if (metadata.id === ProjectConstant.MPM_PROJECT_CUSTOM_METADATA_GROUP || metadata.id === ProjectConstant.MPM_CAMPAIGN_CUSTOM_METADATA_GROUP) {
                            metadata.metadata_element_list.forEach(function (data) {
                                if (data.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO && (data.id === searchFields.OTMM_FIELD_ID || data.domain_id === searchFields.OTMM_FIELD_ID)) {
                                    _this.isDropDown = true;
                                    searchFields.IS_DROP_DOWN = true;
                                    var customDatas = _this.utilService.getLookupDomainValuesById(data.domain_id);
                                    customDatas.forEach(function (customData) {
                                        _this.options.push(customData.display_value);
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
        // standard fields
        if (event === StandardProjectValue.PROJECT_TEAM || event === StandardTaskValue.TASK_TEAM || event === StandardDeliverableValue.DELIVERABLE_TEAM) {
            console.log(this.sharingService.getAllTeams());
            var allTeams = this.sharingService.getAllTeams();
            allTeams.forEach(function (teams) {
                _this.options.push(teams.NAME);
            });
            if (this.options && this.options.length > 0 && this.advancedSearchFieldForm) {
                this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                    return _this._filter(value);
                }));
            }
            console.log(this.options);
        }
        else if (event === StandardProjectValue.PROJECT_PRIORITY || event === StandardProjectValue.PROJECT_PRIORITY_ID || event === StandardTaskValue.TASK_PRIORITY_ID || event === StandardTaskValue.TASK_PRIORITY || event === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event === StandardDeliverableValue.DELIVERABLE_PRIORITY || event === StandardCampaignValue.CAMPAIGN_PRIORITY_ID || event === StandardCampaignValue.CAMPAIGN_PRIORITY) {
            /*  this.entityAppDefService.getPriorities(this.level).subscribe(priorities => {
               console.log(priorities);
               priorities.forEach(priority => {
                 if (event === StandardProjectValue.PROJECT_PRIORITY_ID || event === StandardTaskValue.TASK_PRIORITY_ID || event === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event === StandardCampaignValue.CAMPAIGN_PRIORITY_ID) {
                   this.options.push(priority["MPM_Priority-id"].Id);
                 } else {
                   this.options.push(priority.DESCRIPTION);
                 }
               });
               if (this.options && this.options.length > 0) {
                 this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
                   startWith(''),
                   map(value =>
                     this._filter(value)
       
                   )
                 );
               }
               console.log(this.options)
             }); */
            var priority = this.entityAppDefService.getPriorities(this.level);
            //  .subscribe(priorities => {
            console.log(priority);
            priority.forEach(function (priority) {
                if (event === StandardProjectValue.PROJECT_PRIORITY_ID || event === StandardTaskValue.TASK_PRIORITY_ID || event === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event === StandardCampaignValue.CAMPAIGN_PRIORITY_ID) {
                    _this.options.push(priority["MPM_Priority-id"].Id);
                }
                else {
                    _this.options.push(priority.DESCRIPTION);
                }
            });
            if (this.options && this.options.length > 0 && this.advancedSearchFieldForm) {
                this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                    return _this._filter(value);
                }));
            }
            console.log(this.options);
            // });
        }
        else if (event === StandardProjectValue.PROJECT_STATUS || event === StandardTaskValue.TASK_STATUS || event === StandardDeliverableValue.DELIVERABLE_STATUS || event === StandardCampaignValue.CAMPAIGN_STATUS) {
            /*  this.statusService.getStatusByCategoryLevelName(this.level).subscribe(statuses => {
               console.log(statuses);
               statuses.forEach(status => {
                 this.options.push(status.NAME);
               })
               console.log(this.options)
               if (this.options && this.options.length > 0) {
                 this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
                   startWith(''),
                   map(value =>
                     this._filter(value)
       
                   )
                 );
               }
             }); */
            var statuses = this.statusService.getAllStatusBycategoryName(this.level);
            var status_1 = statuses && statuses.length > 0 && Array.isArray(statuses) ? statuses[0] : statuses;
            status_1.forEach(function (status) {
                _this.options.push(status.NAME);
            });
            console.log(this.options);
            if (this.options && this.options.length > 0 && this.advancedSearchFieldForm) {
                this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                    return _this._filter(value);
                }));
            }
            /* this.statusService.getAllStatusBycategoryName(this.level).subscribe(statuses => {
                    console.log(statuses);
                    statuses.forEach(status => {
                      this.options.push(status.NAME);
                    })
                    console.log(this.options)
                    if (this.options && this.options.length > 0) {
                      this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
                        startWith(''),
                        map(value =>
                          this._filter(value)
            
                        )
                      );
                    }
                  }); */
        }
        else if (event === StandardProjectValue.PROJECT_OWNER_NAME || event === StandardTaskValue.TASK_OWNER_NAME || event === StandardDeliverableValue.DELIVERABLE_OWNER_NAME || event === StandardCampaignValue.CAMPAIGN_OWNER_NAME) {
            var allPersons = this.sharingService.getAllPersons();
            allPersons.forEach(function (persons) {
                if (persons && persons.DisplayName && persons.DisplayName.__text) {
                    _this.options.push(persons.DisplayName.__text);
                }
            });
            if (this.options && this.options.length > 0 && this.advancedSearchFieldForm) {
                this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                    return _this._filter(value);
                }));
            }
        }
        else if (event === StandardTaskValue.TASK_ROLE_NAME || event === StandardDeliverableValue.DELIVERABLE_APPROVER_ROLE_NAME || event === StandardDeliverableValue.DELIVERABLE_OWNER_ROLE_NAME) { //MPMV3-2008
            var allTeamRoles = this.sharingService.getAllTeamRoles();
            allTeamRoles.forEach(function (teamRole) {
                if (teamRole && teamRole.NAME) {
                    _this.options.push(teamRole.NAME);
                }
            });
            if (this.options && this.options.length > 0 && this.advancedSearchFieldForm) {
                this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                    return _this._filter(value);
                }));
            }
        }
        else if (event === StandardDeliverableValue.DELIVERABLE_APPROVER_NAME) {
            this.appService.getAllUsers().subscribe(function (allUsers) {
                if (allUsers && allUsers.User) {
                    allUsers.User.forEach(function (users) {
                        if (users && users.FullName && typeof (users.FullName) !== 'object') {
                            _this.options.push(users.FullName);
                        }
                    });
                    if (_this.options && _this.options.length > 0 && _this.advancedSearchFieldForm) {
                        _this.filteredOptions = _this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                            return _this._filter(value);
                        }));
                    }
                }
            });
        }
        else if (event === StandardProjectValue.PROJECT_CATEGORY) {
            this.projectService.getAllCategoryMetadata().subscribe(function (categoryDetails) {
                console.log(categoryDetails);
                var allcategoryMetadataList = categoryDetails.map(function (data) {
                    return __assign({ displayName: data.METADATA_NAME, value: data['MPM_Category_Metadata-id'].Id, name: data.METADATA_NAME }, data);
                });
                console.log(allcategoryMetadataList);
                allcategoryMetadataList.forEach(function (categoryMetadataList) {
                    if (categoryMetadataList && categoryMetadataList.name) {
                        _this.options.push(categoryMetadataList.name);
                    }
                });
                if (_this.options && _this.options.length > 0 && _this.advancedSearchFieldForm) {
                    _this.filteredOptions = _this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
                        return _this._filter(value);
                    }));
                }
            });
        }
    };
    AdvancedSearchFieldComponent.prototype.onFieldValueChange = function (event) {
        var _this = this;
        this.isDropDown = false;
        this.comboType = false;
        this.comboValues = [];
        this.options = [];
        var selectedFieldId = event.value;
        this.filterRowData.searchField = selectedFieldId;
        var selectedField = this.filterRowData.availableSearchFields.filter(function (field) {
            return field.MPM_FIELD_CONFIG_ID === selectedFieldId;
        });
        if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE === IndexerDataTypes.NUMBER) {
            selectedField[0].DATA_TYPE = IndexerDataTypes.NUMBER;
        }
        if (selectedField && selectedField[0] && selectedField[0].EDIT_TYPE === 'COMBO') {
            this.comboType = true;
            this.comboValues = this.utilService.getLookupDomainValuesById(selectedField[0].MPM_FIELD_CONFIG_ID);
            // get combo values;
        }
        if (!this.comboType) {
            if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE === IndexerDataTypes.STRING) {
                this.charDataType = true;
                this.dateDataType = false;
                this.numberDataType = false;
            }
            else if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE === IndexerDataTypes.DATETIME) {
                this.charDataType = false;
                this.dateDataType = true;
                this.numberDataType = false;
            }
            else if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE === IndexerDataTypes.NUMBER) {
                this.charDataType = false;
                this.dateDataType = false;
                this.numberDataType = true;
            }
        }
        else {
            this.charDataType = false;
            this.dateDataType = false;
            this.numberDataType = false;
        }
        // this.filterRowData.issearchValueRequired = false;
        // this.filterRowData.hasTwoValues = false;
        if (selectedField && selectedField[0] && selectedField[0].DATA_TYPE) {
            this.availableFieldOperators = [];
            this.availableFieldOperators = this.advancedSearchConfigData.searchOperatorList.filter(function (operator) {
                return selectedField[0].DATA_TYPE === operator.DATA_TYPE;
            });
        }
        /*   this.advancedSearchConfigData.availableSearchFields.forEach(searchFields => {
            if (searchFields.MAPPER_NAME === event.value || searchFields.MPM_FIELD_CONFIG_ID === event.value) {
              this.level = searchFields.MAPPER_NAME.includes(MPM_LEVELS.PROJECT) ? MPM_LEVELS.PROJECT : searchFields.MAPPER_NAME.includes(MPM_LEVELS.TASK) ? MPM_LEVELS.TASK : searchFields.MAPPER_NAME.includes(MPM_LEVELS.DELIVERABLE) ? MPM_LEVELS.DELIVERABLE : searchFields.MAPPER_NAME.includes(MPM_LEVELS.CAMPAIGN) ? MPM_LEVELS.CAMPAIGN : '';
              searchFields.IS_DROP_DOWN = true;
            }
            if (searchFields.MPM_FIELD_CONFIG_ID === event.value && searchFields.IS_CUSTOM_METADATA === 'true') {
              // this.isDropDown = true;
              const categoryLevelDetails = this.categoryService.getCategoryLevelDetailsByType(this.level);
              this.otmmService.getMetadatModelById(categoryLevelDetails.METADATA_MODEL_ID)
                .subscribe(metaDataModelDetails => {
                  console.log(metaDataModelDetails);
                  metaDataModelDetails.metadata_element_list.forEach(metadata => {
                    if (metadata.id === ProjectConstant.MPM_PROJECT_CUSTOM_METADATA_GROUP || metadata.id === ProjectConstant.MPM_CAMPAIGN_CUSTOM_METADATA_GROUP) {
                      console.log("inside if")
                      metadata.metadata_element_list.forEach(data => {
                        if (data.edit_type === ProjectConstant.METADATA_EDIT_TYPES.COMBO && data.id === searchFields.OTMM_FIELD_ID) {
                          this.isDropDown = true;
                          searchFields.IS_DROP_DOWN = true;
                          const customDatas = this.utilService.getLookupDomainValuesById(data.domain_id);
                          customDatas.forEach(customData => {
                            this.options.push(customData.display_value);
                          });
                        }
                      });
                    }
                  });
                });
            }
          }); */
        AdvancedSearchStandardValue.ADVANCED_SEARCH_STANDARD_VALUE.forEach(function (standardField) {
            if (standardField === event.value) {
                _this.isDropDown = true;
            }
        });
        this.formAutoSuggestValues(event.value);
        /*  if (event.value === StandardProjectValue.PROJECT_TEAM || event.value === StandardTaskValue.TASK_TEAM || event.value === StandardDeliverableValue.DELIVERABLE_TEAM) {
           console.log(this.sharingService.getAllTeams());
           const allTeams = this.sharingService.getAllTeams();
           allTeams.forEach(teams => {
             this.options.push(teams.NAME);
     
           });
           if (this.options && this.options.length > 0) {
             this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
               startWith(''),
               map(value =>
                 this._filter(value)
     
               )
             );
           }
           console.log(this.options)
         } else if (event.value === StandardProjectValue.PROJECT_PRIORITY || event.value === StandardProjectValue.PROJECT_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY_ID || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY) {
           this.entityAppDefService.getPriorities(this.level).subscribe(priorities => {
             console.log(priorities);
             priorities.forEach(priority => {
               if (event.value === StandardProjectValue.PROJECT_PRIORITY_ID || event.value === StandardTaskValue.TASK_PRIORITY_ID || event.value === StandardDeliverableValue.DELIVERABLE_PRIORITY_ID || event.value === StandardCampaignValue.CAMPAIGN_PRIORITY_ID) {
                 this.options.push(priority["MPM_Priority-id"].Id);
               } else {
                 this.options.push(priority.DESCRIPTION);
               }
             });
             if (this.options && this.options.length > 0) {
               this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
                 startWith(''),
                 map(value =>
                   this._filter(value)
     
                 )
               );
             }
             console.log(this.options)
           });
     
         } else if (event.value === StandardProjectValue.PROJECT_STATUS || event.value === StandardTaskValue.TASK_STATUS || event.value === StandardDeliverableValue.DELIVERABLE_STATUS || event.value === StandardCampaignValue.CAMPAIGN_STATUS) {
           this.statusService.getStatusByCategoryLevelName(this.level).subscribe(statuses => {
             console.log(statuses);
             statuses.forEach(status => {
               this.options.push(status.NAME);
             })
             console.log(this.options)
             if (this.options && this.options.length > 0) {
               this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
                 startWith(''),
                 map(value =>
                   this._filter(value)
     
                 )
               );
             }
           });
     
         } else if (event.value === StandardProjectValue.PROJECT_OWNER_NAME || event.value === StandardTaskValue.TASK_OWNER_NAME || event.value === StandardDeliverableValue.DELIVERABLE_OWNER_NAME || event.value === StandardCampaignValue.CAMPAIGN_OWNER_NAME) {
           const allPersons = this.sharingService.getAllPersons();
           allPersons.forEach(persons => {
             if (persons && persons.DisplayName && persons.DisplayName.__text) {
               this.options.push(persons.DisplayName.__text);
             }
     
           });
           if (this.options && this.options.length > 0) {
             this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(
               startWith(''),
               map(value =>
                 this._filter(value)
     
               )
             );
           }
           console.log(this.options)
         } */
    };
    AdvancedSearchFieldComponent.prototype._filter = function (value) {
        console.log(value);
        var filterValue = value;
        try {
            filterValue = value.toLowerCase();
        }
        catch (exception) {
            filterValue = value;
        }
        // const filterValue = value.toLowerCase();
        return this.options.filter(function (option) { return option.toLowerCase().indexOf(filterValue) === 0; });
    };
    AdvancedSearchFieldComponent.prototype.initializeForm = function () {
        var _this = this;
        this.validateSelectedField();
        this.advancedSearchFieldForm = new FormGroup({
            field: new FormControl(this.filterRowData.searchField),
            fieldType: new FormControl(this.filterRowData.searchOperatorId),
            fieldValue: new FormControl(this.filterRowData.searchValue),
            fieldSecondValue: new FormControl(this.filterRowData.searchSecondValue),
        });
        this.advancedSearchFieldForm.controls.fieldType.valueChanges.subscribe(function (selectedOperatorId) {
            _this.loadFieldValueType(selectedOperatorId, false);
        });
        this.advancedSearchFieldForm.controls.fieldValue.valueChanges.subscribe(function (selectedValue) {
            _this.filterRowData.searchValue = selectedValue;
        });
        this.advancedSearchFieldForm.controls.fieldSecondValue.valueChanges.subscribe(function (selectedValue) {
            _this.filterRowData.searchSecondValue = selectedValue;
        });
        //if (this.options && this.options.length > 0) {
        this.filteredOptions = this.advancedSearchFieldForm.get('fieldValue').valueChanges.pipe(startWith(''), map(function (value) {
            return _this._filter(value);
        }));
        // }
    };
    AdvancedSearchFieldComponent.prototype.changeAutoComplete = function (event) {
        console.log(event);
    };
    AdvancedSearchFieldComponent.prototype.filterSelection = function (selectionValue) {
        if (selectionValue) {
            var isValid = false;
            if (this.filterRowData.searchField) {
                isValid = true;
            }
            if (isValid) {
                if (this.comboType) {
                    this.filterRowData.searchOperatorId = 'ARTESIA.OPERATOR.CHAR.CONTAINS';
                    this.filterRowData.searchOperatorName = 'contains';
                    this.filterRowData.isComboType = true;
                }
                this.filterRowData.isFilterSelected = true;
                this.advancedSearchFieldForm.controls.field.disable();
                this.filterSelected.next(this.filterRowData);
            }
            else {
                this.notificationService.info('Kindly select all values first');
            }
        }
    };
    AdvancedSearchFieldComponent.prototype.removeFilter = function () {
        this.removeFilterField.next(this.filterRowData);
    };
    AdvancedSearchFieldComponent.prototype.ngOnInit = function () {
        this.initializeForm();
        if (navigator.language !== undefined) {
            this.adapter.setLocale(navigator.language);
        }
        if (this.filterRowData.isFilterSelected) {
            this.advancedSearchFieldForm.controls.field.disable();
        }
    };
    AdvancedSearchFieldComponent.ctorParameters = function () { return [
        { type: DateAdapter },
        { type: UtilService },
        { type: NotificationService },
        { type: SharingService },
        { type: StatusService },
        { type: EntityAppDefService },
        { type: CategoryService },
        { type: OTMMService },
        { type: AppService },
        { type: ProjectService }
    ]; };
    __decorate([
        Input()
    ], AdvancedSearchFieldComponent.prototype, "advancedSearchConfigData", void 0);
    __decorate([
        Input()
    ], AdvancedSearchFieldComponent.prototype, "filterRowData", void 0);
    __decorate([
        Output()
    ], AdvancedSearchFieldComponent.prototype, "filterSelected", void 0);
    __decorate([
        Output()
    ], AdvancedSearchFieldComponent.prototype, "removeFilterField", void 0);
    AdvancedSearchFieldComponent = __decorate([
        Component({
            selector: 'mpm-advanced-search-field',
            template: "<div>\r\n    <form [formGroup]=\"advancedSearchFieldForm\">\r\n        <mat-form-field appearance=\"outline\">\r\n            <mat-label>Field</mat-label>\r\n            <mat-select formControlName=\"field\" (selectionChange)=\"onFieldValueChange($event)\">\r\n                <mat-option *ngFor=\"let field of filterRowData.availableSearchFields\"\r\n                    value=\"{{field.MPM_FIELD_CONFIG_ID}}\">\r\n                    {{field.DISPLAY_NAME}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"!comboType\">\r\n            <mat-label>Select</mat-label>\r\n            <mat-select formControlName=\"fieldType\">\r\n                <mat-option *ngFor=\"let operator of availableFieldOperators\" value=\"{{operator.OPERATOR_ID}}\">\r\n                    {{operator.NAME}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"requireFieldValue && !dateDataType && !comboType\">\r\n            <input *ngIf=\"charDataType\" formControlName=\"fieldValue\" matInput placeholder=\"Value to be searched\">\r\n            <input *ngIf=\"numberDataType\" formControlName=\"fieldValue\" matInput placeholder=\"Value to be searched\">\r\n        </mat-form-field>\r\n        <mat-form-field class=\"combo\" appearance=\"outline\" *ngIf=\"requireFieldValue && comboType\">\r\n            <mat-label>Select</mat-label>\r\n            <mat-select formControlName=\"fieldValue\" multiple>\r\n                <mat-option *ngFor=\"let item of comboValues\" [value]=\"item\">{{item.display_value}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"requireFieldValue && dateDataType\">\r\n            <input formControlName=\"fieldValue\" matInput [matDatepicker]=\"fieldDate\"\r\n                placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"fieldDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #fieldDate></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"requireFieldValue && filterRowData.hasTwoValues && numberDataType\">\r\n            <input formControlName=\"fieldSecondValue\" matInput placeholder=\"Value to be searched\">\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"outline\" *ngIf=\"requireFieldValue && filterRowData.hasTwoValues && dateDataType\">\r\n            <input formControlName=\"fieldSecondValue\" matInput [matDatepicker]=\"fieldSecondDate\"\r\n                placeholder=\"Choose a date (MM/DD/YYYY)\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"fieldSecondDate\"></mat-datepicker-toggle>\r\n            <mat-datepicker #fieldSecondDate></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <button class=\"field-row-action\" *ngIf=\"!filterRowData.isFilterSelected\" mat-icon-button color=\"primary\"\r\n            matTooltip=\"Save this Condition\" (click)=\"filterSelection(true)\">\r\n            <mat-icon>done</mat-icon>\r\n        </button>\r\n        <button class=\"field-row-action\" (click)=\"removeFilter()\" mat-icon-button color=\"primary\"\r\n            matTooltip=\"Remove this Condition\">\r\n            <mat-icon>close</mat-icon>\r\n        </button>\r\n    </form>\r\n</div>",
            styles: ["mat-form-field{font-size:12px!important;margin:5px}.mat-form-field{width:212px!important}mat-checkbox.field-row-action{margin-left:7px}mat-form-field.combo{max-width:400px!important}"]
        })
    ], AdvancedSearchFieldComponent);
    return AdvancedSearchFieldComponent;
}());
export { AdvancedSearchFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWR2YW5jZWQtc2VhcmNoLWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9hZHZhbmNlZC1zZWFyY2gtZmllbGQvYWR2YW5jZWQtc2VhcmNoLWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFHcEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0RBQXdELENBQUM7QUFJMUYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnREFBZ0QsQ0FBQztBQUNyRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDakYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFFL0UsT0FBTyxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ25GLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxxQkFBcUIsRUFBRSx3QkFBd0IsRUFBRSxvQkFBb0IsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBTy9LO0lBcUJFLHNDQUNTLE9BQXlCLEVBQ3pCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxjQUE4QixFQUM5QixhQUE0QixFQUM1QixtQkFBd0MsRUFDeEMsZUFBZ0MsRUFDaEMsV0FBd0IsRUFDeEIsVUFBc0IsRUFDdEIsY0FBOEI7UUFUOUIsWUFBTyxHQUFQLE9BQU8sQ0FBa0I7UUFDekIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUExQjdCLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN6QyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBS3RELHNCQUFpQixHQUFHLElBQUksQ0FBQztRQUN6QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBRWpCLFlBQU8sR0FBRyxFQUFFLENBQUM7SUFjVCxDQUFDO0lBRUwsb0RBQWEsR0FBYixVQUFjLFVBQVU7UUFDdEIsSUFBTSxTQUFTLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0Q7YUFBTTtZQUNMLE9BQU8sVUFBVSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELHlEQUFrQixHQUFsQixVQUFtQixrQkFBa0IsRUFBRSxTQUFTO1FBQWhELGlCQXFFQztRQXBFQyxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixHQUFHLGtCQUFrQixDQUFDO1FBQ3pELElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxXQUFXLEtBQUssa0JBQWtCLEVBQTFDLENBQTBDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdEksSUFBTSxhQUFhLEdBQWEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxLQUFLO1lBQ2pGLE9BQU8sQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEtBQUssS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN4RSxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDZCxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUMxQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVE7Z0JBQ3ZFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFO2dCQUNsRCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDakU7WUFDRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ25JLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZFO1NBQ0Y7UUFDRCxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxRQUFRO1lBQ3ZGLE9BQU8sUUFBUSxDQUFDLFdBQVcsS0FBSyxrQkFBa0IsSUFBSSxRQUFRLENBQUMsU0FBUyxLQUFLLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFDdkcsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRTtZQUMxRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO1NBQ2pEO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1NBQ2xEO1FBQ0QsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssQ0FBQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN2QyxJQUFJLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7bUJBQ25HLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUM3RCxJQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDakQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEQ7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbkIsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLENBQUMsTUFBTTttQkFDeEQsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtnQkFDL0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQzthQUM3QjtpQkFBTSxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxnQkFBZ0IsQ0FBQyxPQUFPO21CQUNoRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dCQUM5RCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzVCO2lCQUFNLElBQUksZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtnQkFDdEUsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztnQkFDNUIsSUFBSSxTQUFTLEVBQUU7b0JBQ2IsSUFBSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxLQUFLLFFBQVEsRUFBRTt3QkFDdEQsNEdBQTRHO3dCQUM1RyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUMzRTtvQkFDRCxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxJQUFJLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsS0FBSyxRQUFRLEVBQUU7d0JBQy9GLHdIQUF3SDt3QkFDeEgsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUM7cUJBQ3ZGO2lCQUNGO2FBQ0Y7aUJBQU0sSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dCQUNwRSxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzVCO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsNERBQXFCLEdBQXJCO1FBQUEsaUJBa0NDO1FBakNDLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUU7WUFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQztTQUNsRztRQUVELHlCQUF5QjtRQUN6QixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQ2xDLElBQU0sZUFBYSxHQUFlLElBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLFVBQUEsS0FBSztnQkFDckYsT0FBTyxLQUFLLENBQUMsbUJBQW1CLEtBQUssS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7WUFDdEUsQ0FBQyxDQUFDLENBQUM7WUFDSCxtQ0FBbUM7WUFDbkMsSUFBSSxlQUFhLElBQUksZUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLGVBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0JBQ25FLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7Z0JBQ2xDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLFVBQUEsUUFBUTtvQkFDN0YsT0FBTyxlQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLFFBQVEsQ0FBQyxTQUFTLENBQUM7Z0JBQzNELENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFFRCxJQUFJLGVBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssT0FBTyxFQUFFO2dCQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLGVBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNwRyxvQkFBb0I7YUFDckI7WUFFRCxJQUFJLGVBQWEsSUFBSSxlQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksZUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksS0FBSyxJQUFJLEVBQUU7Z0JBQy9FLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixJQUFJLENBQUMscUJBQXFCLENBQUMsZUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQzFEO1lBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixFQUFFO2dCQUN2QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNwRTtTQUNGO0lBQ0gsQ0FBQztJQUVELDREQUFxQixHQUFyQixVQUFzQixLQUFLO1FBQTNCLGlCQTBOQztRQXhOQyxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVk7WUFDdEUsSUFBSSxZQUFZLENBQUMsV0FBVyxLQUFLLEtBQUssSUFBSSxZQUFZLENBQUMsbUJBQW1CLEtBQUssS0FBSyxFQUFFO2dCQUNwRixLQUFJLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN2RixZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO3dCQUNoSSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDL0csWUFBWSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUM1RDtZQUNELElBQUksWUFBWSxDQUFDLG1CQUFtQixLQUFLLEtBQUssSUFBSSxZQUFZLENBQUMsa0JBQWtCLEtBQUssTUFBTSxFQUFFO2dCQUM1RiwwQkFBMEI7Z0JBQzFCLElBQU0sb0JBQW9CLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyw2QkFBNkIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzVGLEtBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsb0JBQW9CLENBQUMsaUJBQWlCLENBQUM7cUJBQ3pFLFNBQVMsQ0FBQyxVQUFBLG9CQUFvQjtvQkFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUNsQyxvQkFBb0IsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO3dCQUN6RCxJQUFJLFFBQVEsQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLGlDQUFpQyxJQUFJLFFBQVEsQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLGtDQUFrQyxFQUFFOzRCQUMzSSxRQUFRLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQ0FDekMsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLFlBQVksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxZQUFZLENBQUMsYUFBYSxDQUFDLEVBQUU7b0NBQzdKLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29DQUN2QixZQUFZLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQ0FDakMsSUFBTSxXQUFXLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0NBQy9FLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO3dDQUM1QixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7b0NBQzlDLENBQUMsQ0FBQyxDQUFDO2lDQUNKOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKO29CQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILGtCQUFrQjtRQUNsQixJQUFJLEtBQUssS0FBSyxvQkFBb0IsQ0FBQyxZQUFZLElBQUksS0FBSyxLQUFLLGlCQUFpQixDQUFDLFNBQVMsSUFBSSxLQUFLLEtBQUssd0JBQXdCLENBQUMsZ0JBQWdCLEVBQUU7WUFDL0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuRCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztnQkFDcEIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWhDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7Z0JBQzNFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNyRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSztvQkFDUCxPQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUFuQixDQUFtQixDQUVwQixDQUNGLENBQUM7YUFDSDtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQzFCO2FBQU0sSUFBSSxLQUFLLEtBQUssb0JBQW9CLENBQUMsZ0JBQWdCLElBQUksS0FBSyxLQUFLLG9CQUFvQixDQUFDLG1CQUFtQixJQUFJLEtBQUssS0FBSyxpQkFBaUIsQ0FBQyxnQkFBZ0IsSUFBSSxLQUFLLEtBQUssaUJBQWlCLENBQUMsYUFBYSxJQUFJLEtBQUssS0FBSyx3QkFBd0IsQ0FBQyx1QkFBdUIsSUFBSSxLQUFLLEtBQUssd0JBQXdCLENBQUMsb0JBQW9CLElBQUksS0FBSyxLQUFLLHFCQUFxQixDQUFDLG9CQUFvQixJQUFJLEtBQUssS0FBSyxxQkFBcUIsQ0FBQyxpQkFBaUIsRUFBRTtZQUNuYjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzttQkFtQk87WUFFUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNqRSw4QkFBOEI7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTtnQkFDdkIsSUFBSSxLQUFLLEtBQUssb0JBQW9CLENBQUMsbUJBQW1CLElBQUksS0FBSyxLQUFLLGlCQUFpQixDQUFDLGdCQUFnQixJQUFJLEtBQUssS0FBSyx3QkFBd0IsQ0FBQyx1QkFBdUIsSUFBSSxLQUFLLEtBQUsscUJBQXFCLENBQUMsb0JBQW9CLEVBQUU7b0JBQzVOLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNuRDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7aUJBQ3pDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtnQkFDM0UsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQ3JGLFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixHQUFHLENBQUMsVUFBQSxLQUFLO29CQUNQLE9BQUEsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQW5CLENBQW1CLENBRXBCLENBQ0YsQ0FBQzthQUNIO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDekIsTUFBTTtTQUVQO2FBQU0sSUFBSSxLQUFLLEtBQUssb0JBQW9CLENBQUMsY0FBYyxJQUFJLEtBQUssS0FBSyxpQkFBaUIsQ0FBQyxXQUFXLElBQUksS0FBSyxLQUFLLHdCQUF3QixDQUFDLGtCQUFrQixJQUFJLEtBQUssS0FBSyxxQkFBcUIsQ0FBQyxlQUFlLEVBQUU7WUFDL007Ozs7Ozs7Ozs7Ozs7OzttQkFlTztZQUVQLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ3hFLElBQUksUUFBTSxHQUFRLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztZQUN0RyxRQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTtnQkFDbkIsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pDLENBQUMsQ0FBQyxDQUFBO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDekIsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7Z0JBQzNFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNyRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSztvQkFDUCxPQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUFuQixDQUFtQixDQUVwQixDQUNGLENBQUM7YUFDSDtZQUNEOzs7Ozs7Ozs7Ozs7Ozs7d0JBZVk7U0FDYjthQUFNLElBQUksS0FBSyxLQUFLLG9CQUFvQixDQUFDLGtCQUFrQixJQUFJLEtBQUssS0FBSyxpQkFBaUIsQ0FBQyxlQUFlLElBQUksS0FBSyxLQUFLLHdCQUF3QixDQUFDLHNCQUFzQixJQUFJLEtBQUssS0FBSyxxQkFBcUIsQ0FBQyxtQkFBbUIsRUFBRTtZQUMvTixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3ZELFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUN4QixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFO29CQUNoRSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUMvQztZQUVILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7Z0JBQzNFLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNyRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSztvQkFDUCxPQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUFuQixDQUFtQixDQUVwQixDQUNGLENBQUM7YUFDSDtTQUNGO2FBQU0sSUFBSSxLQUFLLEtBQUssaUJBQWlCLENBQUMsY0FBYyxJQUFJLEtBQUssS0FBSyx3QkFBd0IsQ0FBQyw4QkFBOEIsSUFBSSxLQUFLLEtBQUssd0JBQXdCLENBQUMsMkJBQTJCLEVBQUUsRUFBRSxZQUFZO1lBQzFNLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDM0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7Z0JBQzNCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7b0JBQzdCLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDbEM7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO2dCQUMzRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDckYsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLEdBQUcsQ0FBQyxVQUFBLEtBQUs7b0JBQ1AsT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFBbkIsQ0FBbUIsQ0FFcEIsQ0FDRixDQUFDO2FBQ0g7U0FDRjthQUFNLElBQUksS0FBSyxLQUFLLHdCQUF3QixDQUFDLHlCQUF5QixFQUFFO1lBQ3ZFLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDOUMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtvQkFDN0IsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO3dCQUN6QixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsUUFBUSxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssUUFBUSxFQUFFOzRCQUNuRSxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ25DO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksS0FBSSxDQUFDLE9BQU8sSUFBSSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSSxDQUFDLHVCQUF1QixFQUFFO3dCQUMzRSxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDckYsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLEdBQUcsQ0FBQyxVQUFBLEtBQUs7NEJBQ1AsT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQzt3QkFBbkIsQ0FBbUIsQ0FDcEIsQ0FDRixDQUFDO3FCQUNIO2lCQUNGO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNLElBQUksS0FBSyxLQUFLLG9CQUFvQixDQUFDLGdCQUFnQixFQUFFO1lBQzFELElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxlQUFlO2dCQUNwRSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUM3QixJQUFNLHVCQUF1QixHQUFHLGVBQWUsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO29CQUN0RCxrQkFBUyxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxJQUFLLElBQUksRUFBRztnQkFDNUgsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO2dCQUNyQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxvQkFBb0I7b0JBQ2xELElBQUksb0JBQW9CLElBQUksb0JBQW9CLENBQUMsSUFBSSxFQUFFO3dCQUNyRCxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDOUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxLQUFJLENBQUMsT0FBTyxJQUFJLEtBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFJLENBQUMsdUJBQXVCLEVBQUU7b0JBQzNFLEtBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNyRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSzt3QkFDUCxPQUFBLEtBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO29CQUFuQixDQUFtQixDQUNwQixDQUNGLENBQUM7aUJBQ0g7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVELHlEQUFrQixHQUFsQixVQUFtQixLQUFLO1FBQXhCLGlCQStKQztRQTlKQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFNLGVBQWUsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQztRQUNqRCxJQUFNLGFBQWEsR0FBZSxJQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxVQUFBLEtBQUs7WUFDckYsT0FBTyxLQUFLLENBQUMsbUJBQW1CLEtBQUssZUFBZSxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLENBQUMsTUFBTSxFQUFFO1lBQy9GLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUMsTUFBTSxDQUFDO1NBQ3REO1FBQ0QsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssT0FBTyxFQUFFO1lBQy9FLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUNwRyxvQkFBb0I7U0FDckI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNuQixJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQy9GLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7YUFDN0I7aUJBQU0sSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUN4RyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2FBQzdCO2lCQUFNLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDdEcsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQzthQUM1QjtTQUNGO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQztTQUM3QjtRQUNELG9EQUFvRDtRQUNwRCwyQ0FBMkM7UUFDM0MsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7WUFDbkUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLEVBQUUsQ0FBQztZQUNsQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxVQUFBLFFBQVE7Z0JBQzdGLE9BQU8sYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsU0FBUyxDQUFDO1lBQzNELENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztnQkE0QlE7UUFFUiwyQkFBMkIsQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsVUFBQSxhQUFhO1lBQzlFLElBQUksYUFBYSxLQUFLLEtBQUssQ0FBQyxLQUFLLEVBQUU7Z0JBQ2pDLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQ3hCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXhDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7YUEyRUs7SUFDUCxDQUFDO0lBRU0sOENBQU8sR0FBZCxVQUFlLEtBQWE7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNsQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSTtZQUNGLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDbkM7UUFBQyxPQUFPLFNBQVMsRUFBRTtZQUNsQixXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQ3JCO1FBQ0QsMkNBQTJDO1FBRTNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFFRCxxREFBYyxHQUFkO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUMzQyxLQUFLLEVBQUUsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7WUFDdEQsU0FBUyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUM7WUFDL0QsVUFBVSxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO1lBQzNELGdCQUFnQixFQUFFLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUM7U0FDeEUsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLGtCQUFrQjtZQUN2RixLQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUEsYUFBYTtZQUNuRixLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUM7UUFDakQsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhO1lBQ3pGLEtBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEdBQUcsYUFBYSxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFDO1FBQ0gsZ0RBQWdEO1FBQ2hELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNyRixTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsR0FBRyxDQUFDLFVBQUEsS0FBSztZQUNQLE9BQUEsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7UUFBbkIsQ0FBbUIsQ0FFcEIsQ0FDRixDQUFDO1FBQ0YsSUFBSTtJQUNOLENBQUM7SUFFRCx5REFBa0IsR0FBbEIsVUFBbUIsS0FBSztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO0lBQ3BCLENBQUM7SUFFRCxzREFBZSxHQUFmLFVBQWdCLGNBQWM7UUFDNUIsSUFBSSxjQUFjLEVBQUU7WUFDbEIsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3BCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xDLE9BQU8sR0FBRyxJQUFJLENBQUM7YUFDaEI7WUFDRCxJQUFJLE9BQU8sRUFBRTtnQkFDWCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEdBQUcsZ0NBQWdDLENBQUM7b0JBQ3ZFLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEdBQUcsVUFBVSxDQUFDO29CQUNuRCxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ3ZDO2dCQUNELElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUMzQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQzlDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQzthQUNqRTtTQUNGO0lBQ0gsQ0FBQztJQUVELG1EQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQsK0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLFNBQVMsQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUM1QztRQUNELElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsRUFBRTtZQUN2QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN2RDtJQUNILENBQUM7O2dCQTlrQmlCLFdBQVc7Z0JBQ1AsV0FBVztnQkFDSCxtQkFBbUI7Z0JBQ3hCLGNBQWM7Z0JBQ2YsYUFBYTtnQkFDUCxtQkFBbUI7Z0JBQ3ZCLGVBQWU7Z0JBQ25CLFdBQVc7Z0JBQ1osVUFBVTtnQkFDTixjQUFjOztJQTdCOUI7UUFBUixLQUFLLEVBQUU7a0ZBQW9EO0lBQ25EO1FBQVIsS0FBSyxFQUFFO3VFQUE4QjtJQUU1QjtRQUFULE1BQU0sRUFBRTt3RUFBMEM7SUFDekM7UUFBVCxNQUFNLEVBQUU7MkVBQTZDO0lBTjNDLDRCQUE0QjtRQUx4QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsMkJBQTJCO1lBQ3JDLGc3R0FBcUQ7O1NBRXRELENBQUM7T0FDVyw0QkFBNEIsQ0FzbUJ4QztJQUFELG1DQUFDO0NBQUEsQUF0bUJELElBc21CQztTQXRtQlksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NvcmUnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3IgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvb2JqZWN0cy9NUE1TZWFyY2hPcGVyYXRvcic7XHJcbmltcG9ydCB7IEFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YSB9IGZyb20gJy4uL29iamVjdHMvQWR2YW5jZWRTZWFyY2hDb25maWdEYXRhJztcclxuaW1wb3J0IHsgSW5kZXhlckRhdGFUeXBlcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL29iamVjdHMvSW5kZXhlckRhdGFUeXBlcyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBGaWx0ZXJSb3dEYXRhIH0gZnJvbSAnLi4vb2JqZWN0cy9GaWx0ZXJSb3dEYXRhJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3Vic2NyaWJlciwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zdGF0dXMuc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQcm9qZWN0U2VydmljZSB9IGZyb20gJy4uLy4uL3Byb2plY3Qvc2hhcmVkL3NlcnZpY2VzL3Byb2plY3Quc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBtYXAsIHN0YXJ0V2l0aCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgUHJvamVjdENvbnN0YW50IH0gZnJvbSAnLi4vLi4vcHJvamVjdC9wcm9qZWN0LW92ZXJ2aWV3L3Byb2plY3QuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hTdGFuZGFyZFZhbHVlLCBTdGFuZGFyZENhbXBhaWduVmFsdWUsIFN0YW5kYXJkRGVsaXZlcmFibGVWYWx1ZSwgU3RhbmRhcmRQcm9qZWN0VmFsdWUsIFN0YW5kYXJkVGFza1ZhbHVlIH0gZnJvbSAnLi4vb2JqZWN0cy9BZHZhbmNlZFNlYXJjaFN0YW5kYXJkVmFsdWUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tYWR2YW5jZWQtc2VhcmNoLWZpZWxkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vYWR2YW5jZWQtc2VhcmNoLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9hZHZhbmNlZC1zZWFyY2gtZmllbGQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWR2YW5jZWRTZWFyY2hGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIGFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YTogQWR2YW5jZWRTZWFyY2hDb25maWdEYXRhO1xyXG4gIEBJbnB1dCgpIGZpbHRlclJvd0RhdGE6IEZpbHRlclJvd0RhdGE7XHJcblxyXG4gIEBPdXRwdXQoKSBmaWx0ZXJTZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gIEBPdXRwdXQoKSByZW1vdmVGaWx0ZXJGaWVsZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICBmaWx0ZXJlZE9wdGlvbnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gIGFkdmFuY2VkU2VhcmNoRmllbGRGb3JtOiBGb3JtR3JvdXA7XHJcbiAgYXZhaWxhYmxlRmllbGRPcGVyYXRvcnM6IE1QTVNlYXJjaE9wZXJhdG9yW107XHJcbiAgcmVxdWlyZUZpZWxkVmFsdWUgPSB0cnVlO1xyXG4gIGNoYXJEYXRhVHlwZSA9IHRydWU7XHJcbiAgZGF0ZURhdGFUeXBlID0gZmFsc2U7XHJcbiAgbnVtYmVyRGF0YVR5cGUgPSBmYWxzZTtcclxuICBjb21ib1R5cGUgPSBmYWxzZTtcclxuICBjb21ib1ZhbHVlcyA9IFtdO1xyXG4gIGlzRHJvcERvd247XHJcbiAgb3B0aW9ucyA9IFtdO1xyXG4gIGxldmVsO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhZGFwdGVyOiBEYXRlQWRhcHRlcjxhbnk+LFxyXG4gICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBzdGF0dXNTZXJ2aWNlOiBTdGF0dXNTZXJ2aWNlLFxyXG4gICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwdWJsaWMgY2F0ZWdvcnlTZXJ2aWNlOiBDYXRlZ29yeVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgcHJvamVjdFNlcnZpY2U6IFByb2plY3RTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgY29udmVydFRvRGF0ZShkYXRlU3RyaW5nKSB7XHJcbiAgICBjb25zdCBkYXRlQXJyYXkgPSBkYXRlU3RyaW5nLnNwbGl0KCcvJyk7XHJcbiAgICBpZiAoZGF0ZUFycmF5Lmxlbmd0aCA+IDApIHtcclxuICAgICAgcmV0dXJuIG5ldyBEYXRlKGRhdGVBcnJheVsyXSwgZGF0ZUFycmF5WzFdIC0gMSwgZGF0ZUFycmF5WzBdKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBkYXRlU3RyaW5nO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZEZpZWxkVmFsdWVUeXBlKHNlbGVjdGVkT3BlcmF0b3JJZCwgaXNJbml0aWFsKSB7XHJcbiAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCA9IHNlbGVjdGVkT3BlcmF0b3JJZDtcclxuICAgIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hPcGVyYXRvck5hbWUgPSB0aGlzLmF2YWlsYWJsZUZpZWxkT3BlcmF0b3JzLmZpbmQoZWxlbWVudCA9PiBlbGVtZW50Lk9QRVJBVE9SX0lEID09PSBzZWxlY3RlZE9wZXJhdG9ySWQpLk5BTUU7XHJcbiAgICBjb25zdCBzZWxlY3RlZEZpZWxkOiBNUE1GaWVsZCA9IHRoaXMuZmlsdGVyUm93RGF0YS5hdmFpbGFibGVTZWFyY2hGaWVsZHMuZmluZChmaWVsZCA9PiB7XHJcbiAgICAgIHJldHVybiAoZmllbGQuTVBNX0ZJRUxEX0NPTkZJR19JRCA9PT0gdGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaEZpZWxkKTtcclxuICAgIH0pO1xyXG4gICAgaWYgKCFpc0luaXRpYWwpIHtcclxuICAgICAgdGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlID0gJyc7XHJcbiAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hWYWx1ZSA9ICcnO1xyXG4gICAgICBpZiAodGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSAmJiB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmNvbnRyb2xzICYmXHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5jb250cm9scy5maWVsZFZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5jb250cm9scy5maWVsZFZhbHVlLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtICYmIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uY29udHJvbHMgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5jb250cm9scy5maWVsZFNlY29uZFZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5jb250cm9scy5maWVsZFNlY29uZFZhbHVlLnBhdGNoVmFsdWUoJycpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zdCBzZWxlY3RlZE9wZXJhdG9yID0gdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZ0RhdGEuc2VhcmNoT3BlcmF0b3JMaXN0LmZpbHRlcihvcGVyYXRvciA9PiB7XHJcbiAgICAgIHJldHVybiBvcGVyYXRvci5PUEVSQVRPUl9JRCA9PT0gc2VsZWN0ZWRPcGVyYXRvcklkICYmIG9wZXJhdG9yLkRBVEFfVFlQRSA9PT0gc2VsZWN0ZWRGaWVsZC5EQVRBX1RZUEU7XHJcbiAgICB9KTtcclxuICAgIGlmIChzZWxlY3RlZE9wZXJhdG9yWzBdICYmIHNlbGVjdGVkT3BlcmF0b3JbMF0uVkFMVUVfQ09VTlQpIHtcclxuICAgICAgdGhpcy5yZXF1aXJlRmllbGRWYWx1ZSA9IHRydWU7XHJcbiAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5pc3NlYXJjaFZhbHVlUmVxdWlyZWQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yZXF1aXJlRmllbGRWYWx1ZSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0RhdGEuaXNzZWFyY2hWYWx1ZVJlcXVpcmVkID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoc2VsZWN0ZWRPcGVyYXRvclswXS5WQUxVRV9DT1VOVCA9PT0gMikge1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0RhdGEuaGFzVHdvVmFsdWVzID0gdHJ1ZTtcclxuICAgICAgaWYgKHR5cGVvZiB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUgPT09ICdzdHJpbmcnICYmIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hWYWx1ZS5pbmNsdWRlcygnYW5kJylcclxuICAgICAgICAmJiB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUuc3BsaXQoJyBhbmQgJykubGVuZ3RoID4gMSkge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaFZhbHVlcyA9IHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hWYWx1ZS5zcGxpdCgnIGFuZCAnKTtcclxuICAgICAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUgPSBzZWFyY2hWYWx1ZXNbMF07XHJcbiAgICAgICAgdGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlID0gc2VhcmNoVmFsdWVzWzFdO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0RhdGEuaGFzVHdvVmFsdWVzID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuY29tYm9UeXBlKSB7XHJcbiAgICAgIGlmIChzZWxlY3RlZE9wZXJhdG9yWzBdLkRBVEFfVFlQRSA9PT0gSW5kZXhlckRhdGFUeXBlcy5TVFJJTkdcclxuICAgICAgICB8fCBzZWxlY3RlZE9wZXJhdG9yWzBdLkRBVEFfVFlQRSA9PT0gSW5kZXhlckRhdGFUeXBlcy5CT09MRUFOKSB7XHJcbiAgICAgICAgdGhpcy5jaGFyRGF0YVR5cGUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZGF0ZURhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5udW1iZXJEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdGVkT3BlcmF0b3JbMF0uREFUQV9UWVBFID09PSBJbmRleGVyRGF0YVR5cGVzLkRFQ0lNQUxcclxuICAgICAgICB8fCBzZWxlY3RlZE9wZXJhdG9yWzBdLkRBVEFfVFlQRSA9PT0gSW5kZXhlckRhdGFUeXBlcy5OVU1CRVIpIHtcclxuICAgICAgICB0aGlzLmNoYXJEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZGF0ZURhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5udW1iZXJEYXRhVHlwZSA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSBpZiAoc2VsZWN0ZWRPcGVyYXRvclswXS5EQVRBX1RZUEUgPT09IEluZGV4ZXJEYXRhVHlwZXMuREFURVRJTUUpIHtcclxuICAgICAgICB0aGlzLmNoYXJEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZGF0ZURhdGFUeXBlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm51bWJlckRhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKGlzSW5pdGlhbCkge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hWYWx1ZSA9IG5ldyBEYXRlKHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hWYWx1ZS5zcGxpdCgnLycpLnJldmVyc2UoKS5qb2luKCcvJykpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUgPSBuZXcgRGF0ZSh0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHRoaXMuZmlsdGVyUm93RGF0YS5oYXNUd29WYWx1ZXMgJiYgdHlwZW9mIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgLy8gdGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlID0gbmV3IERhdGUodGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlLnNwbGl0KCcvJykucmV2ZXJzZSgpLmpvaW4oJy8nKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSA9IG5ldyBEYXRlKHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hTZWNvbmRWYWx1ZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdGVkT3BlcmF0b3JbMF0uREFUQV9UWVBFID09PSBJbmRleGVyRGF0YVR5cGVzLk5VTUJFUikge1xyXG4gICAgICAgIHRoaXMuY2hhckRhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5kYXRlRGF0YVR5cGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm51bWJlckRhdGFUeXBlID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdmFsaWRhdGVTZWxlY3RlZEZpZWxkKCkge1xyXG4gICAgLy8gdG8gbG9hZCBkZWZhdWx0IGZpZWxkXHJcbiAgICBpZiAoIXRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hGaWVsZCkge1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoRmllbGQgPSB0aGlzLmZpbHRlclJvd0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzWzBdLk1QTV9GSUVMRF9DT05GSUdfSUQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gdG8gbG9hZCBzZWxlY3RlZCBmaWVsZFxyXG4gICAgaWYgKHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hGaWVsZCkge1xyXG4gICAgICBjb25zdCBzZWxlY3RlZEZpZWxkOiBNUE1GaWVsZFtdID0gdGhpcy5maWx0ZXJSb3dEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5maWx0ZXIoZmllbGQgPT4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZC5NUE1fRklFTERfQ09ORklHX0lEID09PSB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoRmllbGQ7XHJcbiAgICAgIH0pO1xyXG4gICAgICAvLyB0byBsb2FkIHNlbGVjdGVkIGZpZWxkIG9wZXJhdG9yc1xyXG4gICAgICBpZiAoc2VsZWN0ZWRGaWVsZCAmJiBzZWxlY3RlZEZpZWxkWzBdICYmIHNlbGVjdGVkRmllbGRbMF0uREFUQV9UWVBFKSB7XHJcbiAgICAgICAgdGhpcy5hdmFpbGFibGVGaWVsZE9wZXJhdG9ycyA9IFtdO1xyXG4gICAgICAgIHRoaXMuYXZhaWxhYmxlRmllbGRPcGVyYXRvcnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hPcGVyYXRvckxpc3QuZmlsdGVyKG9wZXJhdG9yID0+IHtcclxuICAgICAgICAgIHJldHVybiBzZWxlY3RlZEZpZWxkWzBdLkRBVEFfVFlQRSA9PT0gb3BlcmF0b3IuREFUQV9UWVBFO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoc2VsZWN0ZWRGaWVsZFswXS5FRElUX1RZUEUgPT09ICdDT01CTycpIHtcclxuICAgICAgICB0aGlzLmNvbWJvVHlwZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5jb21ib1ZhbHVlcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChzZWxlY3RlZEZpZWxkWzBdLk1QTV9GSUVMRF9DT05GSUdfSUQpO1xyXG4gICAgICAgIC8vIGdldCBjb21ibyB2YWx1ZXM7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChzZWxlY3RlZEZpZWxkICYmIHNlbGVjdGVkRmllbGRbMF0gJiYgc2VsZWN0ZWRGaWVsZFswXS5JU19EUk9QX0RPV04gPT09IHRydWUpIHtcclxuICAgICAgICB0aGlzLmlzRHJvcERvd24gPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZm9ybUF1dG9TdWdnZXN0VmFsdWVzKHNlbGVjdGVkRmllbGRbMF0uTUFQUEVSX05BTUUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaE9wZXJhdG9ySWQpIHtcclxuICAgICAgICB0aGlzLmxvYWRGaWVsZFZhbHVlVHlwZSh0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCwgdHJ1ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGZvcm1BdXRvU3VnZ2VzdFZhbHVlcyhldmVudCkge1xyXG5cclxuICAgIC8vIGN1c3RvbS1maWVsZHNcclxuICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5mb3JFYWNoKHNlYXJjaEZpZWxkcyA9PiB7XHJcbiAgICAgIGlmIChzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUgPT09IGV2ZW50IHx8IHNlYXJjaEZpZWxkcy5NUE1fRklFTERfQ09ORklHX0lEID09PSBldmVudCkge1xyXG4gICAgICAgIHRoaXMubGV2ZWwgPSBzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUuaW5jbHVkZXMoTVBNX0xFVkVMUy5QUk9KRUNUKSA/IE1QTV9MRVZFTFMuUFJPSkVDVCA6XHJcbiAgICAgICAgICBzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUuaW5jbHVkZXMoTVBNX0xFVkVMUy5UQVNLKSA/IE1QTV9MRVZFTFMuVEFTSyA6IHNlYXJjaEZpZWxkcy5NQVBQRVJfTkFNRS5pbmNsdWRlcyhNUE1fTEVWRUxTLkRFTElWRVJBQkxFKSA/XHJcbiAgICAgICAgICAgIE1QTV9MRVZFTFMuREVMSVZFUkFCTEUgOiBzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUuaW5jbHVkZXMoTVBNX0xFVkVMUy5DQU1QQUlHTikgPyBNUE1fTEVWRUxTLkNBTVBBSUdOIDogJyc7XHJcbiAgICAgICAgc2VhcmNoRmllbGRzLklTX0RST1BfRE9XTiA9IHRoaXMuaXNEcm9wRG93biA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoc2VhcmNoRmllbGRzLk1QTV9GSUVMRF9DT05GSUdfSUQgPT09IGV2ZW50ICYmIHNlYXJjaEZpZWxkcy5JU19DVVNUT01fTUVUQURBVEEgPT09ICd0cnVlJykge1xyXG4gICAgICAgIC8vIHRoaXMuaXNEcm9wRG93biA9IHRydWU7XHJcbiAgICAgICAgY29uc3QgY2F0ZWdvcnlMZXZlbERldGFpbHMgPSB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeUxldmVsRGV0YWlsc0J5VHlwZSh0aGlzLmxldmVsKTtcclxuICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldE1ldGFkYXRNb2RlbEJ5SWQoY2F0ZWdvcnlMZXZlbERldGFpbHMuTUVUQURBVEFfTU9ERUxfSUQpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKG1ldGFEYXRhTW9kZWxEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cobWV0YURhdGFNb2RlbERldGFpbHMpO1xyXG4gICAgICAgICAgICBtZXRhRGF0YU1vZGVsRGV0YWlscy5tZXRhZGF0YV9lbGVtZW50X2xpc3QuZm9yRWFjaChtZXRhZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX1BST0pFQ1RfQ1VTVE9NX01FVEFEQVRBX0dST1VQIHx8IG1ldGFkYXRhLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX0NBTVBBSUdOX0NVU1RPTV9NRVRBREFUQV9HUk9VUCkge1xyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0LmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVkaXRfdHlwZSA9PT0gUHJvamVjdENvbnN0YW50Lk1FVEFEQVRBX0VESVRfVFlQRVMuQ09NQk8gJiYgKGRhdGEuaWQgPT09IHNlYXJjaEZpZWxkcy5PVE1NX0ZJRUxEX0lEIHx8IGRhdGEuZG9tYWluX2lkID09PSBzZWFyY2hGaWVsZHMuT1RNTV9GSUVMRF9JRCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzRHJvcERvd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlYXJjaEZpZWxkcy5JU19EUk9QX0RPV04gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGN1c3RvbURhdGFzID0gdGhpcy51dGlsU2VydmljZS5nZXRMb29rdXBEb21haW5WYWx1ZXNCeUlkKGRhdGEuZG9tYWluX2lkKTtcclxuICAgICAgICAgICAgICAgICAgICBjdXN0b21EYXRhcy5mb3JFYWNoKGN1c3RvbURhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnB1c2goY3VzdG9tRGF0YS5kaXNwbGF5X3ZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBzdGFuZGFyZCBmaWVsZHNcclxuICAgIGlmIChldmVudCA9PT0gU3RhbmRhcmRQcm9qZWN0VmFsdWUuUFJPSkVDVF9URUFNIHx8IGV2ZW50ID09PSBTdGFuZGFyZFRhc2tWYWx1ZS5UQVNLX1RFQU0gfHwgZXZlbnQgPT09IFN0YW5kYXJkRGVsaXZlcmFibGVWYWx1ZS5ERUxJVkVSQUJMRV9URUFNKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbXMoKSk7XHJcbiAgICAgIGNvbnN0IGFsbFRlYW1zID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxUZWFtcygpO1xyXG4gICAgICBhbGxUZWFtcy5mb3JFYWNoKHRlYW1zID0+IHtcclxuICAgICAgICB0aGlzLm9wdGlvbnMucHVzaCh0ZWFtcy5OQU1FKTtcclxuXHJcbiAgICAgIH0pO1xyXG4gICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwICYmIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0pIHtcclxuICAgICAgICB0aGlzLmZpbHRlcmVkT3B0aW9ucyA9IHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uZ2V0KCdmaWVsZFZhbHVlJykudmFsdWVDaGFuZ2VzLnBpcGUoXHJcbiAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgbWFwKHZhbHVlID0+XHJcbiAgICAgICAgICAgIHRoaXMuX2ZpbHRlcih2YWx1ZSlcclxuXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnMpXHJcbiAgICB9IGVsc2UgaWYgKGV2ZW50ID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX1BSSU9SSVRZIHx8IGV2ZW50ID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZFRhc2tWYWx1ZS5UQVNLX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZFRhc2tWYWx1ZS5UQVNLX1BSSU9SSVRZIHx8IGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfUFJJT1JJVFlfSUQgfHwgZXZlbnQgPT09IFN0YW5kYXJkRGVsaXZlcmFibGVWYWx1ZS5ERUxJVkVSQUJMRV9QUklPUklUWSB8fCBldmVudCA9PT0gU3RhbmRhcmRDYW1wYWlnblZhbHVlLkNBTVBBSUdOX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZENhbXBhaWduVmFsdWUuQ0FNUEFJR05fUFJJT1JJVFkpIHtcclxuICAgICAgLyogIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRQcmlvcml0aWVzKHRoaXMubGV2ZWwpLnN1YnNjcmliZShwcmlvcml0aWVzID0+IHtcclxuICAgICAgICAgY29uc29sZS5sb2cocHJpb3JpdGllcyk7XHJcbiAgICAgICAgIHByaW9yaXRpZXMuZm9yRWFjaChwcmlvcml0eSA9PiB7XHJcbiAgICAgICAgICAgaWYgKGV2ZW50ID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZFRhc2tWYWx1ZS5UQVNLX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfUFJJT1JJVFlfSUQgfHwgZXZlbnQgPT09IFN0YW5kYXJkQ2FtcGFpZ25WYWx1ZS5DQU1QQUlHTl9QUklPUklUWV9JRCkge1xyXG4gICAgICAgICAgICAgdGhpcy5vcHRpb25zLnB1c2gocHJpb3JpdHlbXCJNUE1fUHJpb3JpdHktaWRcIl0uSWQpO1xyXG4gICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChwcmlvcml0eS5ERVNDUklQVElPTik7XHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICB9KTtcclxuICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG4gXHJcbiAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgKTtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnMpXHJcbiAgICAgICB9KTsgKi9cclxuXHJcbiAgICAgIGxldCBwcmlvcml0eSA9IHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRQcmlvcml0aWVzKHRoaXMubGV2ZWwpXHJcbiAgICAgIC8vICAuc3Vic2NyaWJlKHByaW9yaXRpZXMgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhwcmlvcml0eSk7XHJcbiAgICAgIHByaW9yaXR5LmZvckVhY2gocHJpb3JpdHkgPT4ge1xyXG4gICAgICAgIGlmIChldmVudCA9PT0gU3RhbmRhcmRQcm9qZWN0VmFsdWUuUFJPSkVDVF9QUklPUklUWV9JRCB8fCBldmVudCA9PT0gU3RhbmRhcmRUYXNrVmFsdWUuVEFTS19QUklPUklUWV9JRCB8fCBldmVudCA9PT0gU3RhbmRhcmREZWxpdmVyYWJsZVZhbHVlLkRFTElWRVJBQkxFX1BSSU9SSVRZX0lEIHx8IGV2ZW50ID09PSBTdGFuZGFyZENhbXBhaWduVmFsdWUuQ0FNUEFJR05fUFJJT1JJVFlfSUQpIHtcclxuICAgICAgICAgIHRoaXMub3B0aW9ucy5wdXNoKHByaW9yaXR5W1wiTVBNX1ByaW9yaXR5LWlkXCJdLklkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5vcHRpb25zLnB1c2gocHJpb3JpdHkuREVTQ1JJUFRJT04pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSkge1xyXG4gICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG5cclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMub3B0aW9ucylcclxuICAgICAgLy8gfSk7XHJcblxyXG4gICAgfSBlbHNlIGlmIChldmVudCA9PT0gU3RhbmRhcmRQcm9qZWN0VmFsdWUuUFJPSkVDVF9TVEFUVVMgfHwgZXZlbnQgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfU1RBVFVTIHx8IGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfU1RBVFVTIHx8IGV2ZW50ID09PSBTdGFuZGFyZENhbXBhaWduVmFsdWUuQ0FNUEFJR05fU1RBVFVTKSB7XHJcbiAgICAgIC8qICB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsTmFtZSh0aGlzLmxldmVsKS5zdWJzY3JpYmUoc3RhdHVzZXMgPT4ge1xyXG4gICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXNlcyk7XHJcbiAgICAgICAgIHN0YXR1c2VzLmZvckVhY2goc3RhdHVzID0+IHtcclxuICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChzdGF0dXMuTkFNRSk7XHJcbiAgICAgICAgIH0pXHJcbiAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMub3B0aW9ucylcclxuICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG4gXHJcbiAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgKTtcclxuICAgICAgICAgfVxyXG4gICAgICAgfSk7ICovXHJcblxyXG4gICAgICBsZXQgc3RhdHVzZXMgPSB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0QWxsU3RhdHVzQnljYXRlZ29yeU5hbWUodGhpcy5sZXZlbClcclxuICAgICAgbGV0IHN0YXR1czogYW55ID0gc3RhdHVzZXMgJiYgc3RhdHVzZXMubGVuZ3RoID4gMCAmJiBBcnJheS5pc0FycmF5KHN0YXR1c2VzKSA/IHN0YXR1c2VzWzBdIDogc3RhdHVzZXM7XHJcbiAgICAgIHN0YXR1cy5mb3JFYWNoKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgdGhpcy5vcHRpb25zLnB1c2goc3RhdHVzLk5BTUUpO1xyXG4gICAgICB9KVxyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnMpXHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSkge1xyXG4gICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG5cclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICAgIC8qIHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxTdGF0dXNCeWNhdGVnb3J5TmFtZSh0aGlzLmxldmVsKS5zdWJzY3JpYmUoc3RhdHVzZXMgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKHN0YXR1c2VzKTtcclxuICAgICAgICAgICAgICBzdGF0dXNlcy5mb3JFYWNoKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChzdGF0dXMuTkFNRSk7XHJcbiAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnMpXHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maWx0ZXJlZE9wdGlvbnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmdldCgnZmllbGRWYWx1ZScpLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICAgICAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9maWx0ZXIodmFsdWUpXHJcbiAgICAgIFxyXG4gICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7ICovXHJcbiAgICB9IGVsc2UgaWYgKGV2ZW50ID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX09XTkVSX05BTUUgfHwgZXZlbnQgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfT1dORVJfTkFNRSB8fCBldmVudCA9PT0gU3RhbmRhcmREZWxpdmVyYWJsZVZhbHVlLkRFTElWRVJBQkxFX09XTkVSX05BTUUgfHwgZXZlbnQgPT09IFN0YW5kYXJkQ2FtcGFpZ25WYWx1ZS5DQU1QQUlHTl9PV05FUl9OQU1FKSB7XHJcbiAgICAgIGNvbnN0IGFsbFBlcnNvbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFBlcnNvbnMoKTtcclxuICAgICAgYWxsUGVyc29ucy5mb3JFYWNoKHBlcnNvbnMgPT4ge1xyXG4gICAgICAgIGlmIChwZXJzb25zICYmIHBlcnNvbnMuRGlzcGxheU5hbWUgJiYgcGVyc29ucy5EaXNwbGF5TmFtZS5fX3RleHQpIHtcclxuICAgICAgICAgIHRoaXMub3B0aW9ucy5wdXNoKHBlcnNvbnMuRGlzcGxheU5hbWUuX190ZXh0KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICB9KTtcclxuICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMCAmJiB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtKSB7XHJcbiAgICAgICAgdGhpcy5maWx0ZXJlZE9wdGlvbnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmdldCgnZmllbGRWYWx1ZScpLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgICAgIG1hcCh2YWx1ZSA9PlxyXG4gICAgICAgICAgICB0aGlzLl9maWx0ZXIodmFsdWUpXHJcblxyXG4gICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAoZXZlbnQgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfUk9MRV9OQU1FIHx8IGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfQVBQUk9WRVJfUk9MRV9OQU1FIHx8IGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfT1dORVJfUk9MRV9OQU1FKSB7IC8vTVBNVjMtMjAwOFxyXG4gICAgICBjb25zdCBhbGxUZWFtUm9sZXMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbFRlYW1Sb2xlcygpO1xyXG4gICAgICBhbGxUZWFtUm9sZXMuZm9yRWFjaCh0ZWFtUm9sZSA9PiB7XHJcbiAgICAgICAgaWYgKHRlYW1Sb2xlICYmIHRlYW1Sb2xlLk5BTUUpIHtcclxuICAgICAgICAgIHRoaXMub3B0aW9ucy5wdXNoKHRlYW1Sb2xlLk5BTUUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSkge1xyXG4gICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG5cclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGV2ZW50ID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfQVBQUk9WRVJfTkFNRSkge1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVXNlcnMoKS5zdWJzY3JpYmUoYWxsVXNlcnMgPT4ge1xyXG4gICAgICAgIGlmIChhbGxVc2VycyAmJiBhbGxVc2Vycy5Vc2VyKSB7XHJcbiAgICAgICAgICBhbGxVc2Vycy5Vc2VyLmZvckVhY2godXNlcnMgPT4ge1xyXG4gICAgICAgICAgICBpZiAodXNlcnMgJiYgdXNlcnMuRnVsbE5hbWUgJiYgdHlwZW9mICh1c2Vycy5GdWxsTmFtZSkgIT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnB1c2godXNlcnMuRnVsbE5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSkge1xyXG4gICAgICAgICAgICB0aGlzLmZpbHRlcmVkT3B0aW9ucyA9IHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uZ2V0KCdmaWVsZFZhbHVlJykudmFsdWVDaGFuZ2VzLnBpcGUoXHJcbiAgICAgICAgICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICAgIHRoaXMuX2ZpbHRlcih2YWx1ZSlcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAoZXZlbnQgPT09IFN0YW5kYXJkUHJvamVjdFZhbHVlLlBST0pFQ1RfQ0FURUdPUlkpIHtcclxuICAgICAgdGhpcy5wcm9qZWN0U2VydmljZS5nZXRBbGxDYXRlZ29yeU1ldGFkYXRhKCkuc3Vic2NyaWJlKGNhdGVnb3J5RGV0YWlscyA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coY2F0ZWdvcnlEZXRhaWxzKTtcclxuICAgICAgICBjb25zdCBhbGxjYXRlZ29yeU1ldGFkYXRhTGlzdCA9IGNhdGVnb3J5RGV0YWlscy5tYXAoZGF0YSA9PiB7XHJcbiAgICAgICAgICByZXR1cm4geyBkaXNwbGF5TmFtZTogZGF0YS5NRVRBREFUQV9OQU1FLCB2YWx1ZTogZGF0YVsnTVBNX0NhdGVnb3J5X01ldGFkYXRhLWlkJ10uSWQsIG5hbWU6IGRhdGEuTUVUQURBVEFfTkFNRSwgLi4uZGF0YSB9O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGFsbGNhdGVnb3J5TWV0YWRhdGFMaXN0KTtcclxuICAgICAgICBhbGxjYXRlZ29yeU1ldGFkYXRhTGlzdC5mb3JFYWNoKGNhdGVnb3J5TWV0YWRhdGFMaXN0ID0+IHtcclxuICAgICAgICAgIGlmIChjYXRlZ29yeU1ldGFkYXRhTGlzdCAmJiBjYXRlZ29yeU1ldGFkYXRhTGlzdC5uYW1lKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5wdXNoKGNhdGVnb3J5TWV0YWRhdGFMaXN0Lm5hbWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDAgJiYgdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybSkge1xyXG4gICAgICAgICAgdGhpcy5maWx0ZXJlZE9wdGlvbnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmdldCgnZmllbGRWYWx1ZScpLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICB0aGlzLl9maWx0ZXIodmFsdWUpXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uRmllbGRWYWx1ZUNoYW5nZShldmVudCkge1xyXG4gICAgdGhpcy5pc0Ryb3BEb3duID0gZmFsc2U7XHJcbiAgICB0aGlzLmNvbWJvVHlwZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5jb21ib1ZhbHVlcyA9IFtdO1xyXG4gICAgdGhpcy5vcHRpb25zID0gW107XHJcbiAgICBjb25zdCBzZWxlY3RlZEZpZWxkSWQgPSBldmVudC52YWx1ZTtcclxuICAgIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hGaWVsZCA9IHNlbGVjdGVkRmllbGRJZDtcclxuICAgIGNvbnN0IHNlbGVjdGVkRmllbGQ6IE1QTUZpZWxkW10gPSB0aGlzLmZpbHRlclJvd0RhdGEuYXZhaWxhYmxlU2VhcmNoRmllbGRzLmZpbHRlcihmaWVsZCA9PiB7XHJcbiAgICAgIHJldHVybiBmaWVsZC5NUE1fRklFTERfQ09ORklHX0lEID09PSBzZWxlY3RlZEZpZWxkSWQ7XHJcbiAgICB9KTtcclxuICAgIGlmIChzZWxlY3RlZEZpZWxkICYmIHNlbGVjdGVkRmllbGRbMF0gJiYgc2VsZWN0ZWRGaWVsZFswXS5EQVRBX1RZUEUgPT09IEluZGV4ZXJEYXRhVHlwZXMuTlVNQkVSKSB7XHJcbiAgICAgIHNlbGVjdGVkRmllbGRbMF0uREFUQV9UWVBFID0gSW5kZXhlckRhdGFUeXBlcy5OVU1CRVI7XHJcbiAgICB9XHJcbiAgICBpZiAoc2VsZWN0ZWRGaWVsZCAmJiBzZWxlY3RlZEZpZWxkWzBdICYmIHNlbGVjdGVkRmllbGRbMF0uRURJVF9UWVBFID09PSAnQ09NQk8nKSB7XHJcbiAgICAgIHRoaXMuY29tYm9UeXBlID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jb21ib1ZhbHVlcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChzZWxlY3RlZEZpZWxkWzBdLk1QTV9GSUVMRF9DT05GSUdfSUQpO1xyXG4gICAgICAvLyBnZXQgY29tYm8gdmFsdWVzO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLmNvbWJvVHlwZSkge1xyXG4gICAgICBpZiAoc2VsZWN0ZWRGaWVsZCAmJiBzZWxlY3RlZEZpZWxkWzBdICYmIHNlbGVjdGVkRmllbGRbMF0uREFUQV9UWVBFID09PSBJbmRleGVyRGF0YVR5cGVzLlNUUklORykge1xyXG4gICAgICAgIHRoaXMuY2hhckRhdGFUeXBlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmRhdGVEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubnVtYmVyRGF0YVR5cGUgPSBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmIChzZWxlY3RlZEZpZWxkICYmIHNlbGVjdGVkRmllbGRbMF0gJiYgc2VsZWN0ZWRGaWVsZFswXS5EQVRBX1RZUEUgPT09IEluZGV4ZXJEYXRhVHlwZXMuREFURVRJTUUpIHtcclxuICAgICAgICB0aGlzLmNoYXJEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZGF0ZURhdGFUeXBlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLm51bWJlckRhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgIH0gZWxzZSBpZiAoc2VsZWN0ZWRGaWVsZCAmJiBzZWxlY3RlZEZpZWxkWzBdICYmIHNlbGVjdGVkRmllbGRbMF0uREFUQV9UWVBFID09PSBJbmRleGVyRGF0YVR5cGVzLk5VTUJFUikge1xyXG4gICAgICAgIHRoaXMuY2hhckRhdGFUeXBlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5kYXRlRGF0YVR5cGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm51bWJlckRhdGFUeXBlID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGFyRGF0YVR5cGUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5kYXRlRGF0YVR5cGUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5udW1iZXJEYXRhVHlwZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5maWx0ZXJSb3dEYXRhLmlzc2VhcmNoVmFsdWVSZXF1aXJlZCA9IGZhbHNlO1xyXG4gICAgLy8gdGhpcy5maWx0ZXJSb3dEYXRhLmhhc1R3b1ZhbHVlcyA9IGZhbHNlO1xyXG4gICAgaWYgKHNlbGVjdGVkRmllbGQgJiYgc2VsZWN0ZWRGaWVsZFswXSAmJiBzZWxlY3RlZEZpZWxkWzBdLkRBVEFfVFlQRSkge1xyXG4gICAgICB0aGlzLmF2YWlsYWJsZUZpZWxkT3BlcmF0b3JzID0gW107XHJcbiAgICAgIHRoaXMuYXZhaWxhYmxlRmllbGRPcGVyYXRvcnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnRGF0YS5zZWFyY2hPcGVyYXRvckxpc3QuZmlsdGVyKG9wZXJhdG9yID0+IHtcclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRGaWVsZFswXS5EQVRBX1RZUEUgPT09IG9wZXJhdG9yLkRBVEFfVFlQRTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICAvKiAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhLmF2YWlsYWJsZVNlYXJjaEZpZWxkcy5mb3JFYWNoKHNlYXJjaEZpZWxkcyA9PiB7XHJcbiAgICAgICAgaWYgKHNlYXJjaEZpZWxkcy5NQVBQRVJfTkFNRSA9PT0gZXZlbnQudmFsdWUgfHwgc2VhcmNoRmllbGRzLk1QTV9GSUVMRF9DT05GSUdfSUQgPT09IGV2ZW50LnZhbHVlKSB7XHJcbiAgICAgICAgICB0aGlzLmxldmVsID0gc2VhcmNoRmllbGRzLk1BUFBFUl9OQU1FLmluY2x1ZGVzKE1QTV9MRVZFTFMuUFJPSkVDVCkgPyBNUE1fTEVWRUxTLlBST0pFQ1QgOiBzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUuaW5jbHVkZXMoTVBNX0xFVkVMUy5UQVNLKSA/IE1QTV9MRVZFTFMuVEFTSyA6IHNlYXJjaEZpZWxkcy5NQVBQRVJfTkFNRS5pbmNsdWRlcyhNUE1fTEVWRUxTLkRFTElWRVJBQkxFKSA/IE1QTV9MRVZFTFMuREVMSVZFUkFCTEUgOiBzZWFyY2hGaWVsZHMuTUFQUEVSX05BTUUuaW5jbHVkZXMoTVBNX0xFVkVMUy5DQU1QQUlHTikgPyBNUE1fTEVWRUxTLkNBTVBBSUdOIDogJyc7XHJcbiAgICAgICAgICBzZWFyY2hGaWVsZHMuSVNfRFJPUF9ET1dOID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaEZpZWxkcy5NUE1fRklFTERfQ09ORklHX0lEID09PSBldmVudC52YWx1ZSAmJiBzZWFyY2hGaWVsZHMuSVNfQ1VTVE9NX01FVEFEQVRBID09PSAndHJ1ZScpIHtcclxuICAgICAgICAgIC8vIHRoaXMuaXNEcm9wRG93biA9IHRydWU7XHJcbiAgICAgICAgICBjb25zdCBjYXRlZ29yeUxldmVsRGV0YWlscyA9IHRoaXMuY2F0ZWdvcnlTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxEZXRhaWxzQnlUeXBlKHRoaXMubGV2ZWwpO1xyXG4gICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5nZXRNZXRhZGF0TW9kZWxCeUlkKGNhdGVnb3J5TGV2ZWxEZXRhaWxzLk1FVEFEQVRBX01PREVMX0lEKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKG1ldGFEYXRhTW9kZWxEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhtZXRhRGF0YU1vZGVsRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgbWV0YURhdGFNb2RlbERldGFpbHMubWV0YWRhdGFfZWxlbWVudF9saXN0LmZvckVhY2gobWV0YWRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX1BST0pFQ1RfQ1VTVE9NX01FVEFEQVRBX0dST1VQIHx8IG1ldGFkYXRhLmlkID09PSBQcm9qZWN0Q29uc3RhbnQuTVBNX0NBTVBBSUdOX0NVU1RPTV9NRVRBREFUQV9HUk9VUCkge1xyXG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImluc2lkZSBpZlwiKVxyXG4gICAgICAgICAgICAgICAgICBtZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lZGl0X3R5cGUgPT09IFByb2plY3RDb25zdGFudC5NRVRBREFUQV9FRElUX1RZUEVTLkNPTUJPICYmIGRhdGEuaWQgPT09IHNlYXJjaEZpZWxkcy5PVE1NX0ZJRUxEX0lEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzRHJvcERvd24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgc2VhcmNoRmllbGRzLklTX0RST1BfRE9XTiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjdXN0b21EYXRhcyA9IHRoaXMudXRpbFNlcnZpY2UuZ2V0TG9va3VwRG9tYWluVmFsdWVzQnlJZChkYXRhLmRvbWFpbl9pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBjdXN0b21EYXRhcy5mb3JFYWNoKGN1c3RvbURhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChjdXN0b21EYXRhLmRpc3BsYXlfdmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTsgKi9cclxuXHJcbiAgICBBZHZhbmNlZFNlYXJjaFN0YW5kYXJkVmFsdWUuQURWQU5DRURfU0VBUkNIX1NUQU5EQVJEX1ZBTFVFLmZvckVhY2goc3RhbmRhcmRGaWVsZCA9PiB7XHJcbiAgICAgIGlmIChzdGFuZGFyZEZpZWxkID09PSBldmVudC52YWx1ZSkge1xyXG4gICAgICAgIHRoaXMuaXNEcm9wRG93biA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuZm9ybUF1dG9TdWdnZXN0VmFsdWVzKGV2ZW50LnZhbHVlKTtcclxuXHJcbiAgICAvKiAgaWYgKGV2ZW50LnZhbHVlID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX1RFQU0gfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfVEVBTSB8fCBldmVudC52YWx1ZSA9PT0gU3RhbmRhcmREZWxpdmVyYWJsZVZhbHVlLkRFTElWRVJBQkxFX1RFQU0pIHtcclxuICAgICAgIGNvbnNvbGUubG9nKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbXMoKSk7XHJcbiAgICAgICBjb25zdCBhbGxUZWFtcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbXMoKTtcclxuICAgICAgIGFsbFRlYW1zLmZvckVhY2godGVhbXMgPT4ge1xyXG4gICAgICAgICB0aGlzLm9wdGlvbnMucHVzaCh0ZWFtcy5OQU1FKTtcclxuIFxyXG4gICAgICAgfSk7XHJcbiAgICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgIG1hcCh2YWx1ZSA9PlxyXG4gICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG4gXHJcbiAgICAgICAgICAgKVxyXG4gICAgICAgICApO1xyXG4gICAgICAgfVxyXG4gICAgICAgY29uc29sZS5sb2codGhpcy5vcHRpb25zKVxyXG4gICAgIH0gZWxzZSBpZiAoZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkUHJvamVjdFZhbHVlLlBST0pFQ1RfUFJJT1JJVFkgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkUHJvamVjdFZhbHVlLlBST0pFQ1RfUFJJT1JJVFlfSUQgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfUFJJT1JJVFlfSUQgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfUFJJT1JJVFkgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkRGVsaXZlcmFibGVWYWx1ZS5ERUxJVkVSQUJMRV9QUklPUklUWV9JRCB8fCBldmVudC52YWx1ZSA9PT0gU3RhbmRhcmREZWxpdmVyYWJsZVZhbHVlLkRFTElWRVJBQkxFX1BSSU9SSVRZIHx8IGV2ZW50LnZhbHVlID09PSBTdGFuZGFyZENhbXBhaWduVmFsdWUuQ0FNUEFJR05fUFJJT1JJVFlfSUQgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkQ2FtcGFpZ25WYWx1ZS5DQU1QQUlHTl9QUklPUklUWSkge1xyXG4gICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmdldFByaW9yaXRpZXModGhpcy5sZXZlbCkuc3Vic2NyaWJlKHByaW9yaXRpZXMgPT4ge1xyXG4gICAgICAgICBjb25zb2xlLmxvZyhwcmlvcml0aWVzKTtcclxuICAgICAgICAgcHJpb3JpdGllcy5mb3JFYWNoKHByaW9yaXR5ID0+IHtcclxuICAgICAgICAgICBpZiAoZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkUHJvamVjdFZhbHVlLlBST0pFQ1RfUFJJT1JJVFlfSUQgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfUFJJT1JJVFlfSUQgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkRGVsaXZlcmFibGVWYWx1ZS5ERUxJVkVSQUJMRV9QUklPUklUWV9JRCB8fCBldmVudC52YWx1ZSA9PT0gU3RhbmRhcmRDYW1wYWlnblZhbHVlLkNBTVBBSUdOX1BSSU9SSVRZX0lEKSB7XHJcbiAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChwcmlvcml0eVtcIk1QTV9Qcmlvcml0eS1pZFwiXS5JZCk7XHJcbiAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5wdXNoKHByaW9yaXR5LkRFU0NSSVBUSU9OKTtcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgdGhpcy5maWx0ZXJlZE9wdGlvbnMgPSB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmdldCgnZmllbGRWYWx1ZScpLnZhbHVlQ2hhbmdlcy5waXBlKFxyXG4gICAgICAgICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgICAgICAgIG1hcCh2YWx1ZSA9PlxyXG4gICAgICAgICAgICAgICB0aGlzLl9maWx0ZXIodmFsdWUpXHJcbiBcclxuICAgICAgICAgICAgIClcclxuICAgICAgICAgICApO1xyXG4gICAgICAgICB9XHJcbiAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMub3B0aW9ucylcclxuICAgICAgIH0pO1xyXG4gXHJcbiAgICAgfSBlbHNlIGlmIChldmVudC52YWx1ZSA9PT0gU3RhbmRhcmRQcm9qZWN0VmFsdWUuUFJPSkVDVF9TVEFUVVMgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfU1RBVFVTIHx8IGV2ZW50LnZhbHVlID09PSBTdGFuZGFyZERlbGl2ZXJhYmxlVmFsdWUuREVMSVZFUkFCTEVfU1RBVFVTIHx8IGV2ZW50LnZhbHVlID09PSBTdGFuZGFyZENhbXBhaWduVmFsdWUuQ0FNUEFJR05fU1RBVFVTKSB7XHJcbiAgICAgICB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsTmFtZSh0aGlzLmxldmVsKS5zdWJzY3JpYmUoc3RhdHVzZXMgPT4ge1xyXG4gICAgICAgICBjb25zb2xlLmxvZyhzdGF0dXNlcyk7XHJcbiAgICAgICAgIHN0YXR1c2VzLmZvckVhY2goc3RhdHVzID0+IHtcclxuICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChzdGF0dXMuTkFNRSk7XHJcbiAgICAgICAgIH0pXHJcbiAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMub3B0aW9ucylcclxuICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICAgIHN0YXJ0V2l0aCgnJyksXHJcbiAgICAgICAgICAgICBtYXAodmFsdWUgPT5cclxuICAgICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG4gXHJcbiAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgKTtcclxuICAgICAgICAgfVxyXG4gICAgICAgfSk7XHJcbiBcclxuICAgICB9IGVsc2UgaWYgKGV2ZW50LnZhbHVlID09PSBTdGFuZGFyZFByb2plY3RWYWx1ZS5QUk9KRUNUX09XTkVSX05BTUUgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkVGFza1ZhbHVlLlRBU0tfT1dORVJfTkFNRSB8fCBldmVudC52YWx1ZSA9PT0gU3RhbmRhcmREZWxpdmVyYWJsZVZhbHVlLkRFTElWRVJBQkxFX09XTkVSX05BTUUgfHwgZXZlbnQudmFsdWUgPT09IFN0YW5kYXJkQ2FtcGFpZ25WYWx1ZS5DQU1QQUlHTl9PV05FUl9OQU1FKSB7XHJcbiAgICAgICBjb25zdCBhbGxQZXJzb25zID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxQZXJzb25zKCk7XHJcbiAgICAgICBhbGxQZXJzb25zLmZvckVhY2gocGVyc29ucyA9PiB7XHJcbiAgICAgICAgIGlmIChwZXJzb25zICYmIHBlcnNvbnMuRGlzcGxheU5hbWUgJiYgcGVyc29ucy5EaXNwbGF5TmFtZS5fX3RleHQpIHtcclxuICAgICAgICAgICB0aGlzLm9wdGlvbnMucHVzaChwZXJzb25zLkRpc3BsYXlOYW1lLl9fdGV4dCk7XHJcbiAgICAgICAgIH1cclxuIFxyXG4gICAgICAgfSk7XHJcbiAgICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgIG1hcCh2YWx1ZSA9PlxyXG4gICAgICAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG4gXHJcbiAgICAgICAgICAgKVxyXG4gICAgICAgICApO1xyXG4gICAgICAgfVxyXG4gICAgICAgY29uc29sZS5sb2codGhpcy5vcHRpb25zKVxyXG4gICAgIH0gKi9cclxuICB9XHJcblxyXG4gIHB1YmxpYyBfZmlsdGVyKHZhbHVlOiBzdHJpbmcpOiBzdHJpbmdbXSB7XHJcbiAgICBjb25zb2xlLmxvZyh2YWx1ZSlcclxuICAgIGxldCBmaWx0ZXJWYWx1ZSA9IHZhbHVlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgZmlsdGVyVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7XHJcbiAgICAgIGZpbHRlclZhbHVlID0gdmFsdWU7XHJcbiAgICB9XHJcbiAgICAvLyBjb25zdCBmaWx0ZXJWYWx1ZSA9IHZhbHVlLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMub3B0aW9ucy5maWx0ZXIob3B0aW9uID0+IG9wdGlvbi50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID09PSAwKTtcclxuICB9XHJcblxyXG4gIGluaXRpYWxpemVGb3JtKCkge1xyXG4gICAgdGhpcy52YWxpZGF0ZVNlbGVjdGVkRmllbGQoKTtcclxuICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcclxuICAgICAgZmllbGQ6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoRmllbGQpLFxyXG4gICAgICBmaWVsZFR5cGU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoT3BlcmF0b3JJZCksXHJcbiAgICAgIGZpZWxkVmFsdWU6IG5ldyBGb3JtQ29udHJvbCh0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoVmFsdWUpLFxyXG4gICAgICBmaWVsZFNlY29uZFZhbHVlOiBuZXcgRm9ybUNvbnRyb2wodGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFNlY29uZFZhbHVlKSxcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uY29udHJvbHMuZmllbGRUeXBlLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoc2VsZWN0ZWRPcGVyYXRvcklkID0+IHtcclxuICAgICAgdGhpcy5sb2FkRmllbGRWYWx1ZVR5cGUoc2VsZWN0ZWRPcGVyYXRvcklkLCBmYWxzZSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmNvbnRyb2xzLmZpZWxkVmFsdWUudmFsdWVDaGFuZ2VzLnN1YnNjcmliZShzZWxlY3RlZFZhbHVlID0+IHtcclxuICAgICAgdGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaFZhbHVlID0gc2VsZWN0ZWRWYWx1ZTtcclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uY29udHJvbHMuZmllbGRTZWNvbmRWYWx1ZS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHNlbGVjdGVkVmFsdWUgPT4ge1xyXG4gICAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoU2Vjb25kVmFsdWUgPSBzZWxlY3RlZFZhbHVlO1xyXG4gICAgfSk7XHJcbiAgICAvL2lmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zID0gdGhpcy5hZHZhbmNlZFNlYXJjaEZpZWxkRm9ybS5nZXQoJ2ZpZWxkVmFsdWUnKS52YWx1ZUNoYW5nZXMucGlwZShcclxuICAgICAgc3RhcnRXaXRoKCcnKSxcclxuICAgICAgbWFwKHZhbHVlID0+XHJcbiAgICAgICAgdGhpcy5fZmlsdGVyKHZhbHVlKVxyXG5cclxuICAgICAgKVxyXG4gICAgKTtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG4gIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQpXHJcbiAgfVxyXG5cclxuICBmaWx0ZXJTZWxlY3Rpb24oc2VsZWN0aW9uVmFsdWUpIHtcclxuICAgIGlmIChzZWxlY3Rpb25WYWx1ZSkge1xyXG4gICAgICBsZXQgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICBpZiAodGhpcy5maWx0ZXJSb3dEYXRhLnNlYXJjaEZpZWxkKSB7XHJcbiAgICAgICAgaXNWYWxpZCA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGlzVmFsaWQpIHtcclxuICAgICAgICBpZiAodGhpcy5jb21ib1R5cGUpIHtcclxuICAgICAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5zZWFyY2hPcGVyYXRvcklkID0gJ0FSVEVTSUEuT1BFUkFUT1IuQ0hBUi5DT05UQUlOUyc7XHJcbiAgICAgICAgICB0aGlzLmZpbHRlclJvd0RhdGEuc2VhcmNoT3BlcmF0b3JOYW1lID0gJ2NvbnRhaW5zJztcclxuICAgICAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5pc0NvbWJvVHlwZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZmlsdGVyUm93RGF0YS5pc0ZpbHRlclNlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoRmllbGRGb3JtLmNvbnRyb2xzLmZpZWxkLmRpc2FibGUoKTtcclxuICAgICAgICB0aGlzLmZpbHRlclNlbGVjdGVkLm5leHQodGhpcy5maWx0ZXJSb3dEYXRhKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IHNlbGVjdCBhbGwgdmFsdWVzIGZpcnN0Jyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbW92ZUZpbHRlcigpIHtcclxuICAgIHRoaXMucmVtb3ZlRmlsdGVyRmllbGQubmV4dCh0aGlzLmZpbHRlclJvd0RhdGEpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmluaXRpYWxpemVGb3JtKCk7XHJcbiAgICBpZiAobmF2aWdhdG9yLmxhbmd1YWdlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy5hZGFwdGVyLnNldExvY2FsZShuYXZpZ2F0b3IubGFuZ3VhZ2UpO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZmlsdGVyUm93RGF0YS5pc0ZpbHRlclNlbGVjdGVkKSB7XHJcbiAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hGaWVsZEZvcm0uY29udHJvbHMuZmllbGQuZGlzYWJsZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19