import { __decorate, __param, __read, __spread } from "tslib";
import { Component, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { AdvancedSearchComponent } from '../advanced-search/advanced-search.component';
import { ConfirmationModalComponent } from '../../shared/components/confirmation-modal/confirmation-modal.component';
import { LoaderService } from '../../loader/loader.service';
import { NotificationService } from '../../notification/notification.service';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { SearchDataService } from '../services/search-data.service';
import { SearchSaveService } from '../../project/shared/services/search.save.service';
import { Observable } from 'rxjs';
var SimpleSearchComponent = /** @class */ (function () {
    function SimpleSearchComponent(dialogRef, data, sharingService, dialog, loaderService, notificationService, searchConfigService, searchDataService, searchSaveService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.sharingService = sharingService;
        this.dialog = dialog;
        this.loaderService = loaderService;
        this.notificationService = notificationService;
        this.searchConfigService = searchConfigService;
        this.searchDataService = searchDataService;
        this.searchSaveService = searchSaveService;
        this.savedSearches = [];
        this.recentSearches = [];
    }
    SimpleSearchComponent.prototype.search = function (searchKeyword) {
        if (searchKeyword == null || searchKeyword === '' || (typeof (searchKeyword) === 'string' && searchKeyword.trim() === '')) {
            this.notificationService.info('Kindly enter the value to be searched');
            return;
        }
        if (searchKeyword && searchKeyword[0] && searchKeyword[0].isAdvancedSearch) {
            this.closeSimpleSearchPopup(searchKeyword);
        }
        else {
            var searchConditions = [{
                    metadata_field_id: this.searchDataService.KEYWORD_MAPPER,
                    keyword: searchKeyword,
                }];
            this.sharingService.setRecentSearch(searchKeyword);
            this.closeSimpleSearchPopup(searchConditions);
        }
    };
    SimpleSearchComponent.prototype.closeSimpleSearchPopup = function (data) {
        this.dialogRef.close(data);
    };
    SimpleSearchComponent.prototype.getSavedSearchDetail = function (savedSearch) {
        if (savedSearch.SEARCH_DATA) {
            var searchConditions = savedSearch.SEARCH_DATA.search_condition_list.search_condition;
            searchConditions[0].isAdvancedSearch = true;
            searchConditions[0].savedSearch = true;
            searchConditions[0].NAME = savedSearch.NAME;
            searchConditions[0].savedSearchId = savedSearch['MPM_Saved_Searches-id'].Id;
            this.sharingService.setRecentSearch(searchConditions);
            this.closeSimpleSearchPopup(savedSearch);
        }
    };
    SimpleSearchComponent.prototype.deleteSavedSearch = function (savedSearch) {
        var _this = this;
        var dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: 'Are you sure you want to delete saved search?',
                submitButton: 'Yes',
                cancelButton: 'No'
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.isTrue) {
                _this.loaderService.show();
                _this.searchSaveService.deleteSavedSearch(savedSearch['MPM_Saved_Searches-id'].Id).subscribe(function (response) {
                    _this.getAllSavedSearches().subscribe(function (searchResponse) {
                    });
                    _this.loaderService.hide();
                }, function (error) {
                    _this.loaderService.hide();
                    _this.notificationService.error('Error while deleting saved search');
                });
            }
        });
    };
    SimpleSearchComponent.prototype.editSavedSearch = function (savedSearch) {
        if (this.data && this.data.searchData && this.data.searchData.searchConfig
            && this.data.searchData.searchConfig.advancedSearchConfiguration) {
            this.openAdvancedSearch(savedSearch, false);
        }
    };
    SimpleSearchComponent.prototype.editRecentAdvancedSearch = function (resentSearch) {
        resentSearch.forEach(function (data) {
            data.isEdit = true;
        });
        this.openAdvancedSearch(false, resentSearch);
    };
    SimpleSearchComponent.prototype.getAllSavedSearches = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.searchConfigService.getSavedSearchesForViewAndCurrentUser(_this.data.searchData.searchConfig.searchIdentifier.VIEW).subscribe(function (response) {
                if (response && response.length > 0) {
                    _this.savedSearches = response;
                }
                else {
                    _this.savedSearches = [];
                }
                observer.next(true);
                observer.complete();
            }, function (error) {
                console.error('Something went wrong while getting the saved searches.');
            });
        });
    };
    SimpleSearchComponent.prototype.getAllRecentSearches = function () {
        // const allRecentSearches = this.sharingService.getRecentSearches();
        var allRecentSearches = this.sharingService.getRecentSearchesByView(this.data.searchData.searchConfig.searchIdentifier.VIEW);
        var allowedRecentSearches = [];
        if (Array.isArray(allRecentSearches)) {
            allRecentSearches.forEach(function (item) {
                if (item) {
                    var advItem = item[0];
                    if (advItem && advItem.isAdvancedSearch) {
                        if (!advItem.isEdit) {
                            allowedRecentSearches.push(item);
                        }
                    }
                    else {
                        allowedRecentSearches.push(item);
                    }
                }
            });
        }
        this.recentSearches = __spread(new Set(allowedRecentSearches));
    };
    SimpleSearchComponent.prototype.openAdvancedSearchModal = function (advData) {
        var _this = this;
        if (this.data.searchData && this.data.searchData.advancedSearchConfig && (this.data.searchData.advancedSearchConfig.NAME !== '' || this.data.searchData.advancedSearchConfig[0].NAME !== '')) {
            this.savedSearches.forEach(function (data) {
                if (data.NAME === (_this.data.searchData.advancedSearchConfig.NAME ? _this.data.searchData.advancedSearchConfig.NAME : _this.data.searchData.advancedSearchConfig[0].NAME)) {
                    advData.savedSearch = data;
                }
            });
        }
        else if (advData.recentSearch && advData.recentSearch[0] && advData.recentSearch[0].NAME !== '') {
            this.savedSearches.forEach(function (data) {
                if (data.NAME === advData.recentSearch[0].NAME) {
                    advData.savedSearch = data;
                }
            });
        }
        this.advDialogRef = this.dialog.open(AdvancedSearchComponent, {
            position: { top: '0' },
            width: '1100px',
            // height: '65%',
            disableClose: true,
            data: advData
        });
        this.advDialogRef.afterClosed().subscribe(function (result) {
            if (result === true) {
                //  this.dialogRef.close(result);
            }
            else {
                _this.dialogRef.close(result);
            }
        });
    };
    SimpleSearchComponent.prototype.openAdvancedSearch = function (savedSearchId, recentSearch) {
        var _this = this;
        if (this.data && this.data.searchData && this.data.searchData.searchConfig
            && this.data.searchData.searchConfig.advancedSearchConfiguration) {
            var advData_1 = {
                advancedSearchConfigData: this.data.searchData.searchConfig,
                availableSavedSearch: this.savedSearches,
                savedSearch: savedSearchId,
                recentSearch: recentSearch
            };
            if (savedSearchId) {
                if (typeof savedSearchId === 'string') {
                    this.searchConfigService.getSavedSearcheById(savedSearchId).subscribe(function (savedSearchDetails) {
                        if (savedSearchDetails) {
                            advData_1.savedSearch = savedSearchDetails;
                            _this.openAdvancedSearchModal(advData_1);
                        }
                    });
                }
                else {
                    this.openAdvancedSearchModal(advData_1);
                }
            }
            else {
                this.openAdvancedSearchModal(advData_1);
            }
        }
    };
    SimpleSearchComponent.prototype.onKeyUp = function () {
        if (this.advDialogRef) {
            this.advDialogRef.close();
        }
    };
    SimpleSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.searchKeyword = this.data.searchData.searchValue;
        this.getAllSavedSearches().subscribe(function (response) {
            if (_this.data.searchData && _this.data.searchData.advancedSearchConfig) {
                if (_this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'] &&
                    _this.data.searchData.advancedSearchConfig['MPM_Saved_Searches-id'].Id) {
                    _this.editSavedSearch(_this.data.searchData.advancedSearchConfig);
                }
                else {
                    _this.openAdvancedSearch(false, _this.data.searchData.advancedSearchConfig);
                }
            }
        });
        this.getAllRecentSearches();
    };
    SimpleSearchComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: SharingService },
        { type: MatDialog },
        { type: LoaderService },
        { type: NotificationService },
        { type: SearchConfigService },
        { type: SearchDataService },
        { type: SearchSaveService }
    ]; };
    __decorate([
        HostListener('window:keyup.esc')
    ], SimpleSearchComponent.prototype, "onKeyUp", null);
    SimpleSearchComponent = __decorate([
        Component({
            selector: 'mpm-simple-search',
            template: "<mat-dialog-content>\r\n    <div class=\"search-input\">\r\n        <form class=\"example-form\">\r\n            <mat-form-field class=\"example-full-width\">\r\n                <mat-icon matPrefix class=\"cursor-pointer\" (click)=\"search(searchKeyword)\">search</mat-icon>\r\n                <input type=\"text\" matInput [(ngModel)]=\"searchKeyword\" name=\"searchKeyword\"\r\n                    (keyup.enter)=\"search(searchKeyword)\">\r\n            </mat-form-field>\r\n        </form>\r\n    </div>\r\n\r\n    <div class=\"saved-searches\">\r\n        <span class=\"heading\">Saved Searches</span>\r\n        <mat-chip-list aria-label=\"Saved Search Selection\" class=\"saved-search-items\" *ngIf=\"savedSearches.length > 0\">\r\n            <mat-chip class=\"cursor-pointer\" *ngFor=\"let savedSearch of savedSearches\"\r\n                (click)=\"getSavedSearchDetail(savedSearch)\">\r\n                <span class=\"searchName\">{{savedSearch.NAME}}</span>\r\n                <mat-icon matTooltip=\"Delete this Search\" (click)=\"deleteSavedSearch(savedSearch)\" matChipRemove\r\n                    *ngIf=\"!savedSearch.public\">delete\r\n                </mat-icon>\r\n                <mat-icon matTooltip=\"Edit this Search\" (click)=\"editSavedSearch(savedSearch)\" matChipRemove\r\n                    *ngIf=\"!savedSearch.public\">edit\r\n                </mat-icon>\r\n            </mat-chip>\r\n        </mat-chip-list>\r\n        <p class=\"no-items\" *ngIf=\"savedSearches.length === 0\">No saved searches.</p>\r\n    </div>\r\n\r\n    <div class=\"recent-searches\">\r\n        <span class=\"heading\">Recent Searches</span>\r\n        <mat-list role=\"list\" class=\"recent-search-items\" *ngIf=\"recentSearches.length > 0\">\r\n            <mat-list-item role=\"listitem\" *ngFor=\"let recentSearch of recentSearches\">\r\n                <mat-icon mat-list-icon>replay</mat-icon>\r\n                <p mat-line *ngIf=\"!recentSearch[0].isAdvancedSearch\"> <span class=\"cursor-pointer\"\r\n                        (click)=\"search(recentSearch)\" matTooltip=\"{{recentSearch}}\">\r\n                        Keyword: {{recentSearch}}\r\n                    </span>\r\n                </p>\r\n                <p mat-line *ngIf=\"recentSearch[0] && recentSearch[0].isAdvancedSearch\">\r\n                    <span class=\"cursor-pointer\" (click)=\"search(recentSearch)\"\r\n                        matTooltip=\"{{recentSearch[0].NAME ? recentSearch[0].NAME : 'Advanced Search Criteria'}}\">\r\n                        {{recentSearch[0].NAME ? recentSearch[0].NAME : 'Advanced Search Criteria'}} </span>\r\n                    <!-- <mat-icon (click)=\"editRecentAdvancedSearch(recentSearch)\">edit\r\n                    </mat-icon> -->\r\n                </p>\r\n                <mat-icon class=\"cursor-pointer\" *ngIf=\"recentSearch[0] && recentSearch[0].isAdvancedSearch\"\r\n                    (click)=\"editRecentAdvancedSearch(recentSearch)\">edit\r\n                </mat-icon>\r\n            </mat-list-item>\r\n        </mat-list>\r\n        <p class=\"no-items\" *ngIf=\"recentSearches.length === 0\">No recent searches.</p>\r\n    </div>\r\n\r\n    <div class=\"advanced-search-link\">\r\n        <button mat-stroked-button (click)=\"openAdvancedSearch(false, false)\" mat-button>Advanced Search</button>\r\n    </div>\r\n</mat-dialog-content>",
            styles: ["::ng-deep .mat-dialog-container{padding:12px 24px 0}div.search-input .example-form,div.search-input .example-full-width{width:100%}div.saved-searches{margin:16px 0}div.saved-searches .saved-search-items ::ng-deep .mat-chip-list-wrapper{margin:4px 0;max-height:80px;overflow:auto}div.recent-searches{margin:16px 0}div.recent-searches .recent-search-items{max-height:204px;overflow:auto}div.recent-searches .recent-search-items mat-list-item mat-icon{font-size:18px;height:18px}div.recent-searches .heading,div.saved-searches .heading{font-size:large;margin:4px 0}div.recent-searches p.no-items,div.saved-searches p.no-items{margin:4px 0;text-align:center}div.advanced-search-link button{width:100%;margin-bottom:10px}.cursor-pointer{cursor:pointer;max-width:98%}.searchName{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], SimpleSearchComponent);
    return SimpleSearchComponent;
}());
export { SimpleSearchComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2ltcGxlLXNlYXJjaC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zZWFyY2gvc2ltcGxlLXNlYXJjaC9zaW1wbGUtc2VhcmNoLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3BGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUN2RixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSx5RUFBeUUsQ0FBQztBQUNySCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFJbEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFFcEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDdEYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQVFsQztJQU9JLCtCQUNXLFNBQThDLEVBQ3JCLElBQXNCLEVBQy9DLGNBQThCLEVBQzlCLE1BQWlCLEVBQ2pCLGFBQTRCLEVBQzVCLG1CQUF3QyxFQUN4QyxtQkFBd0MsRUFDeEMsaUJBQW9DLEVBQ3BDLGlCQUFvQztRQVJwQyxjQUFTLEdBQVQsU0FBUyxDQUFxQztRQUNyQixTQUFJLEdBQUosSUFBSSxDQUFrQjtRQUMvQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBZC9DLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLG1CQUFjLEdBQUcsRUFBRSxDQUFDO0lBY2hCLENBQUM7SUFFTCxzQ0FBTSxHQUFOLFVBQU8sYUFBYTtRQUNoQixJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxRQUFRLElBQUksYUFBYSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsdUNBQXVDLENBQUMsQ0FBQztZQUN2RSxPQUFPO1NBQ1Y7UUFDRCxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO1lBQ3hFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUM5QzthQUFNO1lBQ0gsSUFBTSxnQkFBZ0IsR0FBRyxDQUFDO29CQUN0QixpQkFBaUIsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYztvQkFDeEQsT0FBTyxFQUFFLGFBQWE7aUJBRXpCLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELHNEQUFzQixHQUF0QixVQUF1QixJQUFJO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxvREFBb0IsR0FBcEIsVUFBcUIsV0FBMkI7UUFDNUMsSUFBSSxXQUFXLENBQUMsV0FBVyxFQUFFO1lBQ3pCLElBQU0sZ0JBQWdCLEdBQW9CLFdBQVcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUM7WUFDekcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQzVDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDdkMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUM7WUFDNUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM1RSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUM1QztJQUNMLENBQUM7SUFFRCxpREFBaUIsR0FBakIsVUFBa0IsV0FBMkI7UUFBN0MsaUJBdUJDO1FBdEJHLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQzNELEtBQUssRUFBRSxLQUFLO1lBQ1osWUFBWSxFQUFFLElBQUk7WUFDbEIsSUFBSSxFQUFFO2dCQUNGLE9BQU8sRUFBRSwrQ0FBK0M7Z0JBQ3hELFlBQVksRUFBRSxLQUFLO2dCQUNuQixZQUFZLEVBQUUsSUFBSTthQUNyQjtTQUNKLENBQUMsQ0FBQztRQUNILFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ3BDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQ3pCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNoRyxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO29CQUNuRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM5QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQztnQkFDeEUsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtDQUFlLEdBQWYsVUFBZ0IsV0FBVztRQUN2QixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWTtlQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsMkJBQTJCLEVBQUU7WUFDbEUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMvQztJQUNMLENBQUM7SUFFRCx3REFBd0IsR0FBeEIsVUFBeUIsWUFBWTtRQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QixDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELG1EQUFtQixHQUFuQjtRQUFBLGlCQWNDO1FBYkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLHFDQUFxQyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUN0SSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDakMsS0FBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUM7aUJBQ2pDO3FCQUFNO29CQUNILEtBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2lCQUMzQjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixPQUFPLENBQUMsS0FBSyxDQUFDLHdEQUF3RCxDQUFDLENBQUM7WUFDNUUsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvREFBb0IsR0FBcEI7UUFDSSxxRUFBcUU7UUFDckUsSUFBTSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvSCxJQUFNLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNqQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNsQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUMxQixJQUFJLElBQUksRUFBRTtvQkFDTixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7NEJBQ2pCLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDcEM7cUJBQ0o7eUJBQU07d0JBQ0gscUJBQXFCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUNwQztpQkFFSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLENBQUMsY0FBYyxZQUFPLElBQUksR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsdURBQXVCLEdBQXZCLFVBQXdCLE9BQU87UUFBL0IsaUJBNEJDO1FBM0JHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxFQUFFLENBQUMsRUFBRTtZQUMxTCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQzNCLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDckssT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQzlCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNLElBQUksT0FBTyxDQUFDLFlBQVksSUFBSSxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLEVBQUUsRUFBRTtZQUMvRixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQzNCLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRTtvQkFDNUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQzlCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDMUQsUUFBUSxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRTtZQUN0QixLQUFLLEVBQUUsUUFBUTtZQUNmLGlCQUFpQjtZQUNqQixZQUFZLEVBQUUsSUFBSTtZQUNsQixJQUFJLEVBQUUsT0FBTztTQUNoQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDNUMsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO2dCQUNqQixpQ0FBaUM7YUFDcEM7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDaEM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBa0IsR0FBbEIsVUFBbUIsYUFBYSxFQUFFLFlBQVk7UUFBOUMsaUJBd0JDO1FBdkJHLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZO2VBQ25FLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQywyQkFBMkIsRUFBRTtZQUNsRSxJQUFNLFNBQU8sR0FBdUI7Z0JBQ2hDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVk7Z0JBQzNELG9CQUFvQixFQUFFLElBQUksQ0FBQyxhQUFhO2dCQUN4QyxXQUFXLEVBQUUsYUFBYTtnQkFDMUIsWUFBWSxFQUFFLFlBQVk7YUFDN0IsQ0FBQztZQUNGLElBQUksYUFBYSxFQUFFO2dCQUNmLElBQUksT0FBTyxhQUFhLEtBQUssUUFBUSxFQUFFO29CQUNuQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsa0JBQWtCO3dCQUNwRixJQUFJLGtCQUFrQixFQUFFOzRCQUNwQixTQUFPLENBQUMsV0FBVyxHQUFHLGtCQUFrQixDQUFDOzRCQUN6QyxLQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBTyxDQUFDLENBQUM7eUJBQ3pDO29CQUNMLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNO29CQUNILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFPLENBQUMsQ0FBQztpQkFDekM7YUFDSjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBTyxDQUFDLENBQUM7YUFDekM7U0FDSjtJQUNMLENBQUM7SUFFaUMsdUNBQU8sR0FBUDtRQUM5QixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUM3QjtJQUNMLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQztRQUN0RCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ3pDLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ25FLElBQUksS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsdUJBQXVCLENBQUM7b0JBQ2xFLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxFQUFFO29CQUN2RSxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUM7aUJBRW5FO3FCQUFNO29CQUNILEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztpQkFDN0U7YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDaEMsQ0FBQzs7Z0JBdE1xQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTtnQkFDQSxjQUFjO2dCQUN0QixTQUFTO2dCQUNGLGFBQWE7Z0JBQ1AsbUJBQW1CO2dCQUNuQixtQkFBbUI7Z0JBQ3JCLGlCQUFpQjtnQkFDakIsaUJBQWlCOztJQTBLYjtRQUFqQyxZQUFZLENBQUMsa0JBQWtCLENBQUM7d0RBSWhDO0lBOUxRLHFCQUFxQjtRQU5qQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLCt6R0FBNkM7O1NBRWhELENBQUM7UUFXTyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtPQVRuQixxQkFBcUIsQ0ErTWpDO0lBQUQsNEJBQUM7Q0FBQSxBQS9NRCxJQStNQztTQS9NWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0LCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50IH0gZnJvbSAnLi4vYWR2YW5jZWQtc2VhcmNoL2FkdmFuY2VkLXNlYXJjaC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTVNhdmVkU2VhcmNoIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL29iamVjdHMvTVBNU2F2ZWRTZWFyY2gnO1xyXG5pbXBvcnQgeyBTaW1wbGVTZWFyY2hEYXRhIH0gZnJvbSAnLi4vb2JqZWN0cy9TaW1wbGVTZWFyY2hEYXRhJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hEYXRhIH0gZnJvbSAnLi4vb2JqZWN0cy9BZHZhbmNlZFNlYXJjaERhdGEnO1xyXG5pbXBvcnQgeyBTZWFyY2hEYXRhU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3NlYXJjaC1kYXRhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb25kaXRpb25MaXN0IH0gZnJvbSAnLi4vb2JqZWN0cy9Db25kaXRpb25MaXN0JztcclxuaW1wb3J0IHsgU2VhcmNoU2F2ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9zZWFyY2guc2F2ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1zaW1wbGUtc2VhcmNoJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zaW1wbGUtc2VhcmNoLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3NpbXBsZS1zZWFyY2guY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNpbXBsZVNlYXJjaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgc2F2ZWRTZWFyY2hlcyA9IFtdO1xyXG4gICAgcmVjZW50U2VhcmNoZXMgPSBbXTtcclxuICAgIHNlYXJjaEtleXdvcmQ6IHN0cmluZztcclxuICAgIGFkdkRpYWxvZ1JlZjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8U2ltcGxlU2VhcmNoQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IFNpbXBsZVNlYXJjaERhdGEsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNlYXJjaENvbmZpZ1NlcnZpY2U6IFNlYXJjaENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNlYXJjaERhdGFTZXJ2aWNlOiBTZWFyY2hEYXRhU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2VhcmNoU2F2ZVNlcnZpY2U6IFNlYXJjaFNhdmVTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHNlYXJjaChzZWFyY2hLZXl3b3JkKSB7XHJcbiAgICAgICAgaWYgKHNlYXJjaEtleXdvcmQgPT0gbnVsbCB8fCBzZWFyY2hLZXl3b3JkID09PSAnJyB8fCAodHlwZW9mIChzZWFyY2hLZXl3b3JkKSA9PT0gJ3N0cmluZycgJiYgc2VhcmNoS2V5d29yZC50cmltKCkgPT09ICcnKSkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbygnS2luZGx5IGVudGVyIHRoZSB2YWx1ZSB0byBiZSBzZWFyY2hlZCcpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hLZXl3b3JkICYmIHNlYXJjaEtleXdvcmRbMF0gJiYgc2VhcmNoS2V5d29yZFswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xvc2VTaW1wbGVTZWFyY2hQb3B1cChzZWFyY2hLZXl3b3JkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBzZWFyY2hDb25kaXRpb25zID0gW3tcclxuICAgICAgICAgICAgICAgIG1ldGFkYXRhX2ZpZWxkX2lkOiB0aGlzLnNlYXJjaERhdGFTZXJ2aWNlLktFWVdPUkRfTUFQUEVSLFxyXG4gICAgICAgICAgICAgICAga2V5d29yZDogc2VhcmNoS2V5d29yZCxcclxuXHJcbiAgICAgICAgICAgIH1dO1xyXG4gICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFJlY2VudFNlYXJjaChzZWFyY2hLZXl3b3JkKTtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZVNpbXBsZVNlYXJjaFBvcHVwKHNlYXJjaENvbmRpdGlvbnMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjbG9zZVNpbXBsZVNlYXJjaFBvcHVwKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTYXZlZFNlYXJjaERldGFpbChzYXZlZFNlYXJjaDogTVBNU2F2ZWRTZWFyY2gpIHtcclxuICAgICAgICBpZiAoc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VhcmNoQ29uZGl0aW9uczogQ29uZGl0aW9uTGlzdFtdID0gc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb247XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnNbMF0uaXNBZHZhbmNlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgIHNlYXJjaENvbmRpdGlvbnNbMF0uc2F2ZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICBzZWFyY2hDb25kaXRpb25zWzBdLk5BTUUgPSBzYXZlZFNlYXJjaC5OQU1FO1xyXG4gICAgICAgICAgICBzZWFyY2hDb25kaXRpb25zWzBdLnNhdmVkU2VhcmNoSWQgPSBzYXZlZFNlYXJjaFsnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0UmVjZW50U2VhcmNoKHNlYXJjaENvbmRpdGlvbnMpO1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlU2ltcGxlU2VhcmNoUG9wdXAoc2F2ZWRTZWFyY2gpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVTYXZlZFNlYXJjaChzYXZlZFNlYXJjaDogTVBNU2F2ZWRTZWFyY2gpIHtcclxuICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiAnQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSBzYXZlZCBzZWFyY2g/JyxcclxuICAgICAgICAgICAgICAgIHN1Ym1pdEJ1dHRvbjogJ1llcycsXHJcbiAgICAgICAgICAgICAgICBjYW5jZWxCdXR0b246ICdObydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0ICYmIHJlc3VsdC5pc1RydWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFNhdmVTZXJ2aWNlLmRlbGV0ZVNhdmVkU2VhcmNoKHNhdmVkU2VhcmNoWydNUE1fU2F2ZWRfU2VhcmNoZXMtaWQnXS5JZCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEFsbFNhdmVkU2VhcmNoZXMoKS5zdWJzY3JpYmUoc2VhcmNoUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0Vycm9yIHdoaWxlIGRlbGV0aW5nIHNhdmVkIHNlYXJjaCcpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0U2F2ZWRTZWFyY2goc2F2ZWRTZWFyY2gpIHtcclxuICAgICAgICBpZiAodGhpcy5kYXRhICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZ1xyXG4gICAgICAgICAgICAmJiB0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hDb25maWcuYWR2YW5jZWRTZWFyY2hDb25maWd1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3BlbkFkdmFuY2VkU2VhcmNoKHNhdmVkU2VhcmNoLCBmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXRSZWNlbnRBZHZhbmNlZFNlYXJjaChyZXNlbnRTZWFyY2gpIHtcclxuICAgICAgICByZXNlbnRTZWFyY2guZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgICAgICAgZGF0YS5pc0VkaXQgPSB0cnVlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3BlbkFkdmFuY2VkU2VhcmNoKGZhbHNlLCByZXNlbnRTZWFyY2gpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFNhdmVkU2VhcmNoZXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaENvbmZpZ1NlcnZpY2UuZ2V0U2F2ZWRTZWFyY2hlc0ZvclZpZXdBbmRDdXJyZW50VXNlcih0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllci5WSUVXKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoZXMgPSByZXNwb25zZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaGVzID0gW107XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyB0aGUgc2F2ZWQgc2VhcmNoZXMuJyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFJlY2VudFNlYXJjaGVzKCkge1xyXG4gICAgICAgIC8vIGNvbnN0IGFsbFJlY2VudFNlYXJjaGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRSZWNlbnRTZWFyY2hlcygpO1xyXG4gICAgICAgIGNvbnN0IGFsbFJlY2VudFNlYXJjaGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRSZWNlbnRTZWFyY2hlc0J5Vmlldyh0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllci5WSUVXKTtcclxuICAgICAgICBjb25zdCBhbGxvd2VkUmVjZW50U2VhcmNoZXMgPSBbXTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShhbGxSZWNlbnRTZWFyY2hlcykpIHtcclxuICAgICAgICAgICAgYWxsUmVjZW50U2VhcmNoZXMuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYWR2SXRlbSA9IGl0ZW1bMF07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFkdkl0ZW0gJiYgYWR2SXRlbS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghYWR2SXRlbS5pc0VkaXQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsbG93ZWRSZWNlbnRTZWFyY2hlcy5wdXNoKGl0ZW0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxsb3dlZFJlY2VudFNlYXJjaGVzLnB1c2goaXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmVjZW50U2VhcmNoZXMgPSBbLi4ubmV3IFNldChhbGxvd2VkUmVjZW50U2VhcmNoZXMpXTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuQWR2YW5jZWRTZWFyY2hNb2RhbChhZHZEYXRhKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YS5zZWFyY2hEYXRhICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnICYmICh0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZy5OQU1FICE9PSAnJyB8fCB0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ1swXS5OQU1FICE9PSAnJykpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaGVzLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YS5OQU1FID09PSAodGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcuTkFNRSA/IHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnLk5BTUUgOiB0aGlzLmRhdGEuc2VhcmNoRGF0YS5hZHZhbmNlZFNlYXJjaENvbmZpZ1swXS5OQU1FKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkdkRhdGEuc2F2ZWRTZWFyY2ggPSBkYXRhO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2UgaWYgKGFkdkRhdGEucmVjZW50U2VhcmNoICYmIGFkdkRhdGEucmVjZW50U2VhcmNoWzBdICYmIGFkdkRhdGEucmVjZW50U2VhcmNoWzBdLk5BTUUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hlcy5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuTkFNRSA9PT0gYWR2RGF0YS5yZWNlbnRTZWFyY2hbMF0uTkFNRSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGFkdkRhdGEuc2F2ZWRTZWFyY2ggPSBkYXRhO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5hZHZEaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiB7IHRvcDogJzAnIH0sXHJcbiAgICAgICAgICAgIHdpZHRoOiAnMTEwMHB4JyxcclxuICAgICAgICAgICAgLy8gaGVpZ2h0OiAnNjUlJyxcclxuICAgICAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgICAgICBkYXRhOiBhZHZEYXRhXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5hZHZEaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3VsdCA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgLy8gIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShyZXN1bHQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkFkdmFuY2VkU2VhcmNoKHNhdmVkU2VhcmNoSWQsIHJlY2VudFNlYXJjaCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEgJiYgdGhpcy5kYXRhLnNlYXJjaERhdGEuc2VhcmNoQ29uZmlnXHJcbiAgICAgICAgICAgICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhLnNlYXJjaENvbmZpZy5hZHZhbmNlZFNlYXJjaENvbmZpZ3VyYXRpb24pIHtcclxuICAgICAgICAgICAgY29uc3QgYWR2RGF0YTogQWR2YW5jZWRTZWFyY2hEYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgYWR2YW5jZWRTZWFyY2hDb25maWdEYXRhOiB0aGlzLmRhdGEuc2VhcmNoRGF0YS5zZWFyY2hDb25maWcsXHJcbiAgICAgICAgICAgICAgICBhdmFpbGFibGVTYXZlZFNlYXJjaDogdGhpcy5zYXZlZFNlYXJjaGVzLFxyXG4gICAgICAgICAgICAgICAgc2F2ZWRTZWFyY2g6IHNhdmVkU2VhcmNoSWQsXHJcbiAgICAgICAgICAgICAgICByZWNlbnRTZWFyY2g6IHJlY2VudFNlYXJjaFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBpZiAoc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBzYXZlZFNlYXJjaElkID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5nZXRTYXZlZFNlYXJjaGVCeUlkKHNhdmVkU2VhcmNoSWQpLnN1YnNjcmliZShzYXZlZFNlYXJjaERldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2F2ZWRTZWFyY2hEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZHZEYXRhLnNhdmVkU2VhcmNoID0gc2F2ZWRTZWFyY2hEZXRhaWxzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vcGVuQWR2YW5jZWRTZWFyY2hNb2RhbChhZHZEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZW5BZHZhbmNlZFNlYXJjaE1vZGFsKGFkdkRhdGEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcGVuQWR2YW5jZWRTZWFyY2hNb2RhbChhZHZEYXRhKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCd3aW5kb3c6a2V5dXAuZXNjJykgb25LZXlVcCgpIHtcclxuICAgICAgICBpZiAodGhpcy5hZHZEaWFsb2dSZWYpIHtcclxuICAgICAgICAgICAgdGhpcy5hZHZEaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hLZXl3b3JkID0gdGhpcy5kYXRhLnNlYXJjaERhdGEuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgdGhpcy5nZXRBbGxTYXZlZFNlYXJjaGVzKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZGF0YS5zZWFyY2hEYXRhICYmIHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddICYmXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWdbJ01QTV9TYXZlZF9TZWFyY2hlcy1pZCddLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lZGl0U2F2ZWRTZWFyY2godGhpcy5kYXRhLnNlYXJjaERhdGEuYWR2YW5jZWRTZWFyY2hDb25maWcpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcGVuQWR2YW5jZWRTZWFyY2goZmFsc2UsIHRoaXMuZGF0YS5zZWFyY2hEYXRhLmFkdmFuY2VkU2VhcmNoQ29uZmlnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsUmVjZW50U2VhcmNoZXMoKTtcclxuICAgIH1cclxufVxyXG4iXX0=