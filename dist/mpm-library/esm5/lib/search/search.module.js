import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SimpleSearchComponent } from './simple-search/simple-search.component';
import { AdvancedSearchComponent } from './advanced-search/advanced-search.component';
import { SearchIconComponent } from './search-icon/search-icon.component';
import { MaterialModule } from '../material.module';
import { AdvancedSearchFieldComponent } from './advanced-search-field/advanced-search-field.component';
var SearchModule = /** @class */ (function () {
    function SearchModule() {
    }
    SearchModule = __decorate([
        NgModule({
            declarations: [
                SimpleSearchComponent,
                AdvancedSearchComponent,
                SearchIconComponent,
                AdvancedSearchFieldComponent,
            ],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [
                SimpleSearchComponent,
                SearchIconComponent,
                AdvancedSearchComponent
            ],
            entryComponents: [],
            providers: [DatePipe]
        })
    ], SearchModule);
    return SearchModule;
}());
export { SearchModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NlYXJjaC9zZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDaEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDdEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBc0J2RztJQUFBO0lBQTRCLENBQUM7SUFBaEIsWUFBWTtRQXBCeEIsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFO2dCQUNWLHFCQUFxQjtnQkFDckIsdUJBQXVCO2dCQUN2QixtQkFBbUI7Z0JBQ25CLDRCQUE0QjthQUMvQjtZQUNELE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLGNBQWM7YUFDakI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wscUJBQXFCO2dCQUNyQixtQkFBbUI7Z0JBQ25CLHVCQUF1QjthQUMxQjtZQUNELGVBQWUsRUFBRSxFQUFFO1lBQ25CLFNBQVMsRUFBRSxDQUFDLFFBQVEsQ0FBQztTQUN4QixDQUFDO09BRVcsWUFBWSxDQUFJO0lBQUQsbUJBQUM7Q0FBQSxBQUE3QixJQUE2QjtTQUFoQixZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlLCBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFNpbXBsZVNlYXJjaENvbXBvbmVudCB9IGZyb20gJy4vc2ltcGxlLXNlYXJjaC9zaW1wbGUtc2VhcmNoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFkdmFuY2VkU2VhcmNoQ29tcG9uZW50IH0gZnJvbSAnLi9hZHZhbmNlZC1zZWFyY2gvYWR2YW5jZWQtc2VhcmNoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlYXJjaEljb25Db21wb25lbnQgfSBmcm9tICcuL3NlYXJjaC1pY29uL3NlYXJjaC1pY29uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgQWR2YW5jZWRTZWFyY2hGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vYWR2YW5jZWQtc2VhcmNoLWZpZWxkL2FkdmFuY2VkLXNlYXJjaC1maWVsZC5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFNpbXBsZVNlYXJjaENvbXBvbmVudCxcclxuICAgICAgICBBZHZhbmNlZFNlYXJjaENvbXBvbmVudCxcclxuICAgICAgICBTZWFyY2hJY29uQ29tcG9uZW50LFxyXG4gICAgICAgIEFkdmFuY2VkU2VhcmNoRmllbGRDb21wb25lbnQsXHJcbiAgICBdLFxyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBTaW1wbGVTZWFyY2hDb21wb25lbnQsXHJcbiAgICAgICAgU2VhcmNoSWNvbkNvbXBvbmVudCxcclxuICAgICAgICBBZHZhbmNlZFNlYXJjaENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW10sXHJcbiAgICBwcm92aWRlcnM6IFtEYXRlUGlwZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hNb2R1bGUgeyB9XHJcbiJdfQ==