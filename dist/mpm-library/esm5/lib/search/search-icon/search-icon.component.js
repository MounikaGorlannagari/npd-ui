import { __decorate } from "tslib";
import { Component, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SimpleSearchComponent } from '../simple-search/simple-search.component';
import { SearchChangeEventService } from '../services/search-change-event.service';
import { SearchEventTypes } from '../objects/search-event-types';
import { SearchDataService } from '../services/search-data.service';
import { SearchConfigService } from '../../shared/services/search-config.service';
import { forkJoin, BehaviorSubject } from 'rxjs';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MPMSearchOperators } from '../../shared/services/indexer/constants/MPMSearchOperator.constants';
import { MPMSearchOperatorNames } from '../../shared/services/indexer/constants/MPMSearchOperatorName.constants';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { ActivatedRoute } from '@angular/router';
import { StateService } from '../../shared/services/state.service';
var SearchIconComponent = /** @class */ (function () {
    function SearchIconComponent(dialog, searchChangeEventService, searchDataService, searchConfigService, fieldConfigService, activatedRoute, sharingService, stateService) {
        this.dialog = dialog;
        this.searchChangeEventService = searchChangeEventService;
        this.searchDataService = searchDataService;
        this.searchConfigService = searchConfigService;
        this.fieldConfigService = fieldConfigService;
        this.activatedRoute = activatedRoute;
        this.sharingService = sharingService;
        this.stateService = stateService;
        this.search = new EventEmitter();
        this.isAdvancedSearch = false;
        this.searchName = null;
        this.searchData = [];
        this.savedSearchName = null;
        this.isSavedSearchData = new BehaviorSubject(false);
        this.isAdvancedSearchData = new BehaviorSubject(false);
        this.advSearchData = null;
    }
    SearchIconComponent.prototype.clearSearchValue = function (isEmitEvent) {
        this.searchValue = '';
        this.clearSearch = false;
        this.isAdvancedSearch = false;
        this.advancedSearchConfig = null;
        if (isEmitEvent) {
            this.search.emit([]);
        }
    };
    SearchIconComponent.prototype.openSimpleSearchPopup = function (advancedSearchConfig) {
        var _this = this;
        if (advancedSearchConfig && advancedSearchConfig[0]) {
            advancedSearchConfig[0].isEdit = true;
        }
        var dialogRef = this.dialog.open(SimpleSearchComponent, {
            position: {
                top: '0'
            },
            width: '575px',
            data: {
                searchData: {
                    searchValue: this.searchValue,
                    searchConfig: this.searchConfig,
                    advancedSearchConfig: advancedSearchConfig
                }
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result && result.length > 0) {
                if (result[0].isAdvancedSearch) {
                    _this.savedSearchName = result[0].NAME ? result[0].NAME : null;
                    _this.advancedSearchConfig = result;
                    _this.isAdvancedSearch = true;
                }
                else {
                    _this.isAdvancedSearch = false;
                    _this.searchValue = result[0].keyword || result[0].value;
                    _this.searchName = _this.searchValue;
                }
                _this.clearSearch = true;
                _this.search.emit(result);
            }
            else if (result && result.SEARCH_DATA && result.SEARCH_DATA.search_condition_list &&
                result.SEARCH_DATA.search_condition_list.search_condition &&
                result.SEARCH_DATA.search_condition_list.search_condition.length) {
                _this.savedSearchName = result.NAME;
                if (result.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                    _this.advancedSearchConfig = result;
                    _this.isAdvancedSearch = true;
                }
                else {
                    _this.isAdvancedSearch = false;
                    _this.searchValue = result[0].keyword || result[0].value;
                }
                _this.clearSearch = true;
                _this.search.emit(result.SEARCH_DATA.search_condition_list.search_condition);
            }
        });
    };
    SearchIconComponent.prototype.getAdvancedSearchConfig = function () {
        var _this = this;
        forkJoin([this.fieldConfigService.getAllMPMFieldConfig(), this.searchConfigService.setAllAdvancedSearchConfig(), this.searchConfigService.setAllSearchOperators()]).subscribe(function (response) {
            if (response && response[0] && response[1] && response[2]) {
                _this.searchConfig.metadataFields = response[0];
                _this.searchConfig.advancedSearchConfiguration = response[1];
                _this.searchChangeEventService.setAdvancedSearchConfig(_this.searchConfig.advancedSearchConfiguration);
                _this.searchConfig.searchOperatorList = response[2];
                _this.isAdvancedSearchData.next(true);
            }
        });
    };
    SearchIconComponent.prototype.initConfigs = function () {
        this.searchConfig = {
            advancedSearchConfiguration: [],
            metadataFields: [],
            searchOperatorList: [],
            // searchConfigId: '',
            searchIdentifier: null
        };
        this.getAdvancedSearchConfig();
        this.searchConfig.searchIdentifier = this.searchIdentifier;
    };
    SearchIconComponent.prototype.formAdvSearchData = function (data) {
        var searchDataList = JSON.parse(data);
        var advSearchConfigFields = this.searchConfigService.getAdvancedSearchConfigById(searchDataList[0].searchIdentifier);
        var advSearchOperators = this.searchConfigService.getAllSearchOperators();
        var advancedSearchConditionList = [];
        searchDataList.forEach(function (searchData, index) {
            var selectedField = advSearchConfigFields.R_PM_Search_Fields.find(function (searchConfig) { return searchConfig.MAPPER_NAME === searchData.mapperName; });
            var fieldOperators = advSearchOperators.filter(function (operator) { return operator.DATA_TYPE === selectedField.DATA_TYPE; });
            var selectedOperator = searchData.relationalOperatorName ? fieldOperators.find(function (operator) { return operator.NAME === searchData.relationalOperatorName; }) : '';
            var values = searchData.hasTwoValues ? searchData.value.split(' and ') : searchData.value;
            var filterRowData = {
                availableSearchFields: [],
                hasTwoValues: searchData.hasTwoValues,
                isFilterSelected: true,
                issearchValueRequired: searchData.value ? true : false,
                rowId: index + 1,
                searchField: selectedField.MPM_FIELD_CONFIG_ID,
                searchOperatorId: selectedOperator ? selectedOperator.OPERATOR_ID : '',
                searchOperatorName: selectedOperator ? selectedOperator.NAME : '',
                searchSecondValue: searchData.hasTwoValues ? (values[1] ? values[1] : '') : '',
                searchValue: searchData.hasTwoValues ? (values[0] ? values[0] : '') : searchData.value
            };
            var advancedSearchCondition = {
                field_id: selectedField.INDEXER_FIELD_ID,
                filterRowData: filterRowData,
                isAdvancedSearch: true,
                mapper_field_id: selectedField.MPM_FIELD_CONFIG_ID,
                relational_operator: MPMSearchOperators.AND,
                relational_operator_id: selectedOperator ? selectedOperator.OPERATOR_ID : MPMSearchOperators.IS,
                relational_operator_name: selectedOperator ? selectedOperator.NAME : MPMSearchOperatorNames.IS,
                searchIdentifier: searchData.searchIdentifier,
                type: selectedField.DATA_TYPE,
                value: searchData.value,
                mapper_name: selectedField.MAPPER_NAME,
                view: searchData.view
            };
            advancedSearchConditionList.push(advancedSearchCondition);
        });
        return advancedSearchConditionList;
    };
    SearchIconComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.tmpViewToggleEventServiceSubscription$ = this.searchChangeEventService.onViewToggleSearch.subscribe(function (data) {
            if (data && data.action === SearchEventTypes.CLEAR) {
                _this.clearSearchValue(false);
            }
        });
        this.tmpSearchChangeEventServiceSubscription$ = this.searchChangeEventService.onSearchViewChange.subscribe(function (viewConfigDetails) {
            _this.searchIdentifier = viewConfigDetails ? viewConfigDetails : null;
            if (_this.searchIdentifier) {
                _this.searchDataService.setSearchData([]);
            }
            _this.initConfigs();
            _this.searchChangeEventService.updateOnViewChange({ action: SearchEventTypes.CLEAR });
            _this.activatedRoute.queryParams.subscribe(function (params) {
                if ((params.viewName !== _this.selectedView || params.groupByFilter !== _this.groupByFilter ||
                    params.deliverableTaskFilter !== _this.deliverableTaskFilter || params.menuName !== _this.menuName) ||
                    !((params.searchName === _this.searchName) || (params.savedSearchName === _this.savedSearchName) || (params.advSearchData === _this.advSearchData))) {
                    if (params.viewName && _this.selectedView && _this.selectedView !== params.viewName) {
                        _this.selectedView = params.viewName;
                        _this.clearSearchValue(true);
                    }
                    else if ((params.groupByFilter && _this.groupByFilter && _this.groupByFilter !== params.groupByFilter) ||
                        params.deliverableTaskFilter && _this.deliverableTaskFilter && _this.deliverableTaskFilter !== params.deliverableTaskFilter) {
                        _this.groupByFilter = params.groupByFilter;
                        _this.deliverableTaskFilter = params.deliverableTaskFilter;
                        _this.clearSearchValue(true);
                    }
                    else {
                        _this.selectedView = params.viewName;
                        _this.groupByFilter = params.groupByFilter;
                        _this.deliverableTaskFilter = params.deliverableTaskFilter;
                        if (params.searchName && _this.searchIdentifier) {
                            if (_this.searchName !== params.searchName || _this.menuName !== params.menuName) {
                                _this.menuName = params.menuName;
                                _this.searchName = params.searchName;
                                _this.searchData = [];
                                _this.searchData.push({
                                    keyword: _this.searchName,
                                    metadata_field_id: 'MPM.FIELD.ALL_KEYWORD'
                                });
                                if (_this.searchData && _this.searchData[0] && _this.searchData[0].keyword) {
                                    var searches = _this.sharingService.getRecentSearches();
                                    if (searches) {
                                        var isSearched = searches.find(function (searchData) { return searchData === _this.searchName; });
                                        if (!isSearched) {
                                            _this.sharingService.setRecentSearch(_this.searchName);
                                        }
                                    }
                                    else {
                                        _this.sharingService.setRecentSearch(_this.searchName);
                                    }
                                    if (_this.searchData && _this.searchData.length > 0) {
                                        if (_this.searchData[0].isAdvancedSearch) {
                                            _this.advancedSearchConfig = _this.searchData;
                                            _this.isAdvancedSearch = true;
                                        }
                                        else {
                                            _this.isAdvancedSearch = false;
                                            _this.searchValue = _this.searchData[0].keyword || _this.searchData[0].value;
                                        }
                                        _this.clearSearch = true;
                                        _this.search.emit(_this.searchData);
                                    }
                                }
                            }
                        }
                        else if (!params.searchName && _this.searchName !== null) {
                            _this.searchName = null;
                        }
                        if (params.savedSearchName && _this.searchIdentifier) {
                            if (_this.savedSearchName !== params.savedSearchName || _this.menuName !== params.menuName) {
                                _this.menuName = params.menuName;
                                _this.savedSearchName = params.savedSearchName;
                                _this.isSavedSearchTrigger = false;
                                _this.isSavedSearchData.next(true);
                            }
                        }
                        else if (!params.savedSearchName && _this.savedSearchName !== null) {
                            _this.savedSearchName = null;
                        }
                        if (params.advSearchData && _this.searchIdentifier) {
                            _this.advSearchData = params.advSearchData;
                            var advancedSearchData = _this.stateService.getAdvancedSearch(params.menuName);
                            if (advancedSearchData && advancedSearchData.length > 0) {
                                if (advancedSearchData !== _this.advancedSearchConfig || _this.menuName !== params.menuName) {
                                    _this.menuName = params.menuName;
                                    if (advancedSearchData[0].isAdvancedSearch) {
                                        _this.advancedSearchConfig = advancedSearchData;
                                        _this.isAdvancedSearch = true;
                                    }
                                    _this.clearSearch = true;
                                    _this.search.emit(advancedSearchData);
                                }
                            }
                            else {
                                _this.isAdvancedSearchTrigger = false;
                                _this.isAdvancedSearchData.subscribe(function (response) {
                                    if (response && !_this.isAdvancedSearchTrigger && _this.searchConfig.advancedSearchConfiguration) {
                                        _this.isAdvancedSearchTrigger = true;
                                        var searchCondition = _this.formAdvSearchData(_this.advSearchData);
                                        _this.sharingService.setRecentSearch(searchCondition);
                                        if (searchCondition && searchCondition.length > 0) {
                                            if (searchCondition[0].isAdvancedSearch) {
                                                _this.advancedSearchConfig = searchCondition;
                                                _this.isAdvancedSearch = true;
                                            }
                                            _this.clearSearch = true;
                                            _this.search.emit(searchCondition);
                                        }
                                    }
                                });
                            }
                        }
                        else if (!params.advSearchData && _this.advSearchData !== null) {
                            _this.advSearchData = null;
                        }
                        if (!params.searchName && !params.advSearchData && !params.savedSearchName) {
                            _this.menuName = params.menuName;
                        }
                    }
                }
            });
        });
        this.isSavedSearchData.subscribe(function (res) {
            if (res) {
                if (_this.savedSearchName && !_this.isSavedSearchTrigger && _this.searchConfig && _this.searchConfig.searchIdentifier && _this.searchConfig.searchIdentifier.VIEW) {
                    _this.searchConfigService.getSavedSearchesForViewAndCurrentUser(_this.searchConfig.searchIdentifier.VIEW).subscribe(function (responseData) {
                        if (responseData && responseData.length > 0) {
                            _this.savedSearches = responseData;
                        }
                        else {
                            _this.savedSearches = [];
                        }
                        if (_this.savedSearches && !_this.isSavedSearchTrigger) {
                            var result_1 = _this.savedSearches.find(function (data) { return data.NAME === _this.savedSearchName; });
                            if (result_1 && result_1.SEARCH_DATA && result_1.SEARCH_DATA.search_condition_list &&
                                result_1.SEARCH_DATA.search_condition_list.search_condition &&
                                result_1.SEARCH_DATA.search_condition_list.search_condition.length) {
                                result_1.SEARCH_DATA.search_condition_list.search_condition[0].savedSearchId = result_1['MPM_Saved_Searches-id'].Id;
                                result_1.SEARCH_DATA.search_condition_list.search_condition[0].NAME = result_1.NAME;
                                var searches = _this.sharingService.getRecentSearches();
                                if (searches) {
                                    var isSearched = searches.find(function (searchData) { return searchData[0].savedSearchId === result_1.SEARCH_DATA.search_condition_list.search_condition[0].savedSearchId; });
                                    if (!isSearched) {
                                        _this.sharingService.setRecentSearch(result_1.SEARCH_DATA.search_condition_list.search_condition);
                                    }
                                }
                                else {
                                    _this.sharingService.setRecentSearch(result_1.SEARCH_DATA.search_condition_list.search_condition);
                                }
                                _this.isSavedSearchTrigger = true;
                                if (result_1.SEARCH_DATA.search_condition_list.search_condition[0].isAdvancedSearch) {
                                    _this.advancedSearchConfig = result_1;
                                    _this.isAdvancedSearch = true;
                                }
                                else {
                                    _this.isAdvancedSearch = false;
                                    _this.searchValue = result_1[0].keyword || result_1[0].value;
                                }
                                _this.clearSearch = true;
                                _this.search.emit(result_1.SEARCH_DATA.search_condition_list.search_condition);
                            }
                        }
                    });
                }
            }
        });
    };
    SearchIconComponent.prototype.ngOnDestroy = function () {
        if (this.tmpViewToggleEventServiceSubscription$) {
            this.tmpViewToggleEventServiceSubscription$.unsubscribe();
        }
        if (this.tmpSearchChangeEventServiceSubscription$) {
            this.tmpSearchChangeEventServiceSubscription$.unsubscribe();
        }
    };
    SearchIconComponent.ctorParameters = function () { return [
        { type: MatDialog },
        { type: SearchChangeEventService },
        { type: SearchDataService },
        { type: SearchConfigService },
        { type: FieldConfigService },
        { type: ActivatedRoute },
        { type: SharingService },
        { type: StateService }
    ]; };
    __decorate([
        Input()
    ], SearchIconComponent.prototype, "backgroundColor", void 0);
    __decorate([
        Input()
    ], SearchIconComponent.prototype, "searchIdentifier", void 0);
    __decorate([
        Output()
    ], SearchIconComponent.prototype, "search", void 0);
    SearchIconComponent = __decorate([
        Component({
            selector: 'mpm-search-icon',
            template: "<div class=\"search-box\" [style.background-color]=\"backgroundColor\" (click)=\"openSimpleSearchPopup(null)\"\r\n    *ngIf=\"searchIdentifier\">\r\n    <button mat-icon-button color=\"primary\" matTooltip=\"Search\">\r\n        <mat-icon>search</mat-icon>\r\n    </button>\r\n\r\n    <span class=\"placeholder\" *ngIf=\"!searchValue && !isAdvancedSearch\">Search for content..</span>\r\n\r\n    <mat-chip-list #chipList aria-label=\"Current Search\" *ngIf=\"searchValue && !isAdvancedSearch\" class=\"\">\r\n        <mat-chip *ngIf=\"!isAdvancedSearch\" [removable]=\"true\" (removed)=\"clearSearchValue(true)\">\r\n            Keyword: <span class=\"wrap-text-custom\" matTooltip=\"{{searchValue}}\"> {{searchValue}} </span>\r\n            <mat-icon matTooltip=\"Clear\" matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n\r\n    <mat-chip-list #chipList aria-label=\"Current Search\" *ngIf=\"isAdvancedSearch && advancedSearchConfig\">\r\n        <mat-chip class=\"cursor-pointer\" (click)=\"openSimpleSearchPopup(advancedSearchConfig)\" [removable]=\"true\"\r\n            (removed)=\"clearSearchValue(true)\">\r\n            <span class=\"wrap-text-custom\"\r\n                matTooltip=\"{{advancedSearchConfig.NAME ? advancedSearchConfig.NAME : (advancedSearchConfig[0] && advancedSearchConfig[0].NAME ? advancedSearchConfig[0].NAME : 'Advanced Search Criteria')}}\">\r\n                {{advancedSearchConfig.NAME ? advancedSearchConfig.NAME : (advancedSearchConfig[0] && advancedSearchConfig[0].NAME ? advancedSearchConfig[0].NAME : 'Advanced Search Criteria')}}\r\n            </span>\r\n            <mat-icon matTooltip=\"Clear\" matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n    </mat-chip-list>\r\n</div>",
            styles: [".search-box{display:flex;flex-direction:row;width:450px;padding:12px;justify-content:flex-start;align-items:center;cursor:pointer}.search-box .placeholder{font-size:14px;font-weight:300}.search-box mat-chip-list{overflow:hidden}.search-box mat-chip-list mat-chip{max-width:98%}.cursor-pointer{cursor:pointer}.wrap-text-custom{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
        })
    ], SearchIconComponent);
    return SearchIconComponent;
}());
export { SearchIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWljb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2VhcmNoL3NlYXJjaC1pY29uL3NlYXJjaC1pY29uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDakYsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFFbkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDakUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDakQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFFQUFxRSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ2pILE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBSWhGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFTbkU7SUE0QkksNkJBQ1csTUFBaUIsRUFDakIsd0JBQWtELEVBQ2xELGlCQUFvQyxFQUNwQyxtQkFBd0MsRUFDeEMsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLFlBQTBCO1FBUDFCLFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQUNsRCxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBakMzQixXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUs5QyxxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFNekIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLHNCQUFpQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQ3hELHlCQUFvQixHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBUTNELGtCQUFhLEdBQUcsSUFBSSxDQUFDO0lBV2pCLENBQUM7SUFFTCw4Q0FBZ0IsR0FBaEIsVUFBaUIsV0FBVztRQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxXQUFXLEVBQUU7WUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUN4QjtJQUNMLENBQUM7SUFFRCxtREFBcUIsR0FBckIsVUFBc0Isb0JBQTZDO1FBQW5FLGlCQThDQztRQTdDRyxJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2pELG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7U0FDekM7UUFDRCxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUN0RCxRQUFRLEVBQUU7Z0JBQ04sR0FBRyxFQUFFLEdBQUc7YUFDWDtZQUNELEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFO2dCQUNGLFVBQVUsRUFBRTtvQkFDUixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7b0JBQzdCLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtvQkFDL0Isb0JBQW9CLEVBQUUsb0JBQW9CO2lCQUM3QzthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDcEMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO29CQUM1QixLQUFJLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDOUQsS0FBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQztvQkFDbkMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztpQkFDaEM7cUJBQU07b0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztvQkFDOUIsS0FBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ3hELEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztpQkFDdEM7Z0JBQ0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzVCO2lCQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUI7Z0JBQy9FLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCO2dCQUN6RCxNQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDbEUsS0FBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNuQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUU7b0JBQy9FLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUM7b0JBQ25DLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQ2hDO3FCQUFNO29CQUNILEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2lCQUMzRDtnQkFDRCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDeEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQy9FO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscURBQXVCLEdBQXZCO1FBQUEsaUJBVUM7UUFURyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsb0JBQW9CLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsMEJBQTBCLEVBQUUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNsTCxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDdkQsS0FBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxLQUFJLENBQUMsWUFBWSxDQUFDLDJCQUEyQixHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUQsS0FBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsMkJBQTJCLENBQUMsQ0FBQztnQkFDckcsS0FBSSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25ELEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRztZQUNoQiwyQkFBMkIsRUFBRSxFQUFFO1lBQy9CLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLGtCQUFrQixFQUFFLEVBQUU7WUFDdEIsc0JBQXNCO1lBQ3RCLGdCQUFnQixFQUFFLElBQUk7U0FDekIsQ0FBQztRQUNGLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQy9ELENBQUM7SUFFRCwrQ0FBaUIsR0FBakIsVUFBa0IsSUFBSTtRQUNsQixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hDLElBQU0scUJBQXFCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLDJCQUEyQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZILElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDNUUsSUFBTSwyQkFBMkIsR0FBRyxFQUFFLENBQUM7UUFDdkMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQVUsRUFBRSxLQUFLO1lBQ3JDLElBQU0sYUFBYSxHQUFHLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxXQUFXLEtBQUssVUFBVSxDQUFDLFVBQVUsRUFBbEQsQ0FBa0QsQ0FBQyxDQUFDO1lBQ3hJLElBQU0sY0FBYyxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLFFBQVEsQ0FBQyxTQUFTLEtBQUssYUFBYSxDQUFDLFNBQVMsRUFBOUMsQ0FBOEMsQ0FBQyxDQUFDO1lBQzdHLElBQU0sZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUSxJQUFJLE9BQUEsUUFBUSxDQUFDLElBQUksS0FBSyxVQUFVLENBQUMsc0JBQXNCLEVBQW5ELENBQW1ELENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3ZKLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO1lBQzVGLElBQU0sYUFBYSxHQUFHO2dCQUNsQixxQkFBcUIsRUFBRSxFQUFFO2dCQUN6QixZQUFZLEVBQUUsVUFBVSxDQUFDLFlBQVk7Z0JBQ3JDLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLHFCQUFxQixFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDdEQsS0FBSyxFQUFFLEtBQUssR0FBRyxDQUFDO2dCQUNoQixXQUFXLEVBQUUsYUFBYSxDQUFDLG1CQUFtQjtnQkFDOUMsZ0JBQWdCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDdEUsa0JBQWtCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDakUsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzlFLFdBQVcsRUFBRSxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUs7YUFDekYsQ0FBQztZQUNGLElBQU0sdUJBQXVCLEdBQUc7Z0JBQzVCLFFBQVEsRUFBRSxhQUFhLENBQUMsZ0JBQWdCO2dCQUN4QyxhQUFhLEVBQUUsYUFBYTtnQkFDNUIsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsZUFBZSxFQUFFLGFBQWEsQ0FBQyxtQkFBbUI7Z0JBQ2xELG1CQUFtQixFQUFFLGtCQUFrQixDQUFDLEdBQUc7Z0JBQzNDLHNCQUFzQixFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7Z0JBQy9GLHdCQUF3QixFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLEVBQUU7Z0JBQzlGLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxnQkFBZ0I7Z0JBQzdDLElBQUksRUFBRSxhQUFhLENBQUMsU0FBUztnQkFDN0IsS0FBSyxFQUFFLFVBQVUsQ0FBQyxLQUFLO2dCQUN2QixXQUFXLEVBQUUsYUFBYSxDQUFDLFdBQVc7Z0JBQ3RDLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSTthQUN4QixDQUFDO1lBQ0YsMkJBQTJCLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLDJCQUEyQixDQUFDO0lBQ3ZDLENBQUM7SUFFRCxzQ0FBUSxHQUFSO1FBQUEsaUJBNkpDO1FBNUpHLElBQUksQ0FBQyxzQ0FBc0MsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBc0I7WUFDNUgsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNoQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLHdDQUF3QyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsVUFBQyxpQkFBNkI7WUFDckksS0FBSSxDQUFDLGdCQUFnQixHQUFHLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3JFLElBQUksS0FBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN2QixLQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzVDO1lBQ0QsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ3JGLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU07Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLEtBQUksQ0FBQyxZQUFZLElBQUksTUFBTSxDQUFDLGFBQWEsS0FBSyxLQUFJLENBQUMsYUFBYTtvQkFDckYsTUFBTSxDQUFDLHFCQUFxQixLQUFLLEtBQUksQ0FBQyxxQkFBcUIsSUFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLEtBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ2pHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsS0FBSyxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO29CQUNsSixJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLFlBQVksSUFBSSxLQUFJLENBQUMsWUFBWSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7d0JBQy9FLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzt3QkFDcEMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUMvQjt5QkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxhQUFhLEtBQUssTUFBTSxDQUFDLGFBQWEsQ0FBQzt3QkFDbEcsTUFBTSxDQUFDLHFCQUFxQixJQUFJLEtBQUksQ0FBQyxxQkFBcUIsSUFBSSxLQUFJLENBQUMscUJBQXFCLEtBQUssTUFBTSxDQUFDLHFCQUFxQixFQUFFO3dCQUMzSCxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7d0JBQzFDLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUM7d0JBQzFELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDL0I7eUJBQU07d0JBQ0gsS0FBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO3dCQUNwQyxLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7d0JBQzFDLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUMscUJBQXFCLENBQUM7d0JBQzFELElBQUksTUFBTSxDQUFDLFVBQVUsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQzVDLElBQUksS0FBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxRQUFRLEtBQUssTUFBTSxDQUFDLFFBQVEsRUFBRTtnQ0FDNUUsS0FBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO2dDQUNoQyxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0NBQ3BDLEtBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dDQUNyQixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztvQ0FDakIsT0FBTyxFQUFFLEtBQUksQ0FBQyxVQUFVO29DQUN4QixpQkFBaUIsRUFBRSx1QkFBdUI7aUNBQzdDLENBQUMsQ0FBQztnQ0FDSCxJQUFJLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRTtvQ0FDckUsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29DQUN6RCxJQUFJLFFBQVEsRUFBRTt3Q0FDVixJQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsVUFBVSxJQUFJLE9BQUEsVUFBVSxLQUFLLEtBQUksQ0FBQyxVQUFVLEVBQTlCLENBQThCLENBQUMsQ0FBQzt3Q0FDL0UsSUFBSSxDQUFDLFVBQVUsRUFBRTs0Q0FDYixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7eUNBQ3hEO3FDQUNKO3lDQUFNO3dDQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDeEQ7b0NBQ0QsSUFBSSxLQUFJLENBQUMsVUFBVSxJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDL0MsSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFOzRDQUNyQyxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQzs0Q0FDNUMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQzt5Q0FDaEM7NkNBQU07NENBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQzs0Q0FDOUIsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzt5Q0FDN0U7d0NBQ0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7d0NBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztxQ0FDckM7aUNBQ0o7NkJBQ0o7eUJBQ0o7NkJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7NEJBQ3ZELEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3lCQUMxQjt3QkFDRCxJQUFJLE1BQU0sQ0FBQyxlQUFlLElBQUksS0FBSSxDQUFDLGdCQUFnQixFQUFFOzRCQUNqRCxJQUFJLEtBQUksQ0FBQyxlQUFlLEtBQUssTUFBTSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0NBQ3RGLEtBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztnQ0FDaEMsS0FBSSxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDO2dDQUM5QyxLQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO2dDQUNsQyxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNyQzt5QkFDSjs2QkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsSUFBSSxLQUFJLENBQUMsZUFBZSxLQUFLLElBQUksRUFBRTs0QkFDakUsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7eUJBQy9CO3dCQUNELElBQUksTUFBTSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQy9DLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQzs0QkFDMUMsSUFBTSxrQkFBa0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDaEYsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dDQUNyRCxJQUFJLGtCQUFrQixLQUFLLEtBQUksQ0FBQyxvQkFBb0IsSUFBSSxLQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sQ0FBQyxRQUFRLEVBQUU7b0NBQ3ZGLEtBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztvQ0FDaEMsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRTt3Q0FDeEMsS0FBSSxDQUFDLG9CQUFvQixHQUFHLGtCQUFrQixDQUFDO3dDQUMvQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO3FDQUNoQztvQ0FDRCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztvQ0FDeEIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztpQ0FDeEM7NkJBQ0o7aUNBQU07Z0NBQ0gsS0FBSSxDQUFDLHVCQUF1QixHQUFHLEtBQUssQ0FBQztnQ0FDckMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0NBQ3hDLElBQUksUUFBUSxJQUFJLENBQUMsS0FBSSxDQUFDLHVCQUF1QixJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsMkJBQTJCLEVBQUU7d0NBQzVGLEtBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7d0NBQ3BDLElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7d0NBQ25FLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dDQUNyRCxJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0Q0FDL0MsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUU7Z0RBQ3JDLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxlQUFlLENBQUM7Z0RBQzVDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7NkNBQ2hDOzRDQUNELEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDOzRDQUN4QixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzt5Q0FDckM7cUNBQ0o7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NkJBQ047eUJBQ0o7NkJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksS0FBSSxDQUFDLGFBQWEsS0FBSyxJQUFJLEVBQUU7NEJBQzdELEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO3lCQUM3Qjt3QkFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFOzRCQUN4RSxLQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7eUJBQ25DO3FCQUNKO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ2hDLElBQUksR0FBRyxFQUFFO2dCQUNMLElBQUksS0FBSSxDQUFDLGVBQWUsSUFBSSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsSUFBSSxLQUFJLENBQUMsWUFBWSxJQUFJLEtBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7b0JBQzFKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxxQ0FBcUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFlBQVk7d0JBQzFILElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUN6QyxLQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQzt5QkFDckM7NkJBQU07NEJBQ0gsS0FBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7eUJBQzNCO3dCQUNELElBQUksS0FBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsRUFBRTs0QkFDbEQsSUFBTSxRQUFNLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLEtBQUksQ0FBQyxlQUFlLEVBQWxDLENBQWtDLENBQUMsQ0FBQzs0QkFDbkYsSUFBSSxRQUFNLElBQUksUUFBTSxDQUFDLFdBQVcsSUFBSSxRQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtnQ0FDeEUsUUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0I7Z0NBQ3pELFFBQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dDQUNsRSxRQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsR0FBRyxRQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0NBQ2hILFFBQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLFFBQU0sQ0FBQyxJQUFJLENBQUM7Z0NBQ2hGLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQ0FDekQsSUFBSSxRQUFRLEVBQUU7b0NBQ1YsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLFVBQVUsSUFBSSxPQUFBLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEtBQUssUUFBTSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLEVBQTFHLENBQTBHLENBQUMsQ0FBQztvQ0FDM0osSUFBSSxDQUFDLFVBQVUsRUFBRTt3Q0FDYixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxRQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUM7cUNBQ2xHO2lDQUNKO3FDQUFNO29DQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFFBQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztpQ0FDbEc7Z0NBQ0QsS0FBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztnQ0FDakMsSUFBSSxRQUFNLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFO29DQUMvRSxLQUFJLENBQUMsb0JBQW9CLEdBQUcsUUFBTSxDQUFDO29DQUNuQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2lDQUNoQztxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO29DQUM5QixLQUFJLENBQUMsV0FBVyxHQUFHLFFBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksUUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQ0FDM0Q7Z0NBQ0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0NBQ3hCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQU0sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs2QkFDL0U7eUJBQ0o7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFXLEdBQVg7UUFDSSxJQUFJLElBQUksQ0FBQyxzQ0FBc0MsRUFBRTtZQUM3QyxJQUFJLENBQUMsc0NBQXNDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDN0Q7UUFDRCxJQUFJLElBQUksQ0FBQyx3Q0FBd0MsRUFBRTtZQUMvQyxJQUFJLENBQUMsd0NBQXdDLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDL0Q7SUFDTCxDQUFDOztnQkEzU2tCLFNBQVM7Z0JBQ1Msd0JBQXdCO2dCQUMvQixpQkFBaUI7Z0JBQ2YsbUJBQW1CO2dCQUNwQixrQkFBa0I7Z0JBQ3RCLGNBQWM7Z0JBQ2QsY0FBYztnQkFDaEIsWUFBWTs7SUFuQzVCO1FBQVIsS0FBSyxFQUFFO2dFQUFpQjtJQUNoQjtRQUFSLEtBQUssRUFBRTtpRUFBa0I7SUFDaEI7UUFBVCxNQUFNLEVBQUU7dURBQXFDO0lBSHJDLG1CQUFtQjtRQU4vQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLDZ1REFBMkM7O1NBRTlDLENBQUM7T0FFVyxtQkFBbUIsQ0F5VS9CO0lBQUQsMEJBQUM7Q0FBQSxBQXpVRCxJQXlVQztTQXpVWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgU2ltcGxlU2VhcmNoQ29tcG9uZW50IH0gZnJvbSAnLi4vc2ltcGxlLXNlYXJjaC9zaW1wbGUtc2VhcmNoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlYXJjaENoYW5nZUV2ZW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3NlYXJjaC1jaGFuZ2UtZXZlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaEV2ZW50TW9kYWwgfSBmcm9tICcuLi9vYmplY3RzL3NlYXJjaC1ldmVudC1tb2RhbCc7XHJcbmltcG9ydCB7IFNlYXJjaEV2ZW50VHlwZXMgfSBmcm9tICcuLi9vYmplY3RzL3NlYXJjaC1ldmVudC10eXBlcyc7XHJcbmltcG9ydCB7IFNlYXJjaERhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvc2VhcmNoLWRhdGEuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvc2VhcmNoLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgZm9ya0pvaW4sIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvcnMgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9jb25zdGFudHMvTVBNU2VhcmNoT3BlcmF0b3IuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgTVBNU2VhcmNoT3BlcmF0b3JOYW1lcyB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9pbmRleGVyL2NvbnN0YW50cy9NUE1TZWFyY2hPcGVyYXRvck5hbWUuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgRmllbGRDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2ZpZWxkLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlnRGF0YSB9IGZyb20gJy4uL29iamVjdHMvU2VhcmNoQ29uZmlnRGF0YSc7XHJcbmltcG9ydCB7IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNQWR2YW5jZWRTZWFyY2hDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL3N0YXRlLnNlcnZpY2UnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tc2VhcmNoLWljb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NlYXJjaC1pY29uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3NlYXJjaC1pY29uLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hJY29uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgQElucHV0KCkgYmFja2dyb3VuZENvbG9yO1xyXG4gICAgQElucHV0KCkgc2VhcmNoSWRlbnRpZmllcjtcclxuICAgIEBPdXRwdXQoKSBzZWFyY2ggPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcclxuXHJcbiAgICBzZWFyY2hWYWx1ZTogc3RyaW5nO1xyXG4gICAgY2xlYXJTZWFyY2g6IGJvb2xlYW47XHJcbiAgICBzZWFyY2hDb25maWc6IFNlYXJjaENvbmZpZ0RhdGE7XHJcbiAgICBpc0FkdmFuY2VkU2VhcmNoID0gZmFsc2U7XHJcbiAgICBhZHZhbmNlZFNlYXJjaENvbmZpZzsgLy8gTVBNQWR2YW5jZWRTZWFyY2hDb25maWdcclxuXHJcbiAgICB0bXBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2VTdWJzY3JpcHRpb24kO1xyXG4gICAgdG1wVmlld1RvZ2dsZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQ7XHJcblxyXG4gICAgc2VhcmNoTmFtZSA9IG51bGw7XHJcbiAgICBzZWFyY2hEYXRhID0gW107XHJcbiAgICBzYXZlZFNlYXJjaE5hbWUgPSBudWxsO1xyXG4gICAgaXNTYXZlZFNlYXJjaERhdGEgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcclxuICAgIGlzQWR2YW5jZWRTZWFyY2hEYXRhID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XHJcbiAgICBpc0FkdmFuY2VkU2VhcmNoVHJpZ2dlcjtcclxuICAgIHNhdmVkU2VhcmNoZXM7XHJcbiAgICBpc1NhdmVkU2VhcmNoVHJpZ2dlcjtcclxuICAgIHNlbGVjdGVkVmlldztcclxuICAgIGdyb3VwQnlGaWx0ZXI7XHJcbiAgICBkZWxpdmVyYWJsZVRhc2tGaWx0ZXI7XHJcbiAgICBtZW51TmFtZTtcclxuICAgIGFkdlNlYXJjaERhdGEgPSBudWxsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgc2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlOiBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNlYXJjaERhdGFTZXJ2aWNlOiBTZWFyY2hEYXRhU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2VhcmNoQ29uZmlnU2VydmljZTogU2VhcmNoQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBzdGF0ZVNlcnZpY2U6IFN0YXRlU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjbGVhclNlYXJjaFZhbHVlKGlzRW1pdEV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hWYWx1ZSA9ICcnO1xyXG4gICAgICAgIHRoaXMuY2xlYXJTZWFyY2ggPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gbnVsbDtcclxuICAgICAgICBpZiAoaXNFbWl0RXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdChbXSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5TaW1wbGVTZWFyY2hQb3B1cChhZHZhbmNlZFNlYXJjaENvbmZpZzogTVBNQWR2YW5jZWRTZWFyY2hDb25maWcpIHtcclxuICAgICAgICBpZiAoYWR2YW5jZWRTZWFyY2hDb25maWcgJiYgYWR2YW5jZWRTZWFyY2hDb25maWdbMF0pIHtcclxuICAgICAgICAgICAgYWR2YW5jZWRTZWFyY2hDb25maWdbMF0uaXNFZGl0ID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihTaW1wbGVTZWFyY2hDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHtcclxuICAgICAgICAgICAgICAgIHRvcDogJzAnXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHdpZHRoOiAnNTc1cHgnLFxyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBzZWFyY2hEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoVmFsdWU6IHRoaXMuc2VhcmNoVmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgc2VhcmNoQ29uZmlnOiB0aGlzLnNlYXJjaENvbmZpZyxcclxuICAgICAgICAgICAgICAgICAgICBhZHZhbmNlZFNlYXJjaENvbmZpZzogYWR2YW5jZWRTZWFyY2hDb25maWdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdFswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSByZXN1bHRbMF0uTkFNRSA/IHJlc3VsdFswXS5OQU1FIDogbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gcmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoVmFsdWUgPSByZXN1bHRbMF0ua2V5d29yZCB8fCByZXN1bHRbMF0udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hOYW1lID0gdGhpcy5zZWFyY2hWYWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdChyZXN1bHQpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdCAmJiByZXN1bHQuU0VBUkNIX0RBVEEgJiYgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdCAmJlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uICYmXHJcbiAgICAgICAgICAgICAgICByZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24ubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoTmFtZSA9IHJlc3VsdC5OQU1FO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvblswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyA9IHJlc3VsdDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlID0gcmVzdWx0WzBdLmtleXdvcmQgfHwgcmVzdWx0WzBdLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaC5lbWl0KHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBZHZhbmNlZFNlYXJjaENvbmZpZygpIHtcclxuICAgICAgICBmb3JrSm9pbihbdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0QWxsTVBNRmllbGRDb25maWcoKSwgdGhpcy5zZWFyY2hDb25maWdTZXJ2aWNlLnNldEFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnKCksIHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5zZXRBbGxTZWFyY2hPcGVyYXRvcnMoKV0pLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVswXSAmJiByZXNwb25zZVsxXSAmJiByZXNwb25zZVsyXSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hDb25maWcubWV0YWRhdGFGaWVsZHMgPSByZXNwb25zZVswXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnLmFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbiA9IHJlc3BvbnNlWzFdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2Uuc2V0QWR2YW5jZWRTZWFyY2hDb25maWcodGhpcy5zZWFyY2hDb25maWcuYWR2YW5jZWRTZWFyY2hDb25maWd1cmF0aW9uKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoQ29uZmlnLnNlYXJjaE9wZXJhdG9yTGlzdCA9IHJlc3BvbnNlWzJdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoRGF0YS5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdENvbmZpZ3MoKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hDb25maWcgPSB7XHJcbiAgICAgICAgICAgIGFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbjogW10sXHJcbiAgICAgICAgICAgIG1ldGFkYXRhRmllbGRzOiBbXSxcclxuICAgICAgICAgICAgc2VhcmNoT3BlcmF0b3JMaXN0OiBbXSxcclxuICAgICAgICAgICAgLy8gc2VhcmNoQ29uZmlnSWQ6ICcnLFxyXG4gICAgICAgICAgICBzZWFyY2hJZGVudGlmaWVyOiBudWxsXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmdldEFkdmFuY2VkU2VhcmNoQ29uZmlnKCk7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllciA9IHRoaXMuc2VhcmNoSWRlbnRpZmllcjtcclxuICAgIH1cclxuXHJcbiAgICBmb3JtQWR2U2VhcmNoRGF0YShkYXRhKSB7XHJcbiAgICAgICAgY29uc3Qgc2VhcmNoRGF0YUxpc3QgPSBKU09OLnBhcnNlKGRhdGEpO1xyXG4gICAgICAgIGNvbnN0IGFkdlNlYXJjaENvbmZpZ0ZpZWxkcyA9IHRoaXMuc2VhcmNoQ29uZmlnU2VydmljZS5nZXRBZHZhbmNlZFNlYXJjaENvbmZpZ0J5SWQoc2VhcmNoRGF0YUxpc3RbMF0uc2VhcmNoSWRlbnRpZmllcik7XHJcbiAgICAgICAgY29uc3QgYWR2U2VhcmNoT3BlcmF0b3JzID0gdGhpcy5zZWFyY2hDb25maWdTZXJ2aWNlLmdldEFsbFNlYXJjaE9wZXJhdG9ycygpO1xyXG4gICAgICAgIGNvbnN0IGFkdmFuY2VkU2VhcmNoQ29uZGl0aW9uTGlzdCA9IFtdO1xyXG4gICAgICAgIHNlYXJjaERhdGFMaXN0LmZvckVhY2goKHNlYXJjaERhdGEsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkRmllbGQgPSBhZHZTZWFyY2hDb25maWdGaWVsZHMuUl9QTV9TZWFyY2hfRmllbGRzLmZpbmQoc2VhcmNoQ29uZmlnID0+IHNlYXJjaENvbmZpZy5NQVBQRVJfTkFNRSA9PT0gc2VhcmNoRGF0YS5tYXBwZXJOYW1lKTtcclxuICAgICAgICAgICAgY29uc3QgZmllbGRPcGVyYXRvcnMgPSBhZHZTZWFyY2hPcGVyYXRvcnMuZmlsdGVyKG9wZXJhdG9yID0+IG9wZXJhdG9yLkRBVEFfVFlQRSA9PT0gc2VsZWN0ZWRGaWVsZC5EQVRBX1RZUEUpO1xyXG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZE9wZXJhdG9yID0gc2VhcmNoRGF0YS5yZWxhdGlvbmFsT3BlcmF0b3JOYW1lID8gZmllbGRPcGVyYXRvcnMuZmluZChvcGVyYXRvciA9PiBvcGVyYXRvci5OQU1FID09PSBzZWFyY2hEYXRhLnJlbGF0aW9uYWxPcGVyYXRvck5hbWUpIDogJyc7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlcyA9IHNlYXJjaERhdGEuaGFzVHdvVmFsdWVzID8gc2VhcmNoRGF0YS52YWx1ZS5zcGxpdCgnIGFuZCAnKSA6IHNlYXJjaERhdGEudmFsdWU7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlclJvd0RhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBhdmFpbGFibGVTZWFyY2hGaWVsZHM6IFtdLFxyXG4gICAgICAgICAgICAgICAgaGFzVHdvVmFsdWVzOiBzZWFyY2hEYXRhLmhhc1R3b1ZhbHVlcyxcclxuICAgICAgICAgICAgICAgIGlzRmlsdGVyU2VsZWN0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBpc3NlYXJjaFZhbHVlUmVxdWlyZWQ6IHNlYXJjaERhdGEudmFsdWUgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICByb3dJZDogaW5kZXggKyAxLFxyXG4gICAgICAgICAgICAgICAgc2VhcmNoRmllbGQ6IHNlbGVjdGVkRmllbGQuTVBNX0ZJRUxEX0NPTkZJR19JRCxcclxuICAgICAgICAgICAgICAgIHNlYXJjaE9wZXJhdG9ySWQ6IHNlbGVjdGVkT3BlcmF0b3IgPyBzZWxlY3RlZE9wZXJhdG9yLk9QRVJBVE9SX0lEIDogJycsXHJcbiAgICAgICAgICAgICAgICBzZWFyY2hPcGVyYXRvck5hbWU6IHNlbGVjdGVkT3BlcmF0b3IgPyBzZWxlY3RlZE9wZXJhdG9yLk5BTUUgOiAnJyxcclxuICAgICAgICAgICAgICAgIHNlYXJjaFNlY29uZFZhbHVlOiBzZWFyY2hEYXRhLmhhc1R3b1ZhbHVlcyA/ICh2YWx1ZXNbMV0gPyB2YWx1ZXNbMV0gOiAnJykgOiAnJyxcclxuICAgICAgICAgICAgICAgIHNlYXJjaFZhbHVlOiBzZWFyY2hEYXRhLmhhc1R3b1ZhbHVlcyA/ICh2YWx1ZXNbMF0gPyB2YWx1ZXNbMF0gOiAnJykgOiBzZWFyY2hEYXRhLnZhbHVlXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGNvbnN0IGFkdmFuY2VkU2VhcmNoQ29uZGl0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgZmllbGRfaWQ6IHNlbGVjdGVkRmllbGQuSU5ERVhFUl9GSUVMRF9JRCxcclxuICAgICAgICAgICAgICAgIGZpbHRlclJvd0RhdGE6IGZpbHRlclJvd0RhdGEsXHJcbiAgICAgICAgICAgICAgICBpc0FkdmFuY2VkU2VhcmNoOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgbWFwcGVyX2ZpZWxkX2lkOiBzZWxlY3RlZEZpZWxkLk1QTV9GSUVMRF9DT05GSUdfSUQsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yOiBNUE1TZWFyY2hPcGVyYXRvcnMuQU5ELFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpb25hbF9vcGVyYXRvcl9pZDogc2VsZWN0ZWRPcGVyYXRvciA/IHNlbGVjdGVkT3BlcmF0b3IuT1BFUkFUT1JfSUQgOiBNUE1TZWFyY2hPcGVyYXRvcnMuSVMsXHJcbiAgICAgICAgICAgICAgICByZWxhdGlvbmFsX29wZXJhdG9yX25hbWU6IHNlbGVjdGVkT3BlcmF0b3IgPyBzZWxlY3RlZE9wZXJhdG9yLk5BTUUgOiBNUE1TZWFyY2hPcGVyYXRvck5hbWVzLklTLFxyXG4gICAgICAgICAgICAgICAgc2VhcmNoSWRlbnRpZmllcjogc2VhcmNoRGF0YS5zZWFyY2hJZGVudGlmaWVyLFxyXG4gICAgICAgICAgICAgICAgdHlwZTogc2VsZWN0ZWRGaWVsZC5EQVRBX1RZUEUsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogc2VhcmNoRGF0YS52YWx1ZSxcclxuICAgICAgICAgICAgICAgIG1hcHBlcl9uYW1lOiBzZWxlY3RlZEZpZWxkLk1BUFBFUl9OQU1FLFxyXG4gICAgICAgICAgICAgICAgdmlldzogc2VhcmNoRGF0YS52aWV3XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGFkdmFuY2VkU2VhcmNoQ29uZGl0aW9uTGlzdC5wdXNoKGFkdmFuY2VkU2VhcmNoQ29uZGl0aW9uKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gYWR2YW5jZWRTZWFyY2hDb25kaXRpb25MaXN0O1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudG1wVmlld1RvZ2dsZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQgPSB0aGlzLnNlYXJjaENoYW5nZUV2ZW50U2VydmljZS5vblZpZXdUb2dnbGVTZWFyY2guc3Vic2NyaWJlKChkYXRhOiBTZWFyY2hFdmVudE1vZGFsKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEuYWN0aW9uID09PSBTZWFyY2hFdmVudFR5cGVzLkNMRUFSKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoVmFsdWUoZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMudG1wU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlU3Vic2NyaXB0aW9uJCA9IHRoaXMuc2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlLm9uU2VhcmNoVmlld0NoYW5nZS5zdWJzY3JpYmUoKHZpZXdDb25maWdEZXRhaWxzOiBWaWV3Q29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoSWRlbnRpZmllciA9IHZpZXdDb25maWdEZXRhaWxzID8gdmlld0NvbmZpZ0RldGFpbHMgOiBudWxsO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hJZGVudGlmaWVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaERhdGFTZXJ2aWNlLnNldFNlYXJjaERhdGEoW10pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdENvbmZpZ3MoKTtcclxuICAgICAgICAgICAgdGhpcy5zZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2UudXBkYXRlT25WaWV3Q2hhbmdlKHsgYWN0aW9uOiBTZWFyY2hFdmVudFR5cGVzLkNMRUFSIH0pO1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZShwYXJhbXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKChwYXJhbXMudmlld05hbWUgIT09IHRoaXMuc2VsZWN0ZWRWaWV3IHx8IHBhcmFtcy5ncm91cEJ5RmlsdGVyICE9PSB0aGlzLmdyb3VwQnlGaWx0ZXIgfHxcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyICE9PSB0aGlzLmRlbGl2ZXJhYmxlVGFza0ZpbHRlciB8fCBwYXJhbXMubWVudU5hbWUgIT09IHRoaXMubWVudU5hbWUpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgISgocGFyYW1zLnNlYXJjaE5hbWUgPT09IHRoaXMuc2VhcmNoTmFtZSkgfHwgKHBhcmFtcy5zYXZlZFNlYXJjaE5hbWUgPT09IHRoaXMuc2F2ZWRTZWFyY2hOYW1lKSB8fCAocGFyYW1zLmFkdlNlYXJjaERhdGEgPT09IHRoaXMuYWR2U2VhcmNoRGF0YSkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmFtcy52aWV3TmFtZSAmJiB0aGlzLnNlbGVjdGVkVmlldyAmJiB0aGlzLnNlbGVjdGVkVmlldyAhPT0gcGFyYW1zLnZpZXdOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWaWV3ID0gcGFyYW1zLnZpZXdOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoVmFsdWUodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICgocGFyYW1zLmdyb3VwQnlGaWx0ZXIgJiYgdGhpcy5ncm91cEJ5RmlsdGVyICYmIHRoaXMuZ3JvdXBCeUZpbHRlciAhPT0gcGFyYW1zLmdyb3VwQnlGaWx0ZXIpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIgJiYgdGhpcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIgJiYgdGhpcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIgIT09IHBhcmFtcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ncm91cEJ5RmlsdGVyID0gcGFyYW1zLmdyb3VwQnlGaWx0ZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyID0gcGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGVhclNlYXJjaFZhbHVlKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWaWV3ID0gcGFyYW1zLnZpZXdOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdyb3VwQnlGaWx0ZXIgPSBwYXJhbXMuZ3JvdXBCeUZpbHRlcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVRhc2tGaWx0ZXIgPSBwYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1zLnNlYXJjaE5hbWUgJiYgdGhpcy5zZWFyY2hJZGVudGlmaWVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hOYW1lICE9PSBwYXJhbXMuc2VhcmNoTmFtZSB8fCB0aGlzLm1lbnVOYW1lICE9PSBwYXJhbXMubWVudU5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVOYW1lID0gcGFyYW1zLm1lbnVOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoTmFtZSA9IHBhcmFtcy5zZWFyY2hOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoRGF0YSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5d29yZDogdGhpcy5zZWFyY2hOYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9maWVsZF9pZDogJ01QTS5GSUVMRC5BTExfS0VZV09SRCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hEYXRhICYmIHRoaXMuc2VhcmNoRGF0YVswXSAmJiB0aGlzLnNlYXJjaERhdGFbMF0ua2V5d29yZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UmVjZW50U2VhcmNoZXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpc1NlYXJjaGVkID0gc2VhcmNoZXMuZmluZChzZWFyY2hEYXRhID0+IHNlYXJjaERhdGEgPT09IHRoaXMuc2VhcmNoTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWlzU2VhcmNoZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFJlY2VudFNlYXJjaCh0aGlzLnNlYXJjaE5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2godGhpcy5zZWFyY2hOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hEYXRhICYmIHRoaXMuc2VhcmNoRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hEYXRhWzBdLmlzQWR2YW5jZWRTZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gdGhpcy5zZWFyY2hEYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoVmFsdWUgPSB0aGlzLnNlYXJjaERhdGFbMF0ua2V5d29yZCB8fCB0aGlzLnNlYXJjaERhdGFbMF0udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoLmVtaXQodGhpcy5zZWFyY2hEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICghcGFyYW1zLnNlYXJjaE5hbWUgJiYgdGhpcy5zZWFyY2hOYW1lICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaE5hbWUgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lICYmIHRoaXMuc2VhcmNoSWRlbnRpZmllcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2F2ZWRTZWFyY2hOYW1lICE9PSBwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lIHx8IHRoaXMubWVudU5hbWUgIT09IHBhcmFtcy5tZW51TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWVudU5hbWUgPSBwYXJhbXMubWVudU5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlZFNlYXJjaE5hbWUgPSBwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNTYXZlZFNlYXJjaFRyaWdnZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzU2F2ZWRTZWFyY2hEYXRhLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXBhcmFtcy5zYXZlZFNlYXJjaE5hbWUgJiYgdGhpcy5zYXZlZFNlYXJjaE5hbWUgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZWRTZWFyY2hOYW1lID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1zLmFkdlNlYXJjaERhdGEgJiYgdGhpcy5zZWFyY2hJZGVudGlmaWVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdlNlYXJjaERhdGEgPSBwYXJhbXMuYWR2U2VhcmNoRGF0YTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFkdmFuY2VkU2VhcmNoRGF0YSA9IHRoaXMuc3RhdGVTZXJ2aWNlLmdldEFkdmFuY2VkU2VhcmNoKHBhcmFtcy5tZW51TmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYWR2YW5jZWRTZWFyY2hEYXRhICYmIGFkdmFuY2VkU2VhcmNoRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkdmFuY2VkU2VhcmNoRGF0YSAhPT0gdGhpcy5hZHZhbmNlZFNlYXJjaENvbmZpZyB8fCB0aGlzLm1lbnVOYW1lICE9PSBwYXJhbXMubWVudU5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51TmFtZSA9IHBhcmFtcy5tZW51TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkdmFuY2VkU2VhcmNoRGF0YVswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gYWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2guZW1pdChhZHZhbmNlZFNlYXJjaERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoVHJpZ2dlciA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaERhdGEuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmICF0aGlzLmlzQWR2YW5jZWRTZWFyY2hUcmlnZ2VyICYmIHRoaXMuc2VhcmNoQ29uZmlnLmFkdmFuY2VkU2VhcmNoQ29uZmlndXJhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoVHJpZ2dlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hDb25kaXRpb24gPSB0aGlzLmZvcm1BZHZTZWFyY2hEYXRhKHRoaXMuYWR2U2VhcmNoRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFJlY2VudFNlYXJjaChzZWFyY2hDb25kaXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaENvbmRpdGlvbiAmJiBzZWFyY2hDb25kaXRpb24ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWFyY2hDb25kaXRpb25bMF0uaXNBZHZhbmNlZFNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdmFuY2VkU2VhcmNoQ29uZmlnID0gc2VhcmNoQ29uZGl0aW9uO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzQWR2YW5jZWRTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyU2VhcmNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaC5lbWl0KHNlYXJjaENvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICghcGFyYW1zLmFkdlNlYXJjaERhdGEgJiYgdGhpcy5hZHZTZWFyY2hEYXRhICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkdlNlYXJjaERhdGEgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghcGFyYW1zLnNlYXJjaE5hbWUgJiYgIXBhcmFtcy5hZHZTZWFyY2hEYXRhICYmICFwYXJhbXMuc2F2ZWRTZWFyY2hOYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVOYW1lID0gcGFyYW1zLm1lbnVOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmlzU2F2ZWRTZWFyY2hEYXRhLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zYXZlZFNlYXJjaE5hbWUgJiYgIXRoaXMuaXNTYXZlZFNlYXJjaFRyaWdnZXIgJiYgdGhpcy5zZWFyY2hDb25maWcgJiYgdGhpcy5zZWFyY2hDb25maWcuc2VhcmNoSWRlbnRpZmllciAmJiB0aGlzLnNlYXJjaENvbmZpZy5zZWFyY2hJZGVudGlmaWVyLlZJRVcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaENvbmZpZ1NlcnZpY2UuZ2V0U2F2ZWRTZWFyY2hlc0ZvclZpZXdBbmRDdXJyZW50VXNlcih0aGlzLnNlYXJjaENvbmZpZy5zZWFyY2hJZGVudGlmaWVyLlZJRVcpLnN1YnNjcmliZShyZXNwb25zZURhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VEYXRhICYmIHJlc3BvbnNlRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoZXMgPSByZXNwb25zZURhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVkU2VhcmNoZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zYXZlZFNlYXJjaGVzICYmICF0aGlzLmlzU2F2ZWRTZWFyY2hUcmlnZ2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLnNhdmVkU2VhcmNoZXMuZmluZChkYXRhID0+IGRhdGEuTkFNRSA9PT0gdGhpcy5zYXZlZFNlYXJjaE5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAmJiByZXN1bHQuU0VBUkNIX0RBVEEgJiYgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbiAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvbi5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb25bMF0uc2F2ZWRTZWFyY2hJZCA9IHJlc3VsdFsnTVBNX1NhdmVkX1NlYXJjaGVzLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uWzBdLk5BTUUgPSByZXN1bHQuTkFNRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UmVjZW50U2VhcmNoZXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VhcmNoZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaXNTZWFyY2hlZCA9IHNlYXJjaGVzLmZpbmQoc2VhcmNoRGF0YSA9PiBzZWFyY2hEYXRhWzBdLnNhdmVkU2VhcmNoSWQgPT09IHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvblswXS5zYXZlZFNlYXJjaElkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1NlYXJjaGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFJlY2VudFNlYXJjaChyZXN1bHQuU0VBUkNIX0RBVEEuc2VhcmNoX2NvbmRpdGlvbl9saXN0LnNlYXJjaF9jb25kaXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZWNlbnRTZWFyY2gocmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1NhdmVkU2VhcmNoVHJpZ2dlciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC5TRUFSQ0hfREFUQS5zZWFyY2hfY29uZGl0aW9uX2xpc3Quc2VhcmNoX2NvbmRpdGlvblswXS5pc0FkdmFuY2VkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWR2YW5jZWRTZWFyY2hDb25maWcgPSByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNBZHZhbmNlZFNlYXJjaCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0FkdmFuY2VkU2VhcmNoID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoVmFsdWUgPSByZXN1bHRbMF0ua2V5d29yZCB8fCByZXN1bHRbMF0udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJTZWFyY2ggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoLmVtaXQocmVzdWx0LlNFQVJDSF9EQVRBLnNlYXJjaF9jb25kaXRpb25fbGlzdC5zZWFyY2hfY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudG1wVmlld1RvZ2dsZUV2ZW50U2VydmljZVN1YnNjcmlwdGlvbiQpIHtcclxuICAgICAgICAgICAgdGhpcy50bXBWaWV3VG9nZ2xlRXZlbnRTZXJ2aWNlU3Vic2NyaXB0aW9uJC51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy50bXBTZWFyY2hDaGFuZ2VFdmVudFNlcnZpY2VTdWJzY3JpcHRpb24kKSB7XHJcbiAgICAgICAgICAgIHRoaXMudG1wU2VhcmNoQ2hhbmdlRXZlbnRTZXJ2aWNlU3Vic2NyaXB0aW9uJC51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=