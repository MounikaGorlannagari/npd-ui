import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliverableTaskListComponent } from './deliverable-task-list/deliverable-task-list.component';
import { MaterialModule } from '../material.module';
import { DeliverableModule } from '../project/tasks/deliverable/deliverable.module';
import { MpmLibraryModule } from '../mpm-library.module';
var DeliverableTaskModule = /** @class */ (function () {
    function DeliverableTaskModule() {
    }
    DeliverableTaskModule = __decorate([
        NgModule({
            declarations: [
                DeliverableTaskListComponent
            ],
            imports: [
                CommonModule,
                MaterialModule,
                DeliverableModule,
                MpmLibraryModule
            ],
            exports: [
                DeliverableTaskListComponent
            ]
        })
    ], DeliverableTaskModule);
    return DeliverableTaskModule;
}());
export { DeliverableTaskModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdGFzay5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9kZWxpdmVyYWJsZS10YXNrL2RlbGl2ZXJhYmxlLXRhc2subW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUN2RyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDcEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFpQnpEO0lBQUE7SUFBcUMsQ0FBQztJQUF6QixxQkFBcUI7UUFkakMsUUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLDRCQUE0QjthQUM3QjtZQUNELE9BQU8sRUFBRTtnQkFDUCxZQUFZO2dCQUNaLGNBQWM7Z0JBQ2QsaUJBQWlCO2dCQUNqQixnQkFBZ0I7YUFDakI7WUFDRCxPQUFPLEVBQUU7Z0JBQ1AsNEJBQTRCO2FBQzdCO1NBQ0YsQ0FBQztPQUNXLHFCQUFxQixDQUFJO0lBQUQsNEJBQUM7Q0FBQSxBQUF0QyxJQUFzQztTQUF6QixxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBEZWxpdmVyYWJsZVRhc2tMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9kZWxpdmVyYWJsZS10YXNrLWxpc3QvZGVsaXZlcmFibGUtdGFzay1saXN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgRGVsaXZlcmFibGVNb2R1bGUgfSBmcm9tICcuLi9wcm9qZWN0L3Rhc2tzL2RlbGl2ZXJhYmxlL2RlbGl2ZXJhYmxlLm1vZHVsZSc7XHJcbmltcG9ydCB7IE1wbUxpYnJhcnlNb2R1bGUgfSBmcm9tICcuLi9tcG0tbGlicmFyeS5tb2R1bGUnO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBEZWxpdmVyYWJsZVRhc2tMaXN0Q29tcG9uZW50XHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIERlbGl2ZXJhYmxlTW9kdWxlLFxyXG4gICAgTXBtTGlicmFyeU1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgRGVsaXZlcmFibGVUYXNrTGlzdENvbXBvbmVudFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIERlbGl2ZXJhYmxlVGFza01vZHVsZSB7IH1cclxuIl19