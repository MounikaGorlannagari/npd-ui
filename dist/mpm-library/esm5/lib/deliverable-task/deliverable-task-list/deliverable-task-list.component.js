import { __decorate } from "tslib";
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer2, OnChanges, SimpleChanges } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { OTMMDataTypes } from '../../mpm-utils/objects/OTMMDataTypes';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MPMFieldConstants } from '../../shared/constants/mpm.field.constants';
import { UtilService } from '../../mpm-utils/services/util.service';
import { ProjectService } from '../../project/shared/services/project.service';
import { FormatToLocalePipe } from '../../shared/pipe/format-to-locale.pipe';
import { MatSort } from '@angular/material/sort';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import { DeliverableBulkCountService } from '../service/deliverable-bulk-count.service';
import { MatDialog } from '@angular/material/dialog';
import { AssetService } from '../../project/shared/services/asset.service';
import { Observable } from 'rxjs';
import { AssetConstants } from '../../project/assets/asset_constants';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { moveItemInArray } from '@angular/cdk/drag-drop';
//import { DeliverableCreationModalComponent } from 'src/app/core/mpm/project-view/task/task-details/deliverable-creation-modal/deliverable-creation-modal.component';
var DeliverableTaskListComponent = /** @class */ (function () {
    function DeliverableTaskListComponent(utilService, projectService, formatToLocalePipe, element, fieldConfigService, sharingService, deliverableBulkCountService, dialog, assetService, otmmService, router, renderer) {
        this.utilService = utilService;
        this.projectService = projectService;
        this.formatToLocalePipe = formatToLocalePipe;
        this.element = element;
        this.fieldConfigService = fieldConfigService;
        this.sharingService = sharingService;
        this.deliverableBulkCountService = deliverableBulkCountService;
        this.dialog = dialog;
        this.assetService = assetService;
        this.otmmService = otmmService;
        this.router = router;
        this.renderer = renderer;
        this.sortChange = new EventEmitter();
        this.refresh = new EventEmitter();
        this.rowExpanded = new EventEmitter();
        this.rowCollapsed = new EventEmitter();
        this.deliverableDetailsHandler = new EventEmitter();
        this.versionsHandler = new EventEmitter();
        this.shareAssetHandler = new EventEmitter();
        this.crHandler = new EventEmitter();
        this.bulkCheck = new EventEmitter();
        this.deliverableBulkCheckCount = new EventEmitter();
        this.selectedDeliverableCounts = new EventEmitter();
        this.selectedDeliverableData = new EventEmitter();
        this.selectedDeliverableDatas = new EventEmitter();
        this.assetDetailsCallBackHandler = new EventEmitter();
        this.editDeliverableHandler = new EventEmitter();
        this.orderedDisplayableFields = new EventEmitter();
        this.resizeDisplayableFields = new EventEmitter();
        this.colWidth = 270;
        this.metaDataDataTypes = OTMMDataTypes;
        this.MPMFieldConstants = MPMFieldConstants;
        this.selection = new SelectionModel(true, []);
        this.deliverableTaskDataSource = new MatTableDataSource([]);
        this.deliverableId = [];
        this.selected = false;
        this.folderId = [];
        this.project = [];
        this.assetData = [];
        this.assetDetailDatas = [];
    }
    DeliverableTaskListComponent.prototype.ngOnChanges = function (changes) {
        this.reload();
    };
    DeliverableTaskListComponent.prototype.openCR = function (event) {
        this.crHandler.emit(event);
    };
    DeliverableTaskListComponent.prototype.editDeliverable = function (deliverableConfig) {
        this.editDeliverableHandler.next(deliverableConfig);
        /*  const dialogRef = this.dialog.open(DeliverableCreationModalComponent, {
             width: '38%',
             disableClose: true,
             data: {
                 projectId: deliverableConfig.projectId,
                 isTemplate: deliverableConfig.isTemplate,
                 taskId: deliverableConfig.taskId,
                 deliverableId: deliverableConfig.deliverableId,
                 selectedProject: deliverableConfig.selectedProject,
                 selectedTask: deliverableConfig.selectedTask,
                 modalLabel: 'Edit Deliverable'
             }
         });
         dialogRef.afterClosed().subscribe(result => {
             if (result) {
                 console.log(result)
                 //this.taskService.setDeliverableRefreshData(200);
             }
         }); */
    };
    DeliverableTaskListComponent.prototype.deliverableBulkEditCheckCount = function (deliverableData) {
        var id;
        var deliverableid;
        if (this.bulkActions === false && !this.selected) {
            id = [];
            deliverableid = true;
            this.selected = true;
        }
        id = this.deliverableBulkCountService.getBulkDeliverableId(deliverableData, deliverableid);
        if (id.length > 1) {
            this.deliverableBulkCheckCount.next(true);
        }
        else {
            this.deliverableBulkCheckCount.next(false);
        }
        this.selectedDeliverableCounts.next(id.length);
        this.selectedDeliverableDatas.next(id);
    };
    DeliverableTaskListComponent.prototype.selectedBulkDeliverableData = function (deliverableData) {
        this.selectedDeliverableData.next(deliverableData);
    };
    DeliverableTaskListComponent.prototype.refreshDeliverable = function (event) {
        if (event) {
            this.refresh.next(event);
        }
    };
    DeliverableTaskListComponent.prototype.deliverableDetails = function (event) {
        this.deliverableDetailsHandler.emit(event);
    };
    DeliverableTaskListComponent.prototype.viewVersions = function (event) {
        this.versionsHandler.emit(event);
    };
    DeliverableTaskListComponent.prototype.shareAsset = function (event) {
        this.shareAssetHandler.emit(event);
    };
    DeliverableTaskListComponent.prototype.computeGridCols = function () {
        this.cols = Math.floor(this.element.nativeElement.offsetParent.offsetWidth / this.colWidth);
    };
    DeliverableTaskListComponent.prototype.expandRow = function (row) {
        if (row.isExpanded) {
            row.isExpanded = false;
            this.rowCollapsed.next();
        }
        else {
            row.isExpanded = true;
            this.rowExpanded.next(row);
        }
    };
    DeliverableTaskListComponent.prototype.getPriority = function (projectData, columnData) {
        var priorityObj = this.utilService.getPriorityIcon(this.getProperty(projectData, columnData), this.level);
        return {
            color: priorityObj.color,
            icon: priorityObj.icon,
            tooltip: priorityObj.tooltip
        };
    };
    DeliverableTaskListComponent.prototype.getProperty = function (projectData, columnData) {
        return this.fieldConfigService.getFieldValueByDisplayColumn(columnData, projectData);
    };
    DeliverableTaskListComponent.prototype.converToLocalDate = function (data, propertyId) {
        return this.formatToLocalePipe.transform(this.projectService.converToLocalDate(data, propertyId));
    };
    DeliverableTaskListComponent.prototype.openComment = function (row) {
        alert('To Be done');
    };
    /** Whether the number of selected elements matches the total number of rows. */
    DeliverableTaskListComponent.prototype.isAllSelected = function (data) {
        var numSelected = this.selection.selected.length;
        var numRows = this.deliverableTaskDataSource.data.length;
        //if (this.selection.hasValue() && numSelected === numRows && (data || data == undefined)) {
        if (data) {
            this.bulkCheck.next(true);
        }
        else {
            this.bulkCheck.next(false);
        }
        return numSelected === numRows;
    };
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    DeliverableTaskListComponent.prototype.masterToggle = function (event) {
        var _this = this;
        this.isAllSelected(event.checked) ?
            this.selection.clear() :
            this.deliverableTaskDataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    DeliverableTaskListComponent.prototype.refreshEvent = function () {
        this.refresh.emit();
    };
    DeliverableTaskListComponent.prototype.tableDrop = function (event) {
        var _this = this;
        var count = 0;
        if (this.displayColumns.find(function (column) { return column === 'Select'; })) {
            count = count + 1;
        }
        if (this.displayColumns.find(function (column) { return column === 'Expand'; })) {
            count = count + 1;
        }
        moveItemInArray(this.displayColumns, event.previousIndex + count, event.currentIndex + count);
        var orderedFields = [];
        this.displayColumns.forEach(function (column) {
            if (!(column === 'Select' || column === 'Expand' || column === 'Actions')) {
                orderedFields.push(_this.displayableMetadataFields.find(function (displayableField) { return displayableField.INDEXER_FIELD_ID === column; }));
            }
        });
        this.orderedDisplayableFields.next(orderedFields);
    };
    DeliverableTaskListComponent.prototype.onResizeMouseDown = function (event, column) {
        event.stopPropagation();
        event.preventDefault();
        var start = event.target;
        var pressed = true;
        var startX = event.x;
        var startWidth = $(start).parent().width();
        this.initResizableColumns(start, pressed, startX, startWidth, column);
    };
    DeliverableTaskListComponent.prototype.initResizableColumns = function (start, pressed, startX, startWidth, column) {
        var _this = this;
        this.renderer.listen('body', 'mousemove', function (event) {
            if (pressed) {
                var width = startWidth + (event.x - startX);
                column.width = width;
            }
        });
        this.renderer.listen('body', 'mouseup', function (event) {
            if (pressed) {
                pressed = false;
                _this.resizeDisplayableFields.next();
            }
        });
    };
    DeliverableTaskListComponent.prototype.reload = function () {
        var _this = this;
        this.computeGridCols();
        if (!this.displayColumns) {
            this.displayColumns = [];
        }
        if (!this.displayableMetadataFields) {
            this.displayableMetadataFields = [];
        }
        if (!this.dataSource) {
            this.dataSource = [];
        }
        this.deliverableTaskDataSource.data = this.dataSource;
        if (!this.level) {
            console.warn('No Level info was provided to list view to render');
            return;
        }
        if (this.sort) {
            this.sort.disableClear = true;
            this.sortControl.active = this.sort.active;
            this.sortControl.direction = this.sort.direction;
            this.sortControl.disableClear = true;
            this.deliverableTaskDataSource.sort = this.sortControl;
        }
        this.sortControl.sortChange.subscribe(function () {
            _this.sortChange.emit(_this.sortControl);
        });
    };
    DeliverableTaskListComponent.prototype.retrieveAssetData = function (event) {
        this.assetDetailsCallBackHandler.next(event);
    };
    DeliverableTaskListComponent.prototype.getAssets = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.checkOtmmSession()
                .subscribe(function (sessionResponse) {
                var userSessionId;
                if (sessionResponse && sessionResponse.session_resource && sessionResponse.session_resource.session && sessionResponse.session_resource.session.id) {
                    userSessionId = sessionResponse.session_resource.session.id;
                } /* this.skip */
                _this.folderId.forEach(function (folderId) {
                    _this.assetService.getAssets(AssetConstants.ASSET_SEARCH_CONDITION, folderId, 0, 50, userSessionId).subscribe(function (res) {
                        if (res && res.search_result_resource && res.search_result_resource.asset_list) {
                            //  this.loadingHandler(false);
                            _this.totalAssetsCount = res.search_result_resource.search_result.total_hit_count;
                            _this.assets = res.search_result_resource.asset_list;
                            _this.assetData = [];
                            _this.assets.forEach(function (asset) {
                                asset.metadata.metadata_element_list.forEach(function (metadataElement) {
                                    if (metadataElement.id === 'MPM.ASSET.GROUP') {
                                        // [0].metadata_element_list
                                        metadataElement.metadata_element_list.forEach(function (metadataElementList) {
                                            if (metadataElementList.id === 'MPM.ASSET.IS_REFERENCE_ASSET') {
                                                if (metadataElementList.value.value.value === 'true') {
                                                    _this.assetData.push(asset);
                                                }
                                            }
                                        });
                                    }
                                });
                            });
                        }
                        observer.next(_this.assetData);
                        observer.complete();
                    });
                });
            });
        });
    };
    DeliverableTaskListComponent.prototype.ngOnInit = function () {
        console.log(this.displayableMetadataFields);
        this.filterType = (this.selectedFilter === 'MY_TEAM_TASKS') ? true : false;
        //deliverableWorkView
        this.IsNotPMView = this.router.url.includes('deliverableWorkView') ? true : false;
        this.reload();
        this.appConfig = this.sharingService.getAppConfig();
        this.defaultMyTaskViewCollapsed = this.utilService.getBooleanValue(this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.DEFAULT_MYTASK_VIEW_COLLAPSED]);
        if (!this.defaultMyTaskViewCollapsed) {
            this.deliverableTaskDataSource.data[0].isExpanded = true;
            this.rowExpanded.next(this.deliverableTaskDataSource.data[0]);
        }
    };
    DeliverableTaskListComponent.ctorParameters = function () { return [
        { type: UtilService },
        { type: ProjectService },
        { type: FormatToLocalePipe },
        { type: ElementRef },
        { type: FieldConfigService },
        { type: SharingService },
        { type: DeliverableBulkCountService },
        { type: MatDialog },
        { type: AssetService },
        { type: OTMMService },
        { type: Router },
        { type: Renderer2 }
    ]; };
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "level", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "sort", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "displayColumns", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "dataSource", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "displayableMetadataFields", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "bulkActions", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "unselectDeliverable", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "assetDetailData", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "selectedFilter", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "selectedListDependentFilter", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "bulkResponseData", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "sortChange", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "refresh", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "rowExpanded", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "rowCollapsed", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "deliverableDetailsHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "versionsHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "shareAssetHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "crHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "bulkCheck", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "deliverableBulkCheckCount", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "selectedDeliverableCounts", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "selectedDeliverableData", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "selectedDeliverableDatas", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "assetDetailsCallBackHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "editDeliverableHandler", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "orderedDisplayableFields", void 0);
    __decorate([
        Output()
    ], DeliverableTaskListComponent.prototype, "resizeDisplayableFields", void 0);
    __decorate([
        Input()
    ], DeliverableTaskListComponent.prototype, "userPreferenceFreezeCount", void 0);
    __decorate([
        ViewChild(MatSort, { static: true })
    ], DeliverableTaskListComponent.prototype, "sortControl", void 0);
    DeliverableTaskListComponent = __decorate([
        Component({
            selector: 'mpm-deliverable-task-list',
            template: "<table class=\"deliverable-task-list\" mat-table [dataSource]=\"deliverableTaskDataSource\" matSort multiTemplateDataRows>\r\n    <ng-container matColumnDef=\"Expand\">\r\n        <th mat-header-cell *matHeaderCellDef> </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <mat-icon *ngIf=\"!row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_right</mat-icon>\r\n            <mat-icon *ngIf=\"row.isExpanded\" matSuffix color=\"primary\">keyboard_arrow_down</mat-icon>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"Select\">\r\n        <th mat-header-cell *matHeaderCellDef>\r\n            <!--  <mat-checkbox color=\"red\" (change)=\"$event ? masterToggle($event) : null\"\r\n                [checked]=\"selection.hasValue() && isAllSelected(true)\"\r\n                [indeterminate]=\"selection.hasValue() && !isAllSelected(true)\">\r\n            </mat-checkbox> -->\r\n        </th>\r\n        <td mat-cell *matCellDef=\"let row\">\r\n            <!--  <mat-checkbox color=\"primary\" (click)=\"$event.stopPropagation()\"\r\n                (change)=\"$event ? selection.toggle(row) : null\" [checked]=\"selection.isSelected(row)\">\r\n            </mat-checkbox> -->\r\n        </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"{{column.INDEXER_FIELD_ID}}\" *ngFor=\"let column of displayableMetadataFields; index as i\" [sticky]=\"i<userPreferenceFreezeCount\">\r\n        <th mat-header-cell *matHeaderCellDef mat-sort-header [disabled]=\"column.SORTABLE === 'true' ? false : true\">\r\n            {{column.DISPLAY_NAME}} </th>\r\n        <td mat-cell *matCellDef=\"let row\" [ngClass]=\"{'wrap-text-custom':metaDataDataTypes.STRING === column.DATA_TYPE}\">\r\n            <span>\r\n                <div *ngIf=\"(column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) && \r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_PRIORITY) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROGRESS) &&\r\n                                (column.MAPPER_NAME !== MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS)\">\r\n                    <a matTooltip=\"{{getProperty(row, column)}}\">{{getProperty(row, column) || 'NA'}}</a>\r\n                </div>\r\n                <div *ngIf=\"column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PRIORITY || \r\n                column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_PRIORITY\">\r\n                    <mat-icon matTooltip=\"{{getPriority(row, column).tooltip}}\"\r\n                        [style.color]=\"getPriority(row, column).color\">{{getPriority(row, column).icon}}\r\n                    </mat-icon>\r\n                </div>\r\n                <div\r\n                    *ngIf=\"(column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_PROGRESS) || (column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_PROGRESS)\">\r\n                    <span matTooltip=\"{{getProperty(row, column) || '0'}}%\">{{getProperty(row, column) || 'NA'}}%\r\n                        <mat-progress-bar color=\"primary\" mode=\"determinate\" value=\"{{getProperty(row, column) || '0'}}\"\r\n                            class=\"\">\r\n                        </mat-progress-bar>\r\n                    </span>\r\n            </div>\r\n            <div *ngIf=\"(column.MAPPER_NAME === MPMFieldConstants.MPM_PROJECT_FEILDS.PROJECT_STATUS) || (column.MAPPER_NAME === MPMFieldConstants.MPM_TASK_FIELDS.TASK_STATUS)\">\r\n                <mat-chip-list>\r\n                    <mat-chip class=\"status-chip\">{{getProperty(row, column) || 'NA'}}</mat-chip>\r\n                </mat-chip-list>\r\n            </div>\r\n            </span>\r\n        </td>\r\n    </ng-container>\r\n\r\n    <!-- <ng-container matColumnDef=\"Actions\">\r\n        <th mat-header-cell *matHeaderCellDef>Actions </th>\r\n        <td mat-cell *matCellDef=\"let row\" class=\"min-width-action\">\r\n            <div>\r\n                <button mat-icon-button matTooltip=\"Comments\" (click)=\"openComment(row)\">\r\n                    <mat-icon>comment</mat-icon>\r\n                </button>\r\n            </div>\r\n        </td>\r\n    </ng-container> -->\r\n\r\n    <ng-container matColumnDef=\"expandedDetail\">\r\n        <td class=\"task-details\" mat-cell *matCellDef=\"let row\" [attr.colspan]=\"displayColumns?.length\">\r\n            <div class=\"task-element-detail\" [@detailExpand]=\"row.isExpanded ? 'expanded' : 'collapsed'\">\r\n                <div class=\"deliverables-info\" *ngIf=\"(!row.deliverableDetails || row.deliverableDetails.length === 0)\">\r\n                    <div class=\"task-heading\">No Deliverables are there to work</div>\r\n                </div>\r\n                <div class=\"deliverables-card-area\">\r\n                    <mat-grid-list cols=\"{{cols}}\" rowHeight=\"370\" (window:resize)=\"computeGridCols()\" gutterSize=\"10\">\r\n                        <mat-grid-tile *ngFor=\"let deliverableDetail of row.deliverableDetails\">\r\n                            <mpm-deliverable-card [isPMView]=\"false\" [bulkEdit]=\"row\" [deliverableBulkResponse]=\"bulkResponseData\" (deliverableBulkCheckCount)=\"deliverableBulkEditCheckCount($event)\" (selectedDeliverableData)=\"selectedBulkDeliverableData($event)\" [deliverableData]=\"deliverableDetail.deliverableData\"\r\n                                [projectRowData]=\"row\" (refreshDeliverable)=\"refreshDeliverable($event)\" (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\" (editDeliverableHandler)=\"editDeliverable($event)\" (crHandler)=\"openCR($event)\" (deliverableDetailsHandler)=\"deliverableDetails($event)\"\r\n                                (versionsHandler)=\"viewVersions($event)\" (shareAssetHandler)=\"shareAsset($event)\" [level]=\"level\" [bulkActions]=\"bulkActions\" [unselectDeliverable]=\"unselectDeliverable\">\r\n                            </mpm-deliverable-card>\r\n                        </mat-grid-tile>\r\n                        <!-- for reference asset card -->\r\n                        <!--  <mat-grid-tile *ngFor=\"let assetData of assetDetailDatas\">\r\n                            <div *ngFor=\"let asset of assetData\">\r\n                            <mpm-asset-card  [asset]=\"asset\">\r\n\r\n                            </mpm-asset-card>\r\n                            </div>\r\n                        </mat-grid-tile> -->\r\n                        <!-- [isVersion]=\"false\"\r\n                            [selectAllDeliverables]=\"selectAllDeliverables\"(assetVersionsCallBackHandler)=\"retrieveAssetVersionsData($event)\"\r\n                (assetDetailsCallBackHandler)=\"retrieveAssetData($event)\"\r\n                (selectedAssetsCallBackHandler)=\"getSelectedDeliverable(asset)\"\r\n                (unSelectedAssetsCallBackHandler)=\"getUnSelectedDeliverable(asset)\"\r\n                (shareAssetsCallbackHandler)=\"shareAsset($event)\"\r\n                (assetPreviewCallBackHandler)=\"retrieveAssetPreviewData($event)\"\r\n                (downloadCallBackHandler)=\"downloadAsset($event)\" -->\r\n                        <!--  </mat-grid-tile> -->\r\n                    </mat-grid-list>\r\n                </div>\r\n            </div>\r\n        </td>\r\n    </ng-container>\r\n    <tr mat-header-row *matHeaderRowDef=\"displayColumns; sticky: true\"></tr>\r\n    <tr id=\"{{row}}\" mat-row *matRowDef=\"let row; columns: displayColumns\" class=\"task-element-row\" (click)=\"expandRow(row)\">\r\n    </tr>\r\n\r\n    <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\" class=\"task-details-row\"></tr>\r\n</table>\r\n\r\n<div *ngIf=\"(deliverableTaskDataSource && deliverableTaskDataSource.data && deliverableTaskDataSource.data.length == 0)\" class=\"central-info\">\r\n    <button color=\"accent\" disabled mat-flat-button>No tasks available.</button>\r\n</div>",
            animations: [trigger('detailExpand', [
                    state('collapsed', style({ height: '0' })),
                    state('expanded', style({ height: '*' })),
                    transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.42, 0.0, 0.58, 1.0)')),
                ])],
            styles: ["table.deliverable-task-list{width:100%}table.deliverable-task-list tr{cursor:pointer}table.deliverable-task-list tr .mat-checkbox{padding:0 8px}table.deliverable-task-list tr th:last-child{padding-left:12px}table.deliverable-task-list tr td{min-width:8rem}table.deliverable-task-list tr td a.mat-button{padding:0;text-align:left}table.deliverable-task-list tr td mat-icon{font-size:16px;height:16px;width:16px}table.deliverable-task-list tr td .status-chip{padding:4px 8px;border-radius:4px;font-weight:400}.deliverables-info{width:100%;padding:8px;font-weight:lighter;margin:8px 0;display:flex;align-items:center;justify-content:center;text-align:center}.central-info{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center;margin-bottom:20px;margin-top:20px}td.task-details{padding:0!important;cursor:default}tr.task-details-row{height:0}.task-element-row td{border-bottom-width:0}.task-element-detail{overflow:hidden;display:flex;flex-wrap:wrap}.deliverables-card-area{width:100%;padding:8px;font-weight:lighter;margin:0 25px 25px}td.wrap-text-custom span>div{max-width:14em;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.min-width-action{min-width:3em}.mat-checkbox{color:red}"]
        })
    ], DeliverableTaskListComponent);
    return DeliverableTaskListComponent;
}());
export { DeliverableTaskListComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtdGFzay1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL2RlbGl2ZXJhYmxlLXRhc2svZGVsaXZlcmFibGUtdGFzay1saXN0L2RlbGl2ZXJhYmxlLXRhc2stbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0ksT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTdELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDN0UsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBRWhGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUMxRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQztBQUNqRyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUN4RixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFlLGVBQWUsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3RFLHNLQUFzSztBQWF0SztJQWdDSSxzQ0FDVyxXQUF3QixFQUN4QixjQUE4QixFQUM5QixrQkFBc0MsRUFDdEMsT0FBbUIsRUFDbkIsa0JBQXNDLEVBQ3RDLGNBQThCLEVBQzlCLDJCQUF3RCxFQUN4RCxNQUFpQixFQUNqQixZQUEwQixFQUMxQixXQUF3QixFQUN4QixNQUFjLEVBQ2QsUUFBbUI7UUFYbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsWUFBTyxHQUFQLE9BQU8sQ0FBWTtRQUNuQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQ0FBMkIsR0FBM0IsMkJBQTJCLENBQTZCO1FBQ3hELFdBQU0sR0FBTixNQUFNLENBQVc7UUFDakIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUE5QnBCLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3JDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDO1FBQ25DLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUN0QyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdkMsOEJBQXlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwRCxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDMUMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUM1QyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNwQyw4QkFBeUIsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3BELDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDcEQsNEJBQXVCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNsRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDdEQsMkJBQXNCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUNqRCw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ25ELDRCQUF1QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUF1QjVELGFBQVEsR0FBRyxHQUFHLENBQUM7UUFHZixzQkFBaUIsR0FBRyxhQUFhLENBQUM7UUFDbEMsc0JBQWlCLEdBQUcsaUJBQWlCLENBQUM7UUFFdEMsY0FBUyxHQUFHLElBQUksY0FBYyxDQUFNLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUM5Qyw4QkFBeUIsR0FBRyxJQUFJLGtCQUFrQixDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBSzVELGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFHakIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixjQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ2YscUJBQWdCLEdBQUcsRUFBRSxDQUFDO0lBM0JsQixDQUFDO0lBQ0wsa0RBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQzlCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtJQUVqQixDQUFDO0lBOEJELDZDQUFNLEdBQU4sVUFBTyxLQUFLO1FBQ1IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELHNEQUFlLEdBQWYsVUFBZ0IsaUJBQWlCO1FBQzdCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUVwRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBa0JPO0lBQ1gsQ0FBQztJQUVELG9FQUE2QixHQUE3QixVQUE4QixlQUFlO1FBQ3pDLElBQUksRUFBRSxDQUFDO1FBQ1AsSUFBSSxhQUFhLENBQUM7UUFDbEIsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDOUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztZQUNSLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7U0FFeEI7UUFDRCxFQUFFLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLG9CQUFvQixDQUFDLGVBQWUsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUUzRixJQUFJLEVBQUUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2YsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3QzthQUFNO1lBQ0gsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QztRQUNELElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELGtFQUEyQixHQUEzQixVQUE0QixlQUFlO1FBQ3ZDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELHlEQUFrQixHQUFsQixVQUFtQixLQUFLO1FBQ3BCLElBQUksS0FBSyxFQUFFO1lBQ1AsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDNUI7SUFDTCxDQUFDO0lBRUQseURBQWtCLEdBQWxCLFVBQW1CLEtBQUs7UUFDcEIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUQsbURBQVksR0FBWixVQUFhLEtBQUs7UUFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsaURBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxzREFBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFFRCxnREFBUyxHQUFULFVBQVUsR0FBRztRQUNULElBQUksR0FBRyxDQUFDLFVBQVUsRUFBRTtZQUNoQixHQUFHLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzVCO2FBQU07WUFDSCxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUM5QjtJQUNMLENBQUM7SUFFRCxrREFBVyxHQUFYLFVBQVksV0FBVyxFQUFFLFVBQW9CO1FBQ3pDLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUMxRixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFaEIsT0FBTztZQUNILEtBQUssRUFBRSxXQUFXLENBQUMsS0FBSztZQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUk7WUFDdEIsT0FBTyxFQUFFLFdBQVcsQ0FBQyxPQUFPO1NBQy9CLENBQUM7SUFDTixDQUFDO0lBRUQsa0RBQVcsR0FBWCxVQUFZLFdBQVcsRUFBRSxVQUFvQjtRQUN6QyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELHdEQUFpQixHQUFqQixVQUFrQixJQUFJLEVBQUUsVUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN0RyxDQUFDO0lBRUQsa0RBQVcsR0FBWCxVQUFZLEdBQUc7UUFDWCxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVELGdGQUFnRjtJQUNoRixvREFBYSxHQUFiLFVBQWMsSUFBVTtRQUNwQixJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDbkQsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDM0QsNEZBQTRGO1FBQzVGLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzlCO1FBQ0QsT0FBTyxXQUFXLEtBQUssT0FBTyxDQUFDO0lBQ25DLENBQUM7SUFFRCxnRkFBZ0Y7SUFDaEYsbURBQVksR0FBWixVQUFhLEtBQUs7UUFBbEIsaUJBSUM7UUFIRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUExQixDQUEwQixDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELG1EQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxnREFBUyxHQUFULFVBQVUsS0FBNEI7UUFBdEMsaUJBZ0JDO1FBZkcsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sS0FBSyxRQUFRLEVBQW5CLENBQW1CLENBQUMsRUFBRTtZQUN6RCxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUNyQjtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLEtBQUssUUFBUSxFQUFuQixDQUFtQixDQUFDLEVBQUU7WUFDekQsS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUM7U0FDckI7UUFDRCxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssRUFBRSxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzlGLElBQU0sYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07WUFDOUIsSUFBSSxDQUFDLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxTQUFTLENBQUMsRUFBRTtnQkFDdkUsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFVBQUEsZ0JBQWdCLElBQUksT0FBQSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLEVBQTVDLENBQTRDLENBQUMsQ0FBQyxDQUFDO2FBQzdIO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCx3REFBaUIsR0FBakIsVUFBa0IsS0FBSyxFQUFFLE1BQU07UUFDM0IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzNCLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLElBQU0sVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFRCwyREFBb0IsR0FBcEIsVUFBcUIsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU07UUFBL0QsaUJBYUM7UUFaRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLFVBQUMsS0FBSztZQUM1QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFNLEtBQUssR0FBRyxVQUFVLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO2dCQUM5QyxNQUFNLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzthQUN4QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxVQUFDLEtBQUs7WUFDMUMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3ZDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQU0sR0FBTjtRQUFBLGlCQTBCQztRQXpCRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ2pDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxFQUFFLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztTQUN4QjtRQUNELElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNiLE9BQU8sQ0FBQyxJQUFJLENBQUMsbURBQW1ELENBQUMsQ0FBQztZQUNsRSxPQUFPO1NBQ1Y7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDM0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDakQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUMxRDtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztZQUNsQyxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0RBQWlCLEdBQWpCLFVBQWtCLEtBQUs7UUFDbkIsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsZ0RBQVMsR0FBVDtRQUFBLGlCQXFDQztRQXBDRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFO2lCQUM5QixTQUFTLENBQUMsVUFBQSxlQUFlO2dCQUN0QixJQUFJLGFBQWEsQ0FBQztnQkFDbEIsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLGdCQUFnQixJQUFJLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLElBQUksZUFBZSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUU7b0JBQ2hKLGFBQWEsR0FBRyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztpQkFDL0QsQ0FBQSxlQUFlO2dCQUNoQixLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7b0JBQzFCLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO3dCQUM1RyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsc0JBQXNCLElBQUksR0FBRyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRTs0QkFDNUUsK0JBQStCOzRCQUMvQixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUM7NEJBQ2pGLEtBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLHNCQUFzQixDQUFDLFVBQVUsQ0FBQzs0QkFDcEQsS0FBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7NEJBQ3BCLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztnQ0FDckIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxlQUFlO29DQUN4RCxJQUFJLGVBQWUsQ0FBQyxFQUFFLEtBQUssaUJBQWlCLEVBQUU7d0NBQzFDLDRCQUE0Qjt3Q0FDNUIsZUFBZSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxVQUFBLG1CQUFtQjs0Q0FDN0QsSUFBSSxtQkFBbUIsQ0FBQyxFQUFFLEtBQUssOEJBQThCLEVBQUU7Z0RBQzNELElBQUksbUJBQW1CLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssTUFBTSxFQUFFO29EQUNsRCxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpREFDOUI7NkNBQ0o7d0NBQ0wsQ0FBQyxDQUFDLENBQUM7cUNBQ047Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBRTlCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtDQUFRLEdBQVI7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxLQUFLLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMzRSxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDbEYsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLGNBQWMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7UUFDNUosSUFBSSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDekQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pFO0lBQ0wsQ0FBQzs7Z0JBL1N1QixXQUFXO2dCQUNSLGNBQWM7Z0JBQ1Ysa0JBQWtCO2dCQUM3QixVQUFVO2dCQUNDLGtCQUFrQjtnQkFDdEIsY0FBYztnQkFDRCwyQkFBMkI7Z0JBQ2hELFNBQVM7Z0JBQ0gsWUFBWTtnQkFDYixXQUFXO2dCQUNoQixNQUFNO2dCQUNKLFNBQVM7O0lBMUNyQjtRQUFSLEtBQUssRUFBRTsrREFBeUI7SUFDeEI7UUFBUixLQUFLLEVBQUU7OERBQXNCO0lBQ3JCO1FBQVIsS0FBSyxFQUFFO3dFQUFzQztJQUNyQztRQUFSLEtBQUssRUFBRTtvRUFBK0I7SUFDOUI7UUFBUixLQUFLLEVBQUU7bUZBQThDO0lBQzdDO1FBQVIsS0FBSyxFQUFFO3FFQUFvQjtJQUNuQjtRQUFSLEtBQUssRUFBRTs2RUFBNEI7SUFDM0I7UUFBUixLQUFLLEVBQUU7eUVBQXdCO0lBQ3ZCO1FBQVIsS0FBSyxFQUFFO3dFQUF1QjtJQUN0QjtRQUFSLEtBQUssRUFBRTtxRkFBb0M7SUFDbkM7UUFBUixLQUFLLEVBQUU7MEVBQXlCO0lBRXZCO1FBQVQsTUFBTSxFQUFFO29FQUFzQztJQUNyQztRQUFULE1BQU0sRUFBRTtpRUFBb0M7SUFDbkM7UUFBVCxNQUFNLEVBQUU7cUVBQXVDO0lBQ3RDO1FBQVQsTUFBTSxFQUFFO3NFQUF3QztJQUN2QztRQUFULE1BQU0sRUFBRTttRkFBcUQ7SUFDcEQ7UUFBVCxNQUFNLEVBQUU7eUVBQTJDO0lBQzFDO1FBQVQsTUFBTSxFQUFFOzJFQUE2QztJQUM1QztRQUFULE1BQU0sRUFBRTttRUFBcUM7SUFDcEM7UUFBVCxNQUFNLEVBQUU7bUVBQXFDO0lBQ3BDO1FBQVQsTUFBTSxFQUFFO21GQUFxRDtJQUNwRDtRQUFULE1BQU0sRUFBRTttRkFBcUQ7SUFDcEQ7UUFBVCxNQUFNLEVBQUU7aUZBQW1EO0lBQ2xEO1FBQVQsTUFBTSxFQUFFO2tGQUFvRDtJQUNuRDtRQUFULE1BQU0sRUFBRTtxRkFBdUQ7SUFDdEQ7UUFBVCxNQUFNLEVBQUU7Z0ZBQWtEO0lBQ2pEO1FBQVQsTUFBTSxFQUFFO2tGQUFvRDtJQUNuRDtRQUFULE1BQU0sRUFBRTtpRkFBbUQ7SUFDbkQ7UUFBUixLQUFLLEVBQUU7bUZBQTJCO0lBb0JHO1FBQXJDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7cUVBQXNCO0lBbkRsRCw0QkFBNEI7UUFYeEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyx3alFBQXFEO1lBRXJELFVBQVUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUU7b0JBQ2pDLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQzFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQ3pDLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsMENBQTBDLENBQUMsQ0FBQztpQkFDNUYsQ0FBQyxDQUFDOztTQUNOLENBQUM7T0FFVyw0QkFBNEIsQ0FpVnhDO0lBQUQsbUNBQUM7Q0FBQSxBQWpWRCxJQWlWQztTQWpWWSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHsgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdGFibGUnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IE9UTU1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgdHJpZ2dlciwgc3RhdGUsIHN0eWxlLCBhbmltYXRlLCB0cmFuc2l0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9tcG0uZmllbGQuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUHJvamVjdFNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9wcm9qZWN0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtYXRUb0xvY2FsZVBpcGUgfSBmcm9tICcuLi8uLi9zaGFyZWQvcGlwZS9mb3JtYXQtdG8tbG9jYWxlLnBpcGUnO1xyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IERlbGl2ZXJhYmxlQnVsa0NvdW50U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvZGVsaXZlcmFibGUtYnVsay1jb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQXNzZXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvYXNzZXQuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXNzZXRDb25zdGFudHMgfSBmcm9tICcuLi8uLi9wcm9qZWN0L2Fzc2V0cy9hc3NldF9jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wLCBtb3ZlSXRlbUluQXJyYXkgfSBmcm9tICdAYW5ndWxhci9jZGsvZHJhZy1kcm9wJztcclxuLy9pbXBvcnQgeyBEZWxpdmVyYWJsZUNyZWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICdzcmMvYXBwL2NvcmUvbXBtL3Byb2plY3Qtdmlldy90YXNrL3Rhc2stZGV0YWlscy9kZWxpdmVyYWJsZS1jcmVhdGlvbi1tb2RhbC9kZWxpdmVyYWJsZS1jcmVhdGlvbi1tb2RhbC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1kZWxpdmVyYWJsZS10YXNrLWxpc3QnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RlbGl2ZXJhYmxlLXRhc2stbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kZWxpdmVyYWJsZS10YXNrLWxpc3QuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGFuaW1hdGlvbnM6IFt0cmlnZ2VyKCdkZXRhaWxFeHBhbmQnLCBbXHJcbiAgICAgICAgc3RhdGUoJ2NvbGxhcHNlZCcsIHN0eWxlKHsgaGVpZ2h0OiAnMCcgfSkpLFxyXG4gICAgICAgIHN0YXRlKCdleHBhbmRlZCcsIHN0eWxlKHsgaGVpZ2h0OiAnKicgfSkpLFxyXG4gICAgICAgIHRyYW5zaXRpb24oJ2V4cGFuZGVkIDw9PiBjb2xsYXBzZWQnLCBhbmltYXRlKCcyMjVtcyBjdWJpYy1iZXppZXIoMC40MiwgMC4wLCAwLjU4LCAxLjApJykpLFxyXG4gICAgXSldXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVUYXNrTGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCxPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIHB1YmxpYyBsZXZlbDogTVBNX0xFVkVMO1xyXG4gICAgQElucHV0KCkgcHVibGljIHNvcnQ6IE1hdFNvcnQ7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgZGlzcGxheUNvbHVtbnM6IEFycmF5PHN0cmluZz47XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgZGF0YVNvdXJjZTogQXJyYXk8YW55PjtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzOiBBcnJheTxhbnk+O1xyXG4gICAgQElucHV0KCkgcHVibGljIGJ1bGtBY3Rpb25zO1xyXG4gICAgQElucHV0KCkgcHVibGljIHVuc2VsZWN0RGVsaXZlcmFibGU7XHJcbiAgICBASW5wdXQoKSBwdWJsaWMgYXNzZXREZXRhaWxEYXRhO1xyXG4gICAgQElucHV0KCkgcHVibGljIHNlbGVjdGVkRmlsdGVyO1xyXG4gICAgQElucHV0KCkgcHVibGljIHNlbGVjdGVkTGlzdERlcGVuZGVudEZpbHRlcjtcclxuICAgIEBJbnB1dCgpIHB1YmxpYyBidWxrUmVzcG9uc2VEYXRhO1xyXG5cclxuICAgIEBPdXRwdXQoKSBzb3J0Q2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVmcmVzaCA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuICAgIEBPdXRwdXQoKSByb3dFeHBhbmRlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIHJvd0NvbGxhcHNlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRlbGl2ZXJhYmxlRGV0YWlsc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSB2ZXJzaW9uc0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzaGFyZUFzc2V0SGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGNySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGJ1bGtDaGVjayA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQE91dHB1dCgpIGRlbGl2ZXJhYmxlQnVsa0NoZWNrQ291bnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlQ291bnRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWREZWxpdmVyYWJsZURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBzZWxlY3RlZERlbGl2ZXJhYmxlRGF0YXMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBhc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBPdXRwdXQoKSBlZGl0RGVsaXZlcmFibGVIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgb3JkZXJlZERpc3BsYXlhYmxlRmllbGRzID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgcmVzaXplRGlzcGxheWFibGVGaWVsZHMgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICAgIEBJbnB1dCgpIHVzZXJQcmVmZXJlbmNlRnJlZXplQ291bnQ7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBwcm9qZWN0U2VydmljZTogUHJvamVjdFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGZvcm1hdFRvTG9jYWxlUGlwZTogRm9ybWF0VG9Mb2NhbGVQaXBlLFxyXG4gICAgICAgIHB1YmxpYyBlbGVtZW50OiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkZWxpdmVyYWJsZUJ1bGtDb3VudFNlcnZpY2U6IERlbGl2ZXJhYmxlQnVsa0NvdW50U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIGFzc2V0U2VydmljZTogQXNzZXRTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgIHB1YmxpYyByZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgICApIHsgfVxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMucmVsb2FkKClcclxuICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIEBWaWV3Q2hpbGQoTWF0U29ydCwgeyBzdGF0aWM6IHRydWUgfSkgc29ydENvbnRyb2w6IE1hdFNvcnQ7XHJcblxyXG4gICAgY29sV2lkdGggPSAyNzA7XHJcbiAgICBjb2xzOiBudW1iZXI7XHJcblxyXG4gICAgbWV0YURhdGFEYXRhVHlwZXMgPSBPVE1NRGF0YVR5cGVzO1xyXG4gICAgTVBNRmllbGRDb25zdGFudHMgPSBNUE1GaWVsZENvbnN0YW50cztcclxuXHJcbiAgICBzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8YW55Pih0cnVlLCBbXSk7XHJcbiAgICBkZWxpdmVyYWJsZVRhc2tEYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+KFtdKTtcclxuXHJcbiAgICBhcHBDb25maWc7XHJcbiAgICBkZWZhdWx0TXlUYXNrVmlld0NvbGxhcHNlZDogYm9vbGVhbjtcclxuXHJcbiAgICBkZWxpdmVyYWJsZUlkID0gW107XHJcbiAgICBzZWxlY3RlZCA9IGZhbHNlO1xyXG5cclxuICAgIGFzc2V0cztcclxuICAgIGZvbGRlcklkID0gW107XHJcbiAgICBwcm9qZWN0ID0gW107XHJcbiAgICBhc3NldERhdGEgPSBbXTtcclxuICAgIGFzc2V0RGV0YWlsRGF0YXMgPSBbXTtcclxuICAgIGFzc2V0RGV0YWlsO1xyXG4gICAgdG90YWxBc3NldHNDb3VudDtcclxuXHJcbiAgICBmaWx0ZXJUeXBlO1xyXG4gICAgSXNOb3RQTVZpZXc7XHJcblxyXG4gICAgb3BlbkNSKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5jckhhbmRsZXIuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdERlbGl2ZXJhYmxlKGRlbGl2ZXJhYmxlQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5lZGl0RGVsaXZlcmFibGVIYW5kbGVyLm5leHQoZGVsaXZlcmFibGVDb25maWcpO1xyXG5cclxuICAgICAgICAvKiAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihEZWxpdmVyYWJsZUNyZWF0aW9uTW9kYWxDb21wb25lbnQsIHtcclxuICAgICAgICAgICAgIHdpZHRoOiAnMzglJyxcclxuICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICBwcm9qZWN0SWQ6IGRlbGl2ZXJhYmxlQ29uZmlnLnByb2plY3RJZCxcclxuICAgICAgICAgICAgICAgICBpc1RlbXBsYXRlOiBkZWxpdmVyYWJsZUNvbmZpZy5pc1RlbXBsYXRlLFxyXG4gICAgICAgICAgICAgICAgIHRhc2tJZDogZGVsaXZlcmFibGVDb25maWcudGFza0lkLFxyXG4gICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlSWQ6IGRlbGl2ZXJhYmxlQ29uZmlnLmRlbGl2ZXJhYmxlSWQsXHJcbiAgICAgICAgICAgICAgICAgc2VsZWN0ZWRQcm9qZWN0OiBkZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFByb2plY3QsXHJcbiAgICAgICAgICAgICAgICAgc2VsZWN0ZWRUYXNrOiBkZWxpdmVyYWJsZUNvbmZpZy5zZWxlY3RlZFRhc2ssXHJcbiAgICAgICAgICAgICAgICAgbW9kYWxMYWJlbDogJ0VkaXQgRGVsaXZlcmFibGUnXHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQpXHJcbiAgICAgICAgICAgICAgICAgLy90aGlzLnRhc2tTZXJ2aWNlLnNldERlbGl2ZXJhYmxlUmVmcmVzaERhdGEoMjAwKTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7ICovXHJcbiAgICB9XHJcblxyXG4gICAgZGVsaXZlcmFibGVCdWxrRWRpdENoZWNrQ291bnQoZGVsaXZlcmFibGVEYXRhKSB7XHJcbiAgICAgICAgbGV0IGlkO1xyXG4gICAgICAgIGxldCBkZWxpdmVyYWJsZWlkO1xyXG4gICAgICAgIGlmICh0aGlzLmJ1bGtBY3Rpb25zID09PSBmYWxzZSAmJiAhdGhpcy5zZWxlY3RlZCkge1xyXG4gICAgICAgICAgICBpZCA9IFtdO1xyXG4gICAgICAgICAgICBkZWxpdmVyYWJsZWlkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZCA9IHRydWU7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICBpZCA9IHRoaXMuZGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlLmdldEJ1bGtEZWxpdmVyYWJsZUlkKGRlbGl2ZXJhYmxlRGF0YSwgZGVsaXZlcmFibGVpZCk7XHJcblxyXG4gICAgICAgIGlmIChpZC5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVCdWxrQ2hlY2tDb3VudC5uZXh0KHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVCdWxrQ2hlY2tDb3VudC5uZXh0KGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlQ291bnRzLm5leHQoaWQubGVuZ3RoKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGVsaXZlcmFibGVEYXRhcy5uZXh0KGlkKTtcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RlZEJ1bGtEZWxpdmVyYWJsZURhdGEoZGVsaXZlcmFibGVEYXRhKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERlbGl2ZXJhYmxlRGF0YS5uZXh0KGRlbGl2ZXJhYmxlRGF0YSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaERlbGl2ZXJhYmxlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVmcmVzaC5uZXh0KGV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZGVsaXZlcmFibGVEZXRhaWxzKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZURldGFpbHNIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHZpZXdWZXJzaW9ucyhldmVudCkge1xyXG4gICAgICAgIHRoaXMudmVyc2lvbnNIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNoYXJlQXNzZXQoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnNoYXJlQXNzZXRIYW5kbGVyLmVtaXQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXB1dGVHcmlkQ29scygpIHtcclxuICAgICAgICB0aGlzLmNvbHMgPSBNYXRoLmZsb29yKHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50Lm9mZnNldFBhcmVudC5vZmZzZXRXaWR0aCAvIHRoaXMuY29sV2lkdGgpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4cGFuZFJvdyhyb3cpIHtcclxuICAgICAgICBpZiAocm93LmlzRXhwYW5kZWQpIHtcclxuICAgICAgICAgICAgcm93LmlzRXhwYW5kZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5yb3dDb2xsYXBzZWQubmV4dCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJvdy5pc0V4cGFuZGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5yb3dFeHBhbmRlZC5uZXh0KHJvdyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFByaW9yaXR5KHByb2plY3REYXRhLCBjb2x1bW5EYXRhOiBNUE1GaWVsZCkge1xyXG4gICAgICAgIGNvbnN0IHByaW9yaXR5T2JqID0gdGhpcy51dGlsU2VydmljZS5nZXRQcmlvcml0eUljb24odGhpcy5nZXRQcm9wZXJ0eShwcm9qZWN0RGF0YSwgY29sdW1uRGF0YSksXHJcbiAgICAgICAgICAgIHRoaXMubGV2ZWwpO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBjb2xvcjogcHJpb3JpdHlPYmouY29sb3IsXHJcbiAgICAgICAgICAgIGljb246IHByaW9yaXR5T2JqLmljb24sXHJcbiAgICAgICAgICAgIHRvb2x0aXA6IHByaW9yaXR5T2JqLnRvb2x0aXBcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb3BlcnR5KHByb2plY3REYXRhLCBjb2x1bW5EYXRhOiBNUE1GaWVsZCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkVmFsdWVCeURpc3BsYXlDb2x1bW4oY29sdW1uRGF0YSwgcHJvamVjdERhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnZlclRvTG9jYWxEYXRlKGRhdGEsIHByb3BlcnR5SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXRUb0xvY2FsZVBpcGUudHJhbnNmb3JtKHRoaXMucHJvamVjdFNlcnZpY2UuY29udmVyVG9Mb2NhbERhdGUoZGF0YSwgcHJvcGVydHlJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIG9wZW5Db21tZW50KHJvdykge1xyXG4gICAgICAgIGFsZXJ0KCdUbyBCZSBkb25lJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIFdoZXRoZXIgdGhlIG51bWJlciBvZiBzZWxlY3RlZCBlbGVtZW50cyBtYXRjaGVzIHRoZSB0b3RhbCBudW1iZXIgb2Ygcm93cy4gKi9cclxuICAgIGlzQWxsU2VsZWN0ZWQoZGF0YT86IGFueSkge1xyXG4gICAgICAgIGNvbnN0IG51bVNlbGVjdGVkID0gdGhpcy5zZWxlY3Rpb24uc2VsZWN0ZWQubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IG51bVJvd3MgPSB0aGlzLmRlbGl2ZXJhYmxlVGFza0RhdGFTb3VyY2UuZGF0YS5sZW5ndGg7XHJcbiAgICAgICAgLy9pZiAodGhpcy5zZWxlY3Rpb24uaGFzVmFsdWUoKSAmJiBudW1TZWxlY3RlZCA9PT0gbnVtUm93cyAmJiAoZGF0YSB8fCBkYXRhID09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICBpZiAoZGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLmJ1bGtDaGVjay5uZXh0KHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYnVsa0NoZWNrLm5leHQoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVtU2VsZWN0ZWQgPT09IG51bVJvd3M7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIFNlbGVjdHMgYWxsIHJvd3MgaWYgdGhleSBhcmUgbm90IGFsbCBzZWxlY3RlZDsgb3RoZXJ3aXNlIGNsZWFyIHNlbGVjdGlvbi4gKi9cclxuICAgIG1hc3RlclRvZ2dsZShldmVudCkge1xyXG4gICAgICAgIHRoaXMuaXNBbGxTZWxlY3RlZChldmVudC5jaGVja2VkKSA/XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uLmNsZWFyKCkgOlxyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlVGFza0RhdGFTb3VyY2UuZGF0YS5mb3JFYWNoKHJvdyA9PiB0aGlzLnNlbGVjdGlvbi5zZWxlY3Qocm93KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaEV2ZW50KCkge1xyXG4gICAgICAgIHRoaXMucmVmcmVzaC5lbWl0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGFibGVEcm9wKGV2ZW50OiBDZGtEcmFnRHJvcDxzdHJpbmdbXT4pIHtcclxuICAgICAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgICAgIGlmICh0aGlzLmRpc3BsYXlDb2x1bW5zLmZpbmQoY29sdW1uID0+IGNvbHVtbiA9PT0gJ1NlbGVjdCcpKSB7XHJcbiAgICAgICAgICAgIGNvdW50ID0gY291bnQgKyAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5kaXNwbGF5Q29sdW1ucy5maW5kKGNvbHVtbiA9PiBjb2x1bW4gPT09ICdFeHBhbmQnKSkge1xyXG4gICAgICAgICAgICBjb3VudCA9IGNvdW50ICsgMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbW92ZUl0ZW1JbkFycmF5KHRoaXMuZGlzcGxheUNvbHVtbnMsIGV2ZW50LnByZXZpb3VzSW5kZXggKyBjb3VudCwgZXZlbnQuY3VycmVudEluZGV4ICsgY291bnQpO1xyXG4gICAgICAgIGNvbnN0IG9yZGVyZWRGaWVsZHMgPSBbXTtcclxuICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcclxuICAgICAgICAgICAgaWYgKCEoY29sdW1uID09PSAnU2VsZWN0JyB8fCBjb2x1bW4gPT09ICdFeHBhbmQnIHx8IGNvbHVtbiA9PT0gJ0FjdGlvbnMnKSkge1xyXG4gICAgICAgICAgICAgICAgb3JkZXJlZEZpZWxkcy5wdXNoKHRoaXMuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5maW5kKGRpc3BsYXlhYmxlRmllbGQgPT4gZGlzcGxheWFibGVGaWVsZC5JTkRFWEVSX0ZJRUxEX0lEID09PSBjb2x1bW4pKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3JkZXJlZERpc3BsYXlhYmxlRmllbGRzLm5leHQob3JkZXJlZEZpZWxkcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25SZXNpemVNb3VzZURvd24oZXZlbnQsIGNvbHVtbikge1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3Qgc3RhcnQgPSBldmVudC50YXJnZXQ7XHJcbiAgICAgICAgY29uc3QgcHJlc3NlZCA9IHRydWU7XHJcbiAgICAgICAgY29uc3Qgc3RhcnRYID0gZXZlbnQueDtcclxuICAgICAgICBjb25zdCBzdGFydFdpZHRoID0gJChzdGFydCkucGFyZW50KCkud2lkdGgoKTtcclxuICAgICAgICB0aGlzLmluaXRSZXNpemFibGVDb2x1bW5zKHN0YXJ0LCBwcmVzc2VkLCBzdGFydFgsIHN0YXJ0V2lkdGgsIGNvbHVtbik7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFJlc2l6YWJsZUNvbHVtbnMoc3RhcnQsIHByZXNzZWQsIHN0YXJ0WCwgc3RhcnRXaWR0aCwgY29sdW1uKSB7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4oJ2JvZHknLCAnbW91c2Vtb3ZlJywgKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwcmVzc2VkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB3aWR0aCA9IHN0YXJ0V2lkdGggKyAoZXZlbnQueCAtIHN0YXJ0WCk7XHJcbiAgICAgICAgICAgICAgICBjb2x1bW4ud2lkdGggPSB3aWR0aDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIubGlzdGVuKCdib2R5JywgJ21vdXNldXAnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHByZXNzZWQpIHtcclxuICAgICAgICAgICAgICAgIHByZXNzZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVzaXplRGlzcGxheWFibGVGaWVsZHMubmV4dCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVsb2FkKCkgeyAgICAgICBcclxuICAgICAgICB0aGlzLmNvbXB1dGVHcmlkQ29scygpO1xyXG4gICAgICAgIGlmICghdGhpcy5kaXNwbGF5Q29sdW1ucykge1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlDb2x1bW5zID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGhpcy5kaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMuZGF0YVNvdXJjZSkge1xyXG4gICAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kZWxpdmVyYWJsZVRhc2tEYXRhU291cmNlLmRhdGEgPSB0aGlzLmRhdGFTb3VyY2U7XHJcbiAgICAgICAgaWYgKCF0aGlzLmxldmVsKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gTGV2ZWwgaW5mbyB3YXMgcHJvdmlkZWQgdG8gbGlzdCB2aWV3IHRvIHJlbmRlcicpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLnNvcnQpIHtcclxuICAgICAgICAgICAgdGhpcy5zb3J0LmRpc2FibGVDbGVhciA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydENvbnRyb2wuYWN0aXZlID0gdGhpcy5zb3J0LmFjdGl2ZTtcclxuICAgICAgICAgICAgdGhpcy5zb3J0Q29udHJvbC5kaXJlY3Rpb24gPSB0aGlzLnNvcnQuZGlyZWN0aW9uO1xyXG4gICAgICAgICAgICB0aGlzLnNvcnRDb250cm9sLmRpc2FibGVDbGVhciA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrRGF0YVNvdXJjZS5zb3J0ID0gdGhpcy5zb3J0Q29udHJvbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zb3J0Q29udHJvbC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc29ydENoYW5nZS5lbWl0KHRoaXMuc29ydENvbnRyb2wpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHJpZXZlQXNzZXREYXRhKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5hc3NldERldGFpbHNDYWxsQmFja0hhbmRsZXIubmV4dChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXNzZXRzKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuY2hlY2tPdG1tU2Vzc2lvbigpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHVzZXJTZXNzaW9uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlc3Npb25SZXNwb25zZSAmJiBzZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZSAmJiBzZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZS5zZXNzaW9uICYmIHNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXNlclNlc3Npb25JZCA9IHNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfS8qIHRoaXMuc2tpcCAqL1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9sZGVySWQuZm9yRWFjaChmb2xkZXJJZCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXNzZXRTZXJ2aWNlLmdldEFzc2V0cyhBc3NldENvbnN0YW50cy5BU1NFVF9TRUFSQ0hfQ09ORElUSU9OLCBmb2xkZXJJZCwgMCwgNTAsIHVzZXJTZXNzaW9uSWQpLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlcyAmJiByZXMuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZSAmJiByZXMuc2VhcmNoX3Jlc3VsdF9yZXNvdXJjZS5hc3NldF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gIHRoaXMubG9hZGluZ0hhbmRsZXIoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG90YWxBc3NldHNDb3VudCA9IHJlcy5zZWFyY2hfcmVzdWx0X3Jlc291cmNlLnNlYXJjaF9yZXN1bHQudG90YWxfaGl0X2NvdW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXNzZXRzID0gcmVzLnNlYXJjaF9yZXN1bHRfcmVzb3VyY2UuYXNzZXRfbGlzdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFzc2V0RGF0YSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXNzZXRzLmZvckVhY2goYXNzZXQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QuZm9yRWFjaChtZXRhZGF0YUVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRWxlbWVudC5pZCA9PT0gJ01QTS5BU1NFVC5HUk9VUCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBbMF0ubWV0YWRhdGFfZWxlbWVudF9saXN0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFFbGVtZW50Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5mb3JFYWNoKG1ldGFkYXRhRWxlbWVudExpc3QgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50TGlzdC5pZCA9PT0gJ01QTS5BU1NFVC5JU19SRUZFUkVOQ0VfQVNTRVQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFFbGVtZW50TGlzdC52YWx1ZS52YWx1ZS52YWx1ZSA9PT0gJ3RydWUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hc3NldERhdGEucHVzaChhc3NldCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYXNzZXREYXRhKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyk7XHJcbiAgICAgICAgdGhpcy5maWx0ZXJUeXBlID0gKHRoaXMuc2VsZWN0ZWRGaWx0ZXIgPT09ICdNWV9URUFNX1RBU0tTJykgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgLy9kZWxpdmVyYWJsZVdvcmtWaWV3XHJcbiAgICAgICAgdGhpcy5Jc05vdFBNVmlldyA9IHRoaXMucm91dGVyLnVybC5pbmNsdWRlcygnZGVsaXZlcmFibGVXb3JrVmlldycpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucmVsb2FkKCk7XHJcbiAgICAgICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdE15VGFza1ZpZXdDb2xsYXBzZWQgPSB0aGlzLnV0aWxTZXJ2aWNlLmdldEJvb2xlYW5WYWx1ZSh0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5ERUZBVUxUX01ZVEFTS19WSUVXX0NPTExBUFNFRF0pO1xyXG4gICAgICAgIGlmICghdGhpcy5kZWZhdWx0TXlUYXNrVmlld0NvbGxhcHNlZCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlbGl2ZXJhYmxlVGFza0RhdGFTb3VyY2UuZGF0YVswXS5pc0V4cGFuZGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5yb3dFeHBhbmRlZC5uZXh0KHRoaXMuZGVsaXZlcmFibGVUYXNrRGF0YVNvdXJjZS5kYXRhWzBdKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19