import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var DeliverableBulkCountService = /** @class */ (function () {
    function DeliverableBulkCountService() {
        this.deliverableId = [];
    }
    DeliverableBulkCountService.prototype.getBulkDeliverableId = function (deliverableData, deliverableid) {
        if (deliverableid === true) {
            this.deliverableId = [];
        }
        if (this.deliverableId && !this.deliverableId.includes(deliverableData)) {
            this.deliverableId.push(deliverableData);
        }
        else {
            var index = this.deliverableId.indexOf(deliverableData);
            if (index > -1) {
                this.deliverableId.splice(index, 1);
            }
        }
        return this.deliverableId;
    };
    DeliverableBulkCountService.ɵprov = i0.ɵɵdefineInjectable({ factory: function DeliverableBulkCountService_Factory() { return new DeliverableBulkCountService(); }, token: DeliverableBulkCountService, providedIn: "root" });
    DeliverableBulkCountService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], DeliverableBulkCountService);
    return DeliverableBulkCountService;
}());
export { DeliverableBulkCountService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUtYnVsay1jb3VudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL2RlbGl2ZXJhYmxlLWJ1bGstY291bnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLM0M7SUFFRTtRQURBLGtCQUFhLEdBQUcsRUFBRSxDQUFDO0lBQ0gsQ0FBQztJQUVqQiwwREFBb0IsR0FBcEIsVUFBcUIsZUFBZSxFQUFFLGFBQWE7UUFDakQsSUFBSSxhQUFhLEtBQUssSUFBSSxFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1NBQ3pCO1FBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDdkUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzFELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNyQztTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7O0lBbEJVLDJCQUEyQjtRQUh2QyxVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csMkJBQTJCLENBbUJ2QztzQ0F4QkQ7Q0F3QkMsQUFuQkQsSUFtQkM7U0FuQlksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGVsaXZlcmFibGVCdWxrQ291bnRTZXJ2aWNlIHtcclxuICBkZWxpdmVyYWJsZUlkID0gW107XHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgZ2V0QnVsa0RlbGl2ZXJhYmxlSWQoZGVsaXZlcmFibGVEYXRhLCBkZWxpdmVyYWJsZWlkKSB7XHJcbiAgICBpZiAoZGVsaXZlcmFibGVpZCA9PT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLmRlbGl2ZXJhYmxlSWQgPSBbXTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5kZWxpdmVyYWJsZUlkICYmICF0aGlzLmRlbGl2ZXJhYmxlSWQuaW5jbHVkZXMoZGVsaXZlcmFibGVEYXRhKSkge1xyXG4gICAgICB0aGlzLmRlbGl2ZXJhYmxlSWQucHVzaChkZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmRlbGl2ZXJhYmxlSWQuaW5kZXhPZihkZWxpdmVyYWJsZURhdGEpO1xyXG4gICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgIHRoaXMuZGVsaXZlcmFibGVJZC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5kZWxpdmVyYWJsZUlkO1xyXG4gIH1cclxufVxyXG4iXX0=