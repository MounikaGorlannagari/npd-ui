import { __decorate, __read, __spread, __values } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var MetdataListViewFieldObjectHelperService = /** @class */ (function () {
    function MetdataListViewFieldObjectHelperService() {
    }
    MetdataListViewFieldObjectHelperService.prototype.getDisplayMetadataFields = function (metadataFieldList, metadataFilterList, isDefaultMetadataField) {
        var displayableMetadataFields = [];
        if (!Array.isArray(metadataFieldList) || !Array.isArray(metadataFilterList)) {
            return displayableMetadataFields;
        }
        metadataFieldList.map(function (field) {
            var fieldObj = metadataFilterList.find(function (e) { return (e.id === field.FIELD_ID) && (field.IS_GRID_VIEW === 'true'); });
            if (fieldObj) {
                fieldObj.sequence = Number(field.DISPLAY_SEQUENCE);
                fieldObj.sortSequence = Number(field.SORT_SEQUENCE);
                fieldObj.referenceValue = (field && field.REFERENCE_VALUE && typeof field.REFERENCE_VALUE === 'string')
                    ? field.REFERENCE_VALUE : null;
                fieldObj.isDefaultMetadataField = isDefaultMetadataField;
                displayableMetadataFields.push(fieldObj);
            }
        });
        return displayableMetadataFields;
    };
    MetdataListViewFieldObjectHelperService.prototype.getListDisplayColumnConfig = function (metaDataFields, defaultMetadataFields, customMetadataFields) {
        var e_1, _a;
        var data = {
            displayableMetadataFields: [],
            displayColumns: []
        };
        var displayColumns = [];
        var displayableMetadataFields = [];
        var displayableCustomMetadataFields = [];
        if (metaDataFields && metaDataFields.metadata_element_list) {
            try {
                for (var _b = __values(metaDataFields.metadata_element_list), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var metadataElement = _c.value;
                    var metadataFields = metadataElement.metadata_element_list;
                    if (metadataFields && Array.isArray(metadataFields)) {
                        displayableMetadataFields.push.apply(displayableMetadataFields, __spread(this.getDisplayMetadataFields(defaultMetadataFields, metadataFields, true)));
                        // displayableCustomMetadataFields.push(...this.getDisplayMetadataFields(customMetadataFields, metadataFields, false));
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        displayableMetadataFields = displayableMetadataFields.sort(function (a, b) {
            if (a.isDefaultMetadataField === b.isDefaultMetadataField) {
                return a.sequence - b.sequence;
            }
            else {
                return a.isDefaultMetadataField ? -1 : 1;
            }
        });
        displayableMetadataFields = displayableMetadataFields.concat(displayableCustomMetadataFields);
        displayColumns = displayableMetadataFields.map(function (column) { return column.id; });
        displayColumns.unshift('Select');
        displayColumns.unshift('Expand');
        // displayColumns.push('Actions');
        data.displayColumns = displayColumns;
        data.displayableMetadataFields = displayableMetadataFields;
        return data;
    };
    MetdataListViewFieldObjectHelperService.ɵprov = i0.ɵɵdefineInjectable({ factory: function MetdataListViewFieldObjectHelperService_Factory() { return new MetdataListViewFieldObjectHelperService(); }, token: MetdataListViewFieldObjectHelperService, providedIn: "root" });
    MetdataListViewFieldObjectHelperService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], MetdataListViewFieldObjectHelperService);
    return MetdataListViewFieldObjectHelperService;
}());
export { MetdataListViewFieldObjectHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWV0ZGF0YS1saXN0LXZpZXctZmllbGQtb2JqZWN0LWhlbHBlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL21ldGRhdGEtbGlzdC12aWV3LWZpZWxkLW9iamVjdC1oZWxwZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFPM0M7SUFFRTtJQUFnQixDQUFDO0lBRWpCLDBFQUF3QixHQUF4QixVQUF5QixpQkFBNkIsRUFBRSxrQkFBOEIsRUFBRSxzQkFBK0I7UUFDckgsSUFBTSx5QkFBeUIsR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsRUFBRTtZQUMzRSxPQUFPLHlCQUF5QixDQUFDO1NBQ2xDO1FBQ0QsaUJBQWlCLENBQUMsR0FBRyxDQUFDLFVBQUEsS0FBSztZQUN6QixJQUFNLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksS0FBSyxNQUFNLENBQUMsRUFBNUQsQ0FBNEQsQ0FBQyxDQUFDO1lBQzVHLElBQUksUUFBUSxFQUFFO2dCQUNaLFFBQVEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNuRCxRQUFRLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3BELFFBQVEsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLGVBQWUsSUFBSSxPQUFPLEtBQUssQ0FBQyxlQUFlLEtBQUssUUFBUSxDQUFDO29CQUNyRyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNqQyxRQUFRLENBQUMsc0JBQXNCLEdBQUcsc0JBQXNCLENBQUM7Z0JBQ3pELHlCQUF5QixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUMxQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyx5QkFBeUIsQ0FBQztJQUNuQyxDQUFDO0lBRUQsNEVBQTBCLEdBQTFCLFVBQTJCLGNBQWMsRUFBRSxxQkFBcUIsRUFBRSxvQkFBb0I7O1FBQ3BGLElBQU0sSUFBSSxHQUF3QjtZQUNoQyx5QkFBeUIsRUFBRSxFQUFFO1lBQzdCLGNBQWMsRUFBRSxFQUFFO1NBQ25CLENBQUM7UUFDRixJQUFJLGNBQWMsR0FBa0IsRUFBRSxDQUFDO1FBQ3ZDLElBQUkseUJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQ25DLElBQU0sK0JBQStCLEdBQUcsRUFBRSxDQUFDO1FBQzNDLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRTs7Z0JBQzFELEtBQThCLElBQUEsS0FBQSxTQUFBLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBL0QsSUFBTSxlQUFlLFdBQUE7b0JBQ3hCLElBQU0sY0FBYyxHQUFHLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDN0QsSUFBSSxjQUFjLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTt3QkFDbkQseUJBQXlCLENBQUMsSUFBSSxPQUE5Qix5QkFBeUIsV0FBUyxJQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxHQUFFO3dCQUM5Ryx1SEFBdUg7cUJBQ3hIO2lCQUNGOzs7Ozs7Ozs7U0FDRjtRQUVELHlCQUF5QixHQUFHLHlCQUF5QixDQUFDLElBQUksQ0FBQyxVQUFDLENBQUMsRUFBRSxDQUFDO1lBQzlELElBQUksQ0FBQyxDQUFDLHNCQUFzQixLQUFLLENBQUMsQ0FBQyxzQkFBc0IsRUFBRTtnQkFDekQsT0FBTyxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDMUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUdILHlCQUF5QixHQUFHLHlCQUF5QixDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBRTlGLGNBQWMsR0FBRyx5QkFBeUIsQ0FBQyxHQUFHLENBQUMsVUFBQyxNQUFXLElBQUssT0FBQSxNQUFNLENBQUMsRUFBRSxFQUFULENBQVMsQ0FBQyxDQUFDO1FBQzNFLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqQyxrQ0FBa0M7UUFFbEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7UUFDckMsSUFBSSxDQUFDLHlCQUF5QixHQUFHLHlCQUF5QixDQUFDO1FBQzNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQzs7SUE1RFUsdUNBQXVDO1FBSm5ELFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FFVyx1Q0FBdUMsQ0E4RG5EO2tEQXJFRDtDQXFFQyxBQTlERCxJQThEQztTQTlEWSx1Q0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERpc3BsYXlDb2x1bW5Db25maWcgfSBmcm9tICcuLi9PYmplY3RzL0Rpc3BsYXlDb2x1bW5Db25maWcnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE1ldGRhdGFMaXN0Vmlld0ZpZWxkT2JqZWN0SGVscGVyU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIGdldERpc3BsYXlNZXRhZGF0YUZpZWxkcyhtZXRhZGF0YUZpZWxkTGlzdDogQXJyYXk8YW55PiwgbWV0YWRhdGFGaWx0ZXJMaXN0OiBBcnJheTxhbnk+LCBpc0RlZmF1bHRNZXRhZGF0YUZpZWxkOiBib29sZWFuKTogQXJyYXk8YW55PiB7XHJcbiAgICBjb25zdCBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkobWV0YWRhdGFGaWVsZExpc3QpIHx8ICFBcnJheS5pc0FycmF5KG1ldGFkYXRhRmlsdGVyTGlzdCkpIHtcclxuICAgICAgcmV0dXJuIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcbiAgICBtZXRhZGF0YUZpZWxkTGlzdC5tYXAoZmllbGQgPT4ge1xyXG4gICAgICBjb25zdCBmaWVsZE9iaiA9IG1ldGFkYXRhRmlsdGVyTGlzdC5maW5kKGUgPT4gKGUuaWQgPT09IGZpZWxkLkZJRUxEX0lEKSAmJiAoZmllbGQuSVNfR1JJRF9WSUVXID09PSAndHJ1ZScpKTtcclxuICAgICAgaWYgKGZpZWxkT2JqKSB7XHJcbiAgICAgICAgZmllbGRPYmouc2VxdWVuY2UgPSBOdW1iZXIoZmllbGQuRElTUExBWV9TRVFVRU5DRSk7XHJcbiAgICAgICAgZmllbGRPYmouc29ydFNlcXVlbmNlID0gTnVtYmVyKGZpZWxkLlNPUlRfU0VRVUVOQ0UpO1xyXG4gICAgICAgIGZpZWxkT2JqLnJlZmVyZW5jZVZhbHVlID0gKGZpZWxkICYmIGZpZWxkLlJFRkVSRU5DRV9WQUxVRSAmJiB0eXBlb2YgZmllbGQuUkVGRVJFTkNFX1ZBTFVFID09PSAnc3RyaW5nJylcclxuICAgICAgICAgID8gZmllbGQuUkVGRVJFTkNFX1ZBTFVFIDogbnVsbDtcclxuICAgICAgICBmaWVsZE9iai5pc0RlZmF1bHRNZXRhZGF0YUZpZWxkID0gaXNEZWZhdWx0TWV0YWRhdGFGaWVsZDtcclxuICAgICAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLnB1c2goZmllbGRPYmopO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gIH1cclxuXHJcbiAgZ2V0TGlzdERpc3BsYXlDb2x1bW5Db25maWcobWV0YURhdGFGaWVsZHMsIGRlZmF1bHRNZXRhZGF0YUZpZWxkcywgY3VzdG9tTWV0YWRhdGFGaWVsZHMpOiBEaXNwbGF5Q29sdW1uQ29uZmlnIHtcclxuICAgIGNvbnN0IGRhdGE6IERpc3BsYXlDb2x1bW5Db25maWcgPSB7XHJcbiAgICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM6IFtdLFxyXG4gICAgICBkaXNwbGF5Q29sdW1uczogW11cclxuICAgIH07XHJcbiAgICBsZXQgZGlzcGxheUNvbHVtbnM6IEFycmF5PHN0cmluZz4gPSBbXTtcclxuICAgIGxldCBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICBjb25zdCBkaXNwbGF5YWJsZUN1c3RvbU1ldGFkYXRhRmllbGRzID0gW107XHJcbiAgICBpZiAobWV0YURhdGFGaWVsZHMgJiYgbWV0YURhdGFGaWVsZHMubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgIGZvciAoY29uc3QgbWV0YWRhdGFFbGVtZW50IG9mIG1ldGFEYXRhRmllbGRzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICAgIGNvbnN0IG1ldGFkYXRhRmllbGRzID0gbWV0YWRhdGFFbGVtZW50Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdDtcclxuICAgICAgICBpZiAobWV0YWRhdGFGaWVsZHMgJiYgQXJyYXkuaXNBcnJheShtZXRhZGF0YUZpZWxkcykpIHtcclxuICAgICAgICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMucHVzaCguLi50aGlzLmdldERpc3BsYXlNZXRhZGF0YUZpZWxkcyhkZWZhdWx0TWV0YWRhdGFGaWVsZHMsIG1ldGFkYXRhRmllbGRzLCB0cnVlKSk7XHJcbiAgICAgICAgICAvLyBkaXNwbGF5YWJsZUN1c3RvbU1ldGFkYXRhRmllbGRzLnB1c2goLi4udGhpcy5nZXREaXNwbGF5TWV0YWRhdGFGaWVsZHMoY3VzdG9tTWV0YWRhdGFGaWVsZHMsIG1ldGFkYXRhRmllbGRzLCBmYWxzZSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHMgPSBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgaWYgKGEuaXNEZWZhdWx0TWV0YWRhdGFGaWVsZCA9PT0gYi5pc0RlZmF1bHRNZXRhZGF0YUZpZWxkKSB7XHJcbiAgICAgICAgcmV0dXJuIGEuc2VxdWVuY2UgLSBiLnNlcXVlbmNlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBhLmlzRGVmYXVsdE1ldGFkYXRhRmllbGQgPyAtMSA6IDE7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcbiAgICBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzID0gZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcy5jb25jYXQoZGlzcGxheWFibGVDdXN0b21NZXRhZGF0YUZpZWxkcyk7XHJcblxyXG4gICAgZGlzcGxheUNvbHVtbnMgPSBkaXNwbGF5YWJsZU1ldGFkYXRhRmllbGRzLm1hcCgoY29sdW1uOiBhbnkpID0+IGNvbHVtbi5pZCk7XHJcbiAgICBkaXNwbGF5Q29sdW1ucy51bnNoaWZ0KCdTZWxlY3QnKTtcclxuICAgIGRpc3BsYXlDb2x1bW5zLnVuc2hpZnQoJ0V4cGFuZCcpO1xyXG4gICAgLy8gZGlzcGxheUNvbHVtbnMucHVzaCgnQWN0aW9ucycpO1xyXG5cclxuICAgIGRhdGEuZGlzcGxheUNvbHVtbnMgPSBkaXNwbGF5Q29sdW1ucztcclxuICAgIGRhdGEuZGlzcGxheWFibGVNZXRhZGF0YUZpZWxkcyA9IGRpc3BsYXlhYmxlTWV0YWRhdGFGaWVsZHM7XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==