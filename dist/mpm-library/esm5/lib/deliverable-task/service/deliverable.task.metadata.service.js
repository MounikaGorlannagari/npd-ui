import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { CategoryService } from '../../project/shared/services/category.service';
import { MPM_LEVELS } from '../../mpm-utils/objects/Level';
import { ViewTypes } from '../../mpm-utils/objects/ViewTypes';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import * as i0 from "@angular/core";
import * as i1 from "../../project/shared/services/category.service";
import * as i2 from "../../mpm-utils/services/entity.appdef.service";
import * as i3 from "../../mpm-utils/services/otmm.service";
import * as i4 from "../../mpm-utils/services/sharing.service";
import * as i5 from "../../shared/services/view-config.service";
var DeliverableTaskMetadataService = /** @class */ (function () {
    function DeliverableTaskMetadataService(categoryService, entityAppDefService, otmmService, sharingService, viewConfigService) {
        this.categoryService = categoryService;
        this.entityAppDefService = entityAppDefService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.viewConfigService = viewConfigService;
    }
    DeliverableTaskMetadataService.prototype.getMPMFieldsByLevel = function (level) {
        /* if (level) {
          let viewName: string;
          if (level === MPM_LEVELS.PROJECT) {
            viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT;
          } else if (level === MPM_LEVELS.TASK) {
            viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK;
          }
          return this.viewConfigService.getAllDisplayFeildForViewConfig(viewName);
        } */
        if (level) {
            var viewName = void 0;
            if (level === MPM_LEVELS.PROJECT) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_PROJECT.MY_TASK_VIEW_DASHBOARD_BY_PROJECT;
            }
            else if (level === MPM_LEVELS.TASK) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_TASK.MY_TASK_VIEW_DASHBOARD_BY_TASK;
            }
            else if (level === MPM_LEVELS.CAMPAIGN) {
                viewName = ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.VIEW_NAME ? ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.VIEW_NAME : ViewTypes.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN.MY_TASK_VIEW_DASHBOARD_BY_CAMPAIGN;
            }
            return this.viewConfigService.getAllDisplayFeildForViewConfig(viewName);
        }
    };
    DeliverableTaskMetadataService.ctorParameters = function () { return [
        { type: CategoryService },
        { type: EntityAppDefService },
        { type: OTMMService },
        { type: SharingService },
        { type: ViewConfigService }
    ]; };
    DeliverableTaskMetadataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function DeliverableTaskMetadataService_Factory() { return new DeliverableTaskMetadataService(i0.ɵɵinject(i1.CategoryService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.OTMMService), i0.ɵɵinject(i4.SharingService), i0.ɵɵinject(i5.ViewConfigService)); }, token: DeliverableTaskMetadataService, providedIn: "root" });
    DeliverableTaskMetadataService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], DeliverableTaskMetadataService);
    return DeliverableTaskMetadataService;
}());
export { DeliverableTaskMetadataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsaXZlcmFibGUudGFzay5tZXRhZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvZGVsaXZlcmFibGUtdGFzay9zZXJ2aWNlL2RlbGl2ZXJhYmxlLnRhc2subWV0YWRhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBRWpGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUUzRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBRTFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7Ozs7O0FBSzlFO0lBRUUsd0NBQ1MsZUFBZ0MsRUFDaEMsbUJBQXdDLEVBQ3hDLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLGlCQUFvQztRQUpwQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUN6QyxDQUFDO0lBRUwsNERBQW1CLEdBQW5CLFVBQW9CLEtBQUs7UUFFdkI7Ozs7Ozs7O1lBUUk7UUFDSixJQUFJLEtBQUssRUFBRTtZQUNULElBQUksUUFBUSxTQUFRLENBQUM7WUFDckIsSUFBSSxLQUFLLEtBQUssVUFBVSxDQUFDLE9BQU8sRUFBRTtnQkFFaEMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxpQ0FBaUMsQ0FBQyxpQ0FBaUMsQ0FBQzthQUMxTTtpQkFBTSxJQUFJLEtBQUssS0FBSyxVQUFVLENBQUMsSUFBSSxFQUFFO2dCQUNwQyxRQUFRLEdBQUcsU0FBUyxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLDhCQUE4QixDQUFDO2FBQzlMO2lCQUFNLElBQUksS0FBSyxLQUFLLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3hDLFFBQVEsR0FBRyxTQUFTLENBQUMsa0NBQWtDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsa0NBQWtDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsa0NBQWtDLENBQUMsa0NBQWtDLENBQUM7YUFDOU07WUFDRCxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQywrQkFBK0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN6RTtJQUNILENBQUM7O2dCQTlCeUIsZUFBZTtnQkFDWCxtQkFBbUI7Z0JBQzNCLFdBQVc7Z0JBQ1IsY0FBYztnQkFDWCxpQkFBaUI7OztJQVBsQyw4QkFBOEI7UUFIMUMsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLDhCQUE4QixDQWtDMUM7eUNBakREO0NBaURDLEFBbENELElBa0NDO1NBbENZLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT1RNTVNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvb3RtbS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9lbnRpdHkuYXBwZGVmLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXRlZ29yeVNlcnZpY2UgfSBmcm9tICcuLi8uLi9wcm9qZWN0L3NoYXJlZC9zZXJ2aWNlcy9jYXRlZ29yeS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUxTIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTGV2ZWwnO1xyXG5pbXBvcnQgeyBNZXRkYXRhRmllbGRzQ29uZmlnT2JqIH0gZnJvbSAnLi4vT2JqZWN0cy9NZXRkYXRhRmllbGRzQ29uZmlnT2JqJztcclxuaW1wb3J0IHsgVmlld1R5cGVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld1R5cGVzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVmlld0NvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL1ZpZXdDb25maWcnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIERlbGl2ZXJhYmxlVGFza01ldGFkYXRhU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGNhdGVnb3J5U2VydmljZTogQ2F0ZWdvcnlTZXJ2aWNlLFxyXG4gICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyB2aWV3Q29uZmlnU2VydmljZTogVmlld0NvbmZpZ1NlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBnZXRNUE1GaWVsZHNCeUxldmVsKGxldmVsKTogT2JzZXJ2YWJsZTxWaWV3Q29uZmlnPiB7XHJcblxyXG4gICAgLyogaWYgKGxldmVsKSB7XHJcbiAgICAgIGxldCB2aWV3TmFtZTogc3RyaW5nO1xyXG4gICAgICBpZiAobGV2ZWwgPT09IE1QTV9MRVZFTFMuUFJPSkVDVCkge1xyXG4gICAgICAgIHZpZXdOYW1lID0gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfUFJPSkVDVDtcclxuICAgICAgfSBlbHNlIGlmIChsZXZlbCA9PT0gTVBNX0xFVkVMUy5UQVNLKSB7XHJcbiAgICAgICAgdmlld05hbWUgPSBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9UQVNLO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLmdldEFsbERpc3BsYXlGZWlsZEZvclZpZXdDb25maWcodmlld05hbWUpO1xyXG4gICAgfSAqL1xyXG4gICAgaWYgKGxldmVsKSB7XHJcbiAgICAgIGxldCB2aWV3TmFtZTogc3RyaW5nO1xyXG4gICAgICBpZiAobGV2ZWwgPT09IE1QTV9MRVZFTFMuUFJPSkVDVCkge1xyXG5cclxuICAgICAgICB2aWV3TmFtZSA9IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1BST0pFQ1QuVklFV19OQU1FID8gVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfUFJPSkVDVC5WSUVXX05BTUUgOiBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9QUk9KRUNULk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfUFJPSkVDVDtcclxuICAgICAgfSBlbHNlIGlmIChsZXZlbCA9PT0gTVBNX0xFVkVMUy5UQVNLKSB7XHJcbiAgICAgICAgdmlld05hbWUgPSBWaWV3VHlwZXMuTVlfVEFTS19WSUVXX0RBU0hCT0FSRF9CWV9UQVNLLlZJRVdfTkFNRSA/IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1RBU0suVklFV19OQU1FIDogVmlld1R5cGVzLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfVEFTSy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX1RBU0s7XHJcbiAgICAgIH0gZWxzZSBpZiAobGV2ZWwgPT09IE1QTV9MRVZFTFMuQ0FNUEFJR04pIHtcclxuICAgICAgICB2aWV3TmFtZSA9IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX0NBTVBBSUdOLlZJRVdfTkFNRSA/IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX0NBTVBBSUdOLlZJRVdfTkFNRSA6IFZpZXdUeXBlcy5NWV9UQVNLX1ZJRVdfREFTSEJPQVJEX0JZX0NBTVBBSUdOLk1ZX1RBU0tfVklFV19EQVNIQk9BUkRfQllfQ0FNUEFJR047XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuZ2V0QWxsRGlzcGxheUZlaWxkRm9yVmlld0NvbmZpZyh2aWV3TmFtZSk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==