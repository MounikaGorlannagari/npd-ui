import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var AppMenuComponent = /** @class */ (function () {
    function AppMenuComponent() {
        this.menuClicked = new EventEmitter();
    }
    AppMenuComponent.prototype.onMenuClick = function (menu) {
        this.menuClicked.emit(menu.LOCATION);
    };
    AppMenuComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input()
    ], AppMenuComponent.prototype, "menus", void 0);
    __decorate([
        Output()
    ], AppMenuComponent.prototype, "menuClicked", void 0);
    AppMenuComponent = __decorate([
        Component({
            selector: 'mpm-app-menu',
            template: "<div class=\"menu-list\" *ngIf=\"menus && menus.length\">\r\n    <a class=\"menu-link\" mat-button *ngFor=\"let menu of menus\"  [class.active]=\"menu.ACTIVE\"\r\n        (click)=\"onMenuClick(menu)\">{{menu.DISPLAY_NAME}}</a>\r\n</div>",
            styles: [".menu-list{display:flex;flex-direction:row;flex-wrap:wrap;justify-content:center}.menu-list a{font-weight:400;color:#000}"]
        })
    ], AppMenuComponent);
    return AppMenuComponent;
}());
export { AppMenuComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLW1lbnUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvYXBwLW1lbnUvYXBwLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUS9FO0lBS0k7UUFGVSxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7SUFFbkMsQ0FBQztJQUVqQixzQ0FBVyxHQUFYLFVBQVksSUFBSTtRQUNaLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsbUNBQVEsR0FBUjtJQUNBLENBQUM7SUFWUTtRQUFSLEtBQUssRUFBRTttREFBWTtJQUNWO1FBQVQsTUFBTSxFQUFFO3lEQUEwQztJQUgxQyxnQkFBZ0I7UUFONUIsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGNBQWM7WUFDeEIsd1BBQXdDOztTQUUzQyxDQUFDO09BRVcsZ0JBQWdCLENBYzVCO0lBQUQsdUJBQUM7Q0FBQSxBQWRELElBY0M7U0FkWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWFwcC1tZW51JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hcHAtbWVudS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hcHAtbWVudS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgbWVudXM6IGFueTtcclxuICAgIEBPdXRwdXQoKSBtZW51Q2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXI8b2JqZWN0PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgb25NZW51Q2xpY2sobWVudSkge1xyXG4gICAgICAgIHRoaXMubWVudUNsaWNrZWQuZW1pdChtZW51LkxPQ0FUSU9OKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgIH1cclxuXHJcbn1cclxuIl19