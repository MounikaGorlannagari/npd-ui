import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NotificationComponent } from './notification.component';
import { NotificationTypes } from './object/NotificationTypes';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
var NotificationService = /** @class */ (function () {
    function NotificationService(snackBar) {
        var _this = this;
        this.snackBar = snackBar;
        this.enableAutoHide = true;
        this.autoHideDuration = 5000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        this.defaultClass = 'mpm-notification';
        this.info = function (message) {
            if (!message) {
                return;
            }
            var config = new MatSnackBarConfig();
            config.verticalPosition = _this.verticalPosition;
            config.horizontalPosition = _this.horizontalPosition;
            config.panelClass = ['info-notification', _this.defaultClass];
            config.data = {
                message: message,
                type: NotificationTypes.INFO
            };
            if (_this.enableAutoHide) {
                config.duration = _this.autoHideDuration;
            }
            _this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.success = function (message) {
            if (!message) {
                return;
            }
            var config = new MatSnackBarConfig();
            config.verticalPosition = _this.verticalPosition;
            config.horizontalPosition = _this.horizontalPosition;
            config.panelClass = ['success-notification', _this.defaultClass];
            config.data = {
                message: message,
                type: NotificationTypes.SUCCESS
            };
            if (_this.enableAutoHide) {
                config.duration = _this.autoHideDuration;
            }
            _this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.warn = function (message) {
            if (!message) {
                return;
            }
            var config = new MatSnackBarConfig();
            config.verticalPosition = _this.verticalPosition;
            config.horizontalPosition = _this.horizontalPosition;
            config.panelClass = ['warn-notification', _this.defaultClass];
            config.data = {
                message: message,
                type: NotificationTypes.WARN
            };
            if (_this.enableAutoHide) {
                config.duration = _this.autoHideDuration;
            }
            _this.snackBar.openFromComponent(NotificationComponent, config);
        };
        this.error = function (message) {
            if (!message) {
                return;
            }
            var config = new MatSnackBarConfig();
            config.verticalPosition = _this.verticalPosition;
            config.horizontalPosition = _this.horizontalPosition;
            config.panelClass = ['error-notification', _this.defaultClass];
            config.data = {
                message: message,
                type: NotificationTypes.ERROR
            };
            if (_this.enableAutoHide) {
                config.duration = _this.autoHideDuration;
            }
            _this.snackBar.openFromComponent(NotificationComponent, config);
        };
    }
    NotificationService.ctorParameters = function () { return [
        { type: MatSnackBar }
    ]; };
    NotificationService.ɵprov = i0.ɵɵdefineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.ɵɵinject(i1.MatSnackBar)); }, token: NotificationService, providedIn: "root" });
    NotificationService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], NotificationService);
    return NotificationService;
}());
export { NotificationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSw2QkFBNkIsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3pJLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7QUFNL0Q7SUFFSSw2QkFDVyxRQUFxQjtRQURoQyxpQkFFSztRQURNLGFBQVEsR0FBUixRQUFRLENBQWE7UUFHaEMsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLHVCQUFrQixHQUFrQyxRQUFRLENBQUM7UUFDN0QscUJBQWdCLEdBQWdDLFFBQVEsQ0FBQztRQUN6RCxpQkFBWSxHQUFHLGtCQUFrQixDQUFDO1FBRWxDLFNBQUksR0FBRyxVQUFDLE9BQWU7WUFDbkIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPO2FBQ1Y7WUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3BELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0QsTUFBTSxDQUFDLElBQUksR0FBRztnQkFDVixPQUFPLFNBQUE7Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDL0IsQ0FBQztZQUNGLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtRQUVELFlBQU8sR0FBRyxVQUFDLE9BQWU7WUFDdEIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPO2FBQ1Y7WUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3BELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDaEUsTUFBTSxDQUFDLElBQUksR0FBRztnQkFDVixPQUFPLFNBQUE7Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLE9BQU87YUFDbEMsQ0FBQztZQUNGLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtRQUVELFNBQUksR0FBRyxVQUFDLE9BQWU7WUFDbkIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPO2FBQ1Y7WUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3BELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0QsTUFBTSxDQUFDLElBQUksR0FBRztnQkFDVixPQUFPLFNBQUE7Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDL0IsQ0FBQztZQUNGLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtRQUVELFVBQUssR0FBRyxVQUFDLE9BQWU7WUFDcEIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPO2FBQ1Y7WUFDRCxJQUFNLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDdkMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNoRCxNQUFNLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3BELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDOUQsTUFBTSxDQUFDLElBQUksR0FBRztnQkFDVixPQUFPLFNBQUE7Z0JBQ1AsSUFBSSxFQUFFLGlCQUFpQixDQUFDLEtBQUs7YUFDaEMsQ0FBQztZQUNGLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsTUFBTSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUM7YUFDM0M7WUFFRCxLQUFJLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25FLENBQUMsQ0FBQTtJQWxGRyxDQUFDOztnQkFEZ0IsV0FBVzs7O0lBSHZCLG1CQUFtQjtRQUovQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsbUJBQW1CLENBdUYvQjs4QkFoR0Q7Q0FnR0MsQUF2RkQsSUF1RkM7U0F2RlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0U25hY2tCYXJDb25maWcsIE1hdFNuYWNrQmFySG9yaXpvbnRhbFBvc2l0aW9uLCBNYXRTbmFja0JhclZlcnRpY2FsUG9zaXRpb24gfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25UeXBlcyB9IGZyb20gJy4vb2JqZWN0L05vdGlmaWNhdGlvblR5cGVzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvblNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBzbmFja0JhcjogTWF0U25hY2tCYXJcclxuICAgICkgeyB9XHJcblxyXG4gICAgZW5hYmxlQXV0b0hpZGUgPSB0cnVlO1xyXG4gICAgYXV0b0hpZGVEdXJhdGlvbiA9IDUwMDA7XHJcbiAgICBob3Jpem9udGFsUG9zaXRpb246IE1hdFNuYWNrQmFySG9yaXpvbnRhbFBvc2l0aW9uID0gJ2NlbnRlcic7XHJcbiAgICB2ZXJ0aWNhbFBvc2l0aW9uOiBNYXRTbmFja0JhclZlcnRpY2FsUG9zaXRpb24gPSAnYm90dG9tJztcclxuICAgIGRlZmF1bHRDbGFzcyA9ICdtcG0tbm90aWZpY2F0aW9uJztcclxuXHJcbiAgICBpbmZvID0gKG1lc3NhZ2U6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGlmICghbWVzc2FnZSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNvbmZpZyA9IG5ldyBNYXRTbmFja0JhckNvbmZpZygpO1xyXG4gICAgICAgIGNvbmZpZy52ZXJ0aWNhbFBvc2l0aW9uID0gdGhpcy52ZXJ0aWNhbFBvc2l0aW9uO1xyXG4gICAgICAgIGNvbmZpZy5ob3Jpem9udGFsUG9zaXRpb24gPSB0aGlzLmhvcml6b250YWxQb3NpdGlvbjtcclxuICAgICAgICBjb25maWcucGFuZWxDbGFzcyA9IFsnaW5mby1ub3RpZmljYXRpb24nLCB0aGlzLmRlZmF1bHRDbGFzc107XHJcbiAgICAgICAgY29uZmlnLmRhdGEgPSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIHR5cGU6IE5vdGlmaWNhdGlvblR5cGVzLklORk9cclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmICh0aGlzLmVuYWJsZUF1dG9IaWRlKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZy5kdXJhdGlvbiA9IHRoaXMuYXV0b0hpZGVEdXJhdGlvbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc25hY2tCYXIub3BlbkZyb21Db21wb25lbnQoTm90aWZpY2F0aW9uQ29tcG9uZW50LCBjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHN1Y2Nlc3MgPSAobWVzc2FnZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgaWYgKCFtZXNzYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29uZmlnID0gbmV3IE1hdFNuYWNrQmFyQ29uZmlnKCk7XHJcbiAgICAgICAgY29uZmlnLnZlcnRpY2FsUG9zaXRpb24gPSB0aGlzLnZlcnRpY2FsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLmhvcml6b250YWxQb3NpdGlvbiA9IHRoaXMuaG9yaXpvbnRhbFBvc2l0aW9uO1xyXG4gICAgICAgIGNvbmZpZy5wYW5lbENsYXNzID0gWydzdWNjZXNzLW5vdGlmaWNhdGlvbicsIHRoaXMuZGVmYXVsdENsYXNzXTtcclxuICAgICAgICBjb25maWcuZGF0YSA9IHtcclxuICAgICAgICAgICAgbWVzc2FnZSxcclxuICAgICAgICAgICAgdHlwZTogTm90aWZpY2F0aW9uVHlwZXMuU1VDQ0VTU1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlQXV0b0hpZGUpIHtcclxuICAgICAgICAgICAgY29uZmlnLmR1cmF0aW9uID0gdGhpcy5hdXRvSGlkZUR1cmF0aW9uO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zbmFja0Jhci5vcGVuRnJvbUNvbXBvbmVudChOb3RpZmljYXRpb25Db21wb25lbnQsIGNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgd2FybiA9IChtZXNzYWdlOiBzdHJpbmcpID0+IHtcclxuICAgICAgICBpZiAoIW1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBjb25maWcgPSBuZXcgTWF0U25hY2tCYXJDb25maWcoKTtcclxuICAgICAgICBjb25maWcudmVydGljYWxQb3NpdGlvbiA9IHRoaXMudmVydGljYWxQb3NpdGlvbjtcclxuICAgICAgICBjb25maWcuaG9yaXpvbnRhbFBvc2l0aW9uID0gdGhpcy5ob3Jpem9udGFsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLnBhbmVsQ2xhc3MgPSBbJ3dhcm4tbm90aWZpY2F0aW9uJywgdGhpcy5kZWZhdWx0Q2xhc3NdO1xyXG4gICAgICAgIGNvbmZpZy5kYXRhID0ge1xyXG4gICAgICAgICAgICBtZXNzYWdlLFxyXG4gICAgICAgICAgICB0eXBlOiBOb3RpZmljYXRpb25UeXBlcy5XQVJOXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVBdXRvSGlkZSkge1xyXG4gICAgICAgICAgICBjb25maWcuZHVyYXRpb24gPSB0aGlzLmF1dG9IaWRlRHVyYXRpb247XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNuYWNrQmFyLm9wZW5Gcm9tQ29tcG9uZW50KE5vdGlmaWNhdGlvbkNvbXBvbmVudCwgY29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBlcnJvciA9IChtZXNzYWdlOiBzdHJpbmcpID0+IHtcclxuICAgICAgICBpZiAoIW1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBjb25maWcgPSBuZXcgTWF0U25hY2tCYXJDb25maWcoKTtcclxuICAgICAgICBjb25maWcudmVydGljYWxQb3NpdGlvbiA9IHRoaXMudmVydGljYWxQb3NpdGlvbjtcclxuICAgICAgICBjb25maWcuaG9yaXpvbnRhbFBvc2l0aW9uID0gdGhpcy5ob3Jpem9udGFsUG9zaXRpb247XHJcbiAgICAgICAgY29uZmlnLnBhbmVsQ2xhc3MgPSBbJ2Vycm9yLW5vdGlmaWNhdGlvbicsIHRoaXMuZGVmYXVsdENsYXNzXTtcclxuICAgICAgICBjb25maWcuZGF0YSA9IHtcclxuICAgICAgICAgICAgbWVzc2FnZSxcclxuICAgICAgICAgICAgdHlwZTogTm90aWZpY2F0aW9uVHlwZXMuRVJST1JcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmICh0aGlzLmVuYWJsZUF1dG9IaWRlKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZy5kdXJhdGlvbiA9IHRoaXMuYXV0b0hpZGVEdXJhdGlvbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc25hY2tCYXIub3BlbkZyb21Db21wb25lbnQoTm90aWZpY2F0aW9uQ29tcG9uZW50LCBjb25maWcpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==