import { __decorate, __values } from "tslib";
import { Injectable } from '@angular/core';
import { GloabalConfig as config } from '../../mpm-utils/config/config';
import { EntityAppDefService } from '../../mpm-utils/services/entity.appdef.service';
import { UtilService } from '../../mpm-utils/services/util.service';
import { AuthenticationService } from '../../mpm-utils/services/authentication.service';
import { Observable, timer } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoaderService } from '../../loader/loader.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { switchMap } from 'rxjs/operators';
import { OTMMSessionHandler } from '../otmmSessionHandler';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { MPM_MENU_TYPES } from '../../mpm-utils/objects/MenuTypes';
import { OTDSTicketService } from '../../mpm-utils/services/OTDSTicket.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/entity.appdef.service";
import * as i2 from "../../mpm-utils/services/util.service";
import * as i3 from "../../mpm-utils/services/authentication.service";
import * as i4 from "../../loader/loader.service";
import * as i5 from "../../mpm-utils/services/otmm.service";
import * as i6 from "../../login/services/app.route.service";
import * as i7 from "../../mpm-utils/services/app.service";
import * as i8 from "../../mpm-utils/services/sharing.service";
import * as i9 from "../../mpm-utils/services/OTDSTicket.service";
import * as i10 from "@angular/router";
var AuthGuard = /** @class */ (function () {
    function AuthGuard(entityAppDefService, utilServices, authenticationService, loaderService, otmmService, appRouteService, appService, sharingService, otdsTicketService, router) {
        this.entityAppDefService = entityAppDefService;
        this.utilServices = utilServices;
        this.authenticationService = authenticationService;
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.appRouteService = appRouteService;
        this.appService = appService;
        this.sharingService = sharingService;
        this.otdsTicketService = otdsTicketService;
        this.router = router;
        this.otmmSessionSubscription = OTMMSessionHandler.subscribe();
        this.isDataAlradyLoaded = false;
        this.appItems = [];
        this.MENU_HEADER_TYPE = 'HEADER';
        this.MENU_TYPE = 'GENERAL';
        this.iconToAccessMPM = false;
        this.rolesForUser = [];
        this.APP_DEF_PATH = '';
    }
    AuthGuard.prototype.clearLocalStorage = function () {
        window.localStorage.removeItem('MPM_V3_LAST_LINK');
    };
    AuthGuard.prototype.goToLogin = function () {
        this.appRouteService.goToLogin();
    };
    AuthGuard.prototype.loadApp = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.rolesForUser = _this.utilServices.getUserRoles();
            _this.appService.getAllApplicationDetails()
                .subscribe(function (response) {
                var e_1, _a, e_2, _b, e_3, _c;
                console.log(response);
                /* sessionStorage.setItem('ALL_APPLICATION_DETAILS', response); */
                _this.allApplicationItems = acronui.findObjectsByProp(response, 'MPM_Application_Definition');
                console.log(_this.allApplicationItems);
                _this.sharingService.setCampaignRole(acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Campaign Manager'));
                _this.sharingService.setReviewerRole(acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Reviewer'));
                var reviewerRole = _this.sharingService.getReviewerRole();
                var reviewer = (reviewerRole && reviewerRole.length && reviewerRole.length > 0) ? true : false;
                _this.sharingService.setIsReviewer(reviewer);
                _this.sharingService.setApproverRole(acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Approver'));
                var approverRole = _this.sharingService.getApproverRole();
                var approver = (approverRole && approverRole.length && approverRole.length > 0) ? true : false;
                _this.sharingService.setIsApprover(approver);
                _this.sharingService.setMemberRole(acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Member'));
                var memberRole = _this.sharingService.getMemberRole();
                var member = (memberRole && memberRole.length && memberRole.length > 0) ? true : false;
                _this.sharingService.setIsMember(member);
                _this.sharingService.setManagerRole(acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Project Manager'));
                var managerRole = _this.sharingService.getManagerRole();
                var manager = (managerRole && managerRole.length && managerRole.length > 0) ? true : false;
                _this.sharingService.setIsManager(manager);
                /*                     this.sharingService.setCampaignManagerRole(acronui.findObjects(this.rolesForUser, 'Name', 'MPM Campaign Manager'));
                                    const campaignManagerRole = this.sharingService.getCampaignManagerRole();
                                    const campaignManager = (campaignManagerRole && campaignManagerRole.length && campaignManagerRole.length > 0) ? true : false;
                                    this.sharingService.setIsCampaignManager(campaignManager); */ // MVSS-128
                if (_this.allApplicationItems.length === 0) {
                    console.error('No application configuration available.');
                    observer.next('NOACCESS');
                    observer.complete();
                    return;
                }
                _this.appService.getDefaultEmailConfig().subscribe(function (emailResponse) {
                    _this.sharingService.setDefaultEmailConfig(emailResponse ? emailResponse : null);
                });
                _this.appService.getProjectConfigById(_this.allApplicationItems[0].R_PO_PROJECT_CONFIG['MPM_Project_Config-id'].Id).subscribe(function (projectConfigResponse) {
                    _this.sharingService.setProjectConfig(projectConfigResponse.MPM_Project_Config);
                });
                _this.appService.getAppConfigById(_this.allApplicationItems[0].R_PO_APP_CONFIG['MPM_App_Config-id'].Id).subscribe(function (appConfigResponse) {
                    _this.sharingService.setAppConfig(appConfigResponse.MPM_App_Config);
                });
                _this.appService.getAssetConfigById(_this.allApplicationItems[0].R_PO_ASSET_CONFIG['MPM_Asset_Config-id'].Id).subscribe(function (assetConfigResponse) {
                    _this.sharingService.setAssetConfig(assetConfigResponse.MPM_Asset_Config);
                });
                _this.appService.getCommentConfigById(_this.allApplicationItems[0].R_PO_COMMENTS_CONFIG['MPM_Comments_Config-id'].Id).subscribe(function (commentConfigResponse) {
                    _this.sharingService.setCommentConfig(commentConfigResponse.MPM_Comments_Config);
                });
                _this.appService.getBrandConfigById(_this.allApplicationItems[0].R_PO_BRAND_CONFIG['MPM_Brand_Config-id'].Id).subscribe(function (brandConfigResponse) {
                    _this.sharingService.setBrandConfig(brandConfigResponse.MPM_Brand_Config);
                });
                _this.appService.getAllViewConfig().subscribe(function (viewConfigResponse) {
                    _this.sharingService.setAllViewConfig(viewConfigResponse.MPM_View_Config);
                });
                _this.appService.getAllTeamRoles().subscribe(function (allTeamRolesResponse) {
                    _this.sharingService.setAllTeamRoles(allTeamRolesResponse.MPM_Team_Role_Mapping);
                });
                _this.appService.getAllTeams().subscribe(function (allTeamsResponse) {
                    _this.sharingService.setAllTeams(allTeamsResponse.GetAllTeamsResponse.MPM_Teams);
                });
                // this.appService.getAllPersons().subscribe(allPersonsResponse => {
                //     this.sharingService.setAllPersons(allPersonsResponse.Person);
                // });
                _this.appService.getPersonByIdentityUserId({
                    userId: _this.sharingService.getcurrentUserItemID()
                }).subscribe(function (personResponse) {
                    _this.sharingService.setCurrentPerson(personResponse);
                });
                _this.appService.getCurrentUserPreference().subscribe(function (userPreferenceResponse) {
                    _this.sharingService.setCurrentUserPreference(userPreferenceResponse);
                });
                if (_this.allApplicationItems.length === 0) {
                    console.error('No application configuration available.');
                    observer.next(false);
                    observer.complete();
                    return;
                }
                try {
                    for (var _d = __values(_this.rolesForUser), _e = _d.next(); !_e.done; _e = _d.next()) {
                        var role = _e.value;
                        try {
                            for (var _f = (e_2 = void 0, __values(_this.allApplicationItems)), _g = _f.next(); !_g.done; _g = _f.next()) {
                                var appItem = _g.value;
                                if (appItem.R_PO_ACCESS_ROLE &&
                                    appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'] &&
                                    appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'].Id &&
                                    role.Id === _this.utilServices.GetRoleDNByRoleId(appItem.R_PO_ACCESS_ROLE['MPM_APP_Roles-id'].Id).ROLE_DN) {
                                    _this.appItems.push(appItem);
                                }
                            }
                        }
                        catch (e_2_1) { e_2 = { error: e_2_1 }; }
                        finally {
                            try {
                                if (_g && !_g.done && (_b = _f.return)) _b.call(_f);
                            }
                            finally { if (e_2) throw e_2.error; }
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_e && !_e.done && (_a = _d.return)) _a.call(_d);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                if (_this.appItems.length > 0) {
                    var selectedAppItem = void 0;
                    var appId = _this.utilServices.getQueryParameterFromURL('applicationId', document.URL);
                    if (appId && appId !== '') {
                        try {
                            for (var _h = __values(_this.appItems), _j = _h.next(); !_j.done; _j = _h.next()) {
                                var appItem = _j.value;
                                if (appItem.APPLICATION_ID === appId) {
                                    selectedAppItem = appItem;
                                    break;
                                }
                            }
                        }
                        catch (e_3_1) { e_3 = { error: e_3_1 }; }
                        finally {
                            try {
                                if (_j && !_j.done && (_c = _h.return)) _c.call(_h);
                            }
                            finally { if (e_3) throw e_3.error; }
                        }
                    }
                    else {
                        selectedAppItem = _this.appItems[0];
                    }
                    if (!selectedAppItem) {
                        observer.next(false);
                        observer.complete();
                        console.warn('No access to the application.');
                        return;
                    }
                    _this.utilServices.setSelectedApp(selectedAppItem)
                        .subscribe(function (data) {
                        _this.utilServices.setConfigsForApp()
                            .subscribe(function (configsResponse) {
                            var userRoleWithCN = acronui.findObjects(_this.rolesForUser, 'Name', 'MPM Application Access');
                            _this.menus = _this.sharingService.getMenuByType(MPM_MENU_TYPES.HEADER);
                            if (userRoleWithCN.length === 1 && _this.entityAppDefService.hasRole(userRoleWithCN[0].Id) && _this.menus.length > 0) {
                                observer.next(true);
                                observer.complete();
                            }
                            else {
                                observer.error('NOACCESS');
                            }
                        }, function (configsError) {
                            observer.error('Something went wrong while getting application config details.');
                        });
                    }, function (error) {
                        observer.error('Something went wrong while setting application details.');
                    });
                }
                else {
                    observer.next(false);
                    observer.complete();
                    console.warn('No Access to the Application.');
                    return;
                }
            }, function (error) {
                observer.error('Something went wrong while getting application details.');
            });
        });
    };
    AuthGuard.prototype.initLogging = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.authenticationService.logIn().subscribe(function (success) {
                if (success === true) {
                    _this.utilServices.setUserInfo()
                        .subscribe(function (response) {
                        if (response) {
                            _this.loadApp()
                                .subscribe(function (loadAppResponse) {
                                if (loadAppResponse == true) {
                                    observer.next(true);
                                    observer.complete();
                                }
                                else if (loadAppResponse == 'NOACCESS') {
                                    observer.next(loadAppResponse);
                                    observer.complete();
                                }
                                else {
                                    observer.next(false);
                                    observer.complete();
                                }
                            }, function (error) {
                                _this.loaderService.hide();
                                if (error == 'NOACCESS') {
                                    observer.next(error);
                                    observer.complete();
                                }
                                observer.error(new Error(error));
                                observer.complete();
                            });
                        }
                    }, function (error) {
                        console.log(error);
                        if (error == 'NOACCESS') {
                            observer.next(error);
                            observer.complete();
                        }
                        observer.next(false);
                        observer.complete();
                    });
                }
                else {
                    observer.error(new Error('Something went wrong while authenticating.'));
                }
            }, function (error) {
                _this.loaderService.hide();
                observer.error(new Error(error));
            });
        });
    };
    AuthGuard.prototype.storeLastVisitLink = function () {
        var currentUrl = window.location.toString();
        if (!currentUrl) {
            return localStorage.setItem('MPM_V3_LAST_LINK', '');
        }
        else if (currentUrl !== config.getOTDSLoginPageUrl() && currentUrl !== config.getPsOrgEndpoint()) {
            var splittedURL = currentUrl.split('/#/');
            if (Array.isArray(splittedURL) && splittedURL.length === 2 && splittedURL[1] !== '') {
                return localStorage.setItem('MPM_V3_LAST_LINK', currentUrl);
            }
        }
    };
    AuthGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return new Observable(function (observer) {
            _this.loaderService.show();
            _this.initLogging().subscribe(function (validatedResponse) {
                if (validatedResponse == true) {
                    _this.clearLocalStorage();
                    _this.otdsTicketService.setOTDSTicketForUser().subscribe(function (ticket) {
                        _this.otmmService.getOTMMSystemDetails()
                            .subscribe(function (sysDetailsResponse) {
                            // tslint:disable-next-line: max-line-length
                            var sessionTimeOut = sysDetailsResponse.system_details_resource.system_details_map.entry.find(function (entry) { return entry.key === 'HTTP_SESSION_TIME_OUT'; });
                            if (sessionTimeOut && sessionTimeOut.value) {
                                // 75% of the HTTP Session timeout
                                sessionTimeOut = Math.floor((Number(sessionTimeOut.value) * 0.75) * 1000);
                            }
                            else {
                                sessionTimeOut = (typeof _this.sharingService.getMediaManagerConfig().sessionTimeOut === 'string') ?
                                    Number(_this.sharingService.getMediaManagerConfig().sessionTimeOut) * 60000 :
                                    _this.otmmService.STANDARD_SESSION_TIMEOUT_INTERVAL;
                            }
                            _this.otmmSessionSubscription = timer(0, sessionTimeOut)
                                .pipe(switchMap(function () { return _this.otmmService.startOTMMSession(false); }) // MVS-239false
                            )
                                .subscribe(function (otmmSessionResponse) {
                                _this.loaderService.hide();
                                observer.next(true);
                                observer.complete();
                                console.log('OTMM Session is initialized.');
                            }, function (otmmSessionError) {
                                _this.loaderService.hide();
                                observer.next(true);
                                observer.complete();
                                console.error('Something went wrong while getting OTMM Session.');
                                _this.appRouteService.goToLogout();
                            });
                        }, function (sysDetailsError) {
                            _this.loaderService.hide();
                            observer.next(true);
                            observer.complete();
                            console.error('Something went wrong while getting OTMM system details.');
                        });
                    }, function (error) {
                        _this.loaderService.hide();
                        observer.next(true);
                        observer.complete();
                        console.error('Something went wrong while getting OTDS ticket.');
                    });
                }
                else if (validatedResponse == 'NOACCESS') {
                    _this.loaderService.hide();
                    _this.router.navigate(['/apps/mpm/noaccess']);
                    observer.next(true);
                    observer.complete();
                }
            }, function (error) {
                _this.storeLastVisitLink();
                _this.loaderService.hide();
                observer.next(false);
                observer.complete();
                _this.goToLogin();
            });
        });
    };
    AuthGuard.ctorParameters = function () { return [
        { type: EntityAppDefService },
        { type: UtilService },
        { type: AuthenticationService },
        { type: LoaderService },
        { type: OTMMService },
        { type: AppRouteService },
        { type: AppService },
        { type: SharingService },
        { type: OTDSTicketService },
        { type: Router }
    ]; };
    AuthGuard.ɵprov = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.EntityAppDefService), i0.ɵɵinject(i2.UtilService), i0.ɵɵinject(i3.AuthenticationService), i0.ɵɵinject(i4.LoaderService), i0.ɵɵinject(i5.OTMMService), i0.ɵɵinject(i6.AppRouteService), i0.ɵɵinject(i7.AppService), i0.ɵɵinject(i8.SharingService), i0.ɵɵinject(i9.OTDSTicketService), i0.ɵɵinject(i10.Router)); }, token: AuthGuard, providedIn: "root" });
    AuthGuard = __decorate([
        Injectable({
            providedIn: 'root',
        })
    ], AuthGuard);
    return AuthGuard;
}());
export { AuthGuard };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9hdXRoLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDeEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sS0FBSyxPQUFPLE1BQU0sOEJBQThCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUVuRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQzs7Ozs7Ozs7Ozs7O0FBT2hGO0lBSUksbUJBQ1csbUJBQXdDLEVBQ3hDLFlBQXlCLEVBQ3pCLHFCQUE0QyxFQUM1QyxhQUE0QixFQUM1QixXQUF3QixFQUN4QixlQUFnQyxFQUNoQyxVQUFzQixFQUN0QixjQUE4QixFQUM5QixpQkFBb0MsRUFDcEMsTUFBYztRQVRkLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsaUJBQVksR0FBWixZQUFZLENBQWE7UUFDekIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUM1QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBWmxCLDRCQUF1QixHQUFHLGtCQUFrQixDQUFDLFNBQVMsRUFBRSxDQUFDO1FBZWhFLHVCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMzQixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QscUJBQWdCLEdBQUcsUUFBUSxDQUFDO1FBQzVCLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFFdEIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFFeEIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsaUJBQVksR0FBRyxFQUFFLENBQUM7SUFWZCxDQUFDO0lBYUwscUNBQWlCLEdBQWpCO1FBQ0ksTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckMsQ0FBQztJQUVELDJCQUFPLEdBQVA7UUFBQSxpQkF5SkM7UUF4SkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3JELEtBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUU7aUJBQ3JDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7O2dCQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3RCLGtFQUFrRTtnQkFDbEUsS0FBSSxDQUFDLG1CQUFtQixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztnQkFDN0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFFdEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7Z0JBRTVHLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDcEcsSUFBTSxZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDM0QsSUFBTSxRQUFRLEdBQUcsQ0FBQyxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDcEcsSUFBTSxZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDM0QsSUFBTSxRQUFRLEdBQUcsQ0FBQyxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVDLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDaEcsSUFBTSxVQUFVLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDdkQsSUFBTSxNQUFNLEdBQUcsQ0FBQyxVQUFVLElBQUksVUFBVSxDQUFDLE1BQU0sSUFBSSxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDekYsS0FBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxNQUFNLEVBQUUscUJBQXFCLENBQUMsQ0FBQyxDQUFDO2dCQUMxRyxJQUFNLFdBQVcsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN6RCxJQUFNLE9BQU8sR0FBRyxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUM3RixLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUM7OztpR0FHaUYsQ0FBQyxXQUFXO2dCQUM3RixJQUFJLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN2QyxPQUFPLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7b0JBQ3pELFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzFCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDcEIsT0FBTztpQkFDVjtnQkFFRCxLQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsYUFBYTtvQkFDM0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BGLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEscUJBQXFCO29CQUM3SSxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLHFCQUFxQixDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ25GLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGlCQUFpQjtvQkFDN0gsS0FBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3ZFLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsbUJBQW1CO29CQUNySSxLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUM3RSxDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjtvQkFDL0ksS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNwRixDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLG1CQUFtQjtvQkFDckksS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDN0UsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsS0FBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLGtCQUFrQjtvQkFDM0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDN0UsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsS0FBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxvQkFBb0I7b0JBQzVELEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLENBQUM7Z0JBQ3BGLENBQUMsQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsZ0JBQWdCO29CQUNwRCxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDcEYsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsb0VBQW9FO2dCQUNwRSxvRUFBb0U7Z0JBQ3BFLE1BQU07Z0JBRU4sS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQztvQkFDdEMsTUFBTSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7aUJBQ3JELENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO29CQUN2QixLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN6RCxDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsVUFBVSxDQUFDLHdCQUF3QixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsc0JBQXNCO29CQUN2RSxLQUFJLENBQUMsY0FBYyxDQUFDLHdCQUF3QixDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3pFLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQ3ZDLE9BQU8sQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztvQkFDekQsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNwQixPQUFPO2lCQUNWOztvQkFDRCxLQUFtQixJQUFBLEtBQUEsU0FBQSxLQUFJLENBQUMsWUFBWSxDQUFBLGdCQUFBLDRCQUFFO3dCQUFqQyxJQUFNLElBQUksV0FBQTs7NEJBQ1gsS0FBc0IsSUFBQSxvQkFBQSxTQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQSxDQUFBLGdCQUFBLDRCQUFFO2dDQUEzQyxJQUFNLE9BQU8sV0FBQTtnQ0FDZCxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0I7b0NBQ3hCLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQztvQ0FDNUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRTtvQ0FDL0MsSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRTtvQ0FDMUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUNBQy9COzZCQUNKOzs7Ozs7Ozs7cUJBQ0o7Ozs7Ozs7OztnQkFDRCxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDMUIsSUFBSSxlQUFlLFNBQUEsQ0FBQztvQkFDcEIsSUFBTSxLQUFLLEdBQUcsS0FBSSxDQUFDLFlBQVksQ0FBQyx3QkFBd0IsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN4RixJQUFJLEtBQUssSUFBSSxLQUFLLEtBQUssRUFBRSxFQUFFOzs0QkFDdkIsS0FBc0IsSUFBQSxLQUFBLFNBQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQSxnQkFBQSw0QkFBRTtnQ0FBaEMsSUFBTSxPQUFPLFdBQUE7Z0NBQ2QsSUFBSSxPQUFPLENBQUMsY0FBYyxLQUFLLEtBQUssRUFBRTtvQ0FDbEMsZUFBZSxHQUFHLE9BQU8sQ0FBQztvQ0FDMUIsTUFBTTtpQ0FDVDs2QkFDSjs7Ozs7Ozs7O3FCQUNKO3lCQUFNO3dCQUNILGVBQWUsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUN0QztvQkFDRCxJQUFJLENBQUMsZUFBZSxFQUFFO3dCQUNsQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0JBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsQ0FBQzt3QkFDOUMsT0FBTztxQkFDVjtvQkFFRCxLQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUM7eUJBQzVDLFNBQVMsQ0FBQyxVQUFBLElBQUk7d0JBQ1gsS0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTs2QkFDL0IsU0FBUyxDQUFDLFVBQUEsZUFBZTs0QkFDdEIsSUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSx3QkFBd0IsQ0FBQyxDQUFDOzRCQUNoRyxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDdEUsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQ2hILFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs2QkFDdkI7aUNBQU07Z0NBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQzs2QkFDOUI7d0JBQ0wsQ0FBQyxFQUFFLFVBQUEsWUFBWTs0QkFDWCxRQUFRLENBQUMsS0FBSyxDQUFDLGdFQUFnRSxDQUFDLENBQUM7d0JBQ3JGLENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO29CQUM5RSxDQUFDLENBQUMsQ0FBQztpQkFDVjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3BCLE9BQU8sQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsQ0FBQztvQkFDOUMsT0FBTztpQkFDVjtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO1lBQzlFLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsK0JBQVcsR0FBWDtRQUFBLGlCQThDQztRQTdDRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsT0FBTztnQkFDaEQsSUFBSSxPQUFPLEtBQUssSUFBSSxFQUFFO29CQUNsQixLQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTt5QkFDMUIsU0FBUyxDQUFDLFVBQUEsUUFBUTt3QkFDZixJQUFJLFFBQVEsRUFBRTs0QkFDVixLQUFJLENBQUMsT0FBTyxFQUFFO2lDQUNULFNBQVMsQ0FBQyxVQUFBLGVBQWU7Z0NBQ3RCLElBQUksZUFBZSxJQUFJLElBQUksRUFBRTtvQ0FDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQ0FDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUN2QjtxQ0FBTSxJQUFJLGVBQWUsSUFBSSxVQUFVLEVBQUU7b0NBQ3RDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7b0NBQy9CLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDdkI7cUNBQU07b0NBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQ0FDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUN2Qjs0QkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dDQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLElBQUksS0FBSyxJQUFJLFVBQVUsRUFBRTtvQ0FDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQ0FDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUN2QjtnQ0FDRCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0NBQ2pDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs0QkFDeEIsQ0FBQyxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSzt3QkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNuQixJQUFJLEtBQUssSUFBSSxVQUFVLEVBQUU7NEJBQ3JCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzt5QkFDdkI7d0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLENBQUMsQ0FBQztpQkFDVjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUMsQ0FBQztpQkFDM0U7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyQyxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHNDQUFrQixHQUFsQjtRQUNJLElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNiLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUN2RDthQUFNLElBQUksVUFBVSxLQUFLLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLFVBQVUsS0FBSyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtZQUNoRyxJQUFNLFdBQVcsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNqRixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUM7SUFFRCwrQkFBVyxHQUFYLFVBQ0ksSUFBNEIsRUFDNUIsS0FBMEI7UUFGOUIsaUJBZ0VDO1FBNURHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLGlCQUFpQjtnQkFDMUMsSUFBSSxpQkFBaUIsSUFBSSxJQUFJLEVBQUU7b0JBQzNCLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29CQUN6QixLQUFJLENBQUMsaUJBQWlCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO3dCQUMxRCxLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFOzZCQUNsQyxTQUFTLENBQUMsVUFBQSxrQkFBa0I7NEJBQ3pCLDRDQUE0Qzs0QkFDNUMsSUFBSSxjQUFjLEdBQUcsa0JBQWtCLENBQUMsdUJBQXVCLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxHQUFHLEtBQUssdUJBQXVCLEVBQXJDLENBQXFDLENBQUMsQ0FBQzs0QkFDOUksSUFBSSxjQUFjLElBQUksY0FBYyxDQUFDLEtBQUssRUFBRTtnQ0FDeEMsa0NBQWtDO2dDQUNsQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7NkJBQzdFO2lDQUFNO2dDQUNILGNBQWMsR0FBRyxDQUFDLE9BQU8sS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLGNBQWMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29DQUMvRixNQUFNLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDO29DQUM1RSxLQUFJLENBQUMsV0FBVyxDQUFDLGlDQUFpQyxDQUFDOzZCQUMxRDs0QkFDRCxLQUFJLENBQUMsdUJBQXVCLEdBQUcsS0FBSyxDQUFDLENBQUMsRUFBRSxjQUFjLENBQUM7aUNBQ2xELElBQUksQ0FDRCxTQUFTLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEVBQXhDLENBQXdDLENBQUMsQ0FBQyxlQUFlOzZCQUM1RTtpQ0FDQSxTQUFTLENBQUMsVUFBQSxtQkFBbUI7Z0NBQzFCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQ0FDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDOzRCQUNoRCxDQUFDLEVBQUUsVUFBQSxnQkFBZ0I7Z0NBQ2YsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dDQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUM7Z0NBQ2xFLEtBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLENBQUM7NEJBQ3RDLENBQUMsQ0FBQyxDQUFDO3dCQUNYLENBQUMsRUFBRSxVQUFBLGVBQWU7NEJBQ2QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUM7d0JBQzdFLENBQUMsQ0FBQyxDQUFDO29CQUNYLENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUNwQixPQUFPLENBQUMsS0FBSyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7b0JBQ3JFLENBQUMsQ0FBQyxDQUFDO2lCQUNOO3FCQUFNLElBQUksaUJBQWlCLElBQUksVUFBVSxFQUFFO29CQUN4QyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztvQkFDN0MsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzFCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3JCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkF0VCtCLG1CQUFtQjtnQkFDMUIsV0FBVztnQkFDRixxQkFBcUI7Z0JBQzdCLGFBQWE7Z0JBQ2YsV0FBVztnQkFDUCxlQUFlO2dCQUNwQixVQUFVO2dCQUNOLGNBQWM7Z0JBQ1gsaUJBQWlCO2dCQUM1QixNQUFNOzs7SUFkaEIsU0FBUztRQUpyQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsU0FBUyxDQTRUckI7b0JBcFZEO0NBb1ZDLEFBNVRELElBNFRDO1NBNVRZLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgeyBFbnRpdHlBcHBEZWZTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGltZXIgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgT1RNTVNlc3Npb25IYW5kbGVyIH0gZnJvbSAnLi4vb3RtbVNlc3Npb25IYW5kbGVyJztcclxuaW1wb3J0IHsgQXBwUm91dGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9naW4vc2VydmljZXMvYXBwLnJvdXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTVBNX01FTlVfVFlQRVMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9NZW51VHlwZXMnO1xyXG5pbXBvcnQgeyBNUE1NZW51IH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTWVudSc7XHJcbmltcG9ydCB7IE9URFNUaWNrZXRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL09URFNUaWNrZXQuc2VydmljZSc7XHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCB7XHJcblxyXG4gICAgcHVibGljIG90bW1TZXNzaW9uU3Vic2NyaXB0aW9uID0gT1RNTVNlc3Npb25IYW5kbGVyLnN1YnNjcmliZSgpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBlbnRpdHlBcHBEZWZTZXJ2aWNlOiBFbnRpdHlBcHBEZWZTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyB1dGlsU2VydmljZXM6IFV0aWxTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBSb3V0ZVNlcnZpY2U6IEFwcFJvdXRlU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBvdGRzVGlja2V0U2VydmljZTogT1REU1RpY2tldFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHJvdXRlcjogUm91dGVyXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGlzRGF0YUFscmFkeUxvYWRlZCA9IGZhbHNlO1xyXG4gICAgYXBwSXRlbXMgPSBbXTtcclxuICAgIE1FTlVfSEVBREVSX1RZUEUgPSAnSEVBREVSJztcclxuICAgIE1FTlVfVFlQRSA9ICdHRU5FUkFMJztcclxuICAgIG1lbnVzOiBBcnJheTxNUE1NZW51PjtcclxuICAgIGljb25Ub0FjY2Vzc01QTSA9IGZhbHNlO1xyXG4gICAgYWxsQXBwbGljYXRpb25JdGVtcztcclxuICAgIHJvbGVzRm9yVXNlciA9IFtdO1xyXG4gICAgQVBQX0RFRl9QQVRIID0gJyc7XHJcbiAgICB1c2VyTmFtZTtcclxuXHJcbiAgICBjbGVhckxvY2FsU3RvcmFnZSgpIHtcclxuICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ01QTV9WM19MQVNUX0xJTksnKTtcclxuICAgIH1cclxuXHJcbiAgICBnb1RvTG9naW4oKSB7XHJcbiAgICAgICAgdGhpcy5hcHBSb3V0ZVNlcnZpY2UuZ29Ub0xvZ2luKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZEFwcCgpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnJvbGVzRm9yVXNlciA9IHRoaXMudXRpbFNlcnZpY2VzLmdldFVzZXJSb2xlcygpO1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25EZXRhaWxzKClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAvKiBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdBTExfQVBQTElDQVRJT05fREVUQUlMUycsIHJlc3BvbnNlKTsgKi9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX0FwcGxpY2F0aW9uX0RlZmluaXRpb24nKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldENhbXBhaWduUm9sZShhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQ2FtcGFpZ24gTWFuYWdlcicpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRSZXZpZXdlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIFJldmlld2VyJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJldmlld2VyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0UmV2aWV3ZXJSb2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmV2aWV3ZXIgPSAocmV2aWV3ZXJSb2xlICYmIHJldmlld2VyUm9sZS5sZW5ndGggJiYgcmV2aWV3ZXJSb2xlLmxlbmd0aCA+IDApID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0SXNSZXZpZXdlcihyZXZpZXdlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBcHByb3ZlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIEFwcHJvdmVyJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFwcHJvdmVyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QXBwcm92ZXJSb2xlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXBwcm92ZXIgPSAoYXBwcm92ZXJSb2xlICYmIGFwcHJvdmVyUm9sZS5sZW5ndGggJiYgYXBwcm92ZXJSb2xlLmxlbmd0aCA+IDApID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0SXNBcHByb3ZlcihhcHByb3Zlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRNZW1iZXJSb2xlKGFjcm9udWkuZmluZE9iamVjdHModGhpcy5yb2xlc0ZvclVzZXIsICdOYW1lJywgJ01QTSBNZW1iZXInKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVtYmVyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVtYmVyUm9sZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1lbWJlciA9IChtZW1iZXJSb2xlICYmIG1lbWJlclJvbGUubGVuZ3RoICYmIG1lbWJlclJvbGUubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRJc01lbWJlcihtZW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0TWFuYWdlclJvbGUoYWNyb251aS5maW5kT2JqZWN0cyh0aGlzLnJvbGVzRm9yVXNlciwgJ05hbWUnLCAnTVBNIFByb2plY3QgTWFuYWdlcicpKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtYW5hZ2VyUm9sZSA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWFuYWdlclJvbGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtYW5hZ2VyID0gKG1hbmFnZXJSb2xlICYmIG1hbmFnZXJSb2xlLmxlbmd0aCAmJiBtYW5hZ2VyUm9sZS5sZW5ndGggPiAwKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldElzTWFuYWdlcihtYW5hZ2VyKTtcclxuICAgICAgICAgICAgICAgICAgICAvKiAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q2FtcGFpZ25NYW5hZ2VyUm9sZShhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQ2FtcGFpZ24gTWFuYWdlcicpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduTWFuYWdlclJvbGUgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhbXBhaWduTWFuYWdlclJvbGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbXBhaWduTWFuYWdlciA9IChjYW1wYWlnbk1hbmFnZXJSb2xlICYmIGNhbXBhaWduTWFuYWdlclJvbGUubGVuZ3RoICYmIGNhbXBhaWduTWFuYWdlclJvbGUubGVuZ3RoID4gMCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldElzQ2FtcGFpZ25NYW5hZ2VyKGNhbXBhaWduTWFuYWdlcik7ICovIC8vIE1WU1MtMTI4XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYWxsQXBwbGljYXRpb25JdGVtcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignTm8gYXBwbGljYXRpb24gY29uZmlndXJhdGlvbiBhdmFpbGFibGUuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoJ05PQUNDRVNTJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXREZWZhdWx0RW1haWxDb25maWcoKS5zdWJzY3JpYmUoZW1haWxSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0RGVmYXVsdEVtYWlsQ29uZmlnKGVtYWlsUmVzcG9uc2UgPyBlbWFpbFJlc3BvbnNlIDogbnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQcm9qZWN0Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19QUk9KRUNUX0NPTkZJR1snTVBNX1Byb2plY3RfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShwcm9qZWN0Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFByb2plY3RDb25maWcocHJvamVjdENvbmZpZ1Jlc3BvbnNlLk1QTV9Qcm9qZWN0X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRBcHBDb25maWdCeUlkKHRoaXMuYWxsQXBwbGljYXRpb25JdGVtc1swXS5SX1BPX0FQUF9DT05GSUdbJ01QTV9BcHBfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShhcHBDb25maWdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QXBwQ29uZmlnKGFwcENvbmZpZ1Jlc3BvbnNlLk1QTV9BcHBfQ29uZmlnKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldEFzc2V0Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19BU1NFVF9DT05GSUdbJ01QTV9Bc3NldF9Db25maWctaWQnXS5JZCkuc3Vic2NyaWJlKGFzc2V0Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFzc2V0Q29uZmlnKGFzc2V0Q29uZmlnUmVzcG9uc2UuTVBNX0Fzc2V0X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRDb21tZW50Q29uZmlnQnlJZCh0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXNbMF0uUl9QT19DT01NRU5UU19DT05GSUdbJ01QTV9Db21tZW50c19Db25maWctaWQnXS5JZCkuc3Vic2NyaWJlKGNvbW1lbnRDb25maWdSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q29tbWVudENvbmZpZyhjb21tZW50Q29uZmlnUmVzcG9uc2UuTVBNX0NvbW1lbnRzX0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRCcmFuZENvbmZpZ0J5SWQodGhpcy5hbGxBcHBsaWNhdGlvbkl0ZW1zWzBdLlJfUE9fQlJBTkRfQ09ORklHWydNUE1fQnJhbmRfQ29uZmlnLWlkJ10uSWQpLnN1YnNjcmliZShicmFuZENvbmZpZ1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRCcmFuZENvbmZpZyhicmFuZENvbmZpZ1Jlc3BvbnNlLk1QTV9CcmFuZF9Db25maWcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVmlld0NvbmZpZygpLnN1YnNjcmliZSh2aWV3Q29uZmlnUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFZpZXdDb25maWcodmlld0NvbmZpZ1Jlc3BvbnNlLk1QTV9WaWV3X0NvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRBbGxUZWFtUm9sZXMoKS5zdWJzY3JpYmUoYWxsVGVhbVJvbGVzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbFRlYW1Sb2xlcyhhbGxUZWFtUm9sZXNSZXNwb25zZS5NUE1fVGVhbV9Sb2xlX01hcHBpbmcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsVGVhbXMoKS5zdWJzY3JpYmUoYWxsVGVhbXNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsVGVhbXMoYWxsVGVhbXNSZXNwb25zZS5HZXRBbGxUZWFtc1Jlc3BvbnNlLk1QTV9UZWFtcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoaXMuYXBwU2VydmljZS5nZXRBbGxQZXJzb25zKCkuc3Vic2NyaWJlKGFsbFBlcnNvbnNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsUGVyc29ucyhhbGxQZXJzb25zUmVzcG9uc2UuUGVyc29uKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5SWRlbnRpdHlVc2VySWQoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFBlcnNvbihwZXJzb25SZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRDdXJyZW50VXNlclByZWZlcmVuY2UoKS5zdWJzY3JpYmUodXNlclByZWZlcmVuY2VSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlKHVzZXJQcmVmZXJlbmNlUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5hbGxBcHBsaWNhdGlvbkl0ZW1zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdObyBhcHBsaWNhdGlvbiBjb25maWd1cmF0aW9uIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCByb2xlIG9mIHRoaXMucm9sZXNGb3JVc2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgYXBwSXRlbSBvZiB0aGlzLmFsbEFwcGxpY2F0aW9uSXRlbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHBJdGVtLlJfUE9fQUNDRVNTX1JPTEUgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBJdGVtLlJfUE9fQUNDRVNTX1JPTEVbJ01QTV9BUFBfUm9sZXMtaWQnXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFwcEl0ZW0uUl9QT19BQ0NFU1NfUk9MRVsnTVBNX0FQUF9Sb2xlcy1pZCddLklkICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm9sZS5JZCA9PT0gdGhpcy51dGlsU2VydmljZXMuR2V0Um9sZUROQnlSb2xlSWQoYXBwSXRlbS5SX1BPX0FDQ0VTU19ST0xFWydNUE1fQVBQX1JvbGVzLWlkJ10uSWQpLlJPTEVfRE4pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcEl0ZW1zLnB1c2goYXBwSXRlbSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYXBwSXRlbXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRBcHBJdGVtO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhcHBJZCA9IHRoaXMudXRpbFNlcnZpY2VzLmdldFF1ZXJ5UGFyYW1ldGVyRnJvbVVSTCgnYXBwbGljYXRpb25JZCcsIGRvY3VtZW50LlVSTCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChhcHBJZCAmJiBhcHBJZCAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgYXBwSXRlbSBvZiB0aGlzLmFwcEl0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcEl0ZW0uQVBQTElDQVRJT05fSUQgPT09IGFwcElkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQXBwSXRlbSA9IGFwcEl0ZW07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQXBwSXRlbSA9IHRoaXMuYXBwSXRlbXNbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzZWxlY3RlZEFwcEl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gYWNjZXNzIHRvIHRoZSBhcHBsaWNhdGlvbi4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZXMuc2V0U2VsZWN0ZWRBcHAoc2VsZWN0ZWRBcHBJdGVtKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlcy5zZXRDb25maWdzRm9yQXBwKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjb25maWdzUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlclJvbGVXaXRoQ04gPSBhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMucm9sZXNGb3JVc2VyLCAnTmFtZScsICdNUE0gQXBwbGljYXRpb24gQWNjZXNzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZW51QnlUeXBlKE1QTV9NRU5VX1RZUEVTLkhFQURFUik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXNlclJvbGVXaXRoQ04ubGVuZ3RoID09PSAxICYmIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5oYXNSb2xlKHVzZXJSb2xlV2l0aENOWzBdLklkKSAmJiB0aGlzLm1lbnVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdOT0FDQ0VTUycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBjb25maWdzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgYXBwbGljYXRpb24gY29uZmlnIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBzZXR0aW5nIGFwcGxpY2F0aW9uIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKCdObyBBY2Nlc3MgdG8gdGhlIEFwcGxpY2F0aW9uLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIGFwcGxpY2F0aW9uIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0TG9nZ2luZygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5sb2dJbigpLnN1YnNjcmliZShzdWNjZXNzID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChzdWNjZXNzID09PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51dGlsU2VydmljZXMuc2V0VXNlckluZm8oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZEFwcCgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobG9hZEFwcFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChsb2FkQXBwUmVzcG9uc2UgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobG9hZEFwcFJlc3BvbnNlID09ICdOT0FDQ0VTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGxvYWRBcHBSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnJvciA9PSAnTk9BQ0NFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlcnJvciA9PSAnTk9BQ0NFU1MnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgYXV0aGVudGljYXRpbmcuJykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlTGFzdFZpc2l0TGluaygpIHtcclxuICAgICAgICBjb25zdCBjdXJyZW50VXJsID0gd2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgaWYgKCFjdXJyZW50VXJsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnTVBNX1YzX0xBU1RfTElOSycsICcnKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGN1cnJlbnRVcmwgIT09IGNvbmZpZy5nZXRPVERTTG9naW5QYWdlVXJsKCkgJiYgY3VycmVudFVybCAhPT0gY29uZmlnLmdldFBzT3JnRW5kcG9pbnQoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBzcGxpdHRlZFVSTCA9IGN1cnJlbnRVcmwuc3BsaXQoJy8jLycpO1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzcGxpdHRlZFVSTCkgJiYgc3BsaXR0ZWRVUkwubGVuZ3RoID09PSAyICYmIHNwbGl0dGVkVVJMWzFdICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdNUE1fVjNfTEFTVF9MSU5LJywgY3VycmVudFVybCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGUoXHJcbiAgICAgICAgbmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdExvZ2dpbmcoKS5zdWJzY3JpYmUodmFsaWRhdGVkUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbGlkYXRlZFJlc3BvbnNlID09IHRydWUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsZWFyTG9jYWxTdG9yYWdlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vdGRzVGlja2V0U2VydmljZS5zZXRPVERTVGlja2V0Rm9yVXNlcigpLnN1YnNjcmliZSh0aWNrZXQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldE9UTU1TeXN0ZW1EZXRhaWxzKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoc3lzRGV0YWlsc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZXNzaW9uVGltZU91dCA9IHN5c0RldGFpbHNSZXNwb25zZS5zeXN0ZW1fZGV0YWlsc19yZXNvdXJjZS5zeXN0ZW1fZGV0YWlsc19tYXAuZW50cnkuZmluZChlbnRyeSA9PiBlbnRyeS5rZXkgPT09ICdIVFRQX1NFU1NJT05fVElNRV9PVVQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2Vzc2lvblRpbWVPdXQgJiYgc2Vzc2lvblRpbWVPdXQudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gNzUlIG9mIHRoZSBIVFRQIFNlc3Npb24gdGltZW91dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uVGltZU91dCA9IE1hdGguZmxvb3IoKE51bWJlcihzZXNzaW9uVGltZU91dC52YWx1ZSkgKiAwLjc1KSAqIDEwMDApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25UaW1lT3V0ID0gKHR5cGVvZiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnNlc3Npb25UaW1lT3V0ID09PSAnc3RyaW5nJykgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTnVtYmVyKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuc2Vzc2lvblRpbWVPdXQpICogNjAwMDAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5vdG1tU2VydmljZS5TVEFOREFSRF9TRVNTSU9OX1RJTUVPVVRfSU5URVJWQUw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlc3Npb25TdWJzY3JpcHRpb24gPSB0aW1lcigwLCBzZXNzaW9uVGltZU91dClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2hNYXAoKCkgPT4gdGhpcy5vdG1tU2VydmljZS5zdGFydE9UTU1TZXNzaW9uKGZhbHNlKSkgLy8gTVZTLTIzOWZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShvdG1tU2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdPVE1NIFNlc3Npb24gaXMgaW5pdGlhbGl6ZWQuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIG90bW1TZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBPVE1NIFNlc3Npb24uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFJvdXRlU2VydmljZS5nb1RvTG9nb3V0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgc3lzRGV0YWlsc0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIE9UTU0gc3lzdGVtIGRldGFpbHMuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgT1REUyB0aWNrZXQuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZhbGlkYXRlZFJlc3BvbnNlID09ICdOT0FDQ0VTUycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2FwcHMvbXBtL25vYWNjZXNzJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdG9yZUxhc3RWaXNpdExpbmsoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdvVG9Mb2dpbigpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=