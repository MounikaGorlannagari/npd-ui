import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { StatusService } from './status.service';
import * as i0 from "@angular/core";
import * as i1 from "./status.service";
import * as i2 from "../../mpm-utils/services/sharing.service";
var DataCacheService = /** @class */ (function () {
    function DataCacheService(statusService, sharingservice) {
        this.statusService = statusService;
        this.sharingservice = sharingservice;
        this.statusCache = {};
    }
    DataCacheService.prototype.getStatusbyLevel = function (level) {
        var _this = this;
        return new Observable(function (observer) {
            if (level && Array.isArray(_this.statusCache[level]) && _this.statusCache[level].length > 0) {
                observer.next(_this.statusCache[level]);
                observer.complete();
            }
            else {
                /* this.statusService.getStatusByCategoryLevelName(level).subscribe(statuesResponse => {
                    this.statusCache[level] = statuesResponse;
                    observer.next(this.statusCache[level]);
                    observer.complete();
                }); */
                var status_1 = _this.statusService.getAllStatusBycategoryName(level);
                _this.status = status_1 && status_1.length > 0 && Array.isArray(status_1) ? status_1[0] : status_1;
                _this.statusCache[level] = _this.status;
                observer.next(_this.statusCache[level]);
                observer.complete();
            }
        });
    };
    DataCacheService.ctorParameters = function () { return [
        { type: StatusService },
        { type: SharingService }
    ]; };
    DataCacheService.ɵprov = i0.ɵɵdefineInjectable({ factory: function DataCacheService_Factory() { return new DataCacheService(i0.ɵɵinject(i1.StatusService), i0.ɵɵinject(i2.SharingService)); }, token: DataCacheService, providedIn: "root" });
    DataCacheService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], DataCacheService);
    return DataCacheService;
}());
export { DataCacheService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jYWNoZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2RhdGEtY2FjaGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBR2xDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUUxRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7Ozs7QUFNakQ7SUFFSSwwQkFDWSxhQUE0QixFQUM1QixjQUE4QjtRQUQ5QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFHMUMsZ0JBQVcsR0FBZ0IsRUFBRSxDQUFDO0lBRjFCLENBQUM7SUFNTCwyQ0FBZ0IsR0FBaEIsVUFBaUIsS0FBZ0I7UUFBakMsaUJBa0JDO1FBakJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdkYsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSDs7OztzQkFJTTtnQkFDTixJQUFNLFFBQU0sR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLDBCQUEwQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwRSxLQUFJLENBQUMsTUFBTSxHQUFHLFFBQU0sSUFBSSxRQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQU0sQ0FBQztnQkFDeEYsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDO2dCQUN0QyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkExQjBCLGFBQWE7Z0JBQ1osY0FBYzs7O0lBSmpDLGdCQUFnQjtRQUo1QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsZ0JBQWdCLENBK0I1QjsyQkEzQ0Q7Q0EyQ0MsQUEvQkQsSUErQkM7U0EvQlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE1QTV9MRVZFTCB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgU3RhdHVzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvU3RhdHVzJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzQ2FjaGUgfSBmcm9tICcuLi9vYmplY3QvU3RhdHVzQ2FjaGUnO1xyXG5pbXBvcnQgeyBTdGF0dXNTZXJ2aWNlIH0gZnJvbSAnLi9zdGF0dXMuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhQ2FjaGVTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIHN0YXR1c1NlcnZpY2U6IFN0YXR1c1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBzaGFyaW5nc2VydmljZTogU2hhcmluZ1NlcnZpY2VcclxuICAgICkgeyB9XHJcblxyXG4gICAgc3RhdHVzQ2FjaGU6IFN0YXR1c0NhY2hlID0ge307XHJcbiAgICBjYWNoZWRMaXN0RGVwZW5kZW50RmlsdGVyOiBhbnk7XHJcbiAgICBzdGF0dXM7XHJcblxyXG4gICAgZ2V0U3RhdHVzYnlMZXZlbChsZXZlbDogTVBNX0xFVkVMKTogT2JzZXJ2YWJsZTxBcnJheTxTdGF0dXM+IHwgbnVsbD4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChsZXZlbCAmJiBBcnJheS5pc0FycmF5KHRoaXMuc3RhdHVzQ2FjaGVbbGV2ZWxdKSAmJiB0aGlzLnN0YXR1c0NhY2hlW2xldmVsXS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc3RhdHVzQ2FjaGVbbGV2ZWxdKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvKiB0aGlzLnN0YXR1c1NlcnZpY2UuZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsTmFtZShsZXZlbCkuc3Vic2NyaWJlKHN0YXR1ZXNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0dXNDYWNoZVtsZXZlbF0gPSBzdGF0dWVzUmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLnN0YXR1c0NhY2hlW2xldmVsXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzID0gdGhpcy5zdGF0dXNTZXJ2aWNlLmdldEFsbFN0YXR1c0J5Y2F0ZWdvcnlOYW1lKGxldmVsKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdHVzID0gc3RhdHVzICYmIHN0YXR1cy5sZW5ndGggPiAwICYmIEFycmF5LmlzQXJyYXkoc3RhdHVzKSA/IHN0YXR1c1swXSA6IHN0YXR1cztcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RhdHVzQ2FjaGVbbGV2ZWxdID0gdGhpcy5zdGF0dXM7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc3RhdHVzQ2FjaGVbbGV2ZWxdKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19