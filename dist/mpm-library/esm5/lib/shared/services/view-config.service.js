import { __decorate } from "tslib";
import { Observable, forkJoin } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { NotificationService } from '../../notification/notification.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { SessionStorageConstants } from '../constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/sharing.service";
import * as i3 from "../../notification/notification.service";
var ViewConfigService = /** @class */ (function () {
    function ViewConfigService(appService, sharingService, notificationService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.GET_ALL_MPM_VIEW_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_View_Config/operations';
        this.GET_ALL_MPM_VIEW_CONFIG = 'GetAllMPMViewConfig';
        this.GET_ALL_MPM_VIEW_CONFIG_BPM_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.GET_ALL_MPM_VIEW_CONFIG_BPM = 'GetViewConfigDetails';
    }
    ViewConfigService.prototype.GetAllMPMViewConfig = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.sharingService.getAllApplicationViewConfig()) {
                observer.next(_this.sharingService.getAllApplicationViewConfig());
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.GET_ALL_MPM_VIEW_CONFIG_NS, _this.GET_ALL_MPM_VIEW_CONFIG, {})
                    .subscribe(function (response) {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_View_Config'));
                    observer.complete();
                }, function (error) {
                    _this.notificationService.error('Something went wrong while getting MPM Field details');
                    observer.error(error);
                });
            }
        });
    };
    /*  GetAllMPMViewConfigBPM(categoryId: string): Observable<Array<ViewConfig>> {
       const param = {
         categoryId: categoryId,
         roleId: null
       };
       return new Observable(observer => {
         if (this.sharingService.getAllApplicationViewConfig()) {
           observer.next(this.sharingService.getAllApplicationViewConfig());
           observer.complete();
         } else {
           this.appService.invokeRequest(this.GET_ALL_MPM_VIEW_CONFIG_BPM_NS, this.GET_ALL_MPM_VIEW_CONFIG_BPM, param)
             .subscribe(response => {
               const dataSet: Array<ViewConfig> = this.formViewConfigInterface(acronui.findObjectsByProp(response, 'tuple'));
               observer.next(dataSet);
               observer.complete();
             }, error => {
               this.notificationService.error('Something went wrong while getting MPM Field details');
               observer.error(error);
             });
         }
       });
     } */
    ViewConfigService.prototype.GetAllMPMViewConfigBPM = function (categoryId) {
        var _this = this;
        var param = {
            categoryId: categoryId,
            roleId: null
        };
        return new Observable(function (observer) {
            if (_this.sharingService.getAllApplicationViewConfig()) {
                observer.next(_this.sharingService.getAllApplicationViewConfig());
                observer.complete();
            }
            else {
                if (sessionStorage.getItem(SessionStorageConstants.ALL_MPM_VIEW_CONFIG) !== null) {
                    observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_MPM_VIEW_CONFIG)));
                    observer.complete();
                }
                else {
                    _this.appService.invokeRequest(_this.GET_ALL_MPM_VIEW_CONFIG_BPM_NS, _this.GET_ALL_MPM_VIEW_CONFIG_BPM, param)
                        .subscribe(function (response) {
                        var dataSet = _this.formViewConfigInterface(acronui.findObjectsByProp(response, 'tuple'));
                        sessionStorage.setItem(SessionStorageConstants.ALL_MPM_VIEW_CONFIG, JSON.stringify(dataSet));
                        observer.next(dataSet);
                        observer.complete();
                    }, function (error) {
                        _this.notificationService.error('Something went wrong while getting MPM Field details');
                        observer.error(error);
                    });
                }
            }
        });
    };
    ViewConfigService.prototype.formViewConfigInterface = function (viewList) {
        var _this = this;
        var dataSet = [];
        if (viewList && Array.isArray(viewList) && viewList.length > 0) {
            viewList.forEach(function (tempViewTuple) {
                var tempView = tempViewTuple['old']['ViewDetails'];
                var currView = {
                    'MPM_View_Config-id': {
                        Id: tempView['id'],
                        ItemId: null,
                    },
                    VIEW: tempView['view'],
                    CARD_FIELD_ORDER: tempView['cardFieldOrder'],
                    LIST_FIELD_ORDER: tempView['listFieldOrder'],
                    R_PO_DEFAULT_SORT_FIELD: {
                        'MPM_Fields_Config-id': {
                            Id: tempView['sortedFieldId'],
                            ItemId: null,
                        }
                    },
                    R_PO_CATEGORY: {
                        'MPM_Category_Level-id': {
                            Id: tempView['categoryId'],
                            ItemId: null,
                        }
                    },
                    R_PM_LIST_VIEW_MPM_FIELDS: tempView['listFields'] && Array.isArray(tempView['listFields']['field']) ?
                        _this.formMPMFieldDetails(tempView['listFields']['field']) : tempView['listFields']['field'] ?
                        _this.formMPMFieldDetails([tempView['listFields']['field']]) : [],
                    R_PM_CARD_VIEW_MPM_FIELDS: tempView['cardFields'] && Array.isArray(tempView['cardFields']['field']) ?
                        _this.formMPMFieldDetails(tempView['cardFields']['field']) : tempView['cardFields']['field'] ?
                        _this.formMPMFieldDetails([tempView['cardFields']['field']]) : [],
                    ROLE_ID: tempView['roleId'],
                    R_PO_ADVANCED_SEARCH_CONFIG: tempView['advanceSearchConfigId']
                };
                dataSet.push(currView);
            });
        }
        return dataSet;
    };
    ViewConfigService.prototype.formMPMFieldDetails = function (fieldList) {
        return fieldList.map(function (data) {
            var mpmField = {
                'MPM_Fields_Config-id': {
                    Id: data['id'],
                    ItemId: null,
                },
                EDIT_TYPE: data['editType'],
                INDEXER_FIELD_ID: data['indexerFieldId'],
                SEARCHABLE: data['isSearchable'],
                MPM_FIELD_CONFIG_ID: data['fieldConfigId'],
                FIELD_LEVEL: data[''],
                SORTABLE: data['isSortable'],
                FACETABLE: data['isFacetable'],
                OTMM_FIELD_ID: data['otmmFieldId'],
                DATA_TYPE: data['dataType'],
                MAPPER_NAME: data['mapperName'],
                DISPLAY_NAME: data['displayName'],
                IS_CUSTOM_METADATA: data['isCustomFields'],
                IS_DOMAIN_VALUE: typeof data['isDomainValue'] === 'string' ? data['isDomainValue'] : 'false',
                DOMAIN_FIELD_ID: data['domainFieldId'] === 'string' ? data['domainFieldId'] : null,
                R_PO_MPM_CATEGORY_LEVEL: {
                    'MPM_Category_Level-id': {
                        Id: data['categoryLevelId'],
                        ItemId: null
                    }
                }
            };
            return mpmField;
        });
    };
    ViewConfigService.prototype.getviewConfigsByKeyValue = function (key, value) {
        if (this.sharingService.getAllApplicationViewConfig()) {
            if (key === 'ItemId' || key === 'Id') {
                return this.sharingService.getAllApplicationViewConfig().filter(function (data) { return data['MPM_View_Config-id'][key] === value; });
            }
            return this.sharingService.getAllApplicationViewConfig().filter(function (data) { return data[key] === value; });
        }
        return [];
    };
    ViewConfigService.prototype.getviewConfigByKeyValue = function (key, value) {
        var currField = this.getviewConfigsByKeyValue(key, value);
        if (currField && currField.length > 0) {
            return currField[0];
        }
        return null;
    };
    ViewConfigService.prototype.getAllDisplayFeildForViewConfig = function (ViewName) {
        var _this = this;
        return new Observable(function (observer) {
            var currView = _this.getviewConfigByKeyValue('VIEW', ViewName);
            if (currView && !(currView.R_PM_CARD_VIEW_MPM_FIELDS && currView.R_PM_LIST_VIEW_MPM_FIELDS)) {
                var param = {
                    'MPM_View_Config-id': {
                        '@xmlns:ns0': 'http://schemas/AcheronMPMCore/MPM_View_Config',
                        Id: currView['MPM_View_Config-id'].Id
                    }
                };
                forkJoin([
                    _this.appService.invokeRequest(_this.GET_ALL_MPM_VIEW_CONFIG_NS, 'GetR_PM_CARD_VIEW_MPM_FIELDS', param),
                    _this.appService.invokeRequest(_this.GET_ALL_MPM_VIEW_CONFIG_NS, 'GetR_PM_LIST_VIEW_MPM_FIELDS', param)
                ]).subscribe(function (responseList) {
                    currView.R_PM_CARD_VIEW_MPM_FIELDS = acronui.findObjectsByProp(responseList[0], 'MPM_Fields_Config');
                    currView.R_PM_LIST_VIEW_MPM_FIELDS = acronui.findObjectsByProp(responseList[1], 'MPM_Fields_Config');
                    _this.sharingService.setAllApplicationViewConfig(_this.sharingService.getAllApplicationViewConfig().map(function (data) {
                        if (data['MPM_View_Config-id'].Id === currView['MPM_View_Config-id'].Id) {
                            return currView;
                        }
                        return data;
                    }));
                    observer.next(currView);
                    observer.complete();
                });
            }
            else {
                if (currView) {
                    observer.next(currView);
                    observer.complete();
                }
                else {
                    observer.error('Error while getting the view config details.');
                    observer.complete();
                }
            }
        });
    };
    ViewConfigService.prototype.getAdvancedSearchConfigByView = function (viewName) {
        if (!viewName) {
            return;
        }
        var viewConfigs = this.sharingService.getAllApplicationViewConfig();
        if (!viewConfigs || !Array.isArray(viewConfigs)) {
            return;
        }
        var view = viewConfigs.find(function (item) {
            return item.VIEW === viewName;
        });
        if (view && view) {
        }
    };
    ViewConfigService.ctorParameters = function () { return [
        { type: AppService },
        { type: SharingService },
        { type: NotificationService }
    ]; };
    ViewConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ViewConfigService_Factory() { return new ViewConfigService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.NotificationService)); }, token: ViewConfigService, providedIn: "root" });
    ViewConfigService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ViewConfigService);
    return ViewConfigService;
}());
export { ViewConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1jb25maWcuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxLQUFLLE9BQU8sTUFBTSw4QkFBOEIsQ0FBQztBQUl4RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQzs7Ozs7QUFLakY7SUFFRSwyQkFDUyxVQUFzQixFQUN0QixjQUE4QixFQUM5QixtQkFBd0M7UUFGeEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUcxQywrQkFBMEIsR0FBRywwREFBMEQsQ0FBQztRQUN4Riw0QkFBdUIsR0FBRyxxQkFBcUIsQ0FBQztRQUVoRCxtQ0FBOEIsR0FBRywrQ0FBK0MsQ0FBQztRQUNqRixnQ0FBMkIsR0FBRyxzQkFBc0IsQ0FBQztJQU54RCxDQUFDO0lBUUwsK0NBQW1CLEdBQW5CO1FBQUEsaUJBZ0JDO1FBZkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixFQUFFLEVBQUU7Z0JBQ3JELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDLENBQUM7Z0JBQ2pFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMEJBQTBCLEVBQUUsS0FBSSxDQUFDLHVCQUF1QixFQUFFLEVBQUUsQ0FBQztxQkFDN0YsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDakIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLGlCQUFpQixDQUFDLENBQUMsQ0FBQztvQkFDdEUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsc0RBQXNELENBQUMsQ0FBQztvQkFDdkYsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7U0FxQks7SUFFSixrREFBc0IsR0FBdEIsVUFBdUIsVUFBa0I7UUFBekMsaUJBNEJDO1FBM0JDLElBQU0sS0FBSyxHQUFHO1lBQ1osVUFBVSxFQUFFLFVBQVU7WUFDdEIsTUFBTSxFQUFFLElBQUk7U0FDYixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixFQUFFLEVBQUU7Z0JBQ3JELFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDLENBQUM7Z0JBQ2pFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtpQkFBTTtnQkFDTCxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLENBQUMsS0FBSyxJQUFJLEVBQUU7b0JBQ2hGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMvRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3JCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyw4QkFBOEIsRUFBRSxLQUFJLENBQUMsMkJBQTJCLEVBQUUsS0FBSyxDQUFDO3lCQUN4RyxTQUFTLENBQUMsVUFBQSxRQUFRO3dCQUNqQixJQUFNLE9BQU8sR0FBc0IsS0FBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDOUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQzdGLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3ZCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDdEIsQ0FBQyxFQUFFLFVBQUEsS0FBSzt3QkFDTixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLHNEQUFzRCxDQUFDLENBQUM7d0JBQ3ZGLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2lCQUNOO2FBQ0Y7UUFFSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxtREFBdUIsR0FBdkIsVUFBd0IsUUFBb0I7UUFBNUMsaUJBc0NDO1FBckNDLElBQU0sT0FBTyxHQUFzQixFQUFFLENBQUM7UUFDdEMsSUFBSSxRQUFRLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM5RCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsYUFBYTtnQkFDNUIsSUFBTSxRQUFRLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNyRCxJQUFNLFFBQVEsR0FBZTtvQkFDM0Isb0JBQW9CLEVBQUU7d0JBQ3BCLEVBQUUsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDO3dCQUNsQixNQUFNLEVBQUUsSUFBSTtxQkFDYjtvQkFDRCxJQUFJLEVBQUUsUUFBUSxDQUFDLE1BQU0sQ0FBQztvQkFDdEIsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDLGdCQUFnQixDQUFDO29CQUM1QyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsZ0JBQWdCLENBQUM7b0JBQzVDLHVCQUF1QixFQUFFO3dCQUN2QixzQkFBc0IsRUFBRTs0QkFDdEIsRUFBRSxFQUFFLFFBQVEsQ0FBQyxlQUFlLENBQUM7NEJBQzdCLE1BQU0sRUFBRSxJQUFJO3lCQUNiO3FCQUNGO29CQUNELGFBQWEsRUFBRTt3QkFDYix1QkFBdUIsRUFBRTs0QkFDdkIsRUFBRSxFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUM7NEJBQzFCLE1BQU0sRUFBRSxJQUFJO3lCQUNiO3FCQUNGO29CQUNELHlCQUF5QixFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ25HLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQzNGLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3BFLHlCQUF5QixFQUFFLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ25HLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQzNGLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3BFLE9BQU8sRUFBRSxRQUFRLENBQUMsUUFBUSxDQUFDO29CQUMzQiwyQkFBMkIsRUFBRSxRQUFRLENBQUMsdUJBQXVCLENBQUM7aUJBQy9ELENBQUM7Z0JBQ0YsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN6QixDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUNELCtDQUFtQixHQUFuQixVQUFvQixTQUFxQjtRQUN2QyxPQUFPLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ3ZCLElBQU0sUUFBUSxHQUFhO2dCQUN6QixzQkFBc0IsRUFBRTtvQkFDdEIsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2QsTUFBTSxFQUFFLElBQUk7aUJBQ2I7Z0JBQ0QsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUM7Z0JBQzNCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDeEMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQ2hDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxlQUFlLENBQUM7Z0JBQzFDLFdBQVcsRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUNyQixRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDNUIsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQzlCLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUNsQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDM0IsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQy9CLFlBQVksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDO2dCQUNqQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7Z0JBQzFDLGVBQWUsRUFBRSxPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztnQkFDNUYsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtnQkFDbEYsdUJBQXVCLEVBQUU7b0JBQ3ZCLHVCQUF1QixFQUFFO3dCQUN2QixFQUFFLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDO3dCQUMzQixNQUFNLEVBQUUsSUFBSTtxQkFDYjtpQkFDRjthQUNGLENBQUM7WUFDRixPQUFPLFFBQVEsQ0FBQztRQUNsQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxvREFBd0IsR0FBeEIsVUFBeUIsR0FBVyxFQUFFLEtBQVU7UUFDOUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixFQUFFLEVBQUU7WUFDckQsSUFBSSxHQUFHLEtBQUssUUFBUSxJQUFJLEdBQUcsS0FBSyxJQUFJLEVBQUU7Z0JBQ3BDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEtBQUssRUFBekMsQ0FBeUMsQ0FBQyxDQUFDO2FBQ3BIO1lBQ0QsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixFQUFFLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEtBQUssRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDO1NBQzlGO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQsbURBQXVCLEdBQXZCLFVBQXdCLEdBQVcsRUFBRSxLQUFVO1FBQzdDLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDNUQsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDckMsT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDckI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCwyREFBK0IsR0FBL0IsVUFBZ0MsUUFBZ0I7UUFBaEQsaUJBbUNDO1FBbENDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDaEUsSUFBSSxRQUFRLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsSUFBSSxRQUFRLENBQUMseUJBQXlCLENBQUMsRUFBRTtnQkFDM0YsSUFBTSxLQUFLLEdBQUc7b0JBQ1osb0JBQW9CLEVBQUU7d0JBQ3BCLFlBQVksRUFBRSwrQ0FBK0M7d0JBQzdELEVBQUUsRUFBRSxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFO3FCQUN0QztpQkFDRixDQUFDO2dCQUNGLFFBQVEsQ0FBQztvQkFDUCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMEJBQTBCLEVBQUUsOEJBQThCLEVBQUUsS0FBSyxDQUFDO29CQUNyRyxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMEJBQTBCLEVBQUUsOEJBQThCLEVBQUUsS0FBSyxDQUFDO2lCQUN0RyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsWUFBWTtvQkFDdkIsUUFBUSxDQUFDLHlCQUF5QixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDckcsUUFBUSxDQUFDLHlCQUF5QixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDckcsS0FBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSTt3QkFDeEcsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLEtBQUssUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxFQUFFOzRCQUN2RSxPQUFPLFFBQVEsQ0FBQzt5QkFDakI7d0JBQ0QsT0FBTyxJQUFJLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDSixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsSUFBSSxRQUFRLEVBQUU7b0JBQ1osUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNyQjtxQkFBTTtvQkFDTCxRQUFRLENBQUMsS0FBSyxDQUFDLDhDQUE4QyxDQUFDLENBQUM7b0JBQy9ELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDckI7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHlEQUE2QixHQUE3QixVQUE4QixRQUFnQjtRQUM1QyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2IsT0FBTztTQUNSO1FBQ0QsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1FBQ3RFLElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQy9DLE9BQU87U0FDUjtRQUNELElBQU0sSUFBSSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQ2hDLE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxRQUFRLENBQUM7UUFDaEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksSUFBSSxJQUFJLEVBQUU7U0FFakI7SUFDSCxDQUFDOztnQkE3Tm9CLFVBQVU7Z0JBQ04sY0FBYztnQkFDVCxtQkFBbUI7OztJQUx0QyxpQkFBaUI7UUFIN0IsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLGlCQUFpQixDQWtPN0I7NEJBaFBEO0NBZ1BDLEFBbE9ELElBa09DO1NBbE9ZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvVmlld0NvbmZpZyc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBzdHJpY3QgfSBmcm9tICdhc3NlcnQnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFZpZXdDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICApIHsgfVxyXG5cclxuICBwdWJsaWMgR0VUX0FMTF9NUE1fVklFV19DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1ZpZXdfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gIHB1YmxpYyBHRVRfQUxMX01QTV9WSUVXX0NPTkZJRyA9ICdHZXRBbGxNUE1WaWV3Q29uZmlnJztcclxuXHJcbiAgcHVibGljIEdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX0JQTV9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vd3NhcHAvY29yZS8xLjAnO1xyXG4gIHB1YmxpYyBHRVRfQUxMX01QTV9WSUVXX0NPTkZJR19CUE0gPSAnR2V0Vmlld0NvbmZpZ0RldGFpbHMnO1xyXG5cclxuICBHZXRBbGxNUE1WaWV3Q29uZmlnKCk6IE9ic2VydmFibGU8QXJyYXk8YW55Pj4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKCkpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKCkpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX01QTV9WSUVXX0NPTkZJR19OUywgdGhpcy5HRVRfQUxMX01QTV9WSUVXX0NPTkZJRywge30pXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1ZpZXdfQ29uZmlnJykpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgTVBNIEZpZWxkIGRldGFpbHMnKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuIC8qICBHZXRBbGxNUE1WaWV3Q29uZmlnQlBNKGNhdGVnb3J5SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8QXJyYXk8Vmlld0NvbmZpZz4+IHtcclxuICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICBjYXRlZ29yeUlkOiBjYXRlZ29yeUlkLFxyXG4gICAgICByb2xlSWQ6IG51bGxcclxuICAgIH07XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAodGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcoKSkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcoKSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX0JQTV9OUywgdGhpcy5HRVRfQUxMX01QTV9WSUVXX0NPTkZJR19CUE0sIHBhcmFtKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGFTZXQ6IEFycmF5PFZpZXdDb25maWc+ID0gdGhpcy5mb3JtVmlld0NvbmZpZ0ludGVyZmFjZShhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndHVwbGUnKSk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YVNldCk7XHJcbiAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBNUE0gRmllbGQgZGV0YWlscycpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfSAqL1xyXG5cclxuICBHZXRBbGxNUE1WaWV3Q29uZmlnQlBNKGNhdGVnb3J5SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8QXJyYXk8Vmlld0NvbmZpZz4+IHtcclxuICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICBjYXRlZ29yeUlkOiBjYXRlZ29yeUlkLFxyXG4gICAgICByb2xlSWQ6IG51bGxcclxuICAgIH07XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAodGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcoKSkge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxBcHBsaWNhdGlvblZpZXdDb25maWcoKSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfTVBNX1ZJRVdfQ09ORklHKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX01QTV9WSUVXX0NPTkZJRykpKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9NUE1fVklFV19DT05GSUdfQlBNX05TLCB0aGlzLkdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX0JQTSwgcGFyYW0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgIGNvbnN0IGRhdGFTZXQ6IEFycmF5PFZpZXdDb25maWc+ID0gdGhpcy5mb3JtVmlld0NvbmZpZ0ludGVyZmFjZShhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAndHVwbGUnKSk7XHJcbiAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfTVBNX1ZJRVdfQ09ORklHLCBKU09OLnN0cmluZ2lmeShkYXRhU2V0KSk7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhU2V0KTtcclxuICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIE1QTSBGaWVsZCBkZXRhaWxzJyk7XHJcbiAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGZvcm1WaWV3Q29uZmlnSW50ZXJmYWNlKHZpZXdMaXN0OiBBcnJheTxhbnk+KTogQXJyYXk8Vmlld0NvbmZpZz4ge1xyXG4gICAgY29uc3QgZGF0YVNldDogQXJyYXk8Vmlld0NvbmZpZz4gPSBbXTtcclxuICAgIGlmICh2aWV3TGlzdCAmJiBBcnJheS5pc0FycmF5KHZpZXdMaXN0KSAmJiB2aWV3TGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHZpZXdMaXN0LmZvckVhY2godGVtcFZpZXdUdXBsZSA9PiB7XHJcbiAgICAgICAgY29uc3QgdGVtcFZpZXcgPSB0ZW1wVmlld1R1cGxlWydvbGQnXVsnVmlld0RldGFpbHMnXTtcclxuICAgICAgICBjb25zdCBjdXJyVmlldzogVmlld0NvbmZpZyA9IHtcclxuICAgICAgICAgICdNUE1fVmlld19Db25maWctaWQnOiB7XHJcbiAgICAgICAgICAgIElkOiB0ZW1wVmlld1snaWQnXSxcclxuICAgICAgICAgICAgSXRlbUlkOiBudWxsLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIFZJRVc6IHRlbXBWaWV3Wyd2aWV3J10sXHJcbiAgICAgICAgICBDQVJEX0ZJRUxEX09SREVSOiB0ZW1wVmlld1snY2FyZEZpZWxkT3JkZXInXSxcclxuICAgICAgICAgIExJU1RfRklFTERfT1JERVI6IHRlbXBWaWV3WydsaXN0RmllbGRPcmRlciddLFxyXG4gICAgICAgICAgUl9QT19ERUZBVUxUX1NPUlRfRklFTEQ6IHtcclxuICAgICAgICAgICAgJ01QTV9GaWVsZHNfQ29uZmlnLWlkJzoge1xyXG4gICAgICAgICAgICAgIElkOiB0ZW1wVmlld1snc29ydGVkRmllbGRJZCddLFxyXG4gICAgICAgICAgICAgIEl0ZW1JZDogbnVsbCxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIFJfUE9fQ0FURUdPUlk6IHtcclxuICAgICAgICAgICAgJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCc6IHtcclxuICAgICAgICAgICAgICBJZDogdGVtcFZpZXdbJ2NhdGVnb3J5SWQnXSxcclxuICAgICAgICAgICAgICBJdGVtSWQ6IG51bGwsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBSX1BNX0xJU1RfVklFV19NUE1fRklFTERTOiB0ZW1wVmlld1snbGlzdEZpZWxkcyddICYmIEFycmF5LmlzQXJyYXkodGVtcFZpZXdbJ2xpc3RGaWVsZHMnXVsnZmllbGQnXSkgP1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1NUE1GaWVsZERldGFpbHModGVtcFZpZXdbJ2xpc3RGaWVsZHMnXVsnZmllbGQnXSkgOiB0ZW1wVmlld1snbGlzdEZpZWxkcyddWydmaWVsZCddID9cclxuICAgICAgICAgICAgICB0aGlzLmZvcm1NUE1GaWVsZERldGFpbHMoW3RlbXBWaWV3WydsaXN0RmllbGRzJ11bJ2ZpZWxkJ11dKSA6IFtdLFxyXG4gICAgICAgICAgUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUzogdGVtcFZpZXdbJ2NhcmRGaWVsZHMnXSAmJiBBcnJheS5pc0FycmF5KHRlbXBWaWV3WydjYXJkRmllbGRzJ11bJ2ZpZWxkJ10pID9cclxuICAgICAgICAgICAgdGhpcy5mb3JtTVBNRmllbGREZXRhaWxzKHRlbXBWaWV3WydjYXJkRmllbGRzJ11bJ2ZpZWxkJ10pIDogdGVtcFZpZXdbJ2NhcmRGaWVsZHMnXVsnZmllbGQnXSA/XHJcbiAgICAgICAgICAgICAgdGhpcy5mb3JtTVBNRmllbGREZXRhaWxzKFt0ZW1wVmlld1snY2FyZEZpZWxkcyddWydmaWVsZCddXSkgOiBbXSxcclxuICAgICAgICAgIFJPTEVfSUQ6IHRlbXBWaWV3Wydyb2xlSWQnXSxcclxuICAgICAgICAgIFJfUE9fQURWQU5DRURfU0VBUkNIX0NPTkZJRzogdGVtcFZpZXdbJ2FkdmFuY2VTZWFyY2hDb25maWdJZCddXHJcbiAgICAgICAgfTtcclxuICAgICAgICBkYXRhU2V0LnB1c2goY3VyclZpZXcpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBkYXRhU2V0O1xyXG4gIH1cclxuICBmb3JtTVBNRmllbGREZXRhaWxzKGZpZWxkTGlzdDogQXJyYXk8YW55Pik6IEFycmF5PE1QTUZpZWxkPiB7XHJcbiAgICByZXR1cm4gZmllbGRMaXN0Lm1hcChkYXRhID0+IHtcclxuICAgICAgY29uc3QgbXBtRmllbGQ6IE1QTUZpZWxkID0ge1xyXG4gICAgICAgICdNUE1fRmllbGRzX0NvbmZpZy1pZCc6IHtcclxuICAgICAgICAgIElkOiBkYXRhWydpZCddLFxyXG4gICAgICAgICAgSXRlbUlkOiBudWxsLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgRURJVF9UWVBFOiBkYXRhWydlZGl0VHlwZSddLFxyXG4gICAgICAgIElOREVYRVJfRklFTERfSUQ6IGRhdGFbJ2luZGV4ZXJGaWVsZElkJ10sXHJcbiAgICAgICAgU0VBUkNIQUJMRTogZGF0YVsnaXNTZWFyY2hhYmxlJ10sXHJcbiAgICAgICAgTVBNX0ZJRUxEX0NPTkZJR19JRDogZGF0YVsnZmllbGRDb25maWdJZCddLFxyXG4gICAgICAgIEZJRUxEX0xFVkVMOiBkYXRhWycnXSxcclxuICAgICAgICBTT1JUQUJMRTogZGF0YVsnaXNTb3J0YWJsZSddLFxyXG4gICAgICAgIEZBQ0VUQUJMRTogZGF0YVsnaXNGYWNldGFibGUnXSxcclxuICAgICAgICBPVE1NX0ZJRUxEX0lEOiBkYXRhWydvdG1tRmllbGRJZCddLFxyXG4gICAgICAgIERBVEFfVFlQRTogZGF0YVsnZGF0YVR5cGUnXSxcclxuICAgICAgICBNQVBQRVJfTkFNRTogZGF0YVsnbWFwcGVyTmFtZSddLFxyXG4gICAgICAgIERJU1BMQVlfTkFNRTogZGF0YVsnZGlzcGxheU5hbWUnXSxcclxuICAgICAgICBJU19DVVNUT01fTUVUQURBVEE6IGRhdGFbJ2lzQ3VzdG9tRmllbGRzJ10sXHJcbiAgICAgICAgSVNfRE9NQUlOX1ZBTFVFOiB0eXBlb2YgZGF0YVsnaXNEb21haW5WYWx1ZSddID09PSAnc3RyaW5nJyA/IGRhdGFbJ2lzRG9tYWluVmFsdWUnXSA6ICdmYWxzZScsXHJcbiAgICAgICAgRE9NQUlOX0ZJRUxEX0lEOiBkYXRhWydkb21haW5GaWVsZElkJ10gPT09ICdzdHJpbmcnID8gZGF0YVsnZG9tYWluRmllbGRJZCddIDogbnVsbCxcclxuICAgICAgICBSX1BPX01QTV9DQVRFR09SWV9MRVZFTDoge1xyXG4gICAgICAgICAgJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCc6IHtcclxuICAgICAgICAgICAgSWQ6IGRhdGFbJ2NhdGVnb3J5TGV2ZWxJZCddLFxyXG4gICAgICAgICAgICBJdGVtSWQ6IG51bGxcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcbiAgICAgIHJldHVybiBtcG1GaWVsZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0dmlld0NvbmZpZ3NCeUtleVZhbHVlKGtleTogc3RyaW5nLCB2YWx1ZTogYW55KTogQXJyYXk8Vmlld0NvbmZpZz4ge1xyXG4gICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKCkpIHtcclxuICAgICAgaWYgKGtleSA9PT0gJ0l0ZW1JZCcgfHwga2V5ID09PSAnSWQnKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKCkuZmlsdGVyKGRhdGEgPT4gZGF0YVsnTVBNX1ZpZXdfQ29uZmlnLWlkJ11ba2V5XSA9PT0gdmFsdWUpO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZygpLmZpbHRlcihkYXRhID0+IGRhdGFba2V5XSA9PT0gdmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIFtdO1xyXG4gIH1cclxuXHJcbiAgZ2V0dmlld0NvbmZpZ0J5S2V5VmFsdWUoa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpOiBWaWV3Q29uZmlnIHtcclxuICAgIGNvbnN0IGN1cnJGaWVsZCA9IHRoaXMuZ2V0dmlld0NvbmZpZ3NCeUtleVZhbHVlKGtleSwgdmFsdWUpO1xyXG4gICAgaWYgKGN1cnJGaWVsZCAmJiBjdXJyRmllbGQubGVuZ3RoID4gMCkge1xyXG4gICAgICByZXR1cm4gY3VyckZpZWxkWzBdO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG51bGw7XHJcbiAgfVxyXG5cclxuICBnZXRBbGxEaXNwbGF5RmVpbGRGb3JWaWV3Q29uZmlnKFZpZXdOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFZpZXdDb25maWc+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGNvbnN0IGN1cnJWaWV3ID0gdGhpcy5nZXR2aWV3Q29uZmlnQnlLZXlWYWx1ZSgnVklFVycsIFZpZXdOYW1lKTtcclxuICAgICAgaWYgKGN1cnJWaWV3ICYmICEoY3VyclZpZXcuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUyAmJiBjdXJyVmlldy5SX1BNX0xJU1RfVklFV19NUE1fRklFTERTKSkge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgJ01QTV9WaWV3X0NvbmZpZy1pZCc6IHtcclxuICAgICAgICAgICAgJ0B4bWxuczpuczAnOiAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1ZpZXdfQ29uZmlnJyxcclxuICAgICAgICAgICAgSWQ6IGN1cnJWaWV3WydNUE1fVmlld19Db25maWctaWQnXS5JZFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgZm9ya0pvaW4oW1xyXG4gICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX01QTV9WSUVXX0NPTkZJR19OUywgJ0dldFJfUE1fQ0FSRF9WSUVXX01QTV9GSUVMRFMnLCBwYXJhbSksXHJcbiAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX05TLCAnR2V0Ul9QTV9MSVNUX1ZJRVdfTVBNX0ZJRUxEUycsIHBhcmFtKVxyXG4gICAgICAgIF0pLnN1YnNjcmliZShyZXNwb25zZUxpc3QgPT4ge1xyXG4gICAgICAgICAgY3VyclZpZXcuUl9QTV9DQVJEX1ZJRVdfTVBNX0ZJRUxEUyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2VMaXN0WzBdLCAnTVBNX0ZpZWxkc19Db25maWcnKTtcclxuICAgICAgICAgIGN1cnJWaWV3LlJfUE1fTElTVF9WSUVXX01QTV9GSUVMRFMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlTGlzdFsxXSwgJ01QTV9GaWVsZHNfQ29uZmlnJyk7XHJcbiAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZyh0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZygpLm1hcChkYXRhID0+IHtcclxuICAgICAgICAgICAgaWYgKGRhdGFbJ01QTV9WaWV3X0NvbmZpZy1pZCddLklkID09PSBjdXJyVmlld1snTVBNX1ZpZXdfQ29uZmlnLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gY3VyclZpZXc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KGN1cnJWaWV3KTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKGN1cnJWaWV3KSB7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KGN1cnJWaWV3KTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdFcnJvciB3aGlsZSBnZXR0aW5nIHRoZSB2aWV3IGNvbmZpZyBkZXRhaWxzLicpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeVZpZXcodmlld05hbWU6IHN0cmluZyk6IFZpZXdDb25maWcge1xyXG4gICAgaWYgKCF2aWV3TmFtZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCB2aWV3Q29uZmlncyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKCk7XHJcbiAgICBpZiAoIXZpZXdDb25maWdzIHx8ICFBcnJheS5pc0FycmF5KHZpZXdDb25maWdzKSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCB2aWV3ID0gdmlld0NvbmZpZ3MuZmluZChpdGVtID0+IHtcclxuICAgICAgcmV0dXJuIGl0ZW0uVklFVyA9PT0gdmlld05hbWU7XHJcbiAgICB9KTtcclxuICAgIGlmICh2aWV3ICYmIHZpZXcpIHtcclxuXHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=