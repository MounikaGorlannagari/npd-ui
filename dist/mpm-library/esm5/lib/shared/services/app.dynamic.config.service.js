import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var AppDynamicConfigService = /** @class */ (function () {
    function AppDynamicConfigService(http) {
        this.http = http;
    }
    AppDynamicConfigService.prototype.getBrandingAndConfig = function (url) {
        return this.http.get(url);
    };
    AppDynamicConfigService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    AppDynamicConfigService = __decorate([
        Injectable()
    ], AppDynamicConfigService);
    return AppDynamicConfigService;
}());
export { AppDynamicConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmR5bmFtaWMuY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvYXBwLmR5bmFtaWMuY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBR2xEO0lBRUksaUNBQ1csSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUN2QixDQUFDO0lBRUwsc0RBQW9CLEdBQXBCLFVBQXFCLEdBQUc7UUFDcEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM5QixDQUFDOztnQkFMZ0IsVUFBVTs7SUFIbEIsdUJBQXVCO1FBRG5DLFVBQVUsRUFBRTtPQUNBLHVCQUF1QixDQVVuQztJQUFELDhCQUFDO0NBQUEsQUFWRCxJQVVDO1NBVlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwRHluYW1pY0NvbmZpZ1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50XHJcbiAgICApIHsgfVxyXG5cclxuICAgIGdldEJyYW5kaW5nQW5kQ29uZmlnKHVybCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHVybCk7XHJcbiAgICB9XHJcblxyXG59Il19