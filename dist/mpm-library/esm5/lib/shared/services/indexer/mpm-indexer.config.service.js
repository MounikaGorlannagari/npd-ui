import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../../mpm-utils/services/app.service';
import { Observable } from 'rxjs';
import * as acronui from '../../../mpm-utils/auth/utility';
import { SessionStorageConstants } from '../../constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "../../../mpm-utils/services/app.service";
var IndexerConfigService = /** @class */ (function () {
    function IndexerConfigService(appService) {
        this.appService = appService;
        this.MPM_INDEXER_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Indexer_Config/operations';
        this.MPM_INDEXER_CONFIG_WS = 'GetIndexerConfigByCategoryId';
    }
    IndexerConfigService.prototype.getIndexerConfigByCategoryId = function (categoryId) {
        if (categoryId && this.indexerConfig && this.indexerConfig.R_PO_CATEGORY &&
            this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
            return this.indexerConfig;
        }
        return;
    };
    /* setIndexerConfigByCategoryId(categoryId: string): Observable<MPMIndexerConfig> {
      return new Observable(observer => {
        if (categoryId && this.indexerConfig && this.indexerConfig.R_PO_CATEGORY
          && this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
          observer.next(this.indexerConfig);
          observer.complete();
          return;
        }
        const param = {
          CategoryId: categoryId
        };
        this.indexerConfig = null;
        this.appService.invokeRequest(this.MPM_INDEXER_CONFIG_NS, this.MPM_INDEXER_CONFIG_WS, param).subscribe(indexerConfig => {
          const configs = acronui.findObjectsByProp(indexerConfig, 'MPM_Indexer_Config');
          if (configs[0]) {
            this.indexerConfig = configs[0];
          }
          observer.next(this.indexerConfig);
          observer.complete();
        }, error => {
          observer.error();
          observer.complete();
        });
      });
    } */
    IndexerConfigService.prototype.setIndexerConfigByCategoryId = function (categoryId) {
        var _this = this;
        return new Observable(function (observer) {
            if (categoryId && _this.indexerConfig && _this.indexerConfig.R_PO_CATEGORY
                && _this.indexerConfig.R_PO_CATEGORY['MPM_Category-id'].Id === categoryId) {
                observer.next(_this.indexerConfig);
                observer.complete();
                return;
            }
            var param = {
                CategoryId: categoryId
            };
            _this.indexerConfig = null;
            if (sessionStorage.getItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID) !== null) {
                _this.indexerConfig = JSON.parse(sessionStorage.getItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID));
                observer.next(_this.indexerConfig);
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.MPM_INDEXER_CONFIG_NS, _this.MPM_INDEXER_CONFIG_WS, param).subscribe(function (indexerConfig) {
                    var configs = acronui.findObjectsByProp(indexerConfig, 'MPM_Indexer_Config');
                    if (configs[0]) {
                        _this.indexerConfig = configs[0];
                    }
                    sessionStorage.setItem(SessionStorageConstants.INDEXER_CONFIG_BY_CATEGORY_ID, JSON.stringify(_this.indexerConfig));
                    observer.next(_this.indexerConfig);
                    observer.complete();
                }, function (error) {
                    observer.error();
                    observer.complete();
                });
            }
        });
    };
    IndexerConfigService.ctorParameters = function () { return [
        { type: AppService }
    ]; };
    IndexerConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerConfigService_Factory() { return new IndexerConfigService(i0.ɵɵinject(i1.AppService)); }, token: IndexerConfigService, providedIn: "root" });
    IndexerConfigService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], IndexerConfigService);
    return IndexerConfigService;
}());
export { IndexerConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWluZGV4ZXIuY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvaW5kZXhlci9tcG0taW5kZXhlci5jb25maWcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLGlDQUFpQyxDQUFDO0FBRTNELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7QUFLcEY7SUFFRSw4QkFDUyxVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBRy9CLDBCQUFxQixHQUFHLDZEQUE2RCxDQUFDO1FBQ3RGLDBCQUFxQixHQUFHLDhCQUE4QixDQUFDO0lBSG5ELENBQUM7SUFPTCwyREFBNEIsR0FBNUIsVUFBNkIsVUFBa0I7UUFDN0MsSUFBSSxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWE7WUFDdEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVSxFQUFFO1lBQ3ZFLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztTQUMzQjtRQUNELE9BQU87SUFDVCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQXdCSTtJQUVKLDJEQUE0QixHQUE1QixVQUE2QixVQUFrQjtRQUEvQyxpQkFpQ0M7UUFoQ0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxVQUFVLElBQUksS0FBSSxDQUFDLGFBQWEsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLGFBQWE7bUJBQ25FLEtBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxLQUFLLFVBQVUsRUFBRTtnQkFDMUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ2xDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsT0FBTzthQUNSO1lBQ0QsSUFBTSxLQUFLLEdBQUc7Z0JBQ1osVUFBVSxFQUFFLFVBQVU7YUFDdkIsQ0FBQztZQUNGLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDMUYsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO2dCQUMvRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDbEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3JCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxLQUFJLENBQUMscUJBQXFCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsYUFBYTtvQkFDbEgsSUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO29CQUMvRSxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDZCxLQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDakM7b0JBQ0QsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyw2QkFBNkIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUNsSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDbEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNOLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDakIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUNKO1FBR0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkEzRW9CLFVBQVU7OztJQUhwQixvQkFBb0I7UUFIaEMsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLG9CQUFvQixDQWlGaEM7K0JBM0ZEO0NBMkZDLEFBakZELElBaUZDO1NBakZZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0ICogYXMgYWNyb251aSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgTVBNSW5kZXhlckNvbmZpZyB9IGZyb20gJy4vb2JqZWN0cy9NUE1JbmRleGVyQ29uZmlnJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJbmRleGVyQ29uZmlnU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICBNUE1fSU5ERVhFUl9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0luZGV4ZXJfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gIE1QTV9JTkRFWEVSX0NPTkZJR19XUyA9ICdHZXRJbmRleGVyQ29uZmlnQnlDYXRlZ29yeUlkJztcclxuXHJcbiAgaW5kZXhlckNvbmZpZzogTVBNSW5kZXhlckNvbmZpZztcclxuXHJcbiAgZ2V0SW5kZXhlckNvbmZpZ0J5Q2F0ZWdvcnlJZChjYXRlZ29yeUlkOiBzdHJpbmcpOiBNUE1JbmRleGVyQ29uZmlnIHtcclxuICAgIGlmIChjYXRlZ29yeUlkICYmIHRoaXMuaW5kZXhlckNvbmZpZyAmJiB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWSAmJlxyXG4gICAgICB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQgPT09IGNhdGVnb3J5SWQpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuaW5kZXhlckNvbmZpZztcclxuICAgIH1cclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIC8qIHNldEluZGV4ZXJDb25maWdCeUNhdGVnb3J5SWQoY2F0ZWdvcnlJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxNUE1JbmRleGVyQ29uZmlnPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAoY2F0ZWdvcnlJZCAmJiB0aGlzLmluZGV4ZXJDb25maWcgJiYgdGhpcy5pbmRleGVyQ29uZmlnLlJfUE9fQ0FURUdPUllcclxuICAgICAgICAmJiB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQgPT09IGNhdGVnb3J5SWQpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuaW5kZXhlckNvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgcGFyYW0gPSB7XHJcbiAgICAgICAgQ2F0ZWdvcnlJZDogY2F0ZWdvcnlJZFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmluZGV4ZXJDb25maWcgPSBudWxsO1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9JTkRFWEVSX0NPTkZJR19OUywgdGhpcy5NUE1fSU5ERVhFUl9DT05GSUdfV1MsIHBhcmFtKS5zdWJzY3JpYmUoaW5kZXhlckNvbmZpZyA9PiB7XHJcbiAgICAgICAgY29uc3QgY29uZmlncyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoaW5kZXhlckNvbmZpZywgJ01QTV9JbmRleGVyX0NvbmZpZycpO1xyXG4gICAgICAgIGlmIChjb25maWdzWzBdKSB7XHJcbiAgICAgICAgICB0aGlzLmluZGV4ZXJDb25maWcgPSBjb25maWdzWzBdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuaW5kZXhlckNvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9ICovXHJcblxyXG4gIHNldEluZGV4ZXJDb25maWdCeUNhdGVnb3J5SWQoY2F0ZWdvcnlJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxNUE1JbmRleGVyQ29uZmlnPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAoY2F0ZWdvcnlJZCAmJiB0aGlzLmluZGV4ZXJDb25maWcgJiYgdGhpcy5pbmRleGVyQ29uZmlnLlJfUE9fQ0FURUdPUllcclxuICAgICAgICAmJiB0aGlzLmluZGV4ZXJDb25maWcuUl9QT19DQVRFR09SWVsnTVBNX0NhdGVnb3J5LWlkJ10uSWQgPT09IGNhdGVnb3J5SWQpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuaW5kZXhlckNvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgcGFyYW0gPSB7XHJcbiAgICAgICAgQ2F0ZWdvcnlJZDogY2F0ZWdvcnlJZFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmluZGV4ZXJDb25maWcgPSBudWxsO1xyXG4gICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5JTkRFWEVSX0NPTkZJR19CWV9DQVRFR09SWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICB0aGlzLmluZGV4ZXJDb25maWcgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuSU5ERVhFUl9DT05GSUdfQllfQ0FURUdPUllfSUQpKTtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuaW5kZXhlckNvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9JTkRFWEVSX0NPTkZJR19OUywgdGhpcy5NUE1fSU5ERVhFUl9DT05GSUdfV1MsIHBhcmFtKS5zdWJzY3JpYmUoaW5kZXhlckNvbmZpZyA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjb25maWdzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChpbmRleGVyQ29uZmlnLCAnTVBNX0luZGV4ZXJfQ29uZmlnJyk7XHJcbiAgICAgICAgICBpZiAoY29uZmlnc1swXSkge1xyXG4gICAgICAgICAgICB0aGlzLmluZGV4ZXJDb25maWcgPSBjb25maWdzWzBdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5JTkRFWEVSX0NPTkZJR19CWV9DQVRFR09SWV9JRCwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbmRleGVyQ29uZmlnKSk7XHJcbiAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuaW5kZXhlckNvbmZpZyk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG5cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG59XHJcbiJdfQ==