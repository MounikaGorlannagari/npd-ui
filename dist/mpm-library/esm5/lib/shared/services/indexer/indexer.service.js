import { __decorate } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IndexerConfigService } from './mpm-indexer.config.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { IndexerEndPoints } from './objects/IndexerEndPoints';
import { SessionStorageConstants } from '../../constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../../mpm-utils/services/sharing.service";
import * as i3 from "./mpm-indexer.config.service";
var IndexerService = /** @class */ (function () {
    function IndexerService(httpClient, sharingService, indexerConfigService) {
        this.httpClient = httpClient;
        this.sharingService = sharingService;
        this.indexerConfigService = indexerConfigService;
    }
    IndexerService.prototype.indexerEndPoint = function (path) {
        var indexerBaseDomain = isDevMode() ? { SEARCH_URL: '.' } : this.indexerConfigService.getIndexerConfigByCategoryId(this.sharingService.getSelectedCategory()['MPM_Category-id'].Id);
        if (indexerBaseDomain && indexerBaseDomain.SEARCH_URL) {
            return indexerBaseDomain.SEARCH_URL + IndexerEndPoints.base + path;
        }
        return IndexerEndPoints.base + path;
    };
    IndexerService.prototype.search = function (req) {
        var _this = this;
        return new Observable(function (observer) {
            if (req.search_config_id || true) {
                // MPMV3-2237
                console.log(!isDevMode());
                var httpOptions = {
                    headers: new HttpHeaders({
                        'SAMLart': !isDevMode() ? '' : localStorage.getItem('SAMLART'),
                        'OTDSSSO': JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET)) !== null ? JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET)) : '',
                        'currentUserId': !isDevMode() ? JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID)) : localStorage.getItem('OTDS_USER_DN') //JSON.parse(sessionStorage.getItem(SessionStorageConstants.OTDS_USER_DN))
                    })
                };
                //   this.httpClient.post(this.indexerEndPoint(IndexerEndPoints.search), req).subscribe((response: SearchResponse) => {
                // this.httpClient.post(this.indexerEndPoint(IndexerEndPoints.search), req, !isDevMode() ? httpOptions : undefined).subscribe((response: SearchResponse) => {
                // MPMV3-2237
                _this.httpClient.post(_this.indexerEndPoint(IndexerEndPoints.search), req, httpOptions).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                    observer.complete();
                });
            }
            else {
                observer.next({
                    data: [],
                    cursor: { total_records: 0 },
                    facet: null
                });
                observer.complete();
            }
        });
    };
    IndexerService.prototype.searchTemp = function (req) {
        var _this = this;
        return new Observable(function (observer) {
            _this.httpClient.get('./assets/mock.data.json').subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    IndexerService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: SharingService },
        { type: IndexerConfigService }
    ]; };
    IndexerService.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerService_Factory() { return new IndexerService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.IndexerConfigService)); }, token: IndexerService, providedIn: "root" });
    IndexerService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], IndexerService);
    return IndexerService;
}());
export { IndexerService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXhlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvaW5kZXhlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHL0QsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzlELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDOzs7OztBQU1wRjtJQUVFLHdCQUNTLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLG9CQUEwQztRQUYxQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO0lBQy9DLENBQUM7SUFHTCx3Q0FBZSxHQUFmLFVBQWdCLElBQUk7UUFDbEIsSUFBTSxpQkFBaUIsR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyw0QkFBNEIsQ0FDbEgsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkUsSUFBSSxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxVQUFVLEVBQUU7WUFDckQsT0FBTyxpQkFBaUIsQ0FBQyxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztTQUNwRTtRQUNELE9BQU8sZ0JBQWdCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUN0QyxDQUFDO0lBRUQsK0JBQU0sR0FBTixVQUFPLEdBQWtCO1FBQXpCLGlCQWdDQztRQS9CQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUM1QixJQUFJLEdBQUcsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7Z0JBQ2hDLGFBQWE7Z0JBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUE7Z0JBQ3pCLElBQU0sV0FBVyxHQUFHO29CQUNsQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3ZCLFNBQVMsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO3dCQUM5RCxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDMUssZUFBZSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFBLDBFQUEwRTtxQkFDMU4sQ0FBQztpQkFDSCxDQUFDO2dCQUNGLHVIQUF1SDtnQkFFdkgsNkpBQTZKO2dCQUM3SixhQUFhO2dCQUNiLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEVBQUUsR0FBRyxFQUFFLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLFFBQXdCO29CQUN2SCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ1osSUFBSSxFQUFFLEVBQUU7b0JBQ1IsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLENBQUMsRUFBRTtvQkFDNUIsS0FBSyxFQUFFLElBQUk7aUJBQ1osQ0FBQyxDQUFDO2dCQUNILFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELG1DQUFVLEdBQVYsVUFBVyxHQUFrQjtRQUE3QixpQkFVQztRQVRDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLEtBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsUUFBd0I7Z0JBQ2hGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNOLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBMURvQixVQUFVO2dCQUNOLGNBQWM7Z0JBQ1Isb0JBQW9COzs7SUFMeEMsY0FBYztRQUgxQixVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csY0FBYyxDQThEMUI7eUJBNUVEO0NBNEVDLEFBOURELElBOERDO1NBOURZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBpc0Rldk1vZGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgU2VhcmNoUmVzcG9uc2UgfSBmcm9tICcuL29iamVjdHMvU2VhcmNoUmVzcG9uc2UnO1xyXG5pbXBvcnQgeyBTZWFyY2hSZXF1ZXN0IH0gZnJvbSAnLi9vYmplY3RzL1NlYXJjaFJlcXVlc3QnO1xyXG5pbXBvcnQgeyBJbmRleGVyQ29uZmlnU2VydmljZSB9IGZyb20gJy4vbXBtLWluZGV4ZXIuY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJbmRleGVyRW5kUG9pbnRzIH0gZnJvbSAnLi9vYmplY3RzL0luZGV4ZXJFbmRQb2ludHMnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJbmRleGVyU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXHJcbiAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgcHVibGljIGluZGV4ZXJDb25maWdTZXJ2aWNlOiBJbmRleGVyQ29uZmlnU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG5cclxuICBpbmRleGVyRW5kUG9pbnQocGF0aCk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBpbmRleGVyQmFzZURvbWFpbiA9IGlzRGV2TW9kZSgpID8geyBTRUFSQ0hfVVJMOiAnLicgfSA6IHRoaXMuaW5kZXhlckNvbmZpZ1NlcnZpY2UuZ2V0SW5kZXhlckNvbmZpZ0J5Q2F0ZWdvcnlJZChcclxuICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRTZWxlY3RlZENhdGVnb3J5KClbJ01QTV9DYXRlZ29yeS1pZCddLklkKTtcclxuICAgIGlmIChpbmRleGVyQmFzZURvbWFpbiAmJiBpbmRleGVyQmFzZURvbWFpbi5TRUFSQ0hfVVJMKSB7XHJcbiAgICAgIHJldHVybiBpbmRleGVyQmFzZURvbWFpbi5TRUFSQ0hfVVJMICsgSW5kZXhlckVuZFBvaW50cy5iYXNlICsgcGF0aDtcclxuICAgIH1cclxuICAgIHJldHVybiBJbmRleGVyRW5kUG9pbnRzLmJhc2UgKyBwYXRoO1xyXG4gIH1cclxuXHJcbiAgc2VhcmNoKHJlcTogU2VhcmNoUmVxdWVzdCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAocmVxLnNlYXJjaF9jb25maWdfaWQgfHwgdHJ1ZSkge1xyXG4gICAgICAgIC8vIE1QTVYzLTIyMzdcclxuICAgICAgICBjb25zb2xlLmxvZyghaXNEZXZNb2RlKCkpXHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAnU0FNTGFydCc6ICFpc0Rldk1vZGUoKSA/ICcnIDogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ1NBTUxBUlQnKSxcclxuICAgICAgICAgICAgJ09URFNTU08nOiBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19USUNLRVQpKSAhPT0gbnVsbCA/IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1RJQ0tFVCkpIDogJycsXHJcbiAgICAgICAgICAgICdjdXJyZW50VXNlcklkJzogIWlzRGV2TW9kZSgpID8gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVVNFUl9JRCkpIDogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ09URFNfVVNFUl9ETicpLy9KU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19VU0VSX0ROKSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuICAgICAgICAvLyAgIHRoaXMuaHR0cENsaWVudC5wb3N0KHRoaXMuaW5kZXhlckVuZFBvaW50KEluZGV4ZXJFbmRQb2ludHMuc2VhcmNoKSwgcmVxKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG5cclxuICAgICAgICAvLyB0aGlzLmh0dHBDbGllbnQucG9zdCh0aGlzLmluZGV4ZXJFbmRQb2ludChJbmRleGVyRW5kUG9pbnRzLnNlYXJjaCksIHJlcSwgIWlzRGV2TW9kZSgpID8gaHR0cE9wdGlvbnMgOiB1bmRlZmluZWQpLnN1YnNjcmliZSgocmVzcG9uc2U6IFNlYXJjaFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgLy8gTVBNVjMtMjIzN1xyXG4gICAgICAgIHRoaXMuaHR0cENsaWVudC5wb3N0KHRoaXMuaW5kZXhlckVuZFBvaW50KEluZGV4ZXJFbmRQb2ludHMuc2VhcmNoKSwgcmVxLCBodHRwT3B0aW9ucykuc3Vic2NyaWJlKChyZXNwb25zZTogU2VhcmNoUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQoe1xyXG4gICAgICAgICAgZGF0YTogW10sXHJcbiAgICAgICAgICBjdXJzb3I6IHsgdG90YWxfcmVjb3JkczogMCB9LFxyXG4gICAgICAgICAgZmFjZXQ6IG51bGxcclxuICAgICAgICB9KTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgc2VhcmNoVGVtcChyZXE6IFNlYXJjaFJlcXVlc3QpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgdGhpcy5odHRwQ2xpZW50LmdldCgnLi9hc3NldHMvbW9jay5kYXRhLmpzb24nKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBTZWFyY2hSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19