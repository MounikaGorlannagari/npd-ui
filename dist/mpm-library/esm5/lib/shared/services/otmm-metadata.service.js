import { __decorate, __values } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from '../../mpm-utils/services/app.service';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { OTMMDataTypes } from '../../mpm-utils/objects/OTMMDataTypes';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
var OtmmMetadataService = /** @class */ (function () {
    function OtmmMetadataService(appService, otmmService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.METADATA_MAPPING_METHOD_NS = 'http://schemas/AcheronMPMSupportingEntity/Custom_Metadata_Mapping/operations';
        this.GET_METADATA_MAPPING_WS_METHOD_NAME = 'GetMetadataForCategoryLevelName';
        this.metaDataFieldDataTypesMapping = {
            CHAR: 'string',
            DATE: OTMMDataTypes.DATE_TIME,
            NUMBER: 'decimal'
        };
        this.defaultProjectMetadataFields = [];
        this.customProjectMetadataFields = [];
        this.defaultTaskMetadataFields = [];
        this.customTaskMetadataFields = [];
        this.defaultDeliverableMetadataFields = [];
        this.customDeliverableMetadataFields = [];
    }
    OtmmMetadataService.prototype.setDefaultProjectMetadataFields = function (metadataFields) {
        this.defaultProjectMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getDefaultProjectMetadataFields = function () {
        return this.defaultProjectMetadataFields;
    };
    OtmmMetadataService.prototype.setCustomProjectMetadataFields = function (metadataFields) {
        this.customProjectMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getCustomProjectMetadataFields = function () {
        return this.customProjectMetadataFields;
    };
    OtmmMetadataService.prototype.setDefaultTaskMetadataFields = function (metadataFields) {
        this.defaultTaskMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getDefaultTaskMetadataFields = function () {
        return this.defaultTaskMetadataFields;
    };
    OtmmMetadataService.prototype.setCustomTaskMetadataFields = function (metadataFields) {
        this.customTaskMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getCustomTaskMetadataFields = function () {
        return this.customTaskMetadataFields;
    };
    OtmmMetadataService.prototype.setDefaultDeliverableMetadataFields = function (metadataFields) {
        this.defaultDeliverableMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getDefaultDeliverableMetadataFields = function () {
        return this.defaultDeliverableMetadataFields;
    };
    OtmmMetadataService.prototype.setCustomDeliverableMetadataFields = function (metadataFields) {
        this.customDeliverableMetadataFields = metadataFields;
    };
    OtmmMetadataService.prototype.getCustomDeliverableMetadataFields = function () {
        return this.customDeliverableMetadataFields;
    };
    OtmmMetadataService.prototype.getMetadataFieldType = function (metadatModelFieldDataType) {
        var dataType = this.metaDataFieldDataTypesMapping[metadatModelFieldDataType];
        if (!dataType) {
            dataType = 'string';
        }
        return dataType;
    };
    OtmmMetadataService.prototype.getMetaData = function (categoryId, categoryLevelName) {
        var _this = this;
        var getRequestObject = {
            CategoryID: categoryId,
            CategoryLevelName: categoryLevelName
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.METADATA_MAPPING_METHOD_NS, _this.GET_METADATA_MAPPING_WS_METHOD_NAME, getRequestObject)
                .subscribe(function (metadataModelDetails) {
                _this.otmmService.getMetadatModelById(metadataModelDetails.Custom_Metadata_Mapping.METADATA_MODEL_ID)
                    .subscribe(function (metaDataModelDetails) {
                    observer.next(metaDataModelDetails);
                    observer.complete();
                }, function (metadataError) {
                    observer.error(metadataError);
                });
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OtmmMetadataService.prototype.getFieldValueById = function (metaDataValues, fieldId, isDisplayValue) {
        var e_1, _a;
        var _b;
        if (!metaDataValues || !metaDataValues.metadata_element_list) {
            return null;
        }
        try {
            for (var _c = __values(metaDataValues.metadata_element_list), _d = _c.next(); !_d.done; _d = _c.next()) {
                var metdataGroup = _d.value;
                var metadataList = (_b = metdataGroup) === null || _b === void 0 ? void 0 : _b.metadata_element_list.find(function (tmpMedataList) {
                    return (tmpMedataList.id === fieldId) || (tmpMedataList.referenceValue && tmpMedataList.referenceValue === fieldId);
                });
                if (metadataList) {
                    return this.getFieldValue(metadataList, isDisplayValue);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return null;
    };
    OtmmMetadataService.prototype.getFieldValue = function (selectedMetadataField, isDisplayValue) {
        var value = '';
        if (selectedMetadataField && selectedMetadataField.value && selectedMetadataField.value.value) {
            if (selectedMetadataField && selectedMetadataField.value
                && selectedMetadataField.value.value
                && selectedMetadataField.value.value.field_value
                && selectedMetadataField.value.value.field_value.value
                && !isDisplayValue) {
                value = selectedMetadataField.value.value.field_value.value;
            }
            else {
                value = typeof selectedMetadataField.value.value.value !== 'undefined' ? selectedMetadataField.value.value.value : selectedMetadataField.value.value.display_value;
            }
        }
        if (value && selectedMetadataField.value && selectedMetadataField.value.value && selectedMetadataField.value.value.type && selectedMetadataField.value.value.type === OTMMDataTypes.DATE_TIME) {
            value = value.indexOf('+') > 0 ? value.replace('+', '-') : value.replace('-', '+');
            value = new Date(value).toISOString();
        }
        return value;
    };
    /**
     * @author SathishSrinivas
     * @param assetList
     * @param metadataList
     */
    OtmmMetadataService.prototype.setReferenceValueForMetadataFeilds = function (assetList, metadataList) {
        return assetList.map(function (eachAsset) {
            if (eachAsset.metadata && eachAsset.metadata.metadata_element_list
                && Array.isArray(eachAsset.metadata.metadata_element_list)) {
                eachAsset.metadata.metadata_element_list = eachAsset.metadata.metadata_element_list.map(function (eachCategory) {
                    if (eachCategory && eachCategory.metadata_element_list && Array.isArray(eachCategory.metadata_element_list)) {
                        eachCategory.metadata_element_list = eachCategory.metadata_element_list.map(function (eachMetadataFeild) {
                            if (eachMetadataFeild.type && typeof eachMetadataFeild.type === 'string' && eachMetadataFeild.type.indexOf('metadata.MetadataTable') > 0
                                && eachMetadataFeild.metadata_element_list && Array.isArray(eachMetadataFeild.metadata_element_list)) {
                                eachMetadataFeild.metadata_element_list = eachMetadataFeild.metadata_element_list.map(function (eachMetadataTableField) {
                                    var tempValue = acronui.findValueInList(metadataList, eachMetadataTableField.id, 'FIELD_ID');
                                    if (tempValue && tempValue.REFERENCE_VALUE) {
                                        eachMetadataTableField.referenceValue = tempValue.REFERENCE_VALUE;
                                    }
                                    return eachMetadataTableField;
                                });
                            }
                            else if (eachMetadataFeild.type && typeof eachMetadataFeild.type === 'string' && eachMetadataFeild.type.indexOf('metadata.MetadataField') > 0) {
                                var tempValue = acronui.findValueInList(metadataList, eachMetadataFeild.id, 'FIELD_ID');
                                if (tempValue && tempValue.REFERENCE_VALUE) {
                                    eachMetadataFeild.referenceValue = tempValue.REFERENCE_VALUE;
                                }
                            }
                            return eachMetadataFeild;
                        });
                    }
                    return eachCategory;
                });
            }
            return eachAsset;
        });
    };
    /**
     * @author SathishSrinivas
     * @param metadataFieldList
     * @param metadataFilterList
     * @param isDefaultMetadataField
     */
    OtmmMetadataService.prototype.getDisplayMetadataFields = function (metadataFieldList, metadataFilterList, isDefaultMetadataField) {
        if (!Array.isArray(metadataFieldList) || !Array.isArray(metadataFilterList)) {
            return [];
        }
        var resultList = [];
        metadataFieldList.map(function (field) {
            var fieldObj = metadataFilterList.find(function (e) { return (e.id === field.FIELD_ID) && (field.IS_GRID_VIEW === 'true'); });
            if (fieldObj) {
                fieldObj.sequence = field.DISPLAY_SEQUENCE && typeof field.DISPLAY_SEQUENCE === 'string' ? Number(field.DISPLAY_SEQUENCE) : 999;
                fieldObj.referenceValue = (field && field.REFERENCE_VALUE && typeof field.REFERENCE_VALUE === 'string') ? field.REFERENCE_VALUE : null;
                fieldObj.isDefaultMetadataField = isDefaultMetadataField;
                resultList.push(fieldObj);
            }
        });
        return resultList;
    };
    OtmmMetadataService.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService }
    ]; };
    OtmmMetadataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function OtmmMetadataService_Factory() { return new OtmmMetadataService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService)); }, token: OtmmMetadataService, providedIn: "root" });
    OtmmMetadataService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], OtmmMetadataService);
    return OtmmMetadataService;
}());
export { OtmmMetadataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RtbS1tZXRhZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL290bW0tbWV0YWRhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDcEUsT0FBTyxLQUFLLE9BQU8sTUFBTSw4QkFBOEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7Ozs7QUFLdEU7SUFrQkksNkJBQ1csVUFBc0IsRUFDdEIsV0FBd0I7UUFEeEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWxCbkMsK0JBQTBCLEdBQUcsOEVBQThFLENBQUM7UUFDNUcsd0NBQW1DLEdBQUcsaUNBQWlDLENBQUM7UUFFeEUsa0NBQTZCLEdBQUc7WUFDNUIsSUFBSSxFQUFFLFFBQVE7WUFDZCxJQUFJLEVBQUUsYUFBYSxDQUFDLFNBQVM7WUFDN0IsTUFBTSxFQUFFLFNBQVM7U0FDcEIsQ0FBQztRQUVLLGlDQUE0QixHQUFHLEVBQUUsQ0FBQztRQUNsQyxnQ0FBMkIsR0FBRyxFQUFFLENBQUM7UUFDakMsOEJBQXlCLEdBQUcsRUFBRSxDQUFDO1FBQy9CLDZCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUM5QixxQ0FBZ0MsR0FBRyxFQUFFLENBQUM7UUFDdEMsb0NBQStCLEdBQUcsRUFBRSxDQUFDO0lBS3hDLENBQUM7SUFFTCw2REFBK0IsR0FBL0IsVUFBZ0MsY0FBYztRQUMxQyxJQUFJLENBQUMsNEJBQTRCLEdBQUcsY0FBYyxDQUFDO0lBQ3ZELENBQUM7SUFFRCw2REFBK0IsR0FBL0I7UUFDSSxPQUFPLElBQUksQ0FBQyw0QkFBNEIsQ0FBQztJQUM3QyxDQUFDO0lBRUQsNERBQThCLEdBQTlCLFVBQStCLGNBQWM7UUFDekMsSUFBSSxDQUFDLDJCQUEyQixHQUFHLGNBQWMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsNERBQThCLEdBQTlCO1FBQ0ksT0FBTyxJQUFJLENBQUMsMkJBQTJCLENBQUM7SUFDNUMsQ0FBQztJQUVELDBEQUE0QixHQUE1QixVQUE2QixjQUFjO1FBQ3ZDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxjQUFjLENBQUM7SUFDcEQsQ0FBQztJQUVELDBEQUE0QixHQUE1QjtRQUNJLE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDO0lBQzFDLENBQUM7SUFFRCx5REFBMkIsR0FBM0IsVUFBNEIsY0FBYztRQUN0QyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsY0FBYyxDQUFDO0lBQ25ELENBQUM7SUFFRCx5REFBMkIsR0FBM0I7UUFDSSxPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN6QyxDQUFDO0lBRUQsaUVBQW1DLEdBQW5DLFVBQW9DLGNBQWM7UUFDOUMsSUFBSSxDQUFDLGdDQUFnQyxHQUFHLGNBQWMsQ0FBQztJQUMzRCxDQUFDO0lBRUQsaUVBQW1DLEdBQW5DO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0NBQWdDLENBQUM7SUFDakQsQ0FBQztJQUVELGdFQUFrQyxHQUFsQyxVQUFtQyxjQUFjO1FBQzdDLElBQUksQ0FBQywrQkFBK0IsR0FBRyxjQUFjLENBQUM7SUFDMUQsQ0FBQztJQUVELGdFQUFrQyxHQUFsQztRQUNJLE9BQU8sSUFBSSxDQUFDLCtCQUErQixDQUFDO0lBQ2hELENBQUM7SUFFRCxrREFBb0IsR0FBcEIsVUFBcUIseUJBQXlCO1FBQzFDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxRQUFRLEdBQUcsUUFBUSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVELHlDQUFXLEdBQVgsVUFBWSxVQUFVLEVBQUUsaUJBQWlCO1FBQXpDLGlCQW1CQztRQWxCRyxJQUFNLGdCQUFnQixHQUFHO1lBQ3JCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLGlCQUFpQixFQUFFLGlCQUFpQjtTQUN2QyxDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLDBCQUEwQixFQUFFLEtBQUksQ0FBQyxtQ0FBbUMsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDckgsU0FBUyxDQUFDLFVBQUEsb0JBQW9CO2dCQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDO3FCQUMvRixTQUFTLENBQUMsVUFBQSxvQkFBb0I7b0JBQzNCLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDcEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxhQUFhO29CQUNaLFFBQVEsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCLFVBQWtCLGNBQWMsRUFBRSxPQUFPLEVBQUUsY0FBZTs7O1FBQ3RELElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUM7U0FDZjs7WUFDRCxLQUEyQixJQUFBLEtBQUEsU0FBQSxjQUFjLENBQUMscUJBQXFCLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQTVELElBQU0sWUFBWSxXQUFBO2dCQUNuQixJQUFNLFlBQVksU0FBRyxZQUFZLDBDQUFFLHFCQUFxQixDQUFDLElBQUksQ0FDekQsVUFBQSxhQUFhO29CQUNULE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRSxLQUFLLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsSUFBSSxhQUFhLENBQUMsY0FBYyxLQUFLLE9BQU8sQ0FBQyxDQUFDO2dCQUN4SCxDQUFDLENBQ0osQ0FBQztnQkFDRixJQUFJLFlBQVksRUFBRTtvQkFDZCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxFQUFFLGNBQWMsQ0FBQyxDQUFDO2lCQUMzRDthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsMkNBQWEsR0FBYixVQUFjLHFCQUFxQixFQUFFLGNBQWU7UUFDaEQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxxQkFBcUIsSUFBSSxxQkFBcUIsQ0FBQyxLQUFLLElBQUkscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtZQUMzRixJQUFJLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLEtBQUs7bUJBQ2pELHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLO21CQUNqQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVc7bUJBQzdDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUs7bUJBQ25ELENBQUMsY0FBYyxFQUFFO2dCQUNwQixLQUFLLEdBQUcscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2FBQy9EO2lCQUFNO2dCQUNILEtBQUssR0FBRyxPQUFPLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDO2FBQ3RLO1NBQ0o7UUFDRCxJQUFJLEtBQUssSUFBSSxxQkFBcUIsQ0FBQyxLQUFLLElBQUkscUJBQXFCLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsU0FBUyxFQUFDO1lBQzFMLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ25GLEtBQUssR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6QztRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFDRDs7OztPQUlHO0lBQ0gsZ0VBQWtDLEdBQWxDLFVBQW1DLFNBQXFCLEVBQUUsWUFBd0I7UUFDOUUsT0FBTyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQUEsU0FBUztZQUMxQixJQUFJLFNBQVMsQ0FBQyxRQUFRLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUI7bUJBQzNELEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFO2dCQUM1RCxTQUFTLENBQUMsUUFBUSxDQUFDLHFCQUFxQixHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLFVBQUEsWUFBWTtvQkFDaEcsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLHFCQUFxQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7d0JBQ3pHLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLFVBQUEsaUJBQWlCOzRCQUN6RixJQUFJLGlCQUFpQixDQUFDLElBQUksSUFBSSxPQUFPLGlCQUFpQixDQUFDLElBQUksS0FBSyxRQUFRLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUM7bUNBQ2pJLGlCQUFpQixDQUFDLHFCQUFxQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsRUFBRTtnQ0FDdEcsaUJBQWlCLENBQUMscUJBQXFCLEdBQUcsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLFVBQUEsc0JBQXNCO29DQUN4RyxJQUFNLFNBQVMsR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxzQkFBc0IsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUM7b0NBQy9GLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxlQUFlLEVBQUU7d0NBQ3hDLHNCQUFzQixDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUMsZUFBZSxDQUFDO3FDQUNyRTtvQ0FDRCxPQUFPLHNCQUFzQixDQUFDO2dDQUNsQyxDQUFDLENBQUMsQ0FBQzs2QkFDTjtpQ0FBTSxJQUFJLGlCQUFpQixDQUFDLElBQUksSUFBSSxPQUFPLGlCQUFpQixDQUFDLElBQUksS0FBSyxRQUFRLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQ0FDN0ksSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dDQUMxRixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsZUFBZSxFQUFFO29DQUN4QyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQztpQ0FDaEU7NkJBQ0o7NEJBQ0QsT0FBTyxpQkFBaUIsQ0FBQzt3QkFDN0IsQ0FBQyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsT0FBTyxZQUFZLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047WUFDRCxPQUFPLFNBQVMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRDs7Ozs7T0FLRztJQUNILHNEQUF3QixHQUF4QixVQUF5QixpQkFBNkIsRUFBRSxrQkFBOEIsRUFBRSxzQkFBK0I7UUFDbkgsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsRUFBRTtZQUN6RSxPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsSUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxVQUFBLEtBQUs7WUFDdkIsSUFBTSxRQUFRLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEtBQUssTUFBTSxDQUFDLEVBQTVELENBQTRELENBQUMsQ0FBQztZQUM1RyxJQUFJLFFBQVEsRUFBRTtnQkFDVixRQUFRLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxnQkFBZ0IsSUFBSSxPQUFPLEtBQUssQ0FBQyxnQkFBZ0IsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2dCQUNoSSxRQUFRLENBQUMsY0FBYyxHQUFHLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxlQUFlLElBQUksT0FBTyxLQUFLLENBQUMsZUFBZSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZJLFFBQVEsQ0FBQyxzQkFBc0IsR0FBRyxzQkFBc0IsQ0FBQztnQkFDekQsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUM3QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Z0JBOUtzQixVQUFVO2dCQUNULFdBQVc7OztJQXBCMUIsbUJBQW1CO1FBSC9CLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FDVyxtQkFBbUIsQ0FrTS9COzhCQTVNRDtDQTRNQyxBQWxNRCxJQWtNQztTQWxNWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IE9UTU1EYXRhVHlwZXMgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9PVE1NRGF0YVR5cGVzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgT3RtbU1ldGFkYXRhU2VydmljZSB7XHJcblxyXG4gICAgTUVUQURBVEFfTUFQUElOR19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTVN1cHBvcnRpbmdFbnRpdHkvQ3VzdG9tX01ldGFkYXRhX01hcHBpbmcvb3BlcmF0aW9ucyc7XHJcbiAgICBHRVRfTUVUQURBVEFfTUFQUElOR19XU19NRVRIT0RfTkFNRSA9ICdHZXRNZXRhZGF0YUZvckNhdGVnb3J5TGV2ZWxOYW1lJztcclxuXHJcbiAgICBtZXRhRGF0YUZpZWxkRGF0YVR5cGVzTWFwcGluZyA9IHtcclxuICAgICAgICBDSEFSOiAnc3RyaW5nJyxcclxuICAgICAgICBEQVRFOiBPVE1NRGF0YVR5cGVzLkRBVEVfVElNRSxcclxuICAgICAgICBOVU1CRVI6ICdkZWNpbWFsJ1xyXG4gICAgfTtcclxuXHJcbiAgICBwdWJsaWMgZGVmYXVsdFByb2plY3RNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgcHVibGljIGN1c3RvbVByb2plY3RNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgcHVibGljIGRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuICAgIHB1YmxpYyBjdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuICAgIHB1YmxpYyBkZWZhdWx0RGVsaXZlcmFibGVNZXRhZGF0YUZpZWxkcyA9IFtdO1xyXG4gICAgcHVibGljIGN1c3RvbURlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHMgPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHNldERlZmF1bHRQcm9qZWN0TWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZHMpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRQcm9qZWN0TWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0UHJvamVjdE1ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRQcm9qZWN0TWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VzdG9tUHJvamVjdE1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Qcm9qZWN0TWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXN0b21Qcm9qZWN0TWV0YWRhdGFGaWVsZHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VzdG9tUHJvamVjdE1ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldERlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZHMpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0VGFza01ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHRUYXNrTWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VzdG9tVGFza01ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXN0b21UYXNrTWV0YWRhdGFGaWVsZHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VzdG9tVGFza01ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldERlZmF1bHREZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0RGVsaXZlcmFibGVNZXRhZGF0YUZpZWxkcyA9IG1ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlZmF1bHREZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHREZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1c3RvbURlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHMobWV0YWRhdGFGaWVsZHMpIHtcclxuICAgICAgICB0aGlzLmN1c3RvbURlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHMgPSBtZXRhZGF0YUZpZWxkcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXN0b21EZWxpdmVyYWJsZU1ldGFkYXRhRmllbGRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1c3RvbURlbGl2ZXJhYmxlTWV0YWRhdGFGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWV0YWRhdGFGaWVsZFR5cGUobWV0YWRhdE1vZGVsRmllbGREYXRhVHlwZSkge1xyXG4gICAgICAgIGxldCBkYXRhVHlwZSA9IHRoaXMubWV0YURhdGFGaWVsZERhdGFUeXBlc01hcHBpbmdbbWV0YWRhdE1vZGVsRmllbGREYXRhVHlwZV07XHJcbiAgICAgICAgaWYgKCFkYXRhVHlwZSkge1xyXG4gICAgICAgICAgICBkYXRhVHlwZSA9ICdzdHJpbmcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZGF0YVR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWV0YURhdGEoY2F0ZWdvcnlJZCwgY2F0ZWdvcnlMZXZlbE5hbWUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIENhdGVnb3J5SUQ6IGNhdGVnb3J5SWQsXHJcbiAgICAgICAgICAgIENhdGVnb3J5TGV2ZWxOYW1lOiBjYXRlZ29yeUxldmVsTmFtZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5NRVRBREFUQV9NQVBQSU5HX01FVEhPRF9OUywgdGhpcy5HRVRfTUVUQURBVEFfTUFQUElOR19XU19NRVRIT0RfTkFNRSwgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobWV0YWRhdGFNb2RlbERldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlcnZpY2UuZ2V0TWV0YWRhdE1vZGVsQnlJZChtZXRhZGF0YU1vZGVsRGV0YWlscy5DdXN0b21fTWV0YWRhdGFfTWFwcGluZy5NRVRBREFUQV9NT0RFTF9JRClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShtZXRhRGF0YU1vZGVsRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KG1ldGFEYXRhTW9kZWxEZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIG1ldGFkYXRhRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobWV0YWRhdGFFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZpZWxkVmFsdWVCeUlkKG1ldGFEYXRhVmFsdWVzLCBmaWVsZElkLCBpc0Rpc3BsYXlWYWx1ZT8pIHtcclxuICAgICAgICBpZiAoIW1ldGFEYXRhVmFsdWVzIHx8ICFtZXRhRGF0YVZhbHVlcy5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoY29uc3QgbWV0ZGF0YUdyb3VwIG9mIG1ldGFEYXRhVmFsdWVzLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkge1xyXG4gICAgICAgICAgICBjb25zdCBtZXRhZGF0YUxpc3QgPSBtZXRkYXRhR3JvdXA/Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdC5maW5kKFxyXG4gICAgICAgICAgICAgICAgdG1wTWVkYXRhTGlzdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICh0bXBNZWRhdGFMaXN0LmlkID09PSBmaWVsZElkKSB8fCAodG1wTWVkYXRhTGlzdC5yZWZlcmVuY2VWYWx1ZSAmJiB0bXBNZWRhdGFMaXN0LnJlZmVyZW5jZVZhbHVlID09PSBmaWVsZElkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgaWYgKG1ldGFkYXRhTGlzdCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RmllbGRWYWx1ZShtZXRhZGF0YUxpc3QsIGlzRGlzcGxheVZhbHVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWVsZFZhbHVlKHNlbGVjdGVkTWV0YWRhdGFGaWVsZCwgaXNEaXNwbGF5VmFsdWU/KSB7XHJcbiAgICAgICAgbGV0IHZhbHVlID0gJyc7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkTWV0YWRhdGFGaWVsZCAmJiBzZWxlY3RlZE1ldGFkYXRhRmllbGQudmFsdWUgJiYgc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZE1ldGFkYXRhRmllbGQgJiYgc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlXHJcbiAgICAgICAgICAgICAgICAmJiBzZWxlY3RlZE1ldGFkYXRhRmllbGQudmFsdWUudmFsdWVcclxuICAgICAgICAgICAgICAgICYmIHNlbGVjdGVkTWV0YWRhdGFGaWVsZC52YWx1ZS52YWx1ZS5maWVsZF92YWx1ZVxyXG4gICAgICAgICAgICAgICAgJiYgc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlLnZhbHVlLmZpZWxkX3ZhbHVlLnZhbHVlXHJcbiAgICAgICAgICAgICAgICAmJiAhaXNEaXNwbGF5VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlLnZhbHVlLmZpZWxkX3ZhbHVlLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSB0eXBlb2Ygc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlLnZhbHVlLnZhbHVlICE9PSAndW5kZWZpbmVkJyA/IHNlbGVjdGVkTWV0YWRhdGFGaWVsZC52YWx1ZS52YWx1ZS52YWx1ZSA6IHNlbGVjdGVkTWV0YWRhdGFGaWVsZC52YWx1ZS52YWx1ZS5kaXNwbGF5X3ZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh2YWx1ZSAmJiBzZWxlY3RlZE1ldGFkYXRhRmllbGQudmFsdWUgJiYgc2VsZWN0ZWRNZXRhZGF0YUZpZWxkLnZhbHVlLnZhbHVlICYmIHNlbGVjdGVkTWV0YWRhdGFGaWVsZC52YWx1ZS52YWx1ZS50eXBlICYmIHNlbGVjdGVkTWV0YWRhdGFGaWVsZC52YWx1ZS52YWx1ZS50eXBlID09PSBPVE1NRGF0YVR5cGVzLkRBVEVfVElNRSl7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdmFsdWUuaW5kZXhPZignKycpID4gMCA/IHZhbHVlLnJlcGxhY2UoJysnLCAnLScpIDogdmFsdWUucmVwbGFjZSgnLScsICcrJyk7XHJcbiAgICAgICAgICAgIHZhbHVlID0gbmV3IERhdGUodmFsdWUpLnRvSVNPU3RyaW5nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogQGF1dGhvciBTYXRoaXNoU3Jpbml2YXNcclxuICAgICAqIEBwYXJhbSBhc3NldExpc3RcclxuICAgICAqIEBwYXJhbSBtZXRhZGF0YUxpc3RcclxuICAgICAqL1xyXG4gICAgc2V0UmVmZXJlbmNlVmFsdWVGb3JNZXRhZGF0YUZlaWxkcyhhc3NldExpc3Q6IEFycmF5PGFueT4sIG1ldGFkYXRhTGlzdDogQXJyYXk8YW55Pik6IEFycmF5PGFueT4ge1xyXG4gICAgICAgIHJldHVybiBhc3NldExpc3QubWFwKGVhY2hBc3NldCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlYWNoQXNzZXQubWV0YWRhdGEgJiYgZWFjaEFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdFxyXG4gICAgICAgICAgICAgICAgJiYgQXJyYXkuaXNBcnJheShlYWNoQXNzZXQubWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0KSkge1xyXG4gICAgICAgICAgICAgICAgZWFjaEFzc2V0Lm1ldGFkYXRhLm1ldGFkYXRhX2VsZW1lbnRfbGlzdCA9IGVhY2hBc3NldC5tZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QubWFwKGVhY2hDYXRlZ29yeSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVhY2hDYXRlZ29yeSAmJiBlYWNoQ2F0ZWdvcnkubWV0YWRhdGFfZWxlbWVudF9saXN0ICYmIEFycmF5LmlzQXJyYXkoZWFjaENhdGVnb3J5Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWFjaENhdGVnb3J5Lm1ldGFkYXRhX2VsZW1lbnRfbGlzdCA9IGVhY2hDYXRlZ29yeS5tZXRhZGF0YV9lbGVtZW50X2xpc3QubWFwKGVhY2hNZXRhZGF0YUZlaWxkID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlYWNoTWV0YWRhdGFGZWlsZC50eXBlICYmIHR5cGVvZiBlYWNoTWV0YWRhdGFGZWlsZC50eXBlID09PSAnc3RyaW5nJyAmJiBlYWNoTWV0YWRhdGFGZWlsZC50eXBlLmluZGV4T2YoJ21ldGFkYXRhLk1ldGFkYXRhVGFibGUnKSA+IDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiBlYWNoTWV0YWRhdGFGZWlsZC5tZXRhZGF0YV9lbGVtZW50X2xpc3QgJiYgQXJyYXkuaXNBcnJheShlYWNoTWV0YWRhdGFGZWlsZC5tZXRhZGF0YV9lbGVtZW50X2xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWFjaE1ldGFkYXRhRmVpbGQubWV0YWRhdGFfZWxlbWVudF9saXN0ID0gZWFjaE1ldGFkYXRhRmVpbGQubWV0YWRhdGFfZWxlbWVudF9saXN0Lm1hcChlYWNoTWV0YWRhdGFUYWJsZUZpZWxkID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVtcFZhbHVlID0gYWNyb251aS5maW5kVmFsdWVJbkxpc3QobWV0YWRhdGFMaXN0LCBlYWNoTWV0YWRhdGFUYWJsZUZpZWxkLmlkLCAnRklFTERfSUQnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRlbXBWYWx1ZSAmJiB0ZW1wVmFsdWUuUkVGRVJFTkNFX1ZBTFVFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlYWNoTWV0YWRhdGFUYWJsZUZpZWxkLnJlZmVyZW5jZVZhbHVlID0gdGVtcFZhbHVlLlJFRkVSRU5DRV9WQUxVRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWFjaE1ldGFkYXRhVGFibGVGaWVsZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZWFjaE1ldGFkYXRhRmVpbGQudHlwZSAmJiB0eXBlb2YgZWFjaE1ldGFkYXRhRmVpbGQudHlwZSA9PT0gJ3N0cmluZycgJiYgZWFjaE1ldGFkYXRhRmVpbGQudHlwZS5pbmRleE9mKCdtZXRhZGF0YS5NZXRhZGF0YUZpZWxkJykgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVtcFZhbHVlID0gYWNyb251aS5maW5kVmFsdWVJbkxpc3QobWV0YWRhdGFMaXN0LCBlYWNoTWV0YWRhdGFGZWlsZC5pZCwgJ0ZJRUxEX0lEJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRlbXBWYWx1ZSAmJiB0ZW1wVmFsdWUuUkVGRVJFTkNFX1ZBTFVFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVhY2hNZXRhZGF0YUZlaWxkLnJlZmVyZW5jZVZhbHVlID0gdGVtcFZhbHVlLlJFRkVSRU5DRV9WQUxVRTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWFjaE1ldGFkYXRhRmVpbGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWFjaENhdGVnb3J5O1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGVhY2hBc3NldDtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogQGF1dGhvciBTYXRoaXNoU3Jpbml2YXNcclxuICAgICAqIEBwYXJhbSBtZXRhZGF0YUZpZWxkTGlzdFxyXG4gICAgICogQHBhcmFtIG1ldGFkYXRhRmlsdGVyTGlzdFxyXG4gICAgICogQHBhcmFtIGlzRGVmYXVsdE1ldGFkYXRhRmllbGRcclxuICAgICAqL1xyXG4gICAgZ2V0RGlzcGxheU1ldGFkYXRhRmllbGRzKG1ldGFkYXRhRmllbGRMaXN0OiBBcnJheTxhbnk+LCBtZXRhZGF0YUZpbHRlckxpc3Q6IEFycmF5PGFueT4sIGlzRGVmYXVsdE1ldGFkYXRhRmllbGQ6IGJvb2xlYW4pIHtcclxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkobWV0YWRhdGFGaWVsZExpc3QpIHx8ICFBcnJheS5pc0FycmF5KG1ldGFkYXRhRmlsdGVyTGlzdCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCByZXN1bHRMaXN0ID0gW107XHJcbiAgICAgICAgbWV0YWRhdGFGaWVsZExpc3QubWFwKGZpZWxkID0+IHtcclxuICAgICAgICAgICAgY29uc3QgZmllbGRPYmogPSBtZXRhZGF0YUZpbHRlckxpc3QuZmluZChlID0+IChlLmlkID09PSBmaWVsZC5GSUVMRF9JRCkgJiYgKGZpZWxkLklTX0dSSURfVklFVyA9PT0gJ3RydWUnKSk7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZE9iaikge1xyXG4gICAgICAgICAgICAgICAgZmllbGRPYmouc2VxdWVuY2UgPSBmaWVsZC5ESVNQTEFZX1NFUVVFTkNFICYmIHR5cGVvZiBmaWVsZC5ESVNQTEFZX1NFUVVFTkNFID09PSAnc3RyaW5nJyA/IE51bWJlcihmaWVsZC5ESVNQTEFZX1NFUVVFTkNFKSA6IDk5OTtcclxuICAgICAgICAgICAgICAgIGZpZWxkT2JqLnJlZmVyZW5jZVZhbHVlID0gKGZpZWxkICYmIGZpZWxkLlJFRkVSRU5DRV9WQUxVRSAmJiB0eXBlb2YgZmllbGQuUkVGRVJFTkNFX1ZBTFVFID09PSAnc3RyaW5nJykgPyBmaWVsZC5SRUZFUkVOQ0VfVkFMVUUgOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgZmllbGRPYmouaXNEZWZhdWx0TWV0YWRhdGFGaWVsZCA9IGlzRGVmYXVsdE1ldGFkYXRhRmllbGQ7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRMaXN0LnB1c2goZmllbGRPYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdExpc3Q7XHJcbiAgICB9XHJcbn1cclxuIl19