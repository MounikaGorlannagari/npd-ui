import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import { Observable } from 'rxjs';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../notification/notification.service";
var EmailDataService = /** @class */ (function () {
    function EmailDataService(appService, notificationService) {
        this.appService = appService;
        this.notificationService = notificationService;
        this.EmailSearchDataMapNS = 'http://schemas/AcheronMPMCore/Email_Search_Data_Map/operations';
        this.EmailSearchDataMapWS = 'GetEmailSearchDataMapById';
    }
    EmailDataService.prototype.getEmailSearchDataByGuid = function (guid) {
        var _this = this;
        return new Observable(function (observer) {
            if (!guid) {
                observer.error(new Error('Manadatory parameter GUID not passed'));
                observer.complete();
            }
            var param = {
                guid: guid
            };
            _this.appService.invokeRequest(_this.EmailSearchDataMapNS, _this.EmailSearchDataMapWS, param).subscribe(function (data) {
                var emailSearchData = acronui.findObjectByProp(data, 'Email_Search_Data_Map');
                if (emailSearchData && emailSearchData.SEARCH_CONDITION) {
                    emailSearchData.SEARCH_CONDITION = JSON.parse(emailSearchData.SEARCH_CONDITION);
                }
                observer.next(emailSearchData);
                observer.complete();
            }, function (err) {
                _this.notificationService.error('Error while getting email details');
                observer.error();
                observer.complete();
            });
        });
    };
    EmailDataService.ctorParameters = function () { return [
        { type: AppService },
        { type: NotificationService }
    ]; };
    EmailDataService.ɵprov = i0.ɵɵdefineInjectable({ factory: function EmailDataService_Factory() { return new EmailDataService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.NotificationService)); }, token: EmailDataService, providedIn: "root" });
    EmailDataService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], EmailDataService);
    return EmailDataService;
}());
export { EmailDataService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1haWwtZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2VtYWlsLWRhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDOzs7O0FBSzlFO0lBS0UsMEJBQ1MsVUFBc0IsRUFDdEIsbUJBQXdDO1FBRHhDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUwxQyx5QkFBb0IsR0FBRyxnRUFBZ0UsQ0FBQztRQUN4Rix5QkFBb0IsR0FBRywyQkFBMkIsQ0FBQztJQUt0RCxDQUFDO0lBRUwsbURBQXdCLEdBQXhCLFVBQXlCLElBQVk7UUFBckMsaUJBc0JDO1FBckJDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ1QsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNyQjtZQUNELElBQU0sS0FBSyxHQUFHO2dCQUNaLElBQUksTUFBQTthQUNMLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7Z0JBQ3ZHLElBQU0sZUFBZSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFDaEYsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLGdCQUFnQixFQUFFO29CQUN2RCxlQUFlLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztpQkFDakY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxVQUFBLEdBQUc7Z0JBQ0osS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUNwRSxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBMUJvQixVQUFVO2dCQUNELG1CQUFtQjs7O0lBUHRDLGdCQUFnQjtRQUg1QixVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csZ0JBQWdCLENBa0M1QjsyQkE1Q0Q7Q0E0Q0MsQUFsQ0QsSUFrQ0M7U0FsQ1ksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBFbWFpbFNlYXJjaERhdGFNYXAgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvb2JqZWN0cy9FbWFpbFNlYXJjaERhdGFNYXAnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVtYWlsRGF0YVNlcnZpY2Uge1xyXG5cclxuICBwdWJsaWMgRW1haWxTZWFyY2hEYXRhTWFwTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRW1haWxfU2VhcmNoX0RhdGFfTWFwL29wZXJhdGlvbnMnO1xyXG4gIHB1YmxpYyBFbWFpbFNlYXJjaERhdGFNYXBXUyA9ICdHZXRFbWFpbFNlYXJjaERhdGFNYXBCeUlkJztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgZ2V0RW1haWxTZWFyY2hEYXRhQnlHdWlkKGd1aWQ6IHN0cmluZyk6IE9ic2VydmFibGU8RW1haWxTZWFyY2hEYXRhTWFwPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBpZiAoIWd1aWQpIHtcclxuICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ01hbmFkYXRvcnkgcGFyYW1ldGVyIEdVSUQgbm90IHBhc3NlZCcpKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9XHJcbiAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgIGd1aWRcclxuICAgICAgfTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5FbWFpbFNlYXJjaERhdGFNYXBOUywgdGhpcy5FbWFpbFNlYXJjaERhdGFNYXBXUywgcGFyYW0pLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICBjb25zdCBlbWFpbFNlYXJjaERhdGEgPSBhY3JvbnVpLmZpbmRPYmplY3RCeVByb3AoZGF0YSwgJ0VtYWlsX1NlYXJjaF9EYXRhX01hcCcpO1xyXG4gICAgICAgIGlmIChlbWFpbFNlYXJjaERhdGEgJiYgZW1haWxTZWFyY2hEYXRhLlNFQVJDSF9DT05ESVRJT04pIHtcclxuICAgICAgICAgIGVtYWlsU2VhcmNoRGF0YS5TRUFSQ0hfQ09ORElUSU9OID0gSlNPTi5wYXJzZShlbWFpbFNlYXJjaERhdGEuU0VBUkNIX0NPTkRJVElPTik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIG9ic2VydmVyLm5leHQoZW1haWxTZWFyY2hEYXRhKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgZ2V0dGluZyBlbWFpbCBkZXRhaWxzJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbn1cclxuIl19