import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import * as i0 from "@angular/core";
var ProjectBulkCountService = /** @class */ (function () {
    function ProjectBulkCountService() {
        this.projectId = [];
        this.projectDetails = [];
        this.selectedProjects = new SelectionModel(true, []);
    }
    ProjectBulkCountService.prototype.getProjectBulkCount = function (projectData) {
        if (projectData === true) {
            this.projectId = [];
            this.projectDetails = [];
        }
        else {
            if (this.projectId && !this.projectId.includes(projectData.ID)) {
                this.projectId.push(projectData.ID);
                this.projectDetails.push(projectData);
            }
            else {
                var index = this.projectId.indexOf(projectData.ID);
                if (index > -1) {
                    this.projectId.splice(index, 1);
                    this.projectDetails.splice(index, 1);
                }
            }
        }
        //  this.projectId.push(projectData.ID);
        //  return this.projectId;
        return this.projectDetails;
    };
    ProjectBulkCountService.prototype.getProjectBulkData = function (projectData) {
        var _this = this;
        projectData.forEach(function (row) {
            row.selected = true;
            _this.selectedProjects.select(row);
        });
        return this.selectedProjects;
    };
    ProjectBulkCountService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProjectBulkCountService_Factory() { return new ProjectBulkCountService(); }, token: ProjectBulkCountService, providedIn: "root" });
    ProjectBulkCountService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ProjectBulkCountService);
    return ProjectBulkCountService;
}());
export { ProjectBulkCountService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvcHJvamVjdC1idWxrLWNvdW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDOztBQUsxRDtJQUlFO1FBSEEsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHFCQUFnQixHQUFHLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRWpCLHFEQUFtQixHQUFuQixVQUFvQixXQUFXO1FBQzdCLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtZQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztTQUMxQjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFO2dCQUM5RCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUFNO2dCQUNMLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDckQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7aUJBQ3RDO2FBQ0Y7U0FDRjtRQUNELHdDQUF3QztRQUN4QywwQkFBMEI7UUFDMUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzdCLENBQUM7SUFFRCxvREFBa0IsR0FBbEIsVUFBbUIsV0FBVztRQUE5QixpQkFNQztRQUxDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHO1lBQ3JCLEdBQUcsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUMvQixDQUFDOztJQWpDVSx1QkFBdUI7UUFIbkMsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLHVCQUF1QixDQWtDbkM7a0NBeENEO0NBd0NDLEFBbENELElBa0NDO1NBbENZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUHJvamVjdEJ1bGtDb3VudFNlcnZpY2Uge1xyXG4gIHByb2plY3RJZCA9IFtdO1xyXG4gIHByb2plY3REZXRhaWxzID0gW107XHJcbiAgc2VsZWN0ZWRQcm9qZWN0cyA9IG5ldyBTZWxlY3Rpb25Nb2RlbCh0cnVlLCBbXSk7XHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgZ2V0UHJvamVjdEJ1bGtDb3VudChwcm9qZWN0RGF0YSkge1xyXG4gICAgaWYgKHByb2plY3REYXRhID09PSB0cnVlKSB7XHJcbiAgICAgIHRoaXMucHJvamVjdElkID0gW107XHJcbiAgICAgIHRoaXMucHJvamVjdERldGFpbHMgPSBbXTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLnByb2plY3RJZCAmJiAhdGhpcy5wcm9qZWN0SWQuaW5jbHVkZXMocHJvamVjdERhdGEuSUQpKSB7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0SWQucHVzaChwcm9qZWN0RGF0YS5JRCk7XHJcbiAgICAgICAgdGhpcy5wcm9qZWN0RGV0YWlscy5wdXNoKHByb2plY3REYXRhKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMucHJvamVjdElkLmluZGV4T2YocHJvamVjdERhdGEuSUQpO1xyXG4gICAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICB0aGlzLnByb2plY3RJZC5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgdGhpcy5wcm9qZWN0RGV0YWlscy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gIHRoaXMucHJvamVjdElkLnB1c2gocHJvamVjdERhdGEuSUQpO1xyXG4gICAgLy8gIHJldHVybiB0aGlzLnByb2plY3RJZDtcclxuICAgIHJldHVybiB0aGlzLnByb2plY3REZXRhaWxzO1xyXG4gIH1cclxuXHJcbiAgZ2V0UHJvamVjdEJ1bGtEYXRhKHByb2plY3REYXRhKSB7XHJcbiAgICBwcm9qZWN0RGF0YS5mb3JFYWNoKHJvdyA9PiB7XHJcbiAgICAgIHJvdy5zZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0cy5zZWxlY3Qocm93KTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRQcm9qZWN0cztcclxuICB9XHJcbn1cclxuIl19