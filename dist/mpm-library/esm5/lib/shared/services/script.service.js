import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { ScriptStore } from '../interface/script.store';
import * as i0 from "@angular/core";
var ScriptService = /** @class */ (function () {
    function ScriptService() {
        var _this = this;
        this.scripts = {};
        ScriptStore.forEach(function (script) {
            _this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }
    ScriptService.prototype.load = function () {
        var _this = this;
        var scripts = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            scripts[_i] = arguments[_i];
        }
        var promises = [];
        scripts.forEach(function (script) { return promises.push(_this.loadScript(script)); });
        return Promise.all(promises);
    };
    ScriptService.prototype.loadScript = function (name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // resolve if already loaded
            if (_this.scripts[name].loaded) {
                resolve({ script: name, loaded: true, status: 'Script already loaded.' });
            }
            else {
                // load script
                var script_1 = document.createElement('script');
                script_1.type = 'text/javascript';
                script_1.src = _this.scripts[name].src;
                if (script_1.readyState) { // IE
                    script_1.onreadystatechange = function () {
                        if (script_1.readyState === 'loaded' || script_1.readyState === 'complete') {
                            script_1.onreadystatechange = null;
                            _this.scripts[name].loaded = true;
                            resolve({ script: name, loaded: true, status: 'Loaded' });
                        }
                    };
                }
                else { // Others
                    script_1.onload = function () {
                        _this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    };
                }
                script_1.onerror = function (error) { return resolve({ script: name, loaded: false, status: 'Loaded' }); };
                document.getElementsByTagName('head')[0].appendChild(script_1);
            }
        });
    };
    ScriptService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ScriptService_Factory() { return new ScriptService(); }, token: ScriptService, providedIn: "root" });
    ScriptService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ScriptService);
    return ScriptService;
}());
export { ScriptService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2NyaXB0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvc2VydmljZXMvc2NyaXB0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDJCQUEyQixDQUFDOztBQU94RDtJQUlFO1FBQUEsaUJBT0M7UUFUTSxZQUFPLEdBQVEsRUFBRSxDQUFDO1FBR3ZCLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxNQUFXO1lBQzlCLEtBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHO2dCQUMxQixNQUFNLEVBQUUsS0FBSztnQkFDYixHQUFHLEVBQUUsTUFBTSxDQUFDLEdBQUc7YUFDaEIsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUFJLEdBQUo7UUFBQSxpQkFJQztRQUpJLGlCQUFvQjthQUFwQixVQUFvQixFQUFwQixxQkFBb0IsRUFBcEIsSUFBb0I7WUFBcEIsNEJBQW9COztRQUN2QixJQUFNLFFBQVEsR0FBVSxFQUFFLENBQUM7UUFDM0IsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFDLE1BQU0sSUFBSyxPQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUF0QyxDQUFzQyxDQUFDLENBQUM7UUFDcEUsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxrQ0FBVSxHQUFWLFVBQVcsSUFBWTtRQUF2QixpQkE0QkM7UUEzQkMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ2pDLDRCQUE0QjtZQUM1QixJQUFJLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFO2dCQUM3QixPQUFPLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLHdCQUF3QixFQUFFLENBQUMsQ0FBQzthQUMzRTtpQkFBTTtnQkFDTCxjQUFjO2dCQUNkLElBQU0sUUFBTSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2hELFFBQU0sQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7Z0JBQ2hDLFFBQU0sQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUM7Z0JBQ3BDLElBQUksUUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFHLEtBQUs7b0JBQzdCLFFBQU0sQ0FBQyxrQkFBa0IsR0FBRzt3QkFDMUIsSUFBSSxRQUFNLENBQUMsVUFBVSxLQUFLLFFBQVEsSUFBSSxRQUFNLENBQUMsVUFBVSxLQUFLLFVBQVUsRUFBRTs0QkFDdEUsUUFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQzs0QkFDakMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDOzRCQUNqQyxPQUFPLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7eUJBQzNEO29CQUNILENBQUMsQ0FBQztpQkFDSDtxQkFBTSxFQUFHLFNBQVM7b0JBQ2pCLFFBQU0sQ0FBQyxNQUFNLEdBQUc7d0JBQ2QsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO3dCQUNqQyxPQUFPLENBQUMsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7b0JBQzVELENBQUMsQ0FBQztpQkFDSDtnQkFDRCxRQUFNLENBQUMsT0FBTyxHQUFHLFVBQUMsS0FBVSxJQUFLLE9BQUEsT0FBTyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUExRCxDQUEwRCxDQUFDO2dCQUM1RixRQUFRLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFFBQU0sQ0FBQyxDQUFDO2FBQzlEO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztJQS9DVSxhQUFhO1FBSHpCLFVBQVUsQ0FBQztZQUNWLFVBQVUsRUFBRSxNQUFNO1NBQ25CLENBQUM7T0FDVyxhQUFhLENBZ0R6Qjt3QkF6REQ7Q0F5REMsQUFoREQsSUFnREM7U0FoRFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFNjcmlwdFN0b3JlIH0gZnJvbSAnLi4vaW50ZXJmYWNlL3NjcmlwdC5zdG9yZSc7XHJcblxyXG5kZWNsYXJlIHZhciBkb2N1bWVudDogYW55O1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2NyaXB0U2VydmljZSB7XHJcblxyXG4gIHB1YmxpYyBzY3JpcHRzOiBhbnkgPSB7fTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICBTY3JpcHRTdG9yZS5mb3JFYWNoKChzY3JpcHQ6IGFueSkgPT4ge1xyXG4gICAgICB0aGlzLnNjcmlwdHNbc2NyaXB0Lm5hbWVdID0ge1xyXG4gICAgICAgIGxvYWRlZDogZmFsc2UsXHJcbiAgICAgICAgc3JjOiBzY3JpcHQuc3JjXHJcbiAgICAgIH07XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGxvYWQoLi4uc2NyaXB0czogc3RyaW5nW10pIHtcclxuICAgIGNvbnN0IHByb21pc2VzOiBhbnlbXSA9IFtdO1xyXG4gICAgc2NyaXB0cy5mb3JFYWNoKChzY3JpcHQpID0+IHByb21pc2VzLnB1c2godGhpcy5sb2FkU2NyaXB0KHNjcmlwdCkpKTtcclxuICAgIHJldHVybiBQcm9taXNlLmFsbChwcm9taXNlcyk7XHJcbiAgfVxyXG5cclxuICBsb2FkU2NyaXB0KG5hbWU6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgLy8gcmVzb2x2ZSBpZiBhbHJlYWR5IGxvYWRlZFxyXG4gICAgICBpZiAodGhpcy5zY3JpcHRzW25hbWVdLmxvYWRlZCkge1xyXG4gICAgICAgIHJlc29sdmUoeyBzY3JpcHQ6IG5hbWUsIGxvYWRlZDogdHJ1ZSwgc3RhdHVzOiAnU2NyaXB0IGFscmVhZHkgbG9hZGVkLicgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gbG9hZCBzY3JpcHRcclxuICAgICAgICBjb25zdCBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuICAgICAgICBzY3JpcHQudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xyXG4gICAgICAgIHNjcmlwdC5zcmMgPSB0aGlzLnNjcmlwdHNbbmFtZV0uc3JjO1xyXG4gICAgICAgIGlmIChzY3JpcHQucmVhZHlTdGF0ZSkgeyAgLy8gSUVcclxuICAgICAgICAgIHNjcmlwdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzY3JpcHQucmVhZHlTdGF0ZSA9PT0gJ2xvYWRlZCcgfHwgc2NyaXB0LnJlYWR5U3RhdGUgPT09ICdjb21wbGV0ZScpIHtcclxuICAgICAgICAgICAgICBzY3JpcHQub25yZWFkeXN0YXRlY2hhbmdlID0gbnVsbDtcclxuICAgICAgICAgICAgICB0aGlzLnNjcmlwdHNbbmFtZV0ubG9hZGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICByZXNvbHZlKHsgc2NyaXB0OiBuYW1lLCBsb2FkZWQ6IHRydWUsIHN0YXR1czogJ0xvYWRlZCcgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHsgIC8vIE90aGVyc1xyXG4gICAgICAgICAgc2NyaXB0Lm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY3JpcHRzW25hbWVdLmxvYWRlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHJlc29sdmUoeyBzY3JpcHQ6IG5hbWUsIGxvYWRlZDogdHJ1ZSwgc3RhdHVzOiAnTG9hZGVkJyB9KTtcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNjcmlwdC5vbmVycm9yID0gKGVycm9yOiBhbnkpID0+IHJlc29sdmUoeyBzY3JpcHQ6IG5hbWUsIGxvYWRlZDogZmFsc2UsIHN0YXR1czogJ0xvYWRlZCcgfSk7XHJcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXS5hcHBlbmRDaGlsZChzY3JpcHQpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19