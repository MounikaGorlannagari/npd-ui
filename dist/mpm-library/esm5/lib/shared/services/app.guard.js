import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OTMMService } from '../../mpm-utils/services/otmm.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import { AppRouteService } from '../../login/services/app.route.service';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/otmm.service";
import * as i3 from "../../mpm-utils/services/sharing.service";
import * as i4 from "../../login/services/app.route.service";
var AppGuard = /** @class */ (function () {
    function AppGuard(appService, otmmService, sharingService, appRouteService) {
        this.appService = appService;
        this.otmmService = otmmService;
        this.sharingService = sharingService;
        this.appRouteService = appRouteService;
    }
    AppGuard.prototype.getCRActionsForUser = function () {
        var _this = this;
        return new Observable(function (observer) {
            var crActions = _this.sharingService.getCRActions();
            if (crActions && crActions.length > 0) {
                observer.next(true);
                observer.complete();
            }
            else {
                _this.appService.getPossibleCRActionsForUser().subscribe(function (response) {
                    _this.sharingService.setCRActions(acronui.findObjectsByProp(response, 'MPM_Teams'));
                    observer.next(true);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    AppGuard.prototype.getLookupDomains = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.otmmService.checkOtmmSession()
                .subscribe(function (sessionResponse) {
                var lookupDomains = _this.sharingService.getAllLookupDomains();
                if (lookupDomains && lookupDomains.length > 0) {
                    observer.next(true);
                    observer.complete();
                }
                else {
                    _this.otmmService.getAllLookupDomains()
                        .subscribe(function (lookupDomainsResponse) {
                        _this.sharingService.setAllLookupDomains(lookupDomainsResponse);
                        observer.next(true);
                        observer.complete();
                    }, function (lookupDomainsError) {
                        observer.error(lookupDomainsError);
                    });
                }
            }, function (sessionError) {
                _this.appRouteService.goToLogout();
                observer.error(sessionError);
            });
        });
    };
    AppGuard.prototype.canActivate = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.getLookupDomains()
                .subscribe(function (lookupDomainsResponse) {
                _this.getCRActionsForUser()
                    .subscribe(function (crActionsResponse) {
                    observer.next(true);
                    observer.complete();
                }, function (crActionsError) {
                    observer.next(false);
                    observer.complete();
                });
            }, function (lookupDomainsError) {
                observer.next(false);
                observer.complete();
            });
        });
    };
    AppGuard.ctorParameters = function () { return [
        { type: AppService },
        { type: OTMMService },
        { type: SharingService },
        { type: AppRouteService }
    ]; };
    AppGuard.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppGuard_Factory() { return new AppGuard(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.OTMMService), i0.ɵɵinject(i3.SharingService), i0.ɵɵinject(i4.AppRouteService)); }, token: AppGuard, providedIn: "root" });
    AppGuard = __decorate([
        Injectable({
            providedIn: 'root',
        })
    ], AppGuard);
    return AppGuard;
}());
export { AppGuard };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmd1YXJkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL2FwcC5ndWFyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDMUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDOzs7Ozs7QUFNeEQ7SUFFSSxrQkFDVyxVQUFzQixFQUN0QixXQUF3QixFQUN4QixjQUE4QixFQUM5QixlQUFnQztRQUhoQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7SUFDdkMsQ0FBQztJQUVMLHNDQUFtQixHQUFuQjtRQUFBLGlCQWdCQztRQWZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDckQsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ25DLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLDJCQUEyQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDNUQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNuRixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFnQixHQUFoQjtRQUFBLGlCQXdCQztRQXZCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFO2lCQUM5QixTQUFTLENBQUMsVUFBQSxlQUFlO2dCQUN0QixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFLENBQUM7Z0JBQ2hFLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUMzQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILEtBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEVBQUU7eUJBQ2pDLFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjt3QkFDNUIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3dCQUUvRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBRSxVQUFBLGtCQUFrQjt3QkFDakIsUUFBUSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2QyxDQUFDLENBQUMsQ0FBQztpQkFDVjtZQUNMLENBQUMsRUFBRSxVQUFBLFlBQVk7Z0JBQ1gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDbEMsUUFBUSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhCQUFXLEdBQVg7UUFBQSxpQkFpQkM7UUFoQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGdCQUFnQixFQUFFO2lCQUNsQixTQUFTLENBQUMsVUFBQSxxQkFBcUI7Z0JBQzVCLEtBQUksQ0FBQyxtQkFBbUIsRUFBRTtxQkFDckIsU0FBUyxDQUFDLFVBQUEsaUJBQWlCO29CQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLGNBQWM7b0JBQ2IsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBRSxVQUFBLGtCQUFrQjtnQkFDakIsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkFuRXNCLFVBQVU7Z0JBQ1QsV0FBVztnQkFDUixjQUFjO2dCQUNiLGVBQWU7OztJQU5sQyxRQUFRO1FBSnBCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxRQUFRLENBdUVwQjttQkFwRkQ7Q0FvRkMsQUF2RUQsSUF1RUM7U0F2RVksUUFBUSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FuQWN0aXZhdGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE9UTU1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFJvdXRlU2VydmljZSB9IGZyb20gJy4uLy4uL2xvZ2luL3NlcnZpY2VzL2FwcC5yb3V0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9hcHAuc2VydmljZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXBwR3VhcmQgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIG90bW1TZXJ2aWNlOiBPVE1NU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBSb3V0ZVNlcnZpY2U6IEFwcFJvdXRlU2VydmljZVxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBnZXRDUkFjdGlvbnNGb3JVc2VyKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY3JBY3Rpb25zID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDUkFjdGlvbnMoKTtcclxuICAgICAgICAgICAgaWYgKGNyQWN0aW9ucyAmJiBjckFjdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UG9zc2libGVDUkFjdGlvbnNGb3JVc2VyKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldENSQWN0aW9ucyhhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1RlYW1zJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldExvb2t1cERvbWFpbnMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmNoZWNrT3RtbVNlc3Npb24oKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShzZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxvb2t1cERvbWFpbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbExvb2t1cERvbWFpbnMoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobG9va3VwRG9tYWlucyAmJiBsb29rdXBEb21haW5zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXJ2aWNlLmdldEFsbExvb2t1cERvbWFpbnMoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb29rdXBEb21haW5zUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsTG9va3VwRG9tYWlucyhsb29rdXBEb21haW5zUmVzcG9uc2UpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBsb29rdXBEb21haW5zRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGxvb2t1cERvbWFpbnNFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBzZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwUm91dGVTZXJ2aWNlLmdvVG9Mb2dvdXQoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihzZXNzaW9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGUoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5nZXRMb29rdXBEb21haW5zKClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobG9va3VwRG9tYWluc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldENSQWN0aW9uc0ZvclVzZXIoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGNyQWN0aW9uc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBjckFjdGlvbnNFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSwgbG9va3VwRG9tYWluc0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19