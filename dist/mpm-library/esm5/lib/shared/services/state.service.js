import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { MenuConstants } from '../constants/menu.constants';
import * as i0 from "@angular/core";
var StateService = /** @class */ (function () {
    function StateService() {
        this.queryParams = {
            viewName: null,
            isListView: null,
            sortBy: null,
            sortOrder: null,
            pageNumber: null,
            pageSize: null,
            searchName: null,
            savedSearchName: null,
            advSearchData: null,
            facetData: null,
            viewByResource: null,
            menuName: null,
            isTemplate: null,
            campaignId: null,
            isEditTask: null,
            taskId: null,
            isNewDeliverable: null,
            isEditDeliverable: null,
            deliverableId: null,
            groupByFilter: null,
            deliverableTaskFilter: null,
            listDependentFilter: null,
            emailLinkId: null
        };
        this.routeParams = {
            campaignId: null,
            projectId: null,
            taskId: null
        };
        this.route = {
            type: null,
            params: null
        };
        this.routes = [];
        this.selectedCampaignAssetIds = [];
        this.selectedProjectAssetIds = [];
        this.allProjectAssets = [];
        this.allCampaignAssets = [];
    }
    StateService.prototype.resetData = function () {
        this.queryParams = {
            viewName: null,
            isListView: null,
            sortBy: null,
            sortOrder: null,
            pageNumber: null,
            pageSize: null,
            searchName: null,
            savedSearchName: null,
            advSearchData: null,
            facetData: null,
            viewByResource: null,
            menuName: null,
            isTemplate: null,
            campaignId: null,
            isEditTask: null,
            taskId: null,
            isNewDeliverable: null,
            isEditDeliverable: null,
            deliverableId: null,
            groupByFilter: null,
            deliverableTaskFilter: null,
            listDependentFilter: null,
            emailLinkId: null
        };
        this.routeParams = {
            campaignId: null,
            projectId: null,
            taskId: null
        };
        this.route = {
            type: null,
            params: null
        };
    };
    StateService.prototype.resetState = function () {
        this.routes = [];
    };
    StateService.prototype.setRouteParams = function (routeParams) {
        if (routeParams) {
            if (routeParams.projectId) {
                this.routeParams.projectId = routeParams.projectId;
            }
            if (routeParams.campaignId) {
                this.routeParams.campaignId = routeParams.campaignId;
            }
            if (routeParams.taskId) {
                this.routeParams.taskId = routeParams.taskId;
            }
        }
    };
    StateService.prototype.setQueryParams = function (queryParams) {
        if (queryParams) {
            if (queryParams.viewName) {
                this.queryParams.viewName = queryParams.viewName;
            }
            if (queryParams.isListView) {
                this.queryParams.isListView = queryParams.isListView;
            }
            if (queryParams.sortBy) {
                this.queryParams.sortBy = queryParams.sortBy;
            }
            if (queryParams.sortOrder) {
                this.queryParams.sortOrder = queryParams.sortOrder;
            }
            if (queryParams.pageNumber) {
                this.queryParams.pageNumber = queryParams.pageNumber;
            }
            if (queryParams.pageSize) {
                this.queryParams.pageSize = queryParams.pageSize;
            }
            if (queryParams.searchName) {
                this.queryParams.searchName = queryParams.searchName;
            }
            if (queryParams.savedSearchName) {
                this.queryParams.savedSearchName = queryParams.savedSearchName;
            }
            if (queryParams.advSearchData) {
                this.queryParams.advSearchData = queryParams.advSearchData;
            }
            if (queryParams.facetData) {
                this.queryParams.facetData = queryParams.facetData;
            }
            if (queryParams.viewByResource) {
                this.queryParams.viewByResource = queryParams.viewByResource;
            }
            if (queryParams.menuName) {
                this.queryParams.menuName = queryParams.menuName;
            }
            if (queryParams.isTemplate) {
                this.queryParams.isTemplate = queryParams.isTemplate;
            }
            if (queryParams.campaignId) {
                this.queryParams.campaignId = queryParams.campaignId;
            }
            if (queryParams.isEditTask) {
                this.queryParams.isEditTask = queryParams.isEditTask;
            }
            if (queryParams.taskId) {
                this.queryParams.taskId = queryParams.taskId;
            }
            if (queryParams.isNewDeliverable) {
                this.queryParams.isNewDeliverable = queryParams.isNewDeliverable;
            }
            if (queryParams.isEditDeliverable) {
                this.queryParams.isEditDeliverable = queryParams.isEditDeliverable;
            }
            if (queryParams.deliverableId) {
                this.queryParams.deliverableId = queryParams.deliverableId;
            }
            if (queryParams.groupByFilter) {
                this.queryParams.groupByFilter = queryParams.groupByFilter;
            }
            if (queryParams.deliverableTaskFilter) {
                this.queryParams.deliverableTaskFilter = queryParams.deliverableTaskFilter;
            }
            if (queryParams.listDependentFilter) {
                this.queryParams.listDependentFilter = queryParams.listDependentFilter;
            }
            if (queryParams.emailLinkId) {
                this.queryParams.emailLinkId = queryParams.emailLinkId;
            }
        }
    };
    StateService.prototype.setRoute = function (type, queryParams, routeParams) {
        var _this = this;
        var params = {
            queryParams: null,
            routeParams: null
        };
        this.setQueryParams(queryParams);
        this.setRouteParams(routeParams);
        params.queryParams = this.queryParams;
        params.routeParams = this.routeParams;
        this.route.type = type;
        this.route.params = params;
        var routeId = this.routes.findIndex(function (route) { return route.type === _this.route.type; });
        if (routeId >= 0) {
            this.routes[routeId] = this.route;
        }
        else {
            this.routes.push(this.route);
        }
        this.resetData();
    };
    StateService.prototype.getRoute = function (type) {
        return this.routes.find(function (route) { return route.type === type; });
    };
    StateService.prototype.removeRoute = function (type) {
        var index = this.routes.findIndex(function (route) { return route.type === type; });
        if (index >= 0) {
            this.routes.splice(index, 1);
        }
    };
    StateService.prototype.setProjectAdvancedSearch = function (data) {
        this.projectAdvancedSearchData = data;
    };
    StateService.prototype.setTaskAdvancedSearch = function (data) {
        this.taskAdvancedSearchData = data;
    };
    StateService.prototype.setDeliverableAdvancedSearch = function (data) {
        this.deliverableAdvancedSearchData = data;
    };
    StateService.prototype.setCampaignProjectAdvancedSearch = function (data) {
        this.campaignProjectAdvancedSearchData = data;
    };
    StateService.prototype.setDeliverableTaskViewAdvancedSearch = function (data) {
        this.deliverableTaskViewAdvancedSearchData = data;
    };
    StateService.prototype.setDeliverableTaskViewFacetSearch = function (data) {
        this.deliverableTaskViewFacetData = data;
    };
    StateService.prototype.setFacetSearch = function (data) {
        this.facetData = data;
    };
    StateService.prototype.getProjectAdvancedSearch = function () {
        return this.projectAdvancedSearchData;
    };
    StateService.prototype.getTaskAdvancedSearch = function () {
        return this.taskAdvancedSearchData;
    };
    StateService.prototype.getDeliverableAdvancedSearch = function () {
        return this.deliverableAdvancedSearchData;
    };
    StateService.prototype.getCampaignProjectAdvancedSearch = function () {
        return this.campaignProjectAdvancedSearchData;
    };
    StateService.prototype.getDeliverableTaskViewAdvancedSearch = function () {
        return this.deliverableTaskViewAdvancedSearchData;
    };
    StateService.prototype.getDeliverableTaskViewFacetSearch = function () {
        return this.deliverableTaskViewFacetData;
    };
    StateService.prototype.getAdvancedSearch = function (menuName) {
        if (menuName === MenuConstants.PROJECT_MANAGEMENT) {
            return this.projectAdvancedSearchData;
        }
        else if (menuName === MenuConstants.TASKS) {
            return this.taskAdvancedSearchData;
        }
        else if (menuName === MenuConstants.PROJECTS) {
            return this.campaignProjectAdvancedSearchData;
        }
        else if (menuName === MenuConstants.DELIVERABLES) {
            return this.deliverableAdvancedSearchData;
        }
        else if (menuName === MenuConstants.DELIVERABLE_TASK_VIEW) {
            return this.deliverableTaskViewAdvancedSearchData;
        }
        else {
            return '';
        }
    };
    StateService.prototype.getFacetSearch = function () {
        return this.facetData;
    };
    StateService.prototype.setResourceManagementAdvancedSearchData = function (data) {
        this.resourceManagementAdvancedSearchData = data;
    };
    StateService.prototype.getResourceManagementAdvancedSearchData = function () {
        return this.resourceManagementAdvancedSearchData;
    };
    StateService.prototype.setResourceManagementFacetSearch = function (data) {
        this.resourceManagementFacetData = data;
    };
    StateService.prototype.getResourceManagementFacetSearch = function (data) {
        return this.resourceManagementFacetData;
    };
    /* setSelectedCampaignAssets() {
  
    } */
    StateService.prototype.setSelectedCampaignAssets = function (assetIds) {
        this.selectedCampaignAssetIds = assetIds;
    };
    StateService.prototype.getSelectedCampaignAssets = function () {
        return this.selectedCampaignAssetIds;
    };
    /* addSelectedCampaignAssets(assetId) {
      this.selectedCampaignAssetIds.push(assetId);
    }
    
    removeSelectedCampaignAssets(assetId) {
      const index = this.selectedCampaignAssetIds.findIndex(id => id === assetId);
      if (index >= 0) {
        this.selectedCampaignAssetIds.splice(index, 1);
      }
    } */
    StateService.prototype.resetSelectedCampaignAssets = function () {
        this.selectedCampaignAssetIds = [];
    };
    StateService.prototype.setSelectedProjectAssets = function (assetIds) {
        this.selectedProjectAssetIds = assetIds;
    };
    StateService.prototype.getSelectedProjectAssets = function () {
        return this.selectedProjectAssetIds;
    };
    /* addSelectedProjectAssets(assetId) {
      this.selectedProjectAssetIds.push(assetId);
    }
    
    removeSelectedProjectAssets(assetId) {
      const index = this.selectedProjectAssetIds.findIndex(id => id === assetId);
      if (index >= 0) {
        this.selectedProjectAssetIds.splice(index, 1);
      }
    } */
    StateService.prototype.resetSelectedProjectAssets = function () {
        this.selectedProjectAssetIds = [];
    };
    StateService.prototype.setAllProjectAssets = function (assetList) {
        var _this = this;
        assetList.forEach(function (asset) {
            var existingAsset = _this.allProjectAssets.find(function (projectAsset) { return projectAsset.asset_id === asset.asset_id; });
            if (!existingAsset) {
                _this.allProjectAssets.push(asset);
            }
        });
    };
    StateService.prototype.getAllProjectAssets = function () {
        return this.allProjectAssets;
    };
    StateService.prototype.resetAllProjectAssets = function () {
        this.allProjectAssets = [];
    };
    StateService.prototype.setAllCampaignAssets = function (assetList) {
        var _this = this;
        assetList.forEach(function (asset) {
            var existingAsset = _this.allCampaignAssets.find(function (projectAsset) { return projectAsset.asset_id === asset.asset_id; });
            if (!existingAsset) {
                _this.allCampaignAssets.push(asset);
            }
        });
    };
    StateService.prototype.getAllCampaignAssets = function () {
        return this.allCampaignAssets;
    };
    StateService.prototype.resetAllCampaignAssets = function () {
        this.allCampaignAssets = [];
    };
    StateService.ɵprov = i0.ɵɵdefineInjectable({ factory: function StateService_Factory() { return new StateService(); }, token: StateService, providedIn: "root" });
    StateService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], StateService);
    return StateService;
}());
export { StateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9zdGF0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQzs7QUFLNUQ7SUF3REU7UUF0REEsZ0JBQVcsR0FBRztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFLElBQUk7WUFDaEIsTUFBTSxFQUFFLElBQUk7WUFDWixTQUFTLEVBQUUsSUFBSTtZQUNmLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFFBQVEsRUFBRSxJQUFJO1lBQ2QsVUFBVSxFQUFFLElBQUk7WUFDaEIsZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLElBQUk7WUFDbkIsU0FBUyxFQUFFLElBQUk7WUFDZixjQUFjLEVBQUUsSUFBSTtZQUNwQixRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxJQUFJO1lBQ1osZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLGFBQWEsRUFBRSxJQUFJO1lBQ25CLHFCQUFxQixFQUFFLElBQUk7WUFDM0IsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixXQUFXLEVBQUUsSUFBSTtTQUNsQixDQUFDO1FBQ0YsZ0JBQVcsR0FBRztZQUNaLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsTUFBTSxFQUFFLElBQUk7U0FDYixDQUFDO1FBQ0YsVUFBSyxHQUFHO1lBQ04sSUFBSSxFQUFFLElBQUk7WUFDVixNQUFNLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFDRixXQUFNLEdBQUcsRUFBRSxDQUFDO1FBY1osNkJBQXdCLEdBQUcsRUFBRSxDQUFDO1FBQzlCLDRCQUF1QixHQUFHLEVBQUUsQ0FBQztRQUU3QixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIsc0JBQWlCLEdBQUcsRUFBRSxDQUFBO0lBRU4sQ0FBQztJQUVqQixnQ0FBUyxHQUFUO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxJQUFJO1lBQ1osU0FBUyxFQUFFLElBQUk7WUFDZixVQUFVLEVBQUUsSUFBSTtZQUNoQixRQUFRLEVBQUUsSUFBSTtZQUNkLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLFNBQVMsRUFBRSxJQUFJO1lBQ2YsY0FBYyxFQUFFLElBQUk7WUFDcEIsUUFBUSxFQUFFLElBQUk7WUFDZCxVQUFVLEVBQUUsSUFBSTtZQUNoQixVQUFVLEVBQUUsSUFBSTtZQUNoQixVQUFVLEVBQUUsSUFBSTtZQUNoQixNQUFNLEVBQUUsSUFBSTtZQUNaLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixhQUFhLEVBQUUsSUFBSTtZQUNuQixhQUFhLEVBQUUsSUFBSTtZQUNuQixxQkFBcUIsRUFBRSxJQUFJO1lBQzNCLG1CQUFtQixFQUFFLElBQUk7WUFDekIsV0FBVyxFQUFFLElBQUk7U0FDbEIsQ0FBQztRQUNGLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsVUFBVSxFQUFFLElBQUk7WUFDaEIsU0FBUyxFQUFFLElBQUk7WUFDZixNQUFNLEVBQUUsSUFBSTtTQUNiLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1gsSUFBSSxFQUFFLElBQUk7WUFDVixNQUFNLEVBQUUsSUFBSTtTQUNiLENBQUM7SUFDSixDQUFDO0lBRUQsaUNBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxxQ0FBYyxHQUFkLFVBQWUsV0FBVztRQUN4QixJQUFJLFdBQVcsRUFBRTtZQUNmLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQzthQUNwRDtZQUNELElBQUksV0FBVyxDQUFDLFVBQVUsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQzthQUN0RDtZQUNELElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtnQkFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUM5QztTQUNGO0lBQ0gsQ0FBQztJQUVELHFDQUFjLEdBQWQsVUFBZSxXQUFXO1FBQ3hCLElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO2FBQ2xEO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO2dCQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO2FBQzlDO1lBQ0QsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO2dCQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO2FBQ3BEO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO2FBQ2xEO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsZUFBZSxFQUFFO2dCQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDO2FBQ2hFO1lBQ0QsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUFFO2dCQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUMsYUFBYSxDQUFDO2FBQzVEO1lBQ0QsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO2dCQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO2FBQ3BEO1lBQ0QsSUFBSSxXQUFXLENBQUMsY0FBYyxFQUFFO2dCQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUMsY0FBYyxDQUFDO2FBQzlEO1lBQ0QsSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO2dCQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO2FBQ2xEO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsVUFBVSxFQUFFO2dCQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDO2FBQ3REO1lBQ0QsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO2dCQUN0QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO2FBQzlDO1lBQ0QsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsV0FBVyxDQUFDLGdCQUFnQixDQUFDO2FBQ2xFO1lBQ0QsSUFBSSxXQUFXLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsV0FBVyxDQUFDLGlCQUFpQixDQUFDO2FBQ3BFO1lBQ0QsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUFFO2dCQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUMsYUFBYSxDQUFDO2FBQzVEO1lBQ0QsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUFFO2dCQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUMsYUFBYSxDQUFDO2FBQzVEO1lBQ0QsSUFBSSxXQUFXLENBQUMscUJBQXFCLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLEdBQUcsV0FBVyxDQUFDLHFCQUFxQixDQUFDO2FBQzVFO1lBQ0QsSUFBSSxXQUFXLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLG1CQUFtQixDQUFDO2FBQ3hFO1lBQ0QsSUFBSSxXQUFXLENBQUMsV0FBVyxFQUFFO2dCQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDO2FBQ3hEO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsK0JBQVEsR0FBUixVQUFTLElBQUksRUFBRSxXQUFXLEVBQUUsV0FBVztRQUF2QyxpQkFrQkM7UUFqQkMsSUFBTSxNQUFNLEdBQUc7WUFDYixXQUFXLEVBQUUsSUFBSTtZQUNqQixXQUFXLEVBQUUsSUFBSTtTQUNsQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUN0QyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUMzQixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxJQUFJLEtBQUssS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQTlCLENBQThCLENBQUMsQ0FBQztRQUMvRSxJQUFJLE9BQU8sSUFBSSxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ25DO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELCtCQUFRLEdBQVIsVUFBUyxJQUFJO1FBQ1gsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFuQixDQUFtQixDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELGtDQUFXLEdBQVgsVUFBWSxJQUFJO1FBQ2QsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsSUFBSSxLQUFLLElBQUksRUFBbkIsQ0FBbUIsQ0FBQyxDQUFDO1FBQ2xFLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFFRCwrQ0FBd0IsR0FBeEIsVUFBeUIsSUFBSTtRQUMzQixJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO0lBQ3hDLENBQUM7SUFFRCw0Q0FBcUIsR0FBckIsVUFBc0IsSUFBSTtRQUN4QixJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxtREFBNEIsR0FBNUIsVUFBNkIsSUFBSTtRQUMvQixJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDO0lBQzVDLENBQUM7SUFFRCx1REFBZ0MsR0FBaEMsVUFBaUMsSUFBSTtRQUNuQyxJQUFJLENBQUMsaUNBQWlDLEdBQUcsSUFBSSxDQUFDO0lBQ2hELENBQUM7SUFFRCwyREFBb0MsR0FBcEMsVUFBcUMsSUFBSTtRQUN2QyxJQUFJLENBQUMscUNBQXFDLEdBQUcsSUFBSSxDQUFDO0lBQ3BELENBQUM7SUFFRCx3REFBaUMsR0FBakMsVUFBa0MsSUFBSTtRQUNwQyxJQUFJLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxDQUFDO0lBQzNDLENBQUM7SUFFRCxxQ0FBYyxHQUFkLFVBQWUsSUFBSTtRQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztJQUN4QixDQUFDO0lBRUQsK0NBQXdCLEdBQXhCO1FBQ0UsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUM7SUFDeEMsQ0FBQztJQUVELDRDQUFxQixHQUFyQjtRQUNFLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDO0lBQ3JDLENBQUM7SUFFRCxtREFBNEIsR0FBNUI7UUFDRSxPQUFPLElBQUksQ0FBQyw2QkFBNkIsQ0FBQztJQUM1QyxDQUFDO0lBRUQsdURBQWdDLEdBQWhDO1FBQ0UsT0FBTyxJQUFJLENBQUMsaUNBQWlDLENBQUM7SUFDaEQsQ0FBQztJQUVELDJEQUFvQyxHQUFwQztRQUNFLE9BQU8sSUFBSSxDQUFDLHFDQUFxQyxDQUFDO0lBQ3BELENBQUM7SUFFRCx3REFBaUMsR0FBakM7UUFDRSxPQUFPLElBQUksQ0FBQyw0QkFBNEIsQ0FBQztJQUMzQyxDQUFDO0lBRUQsd0NBQWlCLEdBQWpCLFVBQWtCLFFBQVE7UUFDeEIsSUFBSSxRQUFRLEtBQUssYUFBYSxDQUFDLGtCQUFrQixFQUFFO1lBQ2pELE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDO1NBQ3ZDO2FBQU0sSUFBSSxRQUFRLEtBQUssYUFBYSxDQUFDLEtBQUssRUFBRTtZQUMzQyxPQUFPLElBQUksQ0FBQyxzQkFBc0IsQ0FBQztTQUNwQzthQUFNLElBQUksUUFBUSxLQUFLLGFBQWEsQ0FBQyxRQUFRLEVBQUU7WUFDOUMsT0FBTyxJQUFJLENBQUMsaUNBQWlDLENBQUM7U0FDL0M7YUFBTSxJQUFJLFFBQVEsS0FBSyxhQUFhLENBQUMsWUFBWSxFQUFFO1lBQ2xELE9BQU8sSUFBSSxDQUFDLDZCQUE2QixDQUFDO1NBQzNDO2FBQU0sSUFBSSxRQUFRLEtBQUssYUFBYSxDQUFDLHFCQUFxQixFQUFFO1lBQzNELE9BQU8sSUFBSSxDQUFDLHFDQUFxQyxDQUFDO1NBQ25EO2FBQU07WUFDTCxPQUFPLEVBQUUsQ0FBQztTQUNYO0lBQ0gsQ0FBQztJQUVELHFDQUFjLEdBQWQ7UUFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELDhEQUF1QyxHQUF2QyxVQUF3QyxJQUFJO1FBQzFDLElBQUksQ0FBQyxvQ0FBb0MsR0FBRyxJQUFJLENBQUM7SUFDbkQsQ0FBQztJQUVELDhEQUF1QyxHQUF2QztRQUNFLE9BQU8sSUFBSSxDQUFDLG9DQUFvQyxDQUFDO0lBQ25ELENBQUM7SUFFRCx1REFBZ0MsR0FBaEMsVUFBaUMsSUFBSTtRQUNuQyxJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDO0lBQzFDLENBQUM7SUFFRCx1REFBZ0MsR0FBaEMsVUFBaUMsSUFBSTtRQUNuQyxPQUFPLElBQUksQ0FBQywyQkFBMkIsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7O1FBRUk7SUFFSixnREFBeUIsR0FBekIsVUFBMEIsUUFBUTtRQUNoQyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsUUFBUSxDQUFDO0lBQzNDLENBQUM7SUFFRCxnREFBeUIsR0FBekI7UUFDRSxPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN2QyxDQUFDO0lBRUQ7Ozs7Ozs7OztRQVNJO0lBRUosa0RBQTJCLEdBQTNCO1FBQ0UsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsK0NBQXdCLEdBQXhCLFVBQXlCLFFBQVE7UUFDL0IsSUFBSSxDQUFDLHVCQUF1QixHQUFHLFFBQVEsQ0FBQztJQUMxQyxDQUFDO0lBRUQsK0NBQXdCLEdBQXhCO1FBQ0UsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUM7SUFDdEMsQ0FBQztJQUVEOzs7Ozs7Ozs7UUFTSTtJQUVKLGlEQUEwQixHQUExQjtRQUNFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELDBDQUFtQixHQUFuQixVQUFvQixTQUFTO1FBQTdCLGlCQU9DO1FBTkMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDckIsSUFBTSxhQUFhLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsRUFBeEMsQ0FBd0MsQ0FBQyxDQUFDO1lBQzNHLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQ2xCLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwwQ0FBbUIsR0FBbkI7UUFDRSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUMvQixDQUFDO0lBRUQsNENBQXFCLEdBQXJCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsMkNBQW9CLEdBQXBCLFVBQXFCLFNBQVM7UUFBOUIsaUJBT0M7UUFOQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztZQUNyQixJQUFNLGFBQWEsR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQUEsWUFBWSxJQUFJLE9BQUEsWUFBWSxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsUUFBUSxFQUF4QyxDQUF3QyxDQUFDLENBQUM7WUFDNUcsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDbEIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNwQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUFvQixHQUFwQjtRQUNFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ2hDLENBQUM7SUFFRCw2Q0FBc0IsR0FBdEI7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO0lBQzlCLENBQUM7O0lBcFlVLFlBQVk7UUFIeEIsVUFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQztPQUNXLFlBQVksQ0FxWXhCO3VCQTNZRDtDQTJZQyxBQXJZRCxJQXFZQztTQXJZWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNZW51Q29uc3RhbnRzIH0gZnJvbSAnLi4vY29uc3RhbnRzL21lbnUuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFN0YXRlU2VydmljZSB7XHJcblxyXG4gIHF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgdmlld05hbWU6IG51bGwsXHJcbiAgICBpc0xpc3RWaWV3OiBudWxsLFxyXG4gICAgc29ydEJ5OiBudWxsLFxyXG4gICAgc29ydE9yZGVyOiBudWxsLFxyXG4gICAgcGFnZU51bWJlcjogbnVsbCxcclxuICAgIHBhZ2VTaXplOiBudWxsLFxyXG4gICAgc2VhcmNoTmFtZTogbnVsbCxcclxuICAgIHNhdmVkU2VhcmNoTmFtZTogbnVsbCxcclxuICAgIGFkdlNlYXJjaERhdGE6IG51bGwsXHJcbiAgICBmYWNldERhdGE6IG51bGwsXHJcbiAgICB2aWV3QnlSZXNvdXJjZTogbnVsbCxcclxuICAgIG1lbnVOYW1lOiBudWxsLFxyXG4gICAgaXNUZW1wbGF0ZTogbnVsbCxcclxuICAgIGNhbXBhaWduSWQ6IG51bGwsXHJcbiAgICBpc0VkaXRUYXNrOiBudWxsLFxyXG4gICAgdGFza0lkOiBudWxsLFxyXG4gICAgaXNOZXdEZWxpdmVyYWJsZTogbnVsbCxcclxuICAgIGlzRWRpdERlbGl2ZXJhYmxlOiBudWxsLFxyXG4gICAgZGVsaXZlcmFibGVJZDogbnVsbCxcclxuICAgIGdyb3VwQnlGaWx0ZXI6IG51bGwsXHJcbiAgICBkZWxpdmVyYWJsZVRhc2tGaWx0ZXI6IG51bGwsXHJcbiAgICBsaXN0RGVwZW5kZW50RmlsdGVyOiBudWxsLFxyXG4gICAgZW1haWxMaW5rSWQ6IG51bGxcclxuICB9O1xyXG4gIHJvdXRlUGFyYW1zID0ge1xyXG4gICAgY2FtcGFpZ25JZDogbnVsbCxcclxuICAgIHByb2plY3RJZDogbnVsbCxcclxuICAgIHRhc2tJZDogbnVsbFxyXG4gIH07XHJcbiAgcm91dGUgPSB7XHJcbiAgICB0eXBlOiBudWxsLFxyXG4gICAgcGFyYW1zOiBudWxsXHJcbiAgfTtcclxuICByb3V0ZXMgPSBbXTtcclxuXHJcbiAgcHJvamVjdEFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICB0YXNrQWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gIGRlbGl2ZXJhYmxlQWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gIGNhbXBhaWduUHJvamVjdEFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICBkZWxpdmVyYWJsZVRhc2tWaWV3QWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gIGRlbGl2ZXJhYmxlVGFza1ZpZXdGYWNldERhdGE7XHJcbiAgZmFjZXREYXRhO1xyXG5cclxuICByZXNvdXJjZU1hbmFnZW1lbnRGYWNldERhdGE7XHJcblxyXG4gIHJlc291cmNlTWFuYWdlbWVudEFkdmFuY2VkU2VhcmNoRGF0YTtcclxuXHJcbiAgc2VsZWN0ZWRDYW1wYWlnbkFzc2V0SWRzID0gW107XHJcbiAgc2VsZWN0ZWRQcm9qZWN0QXNzZXRJZHMgPSBbXTtcclxuXHJcbiAgYWxsUHJvamVjdEFzc2V0cyA9IFtdO1xyXG4gIGFsbENhbXBhaWduQXNzZXRzID0gW11cclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgcmVzZXREYXRhKCkge1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcyA9IHtcclxuICAgICAgdmlld05hbWU6IG51bGwsXHJcbiAgICAgIGlzTGlzdFZpZXc6IG51bGwsXHJcbiAgICAgIHNvcnRCeTogbnVsbCxcclxuICAgICAgc29ydE9yZGVyOiBudWxsLFxyXG4gICAgICBwYWdlTnVtYmVyOiBudWxsLFxyXG4gICAgICBwYWdlU2l6ZTogbnVsbCxcclxuICAgICAgc2VhcmNoTmFtZTogbnVsbCxcclxuICAgICAgc2F2ZWRTZWFyY2hOYW1lOiBudWxsLFxyXG4gICAgICBhZHZTZWFyY2hEYXRhOiBudWxsLFxyXG4gICAgICBmYWNldERhdGE6IG51bGwsXHJcbiAgICAgIHZpZXdCeVJlc291cmNlOiBudWxsLFxyXG4gICAgICBtZW51TmFtZTogbnVsbCxcclxuICAgICAgaXNUZW1wbGF0ZTogbnVsbCxcclxuICAgICAgY2FtcGFpZ25JZDogbnVsbCxcclxuICAgICAgaXNFZGl0VGFzazogbnVsbCxcclxuICAgICAgdGFza0lkOiBudWxsLFxyXG4gICAgICBpc05ld0RlbGl2ZXJhYmxlOiBudWxsLFxyXG4gICAgICBpc0VkaXREZWxpdmVyYWJsZTogbnVsbCxcclxuICAgICAgZGVsaXZlcmFibGVJZDogbnVsbCxcclxuICAgICAgZ3JvdXBCeUZpbHRlcjogbnVsbCxcclxuICAgICAgZGVsaXZlcmFibGVUYXNrRmlsdGVyOiBudWxsLFxyXG4gICAgICBsaXN0RGVwZW5kZW50RmlsdGVyOiBudWxsLFxyXG4gICAgICBlbWFpbExpbmtJZDogbnVsbFxyXG4gICAgfTtcclxuICAgIHRoaXMucm91dGVQYXJhbXMgPSB7XHJcbiAgICAgIGNhbXBhaWduSWQ6IG51bGwsXHJcbiAgICAgIHByb2plY3RJZDogbnVsbCxcclxuICAgICAgdGFza0lkOiBudWxsXHJcbiAgICB9O1xyXG4gICAgdGhpcy5yb3V0ZSA9IHtcclxuICAgICAgdHlwZTogbnVsbCxcclxuICAgICAgcGFyYW1zOiBudWxsXHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgcmVzZXRTdGF0ZSgpIHtcclxuICAgIHRoaXMucm91dGVzID0gW107XHJcbiAgfVxyXG5cclxuICBzZXRSb3V0ZVBhcmFtcyhyb3V0ZVBhcmFtcykge1xyXG4gICAgaWYgKHJvdXRlUGFyYW1zKSB7XHJcbiAgICAgIGlmIChyb3V0ZVBhcmFtcy5wcm9qZWN0SWQpIHtcclxuICAgICAgICB0aGlzLnJvdXRlUGFyYW1zLnByb2plY3RJZCA9IHJvdXRlUGFyYW1zLnByb2plY3RJZDtcclxuICAgICAgfVxyXG4gICAgICBpZiAocm91dGVQYXJhbXMuY2FtcGFpZ25JZCkge1xyXG4gICAgICAgIHRoaXMucm91dGVQYXJhbXMuY2FtcGFpZ25JZCA9IHJvdXRlUGFyYW1zLmNhbXBhaWduSWQ7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHJvdXRlUGFyYW1zLnRhc2tJZCkge1xyXG4gICAgICAgIHRoaXMucm91dGVQYXJhbXMudGFza0lkID0gcm91dGVQYXJhbXMudGFza0lkO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRRdWVyeVBhcmFtcyhxdWVyeVBhcmFtcykge1xyXG4gICAgaWYgKHF1ZXJ5UGFyYW1zKSB7XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy52aWV3TmFtZSkge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMudmlld05hbWUgPSBxdWVyeVBhcmFtcy52aWV3TmFtZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuaXNMaXN0Vmlldykge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuaXNMaXN0VmlldyA9IHF1ZXJ5UGFyYW1zLmlzTGlzdFZpZXc7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLnNvcnRCeSkge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuc29ydEJ5ID0gcXVlcnlQYXJhbXMuc29ydEJ5O1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5zb3J0T3JkZXIpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLnNvcnRPcmRlciA9IHF1ZXJ5UGFyYW1zLnNvcnRPcmRlcjtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMucGFnZU51bWJlcikge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMucGFnZU51bWJlciA9IHF1ZXJ5UGFyYW1zLnBhZ2VOdW1iZXI7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLnBhZ2VTaXplKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5wYWdlU2l6ZSA9IHF1ZXJ5UGFyYW1zLnBhZ2VTaXplO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5zZWFyY2hOYW1lKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5zZWFyY2hOYW1lID0gcXVlcnlQYXJhbXMuc2VhcmNoTmFtZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuc2F2ZWRTZWFyY2hOYW1lKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5zYXZlZFNlYXJjaE5hbWUgPSBxdWVyeVBhcmFtcy5zYXZlZFNlYXJjaE5hbWU7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLmFkdlNlYXJjaERhdGEpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLmFkdlNlYXJjaERhdGEgPSBxdWVyeVBhcmFtcy5hZHZTZWFyY2hEYXRhO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5mYWNldERhdGEpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLmZhY2V0RGF0YSA9IHF1ZXJ5UGFyYW1zLmZhY2V0RGF0YTtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMudmlld0J5UmVzb3VyY2UpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLnZpZXdCeVJlc291cmNlID0gcXVlcnlQYXJhbXMudmlld0J5UmVzb3VyY2U7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLm1lbnVOYW1lKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5tZW51TmFtZSA9IHF1ZXJ5UGFyYW1zLm1lbnVOYW1lO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5pc1RlbXBsYXRlKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5pc1RlbXBsYXRlID0gcXVlcnlQYXJhbXMuaXNUZW1wbGF0ZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuY2FtcGFpZ25JZCkge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuY2FtcGFpZ25JZCA9IHF1ZXJ5UGFyYW1zLmNhbXBhaWduSWQ7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLmlzRWRpdFRhc2spIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLmlzRWRpdFRhc2sgPSBxdWVyeVBhcmFtcy5pc0VkaXRUYXNrO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy50YXNrSWQpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLnRhc2tJZCA9IHF1ZXJ5UGFyYW1zLnRhc2tJZDtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuaXNOZXdEZWxpdmVyYWJsZSkge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuaXNOZXdEZWxpdmVyYWJsZSA9IHF1ZXJ5UGFyYW1zLmlzTmV3RGVsaXZlcmFibGU7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLmlzRWRpdERlbGl2ZXJhYmxlKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5pc0VkaXREZWxpdmVyYWJsZSA9IHF1ZXJ5UGFyYW1zLmlzRWRpdERlbGl2ZXJhYmxlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5kZWxpdmVyYWJsZUlkKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5kZWxpdmVyYWJsZUlkID0gcXVlcnlQYXJhbXMuZGVsaXZlcmFibGVJZDtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuZ3JvdXBCeUZpbHRlcikge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuZ3JvdXBCeUZpbHRlciA9IHF1ZXJ5UGFyYW1zLmdyb3VwQnlGaWx0ZXI7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHF1ZXJ5UGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlcikge1xyXG4gICAgICAgIHRoaXMucXVlcnlQYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyID0gcXVlcnlQYXJhbXMuZGVsaXZlcmFibGVUYXNrRmlsdGVyO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChxdWVyeVBhcmFtcy5saXN0RGVwZW5kZW50RmlsdGVyKSB7XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcy5saXN0RGVwZW5kZW50RmlsdGVyID0gcXVlcnlQYXJhbXMubGlzdERlcGVuZGVudEZpbHRlcjtcclxuICAgICAgfVxyXG4gICAgICBpZiAocXVlcnlQYXJhbXMuZW1haWxMaW5rSWQpIHtcclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zLmVtYWlsTGlua0lkID0gcXVlcnlQYXJhbXMuZW1haWxMaW5rSWQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldFJvdXRlKHR5cGUsIHF1ZXJ5UGFyYW1zLCByb3V0ZVBhcmFtcykge1xyXG4gICAgY29uc3QgcGFyYW1zID0ge1xyXG4gICAgICBxdWVyeVBhcmFtczogbnVsbCxcclxuICAgICAgcm91dGVQYXJhbXM6IG51bGxcclxuICAgIH07XHJcbiAgICB0aGlzLnNldFF1ZXJ5UGFyYW1zKHF1ZXJ5UGFyYW1zKTtcclxuICAgIHRoaXMuc2V0Um91dGVQYXJhbXMocm91dGVQYXJhbXMpO1xyXG4gICAgcGFyYW1zLnF1ZXJ5UGFyYW1zID0gdGhpcy5xdWVyeVBhcmFtcztcclxuICAgIHBhcmFtcy5yb3V0ZVBhcmFtcyA9IHRoaXMucm91dGVQYXJhbXM7XHJcbiAgICB0aGlzLnJvdXRlLnR5cGUgPSB0eXBlO1xyXG4gICAgdGhpcy5yb3V0ZS5wYXJhbXMgPSBwYXJhbXM7XHJcbiAgICBjb25zdCByb3V0ZUlkID0gdGhpcy5yb3V0ZXMuZmluZEluZGV4KHJvdXRlID0+IHJvdXRlLnR5cGUgPT09IHRoaXMucm91dGUudHlwZSk7XHJcbiAgICBpZiAocm91dGVJZCA+PSAwKSB7XHJcbiAgICAgIHRoaXMucm91dGVzW3JvdXRlSWRdID0gdGhpcy5yb3V0ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucm91dGVzLnB1c2godGhpcy5yb3V0ZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJlc2V0RGF0YSgpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Um91dGUodHlwZSkge1xyXG4gICAgcmV0dXJuIHRoaXMucm91dGVzLmZpbmQocm91dGUgPT4gcm91dGUudHlwZSA9PT0gdHlwZSk7XHJcbiAgfVxyXG5cclxuICByZW1vdmVSb3V0ZSh0eXBlKSB7XHJcbiAgICBjb25zdCBpbmRleCA9IHRoaXMucm91dGVzLmZpbmRJbmRleChyb3V0ZSA9PiByb3V0ZS50eXBlID09PSB0eXBlKTtcclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMucm91dGVzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzZXRQcm9qZWN0QWR2YW5jZWRTZWFyY2goZGF0YSkge1xyXG4gICAgdGhpcy5wcm9qZWN0QWR2YW5jZWRTZWFyY2hEYXRhID0gZGF0YTtcclxuICB9XHJcblxyXG4gIHNldFRhc2tBZHZhbmNlZFNlYXJjaChkYXRhKSB7XHJcbiAgICB0aGlzLnRhc2tBZHZhbmNlZFNlYXJjaERhdGEgPSBkYXRhO1xyXG4gIH1cclxuXHJcbiAgc2V0RGVsaXZlcmFibGVBZHZhbmNlZFNlYXJjaChkYXRhKSB7XHJcbiAgICB0aGlzLmRlbGl2ZXJhYmxlQWR2YW5jZWRTZWFyY2hEYXRhID0gZGF0YTtcclxuICB9XHJcblxyXG4gIHNldENhbXBhaWduUHJvamVjdEFkdmFuY2VkU2VhcmNoKGRhdGEpIHtcclxuICAgIHRoaXMuY2FtcGFpZ25Qcm9qZWN0QWR2YW5jZWRTZWFyY2hEYXRhID0gZGF0YTtcclxuICB9XHJcblxyXG4gIHNldERlbGl2ZXJhYmxlVGFza1ZpZXdBZHZhbmNlZFNlYXJjaChkYXRhKSB7XHJcbiAgICB0aGlzLmRlbGl2ZXJhYmxlVGFza1ZpZXdBZHZhbmNlZFNlYXJjaERhdGEgPSBkYXRhO1xyXG4gIH1cclxuXHJcbiAgc2V0RGVsaXZlcmFibGVUYXNrVmlld0ZhY2V0U2VhcmNoKGRhdGEpIHtcclxuICAgIHRoaXMuZGVsaXZlcmFibGVUYXNrVmlld0ZhY2V0RGF0YSA9IGRhdGE7XHJcbiAgfVxyXG5cclxuICBzZXRGYWNldFNlYXJjaChkYXRhKSB7XHJcbiAgICB0aGlzLmZhY2V0RGF0YSA9IGRhdGE7XHJcbiAgfVxyXG5cclxuICBnZXRQcm9qZWN0QWR2YW5jZWRTZWFyY2goKSB7XHJcbiAgICByZXR1cm4gdGhpcy5wcm9qZWN0QWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gIH1cclxuXHJcbiAgZ2V0VGFza0FkdmFuY2VkU2VhcmNoKCkge1xyXG4gICAgcmV0dXJuIHRoaXMudGFza0FkdmFuY2VkU2VhcmNoRGF0YTtcclxuICB9XHJcblxyXG4gIGdldERlbGl2ZXJhYmxlQWR2YW5jZWRTZWFyY2goKSB7XHJcbiAgICByZXR1cm4gdGhpcy5kZWxpdmVyYWJsZUFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICB9XHJcblxyXG4gIGdldENhbXBhaWduUHJvamVjdEFkdmFuY2VkU2VhcmNoKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuY2FtcGFpZ25Qcm9qZWN0QWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gIH1cclxuXHJcbiAgZ2V0RGVsaXZlcmFibGVUYXNrVmlld0FkdmFuY2VkU2VhcmNoKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZGVsaXZlcmFibGVUYXNrVmlld0FkdmFuY2VkU2VhcmNoRGF0YTtcclxuICB9XHJcblxyXG4gIGdldERlbGl2ZXJhYmxlVGFza1ZpZXdGYWNldFNlYXJjaCgpIHtcclxuICAgIHJldHVybiB0aGlzLmRlbGl2ZXJhYmxlVGFza1ZpZXdGYWNldERhdGE7XHJcbiAgfVxyXG5cclxuICBnZXRBZHZhbmNlZFNlYXJjaChtZW51TmFtZSkge1xyXG4gICAgaWYgKG1lbnVOYW1lID09PSBNZW51Q29uc3RhbnRzLlBST0pFQ1RfTUFOQUdFTUVOVCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5wcm9qZWN0QWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gICAgfSBlbHNlIGlmIChtZW51TmFtZSA9PT0gTWVudUNvbnN0YW50cy5UQVNLUykge1xyXG4gICAgICByZXR1cm4gdGhpcy50YXNrQWR2YW5jZWRTZWFyY2hEYXRhO1xyXG4gICAgfSBlbHNlIGlmIChtZW51TmFtZSA9PT0gTWVudUNvbnN0YW50cy5QUk9KRUNUUykge1xyXG4gICAgICByZXR1cm4gdGhpcy5jYW1wYWlnblByb2plY3RBZHZhbmNlZFNlYXJjaERhdGE7XHJcbiAgICB9IGVsc2UgaWYgKG1lbnVOYW1lID09PSBNZW51Q29uc3RhbnRzLkRFTElWRVJBQkxFUykge1xyXG4gICAgICByZXR1cm4gdGhpcy5kZWxpdmVyYWJsZUFkdmFuY2VkU2VhcmNoRGF0YTtcclxuICAgIH0gZWxzZSBpZiAobWVudU5hbWUgPT09IE1lbnVDb25zdGFudHMuREVMSVZFUkFCTEVfVEFTS19WSUVXKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRlbGl2ZXJhYmxlVGFza1ZpZXdBZHZhbmNlZFNlYXJjaERhdGE7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRGYWNldFNlYXJjaCgpIHtcclxuICAgIHJldHVybiB0aGlzLmZhY2V0RGF0YTtcclxuICB9XHJcblxyXG4gIHNldFJlc291cmNlTWFuYWdlbWVudEFkdmFuY2VkU2VhcmNoRGF0YShkYXRhKSB7XHJcbiAgICB0aGlzLnJlc291cmNlTWFuYWdlbWVudEFkdmFuY2VkU2VhcmNoRGF0YSA9IGRhdGE7XHJcbiAgfVxyXG5cclxuICBnZXRSZXNvdXJjZU1hbmFnZW1lbnRBZHZhbmNlZFNlYXJjaERhdGEoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5yZXNvdXJjZU1hbmFnZW1lbnRBZHZhbmNlZFNlYXJjaERhdGE7XHJcbiAgfVxyXG5cclxuICBzZXRSZXNvdXJjZU1hbmFnZW1lbnRGYWNldFNlYXJjaChkYXRhKSB7XHJcbiAgICB0aGlzLnJlc291cmNlTWFuYWdlbWVudEZhY2V0RGF0YSA9IGRhdGE7XHJcbiAgfVxyXG5cclxuICBnZXRSZXNvdXJjZU1hbmFnZW1lbnRGYWNldFNlYXJjaChkYXRhKSB7XHJcbiAgICByZXR1cm4gdGhpcy5yZXNvdXJjZU1hbmFnZW1lbnRGYWNldERhdGE7XHJcbiAgfVxyXG5cclxuICAvKiBzZXRTZWxlY3RlZENhbXBhaWduQXNzZXRzKCkge1xyXG5cclxuICB9ICovXHJcblxyXG4gIHNldFNlbGVjdGVkQ2FtcGFpZ25Bc3NldHMoYXNzZXRJZHMpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDYW1wYWlnbkFzc2V0SWRzID0gYXNzZXRJZHM7XHJcbiAgfVxyXG5cclxuICBnZXRTZWxlY3RlZENhbXBhaWduQXNzZXRzKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRDYW1wYWlnbkFzc2V0SWRzO1xyXG4gIH1cclxuXHJcbiAgLyogYWRkU2VsZWN0ZWRDYW1wYWlnbkFzc2V0cyhhc3NldElkKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ25Bc3NldElkcy5wdXNoKGFzc2V0SWQpO1xyXG4gIH1cclxuICBcclxuICByZW1vdmVTZWxlY3RlZENhbXBhaWduQXNzZXRzKGFzc2V0SWQpIHtcclxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5zZWxlY3RlZENhbXBhaWduQXNzZXRJZHMuZmluZEluZGV4KGlkID0+IGlkID09PSBhc3NldElkKTtcclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDYW1wYWlnbkFzc2V0SWRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgfSAqL1xyXG5cclxuICByZXNldFNlbGVjdGVkQ2FtcGFpZ25Bc3NldHMoKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2FtcGFpZ25Bc3NldElkcyA9IFtdO1xyXG4gIH1cclxuXHJcbiAgc2V0U2VsZWN0ZWRQcm9qZWN0QXNzZXRzKGFzc2V0SWRzKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkUHJvamVjdEFzc2V0SWRzID0gYXNzZXRJZHM7XHJcbiAgfVxyXG5cclxuICBnZXRTZWxlY3RlZFByb2plY3RBc3NldHMoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zZWxlY3RlZFByb2plY3RBc3NldElkcztcclxuICB9XHJcblxyXG4gIC8qIGFkZFNlbGVjdGVkUHJvamVjdEFzc2V0cyhhc3NldElkKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkUHJvamVjdEFzc2V0SWRzLnB1c2goYXNzZXRJZCk7XHJcbiAgfVxyXG4gIFxyXG4gIHJlbW92ZVNlbGVjdGVkUHJvamVjdEFzc2V0cyhhc3NldElkKSB7XHJcbiAgICBjb25zdCBpbmRleCA9IHRoaXMuc2VsZWN0ZWRQcm9qZWN0QXNzZXRJZHMuZmluZEluZGV4KGlkID0+IGlkID09PSBhc3NldElkKTtcclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRQcm9qZWN0QXNzZXRJZHMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICB9ICovXHJcblxyXG4gIHJlc2V0U2VsZWN0ZWRQcm9qZWN0QXNzZXRzKCkge1xyXG4gICAgdGhpcy5zZWxlY3RlZFByb2plY3RBc3NldElkcyA9IFtdO1xyXG4gIH1cclxuXHJcbiAgc2V0QWxsUHJvamVjdEFzc2V0cyhhc3NldExpc3QpIHtcclxuICAgIGFzc2V0TGlzdC5mb3JFYWNoKGFzc2V0ID0+IHtcclxuICAgICAgY29uc3QgZXhpc3RpbmdBc3NldCA9IHRoaXMuYWxsUHJvamVjdEFzc2V0cy5maW5kKHByb2plY3RBc3NldCA9PiBwcm9qZWN0QXNzZXQuYXNzZXRfaWQgPT09IGFzc2V0LmFzc2V0X2lkKTtcclxuICAgICAgaWYgKCFleGlzdGluZ0Fzc2V0KSB7XHJcbiAgICAgICAgdGhpcy5hbGxQcm9qZWN0QXNzZXRzLnB1c2goYXNzZXQpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEFsbFByb2plY3RBc3NldHMoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5hbGxQcm9qZWN0QXNzZXRzO1xyXG4gIH1cclxuXHJcbiAgcmVzZXRBbGxQcm9qZWN0QXNzZXRzKCkge1xyXG4gICAgdGhpcy5hbGxQcm9qZWN0QXNzZXRzID0gW107XHJcbiAgfVxyXG5cclxuICBzZXRBbGxDYW1wYWlnbkFzc2V0cyhhc3NldExpc3QpIHtcclxuICAgIGFzc2V0TGlzdC5mb3JFYWNoKGFzc2V0ID0+IHtcclxuICAgICAgY29uc3QgZXhpc3RpbmdBc3NldCA9IHRoaXMuYWxsQ2FtcGFpZ25Bc3NldHMuZmluZChwcm9qZWN0QXNzZXQgPT4gcHJvamVjdEFzc2V0LmFzc2V0X2lkID09PSBhc3NldC5hc3NldF9pZCk7XHJcbiAgICAgIGlmICghZXhpc3RpbmdBc3NldCkge1xyXG4gICAgICAgIHRoaXMuYWxsQ2FtcGFpZ25Bc3NldHMucHVzaChhc3NldCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWxsQ2FtcGFpZ25Bc3NldHMoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5hbGxDYW1wYWlnbkFzc2V0cztcclxuICB9XHJcblxyXG4gIHJlc2V0QWxsQ2FtcGFpZ25Bc3NldHMoKSB7XHJcbiAgICB0aGlzLmFsbENhbXBhaWduQXNzZXRzID0gW107XHJcbiAgfVxyXG59XHJcbiJdfQ==