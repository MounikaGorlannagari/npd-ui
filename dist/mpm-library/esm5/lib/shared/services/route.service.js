import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingConstants } from '../constants/routingConstants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var RouteService = /** @class */ (function () {
    function RouteService(router) {
        this.router = router;
        this.routingConstants = RoutingConstants;
    }
    // Resource Management MPMV3-2056
    RouteService.prototype.goToResourceManagement = function (queryParams, searchName, savedSearchName, advSearchData, facetData) {
        this.router.navigate([this.routingConstants.mpmBaseRoute + '/' + this.routingConstants.resourceManagement], {
            queryParams: {
                menuName: 'ResourceManagement',
                groupByFilter: queryParams.groupByFilter,
                //  deliverableTaskFilter: queryParams.deliverableTaskFilter,
                //  listDependentFilter: queryParams.listDependentFilter,
                //  emailLinkId: queryParams.emailLinkId,
                sortBy: queryParams.sortBy,
                sortOrder: queryParams.sortOrder,
                pageNumber: queryParams.pageNumber,
                pageSize: queryParams.pageSize,
                searchName: searchName,
                savedSearchName: savedSearchName,
                advSearchData: advSearchData,
                facetData: facetData
            }
        });
    };
    RouteService.ctorParameters = function () { return [
        { type: Router }
    ]; };
    RouteService.ɵprov = i0.ɵɵdefineInjectable({ factory: function RouteService_Factory() { return new RouteService(i0.ɵɵinject(i1.Router)); }, token: RouteService, providedIn: "root" });
    RouteService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], RouteService);
    return RouteService;
}());
export { RouteService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm91dGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9zZXJ2aWNlcy9yb3V0ZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7O0FBTWpFO0lBSUksc0JBQ1csTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7UUFIekIscUJBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFJaEMsQ0FBQztJQUVSLGlDQUFpQztJQUVqQyw2Q0FBc0IsR0FBdEIsVUFBdUIsV0FBVyxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLFNBQVM7UUFDdEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsRUFBRTtZQUN4RyxXQUFXLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsYUFBYSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dCQUN4Qyw2REFBNkQ7Z0JBQzdELHlEQUF5RDtnQkFDekQseUNBQXlDO2dCQUN6QyxNQUFNLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0JBQzFCLFNBQVMsRUFBRSxXQUFXLENBQUMsU0FBUztnQkFDaEMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxVQUFVO2dCQUNsQyxRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVE7Z0JBQzlCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixlQUFlLEVBQUUsZUFBZTtnQkFDaEMsYUFBYSxFQUFFLGFBQWE7Z0JBQzVCLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBdkJzQixNQUFNOzs7SUFMaEIsWUFBWTtRQUp4QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsWUFBWSxDQThCeEI7dUJBdENEO0NBc0NDLEFBOUJELElBOEJDO1NBOUJZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFJvdXRpbmdDb25zdGFudHMgfSBmcm9tICcuLi9jb25zdGFudHMvcm91dGluZ0NvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBSb3V0ZVNlcnZpY2Uge1xyXG5cclxuICAgIHJvdXRpbmdDb25zdGFudHMgPSBSb3V0aW5nQ29uc3RhbnRzO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICkgeyB9XHJcblxyXG4gLy8gUmVzb3VyY2UgTWFuYWdlbWVudCBNUE1WMy0yMDU2XHJcblxyXG4gZ29Ub1Jlc291cmNlTWFuYWdlbWVudChxdWVyeVBhcmFtcywgc2VhcmNoTmFtZSwgc2F2ZWRTZWFyY2hOYW1lLCBhZHZTZWFyY2hEYXRhLCBmYWNldERhdGEpIHtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnJvdXRpbmdDb25zdGFudHMubXBtQmFzZVJvdXRlICsgJy8nICsgdGhpcy5yb3V0aW5nQ29uc3RhbnRzLnJlc291cmNlTWFuYWdlbWVudF0sIHtcclxuICAgICAgICBxdWVyeVBhcmFtczoge1xyXG4gICAgICAgICAgICBtZW51TmFtZTogJ1Jlc291cmNlTWFuYWdlbWVudCcsXHJcbiAgICAgICAgICAgIGdyb3VwQnlGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmdyb3VwQnlGaWx0ZXIsXHJcbiAgICAgICAgICAgIC8vICBkZWxpdmVyYWJsZVRhc2tGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmRlbGl2ZXJhYmxlVGFza0ZpbHRlcixcclxuICAgICAgICAgICAgLy8gIGxpc3REZXBlbmRlbnRGaWx0ZXI6IHF1ZXJ5UGFyYW1zLmxpc3REZXBlbmRlbnRGaWx0ZXIsXHJcbiAgICAgICAgICAgIC8vICBlbWFpbExpbmtJZDogcXVlcnlQYXJhbXMuZW1haWxMaW5rSWQsXHJcbiAgICAgICAgICAgIHNvcnRCeTogcXVlcnlQYXJhbXMuc29ydEJ5LFxyXG4gICAgICAgICAgICBzb3J0T3JkZXI6IHF1ZXJ5UGFyYW1zLnNvcnRPcmRlcixcclxuICAgICAgICAgICAgcGFnZU51bWJlcjogcXVlcnlQYXJhbXMucGFnZU51bWJlcixcclxuICAgICAgICAgICAgcGFnZVNpemU6IHF1ZXJ5UGFyYW1zLnBhZ2VTaXplLFxyXG4gICAgICAgICAgICBzZWFyY2hOYW1lOiBzZWFyY2hOYW1lLFxyXG4gICAgICAgICAgICBzYXZlZFNlYXJjaE5hbWU6IHNhdmVkU2VhcmNoTmFtZSxcclxuICAgICAgICAgICAgYWR2U2VhcmNoRGF0YTogYWR2U2VhcmNoRGF0YSxcclxuICAgICAgICAgICAgZmFjZXREYXRhOiBmYWNldERhdGFcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufVxyXG5cclxufSJdfQ==