import { __decorate } from "tslib";
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AppService } from '../../mpm-utils/services/app.service';
import * as acronui from '../../mpm-utils/auth/utility';
import { NotificationService } from '../../notification/notification.service';
import { FieldConfigService } from './field-config.service';
import { SharingService } from '../../mpm-utils/services/sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "../../mpm-utils/services/app.service";
import * as i2 from "../../mpm-utils/services/sharing.service";
import * as i3 from "./field-config.service";
import * as i4 from "../../notification/notification.service";
var SearchConfigService = /** @class */ (function () {
    function SearchConfigService(appService, sharingService, fieldConfigService, notificationService) {
        this.appService = appService;
        this.sharingService = sharingService;
        this.fieldConfigService = fieldConfigService;
        this.notificationService = notificationService;
        this.SEARCH_CONFIG_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.SEARCH_CONFIG_WS = 'GetSearchFieldConfig';
        this.GET_PM_SEARCH_FIELDS_WS = 'GetR_PM_SEARCH_FIELDS';
        this.GET_PM_SEARCH_FIELDS_NS = 'http://schemas/AcheronMPMCore/MPM_Advanced_Search_Config/operations';
        this.GetAllAdvancedSearchOperatorsWS = 'GetAllAdvancedSearchOperators';
        this.GetAllAdvancedSearchOperatorsNS = 'http://schemas/AcheronMPMCore/MPM_Advanced_Search_Operators/operations';
        this.GetSavedSearchesWS = 'GetSavedSearches';
        this.GetSavedSearchesNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
        this.GetSavedSearchByIdWS = 'GetSavedSearchById';
        this.GetSavedSearchByIdNS = 'http://schemas/AcheronMPMCore/MPM_Saved_Searches/operations';
    }
    SearchConfigService.prototype.getCastedAdvancedSearchConfigResponse = function (searchConfig) {
        var _this = this;
        var advConfigs = [];
        if (Array.isArray(searchConfig) && searchConfig.length) {
            searchConfig.forEach(function (config) {
                var advConfig = {
                    'MPM_Advanced_Search_Config-id': {
                        Id: config.id,
                    },
                    facetConfigId: config.facetConfigId,
                    SEARCH_NAME: config.searchName,
                    SEARCH_SCOPE: config.searchScope,
                    R_PM_Search_Fields: []
                };
                var serachFields = acronui.findObjectsByProp(config, 'field');
                if (Array.isArray(serachFields) && serachFields.length) {
                    serachFields.forEach(function (field) {
                        if (field && field.id) {
                            var matchField = _this.fieldConfigService.getFieldByKeyValue('Id', field.id);
                            if (matchField) {
                                advConfig.R_PM_Search_Fields.push(matchField);
                            }
                        }
                    });
                }
                advConfigs.push(advConfig);
            });
        }
        return advConfigs;
    };
    SearchConfigService.prototype.setAllAdvancedSearchConfig = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (Array.isArray(_this.allAdvancedSearchConfig) && _this.allAdvancedSearchConfig.length) {
                observer.next(_this.allAdvancedSearchConfig);
                observer.complete();
                return;
            }
            _this.appService.invokeRequest(_this.SEARCH_CONFIG_NS, _this.SEARCH_CONFIG_WS, null).subscribe(function (response) {
                var searchConfig = acronui.findObjectsByProp(response, 'AdvancedSearchConfig');
                _this.allAdvancedSearchConfig = _this.getCastedAdvancedSearchConfigResponse(searchConfig);
                observer.next(_this.allAdvancedSearchConfig);
                observer.complete();
            }, function (err) {
                _this.notificationService.error('Something went wrong while getting search config');
                observer.error();
                observer.complete();
            });
        });
    };
    SearchConfigService.prototype.getAdvancedSearchConfigById = function (advancedSearchId) {
        return this.allAdvancedSearchConfig.find(function (searchConfig) {
            return searchConfig['MPM_Advanced_Search_Config-id'].Id === advancedSearchId;
        });
    };
    SearchConfigService.prototype.getAllAdvancedSearchConfigFields = function (advancedSearchId) {
        var advSearch = this.getAdvancedSearchConfigById(advancedSearchId);
        if (advSearch && Array.isArray(advSearch.R_PM_Search_Fields)) {
            return advSearch.R_PM_Search_Fields;
        }
        return [];
    };
    SearchConfigService.prototype.setAllSearchOperators = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (Array.isArray(_this.allSearchOperators) && _this.allSearchOperators.length) {
                observer.next(_this.allSearchOperators);
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.GetAllAdvancedSearchOperatorsNS, _this.GetAllAdvancedSearchOperatorsWS, null).subscribe(function (response) {
                    var searchOperators = acronui.findObjectsByProp(response, 'MPM_Advanced_Search_Operators');
                    searchOperators.forEach(function (operator) {
                        var fieldValueCount = 0;
                        try {
                            fieldValueCount = parseInt(operator.VALUE_COUNT, 0);
                        }
                        catch (e) {
                            console.warn('no value for parsing value count for the operator config');
                        }
                        operator.VALUE_COUNT = fieldValueCount;
                    });
                    _this.allSearchOperators = searchOperators;
                    observer.next(searchOperators);
                    observer.complete();
                }, function (error) {
                    _this.notificationService.error('Something went wrong while getting search operators');
                    observer.error();
                    observer.complete();
                });
            }
        });
    };
    SearchConfigService.prototype.getSavedSearchesForViewAndCurrentUser = function (viewName) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                userId: _this.sharingService.getcurrentUserItemID(),
                view: viewName
            };
            _this.appService.invokeRequest(_this.GetSavedSearchesNS, _this.GetSavedSearchesWS, param).subscribe(function (savedSearchResposne) {
                var savedSearches = acronui.findObjectsByProp(savedSearchResposne, 'MPM_Saved_Searches');
                savedSearches.forEach(function (search) {
                    if (search.SEARCH_DATA && typeof search.SEARCH_DATA === 'string') {
                        search.SEARCH_DATA = JSON.parse(search.SEARCH_DATA);
                    }
                    else {
                        search.SEARCH_DATA = '';
                    }
                });
                observer.next(savedSearches);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    SearchConfigService.prototype.getSavedSearcheById = function (savedSearchId) {
        var _this = this;
        return new Observable(function (observer) {
            var param = {
                savedSearchId: savedSearchId
            };
            _this.appService.invokeRequest(_this.GetSavedSearchByIdNS, _this.GetSavedSearchByIdWS, param).subscribe(function (searchRes) {
                var savedSearches = acronui.findObjectsByProp(searchRes, 'MPM_Saved_Searches');
                var savedSearch = null;
                if (savedSearches && savedSearches[0]) {
                    savedSearch = savedSearches[0];
                    if (savedSearch.SEARCH_DATA && typeof savedSearch.SEARCH_DATA === 'string') {
                        savedSearch.SEARCH_DATA = JSON.parse(savedSearch.SEARCH_DATA);
                    }
                    else {
                        savedSearch.SEARCH_DATA = '';
                    }
                }
                observer.next(savedSearch);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    SearchConfigService.prototype.getAllSearchOperators = function () {
        return this.allSearchOperators;
    };
    SearchConfigService.ctorParameters = function () { return [
        { type: AppService },
        { type: SharingService },
        { type: FieldConfigService },
        { type: NotificationService }
    ]; };
    SearchConfigService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SearchConfigService_Factory() { return new SearchConfigService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.SharingService), i0.ɵɵinject(i3.FieldConfigService), i0.ɵɵinject(i4.NotificationService)); }, token: SearchConfigService, providedIn: "root" });
    SearchConfigService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], SearchConfigService);
    return SearchConfigService;
}());
export { SearchConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2VzL3NlYXJjaC1jb25maWcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNsRSxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBRXhELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQzs7Ozs7O0FBTzFFO0lBRUUsNkJBQ1MsVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsa0JBQXNDLEVBQ3RDLG1CQUF3QztRQUh4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFHakQscUJBQWdCLEdBQUcsK0NBQStDLENBQUM7UUFDbkUscUJBQWdCLEdBQUcsc0JBQXNCLENBQUM7UUFFMUMsNEJBQXVCLEdBQUcsdUJBQXVCLENBQUM7UUFDbEQsNEJBQXVCLEdBQUcscUVBQXFFLENBQUM7UUFFaEcsb0NBQStCLEdBQUcsK0JBQStCLENBQUM7UUFDbEUsb0NBQStCLEdBQUcsd0VBQXdFLENBQUM7UUFFM0csdUJBQWtCLEdBQUcsa0JBQWtCLENBQUM7UUFDeEMsdUJBQWtCLEdBQUcsNkRBQTZELENBQUM7UUFFbkYseUJBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDNUMseUJBQW9CLEdBQUcsNkRBQTZELENBQUM7SUFmakYsQ0FBQztJQW9CTCxtRUFBcUMsR0FBckMsVUFBc0MsWUFBbUI7UUFBekQsaUJBNEJDO1FBM0JDLElBQU0sVUFBVSxHQUE4QixFQUFFLENBQUM7UUFDakQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDdEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07Z0JBQ3pCLElBQU0sU0FBUyxHQUE0QjtvQkFDekMsK0JBQStCLEVBQUU7d0JBQy9CLEVBQUUsRUFBRSxNQUFNLENBQUMsRUFBRTtxQkFDZDtvQkFDRCxhQUFhLEVBQUUsTUFBTSxDQUFDLGFBQWE7b0JBQ25DLFdBQVcsRUFBRSxNQUFNLENBQUMsVUFBVTtvQkFDOUIsWUFBWSxFQUFFLE1BQU0sQ0FBQyxXQUFXO29CQUNoQyxrQkFBa0IsRUFBRSxFQUFFO2lCQUN2QixDQUFDO2dCQUNGLElBQU0sWUFBWSxHQUFVLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO29CQUN0RCxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSzt3QkFDeEIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLEVBQUUsRUFBRTs0QkFDckIsSUFBTSxVQUFVLEdBQWEsS0FBSSxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQ3hGLElBQUksVUFBVSxFQUFFO2dDQUNkLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7NkJBQy9DO3lCQUNGO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUNELFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7SUFFRCx3REFBMEIsR0FBMUI7UUFBQSxpQkFrQkM7UUFqQkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RGLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsT0FBTzthQUNSO1lBQ0QsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNsRyxJQUFNLFlBQVksR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3hGLEtBQUksQ0FBQyx1QkFBdUIsR0FBRyxLQUFJLENBQUMscUNBQXFDLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3hGLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsVUFBQSxHQUFHO2dCQUNKLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsa0RBQWtELENBQUMsQ0FBQztnQkFDbkYsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx5REFBMkIsR0FBM0IsVUFBNEIsZ0JBQXdCO1FBQ2xELE9BQU8sSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVk7WUFDbkQsT0FBTyxZQUFZLENBQUMsK0JBQStCLENBQUMsQ0FBQyxFQUFFLEtBQUssZ0JBQWdCLENBQUM7UUFDL0UsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsOERBQWdDLEdBQWhDLFVBQWlDLGdCQUF3QjtRQUN2RCxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNyRSxJQUFJLFNBQVMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1lBQzVELE9BQU8sU0FBUyxDQUFDLGtCQUFrQixDQUFDO1NBQ3JDO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQsbURBQXFCLEdBQXJCO1FBQUEsaUJBeUJDO1FBeEJDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFO2dCQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUN2QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDckI7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLCtCQUErQixFQUFFLEtBQUksQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNoSSxJQUFNLGVBQWUsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLCtCQUErQixDQUFDLENBQUM7b0JBQzdGLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQSxRQUFRO3dCQUM5QixJQUFJLGVBQWUsR0FBRyxDQUFDLENBQUM7d0JBQ3hCLElBQUk7NEJBQ0YsZUFBZSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO3lCQUNyRDt3QkFBQyxPQUFPLENBQUMsRUFBRTs0QkFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLDBEQUEwRCxDQUFDLENBQUM7eUJBQUU7d0JBQ3pGLFFBQVEsQ0FBQyxXQUFXLEdBQUcsZUFBZSxDQUFDO29CQUN6QyxDQUFDLENBQUMsQ0FBQztvQkFDSCxLQUFJLENBQUMsa0JBQWtCLEdBQUcsZUFBZSxDQUFDO29CQUMxQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUMvQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ04sS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO29CQUN0RixRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2pCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG1FQUFxQyxHQUFyQyxVQUFzQyxRQUFnQjtRQUF0RCxpQkFzQkM7UUFyQkMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDNUIsSUFBTSxLQUFLLEdBQUc7Z0JBQ1osTUFBTSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2xELElBQUksRUFBRSxRQUFRO2FBQ2YsQ0FBQztZQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsbUJBQW1CO2dCQUNsSCxJQUFNLGFBQWEsR0FBVSxPQUFPLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDbEcsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07b0JBQzFCLElBQUksTUFBTSxDQUFDLFdBQVcsSUFBSSxPQUFPLE1BQU0sQ0FBQyxXQUFXLEtBQUssUUFBUSxFQUFFO3dCQUNoRSxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUNyRDt5QkFBTTt3QkFDTCxNQUFNLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztxQkFDekI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ04sUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxpREFBbUIsR0FBbkIsVUFBb0IsYUFBcUI7UUFBekMsaUJBdUJDO1FBdEJDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzVCLElBQU0sS0FBSyxHQUFHO2dCQUNaLGFBQWEsZUFBQTthQUNkLENBQUM7WUFDRixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFNBQVM7Z0JBQzVHLElBQU0sYUFBYSxHQUFVLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDeEYsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3JDLFdBQVcsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQy9CLElBQUksV0FBVyxDQUFDLFdBQVcsSUFBSSxPQUFPLFdBQVcsQ0FBQyxXQUFXLEtBQUssUUFBUSxFQUFFO3dCQUMxRSxXQUFXLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUMvRDt5QkFBTTt3QkFDTCxXQUFXLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztxQkFDOUI7aUJBQ0Y7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ04sUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxtREFBcUIsR0FBckI7UUFDRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNqQyxDQUFDOztnQkF0S29CLFVBQVU7Z0JBQ04sY0FBYztnQkFDVixrQkFBa0I7Z0JBQ2pCLG1CQUFtQjs7O0lBTnRDLG1CQUFtQjtRQUgvQixVQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO09BQ1csbUJBQW1CLENBMksvQjs4QkEzTEQ7Q0EyTEMsQUEzS0QsSUEyS0M7U0EzS1ksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5pbXBvcnQgeyBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyB9IGZyb20gJy4uLy4uL21wbS11dGlscy9vYmplY3RzL01QTUFkdmFuY2VkU2VhcmNoQ29uZmlnJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE1QTUZpZWxkIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBNUE1TZWFyY2hPcGVyYXRvciB9IGZyb20gJy4vb2JqZWN0cy9NUE1TZWFyY2hPcGVyYXRvcic7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4vZmllbGQtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNUE1TYXZlZFNlYXJjaCB9IGZyb20gJy4vb2JqZWN0cy9NUE1TYXZlZFNlYXJjaCc7XHJcbmltcG9ydCB7IE1QTURlbGV0ZVNhdmVkU2VhcmNoUmVxIH0gZnJvbSAnLi9vYmplY3RzL01QTURlbGV0ZVNhdmVkU2VhcmNoUmVxJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlYXJjaENvbmZpZ1NlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyBmaWVsZENvbmZpZ1NlcnZpY2U6IEZpZWxkQ29uZmlnU2VydmljZSxcclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICkgeyB9XHJcblxyXG4gIFNFQVJDSF9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICBTRUFSQ0hfQ09ORklHX1dTID0gJ0dldFNlYXJjaEZpZWxkQ29uZmlnJztcclxuXHJcbiAgR0VUX1BNX1NFQVJDSF9GSUVMRFNfV1MgPSAnR2V0Ul9QTV9TRUFSQ0hfRklFTERTJztcclxuICBHRVRfUE1fU0VBUkNIX0ZJRUxEU19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy9vcGVyYXRpb25zJztcclxuXHJcbiAgR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNXUyA9ICdHZXRBbGxBZHZhbmNlZFNlYXJjaE9wZXJhdG9ycyc7XHJcbiAgR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQWR2YW5jZWRfU2VhcmNoX09wZXJhdG9ycy9vcGVyYXRpb25zJztcclxuXHJcbiAgR2V0U2F2ZWRTZWFyY2hlc1dTID0gJ0dldFNhdmVkU2VhcmNoZXMnO1xyXG4gIEdldFNhdmVkU2VhcmNoZXNOUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU2F2ZWRfU2VhcmNoZXMvb3BlcmF0aW9ucyc7XHJcblxyXG4gIEdldFNhdmVkU2VhcmNoQnlJZFdTID0gJ0dldFNhdmVkU2VhcmNoQnlJZCc7XHJcbiAgR2V0U2F2ZWRTZWFyY2hCeUlkTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1NhdmVkX1NlYXJjaGVzL29wZXJhdGlvbnMnO1xyXG5cclxuICBhbGxBZHZhbmNlZFNlYXJjaENvbmZpZzogTVBNQWR2YW5jZWRTZWFyY2hDb25maWdbXTtcclxuICBhbGxTZWFyY2hPcGVyYXRvcnM6IE1QTVNlYXJjaE9wZXJhdG9yW107XHJcblxyXG4gIGdldENhc3RlZEFkdmFuY2VkU2VhcmNoQ29uZmlnUmVzcG9uc2Uoc2VhcmNoQ29uZmlnOiBhbnlbXSk6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnW10ge1xyXG4gICAgY29uc3QgYWR2Q29uZmlnczogTVBNQWR2YW5jZWRTZWFyY2hDb25maWdbXSA9IFtdO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoc2VhcmNoQ29uZmlnKSAmJiBzZWFyY2hDb25maWcubGVuZ3RoKSB7XHJcbiAgICAgIHNlYXJjaENvbmZpZy5mb3JFYWNoKGNvbmZpZyA9PiB7XHJcbiAgICAgICAgY29uc3QgYWR2Q29uZmlnOiBNUE1BZHZhbmNlZFNlYXJjaENvbmZpZyA9IHtcclxuICAgICAgICAgICdNUE1fQWR2YW5jZWRfU2VhcmNoX0NvbmZpZy1pZCc6IHtcclxuICAgICAgICAgICAgSWQ6IGNvbmZpZy5pZCxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBmYWNldENvbmZpZ0lkOiBjb25maWcuZmFjZXRDb25maWdJZCxcclxuICAgICAgICAgIFNFQVJDSF9OQU1FOiBjb25maWcuc2VhcmNoTmFtZSxcclxuICAgICAgICAgIFNFQVJDSF9TQ09QRTogY29uZmlnLnNlYXJjaFNjb3BlLFxyXG4gICAgICAgICAgUl9QTV9TZWFyY2hfRmllbGRzOiBbXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3Qgc2VyYWNoRmllbGRzOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoY29uZmlnLCAnZmllbGQnKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShzZXJhY2hGaWVsZHMpICYmIHNlcmFjaEZpZWxkcy5sZW5ndGgpIHtcclxuICAgICAgICAgIHNlcmFjaEZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLmlkKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgbWF0Y2hGaWVsZDogTVBNRmllbGQgPSB0aGlzLmZpZWxkQ29uZmlnU2VydmljZS5nZXRGaWVsZEJ5S2V5VmFsdWUoJ0lkJywgZmllbGQuaWQpO1xyXG4gICAgICAgICAgICAgIGlmIChtYXRjaEZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBhZHZDb25maWcuUl9QTV9TZWFyY2hfRmllbGRzLnB1c2gobWF0Y2hGaWVsZCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgYWR2Q29uZmlncy5wdXNoKGFkdkNvbmZpZyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFkdkNvbmZpZ3M7XHJcbiAgfVxyXG5cclxuICBzZXRBbGxBZHZhbmNlZFNlYXJjaENvbmZpZygpOiBPYnNlcnZhYmxlPEFycmF5PE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnPj4ge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5hbGxBZHZhbmNlZFNlYXJjaENvbmZpZykgJiYgdGhpcy5hbGxBZHZhbmNlZFNlYXJjaENvbmZpZy5sZW5ndGgpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsQWR2YW5jZWRTZWFyY2hDb25maWcpO1xyXG4gICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU0VBUkNIX0NPTkZJR19OUywgdGhpcy5TRUFSQ0hfQ09ORklHX1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaENvbmZpZzogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnQWR2YW5jZWRTZWFyY2hDb25maWcnKTtcclxuICAgICAgICB0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnID0gdGhpcy5nZXRDYXN0ZWRBZHZhbmNlZFNlYXJjaENvbmZpZ1Jlc3BvbnNlKHNlYXJjaENvbmZpZyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnIgPT4ge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBzZWFyY2ggY29uZmlnJyk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWR2YW5jZWRTZWFyY2hDb25maWdCeUlkKGFkdmFuY2VkU2VhcmNoSWQ6IHN0cmluZyk6IE1QTUFkdmFuY2VkU2VhcmNoQ29uZmlnIHtcclxuICAgIHJldHVybiB0aGlzLmFsbEFkdmFuY2VkU2VhcmNoQ29uZmlnLmZpbmQoc2VhcmNoQ29uZmlnID0+IHtcclxuICAgICAgcmV0dXJuIHNlYXJjaENvbmZpZ1snTVBNX0FkdmFuY2VkX1NlYXJjaF9Db25maWctaWQnXS5JZCA9PT0gYWR2YW5jZWRTZWFyY2hJZDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWxsQWR2YW5jZWRTZWFyY2hDb25maWdGaWVsZHMoYWR2YW5jZWRTZWFyY2hJZDogc3RyaW5nKTogTVBNRmllbGRbXSB7XHJcbiAgICBjb25zdCBhZHZTZWFyY2ggPSB0aGlzLmdldEFkdmFuY2VkU2VhcmNoQ29uZmlnQnlJZChhZHZhbmNlZFNlYXJjaElkKTtcclxuICAgIGlmIChhZHZTZWFyY2ggJiYgQXJyYXkuaXNBcnJheShhZHZTZWFyY2guUl9QTV9TZWFyY2hfRmllbGRzKSkge1xyXG4gICAgICByZXR1cm4gYWR2U2VhcmNoLlJfUE1fU2VhcmNoX0ZpZWxkcztcclxuICAgIH1cclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIHNldEFsbFNlYXJjaE9wZXJhdG9ycygpOiBPYnNlcnZhYmxlPE1QTVNlYXJjaE9wZXJhdG9yW10+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzKSAmJiB0aGlzLmFsbFNlYXJjaE9wZXJhdG9ycy5sZW5ndGgpIHtcclxuICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR2V0QWxsQWR2YW5jZWRTZWFyY2hPcGVyYXRvcnNOUywgdGhpcy5HZXRBbGxBZHZhbmNlZFNlYXJjaE9wZXJhdG9yc1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgY29uc3Qgc2VhcmNoT3BlcmF0b3JzID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9BZHZhbmNlZF9TZWFyY2hfT3BlcmF0b3JzJyk7XHJcbiAgICAgICAgICBzZWFyY2hPcGVyYXRvcnMuZm9yRWFjaChvcGVyYXRvciA9PiB7XHJcbiAgICAgICAgICAgIGxldCBmaWVsZFZhbHVlQ291bnQgPSAwO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgIGZpZWxkVmFsdWVDb3VudCA9IHBhcnNlSW50KG9wZXJhdG9yLlZBTFVFX0NPVU5ULCAwKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZSkgeyBjb25zb2xlLndhcm4oJ25vIHZhbHVlIGZvciBwYXJzaW5nIHZhbHVlIGNvdW50IGZvciB0aGUgb3BlcmF0b3IgY29uZmlnJyk7IH1cclxuICAgICAgICAgICAgb3BlcmF0b3IuVkFMVUVfQ09VTlQgPSBmaWVsZFZhbHVlQ291bnQ7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuYWxsU2VhcmNoT3BlcmF0b3JzID0gc2VhcmNoT3BlcmF0b3JzO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIubmV4dChzZWFyY2hPcGVyYXRvcnMpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgc2VhcmNoIG9wZXJhdG9ycycpO1xyXG4gICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2F2ZWRTZWFyY2hlc0ZvclZpZXdBbmRDdXJyZW50VXNlcih2aWV3TmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxNUE1TYXZlZFNlYXJjaFtdPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICB1c2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKSxcclxuICAgICAgICB2aWV3OiB2aWV3TmFtZVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkdldFNhdmVkU2VhcmNoZXNOUywgdGhpcy5HZXRTYXZlZFNlYXJjaGVzV1MsIHBhcmFtKS5zdWJzY3JpYmUoc2F2ZWRTZWFyY2hSZXNwb3NuZSA9PiB7XHJcbiAgICAgICAgY29uc3Qgc2F2ZWRTZWFyY2hlczogYW55W10gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHNhdmVkU2VhcmNoUmVzcG9zbmUsICdNUE1fU2F2ZWRfU2VhcmNoZXMnKTtcclxuICAgICAgICBzYXZlZFNlYXJjaGVzLmZvckVhY2goc2VhcmNoID0+IHtcclxuICAgICAgICAgIGlmIChzZWFyY2guU0VBUkNIX0RBVEEgJiYgdHlwZW9mIHNlYXJjaC5TRUFSQ0hfREFUQSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgc2VhcmNoLlNFQVJDSF9EQVRBID0gSlNPTi5wYXJzZShzZWFyY2guU0VBUkNIX0RBVEEpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2VhcmNoLlNFQVJDSF9EQVRBID0gJyc7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChzYXZlZFNlYXJjaGVzKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoKTtcclxuICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0U2F2ZWRTZWFyY2hlQnlJZChzYXZlZFNlYXJjaElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE1QTVNhdmVkU2VhcmNoPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICBjb25zdCBwYXJhbSA9IHtcclxuICAgICAgICBzYXZlZFNlYXJjaElkXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuR2V0U2F2ZWRTZWFyY2hCeUlkTlMsIHRoaXMuR2V0U2F2ZWRTZWFyY2hCeUlkV1MsIHBhcmFtKS5zdWJzY3JpYmUoc2VhcmNoUmVzID0+IHtcclxuICAgICAgICBjb25zdCBzYXZlZFNlYXJjaGVzOiBhbnlbXSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3Aoc2VhcmNoUmVzLCAnTVBNX1NhdmVkX1NlYXJjaGVzJyk7XHJcbiAgICAgICAgbGV0IHNhdmVkU2VhcmNoID0gbnVsbDtcclxuICAgICAgICBpZiAoc2F2ZWRTZWFyY2hlcyAmJiBzYXZlZFNlYXJjaGVzWzBdKSB7XHJcbiAgICAgICAgICBzYXZlZFNlYXJjaCA9IHNhdmVkU2VhcmNoZXNbMF07XHJcbiAgICAgICAgICBpZiAoc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEgJiYgdHlwZW9mIHNhdmVkU2VhcmNoLlNFQVJDSF9EQVRBID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzYXZlZFNlYXJjaC5TRUFSQ0hfREFUQSA9IEpTT04ucGFyc2Uoc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2F2ZWRTZWFyY2guU0VBUkNIX0RBVEEgPSAnJztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgb2JzZXJ2ZXIubmV4dChzYXZlZFNlYXJjaCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIG9ic2VydmVyLmVycm9yKCk7XHJcbiAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEFsbFNlYXJjaE9wZXJhdG9ycygpOiBNUE1TZWFyY2hPcGVyYXRvcltdIHtcclxuICAgIHJldHVybiB0aGlzLmFsbFNlYXJjaE9wZXJhdG9ycztcclxuICB9XHJcblxyXG59XHJcbiJdfQ==