export var GroupByFilterTypes;
(function (GroupByFilterTypes) {
    /* GROUP_BY_PROJECT = 'GROUP_BY_PROJECT',
    GROUP_BY_TASK = 'GROUP_BY_TASK' */
    GroupByFilterTypes["GROUP_BY_CAMPAIGN"] = "GROUP_BY_CAMPAIGN";
    GroupByFilterTypes["GROUP_BY_PROJECT"] = "GROUP_BY_PROJECT";
    GroupByFilterTypes["GROUP_BY_TASK"] = "GROUP_BY_TASK";
})(GroupByFilterTypes || (GroupByFilterTypes = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiR3JvdXBCeUZpbHRlclR5cGVzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbnN0YW50cy9Hcm91cEJ5RmlsdGVyVHlwZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFOLElBQVksa0JBTVg7QUFORCxXQUFZLGtCQUFrQjtJQUMxQjtzQ0FDa0M7SUFDbEMsNkRBQXVDLENBQUE7SUFDdkMsMkRBQXFDLENBQUE7SUFDckMscURBQStCLENBQUE7QUFDbkMsQ0FBQyxFQU5XLGtCQUFrQixLQUFsQixrQkFBa0IsUUFNN0IiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBHcm91cEJ5RmlsdGVyVHlwZXMge1xyXG4gICAgLyogR1JPVVBfQllfUFJPSkVDVCA9ICdHUk9VUF9CWV9QUk9KRUNUJyxcclxuICAgIEdST1VQX0JZX1RBU0sgPSAnR1JPVVBfQllfVEFTSycgKi9cclxuICAgIEdST1VQX0JZX0NBTVBBSUdOID0gJ0dST1VQX0JZX0NBTVBBSUdOJyxcclxuICAgIEdST1VQX0JZX1BST0pFQ1QgPSAnR1JPVVBfQllfUFJPSkVDVCcsXHJcbiAgICBHUk9VUF9CWV9UQVNLID0gJ0dST1VQX0JZX1RBU0snXHJcbn1cclxuIl19