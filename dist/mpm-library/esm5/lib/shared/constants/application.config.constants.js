export var ApplicationConfigConstants = {
    MPM_PROJECT_CONFIG: {
        ENABLE_PROJECT_TEMPLATE_CREATION: 'ENABLE_PROJECT_TEMPLATE_CREATION',
        ENABLE_CREATE_PROJECT: 'ENABLE_CREATE_PROJECT',
        ENABLE_CREATE_TEMPLATE: 'ENABLE_CREATE_TEMPLATE',
        ENABLE_COPY_PROJECT: 'ENABLE_COPY_PROJECT',
        SHOW_TEMPLATE: 'SHOW_TEMPLATE',
        SHOW_STANDARD_STATUS: 'SHOW_STANDARD_STATUS',
        SHOW_REVISION_REVIEW_REQUIRED: 'SHOW_REVISION_REVIEW_REQUIRED',
        ENABLE_NEED_REVIEW: 'ENABLE_NEED_REVIEW',
        DEFAULT_REVISION_REQUIRED_STATUS: 'DEFAULT_REVISION_REQUIRED_STATUS',
        PROJECT_THUMBNAIL_TYPE: 'PROJECT_THUMBNAIL_TYPE',
        ENABLE_CREATE_CAMPAIGN: 'ENABLE_CREATE_CAMPAIGN',
        ENABLE_REVIEW_APPROVAL: 'ENABLE_REVIEW_APPROVAL',
        ENABLE_DURATION: 'ENABLE_DURATION',
        ENABLE_WORK_WEEK: 'ENABLE_WORK_WEEK',
        ENABLE_DUPLICATE_NAMES: 'ENABLE_DUPLICATE_NAMES'
    },
    MPM_APP_CONFIG: {
        DEFAULT_PROJECT_VIEW: 'DEFAULT_PROJECT_VIEW',
        ENABLE_DUPLICATE_DELIVERABLES: 'ENABLE_DUPLICATE_DELIVERABLES',
        DEFAULT_TASK_VIEW: 'DEFAULT_TASK_VIEW',
        SHOW_TASK_SLA: 'SHOW_TASK_SLA',
        SHOW_REMINDER_OPTION: 'SHOW_REMINDER_OPTION',
        EXTERNAL_APP_ACCESS: 'EXTERNAL_APP_ACCESS',
        OPEN_APP_IN_NEW_TAB: 'OPEN_APP_IN_NEW_TAB',
        SHOW_MY_APPLICATION: 'SHOW_MY_APPLICATION',
        DEFAULT_MYTASK_VIEW_COLLAPSED: 'DEFAULT_MYTASK_VIEW_COLLAPSED',
        NAME_STRING_PATTERN: 'NAME_STRING_PATTERN',
        HIGHLIGHT_HIGH_PRIORITY_PROJECT: 'HIGHLIGHT_HIGH_PRIORITY_PROJECT',
        SHOW_PROJECT_LOGO: 'SHOW_PROJECT_LOGO',
        UPDATE_PROJECT_LOGO: 'UPDATE_PROJECT_LOGO',
        ONLINE_PROOFING_TOOL: 'ONLINE_PROOFING_TOOL',
        ENABLE_ASSET_DOWNLOAD: 'ENABLE_ASSET_DOWNLOAD',
        DISABLE_GRID_VIEW: 'DISABLE_GRID_VIEW',
        ENABLE_DELEGATION: 'ENABLE_DELEGATION',
        ENABLE_REVOKE: 'ENABLE_REVOKE',
        ENABLE_PROJECT_TRANSFER: 'ENABLE_PROJECT_TRANSFER',
        ENABLE_SKIP_TASK: 'ENABLE_SKIP_TASK',
        ENABLE_SYSTEM_NOTIFICATION: 'ENABLE_SYSTEM_NOTIFICATION',
        OVERRIDE_APPROVAL_DECISION: 'OVERRIDE_APPROVAL_DECISION',
        OVERRIDE_UPLOAD_ASSET: 'OVERRIDE_UPLOAD_ASSET',
        DEFAULT_PAGINATION: 'DEFAULT_PAGINATION',
        DEFAULT_SORT_ORDER: 'DEFAULT_SORT_ORDER',
        ALLOW_ACTION_TASK: 'ALLOW_ACTION_TASK',
        ENABLE_CAMPAIGN: 'ENABLE_CAMPAIGN',
        ENABLE_REFERENCE_ASSET: 'ENABLE_REFERENCE_ASSET',
        ENABLE_CUSTOM_WORKFLOW: 'ENABLE_CUSTOM_WORKFLOW',
        ENABLE_PROJECT_RESTART: 'ENABLE_PROJECT_RESTART',
        EXPORT_DATA_FILE_PATH: 'EXPORT_DATA_FILE_PATH',
        EXPORT_DATA_PAGE_LIMIT: 'EXPORT_DATA_PAGE_LIMIT',
        EXPORT_DATA_MIN_COUNT: 'EXPORT_DATA_MIN_COUNT'
    },
    MPM_ASSET_CONFIG: {
        MAX_NO_OF_FILES: 'MAX_NO_OF_FILES',
        MAX_FILE_SIZE: 'MAX_FILE_SIZE',
        ALLOWED_FILE_TYPES: 'ALLOWED_FILE_TYPES'
    },
    MPM_BRAND_CONFIG: {
        FAV_ICON_PATH: 'FAV_ICON_PATH',
        APP_LOGO_PATH: 'APP_LOGO_PATH',
        APP_THEME_NAME: 'APP_THEME_NAME',
        DEFAULT_BRANDING_STYLE_PATH: 'DEFAULT_BRANDING_STYLE_PATH',
        SHOW_COPYRIGHT: 'SHOW_COPYRIGHT'
    },
    MPM_COMMENTS_CONFIG: {
        SHOW_EXTERNAL_INTERNAL_COMMENT_SWITCH: 'SHOW_EXTERNAL_INTERNAL_COMMENT_SWITCH',
        DEFAULT_COMMENT: 'DEFAULT_COMMENT',
        EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL: 'EXTERNAL_INTERNAL_COMMENT_SWITCH_LABEL',
        COMMENTS_ITEM_PER_PAGE: 'COMMENTS_ITEM_PER_PAGE',
        EXTERNAL_COMMENT_LABEL: 'EXTERNAL_COMMENT_LABEL',
        INTERNAL_COMMENT_LABEL: 'INTERNAL_COMMENT_LABEL',
        ENABLE_COMMENT_NOTIFICATION: 'ENABLE_COMMENT_NOTIFICATION'
    },
    OTMM_SYSTEM_DETAILS_CONSTANTS: {
        'BUILD_VERSION': 'BUILD_VERSION'
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQUMsSUFBTSwwQkFBMEIsR0FBRztJQUN0QyxrQkFBa0IsRUFBRTtRQUNoQixnQ0FBZ0MsRUFBRSxrQ0FBa0M7UUFDcEUscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLHNCQUFzQixFQUFFLHdCQUF3QjtRQUNoRCxtQkFBbUIsRUFBRSxxQkFBcUI7UUFDMUMsYUFBYSxFQUFFLGVBQWU7UUFDOUIsb0JBQW9CLEVBQUUsc0JBQXNCO1FBQzVDLDZCQUE2QixFQUFFLCtCQUErQjtRQUM5RCxrQkFBa0IsRUFBRSxvQkFBb0I7UUFDeEMsZ0NBQWdDLEVBQUUsa0NBQWtDO1FBQ3BFLHNCQUFzQixFQUFFLHdCQUF3QjtRQUNoRCxzQkFBc0IsRUFBRSx3QkFBd0I7UUFDaEQsc0JBQXNCLEVBQUUsd0JBQXdCO1FBQ2hELGVBQWUsRUFBRSxpQkFBaUI7UUFDbEMsZ0JBQWdCLEVBQUUsa0JBQWtCO1FBQ3BDLHNCQUFzQixFQUFFLHdCQUF3QjtLQUNuRDtJQUNELGNBQWMsRUFBRTtRQUNaLG9CQUFvQixFQUFFLHNCQUFzQjtRQUM1Qyw2QkFBNkIsRUFBRSwrQkFBK0I7UUFDOUQsaUJBQWlCLEVBQUUsbUJBQW1CO1FBQ3RDLGFBQWEsRUFBRSxlQUFlO1FBQzlCLG9CQUFvQixFQUFFLHNCQUFzQjtRQUM1QyxtQkFBbUIsRUFBRSxxQkFBcUI7UUFDMUMsbUJBQW1CLEVBQUUscUJBQXFCO1FBQzFDLG1CQUFtQixFQUFFLHFCQUFxQjtRQUMxQyw2QkFBNkIsRUFBRSwrQkFBK0I7UUFDOUQsbUJBQW1CLEVBQUUscUJBQXFCO1FBQzFDLCtCQUErQixFQUFFLGlDQUFpQztRQUNsRSxpQkFBaUIsRUFBRSxtQkFBbUI7UUFDdEMsbUJBQW1CLEVBQUUscUJBQXFCO1FBQzFDLG9CQUFvQixFQUFFLHNCQUFzQjtRQUM1QyxxQkFBcUIsRUFBRSx1QkFBdUI7UUFDOUMsaUJBQWlCLEVBQUUsbUJBQW1CO1FBQ3RDLGlCQUFpQixFQUFFLG1CQUFtQjtRQUN0QyxhQUFhLEVBQUUsZUFBZTtRQUM5Qix1QkFBdUIsRUFBRSx5QkFBeUI7UUFDbEQsZ0JBQWdCLEVBQUUsa0JBQWtCO1FBQ3BDLDBCQUEwQixFQUFFLDRCQUE0QjtRQUN4RCwwQkFBMEIsRUFBRSw0QkFBNEI7UUFDeEQscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLGtCQUFrQixFQUFFLG9CQUFvQjtRQUN4QyxrQkFBa0IsRUFBRSxvQkFBb0I7UUFDeEMsaUJBQWlCLEVBQUUsbUJBQW1CO1FBQ3RDLGVBQWUsRUFBRSxpQkFBaUI7UUFDbEMsc0JBQXNCLEVBQUUsd0JBQXdCO1FBQ2hELHNCQUFzQixFQUFFLHdCQUF3QjtRQUNoRCxzQkFBc0IsRUFBRSx3QkFBd0I7UUFDaEQscUJBQXFCLEVBQUUsdUJBQXVCO1FBQzlDLHNCQUFzQixFQUFFLHdCQUF3QjtRQUNoRCxxQkFBcUIsRUFBRSx1QkFBdUI7S0FDakQ7SUFDRCxnQkFBZ0IsRUFBRTtRQUNkLGVBQWUsRUFBRSxpQkFBaUI7UUFDbEMsYUFBYSxFQUFFLGVBQWU7UUFDOUIsa0JBQWtCLEVBQUUsb0JBQW9CO0tBQzNDO0lBQ0QsZ0JBQWdCLEVBQUU7UUFDZCxhQUFhLEVBQUUsZUFBZTtRQUM5QixhQUFhLEVBQUUsZUFBZTtRQUM5QixjQUFjLEVBQUUsZ0JBQWdCO1FBQ2hDLDJCQUEyQixFQUFFLDZCQUE2QjtRQUMxRCxjQUFjLEVBQUUsZ0JBQWdCO0tBQ25DO0lBQ0QsbUJBQW1CLEVBQUU7UUFDakIscUNBQXFDLEVBQUUsdUNBQXVDO1FBQzlFLGVBQWUsRUFBRSxpQkFBaUI7UUFDbEMsc0NBQXNDLEVBQUUsd0NBQXdDO1FBQ2hGLHNCQUFzQixFQUFFLHdCQUF3QjtRQUNoRCxzQkFBc0IsRUFBRSx3QkFBd0I7UUFDaEQsc0JBQXNCLEVBQUUsd0JBQXdCO1FBQ2hELDJCQUEyQixFQUFFLDZCQUE2QjtLQUM3RDtJQUNELDZCQUE2QixFQUFFO1FBQzNCLGVBQWUsRUFBRSxlQUFlO0tBQ25DO0NBRUosQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyA9IHtcclxuICAgIE1QTV9QUk9KRUNUX0NPTkZJRzoge1xyXG4gICAgICAgIEVOQUJMRV9QUk9KRUNUX1RFTVBMQVRFX0NSRUFUSU9OOiAnRU5BQkxFX1BST0pFQ1RfVEVNUExBVEVfQ1JFQVRJT04nLFxyXG4gICAgICAgIEVOQUJMRV9DUkVBVEVfUFJPSkVDVDogJ0VOQUJMRV9DUkVBVEVfUFJPSkVDVCcsXHJcbiAgICAgICAgRU5BQkxFX0NSRUFURV9URU1QTEFURTogJ0VOQUJMRV9DUkVBVEVfVEVNUExBVEUnLFxyXG4gICAgICAgIEVOQUJMRV9DT1BZX1BST0pFQ1Q6ICdFTkFCTEVfQ09QWV9QUk9KRUNUJyxcclxuICAgICAgICBTSE9XX1RFTVBMQVRFOiAnU0hPV19URU1QTEFURScsXHJcbiAgICAgICAgU0hPV19TVEFOREFSRF9TVEFUVVM6ICdTSE9XX1NUQU5EQVJEX1NUQVRVUycsXHJcbiAgICAgICAgU0hPV19SRVZJU0lPTl9SRVZJRVdfUkVRVUlSRUQ6ICdTSE9XX1JFVklTSU9OX1JFVklFV19SRVFVSVJFRCcsXHJcbiAgICAgICAgRU5BQkxFX05FRURfUkVWSUVXOiAnRU5BQkxFX05FRURfUkVWSUVXJyxcclxuICAgICAgICBERUZBVUxUX1JFVklTSU9OX1JFUVVJUkVEX1NUQVRVUzogJ0RFRkFVTFRfUkVWSVNJT05fUkVRVUlSRURfU1RBVFVTJyxcclxuICAgICAgICBQUk9KRUNUX1RIVU1CTkFJTF9UWVBFOiAnUFJPSkVDVF9USFVNQk5BSUxfVFlQRScsXHJcbiAgICAgICAgRU5BQkxFX0NSRUFURV9DQU1QQUlHTjogJ0VOQUJMRV9DUkVBVEVfQ0FNUEFJR04nLFxyXG4gICAgICAgIEVOQUJMRV9SRVZJRVdfQVBQUk9WQUw6ICdFTkFCTEVfUkVWSUVXX0FQUFJPVkFMJyxcclxuICAgICAgICBFTkFCTEVfRFVSQVRJT046ICdFTkFCTEVfRFVSQVRJT04nLFxyXG4gICAgICAgIEVOQUJMRV9XT1JLX1dFRUs6ICdFTkFCTEVfV09SS19XRUVLJyxcclxuICAgICAgICBFTkFCTEVfRFVQTElDQVRFX05BTUVTOiAnRU5BQkxFX0RVUExJQ0FURV9OQU1FUydcclxuICAgIH0sXHJcbiAgICBNUE1fQVBQX0NPTkZJRzoge1xyXG4gICAgICAgIERFRkFVTFRfUFJPSkVDVF9WSUVXOiAnREVGQVVMVF9QUk9KRUNUX1ZJRVcnLFxyXG4gICAgICAgIEVOQUJMRV9EVVBMSUNBVEVfREVMSVZFUkFCTEVTOiAnRU5BQkxFX0RVUExJQ0FURV9ERUxJVkVSQUJMRVMnLFxyXG4gICAgICAgIERFRkFVTFRfVEFTS19WSUVXOiAnREVGQVVMVF9UQVNLX1ZJRVcnLFxyXG4gICAgICAgIFNIT1dfVEFTS19TTEE6ICdTSE9XX1RBU0tfU0xBJyxcclxuICAgICAgICBTSE9XX1JFTUlOREVSX09QVElPTjogJ1NIT1dfUkVNSU5ERVJfT1BUSU9OJyxcclxuICAgICAgICBFWFRFUk5BTF9BUFBfQUNDRVNTOiAnRVhURVJOQUxfQVBQX0FDQ0VTUycsXHJcbiAgICAgICAgT1BFTl9BUFBfSU5fTkVXX1RBQjogJ09QRU5fQVBQX0lOX05FV19UQUInLFxyXG4gICAgICAgIFNIT1dfTVlfQVBQTElDQVRJT046ICdTSE9XX01ZX0FQUExJQ0FUSU9OJyxcclxuICAgICAgICBERUZBVUxUX01ZVEFTS19WSUVXX0NPTExBUFNFRDogJ0RFRkFVTFRfTVlUQVNLX1ZJRVdfQ09MTEFQU0VEJyxcclxuICAgICAgICBOQU1FX1NUUklOR19QQVRURVJOOiAnTkFNRV9TVFJJTkdfUEFUVEVSTicsXHJcbiAgICAgICAgSElHSExJR0hUX0hJR0hfUFJJT1JJVFlfUFJPSkVDVDogJ0hJR0hMSUdIVF9ISUdIX1BSSU9SSVRZX1BST0pFQ1QnLFxyXG4gICAgICAgIFNIT1dfUFJPSkVDVF9MT0dPOiAnU0hPV19QUk9KRUNUX0xPR08nLFxyXG4gICAgICAgIFVQREFURV9QUk9KRUNUX0xPR086ICdVUERBVEVfUFJPSkVDVF9MT0dPJyxcclxuICAgICAgICBPTkxJTkVfUFJPT0ZJTkdfVE9PTDogJ09OTElORV9QUk9PRklOR19UT09MJyxcclxuICAgICAgICBFTkFCTEVfQVNTRVRfRE9XTkxPQUQ6ICdFTkFCTEVfQVNTRVRfRE9XTkxPQUQnLFxyXG4gICAgICAgIERJU0FCTEVfR1JJRF9WSUVXOiAnRElTQUJMRV9HUklEX1ZJRVcnLFxyXG4gICAgICAgIEVOQUJMRV9ERUxFR0FUSU9OOiAnRU5BQkxFX0RFTEVHQVRJT04nLFxyXG4gICAgICAgIEVOQUJMRV9SRVZPS0U6ICdFTkFCTEVfUkVWT0tFJyxcclxuICAgICAgICBFTkFCTEVfUFJPSkVDVF9UUkFOU0ZFUjogJ0VOQUJMRV9QUk9KRUNUX1RSQU5TRkVSJyxcclxuICAgICAgICBFTkFCTEVfU0tJUF9UQVNLOiAnRU5BQkxFX1NLSVBfVEFTSycsXHJcbiAgICAgICAgRU5BQkxFX1NZU1RFTV9OT1RJRklDQVRJT046ICdFTkFCTEVfU1lTVEVNX05PVElGSUNBVElPTicsXHJcbiAgICAgICAgT1ZFUlJJREVfQVBQUk9WQUxfREVDSVNJT046ICdPVkVSUklERV9BUFBST1ZBTF9ERUNJU0lPTicsXHJcbiAgICAgICAgT1ZFUlJJREVfVVBMT0FEX0FTU0VUOiAnT1ZFUlJJREVfVVBMT0FEX0FTU0VUJyxcclxuICAgICAgICBERUZBVUxUX1BBR0lOQVRJT046ICdERUZBVUxUX1BBR0lOQVRJT04nLFxyXG4gICAgICAgIERFRkFVTFRfU09SVF9PUkRFUjogJ0RFRkFVTFRfU09SVF9PUkRFUicsXHJcbiAgICAgICAgQUxMT1dfQUNUSU9OX1RBU0s6ICdBTExPV19BQ1RJT05fVEFTSycsXHJcbiAgICAgICAgRU5BQkxFX0NBTVBBSUdOOiAnRU5BQkxFX0NBTVBBSUdOJyxcclxuICAgICAgICBFTkFCTEVfUkVGRVJFTkNFX0FTU0VUOiAnRU5BQkxFX1JFRkVSRU5DRV9BU1NFVCcsXHJcbiAgICAgICAgRU5BQkxFX0NVU1RPTV9XT1JLRkxPVzogJ0VOQUJMRV9DVVNUT01fV09SS0ZMT1cnLFxyXG4gICAgICAgIEVOQUJMRV9QUk9KRUNUX1JFU1RBUlQ6ICdFTkFCTEVfUFJPSkVDVF9SRVNUQVJUJyxcclxuICAgICAgICBFWFBPUlRfREFUQV9GSUxFX1BBVEg6ICdFWFBPUlRfREFUQV9GSUxFX1BBVEgnLFxyXG4gICAgICAgIEVYUE9SVF9EQVRBX1BBR0VfTElNSVQ6ICdFWFBPUlRfREFUQV9QQUdFX0xJTUlUJyxcclxuICAgICAgICBFWFBPUlRfREFUQV9NSU5fQ09VTlQ6ICdFWFBPUlRfREFUQV9NSU5fQ09VTlQnXHJcbiAgICB9LFxyXG4gICAgTVBNX0FTU0VUX0NPTkZJRzoge1xyXG4gICAgICAgIE1BWF9OT19PRl9GSUxFUzogJ01BWF9OT19PRl9GSUxFUycsXHJcbiAgICAgICAgTUFYX0ZJTEVfU0laRTogJ01BWF9GSUxFX1NJWkUnLFxyXG4gICAgICAgIEFMTE9XRURfRklMRV9UWVBFUzogJ0FMTE9XRURfRklMRV9UWVBFUydcclxuICAgIH0sXHJcbiAgICBNUE1fQlJBTkRfQ09ORklHOiB7XHJcbiAgICAgICAgRkFWX0lDT05fUEFUSDogJ0ZBVl9JQ09OX1BBVEgnLFxyXG4gICAgICAgIEFQUF9MT0dPX1BBVEg6ICdBUFBfTE9HT19QQVRIJyxcclxuICAgICAgICBBUFBfVEhFTUVfTkFNRTogJ0FQUF9USEVNRV9OQU1FJyxcclxuICAgICAgICBERUZBVUxUX0JSQU5ESU5HX1NUWUxFX1BBVEg6ICdERUZBVUxUX0JSQU5ESU5HX1NUWUxFX1BBVEgnLFxyXG4gICAgICAgIFNIT1dfQ09QWVJJR0hUOiAnU0hPV19DT1BZUklHSFQnXHJcbiAgICB9LFxyXG4gICAgTVBNX0NPTU1FTlRTX0NPTkZJRzoge1xyXG4gICAgICAgIFNIT1dfRVhURVJOQUxfSU5URVJOQUxfQ09NTUVOVF9TV0lUQ0g6ICdTSE9XX0VYVEVSTkFMX0lOVEVSTkFMX0NPTU1FTlRfU1dJVENIJyxcclxuICAgICAgICBERUZBVUxUX0NPTU1FTlQ6ICdERUZBVUxUX0NPTU1FTlQnLFxyXG4gICAgICAgIEVYVEVSTkFMX0lOVEVSTkFMX0NPTU1FTlRfU1dJVENIX0xBQkVMOiAnRVhURVJOQUxfSU5URVJOQUxfQ09NTUVOVF9TV0lUQ0hfTEFCRUwnLFxyXG4gICAgICAgIENPTU1FTlRTX0lURU1fUEVSX1BBR0U6ICdDT01NRU5UU19JVEVNX1BFUl9QQUdFJyxcclxuICAgICAgICBFWFRFUk5BTF9DT01NRU5UX0xBQkVMOiAnRVhURVJOQUxfQ09NTUVOVF9MQUJFTCcsXHJcbiAgICAgICAgSU5URVJOQUxfQ09NTUVOVF9MQUJFTDogJ0lOVEVSTkFMX0NPTU1FTlRfTEFCRUwnLFxyXG4gICAgICAgIEVOQUJMRV9DT01NRU5UX05PVElGSUNBVElPTjogJ0VOQUJMRV9DT01NRU5UX05PVElGSUNBVElPTidcclxuICAgIH0sXHJcbiAgICBPVE1NX1NZU1RFTV9ERVRBSUxTX0NPTlNUQU5UUzoge1xyXG4gICAgICAgICdCVUlMRF9WRVJTSU9OJzogJ0JVSUxEX1ZFUlNJT04nXHJcbiAgICB9XHJcblxyXG59OyJdfQ==