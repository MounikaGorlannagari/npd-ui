import * as moment_ from 'moment';
var moment = moment_;
var DateValidators = /** @class */ (function () {
    function DateValidators() {
    }
    DateValidators.dateFormat = function (ac) {
        var localeData = moment.localeData(navigator.language);
        var format = localeData.longDateFormat('l');
        if (ac && ac.value && !moment(ac.value, format).isValid()) {
            return { dateFormat: true };
        }
        return null;
    };
    return DateValidators;
}());
export { DateValidators };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS52YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvdmFsaWRhdG9ycy9kYXRlLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQztBQUVsQyxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFFdkI7SUFBQTtJQVlBLENBQUM7SUFWVSx5QkFBVSxHQUFqQixVQUFrQixFQUFtQjtRQUNqQyxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6RCxJQUFNLE1BQU0sR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTlDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUN2RCxPQUFPLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxDQUFDO1NBQy9CO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQVpELElBWUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcclxuXHJcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XHJcblxyXG5leHBvcnQgY2xhc3MgRGF0ZVZhbGlkYXRvcnMge1xyXG5cclxuICAgIHN0YXRpYyBkYXRlRm9ybWF0KGFjOiBBYnN0cmFjdENvbnRyb2wpIHtcclxuICAgICAgICBjb25zdCBsb2NhbGVEYXRhID0gbW9tZW50LmxvY2FsZURhdGEobmF2aWdhdG9yLmxhbmd1YWdlKTtcclxuICAgICAgICBjb25zdCBmb3JtYXQgPSBsb2NhbGVEYXRhLmxvbmdEYXRlRm9ybWF0KCdsJyk7XHJcblxyXG4gICAgICAgIGlmIChhYyAmJiBhYy52YWx1ZSAmJiAhbW9tZW50KGFjLnZhbHVlLCBmb3JtYXQpLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4geyBkYXRlRm9ybWF0OiB0cnVlIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxufVxyXG4iXX0=