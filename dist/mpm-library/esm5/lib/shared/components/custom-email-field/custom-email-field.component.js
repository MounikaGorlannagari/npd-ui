import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ENTER, COMMA, SEMICOLON, SPACE } from '@angular/cdk/keycodes';
import { NotificationService } from '../../../notification/notification.service';
var CustomEmailFieldComponent = /** @class */ (function () {
    function CustomEmailFieldComponent(notification) {
        this.notification = notification;
        this.valueChanges = new EventEmitter();
        this.separatorKeysCodes = [ENTER, COMMA, SEMICOLON, SPACE];
        this.emails = [];
        this.disabled = false;
        this.mandatory = false;
        this.isIdExists = false;
    }
    CustomEmailFieldComponent.prototype.checkIfEmailInString = function (text) {
        var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        return re.test(text);
    };
    CustomEmailFieldComponent.prototype.addEmail = function (event) {
        var input = event.input;
        var value = event.value;
        var index = this.emails.findIndex(function (val) { return val === value.trim(); });
        if (this.checkIfEmailInString(value)) {
            this.isIdExists = false;
            if ((value.trim() !== '')) {
                this.emailFieldFormControl.setErrors(null);
                var tempEmails = this.emailFieldFormControl.value;
                if (index < 0) {
                    tempEmails.push(value.trim());
                }
                this.emailFieldFormControl.setValue(tempEmails);
                if (this.emailFieldFormControl.valid) {
                    this.emailFieldFormControl.markAsDirty();
                    if (index !== -1) {
                        this.isIdExists = true;
                        this.notification.error('Email id alreday exists');
                    }
                    input.value = '';
                }
                else {
                    if (index !== -1) {
                        this.emails.splice(index, 1);
                    }
                }
                this.valueChanges.next(this.emailFieldFormControl);
            }
            else {
                this.emailFieldFormControl.updateValueAndValidity();
                this.valueChanges.next(this.emailFieldFormControl);
            }
        }
        else {
            if (value != '') {
                this.isIdExists = true;
                this.notification.error('Provided email id is not valid,enter valid email id');
            }
        }
    };
    CustomEmailFieldComponent.prototype.onRemoveEmail = function (email) {
        var index = this.emails.indexOf(email, 0);
        if (index > -1) {
            this.emails.splice(index, 1);
        }
        this.emailFieldFormControl.updateValueAndValidity();
        this.emailFieldFormControl.markAsDirty();
        this.valueChanges.next(this.emailFieldFormControl);
    };
    CustomEmailFieldComponent.prototype.ngOnInit = function () {
        this.emails = this.emailFieldFormControl.value;
        this.emailFieldFormControl.patchValue(this.emails);
        /* if (this.label == 'To' || this.label == 'From') {
            this.required = true;
        } */ /*  else {
            this.asterik = false;
        } */
        if (this.emailFieldFormControl && this.emailFieldFormControl.status === 'DISABLED') {
            this.disabled = true;
        }
        if (this.emailFieldFormControl && ((this.emailFieldFormControl.errors && this.emailFieldFormControl.errors.required) || (this.emailFieldFormControl.pristine && this.emailFieldFormControl.status === 'DISABLED'))) {
            this.mandatory = true;
        }
    };
    CustomEmailFieldComponent.ctorParameters = function () { return [
        { type: NotificationService }
    ]; };
    __decorate([
        Input()
    ], CustomEmailFieldComponent.prototype, "emailFieldFormControl", void 0);
    __decorate([
        Input()
    ], CustomEmailFieldComponent.prototype, "label", void 0);
    __decorate([
        Output()
    ], CustomEmailFieldComponent.prototype, "valueChanges", void 0);
    __decorate([
        Input()
    ], CustomEmailFieldComponent.prototype, "required", void 0);
    CustomEmailFieldComponent = __decorate([
        Component({
            selector: 'mpm-custom-email-field',
            template: "<mat-form-field appearance=\"outline\" class=\"full-width\" [ngClass]=\"{'comment': isIdExists}\">\r\n    <mat-label *ngIf=\"mandatory\">{{label}} *</mat-label>\r\n    <mat-label *ngIf=\"!mandatory\">{{label}}</mat-label>\r\n    <mat-chip-list #emailList>\r\n        <mat-chip class=\"email-chip\" *ngFor=\"let email of emails\" [disabled]=\"disabled\" removable\r\n            (removed)=\"onRemoveEmail(email)\">\r\n            <span class=\"email-value\" matTooltip=\"{{email}}\">\r\n                {{email}}\r\n            </span>\r\n            <mat-icon matChipRemove>cancel</mat-icon>\r\n        </mat-chip>\r\n        <input [disabled]=\"disabled\" class=\"email-chip-input\" [matChipInputAddOnBlur]=\"true\"\r\n            [matChipInputFor]=\"emailList\" [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n            (matChipInputTokenEnd)=\"addEmail($event)\" [required]=\"mandatory\">\r\n    </mat-chip-list>\r\n</mat-form-field>",
            styles: [".full-width{width:100%}.email-chip{padding:8px 4px 8px 8px!important;min-height:24px;font-size:12px;overflow:hidden}.email-chip .email-value{text-overflow:ellipsis;overflow:hidden;max-width:290px;white-space:nowrap}.email-chip-input{min-height:24px}.comment ::ng-deep .mat-form-field-outline{color:red!important}"]
        })
    ], CustomEmailFieldComponent);
    return CustomEmailFieldComponent;
}());
export { CustomEmailFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWVtYWlsLWZpZWxkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1lbWFpbC1maWVsZC9jdXN0b20tZW1haWwtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUd2RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQVNqRjtJQWFJLG1DQUNXLFlBQWlDO1FBQWpDLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQVZsQyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFHakQsdUJBQWtCLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0RCxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ1osYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLGVBQVUsR0FBWSxLQUFLLENBQUM7SUFJeEIsQ0FBQztJQUVMLHdEQUFvQixHQUFwQixVQUFxQixJQUFJO1FBQ3JCLElBQUksRUFBRSxHQUFHLHdKQUF3SixDQUFDO1FBQ2xLLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsNENBQVEsR0FBUixVQUFTLEtBQXdCO1FBQzdCLElBQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsS0FBSyxLQUFLLENBQUMsSUFBSSxFQUFFLEVBQXBCLENBQW9CLENBQUMsQ0FBQztRQUVqRSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNsQyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFO2dCQUN2QixJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzQyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDO2dCQUNwRCxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7b0JBQ1gsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDakM7Z0JBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFO29CQUNsQyxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3pDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO3dCQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO3FCQUN0RDtvQkFDRCxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztpQkFDcEI7cUJBQU07b0JBQ0gsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUNoQztpQkFDSjtnQkFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQzthQUN0RDtpQkFBTTtnQkFDSCxJQUFJLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7YUFDdEQ7U0FDSjthQUFNO1lBQ0gsSUFBSSxLQUFLLElBQUksRUFBRSxFQUFFO2dCQUNiLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO2FBQ2xGO1NBQ0o7SUFDTCxDQUFDO0lBRUQsaURBQWEsR0FBYixVQUFjLEtBQVU7UUFDcEIsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzVDLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ1osSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDcEQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXpDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCw0Q0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRW5EOztZQUVJLENBQUE7O1lBRUE7UUFDSixJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtZQUNoRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUN4QjtRQUNELElBQUksSUFBSSxDQUFDLHFCQUFxQixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEtBQUssVUFBVSxDQUFDLENBQUMsRUFBRTtZQUNoTixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUN6QjtJQUNMLENBQUM7O2dCQTNFd0IsbUJBQW1COztJQVpuQztRQUFSLEtBQUssRUFBRTs0RUFBd0M7SUFDdkM7UUFBUixLQUFLLEVBQUU7NERBQWU7SUFDYjtRQUFULE1BQU0sRUFBRTttRUFBd0M7SUFDeEM7UUFBUixLQUFLLEVBQUU7K0RBQW1CO0lBTGxCLHlCQUF5QjtRQU5yQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLGc4QkFBa0Q7O1NBRXJELENBQUM7T0FFVyx5QkFBeUIsQ0EyRnJDO0lBQUQsZ0NBQUM7Q0FBQSxBQTNGRCxJQTJGQztTQTNGWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEVOVEVSLCBDT01NQSwgU0VNSUNPTE9OLCBTUEFDRSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnO1xyXG5pbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJZlN0bXQgfSBmcm9tICdAYW5ndWxhci9jb21waWxlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXBtLWN1c3RvbS1lbWFpbC1maWVsZCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY3VzdG9tLWVtYWlsLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2N1c3RvbS1lbWFpbC1maWVsZC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ3VzdG9tRW1haWxGaWVsZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgZW1haWxGaWVsZEZvcm1Db250cm9sOiBBYnN0cmFjdENvbnRyb2w7XHJcbiAgICBASW5wdXQoKSBsYWJlbDogc3RyaW5nO1xyXG4gICAgQE91dHB1dCgpIHZhbHVlQ2hhbmdlcyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG4gICAgQElucHV0KCkgcmVxdWlyZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgc2VwYXJhdG9yS2V5c0NvZGVzID0gW0VOVEVSLCBDT01NQSwgU0VNSUNPTE9OLCBTUEFDRV07XHJcbiAgICBlbWFpbHMgPSBbXTtcclxuICAgIGRpc2FibGVkID0gZmFsc2U7XHJcbiAgICBtYW5kYXRvcnkgPSBmYWxzZTtcclxuICAgIGlzSWRFeGlzdHM6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGNoZWNrSWZFbWFpbEluU3RyaW5nKHRleHQpIHtcclxuICAgICAgICB2YXIgcmUgPSAvKChbXjw+KClbXFxdXFxcXC4sOzpcXHNAXFxcIl0rKFxcLltePD4oKVtcXF1cXFxcLiw7Olxcc0BcXFwiXSspKil8KFwiLitcXFwiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkvO1xyXG4gICAgICAgIHJldHVybiByZS50ZXN0KHRleHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZEVtYWlsKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCkge1xyXG4gICAgICAgIGNvbnN0IGlucHV0ID0gZXZlbnQuaW5wdXQ7XHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSBldmVudC52YWx1ZTtcclxuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuZW1haWxzLmZpbmRJbmRleCh2YWwgPT4gdmFsID09PSB2YWx1ZS50cmltKCkpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jaGVja0lmRW1haWxJblN0cmluZyh2YWx1ZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5pc0lkRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICgodmFsdWUudHJpbSgpICE9PSAnJykpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnNldEVycm9ycyhudWxsKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRlbXBFbWFpbHMgPSB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC52YWx1ZTtcclxuICAgICAgICAgICAgICAgIGlmIChpbmRleCA8IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0ZW1wRW1haWxzLnB1c2godmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnNldFZhbHVlKHRlbXBFbWFpbHMpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLnZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wubWFya0FzRGlydHkoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNJZEV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uLmVycm9yKCdFbWFpbCBpZCBhbHJlZGF5IGV4aXN0cycpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW1haWxzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMudmFsdWVDaGFuZ2VzLm5leHQodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy52YWx1ZUNoYW5nZXMubmV4dCh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodmFsdWUgIT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNJZEV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbi5lcnJvcignUHJvdmlkZWQgZW1haWwgaWQgaXMgbm90IHZhbGlkLGVudGVyIHZhbGlkIGVtYWlsIGlkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25SZW1vdmVFbWFpbChlbWFpbDogYW55KSB7XHJcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmVtYWlscy5pbmRleE9mKGVtYWlsLCAwKTtcclxuICAgICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLmVtYWlscy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wudXBkYXRlVmFsdWVBbmRWYWxpZGl0eSgpO1xyXG4gICAgICAgIHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLm1hcmtBc0RpcnR5KCk7XHJcblxyXG4gICAgICAgIHRoaXMudmFsdWVDaGFuZ2VzLm5leHQodGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZW1haWxzID0gdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wudmFsdWU7XHJcbiAgICAgICAgdGhpcy5lbWFpbEZpZWxkRm9ybUNvbnRyb2wucGF0Y2hWYWx1ZSh0aGlzLmVtYWlscyk7XHJcblxyXG4gICAgICAgIC8qIGlmICh0aGlzLmxhYmVsID09ICdUbycgfHwgdGhpcy5sYWJlbCA9PSAnRnJvbScpIHtcclxuICAgICAgICAgICAgdGhpcy5yZXF1aXJlZCA9IHRydWU7XHJcbiAgICAgICAgfSAqLy8qICBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5hc3RlcmlrID0gZmFsc2U7XHJcbiAgICAgICAgfSAqL1xyXG4gICAgICAgIGlmICh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbCAmJiB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5zdGF0dXMgPT09ICdESVNBQkxFRCcpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbCAmJiAoKHRoaXMuZW1haWxGaWVsZEZvcm1Db250cm9sLmVycm9ycyAmJiB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5lcnJvcnMucmVxdWlyZWQpIHx8ICh0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5wcmlzdGluZSAmJiB0aGlzLmVtYWlsRmllbGRGb3JtQ29udHJvbC5zdGF0dXMgPT09ICdESVNBQkxFRCcpKSkge1xyXG4gICAgICAgICAgICB0aGlzLm1hbmRhdG9yeSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=