import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomEmailFieldComponent } from './custom-email-field.component';
import { MaterialModule } from '../../../material.module';
var CustomEmailFieldModule = /** @class */ (function () {
    function CustomEmailFieldModule() {
    }
    CustomEmailFieldModule = __decorate([
        NgModule({
            declarations: [
                CustomEmailFieldComponent
            ],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [
                CustomEmailFieldComponent
            ]
        })
    ], CustomEmailFieldModule);
    return CustomEmailFieldModule;
}());
export { CustomEmailFieldModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWVtYWlsLWZpZWxkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbS1lbWFpbC1maWVsZC9jdXN0b20tZW1haWwtZmllbGQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFlMUQ7SUFBQTtJQUFzQyxDQUFDO0lBQTFCLHNCQUFzQjtRQWJsQyxRQUFRLENBQUM7WUFDTixZQUFZLEVBQUU7Z0JBQ1YseUJBQXlCO2FBQzVCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osY0FBYzthQUNqQjtZQUNELE9BQU8sRUFBRTtnQkFDTCx5QkFBeUI7YUFDNUI7U0FDSixDQUFDO09BRVcsc0JBQXNCLENBQUk7SUFBRCw2QkFBQztDQUFBLEFBQXZDLElBQXVDO1NBQTFCLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEN1c3RvbUVtYWlsRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2N1c3RvbS1lbWFpbC1maWVsZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQ3VzdG9tRW1haWxGaWVsZENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQ3VzdG9tRW1haWxGaWVsZENvbXBvbmVudFxyXG4gICAgXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbUVtYWlsRmllbGRNb2R1bGUgeyB9XHJcbiJdfQ==