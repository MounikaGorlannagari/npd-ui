import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
var ExportDataTrayModalComponent = /** @class */ (function () {
    function ExportDataTrayModalComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'Export Data Tray';
    }
    ExportDataTrayModalComponent.prototype.cancelDialog = function () {
        this.dialogRef.close();
    };
    ExportDataTrayModalComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    ExportDataTrayModalComponent = __decorate([
        Component({
            selector: 'mpm-export-data-tray-modal',
            template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-export-data-tray></mpm-export-data-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
            styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], ExportDataTrayModalComponent);
    return ExportDataTrayModalComponent;
}());
export { ExportDataTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEtdHJheS1tb2RhbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9leHBvcnQtZGF0YS10cmF5LW1vZGFsL2V4cG9ydC1kYXRhLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBUXpFO0lBSUUsc0NBQ1MsU0FBZ0QsRUFDdkIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUF1QztRQUN2QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxrQkFBa0IsQ0FBQztJQUs1QixDQUFDO0lBRUwsbURBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekIsQ0FBQzs7Z0JBTm1CLFlBQVk7Z0RBQzdCLE1BQU0sU0FBQyxlQUFlOztJQU5kLDRCQUE0QjtRQUx4QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsNEJBQTRCO1lBQ3RDLHNmQUFzRDs7U0FFdkQsQ0FBQztRQU9HLFdBQUEsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO09BTmYsNEJBQTRCLENBYXhDO0lBQUQsbUNBQUM7Q0FBQSxBQWJELElBYUM7U0FiWSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgRXhwb3J0RGF0YVRyYXlDb21wb25lbnQgfSBmcm9tICcuLi9leHBvcnQtZGF0YS10cmF5L2V4cG9ydC1kYXRhLXRyYXkuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWV4cG9ydC1kYXRhLXRyYXktbW9kYWwnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9leHBvcnQtZGF0YS10cmF5LW1vZGFsLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9leHBvcnQtZGF0YS10cmF5LW1vZGFsLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEV4cG9ydERhdGFUcmF5TW9kYWxDb21wb25lbnQgICB7XHJcblxyXG4gIG1vZGFsVGl0bGUgPSAnRXhwb3J0IERhdGEgVHJheSc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEV4cG9ydERhdGFUcmF5Q29tcG9uZW50PixcclxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YVxyXG4gICkgeyB9XHJcblxyXG4gIGNhbmNlbERpYWxvZygpIHtcclxuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=