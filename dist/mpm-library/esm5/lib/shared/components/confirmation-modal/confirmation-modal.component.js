import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../../notification/notification.service';
var ConfirmationModalComponent = /** @class */ (function () {
    function ConfirmationModalComponent(dialogRef, data, notificationService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.notificationService = notificationService;
        this.confirmationComment = '';
        this.confirmationHeading = 'Confirmation';
    }
    ConfirmationModalComponent.prototype.closeDialog = function () {
        var result = {
            isTrue: false
        };
        this.dialogRef.close();
    };
    ConfirmationModalComponent.prototype.ngOnInit = function () {
        this.message = this.data.message;
        this.taskName = this.data.name;
        if (this.data.confirmationHeading) {
            this.confirmationHeading = this.data.confirmationHeading;
        }
    };
    ConfirmationModalComponent.prototype.onYesClick = function () {
        var result = {
            isTrue: true,
            confirmationComment: ''
        };
        if (this.data.hasConfirmationComment) {
            if (this.data.isCommentRequired && this.confirmationComment.trim().length === 0) {
                this.notificationService.error(this.data.errorMessage);
                return;
            }
            result.confirmationComment = this.confirmationComment;
        }
        this.dialogRef.close(result);
    };
    ConfirmationModalComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: NotificationService }
    ]; };
    ConfirmationModalComponent = __decorate([
        Component({
            selector: 'mpm-confirmation-modal',
            template: "<h1 mat-dialog-title class=\"confirmation-modal-title\">{{confirmationHeading}}</h1>\r\n<mat-dialog-content>\r\n    <div *ngIf=\"taskName && taskName.length > 0\" class=\"task-info\">\r\n        <!-- class=\"task-info\" -->\r\n        <fieldset>\r\n            <legend>The following task will also be deleted,along with deliverable:</legend>\r\n            <mat-list *ngFor=\"let task of taskName\" style=\"padding-bottom:4px;\">\r\n                <mat-list-item>\r\n                    <mat-icon class=\"brand\">keyboard_arrow_right</mat-icon> <span matTooltip=\"{{task}}\">{{task}}</span>\r\n                </mat-list-item>\r\n            </mat-list>\r\n        </fieldset>\r\n    </div>\r\n    <br />\r\n    <span *ngIf=\"message\">\r\n        <p [innerHTML]=\"message\"></p>\r\n    </span>\r\n    <mat-form-field *ngIf=\"data.hasConfirmationComment\" class=\"comment-field\" appearance=\"outline\"\r\n        style=\"width: 100%!important;\">\r\n        <mat-label>{{data.commentText}}</mat-label>\r\n        <textarea required=\"{{data.isCommentRequired}}\" matInput id=\"confirmationComment\" name=\"\" cols=\"\" rows=\"\"\r\n            [(ngModel)]=\"confirmationComment\"></textarea>\r\n    </mat-form-field>\r\n</mat-dialog-content>\r\n<div align=\"end\">\r\n    <button mat-stroked-button (click)=\"closeDialog()\">{{data.cancelButton}}</button>\r\n    <button color=\"primary\" mat-flat-button (click)=\"onYesClick()\" cdkFocusInitial>{{data.submitButton}}</button>\r\n</div>",
            styles: ["confirmation-modal-title{font-weight:700}.comment-field{margin-top:20px}button{margin:10px}.task-info{max-height:300px;overflow:auto;padding:3px}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], ConfirmationModalComponent);
    return ConfirmationModalComponent;
}());
export { ConfirmationModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2NvbmZpcm1hdGlvbi1tb2RhbC9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBUWpGO0lBTUksb0NBQ1csU0FBbUQsRUFDMUIsSUFBUyxFQUNsQyxtQkFBd0M7UUFGeEMsY0FBUyxHQUFULFNBQVMsQ0FBMEM7UUFDMUIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNsQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBTm5ELHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQUN6Qix3QkFBbUIsR0FBRyxjQUFjLENBQUM7SUFNakMsQ0FBQztJQUVMLGdEQUFXLEdBQVg7UUFDSSxJQUFNLE1BQU0sR0FBRztZQUNYLE1BQU0sRUFBRSxLQUFLO1NBQ2hCLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRCw2Q0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQy9CLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUMvQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUM1RDtJQUNMLENBQUM7SUFFRCwrQ0FBVSxHQUFWO1FBQ0ksSUFBTSxNQUFNLEdBQUc7WUFDWCxNQUFNLEVBQUUsSUFBSTtZQUNaLG1CQUFtQixFQUFFLEVBQUU7U0FDMUIsQ0FBQztRQUNGLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzdFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDdkQsT0FBTzthQUNWO1lBQ0QsTUFBTSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztTQUN6RDtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7O2dCQWpDcUIsWUFBWTtnREFDN0IsTUFBTSxTQUFDLGVBQWU7Z0JBQ0ssbUJBQW1COztJQVQxQywwQkFBMEI7UUFOdEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHdCQUF3QjtZQUNsQyw0OUNBQWtEOztTQUVyRCxDQUFDO1FBVU8sV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7T0FSbkIsMEJBQTBCLENBMEN0QztJQUFELGlDQUFDO0NBQUEsQUExQ0QsSUEwQ0M7U0ExQ1ksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1jb25maXJtYXRpb24tbW9kYWwnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jb25maXJtYXRpb24tbW9kYWwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBtZXNzYWdlO1xyXG4gICAgY29uZmlybWF0aW9uQ29tbWVudCA9ICcnO1xyXG4gICAgY29uZmlybWF0aW9uSGVhZGluZyA9ICdDb25maXJtYXRpb24nO1xyXG4gICAgdGFza05hbWU7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8Q29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQ+LFxyXG4gICAgICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55LFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgKSB7IH1cclxuXHJcbiAgICBjbG9zZURpYWxvZygpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSB7XHJcbiAgICAgICAgICAgIGlzVHJ1ZTogZmFsc2VcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlID0gdGhpcy5kYXRhLm1lc3NhZ2U7XHJcbiAgICAgICAgdGhpcy50YXNrTmFtZSA9IHRoaXMuZGF0YS5uYW1lO1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEuY29uZmlybWF0aW9uSGVhZGluZykge1xyXG4gICAgICAgICAgICB0aGlzLmNvbmZpcm1hdGlvbkhlYWRpbmcgPSB0aGlzLmRhdGEuY29uZmlybWF0aW9uSGVhZGluZztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25ZZXNDbGljaygpIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSB7XHJcbiAgICAgICAgICAgIGlzVHJ1ZTogdHJ1ZSxcclxuICAgICAgICAgICAgY29uZmlybWF0aW9uQ29tbWVudDogJydcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEuaGFzQ29uZmlybWF0aW9uQ29tbWVudCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5kYXRhLmlzQ29tbWVudFJlcXVpcmVkICYmIHRoaXMuY29uZmlybWF0aW9uQ29tbWVudC50cmltKCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IodGhpcy5kYXRhLmVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmVzdWx0LmNvbmZpcm1hdGlvbkNvbW1lbnQgPSB0aGlzLmNvbmZpcm1hdGlvbkNvbW1lbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHJlc3VsdCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==