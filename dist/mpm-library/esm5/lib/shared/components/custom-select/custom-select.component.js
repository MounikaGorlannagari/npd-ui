import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var CustomSelectComponent = /** @class */ (function () {
    function CustomSelectComponent() {
        this.selectionChange = new EventEmitter();
    }
    CustomSelectComponent.prototype.onSelectionChange = function (currentOption) {
        var _this = this;
        this.options.map(function (option) {
            if (option.value === currentOption.value) {
                _this.selectedOption = option;
            }
        });
        this.selectionChange.next(this.selectedOption);
    };
    CustomSelectComponent.prototype.handleDefaultSelection = function () {
        var _this = this;
        if (!this.options || this.options.length <= 0) {
            this.options = [];
            this.selectedOption = null;
        }
        else {
            if (this.selected) {
                this.selectedOption = this.options.find(function (option) { return option.value === _this.selected.value; });
            }
            else {
                this.selectedOption = this.options.find(function (option) { return option.default === true; });
            }
        }
    };
    CustomSelectComponent.prototype.initalize = function () {
        this.handleDefaultSelection();
    };
    CustomSelectComponent.prototype.ngOnChanges = function (changes) {
        if (changes && changes.selected && !changes.selected.firstChange && JSON.stringify(changes.selected.previousValue) !== JSON.stringify(changes.selected.currentValue)
            && changes.selected.currentValue.value !== this.selectedOption.value) {
            this.handleDefaultSelection();
        }
    };
    CustomSelectComponent.prototype.ngOnInit = function () {
        this.initalize();
    };
    __decorate([
        Input()
    ], CustomSelectComponent.prototype, "selected", void 0);
    __decorate([
        Input()
    ], CustomSelectComponent.prototype, "options", void 0);
    __decorate([
        Input()
    ], CustomSelectComponent.prototype, "controlLabel", void 0);
    __decorate([
        Output()
    ], CustomSelectComponent.prototype, "selectionChange", void 0);
    __decorate([
        Input()
    ], CustomSelectComponent.prototype, "bulkEdit", void 0);
    __decorate([
        Input()
    ], CustomSelectComponent.prototype, "deliverableBulkEdit", void 0);
    CustomSelectComponent = __decorate([
        Component({
            selector: 'mpm-custom-select',
            template: "<button class=\"mpm-custom-dropdown-btn\" mat-stroked-button [disabled]=\"!bulkEdit\"*ngIf=\"selectedOption && options?.length>0\"\r\n    [matTooltip]=\"controlLabel ? (controlLabel + ' ' +selectedOption.name) : selectedOption.name\"\r\n    [matMenuTriggerFor]=\"customSelectMenu\">\r\n    <span>{{controlLabel ? (controlLabel + ' ' +selectedOption.name) : selectedOption.name}}</span>\r\n    <mat-icon>arrow_drop_down</mat-icon>\r\n</button>\r\n<mat-menu #customSelectMenu=\"matMenu\">\r\n    <span *ngFor=\"let option of options\">\r\n        <button *ngIf=\"option.value != selectedOption?.value\" mat-menu-item [matTooltip]=\"option.name\"\r\n            (click)=\"onSelectionChange(option)\" [value]=\"option.value\">\r\n            <span>{{option.name}}</span>\r\n        </button>\r\n    </span>\r\n</mat-menu>",
            styles: [""]
        })
    ], CustomSelectComponent);
    return CustomSelectComponent;
}());
export { CustomSelectComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tc2VsZWN0L2N1c3RvbS1zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUE0QixNQUFNLGVBQWUsQ0FBQztBQVF6RztJQVVFO1FBSlUsb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQztJQUk3QyxDQUFDO0lBSWpCLGlEQUFpQixHQUFqQixVQUFrQixhQUFhO1FBQS9CLGlCQU9DO1FBTkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO1lBQ3JCLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxhQUFhLENBQUMsS0FBSyxFQUFFO2dCQUN4QyxLQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQzthQUM5QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxzREFBc0IsR0FBdEI7UUFBQSxpQkFXQztRQVZDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUM3QyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM1QjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNqQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLEtBQUssS0FBSyxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBcEMsQ0FBb0MsQ0FBQyxDQUFDO2FBQ3pGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsT0FBTyxLQUFLLElBQUksRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO2FBQzVFO1NBQ0Y7SUFDSCxDQUFDO0lBRUQseUNBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCwyQ0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDaEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO2VBQy9KLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRTtZQUN0RSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUEvQ1E7UUFBUixLQUFLLEVBQUU7MkRBQXdCO0lBRXZCO1FBQVIsS0FBSyxFQUFFOzBEQUE4QjtJQUM3QjtRQUFSLEtBQUssRUFBRTsrREFBc0I7SUFDcEI7UUFBVCxNQUFNLEVBQUU7a0VBQW9EO0lBRXBEO1FBQVIsS0FBSyxFQUFFOzJEQUFVO0lBQ1Q7UUFBUixLQUFLLEVBQUU7c0VBQXFCO0lBVGxCLHFCQUFxQjtRQUxqQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLDZ6QkFBNkM7O1NBRTlDLENBQUM7T0FDVyxxQkFBcUIsQ0FtRGpDO0lBQUQsNEJBQUM7Q0FBQSxBQW5ERCxJQW1EQztTQW5EWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ3VzdG9tU2VsZWN0IH0gZnJvbSAnLi9vYmplY3RzL0N1c3RvbVNlbGVjdE9iamVjdCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1jdXN0b20tc2VsZWN0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY3VzdG9tLXNlbGVjdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDdXN0b21TZWxlY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcblxyXG4gIEBJbnB1dCgpIHNlbGVjdGVkOiBDdXN0b21TZWxlY3Q7XHJcblxyXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEFycmF5PEN1c3RvbVNlbGVjdD47XHJcbiAgQElucHV0KCkgY29udHJvbExhYmVsOiBzdHJpbmc7XHJcbiAgQE91dHB1dCgpIHNlbGVjdGlvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8Q3VzdG9tU2VsZWN0PigpO1xyXG5cclxuICBASW5wdXQoKSBidWxrRWRpdDtcclxuICBASW5wdXQoKSBkZWxpdmVyYWJsZUJ1bGtFZGl0O1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHNlbGVjdGVkT3B0aW9uOiBDdXN0b21TZWxlY3Q7XHJcblxyXG4gIG9uU2VsZWN0aW9uQ2hhbmdlKGN1cnJlbnRPcHRpb24pIHtcclxuICAgIHRoaXMub3B0aW9ucy5tYXAob3B0aW9uID0+IHtcclxuICAgICAgaWYgKG9wdGlvbi52YWx1ZSA9PT0gY3VycmVudE9wdGlvbi52YWx1ZSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSBvcHRpb247XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UubmV4dCh0aGlzLnNlbGVjdGVkT3B0aW9uKTtcclxuICB9XHJcblxyXG4gIGhhbmRsZURlZmF1bHRTZWxlY3Rpb24oKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMub3B0aW9ucyB8fCB0aGlzLm9wdGlvbnMubGVuZ3RoIDw9IDApIHtcclxuICAgICAgdGhpcy5vcHRpb25zID0gW107XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRPcHRpb24gPSBudWxsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWQpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uID0gdGhpcy5vcHRpb25zLmZpbmQob3B0aW9uID0+IG9wdGlvbi52YWx1ZSA9PT0gdGhpcy5zZWxlY3RlZC52YWx1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9wdGlvbiA9IHRoaXMub3B0aW9ucy5maW5kKG9wdGlvbiA9PiBvcHRpb24uZGVmYXVsdCA9PT0gdHJ1ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGluaXRhbGl6ZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuaGFuZGxlRGVmYXVsdFNlbGVjdGlvbigpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5zZWxlY3RlZCAmJiAhY2hhbmdlcy5zZWxlY3RlZC5maXJzdENoYW5nZSAmJiBKU09OLnN0cmluZ2lmeShjaGFuZ2VzLnNlbGVjdGVkLnByZXZpb3VzVmFsdWUpICE9PSBKU09OLnN0cmluZ2lmeShjaGFuZ2VzLnNlbGVjdGVkLmN1cnJlbnRWYWx1ZSlcclxuICAgICAgJiYgY2hhbmdlcy5zZWxlY3RlZC5jdXJyZW50VmFsdWUudmFsdWUgIT09IHRoaXMuc2VsZWN0ZWRPcHRpb24udmFsdWUpIHtcclxuICAgICAgdGhpcy5oYW5kbGVEZWZhdWx0U2VsZWN0aW9uKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuaW5pdGFsaXplKCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=