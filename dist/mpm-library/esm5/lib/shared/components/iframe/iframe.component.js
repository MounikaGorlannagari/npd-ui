import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
var IframeComponent = /** @class */ (function () {
    function IframeComponent(sanitizer, location) {
        this.sanitizer = sanitizer;
        this.location = location;
    }
    IframeComponent.prototype.close = function () {
        this.location.back();
    };
    IframeComponent.prototype.ngOnInit = function () {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.iframeObj.url);
    };
    IframeComponent.ctorParameters = function () { return [
        { type: DomSanitizer },
        { type: Location }
    ]; };
    __decorate([
        Input()
    ], IframeComponent.prototype, "iframeObj", void 0);
    IframeComponent = __decorate([
        Component({
            selector: 'mpm-iframe',
            template: "<div class=\"app-container\">\r\n    <div class=\"app-nav-bar\" *ngIf=\"iframeObj.headerInfos\">\r\n        <mat-toolbar class=\"app-toolbar mat-elevation-z4\" color=\"primary\">\r\n            <mat-toolbar-row>\r\n                <span *ngFor=\"let headerField of iframeObj.headerInfos\" class=\"header-info\">\r\n                    <mat-label>\r\n                        <span class=\"headerTitle\">{{headerField.title}}: </span>\r\n                        <span class=\"headerValue\">{{headerField.value}}</span>\r\n                    </mat-label>\r\n                </span>\r\n\r\n                <span class=\"spacer\"></span>\r\n\r\n                <button mat-button color=\"accent\" matTooltip=\"Close\" (click)=\"close()\">Close</button>\r\n            </mat-toolbar-row>\r\n        </mat-toolbar>\r\n    </div>\r\n\r\n    <div class=\"app-content\">\r\n        <iframe *ngIf=\"url\" id=\"iFrame\" [src]=\"url\"></iframe>\r\n    </div>\r\n</div>",
            styles: [".spacer{flex:1 1 auto}.app-container{display:flex;flex-direction:column;position:absolute;height:100%;width:100%;top:0;bottom:0;left:0;right:0;background-color:#000}.app-container .app-nav-bar{position:absolute;z-index:100;width:100%}.app-container .app-nav-bar .app-is-mobile .app-toolbar{position:fixed;z-index:2}.app-container .app-nav-bar .app-toolbar .header-info{font-size:14px;margin-right:20px}.app-container .app-content{margin-top:65px;height:calc(100vh - 15px);z-index:100;background-color:#000}.app-container .app-content #iFrame{height:calc(100vh - 90px);width:100vw}"]
        })
    ], IframeComponent);
    return IframeComponent;
}());
export { IframeComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWZyYW1lLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL2lmcmFtZS9pZnJhbWUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDekQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBUTNDO0lBTUkseUJBQ1csU0FBdUIsRUFDdkIsUUFBa0I7UUFEbEIsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixhQUFRLEdBQVIsUUFBUSxDQUFVO0lBQ3pCLENBQUM7SUFFTCwrQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQsa0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7O2dCQVZxQixZQUFZO2dCQUNiLFFBQVE7O0lBTnBCO1FBQVIsS0FBSyxFQUFFO3NEQUFXO0lBRlYsZUFBZTtRQU4zQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0Qix3OEJBQXNDOztTQUV6QyxDQUFDO09BRVcsZUFBZSxDQW1CM0I7SUFBRCxzQkFBQztDQUFBLEFBbkJELElBbUJDO1NBbkJZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0taWZyYW1lJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pZnJhbWUuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vaWZyYW1lLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBJZnJhbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGlmcmFtZU9iajtcclxuXHJcbiAgICB1cmw7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNhbml0aXplcjogRG9tU2FuaXRpemVyLFxyXG4gICAgICAgIHB1YmxpYyBsb2NhdGlvbjogTG9jYXRpb25cclxuICAgICkgeyB9XHJcblxyXG4gICAgY2xvc2UoKSB7XHJcbiAgICAgICAgdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51cmwgPSB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0UmVzb3VyY2VVcmwodGhpcy5pZnJhbWVPYmoudXJsKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19