import { __decorate, __param } from "tslib";
import { Component, Inject } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { RoleConstants } from ".././../../../lib/project/shared/constants/role.constants";
import { Observable } from "rxjs";
import { LoaderService } from "../../../loader/loader.service";
import { AppService } from "../../../mpm-utils/services/app.service";
import { SharingService } from "../../../mpm-utils/services/sharing.service";
import { NotificationService } from "../../../notification/notification.service";
var EmailNotificationPreferenceComponent = /** @class */ (function () {
    function EmailNotificationPreferenceComponent(dialogRef, data, sharingService, appService, formBuilder, loaderService, notification) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.sharingService = sharingService;
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.loaderService = loaderService;
        this.notification = notification;
        this.isPM = false;
        this.isUser = false;
        this.allEmailEvents = [];
        this.addedEvents = [];
        this.modifiedEvents = [];
        this.modifiedEventsFlag = 0;
        this.enabledEvents = [];
        this.disabledEvents = [];
        this.displayEmailEvents = [];
        this.userPreferenceFlag = 0;
        this.userPreferenceEmailEvent = [];
        this.hasUserPreference = false;
        this.commonEvents = [];
        this.notificationPreferenceFlag = 0;
        this.showNotification = false;
        this.hasUserEmailPreference = false;
        this.selectAll = "Select All";
        this.restore = false;
    }
    EmailNotificationPreferenceComponent.prototype.setSelectAll = function (event) {
        var _this = this;
        this.displayEmailEvents.forEach(function (emailLists) {
            if (!(event) == Boolean(JSON.parse(emailLists.isChecked))) {
                _this.masterToggle(event, emailLists.Name, emailLists.Id, emailLists);
            }
            emailLists.isChecked = event;
        });
    };
    EmailNotificationPreferenceComponent.prototype.setAllCheck = function (emailEventLists) {
        this.allCheck = emailEventLists.every(function (emailevent) { return emailevent.isChecked != false; });
    };
    EmailNotificationPreferenceComponent.prototype.closeDialog = function () {
        this.dialogRef.close(false);
    };
    EmailNotificationPreferenceComponent.prototype.restoreDefault = function () {
        var _this = this;
        this.loaderService.show();
        var userPreferenceID = {
            Id: this.userEmailPreferenceId
        };
        this.appService.DeleteMPM_Email_Preference(userPreferenceID).subscribe(function (response) {
            _this.hasUserEmailPreference = false;
            _this.emailPreferences(_this.isPM, _this.isUser);
        });
    };
    EmailNotificationPreferenceComponent.prototype.masterToggle = function (event, eventName, id, emailEvent) {
        var _this = this;
        emailEvent.isChecked = event;
        this.setAllCheck(this.displayEmailEvents);
        this.intermediateCheck = {
            Name: eventName.replace(/_/g, ' '),
            Id: id,
            isChecked: event
        };
        if (Array.isArray(this.modifiedEvents) && this.modifiedEvents.length > 0) {
            this.modifiedEventsFlag = 0;
            this.modifiedEvents.forEach(function (element, index) {
                if (element.Name === eventName.replace(/_/g, ' ')) {
                    _this.modifiedEvents.splice(index, 1);
                    _this.modifiedEventsFlag = 1;
                }
            });
            if (this.modifiedEventsFlag !== 1) {
                this.modifiedEvents.push(this.intermediateCheck);
            }
        }
        else {
            this.modifiedEvents = [];
            this.modifiedEvents.push(this.intermediateCheck);
        }
    };
    EmailNotificationPreferenceComponent.prototype.notificationPreference = function () {
        var _this = this;
        this.addedEvents = [];
        this.enabledEvents = [];
        this.disabledEvents = [];
        this.showNotification = false;
        if (this.hasUserPreference) {
            this.showNotification = false;
            if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                this.modifiedEvents.forEach(function (element, index) {
                    _this.notificationPreferenceFlag = 0;
                    if (_this.userPreferenceEmailEvent.length > 0) {
                        _this.userPreferenceEmailEvent.forEach(function (userEmailEvent) {
                            if (element.Id === userEmailEvent.Id) {
                                _this.commonEvents.push(element);
                                _this.notificationPreferenceFlag = 1;
                            }
                        });
                    }
                    if (_this.notificationPreferenceFlag === 0) {
                        _this.addedEvents.push(element);
                    }
                });
            }
            else if (this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0) {
                if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                    this.showNotification = true;
                }
            }
        }
        else {
            if (this.modifiedEvents && this.modifiedEvents.length > 0 && this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0) {
            }
            else {
                if (this.displayEmailEvents && this.displayEmailEvents.length > 0) {
                    this.displayEmailEvents.forEach(function (element, index) {
                        _this.notificationPreferenceFlag = 0;
                        if (_this.modifiedEvents && _this.modifiedEvents.length > 0) {
                            _this.modifiedEvents.forEach(function (userEmailEvent) {
                                if (element.Id === userEmailEvent.Id) {
                                    _this.notificationPreferenceFlag = 1;
                                }
                            });
                        }
                        if (_this.notificationPreferenceFlag === 0) {
                            _this.addedEvents.push(element);
                        }
                    });
                }
            }
        }
        this.addedEvents.forEach(function (emailEvent) {
            _this.enabledEvents.push({
                Id: emailEvent.Id
            });
        });
        if (this.commonEvents && this.commonEvents.length > 0) {
            this.commonEvents.forEach(function (emailEvent) {
                _this.disabledEvents.push({
                    Id: emailEvent.Id
                });
            });
        }
        if ((this.userPreferenceEmailEvent && this.userPreferenceEmailEvent.length > 0)) {
            if (this.modifiedEvents && this.modifiedEvents.length > 0) {
                var parameters = {
                    userCN: this.userID
                };
                this.appService.getPersonByUserId(parameters).subscribe(function (response) {
                    _this.userId = response.Id;
                    _this.loaderService.show();
                    _this.appService.MailPreference(_this.userId, _this.enabledEvents, _this.disabledEvents).subscribe(function (responses) {
                        _this.loaderService.hide();
                        _this.notification.info('Subscription saved successfully');
                        _this.closeDialog();
                    });
                });
            }
            else {
                this.notification.error('No custom preference to subscribe');
            }
        }
        else {
            var parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(function (response) {
                _this.userId = response.Id;
                _this.loaderService.show();
                _this.appService.MailPreference(_this.userId, _this.enabledEvents, _this.disabledEvents).subscribe(function (responses) {
                    _this.loaderService.hide();
                    _this.notification.info('Subscription saved successfully');
                    _this.closeDialog();
                });
            });
        }
    };
    EmailNotificationPreferenceComponent.prototype.emailEventsForManager = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.GetEmailEventsForManager().subscribe(function (managerEventResponse) {
                _this.allEmailEvents = [];
                if (managerEventResponse && managerEventResponse.Email_Event_Template) {
                    managerEventResponse.Email_Event_Template.forEach(function (emailEvent) {
                        _this.emailEvents = {
                            Name: emailEvent.EVENT_ID.replace(/_/g, ' '),
                            Id: emailEvent['Email_Event_Template-id'].Id,
                            isChecked: emailEvent.MANAGER_EVENT
                        };
                        _this.allEmailEvents.push(_this.emailEvents);
                    });
                }
                observer.next(_this.allEmailEvents);
                observer.complete();
            });
        });
    };
    EmailNotificationPreferenceComponent.prototype.emailEventsForUser = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.GetEmailEventsForUser().subscribe(function (userEventResponse) {
                _this.allEmailEvents = [];
                if (userEventResponse && userEventResponse.Email_Event_Template) {
                    userEventResponse.Email_Event_Template.forEach(function (emailEvent) {
                        _this.emailEvents = {
                            Name: emailEvent.EVENT_ID.replace(/_/g, ' '),
                            Id: emailEvent['Email_Event_Template-id'].Id,
                            isChecked: emailEvent.USER_EVENT
                        };
                        _this.allEmailEvents.push(_this.emailEvents);
                    });
                }
                observer.next(_this.allEmailEvents);
                observer.complete();
            });
        });
    };
    EmailNotificationPreferenceComponent.prototype.removeDuplicateEvents = function (allEvents) {
        // tslint:disable-next-line: prefer-for-of
        for (var i = 0; i < allEvents.length; i++) {
            for (var j = i + 1; j < allEvents.length; j++) {
                if (allEvents[i].Id === allEvents[j].Id) {
                    for (var k = j; k < allEvents.length; k++) {
                        allEvents[k] = allEvents[k + 1];
                    }
                    allEvents.length--;
                    j--;
                }
            }
        }
        return allEvents;
    };
    EmailNotificationPreferenceComponent.prototype.emailPreferences = function (isPM, isUser) {
        var _this = this;
        if (isPM && isUser) {
            var parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(function (personResponse) {
                _this.userId = personResponse.Id;
                _this.appService.GetMailPreferenceByUserId(_this.userId).subscribe(function (preferenceResponse) {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        _this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        _this.hasUserEmailPreference = true;
                        _this.appService.GetUserPreferenceMailTemplates(_this.userID).subscribe(function (userPrefernceMailTemplateResponse) {
                            _this.emailEventsForManager().subscribe(function (allManagerEvents) {
                                _this.emailEventsForUser().subscribe(function (allUserEvents) {
                                    var allEvents = allManagerEvents.concat(allUserEvents);
                                    var allEvent = _this.removeDuplicateEvents(allEvents);
                                    _this.hasUserPreference = true;
                                    if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                        _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                    }
                                    else {
                                        _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                    }
                                    _this.loaderService.show();
                                    allEvent.forEach(function (events) {
                                        _this.userPreferenceFlag = 0;
                                        // userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => {
                                        if (_this.userPreferenceEmailEvent.length > 0) {
                                            _this.userPreferenceEmailEvent.forEach(function (userPreference) {
                                                if (events.Id === userPreference.Id) {
                                                    _this.userPreferenceFlag = 1;
                                                    _this.displayEmailEvents.push(events);
                                                }
                                            });
                                        }
                                        if (_this.userPreferenceFlag === 0) {
                                            events.isChecked = false;
                                            _this.displayEmailEvents.push(events);
                                        }
                                    });
                                    _this.setAllCheck(_this.displayEmailEvents);
                                    _this.loaderService.hide();
                                });
                            });
                        });
                    }
                    else {
                        _this.loaderService.show();
                        _this.modifiedEvents = [];
                        _this.userPreferenceEmailEvent = [];
                        _this.hasUserPreference = false;
                        _this.emailEventsForManager().subscribe(function (allManagerEvents) {
                            _this.emailEventsForUser().subscribe(function (allUserEvents) {
                                var allEvents = allManagerEvents.concat(allUserEvents);
                                var allEvent = _this.removeDuplicateEvents(allEvents);
                                _this.loaderService.hide();
                                _this.displayEmailEvents = allEvent;
                                _this.setAllCheck(_this.displayEmailEvents);
                            });
                        });
                    }
                });
            });
        }
        else if (isPM) {
            var parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(function (personResponse) {
                _this.userId = personResponse.Id;
                _this.appService.GetMailPreferenceByUserId(_this.userId).subscribe(function (preferenceResponse) {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        _this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        _this.hasUserEmailPreference = true;
                        _this.appService.GetUserPreferenceMailTemplates(_this.userID).subscribe(function (userPrefernceMailTemplateResponse) {
                            _this.emailEventsForManager().subscribe(function (allEvents) {
                                _this.hasUserPreference = true;
                                if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                    _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                }
                                else {
                                    _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                }
                                _this.loaderService.show();
                                allEvents.forEach(function (events) {
                                    _this.userPreferenceFlag = 0;
                                    /* userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => { */
                                    if (_this.userPreferenceEmailEvent.length > 0) {
                                        _this.userPreferenceEmailEvent.forEach(function (userPreference) {
                                            if (events.Id === userPreference.Id) {
                                                _this.userPreferenceFlag = 1;
                                                _this.displayEmailEvents.push(events);
                                            }
                                        });
                                    }
                                    if (_this.userPreferenceFlag === 0) {
                                        events.isChecked = false;
                                        _this.displayEmailEvents.push(events);
                                    }
                                });
                                _this.setAllCheck(_this.displayEmailEvents);
                                _this.loaderService.hide();
                            });
                        });
                    }
                    else {
                        _this.loaderService.show();
                        _this.modifiedEvents = [];
                        _this.userPreferenceEmailEvent = [];
                        _this.hasUserPreference = false;
                        _this.emailEventsForManager().subscribe(function (allEvents) {
                            _this.loaderService.hide();
                            _this.displayEmailEvents = allEvents;
                            _this.setAllCheck(_this.displayEmailEvents);
                        });
                    }
                });
            });
        }
        else {
            var parameters = {
                userCN: this.userID
            };
            this.appService.getPersonByUserId(parameters).subscribe(function (personResponse) {
                _this.userId = personResponse.Id;
                _this.appService.GetMailPreferenceByUserId(_this.userId).subscribe(function (preferenceResponse) {
                    if (preferenceResponse && preferenceResponse.MPM_Email_Preference &&
                        preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'] && preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id) {
                        _this.userEmailPreferenceId = preferenceResponse.MPM_Email_Preference['MPM_Email_Preference-id'].Id;
                        _this.hasUserEmailPreference = true;
                        _this.appService.GetUserPreferenceMailTemplates(_this.userID).subscribe(function (userPrefernceMailTemplateResponse) {
                            _this.emailEventsForUser().subscribe(function (allEvents) {
                                _this.hasUserPreference = true;
                                if (Array.isArray(userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent) && userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.length > 0) {
                                    _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                }
                                else {
                                    _this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent !== undefined ? [userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent] : [];
                                }
                                //this.userPreferenceEmailEvent = userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent;
                                _this.loaderService.show();
                                allEvents.forEach(function (events) {
                                    _this.userPreferenceFlag = 0;
                                    //  userPrefernceMailTemplateResponse.tuple.old.EmailPreference.EmailEvents.EmailEvent.forEach(userPreference => {
                                    if (_this.userPreferenceEmailEvent.length > 0) {
                                        _this.userPreferenceEmailEvent.forEach(function (userPreference) {
                                            if (events.Id === userPreference.Id) {
                                                _this.userPreferenceFlag = 1;
                                                _this.displayEmailEvents.push(events);
                                            }
                                        });
                                    }
                                    if (_this.userPreferenceFlag === 0) {
                                        events.isChecked = false;
                                        _this.displayEmailEvents.push(events);
                                    }
                                });
                                _this.setAllCheck(_this.displayEmailEvents);
                                _this.loaderService.hide();
                            });
                        });
                    }
                    else {
                        _this.loaderService.show();
                        _this.modifiedEvents = [];
                        _this.userPreferenceEmailEvent = [];
                        _this.hasUserPreference = false;
                        _this.emailEventsForUser().subscribe(function (allEvents) {
                            _this.loaderService.hide();
                            _this.displayEmailEvents = allEvents;
                            _this.setAllCheck(_this.displayEmailEvents);
                        });
                    }
                });
            });
        }
    };
    // ngOnChanges(changes: SimpleChanges) {
    //     console.log(changes);
    // }
    EmailNotificationPreferenceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loggedInUser = this.sharingService.getCurrentUserObject();
        this.loggedInUser.ManagerFor.Target.forEach(function (user) {
            if (user.Name === RoleConstants.PROJECT_MANAGER) {
                _this.isPM = true;
            }
            else if (user.Name.includes(RoleConstants.PROJECT_MEMBER) ||
                user.Name.includes(RoleConstants.PROJECT_APPROVER) ||
                user.Name.includes(RoleConstants.PROJECT_REVIEWER)) {
                _this.isUser = true;
            }
        });
        this.EmailEventFormGroup = this.formBuilder.group({
            emailPreference: new FormControl(),
        });
        this.userID = this.sharingService.getCurrentUserID();
        this.loaderService.show();
        this.emailPreferences(this.isPM, this.isUser);
    };
    EmailNotificationPreferenceComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: SharingService },
        { type: AppService },
        { type: FormBuilder },
        { type: LoaderService },
        { type: NotificationService }
    ]; };
    EmailNotificationPreferenceComponent = __decorate([
        Component({
            selector: "mpm-email-notification-preference",
            template: "<div class=\"flex-container\">\r\n    <div class=\"headerAlign\">\r\n        <h1 mat-dialog-title class=\"bulk-comments-modal-title\">\r\n            {{ data.message }}\r\n        </h1>\r\n    </div>\r\n    <div>\r\n        <button matTooltip=\"Close\" mat-icon-button color=\"primary\" (click)=\"closeDialog()\" [mat-dialog-close]=\"true\"\r\n            class=\"mat-icon-close-btn\">\r\n            <mat-icon>close</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<form [formGroup]=\"EmailEventFormGroup\">\r\n    <mat-checkbox [checked]=\"allCheck\" (change)=\"setSelectAll($event.checked)\">\r\n        <strong>{{ selectAll}}</strong>\r\n    </mat-checkbox>\r\n\r\n    <div class=\"email-lists\">\r\n        <div *ngFor=\"let emailEvent of displayEmailEvents\">\r\n            <mat-checkbox [checked]=\"emailEvent.isChecked\" (change)=\"\r\n                    $event\r\n                        ? masterToggle($event.checked, emailEvent.Name, emailEvent.Id,emailEvent)\r\n                        : null\r\n                \">\r\n                <span>{{ emailEvent.Name | titlecase }}</span>\r\n            </mat-checkbox>\r\n        </div>\r\n    </div>\r\n    <div class=\"button\">\r\n        <div class=\"flex-row\">\r\n            <div class=\"flex-row-item\">\r\n                <span class=\"form-actions\">\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Cancel\" (click)=\"closeDialog()\">\r\n                        Cancel\r\n                    </button>\r\n                    &nbsp;\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Save\" (click)=\"notificationPreference()\"\r\n                        [disabled]=\"EmailEventFormGroup.invalid\">\r\n                        Save\r\n                    </button>\r\n                    &nbsp;\r\n                    <button mat-stroked-button type=\"button\" matTooltip=\"Restore Default\" (click)=\"restoreDefault()\"\r\n                        [disabled]=\"!hasUserEmailPreference\">\r\n                        Restore Default\r\n                    </button>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\r\n<!-- <div class=\"example-container mat-elevation-z8\">\r\n        <table mat-table [dataSource]=\"displayEmailEvents\">\r\n    \r\n            <ng-container >\r\n    \r\n                <td mat-cell *matCellDef=\"let emailEvent\">\r\n                    <mat-checkbox [checked]=\"emailEvent.isChecked\"\r\n                        (change)=\"$event ? masterToggle($event,emailEvent.Name,emailEvent.Id) : null\">\r\n                    </mat-checkbox>\r\n                </td>\r\n    \r\n            </ng-container>\r\n    \r\n    \r\n            <ng-container >\r\n    \r\n                <td mat-cell *matCellDef=\"let emailEvent\"> <span>{{emailEvent.Name | titlecase}}</span> </td>\r\n    \r\n            </ng-container>\r\n    \r\n    \r\n            <tr mat-row *matRowDef=\"let row;\"></tr>\r\n           \r\n        </table>\r\n    </div> -->",
            styles: [".form-actions{margin-left:auto}.bulk-comments-modal-title{font-weight:700;flex-grow:1}.flex-container{display:flex}.headerAlign{flex-grow:5}.email-lists{height:62vh;margin-top:2%;overflow-y:scroll;overflow-x:hidden}.button{margin-top:30px}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], EmailNotificationPreferenceComponent);
    return EmailNotificationPreferenceComponent;
}());
export { EmailNotificationPreferenceComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UvZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDekUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDckUsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN6RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkRBQTJELENBQUM7QUFDMUYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQU9qRjtJQTRCSSw4Q0FDVyxTQUE2RCxFQUNwQyxJQUFTLEVBQ2xDLGNBQThCLEVBQzlCLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3ZCLGFBQTRCLEVBQzVCLFlBQWlDO1FBTmxDLGNBQVMsR0FBVCxTQUFTLENBQW9EO1FBQ3BDLFNBQUksR0FBSixJQUFJLENBQUs7UUFDbEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBakM3QyxTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2IsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUVmLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBR3BCLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHVCQUFrQixHQUFHLENBQUMsQ0FBQztRQUV2QixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQix1QkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDeEIsdUJBQWtCLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLDZCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUM5QixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsK0JBQTBCLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUV6QiwyQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDL0IsY0FBUyxHQUFHLFlBQVksQ0FBQztRQUV6QixZQUFPLEdBQUcsS0FBSyxDQUFDO0lBVVosQ0FBQztJQUVMLDJEQUFZLEdBQVosVUFBYSxLQUFLO1FBQWxCLGlCQVFDO1FBUEcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxVQUFDLFVBQVU7WUFDdkMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3ZELEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQTthQUN2RTtZQUNELFVBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUVELDBEQUFXLEdBQVgsVUFBWSxlQUFlO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsZUFBZSxDQUFDLEtBQUssQ0FBQyxVQUFDLFVBQVUsSUFBSyxPQUFBLFVBQVUsQ0FBQyxTQUFTLElBQUksS0FBSyxFQUE3QixDQUE2QixDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELDBEQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRUQsNkRBQWMsR0FBZDtRQUFBLGlCQVVDO1FBVEcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMxQixJQUFNLGdCQUFnQixHQUFHO1lBQ3JCLEVBQUUsRUFBRSxJQUFJLENBQUMscUJBQXFCO1NBQ2pDLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUMzRSxLQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUM7SUFFRCwyREFBWSxHQUFaLFVBQWEsS0FBSyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsVUFBVTtRQUE3QyxpQkF3QkM7UUF2QkcsVUFBVSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUUxQyxJQUFJLENBQUMsaUJBQWlCLEdBQUc7WUFDckIsSUFBSSxFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztZQUNsQyxFQUFFLEVBQUUsRUFBRTtZQUNOLFNBQVMsRUFBRSxLQUFLO1NBQ25CLENBQUM7UUFDRixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN0RSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUs7Z0JBQ3ZDLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsRUFBRTtvQkFDL0MsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO2lCQUMvQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEtBQUssQ0FBQyxFQUFFO2dCQUMvQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUNwRDtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7SUFFRCxxRUFBc0IsR0FBdEI7UUFBQSxpQkE2RkM7UUE1RkcsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3ZELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLEtBQUs7b0JBQ3ZDLEtBQUksQ0FBQywwQkFBMEIsR0FBRyxDQUFDLENBQUM7b0JBQ3BDLElBQUksS0FBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzFDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjOzRCQUNoRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtnQ0FFbEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7Z0NBQ2hDLEtBQUksQ0FBQywwQkFBMEIsR0FBRyxDQUFDLENBQUM7NkJBRXZDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3FCQUNOO29CQUNELElBQUksS0FBSSxDQUFDLDBCQUEwQixLQUFLLENBQUMsRUFBRTt3QkFDdkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2xDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU0sSUFBSSxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xGLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQ2hDO2FBQ0o7U0FDSjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7YUFDdkk7aUJBQU07Z0JBRUgsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQy9ELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsS0FBSzt3QkFDM0MsS0FBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxLQUFJLENBQUMsY0FBYyxJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDdkQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjO2dDQUN0QyxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtvQ0FDbEMsS0FBSSxDQUFDLDBCQUEwQixHQUFHLENBQUMsQ0FBQztpQ0FDdkM7NEJBQ0wsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBQ0QsSUFBSSxLQUFJLENBQUMsMEJBQTBCLEtBQUssQ0FBQyxFQUFFOzRCQUN2QyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDbEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtTQUNKO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO1lBQy9CLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDO2dCQUNwQixFQUFFLEVBQUUsVUFBVSxDQUFDLEVBQUU7YUFDcEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25ELElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsVUFBVTtnQkFDaEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7b0JBQ3JCLEVBQUUsRUFBRSxVQUFVLENBQUMsRUFBRTtpQkFDcEIsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRTtZQUM3RSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2RCxJQUFNLFVBQVUsR0FBRztvQkFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07aUJBQ3RCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUM1RCxLQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSSxDQUFDLGFBQWEsRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsU0FBUzt3QkFDcEcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsaUNBQWlDLENBQUMsQ0FBQzt3QkFDMUQsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN2QixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7YUFDaEU7U0FDSjthQUFNO1lBQ0gsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2FBQ3RCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQzVELEtBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEVBQUUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxTQUFTO29CQUNwRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMxQixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO29CQUMxRCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCxvRUFBcUIsR0FBckI7UUFBQSxpQkFrQkM7UUFqQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLG9CQUFvQjtnQkFDckUsS0FBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7Z0JBQ3pCLElBQUksb0JBQW9CLElBQUksb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7b0JBQ25FLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxVQUFBLFVBQVU7d0JBQ3hELEtBQUksQ0FBQyxXQUFXLEdBQUc7NEJBQ2YsSUFBSSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7NEJBQzVDLEVBQUUsRUFBRSxVQUFVLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFOzRCQUM1QyxTQUFTLEVBQUUsVUFBVSxDQUFDLGFBQWE7eUJBQ3RDLENBQUM7d0JBQ0YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMvQyxDQUFDLENBQUMsQ0FBQztpQkFDTjtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDbkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsaUVBQWtCLEdBQWxCO1FBQUEsaUJBa0JDO1FBakJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxpQkFBaUI7Z0JBQy9ELEtBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO2dCQUN6QixJQUFJLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLG9CQUFvQixFQUFFO29CQUM3RCxpQkFBaUIsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO3dCQUNyRCxLQUFJLENBQUMsV0FBVyxHQUFHOzRCQUNmLElBQUksRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDOzRCQUM1QyxFQUFFLEVBQUUsVUFBVSxDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRTs0QkFDNUMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxVQUFVO3lCQUNuQyxDQUFDO3dCQUNGLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDL0MsQ0FBQyxDQUFDLENBQUM7aUJBQ047Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ25DLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9FQUFxQixHQUFyQixVQUFzQixTQUFTO1FBQzNCLDBDQUEwQztRQUMxQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNDLElBQUksU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDdkMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7cUJBQ25DO29CQUNELFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDbkIsQ0FBQyxFQUFFLENBQUM7aUJBQ1A7YUFDSjtTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELCtEQUFnQixHQUFoQixVQUFpQixJQUFJLEVBQUUsTUFBTTtRQUE3QixpQkFnTEM7UUEvS0csSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO1lBQ2hCLElBQU0sVUFBVSxHQUFHO2dCQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTthQUN0QixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO2dCQUNsRSxLQUFJLENBQUMsTUFBTSxHQUFHLGNBQWMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2hDLEtBQUksQ0FBQyxVQUFVLENBQUMseUJBQXlCLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGtCQUFrQjtvQkFDL0UsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxvQkFBb0I7d0JBQzdELGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLElBQUksa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFLEVBQUU7d0JBQzdJLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3QkFDbkcsS0FBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQzt3QkFDbkMsS0FBSSxDQUFDLFVBQVUsQ0FBQyw4QkFBOEIsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsaUNBQWlDOzRCQUNuRyxLQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxnQkFBZ0I7Z0NBQ25ELEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLGFBQWE7b0NBQzdDLElBQU0sU0FBUyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztvQ0FDekQsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUN2RCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO29DQUM5QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3Q0FDcE0sS0FBSSxDQUFDLHdCQUF3QixHQUFHLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUM7cUNBQ3RIO3lDQUFNO3dDQUNILEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQ0FDaE87b0NBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQ0FDMUIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07d0NBQ25CLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLENBQUM7d0NBQzVCLGlIQUFpSDt3Q0FDakgsSUFBSSxLQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0Q0FDMUMsS0FBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLGNBQWM7Z0RBQ2hELElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxjQUFjLENBQUMsRUFBRSxFQUFFO29EQUNqQyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO29EQUM1QixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lEQUN4Qzs0Q0FDTCxDQUFDLENBQUMsQ0FBQzt5Q0FDTjt3Q0FDRCxJQUFJLEtBQUksQ0FBQyxrQkFBa0IsS0FBSyxDQUFDLEVBQUU7NENBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDOzRDQUN6QixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lDQUN4QztvQ0FDTCxDQUFDLENBQUMsQ0FBQztvQ0FDSCxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29DQUMxQyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO2dDQUM5QixDQUFDLENBQUMsQ0FBQzs0QkFDUCxDQUFDLENBQUMsQ0FBQzt3QkFDUCxDQUFDLENBQUMsQ0FBQztxQkFDTjt5QkFBTTt3QkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMxQixLQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQzt3QkFDekIsS0FBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQzt3QkFDbkMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQzt3QkFDL0IsS0FBSSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsZ0JBQWdCOzRCQUNuRCxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxhQUFhO2dDQUM3QyxJQUFNLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7Z0NBQ3pELElBQU0sUUFBUSxHQUFHLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQ0FDdkQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDMUIsS0FBSSxDQUFDLGtCQUFrQixHQUFHLFFBQVEsQ0FBQztnQ0FDbkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQzs0QkFDOUMsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBRU47Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztTQUVOO2FBQU0sSUFBSSxJQUFJLEVBQUU7WUFDYixJQUFNLFVBQVUsR0FBRztnQkFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07YUFDdEIsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsY0FBYztnQkFDbEUsS0FBSSxDQUFDLE1BQU0sR0FBRyxjQUFjLENBQUMsRUFBRSxDQUFDO2dCQUNoQyxLQUFJLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxrQkFBa0I7b0JBQy9FLElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsb0JBQW9CO3dCQUM3RCxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUM3SSxLQUFJLENBQUMscUJBQXFCLEdBQUcsa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ25HLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7d0JBQ25DLEtBQUksQ0FBQyxVQUFVLENBQUMsOEJBQThCLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGlDQUFpQzs0QkFDbkcsS0FBSSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsU0FBUztnQ0FDNUMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQ0FDOUIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0NBQ3BNLEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDO2lDQUN0SDtxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsd0JBQXdCLEdBQUcsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ2hPO2dDQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO29DQUNwQixLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO29DQUM1QixvSEFBb0g7b0NBRXBILElBQUksS0FBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0NBQzFDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjOzRDQUNoRCxJQUFJLE1BQU0sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtnREFDakMsS0FBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztnREFDNUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs2Q0FDeEM7d0NBQ0wsQ0FBQyxDQUFDLENBQUM7cUNBQ047b0NBQ0QsSUFBSSxLQUFJLENBQUMsa0JBQWtCLEtBQUssQ0FBQyxFQUFFO3dDQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3Q0FDekIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQ0FDeEM7Z0NBRUwsQ0FBQyxDQUFDLENBQUM7Z0NBQ0gsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQ0FDMUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDOUIsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7d0JBQ3pCLEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUM7d0JBQ25DLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7d0JBQy9CLEtBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFNBQVM7NEJBQzVDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7NEJBQzFCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUM7NEJBQ3BDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQzlDLENBQUMsQ0FBQyxDQUFDO3FCQUNOO2dCQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBTSxVQUFVLEdBQUc7Z0JBQ2YsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2FBQ3RCLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLGNBQWM7Z0JBQ2xFLEtBQUksQ0FBQyxNQUFNLEdBQUcsY0FBYyxDQUFDLEVBQUUsQ0FBQztnQkFDaEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsa0JBQWtCO29CQUMvRSxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLG9CQUFvQjt3QkFDN0Qsa0JBQWtCLENBQUMsb0JBQW9CLENBQUMseUJBQXlCLENBQUMsSUFBSSxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLEVBQUUsRUFBRTt3QkFDN0ksS0FBSSxDQUFDLHFCQUFxQixHQUFHLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDLHlCQUF5QixDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUNuRyxLQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO3dCQUNuQyxLQUFJLENBQUMsVUFBVSxDQUFDLDhCQUE4QixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxpQ0FBaUM7NEJBQ25HLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFNBQVM7Z0NBQ3pDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0NBQzlCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29DQUNwTSxLQUFJLENBQUMsd0JBQXdCLEdBQUcsaUNBQWlDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQztpQ0FDdEg7cUNBQU07b0NBQ0gsS0FBSSxDQUFDLHdCQUF3QixHQUFHLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUFpQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNoTztnQ0FDRCxxSEFBcUg7Z0NBQ3JILEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzFCLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO29DQUNwQixLQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO29DQUM1QixrSEFBa0g7b0NBQ2xILElBQUksS0FBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0NBQzFDLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxjQUFjOzRDQUNoRCxJQUFJLE1BQU0sQ0FBQyxFQUFFLEtBQUssY0FBYyxDQUFDLEVBQUUsRUFBRTtnREFDakMsS0FBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQztnREFDNUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs2Q0FDeEM7d0NBQ0wsQ0FBQyxDQUFDLENBQUM7cUNBQ047b0NBQ0QsSUFBSSxLQUFJLENBQUMsa0JBQWtCLEtBQUssQ0FBQyxFQUFFO3dDQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3Q0FDekIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztxQ0FDeEM7Z0NBRUwsQ0FBQyxDQUFDLENBQUM7Z0NBQ0gsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQ0FDMUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDOUIsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQyxDQUFDLENBQUM7cUJBQ047eUJBQU07d0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7d0JBQ3pCLEtBQUksQ0FBQyx3QkFBd0IsR0FBRyxFQUFFLENBQUM7d0JBQ25DLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7d0JBQy9CLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFNBQVM7NEJBQ3pDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7NEJBQzFCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUM7NEJBQ3BDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQzlDLENBQUMsQ0FBQyxDQUFDO3FCQUVOO2dCQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFFRCx3Q0FBd0M7SUFDeEMsNEJBQTRCO0lBQzVCLElBQUk7SUFFSix1REFBUSxHQUFSO1FBQUEsaUJBbUJDO1FBbEJHLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQy9ELElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO1lBQzdDLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsZUFBZSxFQUFFO2dCQUM3QyxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzthQUNwQjtpQkFBTSxJQUNILElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEVBQ3BEO2dCQUNFLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDOUMsZUFBZSxFQUFFLElBQUksV0FBVyxFQUFFO1NBQ3JDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3JELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xELENBQUM7O2dCQWphcUIsWUFBWTtnREFDN0IsTUFBTSxTQUFDLGVBQWU7Z0JBQ0EsY0FBYztnQkFDbEIsVUFBVTtnQkFDVCxXQUFXO2dCQUNSLGFBQWE7Z0JBQ2QsbUJBQW1COztJQW5DcEMsb0NBQW9DO1FBTGhELFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQ0FBbUM7WUFDN0MsbS9GQUE2RDs7U0FFaEUsQ0FBQztRQStCTyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtPQTlCbkIsb0NBQW9DLENBK2JoRDtJQUFELDJDQUFDO0NBQUEsQUEvYkQsSUErYkM7U0EvYlksb0NBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCwgU2ltcGxlQ2hhbmdlcyB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtQ29udHJvbCwgRm9ybUdyb3VwIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2RpYWxvZ1wiO1xyXG5pbXBvcnQgeyBSb2xlQ29uc3RhbnRzIH0gZnJvbSBcIi4uLy4vLi4vLi4vLi4vbGliL3Byb2plY3Qvc2hhcmVkL2NvbnN0YW50cy9yb2xlLmNvbnN0YW50c1wiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXBwU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJtcG0tZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2VcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vZW1haWwtbm90aWZpY2F0aW9uLXByZWZlcmVuY2UuY29tcG9uZW50Lmh0bWxcIixcclxuICAgIHN0eWxlVXJsczogW1wiLi9lbWFpbC1ub3RpZmljYXRpb24tcHJlZmVyZW5jZS5jb21wb25lbnQuY3NzXCJdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGxvZ2dlZEluVXNlcjtcclxuICAgIGlzUE0gPSBmYWxzZTtcclxuICAgIGlzVXNlciA9IGZhbHNlO1xyXG4gICAgZW1haWxFdmVudHM7XHJcbiAgICBhbGxFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgRW1haWxFdmVudEZvcm1Hcm91cDogRm9ybUdyb3VwO1xyXG4gICAgaW50ZXJtZWRpYXRlQ2hlY2s7XHJcbiAgICBhZGRlZEV2ZW50czogYW55W10gPSBbXTtcclxuICAgIG1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICBtb2RpZmllZEV2ZW50c0ZsYWcgPSAwO1xyXG4gICAgdXNlcklkO1xyXG4gICAgZW5hYmxlZEV2ZW50cyA9IFtdO1xyXG4gICAgZGlzYWJsZWRFdmVudHMgPSBbXTtcclxuICAgIHVzZXJJRDtcclxuICAgIGRpc3BsYXlFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgdXNlclByZWZlcmVuY2VGbGFnID0gMDtcclxuICAgIHVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IFtdO1xyXG4gICAgaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgIGNvbW1vbkV2ZW50cyA9IFtdO1xyXG4gICAgbm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgdXNlckVtYWlsUHJlZmVyZW5jZUlkO1xyXG4gICAgaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IGZhbHNlO1xyXG4gICAgc2VsZWN0QWxsID0gXCJTZWxlY3QgQWxsXCI7XHJcbiAgICBhbGxDaGVjazogQm9vbGVhbjtcclxuICAgIHJlc3RvcmUgPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8RW1haWxOb3RpZmljYXRpb25QcmVmZXJlbmNlQ29tcG9uZW50PixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHNldFNlbGVjdEFsbChldmVudCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzLmZvckVhY2goKGVtYWlsTGlzdHMpID0+IHtcclxuICAgICAgICAgICAgaWYgKCEoZXZlbnQpID09IEJvb2xlYW4oSlNPTi5wYXJzZShlbWFpbExpc3RzLmlzQ2hlY2tlZCkpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hc3RlclRvZ2dsZShldmVudCwgZW1haWxMaXN0cy5OYW1lLCBlbWFpbExpc3RzLklkLCBlbWFpbExpc3RzKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVtYWlsTGlzdHMuaXNDaGVja2VkID0gZXZlbnQ7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbENoZWNrKGVtYWlsRXZlbnRMaXN0cykge1xyXG4gICAgICAgIHRoaXMuYWxsQ2hlY2sgPSBlbWFpbEV2ZW50TGlzdHMuZXZlcnkoKGVtYWlsZXZlbnQpID0+IGVtYWlsZXZlbnQuaXNDaGVja2VkICE9IGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZURpYWxvZygpIHtcclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzdG9yZURlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICBjb25zdCB1c2VyUHJlZmVyZW5jZUlEID0ge1xyXG4gICAgICAgICAgICBJZDogdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuYXBwU2VydmljZS5EZWxldGVNUE1fRW1haWxfUHJlZmVyZW5jZSh1c2VyUHJlZmVyZW5jZUlEKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmhhc1VzZXJFbWFpbFByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lbWFpbFByZWZlcmVuY2VzKHRoaXMuaXNQTSwgdGhpcy5pc1VzZXIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBtYXN0ZXJUb2dnbGUoZXZlbnQsIGV2ZW50TmFtZSwgaWQsIGVtYWlsRXZlbnQpIHtcclxuICAgICAgICBlbWFpbEV2ZW50LmlzQ2hlY2tlZCA9IGV2ZW50O1xyXG4gICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG5cclxuICAgICAgICB0aGlzLmludGVybWVkaWF0ZUNoZWNrID0ge1xyXG4gICAgICAgICAgICBOYW1lOiBldmVudE5hbWUucmVwbGFjZSgvXy9nLCAnICcpLFxyXG4gICAgICAgICAgICBJZDogaWQsXHJcbiAgICAgICAgICAgIGlzQ2hlY2tlZDogZXZlbnRcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMubW9kaWZpZWRFdmVudHMpICYmIHRoaXMubW9kaWZpZWRFdmVudHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzRmxhZyA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50Lk5hbWUgPT09IGV2ZW50TmFtZS5yZXBsYWNlKC9fL2csICcgJykpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RpZmllZEV2ZW50c0ZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHNGbGFnICE9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzLnB1c2godGhpcy5pbnRlcm1lZGlhdGVDaGVjayk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMucHVzaCh0aGlzLmludGVybWVkaWF0ZUNoZWNrKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbm90aWZpY2F0aW9uUHJlZmVyZW5jZSgpIHtcclxuICAgICAgICB0aGlzLmFkZGVkRXZlbnRzID0gW107XHJcbiAgICAgICAgdGhpcy5lbmFibGVkRXZlbnRzID0gW107XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlZEV2ZW50cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmhhc1VzZXJQcmVmZXJlbmNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd05vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5mb3JFYWNoKHVzZXJFbWFpbEV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50LklkID09PSB1c2VyRW1haWxFdmVudC5JZCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbW1vbkV2ZW50cy5wdXNoKGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkZWRFdmVudHMucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCAmJiB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNob3dOb3RpZmljYXRpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHMgJiYgdGhpcy5tb2RpZmllZEV2ZW50cy5sZW5ndGggPiAwICYmIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ICYmIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kaXNwbGF5RW1haWxFdmVudHMgJiYgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzLmZvckVhY2goKGVsZW1lbnQsIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5tb2RpZmllZEV2ZW50cyAmJiB0aGlzLm1vZGlmaWVkRXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubW9kaWZpZWRFdmVudHMuZm9yRWFjaCh1c2VyRW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVsZW1lbnQuSWQgPT09IHVzZXJFbWFpbEV2ZW50LklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vdGlmaWNhdGlvblByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZGVkRXZlbnRzLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmFkZGVkRXZlbnRzLmZvckVhY2goZW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlZEV2ZW50cy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIElkOiBlbWFpbEV2ZW50LklkXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmICh0aGlzLmNvbW1vbkV2ZW50cyAmJiB0aGlzLmNvbW1vbkV2ZW50cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbW9uRXZlbnRzLmZvckVhY2goZW1haWxFdmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRpc2FibGVkRXZlbnRzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIElkOiBlbWFpbEV2ZW50LklkXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgJiYgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubW9kaWZpZWRFdmVudHMgJiYgdGhpcy5tb2RpZmllZEV2ZW50cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLk1haWxQcmVmZXJlbmNlKHRoaXMudXNlcklkLCB0aGlzLmVuYWJsZWRFdmVudHMsIHRoaXMuZGlzYWJsZWRFdmVudHMpLnN1YnNjcmliZShyZXNwb25zZXMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbi5pbmZvKCdTdWJzY3JpcHRpb24gc2F2ZWQgc3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xvc2VEaWFsb2coKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uZXJyb3IoJ05vIGN1c3RvbSBwcmVmZXJlbmNlIHRvIHN1YnNjcmliZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5NYWlsUHJlZmVyZW5jZSh0aGlzLnVzZXJJZCwgdGhpcy5lbmFibGVkRXZlbnRzLCB0aGlzLmRpc2FibGVkRXZlbnRzKS5zdWJzY3JpYmUocmVzcG9uc2VzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uLmluZm8oJ1N1YnNjcmlwdGlvbiBzYXZlZCBzdWNjZXNzZnVsbHknKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlRGlhbG9nKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGVtYWlsRXZlbnRzRm9yTWFuYWdlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRFbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUobWFuYWdlckV2ZW50UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbGxFbWFpbEV2ZW50cyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgaWYgKG1hbmFnZXJFdmVudFJlc3BvbnNlICYmIG1hbmFnZXJFdmVudFJlc3BvbnNlLkVtYWlsX0V2ZW50X1RlbXBsYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFuYWdlckV2ZW50UmVzcG9uc2UuRW1haWxfRXZlbnRfVGVtcGxhdGUuZm9yRWFjaChlbWFpbEV2ZW50ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50cyA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5hbWU6IGVtYWlsRXZlbnQuRVZFTlRfSUQucmVwbGFjZSgvXy9nLCAnICcpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgSWQ6IGVtYWlsRXZlbnRbJ0VtYWlsX0V2ZW50X1RlbXBsYXRlLWlkJ10uSWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0NoZWNrZWQ6IGVtYWlsRXZlbnQuTUFOQUdFUl9FVkVOVFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFsbEVtYWlsRXZlbnRzLnB1c2godGhpcy5lbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuYWxsRW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZW1haWxFdmVudHNGb3JVc2VyKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldEVtYWlsRXZlbnRzRm9yVXNlcigpLnN1YnNjcmliZSh1c2VyRXZlbnRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFsbEVtYWlsRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlckV2ZW50UmVzcG9uc2UgJiYgdXNlckV2ZW50UmVzcG9uc2UuRW1haWxfRXZlbnRfVGVtcGxhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VyRXZlbnRSZXNwb25zZS5FbWFpbF9FdmVudF9UZW1wbGF0ZS5mb3JFYWNoKGVtYWlsRXZlbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgTmFtZTogZW1haWxFdmVudC5FVkVOVF9JRC5yZXBsYWNlKC9fL2csICcgJyksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBJZDogZW1haWxFdmVudFsnRW1haWxfRXZlbnRfVGVtcGxhdGUtaWQnXS5JZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzQ2hlY2tlZDogZW1haWxFdmVudC5VU0VSX0VWRU5UXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWxsRW1haWxFdmVudHMucHVzaCh0aGlzLmVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5hbGxFbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVEdXBsaWNhdGVFdmVudHMoYWxsRXZlbnRzKSB7XHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBwcmVmZXItZm9yLW9mXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxFdmVudHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgYWxsRXZlbnRzLmxlbmd0aDsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoYWxsRXZlbnRzW2ldLklkID09PSBhbGxFdmVudHNbal0uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBrID0gajsgayA8IGFsbEV2ZW50cy5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHNba10gPSBhbGxFdmVudHNbayArIDFdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHMubGVuZ3RoLS07XHJcbiAgICAgICAgICAgICAgICAgICAgai0tO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhbGxFdmVudHM7XHJcbiAgICB9XHJcblxyXG4gICAgZW1haWxQcmVmZXJlbmNlcyhpc1BNLCBpc1VzZXIpIHtcclxuICAgICAgICBpZiAoaXNQTSAmJiBpc1VzZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHBlcnNvblJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldE1haWxQcmVmZXJlbmNlQnlVc2VySWQodGhpcy51c2VySWQpLnN1YnNjcmliZShwcmVmZXJlbmNlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmVmZXJlbmNlUmVzcG9uc2UgJiYgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWQgPSBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRVc2VyUHJlZmVyZW5jZU1haWxUZW1wbGF0ZXModGhpcy51c2VySUQpLnN1YnNjcmliZSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsTWFuYWdlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0ZvclVzZXIoKS5zdWJzY3JpYmUoYWxsVXNlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGFsbEV2ZW50cyA9IGFsbE1hbmFnZXJFdmVudHMuY29uY2F0KGFsbFVzZXJFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudCA9IHRoaXMucmVtb3ZlRHVwbGljYXRlRXZlbnRzKGFsbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50KSAmJiB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudCAhPT0gdW5kZWZpbmVkID8gW3VzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnRdIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsRXZlbnQuZm9yRWFjaChldmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudC5mb3JFYWNoKHVzZXJQcmVmZXJlbmNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudHMuSWQgPT09IHVzZXJQcmVmZXJlbmNlLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50cy5pc0NoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEFsbENoZWNrKHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsTWFuYWdlckV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzRm9yVXNlcigpLnN1YnNjcmliZShhbGxVc2VyRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudHMgPSBhbGxNYW5hZ2VyRXZlbnRzLmNvbmNhdChhbGxVc2VyRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhbGxFdmVudCA9IHRoaXMucmVtb3ZlRHVwbGljYXRlRXZlbnRzKGFsbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyA9IGFsbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNQTSkge1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgdXNlckNOOiB0aGlzLnVzZXJJRFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycykuc3Vic2NyaWJlKHBlcnNvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlcklkID0gcGVyc29uUmVzcG9uc2UuSWQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuR2V0TWFpbFByZWZlcmVuY2VCeVVzZXJJZCh0aGlzLnVzZXJJZCkuc3Vic2NyaWJlKHByZWZlcmVuY2VSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHByZWZlcmVuY2VSZXNwb25zZSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2UgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlWydNUE1fRW1haWxfUHJlZmVyZW5jZS1pZCddICYmIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJFbWFpbFByZWZlcmVuY2VJZCA9IHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXS5JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYXNVc2VyRW1haWxQcmVmZXJlbmNlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldFVzZXJQcmVmZXJlbmNlTWFpbFRlbXBsYXRlcyh0aGlzLnVzZXJJRCkuc3Vic2NyaWJlKHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVtYWlsRXZlbnRzRm9yTWFuYWdlcigpLnN1YnNjcmliZShhbGxFdmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQpICYmIHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50ICE9PSB1bmRlZmluZWQgPyBbdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudF0gOiBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGxFdmVudHMuZm9yRWFjaChldmVudHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qIHVzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7ICovXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQuZm9yRWFjaCh1c2VyUHJlZmVyZW5jZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50cy5JZCA9PT0gdXNlclByZWZlcmVuY2UuSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VGbGFnID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudHMuaXNDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cy5wdXNoKGV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRBbGxDaGVjayh0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGlmaWVkRXZlbnRzID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlclByZWZlcmVuY2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0Zvck1hbmFnZXIoKS5zdWJzY3JpYmUoYWxsRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFbWFpbEV2ZW50cyA9IGFsbEV2ZW50cztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy51c2VySURcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldFBlcnNvbkJ5VXNlcklkKHBhcmFtZXRlcnMpLnN1YnNjcmliZShwZXJzb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJJZCA9IHBlcnNvblJlc3BvbnNlLklkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLkdldE1haWxQcmVmZXJlbmNlQnlVc2VySWQodGhpcy51c2VySWQpLnN1YnNjcmliZShwcmVmZXJlbmNlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwcmVmZXJlbmNlUmVzcG9uc2UgJiYgcHJlZmVyZW5jZVJlc3BvbnNlLk1QTV9FbWFpbF9QcmVmZXJlbmNlICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZWZlcmVuY2VSZXNwb25zZS5NUE1fRW1haWxfUHJlZmVyZW5jZVsnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnXSAmJiBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRW1haWxQcmVmZXJlbmNlSWQgPSBwcmVmZXJlbmNlUmVzcG9uc2UuTVBNX0VtYWlsX1ByZWZlcmVuY2VbJ01QTV9FbWFpbF9QcmVmZXJlbmNlLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzVXNlckVtYWlsUHJlZmVyZW5jZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5HZXRVc2VyUHJlZmVyZW5jZU1haWxUZW1wbGF0ZXModGhpcy51c2VySUQpLnN1YnNjcmliZSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbWFpbEV2ZW50c0ZvclVzZXIoKS5zdWJzY3JpYmUoYWxsRXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc1VzZXJQcmVmZXJlbmNlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50KSAmJiB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50ID0gdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudCAhPT0gdW5kZWZpbmVkID8gW3VzZXJQcmVmZXJuY2VNYWlsVGVtcGxhdGVSZXNwb25zZS50dXBsZS5vbGQuRW1haWxQcmVmZXJlbmNlLkVtYWlsRXZlbnRzLkVtYWlsRXZlbnRdIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vdGhpcy51c2VyUHJlZmVyZW5jZUVtYWlsRXZlbnQgPSB1c2VyUHJlZmVybmNlTWFpbFRlbXBsYXRlUmVzcG9uc2UudHVwbGUub2xkLkVtYWlsUHJlZmVyZW5jZS5FbWFpbEV2ZW50cy5FbWFpbEV2ZW50O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxsRXZlbnRzLmZvckVhY2goZXZlbnRzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZUZsYWcgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgdXNlclByZWZlcm5jZU1haWxUZW1wbGF0ZVJlc3BvbnNlLnR1cGxlLm9sZC5FbWFpbFByZWZlcmVuY2UuRW1haWxFdmVudHMuRW1haWxFdmVudC5mb3JFYWNoKHVzZXJQcmVmZXJlbmNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VFbWFpbEV2ZW50LmZvckVhY2godXNlclByZWZlcmVuY2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudHMuSWQgPT09IHVzZXJQcmVmZXJlbmNlLklkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VGbGFnID0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMucHVzaChldmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlRmxhZyA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRzLmlzQ2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMucHVzaChldmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsQ2hlY2sodGhpcy5kaXNwbGF5RW1haWxFdmVudHMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RpZmllZEV2ZW50cyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlRW1haWxFdmVudCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc1VzZXJQcmVmZXJlbmNlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW1haWxFdmVudHNGb3JVc2VyKCkuc3Vic2NyaWJlKGFsbEV2ZW50cyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5RW1haWxFdmVudHMgPSBhbGxFdmVudHM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEFsbENoZWNrKHRoaXMuZGlzcGxheUVtYWlsRXZlbnRzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKGNoYW5nZXMpO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlck9iamVjdCgpO1xyXG4gICAgICAgIHRoaXMubG9nZ2VkSW5Vc2VyLk1hbmFnZXJGb3IuVGFyZ2V0LmZvckVhY2goKHVzZXIpID0+IHtcclxuICAgICAgICAgICAgaWYgKHVzZXIuTmFtZSA9PT0gUm9sZUNvbnN0YW50cy5QUk9KRUNUX01BTkFHRVIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNQTSA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICAgICAgICB1c2VyLk5hbWUuaW5jbHVkZXMoUm9sZUNvbnN0YW50cy5QUk9KRUNUX01FTUJFUikgfHxcclxuICAgICAgICAgICAgICAgIHVzZXIuTmFtZS5pbmNsdWRlcyhSb2xlQ29uc3RhbnRzLlBST0pFQ1RfQVBQUk9WRVIpIHx8XHJcbiAgICAgICAgICAgICAgICB1c2VyLk5hbWUuaW5jbHVkZXMoUm9sZUNvbnN0YW50cy5QUk9KRUNUX1JFVklFV0VSKVxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNVc2VyID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuRW1haWxFdmVudEZvcm1Hcm91cCA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgICAgICBlbWFpbFByZWZlcmVuY2U6IG5ldyBGb3JtQ29udHJvbCgpLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMudXNlcklEID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKCk7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnNob3coKTtcclxuICAgICAgICB0aGlzLmVtYWlsUHJlZmVyZW5jZXModGhpcy5pc1BNLCB0aGlzLmlzVXNlcik7XHJcbiAgICB9XHJcbn1cclxuIl19