import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
var CustomSearchFieldComponent = /** @class */ (function () {
    function CustomSearchFieldComponent() {
        this.valueChange = new EventEmitter();
        this.selectedOption = new EventEmitter();
        this.details = new EventEmitter();
        this.selectedValue = {
            id: '',
            value: ''
        };
    }
    CustomSearchFieldComponent.prototype.filterUser = function (value) {
        var filterValue = value;
        try {
            filterValue = value.toLowerCase();
        }
        catch (exception) {
            filterValue = value;
        }
        if (this.searchFieldConfig.filterOptions) {
            return this.searchFieldConfig.filterOptions.filter(function (option) {
                return option.displayName && option.displayName.toLowerCase().indexOf(filterValue) >= 0;
            });
        }
        else {
            return [];
        }
    };
    CustomSearchFieldComponent.prototype.displayFn = function (user) {
        return user ? user.displayName : undefined;
    };
    CustomSearchFieldComponent.prototype.onFocusOut = function (event) {
        if (event.target.value.trim() === '' &&
            (!this.userFormControl || !this.userFormControl.errors || (this.userFormControl.errors && !this.userFormControl.errors.required))) {
            return;
        }
        else {
            if (this.searchFieldConfig.filterOptions) {
                if (!this.searchFieldConfig.filterOptions.some(function (e) { return e.displayName === event.target.value; })) {
                    this.userFormControl.setErrors({ incorrect: true });
                }
                else {
                    this.userFormControl.setErrors(null);
                }
            }
        }
    };
    CustomSearchFieldComponent.prototype.changeAutoComplete = function (event) {
        var _a;
        (_a = this.userFormControl) === null || _a === void 0 ? void 0 : _a.setErrors(null);
        this.selectedValue.id = event.option.value.value;
        this.selectedValue.value = event.option.value.displayName;
        this.valueChange.next(this.selectedValue);
        this.selectedOption.next(event.option._selected);
    };
    CustomSearchFieldComponent.prototype.refreshSearchFields = function () {
        var _this = this;
        if (this.searchFieldConfig) {
            if (this.searchFieldConfig.formControl) {
                this.userFormControl = this.searchFieldConfig.formControl;
            }
            else {
                this.userFormControl = new FormControl();
            }
            if (this.userFormControl && this.userFormControl.errors && this.userFormControl.errors.required) {
                this.requiredField = true;
            }
            else {
                this.requiredField = false;
            }
            if (this.searchFieldConfig.filterOptions && this.searchFieldConfig.filterOptions.length &&
                this.searchFieldConfig.filterOptions.length > 0) {
                this.filterOptions = this.userFormControl.valueChanges
                    .pipe(startWith(''), map(function (value) {
                    _this.searchFieldConfig.filterOptions.map(function (option) {
                        if (option.value === value) {
                            _this.selectedValue.value = option.displayName;
                            _this.selectedValue.id = option.value;
                        }
                    });
                    return _this.filterUser(value);
                }));
            }
        }
        if (this.searchFieldConfig.filterOptions) {
            this.searchFieldConfig.filterOptions.map(function (option) {
                if (option.value === _this.userFormControl.value) {
                    _this.selectedValue.value = option.displayName;
                    _this.selectedValue.id = option.value;
                }
            });
        }
        if (this.userFormControl && this.userFormControl.status === 'DISABLED') {
            this.userFormControl.disable();
        }
    };
    CustomSearchFieldComponent.prototype.getDetails = function (name) {
        this.details.next(name);
    };
    CustomSearchFieldComponent.prototype.ngOnInit = function () {
        this.refreshSearchFields();
    };
    __decorate([
        Input()
    ], CustomSearchFieldComponent.prototype, "searchFieldConfig", void 0);
    __decorate([
        Input()
    ], CustomSearchFieldComponent.prototype, "required", void 0);
    __decorate([
        Output()
    ], CustomSearchFieldComponent.prototype, "valueChange", void 0);
    __decorate([
        Output()
    ], CustomSearchFieldComponent.prototype, "selectedOption", void 0);
    __decorate([
        Output()
    ], CustomSearchFieldComponent.prototype, "details", void 0);
    CustomSearchFieldComponent = __decorate([
        Component({
            selector: 'mpm-custom-search-field',
            template: "<mat-form-field appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\" (keyup)=\"getDetails($event)\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"userFormControl\" (focusout)=\"onFocusOut($event)\"\r\n        [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <div #optionposition\r\n                [matTooltip]=\"spanposition.getBoundingClientRect().width > optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                <span #spanposition\r\n                    [matTooltip]=\"spanposition.getBoundingClientRect().width <= optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                    {{option.displayName}}\r\n                </span>\r\n            </div>\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n    <mat-error *ngIf=\"userFormControl.hasError('incorrect')\">No Matches!</mat-error>\r\n</mat-form-field>",
            styles: ["mat-form-field{width:100%}"]
        })
    ], CustomSearchFieldComponent);
    return CustomSearchFieldComponent;
}());
export { CustomSearchFieldComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLXNlYXJjaC1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9jdXN0b20tc2VhcmNoLWZpZWxkL2N1c3RvbS1zZWFyY2gtZmllbGQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBU2hEO0lBaUJJO1FBYlUsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBQ3RDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUV6QyxZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUs1QyxrQkFBYSxHQUFHO1lBQ1osRUFBRSxFQUFFLEVBQUU7WUFDTixLQUFLLEVBQUUsRUFBRTtTQUNaLENBQUM7SUFFYyxDQUFDO0lBRVYsK0NBQVUsR0FBakIsVUFBa0IsS0FBYTtRQUMzQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSTtZQUNBLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDckM7UUFBQyxPQUFPLFNBQVMsRUFBRTtZQUNoQixXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFO1lBQ3RDLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBQSxNQUFNO2dCQUNyRCxPQUFPLE1BQU0sQ0FBQyxXQUFXLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVGLENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFFTCxDQUFDO0lBRUQsOENBQVMsR0FBVCxVQUFVLElBQVU7UUFDaEIsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztJQUMvQyxDQUFDO0lBRUQsK0NBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUU7WUFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRTtZQUNuSSxPQUFPO1NBQ1Y7YUFBTTtZQUNILElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFdBQVcsS0FBSyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBcEMsQ0FBb0MsQ0FBQyxFQUFFO29CQUN2RixJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2lCQUN2RDtxQkFBTTtvQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDeEM7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUVELHVEQUFrQixHQUFsQixVQUFtQixLQUFLOztRQUNwQixNQUFBLElBQUksQ0FBQyxlQUFlLDBDQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUU7UUFDdEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2pELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztRQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsd0RBQW1CLEdBQW5CO1FBQUEsaUJBMENDO1FBekNHLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3hCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDO2FBQzdEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQzthQUM1QztZQUNELElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQzdGLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO2FBQzdCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzlCO1lBQ0QsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsTUFBTTtnQkFDbkYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWTtxQkFDakQsSUFBSSxDQUNELFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixHQUFHLENBQUMsVUFBQSxLQUFLO29CQUNMLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFVBQUEsTUFBTTt3QkFDM0MsSUFBSSxNQUFNLENBQUMsS0FBSyxLQUFLLEtBQUssRUFBRTs0QkFDeEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQzs0QkFDOUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzt5QkFDeEM7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsQyxDQUFDLENBQUMsQ0FDTCxDQUFDO2FBQ1Q7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRTtZQUN0QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU07Z0JBQzNDLElBQUksTUFBTSxDQUFDLEtBQUssS0FBSyxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRTtvQkFDN0MsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztvQkFDOUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztpQkFDeEM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtZQUNwRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xDO0lBQ0wsQ0FBQztJQUVELCtDQUFVLEdBQVYsVUFBVyxJQUFJO1FBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUNELDZDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBOUdRO1FBQVIsS0FBSyxFQUFFO3lFQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTtnRUFBVTtJQUNSO1FBQVQsTUFBTSxFQUFFO21FQUF1QztJQUN0QztRQUFULE1BQU0sRUFBRTtzRUFBMEM7SUFFekM7UUFBVCxNQUFNLEVBQUU7K0RBQW1DO0lBUG5DLDBCQUEwQjtRQU50QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLHl2Q0FBbUQ7O1NBRXRELENBQUM7T0FFVywwQkFBMEIsQ0FrSHRDO0lBQUQsaUNBQUM7Q0FBQSxBQWxIRCxJQWtIQztTQWxIWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBtYXAsIHN0YXJ0V2l0aCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1jdXN0b20tc2VhcmNoLWZpZWxkJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jdXN0b20tc2VhcmNoLWZpZWxkLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2N1c3RvbS1zZWFyY2gtZmllbGQuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEN1c3RvbVNlYXJjaEZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBzZWFyY2hGaWVsZENvbmZpZztcclxuICAgIEBJbnB1dCgpIHJlcXVpcmVkO1xyXG4gICAgQE91dHB1dCgpIHZhbHVlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgICBAT3V0cHV0KCkgc2VsZWN0ZWRPcHRpb24gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAT3V0cHV0KCkgZGV0YWlscyA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIHVzZXJGb3JtQ29udHJvbDogRm9ybUNvbnRyb2w7XHJcbiAgICBmaWx0ZXJPcHRpb25zOiBPYnNlcnZhYmxlPGFueVtdPjtcclxuICAgIHJlcXVpcmVkRmllbGQ6IGJvb2xlYW47XHJcbiAgICBzZWxlY3RlZFZhbHVlID0ge1xyXG4gICAgICAgIGlkOiAnJyxcclxuICAgICAgICB2YWx1ZTogJydcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICBwdWJsaWMgZmlsdGVyVXNlcih2YWx1ZTogc3RyaW5nKTogc3RyaW5nW10ge1xyXG4gICAgICAgIGxldCBmaWx0ZXJWYWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGZpbHRlclZhbHVlID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHtcclxuICAgICAgICAgICAgZmlsdGVyVmFsdWUgPSB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmZpbHRlcihvcHRpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9wdGlvbi5kaXNwbGF5TmFtZSAmJiBvcHRpb24uZGlzcGxheU5hbWUudG9Mb3dlckNhc2UoKS5pbmRleE9mKGZpbHRlclZhbHVlKSA+PSAwO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gW107XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBkaXNwbGF5Rm4odXNlcj86IGFueSk6IHN0cmluZyB8IHVuZGVmaW5lZCB7XHJcbiAgICAgICAgcmV0dXJuIHVzZXIgPyB1c2VyLmRpc3BsYXlOYW1lIDogdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRm9jdXNPdXQoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQudGFyZ2V0LnZhbHVlLnRyaW0oKSA9PT0gJycgJiZcclxuICAgICAgICAgICAgKCF0aGlzLnVzZXJGb3JtQ29udHJvbCB8fCAhdGhpcy51c2VyRm9ybUNvbnRyb2wuZXJyb3JzIHx8ICh0aGlzLnVzZXJGb3JtQ29udHJvbC5lcnJvcnMgJiYgIXRoaXMudXNlckZvcm1Db250cm9sLmVycm9ycy5yZXF1aXJlZCkpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucy5zb21lKGUgPT4gZS5kaXNwbGF5TmFtZSA9PT0gZXZlbnQudGFyZ2V0LnZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXNlckZvcm1Db250cm9sLnNldEVycm9ycyh7IGluY29ycmVjdDogdHJ1ZSB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRm9ybUNvbnRyb2wuc2V0RXJyb3JzKG51bGwpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZUF1dG9Db21wbGV0ZShldmVudCkge1xyXG4gICAgICAgIHRoaXMudXNlckZvcm1Db250cm9sPy5zZXRFcnJvcnMobnVsbCk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLmlkID0gZXZlbnQub3B0aW9uLnZhbHVlLnZhbHVlO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSA9IGV2ZW50Lm9wdGlvbi52YWx1ZS5kaXNwbGF5TmFtZTtcclxuICAgICAgICB0aGlzLnZhbHVlQ2hhbmdlLm5leHQodGhpcy5zZWxlY3RlZFZhbHVlKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3B0aW9uLm5leHQoZXZlbnQub3B0aW9uLl9zZWxlY3RlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaFNlYXJjaEZpZWxkcygpIHtcclxuICAgICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZy5mb3JtQ29udHJvbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VyRm9ybUNvbnRyb2wgPSB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZvcm1Db250cm9sO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VyRm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy51c2VyRm9ybUNvbnRyb2wgJiYgdGhpcy51c2VyRm9ybUNvbnRyb2wuZXJyb3JzICYmIHRoaXMudXNlckZvcm1Db250cm9sLmVycm9ycy5yZXF1aXJlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXF1aXJlZEZpZWxkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVxdWlyZWRGaWVsZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMgJiYgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmxlbmd0aCAmJlxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlsdGVyT3B0aW9ucyA9IHRoaXMudXNlckZvcm1Db250cm9sLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAgICAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGFydFdpdGgoJycpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXAodmFsdWUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLm1hcChvcHRpb24gPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb24udmFsdWUgPT09IHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSA9IG9wdGlvbi5kaXNwbGF5TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLmlkID0gb3B0aW9uLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmlsdGVyVXNlcih2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucykge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9uLnZhbHVlID09PSB0aGlzLnVzZXJGb3JtQ29udHJvbC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSA9IG9wdGlvbi5kaXNwbGF5TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmFsdWUuaWQgPSBvcHRpb24udmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMudXNlckZvcm1Db250cm9sICYmIHRoaXMudXNlckZvcm1Db250cm9sLnN0YXR1cyA9PT0gJ0RJU0FCTEVEJykge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJGb3JtQ29udHJvbC5kaXNhYmxlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldERldGFpbHMobmFtZSkge1xyXG4gICAgICAgIHRoaXMuZGV0YWlscy5uZXh0KG5hbWUpO1xyXG4gICAgfVxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5yZWZyZXNoU2VhcmNoRmllbGRzKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==