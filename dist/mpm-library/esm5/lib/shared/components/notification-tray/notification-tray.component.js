import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { UtilService } from '../../../mpm-utils/services/util.service';
import * as moment from 'moment';
import { LoaderService } from '../../../loader/loader.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { DatePipe } from '@angular/common';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../notification/notification.service';
import { TaskFilterTypes } from '../../constants/taskListfilters/TaskFilterTypes';
import { TaskListDependentFilterTypes } from '../../constants/taskListfilters/TaskListDependentFilterTypes';
import { GroupByFilterTypes } from '../../constants/GroupByFilterTypes';
import { MPM_LEVELS } from '../../../mpm-utils/objects/Level';
import { MenuConstants } from '../../constants/menu.constants';
var NotificationTrayComponent = /** @class */ (function () {
    function NotificationTrayComponent(utilService, loaderService, appService, datePipe, sharingService, router, notificationService) {
        this.utilService = utilService;
        this.loaderService = loaderService;
        this.appService = appService;
        this.datePipe = datePipe;
        this.sharingService = sharingService;
        this.router = router;
        this.notificationService = notificationService;
        this.notifiedCount = 0;
        this.notificationData = [];
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.skip = 0;
        this.top = 10;
        this.canLoadMore = true;
    }
    NotificationTrayComponent.prototype.ngOnChanges = function (changes) {
        // throw new Error('Method not implemented.');
    };
    /* @HostListener('scroll', [])
    onScroll(): void {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        this.skip = this.skip + this.top;
        this.loadData();
      }
    } */
    /* @HostListener('scroll', ['$event']) onScrollEvent(eventData) {
      // this.myScrollContainer.nativeElement.scrollTop <= 1000
      if (false) {
        this.skip = this.skip + this.top;
        this.loadData();
      }
    }
  
    scrolling = (el): void => {
      const scrollTop = el.srcElement.scrollTop;
      if (scrollTop >= 100) {
        // this.scrolled = true;
      } else {
        // this.scrolled = false;
      }
    } */
    NotificationTrayComponent.prototype.setNotificationData = function () {
        var _this = this;
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.hasUnreadNotification = false;
        this.notificationData.forEach(function (content) {
            if (_this.utilService.isNullOrEmpty(content.NAVIGATION_URL)) {
                _this.formNavigationUrl(content);
            }
            if (content.IS_READ === 'false') {
                _this.hasUnreadNotification = true;
            }
            var yesterdayDate = new Date();
            yesterdayDate.setDate(yesterdayDate.getDate() - 1);
            if (!(_this.utilService.isNullOrEmpty(content.TITLE) || _this.utilService.isNullOrEmpty(content.DESCRIPTION))) {
                if (_this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                    _this.todayNotification.push(content);
                }
                else if (_this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === _this.datePipe.transform(yesterdayDate, 'yyyy-MM-dd')) {
                    _this.yesterdayNotification.push(content);
                }
                else {
                    _this.pastNotification.push(content);
                }
            }
        });
    };
    NotificationTrayComponent.prototype.getDaysBetweenTwoDates = function (startDate, endDate, diffType) {
        var start = moment(startDate);
        var end = moment(endDate);
        return start.diff(end, diffType, true);
    };
    NotificationTrayComponent.prototype.getDateByGiveFormat = function (dateObject, formatValue) {
        var dateMoment = moment(dateObject);
        return dateMoment.format(formatValue);
    };
    NotificationTrayComponent.prototype.handleDateManipulation = function (dateString) {
        if (!dateString) {
            return '';
        }
        if (dateString && !(dateString instanceof Date)) {
            dateString = new Date(dateString);
        }
        var numericValueDiff = 0;
        numericValueDiff = this.getDaysBetweenTwoDates(new Date(), dateString, 'day');
        if (numericValueDiff < 1) {
            numericValueDiff = this.getDaysBetweenTwoDates(new Date(), dateString, 'hours');
            if (numericValueDiff < 1) {
                return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'minutes')) + ' mins ago';
            }
            else {
                return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'hours')) + ' hours ago';
            }
        }
        else {
            return Math.floor(this.getDaysBetweenTwoDates(new Date(), dateString, 'day')) + ' days ago';
        }
        // return this.getDateByGiveFormat(dateString, 'MMM D Y, h:mm a');
    };
    NotificationTrayComponent.prototype.formNavigationUrl = function (notificationContent) {
        var baseUrl;
        var navigationUrl;
        var emailConfig = this.sharingService.getDefaultEmailConfig();
        if (emailConfig) {
            baseUrl = emailConfig.APPLICATION_URL;
        }
        else {
            this.notificationService.error('Application Url is not configured');
            return;
        }
        if (this.sharingService.isManager) {
            if (notificationContent.R_PO_PROJECT && notificationContent.R_PO_PROJECT['Project-id'] && notificationContent.R_PO_PROJECT['Project-id'].Id) {
                if (notificationContent.EVENT_TYPE === MPM_LEVELS.PROJECT) {
                    navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.OVERVIEW + '?menuName=' + MenuConstants.OVERVIEW;
                }
                else if (notificationContent.EVENT_TYPE === MPM_LEVELS.TASK) {
                    if (notificationContent.R_PO_TASK && notificationContent.R_PO_TASK['Task-id'] && notificationContent.R_PO_TASK['Task-id'].Id) {
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '/' +
                            notificationContent.R_PO_TASK['Task-id'].Id + '?menuName=' + MenuConstants.DELIVERABLES;
                    }
                    else {
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '?menuName=' + MenuConstants.TASKS;
                    }
                }
                else if (notificationContent.EVENT_TYPE === MPM_LEVELS.DELIVERABLE && notificationContent.R_PO_TASK && notificationContent.R_PO_TASK['Task-id']) {
                    navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.TASKS + '/' +
                        notificationContent.R_PO_TASK['Task-id'].Id + '?menuName=' + MenuConstants.DELIVERABLES;
                }
                else if (notificationContent.EVENT_TYPE === 'COMMENT') {
                    if (notificationContent.R_PO_DELIVERABLE != undefined)
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.COMMENTS + '?menuName=' + MenuConstants.COMMENTS + '&deliverableId=' + notificationContent.R_PO_DELIVERABLE['Deliverable-id'].Id;
                    else
                        navigationUrl = baseUrl + '/project/' + notificationContent.R_PO_PROJECT['Project-id'].Id + '/' + MenuConstants.COMMENTS + '?menuName=' + MenuConstants.COMMENTS;
                }
                else {
                    navigationUrl = baseUrl;
                }
            }
        }
        else {
            navigationUrl = baseUrl + '/deliverableWorkView?menuName=deliverableWorkView';
            if (notificationContent.ASSIGNMENT_TYPE === 'ROLE') {
                navigationUrl = navigationUrl + '&' + 'groupByFilter' + '=' + GroupByFilterTypes.GROUP_BY_PROJECT + '&' +
                    'deliverableTaskFilter' + '=' + TaskFilterTypes.MY_TEAM_TASKS + '&' + 'listDependentFilter' + '=' + TaskListDependentFilterTypes.ALL_DELIVERABLE_TYPES + '&' +
                    'emailLinkId' + '=' + notificationContent.GUID;
            }
            else {
                navigationUrl = navigationUrl + '&' + 'groupByFilter' + '=' + GroupByFilterTypes.GROUP_BY_PROJECT + '&' +
                    'deliverableTaskFilter' + '=' + TaskFilterTypes.MY_TASKS + '&' + 'listDependentFilter' + '=' + TaskListDependentFilterTypes.TO_DO_FILTER + '&' +
                    'emailLinkId' + '=' + notificationContent.GUID;
            }
        }
        if (navigationUrl) {
            var parameters = {
                'Notification-id': {
                    Id: notificationContent['Notification-id'].Id
                },
                'Notification-update': {
                    NAVIGATION_URL: navigationUrl
                }
            };
            this.appService.updateNotification(parameters).subscribe();
            notificationContent.NAVIGATION_URL = navigationUrl;
        }
    };
    NotificationTrayComponent.prototype.clearNotification = function (notificationContent) {
        var _this = this;
        this.hasUnreadNotification = false;
        var parameters = {
            'Notification-id': {
                Id: notificationContent['Notification-id'].Id
            },
            'Notification-update': {
                IS_DELETED: 'true'
            }
        };
        this.appService.updateNotification(parameters).subscribe();
        var selectedNotificationIndex = this.notificationData.findIndex(function (data) {
            return data['Notification-id'].Id === notificationContent['Notification-id'].Id;
        });
        if (selectedNotificationIndex >= 0) {
            this.notificationData.splice(selectedNotificationIndex, 1);
        }
        var selectedPastNotificationIndex = this.pastNotification.findIndex(function (data) {
            return data['Notification-id'].Id === notificationContent['Notification-id'].Id;
        });
        if (selectedPastNotificationIndex >= 0) {
            this.pastNotification.splice(selectedPastNotificationIndex, 1);
        }
        var selectedTodayNotificationIndex = this.todayNotification.findIndex(function (data) {
            return data['Notification-id'].Id === notificationContent['Notification-id'].Id;
        });
        if (selectedTodayNotificationIndex >= 0) {
            this.todayNotification.splice(selectedTodayNotificationIndex, 1);
        }
        var selectedYesterdayNotificationIndex = this.yesterdayNotification.findIndex(function (data) {
            return data['Notification-id'].Id === notificationContent['Notification-id'].Id;
        });
        if (selectedYesterdayNotificationIndex >= 0) {
            this.yesterdayNotification.splice(selectedYesterdayNotificationIndex, 1);
        }
        this.notificationData.forEach(function (content) {
            if (content.IS_READ === 'false') {
                _this.hasUnreadNotification = true;
            }
        });
    };
    NotificationTrayComponent.prototype.markReadUnreadNotification = function (notificationContent) {
        var _this = this;
        this.hasUnreadNotification = false;
        var parameters = {
            'Notification-id': {
                Id: notificationContent['Notification-id'].Id
            },
            'Notification-update': {
                IS_READ: notificationContent.IS_READ === 'true' ? 'false' : 'true'
            }
        };
        this.appService.updateNotification(parameters).subscribe();
        notificationContent.IS_READ = notificationContent.IS_READ === 'true' ? 'false' : 'true';
        this.notificationData.forEach(function (content) {
            if (content.IS_READ === 'false') {
                _this.hasUnreadNotification = true;
            }
        });
    };
    NotificationTrayComponent.prototype.markAsReadNotification = function (notificationContent) {
        var _this = this;
        this.sharingService.getNotificationDetails(notificationContent);
        if (notificationContent.IS_READ === 'false') {
            this.hasUnreadNotification = false;
            var parameters = {
                'Notification-id': {
                    Id: notificationContent['Notification-id'].Id
                },
                'Notification-update': {
                    IS_READ: 'true'
                }
            };
            this.appService.updateNotification(parameters).subscribe();
            notificationContent.IS_READ = 'true';
            this.notificationData.forEach(function (content) {
                if (content.IS_READ === 'false') {
                    _this.hasUnreadNotification = true;
                }
            });
        }
        return true;
    };
    NotificationTrayComponent.prototype.clearAllNotification = function () {
        var _this = this;
        this.notificationData.forEach(function (data) {
            if (data.IS_DELETED === 'false') {
                var parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_DELETED: 'true'
                    }
                };
                _this.appService.updateNotification(parameters).subscribe();
            }
        });
        this.hasUnreadNotification = false;
        this.notificationData = [];
        this.todayNotification = [];
        this.yesterdayNotification = [];
        this.pastNotification = [];
        this.canLoadMore = false;
    };
    NotificationTrayComponent.prototype.markAllAsReadNotification = function () {
        var _this = this;
        this.hasUnreadNotification = false;
        this.notificationData.forEach(function (data) {
            if (data.IS_READ === 'false') {
                var parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_READ: 'true'
                    }
                };
                _this.appService.updateNotification(parameters).subscribe();
                data.IS_READ = 'true';
            }
        });
        this.setNotificationData();
    };
    NotificationTrayComponent.prototype.updateNotifiedCount = function () {
        var _this = this;
        this.notifiedCount = 0;
        this.notificationData.forEach(function (data) {
            if (data.IS_NOTIFIED === 'false') {
                var parameters = {
                    'Notification-id': {
                        Id: data['Notification-id'].Id
                    },
                    'Notification-update': {
                        IS_NOTIFIED: 'true'
                    }
                };
                _this.appService.updateNotification(parameters).subscribe();
                data.IS_NOTIFIED = 'true';
            }
        });
    };
    NotificationTrayComponent.prototype.loadData = function () {
        var _this = this;
        this.skip = this.skip + this.top;
        this.loaderService.show();
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(function (response) {
            _this.loaderService.hide();
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    response.Notification.forEach(function (notification) {
                        var existingNotification = _this.notificationData.find(function (data) { return data['Notification-id'].Id === notification['Notification-id'].Id; });
                        if (!existingNotification) {
                            _this.notificationData.push(notification);
                        }
                    });
                    if (response.Notification.length < _this.top) {
                        _this.canLoadMore = false;
                    }
                }
                else {
                    var existingNotification = _this.notificationData.find(function (data) { return data['Notification-id'].Id === response.Notification['Notification-id'].Id; });
                    if (!existingNotification) {
                        _this.notificationData.push(response.Notification);
                    }
                    _this.canLoadMore = false;
                }
            }
            else {
                _this.canLoadMore = false;
            }
            _this.updateNotifiedCount();
            _this.setNotificationData();
        });
    };
    NotificationTrayComponent.prototype.refresh = function () {
        var _this = this;
        this.loaderService.show();
        this.skip = 0;
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(function (response) {
            _this.loaderService.hide();
            _this.canLoadMore = true;
            _this.notificationData = [];
            _this.todayNotification = [];
            _this.yesterdayNotification = [];
            _this.pastNotification = [];
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    _this.notificationData = response.Notification;
                    if (response.Notification.length < _this.top) {
                        _this.canLoadMore = false;
                    }
                }
                else {
                    _this.notificationData = [response.Notification];
                    _this.canLoadMore = false;
                }
            }
            else {
                _this.notificationData = [];
                _this.canLoadMore = false;
            }
            _this.updateNotifiedCount();
            _this.setNotificationData();
        });
    };
    NotificationTrayComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loaderService.show();
        this.appService.getNotificationForCurrentUser(this.skip, this.top).subscribe(function (response) {
            if (response && response.Notification) {
                if (Array.isArray(response.Notification)) {
                    _this.notificationData = response.Notification;
                    if (response.Notification.length < _this.top) {
                        _this.canLoadMore = false;
                    }
                }
                else {
                    _this.notificationData = [response.Notification];
                    _this.canLoadMore = false;
                }
            }
            else {
                _this.notificationData = [];
                _this.canLoadMore = false;
            }
            _this.notificationData.forEach(function (content) {
                if (_this.utilService.isNullOrEmpty(content.NAVIGATION_URL)) {
                    _this.formNavigationUrl(content);
                }
                if (content.IS_READ === 'false') {
                    _this.hasUnreadNotification = true;
                }
                if (content.IS_NOTIFIED === 'false') {
                    _this.notifiedCount = _this.notifiedCount + 1;
                }
                var yesterdayDate = new Date();
                yesterdayDate.setDate(yesterdayDate.getDate() - 1);
                if (!(_this.utilService.isNullOrEmpty(content.TITLE) || _this.utilService.isNullOrEmpty(content.DESCRIPTION))) {
                    if (_this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === _this.datePipe.transform(new Date(), 'yyyy-MM-dd')) {
                        _this.todayNotification.push(content);
                    }
                    else if (_this.datePipe.transform(content.Tracking.CreatedDate, 'yyyy-MM-dd') === _this.datePipe.transform(yesterdayDate, 'yyyy-MM-dd')) {
                        _this.yesterdayNotification.push(content);
                    }
                    else {
                        _this.pastNotification.push(content);
                    }
                }
            });
        });
    };
    NotificationTrayComponent.ctorParameters = function () { return [
        { type: UtilService },
        { type: LoaderService },
        { type: AppService },
        { type: DatePipe },
        { type: SharingService },
        { type: Router },
        { type: NotificationService }
    ]; };
    NotificationTrayComponent = __decorate([
        Component({
            selector: 'mpm-notification-tray',
            template: "<button class=\"notification-icon\" mat-icon-button (click)=\"updateNotifiedCount()\" [matMenuTriggerFor]=\"notification\"\r\n    matTooltip=\"Notifications\">\r\n    <mat-icon [matBadge]=\"notifiedCount\" [ngClass]=\"{'hide-icon': notifiedCount === 0}\" color=\"primary\">\r\n        notifications\r\n    </mat-icon>\r\n</button>\r\n<mat-menu #notification=\"matMenu\" xPosition=\"before\">\r\n    <div style=\"padding: 8px;\" (click)=\"$event.stopPropagation()\">\r\n        <p class=\"notification-heading\">Notifications&nbsp;<mat-icon class=\"refresh-icon\" matTooltip=\"Refresh\"\r\n                (click)=\"refresh()\">refresh\r\n            </mat-icon>\r\n        </p>\r\n        <hr />\r\n        <div class=\"notification-data\">\r\n            <span *ngIf=\"notificationData.length > 0\" class=\"notification-action\" (click)=\"clearAllNotification()\">Clear\r\n                all</span>\r\n            <span *ngIf=\"hasUnreadNotification\" class=\"notification-action\" (click)=\"markAllAsReadNotification()\">Mark\r\n                all\r\n                as read</span>\r\n            <span *ngIf=\"todayNotification.length > 0\">\r\n                <span class=\"notification-group\">TODAY</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of todayNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <span *ngIf=\"yesterdayNotification.length > 0\">\r\n                <span class=\"notification-group\">YESTERDAY</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of yesterdayNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <br />\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <span *ngIf=\"pastNotification.length > 0\">\r\n                <span class=\"notification-group\">PAST</span>\r\n                <div style=\"padding: 8px;\" *ngFor=\"let content of pastNotification\">\r\n                    <div class=\"notification-group-data\">\r\n                        <span [ngClass]=\"{'unread-notification': content.IS_READ === 'false'}\">\r\n                            <span class=\"notification-title\" matTooltip=\"{{content.TITLE}}\">{{content.TITLE}}</span>\r\n                            <span class=\"notification-action-icon\">\r\n                                <mat-icon (click)=\"clearNotification(content)\" class=\"notification-clear-icon\"\r\n                                    matTooltip=\"Clear notification\">clear</mat-icon>\r\n                                <mat-icon (click)=\"markReadUnreadNotification(content)\" style=\"cursor: pointer;\"\r\n                                    matTooltip=\"{{(content.IS_READ === 'true') ? 'Mark as unread' : 'Mark as read'}}\"\r\n                                    [ngClass]=\"{'notification-unread-icon': (content.IS_READ === 'false'), 'notification-read-icon': (content.IS_READ === 'true')}\">\r\n                                    radio_button_checked</mat-icon>\r\n                            </span>\r\n                            <br />\r\n                            <a href=\"{{content.NAVIGATION_URL}}\" (click)=\"markAsReadNotification(content)\"\r\n                                oncontextmenu=\"return false\" matTooltip=\"{{content.DESCRIPTION}}\"\r\n                                class=\"notification-description\">{{content.DESCRIPTION}}</a>\r\n                            <br />\r\n                            <span class=\"notification-date\"\r\n                                matTooltip=\"{{content.Tracking.CreatedDate | date:'MMM d, y, h:mm a'}}\">{{handleDateManipulation(content.Tracking.CreatedDate)}}</span>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </span>\r\n            <button mat-button class=\"more-btn\" *ngIf=\"notificationData.length > 0 && canLoadMore\"\r\n                (click)=\"loadData()\">Show more...</button>\r\n        </div>\r\n        <div *ngIf=\"notificationData.length === 0\" color=\"accent\" class=\"no-data\">No\r\n            Notifications</div>\r\n    </div>\r\n</mat-menu>",
            styles: [".notification-icon ::ng-deep .mat-badge-content{width:22px!important;border-radius:15px!important}.unread-notification{font-weight:600}::ng-deep .mat-badge.hide-icon .mat-badge-content{display:none}.notification-action-icon{float:right}.notification-action-icon mat-icon{font-size:20px}.notification-group-data .notification-clear-icon{color:transparent;cursor:pointer}.notification-group-data .notification-read-icon{color:transparent}.notification-group-data .notification-description,.notification-group-data .notification-title{width:316px;text-overflow:ellipsis;display:inline-block;overflow:hidden;white-space:nowrap}.notification-group-data .notification-date{font-size:13px;font-weight:600}::ng-deep div.mat-menu-panel{min-width:112px;max-width:500px}.notification-heading{font-size:24px;font-weight:600;margin-bottom:16px}.notification-group{padding:8px;font-size:14px;font-weight:600}.refresh-icon{cursor:pointer;vertical-align:middle}.notification-action{cursor:pointer;float:right;font-size:12px;margin-right:4px}.notification-action:hover{text-decoration:underline}.notification-data{max-height:570px;overflow:auto}.notification-data .more-btn{float:right;font-weight:600;margin-right:8px}.no-data{font-weight:600;width:400px;text-align:center;margin:8px 8px 0}.notification-group-data{padding:12px}"]
        })
    ], NotificationTrayComponent);
    return NotificationTrayComponent;
}());
export { NotificationTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbm90aWZpY2F0aW9uLXRyYXkvbm90aWZpY2F0aW9uLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFvQyxNQUFNLGVBQWUsQ0FBQztBQUM1RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFFdkUsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7QUFDakMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saURBQWlELENBQUM7QUFDbEYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDNUcsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU8vRDtJQVdFLG1DQUNTLFdBQXdCLEVBQ3hCLGFBQTRCLEVBQzVCLFVBQXNCLEVBQ3RCLFFBQWtCLEVBQ2xCLGNBQThCLEVBQzlCLE1BQWMsRUFDZCxtQkFBd0M7UUFOeEMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2Qsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQWhCakQsa0JBQWEsR0FBRyxDQUFDLENBQUM7UUFDbEIscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUN2QiwwQkFBcUIsR0FBRyxFQUFFLENBQUM7UUFDM0IscUJBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRXRCLFNBQUksR0FBRyxDQUFDLENBQUM7UUFDVCxRQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ1QsZ0JBQVcsR0FBRyxJQUFJLENBQUM7SUFTZixDQUFDO0lBQ0wsK0NBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQ2hDLDhDQUE4QztJQUNoRCxDQUFDO0lBRUQ7Ozs7OztRQU1JO0lBQ0o7Ozs7Ozs7Ozs7Ozs7OztRQWVJO0lBRUosdURBQW1CLEdBQW5CO1FBQUEsaUJBd0JDO1FBdkJDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDbkMsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQzFELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNqQztZQUNELElBQUksT0FBTyxDQUFDLE9BQU8sS0FBSyxPQUFPLEVBQUU7Z0JBQy9CLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDbkM7WUFDRCxJQUFNLGFBQWEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO1lBQ2pDLGFBQWEsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ25ELElBQUksQ0FBQyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRTtnQkFDM0csSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUMsS0FBSyxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFlBQVksQ0FBQyxFQUFFO29CQUM3SCxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN0QztxQkFBTSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsRUFBRTtvQkFDdkksS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDMUM7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDckM7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDBEQUFzQixHQUF0QixVQUF1QixTQUFlLEVBQUUsT0FBYSxFQUFFLFFBQWlDO1FBQ3RGLElBQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoQyxJQUFNLEdBQUcsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDNUIsT0FBTyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELHVEQUFtQixHQUFuQixVQUFvQixVQUFnQixFQUFFLFdBQW1CO1FBQ3ZELElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0QyxPQUFPLFVBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVELDBEQUFzQixHQUF0QixVQUF1QixVQUFVO1FBQy9CLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDZixPQUFPLEVBQUUsQ0FBQztTQUNYO1FBQ0QsSUFBSSxVQUFVLElBQUksQ0FBQyxDQUFDLFVBQVUsWUFBWSxJQUFJLENBQUMsRUFBRTtZQUMvQyxVQUFVLEdBQUcsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbkM7UUFDRCxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUN6QixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDOUUsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEVBQUU7WUFDeEIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2hGLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDLEdBQUcsV0FBVyxDQUFDO2FBQ2pHO2lCQUFNO2dCQUNMLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUM7YUFDaEc7U0FDRjthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQztTQUM3RjtRQUNELGtFQUFrRTtJQUNwRSxDQUFDO0lBRUQscURBQWlCLEdBQWpCLFVBQWtCLG1CQUFtQjtRQUNuQyxJQUFJLE9BQU8sQ0FBQztRQUNaLElBQUksYUFBYSxDQUFDO1FBQ2xCLElBQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUNoRSxJQUFJLFdBQVcsRUFBRTtZQUNmLE9BQU8sR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDO1NBRXZDO2FBQU07WUFDTCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDcEUsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRTtZQUNqQyxJQUFJLG1CQUFtQixDQUFDLFlBQVksSUFBSSxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLElBQUksbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFFM0ksSUFBSSxtQkFBbUIsQ0FBQyxVQUFVLEtBQUssVUFBVSxDQUFDLE9BQU8sRUFBRTtvQkFFekQsYUFBYSxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLFFBQVEsR0FBRyxZQUFZLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztpQkFDbEs7cUJBQU0sSUFBSSxtQkFBbUIsQ0FBQyxVQUFVLEtBQUssVUFBVSxDQUFDLElBQUksRUFBRTtvQkFDN0QsSUFBSSxtQkFBbUIsQ0FBQyxTQUFTLElBQUksbUJBQW1CLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUU7d0JBQzVILGFBQWEsR0FBRyxPQUFPLEdBQUcsV0FBVyxHQUFHLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQyxLQUFLLEdBQUcsR0FBRzs0QkFDekgsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxZQUFZLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztxQkFDM0Y7eUJBQU07d0JBQ0wsYUFBYSxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLEtBQUssR0FBRyxZQUFZLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQztxQkFDNUo7aUJBQ0Y7cUJBQU0sSUFBSSxtQkFBbUIsQ0FBQyxVQUFVLEtBQUssVUFBVSxDQUFDLFdBQVcsSUFBSSxtQkFBbUIsQ0FBQyxTQUFTLElBQUksbUJBQW1CLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxFQUFFO29CQUNqSixhQUFhLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsS0FBSyxHQUFHLEdBQUc7d0JBQ3pILG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEdBQUcsWUFBWSxHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUM7aUJBQzNGO3FCQUFNLElBQUksbUJBQW1CLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtvQkFDdkQsSUFBSSxtQkFBbUIsQ0FBQyxnQkFBZ0IsSUFBSSxTQUFTO3dCQUNuRCxhQUFhLEdBQUcsT0FBTyxHQUFHLFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsUUFBUSxHQUFHLFlBQVksR0FBRyxhQUFhLENBQUMsUUFBUSxHQUFHLGlCQUFpQixHQUFHLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDLENBQUMsRUFBRSxDQUFDOzt3QkFFalAsYUFBYSxHQUFHLE9BQU8sR0FBRyxXQUFXLEdBQUcsbUJBQW1CLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLFFBQVEsR0FBRyxZQUFZLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztpQkFDcEs7cUJBQU07b0JBQ0wsYUFBYSxHQUFHLE9BQU8sQ0FBQztpQkFDekI7YUFDRjtTQUNGO2FBQU07WUFFTCxhQUFhLEdBQUcsT0FBTyxHQUFHLG1EQUFtRCxDQUFDO1lBQzlFLElBQUksbUJBQW1CLENBQUMsZUFBZSxLQUFLLE1BQU0sRUFBRTtnQkFFbEQsYUFBYSxHQUFHLGFBQWEsR0FBRyxHQUFHLEdBQUcsZUFBZSxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHO29CQUNyRyx1QkFBdUIsR0FBRyxHQUFHLEdBQUcsZUFBZSxDQUFDLGFBQWEsR0FBRyxHQUFHLEdBQUcscUJBQXFCLEdBQUcsR0FBRyxHQUFHLDRCQUE0QixDQUFDLHFCQUFxQixHQUFHLEdBQUc7b0JBQzVKLGFBQWEsR0FBRyxHQUFHLEdBQUcsbUJBQW1CLENBQUMsSUFBSSxDQUFDO2FBQ2xEO2lCQUFNO2dCQUVMLGFBQWEsR0FBRyxhQUFhLEdBQUcsR0FBRyxHQUFHLGVBQWUsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsZ0JBQWdCLEdBQUcsR0FBRztvQkFDckcsdUJBQXVCLEdBQUcsR0FBRyxHQUFHLGVBQWUsQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLHFCQUFxQixHQUFHLEdBQUcsR0FBRyw0QkFBNEIsQ0FBQyxZQUFZLEdBQUcsR0FBRztvQkFDOUksYUFBYSxHQUFHLEdBQUcsR0FBRyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7YUFDbEQ7U0FDRjtRQUNELElBQUksYUFBYSxFQUFFO1lBRWpCLElBQU0sVUFBVSxHQUFHO2dCQUNqQixpQkFBaUIsRUFBRTtvQkFDakIsRUFBRSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRTtpQkFDOUM7Z0JBQ0QscUJBQXFCLEVBQUU7b0JBQ3JCLGNBQWMsRUFBRSxhQUFhO2lCQUM5QjthQUNGLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzNELG1CQUFtQixDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUM7U0FDcEQ7SUFDSCxDQUFDO0lBRUQscURBQWlCLEdBQWpCLFVBQWtCLG1CQUFtQjtRQUFyQyxpQkFvQ0M7UUFuQ0MsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFNLFVBQVUsR0FBRztZQUNqQixpQkFBaUIsRUFBRTtnQkFDakIsRUFBRSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRTthQUM5QztZQUNELHFCQUFxQixFQUFFO2dCQUNyQixVQUFVLEVBQUUsTUFBTTthQUNuQjtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzNELElBQU0seUJBQXlCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDcEUsT0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO1FBQXhFLENBQXdFLENBQUMsQ0FBQztRQUM1RSxJQUFJLHlCQUF5QixJQUFJLENBQUMsRUFBRTtZQUNsQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLHlCQUF5QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzVEO1FBQ0QsSUFBTSw2QkFBNkIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUN4RSxPQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7UUFBeEUsQ0FBd0UsQ0FBQyxDQUFDO1FBQzVFLElBQUksNkJBQTZCLElBQUksQ0FBQyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsNkJBQTZCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDaEU7UUFDRCxJQUFNLDhCQUE4QixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQzFFLE9BQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxLQUFLLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRTtRQUF4RSxDQUF3RSxDQUFDLENBQUM7UUFDNUUsSUFBSSw4QkFBOEIsSUFBSSxDQUFDLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyw4QkFBOEIsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNsRTtRQUNELElBQU0sa0NBQWtDLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDbEYsT0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssbUJBQW1CLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO1FBQXhFLENBQXdFLENBQUMsQ0FBQztRQUM1RSxJQUFJLGtDQUFrQyxJQUFJLENBQUMsRUFBRTtZQUMzQyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLGtDQUFrQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87WUFDbkMsSUFBSSxPQUFPLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtnQkFDL0IsS0FBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzthQUNuQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDhEQUEwQixHQUExQixVQUEyQixtQkFBbUI7UUFBOUMsaUJBaUJDO1FBaEJDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBTSxVQUFVLEdBQUc7WUFDakIsaUJBQWlCLEVBQUU7Z0JBQ2pCLEVBQUUsRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLEVBQUU7YUFDOUM7WUFDRCxxQkFBcUIsRUFBRTtnQkFDckIsT0FBTyxFQUFFLG1CQUFtQixDQUFDLE9BQU8sS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBTTthQUNuRTtTQUNGLENBQUM7UUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzNELG1CQUFtQixDQUFDLE9BQU8sR0FBRyxtQkFBbUIsQ0FBQyxPQUFPLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUN4RixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUNuQyxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO2dCQUMvQixLQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2FBQ25DO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsMERBQXNCLEdBQXRCLFVBQXVCLG1CQUFtQjtRQUExQyxpQkFxQkM7UUFwQkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2hFLElBQUksbUJBQW1CLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtZQUMzQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1lBQ25DLElBQU0sVUFBVSxHQUFHO2dCQUNqQixpQkFBaUIsRUFBRTtvQkFDakIsRUFBRSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRTtpQkFDOUM7Z0JBQ0QscUJBQXFCLEVBQUU7b0JBQ3JCLE9BQU8sRUFBRSxNQUFNO2lCQUNoQjthQUNGLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQzNELG1CQUFtQixDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDckMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87Z0JBQ25DLElBQUksT0FBTyxDQUFDLE9BQU8sS0FBSyxPQUFPLEVBQUU7b0JBQy9CLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7aUJBQ25DO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELHdEQUFvQixHQUFwQjtRQUFBLGlCQW9CQztRQW5CQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUNoQyxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssT0FBTyxFQUFFO2dCQUMvQixJQUFNLFVBQVUsR0FBRztvQkFDakIsaUJBQWlCLEVBQUU7d0JBQ2pCLEVBQUUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO3FCQUMvQjtvQkFDRCxxQkFBcUIsRUFBRTt3QkFDckIsVUFBVSxFQUFFLE1BQU07cUJBQ25CO2lCQUNGLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUM1RDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFFRCw2REFBeUIsR0FBekI7UUFBQSxpQkFpQkM7UUFoQkMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUNoQyxJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO2dCQUM1QixJQUFNLFVBQVUsR0FBRztvQkFDakIsaUJBQWlCLEVBQUU7d0JBQ2pCLEVBQUUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO3FCQUMvQjtvQkFDRCxxQkFBcUIsRUFBRTt3QkFDckIsT0FBTyxFQUFFLE1BQU07cUJBQ2hCO2lCQUNGLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCx1REFBbUIsR0FBbkI7UUFBQSxpQkFnQkM7UUFmQyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUNoQyxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssT0FBTyxFQUFFO2dCQUNoQyxJQUFNLFVBQVUsR0FBRztvQkFDakIsaUJBQWlCLEVBQUU7d0JBQ2pCLEVBQUUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO3FCQUMvQjtvQkFDRCxxQkFBcUIsRUFBRTt3QkFDckIsV0FBVyxFQUFFLE1BQU07cUJBQ3BCO2lCQUNGLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7YUFDM0I7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBUSxHQUFSO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ25GLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtnQkFDckMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRTtvQkFDeEMsUUFBUSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBQSxZQUFZO3dCQUN4QyxJQUFNLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEtBQUssWUFBWSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxFQUFqRSxDQUFpRSxDQUFDLENBQUM7d0JBQ25JLElBQUksQ0FBQyxvQkFBb0IsRUFBRTs0QkFDekIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzt5QkFDMUM7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsR0FBRyxFQUFFO3dCQUMzQyxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztxQkFDMUI7aUJBQ0Y7cUJBQU07b0JBQ0wsSUFBTSxvQkFBb0IsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLEVBQTFFLENBQTBFLENBQUMsQ0FBQztvQkFDNUksSUFBSSxDQUFDLG9CQUFvQixFQUFFO3dCQUN6QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztxQkFDbkQ7b0JBQ0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQzFCO2FBQ0Y7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7YUFDMUI7WUFDRCxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztZQUMzQixLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwyQ0FBTyxHQUFQO1FBQUEsaUJBMkJDO1FBMUJDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDbkYsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFDNUIsS0FBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztZQUNoQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLEVBQUU7Z0JBQ3JDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQ3hDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDO29CQUM5QyxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxHQUFHLEVBQUU7d0JBQzNDLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO3FCQUMxQjtpQkFDRjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ2hELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2lCQUMxQjthQUNGO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2FBQzFCO1lBQ0QsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7WUFDM0IsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsNENBQVEsR0FBUjtRQUFBLGlCQXdDQztRQXZDQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNuRixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsWUFBWSxFQUFFO2dCQUNyQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUN4QyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQztvQkFDOUMsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsR0FBRyxFQUFFO3dCQUMzQyxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztxQkFDMUI7aUJBQ0Y7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNoRCxLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDMUI7YUFDRjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO2dCQUMzQixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQzthQUMxQjtZQUNELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO2dCQUNuQyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDMUQsS0FBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNqQztnQkFDRCxJQUFJLE9BQU8sQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO29CQUMvQixLQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2lCQUNuQztnQkFDRCxJQUFJLE9BQU8sQ0FBQyxXQUFXLEtBQUssT0FBTyxFQUFFO29CQUNuQyxLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO2lCQUM3QztnQkFDRCxJQUFNLGFBQWEsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO2dCQUNqQyxhQUFhLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFO29CQUMzRyxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxLQUFLLEtBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxFQUFFLEVBQUUsWUFBWSxDQUFDLEVBQUU7d0JBQzdILEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3RDO3lCQUFNLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLEtBQUssS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxFQUFFO3dCQUN2SSxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUMxQzt5QkFBTTt3QkFDTCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNyQztpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkFoWnFCLFdBQVc7Z0JBQ1QsYUFBYTtnQkFDaEIsVUFBVTtnQkFDWixRQUFRO2dCQUNGLGNBQWM7Z0JBQ3RCLE1BQU07Z0JBQ08sbUJBQW1COztJQWxCdEMseUJBQXlCO1FBTHJDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx1QkFBdUI7WUFDakMsbzZPQUFpRDs7U0FFbEQsQ0FBQztPQUNXLHlCQUF5QixDQTZackM7SUFBRCxnQ0FBQztDQUFBLEFBN1pELElBNlpDO1NBN1pZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25DaGFuZ2VzLCBPbkluaXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgKiBhcyBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2xvYWRlci9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUYXNrRmlsdGVyVHlwZXMgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvdGFza0xpc3RmaWx0ZXJzL1Rhc2tGaWx0ZXJUeXBlcyc7XHJcbmltcG9ydCB7IFRhc2tMaXN0RGVwZW5kZW50RmlsdGVyVHlwZXMgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvdGFza0xpc3RmaWx0ZXJzL1Rhc2tMaXN0RGVwZW5kZW50RmlsdGVyVHlwZXMnO1xyXG5pbXBvcnQgeyBHcm91cEJ5RmlsdGVyVHlwZXMgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvR3JvdXBCeUZpbHRlclR5cGVzJztcclxuaW1wb3J0IHsgTVBNX0xFVkVMUyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgTWVudUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9tZW51LmNvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1ub3RpZmljYXRpb24tdHJheScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbi10cmF5LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9ub3RpZmljYXRpb24tdHJheS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25UcmF5Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICBub3RpZmllZENvdW50ID0gMDtcclxuICBub3RpZmljYXRpb25EYXRhID0gW107XHJcbiAgdG9kYXlOb3RpZmljYXRpb24gPSBbXTtcclxuICB5ZXN0ZXJkYXlOb3RpZmljYXRpb24gPSBbXTtcclxuICBwYXN0Tm90aWZpY2F0aW9uID0gW107XHJcbiAgaGFzVW5yZWFkTm90aWZpY2F0aW9uO1xyXG4gIHNraXAgPSAwO1xyXG4gIHRvcCA9IDEwO1xyXG4gIGNhbkxvYWRNb3JlID0gdHJ1ZTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBhcHBTZXJ2aWNlOiBBcHBTZXJ2aWNlLFxyXG4gICAgcHVibGljIGRhdGVQaXBlOiBEYXRlUGlwZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICkgeyB9XHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgLy8gdGhyb3cgbmV3IEVycm9yKCdNZXRob2Qgbm90IGltcGxlbWVudGVkLicpO1xyXG4gIH1cclxuXHJcbiAgLyogQEhvc3RMaXN0ZW5lcignc2Nyb2xsJywgW10pXHJcbiAgb25TY3JvbGwoKTogdm9pZCB7XHJcbiAgICBpZiAoKHdpbmRvdy5pbm5lckhlaWdodCArIHdpbmRvdy5zY3JvbGxZKSA+PSBkb2N1bWVudC5ib2R5Lm9mZnNldEhlaWdodCkge1xyXG4gICAgICB0aGlzLnNraXAgPSB0aGlzLnNraXAgKyB0aGlzLnRvcDtcclxuICAgICAgdGhpcy5sb2FkRGF0YSgpO1xyXG4gICAgfVxyXG4gIH0gKi9cclxuICAvKiBASG9zdExpc3RlbmVyKCdzY3JvbGwnLCBbJyRldmVudCddKSBvblNjcm9sbEV2ZW50KGV2ZW50RGF0YSkge1xyXG4gICAgLy8gdGhpcy5teVNjcm9sbENvbnRhaW5lci5uYXRpdmVFbGVtZW50LnNjcm9sbFRvcCA8PSAxMDAwXHJcbiAgICBpZiAoZmFsc2UpIHtcclxuICAgICAgdGhpcy5za2lwID0gdGhpcy5za2lwICsgdGhpcy50b3A7XHJcbiAgICAgIHRoaXMubG9hZERhdGEoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNjcm9sbGluZyA9IChlbCk6IHZvaWQgPT4ge1xyXG4gICAgY29uc3Qgc2Nyb2xsVG9wID0gZWwuc3JjRWxlbWVudC5zY3JvbGxUb3A7XHJcbiAgICBpZiAoc2Nyb2xsVG9wID49IDEwMCkge1xyXG4gICAgICAvLyB0aGlzLnNjcm9sbGVkID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIHRoaXMuc2Nyb2xsZWQgPSBmYWxzZTtcclxuICAgIH1cclxuICB9ICovXHJcblxyXG4gIHNldE5vdGlmaWNhdGlvbkRhdGEoKSB7XHJcbiAgICB0aGlzLnRvZGF5Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICB0aGlzLnllc3RlcmRheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgdGhpcy5wYXN0Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLmZvckVhY2goY29udGVudCA9PiB7XHJcbiAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5OQVZJR0FUSU9OX1VSTCkpIHtcclxuICAgICAgICB0aGlzLmZvcm1OYXZpZ2F0aW9uVXJsKGNvbnRlbnQpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChjb250ZW50LklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgY29uc3QgeWVzdGVyZGF5RGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgICAgIHllc3RlcmRheURhdGUuc2V0RGF0ZSh5ZXN0ZXJkYXlEYXRlLmdldERhdGUoKSAtIDEpO1xyXG4gICAgICBpZiAoISh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5USVRMRSkgfHwgdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGNvbnRlbnQuREVTQ1JJUFRJT04pKSkge1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjb250ZW50LlRyYWNraW5nLkNyZWF0ZWREYXRlLCAneXl5eS1NTS1kZCcpID09PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShuZXcgRGF0ZSgpLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICB0aGlzLnRvZGF5Tm90aWZpY2F0aW9uLnB1c2goY29udGVudCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjb250ZW50LlRyYWNraW5nLkNyZWF0ZWREYXRlLCAneXl5eS1NTS1kZCcpID09PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybSh5ZXN0ZXJkYXlEYXRlLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICB0aGlzLnllc3RlcmRheU5vdGlmaWNhdGlvbi5wdXNoKGNvbnRlbnQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24ucHVzaChjb250ZW50KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhzdGFydERhdGU6IERhdGUsIGVuZERhdGU6IERhdGUsIGRpZmZUeXBlOiBtb21lbnRfLnVuaXRPZlRpbWUuRGlmZik6IG51bWJlciB7XHJcbiAgICBjb25zdCBzdGFydCA9IG1vbWVudChzdGFydERhdGUpO1xyXG4gICAgY29uc3QgZW5kID0gbW9tZW50KGVuZERhdGUpO1xyXG4gICAgcmV0dXJuIHN0YXJ0LmRpZmYoZW5kLCBkaWZmVHlwZSwgdHJ1ZSk7XHJcbiAgfVxyXG5cclxuICBnZXREYXRlQnlHaXZlRm9ybWF0KGRhdGVPYmplY3Q6IERhdGUsIGZvcm1hdFZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgY29uc3QgZGF0ZU1vbWVudCA9IG1vbWVudChkYXRlT2JqZWN0KTtcclxuICAgIHJldHVybiBkYXRlTW9tZW50LmZvcm1hdChmb3JtYXRWYWx1ZSk7XHJcbiAgfVxyXG5cclxuICBoYW5kbGVEYXRlTWFuaXB1bGF0aW9uKGRhdGVTdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCFkYXRlU3RyaW5nKSB7XHJcbiAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuICAgIGlmIChkYXRlU3RyaW5nICYmICEoZGF0ZVN0cmluZyBpbnN0YW5jZW9mIERhdGUpKSB7XHJcbiAgICAgIGRhdGVTdHJpbmcgPSBuZXcgRGF0ZShkYXRlU3RyaW5nKTtcclxuICAgIH1cclxuICAgIGxldCBudW1lcmljVmFsdWVEaWZmID0gMDtcclxuICAgIG51bWVyaWNWYWx1ZURpZmYgPSB0aGlzLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ2RheScpO1xyXG4gICAgaWYgKG51bWVyaWNWYWx1ZURpZmYgPCAxKSB7XHJcbiAgICAgIG51bWVyaWNWYWx1ZURpZmYgPSB0aGlzLmdldERheXNCZXR3ZWVuVHdvRGF0ZXMobmV3IERhdGUoKSwgZGF0ZVN0cmluZywgJ2hvdXJzJyk7XHJcbiAgICAgIGlmIChudW1lcmljVmFsdWVEaWZmIDwgMSkge1xyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKHRoaXMuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnbWludXRlcycpKSArICcgbWlucyBhZ28nO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiBNYXRoLmZsb29yKHRoaXMuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnaG91cnMnKSkgKyAnIGhvdXJzIGFnbyc7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKHRoaXMuZ2V0RGF5c0JldHdlZW5Ud29EYXRlcyhuZXcgRGF0ZSgpLCBkYXRlU3RyaW5nLCAnZGF5JykpICsgJyBkYXlzIGFnbyc7XHJcbiAgICB9XHJcbiAgICAvLyByZXR1cm4gdGhpcy5nZXREYXRlQnlHaXZlRm9ybWF0KGRhdGVTdHJpbmcsICdNTU0gRCBZLCBoOm1tIGEnKTtcclxuICB9XHJcblxyXG4gIGZvcm1OYXZpZ2F0aW9uVXJsKG5vdGlmaWNhdGlvbkNvbnRlbnQpIHtcclxuICAgIGxldCBiYXNlVXJsO1xyXG4gICAgbGV0IG5hdmlnYXRpb25Vcmw7XHJcbiAgICBjb25zdCBlbWFpbENvbmZpZyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0RGVmYXVsdEVtYWlsQ29uZmlnKCk7XHJcbiAgICBpZiAoZW1haWxDb25maWcpIHtcclxuICAgICAgYmFzZVVybCA9IGVtYWlsQ29uZmlnLkFQUExJQ0FUSU9OX1VSTDtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ0FwcGxpY2F0aW9uIFVybCBpcyBub3QgY29uZmlndXJlZCcpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5zaGFyaW5nU2VydmljZS5pc01hbmFnZXIpIHtcclxuICAgICAgaWYgKG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19QUk9KRUNUICYmIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19QUk9KRUNUWydQcm9qZWN0LWlkJ10gJiYgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCkge1xyXG5cclxuICAgICAgICBpZiAobm90aWZpY2F0aW9uQ29udGVudC5FVkVOVF9UWVBFID09PSBNUE1fTEVWRUxTLlBST0pFQ1QpIHtcclxuXHJcbiAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvcHJvamVjdC8nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCArICcvJyArIE1lbnVDb25zdGFudHMuT1ZFUlZJRVcgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLk9WRVJWSUVXO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobm90aWZpY2F0aW9uQ29udGVudC5FVkVOVF9UWVBFID09PSBNUE1fTEVWRUxTLlRBU0spIHtcclxuICAgICAgICAgIGlmIChub3RpZmljYXRpb25Db250ZW50LlJfUE9fVEFTSyAmJiBub3RpZmljYXRpb25Db250ZW50LlJfUE9fVEFTS1snVGFzay1pZCddICYmIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLWydUYXNrLWlkJ10uSWQpIHtcclxuICAgICAgICAgICAgbmF2aWdhdGlvblVybCA9IGJhc2VVcmwgKyAnL3Byb2plY3QvJyArIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19QUk9KRUNUWydQcm9qZWN0LWlkJ10uSWQgKyAnLycgKyBNZW51Q29uc3RhbnRzLlRBU0tTICsgJy8nICtcclxuICAgICAgICAgICAgICBub3RpZmljYXRpb25Db250ZW50LlJfUE9fVEFTS1snVGFzay1pZCddLklkICsgJz9tZW51TmFtZT0nICsgTWVudUNvbnN0YW50cy5ERUxJVkVSQUJMRVM7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvcHJvamVjdC8nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCArICcvJyArIE1lbnVDb25zdGFudHMuVEFTS1MgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLlRBU0tTO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAobm90aWZpY2F0aW9uQ29udGVudC5FVkVOVF9UWVBFID09PSBNUE1fTEVWRUxTLkRFTElWRVJBQkxFICYmIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLICYmIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLWydUYXNrLWlkJ10pIHtcclxuICAgICAgICAgIG5hdmlnYXRpb25VcmwgPSBiYXNlVXJsICsgJy9wcm9qZWN0LycgKyBub3RpZmljYXRpb25Db250ZW50LlJfUE9fUFJPSkVDVFsnUHJvamVjdC1pZCddLklkICsgJy8nICsgTWVudUNvbnN0YW50cy5UQVNLUyArICcvJyArXHJcbiAgICAgICAgICAgIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19UQVNLWydUYXNrLWlkJ10uSWQgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLkRFTElWRVJBQkxFUztcclxuICAgICAgICB9IGVsc2UgaWYgKG5vdGlmaWNhdGlvbkNvbnRlbnQuRVZFTlRfVFlQRSA9PT0gJ0NPTU1FTlQnKSB7XHJcbiAgICAgICAgICBpZiAobm90aWZpY2F0aW9uQ29udGVudC5SX1BPX0RFTElWRVJBQkxFICE9IHVuZGVmaW5lZClcclxuICAgICAgICAgICAgbmF2aWdhdGlvblVybCA9IGJhc2VVcmwgKyAnL3Byb2plY3QvJyArIG5vdGlmaWNhdGlvbkNvbnRlbnQuUl9QT19QUk9KRUNUWydQcm9qZWN0LWlkJ10uSWQgKyAnLycgKyBNZW51Q29uc3RhbnRzLkNPTU1FTlRTICsgJz9tZW51TmFtZT0nICsgTWVudUNvbnN0YW50cy5DT01NRU5UUyArICcmZGVsaXZlcmFibGVJZD0nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX0RFTElWRVJBQkxFWydEZWxpdmVyYWJsZS1pZCddLklkO1xyXG4gICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvcHJvamVjdC8nICsgbm90aWZpY2F0aW9uQ29udGVudC5SX1BPX1BST0pFQ1RbJ1Byb2plY3QtaWQnXS5JZCArICcvJyArIE1lbnVDb25zdGFudHMuQ09NTUVOVFMgKyAnP21lbnVOYW1lPScgKyBNZW51Q29uc3RhbnRzLkNPTU1FTlRTO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICBuYXZpZ2F0aW9uVXJsID0gYmFzZVVybCArICcvZGVsaXZlcmFibGVXb3JrVmlldz9tZW51TmFtZT1kZWxpdmVyYWJsZVdvcmtWaWV3JztcclxuICAgICAgaWYgKG5vdGlmaWNhdGlvbkNvbnRlbnQuQVNTSUdOTUVOVF9UWVBFID09PSAnUk9MRScpIHtcclxuXHJcbiAgICAgICAgbmF2aWdhdGlvblVybCA9IG5hdmlnYXRpb25VcmwgKyAnJicgKyAnZ3JvdXBCeUZpbHRlcicgKyAnPScgKyBHcm91cEJ5RmlsdGVyVHlwZXMuR1JPVVBfQllfUFJPSkVDVCArICcmJyArXHJcbiAgICAgICAgICAnZGVsaXZlcmFibGVUYXNrRmlsdGVyJyArICc9JyArIFRhc2tGaWx0ZXJUeXBlcy5NWV9URUFNX1RBU0tTICsgJyYnICsgJ2xpc3REZXBlbmRlbnRGaWx0ZXInICsgJz0nICsgVGFza0xpc3REZXBlbmRlbnRGaWx0ZXJUeXBlcy5BTExfREVMSVZFUkFCTEVfVFlQRVMgKyAnJicgK1xyXG4gICAgICAgICAgJ2VtYWlsTGlua0lkJyArICc9JyArIG5vdGlmaWNhdGlvbkNvbnRlbnQuR1VJRDtcclxuICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgbmF2aWdhdGlvblVybCA9IG5hdmlnYXRpb25VcmwgKyAnJicgKyAnZ3JvdXBCeUZpbHRlcicgKyAnPScgKyBHcm91cEJ5RmlsdGVyVHlwZXMuR1JPVVBfQllfUFJPSkVDVCArICcmJyArXHJcbiAgICAgICAgICAnZGVsaXZlcmFibGVUYXNrRmlsdGVyJyArICc9JyArIFRhc2tGaWx0ZXJUeXBlcy5NWV9UQVNLUyArICcmJyArICdsaXN0RGVwZW5kZW50RmlsdGVyJyArICc9JyArIFRhc2tMaXN0RGVwZW5kZW50RmlsdGVyVHlwZXMuVE9fRE9fRklMVEVSICsgJyYnICtcclxuICAgICAgICAgICdlbWFpbExpbmtJZCcgKyAnPScgKyBub3RpZmljYXRpb25Db250ZW50LkdVSUQ7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmIChuYXZpZ2F0aW9uVXJsKSB7XHJcblxyXG4gICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgICBJZDogbm90aWZpY2F0aW9uQ29udGVudFsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgICB9LFxyXG4gICAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgICAgTkFWSUdBVElPTl9VUkw6IG5hdmlnYXRpb25VcmxcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYXBwU2VydmljZS51cGRhdGVOb3RpZmljYXRpb24ocGFyYW1ldGVycykuc3Vic2NyaWJlKCk7XHJcbiAgICAgIG5vdGlmaWNhdGlvbkNvbnRlbnQuTkFWSUdBVElPTl9VUkwgPSBuYXZpZ2F0aW9uVXJsO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY2xlYXJOb3RpZmljYXRpb24obm90aWZpY2F0aW9uQ29udGVudCkge1xyXG4gICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICdOb3RpZmljYXRpb24taWQnOiB7XHJcbiAgICAgICAgSWQ6IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkXHJcbiAgICAgIH0sXHJcbiAgICAgICdOb3RpZmljYXRpb24tdXBkYXRlJzoge1xyXG4gICAgICAgIElTX0RFTEVURUQ6ICd0cnVlJ1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgIGNvbnN0IHNlbGVjdGVkTm90aWZpY2F0aW9uSW5kZXggPSB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZmluZEluZGV4KGRhdGEgPT5cclxuICAgICAgZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkKTtcclxuICAgIGlmIChzZWxlY3RlZE5vdGlmaWNhdGlvbkluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLnNwbGljZShzZWxlY3RlZE5vdGlmaWNhdGlvbkluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIGNvbnN0IHNlbGVjdGVkUGFzdE5vdGlmaWNhdGlvbkluZGV4ID0gdGhpcy5wYXN0Tm90aWZpY2F0aW9uLmZpbmRJbmRleChkYXRhID0+XHJcbiAgICAgIGRhdGFbJ05vdGlmaWNhdGlvbi1pZCddLklkID09PSBub3RpZmljYXRpb25Db250ZW50WydOb3RpZmljYXRpb24taWQnXS5JZCk7XHJcbiAgICBpZiAoc2VsZWN0ZWRQYXN0Tm90aWZpY2F0aW9uSW5kZXggPj0gMCkge1xyXG4gICAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24uc3BsaWNlKHNlbGVjdGVkUGFzdE5vdGlmaWNhdGlvbkluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIGNvbnN0IHNlbGVjdGVkVG9kYXlOb3RpZmljYXRpb25JbmRleCA9IHRoaXMudG9kYXlOb3RpZmljYXRpb24uZmluZEluZGV4KGRhdGEgPT5cclxuICAgICAgZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkKTtcclxuICAgIGlmIChzZWxlY3RlZFRvZGF5Tm90aWZpY2F0aW9uSW5kZXggPj0gMCkge1xyXG4gICAgICB0aGlzLnRvZGF5Tm90aWZpY2F0aW9uLnNwbGljZShzZWxlY3RlZFRvZGF5Tm90aWZpY2F0aW9uSW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgY29uc3Qgc2VsZWN0ZWRZZXN0ZXJkYXlOb3RpZmljYXRpb25JbmRleCA9IHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uLmZpbmRJbmRleChkYXRhID0+XHJcbiAgICAgIGRhdGFbJ05vdGlmaWNhdGlvbi1pZCddLklkID09PSBub3RpZmljYXRpb25Db250ZW50WydOb3RpZmljYXRpb24taWQnXS5JZCk7XHJcbiAgICBpZiAoc2VsZWN0ZWRZZXN0ZXJkYXlOb3RpZmljYXRpb25JbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uLnNwbGljZShzZWxlY3RlZFllc3RlcmRheU5vdGlmaWNhdGlvbkluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGNvbnRlbnQgPT4ge1xyXG4gICAgICBpZiAoY29udGVudC5JU19SRUFEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG1hcmtSZWFkVW5yZWFkTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbkNvbnRlbnQpIHtcclxuICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAnTm90aWZpY2F0aW9uLWlkJzoge1xyXG4gICAgICAgIElkOiBub3RpZmljYXRpb25Db250ZW50WydOb3RpZmljYXRpb24taWQnXS5JZFxyXG4gICAgICB9LFxyXG4gICAgICAnTm90aWZpY2F0aW9uLXVwZGF0ZSc6IHtcclxuICAgICAgICBJU19SRUFEOiBub3RpZmljYXRpb25Db250ZW50LklTX1JFQUQgPT09ICd0cnVlJyA/ICdmYWxzZScgOiAndHJ1ZSdcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHRoaXMuYXBwU2VydmljZS51cGRhdGVOb3RpZmljYXRpb24ocGFyYW1ldGVycykuc3Vic2NyaWJlKCk7XHJcbiAgICBub3RpZmljYXRpb25Db250ZW50LklTX1JFQUQgPSBub3RpZmljYXRpb25Db250ZW50LklTX1JFQUQgPT09ICd0cnVlJyA/ICdmYWxzZScgOiAndHJ1ZSc7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChjb250ZW50ID0+IHtcclxuICAgICAgaWYgKGNvbnRlbnQuSVNfUkVBRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBtYXJrQXNSZWFkTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbkNvbnRlbnQpIHtcclxuICAgIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uRGV0YWlscyhub3RpZmljYXRpb25Db250ZW50KTtcclxuICAgIGlmIChub3RpZmljYXRpb25Db250ZW50LklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgdGhpcy5oYXNVbnJlYWROb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAnTm90aWZpY2F0aW9uLWlkJzoge1xyXG4gICAgICAgICAgSWQ6IG5vdGlmaWNhdGlvbkNvbnRlbnRbJ05vdGlmaWNhdGlvbi1pZCddLklkXHJcbiAgICAgICAgfSxcclxuICAgICAgICAnTm90aWZpY2F0aW9uLXVwZGF0ZSc6IHtcclxuICAgICAgICAgIElTX1JFQUQ6ICd0cnVlJ1xyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgICAgbm90aWZpY2F0aW9uQ29udGVudC5JU19SRUFEID0gJ3RydWUnO1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChjb250ZW50ID0+IHtcclxuICAgICAgICBpZiAoY29udGVudC5JU19SRUFEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJBbGxOb3RpZmljYXRpb24oKSB7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgaWYgKGRhdGEuSVNfREVMRVRFRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLWlkJzoge1xyXG4gICAgICAgICAgICBJZDogZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLXVwZGF0ZSc6IHtcclxuICAgICAgICAgICAgSVNfREVMRVRFRDogJ3RydWUnXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmFwcFNlcnZpY2UudXBkYXRlTm90aWZpY2F0aW9uKHBhcmFtZXRlcnMpLnN1YnNjcmliZSgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuaGFzVW5yZWFkTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSBbXTtcclxuICAgIHRoaXMudG9kYXlOb3RpZmljYXRpb24gPSBbXTtcclxuICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uID0gW107XHJcbiAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24gPSBbXTtcclxuICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICB9XHJcblxyXG4gIG1hcmtBbGxBc1JlYWROb3RpZmljYXRpb24oKSB7XHJcbiAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLmZvckVhY2goZGF0YSA9PiB7XHJcbiAgICAgIGlmIChkYXRhLklTX1JFQUQgPT09ICdmYWxzZScpIHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgJ05vdGlmaWNhdGlvbi1pZCc6IHtcclxuICAgICAgICAgICAgSWQ6IGRhdGFbJ05vdGlmaWNhdGlvbi1pZCddLklkXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgJ05vdGlmaWNhdGlvbi11cGRhdGUnOiB7XHJcbiAgICAgICAgICAgIElTX1JFQUQ6ICd0cnVlJ1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgICAgICBkYXRhLklTX1JFQUQgPSAndHJ1ZSc7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5zZXROb3RpZmljYXRpb25EYXRhKCk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVOb3RpZmllZENvdW50KCkge1xyXG4gICAgdGhpcy5ub3RpZmllZENvdW50ID0gMDtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YS5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICBpZiAoZGF0YS5JU19OT1RJRklFRCA9PT0gJ2ZhbHNlJykge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLWlkJzoge1xyXG4gICAgICAgICAgICBJZDogZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWRcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAnTm90aWZpY2F0aW9uLXVwZGF0ZSc6IHtcclxuICAgICAgICAgICAgSVNfTk9USUZJRUQ6ICd0cnVlJ1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLnVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoKTtcclxuICAgICAgICBkYXRhLklTX05PVElGSUVEID0gJ3RydWUnO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGxvYWREYXRhKCkge1xyXG4gICAgdGhpcy5za2lwID0gdGhpcy5za2lwICsgdGhpcy50b3A7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLmdldE5vdGlmaWNhdGlvbkZvckN1cnJlbnRVc2VyKHRoaXMuc2tpcCwgdGhpcy50b3ApLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5Ob3RpZmljYXRpb24pIHtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShyZXNwb25zZS5Ob3RpZmljYXRpb24pKSB7XHJcbiAgICAgICAgICByZXNwb25zZS5Ob3RpZmljYXRpb24uZm9yRWFjaChub3RpZmljYXRpb24gPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBleGlzdGluZ05vdGlmaWNhdGlvbiA9IHRoaXMubm90aWZpY2F0aW9uRGF0YS5maW5kKGRhdGEgPT4gZGF0YVsnTm90aWZpY2F0aW9uLWlkJ10uSWQgPT09IG5vdGlmaWNhdGlvblsnTm90aWZpY2F0aW9uLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICBpZiAoIWV4aXN0aW5nTm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLnB1c2gobm90aWZpY2F0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UuTm90aWZpY2F0aW9uLmxlbmd0aCA8IHRoaXMudG9wKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29uc3QgZXhpc3RpbmdOb3RpZmljYXRpb24gPSB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZmluZChkYXRhID0+IGRhdGFbJ05vdGlmaWNhdGlvbi1pZCddLklkID09PSByZXNwb25zZS5Ob3RpZmljYXRpb25bJ05vdGlmaWNhdGlvbi1pZCddLklkKTtcclxuICAgICAgICAgIGlmICghZXhpc3RpbmdOb3RpZmljYXRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhLnB1c2gocmVzcG9uc2UuTm90aWZpY2F0aW9uKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMudXBkYXRlTm90aWZpZWRDb3VudCgpO1xyXG4gICAgICB0aGlzLnNldE5vdGlmaWNhdGlvbkRhdGEoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcmVmcmVzaCgpIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zaG93KCk7XHJcbiAgICB0aGlzLnNraXAgPSAwO1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLmdldE5vdGlmaWNhdGlvbkZvckN1cnJlbnRVc2VyKHRoaXMuc2tpcCwgdGhpcy50b3ApLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSB0cnVlO1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSBbXTtcclxuICAgICAgdGhpcy50b2RheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgICB0aGlzLnllc3RlcmRheU5vdGlmaWNhdGlvbiA9IFtdO1xyXG4gICAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24gPSBbXTtcclxuICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLk5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHJlc3BvbnNlLk5vdGlmaWNhdGlvbikpIHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IHJlc3BvbnNlLk5vdGlmaWNhdGlvbjtcclxuICAgICAgICAgIGlmIChyZXNwb25zZS5Ob3RpZmljYXRpb24ubGVuZ3RoIDwgdGhpcy50b3ApIHtcclxuICAgICAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSBbcmVzcG9uc2UuTm90aWZpY2F0aW9uXTtcclxuICAgICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhID0gW107XHJcbiAgICAgICAgdGhpcy5jYW5Mb2FkTW9yZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMudXBkYXRlTm90aWZpZWRDb3VudCgpO1xyXG4gICAgICB0aGlzLnNldE5vdGlmaWNhdGlvbkRhdGEoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLmdldE5vdGlmaWNhdGlvbkZvckN1cnJlbnRVc2VyKHRoaXMuc2tpcCwgdGhpcy50b3ApLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5Ob3RpZmljYXRpb24pIHtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShyZXNwb25zZS5Ob3RpZmljYXRpb24pKSB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEgPSByZXNwb25zZS5Ob3RpZmljYXRpb247XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2UuTm90aWZpY2F0aW9uLmxlbmd0aCA8IHRoaXMudG9wKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25EYXRhID0gW3Jlc3BvbnNlLk5vdGlmaWNhdGlvbl07XHJcbiAgICAgICAgICB0aGlzLmNhbkxvYWRNb3JlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IFtdO1xyXG4gICAgICAgIHRoaXMuY2FuTG9hZE1vcmUgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbkRhdGEuZm9yRWFjaChjb250ZW50ID0+IHtcclxuICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGNvbnRlbnQuTkFWSUdBVElPTl9VUkwpKSB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1OYXZpZ2F0aW9uVXJsKGNvbnRlbnQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY29udGVudC5JU19SRUFEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICB0aGlzLmhhc1VucmVhZE5vdGlmaWNhdGlvbiA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjb250ZW50LklTX05PVElGSUVEID09PSAnZmFsc2UnKSB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWVkQ291bnQgPSB0aGlzLm5vdGlmaWVkQ291bnQgKyAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB5ZXN0ZXJkYXlEYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgICB5ZXN0ZXJkYXlEYXRlLnNldERhdGUoeWVzdGVyZGF5RGF0ZS5nZXREYXRlKCkgLSAxKTtcclxuICAgICAgICBpZiAoISh0aGlzLnV0aWxTZXJ2aWNlLmlzTnVsbE9yRW1wdHkoY29udGVudC5USVRMRSkgfHwgdGhpcy51dGlsU2VydmljZS5pc051bGxPckVtcHR5KGNvbnRlbnQuREVTQ1JJUFRJT04pKSkge1xyXG4gICAgICAgICAgaWYgKHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKGNvbnRlbnQuVHJhY2tpbmcuQ3JlYXRlZERhdGUsICd5eXl5LU1NLWRkJykgPT09IHRoaXMuZGF0ZVBpcGUudHJhbnNmb3JtKG5ldyBEYXRlKCksICd5eXl5LU1NLWRkJykpIHtcclxuICAgICAgICAgICAgdGhpcy50b2RheU5vdGlmaWNhdGlvbi5wdXNoKGNvbnRlbnQpO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybShjb250ZW50LlRyYWNraW5nLkNyZWF0ZWREYXRlLCAneXl5eS1NTS1kZCcpID09PSB0aGlzLmRhdGVQaXBlLnRyYW5zZm9ybSh5ZXN0ZXJkYXlEYXRlLCAneXl5eS1NTS1kZCcpKSB7XHJcbiAgICAgICAgICAgIHRoaXMueWVzdGVyZGF5Tm90aWZpY2F0aW9uLnB1c2goY29udGVudCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnBhc3ROb3RpZmljYXRpb24ucHVzaChjb250ZW50KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==