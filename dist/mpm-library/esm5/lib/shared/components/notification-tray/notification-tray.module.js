import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationTrayComponent } from './notification-tray.component';
import { MaterialModule } from '../../../material.module';
var NotificationTrayModule = /** @class */ (function () {
    function NotificationTrayModule() {
    }
    NotificationTrayModule = __decorate([
        NgModule({
            declarations: [NotificationTrayComponent],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [NotificationTrayComponent]
        })
    ], NotificationTrayModule);
    return NotificationTrayModule;
}());
export { NotificationTrayModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXRyYXkubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbm90aWZpY2F0aW9uLXRyYXkvbm90aWZpY2F0aW9uLXRyYXkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUMxRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFZMUQ7SUFBQTtJQUFzQyxDQUFDO0lBQTFCLHNCQUFzQjtRQVJsQyxRQUFRLENBQUM7WUFDUixZQUFZLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztZQUN6QyxPQUFPLEVBQUU7Z0JBQ1AsWUFBWTtnQkFDWixjQUFjO2FBQ2Y7WUFDRCxPQUFPLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztTQUNyQyxDQUFDO09BQ1csc0JBQXNCLENBQUk7SUFBRCw2QkFBQztDQUFBLEFBQXZDLElBQXVDO1NBQTFCLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblRyYXlDb21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi10cmF5LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtOb3RpZmljYXRpb25UcmF5Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW05vdGlmaWNhdGlvblRyYXlDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25UcmF5TW9kdWxlIHsgfVxyXG4iXX0=