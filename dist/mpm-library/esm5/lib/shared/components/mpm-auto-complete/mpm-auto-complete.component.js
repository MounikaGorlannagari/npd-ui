import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
var MpmAutoCompleteComponent = /** @class */ (function () {
    function MpmAutoCompleteComponent() {
        this.valueChangeEvent = new EventEmitter();
        this.selectedValue = {
            id: '',
            value: ''
        };
    }
    MpmAutoCompleteComponent.prototype.filterValue = function (value) {
        var filterValue = value;
        try {
            filterValue = value.toLowerCase();
        }
        catch (exception) {
            filterValue = value;
        }
        return this.searchFieldConfig.filterOptions.filter(function (option) { return option.name.toLowerCase().indexOf(filterValue) === 0; });
    };
    MpmAutoCompleteComponent.prototype.displayFn = function (value) {
        return value ? value.displayName : undefined;
    };
    MpmAutoCompleteComponent.prototype.onFocusOut = function (event) {
        //
    };
    MpmAutoCompleteComponent.prototype.changeAutoComplete = function (event) {
        // this.userFormControl.setErrors(null);
        this.selectedValue.id = event.option.value.value;
        this.selectedValue.value = event.option.value.displayName;
        this.valueChangeEvent.next(event.option.value);
    };
    MpmAutoCompleteComponent.prototype.mapInitialSelectedValue = function () {
        var _this = this;
        this.filterOptions = this.formControl.valueChanges
            .pipe(startWith(this.selectedValue.value), map(function (value) {
            _this.searchFieldConfig.filterOptions.map(function (option) {
                if (option.value === value) {
                    _this.selectedValue.value = option.displayName;
                    _this.selectedValue.id = option.value;
                }
            });
            return _this.filterValue(value);
        }));
    };
    MpmAutoCompleteComponent.prototype.initializeSearchFields = function () {
        var _this = this;
        if (this.searchFieldConfig) {
            if (this.searchFieldConfig.formControl) {
                this.formControl = this.searchFieldConfig.formControl;
            }
            else {
                this.formControl = new FormControl();
            }
            if (this.formControl.errors && this.formControl.errors.required) {
                this.requiredField = true;
            }
            else {
                this.requiredField = false;
            }
            if (this.searchFieldConfig.filterOptions && this.searchFieldConfig.filterOptions.length &&
                this.searchFieldConfig.filterOptions.length > 0) {
                this.mapInitialSelectedValue();
            }
        }
        if (this.searchFieldConfig && this.searchFieldConfig.filterOptions) {
            this.searchFieldConfig.filterOptions.map(function (option) {
                if (option.value === _this.formControl.value) {
                    _this.selectedValue.value = option.displayName;
                    _this.selectedValue.id = option.value;
                }
            });
        }
        if (this.formControl.status === 'DISABLED') {
            this.formControl.disable();
        }
    };
    MpmAutoCompleteComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.autoComplete && _this.autoFocus) {
                var inputElement = _this.inputField.nativeElement;
                inputElement.focus();
            }
        }, 500);
    };
    MpmAutoCompleteComponent.prototype.ngOnInit = function () {
        this.initializeSearchFields();
    };
    __decorate([
        ViewChild('userAuto', { static: false })
    ], MpmAutoCompleteComponent.prototype, "autoComplete", void 0);
    __decorate([
        ViewChild('inputField', { static: false })
    ], MpmAutoCompleteComponent.prototype, "inputField", void 0);
    __decorate([
        Input()
    ], MpmAutoCompleteComponent.prototype, "searchFieldConfig", void 0);
    __decorate([
        Input()
    ], MpmAutoCompleteComponent.prototype, "required", void 0);
    __decorate([
        Input()
    ], MpmAutoCompleteComponent.prototype, "autoFocus", void 0);
    __decorate([
        Output()
    ], MpmAutoCompleteComponent.prototype, "valueChangeEvent", void 0);
    MpmAutoCompleteComponent = __decorate([
        Component({
            selector: 'mpm-mpm-auto-complete',
            template: "<!-- <mat-form-field class=\"flex-row-item mat-form-align\" appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"formControl\" (focusout)=\"onFocusOut($event)\" [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <span matTooltip=\"{{option.displayName}}\" matTooltipClass=\"custom-tooltip\">{{option.displayName}}</span>\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n</mat-form-field> -->\r\n<mat-form-field class=\"flex-row-item mat-form-align\" appearance=\"outline\">\r\n    <mat-label>{{searchFieldConfig.label}}</mat-label>\r\n    <input #inputField matInput placeholder=\"{{searchFieldConfig.label}}\" aria-label=\"searchFieldConfig.label\"\r\n        [matAutocomplete]=\"userAuto\" [formControl]=\"formControl\" (focusout)=\"onFocusOut($event)\" [required]=\"required\">\r\n    <mat-autocomplete #userAuto=\"matAutocomplete\" [displayWith]=\"displayFn\"\r\n        (optionSelected)=\"changeAutoComplete($event)\">\r\n        <mat-option *ngFor=\"let option of filterOptions | async\" [value]=\"option\">\r\n            <!-- <div #optionposition\r\n                [matTooltip]=\"spanposition.getBoundingClientRect().width > optionposition.getBoundingClientRect().width ? option.displayName : null\">\r\n                {{option.displayName}}   \r\n                <span #spanposition  \r\n                    [matTooltip]=\"spanposition.getBoundingClientRect().width <= optionposition.getBoundingClientRect().width ? option.displayName : null\" >\r\n                    {{option.displayName}}\r\n                </span>\r\n            </div> -->\r\n            <span matTooltip=\"{{option.displayName}}\">{{option.displayName}}</span>\r\n            <!-- matTooltipClass=\"custom-tooltip\" -->\r\n        </mat-option>\r\n    </mat-autocomplete>\r\n</mat-form-field>",
            encapsulation: ViewEncapsulation.None,
            styles: [".mat-form-align{margin:0 5px}"]
        })
    ], MpmAutoCompleteComponent);
    return MpmAutoCompleteComponent;
}());
export { MpmAutoCompleteComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvbXBtLWF1dG8tY29tcGxldGUvbXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFjLFNBQVMsRUFBaUIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEksT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTdDLE9BQU8sRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFTaEQ7SUFpQkU7UUFUVSxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBS3JELGtCQUFhLEdBQUc7WUFDZCxFQUFFLEVBQUUsRUFBRTtZQUNOLEtBQUssRUFBRSxFQUFFO1NBQ1YsQ0FBQztJQUNjLENBQUM7SUFFViw4Q0FBVyxHQUFsQixVQUFtQixLQUFhO1FBQzlCLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJO1lBQ0YsV0FBVyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNuQztRQUFDLE9BQU8sU0FBUyxFQUFFO1lBQ2xCLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDckI7UUFDRCxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFwRCxDQUFvRCxDQUFDLENBQUM7SUFDckgsQ0FBQztJQUVELDRDQUFTLEdBQVQsVUFBVSxLQUFXO1FBQ25CLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFDL0MsQ0FBQztJQUNELDZDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsRUFBRTtJQUNKLENBQUM7SUFDRCxxREFBa0IsR0FBbEIsVUFBbUIsS0FBSztRQUN0Qix3Q0FBd0M7UUFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2pELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztRQUMxRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELDBEQUF1QixHQUF2QjtRQUFBLGlCQWNDO1FBYkMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVk7YUFDL0MsSUFBSSxDQUNILFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUNuQyxHQUFHLENBQUMsVUFBQSxLQUFLO1lBQ1AsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO2dCQUM3QyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssS0FBSyxFQUFFO29CQUMxQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO29CQUM5QyxLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO2lCQUN0QztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUNILENBQUM7SUFDTixDQUFDO0lBRUQseURBQXNCLEdBQXRCO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzFCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRTtnQkFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDO2FBQ3ZEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQzthQUN0QztZQUNELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO2dCQUMvRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzthQUMzQjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQzthQUM1QjtZQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLE1BQU07Z0JBQ3JGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7YUFDaEM7U0FDRjtRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7WUFDbEUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNO2dCQUM3QyxJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7b0JBQzlDLEtBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7aUJBQ3RDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsa0RBQWUsR0FBZjtRQUFBLGlCQU9DO1FBTkMsVUFBVSxDQUFDO1lBQ1QsSUFBSSxLQUFJLENBQUMsWUFBWSxJQUFJLEtBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3ZDLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO2dCQUNuRCxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDdEI7UUFDSCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDVixDQUFDO0lBRUQsMkNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFsR3lDO1FBQXpDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7a0VBQTBCO0lBQ3ZCO1FBQTNDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUM7Z0VBQXdCO0lBRTFEO1FBQVIsS0FBSyxFQUFFO3VFQUFtQjtJQUNsQjtRQUFSLEtBQUssRUFBRTs4REFBVTtJQUNUO1FBQVIsS0FBSyxFQUFFOytEQUFXO0lBQ1Q7UUFBVCxNQUFNLEVBQUU7c0VBQTRDO0lBUjFDLHdCQUF3QjtRQVBwQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsdUJBQXVCO1lBQ2pDLGlxRUFBaUQ7WUFFakQsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O1NBRXRDLENBQUM7T0FDVyx3QkFBd0IsQ0FzR3BDO0lBQUQsK0JBQUM7Q0FBQSxBQXRHRCxJQXNHQztTQXRHWSx3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBFbGVtZW50UmVmLCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHN0YXJ0V2l0aCwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tbXBtLWF1dG8tY29tcGxldGUnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9tcG0tYXV0by1jb21wbGV0ZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbXBtLWF1dG8tY29tcGxldGUuY29tcG9uZW50LmNzcyddLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxuXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNcG1BdXRvQ29tcGxldGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xyXG5cclxuICBAVmlld0NoaWxkKCd1c2VyQXV0bycsIHsgc3RhdGljOiBmYWxzZSB9KSBhdXRvQ29tcGxldGU6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZCgnaW5wdXRGaWVsZCcsIHsgc3RhdGljOiBmYWxzZSB9KSBpbnB1dEZpZWxkOiBFbGVtZW50UmVmO1xyXG5cclxuICBASW5wdXQoKSBzZWFyY2hGaWVsZENvbmZpZztcclxuICBASW5wdXQoKSByZXF1aXJlZDtcclxuICBASW5wdXQoKSBhdXRvRm9jdXM7XHJcbiAgQE91dHB1dCgpIHZhbHVlQ2hhbmdlRXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgZm9ybUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG4gIGZpbHRlck9wdGlvbnM6IE9ic2VydmFibGU8YW55W10+O1xyXG4gIHJlcXVpcmVkRmllbGQ6IGJvb2xlYW47XHJcbiAgc2VsZWN0ZWRWYWx1ZSA9IHtcclxuICAgIGlkOiAnJyxcclxuICAgIHZhbHVlOiAnJ1xyXG4gIH07XHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgcHVibGljIGZpbHRlclZhbHVlKHZhbHVlOiBzdHJpbmcpOiBzdHJpbmdbXSB7XHJcbiAgICBsZXQgZmlsdGVyVmFsdWUgPSB2YWx1ZTtcclxuICAgIHRyeSB7XHJcbiAgICAgIGZpbHRlclZhbHVlID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcclxuICAgIH0gY2F0Y2ggKGV4Y2VwdGlvbikge1xyXG4gICAgICBmaWx0ZXJWYWx1ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucy5maWx0ZXIob3B0aW9uID0+IG9wdGlvbi5uYW1lLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihmaWx0ZXJWYWx1ZSkgPT09IDApO1xyXG4gIH1cclxuXHJcbiAgZGlzcGxheUZuKHZhbHVlPzogYW55KTogc3RyaW5nIHwgdW5kZWZpbmVkIHtcclxuICAgIHJldHVybiB2YWx1ZSA/IHZhbHVlLmRpc3BsYXlOYW1lIDogdW5kZWZpbmVkO1xyXG4gIH1cclxuICBvbkZvY3VzT3V0KGV2ZW50KSB7XHJcbiAgICAvL1xyXG4gIH1cclxuICBjaGFuZ2VBdXRvQ29tcGxldGUoZXZlbnQpIHtcclxuICAgIC8vIHRoaXMudXNlckZvcm1Db250cm9sLnNldEVycm9ycyhudWxsKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS5pZCA9IGV2ZW50Lm9wdGlvbi52YWx1ZS52YWx1ZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSA9IGV2ZW50Lm9wdGlvbi52YWx1ZS5kaXNwbGF5TmFtZTtcclxuICAgIHRoaXMudmFsdWVDaGFuZ2VFdmVudC5uZXh0KGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBtYXBJbml0aWFsU2VsZWN0ZWRWYWx1ZSgpIHtcclxuICAgIHRoaXMuZmlsdGVyT3B0aW9ucyA9IHRoaXMuZm9ybUNvbnRyb2wudmFsdWVDaGFuZ2VzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHN0YXJ0V2l0aCh0aGlzLnNlbGVjdGVkVmFsdWUudmFsdWUpLFxyXG4gICAgICAgIG1hcCh2YWx1ZSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMubWFwKG9wdGlvbiA9PiB7XHJcbiAgICAgICAgICAgIGlmIChvcHRpb24udmFsdWUgPT09IHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5zZWxlY3RlZFZhbHVlLnZhbHVlID0gb3B0aW9uLmRpc3BsYXlOYW1lO1xyXG4gICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS5pZCA9IG9wdGlvbi52YWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5maWx0ZXJWYWx1ZSh2YWx1ZSk7XHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIGluaXRpYWxpemVTZWFyY2hGaWVsZHMoKSB7XHJcbiAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZykge1xyXG4gICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZy5mb3JtQ29udHJvbCkge1xyXG4gICAgICAgIHRoaXMuZm9ybUNvbnRyb2wgPSB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZvcm1Db250cm9sO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5mb3JtQ29udHJvbC5lcnJvcnMgJiYgdGhpcy5mb3JtQ29udHJvbC5lcnJvcnMucmVxdWlyZWQpIHtcclxuICAgICAgICB0aGlzLnJlcXVpcmVkRmllbGQgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucmVxdWlyZWRGaWVsZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMgJiYgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLmxlbmd0aCAmJlxyXG4gICAgICAgIHRoaXMuc2VhcmNoRmllbGRDb25maWcuZmlsdGVyT3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5tYXBJbml0aWFsU2VsZWN0ZWRWYWx1ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5zZWFyY2hGaWVsZENvbmZpZyAmJiB0aGlzLnNlYXJjaEZpZWxkQ29uZmlnLmZpbHRlck9wdGlvbnMpIHtcclxuICAgICAgdGhpcy5zZWFyY2hGaWVsZENvbmZpZy5maWx0ZXJPcHRpb25zLm1hcChvcHRpb24gPT4ge1xyXG4gICAgICAgIGlmIChvcHRpb24udmFsdWUgPT09IHRoaXMuZm9ybUNvbnRyb2wudmFsdWUpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS52YWx1ZSA9IG9wdGlvbi5kaXNwbGF5TmFtZTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRWYWx1ZS5pZCA9IG9wdGlvbi52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmZvcm1Db250cm9sLnN0YXR1cyA9PT0gJ0RJU0FCTEVEJykge1xyXG4gICAgICB0aGlzLmZvcm1Db250cm9sLmRpc2FibGUoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5hdXRvQ29tcGxldGUgJiYgdGhpcy5hdXRvRm9jdXMpIHtcclxuICAgICAgICBjb25zdCBpbnB1dEVsZW1lbnQgPSB0aGlzLmlucHV0RmllbGQubmF0aXZlRWxlbWVudDtcclxuICAgICAgICBpbnB1dEVsZW1lbnQuZm9jdXMoKTtcclxuICAgICAgfVxyXG4gICAgfSwgNTAwKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5pbml0aWFsaXplU2VhcmNoRmllbGRzKCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=