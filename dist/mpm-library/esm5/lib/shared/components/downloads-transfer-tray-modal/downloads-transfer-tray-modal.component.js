import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
var DownloadsTransferTrayModalComponent = /** @class */ (function () {
    function DownloadsTransferTrayModalComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'My Downloads';
    }
    DownloadsTransferTrayModalComponent.prototype.cancelDialog = function () {
        this.dialogRef.close();
    };
    DownloadsTransferTrayModalComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    DownloadsTransferTrayModalComponent = __decorate([
        Component({
            selector: 'mpm-downloads-transfer-tray-modal',
            template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-downloads-transfer-tray></mpm-downloads-transfer-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
            styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], DownloadsTransferTrayModalComponent);
    return DownloadsTransferTrayModalComponent;
}());
export { DownloadsTransferTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFO0lBSUUsNkNBQ1MsU0FBdUQsRUFDOUIsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUE4QztRQUM5QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxjQUFjLENBQUM7SUFLeEIsQ0FBQztJQUVMLDBEQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3pCLENBQUM7O2dCQU5tQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTs7SUFOZCxtQ0FBbUM7UUFML0MsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG1DQUFtQztZQUM3QyxvZ0JBQTZEOztTQUU5RCxDQUFDO1FBT0csV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7T0FOZixtQ0FBbUMsQ0FhL0M7SUFBRCwwQ0FBQztDQUFBLEFBYkQsSUFhQztTQWJZLG1DQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRG93bmxvYWRzVHJhbnNmZXJUcmF5Q29tcG9uZW50IH0gZnJvbSAnLi4vZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9kb3dubG9hZHMtdHJhbnNmZXItdHJheS1tb2RhbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZG93bmxvYWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRG93bmxvYWRzVHJhbnNmZXJUcmF5TW9kYWxDb21wb25lbnQge1xyXG5cclxuICBtb2RhbFRpdGxlID0gJ015IERvd25sb2Fkcyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPERvd25sb2Fkc1RyYW5zZmVyVHJheUNvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGFcclxuICApIHsgfVxyXG5cclxuICBjYW5jZWxEaWFsb2coKSB7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19