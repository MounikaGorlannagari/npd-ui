import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var NameIconComponent = /** @class */ (function () {
    function NameIconComponent() {
    }
    NameIconComponent.prototype.ngOnInit = function () {
        if (this.userName && this.userName.length && this.userName.length > 0) {
            if (this.charCount) {
                this.userProfileCharacter = this.userName.substring(0, this.charCount).toUpperCase();
            }
            else if (this.userName) {
                this.userProfileCharacter = this.userName.charAt(0).toUpperCase();
            }
        }
        if (!this.color) {
            this.color = '';
        }
    };
    __decorate([
        Input()
    ], NameIconComponent.prototype, "userName", void 0);
    __decorate([
        Input()
    ], NameIconComponent.prototype, "color", void 0);
    __decorate([
        Input()
    ], NameIconComponent.prototype, "charCount", void 0);
    NameIconComponent = __decorate([
        Component({
            selector: 'mpm-name-icon',
            template: "<div class=\"user-profile-section\">\r\n    <span class=\"circled-name-letters\" style.background=\"{{color}}\">{{userProfileCharacter}}</span>\r\n</div>",
            styles: [".circled-name-letters{content:attr(data-letters);display:inline-block;font-size:16px;width:2.5em;line-height:2.5em;text-align:center;border-radius:50%;vertical-align:middle}"]
        })
    ], NameIconComponent);
    return NameIconComponent;
}());
export { NameIconComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmFtZS1pY29uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9jb21wb25lbnRzL25hbWUtaWNvbi9uYW1lLWljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVF6RDtJQVFJO0lBQWdCLENBQUM7SUFFakIsb0NBQVEsR0FBUjtRQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkUsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN4RjtpQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNyRTtTQUNKO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDYixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztTQUNuQjtJQUNMLENBQUM7SUFuQlE7UUFBUixLQUFLLEVBQUU7dURBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTtvREFBTztJQUNOO1FBQVIsS0FBSyxFQUFFO3dEQUFXO0lBSlYsaUJBQWlCO1FBTjdCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLHFLQUF5Qzs7U0FFNUMsQ0FBQztPQUVXLGlCQUFpQixDQXVCN0I7SUFBRCx3QkFBQztDQUFBLEFBdkJELElBdUJDO1NBdkJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21wbS1uYW1lLWljb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25hbWUtaWNvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9uYW1lLWljb24uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5hbWVJY29uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSB1c2VyTmFtZTtcclxuICAgIEBJbnB1dCgpIGNvbG9yO1xyXG4gICAgQElucHV0KCkgY2hhckNvdW50O1xyXG5cclxuICAgIHVzZXJQcm9maWxlQ2hhcmFjdGVyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudXNlck5hbWUgJiYgdGhpcy51c2VyTmFtZS5sZW5ndGggJiYgdGhpcy51c2VyTmFtZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNoYXJDb3VudCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VyUHJvZmlsZUNoYXJhY3RlciA9IHRoaXMudXNlck5hbWUuc3Vic3RyaW5nKDAsIHRoaXMuY2hhckNvdW50KS50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudXNlck5hbWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlclByb2ZpbGVDaGFyYWN0ZXIgPSB0aGlzLnVzZXJOYW1lLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGhpcy5jb2xvcikge1xyXG4gICAgICAgICAgICB0aGlzLmNvbG9yID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=