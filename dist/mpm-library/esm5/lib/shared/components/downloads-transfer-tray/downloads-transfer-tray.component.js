import { __decorate } from "tslib";
import { Component, isDevMode } from '@angular/core';
import { otmmServicesConstants } from '../../../mpm-utils/config/otmmService.constant';
import { MatTableDataSource } from '@angular/material/table';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
var DownloadsTransferTrayComponent = /** @class */ (function () {
    function DownloadsTransferTrayComponent(loaderService, otmmService, notificationService, qdsService, sharingService, utilService) {
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.qdsService = qdsService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.displayedColumns = ['filename', 'no_of_files', 'time', 'size', 'action'];
        this.isLoadingData = true;
        this.noData = false;
        this.showActions = true;
        this.dataSource = new MatTableDataSource([]);
    }
    DownloadsTransferTrayComponent.prototype.convertToMB = function (byte) {
        return Math.round((byte / 1024) / 1024 * 100) / 100;
    };
    DownloadsTransferTrayComponent.prototype.getExportJobs = function () {
        var _this = this;
        this.isLoadingData = true;
        var params = [{
                key: 'after',
                value: '0'
            }, {
                key: 'limit',
                value: '100'
            }, {
                key: 'job_class',
                value: 'EXPORT'
            }, {
                key: 'load_downloadable_jobs_only',
                value: 'true'
            }, {
                key: 'load_asset_details',
                value: 'true'
            }, {
                key: 'asset_ids_limit',
                value: '3'
            }, {
                key: 'sort',
                value: 'desc_start_date_time'
            }, {
                key: 'load_job_details',
                value: 'true'
            }, {
                key: 'status',
                value: 'COMPLETED'
            }];
        this.otmmService.getExportJobs(params)
            .subscribe(function (response) {
            if (response && response.jobs_resource && response.jobs_resource.collection_summary &&
                response.jobs_resource.collection_summary.actual_count_of_items &&
                response.jobs_resource.collection_summary.actual_count_of_items > 0) {
                _this.dataSource = new MatTableDataSource(response.jobs_resource.jobs);
                _this.isLoadingData = false;
            }
            else {
                _this.isLoadingData = false;
                _this.noData = true;
                _this.noDataMsg = 'No items.';
            }
        }, function (error) {
            _this.noData = true;
            _this.noDataMsg = 'Unable to get data from QDS.';
            _this.isLoadingData = false;
        });
    };
    DownloadsTransferTrayComponent.prototype.download = function (job) {
        var _this = this;
        if (otmmServicesConstants.OTMM_SERVICE_VARIABLES.isQDSUpload &&
            otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
            var fileList = [];
            fileList.push({ name: job.job_details.export_zip_name, size: job.job_details.export_size });
            this.qdsService.download(job.job_id, fileList)
                .subscribe(function (response) {
                _this.notificationService.info(response);
            }, function (error) {
                _this.notificationService.error('Something went wrong');
            });
        }
        else {
            this.otmmService.checkOtmmSession().subscribe();
            if (job && job.job_details && job.job_details.relative_download_url) {
                var otmmBaseUrl = isDevMode() ? '' : this.sharingService.getMediaManagerConfig().url;
                var assetURL = otmmBaseUrl + job.job_details.relative_download_url.slice(1);
                this.utilService.initiateDownload(assetURL, job.job_details.export_zip_name);
            }
            else {
                this.notificationService.error('Something went wrong while downloading file');
                return;
            }
        }
    };
    DownloadsTransferTrayComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.otmmService.checkOtmmSession()
            .subscribe(function (response) {
            _this.loaderService.hide();
            _this.isLoadingData = false;
            _this.getExportJobs();
        }, function (error) {
            _this.loaderService.hide();
            _this.isLoadingData = false;
            _this.noData = true;
            _this.noDataMsg = 'Unable to connect to Media Manager.';
            _this.notificationService.error('Unable to initialize OTMM session');
        });
    };
    DownloadsTransferTrayComponent.ctorParameters = function () { return [
        { type: LoaderService },
        { type: OTMMService },
        { type: NotificationService },
        { type: QdsService },
        { type: SharingService },
        { type: UtilService }
    ]; };
    DownloadsTransferTrayComponent = __decorate([
        Component({
            selector: 'mpm-downloads-transfer-tray',
            template: "<mat-spinner diameter=\"40\" *ngIf=\"isLoadingData\"></mat-spinner>\r\n\r\n<div class=\"qds-files-container\" [hidden]=\"isLoadingData\">\r\n    <table mat-table [dataSource]=\"dataSource\" matSort>\r\n        <ng-container matColumnDef=\"filename\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.job_details.export_zip_name}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"no_of_files\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> No. of Files </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.asset_count}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"time\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Exported Time </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{element.end_time | formatToLocaleDateTime}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"size\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Size </th>\r\n            <td mat-cell *matCellDef=\"let element\"> {{convertToMB(element.job_details.export_size)}} MB</td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"action\">\r\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Action </th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <button mat-icon-button aria-label=\"Download\" matTooltip=\"Download\" (click)=\"download(element)\">\r\n                    <mat-icon class=\"success\">vertical_align_bottom</mat-icon>\r\n                </button>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n</div>\r\n\r\n<div *ngIf=\"noData && !isLoadingData\" class=\"no-data\">\r\n    <span>{{noDataMsg}}</span>\r\n</div>",
            styles: [".float-right{float:right}.justify-flex-end{justify-content:flex-end}.flex-spacer{display:flex;flex-grow:5}.qds-files-container{overflow:hidden}.qds-files-container table{width:100%}mat-spinner.mat-progress-spinner{position:absolute;z-index:999999;top:48vh;left:48vw}.success{color:green}.info{color:#00f}.danger{color:red}.warning{color:orange}.no-data{padding:24px;text-align:center}"]
        })
    ], DownloadsTransferTrayComponent);
    return DownloadsTransferTrayComponent;
}());
export { DownloadsTransferTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkvZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUF5QixTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQU83RTtJQVNFLHdDQUNTLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLG1CQUF3QyxFQUN4QyxVQUFzQixFQUN0QixjQUE4QixFQUM5QixXQUF3QjtRQUx4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBYmpDLHFCQUFnQixHQUFhLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRW5GLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBRXJCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixnQkFBVyxHQUFHLElBQUksQ0FBQztRQVdqQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELG9EQUFXLEdBQVgsVUFBWSxJQUFZO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ3RELENBQUM7SUFFRCxzREFBYSxHQUFiO1FBQUEsaUJBaURDO1FBaERDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBRTFCLElBQU0sTUFBTSxHQUFHLENBQUM7Z0JBQ2QsR0FBRyxFQUFFLE9BQU87Z0JBQ1osS0FBSyxFQUFFLEdBQUc7YUFDWCxFQUFFO2dCQUNELEdBQUcsRUFBRSxPQUFPO2dCQUNaLEtBQUssRUFBRSxLQUFLO2FBQ2IsRUFBRTtnQkFDRCxHQUFHLEVBQUUsV0FBVztnQkFDaEIsS0FBSyxFQUFFLFFBQVE7YUFDaEIsRUFBRTtnQkFDRCxHQUFHLEVBQUUsNkJBQTZCO2dCQUNsQyxLQUFLLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLG9CQUFvQjtnQkFDekIsS0FBSyxFQUFFLE1BQU07YUFDZCxFQUFFO2dCQUNELEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxHQUFHO2FBQ1gsRUFBRTtnQkFDRCxHQUFHLEVBQUUsTUFBTTtnQkFDWCxLQUFLLEVBQUUsc0JBQXNCO2FBQzlCLEVBQUU7Z0JBQ0QsR0FBRyxFQUFFLGtCQUFrQjtnQkFDdkIsS0FBSyxFQUFFLE1BQU07YUFDZCxFQUFFO2dCQUNELEdBQUcsRUFBRSxRQUFRO2dCQUNiLEtBQUssRUFBRSxXQUFXO2FBQ25CLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQzthQUNuQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2pCLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0I7Z0JBQ2pGLFFBQVEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMscUJBQXFCO2dCQUMvRCxRQUFRLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixHQUFHLENBQUMsRUFBRTtnQkFDckUsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RFLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzVCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsS0FBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUM7YUFDOUI7UUFDSCxDQUFDLEVBQUUsVUFBQSxLQUFLO1lBQ04sS0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsS0FBSSxDQUFDLFNBQVMsR0FBRyw4QkFBOEIsQ0FBQztZQUNoRCxLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpREFBUSxHQUFSLFVBQVMsR0FBRztRQUFaLGlCQXNCQztRQXJCQyxJQUFJLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLFdBQVc7WUFDMUQscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxFQUFFO1lBQzVELElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNwQixRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDNUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2pCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUMsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDTixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFDekQsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ2hELElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxXQUFXLElBQUksR0FBRyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDbkUsSUFBTSxXQUFXLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztnQkFDdkYsSUFBTSxRQUFRLEdBQUcsV0FBVyxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQzlFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsNkNBQTZDLENBQUMsQ0FBQztnQkFDOUUsT0FBTzthQUNSO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsd0RBQWUsR0FBZjtRQUFBLGlCQWFDO1FBWkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRTthQUNoQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2pCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDTixLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxTQUFTLEdBQUcscUNBQXFDLENBQUM7WUFDdkQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1FBQ3RFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBdkd1QixhQUFhO2dCQUNmLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUM1QixVQUFVO2dCQUNOLGNBQWM7Z0JBQ2pCLFdBQVc7O0lBZnRCLDhCQUE4QjtRQUwxQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLHdoRUFBdUQ7O1NBRXhELENBQUM7T0FDVyw4QkFBOEIsQ0FtSDFDO0lBQUQscUNBQUM7Q0FBQSxBQW5IRCxJQW1IQztTQW5IWSw4QkFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgaXNEZXZNb2RlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9jb25maWcvb3RtbVNlcnZpY2UuY29uc3RhbnQnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXRpbFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvdXRpbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvc2hhcmluZy5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWRvd25sb2Fkcy10cmFuc2Zlci10cmF5JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZG93bmxvYWRzLXRyYW5zZmVyLXRyYXkuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2Rvd25sb2Fkcy10cmFuc2Zlci10cmF5LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIERvd25sb2Fkc1RyYW5zZmVyVHJheUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xyXG5cclxuICBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsnZmlsZW5hbWUnLCAnbm9fb2ZfZmlsZXMnLCAndGltZScsICdzaXplJywgJ2FjdGlvbiddO1xyXG4gIGRhdGFTb3VyY2U6IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+O1xyXG4gIGlzTG9hZGluZ0RhdGEgPSB0cnVlO1xyXG4gIG5vRGF0YU1zZzogc3RyaW5nO1xyXG4gIG5vRGF0YSA9IGZhbHNlO1xyXG4gIHNob3dBY3Rpb25zID0gdHJ1ZTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBvdG1tU2VydmljZTogT1RNTVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgIHB1YmxpYyB1dGlsU2VydmljZTogVXRpbFNlcnZpY2UsXHJcblxyXG4gICkge1xyXG4gICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShbXSk7XHJcbiAgfVxyXG5cclxuICBjb252ZXJ0VG9NQihieXRlOiBudW1iZXIpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKChieXRlIC8gMTAyNCkgLyAxMDI0ICogMTAwKSAvIDEwMDtcclxuICB9XHJcblxyXG4gIGdldEV4cG9ydEpvYnMoKSB7XHJcbiAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSB0cnVlO1xyXG5cclxuICAgIGNvbnN0IHBhcmFtcyA9IFt7XHJcbiAgICAgIGtleTogJ2FmdGVyJyxcclxuICAgICAgdmFsdWU6ICcwJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdsaW1pdCcsXHJcbiAgICAgIHZhbHVlOiAnMTAwJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdqb2JfY2xhc3MnLFxyXG4gICAgICB2YWx1ZTogJ0VYUE9SVCdcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnbG9hZF9kb3dubG9hZGFibGVfam9ic19vbmx5JyxcclxuICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgfSwge1xyXG4gICAgICBrZXk6ICdsb2FkX2Fzc2V0X2RldGFpbHMnLFxyXG4gICAgICB2YWx1ZTogJ3RydWUnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ2Fzc2V0X2lkc19saW1pdCcsXHJcbiAgICAgIHZhbHVlOiAnMydcclxuICAgIH0sIHtcclxuICAgICAga2V5OiAnc29ydCcsXHJcbiAgICAgIHZhbHVlOiAnZGVzY19zdGFydF9kYXRlX3RpbWUnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ2xvYWRfam9iX2RldGFpbHMnLFxyXG4gICAgICB2YWx1ZTogJ3RydWUnXHJcbiAgICB9LCB7XHJcbiAgICAgIGtleTogJ3N0YXR1cycsXHJcbiAgICAgIHZhbHVlOiAnQ09NUExFVEVEJ1xyXG4gICAgfV07XHJcblxyXG4gICAgdGhpcy5vdG1tU2VydmljZS5nZXRFeHBvcnRKb2JzKHBhcmFtcylcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmpvYnNfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkgJiZcclxuICAgICAgICAgIHJlc3BvbnNlLmpvYnNfcmVzb3VyY2UuY29sbGVjdGlvbl9zdW1tYXJ5LmFjdHVhbF9jb3VudF9vZl9pdGVtcyAmJlxyXG4gICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zID4gMCkge1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShyZXNwb25zZS5qb2JzX3Jlc291cmNlLmpvYnMpO1xyXG4gICAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuaXNMb2FkaW5nRGF0YSA9IGZhbHNlO1xyXG4gICAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5ub0RhdGFNc2cgPSAnTm8gaXRlbXMuJztcclxuICAgICAgICB9XHJcbiAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICB0aGlzLm5vRGF0YSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub0RhdGFNc2cgPSAnVW5hYmxlIHRvIGdldCBkYXRhIGZyb20gUURTLic7XHJcbiAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZG93bmxvYWQoam9iKSB7XHJcbiAgICBpZiAob3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaXNRRFNVcGxvYWQgJiZcclxuICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCkge1xyXG4gICAgICBjb25zdCBmaWxlTGlzdCA9IFtdO1xyXG4gICAgICBmaWxlTGlzdC5wdXNoKHsgbmFtZTogam9iLmpvYl9kZXRhaWxzLmV4cG9ydF96aXBfbmFtZSwgc2l6ZTogam9iLmpvYl9kZXRhaWxzLmV4cG9ydF9zaXplIH0pO1xyXG4gICAgICB0aGlzLnFkc1NlcnZpY2UuZG93bmxvYWQoam9iLmpvYl9pZCwgZmlsZUxpc3QpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbyhyZXNwb25zZSk7XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZycpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5vdG1tU2VydmljZS5jaGVja090bW1TZXNzaW9uKCkuc3Vic2NyaWJlKCk7XHJcbiAgICAgIGlmIChqb2IgJiYgam9iLmpvYl9kZXRhaWxzICYmIGpvYi5qb2JfZGV0YWlscy5yZWxhdGl2ZV9kb3dubG9hZF91cmwpIHtcclxuICAgICAgICBjb25zdCBvdG1tQmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICBjb25zdCBhc3NldFVSTCA9IG90bW1CYXNlVXJsICsgam9iLmpvYl9kZXRhaWxzLnJlbGF0aXZlX2Rvd25sb2FkX3VybC5zbGljZSgxKTtcclxuICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmluaXRpYXRlRG93bmxvYWQoYXNzZXRVUkwsIGpvYi5qb2JfZGV0YWlscy5leHBvcnRfemlwX25hbWUpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZG93bmxvYWRpbmcgZmlsZScpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgdGhpcy5vdG1tU2VydmljZS5jaGVja090bW1TZXNzaW9uKClcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmdldEV4cG9ydEpvYnMoKTtcclxuICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubm9EYXRhTXNnID0gJ1VuYWJsZSB0byBjb25uZWN0IHRvIE1lZGlhIE1hbmFnZXIuJztcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1VuYWJsZSB0byBpbml0aWFsaXplIE9UTU0gc2Vzc2lvbicpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==