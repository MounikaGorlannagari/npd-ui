import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LoaderService } from '../../../loader/loader.service';
import { OTMMService } from '../../../mpm-utils/services/otmm.service';
import { NotificationService } from '../../../notification/notification.service';
import { QdsService } from '../../../upload/services/qds.service';
import { UtilService } from '../../../mpm-utils/services/util.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { AppService } from '../../../mpm-utils/services/app.service';
import { DomSanitizer } from '@angular/platform-browser';
import { saveAs } from 'file-saver';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
var ExportDataTrayComponent = /** @class */ (function () {
    function ExportDataTrayComponent(loaderService, otmmService, notificationService, qdsService, sharingService, utilService, appService, sanitizer, dialog) {
        this.loaderService = loaderService;
        this.otmmService = otmmService;
        this.notificationService = notificationService;
        this.qdsService = qdsService;
        this.sharingService = sharingService;
        this.utilService = utilService;
        this.appService = appService;
        this.sanitizer = sanitizer;
        this.dialog = dialog;
        this.displayedColumns = ['fileName', 'exportType', 'time', 'status', 'action', 'delete'];
        this.isLoadingData = false;
        this.noData = false;
        this.showActions = true;
        this.exportTraySort = new MatSort();
    }
    ExportDataTrayComponent.prototype.getExportData = function () {
        var _this = this;
        this.isLoadingData = true;
        var arr = [];
        this.appService.getExportDataByUser(this.loggedInUser).subscribe(function (response) {
            if (response && response.MPM_Export_Data) {
                if (!Array.isArray(response.MPM_Export_Data)) {
                    response.MPM_Export_Data = [response.MPM_Export_Data];
                }
                var exportDataList = response.MPM_Export_Data;
                exportDataList.forEach(function (data) {
                    arr.push({
                        fileName: data.FILE_NAME,
                        status: data.STATUS,
                        time: data.Tracking.LastModifiedDate,
                        exportType: data.EXPORT_TYPE,
                        filePath: data.FILE_PATH,
                        exportDataId: data['MPM_Export_Data-id'].Id
                    });
                });
                _this.isLoadingData = false;
                _this.dataSource = new MatTableDataSource(arr);
                _this.dataSource.sort = _this.exportTraySort;
                _this.exportTraySort.disableClear = true;
            }
            else {
                _this.dataSource = new MatTableDataSource(arr);
                _this.isLoadingData = false;
                _this.noData = true;
                _this.noDataMsg = 'No items.';
            }
        }, function () {
            _this.loaderService.hide();
            _this.noData = true;
            _this.noDataMsg = 'Unable to get data';
            _this.isLoadingData = false;
            _this.notificationService.error('Something went wrong while getting exported data');
        });
    };
    ExportDataTrayComponent.prototype.ngAfterViewInit = function () {
        this.loggedInUser = this.sharingService.getCurrentUserCN();
        this.loggedInUserName = this.sharingService.getCurrentUserDisplayName();
        this.getExportData();
    };
    ExportDataTrayComponent.prototype.refreshData = function () {
        this.getExportData();
    };
    ExportDataTrayComponent.prototype.downloadFile = function (encodedString, fileNameWithExtension) {
        var byteString = window.atob(encodedString);
        var arrayBuffer = new ArrayBuffer(byteString.length);
        var int8Array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < byteString.length; i++) {
            int8Array[i] = byteString.charCodeAt(i);
        } //for each or for of
        // const categoryName = this.templateform.controls.category.value['CATEGORY_NAME'];
        // const timeStamp = new Date().getTime();
        // this.documentName = categoryName + '_' + this.userID + '_' + timeStamp + '.xlsx';
        var blob = new Blob([int8Array], { type: 'application/octet-stream' });
        this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
        saveAs(blob, fileNameWithExtension);
    };
    ExportDataTrayComponent.prototype.download = function (data) {
        var _this = this;
        this.appService.getFileEncodedString(data.filePath).subscribe(function (response) {
            if (response && response.tuple && response.tuple.old && response.tuple.old.downloadFileByPath && response.tuple.old.downloadFileByPath.downloadFileByPath) {
                var encodedString = response.tuple.old.downloadFileByPath.downloadFileByPath;
                _this.downloadFile(encodedString, _this.createFileExtension(data));
            }
        });
    };
    ExportDataTrayComponent.prototype.createFileExtension = function (exportData) {
        return exportData.exportType === "EXCEL" ? exportData.fileName.concat(".xlsx") : exportData.fileName.concat(".csv");
    };
    ExportDataTrayComponent.prototype.deleteExportTray = function (exportData) {
        var _this = this;
        var dialogueRef = this.dialog.open(ConfirmationModalComponent, {
            width: '40%',
            disableClose: true,
            data: {
                message: "Are you sure to delete the " + this.createFileExtension(exportData) + " from the Export Tray and Server?",
                submitButton: 'YES',
                cancelButton: 'NO'
            }
        });
        dialogueRef.afterClosed().subscribe(function (response) {
            if (response) {
                _this.appService.deleteExportDataById(exportData.exportDataId).subscribe(function (deleteExportTrayResponse) {
                    _this.notificationService.success(_this.createFileExtension(exportData) + " from Export Tray deleted successfully");
                    _this.refreshData();
                }, function () {
                    _this.notificationService.error(_this.createFileExtension(exportData) + "Export Tray operation failed, Please try later");
                });
            }
        });
    };
    ExportDataTrayComponent.ctorParameters = function () { return [
        { type: LoaderService },
        { type: OTMMService },
        { type: NotificationService },
        { type: QdsService },
        { type: SharingService },
        { type: UtilService },
        { type: AppService },
        { type: DomSanitizer },
        { type: MatDialog }
    ]; };
    __decorate([
        ViewChild(MatSort, { static: true })
    ], ExportDataTrayComponent.prototype, "exportTraySort", void 0);
    ExportDataTrayComponent = __decorate([
        Component({
            selector: 'mpm-export-data-tray',
            template: "<mat-spinner diameter=\"40\" *ngIf=\"isLoadingData\"></mat-spinner>\r\n\r\n<mat-card class=\"qds-transfer-actions\" [hidden]=\"isLoadingData\">\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-spacer\"></div>\r\n        <div class=\"flex-row-item\" style=\"align-items: right;\">\r\n            <span class=\"download-location\">Export Data List</span>\r\n        </div>\r\n        <div class=\"flex-spacer\"></div>\r\n        <div class=\"flex-row-item\">\r\n            <button mat-button class=\"action-button\" (click)=\"refreshData()\" matTooltip=\"Refresh\">\r\n                <mat-icon>refresh</mat-icon>\r\n            </button>\r\n        </div>\r\n        <!-- <div class=\"flex-row-item justify-flex-end\" style=\"align-items: center;\" >\r\n                    <span class=\"download-location\" >{{loggedInUserName}}</span>\r\n                   \r\n            </div> -->\r\n    </div>\r\n</mat-card>\r\n<hr />\r\n\r\n<div class=\"export-data-container\">\r\n    <table mat-table [dataSource]=\"dataSource\" matSort>\r\n        <ng-container matColumnDef=\"fileName\">\r\n            <th style=\"width:25%\" mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\r\n            <td style=\"width:25%\" mat-cell *matCellDef=\"let element\"> {{element.fileName}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"exportType\">\r\n            <th style=\"width:15%\" mat-header-cell *matHeaderCellDef mat-sort-header> Export Type </th>\r\n            <td style=\"width:15%\" mat-cell *matCellDef=\"let element\"> {{element.exportType}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"time\">\r\n            <th style=\"width:25%\" mat-header-cell *matHeaderCellDef mat-sort-header> Exported Time </th>\r\n            <td style=\"width:25%\" mat-cell *matCellDef=\"let element\"> {{element.time | formatToLocaleDateTime}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"status\">\r\n            <th style=\"width:15%\" mat-header-cell *matHeaderCellDef mat-sort-header> Status </th>\r\n            <td style=\"width:15%\" mat-cell *matCellDef=\"let element\"> {{element.status}} </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"action\">\r\n            <th style=\"width:10%\" mat-header-cell *matHeaderCellDef> Action </th>\r\n            <td style=\"width:10%\" mat-cell *matCellDef=\"let element\">\r\n                <button mat-icon-button aria-label=\"Download\" matTooltip=\"Download\"\r\n                    [disabled]=\"element.status != 'Completed'\" (click)=\"download(element)\">\r\n                    <mat-icon class=\"success\">vertical_align_bottom</mat-icon>\r\n                </button>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <ng-container matColumnDef=\"delete\">\r\n            <th mat-header-cell *matHeaderCellDef>Delete</th>\r\n            <td mat-cell *matCellDef=\"let element\">\r\n                <button mat-icon-button aria-label=\"Delete\" matTooltip=\"Delete\" (click)=\"deleteExportTray(element)\">\r\n                    <mat-icon class=\"mat-18\">delete</mat-icon>\r\n                </button>\r\n            </td>\r\n        </ng-container>\r\n\r\n        <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n    </table>\r\n</div>\r\n\r\n<div *ngIf=\"noData && !isLoadingData\" class=\"no-data\">\r\n    <span>{{noDataMsg}}</span>\r\n</div>",
            styles: [".float-right{float:right}.justify-flex-end{justify-content:flex-end}.flex-spacer{display:flex;flex-grow:5}.export-data-container{overflow:hidden}.export-data-container table{width:100%}mat-spinner.mat-progress-spinner{position:absolute;z-index:999999;top:48vh;left:48vw}.success{color:green}.info{color:#00f}.danger{color:red}.warning{color:orange}.no-data{padding:24px;text-align:center}.qds-transfer-actions{padding:0;border-radius:0}.qds-transfer-actions .flex-row{align-items:center}.qds-transfer-actions .flex-row .flex-row-item{flex-grow:0!important}.qds-transfer-actions mat-checkbox{padding:0 10px}.qds-transfer-actions .action-button{border-radius:0}.qds-transfer-actions .download-location{font-size:15px;max-width:calc(100vh - 300px);white-space:nowrap;overflow:hidden;text-overflow:ellipsis}"]
        })
    ], ExportDataTrayComponent);
    return ExportDataTrayComponent;
}());
export { ExportDataTrayComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEtdHJheS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9leHBvcnQtZGF0YS10cmF5L2V4cG9ydC1kYXRhLXRyYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUF5QixTQUFTLEVBQWdCLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzdELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDakYsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ3BDLE9BQU8sRUFBRSxPQUFPLEVBQVEsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDckQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFRaEc7SUFjRSxpQ0FDUyxhQUE0QixFQUM1QixXQUF3QixFQUN4QixtQkFBd0MsRUFDeEMsVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsVUFBc0IsRUFDdEIsU0FBdUIsRUFDdkIsTUFBaUI7UUFSakIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQXJCMUIscUJBQWdCLEdBQWEsQ0FBQyxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRTlGLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBRXRCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUttQixtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFZakUsQ0FBQztJQUVMLCtDQUFhLEdBQWI7UUFBQSxpQkF5Q0M7UUF4Q0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBTSxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUN2RSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsZUFBZSxFQUFFO2dCQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLEVBQUU7b0JBQzVDLFFBQVEsQ0FBQyxlQUFlLEdBQUcsQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUM7aUJBQ3ZEO2dCQUNELElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7Z0JBQzlDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUN6QixHQUFHLENBQUMsSUFBSSxDQUFDO3dCQUNQLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUzt3QkFDeEIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUNuQixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0I7d0JBQ3BDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVzt3QkFDNUIsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTO3dCQUN4QixZQUFZLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRTtxQkFDNUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2dCQUNILEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzNDLEtBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTthQUV4QztpQkFDSTtnQkFDSCxLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsS0FBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUM7YUFDOUI7UUFFSCxDQUFDLEVBQUU7WUFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFCLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQW9CLENBQUM7WUFDdEMsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDM0IsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1FBRXJGLENBQUMsQ0FBQyxDQUFDO0lBRUwsQ0FBQztJQUNELGlEQUFlLEdBQWY7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUMzRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1FBQ3hFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsNkNBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsOENBQVksR0FBWixVQUFhLGFBQWEsRUFBRSxxQkFBcUI7UUFDL0MsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5QyxJQUFNLFdBQVcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdkQsSUFBTSxTQUFTLEdBQUcsSUFBSSxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDOUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekMsQ0FBQSxvQkFBb0I7UUFDckIsbUZBQW1GO1FBQ25GLDBDQUEwQztRQUMxQyxvRkFBb0Y7UUFDcEYsSUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSwwQkFBMEIsRUFBRSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUMxRCxNQUFNLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FDakMsQ0FBQztRQUNGLE1BQU0sQ0FBQyxJQUFJLEVBQUUscUJBQXFCLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsMENBQVEsR0FBUixVQUFTLElBQUk7UUFBYixpQkFPQztRQU5DLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDcEUsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDekosSUFBTSxhQUFhLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUM7Z0JBQy9FLEtBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ2xFO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQscURBQW1CLEdBQW5CLFVBQW9CLFVBQVU7UUFDNUIsT0FBTyxVQUFVLENBQUMsVUFBVSxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RILENBQUM7SUFFRCxrREFBZ0IsR0FBaEIsVUFBaUIsVUFBVTtRQUEzQixpQkFvQkM7UUFuQkMsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDL0QsS0FBSyxFQUFFLEtBQUs7WUFDWixZQUFZLEVBQUUsSUFBSTtZQUNsQixJQUFJLEVBQUU7Z0JBQ0osT0FBTyxFQUFFLDZCQUE2QixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxtQ0FBbUM7Z0JBQ25ILFlBQVksRUFBRSxLQUFLO2dCQUNuQixZQUFZLEVBQUUsSUFBSTthQUNuQjtTQUNGLENBQUMsQ0FBQTtRQUNGLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQzFDLElBQUksUUFBUSxFQUFFO2dCQUNaLEtBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLHdCQUF3QjtvQkFDOUYsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEdBQUcsd0NBQXdDLENBQUMsQ0FBQztvQkFDbEgsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNyQixDQUFDLEVBQUU7b0JBQ0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEdBQUcsZ0RBQWdELENBQUMsQ0FBQztnQkFDMUgsQ0FBQyxDQUFDLENBQUE7YUFDSDtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQzs7Z0JBakh1QixhQUFhO2dCQUNmLFdBQVc7Z0JBQ0gsbUJBQW1CO2dCQUM1QixVQUFVO2dCQUNOLGNBQWM7Z0JBQ2pCLFdBQVc7Z0JBQ1osVUFBVTtnQkFDWCxZQUFZO2dCQUNmLFNBQVM7O0lBWFk7UUFBckMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQzttRUFBZ0M7SUFaMUQsdUJBQXVCO1FBTG5DLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMseStHQUFnRDs7U0FFakQsQ0FBQztPQUNXLHVCQUF1QixDQWlJbkM7SUFBRCw4QkFBQztDQUFBLEFBaklELElBaUlDO1NBaklZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdJbml0LCBWaWV3Q2hpbGQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9sb2FkZXIvbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL21wbS11dGlscy9zZXJ2aWNlcy9vdG1tLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHsgc2F2ZUFzIH0gZnJvbSAnZmlsZS1zYXZlcic7XHJcbmltcG9ydCB7IE1hdFNvcnQsIFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0JztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEUgfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tZXhwb3J0LWRhdGEtdHJheScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2V4cG9ydC1kYXRhLXRyYXkuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2V4cG9ydC1kYXRhLXRyYXkuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRXhwb3J0RGF0YVRyYXlDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuXHJcbiAgZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbJ2ZpbGVOYW1lJywgJ2V4cG9ydFR5cGUnLCAndGltZScsICdzdGF0dXMnLCAnYWN0aW9uJywgJ2RlbGV0ZSddO1xyXG4gIGRhdGFTb3VyY2U6IE1hdFRhYmxlRGF0YVNvdXJjZTxhbnk+O1xyXG4gIGlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICBub0RhdGFNc2c6IHN0cmluZztcclxuICBub0RhdGEgPSBmYWxzZTtcclxuICBzaG93QWN0aW9ucyA9IHRydWU7XHJcbiAgbG9nZ2VkSW5Vc2VyO1xyXG4gIGxvZ2dlZEluVXNlck5hbWU7XHJcbiAgZmlsZVVybDtcclxuXHJcbiAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBleHBvcnRUcmF5U29ydCA9IG5ldyBNYXRTb3J0KCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBwdWJsaWMgb3RtbVNlcnZpY2U6IE9UTU1TZXJ2aWNlLFxyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwdWJsaWMgcWRzU2VydmljZTogUWRzU2VydmljZSxcclxuICAgIHB1YmxpYyBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgdXRpbFNlcnZpY2U6IFV0aWxTZXJ2aWNlLFxyXG4gICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIsXHJcbiAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2dcclxuICApIHsgfVxyXG5cclxuICBnZXRFeHBvcnREYXRhKCkge1xyXG4gICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gdHJ1ZTtcclxuICAgIGNvbnN0IGFyciA9IFtdO1xyXG4gICAgdGhpcy5hcHBTZXJ2aWNlLmdldEV4cG9ydERhdGFCeVVzZXIodGhpcy5sb2dnZWRJblVzZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5NUE1fRXhwb3J0X0RhdGEpIHtcclxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkocmVzcG9uc2UuTVBNX0V4cG9ydF9EYXRhKSkge1xyXG4gICAgICAgICAgcmVzcG9uc2UuTVBNX0V4cG9ydF9EYXRhID0gW3Jlc3BvbnNlLk1QTV9FeHBvcnRfRGF0YV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBleHBvcnREYXRhTGlzdCA9IHJlc3BvbnNlLk1QTV9FeHBvcnRfRGF0YTtcclxuICAgICAgICBleHBvcnREYXRhTGlzdC5mb3JFYWNoKGRhdGEgPT4ge1xyXG4gICAgICAgICAgYXJyLnB1c2goe1xyXG4gICAgICAgICAgICBmaWxlTmFtZTogZGF0YS5GSUxFX05BTUUsXHJcbiAgICAgICAgICAgIHN0YXR1czogZGF0YS5TVEFUVVMsXHJcbiAgICAgICAgICAgIHRpbWU6IGRhdGEuVHJhY2tpbmcuTGFzdE1vZGlmaWVkRGF0ZSxcclxuICAgICAgICAgICAgZXhwb3J0VHlwZTogZGF0YS5FWFBPUlRfVFlQRSxcclxuICAgICAgICAgICAgZmlsZVBhdGg6IGRhdGEuRklMRV9QQVRILFxyXG4gICAgICAgICAgICBleHBvcnREYXRhSWQ6IGRhdGFbJ01QTV9FeHBvcnRfRGF0YS1pZCddLklkXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmlzTG9hZGluZ0RhdGEgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKGFycik7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlLnNvcnQgPSB0aGlzLmV4cG9ydFRyYXlTb3J0O1xyXG4gICAgICAgIHRoaXMuZXhwb3J0VHJheVNvcnQuZGlzYWJsZUNsZWFyID0gdHJ1ZVxyXG5cclxuICAgICAgfVxyXG4gICAgICBlbHNlIHtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKGFycik7XHJcbiAgICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5ub0RhdGEgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMubm9EYXRhTXNnID0gJ05vIGl0ZW1zLic7XHJcbiAgICAgIH1cclxuXHJcbiAgICB9LCAoKSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIHRoaXMubm9EYXRhID0gdHJ1ZTtcclxuICAgICAgdGhpcy5ub0RhdGFNc2cgPSAnVW5hYmxlIHRvIGdldCBkYXRhJztcclxuICAgICAgdGhpcy5pc0xvYWRpbmdEYXRhID0gZmFsc2U7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBleHBvcnRlZCBkYXRhJyk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gIH1cclxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmxvZ2dlZEluVXNlciA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJDTigpO1xyXG4gICAgdGhpcy5sb2dnZWRJblVzZXJOYW1lID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckRpc3BsYXlOYW1lKCk7XHJcbiAgICB0aGlzLmdldEV4cG9ydERhdGEoKTtcclxuICB9XHJcblxyXG4gIHJlZnJlc2hEYXRhKCkge1xyXG4gICAgdGhpcy5nZXRFeHBvcnREYXRhKCk7XHJcbiAgfVxyXG5cclxuICBkb3dubG9hZEZpbGUoZW5jb2RlZFN0cmluZywgZmlsZU5hbWVXaXRoRXh0ZW5zaW9uKSB7XHJcbiAgICBjb25zdCBieXRlU3RyaW5nID0gd2luZG93LmF0b2IoZW5jb2RlZFN0cmluZyk7XHJcbiAgICBjb25zdCBhcnJheUJ1ZmZlciA9IG5ldyBBcnJheUJ1ZmZlcihieXRlU3RyaW5nLmxlbmd0aCk7XHJcbiAgICBjb25zdCBpbnQ4QXJyYXkgPSBuZXcgVWludDhBcnJheShhcnJheUJ1ZmZlcik7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGJ5dGVTdHJpbmcubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaW50OEFycmF5W2ldID0gYnl0ZVN0cmluZy5jaGFyQ29kZUF0KGkpO1xyXG4gICAgfS8vZm9yIGVhY2ggb3IgZm9yIG9mXHJcbiAgICAvLyBjb25zdCBjYXRlZ29yeU5hbWUgPSB0aGlzLnRlbXBsYXRlZm9ybS5jb250cm9scy5jYXRlZ29yeS52YWx1ZVsnQ0FURUdPUllfTkFNRSddO1xyXG4gICAgLy8gY29uc3QgdGltZVN0YW1wID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XHJcbiAgICAvLyB0aGlzLmRvY3VtZW50TmFtZSA9IGNhdGVnb3J5TmFtZSArICdfJyArIHRoaXMudXNlcklEICsgJ18nICsgdGltZVN0YW1wICsgJy54bHN4JztcclxuICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbaW50OEFycmF5XSwgeyB0eXBlOiAnYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtJyB9KTtcclxuICAgIHRoaXMuZmlsZVVybCA9IHRoaXMuc2FuaXRpemVyLmJ5cGFzc1NlY3VyaXR5VHJ1c3RSZXNvdXJjZVVybChcclxuICAgICAgd2luZG93LlVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYilcclxuICAgICk7XHJcbiAgICBzYXZlQXMoYmxvYiwgZmlsZU5hbWVXaXRoRXh0ZW5zaW9uKTtcclxuICB9XHJcblxyXG4gIGRvd25sb2FkKGRhdGEpIHtcclxuICAgIHRoaXMuYXBwU2VydmljZS5nZXRGaWxlRW5jb2RlZFN0cmluZyhkYXRhLmZpbGVQYXRoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudHVwbGUgJiYgcmVzcG9uc2UudHVwbGUub2xkICYmIHJlc3BvbnNlLnR1cGxlLm9sZC5kb3dubG9hZEZpbGVCeVBhdGggJiYgcmVzcG9uc2UudHVwbGUub2xkLmRvd25sb2FkRmlsZUJ5UGF0aC5kb3dubG9hZEZpbGVCeVBhdGgpIHtcclxuICAgICAgICBjb25zdCBlbmNvZGVkU3RyaW5nID0gcmVzcG9uc2UudHVwbGUub2xkLmRvd25sb2FkRmlsZUJ5UGF0aC5kb3dubG9hZEZpbGVCeVBhdGg7XHJcbiAgICAgICAgdGhpcy5kb3dubG9hZEZpbGUoZW5jb2RlZFN0cmluZywgdGhpcy5jcmVhdGVGaWxlRXh0ZW5zaW9uKGRhdGEpKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVGaWxlRXh0ZW5zaW9uKGV4cG9ydERhdGEpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIGV4cG9ydERhdGEuZXhwb3J0VHlwZSA9PT0gXCJFWENFTFwiID8gZXhwb3J0RGF0YS5maWxlTmFtZS5jb25jYXQoXCIueGxzeFwiKSA6IGV4cG9ydERhdGEuZmlsZU5hbWUuY29uY2F0KFwiLmNzdlwiKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZUV4cG9ydFRyYXkoZXhwb3J0RGF0YSkge1xyXG4gICAgY29uc3QgZGlhbG9ndWVSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LCB7XHJcbiAgICAgIHdpZHRoOiAnNDAlJyxcclxuICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICBkYXRhOiB7XHJcbiAgICAgICAgbWVzc2FnZTogXCJBcmUgeW91IHN1cmUgdG8gZGVsZXRlIHRoZSBcIiArIHRoaXMuY3JlYXRlRmlsZUV4dGVuc2lvbihleHBvcnREYXRhKSArIFwiIGZyb20gdGhlIEV4cG9ydCBUcmF5IGFuZCBTZXJ2ZXI/XCIsXHJcbiAgICAgICAgc3VibWl0QnV0dG9uOiAnWUVTJyxcclxuICAgICAgICBjYW5jZWxCdXR0b246ICdOTydcclxuICAgICAgfVxyXG4gICAgfSlcclxuICAgIGRpYWxvZ3VlUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmRlbGV0ZUV4cG9ydERhdGFCeUlkKGV4cG9ydERhdGEuZXhwb3J0RGF0YUlkKS5zdWJzY3JpYmUoZGVsZXRlRXhwb3J0VHJheVJlc3BvbnNlID0+IHtcclxuICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKHRoaXMuY3JlYXRlRmlsZUV4dGVuc2lvbihleHBvcnREYXRhKSArIFwiIGZyb20gRXhwb3J0IFRyYXkgZGVsZXRlZCBzdWNjZXNzZnVsbHlcIik7XHJcbiAgICAgICAgICB0aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKHRoaXMuY3JlYXRlRmlsZUV4dGVuc2lvbihleHBvcnREYXRhKSArIFwiRXhwb3J0IFRyYXkgb3BlcmF0aW9uIGZhaWxlZCwgUGxlYXNlIHRyeSBsYXRlclwiKTtcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxufVxyXG5cclxuXHJcbiJdfQ==