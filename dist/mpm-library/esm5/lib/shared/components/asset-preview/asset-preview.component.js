import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
import { AppDynamicConfig } from '../../services/app.dynamic.config';
var AssetPreviewComponent = /** @class */ (function () {
    function AssetPreviewComponent() {
        this.tempAppDynamicConfig = AppDynamicConfig.getProjectThumbnail();
    }
    AssetPreviewComponent.prototype.handleDefaultAssetURL = function () {
        if (!this.assetURL) {
            this.assetURL = AppDynamicConfig.getProjectThumbnail();
        }
        if (!this.rectangleWidth) {
            this.rectangleWidth = '100%';
        }
        if (!this.rectangleHeight) {
            this.rectangleHeight = '100%';
        }
    };
    AssetPreviewComponent.prototype.ngOnInit = function () {
        this.handleDefaultAssetURL();
    };
    AssetPreviewComponent.prototype.ngOnChanges = function (simpleChanges) {
        console.log(simpleChanges);
        // this.handleDefaultAssetURL();
    };
    __decorate([
        Input()
    ], AssetPreviewComponent.prototype, "assetURL", void 0);
    __decorate([
        Input()
    ], AssetPreviewComponent.prototype, "rectangleWidth", void 0);
    __decorate([
        Input()
    ], AssetPreviewComponent.prototype, "rectangleHeight", void 0);
    __decorate([
        Input()
    ], AssetPreviewComponent.prototype, "isCollapsed", void 0);
    AssetPreviewComponent = __decorate([
        Component({
            selector: 'mpm-asset-preview',
            template: "<div class=\"flex-row\">\r\n    <div class=\"flex-row-item custom-triangle\">\r\n        <mat-card *ngIf=\"assetURL || tempAppDynamicConfig\" class=\"thumbnail-card\">\r\n            <div class=\"flex-col\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row image-holder\">\r\n                        <div class=\"asset-content-area\">\r\n                            <div class=\"asset-holder\" [ngStyle]=\"{'width': rectangleWidth, 'height': rectangleHeight}\">\r\n                                <img src=\"{{assetURL? assetURL : tempAppDynamicConfig}}\" alt=\"asset\" [ngClass]=\"{'collapsed': isCollapsed}\">\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n        <mat-card *ngIf=\"!(assetURL || tempAppDynamicConfig)\" class=\"thumbnail-card\">\r\n            <div class=\"flex-col\">\r\n                <div class=\"flex-col-item\">\r\n                    <div class=\"flex-row image-holder\">\r\n                        <div class=\"asset-content-area\">\r\n                            <div class=\"asset-holder\" [ngStyle]=\"{'width': rectangleWidth, 'height': rectangleHeight}\">\r\n                                \r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n    </div>\r\n</div>",
            styles: ["mat-card.thumbnail-card{padding:0;margin:auto;box-shadow:none!important}div.asset-content-area{align-items:center;justify-content:center;outline:0;background-color:#ddd}div.asset-content-area .asset-holder img{display:block;left:0;right:0;top:0;bottom:0;margin:auto;width:100%;height:100%;max-width:100%;max-height:100%}.custom-triangle{position:relative}.custom-left-triangle:after{content:\"\";position:absolute;top:40%;left:0;width:0;height:0;border-top:10px solid transparent;border-right:15px solid transparent;border-bottom:10px solid transparent}.custom-right-triangle:after{content:\"\";position:absolute;top:40%;right:0;width:0;height:0;border-top:10px solid transparent;border-left:15px solid transparent;border-bottom:10px solid transparent}.custom-top-triangle:after{content:\"\";position:absolute;top:0;left:45%;width:0;height:0;border-left:10px solid transparent;border-bottom:15px solid transparent;border-right:10px solid transparent}.custom-bottom-triangle:after{content:\"\";position:absolute;bottom:0;left:45%;width:0;height:0;border-left:10px solid transparent;border-top:15px solid transparent;border-right:10px solid transparent}.width-empty{width:0;height:0}"]
        })
    ], AssetPreviewComponent);
    return AssetPreviewComponent;
}());
export { AssetPreviewComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXNzZXQtcHJldmlldy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvY29tcG9uZW50cy9hc3NldC1wcmV2aWV3L2Fzc2V0LXByZXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBNEIsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFRckU7SUFTRTtRQUZBLHlCQUFvQixHQUFHLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFFOUMsQ0FBQztJQUVqQixxREFBcUIsR0FBckI7UUFDRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDeEQ7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN4QixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQztTQUM5QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3pCLElBQUksQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDO1NBQy9CO0lBQ0gsQ0FBQztJQUNELHdDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsMkNBQVcsR0FBWCxVQUFZLGFBQTRCO1FBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDM0IsZ0NBQWdDO0lBQ2xDLENBQUM7SUEzQlE7UUFBUixLQUFLLEVBQUU7MkRBQWtCO0lBQ2pCO1FBQVIsS0FBSyxFQUFFO2lFQUF3QjtJQUN2QjtRQUFSLEtBQUssRUFBRTtrRUFBeUI7SUFDeEI7UUFBUixLQUFLLEVBQUU7OERBQXNCO0lBTG5CLHFCQUFxQjtRQUxqQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLGsrQ0FBNkM7O1NBRTlDLENBQUM7T0FDVyxxQkFBcUIsQ0E4QmpDO0lBQUQsNEJBQUM7Q0FBQSxBQTlCRCxJQThCQztTQTlCWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBEeW5hbWljQ29uZmlnIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYXBwLmR5bmFtaWMuY29uZmlnJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1hc3NldC1wcmV2aWV3JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vYXNzZXQtcHJldmlldy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vYXNzZXQtcHJldmlldy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBc3NldFByZXZpZXdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcblxyXG4gIEBJbnB1dCgpIGFzc2V0VVJMOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgcmVjdGFuZ2xlV2lkdGg6IHN0cmluZztcclxuICBASW5wdXQoKSByZWN0YW5nbGVIZWlnaHQ6IHN0cmluZztcclxuICBASW5wdXQoKSBpc0NvbGxhcHNlZDogYm9vbGVhbjtcclxuXHJcbiAgdGVtcEFwcER5bmFtaWNDb25maWcgPSBBcHBEeW5hbWljQ29uZmlnLmdldFByb2plY3RUaHVtYm5haWwoKTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgaGFuZGxlRGVmYXVsdEFzc2V0VVJMKCkge1xyXG4gICAgaWYgKCF0aGlzLmFzc2V0VVJMKSB7XHJcbiAgICAgIHRoaXMuYXNzZXRVUkwgPSBBcHBEeW5hbWljQ29uZmlnLmdldFByb2plY3RUaHVtYm5haWwoKTtcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5yZWN0YW5nbGVXaWR0aCkge1xyXG4gICAgICB0aGlzLnJlY3RhbmdsZVdpZHRoID0gJzEwMCUnO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLnJlY3RhbmdsZUhlaWdodCkge1xyXG4gICAgICB0aGlzLnJlY3RhbmdsZUhlaWdodCA9ICcxMDAlJztcclxuICAgIH1cclxuICB9XHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmhhbmRsZURlZmF1bHRBc3NldFVSTCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkNoYW5nZXMoc2ltcGxlQ2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgY29uc29sZS5sb2coc2ltcGxlQ2hhbmdlcyk7XHJcbiAgICAvLyB0aGlzLmhhbmRsZURlZmF1bHRBc3NldFVSTCgpO1xyXG4gIH1cclxufVxyXG4iXX0=