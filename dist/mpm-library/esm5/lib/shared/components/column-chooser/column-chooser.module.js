import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColumnChooserComponent } from './column-chooser.component';
import { MaterialModule } from '../../../material.module';
import { ColumnChooserFieldComponent } from './column-chooser-field/column-chooser-field.component';
var ColumnChooserModule = /** @class */ (function () {
    function ColumnChooserModule() {
    }
    ColumnChooserModule = __decorate([
        NgModule({
            declarations: [
                ColumnChooserComponent,
                ColumnChooserFieldComponent
            ],
            imports: [
                CommonModule,
                MaterialModule
            ],
            exports: [
                ColumnChooserComponent,
                ColumnChooserFieldComponent
            ]
        })
    ], ColumnChooserModule);
    return ColumnChooserModule;
}());
export { ColumnChooserModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvY29sdW1uLWNob29zZXIvY29sdW1uLWNob29zZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sdURBQXVELENBQUM7QUFrQnBHO0lBQUE7SUFBbUMsQ0FBQztJQUF2QixtQkFBbUI7UUFkL0IsUUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFO2dCQUNaLHNCQUFzQjtnQkFDdEIsMkJBQTJCO2FBQzVCO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLFlBQVk7Z0JBQ1osY0FBYzthQUNmO1lBQ0QsT0FBTyxFQUFFO2dCQUNQLHNCQUFzQjtnQkFDdEIsMkJBQTJCO2FBQzVCO1NBQ0YsQ0FBQztPQUNXLG1CQUFtQixDQUFJO0lBQUQsMEJBQUM7Q0FBQSxBQUFwQyxJQUFvQztTQUF2QixtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDb2x1bW5DaG9vc2VyQ29tcG9uZW50IH0gZnJvbSAnLi9jb2x1bW4tY2hvb3Nlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IENvbHVtbkNob29zZXJGaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29sdW1uLWNob29zZXItZmllbGQvY29sdW1uLWNob29zZXItZmllbGQuY29tcG9uZW50JztcclxuXHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIENvbHVtbkNob29zZXJDb21wb25lbnQsXHJcbiAgICBDb2x1bW5DaG9vc2VyRmllbGRDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBDb2x1bW5DaG9vc2VyQ29tcG9uZW50LFxyXG4gICAgQ29sdW1uQ2hvb3NlckZpZWxkQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29sdW1uQ2hvb3Nlck1vZHVsZSB7IH1cclxuIl19