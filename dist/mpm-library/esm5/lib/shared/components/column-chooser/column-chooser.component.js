import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
var ColumnChooserComponent = /** @class */ (function () {
    function ColumnChooserComponent() {
        // @Output() columnChooserHandler = new EventEmitter<any[]>();
        this.columnChooserHandler = new EventEmitter();
        this.saveColumnChooserHandler = new EventEmitter();
        this.resetColumnChooserHandler = new EventEmitter();
        this.freezeColumnCount = new EventEmitter();
    }
    ColumnChooserComponent.prototype.ngOnChanges = function () {
    };
    ColumnChooserComponent.prototype.handleColumnChooser = function (columnChooserFields) {
        this.columnChooserHandler.next(columnChooserFields);
    };
    ColumnChooserComponent.prototype.saveColumnChooser = function (columnChooserFields) {
        this.saveColumnChooserHandler.next(columnChooserFields);
    };
    ColumnChooserComponent.prototype.resetColumnChooser = function () {
        this.resetColumnChooserHandler.next();
    };
    ColumnChooserComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "columnChooserFields", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "isListView", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "isPMView", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "isColumnsFreezable", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "isAssetsTab", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "columnChooserHandler", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "saveColumnChooserHandler", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "resetColumnChooserHandler", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "freezeColumnCount", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "userPreferenceFreezeCount", void 0);
    ColumnChooserComponent = __decorate([
        Component({
            selector: 'mpm-column-chooser',
            template: "<button class=\"mpm-preference-setting-button\" mat-icon-button [disabled]=\"!isListView\" [matMenuTriggerFor]=\"columnChooserField\" matTooltip=\"Field chooser\">\r\n    <mat-icon color=\"primary\">settings\r\n    </mat-icon>\r\n</button>\r\n<mat-menu #columnChooserField=\"matMenu\">\r\n    <div class=\"columnChooserFields\">\r\n        <mpm-column-chooser-field [columnChooserFields]=\"columnChooserFields\" [isAssetsTab]=\"isAssetsTab\" [isColumnsFreezable]=\"isColumnsFreezable\" [isPMView]=\"isPMView\" [userPreferenceFreezeCount]=\"userPreferenceFreezeCount\" (resetColumnChooserHandler)=\"resetColumnChooser()\"\r\n            (columnChooserHandler)=\"handleColumnChooser($event)\" (saveColumnChooserHandler)=\"saveColumnChooser($event)\">\r\n        </mpm-column-chooser-field>\r\n    </div>\r\n</mat-menu>\r\n<!--(freezeColumnCount)=\"handleFreezeColumn($event)\"-->",
            styles: [".columnChooserFields{padding-left:8px;padding-right:8px}.mpm-preference-setting-button:disabled mat-icon{color:#ddd!important}"]
        })
    ], ColumnChooserComponent);
    return ColumnChooserComponent;
}());
export { ColumnChooserComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvY29sdW1uLWNob29zZXIvY29sdW1uLWNob29zZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFPekc7SUFhRTtRQU5BLDhEQUE4RDtRQUNwRCx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBc0MsQ0FBQztRQUM5RSw2QkFBd0IsR0FBRyxJQUFJLFlBQVksRUFBUyxDQUFDO1FBQ3JELDhCQUF5QixHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDcEQsc0JBQWlCLEdBQUUsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQUVyQyxDQUFDO0lBQ2pCLDRDQUFXLEdBQVg7SUFDQSxDQUFDO0lBSUQsb0RBQW1CLEdBQW5CLFVBQW9CLG1CQUFtQjtRQUNyQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELGtEQUFpQixHQUFqQixVQUFrQixtQkFBbUI7UUFDbkMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxtREFBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDeEMsQ0FBQztJQUVELHlDQUFRLEdBQVI7SUFFQSxDQUFDO0lBL0JRO1FBQVIsS0FBSyxFQUFFO3VFQUFxQjtJQUNwQjtRQUFSLEtBQUssRUFBRTs4REFBb0I7SUFDbkI7UUFBUixLQUFLLEVBQUU7NERBQVU7SUFDVDtRQUFSLEtBQUssRUFBRTtzRUFBb0I7SUFDbkI7UUFBUixLQUFLLEVBQUU7K0RBQWE7SUFFWDtRQUFULE1BQU0sRUFBRTt3RUFBK0U7SUFDOUU7UUFBVCxNQUFNLEVBQUU7NEVBQXNEO0lBQ3JEO1FBQVQsTUFBTSxFQUFFOzZFQUFxRDtJQUNwRDtRQUFULE1BQU0sRUFBRTtxRUFBNEM7SUFDNUM7UUFBUixLQUFLLEVBQUU7NkVBQTJCO0lBWnhCLHNCQUFzQjtRQUxsQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLHkzQkFBOEM7O1NBRS9DLENBQUM7T0FDVyxzQkFBc0IsQ0FtQ2xDO0lBQUQsNkJBQUM7Q0FBQSxBQW5DRCxJQW1DQztTQW5DWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLWNvbHVtbi1jaG9vc2VyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29sdW1uLWNob29zZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbHVtbkNob29zZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsT25DaGFuZ2VzIHtcclxuXHJcbiAgQElucHV0KCkgY29sdW1uQ2hvb3NlckZpZWxkcztcclxuICBASW5wdXQoKSBpc0xpc3RWaWV3OmJvb2xlYW47XHJcbiAgQElucHV0KCkgaXNQTVZpZXc7XHJcbiAgQElucHV0KCkgaXNDb2x1bW5zRnJlZXphYmxlO1xyXG4gIEBJbnB1dCgpIGlzQXNzZXRzVGFiO1xyXG4gIC8vIEBPdXRwdXQoKSBjb2x1bW5DaG9vc2VySGFuZGxlciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55W10+KCk7XHJcbiAgQE91dHB1dCgpIGNvbHVtbkNob29zZXJIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjx7J2NvbHVtbic6YW55W10sICdmcmV6ZUNvdW50Jzphbnl9PigpO1xyXG4gIEBPdXRwdXQoKSBzYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueVtdPigpO1xyXG4gIEBPdXRwdXQoKSByZXNldENvbHVtbkNob29zZXJIYW5kbGVyID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcbiAgQE91dHB1dCgpIGZyZWV6ZUNvbHVtbkNvdW50PSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuICBASW5wdXQoKSB1c2VyUHJlZmVyZW5jZUZyZWV6ZUNvdW50O1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcbiAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4gIGhhbmRsZUNvbHVtbkNob29zZXIoY29sdW1uQ2hvb3NlckZpZWxkcykge1xyXG4gICAgdGhpcy5jb2x1bW5DaG9vc2VySGFuZGxlci5uZXh0KGNvbHVtbkNob29zZXJGaWVsZHMpO1xyXG4gIH1cclxuXHJcbiAgc2F2ZUNvbHVtbkNob29zZXIoY29sdW1uQ2hvb3NlckZpZWxkcykge1xyXG4gICAgdGhpcy5zYXZlQ29sdW1uQ2hvb3NlckhhbmRsZXIubmV4dChjb2x1bW5DaG9vc2VyRmllbGRzKTtcclxuICB9XHJcblxyXG4gIHJlc2V0Q29sdW1uQ2hvb3NlcigpIHtcclxuICAgIHRoaXMucmVzZXRDb2x1bW5DaG9vc2VySGFuZGxlci5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIFxyXG4gIH1cclxuXHJcbn1cclxuIl19