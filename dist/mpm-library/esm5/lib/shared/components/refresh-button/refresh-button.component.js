import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
var RefreshButtonComponent = /** @class */ (function () {
    function RefreshButtonComponent() {
        this.clicked = new EventEmitter();
    }
    RefreshButtonComponent.prototype.refresh = function () {
        this.clicked.next();
    };
    RefreshButtonComponent.prototype.ngOnInit = function () {
        this.disabled = this.disabled ? true : false;
    };
    __decorate([
        Input()
    ], RefreshButtonComponent.prototype, "disabled", void 0);
    __decorate([
        Output()
    ], RefreshButtonComponent.prototype, "clicked", void 0);
    RefreshButtonComponent = __decorate([
        Component({
            selector: 'mpm-refresh-button',
            template: "<button mat-icon-button matTooltip=\"Refresh\" (click)=\"refresh()\" [disabled]=\"disabled\">\r\n    <mat-icon color=\"primary\" class=\"mpm-refresh-button\">refresh</mat-icon>\r\n</button>",
            styles: [""]
        })
    ], RefreshButtonComponent);
    return RefreshButtonComponent;
}());
export { RefreshButtonComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVmcmVzaC1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvcmVmcmVzaC1idXR0b24vcmVmcmVzaC1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTy9FO0lBS0U7UUFGVSxZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUV2QixDQUFDO0lBRWpCLHdDQUFPLEdBQVA7UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMvQyxDQUFDO0lBWFE7UUFBUixLQUFLLEVBQUU7NERBQVU7SUFDUjtRQUFULE1BQU0sRUFBRTsyREFBOEI7SUFINUIsc0JBQXNCO1FBTGxDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIseU1BQThDOztTQUUvQyxDQUFDO09BQ1csc0JBQXNCLENBZWxDO0lBQUQsNkJBQUM7Q0FBQSxBQWZELElBZUM7U0FmWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1yZWZyZXNoLWJ1dHRvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3JlZnJlc2gtYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9yZWZyZXNoLWJ1dHRvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZWZyZXNoQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgZGlzYWJsZWQ7XHJcbiAgQE91dHB1dCgpIGNsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHJlZnJlc2goKSB7XHJcbiAgICB0aGlzLmNsaWNrZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVkID0gdGhpcy5kaXNhYmxlZCA/IHRydWUgOiBmYWxzZTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==