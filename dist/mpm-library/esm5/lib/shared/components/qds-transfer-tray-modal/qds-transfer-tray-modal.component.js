import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
var QdsTransferTrayModalComponent = /** @class */ (function () {
    function QdsTransferTrayModalComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.modalTitle = 'QDS File Transfers';
    }
    QdsTransferTrayModalComponent.prototype.cancelDialog = function () {
        this.dialogRef.close();
    };
    QdsTransferTrayModalComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    QdsTransferTrayModalComponent = __decorate([
        Component({
            selector: 'mpm-qds-transfer-tray-modal',
            template: "<div class=\"flex-row\">\r\n    <h2 mat-dialog-title class=\"flex-row-item\">{{modalTitle}}</h2>\r\n    <mat-icon class=\"flex-row-item modal-close\" matTooltip=\"Close\" (click)=\"cancelDialog()\">\r\n        close\r\n    </mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <mpm-qds-transfer-tray></mpm-qds-transfer-tray>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button class=\"action-button\" mat-flat-button (click)=\"cancelDialog()\">Close</button>\r\n</mat-dialog-actions>",
            styles: [".float-right{float:right}.modal-close{text-align:right;cursor:pointer;flex-grow:0}mat-dialog-content{height:65vh;padding:0;width:63vw}mat-form-field{margin:0 25px 0 0}.action-button{border-radius:0}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], QdsTransferTrayModalComponent);
    return QdsTransferTrayModalComponent;
}());
export { QdsTransferTrayModalComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwvcWRzLXRyYW5zZmVyLXRyYXktbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBT3pFO0lBSUUsdUNBQ1MsU0FBc0QsRUFDN0IsSUFBSTtRQUQ3QixjQUFTLEdBQVQsU0FBUyxDQUE2QztRQUM3QixTQUFJLEdBQUosSUFBSSxDQUFBO1FBSnRDLGVBQVUsR0FBRyxvQkFBb0IsQ0FBQztJQUs5QixDQUFDO0lBRUwsb0RBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDekIsQ0FBQzs7Z0JBTm1CLFlBQVk7Z0RBQzdCLE1BQU0sU0FBQyxlQUFlOztJQU5kLDZCQUE2QjtRQUx6QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsNkJBQTZCO1lBQ3ZDLGdoQkFBdUQ7O1NBRXhELENBQUM7UUFPRyxXQUFBLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtPQU5mLDZCQUE2QixDQWF6QztJQUFELG9DQUFDO0NBQUEsQUFiRCxJQWFDO1NBYlksNkJBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ21wbS1xZHMtdHJhbnNmZXItdHJheS1tb2RhbCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3Fkcy10cmFuc2Zlci10cmF5LW1vZGFsLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9xZHMtdHJhbnNmZXItdHJheS1tb2RhbC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBRZHNUcmFuc2ZlclRyYXlNb2RhbENvbXBvbmVudCB7XHJcblxyXG4gIG1vZGFsVGl0bGUgPSAnUURTIEZpbGUgVHJhbnNmZXJzJztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8UWRzVHJhbnNmZXJUcmF5TW9kYWxDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhXHJcbiAgKSB7IH1cclxuXHJcbiAgY2FuY2VsRGlhbG9nKCkge1xyXG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==