import { __decorate, __param } from "tslib";
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../../mpm-utils/services/app.service';
import { LoaderService } from '../../../loader/loader.service';
import { NotificationService } from '../../../notification/notification.service';
import { SharingService } from '../../../mpm-utils/services/sharing.service';
import { ApplicationConfigConstants } from '../../constants/application.config.constants';
var ExportDataComponent = /** @class */ (function () {
    function ExportDataComponent(dialogRef, data, appService, formBuilder, loaderService, sharingService, notificationService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.loaderService = loaderService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.exportType = [
            { value: 'EXCEL', viewValue: 'Excel' },
            { value: 'CSV', viewValue: 'CSV' }
        ];
    }
    ExportDataComponent.prototype.ngOnInit = function () {
        this.initialiseForm();
        this.appConfig = this.sharingService.getAppConfig();
        this.filePath = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_FILE_PATH];
        this.pageLimit = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_PAGE_LIMIT];
        this.exportDataMinCount = this.appConfig[ApplicationConfigConstants.MPM_APP_CONFIG.EXPORT_DATA_MIN_COUNT];
        console.log(this.data);
        console.log(this.appConfig, 'appconfig');
        console.log(this.filePath);
    };
    ExportDataComponent.prototype.onExportData = function () {
        var _this = this;
        this.loaderService.show();
        var itemIds = [];
        // this.data.exportData.forEach(data => {
        //   itemIds.push(
        //      data.ITEM_ID
        // );
        // });
        this.appService.exportData(this.data.columns, this.exportDataForm.value.exportFileName, this.exportDataForm.value.exportType, itemIds, this.filePath, this.data.searchRequest, this.pageLimit, this.data.totalCount, this.exportDataMinCount).subscribe(function (response) {
            _this.notificationService.success("Export Data Initiated Successfully! Check in Export Data Tray for the details");
            _this.loaderService.hide();
            _this.dialogRef.close(false);
        }, function () {
            _this.loaderService.hide();
            _this.notificationService.error('Something went wrong while exporting data');
        });
    };
    ExportDataComponent.prototype.initialiseForm = function () {
        var disableField = false;
        this.exportDataForm = null;
        this.exportDataForm = this.formBuilder.group({
            exportFileName: new FormControl('', [Validators.required, Validators.maxLength(120)]),
            exportType: new FormControl('', [Validators.required])
        });
    };
    ExportDataComponent.prototype.closeDialog = function () {
        this.dialogRef.close(false);
    };
    ExportDataComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: AppService },
        { type: FormBuilder },
        { type: LoaderService },
        { type: SharingService },
        { type: NotificationService }
    ]; };
    ExportDataComponent = __decorate([
        Component({
            selector: 'mpm-export-data',
            template: "<h1 mat-dialog-title class=\"export-data-modal-title\">Export Data</h1>\r\n<button matTooltip=\"Close\" mat-icon-button color=\"primary\" (click)=\"closeDialog()\" [mat-dialog-close]=\"true\"\r\n    class=\"mat-icon-close-btn\">\r\n    <mat-icon>close</mat-icon>\r\n</button>\r\n<mat-divider></mat-divider>\r\n<form class=\"export-data-form\" [formGroup]=\"exportDataForm\">\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Export File Name</mat-label>\r\n                <input appAutoFocus matInput placeholder=\"Export File Name\" formControlName=\"exportFileName\" required>\r\n\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item max-width\">\r\n            <mat-form-field appearance=\"outline\">\r\n                <mat-label>Export Type</mat-label>\r\n                <mat-select formControlName=\"exportType\" placeholder=\"Export Type\" name=\"exportType\" required>\r\n                    <mat-option *ngFor=\"let type of exportType\" value=\"{{type.value}}\">\r\n                        {{type.viewValue}}\r\n                    </mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>\r\n    </div>\r\n    <div class=\"flex-row\">\r\n        <div class=\"flex-row-item\">\r\n            <span class=\"form-actions\">\r\n                <button mat-stroked-button type=\"button\" (click)=\"closeDialog()\">\r\n                    Close\r\n                </button>\r\n                <button id=\"bulk-comments-button\" mat-stroked-button color=\"primary\" type=\"button\"\r\n                    (click)=\"onExportData()\" [disabled]=\"(exportDataForm.invalid)\">\r\n                    Done\r\n                </button>\r\n            </span>\r\n        </div>\r\n    </div>\r\n</form>",
            styles: [".export-data-modal-title{font-weight:700;flex-grow:1}mat-form-field{width:100%}.export-data-form{display:flex;flex-direction:column;width:100%}span.form-actions{display:flex;margin-left:auto;margin-bottom:10px}#bulk-comments-button{margin-right:0;border-radius:4px;margin-left:15px}.mat-icon-close-btn{float:right;top:-57px}"]
        }),
        __param(1, Inject(MAT_DIALOG_DATA))
    ], ExportDataComponent);
    return ExportDataComponent;
}());
export { ExportDataComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwb3J0LWRhdGEuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbXBvbmVudHMvZXhwb3J0LWRhdGEvZXhwb3J0LWRhdGEuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQU8xRjtJQWFFLDZCQUNTLFNBQTRDLEVBQ25CLElBQVMsRUFDakMsVUFBc0IsRUFDdEIsV0FBd0IsRUFDeEIsYUFBNEIsRUFDNUIsY0FBOEIsRUFDL0IsbUJBQXdDO1FBTnhDLGNBQVMsR0FBVCxTQUFTLENBQW1DO1FBQ25CLFNBQUksR0FBSixJQUFJLENBQUs7UUFDakMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDL0Isd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVpqRCxlQUFVLEdBQUc7WUFDWCxFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRTtZQUN0QyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRTtTQUNyQyxDQUFDO0lBVUksQ0FBQztJQUVMLHNDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNoRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDbEcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsMEJBQTBCLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDMUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFDLFdBQVcsQ0FBQyxDQUFBO1FBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQzVCLENBQUM7SUFFRCwwQ0FBWSxHQUFaO1FBQUEsaUJBaUJDO1FBaEJDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDMUIsSUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ25CLHlDQUF5QztRQUN6QyxrQkFBa0I7UUFDbEIsb0JBQW9CO1FBQ3BCLEtBQUs7UUFDTCxNQUFNO1FBQ04sSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDOVAsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQywrRUFBK0UsQ0FBQyxDQUFDO1lBQ2xILEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUFFO1lBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7UUFFaEYsQ0FBQyxDQUFDLENBQUM7SUFDSCxDQUFDO0lBRUQsNENBQWMsR0FBZDtRQUNFLElBQU0sWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQzNDLGNBQWMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRixVQUFVLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3RELENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Z0JBbERtQixZQUFZO2dEQUM3QixNQUFNLFNBQUMsZUFBZTtnQkFDSCxVQUFVO2dCQUNULFdBQVc7Z0JBQ1QsYUFBYTtnQkFDWixjQUFjO2dCQUNWLG1CQUFtQjs7SUFwQnRDLG1CQUFtQjtRQUwvQixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLHU1REFBMkM7O1NBRTVDLENBQUM7UUFnQkcsV0FBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7T0FmZixtQkFBbUIsQ0FvRS9CO0lBQUQsMEJBQUM7Q0FBQSxBQXBFRCxJQW9FQztTQXBFWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQ29udHJvbCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbG9hZGVyL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2FwcGxpY2F0aW9uLmNvbmZpZy5jb25zdGFudHMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdtcG0tZXhwb3J0LWRhdGEnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9leHBvcnQtZGF0YS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZXhwb3J0LWRhdGEuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRXhwb3J0RGF0YUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIGV4cG9ydERhdGFGb3JtOiBGb3JtR3JvdXA7XHJcbiAgYXBwQ29uZmlnO1xyXG4gIGZpbGVQYXRoO1xyXG4gIHBhZ2VMaW1pdDtcclxuICBleHBvcnREYXRhTWluQ291bnQ7XHJcblxyXG4gIGV4cG9ydFR5cGUgPSBbXHJcbiAgICB7IHZhbHVlOiAnRVhDRUwnLCB2aWV3VmFsdWU6ICdFeGNlbCcgfSxcclxuICAgIHsgdmFsdWU6ICdDU1YnLCB2aWV3VmFsdWU6ICdDU1YnIH1cclxuXTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8RXhwb3J0RGF0YUNvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgIHByaXZhdGUgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBzaGFyaW5nU2VydmljZTogU2hhcmluZ1NlcnZpY2UsXHJcbiAgICBwdWJsaWMgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5pbml0aWFsaXNlRm9ybSgpO1xyXG4gICAgdGhpcy5hcHBDb25maWcgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFwcENvbmZpZygpO1xyXG4gICAgdGhpcy5maWxlUGF0aCA9IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVYUE9SVF9EQVRBX0ZJTEVfUEFUSF07XHJcbiAgICB0aGlzLnBhZ2VMaW1pdCA9IHRoaXMuYXBwQ29uZmlnW0FwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk1QTV9BUFBfQ09ORklHLkVYUE9SVF9EQVRBX1BBR0VfTElNSVRdO1xyXG4gICAgdGhpcy5leHBvcnREYXRhTWluQ291bnQgPSB0aGlzLmFwcENvbmZpZ1tBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5NUE1fQVBQX0NPTkZJRy5FWFBPUlRfREFUQV9NSU5fQ09VTlRdO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5kYXRhKVxyXG4gICAgY29uc29sZS5sb2codGhpcy5hcHBDb25maWcsJ2FwcGNvbmZpZycpXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmZpbGVQYXRoKVxyXG4gIH1cclxuXHJcbiAgb25FeHBvcnREYXRhKCl7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgY29uc3QgaXRlbUlkcyA9IFtdO1xyXG4gICAgLy8gdGhpcy5kYXRhLmV4cG9ydERhdGEuZm9yRWFjaChkYXRhID0+IHtcclxuICAgIC8vICAgaXRlbUlkcy5wdXNoKFxyXG4gICAgLy8gICAgICBkYXRhLklURU1fSURcclxuICAgIC8vICk7XHJcbiAgICAvLyB9KTtcclxuICAgIHRoaXMuYXBwU2VydmljZS5leHBvcnREYXRhKHRoaXMuZGF0YS5jb2x1bW5zLCB0aGlzLmV4cG9ydERhdGFGb3JtLnZhbHVlLmV4cG9ydEZpbGVOYW1lLCB0aGlzLmV4cG9ydERhdGFGb3JtLnZhbHVlLmV4cG9ydFR5cGUsIGl0ZW1JZHMsIHRoaXMuZmlsZVBhdGgsIHRoaXMuZGF0YS5zZWFyY2hSZXF1ZXN0LCB0aGlzLnBhZ2VMaW1pdCwgdGhpcy5kYXRhLnRvdGFsQ291bnQsIHRoaXMuZXhwb3J0RGF0YU1pbkNvdW50KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcyhcIkV4cG9ydCBEYXRhIEluaXRpYXRlZCBTdWNjZXNzZnVsbHkhIENoZWNrIGluIEV4cG9ydCBEYXRhIFRyYXkgZm9yIHRoZSBkZXRhaWxzXCIpO1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZShmYWxzZSk7XHJcbiAgICB9LCAoKSA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5oaWRlKCk7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZXhwb3J0aW5nIGRhdGEnKTtcclxuICBcclxuICB9KTtcclxuICB9XHJcblxyXG4gIGluaXRpYWxpc2VGb3JtKCkge1xyXG4gICAgY29uc3QgZGlzYWJsZUZpZWxkID0gZmFsc2U7XHJcbiAgICB0aGlzLmV4cG9ydERhdGFGb3JtID0gbnVsbDtcclxuICAgIHRoaXMuZXhwb3J0RGF0YUZvcm0gPSB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgZXhwb3J0RmlsZU5hbWU6IG5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTIwKV0pLFxyXG4gICAgICBleHBvcnRUeXBlOiBuZXcgRm9ybUNvbnRyb2woJycsW1ZhbGlkYXRvcnMucmVxdWlyZWRdKVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBjbG9zZURpYWxvZygpIHtcclxuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGZhbHNlKTtcclxuICB9XHJcblxyXG5cclxuXHJcbn1cclxuIl19