import { __decorate } from "tslib";
import { Directive, Input, Output, EventEmitter, HostListener } from '@angular/core';
var CopyClipboardDirective = /** @class */ (function () {
    function CopyClipboardDirective() {
        this.copiedToClipboard = new EventEmitter();
    }
    CopyClipboardDirective.prototype.onClick = function (event) {
        var _this = this;
        event.preventDefault();
        if (!this.copyToClipboard) {
            return;
        }
        var textArea;
        var isOS = function () {
            return navigator.userAgent.match(/ipad|iphone/i);
        };
        var createTextArea = function (text) {
            textArea = document.createElement('textArea');
            textArea.value = text;
            document.body.appendChild(textArea);
        };
        var selectText = function () {
            var range, selection;
            if (isOS()) {
                range = document.createRange();
                range.selectNodeContents(textArea);
                selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
                textArea.setSelectionRange(0, 999999);
            }
            else {
                textArea.select();
            }
        };
        var copyToClipboard = function () {
            document.execCommand('copy');
            document.body.removeChild(textArea);
            _this.copiedToClipboard.emit(_this.copyToClipboard);
        };
        var copyText = function (text) {
            createTextArea(text);
            selectText();
            copyToClipboard();
        };
        copyText(this.copyToClipboard);
    };
    __decorate([
        Input()
    ], CopyClipboardDirective.prototype, "copyToClipboard", void 0);
    __decorate([
        Output()
    ], CopyClipboardDirective.prototype, "copiedToClipboard", void 0);
    __decorate([
        HostListener('click', ['$event'])
    ], CopyClipboardDirective.prototype, "onClick", null);
    CopyClipboardDirective = __decorate([
        Directive({ selector: '[appCopyToClipboard]' })
    ], CopyClipboardDirective);
    return CopyClipboardDirective;
}());
export { CopyClipboardDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29weUNsaXBib2FyZERpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmUvQ29weUNsaXBib2FyZERpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFJckY7SUFBQTtRQUdxQixzQkFBaUIsR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztJQWtEMUYsQ0FBQztJQS9DVSx3Q0FBTyxHQUFkLFVBQWUsS0FBaUI7UUFEaEMsaUJBK0NDO1FBNUNHLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN2QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLFFBQVEsQ0FBQztRQUViLElBQU0sSUFBSSxHQUFHO1lBQ1QsT0FBTyxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNyRCxDQUFDLENBQUM7UUFFRixJQUFNLGNBQWMsR0FBRyxVQUFDLElBQVk7WUFDaEMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDdEIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDO1FBRUYsSUFBTSxVQUFVLEdBQUc7WUFDZixJQUFJLEtBQUssRUFBRSxTQUFTLENBQUM7WUFFckIsSUFBSSxJQUFJLEVBQUUsRUFBRTtnQkFDUixLQUFLLEdBQUcsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUMvQixLQUFLLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ25DLFNBQVMsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ2xDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDNUIsU0FBUyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsUUFBUSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQzthQUN6QztpQkFBTTtnQkFDSCxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDckI7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFNLGVBQWUsR0FBRztZQUNwQixRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RELENBQUMsQ0FBQztRQUVGLElBQU0sUUFBUSxHQUFHLFVBQUMsSUFBWTtZQUMxQixjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsVUFBVSxFQUFFLENBQUM7WUFDYixlQUFlLEVBQUUsQ0FBQztRQUN0QixDQUFDLENBQUM7UUFDRixRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFsRFE7UUFBUixLQUFLLEVBQUU7bUVBQWdDO0lBQzlCO1FBQVQsTUFBTSxFQUFFO3FFQUE2RTtJQUd0RjtRQURDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5REErQ2pDO0lBcERRLHNCQUFzQjtRQUZsQyxTQUFTLENBQUMsRUFBRSxRQUFRLEVBQUUsc0JBQXNCLEVBQUUsQ0FBQztPQUVuQyxzQkFBc0IsQ0FxRGxDO0lBQUQsNkJBQUM7Q0FBQSxBQXJERCxJQXFEQztTQXJEWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbYXBwQ29weVRvQ2xpcGJvYXJkXScgfSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb3B5Q2xpcGJvYXJkRGlyZWN0aXZlIHtcclxuXHJcbiAgICBASW5wdXQoKSBwdWJsaWMgY29weVRvQ2xpcGJvYXJkOiBzdHJpbmc7XHJcbiAgICBAT3V0cHV0KCkgcHVibGljIGNvcGllZFRvQ2xpcGJvYXJkOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnXSlcclxuICAgIHB1YmxpYyBvbkNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCB7XHJcblxyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKCF0aGlzLmNvcHlUb0NsaXBib2FyZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdGV4dEFyZWE7XHJcblxyXG4gICAgICAgIGNvbnN0IGlzT1MgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9pcGFkfGlwaG9uZS9pKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBjcmVhdGVUZXh0QXJlYSA9ICh0ZXh0OiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgdGV4dEFyZWEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZXh0QXJlYScpO1xyXG4gICAgICAgICAgICB0ZXh0QXJlYS52YWx1ZSA9IHRleHQ7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGV4dEFyZWEpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHNlbGVjdFRleHQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCByYW5nZSwgc2VsZWN0aW9uO1xyXG5cclxuICAgICAgICAgICAgaWYgKGlzT1MoKSkge1xyXG4gICAgICAgICAgICAgICAgcmFuZ2UgPSBkb2N1bWVudC5jcmVhdGVSYW5nZSgpO1xyXG4gICAgICAgICAgICAgICAgcmFuZ2Uuc2VsZWN0Tm9kZUNvbnRlbnRzKHRleHRBcmVhKTtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGlvbiA9IHdpbmRvdy5nZXRTZWxlY3Rpb24oKTtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5hZGRSYW5nZShyYW5nZSk7XHJcbiAgICAgICAgICAgICAgICB0ZXh0QXJlYS5zZXRTZWxlY3Rpb25SYW5nZSgwLCA5OTk5OTkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGV4dEFyZWEuc2VsZWN0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBjb3B5VG9DbGlwYm9hcmQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmV4ZWNDb21tYW5kKCdjb3B5Jyk7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGV4dEFyZWEpO1xyXG4gICAgICAgICAgICB0aGlzLmNvcGllZFRvQ2xpcGJvYXJkLmVtaXQodGhpcy5jb3B5VG9DbGlwYm9hcmQpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IGNvcHlUZXh0ID0gKHRleHQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBjcmVhdGVUZXh0QXJlYSh0ZXh0KTtcclxuICAgICAgICAgICAgc2VsZWN0VGV4dCgpO1xyXG4gICAgICAgICAgICBjb3B5VG9DbGlwYm9hcmQoKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvcHlUZXh0KHRoaXMuY29weVRvQ2xpcGJvYXJkKTtcclxuICAgIH1cclxufVxyXG4iXX0=