import { __decorate } from "tslib";
import { Directive, ElementRef, Input } from '@angular/core';
import { NgControl } from '@angular/forms';
var AutofocusDirective = /** @class */ (function () {
    function AutofocusDirective(elementRef, ngControl) {
        this.elementRef = elementRef;
        this.ngControl = ngControl;
    }
    Object.defineProperty(AutofocusDirective.prototype, "appAutoFocus", {
        set: function (condition) {
            if (condition) {
                this.elementRef.nativeElement.focus();
            }
        },
        enumerable: true,
        configurable: true
    });
    AutofocusDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgControl }
    ]; };
    __decorate([
        Input()
    ], AutofocusDirective.prototype, "appAutoFocus", null);
    AutofocusDirective = __decorate([
        Directive({
            selector: '[appAutoFocus]'
        })
    ], AutofocusDirective);
    return AutofocusDirective;
}());
export { AutofocusDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0b2ZvY3VzRGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2RpcmVjdGl2ZS9BdXRvZm9jdXNEaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFNM0M7SUFFSSw0QkFDVyxVQUFzQixFQUN0QixTQUFvQjtRQURwQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGNBQVMsR0FBVCxTQUFTLENBQVc7SUFDM0IsQ0FBQztJQUVJLHNCQUFJLDRDQUFZO2FBQWhCLFVBQWlCLFNBQWtCO1lBQ3hDLElBQUksU0FBUyxFQUFFO2dCQUNYLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3pDO1FBQ0wsQ0FBQzs7O09BQUE7O2dCQVJzQixVQUFVO2dCQUNYLFNBQVM7O0lBR3RCO1FBQVIsS0FBSyxFQUFFOzBEQUlQO0lBWFEsa0JBQWtCO1FBSjlCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7U0FDN0IsQ0FBQztPQUVXLGtCQUFrQixDQWE5QjtJQUFELHlCQUFDO0NBQUEsQUFiRCxJQWFDO1NBYlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ0NvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FwcEF1dG9Gb2N1c10nXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQXV0b2ZvY3VzRGlyZWN0aXZlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgICAgICBwdWJsaWMgbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICAgICkgeyB9XHJcblxyXG4gICAgQElucHV0KCkgc2V0IGFwcEF1dG9Gb2N1cyhjb25kaXRpb246IGJvb2xlYW4pIHtcclxuICAgICAgICBpZiAoY29uZGl0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmZvY3VzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=