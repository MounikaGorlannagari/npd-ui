import { __decorate } from "tslib";
import { NgControl } from '@angular/forms';
import { Directive, Input } from '@angular/core';
var DisableControlDirective = /** @class */ (function () {
    function DisableControlDirective(ngControl) {
        this.ngControl = ngControl;
    }
    Object.defineProperty(DisableControlDirective.prototype, "disableControl", {
        set: function (condition) {
            var action = condition ? 'disable' : 'enable';
            this.ngControl.control[action]();
        },
        enumerable: true,
        configurable: true
    });
    DisableControlDirective.ctorParameters = function () { return [
        { type: NgControl }
    ]; };
    __decorate([
        Input()
    ], DisableControlDirective.prototype, "disableControl", null);
    DisableControlDirective = __decorate([
        Directive({
            // tslint:disable-next-line: directive-selector
            selector: '[disableControl]'
        })
    ], DisableControlDirective);
    return DisableControlDirective;
}());
export { DisableControlDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGlzYWJsZUNvbnRyb2xEaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvZGlyZWN0aXZlL0Rpc2FibGVDb250cm9sRGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPakQ7SUFDSSxpQ0FDVyxTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO0lBQzNCLENBQUM7SUFFSSxzQkFBSSxtREFBYzthQUFsQixVQUFtQixTQUFrQjtZQUMxQyxJQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1lBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7UUFDckMsQ0FBQzs7O09BQUE7O2dCQU5xQixTQUFTOztJQUd0QjtRQUFSLEtBQUssRUFBRTtpRUFHUDtJQVJRLHVCQUF1QjtRQUxuQyxTQUFTLENBQUM7WUFDUCwrQ0FBK0M7WUFDL0MsUUFBUSxFQUFFLGtCQUFrQjtTQUMvQixDQUFDO09BRVcsdUJBQXVCLENBU25DO0lBQUQsOEJBQUM7Q0FBQSxBQVRELElBU0M7U0FUWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ0NvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBkaXJlY3RpdmUtc2VsZWN0b3JcclxuICAgIHNlbGVjdG9yOiAnW2Rpc2FibGVDb250cm9sXSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBEaXNhYmxlQ29udHJvbERpcmVjdGl2ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICAgICkgeyB9XHJcblxyXG4gICAgQElucHV0KCkgc2V0IGRpc2FibGVDb250cm9sKGNvbmRpdGlvbjogYm9vbGVhbikge1xyXG4gICAgICAgIGNvbnN0IGFjdGlvbiA9IGNvbmRpdGlvbiA/ICdkaXNhYmxlJyA6ICdlbmFibGUnO1xyXG4gICAgICAgIHRoaXMubmdDb250cm9sLmNvbnRyb2xbYWN0aW9uXSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==