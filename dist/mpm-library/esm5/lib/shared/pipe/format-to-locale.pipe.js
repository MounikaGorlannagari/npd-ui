import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
import * as acronui from '../../mpm-utils/auth/utility';
var FormatToLocalePipe = /** @class */ (function () {
    function FormatToLocalePipe() {
    }
    FormatToLocalePipe.prototype.transform = function (value, args) {
        var isoStringToLocale = '';
        if (value !== undefined && value !== '') {
            isoStringToLocale = acronui.getDateWithLocaleTimeZone(value);
            isoStringToLocale = isoStringToLocale.toLocaleDateString(navigator.language);
            return isoStringToLocale;
        }
        return '';
    };
    FormatToLocalePipe = __decorate([
        Pipe({
            name: 'formatToLocale'
        })
    ], FormatToLocalePipe);
    return FormatToLocalePipe;
}());
export { FormatToLocalePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRvLWxvY2FsZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BpcGUvZm9ybWF0LXRvLWxvY2FsZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEtBQUssT0FBTyxNQUFNLDhCQUE4QixDQUFDO0FBTXhEO0lBRUk7SUFBZ0IsQ0FBQztJQUVqQixzQ0FBUyxHQUFULFVBQVUsS0FBVSxFQUFFLElBQVU7UUFDNUIsSUFBSSxpQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDaEMsSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDckMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdELGlCQUFpQixHQUFHLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM3RSxPQUFPLGlCQUFpQixDQUFDO1NBQzVCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBWlEsa0JBQWtCO1FBSjlCLElBQUksQ0FBQztZQUNGLElBQUksRUFBRSxnQkFBZ0I7U0FDekIsQ0FBQztPQUVXLGtCQUFrQixDQWM5QjtJQUFELHlCQUFDO0NBQUEsQUFkRCxJQWNDO1NBZFksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uLy4uL21wbS11dGlscy9hdXRoL3V0aWxpdHknO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2Zvcm1hdFRvTG9jYWxlJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZvcm1hdFRvTG9jYWxlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGFyZ3M/OiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGxldCBpc29TdHJpbmdUb0xvY2FsZTogYW55ID0gJyc7XHJcbiAgICAgICAgaWYgKHZhbHVlICE9PSB1bmRlZmluZWQgJiYgdmFsdWUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGlzb1N0cmluZ1RvTG9jYWxlID0gYWNyb251aS5nZXREYXRlV2l0aExvY2FsZVRpbWVab25lKHZhbHVlKTtcclxuICAgICAgICAgICAgaXNvU3RyaW5nVG9Mb2NhbGUgPSBpc29TdHJpbmdUb0xvY2FsZS50b0xvY2FsZURhdGVTdHJpbmcobmF2aWdhdG9yLmxhbmd1YWdlKTtcclxuICAgICAgICAgICAgcmV0dXJuIGlzb1N0cmluZ1RvTG9jYWxlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==