import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
import * as acronui from '../../mpm-utils/auth/utility';
var FormatToLocaleDateTimePipe = /** @class */ (function () {
    function FormatToLocaleDateTimePipe() {
    }
    FormatToLocaleDateTimePipe.prototype.transform = function (value, args) {
        var isoStringToLocale = '';
        if (value !== undefined && value !== '') {
            isoStringToLocale = acronui.getDateWithLocaleTimeZone(value);
            isoStringToLocale = isoStringToLocale.toLocaleString(navigator.language, { day: '2-digit', month: '2-digit', year: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' });
            return isoStringToLocale;
        }
        return '';
    };
    FormatToLocaleDateTimePipe = __decorate([
        Pipe({
            name: 'formatToLocaleDateTime'
        })
    ], FormatToLocaleDateTimePipe);
    return FormatToLocaleDateTimePipe;
}());
export { FormatToLocaleDateTimePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXRvLWxvY2FsZS1kYXRlLXRpbWUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9waXBlL2Zvcm1hdC10by1sb2NhbGUtZGF0ZS10aW1lLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sS0FBSyxPQUFPLE1BQU0sOEJBQThCLENBQUM7QUFNeEQ7SUFFSTtJQUFnQixDQUFDO0lBRWpCLDhDQUFTLEdBQVQsVUFBVSxLQUFVLEVBQUUsSUFBVTtRQUM1QixJQUFJLGlCQUFpQixHQUFRLEVBQUUsQ0FBQztRQUNoQyxJQUFJLEtBQUssS0FBSyxTQUFTLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUNyQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMseUJBQXlCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0QsaUJBQWlCLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQ25FLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO1lBQ2xILE9BQU8saUJBQWlCLENBQUM7U0FDNUI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFiUSwwQkFBMEI7UUFKdEMsSUFBSSxDQUFDO1lBQ0YsSUFBSSxFQUFFLHdCQUF3QjtTQUNqQyxDQUFDO09BRVcsMEJBQTBCLENBZXRDO0lBQUQsaUNBQUM7Q0FBQSxBQWZELElBZUM7U0FmWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vLi4vbXBtLXV0aWxzL2F1dGgvdXRpbGl0eSc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAnZm9ybWF0VG9Mb2NhbGVEYXRlVGltZSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtYXRUb0xvY2FsZURhdGVUaW1lUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGFyZ3M/OiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGxldCBpc29TdHJpbmdUb0xvY2FsZTogYW55ID0gJyc7XHJcbiAgICAgICAgaWYgKHZhbHVlICE9PSB1bmRlZmluZWQgJiYgdmFsdWUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIGlzb1N0cmluZ1RvTG9jYWxlID0gYWNyb251aS5nZXREYXRlV2l0aExvY2FsZVRpbWVab25lKHZhbHVlKTtcclxuICAgICAgICAgICAgaXNvU3RyaW5nVG9Mb2NhbGUgPSBpc29TdHJpbmdUb0xvY2FsZS50b0xvY2FsZVN0cmluZyhuYXZpZ2F0b3IubGFuZ3VhZ2UsXHJcbiAgICAgICAgICAgICAgICB7IGRheTogJzItZGlnaXQnLCBtb250aDogJzItZGlnaXQnLCB5ZWFyOiAnMi1kaWdpdCcsIGhvdXI6ICcyLWRpZ2l0JywgbWludXRlOiAnMi1kaWdpdCcsIHNlY29uZDogJzItZGlnaXQnIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gaXNvU3RyaW5nVG9Mb2NhbGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuXHJcbn1cclxuIl19