import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
import { MPMFieldKeys } from '../../mpm-utils/objects/MPMField';
import { FieldConfigService } from '../services/field-config.service';
var GetPropertyPipe = /** @class */ (function () {
    function GetPropertyPipe(fieldConfigService) {
        this.fieldConfigService = fieldConfigService;
    }
    GetPropertyPipe.prototype.transform = function (projectData, displayColumn, mapperName) {
        var resultValue = null;
        if (!displayColumn && mapperName) {
            displayColumn = this.fieldConfigService.getFieldByKeyValue(MPMFieldKeys.MAPPER_NAME, mapperName);
        }
        if (displayColumn) {
            resultValue = this.fieldConfigService.getFieldValueByDisplayColumn(displayColumn, projectData);
        }
        return resultValue ? resultValue : 'NA';
    };
    GetPropertyPipe.ctorParameters = function () { return [
        { type: FieldConfigService }
    ]; };
    GetPropertyPipe = __decorate([
        Pipe({
            name: 'getProperties'
        })
    ], GetPropertyPipe);
    return GetPropertyPipe;
}());
export { GetPropertyPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0LXByb3BlcnR5LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvcGlwZS9nZXQtcHJvcGVydHkucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFZLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBS3RFO0lBRUUseUJBQ1Usa0JBQXNDO1FBQXRDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7SUFDN0MsQ0FBQztJQUVKLG1DQUFTLEdBQVQsVUFBVSxXQUFnQixFQUFFLGFBQXVCLEVBQUUsVUFBb0I7UUFDdkUsSUFBSSxXQUFXLEdBQVcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxhQUFhLElBQUksVUFBVSxFQUFFO1lBQ2hDLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNsRztRQUNELElBQUksYUFBYSxFQUFFO1lBQ2hCLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ2pHO1FBQ0QsT0FBTyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQzVDLENBQUM7O2dCQVo2QixrQkFBa0I7O0lBSHJDLGVBQWU7UUFIM0IsSUFBSSxDQUFDO1lBQ0osSUFBSSxFQUFFLGVBQWU7U0FDdEIsQ0FBQztPQUNXLGVBQWUsQ0FpQjNCO0lBQUQsc0JBQUM7Q0FBQSxBQWpCRCxJQWlCQztTQWpCWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCwgTVBNRmllbGRLZXlzIH0gZnJvbSAnLi4vLi4vbXBtLXV0aWxzL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBGaWVsZENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcblxyXG5AUGlwZSh7XHJcbiAgbmFtZTogJ2dldFByb3BlcnRpZXMnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBHZXRQcm9wZXJ0eVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZpZWxkQ29uZmlnU2VydmljZTogRmllbGRDb25maWdTZXJ2aWNlXHJcbiApIHsgfVxyXG5cclxuICB0cmFuc2Zvcm0ocHJvamVjdERhdGE6IGFueSwgZGlzcGxheUNvbHVtbjogTVBNRmllbGQsIG1hcHBlck5hbWU/IDogc3RyaW5nKTogdW5rbm93biB7XHJcbiAgICBsZXQgcmVzdWx0VmFsdWU6IHN0cmluZyA9IG51bGw7XHJcbiAgICAgIGlmICghZGlzcGxheUNvbHVtbiAmJiBtYXBwZXJOYW1lKSB7XHJcbiAgICAgICAgZGlzcGxheUNvbHVtbiA9IHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEZpZWxkQnlLZXlWYWx1ZShNUE1GaWVsZEtleXMuTUFQUEVSX05BTUUsIG1hcHBlck5hbWUpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChkaXNwbGF5Q29sdW1uKSB7XHJcbiAgICAgICAgIHJlc3VsdFZhbHVlID0gdGhpcy5maWVsZENvbmZpZ1NlcnZpY2UuZ2V0RmllbGRWYWx1ZUJ5RGlzcGxheUNvbHVtbihkaXNwbGF5Q29sdW1uLCBwcm9qZWN0RGF0YSk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHJlc3VsdFZhbHVlID8gcmVzdWx0VmFsdWUgOiAnTkEnO1xyXG4gIH1cclxuXHJcbn1cclxuIl19