import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl, Validators } from '@angular/forms';
import { NotificationService } from '../notification/notification.service';
var PaginationComponent = /** @class */ (function () {
    function PaginationComponent(notificationService) {
        this.notificationService = notificationService;
        this.paginator = new EventEmitter();
        this.pageSizeOptions = [5, 10, 25, 50, 100];
        this.totalPageNumber = 1;
    }
    PaginationComponent.prototype.calculateTotalPageSize = function () {
        this.pageIndex = this.pageIndex ? Number(this.pageIndex) : (this.matPaginator.pageIndex + 1);
        var pageSize = this.pageSize ? Number(this.pageSize) : this.matPaginator.pageSize;
        this.matPaginator.pageIndex = this.pageIndex - 1;
        this.matPaginator.pageSize = pageSize;
        this.totalPageNumber = Math.ceil(this.length / pageSize);
    };
    PaginationComponent.prototype.changePaginator = function () {
        if (this.pageSize !== this.matPaginator.pageSize) {
            this.pageIndex = 1;
            this.matPaginator.pageIndex = 0;
        }
        else {
            this.pageIndex = this.matPaginator.pageIndex + 1;
        }
        this.pageSize = this.matPaginator.pageSize;
        this.initializeForm();
        var pageObject = {
            length: this.matPaginator.length,
            pageIndex: this.matPaginator.pageIndex,
            pageSize: this.matPaginator.pageSize,
            totalPages: this.totalPageNumber
        };
        this.paginator.next(pageObject);
    };
    PaginationComponent.prototype.onpageValueChange = function (event) {
        if (event.target && event.target.value && (event.relatedTarget == null || event.relatedTarget == undefined)) {
            if (Number(event.target.value) <= this.totalPageNumber) {
                this.matPaginator.pageIndex = Number(event.target.value) - 1;
                this.changePaginator();
            }
            else {
                this.notificationService.error('Current page exceeds total no. of pages');
            }
        }
    };
    PaginationComponent.prototype.initializeForm = function () {
        this.calculateTotalPageSize();
        this.manualPageChangeControl = new FormControl(this.pageIndex, [Validators.max(this.totalPageNumber), Validators.min(1), Validators.required]);
    };
    PaginationComponent.prototype.ngOnChanges = function () {
        this.initializeForm();
    };
    PaginationComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    PaginationComponent.prototype.setFirstPage = function () {
        this.matPaginator.firstPage();
    };
    PaginationComponent.prototype.setLastPage = function () {
        this.matPaginator.lastPage();
    };
    PaginationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initializeForm();
        this.matPaginator.page.subscribe(function () {
            _this.changePaginator();
        });
    };
    PaginationComponent.ctorParameters = function () { return [
        { type: NotificationService }
    ]; };
    __decorate([
        Input()
    ], PaginationComponent.prototype, "length", void 0);
    __decorate([
        Input()
    ], PaginationComponent.prototype, "pageSize", void 0);
    __decorate([
        Input()
    ], PaginationComponent.prototype, "pageIndex", void 0);
    __decorate([
        Output()
    ], PaginationComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatPaginator, { static: true })
    ], PaginationComponent.prototype, "matPaginator", void 0);
    PaginationComponent = __decorate([
        Component({
            selector: 'mpm-pagination',
            template: "<div class=\"pagination\">\r\n    <mat-form-field (keydown.enter)=\"onpageValueChange($event)\" (focusout)=\"onpageValueChange($event)\">\r\n        <input min=\"1\" (keypress)=\"numberOnly($event)\" matInput type=\"number\" [(value)]=\"pageIndex\" [formControl]=\"manualPageChangeControl\">\r\n    </mat-form-field>\r\n    <span class=\"total-page-section-info\"> of {{totalPageNumber}} page{{(totalPageNumber !== 1) ? 's': ''}}</span>\r\n    <mat-paginator class=\"paginator\" [length]=\"length\" [pageSize]=\"pageSize\" [pageSizeOptions]=\"pageSizeOptions\"\r\n        [showFirstLastButtons]=\"true\">\r\n    </mat-paginator>\r\n</div>",
            styles: [".pagination{display:flex;margin-bottom:0}.pagination mat-form-field{width:42px;font-size:12px;margin:4px 4px 0 0}.pagination .total-page-section-info{font-size:12px;margin-top:20px}.mat-paginator{background-color:transparent!important}.mat-paginator-page-size-select{width:45px!important}.mat-paginator-page-size{font-size:10px!important;margin:0!important}.mat-paginator-range-actions .mat-icon-button{width:30px!important}.mat-paginator-range-actions .mat-icon-button .mat-button-wrapper{line-height:30px!important}.mat-paginator-range-label{margin:0!important}"]
        })
    ], PaginationComponent);
    return PaginationComponent;
}());
export { PaginationComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9wYWdpbmF0aW9uL3BhZ2luYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckcsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzNELE9BQU8sRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFTM0U7SUFhSSw2QkFDVyxtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQVR6QyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUk5QyxvQkFBZSxHQUFhLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2pELG9CQUFlLEdBQUcsQ0FBQyxDQUFDO0lBS2hCLENBQUM7SUFFTCxvREFBc0IsR0FBdEI7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDN0YsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDcEYsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBRXRDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRCw2Q0FBZSxHQUFmO1FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFO1lBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztTQUNuQzthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDcEQ7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO1FBRTNDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFNLFVBQVUsR0FBd0I7WUFDcEMsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTTtZQUNoQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTO1lBQ3RDLFFBQVEsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVE7WUFDcEMsVUFBVSxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ25DLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsK0NBQWlCLEdBQWpCLFVBQWtCLEtBQUs7UUFDbkIsSUFBSSxLQUFLLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxTQUFTLENBQUMsRUFBRTtZQUN6RyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3BELElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDN0QsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQzFCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQzthQUM3RTtTQUNKO0lBQ0wsQ0FBQztJQUVELDRDQUFjLEdBQWQ7UUFDSSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFDekQsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ3hGLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUNaLElBQU0sUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzdELElBQUksUUFBUSxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFO1lBQ25ELE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFFaEIsQ0FBQztJQUVELDBDQUFZLEdBQVo7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFBO0lBQ2pDLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtJQUNoQyxDQUFDO0lBQ0Qsc0NBQVEsR0FBUjtRQUFBLGlCQUtDO1FBSkcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUM3QixLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkF6RStCLG1CQUFtQjs7SUFaMUM7UUFBUixLQUFLLEVBQUU7dURBQVE7SUFDUDtRQUFSLEtBQUssRUFBRTt5REFBVTtJQUNUO1FBQVIsS0FBSyxFQUFFOzBEQUFXO0lBQ1Q7UUFBVCxNQUFNLEVBQUU7MERBQXFDO0lBRUg7UUFBMUMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQzs2REFBNEI7SUFQN0QsbUJBQW1CO1FBTi9CLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxnQkFBZ0I7WUFDMUIsMG9CQUEwQzs7U0FFN0MsQ0FBQztPQUVXLG1CQUFtQixDQTBGL0I7SUFBRCwwQkFBQztDQUFBLEFBMUZELElBMEZDO1NBMUZZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkNoYW5nZXMsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcic7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUGFnZU9iamVjdEludGVyZmFjZSB9IGZyb20gJy4vcGFnZS5vYmplY3QuaW50ZXJmYWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtcG0tcGFnaW5hdGlvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGFnaW5hdGlvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9wYWdpbmF0aW9uLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBQYWdpbmF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG5cclxuICAgIEBJbnB1dCgpIGxlbmd0aDtcclxuICAgIEBJbnB1dCgpIHBhZ2VTaXplO1xyXG4gICAgQElucHV0KCkgcGFnZUluZGV4O1xyXG4gICAgQE91dHB1dCgpIHBhZ2luYXRvciA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yLCB7IHN0YXRpYzogdHJ1ZSB9KSBtYXRQYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuXHJcbiAgICBwYWdlU2l6ZU9wdGlvbnM6IG51bWJlcltdID0gWzUsIDEwLCAyNSwgNTAsIDEwMF07XHJcbiAgICB0b3RhbFBhZ2VOdW1iZXIgPSAxO1xyXG4gICAgbWFudWFsUGFnZUNoYW5nZUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGNhbGN1bGF0ZVRvdGFsUGFnZVNpemUoKSB7XHJcbiAgICAgICAgdGhpcy5wYWdlSW5kZXggPSB0aGlzLnBhZ2VJbmRleCA/IE51bWJlcih0aGlzLnBhZ2VJbmRleCkgOiAodGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4ICsgMSk7XHJcbiAgICAgICAgY29uc3QgcGFnZVNpemUgPSB0aGlzLnBhZ2VTaXplID8gTnVtYmVyKHRoaXMucGFnZVNpemUpIDogdGhpcy5tYXRQYWdpbmF0b3IucGFnZVNpemU7XHJcbiAgICAgICAgdGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4ID0gdGhpcy5wYWdlSW5kZXggLSAxO1xyXG4gICAgICAgIHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VTaXplID0gcGFnZVNpemU7XHJcblxyXG4gICAgICAgIHRoaXMudG90YWxQYWdlTnVtYmVyID0gTWF0aC5jZWlsKHRoaXMubGVuZ3RoIC8gcGFnZVNpemUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZVBhZ2luYXRvcigpIHtcclxuICAgICAgICBpZiAodGhpcy5wYWdlU2l6ZSAhPT0gdGhpcy5tYXRQYWdpbmF0b3IucGFnZVNpemUpIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdlSW5kZXggPSAxO1xyXG4gICAgICAgICAgICB0aGlzLm1hdFBhZ2luYXRvci5wYWdlSW5kZXggPSAwO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZUluZGV4ID0gdGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4ICsgMTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wYWdlU2l6ZSA9IHRoaXMubWF0UGFnaW5hdG9yLnBhZ2VTaXplO1xyXG5cclxuICAgICAgICB0aGlzLmluaXRpYWxpemVGb3JtKCk7XHJcbiAgICAgICAgY29uc3QgcGFnZU9iamVjdDogUGFnZU9iamVjdEludGVyZmFjZSA9IHtcclxuICAgICAgICAgICAgbGVuZ3RoOiB0aGlzLm1hdFBhZ2luYXRvci5sZW5ndGgsXHJcbiAgICAgICAgICAgIHBhZ2VJbmRleDogdGhpcy5tYXRQYWdpbmF0b3IucGFnZUluZGV4LFxyXG4gICAgICAgICAgICBwYWdlU2l6ZTogdGhpcy5tYXRQYWdpbmF0b3IucGFnZVNpemUsXHJcbiAgICAgICAgICAgIHRvdGFsUGFnZXM6IHRoaXMudG90YWxQYWdlTnVtYmVyXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLnBhZ2luYXRvci5uZXh0KHBhZ2VPYmplY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIG9ucGFnZVZhbHVlQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50LnRhcmdldCAmJiBldmVudC50YXJnZXQudmFsdWUgJiYgKGV2ZW50LnJlbGF0ZWRUYXJnZXQgPT0gbnVsbCB8fCBldmVudC5yZWxhdGVkVGFyZ2V0ID09IHVuZGVmaW5lZCkpIHtcclxuICAgICAgICAgICAgaWYgKE51bWJlcihldmVudC50YXJnZXQudmFsdWUpIDw9IHRoaXMudG90YWxQYWdlTnVtYmVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hdFBhZ2luYXRvci5wYWdlSW5kZXggPSBOdW1iZXIoZXZlbnQudGFyZ2V0LnZhbHVlKSAtIDE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYW5nZVBhZ2luYXRvcigpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdDdXJyZW50IHBhZ2UgZXhjZWVkcyB0b3RhbCBuby4gb2YgcGFnZXMnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXplRm9ybSgpIHtcclxuICAgICAgICB0aGlzLmNhbGN1bGF0ZVRvdGFsUGFnZVNpemUoKTtcclxuICAgICAgICB0aGlzLm1hbnVhbFBhZ2VDaGFuZ2VDb250cm9sID0gbmV3IEZvcm1Db250cm9sKHRoaXMucGFnZUluZGV4LFxyXG4gICAgICAgICAgICBbVmFsaWRhdG9ycy5tYXgodGhpcy50b3RhbFBhZ2VOdW1iZXIpLCBWYWxpZGF0b3JzLm1pbigxKSwgVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZUZvcm0oKTtcclxuICAgIH1cclxuXHJcbiAgICBudW1iZXJPbmx5KGV2ZW50KTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgY2hhckNvZGUgPSAoZXZlbnQud2hpY2gpID8gZXZlbnQud2hpY2ggOiBldmVudC5rZXlDb2RlO1xyXG4gICAgICAgIGlmIChjaGFyQ29kZSA+IDMxICYmIChjaGFyQ29kZSA8IDQ4IHx8IGNoYXJDb2RlID4gNTcpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNldEZpcnN0UGFnZSgpe1xyXG4gICAgICAgIHRoaXMubWF0UGFnaW5hdG9yLmZpcnN0UGFnZSgpXHJcbiAgICB9XHJcblxyXG4gICAgc2V0TGFzdFBhZ2UoKXtcclxuICAgICAgICB0aGlzLm1hdFBhZ2luYXRvci5sYXN0UGFnZSgpXHJcbiAgICB9XHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLmluaXRpYWxpemVGb3JtKCk7XHJcbiAgICAgICAgdGhpcy5tYXRQYWdpbmF0b3IucGFnZS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNoYW5nZVBhZ2luYXRvcigpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICBcclxufVxyXG4iXX0=