import * as $ from 'jquery';
export function getSAMLAssertionByUserDetails(username, password) {
    var defer = $.Deferred();
    $.soap({
        header: {
            'wsse:Security': {
                '@xmlns:wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                'wsse:UsernameToken': {
                    '@xmlns:wsse': 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                    'wsse:Username': username,
                    'wsse:Password': password
                }
            }
        },
        body: {
            'samlp:Request': {
                '@xmlns:samlp': 'urn:oasis:names:tc:SAML:1.0:protocol',
                '@MajorVersion': '1',
                '@MinorVersion': '1',
                '@IssueInstant': new Date().toString(),
                'samlp:AuthenticationQuery': {
                    'saml:Subject': {
                        '@xmlns:saml': 'urn:oasis:names:tc:SAML:1.0:assertion',
                        'saml:NameIdentifier': {
                            '@Format': 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
                            __text: username
                        }
                    }
                }
            }
        }
    }).done(function (result) {
        defer.resolve(result.Response.AssertionArtifact.__text, (username && password));
    }).fail(function (err) {
        defer.reject();
    });
    return defer.promise();
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0U0FNTEFzc2VydGlvbkJ5VXNlckRldGFpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9tcG0tdXRpbHMvYXV0aC9nZXRTQU1MQXNzZXJ0aW9uQnlVc2VyRGV0YWlscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixNQUFNLFVBQVUsNkJBQTZCLENBQUMsUUFBUSxFQUFFLFFBQVE7SUFDNUQsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBRTNCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDSCxNQUFNLEVBQUU7WUFDSixlQUFlLEVBQUU7Z0JBQ2IsYUFBYSxFQUFFLG1GQUFtRjtnQkFDbEcsb0JBQW9CLEVBQUU7b0JBQ2xCLGFBQWEsRUFBRSxtRkFBbUY7b0JBQ2xHLGVBQWUsRUFBRSxRQUFRO29CQUN6QixlQUFlLEVBQUUsUUFBUTtpQkFDNUI7YUFDSjtTQUNKO1FBQ0QsSUFBSSxFQUFFO1lBQ0YsZUFBZSxFQUFFO2dCQUNiLGNBQWMsRUFBRSxzQ0FBc0M7Z0JBQ3RELGVBQWUsRUFBRSxHQUFHO2dCQUNwQixlQUFlLEVBQUUsR0FBRztnQkFDcEIsZUFBZSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUN0QywyQkFBMkIsRUFBRTtvQkFDekIsY0FBYyxFQUFFO3dCQUNaLGFBQWEsRUFBRSx1Q0FBdUM7d0JBQ3RELHFCQUFxQixFQUFFOzRCQUNuQixTQUFTLEVBQUUsdURBQXVEOzRCQUNsRSxNQUFNLEVBQUUsUUFBUTt5QkFDbkI7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO0tBQ0osQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLE1BQU07UUFDWCxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsR0FBRztRQUNSLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQixDQUFDLENBQUMsQ0FBQztJQUVILE9BQU8sS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQzNCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyAkIGZyb20gJ2pxdWVyeSc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0U0FNTEFzc2VydGlvbkJ5VXNlckRldGFpbHModXNlcm5hbWUsIHBhc3N3b3JkKSB7XHJcbiAgICBjb25zdCBkZWZlciA9ICQuRGVmZXJyZWQoKTtcclxuXHJcbiAgICAkLnNvYXAoe1xyXG4gICAgICAgIGhlYWRlcjoge1xyXG4gICAgICAgICAgICAnd3NzZTpTZWN1cml0eSc6IHtcclxuICAgICAgICAgICAgICAgICdAeG1sbnM6d3NzZSc6ICdodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktc2VjZXh0LTEuMC54c2QnLFxyXG4gICAgICAgICAgICAgICAgJ3dzc2U6VXNlcm5hbWVUb2tlbic6IHtcclxuICAgICAgICAgICAgICAgICAgICAnQHhtbG5zOndzc2UnOiAnaHR0cDovL2RvY3Mub2FzaXMtb3Blbi5vcmcvd3NzLzIwMDQvMDEvb2FzaXMtMjAwNDAxLXdzcy13c3NlY3VyaXR5LXNlY2V4dC0xLjAueHNkJyxcclxuICAgICAgICAgICAgICAgICAgICAnd3NzZTpVc2VybmFtZSc6IHVzZXJuYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICd3c3NlOlBhc3N3b3JkJzogcGFzc3dvcmRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYm9keToge1xyXG4gICAgICAgICAgICAnc2FtbHA6UmVxdWVzdCc6IHtcclxuICAgICAgICAgICAgICAgICdAeG1sbnM6c2FtbHAnOiAndXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4wOnByb3RvY29sJyxcclxuICAgICAgICAgICAgICAgICdATWFqb3JWZXJzaW9uJzogJzEnLFxyXG4gICAgICAgICAgICAgICAgJ0BNaW5vclZlcnNpb24nOiAnMScsXHJcbiAgICAgICAgICAgICAgICAnQElzc3VlSW5zdGFudCc6IG5ldyBEYXRlKCkudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgICdzYW1scDpBdXRoZW50aWNhdGlvblF1ZXJ5Jzoge1xyXG4gICAgICAgICAgICAgICAgICAgICdzYW1sOlN1YmplY3QnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdAeG1sbnM6c2FtbCc6ICd1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjA6YXNzZXJ0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NhbWw6TmFtZUlkZW50aWZpZXInOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQEZvcm1hdCc6ICd1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfX3RleHQ6IHVzZXJuYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KS5kb25lKChyZXN1bHQpID0+IHtcclxuICAgICAgICBkZWZlci5yZXNvbHZlKHJlc3VsdC5SZXNwb25zZS5Bc3NlcnRpb25BcnRpZmFjdC5fX3RleHQsICh1c2VybmFtZSAmJiBwYXNzd29yZCkpO1xyXG4gICAgfSkuZmFpbCgoZXJyKSA9PiB7XHJcbiAgICAgICAgZGVmZXIucmVqZWN0KCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xyXG59XHJcbiJdfQ==