import * as cookie from 'js-cookie';
var SessionInfo = /** @class */ (function () {
    function SessionInfo() {
        this.checkName = 'CheckName';
        this.sessionInfoCookieName = 'sessionInfo';
        this.samlArtifactCookieName = 'SamlArtifactCookieName';
        this.samlArtifactCookiePath = 'SamlArtifactCookiePath';
        if (SessionInfo._instance) {
            throw new Error('Error: Instantiation failed: Use SessionInfo.getInstance() instead of new.');
        }
        SessionInfo._instance = this;
    }
    SessionInfo.getInstance = function () {
        return SessionInfo._instance;
    };
    SessionInfo.prototype.setSessionInfo = function (preLoginInfoObj) {
        cookie.remove(this.sessionInfoCookieName);
        if (!preLoginInfoObj || !JSON.stringify(preLoginInfoObj)) {
            return false;
        }
        cookie.set(this.sessionInfoCookieName, JSON.stringify(preLoginInfoObj));
    };
    SessionInfo.prototype.getSessionInfoObj = function () {
        var sessionInfo = cookie.get(this.sessionInfoCookieName);
        if (!sessionInfo || !JSON.parse(sessionInfo)) {
            return false;
        }
        return JSON.parse(sessionInfo);
    };
    SessionInfo.prototype.getSessionProp = function (propertyName) {
        if (!propertyName || propertyName === '') {
            return false;
        }
        var sessionObj = this.getSessionInfoObj();
        if (!sessionObj[propertyName]) {
            return false;
        }
        return sessionObj[propertyName];
    };
    // tslint:disable-next-line: variable-name
    SessionInfo._instance = new SessionInfo();
    return SessionInfo;
}());
export { SessionInfo };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvblV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9tcG0tdXRpbHMvYXV0aC9zZXNzaW9uVXRpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEtBQUssTUFBTSxNQUFNLFdBQVcsQ0FBQztBQUdwQztJQVdJO1FBTk8sY0FBUyxHQUFHLFdBQVcsQ0FBQztRQUN4QiwwQkFBcUIsR0FBRyxhQUFhLENBQUM7UUFDdEMsMkJBQXNCLEdBQUcsd0JBQXdCLENBQUM7UUFDbEQsMkJBQXNCLEdBQUcsd0JBQXdCLENBQUM7UUFJckQsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO1lBQ3ZCLE1BQU0sSUFBSSxLQUFLLENBQUMsNEVBQTRFLENBQUMsQ0FBQztTQUNqRztRQUNELFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFYSx1QkFBVyxHQUF6QjtRQUNJLE9BQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQztJQUNqQyxDQUFDO0lBRU0sb0NBQWMsR0FBckIsVUFBc0IsZUFBZTtRQUNqQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQ3RELE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFTSx1Q0FBaUIsR0FBeEI7UUFDSSxJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQzFDLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFTSxvQ0FBYyxHQUFyQixVQUFzQixZQUFZO1FBQzlCLElBQUksQ0FBQyxZQUFZLElBQUksWUFBWSxLQUFLLEVBQUUsRUFBRTtZQUN0QyxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDM0IsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBN0NELDBDQUEwQztJQUM1QixxQkFBUyxHQUFnQixJQUFJLFdBQVcsRUFBRSxDQUFDO0lBNkM3RCxrQkFBQztDQUFBLEFBaERELElBZ0RDO1NBaERZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBjb29raWUgZnJvbSAnanMtY29va2llJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgU2Vzc2lvbkluZm8ge1xyXG5cclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogdmFyaWFibGUtbmFtZVxyXG4gICAgcHVibGljIHN0YXRpYyBfaW5zdGFuY2U6IFNlc3Npb25JbmZvID0gbmV3IFNlc3Npb25JbmZvKCk7XHJcblxyXG4gICAgcHVibGljIGNoZWNrTmFtZSA9ICdDaGVja05hbWUnO1xyXG4gICAgcHVibGljIHNlc3Npb25JbmZvQ29va2llTmFtZSA9ICdzZXNzaW9uSW5mbyc7XHJcbiAgICBwdWJsaWMgc2FtbEFydGlmYWN0Q29va2llTmFtZSA9ICdTYW1sQXJ0aWZhY3RDb29raWVOYW1lJztcclxuICAgIHB1YmxpYyBzYW1sQXJ0aWZhY3RDb29raWVQYXRoID0gJ1NhbWxBcnRpZmFjdENvb2tpZVBhdGgnO1xyXG4gICAgcHVibGljIG90bW1TZXNzaW9uU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgaWYgKFNlc3Npb25JbmZvLl9pbnN0YW5jZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0Vycm9yOiBJbnN0YW50aWF0aW9uIGZhaWxlZDogVXNlIFNlc3Npb25JbmZvLmdldEluc3RhbmNlKCkgaW5zdGVhZCBvZiBuZXcuJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFNlc3Npb25JbmZvLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRJbnN0YW5jZSgpOiBTZXNzaW9uSW5mbyB7XHJcbiAgICAgICAgcmV0dXJuIFNlc3Npb25JbmZvLl9pbnN0YW5jZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0U2Vzc2lvbkluZm8ocHJlTG9naW5JbmZvT2JqKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29va2llLnJlbW92ZSh0aGlzLnNlc3Npb25JbmZvQ29va2llTmFtZSk7XHJcbiAgICAgICAgaWYgKCFwcmVMb2dpbkluZm9PYmogfHwgIUpTT04uc3RyaW5naWZ5KHByZUxvZ2luSW5mb09iaikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb29raWUuc2V0KHRoaXMuc2Vzc2lvbkluZm9Db29raWVOYW1lLCBKU09OLnN0cmluZ2lmeShwcmVMb2dpbkluZm9PYmopKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0U2Vzc2lvbkluZm9PYmooKTogYW55IHtcclxuICAgICAgICBjb25zdCBzZXNzaW9uSW5mbyA9IGNvb2tpZS5nZXQodGhpcy5zZXNzaW9uSW5mb0Nvb2tpZU5hbWUpO1xyXG4gICAgICAgIGlmICghc2Vzc2lvbkluZm8gfHwgIUpTT04ucGFyc2Uoc2Vzc2lvbkluZm8pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2Uoc2Vzc2lvbkluZm8pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBnZXRTZXNzaW9uUHJvcChwcm9wZXJ0eU5hbWUpOiBhbnkge1xyXG4gICAgICAgIGlmICghcHJvcGVydHlOYW1lIHx8IHByb3BlcnR5TmFtZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBzZXNzaW9uT2JqID0gdGhpcy5nZXRTZXNzaW9uSW5mb09iaigpO1xyXG4gICAgICAgIGlmICghc2Vzc2lvbk9ialtwcm9wZXJ0eU5hbWVdKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNlc3Npb25PYmpbcHJvcGVydHlOYW1lXTtcclxuICAgIH1cclxufVxyXG4iXX0=