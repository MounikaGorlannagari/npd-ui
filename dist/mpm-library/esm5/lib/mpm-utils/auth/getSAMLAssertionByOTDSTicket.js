import * as $ from 'jquery';
export function GetSAMLAssertionByOTDSTicket(otdsTicket) {
    var defer = $.Deferred();
    $.soap({
        header: {
            OTAuthentication: {
                '@xmlns': 'urn:api.ecm.opentext.com',
                AuthenticationToken: otdsTicket
            }
        },
        body: {
            'samlp:Request': {
                '@xmlns:samlp': 'urn:oasis:names:tc:SAML:1.0:protocol',
                '@MajorVersion': '1',
                '@MinorVersion': '1',
                '@IssueInstant': new Date().toISOString(),
                'samlp:AuthenticationQuery': {
                    'saml:Subject': {
                        '@xmlns:saml': 'urn:oasis:names:tc:SAML:1.0:assertion',
                        'saml:NameIdentifier': {
                            '@Format': 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified'
                        }
                    }
                }
            }
        }
    }).done(function (result) {
        defer.resolve(result.Response.AssertionArtifact.__text);
    }).fail(function (err) {
        if (err.status === 403) {
            defer.reject(err.statusText);
        }
        else {
            defer.reject(err);
        }
    });
    return defer.promise();
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0U0FNTEFzc2VydGlvbkJ5T1REU1RpY2tldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL21wbS11dGlscy9hdXRoL2dldFNBTUxBc3NlcnRpb25CeU9URFNUaWNrZXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsTUFBTSxVQUFVLDRCQUE0QixDQUFDLFVBQVU7SUFDbkQsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzNCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDSCxNQUFNLEVBQUU7WUFDSixnQkFBZ0IsRUFBRTtnQkFDZCxRQUFRLEVBQUUsMEJBQTBCO2dCQUNwQyxtQkFBbUIsRUFBRSxVQUFVO2FBQ2xDO1NBQ0o7UUFDRCxJQUFJLEVBQUU7WUFDRixlQUFlLEVBQUU7Z0JBQ2IsY0FBYyxFQUFFLHNDQUFzQztnQkFDdEQsZUFBZSxFQUFFLEdBQUc7Z0JBQ3BCLGVBQWUsRUFBRSxHQUFHO2dCQUNwQixlQUFlLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3pDLDJCQUEyQixFQUFFO29CQUN6QixjQUFjLEVBQUU7d0JBQ1osYUFBYSxFQUFFLHVDQUF1Qzt3QkFDdEQscUJBQXFCLEVBQUU7NEJBQ25CLFNBQVMsRUFBRSx1REFBdUQ7eUJBQ3JFO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSjtLQUNKLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNO1FBQ1gsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVELENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLEdBQUc7UUFDUixJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO1lBQ3BCLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ2hDO2FBQU07WUFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFSCxPQUFPLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztBQUMzQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIEdldFNBTUxBc3NlcnRpb25CeU9URFNUaWNrZXQob3Rkc1RpY2tldCkge1xyXG4gICAgY29uc3QgZGVmZXIgPSAkLkRlZmVycmVkKCk7XHJcbiAgICAkLnNvYXAoe1xyXG4gICAgICAgIGhlYWRlcjoge1xyXG4gICAgICAgICAgICBPVEF1dGhlbnRpY2F0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICAnQHhtbG5zJzogJ3VybjphcGkuZWNtLm9wZW50ZXh0LmNvbScsXHJcbiAgICAgICAgICAgICAgICBBdXRoZW50aWNhdGlvblRva2VuOiBvdGRzVGlja2V0XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGJvZHk6IHtcclxuICAgICAgICAgICAgJ3NhbWxwOlJlcXVlc3QnOiB7XHJcbiAgICAgICAgICAgICAgICAnQHhtbG5zOnNhbWxwJzogJ3VybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMDpwcm90b2NvbCcsXHJcbiAgICAgICAgICAgICAgICAnQE1ham9yVmVyc2lvbic6ICcxJyxcclxuICAgICAgICAgICAgICAgICdATWlub3JWZXJzaW9uJzogJzEnLFxyXG4gICAgICAgICAgICAgICAgJ0BJc3N1ZUluc3RhbnQnOiBuZXcgRGF0ZSgpLnRvSVNPU3RyaW5nKCksXHJcbiAgICAgICAgICAgICAgICAnc2FtbHA6QXV0aGVudGljYXRpb25RdWVyeSc6IHtcclxuICAgICAgICAgICAgICAgICAgICAnc2FtbDpTdWJqZWN0Jzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAnQHhtbG5zOnNhbWwnOiAndXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4wOmFzc2VydGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdzYW1sOk5hbWVJZGVudGlmaWVyJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ0BGb3JtYXQnOiAndXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4xOm5hbWVpZC1mb3JtYXQ6dW5zcGVjaWZpZWQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KS5kb25lKChyZXN1bHQpID0+IHtcclxuICAgICAgICBkZWZlci5yZXNvbHZlKHJlc3VsdC5SZXNwb25zZS5Bc3NlcnRpb25BcnRpZmFjdC5fX3RleHQpO1xyXG4gICAgfSkuZmFpbCgoZXJyKSA9PiB7XHJcbiAgICAgICAgaWYgKGVyci5zdGF0dXMgPT09IDQwMykge1xyXG4gICAgICAgICAgICBkZWZlci5yZWplY3QoZXJyLnN0YXR1c1RleHQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRlZmVyLnJlamVjdChlcnIpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHJldHVybiBkZWZlci5wcm9taXNlKCk7XHJcbn1cclxuIl19