import * as $ from 'jquery';
import * as x2js from 'x2js';
import { GloabalConfig as serverConfig } from '../config/config';
var X2JS = new x2js({ attributePrefix: '@' });
X2JS.xml2string = function (xmlObj) {
    if (typeof xmlObj.xml !== 'undefined') {
        return xmlObj.xml;
    }
    else {
        return (new XMLSerializer()).serializeToString(xmlObj);
    }
};
var getCookie = function () {
    var ca = document.cookie.split(';');
    // tslint:disable-next-line: prefer-for-of
    for (var i = 0; i < ca.length; i++) {
        var cookie = ca[i];
        var cookieNameIndex = cookie.indexOf('=');
        var cookieName = cookie.substring(0, cookieNameIndex);
        if (cookieName.endsWith('_ct')) {
            if (cookie.startsWith(' ')) {
                cookie = cookie.substring(1);
            }
            return cookie;
        }
    }
    return '';
};
var ɵ0 = getCookie;
export var GatewayModule = {
    ajaxSetup: {
        converters: {
            'text soap': function (result) {
                return X2JS.parseXmlString(result);
            },
            'soap json': function (result) {
                var myDoc = document.implementation.createDocument('', '', null);
                myDoc.appendChild(myDoc.adoptNode(result.lastChild.lastChild));
                return X2JS.xml2json(myDoc);
            }
        }
    },
    extensions: {
        x2jsRaw: x2js,
        X2JS: X2JS,
        soap: function (config) {
            var objectName = config.method;
            var deferred = $.Deferred();
            var dataType = Array.isArray(config.blueprintArrays)
                ? 'text soap'
                : 'text soap json';
            var blueprint = config.blueprint || {};
            var preprocessor = config.preprocessor;
            var blueprintArrays = config.blueprintArrays;
            var envelope = {
                'SOAP:Envelope': {
                    '@xmlns:SOAP': 'http://schemas.xmlsoap.org/soap/envelope/'
                }
            };
            if (config.header) {
                envelope['SOAP:Envelope']['SOAP:Header'] = config.header;
            }
            if (!config.body) {
                envelope['SOAP:Envelope']['SOAP:Body'] = {};
                if (config.method) {
                    envelope['SOAP:Envelope']['SOAP:Body'][config.method] = config.parameters || {};
                }
                if (config.namespace) {
                    envelope['SOAP:Envelope']['SOAP:Body'][config.method]['@xmlns'] = config.namespace || '';
                }
            }
            else {
                envelope['SOAP:Envelope']['SOAP:Body'] = config.body;
            }
            /* The following inputs are specific to $['soap'], and meaningless to $.ajax. We don't want to mix those in...*/
            delete config.header;
            delete config.body;
            delete config.blueprint;
            delete config.blueprintArrays;
            delete config.preprocessor;
            delete config.method;
            delete config.namespace;
            delete config.dataType;
            /* TODO: Right now we have to attach the Config.js object to the jQuery
            * namespace to be able to support this. Probably would be better if the
            * gateway was passed in during each request.
            */
            config.url = config.url || serverConfig.getGatewayUrl() || '';
            config.data = X2JS.json2xml_str(envelope);
            config.method = 'POST';
            config.contentType = config.contentType || 'text/xml;charset=UTF-8';
            config.dataType = config.dataType || dataType;
            $.ajax(config).done(function (data, textStatus, jqXHR) {
                if (data.Body) {
                    data = data.Body;
                }
                if (objectName && data[objectName + 'Response']) {
                    data = data[objectName + 'Response'];
                }
                if (blueprintArrays) {
                    data = X2JS.parseXmlString(X2JS.xml2string(data.firstChild.lastChild.firstChild));
                    data = new x2js({ attributePrefix: '@', arrayAccessFormPaths: blueprintArrays }).xml2json(data);
                }
                if ($.isFunction(preprocessor)) {
                    preprocessor(data);
                }
                if ($.isEmptyObject(blueprint) || !(typeof data === 'object')) {
                    deferred.resolve(data, textStatus, jqXHR);
                }
                else {
                    deferred.resolve(data, textStatus, jqXHR);
                }
            }).fail(function (error, response, jqXHR) {
                if (error.responseText && error.responseText.length) {
                    error.responseJSON = X2JS.xml_str2json(error.responseText);
                    console.error(error);
                }
                else {
                    console.error('Connection error.');
                }
                deferred.reject(error, response, jqXHR);
            });
            return deferred;
        }
    }
};
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2F0ZXdheS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL21wbS11dGlscy9hdXRoL2dhdGV3YXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxLQUFLLElBQUksTUFBTSxNQUFNLENBQUM7QUFDN0IsT0FBTyxFQUFFLGFBQWEsSUFBSSxZQUFZLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUVqRSxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxFQUFFLGVBQWUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO0FBQ2hELElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBQyxNQUFNO0lBQ3JCLElBQUksT0FBTyxNQUFNLENBQUMsR0FBRyxLQUFLLFdBQVcsRUFBRTtRQUNuQyxPQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUM7S0FDckI7U0FBTTtRQUNILE9BQU8sQ0FBQyxJQUFJLGFBQWEsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7S0FDMUQ7QUFDTCxDQUFDLENBQUM7QUFFRixJQUFNLFNBQVMsR0FBRztJQUNkLElBQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RDLDBDQUEwQztJQUMxQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUNoQyxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkIsSUFBTSxlQUFlLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QyxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUN4RCxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDNUIsSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUN4QixNQUFNLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoQztZQUNELE9BQU8sTUFBTSxDQUFDO1NBQ2pCO0tBQ0o7SUFDRCxPQUFPLEVBQUUsQ0FBQztBQUNkLENBQUMsQ0FBQzs7QUFFRixNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUc7SUFDekIsU0FBUyxFQUFFO1FBQ1AsVUFBVSxFQUFFO1lBQ1IsV0FBVyxZQUFDLE1BQU07Z0JBQ2QsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7WUFDRCxXQUFXLFlBQUMsTUFBTTtnQkFDZCxJQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNuRSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUMvRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEMsQ0FBQztTQUNKO0tBQ0o7SUFFRCxVQUFVLEVBQUU7UUFDUixPQUFPLEVBQUUsSUFBSTtRQUNiLElBQUksTUFBQTtRQUVKLElBQUksWUFBQyxNQUFNO1lBQ1AsSUFBTSxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUNqQyxJQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDOUIsSUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO2dCQUNsRCxDQUFDLENBQUMsV0FBVztnQkFDYixDQUFDLENBQUMsZ0JBQWdCLENBQUM7WUFDdkIsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7WUFDekMsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQztZQUN6QyxJQUFNLGVBQWUsR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDO1lBQy9DLElBQU0sUUFBUSxHQUFHO2dCQUNiLGVBQWUsRUFBRTtvQkFDYixhQUFhLEVBQUUsMkNBQTJDO2lCQUM3RDthQUNKLENBQUM7WUFDRixJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQ2YsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7YUFDNUQ7WUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtnQkFDZCxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUM1QyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ2YsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztpQkFDbkY7Z0JBQ0QsSUFBSSxNQUFNLENBQUMsU0FBUyxFQUFFO29CQUNsQixRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO2lCQUM1RjthQUNKO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxXQUFXLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2FBQ3hEO1lBRUQsZ0hBQWdIO1lBQ2hILE9BQU8sTUFBTSxDQUFDLE1BQU0sQ0FBQztZQUNyQixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDbkIsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ3hCLE9BQU8sTUFBTSxDQUFDLGVBQWUsQ0FBQztZQUM5QixPQUFPLE1BQU0sQ0FBQyxZQUFZLENBQUM7WUFDM0IsT0FBTyxNQUFNLENBQUMsTUFBTSxDQUFDO1lBQ3JCLE9BQU8sTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUN4QixPQUFPLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFFdkI7OztjQUdFO1lBQ0YsTUFBTSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUM7WUFFOUQsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsSUFBSSx3QkFBd0IsQ0FBQztZQUNwRSxNQUFNLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLElBQUksUUFBUSxDQUFDO1lBRTlDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLO2dCQUN4QyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ1gsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQ3BCO2dCQUNELElBQUksVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLEVBQUU7b0JBQzdDLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxDQUFDO2lCQUN4QztnQkFDRCxJQUFJLGVBQWUsRUFBRTtvQkFDakIsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNsRixJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFFLG9CQUFvQixFQUFFLGVBQWUsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNuRztnQkFFRCxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQzVCLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDdEI7Z0JBRUQsSUFBSSxDQUFDLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLElBQUksS0FBSyxRQUFRLENBQUMsRUFBRTtvQkFDM0QsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUM3QztxQkFBTTtvQkFDSCxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7aUJBQzdDO1lBQ0wsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxLQUFLO2dCQUMzQixJQUFJLEtBQUssQ0FBQyxZQUFZLElBQUksS0FBSyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7b0JBQ2pELEtBQUssQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQzNELE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3hCO3FCQUFNO29CQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztpQkFDdEM7Z0JBQ0QsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzVDLENBQUMsQ0FBQyxDQUFDO1lBRUgsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQztLQUNKO0NBRUosQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuaW1wb3J0ICogYXMgeDJqcyBmcm9tICd4MmpzJztcclxuaW1wb3J0IHsgR2xvYWJhbENvbmZpZyBhcyBzZXJ2ZXJDb25maWcgfSBmcm9tICcuLi9jb25maWcvY29uZmlnJztcclxuXHJcbmNvbnN0IFgySlMgPSBuZXcgeDJqcyh7IGF0dHJpYnV0ZVByZWZpeDogJ0AnIH0pO1xyXG5YMkpTLnhtbDJzdHJpbmcgPSAoeG1sT2JqKSA9PiB7XHJcbiAgICBpZiAodHlwZW9mIHhtbE9iai54bWwgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgcmV0dXJuIHhtbE9iai54bWw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiAobmV3IFhNTFNlcmlhbGl6ZXIoKSkuc2VyaWFsaXplVG9TdHJpbmcoeG1sT2JqKTtcclxuICAgIH1cclxufTtcclxuXHJcbmNvbnN0IGdldENvb2tpZSA9ICgpID0+IHtcclxuICAgIGNvbnN0IGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHByZWZlci1mb3Itb2ZcclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY2EubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBsZXQgY29va2llID0gY2FbaV07XHJcbiAgICAgICAgY29uc3QgY29va2llTmFtZUluZGV4ID0gY29va2llLmluZGV4T2YoJz0nKTtcclxuICAgICAgICBjb25zdCBjb29raWVOYW1lID0gY29va2llLnN1YnN0cmluZygwLCBjb29raWVOYW1lSW5kZXgpO1xyXG4gICAgICAgIGlmIChjb29raWVOYW1lLmVuZHNXaXRoKCdfY3QnKSkge1xyXG4gICAgICAgICAgICBpZiAoY29va2llLnN0YXJ0c1dpdGgoJyAnKSkge1xyXG4gICAgICAgICAgICAgICAgY29va2llID0gY29va2llLnN1YnN0cmluZygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gY29va2llO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiAnJztcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBHYXRld2F5TW9kdWxlID0ge1xyXG4gICAgYWpheFNldHVwOiB7XHJcbiAgICAgICAgY29udmVydGVyczoge1xyXG4gICAgICAgICAgICAndGV4dCBzb2FwJyhyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBYMkpTLnBhcnNlWG1sU3RyaW5nKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICdzb2FwIGpzb24nKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbXlEb2MgPSBkb2N1bWVudC5pbXBsZW1lbnRhdGlvbi5jcmVhdGVEb2N1bWVudCgnJywgJycsIG51bGwpO1xyXG4gICAgICAgICAgICAgICAgbXlEb2MuYXBwZW5kQ2hpbGQobXlEb2MuYWRvcHROb2RlKHJlc3VsdC5sYXN0Q2hpbGQubGFzdENoaWxkKSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gWDJKUy54bWwyanNvbihteURvYyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIGV4dGVuc2lvbnM6IHtcclxuICAgICAgICB4MmpzUmF3OiB4MmpzLFxyXG4gICAgICAgIFgySlMsXHJcblxyXG4gICAgICAgIHNvYXAoY29uZmlnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9iamVjdE5hbWUgPSBjb25maWcubWV0aG9kO1xyXG4gICAgICAgICAgICBjb25zdCBkZWZlcnJlZCA9ICQuRGVmZXJyZWQoKTtcclxuICAgICAgICAgICAgY29uc3QgZGF0YVR5cGUgPSBBcnJheS5pc0FycmF5KGNvbmZpZy5ibHVlcHJpbnRBcnJheXMpXHJcbiAgICAgICAgICAgICAgICA/ICd0ZXh0IHNvYXAnXHJcbiAgICAgICAgICAgICAgICA6ICd0ZXh0IHNvYXAganNvbic7XHJcbiAgICAgICAgICAgIGNvbnN0IGJsdWVwcmludCA9IGNvbmZpZy5ibHVlcHJpbnQgfHwge307XHJcbiAgICAgICAgICAgIGNvbnN0IHByZXByb2Nlc3NvciA9IGNvbmZpZy5wcmVwcm9jZXNzb3I7XHJcbiAgICAgICAgICAgIGNvbnN0IGJsdWVwcmludEFycmF5cyA9IGNvbmZpZy5ibHVlcHJpbnRBcnJheXM7XHJcbiAgICAgICAgICAgIGNvbnN0IGVudmVsb3BlID0ge1xyXG4gICAgICAgICAgICAgICAgJ1NPQVA6RW52ZWxvcGUnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ0B4bWxuczpTT0FQJzogJ2h0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBpZiAoY29uZmlnLmhlYWRlcikge1xyXG4gICAgICAgICAgICAgICAgZW52ZWxvcGVbJ1NPQVA6RW52ZWxvcGUnXVsnU09BUDpIZWFkZXInXSA9IGNvbmZpZy5oZWFkZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCFjb25maWcuYm9keSkge1xyXG4gICAgICAgICAgICAgICAgZW52ZWxvcGVbJ1NPQVA6RW52ZWxvcGUnXVsnU09BUDpCb2R5J10gPSB7fTtcclxuICAgICAgICAgICAgICAgIGlmIChjb25maWcubWV0aG9kKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW52ZWxvcGVbJ1NPQVA6RW52ZWxvcGUnXVsnU09BUDpCb2R5J11bY29uZmlnLm1ldGhvZF0gPSBjb25maWcucGFyYW1ldGVycyB8fCB7fTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChjb25maWcubmFtZXNwYWNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW52ZWxvcGVbJ1NPQVA6RW52ZWxvcGUnXVsnU09BUDpCb2R5J11bY29uZmlnLm1ldGhvZF1bJ0B4bWxucyddID0gY29uZmlnLm5hbWVzcGFjZSB8fCAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGVudmVsb3BlWydTT0FQOkVudmVsb3BlJ11bJ1NPQVA6Qm9keSddID0gY29uZmlnLmJvZHk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8qIFRoZSBmb2xsb3dpbmcgaW5wdXRzIGFyZSBzcGVjaWZpYyB0byAkWydzb2FwJ10sIGFuZCBtZWFuaW5nbGVzcyB0byAkLmFqYXguIFdlIGRvbid0IHdhbnQgdG8gbWl4IHRob3NlIGluLi4uKi9cclxuICAgICAgICAgICAgZGVsZXRlIGNvbmZpZy5oZWFkZXI7XHJcbiAgICAgICAgICAgIGRlbGV0ZSBjb25maWcuYm9keTtcclxuICAgICAgICAgICAgZGVsZXRlIGNvbmZpZy5ibHVlcHJpbnQ7XHJcbiAgICAgICAgICAgIGRlbGV0ZSBjb25maWcuYmx1ZXByaW50QXJyYXlzO1xyXG4gICAgICAgICAgICBkZWxldGUgY29uZmlnLnByZXByb2Nlc3NvcjtcclxuICAgICAgICAgICAgZGVsZXRlIGNvbmZpZy5tZXRob2Q7XHJcbiAgICAgICAgICAgIGRlbGV0ZSBjb25maWcubmFtZXNwYWNlO1xyXG4gICAgICAgICAgICBkZWxldGUgY29uZmlnLmRhdGFUeXBlO1xyXG5cclxuICAgICAgICAgICAgLyogVE9ETzogUmlnaHQgbm93IHdlIGhhdmUgdG8gYXR0YWNoIHRoZSBDb25maWcuanMgb2JqZWN0IHRvIHRoZSBqUXVlcnlcclxuICAgICAgICAgICAgKiBuYW1lc3BhY2UgdG8gYmUgYWJsZSB0byBzdXBwb3J0IHRoaXMuIFByb2JhYmx5IHdvdWxkIGJlIGJldHRlciBpZiB0aGVcclxuICAgICAgICAgICAgKiBnYXRld2F5IHdhcyBwYXNzZWQgaW4gZHVyaW5nIGVhY2ggcmVxdWVzdC5cclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgY29uZmlnLnVybCA9IGNvbmZpZy51cmwgfHwgc2VydmVyQ29uZmlnLmdldEdhdGV3YXlVcmwoKSB8fCAnJztcclxuXHJcbiAgICAgICAgICAgIGNvbmZpZy5kYXRhID0gWDJKUy5qc29uMnhtbF9zdHIoZW52ZWxvcGUpO1xyXG4gICAgICAgICAgICBjb25maWcubWV0aG9kID0gJ1BPU1QnO1xyXG4gICAgICAgICAgICBjb25maWcuY29udGVudFR5cGUgPSBjb25maWcuY29udGVudFR5cGUgfHwgJ3RleHQveG1sO2NoYXJzZXQ9VVRGLTgnO1xyXG4gICAgICAgICAgICBjb25maWcuZGF0YVR5cGUgPSBjb25maWcuZGF0YVR5cGUgfHwgZGF0YVR5cGU7XHJcblxyXG4gICAgICAgICAgICAkLmFqYXgoY29uZmlnKS5kb25lKChkYXRhLCB0ZXh0U3RhdHVzLCBqcVhIUikgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuQm9keSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPSBkYXRhLkJvZHk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAob2JqZWN0TmFtZSAmJiBkYXRhW29iamVjdE5hbWUgKyAnUmVzcG9uc2UnXSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPSBkYXRhW29iamVjdE5hbWUgKyAnUmVzcG9uc2UnXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChibHVlcHJpbnRBcnJheXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhID0gWDJKUy5wYXJzZVhtbFN0cmluZyhYMkpTLnhtbDJzdHJpbmcoZGF0YS5maXJzdENoaWxkLmxhc3RDaGlsZC5maXJzdENoaWxkKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YSA9IG5ldyB4MmpzKHsgYXR0cmlidXRlUHJlZml4OiAnQCcsIGFycmF5QWNjZXNzRm9ybVBhdGhzOiBibHVlcHJpbnRBcnJheXMgfSkueG1sMmpzb24oZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCQuaXNGdW5jdGlvbihwcmVwcm9jZXNzb3IpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJlcHJvY2Vzc29yKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmICgkLmlzRW1wdHlPYmplY3QoYmx1ZXByaW50KSB8fCAhKHR5cGVvZiBkYXRhID09PSAnb2JqZWN0JykpIHtcclxuICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGRhdGEsIHRleHRTdGF0dXMsIGpxWEhSKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhLCB0ZXh0U3RhdHVzLCBqcVhIUik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pLmZhaWwoKGVycm9yLCByZXNwb25zZSwganFYSFIpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlcnJvci5yZXNwb25zZVRleHQgJiYgZXJyb3IucmVzcG9uc2VUZXh0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yLnJlc3BvbnNlSlNPTiA9IFgySlMueG1sX3N0cjJqc29uKGVycm9yLnJlc3BvbnNlVGV4dCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Nvbm5lY3Rpb24gZXJyb3IuJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZWplY3QoZXJyb3IsIHJlc3BvbnNlLCBqcVhIUik7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGRlZmVycmVkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn07XHJcbiJdfQ==