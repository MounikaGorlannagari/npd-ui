import * as cookie from 'js-cookie';
import { SessionInfo } from '../auth/sessionUtil';
var GloabalConfig = /** @class */ (function () {
    function GloabalConfig() {
        if (GloabalConfig._instance) {
            throw new Error('Error: Instantiation failed: Use SessionInfo.getInstance() instead of new.');
        }
        GloabalConfig._instance = this;
    }
    GloabalConfig.getInstance = function () {
        return GloabalConfig._instance;
    };
    GloabalConfig.setConfig = function (configObj) {
        GloabalConfig.config = configObj;
        GloabalConfig.protocal = GloabalConfig.config.gatewayProtocol;
        GloabalConfig.webServiceUrl = GloabalConfig.protocal + location.host
            + '/' + GloabalConfig.config.psHome + '/' + GloabalConfig.config.organizationName;
    };
    GloabalConfig.getOtmmSessionCookieName = function () {
        return GloabalConfig.config.otmmSessionCookieName;
    };
    GloabalConfig.getOrganizationDN = function () {
        return 'o=' + GloabalConfig.config.organizationName + ',' + GloabalConfig.config.instanceIdentifier;
    };
    GloabalConfig.getOrganizationName = function () {
        return GloabalConfig.config.organizationName;
    };
    GloabalConfig.getPreLoginUrl = function () {
        return this.webServiceUrl + GloabalConfig.config.preLoginUrl;
    };
    GloabalConfig.getAuthUrl = function (url) {
        var cookieName = this.sessionUtil.getSessionProp(this.sessionUtil.checkName);
        var cookieValue = cookieName
            ? cookie.get(cookieName)
            : '';
        if (cookieValue) {
            url += ('?' + cookieName + '=' + cookieValue);
            url += ('&organization=' + encodeURIComponent(GloabalConfig.getOrganizationDN()));
        }
        return url;
    };
    GloabalConfig.getPsOrgEndpoint = function () {
        return GloabalConfig.config.gatewayProtocol + GloabalConfig.config.gatewayHost
            + (GloabalConfig.config.gatewayPort ? (':' + GloabalConfig.config.gatewayPort) : '')
            + '/' + GloabalConfig.config.psHome + '/' + GloabalConfig.config.organizationName;
    };
    GloabalConfig.getTicketConsumerURL = function () {
        return GloabalConfig.webServiceUrl
            + GloabalConfig.config.psTicketConsumerUrl
            + '?RelayState=' + encodeURIComponent(GloabalConfig.getPsOrgEndpoint());
    };
    GloabalConfig.getSAMLart = function () {
        var cookieName = this.sessionUtil.getSessionProp(this.sessionUtil.samlArtifactCookieName);
        var cookieValue = cookieName
            ? cookie.get(cookieName)
            : '';
        if (!cookieValue) {
            return '';
        }
        return cookieValue;
    };
    GloabalConfig.getGatewayUrl = function () {
        return GloabalConfig.getAuthUrl(this.webServiceUrl + GloabalConfig.config.gatewayUrl);
    };
    GloabalConfig.getGatewayCookieDomain = function () {
        return location.hostname;
    };
    GloabalConfig.getOTDSLoginForm = function () {
        return this.webServiceUrl + GloabalConfig.config.psSSOLoginUrl + '?organization='
            + encodeURIComponent('o=' + GloabalConfig.config.organizationName
                + ',' + GloabalConfig.config.instanceIdentifier) + '&language=en-US&otdsauth=no-sso';
    };
    GloabalConfig.getOTDSBaseDomainUrl = function () {
        return GloabalConfig.config.otdsProtocol
            + GloabalConfig.config.otdsHost
            + (GloabalConfig.config.otdsPort ? (':' + GloabalConfig.config.otdsPort) : '');
    };
    GloabalConfig.getOTDSLoginPageUrl = function () {
        return GloabalConfig.getOTDSBaseDomainUrl()
            + GloabalConfig.config.otdsRestUrl + '/login?RFA='
            + GloabalConfig.config.psOtdsResource + encodeURIComponent(':' + GloabalConfig.getPsOrgEndpoint()
            + GloabalConfig.config.psTicketConsumerUrl
            + '?RelayState=' + encodeURIComponent(GloabalConfig.getPsOrgEndpoint()
            + GloabalConfig.config.psDeployedPath)) + '&PostTicket=true&language=en-US';
    };
    GloabalConfig.getPSSSOLoginUrl = function () {
        return GloabalConfig.webServiceUrl + GloabalConfig.config.psSSOLoginUrl;
    };
    GloabalConfig.getOTDSRESTAuthEndpoint = function () {
        return GloabalConfig.config.otdsProtocol +
            GloabalConfig.config.otdsHost +
            (GloabalConfig.config.otdsPort ? (':' + GloabalConfig.config.otdsPort) : '') +
            GloabalConfig.config.otdsRestUrl +
            '/rest/authentication/credentials';
    };
    /*  public static getOTDSRESTAuthEndpointwithHeaders(): string {
         return GloabalConfig.config.otdsProtocol +
             (isDevMode() ? './' : GloabalConfig.config.otdsHost +
                 (GloabalConfig.config.otdsPort ? (':' + GloabalConfig.config.otdsPort) : '') +
                 GloabalConfig.config.otdsRestUrl) +
             //   '/rest/authentication/credentials';
             // MPMV3-2237
             'otdsws/rest/authentication/headers'  //'/rest/authentication/credentials/headers';
     } */
    // MPMV3-2237
    GloabalConfig.getOTDSRESTAuthEndpointwithHeaders = function () {
        return GloabalConfig.config.otdsProtocol +
            GloabalConfig.config.otdsHost +
            (GloabalConfig.config.otdsPort ? (':' + GloabalConfig.config.otdsPort) : '') +
            GloabalConfig.config.otdsRestUrl +
            '/rest/authentication/headers';
        // 'otdsws/rest/authentication/headers'  
    };
    GloabalConfig.getOTDSTicketCookieName = function () {
        return GloabalConfig.config.otdsTicketCookieName;
    };
    GloabalConfig.getProcessSuiteOTDSResourceId = function () {
        return GloabalConfig.config.psOtdsResource;
    };
    GloabalConfig.getMaxFileSize = function () {
        return GloabalConfig.config.maxFileSize;
    };
    GloabalConfig.getMaxFiles = function () {
        return GloabalConfig.config.maxFiles ? GloabalConfig.config.maxFiles : 1;
    };
    GloabalConfig.getBrandConfigPath = function () {
        return GloabalConfig.config.brandConfigPath;
    };
    GloabalConfig.getFieldRestriction = function () {
        return GloabalConfig.config.enableIndexerFieldRestriction;
    };
    GloabalConfig.getMaximumFieldsAllowed = function () {
        return GloabalConfig.config.maximumFieldsAllowed;
    };
    GloabalConfig.getFreezeColumnCount = function () {
        return GloabalConfig.config.freezeColumnCount;
    };
    // tslint:disable-next-line: variable-name
    GloabalConfig._instance = new GloabalConfig();
    GloabalConfig.config = null;
    GloabalConfig.sessionUtil = SessionInfo.getInstance();
    GloabalConfig.protocal = '';
    GloabalConfig.webServiceUrl = '';
    return GloabalConfig;
}());
export { GloabalConfig };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL2NvbmZpZy9jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxLQUFLLE1BQU0sTUFBTSxXQUFXLENBQUM7QUFDcEMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBSWxEO0lBU0k7UUFDSSxJQUFJLGFBQWEsQ0FBQyxTQUFTLEVBQUU7WUFDekIsTUFBTSxJQUFJLEtBQUssQ0FBQyw0RUFBNEUsQ0FBQyxDQUFDO1NBQ2pHO1FBQ0QsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDbkMsQ0FBQztJQUVhLHlCQUFXLEdBQXpCO1FBQ0ksT0FBTyxhQUFhLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFYSx1QkFBUyxHQUF2QixVQUF3QixTQUFTO1FBQzdCLGFBQWEsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO1FBQ2pDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUM7UUFDOUQsYUFBYSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJO2NBQzlELEdBQUcsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztJQUMxRixDQUFDO0lBRWEsc0NBQXdCLEdBQXRDO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDO0lBQ3RELENBQUM7SUFFYSwrQkFBaUIsR0FBL0I7UUFDSSxPQUFPLElBQUksR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDO0lBQ3hHLENBQUM7SUFFYSxpQ0FBbUIsR0FBakM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUM7SUFDakQsQ0FBQztJQUVhLDRCQUFjLEdBQTVCO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQ2pFLENBQUM7SUFFYSx3QkFBVSxHQUF4QixVQUF5QixHQUFXO1FBQ2hDLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDL0UsSUFBTSxXQUFXLEdBQUcsVUFBVTtZQUMxQixDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULElBQUksV0FBVyxFQUFFO1lBQ2IsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsV0FBVyxDQUFDLENBQUM7WUFDOUMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3JGO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0lBRWEsOEJBQWdCLEdBQTlCO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLGVBQWUsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVc7Y0FDeEUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2NBQ2xGLEdBQUcsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQztJQUUxRixDQUFDO0lBRWEsa0NBQW9CLEdBQWxDO1FBQ0ksT0FBTyxhQUFhLENBQUMsYUFBYTtjQUM1QixhQUFhLENBQUMsTUFBTSxDQUFDLG1CQUFtQjtjQUN4QyxjQUFjLEdBQUcsa0JBQWtCLENBQUMsYUFBYSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQztJQUNoRixDQUFDO0lBRWEsd0JBQVUsR0FBeEI7UUFDSSxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDNUYsSUFBTSxXQUFXLEdBQUcsVUFBVTtZQUMxQixDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7WUFDeEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDZCxPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsT0FBTyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQUVhLDJCQUFhLEdBQTNCO1FBQ0ksT0FBTyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxRixDQUFDO0lBRWEsb0NBQXNCLEdBQXBDO1FBQ0ksT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7SUFFYSw4QkFBZ0IsR0FBOUI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEdBQUcsZ0JBQWdCO2NBQzNFLGtCQUFrQixDQUFDLElBQUksR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLGdCQUFnQjtrQkFDM0QsR0FBRyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRyxpQ0FBaUMsQ0FBQztJQUNqRyxDQUFDO0lBRWEsa0NBQW9CLEdBQWxDO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLFlBQVk7Y0FDbEMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRO2NBQzdCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFFYSxpQ0FBbUIsR0FBakM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTtjQUNyQyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxhQUFhO2NBQ2hELGFBQWEsQ0FBQyxNQUFNLENBQUMsY0FBYyxHQUFHLGtCQUFrQixDQUN0RCxHQUFHLEdBQUcsYUFBYSxDQUFDLGdCQUFnQixFQUFFO2NBQ3BDLGFBQWEsQ0FBQyxNQUFNLENBQUMsbUJBQW1CO2NBQ3hDLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUU7Y0FDaEUsYUFBYSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FDN0MsR0FBRyxpQ0FBaUMsQ0FBQztJQUM5QyxDQUFDO0lBRWEsOEJBQWdCLEdBQTlCO1FBQ0ksT0FBTyxhQUFhLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDO0lBQzVFLENBQUM7SUFFYSxxQ0FBdUIsR0FBckM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsWUFBWTtZQUNwQyxhQUFhLENBQUMsTUFBTSxDQUFDLFFBQVE7WUFDN0IsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzVFLGFBQWEsQ0FBQyxNQUFNLENBQUMsV0FBVztZQUNoQyxrQ0FBa0MsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7Ozs7Ozs7O1NBUUs7SUFDTCxhQUFhO0lBQ0MsZ0RBQWtDLEdBQWhEO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLFlBQVk7WUFDcEMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRO1lBQzdCLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM1RSxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVc7WUFDaEMsOEJBQThCLENBQUM7UUFDbkMseUNBQXlDO0lBQzdDLENBQUM7SUFFYSxxQ0FBdUIsR0FBckM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUM7SUFDckQsQ0FBQztJQUVhLDJDQUE2QixHQUEzQztRQUNJLE9BQU8sYUFBYSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7SUFDL0MsQ0FBQztJQUVhLDRCQUFjLEdBQTVCO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUM1QyxDQUFDO0lBRWEseUJBQVcsR0FBekI7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFYSxnQ0FBa0IsR0FBaEM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO0lBQ2hELENBQUM7SUFFYSxpQ0FBbUIsR0FBakM7UUFDSSxPQUFPLGFBQWEsQ0FBQyxNQUFNLENBQUMsNkJBQTZCLENBQUM7SUFDOUQsQ0FBQztJQUVhLHFDQUF1QixHQUFyQztRQUNJLE9BQU8sYUFBYSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQztJQUNyRCxDQUFDO0lBRWEsa0NBQW9CLEdBQWxDO1FBQ0ksT0FBTyxhQUFhLENBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDO0lBQ2xELENBQUM7SUF6S0QsMENBQTBDO0lBQzVCLHVCQUFTLEdBQWtCLElBQUksYUFBYSxFQUFFLENBQUM7SUFDL0Msb0JBQU0sR0FBYyxJQUFJLENBQUM7SUFDekIseUJBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDeEMsc0JBQVEsR0FBRyxFQUFFLENBQUM7SUFDZCwyQkFBYSxHQUFHLEVBQUUsQ0FBQztJQTBLckMsb0JBQUM7Q0FBQSxBQWpMRCxJQWlMQztTQWpMWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgY29va2llIGZyb20gJ2pzLWNvb2tpZSc7XHJcbmltcG9ydCB7IFNlc3Npb25JbmZvIH0gZnJvbSAnLi4vYXV0aC9zZXNzaW9uVXRpbCc7XHJcbmltcG9ydCB7IEVudkNvbmZpZyB9IGZyb20gJy4vZW52Q29uZmlnJztcclxuaW1wb3J0IHsgaXNEZXZNb2RlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgR2xvYWJhbENvbmZpZyB7XHJcblxyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiB2YXJpYWJsZS1uYW1lXHJcbiAgICBwdWJsaWMgc3RhdGljIF9pbnN0YW5jZTogR2xvYWJhbENvbmZpZyA9IG5ldyBHbG9hYmFsQ29uZmlnKCk7XHJcbiAgICBwdWJsaWMgc3RhdGljIGNvbmZpZzogRW52Q29uZmlnID0gbnVsbDtcclxuICAgIHB1YmxpYyBzdGF0aWMgc2Vzc2lvblV0aWwgPSBTZXNzaW9uSW5mby5nZXRJbnN0YW5jZSgpO1xyXG4gICAgcHVibGljIHN0YXRpYyBwcm90b2NhbCA9ICcnO1xyXG4gICAgcHVibGljIHN0YXRpYyB3ZWJTZXJ2aWNlVXJsID0gJyc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgaWYgKEdsb2FiYWxDb25maWcuX2luc3RhbmNlKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignRXJyb3I6IEluc3RhbnRpYXRpb24gZmFpbGVkOiBVc2UgU2Vzc2lvbkluZm8uZ2V0SW5zdGFuY2UoKSBpbnN0ZWFkIG9mIG5ldy4nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgR2xvYWJhbENvbmZpZy5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0SW5zdGFuY2UoKTogR2xvYWJhbENvbmZpZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuX2luc3RhbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgc2V0Q29uZmlnKGNvbmZpZ09iaikge1xyXG4gICAgICAgIEdsb2FiYWxDb25maWcuY29uZmlnID0gY29uZmlnT2JqO1xyXG4gICAgICAgIEdsb2FiYWxDb25maWcucHJvdG9jYWwgPSBHbG9hYmFsQ29uZmlnLmNvbmZpZy5nYXRld2F5UHJvdG9jb2w7XHJcbiAgICAgICAgR2xvYWJhbENvbmZpZy53ZWJTZXJ2aWNlVXJsID0gR2xvYWJhbENvbmZpZy5wcm90b2NhbCArIGxvY2F0aW9uLmhvc3RcclxuICAgICAgICAgICAgKyAnLycgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5wc0hvbWUgKyAnLycgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0T3RtbVNlc3Npb25Db29raWVOYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuY29uZmlnLm90bW1TZXNzaW9uQ29va2llTmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldE9yZ2FuaXphdGlvbkROKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuICdvPScgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lICsgJywnICsgR2xvYWJhbENvbmZpZy5jb25maWcuaW5zdGFuY2VJZGVudGlmaWVyO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0T3JnYW5pemF0aW9uTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0UHJlTG9naW5VcmwoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy53ZWJTZXJ2aWNlVXJsICsgR2xvYWJhbENvbmZpZy5jb25maWcucHJlTG9naW5Vcmw7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRBdXRoVXJsKHVybDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBjb25zdCBjb29raWVOYW1lID0gdGhpcy5zZXNzaW9uVXRpbC5nZXRTZXNzaW9uUHJvcCh0aGlzLnNlc3Npb25VdGlsLmNoZWNrTmFtZSk7XHJcbiAgICAgICAgY29uc3QgY29va2llVmFsdWUgPSBjb29raWVOYW1lXHJcbiAgICAgICAgICAgID8gY29va2llLmdldChjb29raWVOYW1lKVxyXG4gICAgICAgICAgICA6ICcnO1xyXG4gICAgICAgIGlmIChjb29raWVWYWx1ZSkge1xyXG4gICAgICAgICAgICB1cmwgKz0gKCc/JyArIGNvb2tpZU5hbWUgKyAnPScgKyBjb29raWVWYWx1ZSk7XHJcbiAgICAgICAgICAgIHVybCArPSAoJyZvcmdhbml6YXRpb249JyArIGVuY29kZVVSSUNvbXBvbmVudChHbG9hYmFsQ29uZmlnLmdldE9yZ2FuaXphdGlvbkROKCkpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHVybDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldFBzT3JnRW5kcG9pbnQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gR2xvYWJhbENvbmZpZy5jb25maWcuZ2F0ZXdheVByb3RvY29sICsgR2xvYWJhbENvbmZpZy5jb25maWcuZ2F0ZXdheUhvc3RcclxuICAgICAgICAgICAgKyAoR2xvYWJhbENvbmZpZy5jb25maWcuZ2F0ZXdheVBvcnQgPyAoJzonICsgR2xvYWJhbENvbmZpZy5jb25maWcuZ2F0ZXdheVBvcnQpIDogJycpXHJcbiAgICAgICAgICAgICsgJy8nICsgR2xvYWJhbENvbmZpZy5jb25maWcucHNIb21lICsgJy8nICsgR2xvYWJhbENvbmZpZy5jb25maWcub3JnYW5pemF0aW9uTmFtZTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRUaWNrZXRDb25zdW1lclVSTCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLndlYlNlcnZpY2VVcmxcclxuICAgICAgICAgICAgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5wc1RpY2tldENvbnN1bWVyVXJsXHJcbiAgICAgICAgICAgICsgJz9SZWxheVN0YXRlPScgKyBlbmNvZGVVUklDb21wb25lbnQoR2xvYWJhbENvbmZpZy5nZXRQc09yZ0VuZHBvaW50KCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0U0FNTGFydCgpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGNvb2tpZU5hbWUgPSB0aGlzLnNlc3Npb25VdGlsLmdldFNlc3Npb25Qcm9wKHRoaXMuc2Vzc2lvblV0aWwuc2FtbEFydGlmYWN0Q29va2llTmFtZSk7XHJcbiAgICAgICAgY29uc3QgY29va2llVmFsdWUgPSBjb29raWVOYW1lXHJcbiAgICAgICAgICAgID8gY29va2llLmdldChjb29raWVOYW1lKVxyXG4gICAgICAgICAgICA6ICcnO1xyXG4gICAgICAgIGlmICghY29va2llVmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY29va2llVmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRHYXRld2F5VXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuZ2V0QXV0aFVybCh0aGlzLndlYlNlcnZpY2VVcmwgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5nYXRld2F5VXJsKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldEdhdGV3YXlDb29raWVEb21haW4oKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gbG9jYXRpb24uaG9zdG5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTTG9naW5Gb3JtKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud2ViU2VydmljZVVybCArIEdsb2FiYWxDb25maWcuY29uZmlnLnBzU1NPTG9naW5VcmwgKyAnP29yZ2FuaXphdGlvbj0nXHJcbiAgICAgICAgICAgICsgZW5jb2RlVVJJQ29tcG9uZW50KCdvPScgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lXHJcbiAgICAgICAgICAgICAgICArICcsJyArIEdsb2FiYWxDb25maWcuY29uZmlnLmluc3RhbmNlSWRlbnRpZmllcikgKyAnJmxhbmd1YWdlPWVuLVVTJm90ZHNhdXRoPW5vLXNzbyc7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTQmFzZURvbWFpblVybCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUHJvdG9jb2xcclxuICAgICAgICAgICAgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzSG9zdFxyXG4gICAgICAgICAgICArIChHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUG9ydCA/ICgnOicgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUG9ydCkgOiAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTTG9naW5QYWdlVXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuZ2V0T1REU0Jhc2VEb21haW5VcmwoKVxyXG4gICAgICAgICAgICArIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNSZXN0VXJsICsgJy9sb2dpbj9SRkE9J1xyXG4gICAgICAgICAgICArIEdsb2FiYWxDb25maWcuY29uZmlnLnBzT3Rkc1Jlc291cmNlICsgZW5jb2RlVVJJQ29tcG9uZW50KFxyXG4gICAgICAgICAgICAgICAgJzonICsgR2xvYWJhbENvbmZpZy5nZXRQc09yZ0VuZHBvaW50KClcclxuICAgICAgICAgICAgICAgICsgR2xvYWJhbENvbmZpZy5jb25maWcucHNUaWNrZXRDb25zdW1lclVybFxyXG4gICAgICAgICAgICAgICAgKyAnP1JlbGF5U3RhdGU9JyArIGVuY29kZVVSSUNvbXBvbmVudChHbG9hYmFsQ29uZmlnLmdldFBzT3JnRW5kcG9pbnQoKVxyXG4gICAgICAgICAgICAgICAgICAgICsgR2xvYWJhbENvbmZpZy5jb25maWcucHNEZXBsb3llZFBhdGgpXHJcbiAgICAgICAgICAgICkgKyAnJlBvc3RUaWNrZXQ9dHJ1ZSZsYW5ndWFnZT1lbi1VUyc7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRQU1NTT0xvZ2luVXJsKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcud2ViU2VydmljZVVybCArIEdsb2FiYWxDb25maWcuY29uZmlnLnBzU1NPTG9naW5Vcmw7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTUkVTVEF1dGhFbmRwb2ludCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUHJvdG9jb2wgK1xyXG4gICAgICAgICAgICBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzSG9zdCArXHJcbiAgICAgICAgICAgIChHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUG9ydCA/ICgnOicgKyBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUG9ydCkgOiAnJykgK1xyXG4gICAgICAgICAgICBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzUmVzdFVybCArXHJcbiAgICAgICAgICAgICcvcmVzdC9hdXRoZW50aWNhdGlvbi9jcmVkZW50aWFscyc7XHJcbiAgICB9XHJcblxyXG4gICAgLyogIHB1YmxpYyBzdGF0aWMgZ2V0T1REU1JFU1RBdXRoRW5kcG9pbnR3aXRoSGVhZGVycygpOiBzdHJpbmcge1xyXG4gICAgICAgICByZXR1cm4gR2xvYWJhbENvbmZpZy5jb25maWcub3Rkc1Byb3RvY29sICtcclxuICAgICAgICAgICAgIChpc0Rldk1vZGUoKSA/ICcuLycgOiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzSG9zdCArXHJcbiAgICAgICAgICAgICAgICAgKEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNQb3J0ID8gKCc6JyArIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNQb3J0KSA6ICcnKSArXHJcbiAgICAgICAgICAgICAgICAgR2xvYWJhbENvbmZpZy5jb25maWcub3Rkc1Jlc3RVcmwpICtcclxuICAgICAgICAgICAgIC8vICAgJy9yZXN0L2F1dGhlbnRpY2F0aW9uL2NyZWRlbnRpYWxzJztcclxuICAgICAgICAgICAgIC8vIE1QTVYzLTIyMzdcclxuICAgICAgICAgICAgICdvdGRzd3MvcmVzdC9hdXRoZW50aWNhdGlvbi9oZWFkZXJzJyAgLy8nL3Jlc3QvYXV0aGVudGljYXRpb24vY3JlZGVudGlhbHMvaGVhZGVycyc7XHJcbiAgICAgfSAqL1xyXG4gICAgLy8gTVBNVjMtMjIzN1xyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTUkVTVEF1dGhFbmRwb2ludHdpdGhIZWFkZXJzKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNQcm90b2NvbCArXHJcbiAgICAgICAgICAgIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNIb3N0ICtcclxuICAgICAgICAgICAgKEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNQb3J0ID8gKCc6JyArIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNQb3J0KSA6ICcnKSArXHJcbiAgICAgICAgICAgIEdsb2FiYWxDb25maWcuY29uZmlnLm90ZHNSZXN0VXJsICtcclxuICAgICAgICAgICAgJy9yZXN0L2F1dGhlbnRpY2F0aW9uL2hlYWRlcnMnO1xyXG4gICAgICAgIC8vICdvdGRzd3MvcmVzdC9hdXRoZW50aWNhdGlvbi9oZWFkZXJzJyAgXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRPVERTVGlja2V0Q29va2llTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5vdGRzVGlja2V0Q29va2llTmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldFByb2Nlc3NTdWl0ZU9URFNSZXNvdXJjZUlkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuY29uZmlnLnBzT3Rkc1Jlc291cmNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0TWF4RmlsZVNpemUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gR2xvYWJhbENvbmZpZy5jb25maWcubWF4RmlsZVNpemU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRNYXhGaWxlcygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5tYXhGaWxlcyA/IEdsb2FiYWxDb25maWcuY29uZmlnLm1heEZpbGVzIDogMTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldEJyYW5kQ29uZmlnUGF0aCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5icmFuZENvbmZpZ1BhdGg7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRGaWVsZFJlc3RyaWN0aW9uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5lbmFibGVJbmRleGVyRmllbGRSZXN0cmljdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGdldE1heGltdW1GaWVsZHNBbGxvd2VkKCk6IG51bWJlcntcclxuICAgICAgICByZXR1cm4gR2xvYWJhbENvbmZpZy5jb25maWcubWF4aW11bUZpZWxkc0FsbG93ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBnZXRGcmVlemVDb2x1bW5Db3VudCgpOiBudW1iZXJ7XHJcbiAgICAgICAgcmV0dXJuIEdsb2FiYWxDb25maWcuY29uZmlnLmZyZWV6ZUNvbHVtbkNvdW50O1xyXG4gICAgfVxyXG4gICAgLy9NVlNTLTMyMlxyXG4gICAgLy8gcHVibGljIHN0YXRpYyBnZXRDdXN0b21Xb3JrZmxvd1BhZ2VTaXplKCk6IG51bWJlcntcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhHbG9hYmFsQ29uZmlnLmNvbmZpZy5jdXN0b21Xb3JrZmxvd1BhZ2VTaXplKVxyXG4gICAgLy8gICAgIHJldHVybiBHbG9hYmFsQ29uZmlnLmNvbmZpZy5jdXN0b21Xb3JrZmxvd1BhZ2VTaXplO1xyXG4gICAgLy8gfVxyXG59XHJcbiJdfQ==