import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as acronui from '../auth/utility';
import { SharingService } from './sharing.service';
import { AppService } from './app.service';
import { Observable } from 'rxjs';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "./sharing.service";
import * as i2 from "./app.service";
var EntityAppDefService = /** @class */ (function () {
    function EntityAppDefService(sharingService, appService) {
        this.sharingService = sharingService;
        this.appService = appService;
        this.applicationDetails = {};
        this.userDetails = {};
        this.masterRoles = [];
        this.PUBLISHED_ROLE_DN_CONSTANT = 'cn=packages';
        this.PRIORITY_NS = 'http://schemas/AcheronMPMCore/MPM_Priority/operations';
        this.MENU_NS = 'http://schemas.acheron.com/mpm/app/bpm/1.0';
        this.MPM_METADATA_FIELDS_NS = 'http://schemas/AcheronMPMCore/MPM_Metadata_Fields/operations';
        this.MPM_CATEGORY_METADATA_FIELDS_NS = 'http://schemas/AcheronMPMCore/MPM_Category_Metadata_Fields/operations';
        this.STATUS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Status/operations';
        this.PRIORITY_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Priority/operations';
        this.TEAM_ROLE_MAPPING_NS = 'http://schemas/AcheronMPMTeams/MPM_Team_Role_Mapping/operations';
        this.ENTITY_USER_NS = 'http://schemas/OpenTextEntityIdentityComponents/User/operations';
        this.ENTITY_USER_IDENTITY_NS = 'http://schemas/OpenTextEntityIdentityComponents/Identity';
        this.GET_MENU_FOR_USER_WS = 'GetMenuForUser';
        this.GET_ALL_METADATA_FIELDS_BY_TYPE_WS = 'GetAllMetadataFieldsByType';
        this.GET_ALL_CUSTOM_METADATA_FIELDS_BY_CATEGORY_LEVEL_WS = 'GetAllMetadataFieldsByCategoryLevel';
        this.GET_STATUS_BY_ID_WS = 'ReadMPM_Status';
        this.GET_ALL_STATUS_BY_CATEGORY_AND_STATUS_WS = 'GetAllStatusForCategoryStatusTypeName';
        this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS = 'GetAllStatusForCategoryLevel';
        this.GET_ALL_PRIORITIES_WS = 'GetAllPriorities';
        this.GET_PRIORITY_BY_CATEGORY_LEVEL_NAME_WS = 'GetAllPriorityForCategoryLevelName';
        this.GET_TEAM_ROLES_FOR_TEAM_WS = 'GetTeamRolesForTeam';
        this.READ_USER_ENTITYS = 'ReadUser';
    }
    EntityAppDefService.prototype.setApplicationDetails = function (appDetails) {
        var _this = this;
        this.applicationDetails = appDetails;
        return new Observable(function (observer) {
            _this.getOtmmAssetTemplateDetails()
                .subscribe(function (response) {
                _this.sharingService.setMediaManagerConfig(response);
                observer.next(true);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.setUserDN = function (userDetails) {
        console.log(userDetails);
        var userDetail = acronui.findObjectsByProp(userDetails, 'User');
        if (userDetail && Array.isArray(userDetail) && userDetail != null) {
            sessionStorage.setItem(SessionStorageConstants.OTDS_USER_DN, JSON.stringify(userDetail[0].UserDN));
            localStorage.setItem('OTDS_USER_DN', userDetail[0].UserDN);
        }
        else {
            throw new Error('No user details available.');
        }
    };
    EntityAppDefService.prototype.setUserDetails = function (userInfo) {
        this.userDetails = acronui.findObjectsByProp(userInfo, 'User');
    };
    EntityAppDefService.prototype.getApplicationDetails = function () {
        if (this.applicationDetails && this.applicationDetails != null && this.applicationDetails &&
            this.applicationDetails) {
            return this.applicationDetails;
        }
        else {
            throw new Error('No application details available.');
        }
    };
    EntityAppDefService.prototype.getUserDetails = function () {
        if (this.userDetails && Array.isArray(this.userDetails) && this.userDetails != null) {
            return this.userDetails;
        }
        else {
            throw new Error('No user details available.');
        }
    };
    EntityAppDefService.prototype.getApplicationID = function () {
        if (this.applicationDetails && this.applicationDetails.APPLICATION_ID &&
            this.applicationDetails.APPLICATION_ID != null &&
            typeof this.applicationDetails.APPLICATION_ID !== 'undefined' &&
            this.applicationDetails.APPLICATION_ID !== '') {
            return this.applicationDetails.APPLICATION_ID;
        }
    };
    EntityAppDefService.prototype.getCordysPlatformVersion = function () {
        if (this.applicationDetails && this.applicationDetails.PLATFORM_VERSION &&
            this.applicationDetails.PLATFORM_VERSION != null && this.applicationDetails.PLATFORM_VERSION !== '') {
            return this.applicationDetails.PLATFORM_VERSION;
        }
        else {
            return '';
        }
    };
    EntityAppDefService.prototype.getApplicationName = function () {
        if (this.applicationDetails && this.applicationDetails.APPLICATION_NAME &&
            this.applicationDetails.APPLICATION_NAME !== null &&
            typeof this.applicationDetails.APPLICATION_NAME !== 'undefined' &&
            this.applicationDetails.APPLICATION_NAME !== '') {
            return this.applicationDetails.APPLICATION_NAME;
        }
    };
    EntityAppDefService.prototype.translateRoleDNSuffix = function (roleDN) {
        var translatedRoleDN = '';
        if (this.applicationDetails && this.applicationDetails.APPLICATION_PACKAGE_TYPE &&
            this.applicationDetails.APPLICATION_PACKAGE_TYPE != null &&
            this.applicationDetails.APPLICATION_PACKAGE_TYPE !== '') {
            if (this.applicationDetails.APPLICATION_PACKAGE_TYPE === 'PUBLISHED') {
                translatedRoleDN = this.applicationDetails.APPLICATION_ACCESS_ROLE.split(',')[1] + ',' +
                    this.PUBLISHED_ROLE_DN_CONSTANT + ',' + this.sharingService.getCurrentOrgDN();
                return translatedRoleDN;
            }
            else {
                return roleDN;
            }
        }
        else {
            return roleDN;
        }
    };
    EntityAppDefService.prototype.getRoleDnSuffixForUserServices = function () {
        var returnVal;
        if (this.applicationDetails && this.applicationDetails.APPLICATION_ACCESS_ROLE &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE != null &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE !== '' &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE) {
            // tslint:disable-next-line: max-line-length
            returnVal = this.applicationDetails.APPLICATION_ACCESS_ROLE.substr(this.applicationDetails.APPLICATION_ACCESS_ROLE.indexOf(',') + 1, this.applicationDetails.APPLICATION_ACCESS_ROLE.length - 1);
            if (returnVal && returnVal != null && returnVal !== '') {
                return this.translateRoleDNSuffix(returnVal);
            }
            else {
                return '';
            }
        }
        else {
            throw new Error('No information to send role dn suffix available.');
        }
    };
    EntityAppDefService.prototype.getRoleDnSuffix = function () {
        var returnVal;
        if (this.applicationDetails && this.applicationDetails.APPLICATION_ACCESS_ROLE &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE != null &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE !== '' &&
            this.applicationDetails.APPLICATION_ACCESS_ROLE) {
            // tslint:disable-next-line: max-line-length
            returnVal = this.applicationDetails.APPLICATION_ACCESS_ROLE.substr(this.applicationDetails.APPLICATION_ACCESS_ROLE.indexOf(',') + 1, this.applicationDetails.APPLICATION_ACCESS_ROLE.length - 1);
            if (returnVal && returnVal != null && returnVal !== '') {
                return returnVal;
            }
            else {
                return '';
            }
        }
        else {
            console.warn('No information to send role dn suffix available.');
        }
    };
    EntityAppDefService.prototype.getOtmmAssetTemplateDetails = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (_this.applicationDetails && _this.applicationDetails && _this.applicationDetails.APPLICATION_ID !== ''
                && _this.applicationDetails.APPLICATION_ID != null && _this.applicationDetails.APPLICATION_ID) {
                var parameters = {
                    applicationID: _this.applicationDetails.APPLICATION_ID,
                };
                _this.appService.getMediaManagerConfig(parameters)
                    .subscribe(function (response) {
                    var mediaManagerConfig = acronui.findObjectsByProp(response, 'MPM_Media_Manager_Config');
                    if (mediaManagerConfig.length > 0) {
                        mediaManagerConfig = mediaManagerConfig[0];
                        var data = {
                            url: mediaManagerConfig.MEDIA_MANAGER_URL,
                            callbackType: mediaManagerConfig.CALLBACK_TYPE,
                            sessionTimeOut: mediaManagerConfig.SESSION_TIMEOUT,
                            apiVersion: mediaManagerConfig.API_VERSION,
                            enableQDS: mediaManagerConfig.ENABLE_QDS === 'true' ? true : false,
                            qdsServerURL: mediaManagerConfig.QDS_SERVER_URL,
                            otdsResourceName: mediaManagerConfig.OTDS_RESOURCE_NAME
                        };
                        observer.next(data);
                        observer.complete();
                    }
                    else {
                        observer.error('No Media Manager config available.');
                    }
                }, function () {
                    observer.error('Something went wrong while getting Media Manager configuration.');
                });
            }
            else {
                observer.error('Application Id is invalid.');
            }
        });
    };
    EntityAppDefService.prototype.setMasterRoles = function (masterRole) {
        if (masterRole && masterRole) {
            this.masterRoles = masterRole;
        }
    };
    EntityAppDefService.prototype.hasRole = function (roleDN) {
        var hasRole = false;
        if (roleDN != null && roleDN !== '' && roleDN) {
            var org = acronui.findObjects(this.masterRoles, 'dn', this.sharingService.getCurrentOrgDN());
            if (org && org.length && org.length > 0) {
                var role = acronui.findObjects(org, '@id', roleDN);
                if (role && role != null && Array.isArray(role) && role.length > 0) {
                    hasRole = true;
                }
                else {
                    hasRole = false;
                }
            }
            else {
                hasRole = false;
            }
        }
        else {
            hasRole = true;
        }
        return hasRole;
    };
    /* getAllPriorities(): Observable<any> {
        return new Observable(observer => {
            this.appService.invokeRequest(this.PRIORITY_NS, this.GET_ALL_PRIORITIES_WS, null)
                .subscribe(response => {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Priority'));
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
        });
    } */
    EntityAppDefService.prototype.getAllPriorities = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.PRIORITY_NS, _this.GET_ALL_PRIORITIES_WS, null).subscribe(function (response) {
                    var priority = acronui.findObjectsByProp(response, 'MPM_Priority');
                    sessionStorage.setItem(SessionStorageConstants.ALL_PRIORITIES, JSON.stringify(priority));
                    observer.next(priority);
                    observer.complete();
                });
            }
        });
    };
    /* getMenuForUser(): Observable<any> {
        return new Observable(observer => {
            this.appService.invokeRequest(this.MENU_NS, this.GET_MENU_FOR_USER_WS, null)
                .subscribe(response => {
                    observer.next(acronui.findObjectsByProp(response, 'MPM_Menu'));
                    observer.complete();
                }, error => {
                    observer.error(error);
                });
        });
    } */
    EntityAppDefService.prototype.getMenuForUser = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.MENU_FOR_USER) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.MENU_FOR_USER)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.MENU_NS, _this.GET_MENU_FOR_USER_WS, null).subscribe(function (response) {
                    var menu = acronui.findObjectsByProp(response, 'MPM_Menu');
                    sessionStorage.setItem(SessionStorageConstants.MENU_FOR_USER, JSON.stringify(menu));
                    observer.next(menu);
                    observer.complete();
                });
            }
        });
    };
    EntityAppDefService.prototype.getAllMetadataFieldsByType = function (type) {
        var _this = this;
        var parameters = {
            applicationID: this.getApplicationID(),
            type: type
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.MPM_METADATA_FIELDS_NS, _this.GET_ALL_METADATA_FIELDS_BY_TYPE_WS, parameters)
                .subscribe(function (response) {
                observer.next(acronui.findObjectsByProp(response, 'MPM_Metadata_Fields'));
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getAllDisplayFieldsForView = function (viewType, categoryId) {
        var _this = this;
        var parameters = {
            viewType: viewType,
            categoryID: categoryId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest('http://schemas/AcheronMPMCore/MPM_Task_View_Config/operations', 'GetTaskViewConfig', parameters)
                .subscribe(function (response) {
                observer.next(acronui.findObjectsByProp(response, 'MPM_Task_View_Config'));
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getAllCustomMetadataFieldsByCategoryLevel = function (categoryLevelId) {
        var _this = this;
        var parameters = {
            categoryLevelID: categoryLevelId
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.MPM_CATEGORY_METADATA_FIELDS_NS, _this.GET_ALL_CUSTOM_METADATA_FIELDS_BY_CATEGORY_LEVEL_WS, parameters)
                .subscribe(function (response) {
                observer.next(acronui.findObjectsByProp(response, 'MPM_Category_Metadata_Fields'));
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getStatusById = function (statusId) {
        var _this = this;
        var getRequestObject = {
            'MPM_Status-id': {
                Id: statusId
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_STATUS_BY_ID_WS, getRequestObject)
                .subscribe(function (response) {
                observer.next(acronui.findObjectByProp(response, 'MPM_Status'));
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getStatus = function (categoryLevelName, statusTypeName) {
        var _this = this;
        var getRequestObject = {
            categoryLevelName: categoryLevelName,
            statusType: statusTypeName
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_ALL_STATUS_BY_CATEGORY_AND_STATUS_WS, getRequestObject)
                .subscribe(function (response) {
                var statuses = acronui.findObjectsByProp(response, 'MPM_Status');
                observer.next(statuses);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getStatusByCategoryLevel = function (categoryLevelName) {
        var _this = this;
        var getRequestObject = {
            categoryLevelName: categoryLevelName,
            categoryLevelID: ''
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS, getRequestObject)
                .subscribe(function (response) {
                var statuses = acronui.findObjectsByProp(response, 'MPM_Status');
                observer.next(statuses);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    /*  getPriorities(categoryLevelName: MPM_LEVEL): Observable<Array<Priority>> {
         const getRequestObject = {
             categoryLevelName: categoryLevelName
         };
         return new Observable(observer => {
             this.appService.invokeRequest(this.PRIORITY_METHOD_NS, this.GET_PRIORITY_BY_CATEGORY_LEVEL_NAME_WS, getRequestObject)
                 .subscribe(response => {
                     const priorities: Array<Priority> = acronui.findObjectsByProp(response, 'MPM_Priority');
                     observer.next(priorities);
                     observer.complete();
                 }, error => {
                     observer.error(error);
                 });
         });
     } */
    // added for code hardeniing 
    EntityAppDefService.prototype.getPriorities = function (categoryLevelName) {
        var currCategoryObj = this.sharingService.getCategoryLevels().find(function (data) {
            return data.CATEGORY_LEVEL_TYPE === categoryLevelName;
        });
        if (currCategoryObj) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES) !== null) {
                var allStatuses = JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_PRIORITIES));
                var allStatus = allStatuses.filter(function (status) {
                    return status.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currCategoryObj['MPM_Category_Level-id'].Id;
                });
                return allStatus;
            }
            else {
                var allStatus = this.sharingService.getAllPriorities().filter(function (status) {
                    return status.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currCategoryObj['MPM_Category_Level-id'].Id;
                });
                return allStatus;
            }
        }
        return [];
    };
    EntityAppDefService.prototype.getTeamRolesForTeam = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.TEAM_ROLE_MAPPING_NS, _this.GET_TEAM_ROLES_FOR_TEAM_WS, parameters)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.readUser = function (parameters) {
        var _this = this;
        var readUserRequest = {
            'Identity-id': {
                '@xmlns': this.ENTITY_USER_IDENTITY_NS,
                Id: parameters.Id,
                ItemId: parameters.ItemId
            }
        };
        return new Observable(function (observer) {
            _this.appService.invokeRequest(_this.ENTITY_USER_NS, _this.READ_USER_ENTITYS, readUserRequest)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    EntityAppDefService.prototype.getOtmmAssetUploadDetails = function () {
        var otmmAssetTemplateObj = this.sharingService.getMediaManagerConfig();
        if (otmmAssetTemplateObj && otmmAssetTemplateObj != null) {
            return otmmAssetTemplateObj;
        }
        else {
            console.warn('No OTMM Config available.');
        }
    };
    EntityAppDefService.ctorParameters = function () { return [
        { type: SharingService },
        { type: AppService }
    ]; };
    EntityAppDefService.ɵprov = i0.ɵɵdefineInjectable({ factory: function EntityAppDefService_Factory() { return new EntityAppDefService(i0.ɵɵinject(i1.SharingService), i0.ɵɵinject(i2.AppService)); }, token: EntityAppDefService, providedIn: "root" });
    EntityAppDefService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], EntityAppDefService);
    return EntityAppDefService;
}());
export { EntityAppDefService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5LmFwcGRlZi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL2VudGl0eS5hcHBkZWYuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEtBQUssT0FBTyxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFPbEMsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sa0RBQWtELENBQUM7Ozs7QUFPM0Y7SUFFSSw2QkFDVyxjQUE4QixFQUM5QixVQUFzQjtRQUR0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUdqQyx1QkFBa0IsR0FBUSxFQUFFLENBQUM7UUFDN0IsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFFdEIsZ0JBQVcsR0FBZSxFQUFFLENBQUM7UUFDN0IsK0JBQTBCLEdBQUcsYUFBYSxDQUFDO1FBRTNDLGdCQUFXLEdBQUcsdURBQXVELENBQUM7UUFDdEUsWUFBTyxHQUFHLDRDQUE0QyxDQUFDO1FBQ3ZELDJCQUFzQixHQUFHLDhEQUE4RCxDQUFDO1FBQ3hGLG9DQUErQixHQUFHLHVFQUF1RSxDQUFDO1FBQzFHLHFCQUFnQixHQUFHLHFEQUFxRCxDQUFDO1FBQ3pFLHVCQUFrQixHQUFHLHVEQUF1RCxDQUFDO1FBQzdFLHlCQUFvQixHQUFHLGlFQUFpRSxDQUFDO1FBQ3pGLG1CQUFjLEdBQUcsaUVBQWlFLENBQUM7UUFDbkYsNEJBQXVCLEdBQUcsMERBQTBELENBQUM7UUFHckYseUJBQW9CLEdBQUcsZ0JBQWdCLENBQUM7UUFDeEMsdUNBQWtDLEdBQUcsNEJBQTRCLENBQUM7UUFDbEUsd0RBQW1ELEdBQUcscUNBQXFDLENBQUM7UUFDNUYsd0JBQW1CLEdBQUcsZ0JBQWdCLENBQUM7UUFDdkMsNkNBQXdDLEdBQUcsdUNBQXVDLENBQUM7UUFDbkYseUNBQW9DLEdBQUcsOEJBQThCLENBQUM7UUFDdEUsMEJBQXFCLEdBQUcsa0JBQWtCLENBQUM7UUFDM0MsMkNBQXNDLEdBQUcsb0NBQW9DLENBQUM7UUFDOUUsK0JBQTBCLEdBQUcscUJBQXFCLENBQUM7UUFDbkQsc0JBQWlCLEdBQUcsVUFBVSxDQUFDO0lBNUIzQixDQUFDO0lBOEJMLG1EQUFxQixHQUFyQixVQUFzQixVQUFVO1FBQWhDLGlCQVlDO1FBWEcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFVBQVUsQ0FBQztRQUNyQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsMkJBQTJCLEVBQUU7aUJBQzdCLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDcEQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFTLEdBQVQsVUFBVSxXQUFXO1FBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsSUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNsRSxJQUFJLFVBQVUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFVBQVUsSUFBSSxJQUFJLEVBQUU7WUFDL0QsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNuRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDOUQ7YUFBTTtZQUNILE1BQU0sSUFBSSxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQztTQUNqRDtJQUVMLENBQUM7SUFFRCw0Q0FBYyxHQUFkLFVBQWUsUUFBUTtRQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVELG1EQUFxQixHQUFyQjtRQUNJLElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGtCQUFrQjtZQUNyRixJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDekIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7U0FDbEM7YUFBTTtZQUNILE1BQU0sSUFBSSxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQztTQUN4RDtJQUNMLENBQUM7SUFFRCw0Q0FBYyxHQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxFQUFFO1lBQ2pGLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUMzQjthQUFNO1lBQ0gsTUFBTSxJQUFJLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQztJQUVELDhDQUFnQixHQUFoQjtRQUNJLElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjO1lBQ2pFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLElBQUksSUFBSTtZQUM5QyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEtBQUssV0FBVztZQUM3RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxLQUFLLEVBQUUsRUFBRTtZQUMvQyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLENBQUM7U0FDakQ7SUFDTCxDQUFDO0lBRUQsc0RBQXdCLEdBQXhCO1FBQ0ksSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQjtZQUNuRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsS0FBSyxFQUFFLEVBQUU7WUFDckcsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7U0FDbkQ7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsZ0RBQWtCLEdBQWxCO1FBQ0ksSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQjtZQUNuRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLEtBQUssSUFBSTtZQUNqRCxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXO1lBQy9ELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsS0FBSyxFQUFFLEVBQUU7WUFDakQsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7U0FDbkQ7SUFDTCxDQUFDO0lBRUQsbURBQXFCLEdBQXJCLFVBQXNCLE1BQU07UUFDeEIsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QjtZQUMzRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsd0JBQXdCLElBQUksSUFBSTtZQUN4RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsd0JBQXdCLEtBQUssRUFBRSxFQUFFO1lBQ3pELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixLQUFLLFdBQVcsRUFBRTtnQkFDbEUsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHO29CQUNsRixJQUFJLENBQUMsMEJBQTBCLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ2xGLE9BQU8sZ0JBQWdCLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0gsT0FBTyxNQUFNLENBQUM7YUFDakI7U0FDSjthQUFNO1lBQ0gsT0FBTyxNQUFNLENBQUM7U0FDakI7SUFDTCxDQUFDO0lBRUQsNERBQThCLEdBQTlCO1FBQ0ksSUFBSSxTQUFTLENBQUM7UUFDZCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCO1lBQzFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsSUFBSSxJQUFJO1lBQ3ZELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsS0FBSyxFQUFFO1lBQ3RELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsRUFBRTtZQUVqRCw0Q0FBNEM7WUFDNUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUVqTSxJQUFJLFNBQVMsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3BELE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hEO2lCQUFNO2dCQUNILE9BQU8sRUFBRSxDQUFDO2FBQ2I7U0FFSjthQUFNO1lBQ0gsTUFBTSxJQUFJLEtBQUssQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO1NBQ3ZFO0lBQ0wsQ0FBQztJQUVELDZDQUFlLEdBQWY7UUFDSSxJQUFJLFNBQVMsQ0FBQztRQUNkLElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUI7WUFDMUUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixJQUFJLElBQUk7WUFDdkQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixLQUFLLEVBQUU7WUFDdEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixFQUFFO1lBRWpELDRDQUE0QztZQUM1QyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBRWpNLElBQUksU0FBUyxJQUFJLFNBQVMsSUFBSSxJQUFJLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTtnQkFDcEQsT0FBTyxTQUFTLENBQUM7YUFDcEI7aUJBQU07Z0JBQ0gsT0FBTyxFQUFFLENBQUM7YUFDYjtTQUNKO2FBQU07WUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLGtEQUFrRCxDQUFDLENBQUM7U0FDcEU7SUFDTCxDQUFDO0lBRUQseURBQTJCLEdBQTNCO1FBQUEsaUJBcUNDO1FBcENHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksS0FBSSxDQUFDLGtCQUFrQixJQUFJLEtBQUksQ0FBQyxrQkFBa0IsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxLQUFLLEVBQUU7bUJBQ2hHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLElBQUksSUFBSSxJQUFJLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEVBQUU7Z0JBQzdGLElBQU0sVUFBVSxHQUFHO29CQUNmLGFBQWEsRUFBRSxLQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYztpQkFDeEQsQ0FBQztnQkFDRixLQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQztxQkFDNUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFFZixJQUFJLGtCQUFrQixHQUFRLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztvQkFFOUYsSUFBSSxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMvQixrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFFM0MsSUFBTSxJQUFJLEdBQXVCOzRCQUM3QixHQUFHLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCOzRCQUN6QyxZQUFZLEVBQUUsa0JBQWtCLENBQUMsYUFBYTs0QkFDOUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLGVBQWU7NEJBQ2xELFVBQVUsRUFBRSxrQkFBa0IsQ0FBQyxXQUFXOzRCQUMxQyxTQUFTLEVBQUUsa0JBQWtCLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLOzRCQUNsRSxZQUFZLEVBQUUsa0JBQWtCLENBQUMsY0FBYzs0QkFDL0MsZ0JBQWdCLEVBQUUsa0JBQWtCLENBQUMsa0JBQWtCO3lCQUMxRCxDQUFDO3dCQUVGLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDO3FCQUN4RDtnQkFDTCxDQUFDLEVBQUU7b0JBQ0MsUUFBUSxDQUFDLEtBQUssQ0FBQyxpRUFBaUUsQ0FBQyxDQUFDO2dCQUN0RixDQUFDLENBQUMsQ0FBQzthQUNWO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQzthQUNoRDtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFjLEdBQWQsVUFBZSxVQUFVO1FBQ3JCLElBQUksVUFBVSxJQUFJLFVBQVUsRUFBRTtZQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQztTQUNqQztJQUNMLENBQUM7SUFFRCxxQ0FBTyxHQUFQLFVBQVEsTUFBTTtRQUNWLElBQUksT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxLQUFLLEVBQUUsSUFBSSxNQUFNLEVBQUU7WUFDM0MsSUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDL0YsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDckMsSUFBTSxJQUFJLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2hFLE9BQU8sR0FBRyxJQUFJLENBQUM7aUJBQ2xCO3FCQUFNO29CQUNILE9BQU8sR0FBRyxLQUFLLENBQUM7aUJBQ25CO2FBQ0o7aUJBQU07Z0JBQ0gsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUNuQjtTQUNKO2FBQU07WUFDSCxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ2xCO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVEOzs7Ozs7Ozs7O1FBVUk7SUFFSiw4Q0FBZ0IsR0FBaEI7UUFBQSxpQkFjQztRQWJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3pFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUYsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2hHLElBQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7b0JBQ3JFLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDekYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0Q7Ozs7Ozs7Ozs7UUFVSTtJQUVKLDRDQUFjLEdBQWQ7UUFBQSxpQkFjQztRQWJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxhQUFhLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3hFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekYsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzNGLElBQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQzdELGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDcEYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0Qsd0RBQTBCLEdBQTFCLFVBQTJCLElBQWU7UUFBMUMsaUJBY0M7UUFiRyxJQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdEMsSUFBSSxFQUFFLElBQUk7U0FDYixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLEtBQUksQ0FBQyxrQ0FBa0MsRUFBRSxVQUFVLENBQUM7aUJBQzFHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLHFCQUFxQixDQUFDLENBQUMsQ0FBQztnQkFDMUUsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdEQUEwQixHQUExQixVQUEyQixRQUFnQixFQUFFLFVBQWtCO1FBQS9ELGlCQWNDO1FBYkcsSUFBTSxVQUFVLEdBQUc7WUFDZixRQUFRLEVBQUUsUUFBUTtZQUNsQixVQUFVLEVBQUUsVUFBVTtTQUN6QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsK0RBQStELEVBQUUsbUJBQW1CLEVBQUUsVUFBVSxDQUFDO2lCQUMxSCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7Z0JBQzNFLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1RUFBeUMsR0FBekMsVUFBMEMsZUFBdUI7UUFBakUsaUJBYUM7UUFaRyxJQUFNLFVBQVUsR0FBRztZQUNmLGVBQWUsRUFBRSxlQUFlO1NBQ25DLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsK0JBQStCLEVBQUUsS0FBSSxDQUFDLG1EQUFtRCxFQUFFLFVBQVUsQ0FBQztpQkFDcEksU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsOEJBQThCLENBQUMsQ0FBQyxDQUFDO2dCQUNuRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMkNBQWEsR0FBYixVQUFjLFFBQWdCO1FBQTlCLGlCQWVDO1FBZEcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixlQUFlLEVBQUU7Z0JBQ2IsRUFBRSxFQUFFLFFBQVE7YUFDZjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixFQUFFLGdCQUFnQixDQUFDO2lCQUMzRixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUNBQVMsR0FBVCxVQUFVLGlCQUE0QixFQUFFLGNBQTBCO1FBQWxFLGlCQWVDO1FBZEcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixpQkFBaUIsRUFBRSxpQkFBaUI7WUFDcEMsVUFBVSxFQUFFLGNBQWM7U0FDN0IsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsd0NBQXdDLEVBQUUsZ0JBQWdCLENBQUM7aUJBQ2hILFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBTSxRQUFRLEdBQWtCLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ2xGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzREFBd0IsR0FBeEIsVUFBeUIsaUJBQTRCO1FBQXJELGlCQWVDO1FBZEcsSUFBTSxnQkFBZ0IsR0FBRztZQUNyQixpQkFBaUIsRUFBRSxpQkFBaUI7WUFDcEMsZUFBZSxFQUFFLEVBQUU7U0FDdEIsQ0FBQztRQUNGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMsb0NBQW9DLEVBQUUsZ0JBQWdCLENBQUM7aUJBQzVHLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBTSxRQUFRLEdBQWtCLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ2xGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7U0FjSztJQUVMLDZCQUE2QjtJQUM3QiwyQ0FBYSxHQUFiLFVBQWMsaUJBQTRCO1FBRXRDLElBQU0sZUFBZSxHQUFrQixJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSTtZQUNwRixPQUFPLElBQUksQ0FBQyxtQkFBbUIsS0FBSyxpQkFBaUIsQ0FBQztRQUMxRCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksZUFBZSxFQUFFO1lBQ2pCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3pFLElBQUksV0FBVyxHQUFvQixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDOUcsSUFBSSxTQUFTLEdBQW9CLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBQyxNQUFnQjtvQkFDakUsT0FBTyxNQUFNLENBQUMsbUJBQW1CLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLEtBQUssZUFBZSxDQUFDLHVCQUF1QixDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNsSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLFNBQVMsQ0FBQzthQUVwQjtpQkFBTTtnQkFDSCxJQUFJLFNBQVMsR0FBb0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFDLE1BQWdCO29CQUM1RixPQUFPLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ2xILENBQUMsQ0FBQyxDQUFDO2dCQUNILE9BQU8sU0FBUyxDQUFDO2FBQ3BCO1NBQ0o7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxpREFBbUIsR0FBbkIsVUFBb0IsVUFBVTtRQUE5QixpQkFVQztRQVRHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxLQUFJLENBQUMsMEJBQTBCLEVBQUUsVUFBVSxDQUFDO2lCQUNoRyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQ0FBUSxHQUFSLFVBQVMsVUFBVTtRQUFuQixpQkFpQkM7UUFoQkcsSUFBTSxlQUFlLEdBQUc7WUFDcEIsYUFBYSxFQUFFO2dCQUNYLFFBQVEsRUFBRSxJQUFJLENBQUMsdUJBQXVCO2dCQUN0QyxFQUFFLEVBQUUsVUFBVSxDQUFDLEVBQUU7Z0JBQ2pCLE1BQU0sRUFBRSxVQUFVLENBQUMsTUFBTTthQUM1QjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxlQUFlLENBQUM7aUJBQ3RGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVEQUF5QixHQUF6QjtRQUNJLElBQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ3pFLElBQUksb0JBQW9CLElBQUksb0JBQW9CLElBQUksSUFBSSxFQUFFO1lBQ3RELE9BQU8sb0JBQW9CLENBQUM7U0FDL0I7YUFBTTtZQUNILE9BQU8sQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztTQUM3QztJQUNMLENBQUM7O2dCQS9jMEIsY0FBYztnQkFDbEIsVUFBVTs7O0lBSnhCLG1CQUFtQjtRQUovQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsbUJBQW1CLENBb2QvQjs4QkF0ZUQ7Q0FzZUMsQUFwZEQsSUFvZEM7U0FwZFksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTWVkaWFNYW5hZ2VyQ29uZmlnIH0gZnJvbSAnLi4vb2JqZWN0cy9NZWRpYU1hbmFnZXJDb25maWcnO1xyXG5pbXBvcnQgeyBNUE1fTEVWRUwgfSBmcm9tICcuLi9vYmplY3RzL0xldmVsJztcclxuaW1wb3J0IHsgU3RhdHVzIH0gZnJvbSAnLi4vb2JqZWN0cy9TdGF0dXMnO1xyXG5pbXBvcnQgeyBTdGF0dXNUeXBlIH0gZnJvbSAnLi4vb2JqZWN0cy9TdGF0dXNUeXBlJztcclxuaW1wb3J0IHsgUHJpb3JpdHkgfSBmcm9tICcuLi9vYmplY3RzL1ByaW9yaXR5JztcclxuaW1wb3J0IHsgVmlld1R5cGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9WaWV3VHlwZXMnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IENhdGVnb3J5TGV2ZWwgfSBmcm9tICcuLi9vYmplY3RzL0NhdGVnb3J5TGV2ZWwnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRW50aXR5QXBwRGVmU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICkgeyB9XHJcblxyXG4gICAgYXBwbGljYXRpb25EZXRhaWxzOiBhbnkgPSB7fTtcclxuICAgIHVzZXJEZXRhaWxzOiBhbnkgPSB7fTtcclxuXHJcbiAgICBtYXN0ZXJSb2xlczogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgUFVCTElTSEVEX1JPTEVfRE5fQ09OU1RBTlQgPSAnY249cGFja2FnZXMnO1xyXG5cclxuICAgIFBSSU9SSVRZX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9Qcmlvcml0eS9vcGVyYXRpb25zJztcclxuICAgIE1FTlVfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2FwcC9icG0vMS4wJztcclxuICAgIE1QTV9NRVRBREFUQV9GSUVMRFNfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX01ldGFkYXRhX0ZpZWxkcy9vcGVyYXRpb25zJztcclxuICAgIE1QTV9DQVRFR09SWV9NRVRBREFUQV9GSUVMRFNfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0NhdGVnb3J5X01ldGFkYXRhX0ZpZWxkcy9vcGVyYXRpb25zJztcclxuICAgIFNUQVRVU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1N0YXR1cy9vcGVyYXRpb25zJztcclxuICAgIFBSSU9SSVRZX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fUHJpb3JpdHkvb3BlcmF0aW9ucyc7XHJcbiAgICBURUFNX1JPTEVfTUFQUElOR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNVGVhbXMvTVBNX1RlYW1fUm9sZV9NYXBwaW5nL29wZXJhdGlvbnMnO1xyXG4gICAgRU5USVRZX1VTRVJfTlMgPSAnaHR0cDovL3NjaGVtYXMvT3BlblRleHRFbnRpdHlJZGVudGl0eUNvbXBvbmVudHMvVXNlci9vcGVyYXRpb25zJztcclxuICAgIEVOVElUWV9VU0VSX0lERU5USVRZX05TID0gJ2h0dHA6Ly9zY2hlbWFzL09wZW5UZXh0RW50aXR5SWRlbnRpdHlDb21wb25lbnRzL0lkZW50aXR5JztcclxuXHJcblxyXG4gICAgR0VUX01FTlVfRk9SX1VTRVJfV1MgPSAnR2V0TWVudUZvclVzZXInO1xyXG4gICAgR0VUX0FMTF9NRVRBREFUQV9GSUVMRFNfQllfVFlQRV9XUyA9ICdHZXRBbGxNZXRhZGF0YUZpZWxkc0J5VHlwZSc7XHJcbiAgICBHRVRfQUxMX0NVU1RPTV9NRVRBREFUQV9GSUVMRFNfQllfQ0FURUdPUllfTEVWRUxfV1MgPSAnR2V0QWxsTWV0YWRhdGFGaWVsZHNCeUNhdGVnb3J5TGV2ZWwnO1xyXG4gICAgR0VUX1NUQVRVU19CWV9JRF9XUyA9ICdSZWFkTVBNX1N0YXR1cyc7XHJcbiAgICBHRVRfQUxMX1NUQVRVU19CWV9DQVRFR09SWV9BTkRfU1RBVFVTX1dTID0gJ0dldEFsbFN0YXR1c0ZvckNhdGVnb3J5U3RhdHVzVHlwZU5hbWUnO1xyXG4gICAgR0VUX1NUQVRVU19CWV9DQVRFR09SWV9MRVZFTF9OQU1FX1dTID0gJ0dldEFsbFN0YXR1c0ZvckNhdGVnb3J5TGV2ZWwnO1xyXG4gICAgR0VUX0FMTF9QUklPUklUSUVTX1dTID0gJ0dldEFsbFByaW9yaXRpZXMnO1xyXG4gICAgR0VUX1BSSU9SSVRZX0JZX0NBVEVHT1JZX0xFVkVMX05BTUVfV1MgPSAnR2V0QWxsUHJpb3JpdHlGb3JDYXRlZ29yeUxldmVsTmFtZSc7XHJcbiAgICBHRVRfVEVBTV9ST0xFU19GT1JfVEVBTV9XUyA9ICdHZXRUZWFtUm9sZXNGb3JUZWFtJztcclxuICAgIFJFQURfVVNFUl9FTlRJVFlTID0gJ1JlYWRVc2VyJztcclxuXHJcbiAgICBzZXRBcHBsaWNhdGlvbkRldGFpbHMoYXBwRGV0YWlscyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMgPSBhcHBEZXRhaWxzO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0T3RtbUFzc2V0VGVtcGxhdGVEZXRhaWxzKClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0TWVkaWFNYW5hZ2VyQ29uZmlnKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VXNlckROKHVzZXJEZXRhaWxzKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2codXNlckRldGFpbHMpO1xyXG4gICAgICAgIGNvbnN0IHVzZXJEZXRhaWwgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHVzZXJEZXRhaWxzLCAnVXNlcicpO1xyXG4gICAgICAgIGlmICh1c2VyRGV0YWlsICYmIEFycmF5LmlzQXJyYXkodXNlckRldGFpbCkgJiYgdXNlckRldGFpbCAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19VU0VSX0ROLCBKU09OLnN0cmluZ2lmeSh1c2VyRGV0YWlsWzBdLlVzZXJETikpO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnT1REU19VU0VSX0ROJywgdXNlckRldGFpbFswXS5Vc2VyRE4pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTm8gdXNlciBkZXRhaWxzIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNldFVzZXJEZXRhaWxzKHVzZXJJbmZvKSB7XHJcbiAgICAgICAgdGhpcy51c2VyRGV0YWlscyA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AodXNlckluZm8sICdVc2VyJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXBwbGljYXRpb25EZXRhaWxzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAmJiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAhPSBudWxsICYmIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIGFwcGxpY2F0aW9uIGRldGFpbHMgYXZhaWxhYmxlLicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VyRGV0YWlscygpIHtcclxuICAgICAgICBpZiAodGhpcy51c2VyRGV0YWlscyAmJiBBcnJheS5pc0FycmF5KHRoaXMudXNlckRldGFpbHMpICYmIHRoaXMudXNlckRldGFpbHMgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy51c2VyRGV0YWlscztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIHVzZXIgZGV0YWlscyBhdmFpbGFibGUuJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEFwcGxpY2F0aW9uSUQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXBwbGljYXRpb25EZXRhaWxzICYmIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0lEICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0lEICE9IG51bGwgJiZcclxuICAgICAgICAgICAgdHlwZW9mIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0lEICE9PSAndW5kZWZpbmVkJyAmJlxyXG4gICAgICAgICAgICB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9JRCAhPT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0lEO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRDb3JkeXNQbGF0Zm9ybVZlcnNpb24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXBwbGljYXRpb25EZXRhaWxzICYmIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLlBMQVRGT1JNX1ZFUlNJT04gJiZcclxuICAgICAgICAgICAgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuUExBVEZPUk1fVkVSU0lPTiAhPSBudWxsICYmIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLlBMQVRGT1JNX1ZFUlNJT04gIT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5QTEFURk9STV9WRVJTSU9OO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXBwbGljYXRpb25OYW1lKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAmJiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9OQU1FICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX05BTUUgIT09IG51bGwgJiZcclxuICAgICAgICAgICAgdHlwZW9mIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX05BTUUgIT09ICd1bmRlZmluZWQnICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX05BTUUgIT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9OQU1FO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0cmFuc2xhdGVSb2xlRE5TdWZmaXgocm9sZUROKSB7XHJcbiAgICAgICAgbGV0IHRyYW5zbGF0ZWRSb2xlRE4gPSAnJztcclxuICAgICAgICBpZiAodGhpcy5hcHBsaWNhdGlvbkRldGFpbHMgJiYgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fUEFDS0FHRV9UWVBFICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX1BBQ0tBR0VfVFlQRSAhPSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX1BBQ0tBR0VfVFlQRSAhPT0gJycpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX1BBQ0tBR0VfVFlQRSA9PT0gJ1BVQkxJU0hFRCcpIHtcclxuICAgICAgICAgICAgICAgIHRyYW5zbGF0ZWRSb2xlRE4gPSB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRS5zcGxpdCgnLCcpWzFdICsgJywnICtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLlBVQkxJU0hFRF9ST0xFX0ROX0NPTlNUQU5UICsgJywnICsgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50T3JnRE4oKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cmFuc2xhdGVkUm9sZUROO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJvbGVETjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiByb2xlRE47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFJvbGVEblN1ZmZpeEZvclVzZXJTZXJ2aWNlcygpIHtcclxuICAgICAgICBsZXQgcmV0dXJuVmFsO1xyXG4gICAgICAgIGlmICh0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAmJiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRSAmJlxyXG4gICAgICAgICAgICB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRSAhPSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0FDQ0VTU19ST0xFICE9PSAnJyAmJlxyXG4gICAgICAgICAgICB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRSkge1xyXG5cclxuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgICAgICAgcmV0dXJuVmFsID0gdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fQUNDRVNTX1JPTEUuc3Vic3RyKHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0FDQ0VTU19ST0xFLmluZGV4T2YoJywnKSArIDEsIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0FDQ0VTU19ST0xFLmxlbmd0aCAtIDEpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHJldHVyblZhbCAmJiByZXR1cm5WYWwgIT0gbnVsbCAmJiByZXR1cm5WYWwgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGVSb2xlRE5TdWZmaXgocmV0dXJuVmFsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIGluZm9ybWF0aW9uIHRvIHNlbmQgcm9sZSBkbiBzdWZmaXggYXZhaWxhYmxlLicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRSb2xlRG5TdWZmaXgoKSB7XHJcbiAgICAgICAgbGV0IHJldHVyblZhbDtcclxuICAgICAgICBpZiAodGhpcy5hcHBsaWNhdGlvbkRldGFpbHMgJiYgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fQUNDRVNTX1JPTEUgJiZcclxuICAgICAgICAgICAgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fQUNDRVNTX1JPTEUgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRSAhPT0gJycgJiZcclxuICAgICAgICAgICAgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fQUNDRVNTX1JPTEUpIHtcclxuXHJcbiAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgIHJldHVyblZhbCA9IHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0FDQ0VTU19ST0xFLnN1YnN0cih0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRS5pbmRleE9mKCcsJykgKyAxLCB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9BQ0NFU1NfUk9MRS5sZW5ndGggLSAxKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChyZXR1cm5WYWwgJiYgcmV0dXJuVmFsICE9IG51bGwgJiYgcmV0dXJuVmFsICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJldHVyblZhbDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignTm8gaW5mb3JtYXRpb24gdG8gc2VuZCByb2xlIGRuIHN1ZmZpeCBhdmFpbGFibGUuJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldE90bW1Bc3NldFRlbXBsYXRlRGV0YWlscygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAmJiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscyAmJiB0aGlzLmFwcGxpY2F0aW9uRGV0YWlscy5BUFBMSUNBVElPTl9JRCAhPT0gJydcclxuICAgICAgICAgICAgICAgICYmIHRoaXMuYXBwbGljYXRpb25EZXRhaWxzLkFQUExJQ0FUSU9OX0lEICE9IG51bGwgJiYgdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fSUQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXBwbGljYXRpb25JRDogdGhpcy5hcHBsaWNhdGlvbkRldGFpbHMuQVBQTElDQVRJT05fSUQsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZyhwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG1lZGlhTWFuYWdlckNvbmZpZzogYW55ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9NZWRpYV9NYW5hZ2VyX0NvbmZpZycpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1lZGlhTWFuYWdlckNvbmZpZy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZWRpYU1hbmFnZXJDb25maWcgPSBtZWRpYU1hbmFnZXJDb25maWdbMF07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0YTogTWVkaWFNYW5hZ2VyQ29uZmlnID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogbWVkaWFNYW5hZ2VyQ29uZmlnLk1FRElBX01BTkFHRVJfVVJMLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrVHlwZTogbWVkaWFNYW5hZ2VyQ29uZmlnLkNBTExCQUNLX1RZUEUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblRpbWVPdXQ6IG1lZGlhTWFuYWdlckNvbmZpZy5TRVNTSU9OX1RJTUVPVVQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBpVmVyc2lvbjogbWVkaWFNYW5hZ2VyQ29uZmlnLkFQSV9WRVJTSU9OLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVuYWJsZVFEUzogbWVkaWFNYW5hZ2VyQ29uZmlnLkVOQUJMRV9RRFMgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxZHNTZXJ2ZXJVUkw6IG1lZGlhTWFuYWdlckNvbmZpZy5RRFNfU0VSVkVSX1VSTCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdGRzUmVzb3VyY2VOYW1lOiBtZWRpYU1hbmFnZXJDb25maWcuT1REU19SRVNPVVJDRV9OQU1FXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ05vIE1lZGlhIE1hbmFnZXIgY29uZmlnIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgTWVkaWEgTWFuYWdlciBjb25maWd1cmF0aW9uLicpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ0FwcGxpY2F0aW9uIElkIGlzIGludmFsaWQuJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRNYXN0ZXJSb2xlcyhtYXN0ZXJSb2xlKSB7XHJcbiAgICAgICAgaWYgKG1hc3RlclJvbGUgJiYgbWFzdGVyUm9sZSkge1xyXG4gICAgICAgICAgICB0aGlzLm1hc3RlclJvbGVzID0gbWFzdGVyUm9sZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaGFzUm9sZShyb2xlRE4pIHtcclxuICAgICAgICBsZXQgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmIChyb2xlRE4gIT0gbnVsbCAmJiByb2xlRE4gIT09ICcnICYmIHJvbGVETikge1xyXG4gICAgICAgICAgICBjb25zdCBvcmcgPSBhY3JvbnVpLmZpbmRPYmplY3RzKHRoaXMubWFzdGVyUm9sZXMsICdkbicsIHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudE9yZ0ROKCkpO1xyXG4gICAgICAgICAgICBpZiAob3JnICYmIG9yZy5sZW5ndGggJiYgb3JnLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvbGUgPSBhY3JvbnVpLmZpbmRPYmplY3RzKG9yZywgJ0BpZCcsIHJvbGVETik7XHJcbiAgICAgICAgICAgICAgICBpZiAocm9sZSAmJiByb2xlICE9IG51bGwgJiYgQXJyYXkuaXNBcnJheShyb2xlKSAmJiByb2xlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBoYXNSb2xlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaGFzUm9sZSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBoYXNSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEFsbFByaW9yaXRpZXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlBSSU9SSVRZX05TLCB0aGlzLkdFVF9BTExfUFJJT1JJVElFU19XUywgbnVsbClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9Qcmlvcml0eScpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFsbFByaW9yaXRpZXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUFJJT1JJVElFUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9QUklPUklUSUVTKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuUFJJT1JJVFlfTlMsIHRoaXMuR0VUX0FMTF9QUklPUklUSUVTX1dTLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHByaW9yaXR5ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9Qcmlvcml0eScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1BSSU9SSVRJRVMsIEpTT04uc3RyaW5naWZ5KHByaW9yaXR5KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChwcmlvcml0eSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8qIGdldE1lbnVGb3JVc2VyKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5NRU5VX05TLCB0aGlzLkdFVF9NRU5VX0ZPUl9VU0VSX1dTLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX01lbnUnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRNZW51Rm9yVXNlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk1FTlVfRk9SX1VTRVIpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5NRU5VX0ZPUl9VU0VSKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuTUVOVV9OUywgdGhpcy5HRVRfTUVOVV9GT1JfVVNFUl9XUywgbnVsbCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBtZW51ID0gYWNyb251aS5maW5kT2JqZWN0c0J5UHJvcChyZXNwb25zZSwgJ01QTV9NZW51Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5NRU5VX0ZPUl9VU0VSLCBKU09OLnN0cmluZ2lmeShtZW51KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChtZW51KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgZ2V0QWxsTWV0YWRhdGFGaWVsZHNCeVR5cGUodHlwZTogTVBNX0xFVkVMKTogT2JzZXJ2YWJsZTxBcnJheTxhbnk+PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgYXBwbGljYXRpb25JRDogdGhpcy5nZXRBcHBsaWNhdGlvbklEKCksXHJcbiAgICAgICAgICAgIHR5cGU6IHR5cGVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX01FVEFEQVRBX0ZJRUxEU19OUywgdGhpcy5HRVRfQUxMX01FVEFEQVRBX0ZJRUxEU19CWV9UWVBFX1dTLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX01ldGFkYXRhX0ZpZWxkcycpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbERpc3BsYXlGaWVsZHNGb3JWaWV3KHZpZXdUeXBlOiBzdHJpbmcsIGNhdGVnb3J5SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8QXJyYXk8YW55Pj4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIHZpZXdUeXBlOiB2aWV3VHlwZSxcclxuICAgICAgICAgICAgY2F0ZWdvcnlJRDogY2F0ZWdvcnlJZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QoJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9UYXNrX1ZpZXdfQ29uZmlnL29wZXJhdGlvbnMnLCAnR2V0VGFza1ZpZXdDb25maWcnLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1Rhc2tfVmlld19Db25maWcnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxDdXN0b21NZXRhZGF0YUZpZWxkc0J5Q2F0ZWdvcnlMZXZlbChjYXRlZ29yeUxldmVsSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbElEOiBjYXRlZ29yeUxldmVsSWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0NBVEVHT1JZX01FVEFEQVRBX0ZJRUxEU19OUywgdGhpcy5HRVRfQUxMX0NVU1RPTV9NRVRBREFUQV9GSUVMRFNfQllfQ0FURUdPUllfTEVWRUxfV1MsIHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fQ2F0ZWdvcnlfTWV0YWRhdGFfRmllbGRzJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U3RhdHVzQnlJZChzdGF0dXNJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxTdGF0dXM+IHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICAnTVBNX1N0YXR1cy1pZCc6IHtcclxuICAgICAgICAgICAgICAgIElkOiBzdGF0dXNJZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLlNUQVRVU19NRVRIT0RfTlMsIHRoaXMuR0VUX1NUQVRVU19CWV9JRF9XUywgZ2V0UmVxdWVzdE9iamVjdClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYWNyb251aS5maW5kT2JqZWN0QnlQcm9wKHJlc3BvbnNlLCAnTVBNX1N0YXR1cycpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFN0YXR1cyhjYXRlZ29yeUxldmVsTmFtZTogTVBNX0xFVkVMLCBzdGF0dXNUeXBlTmFtZTogU3RhdHVzVHlwZSk6IE9ic2VydmFibGU8QXJyYXk8U3RhdHVzPj4ge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxOYW1lOiBjYXRlZ29yeUxldmVsTmFtZSxcclxuICAgICAgICAgICAgc3RhdHVzVHlwZTogc3RhdHVzVHlwZU5hbWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU1RBVFVTX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX1NUQVRVU19CWV9DQVRFR09SWV9BTkRfU1RBVFVTX1dTLCBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzZXM6IEFycmF5PFN0YXR1cz4gPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJlc3BvbnNlLCAnTVBNX1N0YXR1cycpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U3RhdHVzQnlDYXRlZ29yeUxldmVsKGNhdGVnb3J5TGV2ZWxOYW1lOiBNUE1fTEVWRUwpOiBPYnNlcnZhYmxlPEFycmF5PFN0YXR1cz4+IHtcclxuICAgICAgICBjb25zdCBnZXRSZXF1ZXN0T2JqZWN0ID0ge1xyXG4gICAgICAgICAgICBjYXRlZ29yeUxldmVsTmFtZTogY2F0ZWdvcnlMZXZlbE5hbWUsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxJRDogJydcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuU1RBVFVTX01FVEhPRF9OUywgdGhpcy5HRVRfU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUVfV1MsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXNlczogQXJyYXk8U3RhdHVzPiA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fU3RhdHVzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChzdGF0dXNlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgZ2V0UHJpb3JpdGllcyhjYXRlZ29yeUxldmVsTmFtZTogTVBNX0xFVkVMKTogT2JzZXJ2YWJsZTxBcnJheTxQcmlvcml0eT4+IHtcclxuICAgICAgICAgY29uc3QgZ2V0UmVxdWVzdE9iamVjdCA9IHtcclxuICAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxOYW1lOiBjYXRlZ29yeUxldmVsTmFtZVxyXG4gICAgICAgICB9O1xyXG4gICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5QUklPUklUWV9NRVRIT0RfTlMsIHRoaXMuR0VUX1BSSU9SSVRZX0JZX0NBVEVHT1JZX0xFVkVMX05BTUVfV1MsIGdldFJlcXVlc3RPYmplY3QpXHJcbiAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByaW9yaXRpZXM6IEFycmF5PFByaW9yaXR5PiA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fUHJpb3JpdHknKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChwcmlvcml0aWVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgfSk7XHJcbiAgICAgfSAqL1xyXG5cclxuICAgIC8vIGFkZGVkIGZvciBjb2RlIGhhcmRlbmlpbmcgXHJcbiAgICBnZXRQcmlvcml0aWVzKGNhdGVnb3J5TGV2ZWxOYW1lOiBNUE1fTEVWRUwpOiBBcnJheTxQcmlvcml0eT4ge1xyXG5cclxuICAgICAgICBjb25zdCBjdXJyQ2F0ZWdvcnlPYmo6IENhdGVnb3J5TGV2ZWwgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxzKCkuZmluZChkYXRhID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGRhdGEuQ0FURUdPUllfTEVWRUxfVFlQRSA9PT0gY2F0ZWdvcnlMZXZlbE5hbWU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGN1cnJDYXRlZ29yeU9iaikge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUFJJT1JJVElFUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIGxldCBhbGxTdGF0dXNlczogQXJyYXk8UHJpb3JpdHk+ID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9QUklPUklUSUVTKSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgYWxsU3RhdHVzOiBBcnJheTxQcmlvcml0eT4gPSBhbGxTdGF0dXNlcy5maWx0ZXIoKHN0YXR1czogUHJpb3JpdHkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc3RhdHVzLlJfUE9fQ0FURUdPUllfTEVWRUxbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkID09PSBjdXJyQ2F0ZWdvcnlPYmpbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYWxsU3RhdHVzO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBhbGxTdGF0dXM6IEFycmF5PFByaW9yaXR5PiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUHJpb3JpdGllcygpLmZpbHRlcigoc3RhdHVzOiBQcmlvcml0eSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdGF0dXMuUl9QT19DQVRFR09SWV9MRVZFTFsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQgPT09IGN1cnJDYXRlZ29yeU9ialsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhbGxTdGF0dXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRlYW1Sb2xlc0ZvclRlYW0ocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5URUFNX1JPTEVfTUFQUElOR19OUywgdGhpcy5HRVRfVEVBTV9ST0xFU19GT1JfVEVBTV9XUywgcGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVhZFVzZXIocGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcmVhZFVzZXJSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAnSWRlbnRpdHktaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAnQHhtbG5zJzogdGhpcy5FTlRJVFlfVVNFUl9JREVOVElUWV9OUyxcclxuICAgICAgICAgICAgICAgIElkOiBwYXJhbWV0ZXJzLklkLFxyXG4gICAgICAgICAgICAgICAgSXRlbUlkOiBwYXJhbWV0ZXJzLkl0ZW1JZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLkVOVElUWV9VU0VSX05TLCB0aGlzLlJFQURfVVNFUl9FTlRJVFlTLCByZWFkVXNlclJlcXVlc3QpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE90bW1Bc3NldFVwbG9hZERldGFpbHMoKSB7XHJcbiAgICAgICAgY29uc3Qgb3RtbUFzc2V0VGVtcGxhdGVPYmogPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpO1xyXG4gICAgICAgIGlmIChvdG1tQXNzZXRUZW1wbGF0ZU9iaiAmJiBvdG1tQXNzZXRUZW1wbGF0ZU9iaiAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvdG1tQXNzZXRUZW1wbGF0ZU9iajtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ05vIE9UTU0gQ29uZmlnIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==