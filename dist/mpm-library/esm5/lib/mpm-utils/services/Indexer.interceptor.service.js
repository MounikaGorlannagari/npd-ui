import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { OTDSTicketService } from './OTDSTicket.service';
import * as i0 from "@angular/core";
import * as i1 from "./OTDSTicket.service";
var IndexerInterceptor = /** @class */ (function () {
    function IndexerInterceptor(otdsTicketService) {
        this.otdsTicketService = otdsTicketService;
    }
    IndexerInterceptor.prototype.intercept = function (req, next) {
        var ticket = this.otdsTicketService.getOTDSTicketForUser();
        var newHeaders = req.headers;
        if (ticket) {
            newHeaders = newHeaders.append('OTDSTicket', ticket);
        }
        // We have to clone our request with our new headers
        // This is required because HttpRequests are immutable
        var authReq = req.clone({ headers: newHeaders });
        return next.handle(authReq);
    };
    IndexerInterceptor.ctorParameters = function () { return [
        { type: OTDSTicketService }
    ]; };
    IndexerInterceptor.ɵprov = i0.ɵɵdefineInjectable({ factory: function IndexerInterceptor_Factory() { return new IndexerInterceptor(i0.ɵɵinject(i1.OTDSTicketService)); }, token: IndexerInterceptor, providedIn: "root" });
    IndexerInterceptor = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], IndexerInterceptor);
    return IndexerInterceptor;
}());
export { IndexerInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5kZXhlci5pbnRlcmNlcHRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL0luZGV4ZXIuaW50ZXJjZXB0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBTXpEO0lBRUksNEJBQ1csaUJBQW9DO1FBQXBDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7SUFDM0MsQ0FBQztJQUVMLHNDQUFTLEdBQVQsVUFBVSxHQUFxQixFQUFFLElBQWlCO1FBQzlDLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzdELElBQUksVUFBVSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDN0IsSUFBSSxNQUFNLEVBQUU7WUFDUixVQUFVLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDeEQ7UUFDRCxvREFBb0Q7UUFDcEQsc0RBQXNEO1FBQ3RELElBQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUNuRCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7Z0JBYjZCLGlCQUFpQjs7O0lBSHRDLGtCQUFrQjtRQUo5QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsa0JBQWtCLENBa0I5Qjs2QkEzQkQ7Q0EyQkMsQUFsQkQsSUFrQkM7U0FsQlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXZlbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9URFNUaWNrZXRTZXJ2aWNlIH0gZnJvbSAnLi9PVERTVGlja2V0LnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgSW5kZXhlckludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgb3Rkc1RpY2tldFNlcnZpY2U6IE9URFNUaWNrZXRTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIGludGVyY2VwdChyZXE6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgICAgIGNvbnN0IHRpY2tldCA9IHRoaXMub3Rkc1RpY2tldFNlcnZpY2UuZ2V0T1REU1RpY2tldEZvclVzZXIoKTtcclxuICAgICAgICBsZXQgbmV3SGVhZGVycyA9IHJlcS5oZWFkZXJzO1xyXG4gICAgICAgIGlmICh0aWNrZXQpIHtcclxuICAgICAgICAgICAgbmV3SGVhZGVycyA9IG5ld0hlYWRlcnMuYXBwZW5kKCdPVERTVGlja2V0JywgdGlja2V0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gV2UgaGF2ZSB0byBjbG9uZSBvdXIgcmVxdWVzdCB3aXRoIG91ciBuZXcgaGVhZGVyc1xyXG4gICAgICAgIC8vIFRoaXMgaXMgcmVxdWlyZWQgYmVjYXVzZSBIdHRwUmVxdWVzdHMgYXJlIGltbXV0YWJsZVxyXG4gICAgICAgIGNvbnN0IGF1dGhSZXEgPSByZXEuY2xvbmUoeyBoZWFkZXJzOiBuZXdIZWFkZXJzIH0pO1xyXG4gICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShhdXRoUmVxKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19