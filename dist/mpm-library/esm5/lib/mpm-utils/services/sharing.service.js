import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
var SharingService = /** @class */ (function () {
    function SharingService() {
        this.recentSearches = [];
        this.lookupDomains = [];
        this.taskEvents = [];
        this.taskWorkflowActions = [];
        this.allApplicationFields = [];
        this.allApplicationViewConfig = [];
        this.persons = [];
        this.categoryMetadata = [];
        this.currentUserPreference = [];
        this.userPreferences = [];
        this.subscribableBrandConfig = new BehaviorSubject('');
        this.resetView = new BehaviorSubject('');
        this.notificationPass = new BehaviorSubject('');
        this.isMenuChanged = new BehaviorSubject(false);
    }
    SharingService.prototype.setMenuChanged = function (menuchanged) {
        this.isMenuChanged.next(menuchanged);
    };
    SharingService.prototype.getMenuChanged = function () {
        return this.isMenuChanged.asObservable();
    };
    SharingService.prototype.unsubscribeMenuChangeObserable = function () {
        return this.isMenuChanged.unsubscribe();
    };
    SharingService.prototype.getNotificationDetails = function (data) {
        this.notificationPass.next(data);
    };
    SharingService.prototype.getCategoryMetadata = function () {
        return this.categoryMetadata;
    };
    SharingService.prototype.setCategoryMetadata = function (categoryMetadata) {
        this.categoryMetadata = categoryMetadata;
    };
    SharingService.prototype.getdisplayName = function () {
        return this.displayName;
    };
    SharingService.prototype.getcurrentUserItemID = function () {
        return this.currentUserItemID;
    };
    SharingService.prototype.setCurrentUserObject = function (currentUserObject) {
        this.currentUserObject = currentUserObject;
    };
    SharingService.prototype.getCurrentUserObject = function () {
        return this.currentUserObject;
    };
    SharingService.prototype.setCurrentUserID = function (currentUserId) {
        this.currentUserID = currentUserId;
    };
    SharingService.prototype.getCurrentUserID = function () {
        return this.currentUserID;
    };
    SharingService.prototype.getCurrentUserItemID = function () {
        return this.currentUserItemID;
    };
    SharingService.prototype.setCurrentUserItemID = function (currentUserItemId) {
        this.currentUserItemID = currentUserItemId;
    };
    SharingService.prototype.setCurrentUserDN = function (currentUserDN) {
        this.currentUserDN = currentUserDN;
    };
    SharingService.prototype.getCurrentUserDN = function () {
        return this.currentUserDN;
    };
    SharingService.prototype.setCurrentUserDisplayName = function (displayName) {
        this.displayName = displayName;
    };
    SharingService.prototype.getCurrentOrgDN = function () {
        return this.currentOrgDN;
    };
    SharingService.prototype.setCurrentOrgDN = function (orgDN) {
        this.currentOrgDN = orgDN;
        if (this.currentOrgDN && typeof this.currentOrgDN === 'string') {
            var instanceIdentifier = this.currentOrgDN.split(',');
            this.instanceIdentifier = instanceIdentifier.slice(1, instanceIdentifier.length).join(',');
        }
    };
    SharingService.prototype.setCurrentUserEmailId = function (emailId) {
        this.currentUserEmailId = emailId;
    };
    SharingService.prototype.getCurrentUserEmailId = function () {
        return this.currentUserEmailId;
    };
    SharingService.prototype.getInstanceIdentifier = function () {
        return this.instanceIdentifier;
    };
    SharingService.prototype.getCurrentUserDisplayName = function () {
        return this.displayName;
    };
    SharingService.prototype.getCurrentUserCN = function () {
        if (this.currentUserDN) {
            return this.currentUserDN.split(',')[0].split('=')[1];
        }
        else {
            return '';
        }
    };
    SharingService.prototype.setRecentSearch = function (recentSearch) {
        this.recentSearches.unshift(recentSearch);
    };
    SharingService.prototype.getRecentSearches = function () {
        return this.recentSearches;
    };
    SharingService.prototype.getRecentSearchesByView = function (view) {
        return this.recentSearches.filter(function (search) { return search[0].view === view || typeof search === 'string'; });
    };
    SharingService.prototype.setMenu = function (menuList) {
        this.menu = menuList;
    };
    SharingService.prototype.getAllMenu = function () {
        return this.menu;
    };
    SharingService.prototype.getMenuByType = function (type) {
        return this.menu.filter(function (tmpMenu) { return tmpMenu.TYPE === type; });
    };
    SharingService.prototype.setTaskEvents = function (events) {
        if (events && events.length > 0) {
            this.taskEvents = events;
        }
        else if (events) {
            this.taskEvents = [events];
        }
        else {
            this.taskEvents = [];
        }
    };
    SharingService.prototype.getTaskEvents = function () {
        return this.taskEvents;
    };
    SharingService.prototype.setTaskWorkflowActions = function (workflowActions) {
        if (workflowActions && workflowActions.length > 0) {
            this.taskWorkflowActions = workflowActions;
        }
        else if (workflowActions) {
            this.taskWorkflowActions = [workflowActions];
        }
        else {
            this.taskWorkflowActions = [];
        }
    };
    SharingService.prototype.getTaskWorkflowActions = function () {
        return this.taskWorkflowActions;
    };
    SharingService.prototype.setAllCategories = function (categories) {
        this.allCategories = categories;
    };
    SharingService.prototype.getAllCategories = function () {
        return this.allCategories;
    };
    /* setDefaultCategoryDetails(category) {
        this.defaultCategoryDetails = category;
    }

    setCategoryDetails(category) {
        this.categoryDetails.push(category);
    }

    getCategoryDetailsById(categoryId) {
        return this.categoryDetails.find(category => category.id === categoryId);
    }

    getDefaultCategoryDetails() {
        return this.defaultCategoryDetails;
    } */
    SharingService.prototype.setSelectedCategory = function (selectedCategory) {
        this.selectedCategory = selectedCategory;
    };
    SharingService.prototype.getSelectedCategory = function () {
        return this.selectedCategory;
    };
    SharingService.prototype.setAllLookupDomains = function (lookupDomains) {
        this.lookupDomains = lookupDomains;
    };
    SharingService.prototype.getAllLookupDomains = function () {
        return this.lookupDomains;
    };
    SharingService.prototype.setAllRoles = function (roles) {
        this.roles = roles;
    };
    SharingService.prototype.getAllRoles = function () {
        return this.roles;
    };
    SharingService.prototype.setUserRoles = function (roles) {
        this.userRoles = roles;
    };
    SharingService.prototype.getUserRoles = function () {
        return this.userRoles;
    };
    SharingService.prototype.getRoleByName = function (roleName) {
        return this.roles.find(function (role) { return role.ROLE_NAME === roleName; });
    };
    SharingService.prototype.setMediaManagerConfig = function (mediaManagerConfig) {
        this.mediaManagerConfig = mediaManagerConfig;
    };
    SharingService.prototype.getMediaManagerConfig = function () {
        return this.mediaManagerConfig;
    };
    SharingService.prototype.setCategoryLevels = function (categoryLevels) {
        this.categoryLevels = categoryLevels;
    };
    SharingService.prototype.getCategoryLevels = function () {
        return this.categoryLevels ? this.categoryLevels : [];
    };
    SharingService.prototype.getCategoryLevelsById = function () {
        var arr_categoryLevel = [];
        this.categoryLevels.forEach(function (categoryLevel) {
            arr_categoryLevel.push({
                id: categoryLevel['MPM_Category_Level-id'].Id,
                categoryLevelType: categoryLevel.CATEGORY_LEVEL_TYPE
            });
        });
        return arr_categoryLevel;
    };
    SharingService.prototype.setAllPriorities = function (allPriorities) {
        this.allPriorities = allPriorities;
    };
    SharingService.prototype.getAllPriorities = function () {
        return this.allPriorities;
    };
    SharingService.prototype.setCRActions = function (actions) {
        this.crActions = actions ? JSON.parse(JSON.stringify(actions)) : [];
    };
    SharingService.prototype.getCRActions = function () {
        return this.crActions ? this.crActions : [];
    };
    SharingService.prototype.setAllStatusConfig = function (statusList) {
        this.allStatusConfig = statusList;
    };
    SharingService.prototype.getAllStatusConfig = function () {
        return this.allStatusConfig;
    };
    SharingService.prototype.setAllApplicationFields = function (otmmFields) {
        this.allApplicationFields = otmmFields ? otmmFields : [];
    };
    SharingService.prototype.getAllApplicationFields = function () {
        return (this.allApplicationFields && this.allApplicationFields.length > 0) ? this.allApplicationFields : null;
    };
    SharingService.prototype.setAllApplicationViewConfig = function (viewConfigs) {
        this.allApplicationViewConfig = viewConfigs ? viewConfigs : [];
    };
    SharingService.prototype.getAllApplicationViewConfig = function () {
        return (this.allApplicationViewConfig && this.allApplicationViewConfig.length > 0) ? this.allApplicationViewConfig : null;
    };
    SharingService.prototype.setProjectConfig = function (data) {
        this.projectConfig = data;
    };
    SharingService.prototype.getProjectConfig = function () {
        return this.projectConfig;
    };
    SharingService.prototype.setBrandConfig = function (data) {
        this.brandConfig = data;
        this.subscribableBrandConfig.next(data);
    };
    SharingService.prototype.getBrandConfig = function () {
        return this.brandConfig;
    };
    SharingService.prototype.setAllViewConfig = function (data) {
        this.viewConfig = data;
    };
    SharingService.prototype.getAllViewConfig = function () {
        return this.viewConfig;
    };
    SharingService.prototype.getViewConfigById = function (id) {
        return this.viewConfig.find(function (view) { return view['MPM_View_Config-id'].Id === id; });
    };
    SharingService.prototype.getViewConfigByName = function (viewName) {
        var selectedViewConfig = this.viewConfig.find(function (view) { return view.VIEW === viewName; });
        return selectedViewConfig ? selectedViewConfig : null;
    };
    SharingService.prototype.getSubscribableBrandConfig = function () {
        return this.subscribableBrandConfig.asObservable();
    };
    SharingService.prototype.resetViewData = function () {
        return this.resetView.asObservable();
    };
    SharingService.prototype.onResetView = function (viewId) {
        this.resetView.next(viewId);
    };
    SharingService.prototype.setAppConfig = function (data) {
        this.appConfig = data;
    };
    SharingService.prototype.getAppConfig = function () {
        return this.appConfig;
    };
    SharingService.prototype.setCommentConfig = function (data) {
        this.commentConfig = data;
    };
    SharingService.prototype.getCommentConfig = function () {
        return this.commentConfig;
    };
    SharingService.prototype.setAssetConfig = function (data) {
        this.assetConfig = data;
    };
    SharingService.prototype.getAssetConfig = function () {
        return this.assetConfig;
    };
    SharingService.prototype.setAllTeamRoles = function (data) {
        console.log(data);
        this.teamRoles = data;
        console.log(this.teamRoles);
    };
    SharingService.prototype.getAllTeamRoles = function () {
        return this.teamRoles;
    };
    SharingService.prototype.setAllTeams = function (data) {
        if (Array.isArray(data)) {
            this.teams = data;
        }
        else {
            this.teams = [data];
        }
    };
    SharingService.prototype.getAllTeams = function () {
        return this.teams;
    };
    SharingService.prototype.setAllPersons = function (data) {
        this.persons = data;
    };
    SharingService.prototype.getAllPersons = function () {
        return this.persons;
    };
    SharingService.prototype.setCurrentPerson = function (data) {
        this.currentPerson = data;
    };
    SharingService.prototype.getCurrentPerson = function () {
        return this.currentPerson;
    };
    SharingService.prototype.getDefaultEmailConfig = function () {
        return this.emailConfig;
    };
    SharingService.prototype.setDefaultEmailConfig = function (data) {
        this.emailConfig = data;
        sessionStorage.setItem('EMAIL_CONFIG', JSON.stringify(this.emailConfig));
    };
    SharingService.prototype.updateCurrentUserPreference = function (userPreferenceData) {
        var _this = this;
        console.log(userPreferenceData);
        if (Array.isArray(userPreferenceData)) {
            userPreferenceData.forEach(function (userPreference) {
                var selectedUserPreferenceIndex = _this.currentUserPreference.findIndex(function (selectedUserPreference) {
                    return selectedUserPreference['MPM_User_Preference-id'].Id === userPreference['MPM_User_Preference-id'].Id;
                });
                if (selectedUserPreferenceIndex >= 0) {
                    _this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
                }
                _this.currentUserPreference.push(userPreference);
            });
        }
        else {
            var selectedUserPreferenceIndex = this.currentUserPreference.findIndex(function (userPreference) {
                return userPreference['MPM_User_Preference-id'].Id === userPreferenceData['MPM_User_Preference-id'].Id;
            });
            if (selectedUserPreferenceIndex >= 0) {
                this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
            }
            this.currentUserPreference.push(userPreferenceData);
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    };
    SharingService.prototype.removeCurrentUserPreference = function (userPreferenceId) {
        var selectedUserPreferenceIndex = this.currentUserPreference.findIndex(function (userPreference) {
            return userPreference['MPM_User_Preference-id'].Id === userPreferenceId;
        });
        if (selectedUserPreferenceIndex >= 0) {
            this.currentUserPreference.splice(selectedUserPreferenceIndex, 1);
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    };
    SharingService.prototype.setCurrentUserPreference = function (data) {
        if (Array.isArray(data)) {
            this.currentUserPreference = data;
        }
        else {
            this.currentUserPreference = [data];
        }
        sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(this.currentUserPreference));
    };
    SharingService.prototype.getCurrentUserPreference = function () {
        return this.currentUserPreference;
    };
    SharingService.prototype.getCurrentUserPreferenceByViewId = function (viewId) {
        console.log(this.currentUserPreference);
        if (this.userPreferences.length <= 0 && this.currentUserPreference.length > 0) {
            this.userPreferences.push(this.currentUserPreference);
        }
        if (this.currentUserPreference && this.currentUserPreference.length > 0) {
            return this.currentUserPreference.find(function (userPreference) { return userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId; });
        }
        else {
            return '';
        }
    };
    SharingService.prototype.getUserPreferencesByViewId = function () {
        return this.userPreferences;
    };
    SharingService.prototype.getDefaultCurrentUserPreferenceByMenu = function (menuId) {
        return this.currentUserPreference.find(function (userPreference) { return (userPreference.IS_DEFAULT_VIEW === 'true' || userPreference.IS_DEFAULT_VIEW === true)
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId; });
    };
    SharingService.prototype.getCurrentUserPreferenceByViewMenu = function (menuId, viewId) {
        return this.currentUserPreference.find(function (userPreference) { return userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId; });
    };
    SharingService.prototype.getDefaultCurrentUserPreferenceByListFilter = function (menuId, taskName) {
        return this.currentUserPreference.find(function (userPreference) { return (userPreference.IS_DEFAULT_VIEW === 'true' || userPreference.IS_DEFAULT_VIEW === true)
            && userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName; });
    };
    SharingService.prototype.getCurrentUserPreferenceByList = function (menuId, taskName) {
        return this.currentUserPreference.find(function (userPreference) { return (userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName); });
    };
    SharingService.prototype.getCurrentUserPreferenceByListView = function (menuId, taskName, viewId) {
        return this.currentUserPreference.find(function (userPreference) { return (userPreference.R_PO_MPM_MENU_ID['MPM_Menu-id'].Id === menuId
            && userPreference.FILTER_NAME === taskName
            && userPreference.R_PO_MPM_VIEW_CONFIG['MPM_View_Config-id'].Id === viewId); });
    };
    SharingService.prototype.setCampaignRole = function (data) {
        this.campaignRole = data;
    };
    SharingService.prototype.getCampaignRole = function () {
        return this.campaignRole;
    };
    SharingService.prototype.setReviewerRole = function (data) {
        this.reviewerRole = data;
    };
    SharingService.prototype.getReviewerRole = function () {
        return this.reviewerRole;
    };
    SharingService.prototype.setIsReviewer = function (data) {
        this.isReviewer = data;
    };
    SharingService.prototype.getIsReviewer = function () {
        return this.isReviewer;
    };
    SharingService.prototype.setApproverRole = function (data) {
        this.approverRole = data;
    };
    SharingService.prototype.getApproverRole = function () {
        return this.approverRole;
    };
    SharingService.prototype.setIsApprover = function (data) {
        this.isApprover = data;
    };
    SharingService.prototype.getIsApprover = function () {
        return this.isApprover;
    };
    SharingService.prototype.setManagerRole = function (data) {
        this.ManagerRole = data;
    };
    SharingService.prototype.getManagerRole = function () {
        return this.ManagerRole;
    };
    SharingService.prototype.setIsManager = function (data) {
        this.isManager = data;
    };
    SharingService.prototype.getIsManager = function () {
        return this.isManager;
    };
    /* setCampaignManagerRole(data): any {
        this.campaignManagerRole = data;
    }

    getCampaignManagerRole(): any {
        return this.campaignManagerRole;
    }

    setIsCampaignManager(data): any {
        this.isCampaignManager = data;
    }

    getIsCampaignManager(): any {
        return this.isCampaignManager;
    } */ // - MVSS-128
    SharingService.prototype.setMemberRole = function (data) {
        this.memberRole = data;
    };
    SharingService.prototype.getMemberRole = function () {
        return this.memberRole;
    };
    SharingService.prototype.setIsMember = function (data) {
        this.isMember = data;
    };
    SharingService.prototype.getIsMember = function () {
        return this.isMember;
    };
    SharingService.prototype.setIsUploaded = function (data) {
        this.isUploaded = data;
    };
    SharingService.prototype.getIsUploaded = function () {
        return this.isUploaded;
    };
    SharingService.prototype.getTeamRoleIdByRoleDN = function (roleDN) {
        this.getAllTeams().subscribe(function (allTeams) {
            console.log(allTeams);
        });
    };
    SharingService.prototype.setCurrentMenu = function (menu) {
        this.currentMenu = menu;
    };
    SharingService.prototype.getCurrentMenu = function () {
        return this.currentMenu;
    };
    SharingService.ɵprov = i0.ɵɵdefineInjectable({ factory: function SharingService_Factory() { return new SharingService(); }, token: SharingService, providedIn: "root" });
    SharingService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], SharingService);
    return SharingService;
}());
export { SharingService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL3NoYXJpbmcuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQVl0RCxPQUFPLEVBQWMsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOztBQU0zRjtJQTRESTtRQWxETyxtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUVwQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUluQixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQU96Qix5QkFBb0IsR0FBb0IsRUFBRSxDQUFDO1FBQzNDLDZCQUF3QixHQUFlLEVBQUUsQ0FBQztRQVUxQyxZQUFPLEdBQVEsRUFBRSxDQUFDO1FBWWxCLHFCQUFnQixHQUFlLEVBQUUsQ0FBQztRQUdsQywwQkFBcUIsR0FBZSxFQUFFLENBQUM7UUFPdkMsb0JBQWUsR0FBZSxFQUFFLENBQUM7UUFJcEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksZUFBZSxDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxlQUFlLENBQU0sRUFBRSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksZUFBZSxDQUFNLEVBQUUsQ0FBQyxDQUFBO1FBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFlLENBQVUsS0FBSyxDQUFDLENBQUM7SUFFN0QsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxXQUFXO1FBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFBO0lBQzVDLENBQUM7SUFFRCx1REFBOEIsR0FBOUI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUE7SUFDM0MsQ0FBQztJQUdELCtDQUFzQixHQUF0QixVQUF1QixJQUFJO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7SUFDcEMsQ0FBQztJQUVELDRDQUFtQixHQUFuQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRCw0Q0FBbUIsR0FBbkIsVUFBb0IsZ0JBQTRCO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztJQUM3QyxDQUFDO0lBRUQsdUNBQWMsR0FBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQsNkNBQW9CLEdBQXBCO1FBQ0ksT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDbEMsQ0FBQztJQUVELDZDQUFvQixHQUFwQixVQUFxQixpQkFBOEI7UUFDL0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGlCQUFpQixDQUFDO0lBQy9DLENBQUM7SUFFRCw2Q0FBb0IsR0FBcEI7UUFDSSxPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztJQUNsQyxDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLGFBQXFCO1FBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELDZDQUFvQixHQUFwQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ2xDLENBQUM7SUFFRCw2Q0FBb0IsR0FBcEIsVUFBcUIsaUJBQXlCO1FBQzFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztJQUMvQyxDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLGFBQXFCO1FBQ2xDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELGtEQUF5QixHQUF6QixVQUEwQixXQUFtQjtRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztJQUNuQyxDQUFDO0lBRUQsd0NBQWUsR0FBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixLQUFhO1FBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxPQUFPLElBQUksQ0FBQyxZQUFZLEtBQUssUUFBUSxFQUFFO1lBQzVELElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQzlGO0lBQ0wsQ0FBQztJQUVELDhDQUFxQixHQUFyQixVQUFzQixPQUFlO1FBQ2pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxPQUFPLENBQUM7SUFDdEMsQ0FBQztJQUVELDhDQUFxQixHQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ25DLENBQUM7SUFFRCw4Q0FBcUIsR0FBckI7UUFDSSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNuQyxDQUFDO0lBRUQsa0RBQXlCLEdBQXpCO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekQ7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixZQUFZO1FBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCwwQ0FBaUIsR0FBakI7UUFDSSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELGdEQUF1QixHQUF2QixVQUF3QixJQUFJO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksSUFBSSxPQUFPLE1BQU0sS0FBSyxRQUFRLEVBQXJELENBQXFELENBQUMsQ0FBQztJQUN2RyxDQUFDO0lBRUQsZ0NBQU8sR0FBUCxVQUFRLFFBQXdCO1FBQzVCLElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxtQ0FBVSxHQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxzQ0FBYSxHQUFiLFVBQWMsSUFBaUI7UUFDM0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFyQixDQUFxQixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxNQUFNO1FBQ2hCLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1NBQzVCO2FBQU0sSUFBSSxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDOUI7YUFBTTtZQUNILElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3hCO0lBQ0wsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELCtDQUFzQixHQUF0QixVQUF1QixlQUFlO1FBQ2xDLElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9DLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxlQUFlLENBQUM7U0FDOUM7YUFBTSxJQUFJLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNoRDthQUFNO1lBQ0gsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztTQUNqQztJQUNMLENBQUM7SUFFRCwrQ0FBc0IsR0FBdEI7UUFDSSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztJQUNwQyxDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLFVBQTJCO1FBQ3hDLElBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFDO0lBQ3BDLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7OztRQWNJO0lBRUosNENBQW1CLEdBQW5CLFVBQW9CLGdCQUEwQjtRQUMxQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUM7SUFDN0MsQ0FBQztJQUVELDRDQUFtQixHQUFuQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ2pDLENBQUM7SUFFRCw0Q0FBbUIsR0FBbkIsVUFBb0IsYUFBYTtRQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztJQUN2QyxDQUFDO0lBRUQsNENBQW1CLEdBQW5CO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksS0FBbUI7UUFDM0IsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVELHFDQUFZLEdBQVosVUFBYSxLQUFtQjtRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRUQscUNBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFjLFFBQWtCO1FBQzVCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsU0FBUyxLQUFLLFFBQVEsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCw4Q0FBcUIsR0FBckIsVUFBc0Isa0JBQXNDO1FBQ3hELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztJQUNqRCxDQUFDO0lBRUQsOENBQXFCLEdBQXJCO1FBQ0ksT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbkMsQ0FBQztJQUVELDBDQUFpQixHQUFqQixVQUFrQixjQUFvQztRQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsMENBQWlCLEdBQWpCO1FBQ0ksT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDMUQsQ0FBQztJQUVELDhDQUFxQixHQUFyQjtRQUNJLElBQUksaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFVBQUEsYUFBYTtZQUNyQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7Z0JBQ25CLEVBQUUsRUFBRSxhQUFhLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxFQUFFO2dCQUM3QyxpQkFBaUIsRUFBRSxhQUFhLENBQUMsbUJBQW1CO2FBQ3ZELENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxpQkFBaUIsQ0FBQztJQUM3QixDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLGFBQXlCO1FBQ3RDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELHFDQUFZLEdBQVosVUFBYSxPQUFtQjtRQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0lBRUQscUNBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ2hELENBQUM7SUFFRCwyQ0FBa0IsR0FBbEIsVUFBbUIsVUFBeUI7UUFDeEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7SUFDdEMsQ0FBQztJQUVELDJDQUFrQixHQUFsQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsZ0RBQXVCLEdBQXZCLFVBQXdCLFVBQTJCO1FBQy9DLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdELENBQUM7SUFFRCxnREFBdUIsR0FBdkI7UUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ2xILENBQUM7SUFFRCxvREFBMkIsR0FBM0IsVUFBNEIsV0FBOEI7UUFDdEQsSUFBSSxDQUFDLHdCQUF3QixHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDbkUsQ0FBQztJQUVELG9EQUEyQixHQUEzQjtRQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsd0JBQXdCLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDOUgsQ0FBQztJQUVELHlDQUFnQixHQUFoQixVQUFpQixJQUFJO1FBQ2pCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzlCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxJQUFJO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsdUNBQWMsR0FBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQseUNBQWdCLEdBQWhCLFVBQWlCLElBQUk7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELHlDQUFnQixHQUFoQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsMENBQWlCLEdBQWpCLFVBQWtCLEVBQUU7UUFDaEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQXBDLENBQW9DLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQsNENBQW1CLEdBQW5CLFVBQW9CLFFBQVE7UUFDeEIsSUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUF0QixDQUFzQixDQUFDLENBQUM7UUFDaEYsT0FBTyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxRCxDQUFDO0lBRUQsbURBQTBCLEdBQTFCO1FBQ0ksT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdkQsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxNQUFNO1FBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVELHFDQUFZLEdBQVosVUFBYSxJQUFJO1FBQ2IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQztJQUVELHFDQUFZLEdBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDMUIsQ0FBQztJQUVELHlDQUFnQixHQUFoQixVQUFpQixJQUFJO1FBQ2pCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzlCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELHVDQUFjLEdBQWQsVUFBZSxJQUFJO1FBQ2YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQztJQUVELHVDQUFjLEdBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVELHdDQUFlLEdBQWYsVUFBZ0IsSUFBSTtRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFRCx3Q0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksSUFBSTtRQUNaLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxJQUFJO1FBQ2QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDeEIsQ0FBQztJQUVELHlDQUFnQixHQUFoQixVQUFpQixJQUFJO1FBQ2pCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO0lBQzlCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELDhDQUFxQixHQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQsOENBQXFCLEdBQXJCLFVBQXNCLElBQUk7UUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsY0FBYyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsb0RBQTJCLEdBQTNCLFVBQTRCLGtCQUFrQjtRQUE5QyxpQkFvQkM7UUFuQkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBQy9CLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1lBQ25DLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGNBQWM7Z0JBQ3JDLElBQU0sMkJBQTJCLEdBQUcsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxVQUFBLHNCQUFzQjtvQkFDM0YsT0FBQSxzQkFBc0IsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxjQUFjLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFO2dCQUFuRyxDQUFtRyxDQUFDLENBQUM7Z0JBQ3pHLElBQUksMkJBQTJCLElBQUksQ0FBQyxFQUFFO29CQUNsQyxLQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLDJCQUEyQixFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUNyRTtnQkFDRCxLQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTTtZQUNILElBQU0sMkJBQTJCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsQ0FBQyxVQUFBLGNBQWM7Z0JBQ25GLE9BQUEsY0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRSxLQUFLLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRTtZQUEvRixDQUErRixDQUFDLENBQUM7WUFDckcsSUFBSSwyQkFBMkIsSUFBSSxDQUFDLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsMkJBQTJCLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDckU7WUFDRCxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDdkQ7UUFDRCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM1SCxDQUFDO0lBRUQsb0RBQTJCLEdBQTNCLFVBQTRCLGdCQUFnQjtRQUN4QyxJQUFNLDJCQUEyQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsVUFBQSxjQUFjO1lBQ25GLE9BQUEsY0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRSxLQUFLLGdCQUFnQjtRQUFoRSxDQUFnRSxDQUFDLENBQUM7UUFDdEUsSUFBSSwyQkFBMkIsSUFBSSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQywyQkFBMkIsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNyRTtRQUNELGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDO0lBQzVILENBQUM7SUFFRCxpREFBd0IsR0FBeEIsVUFBeUIsSUFBSTtRQUN6QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztTQUNyQzthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7UUFDRCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQztJQUM1SCxDQUFDO0lBRUQsaURBQXdCLEdBQXhCO1FBQ0ksT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUM7SUFDdEMsQ0FBQztJQUVELHlEQUFnQyxHQUFoQyxVQUFpQyxNQUFNO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDeEMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7U0FDekQ7UUFDRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyRSxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxjQUFjLElBQUksT0FBQSxjQUFjLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxFQUF2RSxDQUF1RSxDQUFDLENBQUM7U0FDckk7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDO0lBRUQsbURBQTBCLEdBQTFCO1FBQ0ksT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7SUFFRCw4REFBcUMsR0FBckMsVUFBc0MsTUFBTTtRQUN4QyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxjQUFjLElBQUksT0FBQSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEtBQUssTUFBTSxJQUFJLGNBQWMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDO2VBQ3hJLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTSxFQURWLENBQ1UsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCwyREFBa0MsR0FBbEMsVUFBbUMsTUFBTSxFQUFFLE1BQU07UUFDN0MsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFVBQUEsY0FBYyxJQUFJLE9BQUEsY0FBYyxDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxLQUFLLE1BQU07ZUFDekgsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLEVBRFYsQ0FDVSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELG9FQUEyQyxHQUEzQyxVQUE0QyxNQUFNLEVBQUUsUUFBUTtRQUN4RCxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsVUFBQSxjQUFjLElBQUksT0FBQSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEtBQUssTUFBTSxJQUFJLGNBQWMsQ0FBQyxlQUFlLEtBQUssSUFBSSxDQUFDO2VBQ3hJLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLEtBQUssTUFBTTtlQUM1RCxjQUFjLENBQUMsV0FBVyxLQUFLLFFBQVEsRUFGVyxDQUVYLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsdURBQThCLEdBQTlCLFVBQStCLE1BQU0sRUFBRSxRQUFRO1FBQzNDLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxVQUFBLGNBQWMsSUFBSSxPQUFBLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNO2VBQy9HLGNBQWMsQ0FBQyxXQUFXLEtBQUssUUFBUSxDQUFDLEVBRFUsQ0FDVixDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELDJEQUFrQyxHQUFsQyxVQUFtQyxNQUFNLEVBQUUsUUFBUSxFQUFFLE1BQU07UUFDdkQsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFVBQUEsY0FBYyxJQUFJLE9BQUEsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxLQUFLLE1BQU07ZUFDL0csY0FBYyxDQUFDLFdBQVcsS0FBSyxRQUFRO2VBQ3ZDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsRUFGdEIsQ0FFc0IsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLElBQUk7UUFDaEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7SUFDN0IsQ0FBQztJQUVELHdDQUFlLEdBQWY7UUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztJQUVELHdDQUFlLEdBQWYsVUFBZ0IsSUFBSTtRQUNoQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUQsd0NBQWUsR0FBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFjLElBQUk7UUFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUMzQixDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixJQUFJO1FBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO0lBQzdCLENBQUM7SUFFRCx3Q0FBZSxHQUFmO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCxzQ0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQzNCLENBQUM7SUFFRCxzQ0FBYSxHQUFiO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7SUFFRCx1Q0FBYyxHQUFkLFVBQWUsSUFBSTtRQUNmLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxxQ0FBWSxHQUFaLFVBQWEsSUFBSTtRQUNiLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFFRCxxQ0FBWSxHQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7UUFjSSxDQUFDLGFBQWE7SUFFbEIsc0NBQWEsR0FBYixVQUFjLElBQUk7UUFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUMzQixDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsb0NBQVcsR0FBWCxVQUFZLElBQUk7UUFDWixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQsb0NBQVcsR0FBWDtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFjLElBQUk7UUFDZCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUMzQixDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQsOENBQXFCLEdBQXJCLFVBQXNCLE1BQU07UUFDeEIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCx1Q0FBYyxHQUFkLFVBQWUsSUFBSTtRQUNmLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7O0lBeHBCUSxjQUFjO1FBSjFCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxjQUFjLENBMHBCMUI7eUJBN3FCRDtDQTZxQkMsQUExcEJELElBMHBCQztTQTFwQlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBVc2VyRGV0YWlscyB9IGZyb20gJy4uL29iamVjdHMvVXNlckRldGFpbHMnO1xyXG5pbXBvcnQgeyBNUE1NZW51VHlwZSB9IGZyb20gJy4uL29iamVjdHMvTWVudVR5cGVzJztcclxuaW1wb3J0IHsgUm9sZXMgfSBmcm9tICcuLi9vYmplY3RzL1JvbGVzJztcclxuaW1wb3J0IHsgTWVkaWFNYW5hZ2VyQ29uZmlnIH0gZnJvbSAnLi4vb2JqZWN0cy9NZWRpYU1hbmFnZXJDb25maWcnO1xyXG5pbXBvcnQgeyBDYXRlZ29yeSB9IGZyb20gJy4uL29iamVjdHMvQ2F0ZWdvcnknO1xyXG5pbXBvcnQgeyBDYXRlZ29yeUxldmVsIH0gZnJvbSAnLi4vb2JqZWN0cy9DYXRlZ29yeUxldmVsJztcclxuaW1wb3J0IHsgTVBNX1JPTEUgfSBmcm9tICcuLi9vYmplY3RzL1JvbGUnO1xyXG5pbXBvcnQgeyBNUE1NZW51IH0gZnJvbSAnLi4vb2JqZWN0cy9NZW51JztcclxuaW1wb3J0IHsgU3RhdHVzIH0gZnJvbSAnLi4vb2JqZWN0cy9TdGF0dXMnO1xyXG5pbXBvcnQgeyBNUE1GaWVsZCB9IGZyb20gJy4uL29iamVjdHMvTVBNRmllbGQnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnIH0gZnJvbSAnLi4vb2JqZWN0cy9WaWV3Q29uZmlnJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzIH0gZnJvbSAnLi4vLi4vc2hhcmVkL2NvbnN0YW50cy9zZXNzaW9uLXN0b3JhZ2UuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNoYXJpbmdTZXJ2aWNlIHtcclxuXHJcbiAgICBwdWJsaWMgY3VycmVudFVzZXJPYmplY3Q6IFVzZXJEZXRhaWxzO1xyXG4gICAgcHVibGljIGN1cnJlbnRVc2VySUQ6IHN0cmluZztcclxuICAgIHB1YmxpYyBjdXJyZW50VXNlckl0ZW1JRDogc3RyaW5nO1xyXG4gICAgcHVibGljIGN1cnJlbnRVc2VyRE46IHN0cmluZztcclxuICAgIHB1YmxpYyBkaXNwbGF5TmFtZTogc3RyaW5nO1xyXG4gICAgcHVibGljIGN1cnJlbnRPcmdETjogc3RyaW5nO1xyXG4gICAgcHVibGljIGN1cnJlbnRVc2VyRW1haWxJZDogc3RyaW5nO1xyXG4gICAgcHVibGljIGluc3RhbmNlSWRlbnRpZmllcjogc3RyaW5nO1xyXG4gICAgcHVibGljIHJlY2VudFNlYXJjaGVzID0gW107XHJcbiAgICBwdWJsaWMgbWVudTogQXJyYXk8TVBNTWVudT47XHJcbiAgICBwdWJsaWMgbG9va3VwRG9tYWlucyA9IFtdO1xyXG4gICAgcHVibGljIHJvbGVzOiBBcnJheTxSb2xlcz47XHJcbiAgICBwdWJsaWMgbWVkaWFNYW5hZ2VyQ29uZmlnOiBNZWRpYU1hbmFnZXJDb25maWc7XHJcbiAgICBwdWJsaWMgYWxsQ2F0ZWdvcmllczogQXJyYXk8Q2F0ZWdvcnk+O1xyXG4gICAgcHVibGljIHRhc2tFdmVudHMgPSBbXTtcclxuICAgIHB1YmxpYyB0YXNrV29ya2Zsb3dBY3Rpb25zID0gW107XHJcbiAgICBwdWJsaWMgc2VsZWN0ZWRDYXRlZ29yeTogQ2F0ZWdvcnk7XHJcbiAgICBwdWJsaWMgY2F0ZWdvcnlMZXZlbHM6IEFycmF5PENhdGVnb3J5TGV2ZWw+O1xyXG4gICAgcHVibGljIGFsbFByaW9yaXRpZXM6IEFycmF5PGFueT47XHJcbiAgICBwdWJsaWMgdXNlclJvbGVzOiBBcnJheTxSb2xlcz47XHJcbiAgICBwdWJsaWMgY3JBY3Rpb25zOiBhbnk7XHJcbiAgICBwdWJsaWMgYWxsU3RhdHVzQ29uZmlnOiBBcnJheTxTdGF0dXM+O1xyXG4gICAgcHVibGljIGFsbEFwcGxpY2F0aW9uRmllbGRzOiBBcnJheTxNUE1GaWVsZD4gPSBbXTtcclxuICAgIHB1YmxpYyBhbGxBcHBsaWNhdGlvblZpZXdDb25maWc6IEFycmF5PGFueT4gPSBbXTtcclxuICAgIHB1YmxpYyBwcm9qZWN0Q29uZmlnOiBhbnk7XHJcbiAgICBwdWJsaWMgYXBwQ29uZmlnOiBhbnk7XHJcbiAgICBwdWJsaWMgYXNzZXRDb25maWc6IGFueTtcclxuICAgIHB1YmxpYyBicmFuZENvbmZpZzogYW55O1xyXG4gICAgcHVibGljIGNvbW1lbnRDb25maWc6IGFueTtcclxuICAgIHB1YmxpYyB2aWV3Q29uZmlnOiBhbnk7XHJcbiAgICBwdWJsaWMgdGVhbVJvbGVzOiBhbnk7XHJcbiAgICBwdWJsaWMgY2FtcGFpZ25Sb2xlOiBhbnk7XHJcbiAgICBwdWJsaWMgdGVhbXM6IGFueTtcclxuICAgIHB1YmxpYyBwZXJzb25zOiBhbnkgPSBbXTtcclxuICAgIHB1YmxpYyByZXZpZXdlclJvbGU6IGFueTtcclxuICAgIHB1YmxpYyBpc1Jldmlld2VyOiBhbnk7XHJcbiAgICBwdWJsaWMgYXBwcm92ZXJSb2xlOiBhbnk7XHJcbiAgICBwdWJsaWMgaXNBcHByb3ZlcjogYW55O1xyXG4gICAgcHVibGljIE1hbmFnZXJSb2xlOiBhbnk7XHJcbiAgICAvLyBwdWJsaWMgY2FtcGFpZ25NYW5hZ2VyUm9sZTogYW55OyAtIE1WU1MtMTI4XHJcbiAgICBwdWJsaWMgaXNNYW5hZ2VyOiBhbnk7XHJcbiAgICAvLyBwdWJsaWMgaXNDYW1wYWlnbk1hbmFnZXI6IGFueTsgLSBNVlNTLTEyOFxyXG4gICAgcHVibGljIGlzVXBsb2FkZWQ6IGFueTtcclxuICAgIHB1YmxpYyBtZW1iZXJSb2xlOiBhbnk7XHJcbiAgICBwdWJsaWMgaXNNZW1iZXI6IGFueTtcclxuICAgIHB1YmxpYyBjYXRlZ29yeU1ldGFkYXRhOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICBwdWJsaWMgY3VycmVudFBlcnNvbjogYW55O1xyXG4gICAgcHVibGljIGN1cnJlbnRNZW51OiBhbnk7XHJcbiAgICBwdWJsaWMgY3VycmVudFVzZXJQcmVmZXJlbmNlOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICAvLyBwdWJsaWMgY2F0ZWdvcnlEZXRhaWxzID0gW107XHJcbiAgICAvLyBwdWJsaWMgZGVmYXVsdENhdGVnb3J5RGV0YWlsczogYW55O1xyXG4gICAgcHVibGljIG5vdGlmaWNhdGlvblBhc3M6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gICAgcHVibGljIHN1YnNjcmliYWJsZUJyYW5kQ29uZmlnOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICAgIHB1YmxpYyByZXNldFZpZXc6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gICAgcHVibGljIGVtYWlsQ29uZmlnO1xyXG4gICAgcHVibGljIHVzZXJQcmVmZXJlbmNlczogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgcHVibGljIGlzTWVudUNoYW5nZWQ6IEJlaGF2aW9yU3ViamVjdDxCb29sZWFuPjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmliYWJsZUJyYW5kQ29uZmlnID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KCcnKTtcclxuICAgICAgICB0aGlzLnJlc2V0VmlldyA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PignJyk7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25QYXNzID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KCcnKVxyXG4gICAgICAgIHRoaXMuaXNNZW51Q2hhbmdlZCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Qm9vbGVhbj4oZmFsc2UpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZXRNZW51Q2hhbmdlZChtZW51Y2hhbmdlZCkge1xyXG4gICAgICAgIHRoaXMuaXNNZW51Q2hhbmdlZC5uZXh0KG1lbnVjaGFuZ2VkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNZW51Q2hhbmdlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTWVudUNoYW5nZWQuYXNPYnNlcnZhYmxlKClcclxuICAgIH1cclxuXHJcbiAgICB1bnN1YnNjcmliZU1lbnVDaGFuZ2VPYnNlcmFibGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNNZW51Q2hhbmdlZC51bnN1YnNjcmliZSgpXHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldE5vdGlmaWNhdGlvbkRldGFpbHMoZGF0YSkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uUGFzcy5uZXh0KGRhdGEpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2F0ZWdvcnlNZXRhZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYXRlZ29yeU1ldGFkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIHNldENhdGVnb3J5TWV0YWRhdGEoY2F0ZWdvcnlNZXRhZGF0YTogQXJyYXk8YW55Pikge1xyXG4gICAgICAgIHRoaXMuY2F0ZWdvcnlNZXRhZGF0YSA9IGNhdGVnb3J5TWV0YWRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0ZGlzcGxheU5hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheU5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Y3VycmVudFVzZXJJdGVtSUQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJJdGVtSUQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudFVzZXJPYmplY3QoY3VycmVudFVzZXJPYmplY3Q6IFVzZXJEZXRhaWxzKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlck9iamVjdCA9IGN1cnJlbnRVc2VyT2JqZWN0O1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyT2JqZWN0KCk6IFVzZXJEZXRhaWxzIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlck9iamVjdDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50VXNlcklEKGN1cnJlbnRVc2VySWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXJJRCA9IGN1cnJlbnRVc2VySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJJRCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VySUQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJJdGVtSUQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlckl0ZW1JRDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50VXNlckl0ZW1JRChjdXJyZW50VXNlckl0ZW1JZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlckl0ZW1JRCA9IGN1cnJlbnRVc2VySXRlbUlkO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRVc2VyRE4oY3VycmVudFVzZXJETjogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VXNlckROID0gY3VycmVudFVzZXJETjtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlckROKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJETjtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50VXNlckRpc3BsYXlOYW1lKGRpc3BsYXlOYW1lOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmRpc3BsYXlOYW1lID0gZGlzcGxheU5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudE9yZ0ROKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudE9yZ0ROO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRPcmdETihvcmdETjogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50T3JnRE4gPSBvcmdETjtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50T3JnRE4gJiYgdHlwZW9mIHRoaXMuY3VycmVudE9yZ0ROID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBjb25zdCBpbnN0YW5jZUlkZW50aWZpZXIgPSB0aGlzLmN1cnJlbnRPcmdETi5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICB0aGlzLmluc3RhbmNlSWRlbnRpZmllciA9IGluc3RhbmNlSWRlbnRpZmllci5zbGljZSgxLCBpbnN0YW5jZUlkZW50aWZpZXIubGVuZ3RoKS5qb2luKCcsJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRVc2VyRW1haWxJZChlbWFpbElkOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRVc2VyRW1haWxJZCA9IGVtYWlsSWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJFbWFpbElkKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJFbWFpbElkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEluc3RhbmNlSWRlbnRpZmllcigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmluc3RhbmNlSWRlbnRpZmllcjtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlckRpc3BsYXlOYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheU5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJDTigpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRVc2VyRE4pIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJETi5zcGxpdCgnLCcpWzBdLnNwbGl0KCc9JylbMV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXRSZWNlbnRTZWFyY2gocmVjZW50U2VhcmNoKSB7XHJcbiAgICAgICAgdGhpcy5yZWNlbnRTZWFyY2hlcy51bnNoaWZ0KHJlY2VudFNlYXJjaCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVjZW50U2VhcmNoZXMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVjZW50U2VhcmNoZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVjZW50U2VhcmNoZXNCeVZpZXcodmlldykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlY2VudFNlYXJjaGVzLmZpbHRlcihzZWFyY2ggPT4gc2VhcmNoWzBdLnZpZXcgPT09IHZpZXcgfHwgdHlwZW9mIHNlYXJjaCA9PT0gJ3N0cmluZycpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldE1lbnUobWVudUxpc3Q6IEFycmF5PE1QTU1lbnU+KSB7XHJcbiAgICAgICAgdGhpcy5tZW51ID0gbWVudUxpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsTWVudSgpOiBBcnJheTxNUE1NZW51PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubWVudTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNZW51QnlUeXBlKHR5cGU6IE1QTU1lbnVUeXBlKTogQXJyYXk8TVBNTWVudT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1lbnUuZmlsdGVyKHRtcE1lbnUgPT4gdG1wTWVudS5UWVBFID09PSB0eXBlKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRUYXNrRXZlbnRzKGV2ZW50cykge1xyXG4gICAgICAgIGlmIChldmVudHMgJiYgZXZlbnRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrRXZlbnRzID0gZXZlbnRzO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnRzKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0V2ZW50cyA9IFtldmVudHNdO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza0V2ZW50cyA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrRXZlbnRzKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRhc2tFdmVudHM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0VGFza1dvcmtmbG93QWN0aW9ucyh3b3JrZmxvd0FjdGlvbnMpIHtcclxuICAgICAgICBpZiAod29ya2Zsb3dBY3Rpb25zICYmIHdvcmtmbG93QWN0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFza1dvcmtmbG93QWN0aW9ucyA9IHdvcmtmbG93QWN0aW9ucztcclxuICAgICAgICB9IGVsc2UgaWYgKHdvcmtmbG93QWN0aW9ucykge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tXb3JrZmxvd0FjdGlvbnMgPSBbd29ya2Zsb3dBY3Rpb25zXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tXb3JrZmxvd0FjdGlvbnMgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza1dvcmtmbG93QWN0aW9ucygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YXNrV29ya2Zsb3dBY3Rpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbENhdGVnb3JpZXMoY2F0ZWdvcmllczogQXJyYXk8Q2F0ZWdvcnk+KSB7XHJcbiAgICAgICAgdGhpcy5hbGxDYXRlZ29yaWVzID0gY2F0ZWdvcmllcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxDYXRlZ29yaWVzKCk6IEFycmF5PENhdGVnb3J5PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxsQ2F0ZWdvcmllcztcclxuICAgIH1cclxuXHJcbiAgICAvKiBzZXREZWZhdWx0Q2F0ZWdvcnlEZXRhaWxzKGNhdGVnb3J5KSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0Q2F0ZWdvcnlEZXRhaWxzID0gY2F0ZWdvcnk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q2F0ZWdvcnlEZXRhaWxzKGNhdGVnb3J5KSB7XHJcbiAgICAgICAgdGhpcy5jYXRlZ29yeURldGFpbHMucHVzaChjYXRlZ29yeSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2F0ZWdvcnlEZXRhaWxzQnlJZChjYXRlZ29yeUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2F0ZWdvcnlEZXRhaWxzLmZpbmQoY2F0ZWdvcnkgPT4gY2F0ZWdvcnkuaWQgPT09IGNhdGVnb3J5SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlZmF1bHRDYXRlZ29yeURldGFpbHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdENhdGVnb3J5RGV0YWlscztcclxuICAgIH0gKi9cclxuXHJcbiAgICBzZXRTZWxlY3RlZENhdGVnb3J5KHNlbGVjdGVkQ2F0ZWdvcnk6IENhdGVnb3J5KSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZENhdGVnb3J5ID0gc2VsZWN0ZWRDYXRlZ29yeTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTZWxlY3RlZENhdGVnb3J5KCk6IENhdGVnb3J5IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3RlZENhdGVnb3J5O1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbExvb2t1cERvbWFpbnMobG9va3VwRG9tYWlucykge1xyXG4gICAgICAgIHRoaXMubG9va3VwRG9tYWlucyA9IGxvb2t1cERvbWFpbnM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsTG9va3VwRG9tYWlucygpOiBBcnJheTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5sb29rdXBEb21haW5zO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFJvbGVzKHJvbGVzOiBBcnJheTxSb2xlcz4pIHtcclxuICAgICAgICB0aGlzLnJvbGVzID0gcm9sZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsUm9sZXMoKTogQXJyYXk8Um9sZXM+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yb2xlcztcclxuICAgIH1cclxuXHJcbiAgICBzZXRVc2VyUm9sZXMocm9sZXM6IEFycmF5PFJvbGVzPikge1xyXG4gICAgICAgIHRoaXMudXNlclJvbGVzID0gcm9sZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlclJvbGVzKCk6IEFycmF5PFJvbGVzPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudXNlclJvbGVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJvbGVCeU5hbWUocm9sZU5hbWU6IE1QTV9ST0xFKTogUm9sZXMge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJvbGVzLmZpbmQocm9sZSA9PiByb2xlLlJPTEVfTkFNRSA9PT0gcm9sZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldE1lZGlhTWFuYWdlckNvbmZpZyhtZWRpYU1hbmFnZXJDb25maWc6IE1lZGlhTWFuYWdlckNvbmZpZykge1xyXG4gICAgICAgIHRoaXMubWVkaWFNYW5hZ2VyQ29uZmlnID0gbWVkaWFNYW5hZ2VyQ29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1lZGlhTWFuYWdlckNvbmZpZygpOiBNZWRpYU1hbmFnZXJDb25maWcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm1lZGlhTWFuYWdlckNvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRDYXRlZ29yeUxldmVscyhjYXRlZ29yeUxldmVsczogQXJyYXk8Q2F0ZWdvcnlMZXZlbD4pIHtcclxuICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWxzID0gY2F0ZWdvcnlMZXZlbHM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2F0ZWdvcnlMZXZlbHMoKTogQXJyYXk8Q2F0ZWdvcnlMZXZlbD4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNhdGVnb3J5TGV2ZWxzID8gdGhpcy5jYXRlZ29yeUxldmVscyA6IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENhdGVnb3J5TGV2ZWxzQnlJZCgpOiBBcnJheTxhbnk+IHtcclxuICAgICAgICBsZXQgYXJyX2NhdGVnb3J5TGV2ZWwgPSBbXTtcclxuICAgICAgICB0aGlzLmNhdGVnb3J5TGV2ZWxzLmZvckVhY2goY2F0ZWdvcnlMZXZlbCA9PiB7XHJcbiAgICAgICAgICAgIGFycl9jYXRlZ29yeUxldmVsLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgaWQ6IGNhdGVnb3J5TGV2ZWxbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkLFxyXG4gICAgICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbFR5cGU6IGNhdGVnb3J5TGV2ZWwuQ0FURUdPUllfTEVWRUxfVFlQRVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gYXJyX2NhdGVnb3J5TGV2ZWw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsUHJpb3JpdGllcyhhbGxQcmlvcml0aWVzOiBBcnJheTxhbnk+KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hbGxQcmlvcml0aWVzID0gYWxsUHJpb3JpdGllcztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxQcmlvcml0aWVzKCk6IEFycmF5PGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsbFByaW9yaXRpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q1JBY3Rpb25zKGFjdGlvbnM6IEFycmF5PGFueT4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNyQWN0aW9ucyA9IGFjdGlvbnMgPyBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGFjdGlvbnMpKSA6IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENSQWN0aW9ucygpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNyQWN0aW9ucyA/IHRoaXMuY3JBY3Rpb25zIDogW107XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsU3RhdHVzQ29uZmlnKHN0YXR1c0xpc3Q6IEFycmF5PFN0YXR1cz4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmFsbFN0YXR1c0NvbmZpZyA9IHN0YXR1c0xpc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsU3RhdHVzQ29uZmlnKCk6IEFycmF5PFN0YXR1cz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsbFN0YXR1c0NvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRBbGxBcHBsaWNhdGlvbkZpZWxkcyhvdG1tRmllbGRzOiBBcnJheTxNUE1GaWVsZD4pIHtcclxuICAgICAgICB0aGlzLmFsbEFwcGxpY2F0aW9uRmllbGRzID0gb3RtbUZpZWxkcyA/IG90bW1GaWVsZHMgOiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxBcHBsaWNhdGlvbkZpZWxkcygpOiBBcnJheTxNUE1GaWVsZD4ge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5hbGxBcHBsaWNhdGlvbkZpZWxkcyAmJiB0aGlzLmFsbEFwcGxpY2F0aW9uRmllbGRzLmxlbmd0aCA+IDApID8gdGhpcy5hbGxBcHBsaWNhdGlvbkZpZWxkcyA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKHZpZXdDb25maWdzOiBBcnJheTxWaWV3Q29uZmlnPikge1xyXG4gICAgICAgIHRoaXMuYWxsQXBwbGljYXRpb25WaWV3Q29uZmlnID0gdmlld0NvbmZpZ3MgPyB2aWV3Q29uZmlncyA6IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZygpOiBBcnJheTxWaWV3Q29uZmlnPiB7XHJcbiAgICAgICAgcmV0dXJuICh0aGlzLmFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZyAmJiB0aGlzLmFsbEFwcGxpY2F0aW9uVmlld0NvbmZpZy5sZW5ndGggPiAwKSA/IHRoaXMuYWxsQXBwbGljYXRpb25WaWV3Q29uZmlnIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRQcm9qZWN0Q29uZmlnKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMucHJvamVjdENvbmZpZyA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdENvbmZpZygpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb2plY3RDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QnJhbmRDb25maWcoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5icmFuZENvbmZpZyA9IGRhdGE7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpYmFibGVCcmFuZENvbmZpZy5uZXh0KGRhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEJyYW5kQ29uZmlnKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmJyYW5kQ29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFZpZXdDb25maWcoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy52aWV3Q29uZmlnID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxWaWV3Q29uZmlnKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZpZXdDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Vmlld0NvbmZpZ0J5SWQoaWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy52aWV3Q29uZmlnLmZpbmQodmlldyA9PiB2aWV3WydNUE1fVmlld19Db25maWctaWQnXS5JZCA9PT0gaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZpZXdDb25maWdCeU5hbWUodmlld05hbWUpIHtcclxuICAgICAgICBjb25zdCBzZWxlY3RlZFZpZXdDb25maWcgPSB0aGlzLnZpZXdDb25maWcuZmluZCh2aWV3ID0+IHZpZXcuVklFVyA9PT0gdmlld05hbWUpO1xyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZFZpZXdDb25maWcgPyBzZWxlY3RlZFZpZXdDb25maWcgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFN1YnNjcmliYWJsZUJyYW5kQ29uZmlnKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3Vic2NyaWJhYmxlQnJhbmRDb25maWcuYXNPYnNlcnZhYmxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRWaWV3RGF0YSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlc2V0Vmlldy5hc09ic2VydmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJlc2V0Vmlldyh2aWV3SWQpIHtcclxuICAgICAgICB0aGlzLnJlc2V0Vmlldy5uZXh0KHZpZXdJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QXBwQ29uZmlnKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuYXBwQ29uZmlnID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBcHBDb25maWcoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHBDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q29tbWVudENvbmZpZyhkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmNvbW1lbnRDb25maWcgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbW1lbnRDb25maWcoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb21tZW50Q29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFzc2V0Q29uZmlnKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuYXNzZXRDb25maWcgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFzc2V0Q29uZmlnKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXNzZXRDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsVGVhbVJvbGVzKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG4gICAgICAgIHRoaXMudGVhbVJvbGVzID0gZGF0YTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRlYW1Sb2xlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsVGVhbVJvbGVzKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGVhbVJvbGVzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFsbFRlYW1zKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGRhdGEpKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVhbXMgPSBkYXRhO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVhbXMgPSBbZGF0YV07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFRlYW1zKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGVhbXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0QWxsUGVyc29ucyhkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLnBlcnNvbnMgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEFsbFBlcnNvbnMoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wZXJzb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEN1cnJlbnRQZXJzb24oZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50UGVyc29uID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50UGVyc29uKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFBlcnNvbjtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0RW1haWxDb25maWcoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZW1haWxDb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RGVmYXVsdEVtYWlsQ29uZmlnKGRhdGEpIHtcclxuICAgICAgICB0aGlzLmVtYWlsQ29uZmlnID0gZGF0YTtcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdFTUFJTF9DT05GSUcnLCBKU09OLnN0cmluZ2lmeSh0aGlzLmVtYWlsQ29uZmlnKSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlQ3VycmVudFVzZXJQcmVmZXJlbmNlKHVzZXJQcmVmZXJlbmNlRGF0YSk6IGFueSB7XHJcbiAgICAgICAgY29uc29sZS5sb2codXNlclByZWZlcmVuY2VEYXRhKVxyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHVzZXJQcmVmZXJlbmNlRGF0YSkpIHtcclxuICAgICAgICAgICAgdXNlclByZWZlcmVuY2VEYXRhLmZvckVhY2godXNlclByZWZlcmVuY2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4ID0gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZEluZGV4KHNlbGVjdGVkVXNlclByZWZlcmVuY2UgPT5cclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFVzZXJQcmVmZXJlbmNlWydNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJ10uSWQgPT09IHVzZXJQcmVmZXJlbmNlWydNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2Uuc3BsaWNlKHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5wdXNoKHVzZXJQcmVmZXJlbmNlKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4ID0gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZEluZGV4KHVzZXJQcmVmZXJlbmNlID0+XHJcbiAgICAgICAgICAgICAgICB1c2VyUHJlZmVyZW5jZVsnTVBNX1VzZXJfUHJlZmVyZW5jZS1pZCddLklkID09PSB1c2VyUHJlZmVyZW5jZURhdGFbJ01QTV9Vc2VyX1ByZWZlcmVuY2UtaWQnXS5JZCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZFVzZXJQcmVmZXJlbmNlSW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2Uuc3BsaWNlKHNlbGVjdGVkVXNlclByZWZlcmVuY2VJbmRleCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UucHVzaCh1c2VyUHJlZmVyZW5jZURhdGEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkNVUlJFTlRfTVBNX1VTRVJfUFJFRkVSRU5DRSwgSlNPTi5zdHJpbmdpZnkodGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UpKTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVDdXJyZW50VXNlclByZWZlcmVuY2UodXNlclByZWZlcmVuY2VJZCk6IGFueSB7XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4ID0gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZEluZGV4KHVzZXJQcmVmZXJlbmNlID0+XHJcbiAgICAgICAgICAgIHVzZXJQcmVmZXJlbmNlWydNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJ10uSWQgPT09IHVzZXJQcmVmZXJlbmNlSWQpO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFVzZXJQcmVmZXJlbmNlSW5kZXggPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5zcGxpY2Uoc2VsZWN0ZWRVc2VyUHJlZmVyZW5jZUluZGV4LCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UsIEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGRhdGEpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlID0gZGF0YTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZSA9IFtkYXRhXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UsIEpTT04uc3RyaW5naWZ5KHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q3VycmVudFVzZXJQcmVmZXJlbmNlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyUHJlZmVyZW5jZUJ5Vmlld0lkKHZpZXdJZCk6IGFueSB7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UpO1xyXG4gICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlcy5sZW5ndGggPD0gMCAmJiB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzLnB1c2godGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UgJiYgdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZCh1c2VyUHJlZmVyZW5jZSA9PiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9WSUVXX0NPTkZJR1snTVBNX1ZpZXdfQ29uZmlnLWlkJ10uSWQgPT09IHZpZXdJZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VyUHJlZmVyZW5jZXNCeVZpZXdJZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy51c2VyUHJlZmVyZW5jZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVmYXVsdEN1cnJlbnRVc2VyUHJlZmVyZW5jZUJ5TWVudShtZW51SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZCh1c2VyUHJlZmVyZW5jZSA9PiAodXNlclByZWZlcmVuY2UuSVNfREVGQVVMVF9WSUVXID09PSAndHJ1ZScgfHwgdXNlclByZWZlcmVuY2UuSVNfREVGQVVMVF9WSUVXID09PSB0cnVlKVxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9NRU5VX0lEWydNUE1fTWVudS1pZCddLklkID09PSBtZW51SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyUHJlZmVyZW5jZUJ5Vmlld01lbnUobWVudUlkLCB2aWV3SWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclByZWZlcmVuY2UuZmluZCh1c2VyUHJlZmVyZW5jZSA9PiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9WSUVXX0NPTkZJR1snTVBNX1ZpZXdfQ29uZmlnLWlkJ10uSWQgPT09IHZpZXdJZFxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9NRU5VX0lEWydNUE1fTWVudS1pZCddLklkID09PSBtZW51SWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlZmF1bHRDdXJyZW50VXNlclByZWZlcmVuY2VCeUxpc3RGaWx0ZXIobWVudUlkLCB0YXNrTmFtZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5maW5kKHVzZXJQcmVmZXJlbmNlID0+ICh1c2VyUHJlZmVyZW5jZS5JU19ERUZBVUxUX1ZJRVcgPT09ICd0cnVlJyB8fCB1c2VyUHJlZmVyZW5jZS5JU19ERUZBVUxUX1ZJRVcgPT09IHRydWUpXHJcbiAgICAgICAgICAgICYmIHVzZXJQcmVmZXJlbmNlLlJfUE9fTVBNX01FTlVfSURbJ01QTV9NZW51LWlkJ10uSWQgPT09IG1lbnVJZFxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5GSUxURVJfTkFNRSA9PT0gdGFza05hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEN1cnJlbnRVc2VyUHJlZmVyZW5jZUJ5TGlzdChtZW51SWQsIHRhc2tOYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJQcmVmZXJlbmNlLmZpbmQodXNlclByZWZlcmVuY2UgPT4gKHVzZXJQcmVmZXJlbmNlLlJfUE9fTVBNX01FTlVfSURbJ01QTV9NZW51LWlkJ10uSWQgPT09IG1lbnVJZFxyXG4gICAgICAgICAgICAmJiB1c2VyUHJlZmVyZW5jZS5GSUxURVJfTkFNRSA9PT0gdGFza05hbWUpKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2VCeUxpc3RWaWV3KG1lbnVJZCwgdGFza05hbWUsIHZpZXdJZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRVc2VyUHJlZmVyZW5jZS5maW5kKHVzZXJQcmVmZXJlbmNlID0+ICh1c2VyUHJlZmVyZW5jZS5SX1BPX01QTV9NRU5VX0lEWydNUE1fTWVudS1pZCddLklkID09PSBtZW51SWRcclxuICAgICAgICAgICAgJiYgdXNlclByZWZlcmVuY2UuRklMVEVSX05BTUUgPT09IHRhc2tOYW1lXHJcbiAgICAgICAgICAgICYmIHVzZXJQcmVmZXJlbmNlLlJfUE9fTVBNX1ZJRVdfQ09ORklHWydNUE1fVmlld19Db25maWctaWQnXS5JZCA9PT0gdmlld0lkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q2FtcGFpZ25Sb2xlKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuY2FtcGFpZ25Sb2xlID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDYW1wYWlnblJvbGUoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYW1wYWlnblJvbGU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0UmV2aWV3ZXJSb2xlKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMucmV2aWV3ZXJSb2xlID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZXZpZXdlclJvbGUoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZXZpZXdlclJvbGU7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0SXNSZXZpZXdlcihkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmlzUmV2aWV3ZXIgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElzUmV2aWV3ZXIoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc1Jldmlld2VyO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFwcHJvdmVyUm9sZShkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmFwcHJvdmVyUm9sZSA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXBwcm92ZXJSb2xlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwcm92ZXJSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldElzQXBwcm92ZXIoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5pc0FwcHJvdmVyID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJc0FwcHJvdmVyKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNBcHByb3ZlcjtcclxuICAgIH1cclxuXHJcbiAgICBzZXRNYW5hZ2VyUm9sZShkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLk1hbmFnZXJSb2xlID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNYW5hZ2VyUm9sZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLk1hbmFnZXJSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldElzTWFuYWdlcihkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmlzTWFuYWdlciA9IGRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SXNNYW5hZ2VyKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNNYW5hZ2VyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIHNldENhbXBhaWduTWFuYWdlclJvbGUoZGF0YSk6IGFueSB7XHJcbiAgICAgICAgdGhpcy5jYW1wYWlnbk1hbmFnZXJSb2xlID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDYW1wYWlnbk1hbmFnZXJSb2xlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FtcGFpZ25NYW5hZ2VyUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRJc0NhbXBhaWduTWFuYWdlcihkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmlzQ2FtcGFpZ25NYW5hZ2VyID0gZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJc0NhbXBhaWduTWFuYWdlcigpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzQ2FtcGFpZ25NYW5hZ2VyO1xyXG4gICAgfSAqLyAvLyAtIE1WU1MtMTI4XHJcblxyXG4gICAgc2V0TWVtYmVyUm9sZShkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLm1lbWJlclJvbGUgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1lbWJlclJvbGUoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5tZW1iZXJSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldElzTWVtYmVyKGRhdGEpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuaXNNZW1iZXIgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElzTWVtYmVyKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNNZW1iZXI7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0SXNVcGxvYWRlZChkYXRhKTogYW55IHtcclxuICAgICAgICB0aGlzLmlzVXBsb2FkZWQgPSBkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElzVXBsb2FkZWQoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc1VwbG9hZGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRlYW1Sb2xlSWRCeVJvbGVETihyb2xlRE4pOiBhbnkge1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsVGVhbXMoKS5zdWJzY3JpYmUoYWxsVGVhbXMgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhhbGxUZWFtcyk7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBzZXRDdXJyZW50TWVudShtZW51KTogYW55IHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRNZW51ID0gbWVudTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDdXJyZW50TWVudSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnRNZW51O1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=