import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { GatewayModule } from '../auth/gateway';
import { clearAuthCookies } from '../auth/clearAuthenticationCookies';
import { SessionInfo } from '../auth/sessionUtil';
import { getPreLoginInfo } from '../auth/getPreLoginInfo';
import { GetUserProfile } from '../auth/getUserProfile';
import { HandleOTDSRESTAuthentication } from '../auth/handleOTDSRESTAuthentication';
import * as _ from 'underscore';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { setAuthContextCookie } from '../auth/setAuthContextCookie';
import { HttpClient } from '@angular/common/http';
import { GloabalConfig as config } from '../config/config';
import * as acronui from '../auth/utility';
import { soapServicesConstants } from '../config/soapServicesConstants';
import * as X2JS from 'x2js';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var x2js = new X2JS();
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.setSessionInfo = function (data) {
            if (data && data.Authenticator) {
                AuthenticationService_1.sessionUtil.setSessionInfo(data.Authenticator);
            }
        };
        this.getPreLoginDetails = function () {
            return new Observable(function (observer) {
                getPreLoginInfo().then(function (data) {
                    observer.next(data);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            });
        };
        this.getUserInfo = function () {
            return new Observable(function (observer) {
                GetUserProfile(soapServicesConstants.USER_NSPC).then(function (data) {
                    observer.next(data);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            });
        };
        this.authenticateInOtds = function (username, password) {
            return new Observable(function (observer) {
                HandleOTDSRESTAuthentication(username, password).then(function (data) {
                    observer.next(data);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            });
        };
        this.logout = function () {
            return new Observable(function (observer) {
                clearAuthCookies().then(function (data) {
                    observer.next(data);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            });
        };
        this.setPreLoginCookie = function () {
            return setAuthContextCookie();
        };
        _.extend($, GatewayModule.extensions);
        $.ajaxSetup(GatewayModule.ajaxSetup);
    }
    AuthenticationService_1 = AuthenticationService;
    AuthenticationService.prototype.fireRestUrl = function (url, urlparam) {
        return this.http.get(url + '?' + urlparam);
    };
    AuthenticationService.prototype.getauthenticationCookie = function (url, paramters) {
        return this.fireRestUrl(url + config.config.preLoginUrl, paramters);
    };
    AuthenticationService.prototype.getCookieValue = function (cname) {
        var name = cname + '=';
        var ca = document.cookie.split(';');
        // tslint:disable-next-line: prefer-for-of
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    };
    AuthenticationService.prototype.getUrlParams = function () {
        var urlParams = {};
        var params = window.location.search.substring(1).split('&');
        if (params && params.length > 0) {
            // tslint:disable-next-line: prefer-for-of
            for (var i = 0; i < params.length; i++) {
                if (params[i] !== '' && params[i].indexOf('=') !== -1) {
                    var tmp = params[i].split('=');
                    urlParams[tmp[0]] = decodeURI(tmp[1]);
                }
            }
        }
        return urlParams;
    };
    AuthenticationService.prototype.isValidCookie = function (currentCookie) {
        var _this = this;
        var orgDn = config.getOrganizationDN();
        return new Observable(function (observer) {
            _this.getauthenticationCookie(config.webServiceUrl, encodeURIComponent(orgDn))
                .subscribe(function (response) {
                var xml = $.parseXML(response.toString());
                xml = x2js.xml2json(xml);
                var preLoginCookie = acronui.findObjectsByProp(xml, 'SamlArtifactCookieName');
                if (currentCookie && currentCookie !== undefined) {
                    var pspCookie = _this.getCookieValue(preLoginCookie[0].text);
                    if (pspCookie == null || pspCookie === '') {
                        var params = _this.getUrlParams();
                        var viewToRedirect = params['redirectView'];
                        var projectToRedirect = params['task'];
                        var taskToRedirect = params['project'];
                        var deliverableToRedirect = params['deliverable'];
                        var popupType = params['type'];
                        var assetKey = params['assetKey'];
                        var taskId = params['taskId'];
                        var projectId = params['projectId'];
                        var transitTo = params['goTo'];
                        if (transitTo && transitTo != null && transitTo !== '') {
                            window.sessionStorage.setItem('transitTo', transitTo);
                        }
                        else if (viewToRedirect != null && viewToRedirect) {
                            window.sessionStorage.setItem('appRedirectView', viewToRedirect);
                        }
                        else if (projectToRedirect != null && projectToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('ProjectRedirectView', projectToRedirect);
                        }
                        else if (taskToRedirect != null && taskToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('TaskRedirectView', taskToRedirect);
                        }
                        else if (popupType && popupType != null && popupType === 'deliverable' && assetKey && assetKey != null) {
                            window.sessionStorage.setItem('PopupDeliverable', assetKey);
                        }
                        else if (popupType && popupType != null && popupType === 'task' && taskId && taskId != null &&
                            projectId && projectId != null) {
                            var popupTask = { taskId: taskId, projectId: projectId };
                            window.sessionStorage.setItem('PopupTask', JSON.stringify(popupTask));
                        }
                        else if (deliverableToRedirect != null && deliverableToRedirect) {
                            // links from email may have Project, Task and Deliverable to redirect the user to corresponding
                            window.sessionStorage.setItem('DeliverableRedirectView', deliverableToRedirect);
                        }
                        window.location.href = config.getOTDSLoginForm();
                        observer.next(true);
                        observer.complete();
                    }
                }
            }, function (error) {
                observer.next(false);
                observer.complete();
                console.warn('Unable to fire pre-login session to get cookie name.');
            });
        });
    };
    AuthenticationService.prototype.logIn = function () {
        var self = this;
        return new Observable(function (observer) {
            GetUserProfile(soapServicesConstants.USER_LDAP).then(function (data) {
                var userObject = acronui.findObjectsByProp(data, 'user');
                var userDN = userObject[0].authuserdn;
                var userName = userObject[0].authuserdn.split(',')[0].split('=')[1];
                var org = userObject[0].authuserdn.split('cn=anonymous,cn=authenticated users,')[1];
                var orgDN = 'o=' + config.config.organizationName + ',';
                orgDN = orgDN.concat(org);
                if (userName === 'anonymous') {
                    self.isValidCookie(document.cookie)
                        .subscribe(function (response) {
                        if (!response) {
                            observer.next(false);
                            observer.error(new Error());
                        }
                    }, function (error) {
                        observer.next(false);
                        observer.error(new Error(error));
                    });
                }
                else {
                    observer.next(true);
                    observer.complete();
                }
            }, function (error) {
                observer.next(false);
                observer.error(new Error(error));
            });
        });
    };
    var AuthenticationService_1;
    AuthenticationService.sessionUtil = SessionInfo.getInstance();
    AuthenticationService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    AuthenticationService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AuthenticationService_Factory() { return new AuthenticationService(i0.ɵɵinject(i1.HttpClient)); }, token: AuthenticationService, providedIn: "root" });
    AuthenticationService = AuthenticationService_1 = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AuthenticationService);
    return AuthenticationService;
}());
export { AuthenticationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL21wbS1saWJyYXJ5LyIsInNvdXJjZXMiOlsibGliL21wbS11dGlscy9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNoRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUNwRixPQUFPLEtBQUssQ0FBQyxNQUFNLFlBQVksQ0FBQztBQUNoQyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsYUFBYSxJQUFJLE1BQU0sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzNELE9BQU8sS0FBSyxPQUFPLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDeEUsT0FBTyxLQUFLLElBQUksTUFBTSxNQUFNLENBQUM7OztBQUU3QixJQUFNLElBQUksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDO0FBTXhCO0lBSUksK0JBQ1csSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQU0zQixtQkFBYyxHQUFHLFVBQUMsSUFBSTtZQUNsQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM1Qix1QkFBcUIsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUN4RTtRQUNMLENBQUMsQ0FBQTtRQUVELHVCQUFrQixHQUFHO1lBQ2pCLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO2dCQUMxQixlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJO29CQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFDLEtBQUs7b0JBQ0wsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFBO1FBRUQsZ0JBQVcsR0FBRztZQUNWLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO2dCQUMxQixjQUFjLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtvQkFDdEQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQyxLQUFLO29CQUNMLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQTtRQUVELHVCQUFrQixHQUFHLFVBQUMsUUFBUSxFQUFFLFFBQVE7WUFDcEMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFDLFFBQVE7Z0JBQzNCLDRCQUE0QixDQUFDLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJO29CQUN2RCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFDLEtBQUs7b0JBQ0wsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFBO1FBRUQsV0FBTSxHQUFHO1lBQ0wsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7Z0JBQzFCLGdCQUFnQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtvQkFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQyxLQUFLO29CQUNMLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQTtRQUVELHNCQUFpQixHQUFHO1lBQ2hCLE9BQU8sb0JBQW9CLEVBQUUsQ0FBQztRQUNsQyxDQUFDLENBQUE7UUF4REcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7OEJBVFEscUJBQXFCO0lBaUU5QiwyQ0FBVyxHQUFYLFVBQVksR0FBRyxFQUFFLFFBQVE7UUFDckIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCx1REFBdUIsR0FBdkIsVUFBd0IsR0FBRyxFQUFFLFNBQVM7UUFDbEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQsOENBQWMsR0FBZCxVQUFlLEtBQUs7UUFDaEIsSUFBTSxJQUFJLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUN6QixJQUFNLEVBQUUsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QywwQ0FBMEM7UUFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2QsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtnQkFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUFFO1lBQ25ELElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQUUsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQUU7U0FDNUU7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCw0Q0FBWSxHQUFaO1FBQ0ksSUFBTSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0IsMENBQTBDO1lBQzFDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtvQkFDbkQsSUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDakMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDekM7YUFDSjtTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVELDZDQUFhLEdBQWIsVUFBYyxhQUFhO1FBQTNCLGlCQXFEQztRQXBERyxJQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDeEUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2dCQUMxQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsSUFBTSxjQUFjLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO2dCQUNoRixJQUFJLGFBQWEsSUFBSSxhQUFhLEtBQUssU0FBUyxFQUFFO29CQUM5QyxJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsS0FBSyxFQUFFLEVBQUU7d0JBQ3ZDLElBQU0sTUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzt3QkFDbkMsSUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3dCQUM5QyxJQUFNLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDekMsSUFBTSxjQUFjLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUN6QyxJQUFNLHFCQUFxQixHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDcEQsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNqQyxJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ3BDLElBQU0sTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDaEMsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUN0QyxJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ2pDLElBQUksU0FBUyxJQUFJLFNBQVMsSUFBSSxJQUFJLElBQUksU0FBUyxLQUFLLEVBQUUsRUFBRTs0QkFDcEQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3lCQUN6RDs2QkFBTSxJQUFJLGNBQWMsSUFBSSxJQUFJLElBQUksY0FBYyxFQUFFOzRCQUNqRCxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxjQUFjLENBQUMsQ0FBQzt5QkFDcEU7NkJBQU0sSUFBSSxpQkFBaUIsSUFBSSxJQUFJLElBQUksaUJBQWlCLEVBQUU7NEJBQ3ZELGdHQUFnRzs0QkFDaEcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQzt5QkFDM0U7NkJBQU0sSUFBSSxjQUFjLElBQUksSUFBSSxJQUFJLGNBQWMsRUFBRTs0QkFDakQsZ0dBQWdHOzRCQUNoRyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxjQUFjLENBQUMsQ0FBQzt5QkFDckU7NkJBQU0sSUFBSSxTQUFTLElBQUksU0FBUyxJQUFJLElBQUksSUFBSSxTQUFTLEtBQUssYUFBYSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksSUFBSSxFQUFFOzRCQUN0RyxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxRQUFRLENBQUMsQ0FBQzt5QkFDL0Q7NkJBQU0sSUFBSSxTQUFTLElBQUksU0FBUyxJQUFJLElBQUksSUFBSSxTQUFTLEtBQUssTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksSUFBSTs0QkFDekYsU0FBUyxJQUFJLFNBQVMsSUFBSSxJQUFJLEVBQUU7NEJBQ2hDLElBQU0sU0FBUyxHQUFHLEVBQUUsTUFBTSxRQUFBLEVBQUUsU0FBUyxXQUFBLEVBQUUsQ0FBQzs0QkFDeEMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt5QkFDekU7NkJBQU0sSUFBSSxxQkFBcUIsSUFBSSxJQUFJLElBQUkscUJBQXFCLEVBQUU7NEJBQy9ELGdHQUFnRzs0QkFDaEcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUscUJBQXFCLENBQUMsQ0FBQzt5QkFDbkY7d0JBRUQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7d0JBQ2pELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7aUJBQ0o7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsT0FBTyxDQUFDLElBQUksQ0FBQyxzREFBc0QsQ0FBQyxDQUFDO1lBQ3pFLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQscUNBQUssR0FBTDtRQUNJLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQztRQUNsQixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixjQUFjLENBQUMscUJBQXFCLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtnQkFDdEQsSUFBTSxVQUFVLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDM0QsSUFBTSxNQUFNLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsSUFBTSxRQUFRLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0RSxJQUFNLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0RixJQUFJLEtBQUssR0FBRyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUM7Z0JBQ3hELEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMxQixJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzt5QkFDOUIsU0FBUyxDQUFDLFVBQUEsUUFBUTt3QkFDZixJQUFJLENBQUMsUUFBUSxFQUFFOzRCQUNYLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3JCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxDQUFDO3lCQUMvQjtvQkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO3dCQUNKLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ3JCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDckMsQ0FBQyxDQUFDLENBQUM7aUJBQ1Y7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztJQXZMYSxpQ0FBVyxHQUFHLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7Z0JBR3JDLFVBQVU7OztJQUxsQixxQkFBcUI7UUFKakMsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUVXLHFCQUFxQixDQTJMakM7Z0NBbE5EO0NBa05DLEFBM0xELElBMkxDO1NBM0xZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgR2F0ZXdheU1vZHVsZSB9IGZyb20gJy4uL2F1dGgvZ2F0ZXdheSc7XHJcbmltcG9ydCB7IGNsZWFyQXV0aENvb2tpZXMgfSBmcm9tICcuLi9hdXRoL2NsZWFyQXV0aGVudGljYXRpb25Db29raWVzJztcclxuaW1wb3J0IHsgU2Vzc2lvbkluZm8gfSBmcm9tICcuLi9hdXRoL3Nlc3Npb25VdGlsJztcclxuaW1wb3J0IHsgZ2V0UHJlTG9naW5JbmZvIH0gZnJvbSAnLi4vYXV0aC9nZXRQcmVMb2dpbkluZm8nO1xyXG5pbXBvcnQgeyBHZXRVc2VyUHJvZmlsZSB9IGZyb20gJy4uL2F1dGgvZ2V0VXNlclByb2ZpbGUnO1xyXG5pbXBvcnQgeyBIYW5kbGVPVERTUkVTVEF1dGhlbnRpY2F0aW9uIH0gZnJvbSAnLi4vYXV0aC9oYW5kbGVPVERTUkVTVEF1dGhlbnRpY2F0aW9uJztcclxuaW1wb3J0ICogYXMgXyBmcm9tICd1bmRlcnNjb3JlJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHNldEF1dGhDb250ZXh0Q29va2llIH0gZnJvbSAnLi4vYXV0aC9zZXRBdXRoQ29udGV4dENvb2tpZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vY29uZmlnL2NvbmZpZyc7XHJcbmltcG9ydCAqIGFzIGFjcm9udWkgZnJvbSAnLi4vYXV0aC91dGlsaXR5JztcclxuaW1wb3J0IHsgc29hcFNlcnZpY2VzQ29uc3RhbnRzIH0gZnJvbSAnLi4vY29uZmlnL3NvYXBTZXJ2aWNlc0NvbnN0YW50cyc7XHJcbmltcG9ydCAqIGFzIFgySlMgZnJvbSAneDJqcyc7XHJcblxyXG5jb25zdCB4MmpzID0gbmV3IFgySlMoKTtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBzZXNzaW9uVXRpbCA9IFNlc3Npb25JbmZvLmdldEluc3RhbmNlKCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICApIHtcclxuICAgICAgICBfLmV4dGVuZCgkLCBHYXRld2F5TW9kdWxlLmV4dGVuc2lvbnMpO1xyXG4gICAgICAgICQuYWpheFNldHVwKEdhdGV3YXlNb2R1bGUuYWpheFNldHVwKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRTZXNzaW9uSW5mbyA9IChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5BdXRoZW50aWNhdG9yKSB7XHJcbiAgICAgICAgICAgIEF1dGhlbnRpY2F0aW9uU2VydmljZS5zZXNzaW9uVXRpbC5zZXRTZXNzaW9uSW5mbyhkYXRhLkF1dGhlbnRpY2F0b3IpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmVMb2dpbkRldGFpbHMgPSAoKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgZ2V0UHJlTG9naW5JbmZvKCkudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJJbmZvID0gKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIEdldFVzZXJQcm9maWxlKHNvYXBTZXJ2aWNlc0NvbnN0YW50cy5VU0VSX05TUEMpLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhdXRoZW50aWNhdGVJbk90ZHMgPSAodXNlcm5hbWUsIHBhc3N3b3JkKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICBIYW5kbGVPVERTUkVTVEF1dGhlbnRpY2F0aW9uKHVzZXJuYW1lLCBwYXNzd29yZCkudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCA9ICgpID0+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjbGVhckF1dGhDb29raWVzKCkudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFByZUxvZ2luQ29va2llID0gKCkgPT4ge1xyXG4gICAgICAgIHJldHVybiBzZXRBdXRoQ29udGV4dENvb2tpZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGZpcmVSZXN0VXJsKHVybCwgdXJscGFyYW0pIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldCh1cmwgKyAnPycgKyB1cmxwYXJhbSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0YXV0aGVudGljYXRpb25Db29raWUodXJsLCBwYXJhbXRlcnMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maXJlUmVzdFVybCh1cmwgKyBjb25maWcuY29uZmlnLnByZUxvZ2luVXJsLCBwYXJhbXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvb2tpZVZhbHVlKGNuYW1lKSB7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9IGNuYW1lICsgJz0nO1xyXG4gICAgICAgIGNvbnN0IGNhID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBwcmVmZXItZm9yLW9mXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjYS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBsZXQgYyA9IGNhW2ldO1xyXG4gICAgICAgICAgICB3aGlsZSAoYy5jaGFyQXQoMCkgPT09ICcgJykgeyBjID0gYy5zdWJzdHJpbmcoMSk7IH1cclxuICAgICAgICAgICAgaWYgKGMuaW5kZXhPZihuYW1lKSA9PT0gMCkgeyByZXR1cm4gYy5zdWJzdHJpbmcobmFtZS5sZW5ndGgsIGMubGVuZ3RoKTsgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXJsUGFyYW1zKCkge1xyXG4gICAgICAgIGNvbnN0IHVybFBhcmFtcyA9IHt9O1xyXG4gICAgICAgIGNvbnN0IHBhcmFtcyA9IHdpbmRvdy5sb2NhdGlvbi5zZWFyY2guc3Vic3RyaW5nKDEpLnNwbGl0KCcmJyk7XHJcbiAgICAgICAgaWYgKHBhcmFtcyAmJiBwYXJhbXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IHByZWZlci1mb3Itb2ZcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJhbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbXNbaV0gIT09ICcnICYmIHBhcmFtc1tpXS5pbmRleE9mKCc9JykgIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG1wID0gcGFyYW1zW2ldLnNwbGl0KCc9Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsUGFyYW1zW3RtcFswXV0gPSBkZWNvZGVVUkkodG1wWzFdKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdXJsUGFyYW1zO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWRDb29raWUoY3VycmVudENvb2tpZSkge1xyXG4gICAgICAgIGNvbnN0IG9yZ0RuID0gY29uZmlnLmdldE9yZ2FuaXphdGlvbkROKCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5nZXRhdXRoZW50aWNhdGlvbkNvb2tpZShjb25maWcud2ViU2VydmljZVVybCwgZW5jb2RlVVJJQ29tcG9uZW50KG9yZ0RuKSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB4bWwgPSAkLnBhcnNlWE1MKHJlc3BvbnNlLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHhtbCA9IHgyanMueG1sMmpzb24oeG1sKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBwcmVMb2dpbkNvb2tpZSA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AoeG1sLCAnU2FtbEFydGlmYWN0Q29va2llTmFtZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50Q29va2llICYmIGN1cnJlbnRDb29raWUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwc3BDb29raWUgPSB0aGlzLmdldENvb2tpZVZhbHVlKHByZUxvZ2luQ29va2llWzBdLnRleHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocHNwQ29va2llID09IG51bGwgfHwgcHNwQ29va2llID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gdGhpcy5nZXRVcmxQYXJhbXMoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHZpZXdUb1JlZGlyZWN0ID0gcGFyYW1zWydyZWRpcmVjdFZpZXcnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb2plY3RUb1JlZGlyZWN0ID0gcGFyYW1zWyd0YXNrJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0YXNrVG9SZWRpcmVjdCA9IHBhcmFtc1sncHJvamVjdCddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVsaXZlcmFibGVUb1JlZGlyZWN0ID0gcGFyYW1zWydkZWxpdmVyYWJsZSddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcG9wdXBUeXBlID0gcGFyYW1zWyd0eXBlJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBhc3NldEtleSA9IHBhcmFtc1snYXNzZXRLZXknXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhc2tJZCA9IHBhcmFtc1sndGFza0lkJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9qZWN0SWQgPSBwYXJhbXNbJ3Byb2plY3RJZCddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdHJhbnNpdFRvID0gcGFyYW1zWydnb1RvJ107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNpdFRvICYmIHRyYW5zaXRUbyAhPSBudWxsICYmIHRyYW5zaXRUbyAhPT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgndHJhbnNpdFRvJywgdHJhbnNpdFRvKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmlld1RvUmVkaXJlY3QgIT0gbnVsbCAmJiB2aWV3VG9SZWRpcmVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdhcHBSZWRpcmVjdFZpZXcnLCB2aWV3VG9SZWRpcmVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHByb2plY3RUb1JlZGlyZWN0ICE9IG51bGwgJiYgcHJvamVjdFRvUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBsaW5rcyBmcm9tIGVtYWlsIG1heSBoYXZlIFByb2plY3QsIFRhc2sgYW5kIERlbGl2ZXJhYmxlIHRvIHJlZGlyZWN0IHRoZSB1c2VyIHRvIGNvcnJlc3BvbmRpbmdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnUHJvamVjdFJlZGlyZWN0VmlldycsIHByb2plY3RUb1JlZGlyZWN0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGFza1RvUmVkaXJlY3QgIT0gbnVsbCAmJiB0YXNrVG9SZWRpcmVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGxpbmtzIGZyb20gZW1haWwgbWF5IGhhdmUgUHJvamVjdCwgVGFzayBhbmQgRGVsaXZlcmFibGUgdG8gcmVkaXJlY3QgdGhlIHVzZXIgdG8gY29ycmVzcG9uZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdUYXNrUmVkaXJlY3RWaWV3JywgdGFza1RvUmVkaXJlY3QpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwb3B1cFR5cGUgJiYgcG9wdXBUeXBlICE9IG51bGwgJiYgcG9wdXBUeXBlID09PSAnZGVsaXZlcmFibGUnICYmIGFzc2V0S2V5ICYmIGFzc2V0S2V5ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnUG9wdXBEZWxpdmVyYWJsZScsIGFzc2V0S2V5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocG9wdXBUeXBlICYmIHBvcHVwVHlwZSAhPSBudWxsICYmIHBvcHVwVHlwZSA9PT0gJ3Rhc2snICYmIHRhc2tJZCAmJiB0YXNrSWQgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2plY3RJZCAmJiBwcm9qZWN0SWQgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBvcHVwVGFzayA9IHsgdGFza0lkLCBwcm9qZWN0SWQgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnUG9wdXBUYXNrJywgSlNPTi5zdHJpbmdpZnkocG9wdXBUYXNrKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRlbGl2ZXJhYmxlVG9SZWRpcmVjdCAhPSBudWxsICYmIGRlbGl2ZXJhYmxlVG9SZWRpcmVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGxpbmtzIGZyb20gZW1haWwgbWF5IGhhdmUgUHJvamVjdCwgVGFzayBhbmQgRGVsaXZlcmFibGUgdG8gcmVkaXJlY3QgdGhlIHVzZXIgdG8gY29ycmVzcG9uZGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5zZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdEZWxpdmVyYWJsZVJlZGlyZWN0VmlldycsIGRlbGl2ZXJhYmxlVG9SZWRpcmVjdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBjb25maWcuZ2V0T1REU0xvZ2luRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oJ1VuYWJsZSB0byBmaXJlIHByZS1sb2dpbiBzZXNzaW9uIHRvIGdldCBjb29raWUgbmFtZS4nKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ0luKCkge1xyXG4gICAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIEdldFVzZXJQcm9maWxlKHNvYXBTZXJ2aWNlc0NvbnN0YW50cy5VU0VSX0xEQVApLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJPYmplY3QgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKGRhdGEsICd1c2VyJyk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1c2VyRE4gPSB1c2VyT2JqZWN0WzBdLmF1dGh1c2VyZG47XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1c2VyTmFtZSA9IHVzZXJPYmplY3RbMF0uYXV0aHVzZXJkbi5zcGxpdCgnLCcpWzBdLnNwbGl0KCc9JylbMV07XHJcbiAgICAgICAgICAgICAgICBjb25zdCBvcmcgPSB1c2VyT2JqZWN0WzBdLmF1dGh1c2VyZG4uc3BsaXQoJ2NuPWFub255bW91cyxjbj1hdXRoZW50aWNhdGVkIHVzZXJzLCcpWzFdO1xyXG4gICAgICAgICAgICAgICAgbGV0IG9yZ0ROID0gJ289JyArIGNvbmZpZy5jb25maWcub3JnYW5pemF0aW9uTmFtZSArICcsJztcclxuICAgICAgICAgICAgICAgIG9yZ0ROID0gb3JnRE4uY29uY2F0KG9yZyk7XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlck5hbWUgPT09ICdhbm9ueW1vdXMnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5pc1ZhbGlkQ29va2llKGRvY3VtZW50LmNvb2tpZSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19