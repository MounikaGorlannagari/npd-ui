import { __decorate, __values } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { AppService } from './app.service';
import { Observable, forkJoin } from 'rxjs';
import { EntityAppDefService } from './entity.appdef.service';
import { UtilService } from './util.service';
import { SharingService } from './sharing.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { otmmServicesConstants } from '../config/otmmService.constant';
import { GloabalConfig as config } from '../config/config';
import { NotificationService } from '../../notification/notification.service';
import { OTMMMPMDataTypes } from '../objects/OTMMMPMDataTypes';
import { QdsService } from '../../upload/services/qds.service';
import { FileTypes } from '../objects/FileTypes';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import { ApplicationConfigConstants } from '../../shared/constants/application.config.constants';
import * as i0 from "@angular/core";
import * as i1 from "./entity.appdef.service";
import * as i2 from "./app.service";
import * as i3 from "./util.service";
import * as i4 from "./sharing.service";
import * as i5 from "../../notification/notification.service";
import * as i6 from "@angular/common/http";
import * as i7 from "../../upload/services/qds.service";
var OTMMService = /** @class */ (function () {
    function OTMMService(entityAppDefService, appService, utilService, sharingService, notificationService, http, qdsService) {
        this.entityAppDefService = entityAppDefService;
        this.appService = appService;
        this.utilService = utilService;
        this.sharingService = sharingService;
        this.notificationService = notificationService;
        this.http = http;
        this.qdsService = qdsService;
        this.renditionCount = 0;
        this.otmmSessionExists = false;
        this.executedCount = 0;
        this.isSessionMasking = false;
        this.INTERVAL_DELAY = 15000;
        this.STANDARD_SESSION_TIMEOUT_INTERVAL = 600000;
        this.userDetailsObj = {
            email: ''
        };
    }
    OTMMService.prototype.getAssetsbyAssetUrl = function (url, parameters) {
        return this.fireGetRestRequest(url, parameters);
    };
    OTMMService.prototype.setMasking = function (isMasking) {
        this.isSessionMasking = isMasking;
    };
    OTMMService.prototype.getMasking = function () {
        return this.isSessionMasking;
    };
    OTMMService.prototype.getMaskingType = function () {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.maskingType;
    };
    OTMMService.prototype.getMediaManagerUrl = function () {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.mediaManagerConfigUrl;
    };
    OTMMService.prototype.getJessionIdFromWebServer = function () {
        return otmmServicesConstants.OTMM_SERVICE_VARIABLES.httpOTMMTicket;
    };
    OTMMService.prototype.fireGetRestRequest = function (url, parameters) {
        var _this = this;
        return new Observable(function (observer) {
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            _this.http.get(_this.formRestUrl(url, parameters), httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.firePostRestRequest = function (url, parameters, contentType, headerObject) {
        var _this = this;
        var httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': contentType
            })
        };
        return new Observable(function (observer) {
            _this.http.post(_this.formRestUrl(url, null, false), parameters, headerObject)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.fireRequestforJsessionCookie = function (url, parameters, contentType) {
        var _this = this;
        var httpOptions = {
            withCredentials: true,
            responseType: 'text',
            headers: new HttpHeaders({
                'Content-Type': contentType,
            })
        };
        return new Observable(function (observer) {
            _this.http.post(url, parameters, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.next(error);
                observer.complete();
            });
        });
    };
    OTMMService.prototype.shareAssetsPostRequest = function (otmmapiurl, parameters, contentType) {
        var _this = this;
        var mediaManagerConfig = this.sharingService.getMediaManagerConfig();
        var baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        if (!mediaManagerConfig) {
            throw new Error('Media Manager config not found');
            return;
        }
        var urlToBeFired = baseUrl + otmmServicesConstants.otmmapiBaseUrl + mediaManagerConfig.apiVersion + '/' + otmmapiurl;
        var httpOptions = {
            withCredentials: true,
            headers: new HttpHeaders({
                'Content-Type': contentType,
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()
            }),
        };
        return new Observable(function (observer) {
            _this.http.post(urlToBeFired, parameters, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.next(error);
                observer.complete();
            });
        });
    };
    OTMMService.prototype.fireDeleteRestRequest = function (url, httpOptions) {
        var _this = this;
        httpOptions = {
            withCredentials: true,
            responseType: 'text',
            headers: new HttpHeaders({
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
            })
        };
        return new Observable(function (observer) {
            _this.http.delete(_this.formRestUrl(url, null, false), httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.firePatchRestRequest = function (url, parameters, contentType, headerObject) {
        var _this = this;
        var httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString())
                .set('Content-Type', 'application/json'),
            withCredentials: true
        };
        return new Observable(function (observer) {
            _this.http.patch(_this.formRestUrl(url, null, false), parameters, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.firePutRestRequest = function (url, parameters, contentType, headerObject) {
        var _this = this;
        var postReq;
        var httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        postReq = this.http.put(this.formRestUrl(url, null), parameters, httpOptions);
        return new Observable(function (observer) {
            postReq.subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                _this.appService.checkPSSession(function () {
                    observer.error(new Error(error));
                });
            });
        });
    };
    OTMMService.prototype.getCookie = function (cname) {
        var e_1, _a;
        var name = cname + '=';
        var cookies = document.cookie.split(';');
        try {
            for (var cookies_1 = __values(cookies), cookies_1_1 = cookies_1.next(); !cookies_1_1.done; cookies_1_1 = cookies_1.next()) {
                var cookie = cookies_1_1.value;
                var c = cookie;
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (cookies_1_1 && !cookies_1_1.done && (_a = cookies_1.return)) _a.call(cookies_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return '';
    };
    OTMMService.prototype.formRestParams = function (parameters) {
        var param = '?';
        for (var i = 0; i < parameters.length; i++) {
            if (parameters[i]['key'] === 'json') {
                param += i === 0 ? parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value)) :
                    '&' + parameters[i].value.key + '=' + encodeURIComponent(JSON.stringify(parameters[i].value.value));
            }
            else {
                param += i === 0 ? parameters[i].key + '=' + parameters[i].value : '&' + parameters[i].key + '=' + parameters[i].value;
            }
        }
        return param;
    };
    OTMMService.prototype.formRestUrl = function (otmmapiurl, urlparam, isRendition) {
        var baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        if (isRendition === true) {
            return baseUrl + otmmapiurl;
        }
        else {
            var mediaManagerConfig = this.sharingService.getMediaManagerConfig();
            if (!mediaManagerConfig) {
                throw new Error('Media Manager config not found');
                return;
            }
            return baseUrl + otmmServicesConstants.otmmapiBaseUrl +
                this.sharingService.getMediaManagerConfig().apiVersion + '/' + otmmapiurl +
                (urlparam != null ? this.formRestParams(urlparam) : '');
        }
    };
    OTMMService.prototype.doPostRequest = function (url, parameters, headerObject, isOnlyResponse) {
        var _this = this;
        return new Observable(function (observer) {
            if (headerObject && headerObject != null) {
                _this.http.post(url, parameters, headerObject).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            }
            else {
                var headerOptions = {
                    withCredentials: true,
                    headers: new HttpHeaders({
                        'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString()
                    })
                };
                _this.http.post(url, parameters, headerOptions).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            }
        });
    };
    OTMMService.prototype.doPutRequest = function (url, parameters, headerObject, isOnlyResponse) {
        var _this = this;
        return new Observable(function (observer) {
            if (headerObject) {
                _this.http.put(url, parameters, headerObject).subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            }
            else {
                var httpOptions = {
                    withCredentials: true,
                    headers: new HttpHeaders({
                        'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    })
                };
                _this.http.put(url, parameters, httpOptions)
                    .subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(new Error(error));
                });
            }
        });
    };
    OTMMService.prototype.getExportJob = function () {
        var params = [{
                key: 'load_asset_details',
                value: 'true'
            }, {
                key: 'sort',
                value: 'desc_last_updated_date_time'
            }, {
                key: 'load_job_details',
                value: 'true'
            }, {
                key: 'last_updated_date_time',
                value: 'TODAY'
            }, {
                key: 'limit',
                value: '5000'
            }];
        this.getExportJobs(params)
            .subscribe(function (response) {
            if (response && response.jobs_resource && response.jobs_resource.collection_summary &&
                response.jobs_resource.collection_summary.actual_count_of_items &&
                response.jobs_resource.collection_summary.actual_count_of_items > 0) {
                console.log("get export response");
            }
            else {
                console.log("No items");
            }
        }, function (error) {
            console.log("Unable to get data from QDS.");
        });
    };
    OTMMService.prototype.checkOtmmSession = function (isUpload, openQDSTray) {
        var _this = this;
        return new Observable(function (observer) {
            // MSV-239
            // if (!isUpload) {
            _this.fireGetRestRequest(otmmServicesConstants.otmmSessionUrl, null)
                .subscribe(function (getSessionResponse) {
                if (getSessionResponse && getSessionResponse.session_resource && getSessionResponse.session_resource.session.id) {
                    // MSV-239
                    _this.getSessionResponse = getSessionResponse;
                    otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId = getSessionResponse.session_resource.session.id;
                }
            }, function (getSessionError) {
                _this.startOTMMSession(true) // MVS-239 true
                    .subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            });
            // } else { // MSV -239
            if (_this.sharingService.getMediaManagerConfig().enableQDS &&
                otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
                // tslint:disable-next-line: max-line-length
                _this.qdsService.getQDSSession(otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId)
                    .subscribe(function (qdsSessionResponse) {
                    // tslint:disable-next-line: max-line-length
                    // MSV-239
                    _this.qdsSessionResponse = qdsSessionResponse;
                    _this.getExportJob();
                    _this.qdsService.connectToQDS(qdsSessionResponse.transferschemes_session_resource.transfer_session, isUpload)
                        .subscribe(function (response) {
                        observer.next(_this.getSessionResponse);
                        observer.complete();
                    }, function (error) {
                        observer.next(_this.getSessionResponse);
                        observer.complete();
                    });
                }, function (qdsSessionError) {
                    _this.notificationService.error('Something went wrong while getting QDS session');
                    observer.next(_this.getSessionResponse);
                    observer.complete();
                });
            }
            else {
                observer.next(_this.getSessionResponse);
                observer.complete();
            }
            // }
        });
    };
    OTMMService.prototype.getLoggedInUserDetails = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.userDetailsUrl + userId, null)
                .subscribe(function (response) {
                if (response && response.user_resource && response.user_resource.user) {
                    _this.userDetailsObj.email = response.user_resource.user.email_address;
                    observer.next(response.user_resource.user);
                    observer.complete();
                }
                else {
                    observer.error('Something went wrong while getting user details from OTMM.');
                }
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.matchSessionAttributes = function (compareUserId, loginname) {
        var userId = this.sharingService.getCurrentUserID();
        var flag = false;
        if (this.utilService.isValid(userId) && this.utilService.isValid(compareUserId)) {
            if (this.utilService.isEqualWithCase(userId, compareUserId) || (compareUserId.indexOf('@') >= 0 && this.utilService.isEqualWithCase(userId, compareUserId.split('@')[0]))) {
                flag = true;
            }
        }
        if (this.utilService.isValid(userId) && this.utilService.isValid(loginname)) {
            if (this.utilService.isEqualWithCase(userId, loginname) || (loginname.indexOf('@') >= 0 && this.utilService.isEqualWithCase(userId, loginname.split('@')[0]))) {
                flag = true;
            }
        }
        return flag;
    };
    OTMMService.prototype.startOTMMSession = function (initialLogin) {
        var _this = this;
        var parameters = {
            organization: config.config.organizationName,
            // sourceResourceName: '__OTDS#Organizational#Platform#Resource__',
            sourceResourceName: this.entityAppDefService.applicationDetails.OTDS_APPWORKS_RESOURCE_NAME,
            targetResourceName: this.sharingService.getMediaManagerConfig().otdsResourceName,
            isOrgSpace: 'true'
        };
        return new Observable(function (observer) {
            _this.appService.getOTDSTicketForUser(parameters)
                .subscribe(function (otdsTicketResponse) {
                var ticket = 'OTDSTicket=';
                var token = otdsTicketResponse.tuple.old.OTDSAccessTicket.otmmTicket;
                if (token != null) {
                    ticket += token;
                    var otmmUrl = isDevMode() ? './' : _this.sharingService.getMediaManagerConfig().url;
                    otmmUrl = otmmUrl.concat('otmm/ux-html/index.html');
                    // if (initialLogin) {
                    _this.fireRequestforJsessionCookie(otmmUrl, ticket, otmmServicesConstants.CONTENT_TYPE.URLENCODED)
                        .subscribe(function (jSessionResponse) {
                        _this.fireGetRestRequest(otmmServicesConstants.otmmSessionUrl, null)
                            .subscribe(function (getSessionResponse) {
                            otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId =
                                _this.utilService.isValid(getSessionResponse) &&
                                    _this.utilService.isValid(getSessionResponse.session_resource) &&
                                    _this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                    _this.utilService.isValid(getSessionResponse.session_resource.session.id) ?
                                    getSessionResponse.session_resource.session.id : '';
                            var loginName = _this.utilService.isValid(getSessionResponse) &&
                                _this.utilService.isValid(getSessionResponse.session_resource) &&
                                _this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                _this.utilService.isValid(getSessionResponse.session_resource.session.login_name) ?
                                getSessionResponse.session_resource.session.login_name : '';
                            var userId = _this.utilService.isValid(getSessionResponse) &&
                                _this.utilService.isValid(getSessionResponse.session_resource) &&
                                _this.utilService.isValid(getSessionResponse.session_resource.session) &&
                                _this.utilService.isValid(getSessionResponse.session_resource.session.user_id) ?
                                getSessionResponse.session_resource.session.user_id : '';
                            _this.getLoggedInUserDetails(userId)
                                .subscribe(function (userResource) {
                                var attrsMatched = _this.matchSessionAttributes(userResource.otds_user_id, loginName);
                                if (!attrsMatched) {
                                    _this.otmmSessionExists = false;
                                    observer.next(_this.destroyOtmmSession().subscribe());
                                    observer.complete();
                                }
                                else {
                                    _this.otmmSessionExists = true;
                                    if (_this.sharingService.getMediaManagerConfig().enableQDS &&
                                        otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient && initialLogin) { //MVS-239 && initialLogin
                                        _this.qdsService.getQDSSession(otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId)
                                            .subscribe(function (qdsSessionResponse) {
                                            // tslint:disable-next-line: max-line-length
                                            _this.qdsService.connectToQDS(qdsSessionResponse.transferschemes_session_resource.transfer_session)
                                                .subscribe(function (response) {
                                                observer.next(response);
                                                observer.complete();
                                            }, function (error) {
                                                observer.next(error);
                                                observer.complete();
                                            });
                                        }, function (qdsSessionError) {
                                            // tslint:disable-next-line: max-line-length
                                            _this.notificationService.error('Something went wrong while getting QDS session');
                                            observer.next(_this.otmmSessionExists);
                                            observer.complete();
                                        });
                                        //observer.next(this.otmmSessionExists);
                                        //observer.complete();
                                    }
                                    else {
                                        observer.next(_this.otmmSessionExists);
                                        observer.complete();
                                    }
                                }
                            }, function (emailIdError) {
                                observer.error(emailIdError);
                            });
                        }, function (getSessionError) {
                            observer.error(getSessionError);
                        });
                    }, function (jSessionError) {
                        observer.error(jSessionError);
                    });
                    //  }
                }
            }, function (otdsTicketError) {
                observer.error(otdsTicketError);
            });
        });
    };
    OTMMService.prototype.destroyOtmmSession = function () {
        var _this = this;
        return new Observable(function (observer) {
            otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId = '';
            _this.fireDeleteRestRequest(otmmServicesConstants.otmmSessionUrl)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error('Error while deleting OTMM session.');
            });
        });
    };
    OTMMService.prototype.getLoginEmailId = function (userId) {
        var _this = this;
        return new Observable(function (observer) {
            var emailId = '';
            var req = _this.fireGetRestRequest(otmmServicesConstants.userDetailsUrl + userId, null);
            req.subscribe(function (response) {
                if (response && response.user_resource && response.user_resource.user &&
                    response.user_resource.user.email_address) {
                    emailId = response.user_resource.user.email_address;
                }
                observer.next(emailId);
                observer.complete();
            }, function (error) {
                observer.next(emailId);
                observer.complete();
            });
        });
    };
    OTMMService.prototype.getUploadManifest = function (files) {
        return this.createUploadManifest(files);
    };
    OTMMService.prototype.createUploadManifest = function (files) {
        var uploadManifest = null;
        if (files) {
            if (files.length === 1) {
                uploadManifest = {
                    upload_manifest: {
                        master_files: [{
                                file: {
                                    file_name: files[0].name,
                                    file_type: files[0].type
                                }
                            }]
                    }
                };
            }
            else {
                var masterFiles = new Array();
                for (var i = 0; i < files.length; i++) {
                    masterFiles.push({
                        file: {
                            file_name: files[i].name,
                            file_type: files[i].type
                        }
                    });
                }
                uploadManifest = {
                    upload_manifest: {
                        master_files: masterFiles
                    }
                };
            }
        }
        else {
            throw 'Unable to create upload manifest:\n parameter files is either undefined or invalid.';
        }
        return uploadManifest;
    };
    OTMMService.prototype.createUploadManifestForRevision = function (files) {
        var uploadManifest = null;
        if (files) {
            uploadManifest = {
                upload_manifest: {
                    master_files: [{
                            file: {
                                file_name: files[0].name
                            },
                            uoi_id: files[0].assetId
                        }]
                }
            };
        }
        else {
            var masterFiles = new Array();
            for (var i = 0; i < files.length; i++) {
                masterFiles.push({
                    file: {
                        file_name: files[i].name
                    },
                    uoi_id: files[i].assetId
                });
            }
            uploadManifest = {
                upload_manifest: {
                    master_files: masterFiles
                }
            };
        }
        return uploadManifest;
    };
    OTMMService.prototype.createTransferDetails = function (scheme, files) {
        var transferDetails = null;
        var transferFileItems = new Array();
        for (var i = 0; i < files.length; i++) {
            transferFileItems.push({
                scheme_file_id: '',
                scheme_file_name: ''
            });
        }
        transferDetails = {
            transfer_details_param: {
                transfer_file_items: transferFileItems,
                transfer_scheme: scheme
            }
        };
        return transferDetails;
    };
    OTMMService.prototype.getAssetRepresentation = function (metadataFields, metaDataFieldValues, metadataModel) {
        return this.createAssetRepresentation(metadataFields, metaDataFieldValues, metadataModel, false);
    };
    OTMMService.prototype.createAssetRepresentation = function (metadataFields, metaDataFieldValues, metaDataModel, securityPolicyID) {
        var assetRepresentation = null;
        var metadataFieldList = new Array();
        var metaDataElement = null;
        var securityPolicies = [];
        if (metadataFields && Array.isArray(metadataFields) && metaDataModel && metaDataModel != null && metaDataModel !== '') {
            var defaultSecPolicyId = 1;
            var securityPolicy = defaultSecPolicyId;
            if (securityPolicyID && securityPolicyID != null && securityPolicyID !== '') {
                var ids = securityPolicyID.split(',');
                for (var i = 0; i < ids.length; i++) {
                    var tempObj = {};
                    tempObj['id'] = ids[i];
                    securityPolicies.push(tempObj);
                }
            }
            else {
                var policy = {
                    id: defaultSecPolicyId,
                };
                securityPolicies.push(policy);
                policy = null;
            }
            // iterate all configured metadatafields and prepare metadata field configuration to send with request.
            for (var i = 0; i < metadataFields.length; i++) {
                metaDataElement = {};
                metaDataElement.id = metadataFields[i].id;
                var type = metadataFields[i].type;
                if (!type || type == null || type.trim() === '') {
                    type = 'com.artesia.metadata.MetadataField';
                }
                metaDataElement.type = type;
                // if metaDataFieldValues is defined and field from entity is of cascading type (eg: metadataFields[i].fieldRef = 'brand')
                if (metaDataFieldValues && metaDataFieldValues != null && metadataFields[i].fieldRef &&
                    metadataFields[i].fieldRef !== '' && metadataFields[i].fieldRef != null &&
                    metadataFields[i].fieldRef.indexOf('^') === -1 && metaDataFieldValues[metadataFields[i].fieldRef] !== '') {
                    var metadataValue = metaDataFieldValues[metadataFields[i].fieldRef];
                    // array of value inside values
                    if (type === 'com.artesia.metadata.MetadataTableField') {
                        var values = [];
                        if (Array.isArray(metadataValue) && metadataValue.length > 0) {
                            for (var index = 0; index < metadataValue.length; index++) {
                                if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string') {
                                    metadataValue[index] = metadataValue[index].toString();
                                }
                                if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number') {
                                    metadataValue[index] = metadataValue[index].parseInt();
                                }
                                var value = {
                                    value: {
                                        value: metadataValue[index],
                                        type: metadataFields[i].datatype
                                    }
                                };
                                values.push(value);
                            }
                        }
                        else {
                            if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string' &&
                                this.utilService.isValid(metadataValue)) {
                                metadataValue = metadataValue.toString();
                            }
                            else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number'
                                && this.utilService.isValid(metadataValue)) {
                                metadataValue = metadataValue.parseInt();
                            }
                            var value = {
                                value: {
                                    value: metadataValue,
                                    type: metadataFields[i].datatype
                                }
                            };
                            values.push(value);
                        }
                        metaDataElement.values = values;
                    }
                    else {
                        if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string' &&
                            this.utilService.isValid(metadataValue)) {
                            metadataValue = metadataValue.toString();
                        }
                        else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number'
                            && this.utilService.isValid(metadataValue)) {
                            metadataValue = metadataValue.parseInt();
                        }
                        else {
                        }
                        var value = {
                            value: {
                                value: metadataValue,
                                type: metadataFields[i].datatype
                            }
                        };
                        metaDataElement.value = value;
                    }
                }
                else if (metaDataFieldValues && metaDataFieldValues != null && metadataFields[i].fieldRef &&
                    metadataFields[i].fieldRef !== '' && metadataFields[i].fieldRef != null &&
                    metadataFields[i].fieldRef.indexOf('^') !== -1 && metaDataFieldValues[metadataFields[i].fieldRef] !== '') {
                    var fields = metadataFields[i].fieldRef.split('^');
                    var value = {
                        value: {
                            value: '',
                            type: metadataFields[i].datatype
                        }
                    };
                    // iterate all cascade fields
                    for (var x = 0; x < fields.length; x++) {
                        var metadatavalue = metaDataFieldValues[fields[x]];
                        // each cascaded field value only one value
                        if (Array.isArray(metadatavalue)) {
                            metadatavalue = metadatavalue[0];
                        }
                        // on metadatavalue is received
                        if (metadatavalue !== '' && metadatavalue != null && metadatavalue) {
                            // condition to check already value has appended for cascading field
                            if (value.value.value !== '' && value.value.value != null && value.value.value) {
                                value.value.value = value.value.value + '^' + metadatavalue;
                            }
                            else {
                                value.value.value = metadatavalue;
                            }
                        }
                    }
                }
                else {
                    var defvalue = void 0;
                    // if default value is present
                    if (metadataFields[i].default !== '' && metadataFields[i].default != null && metadataFields[i].default) {
                        defvalue = metadataFields[i].default;
                    }
                    else {
                        if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'string') {
                            defvalue = 'NA';
                        }
                        else if (this.utilService.isValid(metadataFields[i].datatype) && metadataFields[i].datatype === 'number') {
                            defvalue = 0;
                        }
                        else {
                        }
                    }
                    var value = {
                        value: {
                            value: defvalue,
                            type: metadataFields[i].datatype
                        }
                    };
                    metaDataElement.value = value;
                }
                metadataFieldList.push(metaDataElement);
            }
            var metadataModelId = '';
            if (metaDataModel && metaDataModel != null && metaDataModel.trim() !== '') {
                metadataModelId = metaDataModel;
            }
            else {
                throw 'MetaData ModelID is missing.';
            }
            assetRepresentation = {
                asset_resource: {
                    asset: {
                        metadata: {
                            metadata_element_list: metadataFieldList
                        },
                        metadata_model_id: metadataModelId,
                        security_policy_list: securityPolicies
                    }
                }
            };
        }
        else {
            throw 'Unable to create asset representation'
                + ' the parameters metadataFields & metaDataFieldValues are not correct or undefined.';
        }
        return assetRepresentation;
    };
    OTMMService.prototype.uploadAssets = function (files, metadataFields, metadataFieldValues, parentForlderID, importTemplateID, metadataModel, securityPolicyID) {
        var e_2, _a;
        var url = this.formRestUrl(otmmServicesConstants.asssetUrl, null, false);
        var param = new FormData();
        var blob = null;
        var content = null;
        if (files && files != null && metadataFieldValues && metadataFieldValues != null && metadataFields && parentForlderID &&
            importTemplateID) {
            content = JSON.stringify(this.createAssetRepresentation(metadataFields, metadataFieldValues, metadataModel, securityPolicyID));
            blob = new Blob([content], {
                type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
            });
            param.append('asset_representation', blob);
            param.append('parent_folder_id', parentForlderID);
            param.append('import_template_id', importTemplateID);
            param.append('no_content', 'false');
            content = JSON.stringify(this.createUploadManifest(files));
            blob = new Blob([content], {
                type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
            });
            param.append('manifest', blob);
            try {
                for (var files_1 = __values(files), files_1_1 = files_1.next(); !files_1_1.done; files_1_1 = files_1.next()) {
                    var file = files_1_1.value;
                    param.append('files', file);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (files_1_1 && !files_1_1.done && (_a = files_1.return)) _a.call(files_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        else {
            throw 'Unable to upload assets:\nbecause the Files are meta data values are not passed.';
            return null;
        }
    };
    OTMMService.prototype.formAssetRepresentation = function (metadata, metaDataModel, securityPolicyID) {
        var e_3, _a, e_4, _b, e_5, _c;
        var assetRepresentation = null;
        var metadataElementList = new Array();
        var securityPolicies = [];
        if (metadata && metaDataModel && metaDataModel != null && metaDataModel !== '') {
            var defaultSecPolicyId = 1;
            if (securityPolicyID && securityPolicyID != null && securityPolicyID !== '') {
                var ids = securityPolicyID.split(';');
                try {
                    for (var ids_1 = __values(ids), ids_1_1 = ids_1.next(); !ids_1_1.done; ids_1_1 = ids_1.next()) {
                        var id = ids_1_1.value;
                        var tempObj = {};
                        tempObj['id'] = Number(id);
                        securityPolicies.push(tempObj);
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (ids_1_1 && !ids_1_1.done && (_a = ids_1.return)) _a.call(ids_1);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }
            else {
                var policy = {
                    id: defaultSecPolicyId,
                };
                securityPolicies.push(policy);
                policy = null;
            }
            var metadataModelId = '';
            if (metaDataModel && metaDataModel != null && metaDataModel.trim() !== '') {
                metadataModelId = metaDataModel;
            }
            else {
                throw new Error('Metadata ModelID is missing.');
            }
            if (metadata && metadata.metadata_element_list && Array.isArray(metadata.metadata_element_list)) {
                try {
                    for (var _d = __values(metadata.metadata_element_list), _e = _d.next(); !_e.done; _e = _d.next()) {
                        var metadataGroup = _e.value;
                        try {
                            for (var _f = (e_5 = void 0, __values(metadataGroup.metadata_element_list)), _g = _f.next(); !_g.done; _g = _f.next()) {
                                var metadataElement = _g.value;
                                if (metadataElement && metadataElement.id === 'MPM.UTILS.DATA_TYPE' && metadataElement.value && metadataElement.value.value) {
                                    metadataElement.value.value.value = OTMMMPMDataTypes.ASSET;
                                }
                                metadataElementList.push(metadataElement);
                            }
                        }
                        catch (e_5_1) { e_5 = { error: e_5_1 }; }
                        finally {
                            try {
                                if (_g && !_g.done && (_c = _f.return)) _c.call(_f);
                            }
                            finally { if (e_5) throw e_5.error; }
                        }
                    }
                }
                catch (e_4_1) { e_4 = { error: e_4_1 }; }
                finally {
                    try {
                        if (_e && !_e.done && (_b = _d.return)) _b.call(_d);
                    }
                    finally { if (e_4) throw e_4.error; }
                }
            }
            assetRepresentation = {
                asset_resource: {
                    asset: {
                        metadata: {
                            metadata_element_list: metadataElementList
                        },
                        metadata_model_id: metadataModelId,
                        security_policy_list: securityPolicies
                    }
                }
            };
        }
        else {
            console.error("Unable to create asset representation:\n            the parameters metadataFields & metaDataFieldValues are not correct or undefined.");
        }
        return assetRepresentation;
    };
    OTMMService.prototype.uploadSingleAsset = function (files, metaDataFieldValues, parentFolderID, importTemplateID, metadataModel, securityPolicy) {
        var e_6, _a;
        var url = this.formRestUrl(otmmServicesConstants.asssetUrl, null, false);
        var param = new FormData();
        var blob = null;
        var content = null;
        if ((files && Array.isArray(files)) && metaDataFieldValues && parentFolderID && importTemplateID) {
            try {
                for (var files_2 = __values(files), files_2_1 = files_2.next(); !files_2_1.done; files_2_1 = files_2.next()) {
                    var file = files_2_1.value;
                    content = JSON.stringify(this.formAssetRepresentation(metaDataFieldValues, metadataModel, securityPolicy));
                    blob = new Blob([content], {
                        type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                    });
                    param.append('asset_representation', blob);
                    param.append('parent_folder_id', parentFolderID);
                    param.append('import_template_id', importTemplateID);
                    param.append('no_content', 'false');
                    content = JSON.stringify(this.createUploadManifest(files));
                    blob = new Blob([content], {
                        type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                    });
                    param.append('manifest', blob);
                    param.append('files', file);
                }
            }
            catch (e_6_1) { e_6 = { error: e_6_1 }; }
            finally {
                try {
                    if (files_2_1 && !files_2_1.done && (_a = files_2.return)) _a.call(files_2);
                }
                finally { if (e_6) throw e_6.error; }
            }
            return this.doPostRequest(url, param, null, false);
        }
        else {
            console.error('Unable to upload assets:\nbecause the parameters are either incorrect or undefined.');
            return null;
        }
    };
    OTMMService.prototype.createOTMMJob = function (fileName) {
        var _this = this;
        return new Observable(function (observer) {
            var baseUrl = isDevMode() ? './' : _this.sharingService.getMediaManagerConfig().url;
            var version = _this.sharingService.getMediaManagerConfig().apiVersion;
            var url = baseUrl + 'otmmapi/' + version + '/jobs/imports';
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.MULTIPARTFORMDATA,
                    'X-OTMM-Request': 'true',
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    'X-USES-CHUNKS': 'false'
                })
            };
            var formData = new FormData();
            formData.append('file_name', fileName);
            _this.http.post(url, formData, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.importOTMMJob = function (files, metadataFields, metaDataFieldValues, parentForlderID, importTemplateID, metadataModel, securityPolicy, importJobId, isRevision) {
        var url = this.formRestUrl((isRevision ? otmmServicesConstants.checkinUrl : otmmServicesConstants.importJobUrl) + '/' + importJobId, null, false);
        var param = new FormData();
        var blob = null;
        var content = null;
        if ((files && Array.isArray(files)) && metaDataFieldValues &&
            parentForlderID && importTemplateID) {
            for (var i = 0; i < files.length; i++) {
                content = JSON.stringify(this.formAssetRepresentation(metaDataFieldValues, metadataModel, securityPolicy));
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                if (!isRevision) {
                    param.append('asset_representation', blob);
                    param.append('parent_folder_id', parentForlderID);
                }
                param.append('import_job_id', importJobId);
                param.append('file_name', files[i].name);
                if (isRevision) {
                    content = JSON.stringify(this.createUploadManifestForRevision(files));
                }
                else {
                    content = JSON.stringify(this.createUploadManifest(files));
                }
                blob = new Blob([content], {
                    type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                });
                param.append('manifest', blob);
                if (this.sharingService.getMediaManagerConfig().enableQDS && otmmServicesConstants.OTMM_SERVICE_VARIABLES.findQDSClient) {
                    content = JSON.stringify(this.createTransferDetails('QDS', files));
                    blob = new Blob([content], {
                        type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
                    });
                    param.append('transfer_details', blob);
                }
                else {
                    // content = JSON.stringify(this.createUploadManifest(files));
                    param.append('files', files[i]);
                }
            }
            return this.doPutRequest(url, param, null, false);
        }
        else {
            console.error('Unable to upload assets:\nbecause the parameters are either incorrect or undefined.');
            return null;
        }
    };
    OTMMService.prototype.validateAssetLockState = function (assetList) {
        var fileName = '';
        var fileCounter = 0;
        for (var k = 0; k < assetList.length; k++) {
            if (assetList[k].asset_state === 'LOCKED' &&
                assetList[k].metadata_lock_state_user_name !== this.sharingService.getCurrentUserID()) {
                fileCounter++;
                if (k > 0) {
                    fileName += ',';
                }
                fileName += fileCounter + ')' + assetList[k].name + ' - Locked By : ' + assetList[k].metadata_lock_state_user_name;
                fileName += '\n';
            }
        }
        if (fileName.length > 0) {
            this.notificationService.info("Following file(s) are locked by different user and cannot proceed with revision upload,\n             Kindly contact the respective users or administrator to remove lock on the file(s) \n" + fileName);
            return false;
        }
        else {
            return true;
        }
    };
    OTMMService.prototype.assetCheckout = function (assetIds) {
        var _this = this;
        return new Observable(function (observer) {
            if (!assetIds || !assetIds.length || assetIds.length === 0) {
                _this.notificationService.info('No asset ids are available to check out');
                observer.next(false);
                observer.complete();
            }
            var urlParam = [{
                    key: 'action',
                    value: 'check_out'
                }, {
                    key: 'json',
                    value: {
                        key: 'selection_context',
                        value: {
                            selection_context_param: {
                                selection_context: {
                                    asset_ids: assetIds,
                                    type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                                    include_descendants: 'IMMEDIATE'
                                }
                            }
                        }
                    }
                }];
            var url = _this.formRestUrl('assets/state', null, false);
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED
                }),
                transformRequest: 'xWwwFormUrlencoded'
            };
            _this.doPutRequest(url, _this.formRestParams(urlParam).substring(1), httpOptions, false)
                .subscribe(function (response) {
                if (response && response['bulk_asset_result_representation'] &&
                    response['bulk_asset_result_representation'].bulk_asset_result &&
                    response['bulk_asset_result_representation'].bulk_asset_result) {
                    if (response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list &&
                        response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length &&
                        response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length > 0) {
                        observer.error(new Error('Unable to check out ' +
                            response['bulk_asset_result_representation'].bulk_asset_result.failed_object_list.length + ' assets.'));
                    }
                    else if (response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list &&
                        response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length &&
                        response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length > 0 &&
                        // tslint:disable-next-line: max-line-length
                        assetIds.length === response['bulk_asset_result_representation'].bulk_asset_result.successful_object_list.length) {
                        observer.next(response);
                        observer.complete();
                    }
                    else {
                        observer.error(new Error('Unable to check out ' + assetIds.length + ' assets.'));
                    }
                }
                else {
                    observer.error(new Error('Invalid Asset Ids.'));
                }
            });
        });
    };
    OTMMService.prototype.lockAssets = function (assetIds) {
        var urlParam = [{
                key: 'action',
                value: 'lock'
            }, {
                key: 'json',
                value: {
                    key: 'selection_context',
                    value: {
                        selection_context_param: {
                            selection_context: {
                                asset_ids: assetIds,
                                type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                                include_descendants: 'NONE'
                            }
                        }
                    }
                }
            }];
        var url = this.formRestUrl('assets/state', null, false);
        return this.doPutRequest(url, this.formRestParams(urlParam).substring(1), {
            withCredentials: true,
            headers: new HttpHeaders({
                'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED
            })
        }, false);
    };
    OTMMService.prototype.assetImport = function (fileName) {
        var url = this.formRestUrl(otmmServicesConstants.importJobUrl, null, false);
        var param;
        if (fileName) {
            param = this.formRestParams([{
                    key: 'file_name',
                    value: fileName
                }
            ]);
        }
        return this.doPostRequest(url, param, false, false);
    };
    OTMMService.prototype.assetRendition = function (file, importJobID) {
        var url = this.formRestUrl(otmmServicesConstants.renditionsUrl, null, false);
        var param = new FormData();
        param.append('import_job_id', importJobID);
        param.append('file', file);
        param.append('file_name', file.name);
        return this.doPostRequest(url, param, false, false);
    };
    OTMMService.prototype.assetsRendition = function (files, importJobID) {
        var e_7, _a;
        var promises = [];
        try {
            for (var files_3 = __values(files), files_3_1 = files_3.next(); !files_3_1.done; files_3_1 = files_3.next()) {
                var file = files_3_1.value;
                promises.push(this.assetRendition(file, importJobID));
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (files_3_1 && !files_3_1.done && (_a = files_3.return)) _a.call(files_3);
            }
            finally { if (e_7) throw e_7.error; }
        }
        return new Observable(function (observer) {
            forkJoin(promises).subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.assetCheckIn = function (assetDetail, importJobID) {
        var url = this.formRestUrl(otmmServicesConstants.importJobUrl + '/' + importJobID, null, false);
        var param = new FormData();
        var blob = null;
        var content = null;
        param.append('import_job_id', importJobID);
        var uploadManifest = null;
        uploadManifest = {
            upload_manifest: {
                master_files: [{
                        file: {
                            file_name: assetDetail.name
                        },
                        uoi_id: assetDetail.assetId
                    }]
            }
        };
        param.append('file_name', assetDetail.name);
        content = uploadManifest;
        blob = new Blob([JSON.stringify(content)], {
            type: otmmServicesConstants.CONTENT_TYPE.JSONDATA
        });
        param.append('manifest', blob);
        return this.doPutRequest(url, param, false, false);
    };
    OTMMService.prototype.getFilePaths = function (files) {
        var filePaths = [];
        for (var i = 0; i < files.length; i++) {
            if (files[i].path) {
                filePaths.push(files[i].path);
            }
        }
        return filePaths;
    };
    OTMMService.prototype.formDownloadName = function (downloadType) {
        console.log('Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + (downloadType ? '.' + downloadType : '.zip'));
        return 'Download_' + new Date().toJSON().replace(/[-:]/g, '').split('.')[0] + (downloadType ? '.' + downloadType : '.zip');
    };
    OTMMService.prototype.createExportJob = function (assetIds, downloadType) {
        var _this = this;
        return new Observable(function (observer) {
            var baseUrl = isDevMode() ? './' : _this.sharingService.getMediaManagerConfig().url;
            var version = _this.sharingService.getMediaManagerConfig().apiVersion;
            var url = baseUrl + 'otmmapi/' + version + '/jobs/exports';
            var httpOptions = {
                withCredentials: true,
                headers: new HttpHeaders({
                    'Content-Type': otmmServicesConstants.CONTENT_TYPE.URLENCODED,
                    'X-Requested-By': otmmServicesConstants.OTMM_SERVICE_VARIABLES.userSessionId.toString(),
                })
            };
            var exportRequest;
            if (downloadType.toUpperCase() == FileTypes.ZIP) {
                exportRequest = {
                    export_request_param: {
                        export_request: {
                            content_request: {
                                export_preview_content: false,
                                export_master_content: true,
                                export_supporting_content: false
                            },
                            create_collection_folders: false,
                            delivery_template: {
                                attribute_values: [
                                    {
                                        argument_number: 1,
                                        value: '2'
                                    },
                                    {
                                        argument_number: 4,
                                        value: 'N'
                                    }
                                ],
                                id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD.DEFAULT',
                                transformer_id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD'
                            },
                            error_handling_action: 'DEFAULT_HANDLING',
                            export_job_transformers: [
                                {
                                    transformer_id: 'ARTESIA.TRANSFORMER.ZIP COMPRESSION.DEFAULT',
                                    arguments: [
                                        null,
                                        _this.formDownloadName()
                                    ]
                                }
                            ],
                            process_asynchronously: true,
                            remove_working_directory: true,
                            replace_export_dir: true,
                            write_xml: true
                        }
                    }
                };
            }
            else {
                exportRequest = {
                    export_request_param: {
                        export_request: {
                            content_request: {
                                export_master_content: true,
                                export_supporting_content: false
                            },
                            create_collection_folders: false,
                            delivery_template: {
                                attribute_values: [
                                    {
                                        argument_number: 1,
                                        value: '2'
                                    },
                                ],
                                id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD.DEFAULT',
                                transformer_id: 'ARTESIA.TRANSFORMER.PROFILE.DOWNLOAD'
                            },
                            error_handling_action: 'DEFAULT_HANDLING',
                            process_asynchronously: true,
                            remove_working_directory: true,
                            replace_export_dir: true,
                            write_xml: false
                        }
                    }
                };
            }
            var selectionContext = {
                selection_context_param: {
                    selection_context: {
                        asset_ids: assetIds,
                        type: 'com.artesia.asset.selection.AssetIdsSelectionContext',
                        include_descendants: 'ALL',
                        child_type: 'ASSETS'
                    }
                }
            };
            var params = 'export_request=' + JSON.stringify(exportRequest) +
                '&selection_context=' + encodeURIComponent(JSON.stringify(selectionContext));
            _this.http.post(url, params, httpOptions)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getExportJobs = function (params) {
        var _this = this;
        return new Observable(function (observer) {
            var url = 'jobs'; // MSV-239 -> 'jobs/'
            _this.fireGetRestRequest(url, params)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getOTMMSystemDetails = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS) != null) {
                var otmmVersion = JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS));
                if (otmmVersion == null || otmmVersion == undefined || parseFloat(otmmVersion) < 0) {
                    JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS)).system_details_resource.system_details_map.entry.forEach(function (e) {
                        if (e.key.toString() === ApplicationConfigConstants.OTMM_SYSTEM_DETAILS_CONSTANTS.BUILD_VERSION)
                            sessionStorage.setItem(SessionStorageConstants.OTMM_VERSION, JSON.stringify(e.value));
                    });
                }
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.SYSTEM_DETAILS)));
                observer.complete();
            }
            else {
                _this.fireGetRestRequest(otmmServicesConstants.systemdetailsUrl, null)
                    .subscribe(function (response) {
                    if (response != null) {
                        response.system_details_resource.system_details_map.entry.forEach(function (e) {
                            if (e.key.toString() === ApplicationConfigConstants.OTMM_SYSTEM_DETAILS_CONSTANTS.BUILD_VERSION)
                                sessionStorage.setItem(SessionStorageConstants.OTMM_VERSION, JSON.stringify(e.value));
                        });
                        sessionStorage.setItem(SessionStorageConstants.SYSTEM_DETAILS, JSON.stringify(response));
                        observer.next(response);
                        observer.complete();
                    }
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    OTMMService.prototype.getOTMMAssetById = function (assetId, loadType) {
        var _this = this;
        var assetDetails = '';
        var dataLoadRequest = {
            data_load_request: {
                load_multilingual_field_values: true,
                load_subscribed_to: true,
                load_asset_content_info: true,
                load_metadata: true,
                load_inherited_metadata: true,
                load_thumbnail_info: true,
                load_preview_info: true,
                load_pdf_preview_info: true,
                load_3d_preview_info: true,
                load_destination_links: true,
                load_security_policies: true,
                load_path: true
            }
        };
        var urlParam = [{
                key: 'load_type',
                value: loadType
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        if (loadType === 'custom') {
            urlParam.push({
                key: 'data_load_request',
                value: encodeURIComponent(JSON.stringify(dataLoadRequest))
            });
        }
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId, urlParam)
                .subscribe(function (response) {
                if (response && response.asset_resource.asset) {
                    assetDetails = response.asset_resource.asset;
                }
                observer.next(assetDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getOTMMFodlerById = function (folderId) {
        var _this = this;
        var folderDetails = '';
        var urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'load_multilingual_values',
                value: 'true'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }, {
                key: 'after',
                value: 'after'
            }, {
                key: 'limit',
                value: '50'
            }];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.foldersUrl + folderId, urlParam)
                .subscribe(function (response) {
                if (response && response.folder_resource && response.folder_resource.folder) {
                    folderDetails = response.folder_resource.folder;
                }
                observer.next(folderDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getSearchOperatorsList = function () {
        var _this = this;
        var searchOperatorList = '';
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.searchoperators, null)
                .subscribe(function (response) {
                if (response && response.search_operators_resource && response.search_operators_resource.search_operator_list) {
                    searchOperatorList = response.search_operators_resource.search_operator_list;
                }
                observer.next(searchOperatorList);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getAllMetadatModelList = function () {
        var _this = this;
        var metaDataModelList = '';
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.metadatamodels, null)
                .subscribe(function (response) {
                if (response) {
                    metaDataModelList = response.metadata_models_resource;
                }
                observer.next(metaDataModelList);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getMetadatModelById = function (modelId) {
        var _this = this;
        var metaDataModelDetails = '';
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.metadatamodels + modelId, null)
                .subscribe(function (response) {
                if (response) {
                    metaDataModelDetails = response.metadata_model_resource.metadata_model;
                }
                observer.next(metaDataModelDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.saveSearch = function (searchConfig, skip, top, userSessionId) {
        var url = otmmServicesConstants.savedSearchUrl;
        /* if (searchConfig.savedSearchId) {
            url = otmmServicesConstants.savedsearches + searchConfig.savedSearchId
        } */
        var formData = [];
        if (searchConfig.searchKey) {
            formData.push(searchConfig.searchKey);
        }
        if (searchConfig.sortString) {
            formData.push('sort=' + searchConfig.sortString);
        }
        if (searchConfig.keyword_query) {
            formData.push('keyword_query=' + encodeURIComponent(searchConfig.keyword_query));
        }
        if (searchConfig.load_type === 'custom') {
            formData.push('data_load_request=' + '{"data_load_request":{"child_count_load_type":' + '"' + searchConfig.child_count_load_type + '"' + ' }}');
        }
        formData.push('after=' + skip);
        formData.push('limit=' + top);
        formData.push('multilingual_language_code=en_US');
        if (searchConfig.folder_filter) {
            formData.push('folder_filter=' + searchConfig.folder_filter);
        }
        if (searchConfig.folder_filter_type) {
            formData.push('folder_filter_type=' + searchConfig.folder_filter_type);
        }
        if (searchConfig.facet_restriction_list) {
            formData.push('facet_restriction_list=' + encodeURIComponent(JSON.stringify(searchConfig.facet_restriction_list)));
        }
        if (searchConfig.search_config_id) {
            formData.push('search_config_id=' + searchConfig.search_config_id);
        }
        if (typeof searchConfig.is_favorite === 'boolean') {
            formData.push('is_favorite=' + searchConfig.is_favorite);
        }
        if (searchConfig.name) {
            formData.push('name=' + encodeURIComponent(searchConfig.name));
        }
        if (searchConfig.description) {
            formData.push('description=' + encodeURIComponent(searchConfig.description));
        }
        if (typeof searchConfig.is_public === 'boolean') {
            formData.push('is_public=' + searchConfig.is_public);
        }
        if (searchConfig.preference_id) {
            formData.push('preference_id=' + (searchConfig.preference_id ? searchConfig.preference_id : 'ARTESIA.PREFERENCE.GALLERYVIEW.DISPLAYED_FIELDS'));
        }
        if (searchConfig.metadata_to_return) {
            formData.push('metadata_to_return=' + searchConfig.metadata_to_return);
        }
        if (searchConfig.search_condition_list) {
            formData.push('search_condition_list=' + encodeURIComponent(JSON.stringify(searchConfig.search_condition_list)));
        }
        if (searchConfig.savedSearchId) {
            formData.push('id=' + searchConfig.savedSearchId);
        }
        var param = formData.join('&');
        var httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        /* if (searchConfig.savedSearchId) {
            return this.firePatchRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
        } */
        return this.firePostRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
    };
    OTMMService.prototype.searchCustomText = function (searchConfig, skip, top, userSessionId) {
        var url = otmmServicesConstants.textAssetSearchUrl;
        if (searchConfig.isSavedSearch) {
            url = otmmServicesConstants.savedSearchUrl;
        }
        var formData = [];
        if (searchConfig.searchKey) {
            formData.push(searchConfig.searchKey);
        }
        if (searchConfig.sortString) {
            formData.push('sort=' + searchConfig.sortString);
        }
        if (searchConfig.keyword_query) {
            formData.push('keyword_query=' + encodeURIComponent(searchConfig.keyword_query));
        }
        formData.push('load_type=' + (searchConfig.load_type ? searchConfig.load_type : 'metadata'));
        formData.push('load_multilingual_values=true');
        formData.push('level_of_detail=' + (searchConfig.level_of_detail ? searchConfig.level_of_detail : 'slim'));
        if (searchConfig.load_type === 'custom') {
            formData.push('data_load_request=' + '{"data_load_request":{"child_count_load_type":' + '"' + searchConfig.child_count_load_type + '"' + ' }}');
        }
        formData.push('after=' + skip);
        formData.push('limit=' + top);
        formData.push('multilingual_language_code=en_US');
        if (searchConfig.folder_filter) {
            formData.push('folder_filter=' + searchConfig.folder_filter);
        }
        if (searchConfig.folder_filter_type) {
            formData.push('folder_filter_type=' + searchConfig.folder_filter_type);
        }
        if (searchConfig.facet_restriction_list) {
            formData.push('facet_restriction_list=' + encodeURIComponent(JSON.stringify(searchConfig.facet_restriction_list)));
        }
        if (searchConfig.search_config_id) {
            formData.push('search_config_id=' + searchConfig.search_config_id);
        }
        if (searchConfig.is_favorite) {
            formData.push('is_favorite=' + searchConfig.is_favorite);
        }
        if (searchConfig.name) {
            formData.push('name=' + encodeURIComponent(searchConfig.name));
        }
        if (searchConfig.is_public) {
            formData.push('is_public=' + searchConfig.is_public);
        }
        if (searchConfig.preference_id) {
            formData.push('preference_id=' + (searchConfig.preference_id ? searchConfig.preference_id : 'ARTESIA.PREFERENCE.GALLERYVIEW.DISPLAYED_FIELDS'));
        }
        if (searchConfig.metadata_to_return) {
            formData.push('metadata_to_return=' + searchConfig.metadata_to_return);
        }
        if (searchConfig.search_condition_list) {
            formData.push('search_condition_list=' + encodeURIComponent(JSON.stringify(searchConfig.search_condition_list)));
        }
        var param = formData.join('&');
        var httpOptions = {
            headers: new HttpHeaders().set('X-Requested-By', userSessionId.toString())
                .set('Content-Type', 'application/x-www-form-urlencoded'),
            withCredentials: true
        };
        return this.firePostRestRequest(url, param, 'application/x-www-form-urlencoded', httpOptions);
    };
    OTMMService.prototype.updateMetadata = function (assetId, type, metadataParameters) {
        var _this = this;
        return new Observable(function (observer) {
            var contentType = 'application/x-www-form-urlencoded';
            var lockFormData = [];
            lockFormData.push('action=lock');
            var param = lockFormData.join('&');
            _this.firePutRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/state', param, null, null)
                .subscribe(function (lockResponse) {
                _this.firePatchRestRequest(((type === 'FOLDER' ? otmmServicesConstants.foldersUrl : (otmmServicesConstants.asssetUrl + '/')) + assetId), metadataParameters, null, null)
                    .subscribe(function (response) {
                    var unlockFormData = [];
                    unlockFormData.push('action=unlock');
                    var parameter = unlockFormData.join('&');
                    _this.firePutRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/state', parameter, null, null)
                        .subscribe(function (unlockResponse) {
                        observer.next(unlockResponse);
                        observer.complete();
                    }, function (error) {
                        observer.error(error);
                    });
                });
            });
        });
    };
    OTMMService.prototype.getAllSavedSearches = function () {
        var _this = this;
        var savedsearches = [];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.savedsearches, null)
                .subscribe(function (response) {
                if (response && response.saved_searches_resource && response.saved_searches_resource.saved_search_list) {
                    savedsearches = response.saved_searches_resource.saved_search_list;
                }
                observer.next(savedsearches);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getSavedSearchById = function (id) {
        var _this = this;
        var savedsearch = {};
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.savedsearches + id, null)
                .subscribe(function (response) {
                if (response && response.saved_search_resource && response.saved_search_resource.saved_search) {
                    savedsearch = response.saved_search_resource.saved_search;
                }
                observer.next(savedsearch);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.deleteSavedSearch = function (id) {
        var _this = this;
        return new Observable(function (observer) {
            _this.fireDeleteRestRequest(otmmServicesConstants.savedsearches + id, null)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.addRelationalOperator = function (searchConditions) {
        searchConditions.forEach(function (searchCondition) {
            if (searchCondition.relational_operator && searchCondition.relational_operator !== '') {
                return;
            }
            searchCondition.relational_operator = 'and';
        });
        return searchConditions;
    };
    OTMMService.prototype.getAssetVersionsByAssetId = function (assetId) {
        var _this = this;
        var assetVersionDetails = '';
        var urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId + '/versions', urlParam)
                .subscribe(function (response) {
                if (response && response.assets_resource && response.assets_resource.asset_list) {
                    assetVersionDetails = response.assets_resource.asset_list;
                }
                observer.next(assetVersionDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getAssetByAssetId = function (assetId) {
        var _this = this;
        var assetVersionDetails = '';
        var urlParam = [{
                key: 'load_type',
                value: 'metadata'
            }, {
                key: 'level_of_detail',
                value: 'slim'
            }];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.asssetUrl + '/' + assetId, urlParam)
                .subscribe(function (response) {
                if (response && response.asset_resource) {
                    assetVersionDetails = response.asset_resource.asset;
                }
                observer.next(assetVersionDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getsearchconfigurations = function () {
        var _this = this;
        return new Observable(function (observer) {
            var urlParam = [{
                    key: 'retrieval_type',
                    value: 'full'
                }];
            _this.fireGetRestRequest(otmmServicesConstants.searchconfigurations, urlParam)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getAllLookupDomains = function () {
        var _this = this;
        var lookupDomains = [];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.lookupdomains, null)
                .subscribe(function (response) {
                if (response && response.lookup_domains_resource && response.lookup_domains_resource.lookup_domains) {
                    lookupDomains = response.lookup_domains_resource.lookup_domains;
                }
                observer.next(lookupDomains);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getLookupDomain = function (lookupDomainId) {
        var _this = this;
        var lookupDomainValues = [];
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.lookupdomains + lookupDomainId, null)
                .subscribe(function (response) {
                if (response && response.lookup_domain_resource && response.lookup_domain_resource.lookup_domain &&
                    response.lookup_domain_resource.lookup_domain.domainValues) {
                    lookupDomainValues = response.lookup_domain_resource.lookup_domain.domainValues;
                }
                observer.next(lookupDomainValues);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    OTMMService.prototype.getFacetConfigurations = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.fireGetRestRequest(otmmServicesConstants.facetconfigurations, null).subscribe(function (response) {
                var facetConfigurations = [];
                if (response && response.facet_configurations_resource &&
                    Array(response.facet_configurations_resource.facet_configuration_list)) {
                    facetConfigurations = response.facet_configurations_resource.facet_configuration_list;
                }
                observer.next(facetConfigurations);
                observer.complete();
            }, function (error) {
                _this.notificationService.error('Error while getting facet configurations');
                observer.error(error);
            });
        });
    };
    OTMMService.ctorParameters = function () { return [
        { type: EntityAppDefService },
        { type: AppService },
        { type: UtilService },
        { type: SharingService },
        { type: NotificationService },
        { type: HttpClient },
        { type: QdsService }
    ]; };
    OTMMService.ɵprov = i0.ɵɵdefineInjectable({ factory: function OTMMService_Factory() { return new OTMMService(i0.ɵɵinject(i1.EntityAppDefService), i0.ɵɵinject(i2.AppService), i0.ɵɵinject(i3.UtilService), i0.ɵɵinject(i4.SharingService), i0.ɵɵinject(i5.NotificationService), i0.ɵɵinject(i6.HttpClient), i0.ɵɵinject(i7.QdsService)); }, token: OTMMService, providedIn: "root" });
    OTMMService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], OTMMService);
    return OTMMService;
}());
export { OTMMService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3RtbS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL290bW0uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzNFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDL0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUMzRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxxREFBcUQsQ0FBQzs7Ozs7Ozs7O0FBTWpHO0lBRUkscUJBQ1csbUJBQXdDLEVBQ3hDLFVBQXNCLEVBQ3RCLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLG1CQUF3QyxFQUN4QyxJQUFnQixFQUNoQixVQUFzQjtRQU50Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBR2pDLG1CQUFjLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMxQixrQkFBYSxHQUFHLENBQUMsQ0FBQztRQUVsQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsc0NBQWlDLEdBQUcsTUFBTSxDQUFDO1FBRTNDLG1CQUFjLEdBQUc7WUFDYixLQUFLLEVBQUUsRUFBRTtTQUNaLENBQUM7SUFiRSxDQUFDO0lBa0JMLHlDQUFtQixHQUFuQixVQUFvQixHQUFHLEVBQUUsVUFBVTtRQUMvQixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELGdDQUFVLEdBQVYsVUFBVyxTQUFTO1FBQ2hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7SUFDdEMsQ0FBQztJQUVELGdDQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUNqQyxDQUFDO0lBRUQsb0NBQWMsR0FBZDtRQUNJLE9BQU8scUJBQXFCLENBQUMsc0JBQXNCLENBQUMsV0FBVyxDQUFDO0lBQ3BFLENBQUM7SUFFRCx3Q0FBa0IsR0FBbEI7UUFDSSxPQUFPLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDO0lBQzlFLENBQUM7SUFFRCwrQ0FBeUIsR0FBekI7UUFDSSxPQUFPLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGNBQWMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsd0NBQWtCLEdBQWxCLFVBQW1CLEdBQUcsRUFBRSxVQUFVO1FBQWxDLGlCQWdCQztRQWZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sV0FBVyxHQUFHO2dCQUNoQixlQUFlLEVBQUUsSUFBSTtnQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO29CQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO2lCQUMxRixDQUFDO2FBQ0wsQ0FBQztZQUNGLEtBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxFQUFFLFdBQVcsQ0FBQztpQkFDeEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQW1CLEdBQW5CLFVBQW9CLEdBQUcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFlBQVk7UUFBOUQsaUJBZ0JDO1FBZkcsSUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixjQUFjLEVBQUUsV0FBVzthQUM5QixDQUFDO1NBQ0wsQ0FBQztRQUVGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsRUFBRSxVQUFVLEVBQUUsWUFBWSxDQUFDO2lCQUN2RSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBNEIsR0FBNUIsVUFBNkIsR0FBRyxFQUFFLFVBQVUsRUFBRSxXQUFXO1FBQXpELGlCQW1CQztRQWxCRyxJQUFNLFdBQVcsR0FBRztZQUNoQixlQUFlLEVBQUUsSUFBSTtZQUNyQixZQUFZLEVBQUUsTUFBZ0I7WUFDOUIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUNyQixjQUFjLEVBQUUsV0FBVzthQUM5QixDQUFDO1NBQ0wsQ0FBQztRQUVGLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsV0FBVyxDQUFDO2lCQUN2QyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3JCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFzQixHQUF0QixVQUF1QixVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVc7UUFBMUQsaUJBMkJDO1FBMUJHLElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQ3ZFLElBQU0sT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFDckYsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3JCLE1BQU0sSUFBSSxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztZQUNsRCxPQUFPO1NBQ1Y7UUFDRCxJQUFNLFlBQVksR0FBRyxPQUFPLEdBQUcscUJBQXFCLENBQUMsY0FBYyxHQUFHLGtCQUFrQixDQUFDLFVBQVUsR0FBRyxHQUFHLEdBQUcsVUFBVSxDQUFDO1FBRXZILElBQU0sV0FBVyxHQUFHO1lBQ2hCLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDckIsY0FBYyxFQUFFLFdBQVc7Z0JBQzNCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7YUFDMUYsQ0FBQztTQUNMLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQztpQkFDaEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwyQ0FBcUIsR0FBckIsVUFBc0IsR0FBRyxFQUFFLFdBQVk7UUFBdkMsaUJBa0JDO1FBakJHLFdBQVcsR0FBRztZQUNWLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLFlBQVksRUFBRSxNQUFnQjtZQUM5QixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3JCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7YUFDMUYsQ0FBQztTQUNMLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsV0FBVyxDQUFDO2lCQUM1RCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBb0IsR0FBcEIsVUFBcUIsR0FBRyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWTtRQUEvRCxpQkFnQkM7UUFmRyxJQUFNLFdBQVcsR0FBRztZQUNoQixPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNsSCxHQUFHLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDO1lBQzVDLGVBQWUsRUFBRSxJQUFJO1NBQ3hCLENBQUM7UUFFRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQztpQkFDdkUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0NBQWtCLEdBQWxCLFVBQW1CLEdBQUcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFlBQXlCO1FBQTFFLGlCQWtCQztRQWpCRyxJQUFJLE9BQXdCLENBQUM7UUFDN0IsSUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDbEgsR0FBRyxDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQztZQUM3RCxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBQ0YsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUM5RSxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixPQUFPLENBQUMsU0FBUyxDQUFDLFVBQUMsUUFBUTtnQkFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFDLEtBQUs7Z0JBQ0wsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7b0JBQzNCLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELCtCQUFTLEdBQVQsVUFBVSxLQUFLOztRQUNYLElBQU0sSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDekIsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7O1lBQzNDLEtBQXFCLElBQUEsWUFBQSxTQUFBLE9BQU8sQ0FBQSxnQ0FBQSxxREFBRTtnQkFBekIsSUFBTSxNQUFNLG9CQUFBO2dCQUNiLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQztnQkFDZixPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO29CQUN4QixDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEI7Z0JBQ0QsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDdkIsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUM3QzthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRCxvQ0FBYyxHQUFkLFVBQWUsVUFBVTtRQUNyQixJQUFJLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDaEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssTUFBTSxFQUFFO2dCQUNqQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDM0c7aUJBQU07Z0JBQ0gsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQzFIO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLFVBQVUsRUFBRSxRQUFRLEVBQUUsV0FBWTtRQUMxQyxJQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1FBRXJGLElBQUksV0FBVyxLQUFLLElBQUksRUFBRTtZQUN0QixPQUFPLE9BQU8sR0FBRyxVQUFVLENBQUM7U0FDL0I7YUFBTTtZQUNILElBQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQ3ZFLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDckIsTUFBTSxJQUFJLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO2dCQUNsRCxPQUFPO2FBQ1Y7WUFDRCxPQUFPLE9BQU8sR0FBRyxxQkFBcUIsQ0FBQyxjQUFjO2dCQUNqRCxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxVQUFVO2dCQUN6RSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQy9EO0lBQ0wsQ0FBQztJQUVELG1DQUFhLEdBQWIsVUFBYyxHQUFHLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxjQUFjO1FBQTNELGlCQXlCQztRQXhCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLFlBQVksSUFBSSxZQUFZLElBQUksSUFBSSxFQUFFO2dCQUN0QyxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzVELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQU07Z0JBQ0gsSUFBTSxhQUFhLEdBQUc7b0JBQ2xCLGVBQWUsRUFBRSxJQUFJO29CQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7d0JBQ3JCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7cUJBQzFGLENBQUM7aUJBQ0wsQ0FBQztnQkFFRixLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzdELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQ0FBWSxHQUFaLFVBQWEsR0FBRyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsY0FBYztRQUExRCxpQkF5QkM7UUF4QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxZQUFZLEVBQUU7Z0JBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMzRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFNO2dCQUNILElBQU0sV0FBVyxHQUFHO29CQUNoQixlQUFlLEVBQUUsSUFBSTtvQkFDckIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO3dCQUNyQixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO3FCQUMxRixDQUFDO2lCQUNMLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUM7cUJBQ3RDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7YUFDVjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUdELGtDQUFZLEdBQVo7UUFFSSxJQUFNLE1BQU0sR0FBRyxDQUFDO2dCQUVaLEdBQUcsRUFBRSxvQkFBb0I7Z0JBQ3pCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLE1BQU07Z0JBQ1gsS0FBSyxFQUFFLDZCQUE2QjthQUN2QyxFQUFFO2dCQUNDLEdBQUcsRUFBRSxrQkFBa0I7Z0JBQ3ZCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLHdCQUF3QjtnQkFDN0IsS0FBSyxFQUFFLE9BQU87YUFDakIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsT0FBTztnQkFDWixLQUFLLEVBQUUsTUFBTTthQUNoQixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQzthQUNyQixTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGFBQWEsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLGtCQUFrQjtnQkFDL0UsUUFBUSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUI7Z0JBQy9ELFFBQVEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxFQUFFO2dCQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7YUFDdEM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUMzQjtRQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBR0Qsc0NBQWdCLEdBQWhCLFVBQWlCLFFBQVMsRUFBRSxXQUFZO1FBQXhDLGlCQWlEQztRQWhERyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixVQUFVO1lBQ1YsbUJBQW1CO1lBQ2YsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUM7aUJBQzlELFNBQVMsQ0FBQyxVQUFBLGtCQUFrQjtnQkFDekIsSUFBSSxrQkFBa0IsSUFBSSxrQkFBa0IsQ0FBQyxnQkFBZ0IsSUFBSSxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFO29CQUM3RyxVQUFVO29CQUNWLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztvQkFDN0MscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxHQUFHLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7aUJBQy9HO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsZUFBZTtnQkFDZCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsZUFBZTtxQkFDdEMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLENBQUM7WUFDWCxDQUFDLENBQUMsQ0FBQztZQUNYLHVCQUF1QjtZQUNuQixJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTO2dCQUNyRCxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLEVBQUU7Z0JBQzVELDRDQUE0QztnQkFDNUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDO3FCQUNwRixTQUFTLENBQUMsVUFBQSxrQkFBa0I7b0JBQ3pCLDRDQUE0QztvQkFDNUMsVUFBVTtvQkFDVixLQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7b0JBQzdDLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDcEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsZ0NBQWdDLENBQUMsZ0JBQWdCLEVBQUUsUUFBUSxDQUFDO3lCQUN2RyxTQUFTLENBQUMsVUFBQSxRQUFRO3dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7d0JBQ3ZDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSzt3QkFDSixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO3dCQUN2QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsRUFBRSxVQUFBLGVBQWU7b0JBQ2QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO29CQUNqRixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUN2QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDdkMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO1lBQ0wsSUFBSTtRQUNSLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFzQixHQUF0QixVQUF1QixNQUFNO1FBQTdCLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsR0FBRyxNQUFNLEVBQUUsSUFBSSxDQUFDO2lCQUN2RSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxhQUFhLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUU7b0JBQ25FLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztvQkFDdEUsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMzQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsNERBQTRELENBQUMsQ0FBQztpQkFDaEY7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBc0IsR0FBdEIsVUFBdUIsYUFBYSxFQUFFLFNBQVM7UUFDM0MsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3RELElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQzdFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUN2SyxJQUFJLEdBQUcsSUFBSSxDQUFDO2FBQ2Y7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDekUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzNKLElBQUksR0FBRyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELHNDQUFnQixHQUFoQixVQUFpQixZQUFhO1FBQTlCLGlCQTZGQztRQTVGRyxJQUFNLFVBQVUsR0FBRztZQUNmLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLGdCQUFnQjtZQUM1QyxtRUFBbUU7WUFDbkUsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLDJCQUEyQjtZQUMzRixrQkFBa0IsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsZ0JBQWdCO1lBQ2hGLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7UUFHRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQztpQkFDM0MsU0FBUyxDQUFDLFVBQUEsa0JBQWtCO2dCQUN6QixJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUM7Z0JBQzNCLElBQU0sS0FBSyxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO2dCQUV2RSxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7b0JBQ2YsTUFBTSxJQUFJLEtBQUssQ0FBQztvQkFFaEIsSUFBSSxPQUFPLEdBQUcsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztvQkFDbkYsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztvQkFDcEQsc0JBQXNCO29CQUN0QixLQUFJLENBQUMsNEJBQTRCLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDO3lCQUM1RixTQUFTLENBQUMsVUFBQSxnQkFBZ0I7d0JBQ3ZCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDOzZCQUM5RCxTQUFTLENBQUMsVUFBQSxrQkFBa0I7NEJBQ3pCLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWE7Z0NBQ3RELEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDO29DQUN4QyxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQztvQ0FDN0QsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO29DQUNyRSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQ0FDMUUsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUM1RCxJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQztnQ0FDMUQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7Z0NBQzdELEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztnQ0FDckUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0NBQ2xGLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDaEUsSUFBTSxNQUFNLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUM7Z0NBQ3ZELEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDO2dDQUM3RCxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7Z0NBQ3JFLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dDQUMvRSxrQkFBa0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBRTdELEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLENBQUM7aUNBQzlCLFNBQVMsQ0FBQyxVQUFBLFlBQVk7Z0NBQ25CLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFDLFNBQVMsQ0FBQyxDQUFDO2dDQUN0RixJQUFJLENBQUMsWUFBWSxFQUFFO29DQUNmLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7b0NBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztvQ0FDckQsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUN2QjtxQ0FBTTtvQ0FDSCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO29DQUM5QixJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxTQUFTO3dDQUNyRCxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLElBQUksWUFBWSxFQUFFLEVBQUUseUJBQXlCO3dDQUN2RyxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUM7NkNBQ3BGLFNBQVMsQ0FBQyxVQUFBLGtCQUFrQjs0Q0FDekIsNENBQTRDOzRDQUM1QyxLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxnQ0FBZ0MsQ0FBQyxnQkFBZ0IsQ0FBQztpREFDN0YsU0FBUyxDQUFDLFVBQUEsUUFBUTtnREFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dEQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7NENBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0RBQ0osUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnREFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRDQUN4QixDQUFDLENBQUMsQ0FBQzt3Q0FDWCxDQUFDLEVBQUUsVUFBQSxlQUFlOzRDQUNkLDRDQUE0Qzs0Q0FDNUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDOzRDQUNqRixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDOzRDQUN0QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7d0NBQ3hCLENBQUMsQ0FBQyxDQUFDO3dDQUNQLHdDQUF3Qzt3Q0FDeEMsc0JBQXNCO3FDQUN6Qjt5Q0FBTTt3Q0FDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO3dDQUN0QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7cUNBQ3ZCO2lDQUNKOzRCQUNMLENBQUMsRUFBRSxVQUFBLFlBQVk7Z0NBQ1gsUUFBUSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDakMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsQ0FBQyxFQUFFLFVBQUEsZUFBZTs0QkFDZCxRQUFRLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUNwQyxDQUFDLENBQUMsQ0FBQztvQkFDWCxDQUFDLEVBQUUsVUFBQSxhQUFhO3dCQUNaLFFBQVEsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxDQUFDO29CQUNQLEtBQUs7aUJBQ1I7WUFDTCxDQUFDLEVBQUUsVUFBQSxlQUFlO2dCQUNkLFFBQVEsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBa0IsR0FBbEI7UUFBQSxpQkFXQztRQVZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDaEUsS0FBSSxDQUFDLHFCQUFxQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztpQkFDM0QsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7WUFDekQsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxxQ0FBZSxHQUFmLFVBQWdCLE1BQU07UUFBdEIsaUJBZ0JDO1FBZkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLElBQU0sR0FBRyxHQUFHLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEdBQUcsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3pGLEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNsQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsYUFBYSxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSTtvQkFDakUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUMzQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO2lCQUN2RDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN2QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN2QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1Q0FBaUIsR0FBakIsVUFBa0IsS0FBSztRQUNuQixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsMENBQW9CLEdBQXBCLFVBQXFCLEtBQUs7UUFDdEIsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksS0FBSyxFQUFFO1lBQ1AsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDcEIsY0FBYyxHQUFHO29CQUNiLGVBQWUsRUFBRTt3QkFDYixZQUFZLEVBQUUsQ0FBQztnQ0FDWCxJQUFJLEVBQUU7b0NBQ0YsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO29DQUN4QixTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7aUNBQzNCOzZCQUNKLENBQUM7cUJBQ0w7aUJBQ0osQ0FBQzthQUNMO2lCQUFNO2dCQUNILElBQU0sV0FBVyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7Z0JBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNuQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUNiLElBQUksRUFBRTs0QkFDRixTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7NEJBQ3hCLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTt5QkFDM0I7cUJBQ0osQ0FBQyxDQUFDO2lCQUNOO2dCQUNELGNBQWMsR0FBRztvQkFDYixlQUFlLEVBQUU7d0JBQ2IsWUFBWSxFQUFFLFdBQVc7cUJBQzVCO2lCQUNKLENBQUM7YUFDTDtTQUNKO2FBQU07WUFDSCxNQUFNLHFGQUFxRixDQUFDO1NBQy9GO1FBQ0QsT0FBTyxjQUFjLENBQUM7SUFDMUIsQ0FBQztJQUVELHFEQUErQixHQUEvQixVQUFnQyxLQUFLO1FBQ2pDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLEtBQUssRUFBRTtZQUNQLGNBQWMsR0FBRztnQkFDYixlQUFlLEVBQUU7b0JBQ2IsWUFBWSxFQUFFLENBQUM7NEJBQ1gsSUFBSSxFQUFFO2dDQUNGLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs2QkFDM0I7NEJBQ0QsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPO3lCQUMzQixDQUFDO2lCQUNMO2FBQ0osQ0FBQztTQUNMO2FBQU07WUFDSCxJQUFNLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO1lBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuQyxXQUFXLENBQUMsSUFBSSxDQUFDO29CQUNiLElBQUksRUFBRTt3QkFDRixTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7cUJBQzNCO29CQUNELE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTztpQkFDM0IsQ0FBQyxDQUFDO2FBQ047WUFDRCxjQUFjLEdBQUc7Z0JBQ2IsZUFBZSxFQUFFO29CQUNiLFlBQVksRUFBRSxXQUFXO2lCQUM1QjthQUNKLENBQUM7U0FDTDtRQUNELE9BQU8sY0FBYyxDQUFDO0lBQzFCLENBQUM7SUFFRCwyQ0FBcUIsR0FBckIsVUFBc0IsTUFBYyxFQUFFLEtBQWlCO1FBQ25ELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQztRQUUzQixJQUFNLGlCQUFpQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDdEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO2dCQUNuQixjQUFjLEVBQUUsRUFBRTtnQkFDbEIsZ0JBQWdCLEVBQUUsRUFBRTthQUN2QixDQUFDLENBQUM7U0FDTjtRQUNELGVBQWUsR0FBRztZQUNkLHNCQUFzQixFQUFFO2dCQUNwQixtQkFBbUIsRUFBRSxpQkFBaUI7Z0JBQ3RDLGVBQWUsRUFBRSxNQUFNO2FBQzFCO1NBQ0osQ0FBQztRQUVGLE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFFRCw0Q0FBc0IsR0FBdEIsVUFBdUIsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGFBQWE7UUFDckUsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUMsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNyRyxDQUFDO0lBRUQsK0NBQXlCLEdBQXpCLFVBQTBCLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCO1FBQzFGLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQU0saUJBQWlCLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUN0QyxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBTSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFNUIsSUFBSSxjQUFjLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxhQUFhLElBQUksYUFBYSxJQUFJLElBQUksSUFBSSxhQUFhLEtBQUssRUFBRSxFQUFFO1lBRW5ILElBQU0sa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLElBQU0sY0FBYyxHQUFHLGtCQUFrQixDQUFDO1lBQzFDLElBQUksZ0JBQWdCLElBQUksZ0JBQWdCLElBQUksSUFBSSxJQUFJLGdCQUFnQixLQUFLLEVBQUUsRUFBRTtnQkFDekUsSUFBTSxHQUFHLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN4QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDakMsSUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNuQixPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ2xDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxNQUFNLEdBQUc7b0JBQ1QsRUFBRSxFQUFFLGtCQUFrQjtpQkFDekIsQ0FBQztnQkFDRixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDakI7WUFDRCx1R0FBdUc7WUFDdkcsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzVDLGVBQWUsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLGVBQWUsQ0FBQyxFQUFFLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDMUMsSUFBSSxJQUFJLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbEMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUU7b0JBQzdDLElBQUksR0FBRyxvQ0FBb0MsQ0FBQztpQkFDL0M7Z0JBQ0QsZUFBZSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBQzVCLDBIQUEwSDtnQkFDMUgsSUFBSSxtQkFBbUIsSUFBSSxtQkFBbUIsSUFBSSxJQUFJLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7b0JBQ2hGLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssRUFBRSxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksSUFBSTtvQkFDdkUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFFMUcsSUFBSSxhQUFhLEdBQUcsbUJBQW1CLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNwRSwrQkFBK0I7b0JBQy9CLElBQUksSUFBSSxLQUFLLHlDQUF5QyxFQUFFO3dCQUNwRCxJQUFNLE1BQU0sR0FBRyxFQUFFLENBQUM7d0JBQ2xCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDMUQsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLGFBQWEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0NBQ3ZELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUSxFQUFFO29DQUNqRyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO2lDQUMxRDtnQ0FDRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxLQUFLLFFBQVEsRUFBRTtvQ0FDakcsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQ0FDMUQ7Z0NBQ0QsSUFBTSxLQUFLLEdBQUc7b0NBQ1YsS0FBSyxFQUFFO3dDQUNILEtBQUssRUFBRSxhQUFhLENBQUMsS0FBSyxDQUFDO3dDQUMzQixJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7cUNBQ25DO2lDQUNKLENBQUM7Z0NBRUYsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDdEI7eUJBQ0o7NkJBQU07NEJBQ0gsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRO2dDQUMvRixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtnQ0FDekMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs2QkFDNUM7aUNBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRO21DQUNuRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtnQ0FDNUMsYUFBYSxHQUFHLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzs2QkFDNUM7NEJBQ0QsSUFBTSxLQUFLLEdBQUc7Z0NBQ1YsS0FBSyxFQUFFO29DQUNILEtBQUssRUFBRSxhQUFhO29DQUNwQixJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7aUNBQ25DOzZCQUNKLENBQUM7NEJBQ0YsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdEI7d0JBQ0QsZUFBZSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7cUJBQ25DO3lCQUFNO3dCQUNILElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUTs0QkFDL0YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7NEJBQ3pDLGFBQWEsR0FBRyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQzVDOzZCQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUTsrQkFDbkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7NEJBQzVDLGFBQWEsR0FBRyxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7eUJBQzVDOzZCQUFNO3lCQUVOO3dCQUNELElBQU0sS0FBSyxHQUFHOzRCQUNWLEtBQUssRUFBRTtnQ0FDSCxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFROzZCQUNuQzt5QkFDSixDQUFDO3dCQUNGLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO3FCQUNqQztpQkFDSjtxQkFBTSxJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixJQUFJLElBQUksSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTtvQkFDdkYsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxFQUFFLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxJQUFJO29CQUN2RSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUMxRyxJQUFNLE1BQU0sR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDckQsSUFBTSxLQUFLLEdBQUc7d0JBQ1YsS0FBSyxFQUFFOzRCQUNILEtBQUssRUFBRSxFQUFFOzRCQUNULElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTt5QkFDbkM7cUJBQ0osQ0FBQztvQkFFRiw2QkFBNkI7b0JBQzdCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO3dCQUNwQyxJQUFJLGFBQWEsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkQsMkNBQTJDO3dCQUMzQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUU7NEJBQzlCLGFBQWEsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3BDO3dCQUNELCtCQUErQjt3QkFDL0IsSUFBSSxhQUFhLEtBQUssRUFBRSxJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksYUFBYSxFQUFFOzRCQUNoRSxvRUFBb0U7NEJBQ3BFLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtnQ0FDNUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQzs2QkFDL0Q7aUNBQU07Z0NBQ0gsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDOzZCQUNyQzt5QkFDSjtxQkFDSjtpQkFDSjtxQkFBTTtvQkFDSCxJQUFJLFFBQVEsU0FBQSxDQUFDO29CQUNiLDhCQUE4QjtvQkFDOUIsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLEVBQUUsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFO3dCQUNwRyxRQUFRLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztxQkFDeEM7eUJBQU07d0JBQ0gsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7NEJBQ2pHLFFBQVEsR0FBRyxJQUFJLENBQUM7eUJBQ25COzZCQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUSxFQUFFOzRCQUN4RyxRQUFRLEdBQUcsQ0FBQyxDQUFDO3lCQUNoQjs2QkFBTTt5QkFFTjtxQkFDSjtvQkFDRCxJQUFNLEtBQUssR0FBRzt3QkFDVixLQUFLLEVBQUU7NEJBQ0gsS0FBSyxFQUFFLFFBQVE7NEJBQ2YsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO3lCQUNuQztxQkFDSixDQUFDO29CQUNGLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNqQztnQkFDRCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDM0M7WUFFRCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxhQUFhLElBQUksYUFBYSxJQUFJLElBQUksSUFBSSxhQUFhLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUN2RSxlQUFlLEdBQUcsYUFBYSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNILE1BQU0sOEJBQThCLENBQUM7YUFDeEM7WUFDRCxtQkFBbUIsR0FBRztnQkFDbEIsY0FBYyxFQUFFO29CQUNaLEtBQUssRUFBRTt3QkFDSCxRQUFRLEVBQUU7NEJBQ04scUJBQXFCLEVBQUUsaUJBQWlCO3lCQUMzQzt3QkFDRCxpQkFBaUIsRUFBRSxlQUFlO3dCQUNsQyxvQkFBb0IsRUFBRSxnQkFBZ0I7cUJBQ3pDO2lCQUNKO2FBQ0osQ0FBQztTQUNMO2FBQU07WUFDSCxNQUFNLHVDQUF1QztrQkFDM0Msb0ZBQW9GLENBQUM7U0FDMUY7UUFDRCxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7SUFFRCxrQ0FBWSxHQUFaLFVBQWEsS0FBSyxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxlQUFlLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLGdCQUFnQjs7UUFDdkgsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzNFLElBQU0sS0FBSyxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7UUFDN0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLEtBQUssSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixJQUFJLElBQUksSUFBSSxjQUFjLElBQUksZUFBZTtZQUNqSCxnQkFBZ0IsRUFBRTtZQUVsQixPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMseUJBQXlCLENBQUMsY0FBYyxFQUFFLG1CQUFtQixFQUFFLGFBQWEsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDL0gsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3ZCLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsUUFBUTthQUNwRCxDQUFDLENBQUM7WUFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzNDLEtBQUssQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsZUFBZSxDQUFDLENBQUM7WUFDbEQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3JELEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ3BDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzNELElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN2QixJQUFJLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFFBQVE7YUFDcEQsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7O2dCQUMvQixLQUFtQixJQUFBLFVBQUEsU0FBQSxLQUFLLENBQUEsNEJBQUEsK0NBQUU7b0JBQXJCLElBQU0sSUFBSSxrQkFBQTtvQkFDWCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDL0I7Ozs7Ozs7OztTQUNKO2FBQU07WUFDSCxNQUFNLGtGQUFrRixDQUFDO1lBQ3pGLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsNkNBQXVCLEdBQXZCLFVBQXdCLFFBQVEsRUFBRSxhQUFhLEVBQUUsZ0JBQWdCOztRQUM3RCxJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUMvQixJQUFNLG1CQUFtQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7UUFDeEMsSUFBTSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFNUIsSUFBSSxRQUFRLElBQUksYUFBYSxJQUFJLGFBQWEsSUFBSSxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsRUFBRTtZQUU1RSxJQUFNLGtCQUFrQixHQUFHLENBQUMsQ0FBQztZQUM3QixJQUFJLGdCQUFnQixJQUFJLGdCQUFnQixJQUFJLElBQUksSUFBSSxnQkFBZ0IsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pFLElBQU0sR0FBRyxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs7b0JBQ3hDLEtBQWlCLElBQUEsUUFBQSxTQUFBLEdBQUcsQ0FBQSx3QkFBQSx5Q0FBRTt3QkFBakIsSUFBTSxFQUFFLGdCQUFBO3dCQUNULElBQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDbkIsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFDM0IsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUNsQzs7Ozs7Ozs7O2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxNQUFNLEdBQUc7b0JBQ1QsRUFBRSxFQUFFLGtCQUFrQjtpQkFDekIsQ0FBQztnQkFDRixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDakI7WUFFRCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxhQUFhLElBQUksYUFBYSxJQUFJLElBQUksSUFBSSxhQUFhLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUN2RSxlQUFlLEdBQUcsYUFBYSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNILE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQzthQUNuRDtZQUVELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxxQkFBcUIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFOztvQkFDN0YsS0FBNEIsSUFBQSxLQUFBLFNBQUEsUUFBUSxDQUFDLHFCQUFxQixDQUFBLGdCQUFBLDRCQUFFO3dCQUF2RCxJQUFNLGFBQWEsV0FBQTs7NEJBQ3BCLEtBQThCLElBQUEsb0JBQUEsU0FBQSxhQUFhLENBQUMscUJBQXFCLENBQUEsQ0FBQSxnQkFBQSw0QkFBRTtnQ0FBOUQsSUFBTSxlQUFlLFdBQUE7Z0NBQ3RCLElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxFQUFFLEtBQUsscUJBQXFCLElBQUksZUFBZSxDQUFDLEtBQUssSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtvQ0FDekgsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQztpQ0FDOUQ7Z0NBQ0QsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDOzZCQUM3Qzs7Ozs7Ozs7O3FCQUNKOzs7Ozs7Ozs7YUFDSjtZQUVELG1CQUFtQixHQUFHO2dCQUNsQixjQUFjLEVBQUU7b0JBQ1osS0FBSyxFQUFFO3dCQUNILFFBQVEsRUFBRTs0QkFDTixxQkFBcUIsRUFBRSxtQkFBbUI7eUJBQzdDO3dCQUNELGlCQUFpQixFQUFFLGVBQWU7d0JBQ2xDLG9CQUFvQixFQUFFLGdCQUFnQjtxQkFDekM7aUJBQ0o7YUFDSixDQUFDO1NBQ0w7YUFBTTtZQUNILE9BQU8sQ0FBQyxLQUFLLENBQUMsdUlBQ29FLENBQUMsQ0FBQztTQUN2RjtRQUNELE9BQU8sbUJBQW1CLENBQUM7SUFDL0IsQ0FBQztJQUVELHVDQUFpQixHQUFqQixVQUFrQixLQUFLLEVBQUUsbUJBQW1CLEVBQUUsY0FBYyxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxjQUFjOztRQUN6RyxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0UsSUFBTSxLQUFLLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLG1CQUFtQixJQUFJLGNBQWMsSUFBSSxnQkFBZ0IsRUFBRTs7Z0JBQzlGLEtBQW1CLElBQUEsVUFBQSxTQUFBLEtBQUssQ0FBQSw0QkFBQSwrQ0FBRTtvQkFBckIsSUFBTSxJQUFJLGtCQUFBO29CQUNYLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsRUFBRSxhQUFhLEVBQUUsY0FBYyxDQUFDLENBQUMsQ0FBQztvQkFDM0csSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ3ZCLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsUUFBUTtxQkFDcEQsQ0FBQyxDQUFDO29CQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzNDLEtBQUssQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsY0FBYyxDQUFDLENBQUM7b0JBQ2pELEtBQUssQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztvQkFDckQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ3BDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTt3QkFDdkIsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO3FCQUNwRCxDQUFDLENBQUM7b0JBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQy9CLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUMvQjs7Ozs7Ozs7O1lBRUQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLHFGQUFxRixDQUFDLENBQUM7WUFDckcsT0FBTyxJQUFJLENBQUM7U0FDZjtJQUNMLENBQUM7SUFFRCxtQ0FBYSxHQUFiLFVBQWMsUUFBZ0I7UUFBOUIsaUJBMkJDO1FBMUJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDckYsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUN2RSxJQUFNLEdBQUcsR0FBRyxPQUFPLEdBQUcsVUFBVSxHQUFDLE9BQU8sR0FBQyxlQUFlLENBQUM7WUFFekQsSUFBTSxXQUFXLEdBQUc7Z0JBQ2hCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7b0JBQ3JCLGNBQWMsRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsaUJBQWlCO29CQUNwRSxnQkFBZ0IsRUFBRSxNQUFNO29CQUN4QixnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO29CQUN2RixlQUFlLEVBQUUsT0FBTztpQkFDM0IsQ0FBQzthQUNMLENBQUM7WUFFRixJQUFNLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQ2hDLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBRXZDLEtBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxRQUFRLEVBQUUsV0FBVyxDQUFDO2lCQUNyQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQ0FBYSxHQUFiLFVBQWMsS0FBSyxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsRUFBRSxlQUFlLEVBQUUsZ0JBQWdCLEVBQUUsYUFBYSxFQUN0RyxjQUFjLEVBQUUsV0FBVyxFQUFFLFVBQVc7UUFFeEMsSUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsR0FBRyxHQUFHLEdBQUcsV0FBVyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNwSixJQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFFbkIsSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CO1lBQ3RELGVBQWUsSUFBSSxnQkFBZ0IsRUFBRTtZQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLG1CQUFtQixFQUFFLGFBQWEsRUFBRSxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUMzRyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTtvQkFDdkIsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO2lCQUNwRCxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDYixLQUFLLENBQUMsTUFBTSxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxDQUFDO29CQUMzQyxLQUFLLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxDQUFDO2lCQUNyRDtnQkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLGVBQWUsRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDM0MsS0FBSyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUV6QyxJQUFJLFVBQVUsRUFBRTtvQkFDWixPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztpQkFDekU7cUJBQU07b0JBQ0gsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7aUJBQzlEO2dCQUNELElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUN2QixJQUFJLEVBQUUscUJBQXFCLENBQUMsWUFBWSxDQUFDLFFBQVE7aUJBQ3BELENBQUMsQ0FBQztnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDL0IsSUFBRyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxJQUFJLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsRUFBQztvQkFDbkgsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNuRSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRTt3QkFDdkIsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO3FCQUNwRCxDQUFDLENBQUM7b0JBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDMUM7cUJBQUk7b0JBQ0QsOERBQThEO29CQUM5RCxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDbEM7YUFFSjtZQUNELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUNyRDthQUFNO1lBQ0gsT0FBTyxDQUFDLEtBQUssQ0FBQyxxRkFBcUYsQ0FBQyxDQUFDO1lBQ3JHLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsNENBQXNCLEdBQXRCLFVBQXVCLFNBQVM7UUFDNUIsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztRQUNwQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFJLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEtBQUssUUFBUTtnQkFDckMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsRUFBRTtnQkFDdkYsV0FBVyxFQUFFLENBQUM7Z0JBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNQLFFBQVEsSUFBSSxHQUFHLENBQUM7aUJBQ25CO2dCQUNELFFBQVEsSUFBSSxXQUFXLEdBQUcsR0FBRyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixDQUFDO2dCQUNuSCxRQUFRLElBQUksSUFBSSxDQUFDO2FBQ3BCO1NBQ0o7UUFDRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsNkxBQ3lELEdBQUcsUUFBUSxDQUFDLENBQUM7WUFDcEcsT0FBTyxLQUFLLENBQUM7U0FDaEI7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDO0lBRUQsbUNBQWEsR0FBYixVQUFjLFFBQXVCO1FBQXJDLGlCQStEQztRQTlERyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDeEQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO2dCQUN6RSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7WUFFRCxJQUFNLFFBQVEsR0FBRyxDQUFDO29CQUNkLEdBQUcsRUFBRSxRQUFRO29CQUNiLEtBQUssRUFBRSxXQUFXO2lCQUNyQixFQUFFO29CQUNDLEdBQUcsRUFBRSxNQUFNO29CQUNYLEtBQUssRUFBRTt3QkFDSCxHQUFHLEVBQUUsbUJBQW1CO3dCQUN4QixLQUFLLEVBQUU7NEJBQ0gsdUJBQXVCLEVBQUU7Z0NBQ3JCLGlCQUFpQixFQUFFO29DQUNmLFNBQVMsRUFBRSxRQUFRO29DQUNuQixJQUFJLEVBQUUsc0RBQXNEO29DQUM1RCxtQkFBbUIsRUFBRSxXQUFXO2lDQUNuQzs2QkFDSjt5QkFDSjtxQkFDSjtpQkFDSixDQUFDLENBQUM7WUFFSCxJQUFNLEdBQUcsR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDMUQsSUFBTSxXQUFXLEdBQUc7Z0JBQ2hCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7b0JBQ3JCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7b0JBQ3ZGLGNBQWMsRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsVUFBVTtpQkFDaEUsQ0FBQztnQkFDRixnQkFBZ0IsRUFBRSxvQkFBb0I7YUFDekMsQ0FBQztZQUNGLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUM7aUJBQ2pGLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGtDQUFrQyxDQUFDO29CQUN4RCxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUI7b0JBQzlELFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixFQUFFO29CQUNoRSxJQUFJLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQjt3QkFDakYsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsTUFBTTt3QkFDeEYsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFFOUYsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQkFBc0I7NEJBQzNDLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO3FCQUUvRzt5QkFBTSxJQUFJLFFBQVEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLHNCQUFzQjt3QkFDNUYsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsTUFBTTt3QkFDNUYsUUFBUSxDQUFDLGtDQUFrQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsc0JBQXNCLENBQUMsTUFBTSxHQUFHLENBQUM7d0JBQ2hHLDRDQUE0Qzt3QkFDNUMsUUFBUSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUU7d0JBQ2xILFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxzQkFBc0IsR0FBRyxRQUFRLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUM7cUJBQ3BGO2lCQUNKO3FCQUFNO29CQUNILFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDO2lCQUNuRDtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsZ0NBQVUsR0FBVixVQUFXLFFBQXVCO1FBQzlCLElBQU0sUUFBUSxHQUFHLENBQUM7Z0JBQ2QsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsS0FBSyxFQUFFLE1BQU07YUFDaEIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsTUFBTTtnQkFDWCxLQUFLLEVBQUU7b0JBQ0gsR0FBRyxFQUFFLG1CQUFtQjtvQkFDeEIsS0FBSyxFQUFFO3dCQUNILHVCQUF1QixFQUFFOzRCQUNyQixpQkFBaUIsRUFBRTtnQ0FDZixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsSUFBSSxFQUFFLHNEQUFzRDtnQ0FDNUQsbUJBQW1CLEVBQUUsTUFBTTs2QkFDOUI7eUJBQ0o7cUJBQ0o7aUJBQ0o7YUFDSixDQUFDLENBQUM7UUFDSCxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUQsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN0RSxlQUFlLEVBQUUsSUFBSTtZQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3JCLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3ZGLGNBQWMsRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsVUFBVTthQUNoRSxDQUFDO1NBQ0wsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNkLENBQUM7SUFFRCxpQ0FBVyxHQUFYLFVBQVksUUFBUTtRQUNoQixJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDOUUsSUFBSSxLQUFLLENBQUM7UUFDVixJQUFJLFFBQVEsRUFBRTtZQUNWLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7b0JBQ3pCLEdBQUcsRUFBRSxXQUFXO29CQUNoQixLQUFLLEVBQUUsUUFBUTtpQkFDbEI7YUFDQSxDQUFDLENBQUM7U0FDTjtRQUNELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQsb0NBQWMsR0FBZCxVQUFlLElBQUksRUFBRSxXQUFXO1FBQzVCLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMvRSxJQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQzdCLEtBQUssQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQzNDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNCLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUVELHFDQUFlLEdBQWYsVUFBZ0IsS0FBSyxFQUFFLFdBQVc7O1FBQzlCLElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQzs7WUFDcEIsS0FBbUIsSUFBQSxVQUFBLFNBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO2dCQUFyQixJQUFNLElBQUksa0JBQUE7Z0JBQ1gsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO2FBQ3pEOzs7Ozs7Ozs7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtDQUFZLEdBQVosVUFBYSxXQUFXLEVBQUUsV0FBVztRQUNqQyxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFlBQVksR0FBRyxHQUFHLEdBQUcsV0FBVyxFQUFFLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNsRyxJQUFNLEtBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDbkIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDM0MsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzFCLGNBQWMsR0FBRztZQUNiLGVBQWUsRUFBRTtnQkFDYixZQUFZLEVBQUUsQ0FBQzt3QkFDWCxJQUFJLEVBQUU7NEJBQ0YsU0FBUyxFQUFFLFdBQVcsQ0FBQyxJQUFJO3lCQUM5Qjt3QkFDRCxNQUFNLEVBQUUsV0FBVyxDQUFDLE9BQU87cUJBQzlCLENBQUM7YUFDTDtTQUNKLENBQUM7UUFDRixLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUMsT0FBTyxHQUFHLGNBQWMsQ0FBQztRQUN6QixJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUU7WUFDdkMsSUFBSSxFQUFFLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxRQUFRO1NBQ3BELENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRS9CLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsa0NBQVksR0FBWixVQUFhLEtBQWlCO1FBQzFCLElBQU0sU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNyQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUU7Z0JBQ2YsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakM7U0FDSjtRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRCxzQ0FBZ0IsR0FBaEIsVUFBaUIsWUFBYTtRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUMsWUFBWSxDQUFBLENBQUMsQ0FBQSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzdILE9BQU8sV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBQyxZQUFZLENBQUEsQ0FBQyxDQUFBLE1BQU0sQ0FBQyxDQUFDO0lBQzNILENBQUM7SUFFRCxxQ0FBZSxHQUFmLFVBQWdCLFFBQW9CLEVBQUUsWUFBb0I7UUFBMUQsaUJBeUdDO1FBeEdHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sT0FBTyxHQUFHLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFDckYsSUFBTSxPQUFPLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUN2RSxJQUFNLEdBQUcsR0FBRyxPQUFPLEdBQUcsVUFBVSxHQUFDLE9BQU8sR0FBQyxlQUFlLENBQUM7WUFFekQsSUFBTSxXQUFXLEdBQUc7Z0JBQ2hCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7b0JBQ3JCLGNBQWMsRUFBRSxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsVUFBVTtvQkFDN0QsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTtpQkFDMUYsQ0FBQzthQUNMLENBQUM7WUFDRixJQUFJLGFBQWEsQ0FBQztZQUNsQixJQUFHLFlBQVksQ0FBQyxXQUFXLEVBQUUsSUFBSSxTQUFTLENBQUMsR0FBRyxFQUFDO2dCQUM5QyxhQUFhLEdBQUc7b0JBQ2Isb0JBQW9CLEVBQUU7d0JBQ2xCLGNBQWMsRUFBRTs0QkFDWixlQUFlLEVBQUU7Z0NBQ2Isc0JBQXNCLEVBQUUsS0FBSztnQ0FDN0IscUJBQXFCLEVBQUUsSUFBSTtnQ0FDM0IseUJBQXlCLEVBQUUsS0FBSzs2QkFDbkM7NEJBQ0QseUJBQXlCLEVBQUUsS0FBSzs0QkFDaEMsaUJBQWlCLEVBQUU7Z0NBQ2YsZ0JBQWdCLEVBQUU7b0NBQ2Q7d0NBQ0ksZUFBZSxFQUFFLENBQUM7d0NBQ2xCLEtBQUssRUFBRSxHQUFHO3FDQUNiO29DQUNEO3dDQUNJLGVBQWUsRUFBRSxDQUFDO3dDQUNsQixLQUFLLEVBQUUsR0FBRztxQ0FDYjtpQ0FDSjtnQ0FDRCxFQUFFLEVBQUUsOENBQThDO2dDQUNsRCxjQUFjLEVBQUUsc0NBQXNDOzZCQUN6RDs0QkFDRCxxQkFBcUIsRUFBRSxrQkFBa0I7NEJBQ3pDLHVCQUF1QixFQUFFO2dDQUNyQjtvQ0FDSSxjQUFjLEVBQUUsNkNBQTZDO29DQUM3RCxTQUFTLEVBQUU7d0NBQ1AsSUFBSTt3Q0FDSixLQUFJLENBQUMsZ0JBQWdCLEVBQUU7cUNBQzFCO2lDQUNKOzZCQUNKOzRCQUNELHNCQUFzQixFQUFFLElBQUk7NEJBQzVCLHdCQUF3QixFQUFFLElBQUk7NEJBQzlCLGtCQUFrQixFQUFFLElBQUk7NEJBQ3hCLFNBQVMsRUFBRSxJQUFJO3lCQUNsQjtxQkFDSjtpQkFDSixDQUFDO2FBQ0Q7aUJBQUk7Z0JBQ0QsYUFBYSxHQUFHO29CQUNaLG9CQUFvQixFQUFFO3dCQUNsQixjQUFjLEVBQUU7NEJBQ1osZUFBZSxFQUFFO2dDQUNiLHFCQUFxQixFQUFFLElBQUk7Z0NBQzNCLHlCQUF5QixFQUFFLEtBQUs7NkJBQ25DOzRCQUNELHlCQUF5QixFQUFFLEtBQUs7NEJBQ2hDLGlCQUFpQixFQUFFO2dDQUNmLGdCQUFnQixFQUFFO29DQUNkO3dDQUNJLGVBQWUsRUFBRSxDQUFDO3dDQUNsQixLQUFLLEVBQUUsR0FBRztxQ0FDYjtpQ0FDSjtnQ0FDRCxFQUFFLEVBQUUsOENBQThDO2dDQUNsRCxjQUFjLEVBQUUsc0NBQXNDOzZCQUN6RDs0QkFDRCxxQkFBcUIsRUFBRSxrQkFBa0I7NEJBQ3pDLHNCQUFzQixFQUFFLElBQUk7NEJBQzVCLHdCQUF3QixFQUFFLElBQUk7NEJBQzlCLGtCQUFrQixFQUFFLElBQUk7NEJBQ3hCLFNBQVMsRUFBRSxLQUFLO3lCQUNuQjtxQkFDSjtpQkFDSixDQUFDO2FBQ0w7WUFDRCxJQUFNLGdCQUFnQixHQUFHO2dCQUNyQix1QkFBdUIsRUFBRTtvQkFDckIsaUJBQWlCLEVBQUU7d0JBQ2YsU0FBUyxFQUFFLFFBQVE7d0JBQ25CLElBQUksRUFBRSxzREFBc0Q7d0JBQzVELG1CQUFtQixFQUFFLEtBQUs7d0JBQzFCLFVBQVUsRUFBRSxRQUFRO3FCQUN2QjtpQkFDSjthQUNKLENBQUM7WUFFRixJQUFNLE1BQU0sR0FBRyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztnQkFDNUQscUJBQXFCLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFFakYsS0FBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUM7aUJBQ25DLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG1DQUFhLEdBQWIsVUFBYyxNQUFXO1FBQXpCLGlCQVdDO1FBVkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMscUJBQXFCO1lBQ3pDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDO2lCQUMvQixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBb0IsR0FBcEI7UUFBQSxpQkErQkM7UUE3QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksRUFBRztnQkFDekUsSUFBSSxXQUFXLEdBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUE7Z0JBQzNGLElBQUcsV0FBVyxJQUFHLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRSxDQUFDLEVBQUM7b0JBQzVFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDO3dCQUNqSSxJQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssMEJBQTBCLENBQUMsNkJBQTZCLENBQUMsYUFBYTs0QkFDOUYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtvQkFDeEYsQ0FBQyxDQUFDLENBQUE7aUJBQ047Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBRUgsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQztxQkFDcEUsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixJQUFHLFFBQVEsSUFBRyxJQUFJLEVBQUM7d0JBQ2xCLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQzs0QkFDaEUsSUFBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxLQUFLLDBCQUEwQixDQUFDLDZCQUE2QixDQUFDLGFBQWE7Z0NBQzlGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7d0JBQ3hGLENBQUMsQ0FBQyxDQUFBO3dCQUNILGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQTt3QkFDeEYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUNuQjtnQkFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxzQ0FBZ0IsR0FBaEIsVUFBaUIsT0FBTyxFQUFFLFFBQVE7UUFBbEMsaUJBNENDO1FBM0NHLElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFNLGVBQWUsR0FBRztZQUNwQixpQkFBaUIsRUFBRTtnQkFDZiw4QkFBOEIsRUFBRSxJQUFJO2dCQUNwQyxrQkFBa0IsRUFBRSxJQUFJO2dCQUN4Qix1QkFBdUIsRUFBRSxJQUFJO2dCQUM3QixhQUFhLEVBQUUsSUFBSTtnQkFDbkIsdUJBQXVCLEVBQUUsSUFBSTtnQkFDN0IsbUJBQW1CLEVBQUUsSUFBSTtnQkFDekIsaUJBQWlCLEVBQUUsSUFBSTtnQkFDdkIscUJBQXFCLEVBQUUsSUFBSTtnQkFDM0Isb0JBQW9CLEVBQUUsSUFBSTtnQkFDMUIsc0JBQXNCLEVBQUUsSUFBSTtnQkFDNUIsc0JBQXNCLEVBQUUsSUFBSTtnQkFDNUIsU0FBUyxFQUFFLElBQUk7YUFDbEI7U0FDSixDQUFDO1FBQ0YsSUFBTSxRQUFRLEdBQUcsQ0FBQztnQkFDZCxHQUFHLEVBQUUsV0FBVztnQkFDaEIsS0FBSyxFQUFFLFFBQVE7YUFDbEIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsaUJBQWlCO2dCQUN0QixLQUFLLEVBQUUsTUFBTTthQUNoQixDQUFDLENBQUM7UUFDSCxJQUFJLFFBQVEsS0FBSyxRQUFRLEVBQUU7WUFDdkIsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDVixHQUFHLEVBQUUsbUJBQW1CO2dCQUN4QixLQUFLLEVBQUUsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUM3RCxDQUFDLENBQUM7U0FDTjtRQUVELE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLE9BQU8sRUFBRSxRQUFRLENBQUM7aUJBQzdFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUU7b0JBQzNDLFlBQVksR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQztpQkFDaEQ7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDNUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFpQixHQUFqQixVQUFrQixRQUFRO1FBQTFCLGlCQThCQztRQTdCRyxJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBTSxRQUFRLEdBQUcsQ0FBQztnQkFDZCxHQUFHLEVBQUUsV0FBVztnQkFDaEIsS0FBSyxFQUFFLFVBQVU7YUFDcEIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsMEJBQTBCO2dCQUMvQixLQUFLLEVBQUUsTUFBTTthQUNoQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLEVBQUU7Z0JBQ0MsR0FBRyxFQUFFLE9BQU87Z0JBQ1osS0FBSyxFQUFFLE9BQU87YUFDakIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsT0FBTztnQkFDWixLQUFLLEVBQUUsSUFBSTthQUNkLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLEdBQUcsUUFBUSxFQUFFLFFBQVEsQ0FBQztpQkFDekUsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsZUFBZSxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFO29CQUN6RSxhQUFhLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7aUJBQ25EO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQzdCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBc0IsR0FBdEI7UUFBQSxpQkFjQztRQWJHLElBQUksa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1FBQzVCLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDO2lCQUMvRCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyx5QkFBeUIsSUFBSSxRQUFRLENBQUMseUJBQXlCLENBQUMsb0JBQW9CLEVBQUU7b0JBQzNHLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUNsQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNENBQXNCLEdBQXRCO1FBQUEsaUJBY0M7UUFiRyxJQUFJLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUMzQixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQztpQkFDOUQsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsRUFBRTtvQkFDVixpQkFBaUIsR0FBRyxRQUFRLENBQUMsd0JBQXdCLENBQUM7aUJBQ3pEO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDakMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFtQixHQUFuQixVQUFvQixPQUFPO1FBQTNCLGlCQWVDO1FBZEcsSUFBSSxvQkFBb0IsR0FBRyxFQUFFLENBQUM7UUFFOUIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGNBQWMsR0FBRyxPQUFPLEVBQUUsSUFBSSxDQUFDO2lCQUN4RSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxFQUFFO29CQUNWLG9CQUFvQixHQUFHLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxjQUFjLENBQUM7aUJBQzFFO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDcEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGdDQUFVLEdBQVYsVUFBVyxZQUFZLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxhQUFhO1FBQzdDLElBQU0sR0FBRyxHQUFHLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztRQUVqRDs7WUFFSTtRQUVKLElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7WUFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUU7WUFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDcEY7UUFDRCxJQUFJLFlBQVksQ0FBQyxTQUFTLEtBQUssUUFBUSxFQUFFO1lBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsZ0RBQWdELEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUM7U0FDbko7UUFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsQ0FBQztRQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsQ0FBQztRQUM5QixRQUFRLENBQUMsSUFBSSxDQUFDLGtDQUFrQyxDQUFDLENBQUM7UUFDbEQsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxZQUFZLENBQUMsa0JBQWtCLEVBQUU7WUFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksWUFBWSxDQUFDLHNCQUFzQixFQUFFO1lBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMseUJBQXlCLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEg7UUFDRCxJQUFJLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3RFO1FBQ0QsSUFBSSxPQUFPLFlBQVksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFO1lBQy9DLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUM1RDtRQUNELElBQUksWUFBWSxDQUFDLElBQUksRUFBRTtZQUNuQixRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNsRTtRQUNELElBQUksWUFBWSxDQUFDLFdBQVcsRUFBRTtZQUMxQixRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztTQUNoRjtRQUNELElBQUksT0FBTyxZQUFZLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRTtZQUM3QyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDeEQ7UUFDRCxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLGlEQUFpRCxDQUFDLENBQUMsQ0FBQztTQUNuSjtRQUNELElBQUksWUFBWSxDQUFDLGtCQUFrQixFQUFFO1lBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLFlBQVksQ0FBQyxxQkFBcUIsRUFBRTtZQUNwQyxRQUFRLENBQUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3BIO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNyRDtRQUVELElBQU0sS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsSUFBTSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDckUsR0FBRyxDQUFDLGNBQWMsRUFBRSxtQ0FBbUMsQ0FBQztZQUM3RCxlQUFlLEVBQUUsSUFBSTtTQUN4QixDQUFDO1FBQ0Y7O1lBRUk7UUFDSixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLG1DQUFtQyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCxzQ0FBZ0IsR0FBaEIsVUFBaUIsWUFBWSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsYUFBYTtRQUNuRCxJQUFJLEdBQUcsR0FBRyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQztRQUNuRCxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDNUIsR0FBRyxHQUFHLHFCQUFxQixDQUFDLGNBQWMsQ0FBQztTQUM5QztRQUNELElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLFlBQVksQ0FBQyxTQUFTLEVBQUU7WUFDeEIsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLFlBQVksQ0FBQyxVQUFVLEVBQUU7WUFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO1lBQzVCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsa0JBQWtCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDcEY7UUFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDN0YsUUFBUSxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO1FBQy9DLFFBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzNHLElBQUksWUFBWSxDQUFDLFNBQVMsS0FBSyxRQUFRLEVBQUU7WUFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxnREFBZ0QsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQztTQUNuSjtRQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLFFBQVEsQ0FBQyxJQUFJLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUNsRCxJQUFJLFlBQVksQ0FBQyxhQUFhLEVBQUU7WUFDNUIsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDaEU7UUFDRCxJQUFJLFlBQVksQ0FBQyxrQkFBa0IsRUFBRTtZQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxZQUFZLENBQUMsc0JBQXNCLEVBQUU7WUFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0SDtRQUNELElBQUksWUFBWSxDQUFDLGdCQUFnQixFQUFFO1lBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLFlBQVksQ0FBQyxXQUFXLEVBQUU7WUFDMUIsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzVEO1FBQ0QsSUFBSSxZQUFZLENBQUMsSUFBSSxFQUFFO1lBQ25CLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ2xFO1FBQ0QsSUFBSSxZQUFZLENBQUMsU0FBUyxFQUFFO1lBQ3hCLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN4RDtRQUNELElBQUksWUFBWSxDQUFDLGFBQWEsRUFBRTtZQUM1QixRQUFRLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsaURBQWlELENBQUMsQ0FBQyxDQUFDO1NBQ25KO1FBQ0QsSUFBSSxZQUFZLENBQUMsa0JBQWtCLEVBQUU7WUFDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksWUFBWSxDQUFDLHFCQUFxQixFQUFFO1lBQ3BDLFFBQVEsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDcEg7UUFFRCxJQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pDLElBQU0sV0FBVyxHQUFHO1lBQ2hCLE9BQU8sRUFBRSxJQUFJLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxhQUFhLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3JFLEdBQUcsQ0FBQyxjQUFjLEVBQUUsbUNBQW1DLENBQUM7WUFDN0QsZUFBZSxFQUFFLElBQUk7U0FDeEIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsbUNBQW1DLEVBQUUsV0FBVyxDQUFDLENBQUM7SUFDbEcsQ0FBQztJQUVELG9DQUFjLEdBQWQsVUFBZSxPQUFPLEVBQUUsSUFBSSxFQUFFLGtCQUFrQjtRQUFoRCxpQkF1QkM7UUF0QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxXQUFXLEdBQUcsbUNBQW1DLENBQUM7WUFDeEQsSUFBTSxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDakMsSUFBTSxLQUFLLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNyQyxLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLEdBQUcsUUFBUSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO2lCQUNqRyxTQUFTLENBQUMsVUFBQSxZQUFZO2dCQUNuQixLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsRUFBRSxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO3FCQUNsSyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNmLElBQU0sY0FBYyxHQUFHLEVBQUUsQ0FBQztvQkFDMUIsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDckMsSUFBTSxTQUFTLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDM0MsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFNBQVMsR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLFFBQVEsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQzt5QkFDckcsU0FBUyxDQUFDLFVBQUEsY0FBYzt3QkFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDOUIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO3dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzFCLENBQUMsQ0FBQyxDQUFDO2dCQUNYLENBQUMsQ0FBQyxDQUFDO1lBQ1gsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBbUIsR0FBbkI7UUFBQSxpQkFjQztRQWJHLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN2QixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQztpQkFDN0QsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDZixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsdUJBQXVCLElBQUksUUFBUSxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixFQUFFO29CQUNwRyxhQUFhLEdBQUcsUUFBUSxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDO2lCQUN0RTtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0NBQWtCLEdBQWxCLFVBQW1CLEVBQU87UUFBMUIsaUJBY0M7UUFiRyxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDO2lCQUNsRSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxxQkFBcUIsSUFBSSxRQUFRLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFO29CQUMzRixXQUFXLEdBQUcsUUFBUSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQztpQkFDN0Q7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUFpQixHQUFqQixVQUFrQixFQUFPO1FBQXpCLGlCQVVDO1FBVEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLHFCQUFxQixDQUFDLHFCQUFxQixDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDO2lCQUNyRSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwyQ0FBcUIsR0FBckIsVUFBc0IsZ0JBQXVCO1FBQ3pDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLGVBQWU7WUFDcEMsSUFBSSxlQUFlLENBQUMsbUJBQW1CLElBQUksZUFBZSxDQUFDLG1CQUFtQixLQUFLLEVBQUUsRUFBRTtnQkFDbkYsT0FBTzthQUNWO1lBRUQsZUFBZSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztRQUVILE9BQU8sZ0JBQWdCLENBQUM7SUFDNUIsQ0FBQztJQUVELCtDQUF5QixHQUF6QixVQUEwQixPQUFPO1FBQWpDLGlCQXFCQztRQXBCRyxJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFNLFFBQVEsR0FBRyxDQUFDO2dCQUNkLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixLQUFLLEVBQUUsVUFBVTthQUNwQixFQUFFO2dCQUNDLEdBQUcsRUFBRSxpQkFBaUI7Z0JBQ3RCLEtBQUssRUFBRSxNQUFNO2FBQ2hCLENBQUMsQ0FBQztRQUNILE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLE9BQU8sR0FBRyxXQUFXLEVBQUUsUUFBUSxDQUFDO2lCQUMzRixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxlQUFlLElBQUksUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUU7b0JBQzdFLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO2lCQUM3RDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ25DLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1Q0FBaUIsR0FBakIsVUFBa0IsT0FBTztRQUF6QixpQkFxQkM7UUFwQkcsSUFBSSxtQkFBbUIsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBTSxRQUFRLEdBQUcsQ0FBQztnQkFDZCxHQUFHLEVBQUUsV0FBVztnQkFDaEIsS0FBSyxFQUFFLFVBQVU7YUFDcEIsRUFBRTtnQkFDQyxHQUFHLEVBQUUsaUJBQWlCO2dCQUN0QixLQUFLLEVBQUUsTUFBTTthQUNoQixDQUFDLENBQUM7UUFDSCxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLEVBQUUsUUFBUSxDQUFDO2lCQUM3RSxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxjQUFjLEVBQUU7b0JBQ3JDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO2lCQUN2RDtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ25DLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw2Q0FBdUIsR0FBdkI7UUFBQSxpQkFjQztRQWJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQU0sUUFBUSxHQUFHLENBQUM7b0JBQ2QsR0FBRyxFQUFFLGdCQUFnQjtvQkFDckIsS0FBSyxFQUFFLE1BQU07aUJBQ2hCLENBQUMsQ0FBQztZQUNILEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxvQkFBb0IsRUFBRSxRQUFRLENBQUM7aUJBQ3hFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFtQixHQUFuQjtRQUFBLGlCQWNDO1FBYkcsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDO2lCQUM3RCxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyx1QkFBdUIsSUFBSSxRQUFRLENBQUMsdUJBQXVCLENBQUMsY0FBYyxFQUFFO29CQUNqRyxhQUFhLEdBQUcsUUFBUSxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQztpQkFDbkU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDN0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFDQUFlLEdBQWYsVUFBZ0IsY0FBc0I7UUFBdEMsaUJBZUM7UUFkRyxJQUFJLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM1QixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsYUFBYSxHQUFHLGNBQWMsRUFBRSxJQUFJLENBQUM7aUJBQzlFLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ2YsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLHNCQUFzQixJQUFJLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO29CQUM1RixRQUFRLENBQUMsc0JBQXNCLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRTtvQkFDNUQsa0JBQWtCLEdBQUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7aUJBQ25GO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDbEMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDRDQUFzQixHQUF0QjtRQUFBLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQ3ZGLElBQUksbUJBQW1CLEdBQUcsRUFBRSxDQUFDO2dCQUM3QixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsNkJBQTZCO29CQUNsRCxLQUFLLENBQUMsUUFBUSxDQUFDLDZCQUE2QixDQUFDLHdCQUF3QixDQUFDLEVBQUU7b0JBQ3hFLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyw2QkFBNkIsQ0FBQyx3QkFBd0IsQ0FBQztpQkFDekY7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUNuQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUM7Z0JBQzNFLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQWp4RCtCLG1CQUFtQjtnQkFDNUIsVUFBVTtnQkFDVCxXQUFXO2dCQUNSLGNBQWM7Z0JBQ1QsbUJBQW1CO2dCQUNsQyxVQUFVO2dCQUNKLFVBQVU7OztJQVR4QixXQUFXO1FBSnZCLFVBQVUsQ0FBQztZQUNSLFVBQVUsRUFBRSxNQUFNO1NBQ3JCLENBQUM7T0FFVyxXQUFXLENBcXhEdkI7c0JBenlERDtDQXl5REMsQUFyeERELElBcXhEQztTQXJ4RFksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIGlzRGV2TW9kZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEVudGl0eUFwcERlZlNlcnZpY2UgfSBmcm9tICcuL2VudGl0eS5hcHBkZWYuc2VydmljZSc7XHJcbmltcG9ydCB7IFV0aWxTZXJ2aWNlIH0gZnJvbSAnLi91dGlsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaGFyaW5nU2VydmljZSB9IGZyb20gJy4vc2hhcmluZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IG90bW1TZXJ2aWNlc0NvbnN0YW50cyB9IGZyb20gJy4uL2NvbmZpZy9vdG1tU2VydmljZS5jb25zdGFudCc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vY29uZmlnL2NvbmZpZyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPVE1NTVBNRGF0YVR5cGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9PVE1NTVBNRGF0YVR5cGVzJztcclxuaW1wb3J0IHsgUWRzU2VydmljZSB9IGZyb20gJy4uLy4uL3VwbG9hZC9zZXJ2aWNlcy9xZHMuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpbGVUeXBlcyB9IGZyb20gJy4uL29iamVjdHMvRmlsZVR5cGVzJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvYXBwbGljYXRpb24uY29uZmlnLmNvbnN0YW50cyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBPVE1NU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHV0aWxTZXJ2aWNlOiBVdGlsU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgIHB1YmxpYyBxZHNTZXJ2aWNlOiBRZHNTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIHJlbmRpdGlvbkNvdW50ID0gMDtcclxuICAgIG90bW1TZXNzaW9uRXhpc3RzID0gZmFsc2U7XHJcbiAgICBleGVjdXRlZENvdW50ID0gMDtcclxuICAgIGludGVydmFsSUQ7XHJcbiAgICBpc1Nlc3Npb25NYXNraW5nID0gZmFsc2U7XHJcbiAgICBJTlRFUlZBTF9ERUxBWSA9IDE1MDAwO1xyXG4gICAgbWFza2luZ1VzZXJJZDtcclxuICAgIFNUQU5EQVJEX1NFU1NJT05fVElNRU9VVF9JTlRFUlZBTCA9IDYwMDAwMDtcclxuICAgIHN0YW5kYXJkSW50ZXJ2YWxJRDtcclxuICAgIHVzZXJEZXRhaWxzT2JqID0ge1xyXG4gICAgICAgIGVtYWlsOiAnJ1xyXG4gICAgfTtcclxuXHJcbiAgICBnZXRTZXNzaW9uUmVzcG9uc2U7XHJcbiAgICBxZHNTZXNzaW9uUmVzcG9uc2VcclxuXHJcbiAgICBnZXRBc3NldHNieUFzc2V0VXJsKHVybCwgcGFyYW1ldGVycykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldE1hc2tpbmcoaXNNYXNraW5nKSB7XHJcbiAgICAgICAgdGhpcy5pc1Nlc3Npb25NYXNraW5nID0gaXNNYXNraW5nO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1hc2tpbmcoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTZXNzaW9uTWFza2luZztcclxuICAgIH1cclxuXHJcbiAgICBnZXRNYXNraW5nVHlwZSgpIHtcclxuICAgICAgICByZXR1cm4gb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMubWFza2luZ1R5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWVkaWFNYW5hZ2VyVXJsKCkge1xyXG4gICAgICAgIHJldHVybiBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy5tZWRpYU1hbmFnZXJDb25maWdVcmw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SmVzc2lvbklkRnJvbVdlYlNlcnZlcigpIHtcclxuICAgICAgICByZXR1cm4gb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuaHR0cE9UTU1UaWNrZXQ7XHJcbiAgICB9XHJcblxyXG4gICAgZmlyZUdldFJlc3RSZXF1ZXN0KHVybCwgcGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5nZXQodGhpcy5mb3JtUmVzdFVybCh1cmwsIHBhcmFtZXRlcnMpLCBodHRwT3B0aW9ucylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZmlyZVBvc3RSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMsIGNvbnRlbnRUeXBlLCBoZWFkZXJPYmplY3QpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IGNvbnRlbnRUeXBlXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5odHRwLnBvc3QodGhpcy5mb3JtUmVzdFVybCh1cmwsIG51bGwsIGZhbHNlKSwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUmVxdWVzdGZvckpzZXNzaW9uQ29va2llKHVybCwgcGFyYW1ldGVycywgY29udGVudFR5cGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgIHJlc3BvbnNlVHlwZTogJ3RleHQnIGFzICd0ZXh0JyxcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBjb250ZW50VHlwZSxcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmh0dHAucG9zdCh1cmwsIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hhcmVBc3NldHNQb3N0UmVxdWVzdChvdG1tYXBpdXJsLCBwYXJhbWV0ZXJzLCBjb250ZW50VHlwZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgbWVkaWFNYW5hZ2VyQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKTtcclxuICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICAgICAgaWYgKCFtZWRpYU1hbmFnZXJDb25maWcpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZWRpYSBNYW5hZ2VyIGNvbmZpZyBub3QgZm91bmQnKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmxUb0JlRmlyZWQgPSBiYXNlVXJsICsgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLm90bW1hcGlCYXNlVXJsICsgbWVkaWFNYW5hZ2VyQ29uZmlnLmFwaVZlcnNpb24gKyAnLycgKyBvdG1tYXBpdXJsO1xyXG5cclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IGNvbnRlbnRUeXBlLFxyXG4gICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpXHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybFRvQmVGaXJlZCwgcGFyYW1ldGVycywgaHR0cE9wdGlvbnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlRGVsZXRlUmVzdFJlcXVlc3QodXJsLCBodHRwT3B0aW9ucz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgIHJlc3BvbnNlVHlwZTogJ3RleHQnIGFzICd0ZXh0JyxcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmh0dHAuZGVsZXRlKHRoaXMuZm9ybVJlc3RVcmwodXJsLCBudWxsLCBmYWxzZSksIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUGF0Y2hSZXN0UmVxdWVzdCh1cmwsIHBhcmFtZXRlcnMsIGNvbnRlbnRUeXBlLCBoZWFkZXJPYmplY3QpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ1gtUmVxdWVzdGVkLUJ5Jywgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpKVxyXG4gICAgICAgICAgICAgICAgLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL2pzb24nKSxcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5odHRwLnBhdGNoKHRoaXMuZm9ybVJlc3RVcmwodXJsLCBudWxsLCBmYWxzZSksIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBmaXJlUHV0UmVzdFJlcXVlc3QodXJsLCBwYXJhbWV0ZXJzLCBjb250ZW50VHlwZSwgaGVhZGVyT2JqZWN0OiBIdHRwSGVhZGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IHBvc3RSZXE6IE9ic2VydmFibGU8YW55PjtcclxuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKCkuc2V0KCdYLVJlcXVlc3RlZC1CeScsIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSlcclxuICAgICAgICAgICAgICAgIC5zZXQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKSxcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBwb3N0UmVxID0gdGhpcy5odHRwLnB1dCh0aGlzLmZvcm1SZXN0VXJsKHVybCwgbnVsbCksIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBwb3N0UmVxLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSwgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuY2hlY2tQU1Nlc3Npb24oKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvb2tpZShjbmFtZSkge1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSBjbmFtZSArICc9JztcclxuICAgICAgICBjb25zdCBjb29raWVzID0gZG9jdW1lbnQuY29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICAgICAgZm9yIChjb25zdCBjb29raWUgb2YgY29va2llcykge1xyXG4gICAgICAgICAgICBsZXQgYyA9IGNvb2tpZTtcclxuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09PSAnICcpIHtcclxuICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoYy5pbmRleE9mKG5hbWUpID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZS5sZW5ndGgsIGMubGVuZ3RoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybVJlc3RQYXJhbXMocGFyYW1ldGVycykge1xyXG4gICAgICAgIGxldCBwYXJhbSA9ICc/JztcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBhcmFtZXRlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKHBhcmFtZXRlcnNbaV1bJ2tleSddID09PSAnanNvbicpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtICs9IGkgPT09IDAgPyBwYXJhbWV0ZXJzW2ldLnZhbHVlLmtleSArICc9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShwYXJhbWV0ZXJzW2ldLnZhbHVlLnZhbHVlKSkgOlxyXG4gICAgICAgICAgICAgICAgICAgICcmJyArIHBhcmFtZXRlcnNbaV0udmFsdWUua2V5ICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHBhcmFtZXRlcnNbaV0udmFsdWUudmFsdWUpKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtICs9IGkgPT09IDAgPyBwYXJhbWV0ZXJzW2ldLmtleSArICc9JyArIHBhcmFtZXRlcnNbaV0udmFsdWUgOiAnJicgKyBwYXJhbWV0ZXJzW2ldLmtleSArICc9JyArIHBhcmFtZXRlcnNbaV0udmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHBhcmFtO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1SZXN0VXJsKG90bW1hcGl1cmwsIHVybHBhcmFtLCBpc1JlbmRpdGlvbj8pIHtcclxuICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcblxyXG4gICAgICAgIGlmIChpc1JlbmRpdGlvbiA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gYmFzZVVybCArIG90bW1hcGl1cmw7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgbWVkaWFNYW5hZ2VyQ29uZmlnID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKTtcclxuICAgICAgICAgICAgaWYgKCFtZWRpYU1hbmFnZXJDb25maWcpIHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignTWVkaWEgTWFuYWdlciBjb25maWcgbm90IGZvdW5kJyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGJhc2VVcmwgKyBvdG1tU2VydmljZXNDb25zdGFudHMub3RtbWFwaUJhc2VVcmwgK1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5hcGlWZXJzaW9uICsgJy8nICsgb3RtbWFwaXVybCArXHJcbiAgICAgICAgICAgICAgICAodXJscGFyYW0gIT0gbnVsbCA/IHRoaXMuZm9ybVJlc3RQYXJhbXModXJscGFyYW0pIDogJycpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkb1Bvc3RSZXF1ZXN0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0LCBpc09ubHlSZXNwb25zZSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChoZWFkZXJPYmplY3QgJiYgaGVhZGVyT2JqZWN0ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT2JqZWN0KS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhlYWRlck9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgcGFyYW1ldGVycywgaGVhZGVyT3B0aW9ucykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkb1B1dFJlcXVlc3QodXJsLCBwYXJhbWV0ZXJzLCBoZWFkZXJPYmplY3QsIGlzT25seVJlc3BvbnNlKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKGhlYWRlck9iamVjdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5odHRwLnB1dCh1cmwsIHBhcmFtZXRlcnMsIGhlYWRlck9iamVjdCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5odHRwLnB1dCh1cmwsIHBhcmFtZXRlcnMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihlcnJvcikpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldEV4cG9ydEpvYigpIHtcclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW1zID0gW3tcclxuXHJcbiAgICAgICAgICAgIGtleTogJ2xvYWRfYXNzZXRfZGV0YWlscycsXHJcbiAgICAgICAgICAgIHZhbHVlOiAndHJ1ZSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ3NvcnQnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2Rlc2NfbGFzdF91cGRhdGVkX2RhdGVfdGltZSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xvYWRfam9iX2RldGFpbHMnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ3RydWUnXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBrZXk6ICdsYXN0X3VwZGF0ZWRfZGF0ZV90aW1lJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdUT0RBWSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xpbWl0JyxcclxuICAgICAgICAgICAgdmFsdWU6ICc1MDAwJ1xyXG4gICAgICAgIH1dO1xyXG5cclxuICAgICAgICB0aGlzLmdldEV4cG9ydEpvYnMocGFyYW1zKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5qb2JzX3Jlc291cmNlICYmIHJlc3BvbnNlLmpvYnNfcmVzb3VyY2UuY29sbGVjdGlvbl9zdW1tYXJ5ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zICYmXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuam9ic19yZXNvdXJjZS5jb2xsZWN0aW9uX3N1bW1hcnkuYWN0dWFsX2NvdW50X29mX2l0ZW1zID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2V0IGV4cG9ydCByZXNwb25zZVwiKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJObyBpdGVtc1wiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJVbmFibGUgdG8gZ2V0IGRhdGEgZnJvbSBRRFMuXCIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgY2hlY2tPdG1tU2Vzc2lvbihpc1VwbG9hZD8sIG9wZW5RRFNUcmF5Pyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgLy8gTVNWLTIzOVxyXG4gICAgICAgICAgICAvLyBpZiAoIWlzVXBsb2FkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMub3RtbVNlc3Npb25VcmwsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShnZXRTZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZ2V0U2Vzc2lvblJlc3BvbnNlICYmIGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlICYmIGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIE1TVi0yMzlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlID0gZ2V0U2Vzc2lvblJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZCA9IGdldFNlc3Npb25SZXNwb25zZS5zZXNzaW9uX3Jlc291cmNlLnNlc3Npb24uaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCBnZXRTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXJ0T1RNTVNlc3Npb24odHJ1ZSkgLy8gTVZTLTIzOSB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyB9IGVsc2UgeyAvLyBNU1YgLTIzOVxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuZW5hYmxlUURTICYmXHJcbiAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5xZHNTZXJ2aWNlLmdldFFEU1Nlc3Npb24ob3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShxZHNTZXNzaW9uUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIE1TVi0yMzlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2Vzc2lvblJlc3BvbnNlID0gcWRzU2Vzc2lvblJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRFeHBvcnRKb2IoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5jb25uZWN0VG9RRFMocWRzU2Vzc2lvblJlc3BvbnNlLnRyYW5zZmVyc2NoZW1lc19zZXNzaW9uX3Jlc291cmNlLnRyYW5zZmVyX3Nlc3Npb24sIGlzVXBsb2FkKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5nZXRTZXNzaW9uUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBxZHNTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdTb21ldGhpbmcgd2VudCB3cm9uZyB3aGlsZSBnZXR0aW5nIFFEUyBzZXNzaW9uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZ2V0U2Vzc2lvblJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldExvZ2dlZEluVXNlckRldGFpbHModXNlcklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMudXNlckRldGFpbHNVcmwgKyB1c2VySWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcl9yZXNvdXJjZSAmJiByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyRGV0YWlsc09iai5lbWFpbCA9IHJlc3BvbnNlLnVzZXJfcmVzb3VyY2UudXNlci5lbWFpbF9hZGRyZXNzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlLnVzZXJfcmVzb3VyY2UudXNlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ1NvbWV0aGluZyB3ZW50IHdyb25nIHdoaWxlIGdldHRpbmcgdXNlciBkZXRhaWxzIGZyb20gT1RNTS4nKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbWF0Y2hTZXNzaW9uQXR0cmlidXRlcyhjb21wYXJlVXNlcklkLCBsb2dpbm5hbWUpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySUQoKTtcclxuICAgICAgICBsZXQgZmxhZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQodXNlcklkKSAmJiB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoY29tcGFyZVVzZXJJZCkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNFcXVhbFdpdGhDYXNlKHVzZXJJZCwgY29tcGFyZVVzZXJJZCkgfHwgKGNvbXBhcmVVc2VySWQuaW5kZXhPZignQCcpID49IDAgJiYgdGhpcy51dGlsU2VydmljZS5pc0VxdWFsV2l0aENhc2UodXNlcklkLCBjb21wYXJlVXNlcklkLnNwbGl0KCdAJylbMF0pKSkge1xyXG4gICAgICAgICAgICAgICAgZmxhZyA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZCh1c2VySWQpICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChsb2dpbm5hbWUpKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzRXF1YWxXaXRoQ2FzZSh1c2VySWQsIGxvZ2lubmFtZSkgfHwgKGxvZ2lubmFtZS5pbmRleE9mKCdAJykgPj0gMCAmJiB0aGlzLnV0aWxTZXJ2aWNlLmlzRXF1YWxXaXRoQ2FzZSh1c2VySWQsIGxvZ2lubmFtZS5zcGxpdCgnQCcpWzBdKSkpIHtcclxuICAgICAgICAgICAgICAgIGZsYWcgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmxhZztcclxuICAgIH1cclxuXHJcbiAgICBzdGFydE9UTU1TZXNzaW9uKGluaXRpYWxMb2dpbj8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIG9yZ2FuaXphdGlvbjogY29uZmlnLmNvbmZpZy5vcmdhbml6YXRpb25OYW1lLFxyXG4gICAgICAgICAgICAvLyBzb3VyY2VSZXNvdXJjZU5hbWU6ICdfX09URFMjT3JnYW5pemF0aW9uYWwjUGxhdGZvcm0jUmVzb3VyY2VfXycsXHJcbiAgICAgICAgICAgIHNvdXJjZVJlc291cmNlTmFtZTogdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLmFwcGxpY2F0aW9uRGV0YWlscy5PVERTX0FQUFdPUktTX1JFU09VUkNFX05BTUUsXHJcbiAgICAgICAgICAgIHRhcmdldFJlc291cmNlTmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5vdGRzUmVzb3VyY2VOYW1lLFxyXG4gICAgICAgICAgICBpc09yZ1NwYWNlOiAndHJ1ZSdcclxuICAgICAgICB9O1xyXG5cclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hcHBTZXJ2aWNlLmdldE9URFNUaWNrZXRGb3JVc2VyKHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKG90ZHNUaWNrZXRSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHRpY2tldCA9ICdPVERTVGlja2V0PSc7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9rZW4gPSBvdGRzVGlja2V0UmVzcG9uc2UudHVwbGUub2xkLk9URFNBY2Nlc3NUaWNrZXQub3RtbVRpY2tldDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRva2VuICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGlja2V0ICs9IHRva2VuO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG90bW1VcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVVybCA9IG90bW1VcmwuY29uY2F0KCdvdG1tL3V4LWh0bWwvaW5kZXguaHRtbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiAoaW5pdGlhbExvZ2luKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVJlcXVlc3Rmb3JKc2Vzc2lvbkNvb2tpZShvdG1tVXJsLCB0aWNrZXQsIG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuVVJMRU5DT0RFRClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoalNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLm90bW1TZXNzaW9uVXJsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGdldFNlc3Npb25SZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdG1tU2VydmljZXNDb25zdGFudHMuT1RNTV9TRVJWSUNFX1ZBUklBQkxFUy51c2VyU2Vzc2lvbklkID1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2UpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZS5zZXNzaW9uKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5pZCkgP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZS5zZXNzaW9uLmlkIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBsb2dpbk5hbWUgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5sb2dpbl9uYW1lKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi5sb2dpbl9uYW1lIDogJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VySWQgPSB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlKSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChnZXRTZXNzaW9uUmVzcG9uc2Uuc2Vzc2lvbl9yZXNvdXJjZSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbikgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQoZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi51c2VyX2lkKSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZ2V0U2Vzc2lvblJlc3BvbnNlLnNlc3Npb25fcmVzb3VyY2Uuc2Vzc2lvbi51c2VyX2lkIDogJyc7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRMb2dnZWRJblVzZXJEZXRhaWxzKHVzZXJJZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVzZXJSZXNvdXJjZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dHJzTWF0Y2hlZCA9IHRoaXMubWF0Y2hTZXNzaW9uQXR0cmlidXRlcyh1c2VyUmVzb3VyY2Uub3Rkc191c2VyX2lkLGxvZ2luTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghYXR0cnNNYXRjaGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm90bW1TZXNzaW9uRXhpc3RzID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRoaXMuZGVzdHJveU90bW1TZXNzaW9uKCkuc3Vic2NyaWJlKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3RtbVNlc3Npb25FeGlzdHMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuZW5hYmxlUURTICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCAmJiBpbml0aWFsTG9naW4pIHsgLy9NVlMtMjM5ICYmIGluaXRpYWxMb2dpblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucWRzU2VydmljZS5nZXRRRFNTZXNzaW9uKG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocWRzU2Vzc2lvblJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnFkc1NlcnZpY2UuY29ubmVjdFRvUURTKHFkc1Nlc3Npb25SZXNwb25zZS50cmFuc2ZlcnNjaGVtZXNfc2Vzc2lvbl9yZXNvdXJjZS50cmFuc2Zlcl9zZXNzaW9uKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBxZHNTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBtYXgtbGluZS1sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignU29tZXRoaW5nIHdlbnQgd3Jvbmcgd2hpbGUgZ2V0dGluZyBRRFMgc2Vzc2lvbicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLm90bW1TZXNzaW9uRXhpc3RzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vb2JzZXJ2ZXIubmV4dCh0aGlzLm90bW1TZXNzaW9uRXhpc3RzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL29ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodGhpcy5vdG1tU2Vzc2lvbkV4aXN0cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVtYWlsSWRFcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVtYWlsSWRFcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGdldFNlc3Npb25FcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihnZXRTZXNzaW9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGpTZXNzaW9uRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGpTZXNzaW9uRXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgb3Rkc1RpY2tldEVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihvdGRzVGlja2V0RXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVzdHJveU90bW1TZXNzaW9uKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZCA9ICcnO1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVEZWxldGVSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMub3RtbVNlc3Npb25VcmwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdFcnJvciB3aGlsZSBkZWxldGluZyBPVE1NIHNlc3Npb24uJyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRMb2dpbkVtYWlsSWQodXNlcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgbGV0IGVtYWlsSWQgPSAnJztcclxuICAgICAgICAgICAgY29uc3QgcmVxID0gdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnVzZXJEZXRhaWxzVXJsICsgdXNlcklkLCBudWxsKTtcclxuICAgICAgICAgICAgcmVxLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UudXNlcl9yZXNvdXJjZSAmJiByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIgJiZcclxuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIuZW1haWxfYWRkcmVzcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGVtYWlsSWQgPSByZXNwb25zZS51c2VyX3Jlc291cmNlLnVzZXIuZW1haWxfYWRkcmVzcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZW1haWxJZCk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGVtYWlsSWQpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXBsb2FkTWFuaWZlc3QoZmlsZXMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpIHtcclxuICAgICAgICBsZXQgdXBsb2FkTWFuaWZlc3QgPSBudWxsO1xyXG4gICAgICAgIGlmIChmaWxlcykge1xyXG4gICAgICAgICAgICBpZiAoZmlsZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB1cGxvYWRNYW5pZmVzdCA9IHtcclxuICAgICAgICAgICAgICAgICAgICB1cGxvYWRfbWFuaWZlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFzdGVyX2ZpbGVzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogZmlsZXNbMF0ubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlX3R5cGU6IGZpbGVzWzBdLnR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWFzdGVyRmlsZXMgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBtYXN0ZXJGaWxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmlsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZV9uYW1lOiBmaWxlc1tpXS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZV90eXBlOiBmaWxlc1tpXS50eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHVwbG9hZF9tYW5pZmVzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXN0ZXJfZmlsZXM6IG1hc3RlckZpbGVzXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRocm93ICdVbmFibGUgdG8gY3JlYXRlIHVwbG9hZCBtYW5pZmVzdDpcXG4gcGFyYW1ldGVyIGZpbGVzIGlzIGVpdGhlciB1bmRlZmluZWQgb3IgaW52YWxpZC4nO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdXBsb2FkTWFuaWZlc3Q7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlVXBsb2FkTWFuaWZlc3RGb3JSZXZpc2lvbihmaWxlcykge1xyXG4gICAgICAgIGxldCB1cGxvYWRNYW5pZmVzdCA9IG51bGw7XHJcbiAgICAgICAgaWYgKGZpbGVzKSB7XHJcbiAgICAgICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgdXBsb2FkX21hbmlmZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFzdGVyX2ZpbGVzOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlX25hbWU6IGZpbGVzWzBdLm5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBmaWxlc1swXS5hc3NldElkXHJcbiAgICAgICAgICAgICAgICAgICAgfV1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBtYXN0ZXJGaWxlcyA9IG5ldyBBcnJheSgpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBtYXN0ZXJGaWxlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogZmlsZXNbaV0ubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBmaWxlc1tpXS5hc3NldElkXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB1cGxvYWRNYW5pZmVzdCA9IHtcclxuICAgICAgICAgICAgICAgIHVwbG9hZF9tYW5pZmVzdDoge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hc3Rlcl9maWxlczogbWFzdGVyRmlsZXNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHVwbG9hZE1hbmlmZXN0O1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZVRyYW5zZmVyRGV0YWlscyhzY2hlbWU6IHN0cmluZywgZmlsZXM6IEFycmF5PGFueT4pIHtcclxuICAgICAgICBsZXQgdHJhbnNmZXJEZXRhaWxzID0gbnVsbDtcclxuXHJcbiAgICAgICAgY29uc3QgdHJhbnNmZXJGaWxlSXRlbXMgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRyYW5zZmVyRmlsZUl0ZW1zLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgc2NoZW1lX2ZpbGVfaWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgc2NoZW1lX2ZpbGVfbmFtZTogJydcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyYW5zZmVyRGV0YWlscyA9IHtcclxuICAgICAgICAgICAgdHJhbnNmZXJfZGV0YWlsc19wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgdHJhbnNmZXJfZmlsZV9pdGVtczogdHJhbnNmZXJGaWxlSXRlbXMsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zlcl9zY2hlbWU6IHNjaGVtZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRyYW5zZmVyRGV0YWlscztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldFJlcHJlc2VudGF0aW9uKG1ldGFkYXRhRmllbGRzLCBtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YUZpZWxkcywgbWV0YURhdGFGaWVsZFZhbHVlcywgbWV0YWRhdGFNb2RlbCwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZUFzc2V0UmVwcmVzZW50YXRpb24obWV0YWRhdGFGaWVsZHMsIG1ldGFEYXRhRmllbGRWYWx1ZXMsIG1ldGFEYXRhTW9kZWwsIHNlY3VyaXR5UG9saWN5SUQpIHtcclxuICAgICAgICBsZXQgYXNzZXRSZXByZXNlbnRhdGlvbiA9IG51bGw7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGFGaWVsZExpc3QgPSBuZXcgQXJyYXkoKTtcclxuICAgICAgICBsZXQgbWV0YURhdGFFbGVtZW50ID0gbnVsbDtcclxuICAgICAgICBjb25zdCBzZWN1cml0eVBvbGljaWVzID0gW107XHJcblxyXG4gICAgICAgIGlmIChtZXRhZGF0YUZpZWxkcyAmJiBBcnJheS5pc0FycmF5KG1ldGFkYXRhRmllbGRzKSAmJiBtZXRhRGF0YU1vZGVsICYmIG1ldGFEYXRhTW9kZWwgIT0gbnVsbCAmJiBtZXRhRGF0YU1vZGVsICE9PSAnJykge1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZGVmYXVsdFNlY1BvbGljeUlkID0gMTtcclxuICAgICAgICAgICAgY29uc3Qgc2VjdXJpdHlQb2xpY3kgPSBkZWZhdWx0U2VjUG9saWN5SWQ7XHJcbiAgICAgICAgICAgIGlmIChzZWN1cml0eVBvbGljeUlEICYmIHNlY3VyaXR5UG9saWN5SUQgIT0gbnVsbCAmJiBzZWN1cml0eVBvbGljeUlEICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaWRzID0gc2VjdXJpdHlQb2xpY3lJRC5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialsnaWQnXSA9IGlkc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICBzZWN1cml0eVBvbGljaWVzLnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9saWN5ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBkZWZhdWx0U2VjUG9saWN5SWQsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgc2VjdXJpdHlQb2xpY2llcy5wdXNoKHBvbGljeSk7XHJcbiAgICAgICAgICAgICAgICBwb2xpY3kgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGl0ZXJhdGUgYWxsIGNvbmZpZ3VyZWQgbWV0YWRhdGFmaWVsZHMgYW5kIHByZXBhcmUgbWV0YWRhdGEgZmllbGQgY29uZmlndXJhdGlvbiB0byBzZW5kIHdpdGggcmVxdWVzdC5cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBtZXRhZGF0YUZpZWxkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgbWV0YURhdGFFbGVtZW50ID0ge307XHJcbiAgICAgICAgICAgICAgICBtZXRhRGF0YUVsZW1lbnQuaWQgPSBtZXRhZGF0YUZpZWxkc1tpXS5pZDtcclxuICAgICAgICAgICAgICAgIGxldCB0eXBlID0gbWV0YWRhdGFGaWVsZHNbaV0udHlwZTtcclxuICAgICAgICAgICAgICAgIGlmICghdHlwZSB8fCB0eXBlID09IG51bGwgfHwgdHlwZS50cmltKCkgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZSA9ICdjb20uYXJ0ZXNpYS5tZXRhZGF0YS5NZXRhZGF0YUZpZWxkJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC50eXBlID0gdHlwZTtcclxuICAgICAgICAgICAgICAgIC8vIGlmIG1ldGFEYXRhRmllbGRWYWx1ZXMgaXMgZGVmaW5lZCBhbmQgZmllbGQgZnJvbSBlbnRpdHkgaXMgb2YgY2FzY2FkaW5nIHR5cGUgKGVnOiBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiA9ICdicmFuZCcpXHJcbiAgICAgICAgICAgICAgICBpZiAobWV0YURhdGFGaWVsZFZhbHVlcyAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICE9IG51bGwgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgJiZcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRzW2ldLmZpZWxkUmVmLmluZGV4T2YoJ14nKSA9PT0gLTEgJiYgbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl0gIT09ICcnKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBtZXRhZGF0YVZhbHVlID0gbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl07XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gYXJyYXkgb2YgdmFsdWUgaW5zaWRlIHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlID09PSAnY29tLmFydGVzaWEubWV0YWRhdGEuTWV0YWRhdGFUYWJsZUZpZWxkJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZXMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkobWV0YWRhdGFWYWx1ZSkgJiYgbWV0YWRhdGFWYWx1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgbWV0YWRhdGFWYWx1ZS5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZVtpbmRleF0gPSBtZXRhZGF0YVZhbHVlW2luZGV4XS50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZVtpbmRleF0gPSBtZXRhZGF0YVZhbHVlW2luZGV4XS5wYXJzZUludCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBtZXRhZGF0YVZhbHVlW2luZGV4XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMucHVzaCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ3N0cmluZycgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFWYWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YVZhbHVlID0gbWV0YWRhdGFWYWx1ZS50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUpICYmIG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlID09PSAnbnVtYmVyJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YVZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhVmFsdWUgPSBtZXRhZGF0YVZhbHVlLnBhcnNlSW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbWV0YWRhdGFWYWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzLnB1c2godmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC52YWx1ZXMgPSB2YWx1ZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSkgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUgPT09ICdzdHJpbmcnICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFWYWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhVmFsdWUgPSBtZXRhZGF0YVZhbHVlLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy51dGlsU2VydmljZS5pc1ZhbGlkKG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlKSAmJiBtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSA9PT0gJ251bWJlcidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYmIHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YVZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFWYWx1ZSA9IG1ldGFkYXRhVmFsdWUucGFyc2VJbnQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBtZXRhZGF0YVZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhRWxlbWVudC52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobWV0YURhdGFGaWVsZFZhbHVlcyAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICE9IG51bGwgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgJiZcclxuICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZiAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRmllbGRzW2ldLmZpZWxkUmVmLmluZGV4T2YoJ14nKSAhPT0gLTEgJiYgbWV0YURhdGFGaWVsZFZhbHVlc1ttZXRhZGF0YUZpZWxkc1tpXS5maWVsZFJlZl0gIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRzID0gbWV0YWRhdGFGaWVsZHNbaV0uZmllbGRSZWYuc3BsaXQoJ14nKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBpdGVyYXRlIGFsbCBjYXNjYWRlIGZpZWxkc1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IHggPSAwOyB4IDwgZmllbGRzLmxlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBtZXRhZGF0YXZhbHVlID0gbWV0YURhdGFGaWVsZFZhbHVlc1tmaWVsZHNbeF1dO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBlYWNoIGNhc2NhZGVkIGZpZWxkIHZhbHVlIG9ubHkgb25lIHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KG1ldGFkYXRhdmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YXZhbHVlID0gbWV0YWRhdGF2YWx1ZVswXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBvbiBtZXRhZGF0YXZhbHVlIGlzIHJlY2VpdmVkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXRhZGF0YXZhbHVlICE9PSAnJyAmJiBtZXRhZGF0YXZhbHVlICE9IG51bGwgJiYgbWV0YWRhdGF2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uZGl0aW9uIHRvIGNoZWNrIGFscmVhZHkgdmFsdWUgaGFzIGFwcGVuZGVkIGZvciBjYXNjYWRpbmcgZmllbGRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh2YWx1ZS52YWx1ZS52YWx1ZSAhPT0gJycgJiYgdmFsdWUudmFsdWUudmFsdWUgIT0gbnVsbCAmJiB2YWx1ZS52YWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlLnZhbHVlLnZhbHVlID0gdmFsdWUudmFsdWUudmFsdWUgKyAnXicgKyBtZXRhZGF0YXZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZS52YWx1ZS52YWx1ZSA9IG1ldGFkYXRhdmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkZWZ2YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBpZiBkZWZhdWx0IHZhbHVlIGlzIHByZXNlbnRcclxuICAgICAgICAgICAgICAgICAgICBpZiAobWV0YWRhdGFGaWVsZHNbaV0uZGVmYXVsdCAhPT0gJycgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGVmYXVsdCAhPSBudWxsICYmIG1ldGFkYXRhRmllbGRzW2ldLmRlZmF1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmdmFsdWUgPSBtZXRhZGF0YUZpZWxkc1tpXS5kZWZhdWx0O1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnV0aWxTZXJ2aWNlLmlzVmFsaWQobWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUpICYmIG1ldGFkYXRhRmllbGRzW2ldLmRhdGF0eXBlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmdmFsdWUgPSAnTkEnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudXRpbFNlcnZpY2UuaXNWYWxpZChtZXRhZGF0YUZpZWxkc1tpXS5kYXRhdHlwZSkgJiYgbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGUgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZ2YWx1ZSA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHZhbHVlID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGRlZnZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogbWV0YWRhdGFGaWVsZHNbaV0uZGF0YXR5cGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgbWV0YURhdGFFbGVtZW50LnZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YUZpZWxkTGlzdC5wdXNoKG1ldGFEYXRhRWxlbWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGxldCBtZXRhZGF0YU1vZGVsSWQgPSAnJztcclxuICAgICAgICAgICAgaWYgKG1ldGFEYXRhTW9kZWwgJiYgbWV0YURhdGFNb2RlbCAhPSBudWxsICYmIG1ldGFEYXRhTW9kZWwudHJpbSgpICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGFNb2RlbElkID0gbWV0YURhdGFNb2RlbDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRocm93ICdNZXRhRGF0YSBNb2RlbElEIGlzIG1pc3NpbmcuJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBhc3NldFJlcHJlc2VudGF0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgYXNzZXRfcmVzb3VyY2U6IHtcclxuICAgICAgICAgICAgICAgICAgICBhc3NldDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGFfZWxlbWVudF9saXN0OiBtZXRhZGF0YUZpZWxkTGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogbWV0YWRhdGFNb2RlbElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWN1cml0eV9wb2xpY3lfbGlzdDogc2VjdXJpdHlQb2xpY2llc1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aHJvdyAnVW5hYmxlIHRvIGNyZWF0ZSBhc3NldCByZXByZXNlbnRhdGlvbidcclxuICAgICAgICAgICAgKyAnIHRoZSBwYXJhbWV0ZXJzIG1ldGFkYXRhRmllbGRzICYgbWV0YURhdGFGaWVsZFZhbHVlcyBhcmUgbm90IGNvcnJlY3Qgb3IgdW5kZWZpbmVkLic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhc3NldFJlcHJlc2VudGF0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIHVwbG9hZEFzc2V0cyhmaWxlcywgbWV0YWRhdGFGaWVsZHMsIG1ldGFkYXRhRmllbGRWYWx1ZXMsIHBhcmVudEZvcmxkZXJJRCwgaW1wb3J0VGVtcGxhdGVJRCwgbWV0YWRhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmFzc3NldFVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuICAgICAgICBpZiAoZmlsZXMgJiYgZmlsZXMgIT0gbnVsbCAmJiBtZXRhZGF0YUZpZWxkVmFsdWVzICYmIG1ldGFkYXRhRmllbGRWYWx1ZXMgIT0gbnVsbCAmJiBtZXRhZGF0YUZpZWxkcyAmJiBwYXJlbnRGb3JsZGVySUQgJiZcclxuICAgICAgICAgICAgaW1wb3J0VGVtcGxhdGVJRCkge1xyXG5cclxuICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YUZpZWxkcywgbWV0YWRhdGFGaWVsZFZhbHVlcywgbWV0YWRhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkpO1xyXG4gICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2Fzc2V0X3JlcHJlc2VudGF0aW9uJywgYmxvYik7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgncGFyZW50X2ZvbGRlcl9pZCcsIHBhcmVudEZvcmxkZXJJRCk7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X3RlbXBsYXRlX2lkJywgaW1wb3J0VGVtcGxhdGVJRCk7XHJcbiAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbm9fY29udGVudCcsICdmYWxzZScpO1xyXG4gICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcykpO1xyXG4gICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2NvbnRlbnRdLCB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ21hbmlmZXN0JywgYmxvYik7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgZmlsZSBvZiBmaWxlcykge1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdmaWxlcycsIGZpbGUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhyb3cgJ1VuYWJsZSB0byB1cGxvYWQgYXNzZXRzOlxcbmJlY2F1c2UgdGhlIEZpbGVzIGFyZSBtZXRhIGRhdGEgdmFsdWVzIGFyZSBub3QgcGFzc2VkLic7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhZGF0YSwgbWV0YURhdGFNb2RlbCwgc2VjdXJpdHlQb2xpY3lJRCkge1xyXG4gICAgICAgIGxldCBhc3NldFJlcHJlc2VudGF0aW9uID0gbnVsbDtcclxuICAgICAgICBjb25zdCBtZXRhZGF0YUVsZW1lbnRMaXN0ID0gbmV3IEFycmF5KCk7XHJcbiAgICAgICAgY29uc3Qgc2VjdXJpdHlQb2xpY2llcyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAobWV0YWRhdGEgJiYgbWV0YURhdGFNb2RlbCAmJiBtZXRhRGF0YU1vZGVsICE9IG51bGwgJiYgbWV0YURhdGFNb2RlbCAhPT0gJycpIHtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGRlZmF1bHRTZWNQb2xpY3lJZCA9IDE7XHJcbiAgICAgICAgICAgIGlmIChzZWN1cml0eVBvbGljeUlEICYmIHNlY3VyaXR5UG9saWN5SUQgIT0gbnVsbCAmJiBzZWN1cml0eVBvbGljeUlEICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaWRzID0gc2VjdXJpdHlQb2xpY3lJRC5zcGxpdCgnOycpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBpZCBvZiBpZHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialsnaWQnXSA9IE51bWJlcihpZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VjdXJpdHlQb2xpY2llcy5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvbGljeSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogZGVmYXVsdFNlY1BvbGljeUlkLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHNlY3VyaXR5UG9saWNpZXMucHVzaChwb2xpY3kpO1xyXG4gICAgICAgICAgICAgICAgcG9saWN5ID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IG1ldGFkYXRhTW9kZWxJZCA9ICcnO1xyXG4gICAgICAgICAgICBpZiAobWV0YURhdGFNb2RlbCAmJiBtZXRhRGF0YU1vZGVsICE9IG51bGwgJiYgbWV0YURhdGFNb2RlbC50cmltKCkgIT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YU1vZGVsSWQgPSBtZXRhRGF0YU1vZGVsO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZXRhZGF0YSBNb2RlbElEIGlzIG1pc3NpbmcuJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChtZXRhZGF0YSAmJiBtZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QgJiYgQXJyYXkuaXNBcnJheShtZXRhZGF0YS5tZXRhZGF0YV9lbGVtZW50X2xpc3QpKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IG1ldGFkYXRhR3JvdXAgb2YgbWV0YWRhdGEubWV0YWRhdGFfZWxlbWVudF9saXN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBtZXRhZGF0YUVsZW1lbnQgb2YgbWV0YWRhdGFHcm91cC5tZXRhZGF0YV9lbGVtZW50X2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG1ldGFkYXRhRWxlbWVudCAmJiBtZXRhZGF0YUVsZW1lbnQuaWQgPT09ICdNUE0uVVRJTFMuREFUQV9UWVBFJyAmJiBtZXRhZGF0YUVsZW1lbnQudmFsdWUgJiYgbWV0YWRhdGFFbGVtZW50LnZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YUVsZW1lbnQudmFsdWUudmFsdWUudmFsdWUgPSBPVE1NTVBNRGF0YVR5cGVzLkFTU0VUO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhRWxlbWVudExpc3QucHVzaChtZXRhZGF0YUVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgYXNzZXRSZXByZXNlbnRhdGlvbiA9IHtcclxuICAgICAgICAgICAgICAgIGFzc2V0X3Jlc291cmNlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXNzZXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWV0YWRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFkYXRhX2VsZW1lbnRfbGlzdDogbWV0YWRhdGFFbGVtZW50TGlzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YV9tb2RlbF9pZDogbWV0YWRhdGFNb2RlbElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWN1cml0eV9wb2xpY3lfbGlzdDogc2VjdXJpdHlQb2xpY2llc1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGBVbmFibGUgdG8gY3JlYXRlIGFzc2V0IHJlcHJlc2VudGF0aW9uOlxyXG4gICAgICAgICAgICB0aGUgcGFyYW1ldGVycyBtZXRhZGF0YUZpZWxkcyAmIG1ldGFEYXRhRmllbGRWYWx1ZXMgYXJlIG5vdCBjb3JyZWN0IG9yIHVuZGVmaW5lZC5gKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGFzc2V0UmVwcmVzZW50YXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgdXBsb2FkU2luZ2xlQXNzZXQoZmlsZXMsIG1ldGFEYXRhRmllbGRWYWx1ZXMsIHBhcmVudEZvbGRlcklELCBpbXBvcnRUZW1wbGF0ZUlELCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmFzc3NldFVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuICAgICAgICBpZiAoKGZpbGVzICYmIEFycmF5LmlzQXJyYXkoZmlsZXMpKSAmJiBtZXRhRGF0YUZpZWxkVmFsdWVzICYmIHBhcmVudEZvbGRlcklEICYmIGltcG9ydFRlbXBsYXRlSUQpIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBmaWxlIG9mIGZpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5mb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkpO1xyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdhc3NldF9yZXByZXNlbnRhdGlvbicsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdwYXJlbnRfZm9sZGVyX2lkJywgcGFyZW50Rm9sZGVySUQpO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdpbXBvcnRfdGVtcGxhdGVfaWQnLCBpbXBvcnRUZW1wbGF0ZUlEKTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbm9fY29udGVudCcsICdmYWxzZScpO1xyXG4gICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpKTtcclxuICAgICAgICAgICAgICAgIGJsb2IgPSBuZXcgQmxvYihbY29udGVudF0sIHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnbWFuaWZlc3QnLCBibG9iKTtcclxuICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgnZmlsZXMnLCBmaWxlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZG9Qb3N0UmVxdWVzdCh1cmwsIHBhcmFtLCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwbG9hZCBhc3NldHM6XFxuYmVjYXVzZSB0aGUgcGFyYW1ldGVycyBhcmUgZWl0aGVyIGluY29ycmVjdCBvciB1bmRlZmluZWQuJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVPVE1NSm9iKGZpbGVOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhc2VVcmwgPSBpc0Rldk1vZGUoKSA/ICcuLycgOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLnVybDtcclxuICAgICAgICAgICAgY29uc3QgdmVyc2lvbiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkuYXBpVmVyc2lvbjtcclxuICAgICAgICAgICAgY29uc3QgdXJsID0gYmFzZVVybCArICdvdG1tYXBpLycrdmVyc2lvbisnL2pvYnMvaW1wb3J0cyc7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuTVVMVElQQVJURk9STURBVEEsXHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtT1RNTS1SZXF1ZXN0JzogJ3RydWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICdYLVJlcXVlc3RlZC1CeSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5PVE1NX1NFUlZJQ0VfVkFSSUFCTEVTLnVzZXJTZXNzaW9uSWQudG9TdHJpbmcoKSxcclxuICAgICAgICAgICAgICAgICAgICAnWC1VU0VTLUNIVU5LUyc6ICdmYWxzZSdcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGVOYW1lKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaHR0cC5wb3N0KHVybCwgZm9ybURhdGEsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbXBvcnRPVE1NSm9iKGZpbGVzLCBtZXRhZGF0YUZpZWxkcywgbWV0YURhdGFGaWVsZFZhbHVlcywgcGFyZW50Rm9ybGRlcklELCBpbXBvcnRUZW1wbGF0ZUlELCBtZXRhZGF0YU1vZGVsLFxyXG4gICAgICAgIHNlY3VyaXR5UG9saWN5LCBpbXBvcnRKb2JJZCwgaXNSZXZpc2lvbj8pIHtcclxuXHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5mb3JtUmVzdFVybCgoaXNSZXZpc2lvbiA/IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5jaGVja2luVXJsIDogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCkgKyAnLycgKyBpbXBvcnRKb2JJZCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG4gICAgICAgIGxldCBjb250ZW50ID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKChmaWxlcyAmJiBBcnJheS5pc0FycmF5KGZpbGVzKSkgJiYgbWV0YURhdGFGaWVsZFZhbHVlcyAmJlxyXG4gICAgICAgICAgICBwYXJlbnRGb3JsZGVySUQgJiYgaW1wb3J0VGVtcGxhdGVJRCkge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5mb3JtQXNzZXRSZXByZXNlbnRhdGlvbihtZXRhRGF0YUZpZWxkVmFsdWVzLCBtZXRhZGF0YU1vZGVsLCBzZWN1cml0eVBvbGljeSkpO1xyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFpc1JldmlzaW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdhc3NldF9yZXByZXNlbnRhdGlvbicsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgncGFyZW50X2ZvbGRlcl9pZCcsIHBhcmVudEZvcmxkZXJJRCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ltcG9ydF9qb2JfaWQnLCBpbXBvcnRKb2JJZCk7XHJcbiAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGVzW2ldLm5hbWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpc1JldmlzaW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3RGb3JSZXZpc2lvbihmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50ID0gSlNPTi5zdHJpbmdpZnkodGhpcy5jcmVhdGVVcGxvYWRNYW5pZmVzdChmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtjb250ZW50XSwge1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuSlNPTkRBVEFcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgcGFyYW0uYXBwZW5kKCdtYW5pZmVzdCcsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS5lbmFibGVRRFMgJiYgb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMuZmluZFFEU0NsaWVudCl7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVHJhbnNmZXJEZXRhaWxzKCdRRFMnLCBmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJsb2IgPSBuZXcgQmxvYihbY29udGVudF0sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLkNPTlRFTlRfVFlQRS5KU09OREFUQVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHBhcmFtLmFwcGVuZCgndHJhbnNmZXJfZGV0YWlscycsIGJsb2IpO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gY29udGVudCA9IEpTT04uc3RyaW5naWZ5KHRoaXMuY3JlYXRlVXBsb2FkTWFuaWZlc3QoZmlsZXMpKTtcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVzJyxmaWxlc1tpXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRvUHV0UmVxdWVzdCh1cmwsIHBhcmFtLCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignVW5hYmxlIHRvIHVwbG9hZCBhc3NldHM6XFxuYmVjYXVzZSB0aGUgcGFyYW1ldGVycyBhcmUgZWl0aGVyIGluY29ycmVjdCBvciB1bmRlZmluZWQuJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZUFzc2V0TG9ja1N0YXRlKGFzc2V0TGlzdCkge1xyXG4gICAgICAgIGxldCBmaWxlTmFtZSA9ICcnO1xyXG4gICAgICAgIGxldCBmaWxlQ291bnRlciA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQgayA9IDA7IGsgPCBhc3NldExpc3QubGVuZ3RoOyBrKyspIHtcclxuICAgICAgICAgICAgaWYgKGFzc2V0TGlzdFtrXS5hc3NldF9zdGF0ZSA9PT0gJ0xPQ0tFRCcgJiZcclxuICAgICAgICAgICAgICAgIGFzc2V0TGlzdFtrXS5tZXRhZGF0YV9sb2NrX3N0YXRlX3VzZXJfbmFtZSAhPT0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKCkpIHtcclxuICAgICAgICAgICAgICAgIGZpbGVDb3VudGVyKys7XHJcbiAgICAgICAgICAgICAgICBpZiAoayA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSAnLCc7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSBmaWxlQ291bnRlciArICcpJyArIGFzc2V0TGlzdFtrXS5uYW1lICsgJyAtIExvY2tlZCBCeSA6ICcgKyBhc3NldExpc3Rba10ubWV0YWRhdGFfbG9ja19zdGF0ZV91c2VyX25hbWU7XHJcbiAgICAgICAgICAgICAgICBmaWxlTmFtZSArPSAnXFxuJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZmlsZU5hbWUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbyhgRm9sbG93aW5nIGZpbGUocykgYXJlIGxvY2tlZCBieSBkaWZmZXJlbnQgdXNlciBhbmQgY2Fubm90IHByb2NlZWQgd2l0aCByZXZpc2lvbiB1cGxvYWQsXHJcbiAgICAgICAgICAgICBLaW5kbHkgY29udGFjdCB0aGUgcmVzcGVjdGl2ZSB1c2VycyBvciBhZG1pbmlzdHJhdG9yIHRvIHJlbW92ZSBsb2NrIG9uIHRoZSBmaWxlKHMpIFxcbmAgKyBmaWxlTmFtZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYXNzZXRDaGVja291dChhc3NldElkczogQXJyYXk8c3RyaW5nPikge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmICghYXNzZXRJZHMgfHwgIWFzc2V0SWRzLmxlbmd0aCB8fCBhc3NldElkcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdObyBhc3NldCBpZHMgYXJlIGF2YWlsYWJsZSB0byBjaGVjayBvdXQnKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICAgICAga2V5OiAnYWN0aW9uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiAnY2hlY2tfb3V0J1xyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBrZXk6ICdqc29uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAga2V5OiAnc2VsZWN0aW9uX2NvbnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbl9jb250ZXh0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2NvbS5hcnRlc2lhLmFzc2V0LnNlbGVjdGlvbi5Bc3NldElkc1NlbGVjdGlvbkNvbnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluY2x1ZGVfZGVzY2VuZGFudHM6ICdJTU1FRElBVEUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1dO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsID0gdGhpcy5mb3JtUmVzdFVybCgnYXNzZXRzL3N0YXRlJywgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLlVSTEVOQ09ERURcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtUmVxdWVzdDogJ3hXd3dGb3JtVXJsZW5jb2RlZCdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5kb1B1dFJlcXVlc3QodXJsLCB0aGlzLmZvcm1SZXN0UGFyYW1zKHVybFBhcmFtKS5zdWJzdHJpbmcoMSksIGh0dHBPcHRpb25zLCBmYWxzZSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2VbJ2J1bGtfYXNzZXRfcmVzdWx0X3JlcHJlc2VudGF0aW9uJ10uYnVsa19hc3NldF9yZXN1bHQuZmFpbGVkX29iamVjdF9saXN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoID4gMCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKG5ldyBFcnJvcignVW5hYmxlIHRvIGNoZWNrIG91dCAnICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5mYWlsZWRfb2JqZWN0X2xpc3QubGVuZ3RoICsgJyBhc3NldHMuJykpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0ICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0Lmxlbmd0aCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2VbJ2J1bGtfYXNzZXRfcmVzdWx0X3JlcHJlc2VudGF0aW9uJ10uYnVsa19hc3NldF9yZXN1bHQuc3VjY2Vzc2Z1bF9vYmplY3RfbGlzdC5sZW5ndGggPiAwICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG1heC1saW5lLWxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZHMubGVuZ3RoID09PSByZXNwb25zZVsnYnVsa19hc3NldF9yZXN1bHRfcmVwcmVzZW50YXRpb24nXS5idWxrX2Fzc2V0X3Jlc3VsdC5zdWNjZXNzZnVsX29iamVjdF9saXN0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKCdVbmFibGUgdG8gY2hlY2sgb3V0ICcgKyBhc3NldElkcy5sZW5ndGggKyAnIGFzc2V0cy4nKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ0ludmFsaWQgQXNzZXQgSWRzLicpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBsb2NrQXNzZXRzKGFzc2V0SWRzOiBBcnJheTxzdHJpbmc+KSB7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdhY3Rpb24nLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2xvY2snXHJcbiAgICAgICAgfSwge1xyXG4gICAgICAgICAgICBrZXk6ICdqc29uJyxcclxuICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgIGtleTogJ3NlbGVjdGlvbl9jb250ZXh0JyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uX2NvbnRleHRfcGFyYW06IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uX2NvbnRleHQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY29tLmFydGVzaWEuYXNzZXQuc2VsZWN0aW9uLkFzc2V0SWRzU2VsZWN0aW9uQ29udGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmNsdWRlX2Rlc2NlbmRhbnRzOiAnTk9ORSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwoJ2Fzc2V0cy9zdGF0ZScsIG51bGwsIGZhbHNlKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5kb1B1dFJlcXVlc3QodXJsLCB0aGlzLmZvcm1SZXN0UGFyYW1zKHVybFBhcmFtKS5zdWJzdHJpbmcoMSksIHtcclxuICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5DT05URU5UX1RZUEUuVVJMRU5DT0RFRFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0sIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3NldEltcG9ydChmaWxlTmFtZSkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCwgbnVsbCwgZmFsc2UpO1xyXG4gICAgICAgIGxldCBwYXJhbTtcclxuICAgICAgICBpZiAoZmlsZU5hbWUpIHtcclxuICAgICAgICAgICAgcGFyYW0gPSB0aGlzLmZvcm1SZXN0UGFyYW1zKFt7XHJcbiAgICAgICAgICAgICAgICBrZXk6ICdmaWxlX25hbWUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IGZpbGVOYW1lXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmRvUG9zdFJlcXVlc3QodXJsLCBwYXJhbSwgZmFsc2UsIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3NldFJlbmRpdGlvbihmaWxlLCBpbXBvcnRKb2JJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnJlbmRpdGlvbnNVcmwsIG51bGwsIGZhbHNlKTtcclxuICAgICAgICBjb25zdCBwYXJhbSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X2pvYl9pZCcsIGltcG9ydEpvYklEKTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGUnLCBmaWxlKTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGZpbGUubmFtZSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZG9Qb3N0UmVxdWVzdCh1cmwsIHBhcmFtLCBmYWxzZSwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGFzc2V0c1JlbmRpdGlvbihmaWxlcywgaW1wb3J0Sm9iSUQpIHtcclxuICAgICAgICBjb25zdCBwcm9taXNlcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgZmlsZSBvZiBmaWxlcykge1xyXG4gICAgICAgICAgICBwcm9taXNlcy5wdXNoKHRoaXMuYXNzZXRSZW5kaXRpb24oZmlsZSwgaW1wb3J0Sm9iSUQpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgZm9ya0pvaW4ocHJvbWlzZXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXNzZXRDaGVja0luKGFzc2V0RGV0YWlsLCBpbXBvcnRKb2JJRCkge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuZm9ybVJlc3RVcmwob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmltcG9ydEpvYlVybCArICcvJyArIGltcG9ydEpvYklELCBudWxsLCBmYWxzZSk7XHJcbiAgICAgICAgY29uc3QgcGFyYW0gPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgICBsZXQgYmxvYiA9IG51bGw7XHJcbiAgICAgICAgbGV0IGNvbnRlbnQgPSBudWxsO1xyXG4gICAgICAgIHBhcmFtLmFwcGVuZCgnaW1wb3J0X2pvYl9pZCcsIGltcG9ydEpvYklEKTtcclxuICAgICAgICBsZXQgdXBsb2FkTWFuaWZlc3QgPSBudWxsO1xyXG4gICAgICAgIHVwbG9hZE1hbmlmZXN0ID0ge1xyXG4gICAgICAgICAgICB1cGxvYWRfbWFuaWZlc3Q6IHtcclxuICAgICAgICAgICAgICAgIG1hc3Rlcl9maWxlczogW3tcclxuICAgICAgICAgICAgICAgICAgICBmaWxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVfbmFtZTogYXNzZXREZXRhaWwubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdW9pX2lkOiBhc3NldERldGFpbC5hc3NldElkXHJcbiAgICAgICAgICAgICAgICB9XVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBwYXJhbS5hcHBlbmQoJ2ZpbGVfbmFtZScsIGFzc2V0RGV0YWlsLm5hbWUpO1xyXG4gICAgICAgIGNvbnRlbnQgPSB1cGxvYWRNYW5pZmVzdDtcclxuICAgICAgICBibG9iID0gbmV3IEJsb2IoW0pTT04uc3RyaW5naWZ5KGNvbnRlbnQpXSwge1xyXG4gICAgICAgICAgICB0eXBlOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLkpTT05EQVRBXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcGFyYW0uYXBwZW5kKCdtYW5pZmVzdCcsIGJsb2IpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5kb1B1dFJlcXVlc3QodXJsLCBwYXJhbSwgZmFsc2UsIGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWxlUGF0aHMoZmlsZXM6IEFycmF5PGFueT4pIHtcclxuICAgICAgICBjb25zdCBmaWxlUGF0aHMgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWxlc1tpXS5wYXRoKSB7XHJcbiAgICAgICAgICAgICAgICBmaWxlUGF0aHMucHVzaChmaWxlc1tpXS5wYXRoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZpbGVQYXRocztcclxuICAgIH1cclxuXHJcbiAgICBmb3JtRG93bmxvYWROYW1lKGRvd25sb2FkVHlwZT8pOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdEb3dubG9hZF8nICsgbmV3IERhdGUoKS50b0pTT04oKS5yZXBsYWNlKC9bLTpdL2csICcnKS5zcGxpdCgnLicpWzBdICsgKGRvd25sb2FkVHlwZSA/ICcuJytkb3dubG9hZFR5cGU6Jy56aXAnKSk7XHJcbiAgICAgICAgcmV0dXJuICdEb3dubG9hZF8nICsgbmV3IERhdGUoKS50b0pTT04oKS5yZXBsYWNlKC9bLTpdL2csICcnKS5zcGxpdCgnLicpWzBdICsgKGRvd25sb2FkVHlwZSA/ICcuJytkb3dubG9hZFR5cGU6Jy56aXAnKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVFeHBvcnRKb2IoYXNzZXRJZHM6IEFycmF5PGFueT4sIGRvd25sb2FkVHlwZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBiYXNlVXJsID0gaXNEZXZNb2RlKCkgPyAnLi8nIDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRNZWRpYU1hbmFnZXJDb25maWcoKS51cmw7XHJcbiAgICAgICAgICAgIGNvbnN0IHZlcnNpb24gPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLmFwaVZlcnNpb247XHJcbiAgICAgICAgICAgIGNvbnN0IHVybCA9IGJhc2VVcmwgKyAnb3RtbWFwaS8nK3ZlcnNpb24rJy9qb2JzL2V4cG9ydHMnO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICdDb250ZW50LVR5cGUnOiBvdG1tU2VydmljZXNDb25zdGFudHMuQ09OVEVOVF9UWVBFLlVSTEVOQ09ERUQsXHJcbiAgICAgICAgICAgICAgICAgICAgJ1gtUmVxdWVzdGVkLUJ5Jzogb3RtbVNlcnZpY2VzQ29uc3RhbnRzLk9UTU1fU0VSVklDRV9WQVJJQUJMRVMudXNlclNlc3Npb25JZC50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgbGV0IGV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgICAgICAgIGlmKGRvd25sb2FkVHlwZS50b1VwcGVyQ2FzZSgpID09IEZpbGVUeXBlcy5aSVApe1xyXG4gICAgICAgICAgICAgZXhwb3J0UmVxdWVzdCA9IHtcclxuICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3JlcXVlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfcHJldmlld19jb250ZW50OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9tYXN0ZXJfY29udGVudDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9zdXBwb3J0aW5nX2NvbnRlbnQ6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZV9jb2xsZWN0aW9uX2ZvbGRlcnM6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyeV90ZW1wbGF0ZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cmlidXRlX3ZhbHVlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXJndW1lbnRfbnVtYmVyOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJzInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50X251bWJlcjogNCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdOJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRC5ERUZBVUxUJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybWVyX2lkOiAnQVJURVNJQS5UUkFOU0ZPUk1FUi5QUk9GSUxFLkRPV05MT0FEJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcl9oYW5kbGluZ19hY3Rpb246ICdERUZBVUxUX0hBTkRMSU5HJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X2pvYl90cmFuc2Zvcm1lcnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1lcl9pZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuWklQIENPTVBSRVNTSU9OLkRFRkFVTFQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3VtZW50czogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1Eb3dubG9hZE5hbWUoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc19hc3luY2hyb25vdXNseTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlX3dvcmtpbmdfZGlyZWN0b3J5OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlX2V4cG9ydF9kaXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdyaXRlX3htbDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBleHBvcnRSZXF1ZXN0ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0X3BhcmFtOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4cG9ydF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50X3JlcXVlc3Q6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHBvcnRfbWFzdGVyX2NvbnRlbnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwb3J0X3N1cHBvcnRpbmdfY29udGVudDogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVfY29sbGVjdGlvbl9mb2xkZXJzOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJ5X3RlbXBsYXRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cmlidXRlX3ZhbHVlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmd1bWVudF9udW1iZXI6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJzInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRC5ERUZBVUxUJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm1lcl9pZDogJ0FSVEVTSUEuVFJBTlNGT1JNRVIuUFJPRklMRS5ET1dOTE9BRCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcl9oYW5kbGluZ19hY3Rpb246ICdERUZBVUxUX0hBTkRMSU5HJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3NfYXN5bmNocm9ub3VzbHk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW1vdmVfd29ya2luZ19kaXJlY3Rvcnk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlX2V4cG9ydF9kaXI6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3cml0ZV94bWw6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdGlvbkNvbnRleHQgPSB7XHJcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25fY29udGV4dF9wYXJhbToge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbl9jb250ZXh0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzc2V0X2lkczogYXNzZXRJZHMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjb20uYXJ0ZXNpYS5hc3NldC5zZWxlY3Rpb24uQXNzZXRJZHNTZWxlY3Rpb25Db250ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5jbHVkZV9kZXNjZW5kYW50czogJ0FMTCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkX3R5cGU6ICdBU1NFVFMnXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gJ2V4cG9ydF9yZXF1ZXN0PScgKyBKU09OLnN0cmluZ2lmeShleHBvcnRSZXF1ZXN0KSArXHJcbiAgICAgICAgICAgICAgICAnJnNlbGVjdGlvbl9jb250ZXh0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc2VsZWN0aW9uQ29udGV4dCkpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5odHRwLnBvc3QodXJsLCBwYXJhbXMsIGh0dHBPcHRpb25zKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRFeHBvcnRKb2JzKHBhcmFtczogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSAnam9icyc7IC8vIE1TVi0yMzkgLT4gJ2pvYnMvJ1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdCh1cmwsIHBhcmFtcylcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T1RNTVN5c3RlbURldGFpbHMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1lTVEVNX0RFVEFJTFMpICE9IG51bGwgKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgb3RtbVZlcnNpb249IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5TWVNURU1fREVUQUlMUykpXHJcbiAgICAgICAgICAgICAgICBpZihvdG1tVmVyc2lvbj09IG51bGwgfHwgb3RtbVZlcnNpb24gPT0gdW5kZWZpbmVkIHx8IHBhcnNlRmxvYXQob3RtbVZlcnNpb24pPCAwKXtcclxuICAgICAgICAgICAgICAgICAgICBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1lTVEVNX0RFVEFJTFMpKS5zeXN0ZW1fZGV0YWlsc19yZXNvdXJjZS5zeXN0ZW1fZGV0YWlsc19tYXAuZW50cnkuZm9yRWFjaChlPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGUua2V5LnRvU3RyaW5nKCkgPT09IEFwcGxpY2F0aW9uQ29uZmlnQ29uc3RhbnRzLk9UTU1fU1lTVEVNX0RFVEFJTFNfQ09OU1RBTlRTLkJVSUxEX1ZFUlNJT04pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1RNTV9WRVJTSU9OLCBKU09OLnN0cmluZ2lmeShlLnZhbHVlKSlcclxuICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlNZU1RFTV9ERVRBSUxTKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zeXN0ZW1kZXRhaWxzVXJsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2UhPSBudWxsKXtcclxuICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuc3lzdGVtX2RldGFpbHNfcmVzb3VyY2Uuc3lzdGVtX2RldGFpbHNfbWFwLmVudHJ5LmZvckVhY2goZT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihlLmtleS50b1N0cmluZygpID09PSBBcHBsaWNhdGlvbkNvbmZpZ0NvbnN0YW50cy5PVE1NX1NZU1RFTV9ERVRBSUxTX0NPTlNUQU5UUy5CVUlMRF9WRVJTSU9OKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9UTU1fVkVSU0lPTiwgSlNPTi5zdHJpbmdpZnkoZS52YWx1ZSkpXHJcbiAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5TWVNURU1fREVUQUlMUywgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGdldE9UTU1Bc3NldEJ5SWQoYXNzZXRJZCwgbG9hZFR5cGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBhc3NldERldGFpbHMgPSAnJztcclxuICAgICAgICBjb25zdCBkYXRhTG9hZFJlcXVlc3QgPSB7XHJcbiAgICAgICAgICAgIGRhdGFfbG9hZF9yZXF1ZXN0OiB7XHJcbiAgICAgICAgICAgICAgICBsb2FkX211bHRpbGluZ3VhbF9maWVsZF92YWx1ZXM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3N1YnNjcmliZWRfdG86IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX2Fzc2V0X2NvbnRlbnRfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfbWV0YWRhdGE6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX2luaGVyaXRlZF9tZXRhZGF0YTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfdGh1bWJuYWlsX2luZm86IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3ByZXZpZXdfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfcGRmX3ByZXZpZXdfaW5mbzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfM2RfcHJldmlld19pbmZvOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgbG9hZF9kZXN0aW5hdGlvbl9saW5rczogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGxvYWRfc2VjdXJpdHlfcG9saWNpZXM6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBsb2FkX3BhdGg6IHRydWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogbG9hZFR5cGVcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xldmVsX29mX2RldGFpbCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnc2xpbSdcclxuICAgICAgICB9XTtcclxuICAgICAgICBpZiAobG9hZFR5cGUgPT09ICdjdXN0b20nKSB7XHJcbiAgICAgICAgICAgIHVybFBhcmFtLnB1c2goe1xyXG4gICAgICAgICAgICAgICAga2V5OiAnZGF0YV9sb2FkX3JlcXVlc3QnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShkYXRhTG9hZFJlcXVlc3QpKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkLCB1cmxQYXJhbSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5hc3NldF9yZXNvdXJjZS5hc3NldCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NldERldGFpbHMgPSByZXNwb25zZS5hc3NldF9yZXNvdXJjZS5hc3NldDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChhc3NldERldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T1RNTUZvZGxlckJ5SWQoZm9sZGVySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBmb2xkZXJEZXRhaWxzID0gJyc7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ21ldGFkYXRhJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbG9hZF9tdWx0aWxpbmd1YWxfdmFsdWVzJyxcclxuICAgICAgICAgICAgdmFsdWU6ICd0cnVlJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGV2ZWxfb2ZfZGV0YWlsJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdzbGltJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnYWZ0ZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ2FmdGVyJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGltaXQnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJzUwJ1xyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mb2xkZXJzVXJsICsgZm9sZGVySWQsIHVybFBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmZvbGRlcl9yZXNvdXJjZSAmJiByZXNwb25zZS5mb2xkZXJfcmVzb3VyY2UuZm9sZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbGRlckRldGFpbHMgPSByZXNwb25zZS5mb2xkZXJfcmVzb3VyY2UuZm9sZGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZvbGRlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2VhcmNoT3BlcmF0b3JzTGlzdCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzZWFyY2hPcGVyYXRvckxpc3QgPSAnJztcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuc2VhcmNob3BlcmF0b3JzLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLnNlYXJjaF9vcGVyYXRvcnNfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuc2VhcmNoX29wZXJhdG9yc19yZXNvdXJjZS5zZWFyY2hfb3BlcmF0b3JfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWFyY2hPcGVyYXRvckxpc3QgPSByZXNwb25zZS5zZWFyY2hfb3BlcmF0b3JzX3Jlc291cmNlLnNlYXJjaF9vcGVyYXRvcl9saXN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNlYXJjaE9wZXJhdG9yTGlzdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxNZXRhZGF0TW9kZWxMaXN0KCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IG1ldGFEYXRhTW9kZWxMaXN0ID0gJyc7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLm1ldGFkYXRhbW9kZWxzLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1ldGFEYXRhTW9kZWxMaXN0ID0gcmVzcG9uc2UubWV0YWRhdGFfbW9kZWxzX3Jlc291cmNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KG1ldGFEYXRhTW9kZWxMaXN0KTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1ldGFkYXRNb2RlbEJ5SWQobW9kZWxJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IG1ldGFEYXRhTW9kZWxEZXRhaWxzID0gJyc7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5tZXRhZGF0YW1vZGVscyArIG1vZGVsSWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWV0YURhdGFNb2RlbERldGFpbHMgPSByZXNwb25zZS5tZXRhZGF0YV9tb2RlbF9yZXNvdXJjZS5tZXRhZGF0YV9tb2RlbDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChtZXRhRGF0YU1vZGVsRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzYXZlU2VhcmNoKHNlYXJjaENvbmZpZywgc2tpcCwgdG9wLCB1c2VyU2Vzc2lvbklkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSBvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRTZWFyY2hVcmw7XHJcblxyXG4gICAgICAgIC8qIGlmIChzZWFyY2hDb25maWcuc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICB1cmwgPSBvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRzZWFyY2hlcyArIHNlYXJjaENvbmZpZy5zYXZlZFNlYXJjaElkXHJcbiAgICAgICAgfSAqL1xyXG5cclxuICAgICAgICBjb25zdCBmb3JtRGF0YSA9IFtdO1xyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc2VhcmNoS2V5KSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goc2VhcmNoQ29uZmlnLnNlYXJjaEtleSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc29ydFN0cmluZykge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzb3J0PScgKyBzZWFyY2hDb25maWcuc29ydFN0cmluZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcua2V5d29yZF9xdWVyeSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdrZXl3b3JkX3F1ZXJ5PScgKyBlbmNvZGVVUklDb21wb25lbnQoc2VhcmNoQ29uZmlnLmtleXdvcmRfcXVlcnkpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5sb2FkX3R5cGUgPT09ICdjdXN0b20nKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2RhdGFfbG9hZF9yZXF1ZXN0PScgKyAne1wiZGF0YV9sb2FkX3JlcXVlc3RcIjp7XCJjaGlsZF9jb3VudF9sb2FkX3R5cGVcIjonICsgJ1wiJyArIHNlYXJjaENvbmZpZy5jaGlsZF9jb3VudF9sb2FkX3R5cGUgKyAnXCInICsgJyB9fScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdhZnRlcj0nICsgc2tpcCk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbGltaXQ9JyArIHRvcCk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbXVsdGlsaW5ndWFsX2xhbmd1YWdlX2NvZGU9ZW5fVVMnKTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXIpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZm9sZGVyX2ZpbHRlcj0nICsgc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXJfdHlwZSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmb2xkZXJfZmlsdGVyX3R5cGU9JyArIHNlYXJjaENvbmZpZy5mb2xkZXJfZmlsdGVyX3R5cGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmZhY2V0X3Jlc3RyaWN0aW9uX2xpc3QpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZmFjZXRfcmVzdHJpY3Rpb25fbGlzdD0nICsgZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNlYXJjaENvbmZpZy5mYWNldF9yZXN0cmljdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25maWdfaWQpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnc2VhcmNoX2NvbmZpZ19pZD0nICsgc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25maWdfaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodHlwZW9mIHNlYXJjaENvbmZpZy5pc19mYXZvcml0ZSA9PT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2lzX2Zhdm9yaXRlPScgKyBzZWFyY2hDb25maWcuaXNfZmF2b3JpdGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm5hbWUpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnbmFtZT0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHNlYXJjaENvbmZpZy5uYW1lKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZGVzY3JpcHRpb249JyArIGVuY29kZVVSSUNvbXBvbmVudChzZWFyY2hDb25maWcuZGVzY3JpcHRpb24pKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiBzZWFyY2hDb25maWcuaXNfcHVibGljID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfcHVibGljPScgKyBzZWFyY2hDb25maWcuaXNfcHVibGljKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ3ByZWZlcmVuY2VfaWQ9JyArIChzZWFyY2hDb25maWcucHJlZmVyZW5jZV9pZCA/IHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkIDogJ0FSVEVTSUEuUFJFRkVSRU5DRS5HQUxMRVJZVklFVy5ESVNQTEFZRURfRklFTERTJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm1ldGFkYXRhX3RvX3JldHVybikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdtZXRhZGF0YV90b19yZXR1cm49JyArIHNlYXJjaENvbmZpZy5tZXRhZGF0YV90b19yZXR1cm4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25kaXRpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZGl0aW9uX2xpc3Q9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzZWFyY2hDb25maWcuc2VhcmNoX2NvbmRpdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNhdmVkU2VhcmNoSWQpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaWQ9JyArIHNlYXJjaENvbmZpZy5zYXZlZFNlYXJjaElkKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gZm9ybURhdGEuam9pbignJicpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoKS5zZXQoJ1gtUmVxdWVzdGVkLUJ5JywgdXNlclNlc3Npb25JZC50b1N0cmluZygpKVxyXG4gICAgICAgICAgICAgICAgLnNldCgnQ29udGVudC1UeXBlJywgJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCcpLFxyXG4gICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8qIGlmIChzZWFyY2hDb25maWcuc2F2ZWRTZWFyY2hJZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5maXJlUGF0Y2hSZXN0UmVxdWVzdCh1cmwsIHBhcmFtLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJywgaHR0cE9wdGlvbnMpO1xyXG4gICAgICAgIH0gKi9cclxuICAgICAgICByZXR1cm4gdGhpcy5maXJlUG9zdFJlc3RSZXF1ZXN0KHVybCwgcGFyYW0sICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnLCBodHRwT3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoQ3VzdG9tVGV4dChzZWFyY2hDb25maWcsIHNraXAsIHRvcCwgdXNlclNlc3Npb25JZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IHVybCA9IG90bW1TZXJ2aWNlc0NvbnN0YW50cy50ZXh0QXNzZXRTZWFyY2hVcmw7XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5pc1NhdmVkU2VhcmNoKSB7XHJcbiAgICAgICAgICAgIHVybCA9IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zYXZlZFNlYXJjaFVybDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgZm9ybURhdGEgPSBbXTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaEtleSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKHNlYXJjaENvbmZpZy5zZWFyY2hLZXkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNvcnRTdHJpbmcpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnc29ydD0nICsgc2VhcmNoQ29uZmlnLnNvcnRTdHJpbmcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmtleXdvcmRfcXVlcnkpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgna2V5d29yZF9xdWVyeT0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHNlYXJjaENvbmZpZy5rZXl3b3JkX3F1ZXJ5KSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2xvYWRfdHlwZT0nICsgKHNlYXJjaENvbmZpZy5sb2FkX3R5cGUgPyBzZWFyY2hDb25maWcubG9hZF90eXBlIDogJ21ldGFkYXRhJykpO1xyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2xvYWRfbXVsdGlsaW5ndWFsX3ZhbHVlcz10cnVlJyk7XHJcbiAgICAgICAgZm9ybURhdGEucHVzaCgnbGV2ZWxfb2ZfZGV0YWlsPScgKyAoc2VhcmNoQ29uZmlnLmxldmVsX29mX2RldGFpbCA/IHNlYXJjaENvbmZpZy5sZXZlbF9vZl9kZXRhaWwgOiAnc2xpbScpKTtcclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLmxvYWRfdHlwZSA9PT0gJ2N1c3RvbScpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnZGF0YV9sb2FkX3JlcXVlc3Q9JyArICd7XCJkYXRhX2xvYWRfcmVxdWVzdFwiOntcImNoaWxkX2NvdW50X2xvYWRfdHlwZVwiOicgKyAnXCInICsgc2VhcmNoQ29uZmlnLmNoaWxkX2NvdW50X2xvYWRfdHlwZSArICdcIicgKyAnIH19Jyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1EYXRhLnB1c2goJ2FmdGVyPScgKyBza2lwKTtcclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdsaW1pdD0nICsgdG9wKTtcclxuICAgICAgICBmb3JtRGF0YS5wdXNoKCdtdWx0aWxpbmd1YWxfbGFuZ3VhZ2VfY29kZT1lbl9VUycpO1xyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmb2xkZXJfZmlsdGVyPScgKyBzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZm9sZGVyX2ZpbHRlcl90eXBlKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ2ZvbGRlcl9maWx0ZXJfdHlwZT0nICsgc2VhcmNoQ29uZmlnLmZvbGRlcl9maWx0ZXJfdHlwZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuZmFjZXRfcmVzdHJpY3Rpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdmYWNldF9yZXN0cmljdGlvbl9saXN0PScgKyBlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc2VhcmNoQ29uZmlnLmZhY2V0X3Jlc3RyaWN0aW9uX2xpc3QpKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuc2VhcmNoX2NvbmZpZ19pZCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZmlnX2lkPScgKyBzZWFyY2hDb25maWcuc2VhcmNoX2NvbmZpZ19pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcuaXNfZmF2b3JpdGUpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfZmF2b3JpdGU9JyArIHNlYXJjaENvbmZpZy5pc19mYXZvcml0ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzZWFyY2hDb25maWcubmFtZSkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCduYW1lPScgKyBlbmNvZGVVUklDb21wb25lbnQoc2VhcmNoQ29uZmlnLm5hbWUpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5pc19wdWJsaWMpIHtcclxuICAgICAgICAgICAgZm9ybURhdGEucHVzaCgnaXNfcHVibGljPScgKyBzZWFyY2hDb25maWcuaXNfcHVibGljKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLnB1c2goJ3ByZWZlcmVuY2VfaWQ9JyArIChzZWFyY2hDb25maWcucHJlZmVyZW5jZV9pZCA/IHNlYXJjaENvbmZpZy5wcmVmZXJlbmNlX2lkIDogJ0FSVEVTSUEuUFJFRkVSRU5DRS5HQUxMRVJZVklFVy5ESVNQTEFZRURfRklFTERTJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLm1ldGFkYXRhX3RvX3JldHVybikge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdtZXRhZGF0YV90b19yZXR1cm49JyArIHNlYXJjaENvbmZpZy5tZXRhZGF0YV90b19yZXR1cm4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VhcmNoQ29uZmlnLnNlYXJjaF9jb25kaXRpb25fbGlzdCkge1xyXG4gICAgICAgICAgICBmb3JtRGF0YS5wdXNoKCdzZWFyY2hfY29uZGl0aW9uX2xpc3Q9JyArIGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzZWFyY2hDb25maWcuc2VhcmNoX2NvbmRpdGlvbl9saXN0KSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcGFyYW0gPSBmb3JtRGF0YS5qb2luKCcmJyk7XHJcbiAgICAgICAgY29uc3QgaHR0cE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycygpLnNldCgnWC1SZXF1ZXN0ZWQtQnknLCB1c2VyU2Vzc2lvbklkLnRvU3RyaW5nKCkpXHJcbiAgICAgICAgICAgICAgICAuc2V0KCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyksXHJcbiAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmlyZVBvc3RSZXN0UmVxdWVzdCh1cmwsIHBhcmFtLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJywgaHR0cE9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU1ldGFkYXRhKGFzc2V0SWQsIHR5cGUsIG1ldGFkYXRhUGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudFR5cGUgPSAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJztcclxuICAgICAgICAgICAgY29uc3QgbG9ja0Zvcm1EYXRhID0gW107XHJcbiAgICAgICAgICAgIGxvY2tGb3JtRGF0YS5wdXNoKCdhY3Rpb249bG9jaycpO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbSA9IGxvY2tGb3JtRGF0YS5qb2luKCcmJyk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZVB1dFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy9zdGF0ZScsIHBhcmFtLCBudWxsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb2NrUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVBhdGNoUmVzdFJlcXVlc3QoKCh0eXBlID09PSAnRk9MREVSJyA/IG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mb2xkZXJzVXJsIDogKG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycpKSArIGFzc2V0SWQpLCBtZXRhZGF0YVBhcmFtZXRlcnMsIG51bGwsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdW5sb2NrRm9ybURhdGEgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVubG9ja0Zvcm1EYXRhLnB1c2goJ2FjdGlvbj11bmxvY2snKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHVubG9ja0Zvcm1EYXRhLmpvaW4oJyYnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmlyZVB1dFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy9zdGF0ZScsIHBhcmFtZXRlciwgbnVsbCwgbnVsbClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHVubG9ja1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1bmxvY2tSZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsU2F2ZWRTZWFyY2hlcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzYXZlZHNlYXJjaGVzID0gW107XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnNhdmVkc2VhcmNoZXMsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoZXNfcmVzb3VyY2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoZXNfcmVzb3VyY2Uuc2F2ZWRfc2VhcmNoX2xpc3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZWRzZWFyY2hlcyA9IHJlc3BvbnNlLnNhdmVkX3NlYXJjaGVzX3Jlc291cmNlLnNhdmVkX3NlYXJjaF9saXN0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNhdmVkc2VhcmNoZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U2F2ZWRTZWFyY2hCeUlkKGlkOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBzYXZlZHNlYXJjaCA9IHt9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5zYXZlZHNlYXJjaGVzICsgaWQsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuc2F2ZWRfc2VhcmNoX3Jlc291cmNlICYmIHJlc3BvbnNlLnNhdmVkX3NlYXJjaF9yZXNvdXJjZS5zYXZlZF9zZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZWRzZWFyY2ggPSByZXNwb25zZS5zYXZlZF9zZWFyY2hfcmVzb3VyY2Uuc2F2ZWRfc2VhcmNoO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHNhdmVkc2VhcmNoKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVNhdmVkU2VhcmNoKGlkOiBhbnkpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVEZWxldGVSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuc2F2ZWRzZWFyY2hlcyArIGlkLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRSZWxhdGlvbmFsT3BlcmF0b3Ioc2VhcmNoQ29uZGl0aW9uczogYW55W10pIHtcclxuICAgICAgICBzZWFyY2hDb25kaXRpb25zLmZvckVhY2goc2VhcmNoQ29uZGl0aW9uID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlYXJjaENvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yICYmIHNlYXJjaENvbmRpdGlvbi5yZWxhdGlvbmFsX29wZXJhdG9yICE9PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBzZWFyY2hDb25kaXRpb24ucmVsYXRpb25hbF9vcGVyYXRvciA9ICdhbmQnO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gc2VhcmNoQ29uZGl0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldFZlcnNpb25zQnlBc3NldElkKGFzc2V0SWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBhc3NldFZlcnNpb25EZXRhaWxzID0gJyc7XHJcbiAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICBrZXk6ICdsb2FkX3R5cGUnLFxyXG4gICAgICAgICAgICB2YWx1ZTogJ21ldGFkYXRhJ1xyXG4gICAgICAgIH0sIHtcclxuICAgICAgICAgICAga2V5OiAnbGV2ZWxfb2ZfZGV0YWlsJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdzbGltJ1xyXG4gICAgICAgIH1dO1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5hc3NzZXRVcmwgKyAnLycgKyBhc3NldElkICsgJy92ZXJzaW9ucycsIHVybFBhcmFtKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmFzc2V0c19yZXNvdXJjZSAmJiByZXNwb25zZS5hc3NldHNfcmVzb3VyY2UuYXNzZXRfbGlzdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhc3NldFZlcnNpb25EZXRhaWxzID0gcmVzcG9uc2UuYXNzZXRzX3Jlc291cmNlLmFzc2V0X2xpc3Q7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoYXNzZXRWZXJzaW9uRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBc3NldEJ5QXNzZXRJZChhc3NldElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBsZXQgYXNzZXRWZXJzaW9uRGV0YWlscyA9ICcnO1xyXG4gICAgICAgIGNvbnN0IHVybFBhcmFtID0gW3tcclxuICAgICAgICAgICAga2V5OiAnbG9hZF90eXBlJyxcclxuICAgICAgICAgICAgdmFsdWU6ICdtZXRhZGF0YSdcclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGtleTogJ2xldmVsX29mX2RldGFpbCcsXHJcbiAgICAgICAgICAgIHZhbHVlOiAnc2xpbSdcclxuICAgICAgICB9XTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMuYXNzc2V0VXJsICsgJy8nICsgYXNzZXRJZCwgdXJsUGFyYW0pXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuYXNzZXRfcmVzb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRWZXJzaW9uRGV0YWlscyA9IHJlc3BvbnNlLmFzc2V0X3Jlc291cmNlLmFzc2V0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGFzc2V0VmVyc2lvbkRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0c2VhcmNoY29uZmlndXJhdGlvbnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXJsUGFyYW0gPSBbe1xyXG4gICAgICAgICAgICAgICAga2V5OiAncmV0cmlldmFsX3R5cGUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdmdWxsJ1xyXG4gICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLnNlYXJjaGNvbmZpZ3VyYXRpb25zLCB1cmxQYXJhbSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QWxsTG9va3VwRG9tYWlucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBsb29rdXBEb21haW5zID0gW107XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5maXJlR2V0UmVzdFJlcXVlc3Qob3RtbVNlcnZpY2VzQ29uc3RhbnRzLmxvb2t1cGRvbWFpbnMsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbnNfcmVzb3VyY2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbnNfcmVzb3VyY2UubG9va3VwX2RvbWFpbnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9va3VwRG9tYWlucyA9IHJlc3BvbnNlLmxvb2t1cF9kb21haW5zX3Jlc291cmNlLmxvb2t1cF9kb21haW5zO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGxvb2t1cERvbWFpbnMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TG9va3VwRG9tYWluKGxvb2t1cERvbWFpbklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBsb29rdXBEb21haW5WYWx1ZXMgPSBbXTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpcmVHZXRSZXN0UmVxdWVzdChvdG1tU2VydmljZXNDb25zdGFudHMubG9va3VwZG9tYWlucyArIGxvb2t1cERvbWFpbklkLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmxvb2t1cF9kb21haW5fcmVzb3VyY2UgJiYgcmVzcG9uc2UubG9va3VwX2RvbWFpbl9yZXNvdXJjZS5sb29rdXBfZG9tYWluICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmxvb2t1cF9kb21haW5fcmVzb3VyY2UubG9va3VwX2RvbWFpbi5kb21haW5WYWx1ZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbG9va3VwRG9tYWluVmFsdWVzID0gcmVzcG9uc2UubG9va3VwX2RvbWFpbl9yZXNvdXJjZS5sb29rdXBfZG9tYWluLmRvbWFpblZhbHVlcztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChsb29rdXBEb21haW5WYWx1ZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmFjZXRDb25maWd1cmF0aW9ucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmlyZUdldFJlc3RSZXF1ZXN0KG90bW1TZXJ2aWNlc0NvbnN0YW50cy5mYWNldGNvbmZpZ3VyYXRpb25zLCBudWxsKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGZhY2V0Q29uZmlndXJhdGlvbnMgPSBbXTtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5mYWNldF9jb25maWd1cmF0aW9uc19yZXNvdXJjZSAmJlxyXG4gICAgICAgICAgICAgICAgICAgIEFycmF5KHJlc3BvbnNlLmZhY2V0X2NvbmZpZ3VyYXRpb25zX3Jlc291cmNlLmZhY2V0X2NvbmZpZ3VyYXRpb25fbGlzdCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBmYWNldENvbmZpZ3VyYXRpb25zID0gcmVzcG9uc2UuZmFjZXRfY29uZmlndXJhdGlvbnNfcmVzb3VyY2UuZmFjZXRfY29uZmlndXJhdGlvbl9saXN0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWNldENvbmZpZ3VyYXRpb25zKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgZ2V0dGluZyBmYWNldCBjb25maWd1cmF0aW9ucycpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=