import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { AppService } from './app.service';
import { GloabalConfig as config } from '../config/config';
import { Observable } from 'rxjs';
import { EntityAppDefService } from './entity.appdef.service';
import { SharingService } from './sharing.service';
import * as i0 from "@angular/core";
import * as i1 from "./app.service";
import * as i2 from "./entity.appdef.service";
import * as i3 from "./sharing.service";
var OTDSTicketService = /** @class */ (function () {
    function OTDSTicketService(appService, entityAppDefService, sharingService) {
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.sharingService = sharingService;
        this.otdsticket = '';
    }
    OTDSTicketService.prototype.setOTDSTicketForUser = function () {
        var _this = this;
        this.otdsticket = '';
        return new Observable(function (observer) {
            if (_this.otdsticket) {
                observer.next(_this.otdsticket);
                observer.complete();
                return;
            }
            var parameters = {
                organization: config.config.organizationName,
                // sourceResourceName: '__OTDS#Organizational#Platform#Resource__',
                sourceResourceName: _this.entityAppDefService.applicationDetails.OTDS_APPWORKS_RESOURCE_NAME,
                targetResourceName: _this.sharingService.getMediaManagerConfig().otdsResourceName,
                isOrgSpace: 'true'
            };
            _this.appService.getOTDSTicketForUser(parameters)
                .subscribe(function (otdsTicketResponse) {
                var ticket = otdsTicketResponse.tuple.old.OTDSAccessTicket.otmmTicket;
                _this.otdsticket = ticket;
                observer.next(ticket);
                observer.complete();
            }, function (error) {
                observer.error();
                observer.complete();
            });
        });
    };
    OTDSTicketService.prototype.getOTDSTicketForUser = function () {
        return this.otdsticket;
    };
    OTDSTicketService.ctorParameters = function () { return [
        { type: AppService },
        { type: EntityAppDefService },
        { type: SharingService }
    ]; };
    OTDSTicketService.ɵprov = i0.ɵɵdefineInjectable({ factory: function OTDSTicketService_Factory() { return new OTDSTicketService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.SharingService)); }, token: OTDSTicketService, providedIn: "root" });
    OTDSTicketService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], OTDSTicketService);
    return OTDSTicketService;
}());
export { OTDSTicketService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT1REU1RpY2tldC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL09URFNUaWNrZXQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNsQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7Ozs7O0FBTW5EO0lBRUksMkJBQ1csVUFBc0IsRUFDdEIsbUJBQXdDLEVBQ3hDLGNBQThCO1FBRjlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFJekMsZUFBVSxHQUFHLEVBQUUsQ0FBQztJQUZaLENBQUM7SUFJTCxnREFBb0IsR0FBcEI7UUFBQSxpQkEyQkM7UUExQkcsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDL0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNwQixPQUFPO2FBQ1Y7WUFDRCxJQUFNLFVBQVUsR0FBRztnQkFDZixZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0I7Z0JBQzVDLG1FQUFtRTtnQkFDbkUsa0JBQWtCLEVBQUUsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDLDJCQUEyQjtnQkFDM0Ysa0JBQWtCLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLGdCQUFnQjtnQkFDaEYsVUFBVSxFQUFFLE1BQU07YUFDckIsQ0FBQztZQUVGLEtBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDO2lCQUMzQyxTQUFTLENBQUMsVUFBQSxrQkFBa0I7Z0JBQ3pCLElBQU0sTUFBTSxHQUFHLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDO2dCQUN4RSxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztnQkFDekIsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNqQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnREFBb0IsR0FBcEI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQzs7Z0JBdkNzQixVQUFVO2dCQUNELG1CQUFtQjtnQkFDeEIsY0FBYzs7O0lBTGhDLGlCQUFpQjtRQUo3QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsaUJBQWlCLENBNEM3Qjs0QkF2REQ7Q0F1REMsQUE1Q0QsSUE0Q0M7U0E1Q1ksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBTZXJ2aWNlIH0gZnJvbSAnLi9hcHAuc2VydmljZSc7XHJcbmltcG9ydCB7IEdsb2FiYWxDb25maWcgYXMgY29uZmlnIH0gZnJvbSAnLi4vY29uZmlnL2NvbmZpZyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4vZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuL3NoYXJpbmcuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBPVERTVGlja2V0U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFwcFNlcnZpY2U6IEFwcFNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIGVudGl0eUFwcERlZlNlcnZpY2U6IEVudGl0eUFwcERlZlNlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZVxyXG5cclxuICAgICkgeyB9XHJcblxyXG4gICAgb3Rkc3RpY2tldCA9ICcnO1xyXG5cclxuICAgIHNldE9URFNUaWNrZXRGb3JVc2VyKCk6IE9ic2VydmFibGU8c3RyaW5nPiB7XHJcbiAgICAgICAgdGhpcy5vdGRzdGlja2V0ID0gJyc7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMub3Rkc3RpY2tldCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0aGlzLm90ZHN0aWNrZXQpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICAgICAgb3JnYW5pemF0aW9uOiBjb25maWcuY29uZmlnLm9yZ2FuaXphdGlvbk5hbWUsXHJcbiAgICAgICAgICAgICAgICAvLyBzb3VyY2VSZXNvdXJjZU5hbWU6ICdfX09URFMjT3JnYW5pemF0aW9uYWwjUGxhdGZvcm0jUmVzb3VyY2VfXycsXHJcbiAgICAgICAgICAgICAgICBzb3VyY2VSZXNvdXJjZU5hbWU6IHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5hcHBsaWNhdGlvbkRldGFpbHMuT1REU19BUFBXT1JLU19SRVNPVVJDRV9OQU1FLFxyXG4gICAgICAgICAgICAgICAgdGFyZ2V0UmVzb3VyY2VOYW1lOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldE1lZGlhTWFuYWdlckNvbmZpZygpLm90ZHNSZXNvdXJjZU5hbWUsXHJcbiAgICAgICAgICAgICAgICBpc09yZ1NwYWNlOiAndHJ1ZSdcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRPVERTVGlja2V0Rm9yVXNlcihwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShvdGRzVGlja2V0UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRpY2tldCA9IG90ZHNUaWNrZXRSZXNwb25zZS50dXBsZS5vbGQuT1REU0FjY2Vzc1RpY2tldC5vdG1tVGlja2V0O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3Rkc3RpY2tldCA9IHRpY2tldDtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRpY2tldCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRPVERTVGlja2V0Rm9yVXNlcigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vdGRzdGlja2V0O1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=