import { __decorate, __values } from "tslib";
import { Injectable, isDevMode } from '@angular/core';
import { EntityAppDefService } from './entity.appdef.service';
import { SharingService } from './sharing.service';
import { AppService } from './app.service';
import { Observable, forkJoin } from 'rxjs';
import { CategoryService } from '../../project/shared/services/category.service';
import * as acronui from '../auth/utility';
import * as moment_ from 'moment';
import { StatusService } from '../../shared/services/status.service';
import { FieldConfigService } from '../../shared/services/field-config.service';
import { IndexerConfigService } from '../../shared/services/indexer/mpm-indexer.config.service';
import { ViewConfigService } from '../../shared/services/view-config.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import { GloabalConfig as config } from '../config/config';
import * as $ from 'jquery';
import * as i0 from "@angular/core";
import * as i1 from "./app.service";
import * as i2 from "./entity.appdef.service";
import * as i3 from "./sharing.service";
import * as i4 from "../../project/shared/services/category.service";
import * as i5 from "../../shared/services/status.service";
import * as i6 from "../../shared/services/field-config.service";
import * as i7 from "../../shared/services/view-config.service";
import * as i8 from "../../shared/services/indexer/mpm-indexer.config.service";
var moment = moment_;
var UtilService = /** @class */ (function () {
    function UtilService(appService, entityAppDefService, sharingService, categoryService, statusService, fieldConfigService, viewConfigService, indexerConfigService) {
        this.appService = appService;
        this.entityAppDefService = entityAppDefService;
        this.sharingService = sharingService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.fieldConfigService = fieldConfigService;
        this.viewConfigService = viewConfigService;
        this.indexerConfigService = indexerConfigService;
        this.rolesForUser = [];
        this.WL_NSPC = 'http://schemas.cordys.com/notification/workflow/1.0';
        this.USER_NSPC = 'http://schemas.cordys.com/1.1/ldap';
        this.GET_ALL_WORKLIST_FOR_USER = 'GetAllWorklistsForUser';
    }
    UtilService.prototype.setProjectCustomMetadata = function (projectCustomMetadata) {
        this.newProjectCustomMetadata = projectCustomMetadata;
    };
    UtilService.prototype.getProjectCustomMetadata = function () {
        return this.newProjectCustomMetadata;
    };
    UtilService.prototype.navigateToApp = function () {
        window.location.href = this.APP_PATH;
    };
    UtilService.prototype.setUserMappedRoles = function (roleMappers) {
        var e_1, _a;
        var mappedRoles = [];
        try {
            for (var roleMappers_1 = __values(roleMappers), roleMappers_1_1 = roleMappers_1.next(); !roleMappers_1_1.done; roleMappers_1_1 = roleMappers_1.next()) {
                var roleMapper = roleMappers_1_1.value;
                if (this.entityAppDefService.hasRole(roleMapper.ROLE_DN)) {
                    mappedRoles.push(roleMapper);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (roleMappers_1_1 && !roleMappers_1_1.done && (_a = roleMappers_1.return)) _a.call(roleMappers_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.sharingService.setUserRoles(mappedRoles);
        console.log('sharing user roles', this.sharingService.getUserRoles());
    };
    // added for code hardeniing 
    UtilService.prototype.getUserDetailsLDAP = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.USER_NSPC, 'GetUserDetails', null).subscribe(function (userDetails) {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_LDAP, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    };
    // added for code hardeniing 
    UtilService.prototype.getUserDetailsWorkflow = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_WORKFLOW) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_WORKFLOW)));
                observer.complete();
            }
            else {
                _this.appService.invokeRequest(_this.WL_NSPC, 'GetUserDetails', null).subscribe(function (userDetails) {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_WORKFLOW, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    };
    UtilService.prototype.formRestUrl = function (otmmapiurl) {
        var baseUrl = isDevMode() ? './' : this.sharingService.getMediaManagerConfig().url;
        return baseUrl + otmmapiurl;
    };
    // MPMV3-2237
    UtilService.prototype.indexer = function () {
        var defer = $.Deferred();
        $.ajax({
            url: config.getOTDSRESTAuthEndpointwithHeaders(),
            data: {},
            crossDomain: true,
            contentType: 'application/json',
            dataType: 'json',
            method: 'GET',
        }).done(function (result) {
            var otdsAuthInfo = {
                resourceId: result.resourceID,
                ticket: result.ticket,
                token: result.token,
                userId: result.userId,
                failureReason: result.failureReason,
                error: result.error
            };
            console.log("#####################");
            console.log(otdsAuthInfo);
            // store otds-ticket and user id in session storage for headers purpose 
            // console.log('Otds-ticket--  ',otdsAuthInfo)
            sessionStorage.setItem(SessionStorageConstants.OTDS_TICKET, JSON.stringify(otdsAuthInfo.ticket));
            sessionStorage.setItem(SessionStorageConstants.OTDS_USER_ID, JSON.stringify(otdsAuthInfo.userId));
            if (typeof result.error === 'undefined') {
                defer.resolve(otdsAuthInfo);
            }
            else {
                defer.reject(otdsAuthInfo);
            }
        }).fail(function (error) {
            console.log(error);
            defer.reject(error);
        });
        return defer.promise();
    };
    UtilService.prototype.setUserInfo = function () {
        var _this = this;
        return new Observable(function (setUserObserver) {
            /* const userDetails = this.appService.invokeRequest(this.WL_NSPC, 'GetUserDetails', null);
            userDetails.subscribe(response => { */
            _this.getUserDetailsWorkflow().subscribe(function (response) {
                var defer = $.Deferred();
                // MPMV3-2237
                if (!isDevMode()) {
                    _this.indexer().then(function (ticketObj) {
                        console.log(ticketObj);
                    }).fail(function (errCode) {
                        console.log(errCode);
                        defer.reject(errCode);
                    });
                }
                _this.entityAppDefService.setUserDetails(response);
                _this.entityAppDefService.setUserDN(response);
                var currentUser = acronui.findObjectByProp(response, 'User');
                if (currentUser) {
                    if (currentUser.ManagerFor && currentUser.ManagerFor.Target && Array.isArray(currentUser.ManagerFor.Target)) {
                        _this.rolesForUser = currentUser.ManagerFor.Target;
                    }
                    else if (currentUser.ManagerFor && currentUser.ManagerFor.Target &&
                        typeof (currentUser.ManagerFor.Target) === 'object') {
                        _this.rolesForUser.push(currentUser.ManagerFor.Target);
                    }
                    _this.sharingService.setCurrentUserID(currentUser.UserDN.split('=')[1].split(',')[0]);
                    _this.sharingService.setCurrentUserObject(currentUser);
                    _this.sharingService.setCurrentUserDN(currentUser.UserDN);
                    _this.sharingService.setCurrentUserDisplayName(currentUser.UserDisplayName);
                    _this.sharingService.setCurrentOrgDN(currentUser.UserDN.split(',').slice(2).join(','));
                    var parameters = {
                        userCN: _this.sharingService.getCurrentUserID()
                    };
                    _this.appService.getPersonDetailsByUserCN(parameters)
                        .subscribe(function (userDetailsResponse) {
                        _this.sharingService.setCurrentUserItemID(userDetailsResponse.Person.PersonToUser['Identity-id'].Id);
                        _this.sharingService.setCurrentUserEmailId(userDetailsResponse.Person.Email.__text);
                        _this.appService.getAllRoles()
                            .subscribe(function (rolesResonse) {
                            var roles = acronui.findObjectsByProp(rolesResonse, 'MPM_APP_Roles');
                            _this.sharingService.setAllRoles(roles);
                            // code hardening -Keerthana
                            // this.appService.invokeRequest(this.USER_NSPC, 'GetUserDetails', null)
                            _this.getUserDetailsLDAP()
                                .subscribe(function (getUserDetailsResponse) {
                                _this.entityAppDefService.setMasterRoles(getUserDetailsResponse);
                                _this.setUserMappedRoles(roles);
                                setUserObserver.next(currentUser);
                                setUserObserver.complete();
                            }, function (getUserDetailsError) {
                                setUserObserver.error(new Error(getUserDetailsError));
                            });
                        }, function (rolesError) {
                            setUserObserver.error('NOACCESS');
                            //setUserObserver.error(new Error(rolesError));
                        });
                    }, function (userDetailsError) {
                        setUserObserver.error('NOACCESS');
                        //setUserObserver.error(new Error(userDetailsError));
                    });
                }
                else {
                    setUserObserver.error(new Error('No user details available.'));
                }
            }, function (error) {
                setUserObserver.error(new Error(error));
            });
        });
    };
    UtilService.prototype.getUserRoles = function () {
        if (this.rolesForUser.length > 0) {
            return this.rolesForUser;
        }
        else {
            console.error('No roles are available.');
        }
    };
    UtilService.prototype.getUserGroups = function () {
        return this.appService.invokeRequest(this.WL_NSPC, this.GET_ALL_WORKLIST_FOR_USER, null);
    };
    UtilService.prototype.setSelectedApp = function (application) {
        var _this = this;
        return new Observable(function (observer) {
            if (application != null && application) {
                _this.APP_ID = application.APPLICATION_ID;
                _this.entityAppDefService.setApplicationDetails(application)
                    .subscribe(function (response) {
                    observer.next(response);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
            else {
                observer.error('No Application ID property is available for application: ' +
                    application.applicationId);
            }
        });
    };
    UtilService.prototype.setConfigsForApp = function () {
        var _this = this;
        return new Observable(function (observer) {
            forkJoin([_this.entityAppDefService.getMenuForUser(),
                _this.categoryService.getAllCategories(),
                _this.entityAppDefService.getAllPriorities(),
                _this.statusService.getAllStatusConfigDetails(),
                _this.fieldConfigService.getAllMPMFieldConfig(),
            ]).subscribe(function (response) {
                response[0].sort(function (a, b) { return (parseInt(a.SEQUENCE) > parseInt(b.SEQUENCE)) ? 1 : ((parseInt(b.SEQUENCE) > parseInt(a.SEQUENCE)) ? -1 : 0); });
                _this.sharingService.setMenu(response[0]);
                _this.sharingService.setAllCategories(response[1]);
                _this.sharingService.setAllPriorities(response[2]);
                _this.sharingService.setAllStatusConfig(response[3] ? response[3] : []);
                _this.sharingService.setAllApplicationFields(response[4] ? response[4] : []);
                // this.sharingService.setDefaultCategoryDetails(response[5] ? response[5] : null);
                // const defaultCategory = this.sharingService.getDefaultCategoryDetails();
                var categories = _this.sharingService.getAllCategories();
                _this.sharingService.setSelectedCategory(categories[0]);
                // const selectedCategory = categories.find(category => category['MPM_Category-id'].Id === defaultCategory.id);
                // this.sharingService.setSelectedCategory(selectedCategory);
                /* forkJoin([
                    this.viewConfigService.GetAllMPMViewConfigBPM(defaultCategory.id),
                    this.indexerConfigService.setIndexerConfigByCategoryId(defaultCategory.id),
                ]).subscribe(viewResponse => {
                    this.sharingService.setAllApplicationViewConfig(viewResponse[0] ? viewResponse[0] : []);
                    this.categoryService.getAllCategoryLevelDetailsByCategoryId(defaultCategory.id)
                        .subscribe(categoryLevelResponse => {
                            this.sharingService.setCategoryLevels(categoryLevelResponse);
                            observer.next(true);
                            observer.complete();
                        }, error => {
                            observer.error(error);
                        });
                }, error => {
                    observer.error(error);
                }); */
                forkJoin([
                    _this.viewConfigService.GetAllMPMViewConfigBPM(categories[0]['MPM_Category-id'].Id),
                    _this.indexerConfigService.setIndexerConfigByCategoryId(categories[0]['MPM_Category-id'].Id),
                ]).subscribe(function (viewResponse) {
                    _this.sharingService.setAllApplicationViewConfig(viewResponse[0] ? viewResponse[0] : []);
                    _this.categoryService.getAllCategoryLevelDetailsByCategoryId(categories[0]['MPM_Category-id'].Id)
                        .subscribe(function (categoryLevelResponse) {
                        _this.sharingService.setCategoryLevels(categoryLevelResponse);
                        observer.next(true);
                        observer.complete();
                    }, function (error) {
                        observer.error(error);
                    });
                }, function (error) {
                    observer.error(error);
                });
            });
        });
    };
    UtilService.prototype.initiateDownload = function (fileURL, fileName) {
        // for non-IE
        if (!window['ActiveXObject']) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', fileURL, true);
            xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
            xhr.withCredentials = true;
            xhr.responseType = 'blob';
            xhr.onload = function () {
                var urlCreator = window.URL || window['webkitURL'];
                var imageUrl = urlCreator.createObjectURL(this.response);
                var tag = document.createElement('a');
                tag.href = imageUrl;
                tag.download = fileName;
                document.body.appendChild(tag);
                tag.click();
                document.body.removeChild(tag);
            };
            xhr.send();
        }
        else if (window['ActiveXObject'] && document.execCommand) {
            var newWindow = window.open(fileURL, '_blank');
            newWindow.document.close();
            newWindow.document.execCommand('SaveAs', true, fileName || fileURL);
            newWindow.close();
        }
    };
    UtilService.prototype.isNullOrEmpty = function (data) {
        if ((data['@xsi:nil'] === 'true') || (data['@nil'] === 'true') || (data === '')) {
            return true;
        }
        else {
            return false;
        }
    };
    UtilService.prototype.getBooleanValue = function (data) {
        return data === 'true' ? true : false;
    };
    UtilService.prototype.isValid = function (variableToCheck) {
        if (variableToCheck && variableToCheck != null && variableToCheck !== '') {
            return true;
        }
        return false;
    };
    UtilService.prototype.isArray = function (variableToCheck) {
        if (variableToCheck && variableToCheck != null && Array.isArray(variableToCheck)) {
            return true;
        }
        return false;
    };
    UtilService.prototype.currentUserHasRole = function (roleName) {
        var e_2, _a;
        var hasRole = false;
        if (!roleName) {
            return hasRole;
        }
        var currentUserRoles = this.sharingService.getCurrentUserObject().ManagerFor.Target;
        if (currentUserRoles && !Array.isArray(currentUserRoles)) {
            var obj = currentUserRoles;
            currentUserRoles = [obj];
        }
        try {
            for (var currentUserRoles_1 = __values(currentUserRoles), currentUserRoles_1_1 = currentUserRoles_1.next(); !currentUserRoles_1_1.done; currentUserRoles_1_1 = currentUserRoles_1.next()) {
                var currentUserRole = currentUserRoles_1_1.value;
                if (currentUserRole.Name && (currentUserRole.Name.toLowerCase() === roleName.toLowerCase())) {
                    hasRole = true;
                    break;
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (currentUserRoles_1_1 && !currentUserRoles_1_1.done && (_a = currentUserRoles_1.return)) _a.call(currentUserRoles_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return hasRole;
    };
    UtilService.prototype.isEqualWithCase = function (string1, string2) {
        if (string1 && string2 && (string1 === string2)) {
            return true;
        }
        return false;
    };
    UtilService.prototype.openWindow = function (urlToOpen, windowName, features) {
        var windowHandle = null;
        var previousURL = null;
        if (windowHandle == null || windowHandle.closed) {
            windowHandle = window.open(urlToOpen, windowName, features);
        }
        else if (previousURL !== urlToOpen) {
            windowHandle = window.open(urlToOpen, windowName, features);
            windowHandle.focus();
        }
        else {
            windowHandle.focus();
        }
        previousURL = urlToOpen;
    };
    UtilService.prototype.getAllQueryParametersFromURL = function (url) {
        var e_3, _a;
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        var obj = {};
        if (queryString) {
            queryString = queryString.split('#')[0];
            var queries = queryString.split('&');
            try {
                for (var queries_1 = __values(queries), queries_1_1 = queries_1.next(); !queries_1_1.done; queries_1_1 = queries_1.next()) {
                    var query = queries_1_1.value;
                    var a = query.split('=');
                    var paramName = a[0];
                    var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
                    paramName = paramName.toLowerCase();
                    if (typeof paramValue === 'string') {
                        paramValue = paramValue.toLowerCase();
                    }
                    if (paramName.match(/\[(\d+)?\]$/)) {
                        var key = paramName.replace(/\[(\d+)?\]/, '');
                        if (!obj[key]) {
                            obj[key] = [];
                        }
                        if (paramName.match(/\[\d+\]$/)) {
                            var index = /\[(\d+)\]/.exec(paramName)[1];
                            obj[key][index] = paramValue;
                        }
                        else {
                            obj[key].push(paramValue);
                        }
                    }
                    else {
                        if (!obj[paramName]) {
                            obj[paramName] = paramValue;
                        }
                        else if (obj[paramName] && typeof obj[paramName] === 'string') {
                            obj[paramName] = [obj[paramName]];
                            obj[paramName].push(paramValue);
                        }
                        else {
                            obj[paramName].push(paramValue);
                        }
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (queries_1_1 && !queries_1_1.done && (_a = queries_1.return)) _a.call(queries_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
        return obj;
    };
    UtilService.prototype.getQueryParameterFromURL = function (parameter, url) {
        var reg = new RegExp('[?&]' + parameter + '=([^&#]*)', 'i');
        var str = reg.exec(url);
        return str ? str[1] : undefined;
    };
    UtilService.prototype.getPriorityIcon = function (name, categoryLevelType) {
        var currentCategoryLevel = this.sharingService.getCategoryLevels().find(function (eachCategoryLevel) {
            return eachCategoryLevel.CATEGORY_LEVEL_TYPE === categoryLevelType;
        });
        var priorityObj = null;
        var priorityReturnObj = { icon: '', color: '', tooltip: '' };
        if (currentCategoryLevel && currentCategoryLevel['MPM_Category_Level-id']) {
            priorityObj = this.sharingService.getAllPriorities()
                .find(function (priority) { return name === priority.NAME && priority.R_PO_CATEGORY_LEVEL['MPM_Category_Level-id'].Id === currentCategoryLevel['MPM_Category_Level-id'].Id; });
            if (!priorityObj) {
                return priorityReturnObj;
            }
            return {
                icon: priorityObj.ICON_DIRECTION === 'Up' ? 'arrow_upward' : 'arrow_downward',
                color: priorityObj.ICON_COLOR,
                tooltip: priorityObj.DESCRIPTION
            };
        }
        return priorityReturnObj;
    };
    /* Define function for escaping user input to be treated as
        a literal string within a regular expression */
    UtilService.prototype.escapeRegExp = function (str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    };
    /* Define functin to find and replace specified term with replacement string */
    UtilService.prototype.replaceAll = function (str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);
    };
    UtilService.prototype.getLookupDomainValuesById = function (lookupDomainId) {
        var lookupDomains = this.sharingService.getAllLookupDomains();
        if (Array.isArray(lookupDomains) && lookupDomains.length === 0) {
            return {};
        }
        var tmpLookupDomains = lookupDomains.find(function (lookupDomain) { return lookupDomain.domainId === lookupDomainId; });
        if (tmpLookupDomains && !Array.isArray(tmpLookupDomains)) {
            return tmpLookupDomains.domainValues;
        }
        return {};
    };
    UtilService.prototype.GetRoleDNByRoleId = function (roleId) {
        var allRoles = this.sharingService.getAllRoles();
        return allRoles.find(function (role) { return role['MPM_APP_Roles-id'].Id === roleId; });
    };
    UtilService.prototype.getTeamRoleDNByRoleId = function (roleId) {
        var allRoles = this.sharingService.getAllTeamRoles();
        return allRoles.find(function (role) { return parseInt(role['MPM_Team_Role_Mapping-id'].Id) === roleId; });
    };
    UtilService.prototype.convertToOTMMDateFormat = function (date) {
        date = new Date(date);
        return moment(date).format('MM/DD/YYYYT00:00:00');
    };
    UtilService.prototype.removeSpclCharsFromAssetName = function (assetName) {
        assetName = assetName.trim();
        if (assetName.includes('$') || assetName.includes('\'')) {
            assetName = assetName.replace(/[$']/g, '_');
        }
        return assetName;
    };
    UtilService.prototype.convertStringToNumber = function (data) {
        var pageSize = data;
        var pageNumber = +pageSize;
        return pageNumber;
    };
    UtilService.prototype.getDisplayOrderData = function (data, displayOrder) {
        var fieldObjArray = [];
        if (this.isValid(displayOrder) && !displayOrder['@null']) {
            var arrDisplayOrder = displayOrder.split(',');
            arrDisplayOrder.forEach(function (element) {
                fieldObjArray.push(data.find(function (fieldData) {
                    return fieldData.MAPPER_NAME === element;
                }));
            });
        }
        return (fieldObjArray.length > 0 && data.length === fieldObjArray.length) ? fieldObjArray : data;
    };
    UtilService.ctorParameters = function () { return [
        { type: AppService },
        { type: EntityAppDefService },
        { type: SharingService },
        { type: CategoryService },
        { type: StatusService },
        { type: FieldConfigService },
        { type: ViewConfigService },
        { type: IndexerConfigService }
    ]; };
    UtilService.ɵprov = i0.ɵɵdefineInjectable({ factory: function UtilService_Factory() { return new UtilService(i0.ɵɵinject(i1.AppService), i0.ɵɵinject(i2.EntityAppDefService), i0.ɵɵinject(i3.SharingService), i0.ɵɵinject(i4.CategoryService), i0.ɵɵinject(i5.StatusService), i0.ɵɵinject(i6.FieldConfigService), i0.ɵɵinject(i7.ViewConfigService), i0.ɵɵinject(i8.IndexerConfigService)); }, token: UtilService, providedIn: "root" });
    UtilService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], UtilService);
    return UtilService;
}());
export { UtilService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL3NlcnZpY2VzL3V0aWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBRWpGLE9BQU8sS0FBSyxPQUFPLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7QUFJbEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBRTNGLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDM0QsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7Ozs7Ozs7Ozs7QUFFNUIsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBTXZCO0lBRUkscUJBQ1csVUFBc0IsRUFDdEIsbUJBQXdDLEVBQ3hDLGNBQThCLEVBQzlCLGVBQWdDLEVBQ2hDLGFBQTRCLEVBQzVCLGtCQUFzQyxFQUN0QyxpQkFBb0MsRUFDcEMsb0JBQTBDO1FBUDFDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBQzVCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBS3JELGlCQUFZLEdBQWUsRUFBRSxDQUFDO1FBQzlCLFlBQU8sR0FBRyxxREFBcUQsQ0FBQztRQUNoRSxjQUFTLEdBQUcsb0NBQW9DLENBQUM7UUFDakQsOEJBQXlCLEdBQUcsd0JBQXdCLENBQUM7SUFQakQsQ0FBQztJQVVMLDhDQUF3QixHQUF4QixVQUF5QixxQkFBMEI7UUFDL0MsSUFBSSxDQUFDLHdCQUF3QixHQUFHLHFCQUFxQixDQUFDO0lBQzFELENBQUM7SUFFRCw4Q0FBd0IsR0FBeEI7UUFDSSxPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUN6QyxDQUFDO0lBRUQsbUNBQWEsR0FBYjtRQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekMsQ0FBQztJQUVELHdDQUFrQixHQUFsQixVQUFtQixXQUF5Qjs7UUFDeEMsSUFBTSxXQUFXLEdBQWlCLEVBQUUsQ0FBQzs7WUFDckMsS0FBeUIsSUFBQSxnQkFBQSxTQUFBLFdBQVcsQ0FBQSx3Q0FBQSxpRUFBRTtnQkFBakMsSUFBTSxVQUFVLHdCQUFBO2dCQUNqQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUN0RCxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNoQzthQUNKOzs7Ozs7Ozs7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5QyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsNkJBQTZCO0lBQzdCLHdDQUFrQixHQUFsQixVQUFtQixVQUFXO1FBQTlCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0YsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsV0FBVztvQkFDdkYsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQy9GLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDZCQUE2QjtJQUM3Qiw0Q0FBc0IsR0FBdEIsVUFBdUIsVUFBVztRQUFsQyxpQkFhQztRQVpHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDaEYsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pHLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFdBQVc7b0JBQ3JGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO29CQUNuRyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxpQ0FBVyxHQUFYLFVBQVksVUFBVTtRQUNsQixJQUFNLE9BQU8sR0FBRyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRyxDQUFDO1FBR3JGLE9BQU8sT0FBTyxHQUFHLFVBQVUsQ0FBQztJQUVoQyxDQUFDO0lBRUQsYUFBYTtJQUNiLDZCQUFPLEdBQVA7UUFDSSxJQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNILEdBQUcsRUFBRSxNQUFNLENBQUMsa0NBQWtDLEVBQUU7WUFDaEQsSUFBSSxFQUFFLEVBQUU7WUFDUixXQUFXLEVBQUUsSUFBSTtZQUNqQixXQUFXLEVBQUUsa0JBQWtCO1lBQy9CLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLE1BQU0sRUFBRSxLQUFLO1NBSWhCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNO1lBQ1gsSUFBTSxZQUFZLEdBQUc7Z0JBQ2pCLFVBQVUsRUFBRSxNQUFNLENBQUMsVUFBVTtnQkFDN0IsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNO2dCQUNyQixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7Z0JBQ25CLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTTtnQkFDckIsYUFBYSxFQUFFLE1BQU0sQ0FBQyxhQUFhO2dCQUNuQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7YUFDdEIsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRTFCLHdFQUF3RTtZQUN4RSw4Q0FBOEM7WUFDOUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNqRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2xHLElBQUksT0FBTyxNQUFNLENBQUMsS0FBSyxLQUFLLFdBQVcsRUFBRTtnQkFDckMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUMvQjtpQkFBTTtnQkFDSCxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzlCO1FBRUwsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSztZQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDbEIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRCxpQ0FBVyxHQUFYO1FBQUEsaUJBc0VDO1FBckVHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxlQUFlO1lBQ2pDO2tEQUNzQztZQUN0QyxLQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUM1QyxJQUFNLEtBQUssR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQzNCLGFBQWE7Z0JBQ1gsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFO29CQUNkLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQ2YsVUFBQyxTQUFTO3dCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQzNCLENBQUMsQ0FDSixDQUFDLElBQUksQ0FBQyxVQUFDLE9BQU87d0JBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDckIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLENBQUM7aUJBQ047Z0JBRUgsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbEQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDN0MsSUFBTSxXQUFXLEdBQVEsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxXQUFXLEVBQUU7b0JBQ2IsSUFBSSxXQUFXLENBQUMsVUFBVSxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDekcsS0FBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztxQkFDckQ7eUJBQU0sSUFBSSxXQUFXLENBQUMsVUFBVSxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTTt3QkFDOUQsT0FBTyxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssUUFBUSxFQUFFO3dCQUNyRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3FCQUN6RDtvQkFDRCxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyRixLQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN0RCxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDekQsS0FBSSxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQzNFLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDdEYsSUFBTSxVQUFVLEdBQUc7d0JBQ2YsTUFBTSxFQUFFLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7cUJBQ2pELENBQUM7b0JBQ0YsS0FBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUM7eUJBQy9DLFNBQVMsQ0FBQyxVQUFBLG1CQUFtQjt3QkFDMUIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUNwRyxLQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQ25GLEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFOzZCQUN4QixTQUFTLENBQUMsVUFBQSxZQUFZOzRCQUNuQixJQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQyxDQUFDOzRCQUN2RSxLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDdkMsNEJBQTRCOzRCQUM1Qix3RUFBd0U7NEJBQ3hFLEtBQUksQ0FBQyxrQkFBa0IsRUFBRTtpQ0FDcEIsU0FBUyxDQUFDLFVBQUEsc0JBQXNCO2dDQUM3QixLQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0NBQ2hFLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDL0IsZUFBZSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQ0FDbEMsZUFBZSxDQUFDLFFBQVEsRUFBRSxDQUFDOzRCQUMvQixDQUFDLEVBQUUsVUFBQSxtQkFBbUI7Z0NBQ2xCLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDOzRCQUMxRCxDQUFDLENBQUMsQ0FBQzt3QkFDWCxDQUFDLEVBQUUsVUFBQSxVQUFVOzRCQUNULGVBQWUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQ2xDLCtDQUErQzt3QkFDbkQsQ0FBQyxDQUFDLENBQUM7b0JBQ1gsQ0FBQyxFQUFFLFVBQUEsZ0JBQWdCO3dCQUNmLGVBQWUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQ2xDLHFEQUFxRDtvQkFDekQsQ0FBQyxDQUFDLENBQUM7aUJBQ1Y7cUJBQU07b0JBQ0gsZUFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7aUJBQ2xFO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixlQUFlLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQ0FBWSxHQUFaO1FBQ0ksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDOUIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1NBQzVCO2FBQU07WUFDSCxPQUFPLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBRUQsbUNBQWEsR0FBYjtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMseUJBQXlCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDN0YsQ0FBQztJQUVELG9DQUFjLEdBQWQsVUFBZSxXQUFXO1FBQTFCLGlCQWdCQztRQWZHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksV0FBVyxJQUFJLElBQUksSUFBSSxXQUFXLEVBQUU7Z0JBQ3BDLEtBQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQztnQkFDekMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQztxQkFDdEQsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDZixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLENBQUM7YUFDVjtpQkFBTTtnQkFDSCxRQUFRLENBQUMsS0FBSyxDQUFDLDJEQUEyRDtvQkFDdEUsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQ2xDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0NBQWdCLEdBQWhCO1FBQUEsaUJBdURDO1FBdERHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLFFBQVEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLEVBQUU7Z0JBQ25ELEtBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3ZDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDM0MsS0FBSSxDQUFDLGFBQWEsQ0FBQyx5QkFBeUIsRUFBRTtnQkFDOUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixFQUFFO2FBRTdDLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNwQixRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQyxFQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQTNHLENBQTJHLENBQUMsQ0FBQTtnQkFDbkksS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELEtBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2RSxLQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDNUUsbUZBQW1GO2dCQUNuRiwyRUFBMkU7Z0JBQzNFLElBQU0sVUFBVSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkQsK0dBQStHO2dCQUMvRyw2REFBNkQ7Z0JBQzdEOzs7Ozs7Ozs7Ozs7Ozs7c0JBZU07Z0JBQ04sUUFBUSxDQUFDO29CQUNMLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xGLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyw0QkFBNEIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQzlGLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxZQUFZO29CQUNyQixLQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDeEYsS0FBSSxDQUFDLGVBQWUsQ0FBQyxzQ0FBc0MsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7eUJBQzNGLFNBQVMsQ0FBQyxVQUFBLHFCQUFxQjt3QkFDNUIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO3dCQUM3RCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7d0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsQ0FBQyxFQUFFLFVBQUEsS0FBSztvQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUMxQixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsc0NBQWdCLEdBQWhCLFVBQWlCLE9BQU8sRUFBRSxRQUFRO1FBQzlCLGFBQWE7UUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQzFCLElBQU0sR0FBRyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7WUFDakMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQy9CLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUN6RCxHQUFHLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUMzQixHQUFHLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQztZQUMxQixHQUFHLENBQUMsTUFBTSxHQUFHO2dCQUNULElBQU0sVUFBVSxHQUFHLE1BQU0sQ0FBQyxHQUFHLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyRCxJQUFNLFFBQVEsR0FBRyxVQUFVLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0QsSUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEMsR0FBRyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7Z0JBQ3BCLEdBQUcsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO2dCQUN4QixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDL0IsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNaLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25DLENBQUMsQ0FBQztZQUNGLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNkO2FBQU0sSUFBSSxNQUFNLENBQUMsZUFBZSxDQUFDLElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRTtZQUN4RCxJQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztZQUNqRCxTQUFTLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzNCLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxJQUFJLE9BQU8sQ0FBQyxDQUFDO1lBQ3BFLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNyQjtJQUNMLENBQUM7SUFFRCxtQ0FBYSxHQUFiLFVBQWMsSUFBSTtRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDLEVBQUU7WUFDN0UsT0FBTyxJQUFJLENBQUM7U0FDZjthQUFNO1lBQ0gsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDO0lBRUQscUNBQWUsR0FBZixVQUFnQixJQUFJO1FBQ2hCLE9BQU8sSUFBSSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDMUMsQ0FBQztJQUVELDZCQUFPLEdBQVAsVUFBUSxlQUFlO1FBQ25CLElBQUksZUFBZSxJQUFJLGVBQWUsSUFBSSxJQUFJLElBQUksZUFBZSxLQUFLLEVBQUUsRUFBRTtZQUN0RSxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELDZCQUFPLEdBQVAsVUFBUSxlQUFlO1FBQ25CLElBQUksZUFBZSxJQUFJLGVBQWUsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUM5RSxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELHdDQUFrQixHQUFsQixVQUFtQixRQUFROztRQUN2QixJQUFJLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBQ0QsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixFQUFFLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztRQUNwRixJQUFJLGdCQUFnQixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO1lBQ3RELElBQU0sR0FBRyxHQUFHLGdCQUFnQixDQUFDO1lBQzdCLGdCQUFnQixHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDNUI7O1lBQ0QsS0FBOEIsSUFBQSxxQkFBQSxTQUFBLGdCQUFnQixDQUFBLGtEQUFBLGdGQUFFO2dCQUEzQyxJQUFNLGVBQWUsNkJBQUE7Z0JBQ3RCLElBQUksZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLEVBQUU7b0JBQ3pGLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQ2YsTUFBTTtpQkFDVDthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0lBRUQscUNBQWUsR0FBZixVQUFnQixPQUFPLEVBQUUsT0FBTztRQUM1QixJQUFJLE9BQU8sSUFBSSxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssT0FBTyxDQUFDLEVBQUU7WUFDN0MsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxnQ0FBVSxHQUFWLFVBQVcsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRO1FBQ3RDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxZQUFZLElBQUksSUFBSSxJQUFJLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDN0MsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUMvRDthQUFNLElBQUksV0FBVyxLQUFLLFNBQVMsRUFBRTtZQUNsQyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzVELFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN4QjthQUFNO1lBQ0gsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3hCO1FBQ0QsV0FBVyxHQUFHLFNBQVMsQ0FBQztJQUM1QixDQUFDO0lBRUQsa0RBQTRCLEdBQTVCLFVBQTZCLEdBQUc7O1FBQzVCLElBQUksV0FBVyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzVFLElBQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUVmLElBQUksV0FBVyxFQUFFO1lBQ2IsV0FBVyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEMsSUFBTSxPQUFPLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs7Z0JBRXZDLEtBQW9CLElBQUEsWUFBQSxTQUFBLE9BQU8sQ0FBQSxnQ0FBQSxxREFBRTtvQkFBeEIsSUFBTSxLQUFLLG9CQUFBO29CQUNaLElBQU0sQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzNCLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckIsSUFBSSxVQUFVLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBRTdELFNBQVMsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BDLElBQUksT0FBTyxVQUFVLEtBQUssUUFBUSxFQUFFO3dCQUFFLFVBQVUsR0FBRyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7cUJBQUU7b0JBRTlFLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsRUFBRTt3QkFDaEMsSUFBTSxHQUFHLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7d0JBQ2hELElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7NEJBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQzt5QkFBRTt3QkFFakMsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUM3QixJQUFNLEtBQUssR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUM3QyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsVUFBVSxDQUFDO3lCQUNoQzs2QkFBTTs0QkFDSCxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUM3QjtxQkFDSjt5QkFBTTt3QkFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxFQUFFOzRCQUNqQixHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsVUFBVSxDQUFDO3lCQUMvQjs2QkFBTSxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxRQUFRLEVBQUU7NEJBQzdELEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNsQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNuQzs2QkFBTTs0QkFDSCxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNuQztxQkFDSjtpQkFDSjs7Ozs7Ozs7O1NBQ0o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRCw4Q0FBd0IsR0FBeEIsVUFBeUIsU0FBUyxFQUFFLEdBQUc7UUFDbkMsSUFBTSxHQUFHLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxXQUFXLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDOUQsSUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxQixPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFDcEMsQ0FBQztJQUVELHFDQUFlLEdBQWYsVUFBZ0IsSUFBWSxFQUFFLGlCQUE0QjtRQUN0RCxJQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxpQkFBaUI7WUFDeEYsT0FBTyxpQkFBaUIsQ0FBQyxtQkFBbUIsS0FBSyxpQkFBaUIsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksV0FBVyxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFNLGlCQUFpQixHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUMvRCxJQUFJLG9CQUFvQixJQUFJLG9CQUFvQixDQUFDLHVCQUF1QixDQUFDLEVBQUU7WUFDdkUsV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUU7aUJBQy9DLElBQUksQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLElBQUksS0FBSyxRQUFRLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsS0FBSyxvQkFBb0IsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLEVBQUUsRUFBdkksQ0FBdUksQ0FBQyxDQUFDO1lBQy9KLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2QsT0FBTyxpQkFBaUIsQ0FBQzthQUM1QjtZQUNELE9BQU87Z0JBQ0gsSUFBSSxFQUFFLFdBQVcsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLGdCQUFnQjtnQkFDN0UsS0FBSyxFQUFFLFdBQVcsQ0FBQyxVQUFVO2dCQUM3QixPQUFPLEVBQUUsV0FBVyxDQUFDLFdBQVc7YUFDbkMsQ0FBQztTQUNMO1FBQ0QsT0FBTyxpQkFBaUIsQ0FBQztJQUM3QixDQUFDO0lBRUQ7dURBQ21EO0lBQ25ELGtDQUFZLEdBQVosVUFBYSxHQUFHO1FBQ1osT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCwrRUFBK0U7SUFDL0UsZ0NBQVUsR0FBVixVQUFXLEdBQUcsRUFBRSxJQUFJLEVBQUUsV0FBVztRQUM3QixPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBRUQsK0NBQXlCLEdBQXpCLFVBQTBCLGNBQXNCO1FBQzVDLElBQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUNoRSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksYUFBYSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDNUQsT0FBTyxFQUFFLENBQUM7U0FDYjtRQUNELElBQU0sZ0JBQWdCLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxRQUFRLEtBQUssY0FBYyxFQUF4QyxDQUF3QyxDQUFDLENBQUM7UUFDdEcsSUFBSSxnQkFBZ0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtZQUN0RCxPQUFPLGdCQUFnQixDQUFDLFlBQVksQ0FBQztTQUN4QztRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVELHVDQUFpQixHQUFqQixVQUFrQixNQUFNO1FBQ3BCLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRSxLQUFLLE1BQU0sRUFBdEMsQ0FBc0MsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFRCwyQ0FBcUIsR0FBckIsVUFBc0IsTUFBTTtRQUN4QixJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxNQUFNLEVBQXhELENBQXdELENBQUMsQ0FBQztJQUMzRixDQUFDO0lBRUQsNkNBQXVCLEdBQXZCLFVBQXdCLElBQUk7UUFDeEIsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxrREFBNEIsR0FBNUIsVUFBNkIsU0FBUztRQUNsQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzdCLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JELFNBQVMsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztTQUMvQztRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyQ0FBcUIsR0FBckIsVUFBc0IsSUFBSTtRQUN0QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxVQUFVLEdBQVcsQ0FBQyxRQUFRLENBQUM7UUFDbkMsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUVELHlDQUFtQixHQUFuQixVQUFvQixJQUFJLEVBQUUsWUFBWTtRQUNsQyxJQUFNLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3RELElBQU0sZUFBZSxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEQsZUFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFBLE9BQU87Z0JBQzNCLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFBLFNBQVM7b0JBQ2xDLE9BQU8sU0FBUyxDQUFDLFdBQVcsS0FBSyxPQUFPLENBQUM7Z0JBQzdDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDUixDQUFDLENBQUMsQ0FBQztTQUNOO1FBRUQsT0FBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNyRyxDQUFDOztnQkEzZnNCLFVBQVU7Z0JBQ0QsbUJBQW1CO2dCQUN4QixjQUFjO2dCQUNiLGVBQWU7Z0JBQ2pCLGFBQWE7Z0JBQ1Isa0JBQWtCO2dCQUNuQixpQkFBaUI7Z0JBQ2Qsb0JBQW9COzs7SUFWNUMsV0FBVztRQUp2QixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BRVcsV0FBVyxDQWdnQnZCO3NCQTNoQkQ7Q0EyaEJDLEFBaGdCRCxJQWdnQkM7U0FoZ0JZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBpc0Rldk1vZGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRW50aXR5QXBwRGVmU2VydmljZSB9IGZyb20gJy4vZW50aXR5LmFwcGRlZi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2hhcmluZ1NlcnZpY2UgfSBmcm9tICcuL3NoYXJpbmcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcFNlcnZpY2UgfSBmcm9tICcuL2FwcC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZm9ya0pvaW4gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcHJvamVjdC9zaGFyZWQvc2VydmljZXMvY2F0ZWdvcnkuc2VydmljZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgTVBNX0xFVkVMIH0gZnJvbSAnLi4vb2JqZWN0cy9MZXZlbCc7XHJcbmltcG9ydCB7IFJvbGVzIH0gZnJvbSAnLi4vb2JqZWN0cy9Sb2xlcyc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zaGFyZWQvc2VydmljZXMvc2VhcmNoLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdHVzU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9zdGF0dXMuc2VydmljZSc7XHJcbmltcG9ydCB7IEZpZWxkQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy9maWVsZC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IEluZGV4ZXJDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2hhcmVkL3NlcnZpY2VzL2luZGV4ZXIvbXBtLWluZGV4ZXIuY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBWaWV3Q29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL3NoYXJlZC9zZXJ2aWNlcy92aWV3LWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMgfSBmcm9tICcuLi8uLi9zaGFyZWQvY29uc3RhbnRzL3Nlc3Npb24tc3RvcmFnZS5jb25zdGFudHMnO1xyXG5cclxuaW1wb3J0IHsgR2xvYWJhbENvbmZpZyBhcyBjb25maWcgfSBmcm9tICcuLi9jb25maWcvY29uZmlnJztcclxuaW1wb3J0ICogYXMgJCBmcm9tICdqcXVlcnknO1xyXG5cclxuY29uc3QgbW9tZW50ID0gbW9tZW50XztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFV0aWxTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYXBwU2VydmljZTogQXBwU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZW50aXR5QXBwRGVmU2VydmljZTogRW50aXR5QXBwRGVmU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc2hhcmluZ1NlcnZpY2U6IFNoYXJpbmdTZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBjYXRlZ29yeVNlcnZpY2U6IENhdGVnb3J5U2VydmljZSxcclxuICAgICAgICBwdWJsaWMgc3RhdHVzU2VydmljZTogU3RhdHVzU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZmllbGRDb25maWdTZXJ2aWNlOiBGaWVsZENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHVibGljIHZpZXdDb25maWdTZXJ2aWNlOiBWaWV3Q29uZmlnU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgaW5kZXhlckNvbmZpZ1NlcnZpY2U6IEluZGV4ZXJDb25maWdTZXJ2aWNlXHJcbiAgICApIHsgfVxyXG5cclxuICAgIEFQUF9QQVRIOiBzdHJpbmc7XHJcbiAgICBBUFBfSUQ6IHN0cmluZztcclxuICAgIHJvbGVzRm9yVXNlcjogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgV0xfTlNQQyA9ICdodHRwOi8vc2NoZW1hcy5jb3JkeXMuY29tL25vdGlmaWNhdGlvbi93b3JrZmxvdy8xLjAnO1xyXG4gICAgVVNFUl9OU1BDID0gJ2h0dHA6Ly9zY2hlbWFzLmNvcmR5cy5jb20vMS4xL2xkYXAnO1xyXG4gICAgR0VUX0FMTF9XT1JLTElTVF9GT1JfVVNFUiA9ICdHZXRBbGxXb3JrbGlzdHNGb3JVc2VyJztcclxuICAgIG5ld1Byb2plY3RDdXN0b21NZXRhZGF0YTogYW55O1xyXG5cclxuICAgIHNldFByb2plY3RDdXN0b21NZXRhZGF0YShwcm9qZWN0Q3VzdG9tTWV0YWRhdGE6IGFueSkge1xyXG4gICAgICAgIHRoaXMubmV3UHJvamVjdEN1c3RvbU1ldGFkYXRhID0gcHJvamVjdEN1c3RvbU1ldGFkYXRhO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByb2plY3RDdXN0b21NZXRhZGF0YSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5ld1Byb2plY3RDdXN0b21NZXRhZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBuYXZpZ2F0ZVRvQXBwKCkge1xyXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gdGhpcy5BUFBfUEFUSDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRVc2VyTWFwcGVkUm9sZXMocm9sZU1hcHBlcnM6IEFycmF5PFJvbGVzPikge1xyXG4gICAgICAgIGNvbnN0IG1hcHBlZFJvbGVzOiBBcnJheTxSb2xlcz4gPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IHJvbGVNYXBwZXIgb2Ygcm9sZU1hcHBlcnMpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5oYXNSb2xlKHJvbGVNYXBwZXIuUk9MRV9ETikpIHtcclxuICAgICAgICAgICAgICAgIG1hcHBlZFJvbGVzLnB1c2gocm9sZU1hcHBlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRVc2VyUm9sZXMobWFwcGVkUm9sZXMpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdzaGFyaW5nIHVzZXIgcm9sZXMnLCB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldFVzZXJSb2xlcygpKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBhZGRlZCBmb3IgY29kZSBoYXJkZW5paW5nIFxyXG4gICAgZ2V0VXNlckRldGFpbHNMREFQKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5VU0VSX0RFVEFJTFNfTERBUCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19MREFQKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5pbnZva2VSZXF1ZXN0KHRoaXMuVVNFUl9OU1BDLCAnR2V0VXNlckRldGFpbHMnLCBudWxsKS5zdWJzY3JpYmUodXNlckRldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX0xEQVAsIEpTT04uc3RyaW5naWZ5KHVzZXJEZXRhaWxzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gYWRkZWQgZm9yIGNvZGUgaGFyZGVuaWluZyBcclxuICAgIGdldFVzZXJEZXRhaWxzV29ya2Zsb3cocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19XT1JLRkxPVykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19XT1JLRkxPVykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLldMX05TUEMsICdHZXRVc2VyRGV0YWlscycsIG51bGwpLnN1YnNjcmliZSh1c2VyRGV0YWlscyA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5VU0VSX0RFVEFJTFNfV09SS0ZMT1csIEpTT04uc3RyaW5naWZ5KHVzZXJEZXRhaWxzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBmb3JtUmVzdFVybChvdG1tYXBpdXJsKSB7XHJcbiAgICAgICAgY29uc3QgYmFzZVVybCA9IGlzRGV2TW9kZSgpID8gJy4vJyA6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKCkudXJsO1xyXG5cclxuXHJcbiAgICAgICAgcmV0dXJuIGJhc2VVcmwgKyBvdG1tYXBpdXJsO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvLyBNUE1WMy0yMjM3XHJcbiAgICBpbmRleGVyKCkge1xyXG4gICAgICAgIGNvbnN0IGRlZmVyID0gJC5EZWZlcnJlZCgpO1xyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgIHVybDogY29uZmlnLmdldE9URFNSRVNUQXV0aEVuZHBvaW50d2l0aEhlYWRlcnMoKSwgLy90aGlzLmZvcm1SZXN0VXJsKCdvdGRzd3MvcmVzdC9hdXRoZW50aWNhdGlvbi9oZWFkZXJzJyksIC8vY29uZmlnLmdldE9URFNSRVNUQXV0aEVuZHBvaW50d2l0aEhlYWRlcnMoKSwgXHJcbiAgICAgICAgICAgIGRhdGE6IHt9LFxyXG4gICAgICAgICAgICBjcm9zc0RvbWFpbjogdHJ1ZSxcclxuICAgICAgICAgICAgY29udGVudFR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJyxcclxuICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyxcclxuICAgICAgICAgICAvLyBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgLy8gICAgICdPVERTVGlja2V0JzogSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVElDS0VUUykpXHJcbiAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIH0pLmRvbmUoKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBvdGRzQXV0aEluZm8gPSB7XHJcbiAgICAgICAgICAgICAgICByZXNvdXJjZUlkOiByZXN1bHQucmVzb3VyY2VJRCxcclxuICAgICAgICAgICAgICAgIHRpY2tldDogcmVzdWx0LnRpY2tldCxcclxuICAgICAgICAgICAgICAgIHRva2VuOiByZXN1bHQudG9rZW4sXHJcbiAgICAgICAgICAgICAgICB1c2VySWQ6IHJlc3VsdC51c2VySWQsXHJcbiAgICAgICAgICAgICAgICBmYWlsdXJlUmVhc29uOiByZXN1bHQuZmFpbHVyZVJlYXNvbixcclxuICAgICAgICAgICAgICAgIGVycm9yOiByZXN1bHQuZXJyb3JcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIjIyMjIyMjIyMjIyMjIyMjIyMjIyNcIilcclxuICAgICAgICAgICAgY29uc29sZS5sb2cob3Rkc0F1dGhJbmZvKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHN0b3JlIG90ZHMtdGlja2V0IGFuZCB1c2VyIGlkIGluIHNlc3Npb24gc3RvcmFnZSBmb3IgaGVhZGVycyBwdXJwb3NlIFxyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnT3Rkcy10aWNrZXQtLSAgJyxvdGRzQXV0aEluZm8pXHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19USUNLRVQsIEpTT04uc3RyaW5naWZ5KG90ZHNBdXRoSW5mby50aWNrZXQpKTtcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5PVERTX1VTRVJfSUQsIEpTT04uc3RyaW5naWZ5KG90ZHNBdXRoSW5mby51c2VySWQpKTtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuZXJyb3IgPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgICAgICBkZWZlci5yZXNvbHZlKG90ZHNBdXRoSW5mbyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkZWZlci5yZWplY3Qob3Rkc0F1dGhJbmZvKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9KS5mYWlsKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgICAgICAgZGVmZXIucmVqZWN0KGVycm9yKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFVzZXJJbmZvKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKHNldFVzZXJPYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIC8qIGNvbnN0IHVzZXJEZXRhaWxzID0gdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5XTF9OU1BDLCAnR2V0VXNlckRldGFpbHMnLCBudWxsKTtcclxuICAgICAgICAgICAgdXNlckRldGFpbHMuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHsgKi9cclxuICAgICAgICAgICAgdGhpcy5nZXRVc2VyRGV0YWlsc1dvcmtmbG93KCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRlZmVyID0gJC5EZWZlcnJlZCgpO1xyXG4gICAgICAgICAgICAgICAgLy8gTVBNVjMtMjIzN1xyXG4gICAgICAgICAgICAgICAgICBpZiAoIWlzRGV2TW9kZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXIoKS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICh0aWNrZXRPYmopID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGlja2V0T2JqKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICApLmZhaWwoKGVyckNvZGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJDb2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZlci5yZWplY3QoZXJyQ29kZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5zZXRVc2VyRGV0YWlscyhyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVudGl0eUFwcERlZlNlcnZpY2Uuc2V0VXNlckROKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRVc2VyOiBhbnkgPSBhY3JvbnVpLmZpbmRPYmplY3RCeVByb3AocmVzcG9uc2UsICdVc2VyJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY3VycmVudFVzZXIuTWFuYWdlckZvciAmJiBjdXJyZW50VXNlci5NYW5hZ2VyRm9yLlRhcmdldCAmJiBBcnJheS5pc0FycmF5KGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvbGVzRm9yVXNlciA9IGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0O1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY3VycmVudFVzZXIuTWFuYWdlckZvciAmJiBjdXJyZW50VXNlci5NYW5hZ2VyRm9yLlRhcmdldCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlb2YgKGN1cnJlbnRVc2VyLk1hbmFnZXJGb3IuVGFyZ2V0KSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb2xlc0ZvclVzZXIucHVzaChjdXJyZW50VXNlci5NYW5hZ2VyRm9yLlRhcmdldCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJJRChjdXJyZW50VXNlci5Vc2VyRE4uc3BsaXQoJz0nKVsxXS5zcGxpdCgnLCcpWzBdKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEN1cnJlbnRVc2VyT2JqZWN0KGN1cnJlbnRVc2VyKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEN1cnJlbnRVc2VyRE4oY3VycmVudFVzZXIuVXNlckROKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEN1cnJlbnRVc2VyRGlzcGxheU5hbWUoY3VycmVudFVzZXIuVXNlckRpc3BsYXlOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEN1cnJlbnRPcmdETihjdXJyZW50VXNlci5Vc2VyRE4uc3BsaXQoJywnKS5zbGljZSgyKS5qb2luKCcsJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXJDTjogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlcklEKClcclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBwU2VydmljZS5nZXRQZXJzb25EZXRhaWxzQnlVc2VyQ04ocGFyYW1ldGVycylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSh1c2VyRGV0YWlsc1Jlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJJdGVtSUQodXNlckRldGFpbHNSZXNwb25zZS5QZXJzb24uUGVyc29uVG9Vc2VyWydJZGVudGl0eS1pZCddLklkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q3VycmVudFVzZXJFbWFpbElkKHVzZXJEZXRhaWxzUmVzcG9uc2UuUGVyc29uLkVtYWlsLl9fdGV4dCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcFNlcnZpY2UuZ2V0QWxsUm9sZXMoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocm9sZXNSZXNvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgcm9sZXMgPSBhY3JvbnVpLmZpbmRPYmplY3RzQnlQcm9wKHJvbGVzUmVzb25zZSwgJ01QTV9BUFBfUm9sZXMnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxSb2xlcyhyb2xlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvZGUgaGFyZGVuaW5nIC1LZWVydGhhbmFcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5hcHBTZXJ2aWNlLmludm9rZVJlcXVlc3QodGhpcy5VU0VSX05TUEMsICdHZXRVc2VyRGV0YWlscycsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0VXNlckRldGFpbHNMREFQKClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZ2V0VXNlckRldGFpbHNSZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLnNldE1hc3RlclJvbGVzKGdldFVzZXJEZXRhaWxzUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VXNlck1hcHBlZFJvbGVzKHJvbGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIubmV4dChjdXJyZW50VXNlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VXNlck9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCBnZXRVc2VyRGV0YWlsc0Vycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGdldFVzZXJEZXRhaWxzRXJyb3IpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIHJvbGVzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIuZXJyb3IoJ05PQUNDRVNTJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vc2V0VXNlck9ic2VydmVyLmVycm9yKG5ldyBFcnJvcihyb2xlc0Vycm9yKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIHVzZXJEZXRhaWxzRXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0VXNlck9ic2VydmVyLmVycm9yKCdOT0FDQ0VTUycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9zZXRVc2VyT2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKHVzZXJEZXRhaWxzRXJyb3IpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFVzZXJPYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoJ05vIHVzZXIgZGV0YWlscyBhdmFpbGFibGUuJykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZXRVc2VyT2JzZXJ2ZXIuZXJyb3IobmV3IEVycm9yKGVycm9yKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJSb2xlcygpIHtcclxuICAgICAgICBpZiAodGhpcy5yb2xlc0ZvclVzZXIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yb2xlc0ZvclVzZXI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignTm8gcm9sZXMgYXJlIGF2YWlsYWJsZS4nKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlckdyb3VwcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcFNlcnZpY2UuaW52b2tlUmVxdWVzdCh0aGlzLldMX05TUEMsIHRoaXMuR0VUX0FMTF9XT1JLTElTVF9GT1JfVVNFUiwgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U2VsZWN0ZWRBcHAoYXBwbGljYXRpb24pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcHBsaWNhdGlvbiAhPSBudWxsICYmIGFwcGxpY2F0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLkFQUF9JRCA9IGFwcGxpY2F0aW9uLkFQUExJQ0FUSU9OX0lEO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbnRpdHlBcHBEZWZTZXJ2aWNlLnNldEFwcGxpY2F0aW9uRGV0YWlscyhhcHBsaWNhdGlvbilcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignTm8gQXBwbGljYXRpb24gSUQgcHJvcGVydHkgaXMgYXZhaWxhYmxlIGZvciBhcHBsaWNhdGlvbjogJyArXHJcbiAgICAgICAgICAgICAgICAgICAgYXBwbGljYXRpb24uYXBwbGljYXRpb25JZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRDb25maWdzRm9yQXBwKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgZm9ya0pvaW4oW3RoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRNZW51Rm9yVXNlcigpLFxyXG4gICAgICAgICAgICB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRBbGxDYXRlZ29yaWVzKCksXHJcbiAgICAgICAgICAgIHRoaXMuZW50aXR5QXBwRGVmU2VydmljZS5nZXRBbGxQcmlvcml0aWVzKCksXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdHVzU2VydmljZS5nZXRBbGxTdGF0dXNDb25maWdEZXRhaWxzKCksXHJcbiAgICAgICAgICAgIHRoaXMuZmllbGRDb25maWdTZXJ2aWNlLmdldEFsbE1QTUZpZWxkQ29uZmlnKCksXHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRDYXRlZ29yeURldGFpbHMoKVxyXG4gICAgICAgICAgICBdKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgcmVzcG9uc2VbMF0uc29ydCgoYSxiKSA9PiAocGFyc2VJbnQoYS5TRVFVRU5DRSk+IHBhcnNlSW50KGIuU0VRVUVOQ0UpKSA/IDEgOiAoKHBhcnNlSW50KGIuU0VRVUVOQ0UpID4gcGFyc2VJbnQoYS5TRVFVRU5DRSkpID8gLTEgOiAwKSlcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0TWVudShyZXNwb25zZVswXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbENhdGVnb3JpZXMocmVzcG9uc2VbMV0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRBbGxQcmlvcml0aWVzKHJlc3BvbnNlWzJdKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsU3RhdHVzQ29uZmlnKHJlc3BvbnNlWzNdID8gcmVzcG9uc2VbM10gOiBbXSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldEFsbEFwcGxpY2F0aW9uRmllbGRzKHJlc3BvbnNlWzRdID8gcmVzcG9uc2VbNF0gOiBbXSk7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldERlZmF1bHRDYXRlZ29yeURldGFpbHMocmVzcG9uc2VbNV0gPyByZXNwb25zZVs1XSA6IG51bGwpO1xyXG4gICAgICAgICAgICAgICAgLy8gY29uc3QgZGVmYXVsdENhdGVnb3J5ID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXREZWZhdWx0Q2F0ZWdvcnlEZXRhaWxzKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjYXRlZ29yaWVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxDYXRlZ29yaWVzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnNldFNlbGVjdGVkQ2F0ZWdvcnkoY2F0ZWdvcmllc1swXSk7XHJcbiAgICAgICAgICAgICAgICAvLyBjb25zdCBzZWxlY3RlZENhdGVnb3J5ID0gY2F0ZWdvcmllcy5maW5kKGNhdGVnb3J5ID0+IGNhdGVnb3J5WydNUE1fQ2F0ZWdvcnktaWQnXS5JZCA9PT0gZGVmYXVsdENhdGVnb3J5LmlkKTtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0U2VsZWN0ZWRDYXRlZ29yeShzZWxlY3RlZENhdGVnb3J5KTtcclxuICAgICAgICAgICAgICAgIC8qIGZvcmtKb2luKFtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb25maWdTZXJ2aWNlLkdldEFsbE1QTVZpZXdDb25maWdCUE0oZGVmYXVsdENhdGVnb3J5LmlkKSxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmluZGV4ZXJDb25maWdTZXJ2aWNlLnNldEluZGV4ZXJDb25maWdCeUNhdGVnb3J5SWQoZGVmYXVsdENhdGVnb3J5LmlkKSxcclxuICAgICAgICAgICAgICAgIF0pLnN1YnNjcmliZSh2aWV3UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKHZpZXdSZXNwb25zZVswXSA/IHZpZXdSZXNwb25zZVswXSA6IFtdKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRBbGxDYXRlZ29yeUxldmVsRGV0YWlsc0J5Q2F0ZWdvcnlJZChkZWZhdWx0Q2F0ZWdvcnkuaWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoY2F0ZWdvcnlMZXZlbFJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0Q2F0ZWdvcnlMZXZlbHMoY2F0ZWdvcnlMZXZlbFJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIH0pOyAqL1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oW1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlld0NvbmZpZ1NlcnZpY2UuR2V0QWxsTVBNVmlld0NvbmZpZ0JQTShjYXRlZ29yaWVzWzBdWydNUE1fQ2F0ZWdvcnktaWQnXS5JZCksXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbmRleGVyQ29uZmlnU2VydmljZS5zZXRJbmRleGVyQ29uZmlnQnlDYXRlZ29yeUlkKGNhdGVnb3JpZXNbMF1bJ01QTV9DYXRlZ29yeS1pZCddLklkKSxcclxuICAgICAgICAgICAgICAgIF0pLnN1YnNjcmliZSh2aWV3UmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2Uuc2V0QWxsQXBwbGljYXRpb25WaWV3Q29uZmlnKHZpZXdSZXNwb25zZVswXSA/IHZpZXdSZXNwb25zZVswXSA6IFtdKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNhdGVnb3J5U2VydmljZS5nZXRBbGxDYXRlZ29yeUxldmVsRGV0YWlsc0J5Q2F0ZWdvcnlJZChjYXRlZ29yaWVzWzBdWydNUE1fQ2F0ZWdvcnktaWQnXS5JZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShjYXRlZ29yeUxldmVsUmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5zZXRDYXRlZ29yeUxldmVscyhjYXRlZ29yeUxldmVsUmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRpYXRlRG93bmxvYWQoZmlsZVVSTCwgZmlsZU5hbWUpIHtcclxuICAgICAgICAvLyBmb3Igbm9uLUlFXHJcbiAgICAgICAgaWYgKCF3aW5kb3dbJ0FjdGl2ZVhPYmplY3QnXSkge1xyXG4gICAgICAgICAgICBjb25zdCB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcclxuICAgICAgICAgICAgeGhyLm9wZW4oJ0dFVCcsIGZpbGVVUkwsIHRydWUpO1xyXG4gICAgICAgICAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcignQWNjZXNzLUNvbnRyb2wtQWxsb3ctT3JpZ2luJywgJyonKTtcclxuICAgICAgICAgICAgeGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XHJcbiAgICAgICAgICAgIHhoci5yZXNwb25zZVR5cGUgPSAnYmxvYic7XHJcbiAgICAgICAgICAgIHhoci5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1cmxDcmVhdG9yID0gd2luZG93LlVSTCB8fCB3aW5kb3dbJ3dlYmtpdFVSTCddO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaW1hZ2VVcmwgPSB1cmxDcmVhdG9yLmNyZWF0ZU9iamVjdFVSTCh0aGlzLnJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRhZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcclxuICAgICAgICAgICAgICAgIHRhZy5ocmVmID0gaW1hZ2VVcmw7XHJcbiAgICAgICAgICAgICAgICB0YWcuZG93bmxvYWQgPSBmaWxlTmFtZTtcclxuICAgICAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGFnKTtcclxuICAgICAgICAgICAgICAgIHRhZy5jbGljaygpO1xyXG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZCh0YWcpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB4aHIuc2VuZCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAod2luZG93WydBY3RpdmVYT2JqZWN0J10gJiYgZG9jdW1lbnQuZXhlY0NvbW1hbmQpIHtcclxuICAgICAgICAgICAgY29uc3QgbmV3V2luZG93ID0gd2luZG93Lm9wZW4oZmlsZVVSTCwgJ19ibGFuaycpO1xyXG4gICAgICAgICAgICBuZXdXaW5kb3cuZG9jdW1lbnQuY2xvc2UoKTtcclxuICAgICAgICAgICAgbmV3V2luZG93LmRvY3VtZW50LmV4ZWNDb21tYW5kKCdTYXZlQXMnLCB0cnVlLCBmaWxlTmFtZSB8fCBmaWxlVVJMKTtcclxuICAgICAgICAgICAgbmV3V2luZG93LmNsb3NlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlzTnVsbE9yRW1wdHkoZGF0YSkge1xyXG4gICAgICAgIGlmICgoZGF0YVsnQHhzaTpuaWwnXSA9PT0gJ3RydWUnKSB8fCAoZGF0YVsnQG5pbCddID09PSAndHJ1ZScpIHx8IChkYXRhID09PSAnJykpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRCb29sZWFuVmFsdWUoZGF0YSkge1xyXG4gICAgICAgIHJldHVybiBkYXRhID09PSAndHJ1ZScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZCh2YXJpYWJsZVRvQ2hlY2spIHtcclxuICAgICAgICBpZiAodmFyaWFibGVUb0NoZWNrICYmIHZhcmlhYmxlVG9DaGVjayAhPSBudWxsICYmIHZhcmlhYmxlVG9DaGVjayAhPT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0FycmF5KHZhcmlhYmxlVG9DaGVjaykge1xyXG4gICAgICAgIGlmICh2YXJpYWJsZVRvQ2hlY2sgJiYgdmFyaWFibGVUb0NoZWNrICE9IG51bGwgJiYgQXJyYXkuaXNBcnJheSh2YXJpYWJsZVRvQ2hlY2spKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgY3VycmVudFVzZXJIYXNSb2xlKHJvbGVOYW1lKSB7XHJcbiAgICAgICAgbGV0IGhhc1JvbGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAoIXJvbGVOYW1lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBoYXNSb2xlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgY3VycmVudFVzZXJSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFVzZXJPYmplY3QoKS5NYW5hZ2VyRm9yLlRhcmdldDtcclxuICAgICAgICBpZiAoY3VycmVudFVzZXJSb2xlcyAmJiAhQXJyYXkuaXNBcnJheShjdXJyZW50VXNlclJvbGVzKSkge1xyXG4gICAgICAgICAgICBjb25zdCBvYmogPSBjdXJyZW50VXNlclJvbGVzO1xyXG4gICAgICAgICAgICBjdXJyZW50VXNlclJvbGVzID0gW29ial07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoY29uc3QgY3VycmVudFVzZXJSb2xlIG9mIGN1cnJlbnRVc2VyUm9sZXMpIHtcclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRVc2VyUm9sZS5OYW1lICYmIChjdXJyZW50VXNlclJvbGUuTmFtZS50b0xvd2VyQ2FzZSgpID09PSByb2xlTmFtZS50b0xvd2VyQ2FzZSgpKSkge1xyXG4gICAgICAgICAgICAgICAgaGFzUm9sZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaGFzUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VxdWFsV2l0aENhc2Uoc3RyaW5nMSwgc3RyaW5nMikge1xyXG4gICAgICAgIGlmIChzdHJpbmcxICYmIHN0cmluZzIgJiYgKHN0cmluZzEgPT09IHN0cmluZzIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbldpbmRvdyh1cmxUb09wZW4sIHdpbmRvd05hbWUsIGZlYXR1cmVzKSB7XHJcbiAgICAgICAgbGV0IHdpbmRvd0hhbmRsZSA9IG51bGw7XHJcbiAgICAgICAgbGV0IHByZXZpb3VzVVJMID0gbnVsbDtcclxuICAgICAgICBpZiAod2luZG93SGFuZGxlID09IG51bGwgfHwgd2luZG93SGFuZGxlLmNsb3NlZCkge1xyXG4gICAgICAgICAgICB3aW5kb3dIYW5kbGUgPSB3aW5kb3cub3Blbih1cmxUb09wZW4sIHdpbmRvd05hbWUsIGZlYXR1cmVzKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHByZXZpb3VzVVJMICE9PSB1cmxUb09wZW4pIHtcclxuICAgICAgICAgICAgd2luZG93SGFuZGxlID0gd2luZG93Lm9wZW4odXJsVG9PcGVuLCB3aW5kb3dOYW1lLCBmZWF0dXJlcyk7XHJcbiAgICAgICAgICAgIHdpbmRvd0hhbmRsZS5mb2N1cygpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHdpbmRvd0hhbmRsZS5mb2N1cygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwcmV2aW91c1VSTCA9IHVybFRvT3BlbjtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxRdWVyeVBhcmFtZXRlcnNGcm9tVVJMKHVybCkge1xyXG4gICAgICAgIGxldCBxdWVyeVN0cmluZyA9IHVybCA/IHVybC5zcGxpdCgnPycpWzFdIDogd2luZG93LmxvY2F0aW9uLnNlYXJjaC5zbGljZSgxKTtcclxuICAgICAgICBjb25zdCBvYmogPSB7fTtcclxuXHJcbiAgICAgICAgaWYgKHF1ZXJ5U3RyaW5nKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5U3RyaW5nID0gcXVlcnlTdHJpbmcuc3BsaXQoJyMnKVswXTtcclxuICAgICAgICAgICAgY29uc3QgcXVlcmllcyA9IHF1ZXJ5U3RyaW5nLnNwbGl0KCcmJyk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHF1ZXJ5IG9mIHF1ZXJpZXMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGEgPSBxdWVyeS5zcGxpdCgnPScpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBhcmFtTmFtZSA9IGFbMF07XHJcbiAgICAgICAgICAgICAgICBsZXQgcGFyYW1WYWx1ZSA9IHR5cGVvZiAoYVsxXSkgPT09ICd1bmRlZmluZWQnID8gdHJ1ZSA6IGFbMV07XHJcblxyXG4gICAgICAgICAgICAgICAgcGFyYW1OYW1lID0gcGFyYW1OYW1lLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtVmFsdWUgPT09ICdzdHJpbmcnKSB7IHBhcmFtVmFsdWUgPSBwYXJhbVZhbHVlLnRvTG93ZXJDYXNlKCk7IH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocGFyYW1OYW1lLm1hdGNoKC9cXFsoXFxkKyk/XFxdJC8pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qga2V5ID0gcGFyYW1OYW1lLnJlcGxhY2UoL1xcWyhcXGQrKT9cXF0vLCAnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFvYmpba2V5XSkgeyBvYmpba2V5XSA9IFtdOyB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbU5hbWUubWF0Y2goL1xcW1xcZCtcXF0kLykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaW5kZXggPSAvXFxbKFxcZCspXFxdLy5leGVjKHBhcmFtTmFtZSlbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ialtrZXldW2luZGV4XSA9IHBhcmFtVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqW2tleV0ucHVzaChwYXJhbVZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghb2JqW3BhcmFtTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0gPSBwYXJhbVZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAob2JqW3BhcmFtTmFtZV0gJiYgdHlwZW9mIG9ialtwYXJhbU5hbWVdID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYmpbcGFyYW1OYW1lXSA9IFtvYmpbcGFyYW1OYW1lXV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ialtwYXJhbU5hbWVdLnB1c2gocGFyYW1WYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JqW3BhcmFtTmFtZV0ucHVzaChwYXJhbVZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG9iajtcclxuICAgIH1cclxuXHJcbiAgICBnZXRRdWVyeVBhcmFtZXRlckZyb21VUkwocGFyYW1ldGVyLCB1cmwpIHtcclxuICAgICAgICBjb25zdCByZWcgPSBuZXcgUmVnRXhwKCdbPyZdJyArIHBhcmFtZXRlciArICc9KFteJiNdKiknLCAnaScpO1xyXG4gICAgICAgIGNvbnN0IHN0ciA9IHJlZy5leGVjKHVybCk7XHJcbiAgICAgICAgcmV0dXJuIHN0ciA/IHN0clsxXSA6IHVuZGVmaW5lZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQcmlvcml0eUljb24obmFtZTogc3RyaW5nLCBjYXRlZ29yeUxldmVsVHlwZTogTVBNX0xFVkVMKSB7XHJcbiAgICAgICAgY29uc3QgY3VycmVudENhdGVnb3J5TGV2ZWwgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldENhdGVnb3J5TGV2ZWxzKCkuZmluZCgoZWFjaENhdGVnb3J5TGV2ZWwpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGVhY2hDYXRlZ29yeUxldmVsLkNBVEVHT1JZX0xFVkVMX1RZUEUgPT09IGNhdGVnb3J5TGV2ZWxUeXBlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxldCBwcmlvcml0eU9iaiA9IG51bGw7XHJcbiAgICAgICAgY29uc3QgcHJpb3JpdHlSZXR1cm5PYmogPSB7IGljb246ICcnLCBjb2xvcjogJycsIHRvb2x0aXA6ICcnIH07XHJcbiAgICAgICAgaWYgKGN1cnJlbnRDYXRlZ29yeUxldmVsICYmIGN1cnJlbnRDYXRlZ29yeUxldmVsWydNUE1fQ2F0ZWdvcnlfTGV2ZWwtaWQnXSkge1xyXG4gICAgICAgICAgICBwcmlvcml0eU9iaiA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsUHJpb3JpdGllcygpXHJcbiAgICAgICAgICAgICAgICAuZmluZChwcmlvcml0eSA9PiBuYW1lID09PSBwcmlvcml0eS5OQU1FICYmIHByaW9yaXR5LlJfUE9fQ0FURUdPUllfTEVWRUxbJ01QTV9DYXRlZ29yeV9MZXZlbC1pZCddLklkID09PSBjdXJyZW50Q2F0ZWdvcnlMZXZlbFsnTVBNX0NhdGVnb3J5X0xldmVsLWlkJ10uSWQpO1xyXG4gICAgICAgICAgICBpZiAoIXByaW9yaXR5T2JqKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJpb3JpdHlSZXR1cm5PYmo7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIGljb246IHByaW9yaXR5T2JqLklDT05fRElSRUNUSU9OID09PSAnVXAnID8gJ2Fycm93X3Vwd2FyZCcgOiAnYXJyb3dfZG93bndhcmQnLFxyXG4gICAgICAgICAgICAgICAgY29sb3I6IHByaW9yaXR5T2JqLklDT05fQ09MT1IsXHJcbiAgICAgICAgICAgICAgICB0b29sdGlwOiBwcmlvcml0eU9iai5ERVNDUklQVElPTlxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcHJpb3JpdHlSZXR1cm5PYmo7XHJcbiAgICB9XHJcblxyXG4gICAgLyogRGVmaW5lIGZ1bmN0aW9uIGZvciBlc2NhcGluZyB1c2VyIGlucHV0IHRvIGJlIHRyZWF0ZWQgYXNcclxuICAgICAgICBhIGxpdGVyYWwgc3RyaW5nIHdpdGhpbiBhIHJlZ3VsYXIgZXhwcmVzc2lvbiAqL1xyXG4gICAgZXNjYXBlUmVnRXhwKHN0cikge1xyXG4gICAgICAgIHJldHVybiBzdHIucmVwbGFjZSgvWy4qKz9eJHt9KCl8W1xcXVxcXFxdL2csICdcXFxcJCYnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBEZWZpbmUgZnVuY3RpbiB0byBmaW5kIGFuZCByZXBsYWNlIHNwZWNpZmllZCB0ZXJtIHdpdGggcmVwbGFjZW1lbnQgc3RyaW5nICovXHJcbiAgICByZXBsYWNlQWxsKHN0ciwgdGVybSwgcmVwbGFjZW1lbnQpIHtcclxuICAgICAgICByZXR1cm4gc3RyLnJlcGxhY2UobmV3IFJlZ0V4cCh0aGlzLmVzY2FwZVJlZ0V4cCh0ZXJtKSwgJ2cnKSwgcmVwbGFjZW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldExvb2t1cERvbWFpblZhbHVlc0J5SWQobG9va3VwRG9tYWluSWQ6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGxvb2t1cERvbWFpbnMgPSB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEFsbExvb2t1cERvbWFpbnMoKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShsb29rdXBEb21haW5zKSAmJiBsb29rdXBEb21haW5zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4ge307XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHRtcExvb2t1cERvbWFpbnMgPSBsb29rdXBEb21haW5zLmZpbmQobG9va3VwRG9tYWluID0+IGxvb2t1cERvbWFpbi5kb21haW5JZCA9PT0gbG9va3VwRG9tYWluSWQpO1xyXG4gICAgICAgIGlmICh0bXBMb29rdXBEb21haW5zICYmICFBcnJheS5pc0FycmF5KHRtcExvb2t1cERvbWFpbnMpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0bXBMb29rdXBEb21haW5zLmRvbWFpblZhbHVlcztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgfVxyXG5cclxuICAgIEdldFJvbGVETkJ5Um9sZUlkKHJvbGVJZCkge1xyXG4gICAgICAgIGNvbnN0IGFsbFJvbGVzID0gdGhpcy5zaGFyaW5nU2VydmljZS5nZXRBbGxSb2xlcygpO1xyXG4gICAgICAgIHJldHVybiBhbGxSb2xlcy5maW5kKHJvbGUgPT4gcm9sZVsnTVBNX0FQUF9Sb2xlcy1pZCddLklkID09PSByb2xlSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRlYW1Sb2xlRE5CeVJvbGVJZChyb2xlSWQpIHtcclxuICAgICAgICBjb25zdCBhbGxSb2xlcyA9IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0QWxsVGVhbVJvbGVzKCk7XHJcbiAgICAgICAgcmV0dXJuIGFsbFJvbGVzLmZpbmQocm9sZSA9PiBwYXJzZUludChyb2xlWydNUE1fVGVhbV9Sb2xlX01hcHBpbmctaWQnXS5JZCkgPT09IHJvbGVJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29udmVydFRvT1RNTURhdGVGb3JtYXQoZGF0ZSkge1xyXG4gICAgICAgIGRhdGUgPSBuZXcgRGF0ZShkYXRlKTtcclxuICAgICAgICByZXR1cm4gbW9tZW50KGRhdGUpLmZvcm1hdCgnTU0vREQvWVlZWVQwMDowMDowMCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZVNwY2xDaGFyc0Zyb21Bc3NldE5hbWUoYXNzZXROYW1lKSB7XHJcbiAgICAgICAgYXNzZXROYW1lID0gYXNzZXROYW1lLnRyaW0oKTtcclxuICAgICAgICBpZiAoYXNzZXROYW1lLmluY2x1ZGVzKCckJykgfHwgYXNzZXROYW1lLmluY2x1ZGVzKCdcXCcnKSkge1xyXG4gICAgICAgICAgICBhc3NldE5hbWUgPSBhc3NldE5hbWUucmVwbGFjZSgvWyQnXS9nLCAnXycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFzc2V0TmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBjb252ZXJ0U3RyaW5nVG9OdW1iZXIoZGF0YSkge1xyXG4gICAgICAgIHZhciBwYWdlU2l6ZSA9IGRhdGE7XHJcbiAgICAgICAgdmFyIHBhZ2VOdW1iZXI6IG51bWJlciA9ICtwYWdlU2l6ZTtcclxuICAgICAgICByZXR1cm4gcGFnZU51bWJlcjtcclxuICAgIH1cclxuXHJcbiAgICBnZXREaXNwbGF5T3JkZXJEYXRhKGRhdGEsIGRpc3BsYXlPcmRlcikge1xyXG4gICAgICAgIGNvbnN0IGZpZWxkT2JqQXJyYXkgPSBbXTtcclxuICAgICAgICBpZiAodGhpcy5pc1ZhbGlkKGRpc3BsYXlPcmRlcikgJiYgIWRpc3BsYXlPcmRlclsnQG51bGwnXSkge1xyXG4gICAgICAgICAgICBjb25zdCBhcnJEaXNwbGF5T3JkZXIgPSBkaXNwbGF5T3JkZXIuc3BsaXQoJywnKTtcclxuICAgICAgICAgICAgYXJyRGlzcGxheU9yZGVyLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZE9iakFycmF5LnB1c2goZGF0YS5maW5kKGZpZWxkRGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZpZWxkRGF0YS5NQVBQRVJfTkFNRSA9PT0gZWxlbWVudDtcclxuICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKGZpZWxkT2JqQXJyYXkubGVuZ3RoID4gMCAmJiBkYXRhLmxlbmd0aCA9PT0gZmllbGRPYmpBcnJheS5sZW5ndGgpID8gZmllbGRPYmpBcnJheSA6IGRhdGE7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==