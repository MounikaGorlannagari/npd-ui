import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import * as acronui from '../auth/utility';
import * as $ from 'jquery';
import { Observable } from 'rxjs';
import { GloabalConfig as config } from '../config/config';
import { SharingService } from './sharing.service';
import { SessionStorageConstants } from '../../shared/constants/session-storage.constants';
import * as i0 from "@angular/core";
import * as i1 from "./sharing.service";
var AppService = /** @class */ (function () {
    function AppService(sharingService) {
        this.sharingService = sharingService;
        this.USER_NS = 'http://schemas.cordys.com/1.1/ldap';
        this.GET_ALL_APPLICATION_DETAILS_NS = 'http://schemas.acheron.com/mpm/app/bpm/1.0';
        this.PERSON_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.MPM_APP_ROLES_NS = 'http://schemas/AcheronMPMOrganizationModel/MPM_APP_Roles/operations';
        this.MEDIA_MANAGER_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Media_Manager_Config/operations';
        this.WSAPP_CORE_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.GET_PERSON_BY_USER_ID_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.GET_PERSON_BY_IDENTITY_USER_ID_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.TEAMS_BPM_NS = 'http://schemas.acheron.com/mpm/teams/bpm/1.0';
        this.REVIEW_BPM_NS = 'http://schemas.acheron.com/mpm/review/bpm/1.0';
        this.APP_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_App_Config/operations';
        this.PROJECT_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Project_Config/operations';
        this.ASSET_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Asset_Config/operations';
        this.BRAND_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Brand_Config/operations';
        this.COMMENT_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_Comments_Config/operations';
        this.BULK_CR_APPROVAL_NS = 'http://schemas.acheron.com/mpm/review/bpm/1.0';
        this.VIEW_CONFIG_NS = 'http://schemas/AcheronMPMCore/MPM_View_Config/operations';
        this.TEAM_ROLE_MAPPING_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Team_Role_Mapping/operations';
        this.GET_ALL_TEAMS_NS = 'http://schemas/AcheronMPMCore/MPM_Teams/operations';
        this.GET_ALL_PERSONS_NS = 'http://schemas/MPMCustomizedApplicationPackages/Person/operations';
        this.DELIVERABLE_REVIEW_METHOD_NS = 'http://schemas/AcheronMPMCore/DeliverableReview/operations';
        this.TASK_REVIEW_METHOD_NS = 'http://schemas/AcheronMPMCore/DeliverableReview/operations';
        this.MPM_EVENTS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Events/operations';
        this.MPM_WORKFLOW_ACTIONS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Workflow_Actions/operations';
        this.GET_EMAIL_EVENTS_FOR_MANAGER_NS = 'http://schemas/AcheronMPMCore/Email_Event_Template/operations';
        this.GET_EMAIL_EVENTS_FOR_USER_NS = 'http://schemas/AcheronMPMCore/Email_Event_Template/operations';
        this.GET_MAIL_PREFERENCE_BY_USER_ID_NS = 'http://schemas/AcheronMPMCore/MPM_Email_Preference/operations';
        this.GET_USER_PREFERENCE_MAIL_TEMPLATES_NS = 'http://schemas.acheron.com/mpm/wsapp/core/1.0';
        this.MPM_WORKFLOW_NS = 'http://schemas.acheron.com/mpm/core/workflow/bpm/1.0';
        this.DELETE_MPM_EMAIL_PREFERENCE_NS = 'http://schemas/AcheronMPMCore/MPM_Email_Preference/operations';
        this.MPM_USER_PREFERENCE_NS = 'http://schemas/AcheronMPMCore/MPM_User_Preference/operations';
        this.CAMPAIGN_DETAILS_METHOD_NS = 'http://schemas/AcheronMPMCore/Campaign/operations';
        this.STATUS_METHOD_NS = 'http://schemas/AcheronMPMCore/MPM_Status/operations';
        this.GET_ALL_USERS_NS = 'http://schemas/OpenTextEntityIdentityComponents/User/operations';
        this.GET_EXPORT_DATA_BY_USER_NS = 'http://schemas/AcheronMPMCore/MPM_Export_Data/operations';
        this.DOWNLOAD_FILE_BY_PATH_NS = 'http://schemas.acheron.com/mpm/wsapp/excel/1.0';
        this.MPM_NOTIFICATION_NS = 'http://schemas/AcheronMPMCore/Notification/operations';
        this.EMAIL_CONFIG_NS = 'http://schemas/AcheronMPMCore/General_Email_Config/operations';
        this.CURSOR_NS = 'http://schemas.opentext.com/bps/entity/core';
        this.GET_PERSON_DETAILS_BY_USER_ID_WS = 'GetPersonByUserId';
        this.GET_ALL_APPLICATION_DETAILS_WS = 'GetApplicationForUser';
        this.GET_ALL_ROLES_WS = 'GetAllRoles';
        this.GET_MEDIA_MANAGER_CONFIG_WS = 'GetMediaManagerConfig';
        this.GET_PERSON_BY_IDENTITY_USER_ID_WS = 'GetPersonByIdentityUserId';
        this.GET_TEAM_ROLES_WS = 'GetTeamRoles';
        this.GET_USES_FOR_TEAM_WS = 'GetUsersForTeam';
        this.GET_POSSIBLE_CR_ACTIONS_FOR_USER_WS = 'GetPossibileCRActionsForUser';
        this.OPEN_FILE_PROOFING_SESSION_WS = 'OpenFileProofSession';
        this.GET_APP_CONFIG_BY_ID_WS = 'GetAppConfigById';
        this.GET_PROJECT_CONFIG_BY_ID_WS = 'GetProjectConfigById';
        this.GET_ASSET_CONFIG_BY_ID_WS = 'GetAssetConfigById';
        this.GET_BRAND_CONFIG_BY_ID_WS = 'GetBrandConfigById';
        this.GET_COMMENT_CONFIG_BY_ID_WS = 'GetCommentsConfigById';
        this.HANDEL_CR_APPROVAL_WS = 'HandleCRApproval';
        this.GET_ALL_MPM_VIEW_CONFIG_WS = 'GetAllMPMViewConfig';
        this.GET_ALL_TEAM_ROLES_WS = 'GetAllTeamRoles';
        this.GET_FILE_PROOFING_SESSION_WS = 'GetFileProofingSession';
        this.GET_ALL_TEAMS_WS = 'GetAllTeams ';
        this.GET_ALL_PERSONS_WS = 'GetAllPersons';
        this.GET_PERSONS_BY_KEYWORD_WS = 'GetPersonsByKeyword';
        this.GET_DELIVERABLE_REVIEW_BY_DELIVERABLE_ID_WS = 'GetDeliverableReviewByDeliverableID';
        this.GET_DELIVERABLE_REVIEW_BY_TASK_ID_WS = 'GetDeliverableReviewByTaskID';
        this.GET_USERS_FOR_TEAM_ROLE_WS = 'GetUsersForTeamRole';
        this.GET_MPM_EVENTS_BY_CATEGORY_LEVEL_WS = 'GetMPMEventsByCategoryLevel';
        this.GET_WORKFLOW_ACTIONS_BY_CATEGORY_WS = 'GetWorkflowActionsByCategory';
        this.GET_EMAIL_EVENTS_FOR_MANAGER_WS = 'GetEmailEventsForManager';
        this.GET_EMAIL_EVENTS_FOR_USER_WS = 'GetEmailEventsForUser';
        this.GET_MAIL_PREFERENCE_BY_USER_ID_WS = 'GetMailPreferenceByUserId';
        this.GET_USER_PREFERENCE_MAIL_TEMPLATES_WS = 'GetUserPreferenceMailTemplates';
        this.MAIL_PREFERENCE_WS = 'MailPreference';
        this.DELETE_MPM_EMAIL_PREFERENCE_WS = 'DeleteMPM_Email_Preference ';
        this.EXPORT_DATA_WS = 'ExportData';
        this.GET_EXPORT_DATA_BY_USER_WS = 'GetExportDataByUser';
        this.DOWNLOAD_FILE_BY_PATH_WS = 'DownloadFileByPath';
        this.GET_MPM_USER_PREFERENCE_BY_USER_WS = 'GetMPMUserPreferenceByUser';
        this.CREATE_MPM_USER_PREFERENCE_WS = 'CreateMPM_User_Preference';
        this.UPDATE_MPM_USER_PREFERENCE_WS = 'UpdateMPM_User_Preference';
        this.DELETE_MPM_USER_PREFERENCE_WS = 'DeleteMPM_User_Preference';
        this.GET_ALL_CAMPAIGN_WS_METHOD_NAME = 'GetAllCampaign';
        this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS = 'GetAllStatusForCategoryLevel';
        this.GET_ALL_USERS = 'GetAllUsers';
        this.GET_ALL_STATUS = 'GetAllStatus';
        this.UPDATE_DEFAULT_USER_PREFERENCE_WS = 'UpdateDefaultUserPreference';
        this.GET_NOTIFICATION_BY_USER_WS = 'GetNotificationByUser';
        this.UPDATE_NOTIFICATION_WS = 'UpdateNotification';
        this.GET_DEFAULT_EMAIL_CONFIG_WS = 'GetDefaultEmailConfig';
        this.DELETE_MPM_EXPORT_DATA_BY_ID_WS = 'DeleteMPM_Export_Data';
        this.DELETE_MPM_EXPORT_DATA_BY_ID_NS = 'http://schemas/AcheronMPMCore/MPM_Export_Data/operations';
    }
    AppService.prototype.invokeRequest = function (namespace, method, parameters) {
        if (namespace == null || namespace === '' || typeof (namespace) === 'undefined') {
            console.warn('Please provide valid namespace.');
            return;
        }
        if (method == null || method === '' || typeof (method) === 'undefined') {
            console.warn('Please provide valid method name.');
            return;
        }
        if (parameters == null || parameters === '' || typeof (parameters) === 'undefined') {
            parameters = '';
        }
        return new Observable(function (appServiceObserver) {
            $.soap({
                namespace: namespace,
                method: method,
                parameters: parameters
            }).then(function (data) {
                appServiceObserver.next(data);
                appServiceObserver.complete();
            }, function (error) {
                appServiceObserver.error(new Error(error));
            });
        });
    };
    // MPMV3-2008
    AppService.prototype.getAllUsers = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_USERS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_USERS)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.GET_ALL_USERS_NS, _this.GET_ALL_USERS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_USERS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    // added for code hardeniing -keerthana
    AppService.prototype.getStatusByCategoryLevelName = function (categoryLevelName) {
        var _this = this;
        var getRequestObject = {
            categoryLevelName: categoryLevelName,
            categoryLevelID: ''
        };
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.STATUS_METHOD_NS, _this.GET_STATUS_BY_CATEGORY_LEVEL_NAME_WS, getRequestObject)
                    .subscribe(function (response) {
                    var statuses = acronui.findObjectsByProp(response, 'MPM_Status');
                    sessionStorage.setItem(SessionStorageConstants.STATUS_BY_CATEGORY_LEVEL_NAME, JSON.stringify(statuses));
                    observer.next(statuses);
                    observer.complete();
                }, function (error) {
                    observer.error(error);
                });
            }
        });
    };
    // added for code hardeniing -keerthana
    /* getAllCampaign(parameters?): Observable<any> {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_CAMPAIGN) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_CAMPAIGN)));
                observer.complete();
            } else {
                this.invokeRequest(this.CAMPAIGN_DETAILS_METHOD_NS, this.GET_ALL_CAMPAIGN_WS_METHOD_NAME, null).subscribe(campaignDetails => {
                    sessionStorage.setItem(SessionStorageConstants.ALL_CAMPAIGN, JSON.stringify(campaignDetails));
                    observer.next(campaignDetails);
                    observer.complete();
                });
            }
        });
    } */
    // added for code hardeniing 
    AppService.prototype.getUserDetailsLDAP = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.USER_DETAILS_LDAP)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.USER_NS, 'GetUserDetails', '').subscribe(function (userDetails) {
                    sessionStorage.setItem(SessionStorageConstants.USER_DETAILS_LDAP, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.logIn = function (callback) {
        /*  const getUser: Observable<any> = this.invokeRequest(this.USER_NS, 'GetUserDetails', '');
         getUser.subscribe(response => { */
        this.getUserDetailsLDAP().subscribe(function (response) {
            var userObject = acronui.findObjectsByProp(response, 'user');
            var userName = userObject[0].authuserdn.split(',')[0].split('=')[1];
            if (userName === 'anonymous') {
                window.location.href = config.config.logoutUrl;
            }
            else {
                if (callback) {
                    callback(true);
                }
            }
        }, function (errData) {
            console.error('Error while firing GetUserDetails : ' + errData.status);
            if (callback) {
                callback(false);
            }
        });
    };
    AppService.prototype.checkPSSession = function (callback) {
        this.logIn(callback);
    };
    AppService.prototype.getPersonCastedObj = function (data) {
        var userDetails = {
            Id: '',
            ItemId: '',
            userCN: '',
            fullName: '',
            userId: ''
        };
        if (data) {
            if (data.Person && data.Person.User_ID && data.Person.User_ID.__text) {
                userDetails.userCN = data.Person.User_ID.__text;
            }
            if (data.Person && data.Person['Person-id'] && data.Person['Person-id'].Id) {
                userDetails.Id = data.Person['Person-id'].Id;
            }
            if (data.Person && data.Person['Person-id'] && data.Person['Person-id'].ItemId) {
                userDetails.ItemId = data.Person['Person-id'].ItemId;
            }
            if (data.Person && data.Person.DisplayName && data.Person.DisplayName.__text) {
                userDetails.fullName = data.Person.DisplayName.__text;
            }
            if (data.Person && data.Person.PersonToUser && data.Person.PersonToUser['Identity-id'] && data.Person.PersonToUser['Identity-id'].Id) {
                userDetails.userId = data.Person.PersonToUser['Identity-id'].Id;
            }
        }
        return userDetails;
    };
    AppService.prototype.getPersonByIdentityUserId = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            _this.invokeRequest(_this.GET_PERSON_BY_IDENTITY_USER_ID_NS, _this.GET_PERSON_BY_IDENTITY_USER_ID_WS, parameters)
                .subscribe(function (data) {
                var userDetails = _this.getPersonCastedObj(data);
                observer.next(userDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    /* getPersonByIdentityUserId(parameters?): Observable<Person> {
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.GET_PERSON_BY_IDENTITY_USER_ID_NS, this.GET_PERSON_BY_IDENTITY_USER_ID_WS, parameters).subscribe(data => {
                    const userDetails: Person = this.getPersonCastedObj(data);
                    sessionStorage.setItem(SessionStorageConstants.PERSON_BY_IDENTITY_USER_ID, JSON.stringify(userDetails));
                    observer.next(userDetails);
                    observer.complete();
                });
            }
        });
    }
 */
    AppService.prototype.getPersonByUserId = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            _this.invokeRequest(_this.GET_PERSON_BY_USER_ID_NS, _this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters)
                .subscribe(function (data) {
                var userDetails = _this.getPersonCastedObj(data);
                observer.next(userDetails);
                observer.complete();
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    /*  getPersonByUserId(parameters?): Observable<Person> {
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.PERSON_BY_USER_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PERSON_BY_USER_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.GET_PERSON_BY_USER_ID_NS, this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters)
                     .subscribe(data => {
                         const userDetails: Person = this.getPersonCastedObj(data);
                         sessionStorage.setItem(SessionStorageConstants.PERSON_BY_USER_ID, JSON.stringify(userDetails));
                         observer.next(userDetails);
                         observer.complete();
                     }, error => {
                         observer.error(error);
                         observer.complete();
                     });
             }
         });
     } */
    /* getAllApplicationDetails(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_APPLICATION_DETAILS_NS, this.GET_ALL_APPLICATION_DETAILS_WS, parameters);
    } */
    AppService.prototype.getAllApplicationDetails = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_APPLICATION_DETAILS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_APPLICATION_DETAILS)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.GET_ALL_APPLICATION_DETAILS_NS, _this.GET_ALL_APPLICATION_DETAILS_WS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_APPLICATION_DETAILS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.getProjectConfigById = function (id) {
        var parameters = {
            projectConfigId: id
        };
        return this.invokeRequest(this.PROJECT_CONFIG_NS, this.GET_PROJECT_CONFIG_BY_ID_WS, parameters);
    };
    /* getProjectConfigById(id): Observable<any> {
        const parameters = {
            projectConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.PROJECT_CONFIG_NS, this.GET_PROJECT_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.PROJECT_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    AppService.prototype.getAppConfigById = function (id) {
        var parameters = {
            appConfigId: id
        };
        return this.invokeRequest(this.APP_CONFIG_NS, this.GET_APP_CONFIG_BY_ID_WS, parameters);
    };
    /* getAppConfigById(id): Observable<any> {
        const parameters = {
            appConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.APP_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.APP_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.APP_CONFIG_NS, this.GET_APP_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.APP_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    AppService.prototype.getAssetConfigById = function (id) {
        var parameters = {
            assetConfigId: id
        };
        return this.invokeRequest(this.ASSET_CONFIG_NS, this.GET_ASSET_CONFIG_BY_ID_WS, parameters);
    };
    /*  getAssetConfigById(id): Observable<any> {
         const parameters = {
             assetConfigId: id
         };
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.ASSET_CONFIG_BY_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ASSET_CONFIG_BY_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.ASSET_CONFIG_NS, this.GET_ASSET_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                     sessionStorage.setItem(SessionStorageConstants.ASSET_CONFIG_BY_ID, JSON.stringify(response));
                     observer.next(response);
                     observer.complete();
                 });
             }
         });
     } */
    AppService.prototype.getCommentConfigById = function (id) {
        var parameters = {
            commentConfigId: id
        };
        return this.invokeRequest(this.COMMENT_CONFIG_NS, this.GET_COMMENT_CONFIG_BY_ID_WS, parameters);
    };
    /*  getCommentConfigById(id): Observable<any> {
         const parameters = {
             commentConfigId: id
         };
         return new Observable(observer => {
             if (sessionStorage.getItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID) !== null) {
                 observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID)));
                 observer.complete();
             } else {
                 this.invokeRequest(this.COMMENT_CONFIG_NS, this.GET_COMMENT_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                     sessionStorage.setItem(SessionStorageConstants.COMMENT_CONFIG_BY_ID, JSON.stringify(response));
                     observer.next(response);
                     observer.complete();
                 });
             }
         });
     } */
    AppService.prototype.getBrandConfigById = function (id) {
        var parameters = {
            brandConfigId: id
        };
        return this.invokeRequest(this.BRAND_CONFIG_NS, this.GET_BRAND_CONFIG_BY_ID_WS, parameters);
    };
    /* getBrandConfigById(id): Observable<any> {
        const parameters = {
            brandConfigId: id
        };
        return new Observable(observer => {
            if (sessionStorage.getItem(SessionStorageConstants.BRAND_CONFIG_BY_ID) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.BRAND_CONFIG_BY_ID)));
                observer.complete();
            } else {
                this.invokeRequest(this.BRAND_CONFIG_NS, this.GET_BRAND_CONFIG_BY_ID_WS, parameters).subscribe(response => {
                    sessionStorage.setItem(SessionStorageConstants.BRAND_CONFIG_BY_ID, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    } */
    /* getAllViewConfig(parameters?): Observable<any> {
        return this.invokeRequest(this.VIEW_CONFIG_NS, this.GET_ALL_MPM_VIEW_CONFIG_WS, '');
    } */
    AppService.prototype.getAllViewConfig = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_VIEW_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_VIEW_CONFIG)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.VIEW_CONFIG_NS, _this.GET_ALL_MPM_VIEW_CONFIG_WS, '').subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_VIEW_CONFIG, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.getUsersForRole = function (parameters) {
        return this.invokeRequest(this.USER_NS, 'GetUsersForRole', parameters);
    };
    AppService.prototype.getOTDSTicketForUser = function (parameters) {
        return this.invokeRequest(this.WSAPP_CORE_NS, 'GetOTDSTicketForUser', parameters);
    };
    AppService.prototype.getPersonDetailsByUserCN = function (parameters) {
        return this.invokeRequest(this.PERSON_NS, this.GET_PERSON_DETAILS_BY_USER_ID_WS, parameters);
    };
    /* getAllRoles(parameters?) {
        return this.invokeRequest(this.MPM_APP_ROLES_NS, this.GET_ALL_ROLES_WS, parameters);
    } */
    AppService.prototype.getAllRoles = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_ROLES) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_ROLES)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.MPM_APP_ROLES_NS, _this.GET_ALL_ROLES_WS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_ROLES, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.getTaskEvents = function () {
        var parameters = {
            CategoryLevel: 'TASK'
        };
        return this.invokeRequest(this.MPM_EVENTS_METHOD_NS, this.GET_MPM_EVENTS_BY_CATEGORY_LEVEL_WS, parameters);
    };
    AppService.prototype.getTaskWorkflowActions = function () {
        var parameters = {
            categoryLevel: 'TASK'
        };
        return this.invokeRequest(this.MPM_WORKFLOW_ACTIONS_METHOD_NS, this.GET_WORKFLOW_ACTIONS_BY_CATEGORY_WS, parameters);
    };
    /*  getMediaManagerConfig(parameters): Observable<any> {
         return this.invokeRequest(this.MEDIA_MANAGER_CONFIG_NS, this.GET_MEDIA_MANAGER_CONFIG_WS, parameters);
     } */
    AppService.prototype.getMediaManagerConfig = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.MEDIA_MANAGER_CONFIG_NS, _this.GET_MEDIA_MANAGER_CONFIG_WS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.MEDIA_MANAGER_CONFIG, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.getTeamRoles = function (parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_TEAM_ROLES_WS, parameters);
    };
    /*     getAllTeamRoles(parameters?): Observable<any> {
            return this.invokeRequest(this.TEAM_ROLE_MAPPING_METHOD_NS, this.GET_ALL_TEAM_ROLES_WS, parameters);
        } */
    AppService.prototype.getAllTeamRoles = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_TEAM_ROLES) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_TEAM_ROLES)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.TEAM_ROLE_MAPPING_METHOD_NS, _this.GET_ALL_TEAM_ROLES_WS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_TEAM_ROLES, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    /* getAllTeams(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_TEAMS_NS, this.GET_ALL_TEAMS_WS, parameters);
    } */
    AppService.prototype.getAllTeams = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_TEAMS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_TEAMS)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.GET_ALL_TEAMS_NS, _this.GET_ALL_TEAMS_WS, parameters).subscribe(function (response) {
                    console.log(response);
                    sessionStorage.setItem(SessionStorageConstants.ALL_TEAMS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    /* getAllPersons(parameters?): Observable<any> {
        return this.invokeRequest(this.GET_ALL_PERSONS_NS, this.GET_ALL_PERSONS_WS, parameters);
    } */
    AppService.prototype.getAllPersons = function (parameters) {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.ALL_PERSONS) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.ALL_PERSONS)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.GET_ALL_PERSONS_NS, _this.GET_ALL_PERSONS_WS, parameters).subscribe(function (response) {
                    sessionStorage.setItem(SessionStorageConstants.ALL_PERSONS, JSON.stringify(response));
                    observer.next(response);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.getPersonsByKeyword = function (userKeyword) {
        var _this = this;
        var parameters = {
            userKeyword: userKeyword,
            Cursor: {
                '@xmlns': this.CURSOR_NS,
                '@offset': 0,
                '@limit': 30
            }
        };
        return new Observable(function (observer) {
            _this.invokeRequest(_this.GET_ALL_PERSONS_NS, _this.GET_PERSONS_BY_KEYWORD_WS, parameters).subscribe(function (response) {
                observer.next(response);
                observer.complete();
            });
        });
    };
    AppService.prototype.getUsersForTeam = function (parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_USES_FOR_TEAM_WS, parameters);
    };
    AppService.prototype.getUsersForTeamRole = function (parameters) {
        return this.invokeRequest(this.TEAMS_BPM_NS, this.GET_USERS_FOR_TEAM_ROLE_WS, parameters);
    };
    AppService.prototype.getDeliverableReviewByDeliverableID = function (id) {
        var parameters = {
            DeliverableID: id
        };
        return this.invokeRequest(this.DELIVERABLE_REVIEW_METHOD_NS, this.GET_DELIVERABLE_REVIEW_BY_DELIVERABLE_ID_WS, parameters);
    };
    AppService.prototype.getDeliverableReviewByTaskID = function (id) {
        var parameters = {
            TaskID: id
        };
        return this.invokeRequest(this.TASK_REVIEW_METHOD_NS, this.GET_DELIVERABLE_REVIEW_BY_TASK_ID_WS, parameters);
    };
    AppService.prototype.getPossibleCRActionsForUser = function () {
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_POSSIBLE_CR_ACTIONS_FOR_USER_WS, null);
    };
    AppService.prototype.openFileProofingSession = function (appId, deliverableId, assetId, taskName, reviewRoleId, versioning) {
        var parameters = {
            openFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: {
                        deliverableId: deliverableId,
                        versioning: versioning
                    }
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    }
                }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.OPEN_FILE_PROOFING_SESSION_WS, parameters);
    };
    AppService.prototype.getFileProofingSession = function (appId, deliverableId, assetId, taskName, reviewRoleId, versioning) {
        var parameters = {
            getFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: {
                        deliverableId: deliverableId,
                        versioning: versioning
                    }
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    },
                },
                person: null
                // {
                //     firstName: this.sharingService.getCurrentUserDisplayName() ,
                //     lastName: '',
                //     id: this.sharingService.getCurrentUserItemID()
                // }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_FILE_PROOFING_SESSION_WS, parameters);
    };
    AppService.prototype.getMultipleFileProofingSession = function (appId, deliverable, assetId, taskName, reviewRoleId, versioning) {
        var parameters = {
            getFileProofingSession: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                deliverableDetails: {
                    deliverable: deliverable
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    },
                },
                person: null
                // {
                //     firstName: this.sharingService.getCurrentUserDisplayName() ,
                //     lastName: '',
                //     id: this.sharingService.getCurrentUserItemID()
                // }
            }
        };
        return this.invokeRequest(this.REVIEW_BPM_NS, this.GET_FILE_PROOFING_SESSION_WS, parameters);
    };
    AppService.prototype.bulkDeliverableId = function (deliverableId, versioning, taskName) {
        var deliverables = [];
        deliverableId.forEach(function (element) {
            deliverables.push({
                deliverableId: element,
                versioning: versioning,
                taskName: taskName
            });
        });
        return deliverables;
    };
    AppService.prototype.handleCRApproval = function (appId, deliverableId, assetId, taskName, reviewRoleId, versioning, commentText, isBulkApproval, statusId, selectedReasonsData, userCN) {
        var parameters = {
            SetStatusApproval: {
                applicationId: appId,
                versioning: versioning,
                reviewRoleId: reviewRoleId,
                taskName: taskName,
                statusId: statusId,
                deliverableDetails: {
                    deliverable: this.bulkDeliverableId(deliverableId, versioning, taskName)
                },
                customData: {
                    data: {
                        deliverable: {
                            assetId: assetId,
                            reviewDeliverableId: ''
                        }
                    }
                },
                commentText: commentText,
                selectedReasons: {
                    ApprovalReason: selectedReasonsData
                },
                isBulkApproval: isBulkApproval,
                reviewerUserCN: userCN
            }
        };
        return this.invokeRequest(this.BULK_CR_APPROVAL_NS, this.HANDEL_CR_APPROVAL_WS, parameters);
    };
    /*  getCurrentUserPreference(): Observable<any> {
         const parameter = {
             UserId: this.sharingService.getcurrentUserItemID()
         };
         return this.invokeRequest(this.MPM_USER_PREFERENCE_NS, this.GET_MPM_USER_PREFERENCE_BY_USER_WS, parameter);
     } */
    AppService.prototype.getCurrentUserPreference = function () {
        var _this = this;
        var parameter = {
            UserId: this.sharingService.getcurrentUserItemID()
        };
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.MPM_USER_PREFERENCE_NS, _this.GET_MPM_USER_PREFERENCE_BY_USER_WS, parameter).subscribe(function (response) {
                    // sessionStorage.setItem(SessionStorageConstants.CURRENT_MPM_USER_PREFERENCE, JSON.stringify(response));
                    var userPreferenceResponse = response.MPM_User_Preference ? response.MPM_User_Preference : [];
                    observer.next(userPreferenceResponse);
                    observer.complete();
                });
            }
        });
    };
    AppService.prototype.updateUserPreference = function (requestObj, existingUserPreferenceId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter;
            if (existingUserPreferenceId) {
                parameter = {
                    'MPM_User_Preference-id': {
                        Id: existingUserPreferenceId
                    },
                    'MPM_User_Preference-update': requestObj
                };
            }
            else {
                parameter = {
                    'MPM_User_Preference-create': requestObj
                };
            }
            _this.invokeRequest(_this.MPM_USER_PREFERENCE_NS, existingUserPreferenceId ? _this.UPDATE_MPM_USER_PREFERENCE_WS : _this.CREATE_MPM_USER_PREFERENCE_WS, parameter).subscribe(function (response) {
                if (response && response.MPM_User_Preference) {
                    _this.sharingService.updateCurrentUserPreference(response.MPM_User_Preference);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    AppService.prototype.updateDefaultUserPreference = function (viewId, filterName, menuId, fieldData) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                ViewId: viewId,
                FilterName: filterName,
                PersonId: _this.sharingService.getCurrentPerson().Id,
                MenuId: menuId,
                FieldData: fieldData
            };
            _this.invokeRequest(_this.MPM_WORKFLOW_NS, _this.UPDATE_DEFAULT_USER_PREFERENCE_WS, parameter).subscribe(function (response) {
                if (response && response.APIResponse && response.APIResponse.statusCode === '200' && response.APIResponse.data && response.APIResponse.data.MPM_User_Preference) {
                    _this.sharingService.updateCurrentUserPreference(response.APIResponse.data.MPM_User_Preference);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    AppService.prototype.deleteUserPreference = function (userPreferenceId) {
        var _this = this;
        return new Observable(function (observer) {
            var parameter = {
                MPM_User_Preference: {
                    'MPM_User_Preference-id': {
                        Id: userPreferenceId
                    }
                }
            };
            _this.invokeRequest(_this.MPM_USER_PREFERENCE_NS, _this.DELETE_MPM_USER_PREFERENCE_WS, parameter).subscribe(function (response) {
                if (response) {
                    _this.sharingService.removeCurrentUserPreference(userPreferenceId);
                    observer.next(true);
                    observer.complete();
                }
                else {
                    observer.next(false);
                    observer.complete();
                }
            }, function (error) {
                observer.error(error);
                observer.complete();
            });
        });
    };
    /*
    MPMV3-1448
    */
    AppService.prototype.GetEmailEventsForManager = function (parameters) {
        return this.invokeRequest(this.GET_EMAIL_EVENTS_FOR_MANAGER_NS, this.GET_EMAIL_EVENTS_FOR_MANAGER_WS, '');
    };
    /*
    MPMV3-1448
    */
    AppService.prototype.GetEmailEventsForUser = function (parameters) {
        return this.invokeRequest(this.GET_EMAIL_EVENTS_FOR_USER_NS, this.GET_EMAIL_EVENTS_FOR_USER_WS, '');
    };
    AppService.prototype.GetMailPreferenceByUserId = function (userId) {
        var parameters = {
            userId: userId
        };
        return this.invokeRequest(this.GET_MAIL_PREFERENCE_BY_USER_ID_NS, this.GET_MAIL_PREFERENCE_BY_USER_ID_WS, parameters);
    };
    AppService.prototype.GetUserPreferenceMailTemplates = function (userId) {
        var parameters = {
            userId: userId
        };
        return this.invokeRequest(this.GET_USER_PREFERENCE_MAIL_TEMPLATES_NS, this.GET_USER_PREFERENCE_MAIL_TEMPLATES_WS, parameters);
    };
    AppService.prototype.MailPreference = function (userId, enabledEvents, disabledEvents) {
        var parameters = {
            UserId: userId,
            EnabledEvents: {
                // email: enabledEvents
                'Email_Event_Template-id': enabledEvents
            },
            DisabledEvents: {
                // disable: disabledEvents
                'Email_Event_Template-id': disabledEvents
            }
        };
        return this.invokeRequest(this.MPM_WORKFLOW_NS, this.MAIL_PREFERENCE_WS, parameters);
    };
    AppService.prototype.DeleteMPM_Email_Preference = function (userEmailPreferenceId) {
        var parameters = {
            MPM_Email_Preference: {
                'MPM_Email_Preference-id': userEmailPreferenceId
            }
        };
        return this.invokeRequest(this.DELETE_MPM_EMAIL_PREFERENCE_NS, this.DELETE_MPM_EMAIL_PREFERENCE_WS, parameters);
    };
    AppService.prototype.exportData = function (columns, fileName, exportType, itemId, filePath, searchRequest, pageLimit, totalCount, exportDataMinCount) {
        var otdsTicket = sessionStorage.getItem(SessionStorageConstants.OTDS_TICKET);
        var currentUserId = sessionStorage.getItem(SessionStorageConstants.OTDS_USER_ID);
        var parameters = {
            columns: columns,
            fileName: fileName,
            exportType: exportType,
            filePath: filePath,
            IndexerSearchRequest: searchRequest,
            pageLimit: pageLimit,
            totalCount: totalCount,
            exportDataMinCount: exportDataMinCount,
            otdsTicket: (otdsTicket && otdsTicket != null) ? JSON.parse(otdsTicket) : "",
            currentUserId: (currentUserId && currentUserId != null) ? JSON.parse(currentUserId) : "",
            data: {
                ITEM_ID: itemId
            }
        };
        return this.invokeRequest(this.MPM_WORKFLOW_NS, this.EXPORT_DATA_WS, parameters);
    };
    AppService.prototype.getExportDataByUser = function (userCN) {
        var parameters = {
            UserCN: userCN
        };
        return this.invokeRequest(this.GET_EXPORT_DATA_BY_USER_NS, this.GET_EXPORT_DATA_BY_USER_WS, parameters);
    };
    // MPMV3-2368
    AppService.prototype.deleteExportDataById = function (id) {
        var parameters = {
            MPM_Export_Data: {
                'MPM_Export_Data-id': {
                    Id: id
                }
            }
        };
        return this.invokeRequest(this.DELETE_MPM_EXPORT_DATA_BY_ID_NS, this.DELETE_MPM_EXPORT_DATA_BY_ID_WS, parameters);
    };
    AppService.prototype.getFileEncodedString = function (filePath) {
        var _this = this;
        var parameter = {
            filePath: filePath
        };
        return new Observable(function (observer) {
            _this.invokeRequest(_this.DOWNLOAD_FILE_BY_PATH_NS, _this.DOWNLOAD_FILE_BY_PATH_WS, parameter)
                .subscribe(function (response) {
                observer.next(response);
                observer.complete();
            }, function (error) {
                observer.error(error);
            });
        });
    };
    AppService.prototype.getNotificationForCurrentUser = function (skip, top) {
        var parameters = {
            UserId: this.sharingService.getCurrentPerson().Id,
            Cursor: {
                '@xmlns': this.CURSOR_NS,
                '@offset': skip,
                '@limit': top
            }
        };
        return this.invokeRequest(this.MPM_NOTIFICATION_NS, this.GET_NOTIFICATION_BY_USER_WS, parameters);
    };
    AppService.prototype.updateNotification = function (parameters) {
        return this.invokeRequest(this.MPM_NOTIFICATION_NS, this.UPDATE_NOTIFICATION_WS, parameters);
    };
    AppService.prototype.getDefaultEmailConfig = function () {
        var _this = this;
        return new Observable(function (observer) {
            if (sessionStorage.getItem(SessionStorageConstants.EMAIL_CONFIG) !== null) {
                observer.next(JSON.parse(sessionStorage.getItem(SessionStorageConstants.EMAIL_CONFIG)));
                observer.complete();
            }
            else {
                _this.invokeRequest(_this.EMAIL_CONFIG_NS, _this.GET_DEFAULT_EMAIL_CONFIG_WS, null).subscribe(function (response) {
                    if (response && response.General_Email_Config) {
                        observer.next(response.General_Email_Config);
                        observer.complete();
                    }
                    else {
                        observer.next(false);
                        observer.complete();
                    }
                });
            }
        });
    };
    AppService.ctorParameters = function () { return [
        { type: SharingService }
    ]; };
    AppService.ɵprov = i0.ɵɵdefineInjectable({ factory: function AppService_Factory() { return new AppService(i0.ɵɵinject(i1.SharingService)); }, token: AppService, providedIn: "root" });
    AppService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], AppService);
    return AppService;
}());
export { AppService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9tcG0tdXRpbHMvc2VydmljZXMvYXBwLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxLQUFLLE9BQU8sTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxhQUFhLElBQUksTUFBTSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFM0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDOzs7QUFPM0Y7SUFzR0ksb0JBQ1csY0FBOEI7UUFBOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBckdsQyxZQUFPLEdBQUcsb0NBQW9DLENBQUM7UUFDL0MsbUNBQThCLEdBQUcsNENBQTRDLENBQUM7UUFDOUUsY0FBUyxHQUFHLG1FQUFtRSxDQUFDO1FBQ2hGLHFCQUFnQixHQUFHLHFFQUFxRSxDQUFDO1FBQ3pGLDRCQUF1QixHQUFHLG1FQUFtRSxDQUFDO1FBQzlGLGtCQUFhLEdBQUcsK0NBQStDLENBQUM7UUFDaEUsNkJBQXdCLEdBQUcsbUVBQW1FLENBQUM7UUFDL0Ysc0NBQWlDLEdBQUcsbUVBQW1FLENBQUM7UUFDeEcsaUJBQVksR0FBRyw4Q0FBOEMsQ0FBQztRQUM5RCxrQkFBYSxHQUFHLCtDQUErQyxDQUFDO1FBQ2hFLGtCQUFhLEdBQUcseURBQXlELENBQUM7UUFDMUUsc0JBQWlCLEdBQUcsNkRBQTZELENBQUM7UUFDbEYsb0JBQWUsR0FBRywyREFBMkQsQ0FBQztRQUM5RSxvQkFBZSxHQUFHLDJEQUEyRCxDQUFDO1FBQzlFLHNCQUFpQixHQUFHLDhEQUE4RCxDQUFDO1FBQ25GLHdCQUFtQixHQUFHLCtDQUErQyxDQUFDO1FBQ3RFLG1CQUFjLEdBQUcsMERBQTBELENBQUM7UUFDNUUsZ0NBQTJCLEdBQUcsZ0VBQWdFLENBQUM7UUFDL0YscUJBQWdCLEdBQUcsb0RBQW9ELENBQUM7UUFDeEUsdUJBQWtCLEdBQUcsbUVBQW1FLENBQUM7UUFDekYsaUNBQTRCLEdBQUcsNERBQTRELENBQUM7UUFDNUYsMEJBQXFCLEdBQUcsNERBQTRELENBQUM7UUFDckYseUJBQW9CLEdBQUcscURBQXFELENBQUM7UUFDN0UsbUNBQThCLEdBQUcsK0RBQStELENBQUM7UUFDakcsb0NBQStCLEdBQUcsK0RBQStELENBQUM7UUFDbEcsaUNBQTRCLEdBQUcsK0RBQStELENBQUM7UUFDL0Ysc0NBQWlDLEdBQUcsK0RBQStELENBQUM7UUFDcEcsMENBQXFDLEdBQUcsK0NBQStDLENBQUM7UUFDeEYsb0JBQWUsR0FBRyxzREFBc0QsQ0FBQztRQUN6RSxtQ0FBOEIsR0FBRywrREFBK0QsQ0FBQztRQUNqRywyQkFBc0IsR0FBRyw4REFBOEQsQ0FBQztRQUV4RiwrQkFBMEIsR0FBRyxtREFBbUQsQ0FBQztRQUNqRixxQkFBZ0IsR0FBRyxxREFBcUQsQ0FBQztRQUV6RSxxQkFBZ0IsR0FBRyxpRUFBaUUsQ0FBQztRQUNyRiwrQkFBMEIsR0FBRywwREFBMEQsQ0FBQztRQUN4Riw2QkFBd0IsR0FBRyxnREFBZ0QsQ0FBQztRQUU1RSx3QkFBbUIsR0FBRyx1REFBdUQsQ0FBQztRQUM5RSxvQkFBZSxHQUFHLCtEQUErRCxDQUFDO1FBRWxGLGNBQVMsR0FBRyw2Q0FBNkMsQ0FBQztRQUUxRCxxQ0FBZ0MsR0FBRyxtQkFBbUIsQ0FBQztRQUN2RCxtQ0FBOEIsR0FBRyx1QkFBdUIsQ0FBQztRQUN6RCxxQkFBZ0IsR0FBRyxhQUFhLENBQUM7UUFDakMsZ0NBQTJCLEdBQUcsdUJBQXVCLENBQUM7UUFDdEQsc0NBQWlDLEdBQUcsMkJBQTJCLENBQUM7UUFDaEUsc0JBQWlCLEdBQUcsY0FBYyxDQUFDO1FBQ25DLHlCQUFvQixHQUFHLGlCQUFpQixDQUFDO1FBQ3pDLHdDQUFtQyxHQUFHLDhCQUE4QixDQUFDO1FBQ3JFLGtDQUE2QixHQUFHLHNCQUFzQixDQUFDO1FBQ3ZELDRCQUF1QixHQUFHLGtCQUFrQixDQUFDO1FBQzdDLGdDQUEyQixHQUFHLHNCQUFzQixDQUFDO1FBQ3JELDhCQUF5QixHQUFHLG9CQUFvQixDQUFDO1FBQ2pELDhCQUF5QixHQUFHLG9CQUFvQixDQUFDO1FBQ2pELGdDQUEyQixHQUFHLHVCQUF1QixDQUFDO1FBQ3RELDBCQUFxQixHQUFHLGtCQUFrQixDQUFDO1FBQzNDLCtCQUEwQixHQUFHLHFCQUFxQixDQUFDO1FBQ25ELDBCQUFxQixHQUFHLGlCQUFpQixDQUFDO1FBQzFDLGlDQUE0QixHQUFHLHdCQUF3QixDQUFDO1FBQ3hELHFCQUFnQixHQUFHLGNBQWMsQ0FBQztRQUNsQyx1QkFBa0IsR0FBRyxlQUFlLENBQUM7UUFDckMsOEJBQXlCLEdBQUcscUJBQXFCLENBQUM7UUFDbEQsZ0RBQTJDLEdBQUcscUNBQXFDLENBQUM7UUFDcEYseUNBQW9DLEdBQUcsOEJBQThCLENBQUM7UUFDdEUsK0JBQTBCLEdBQUcscUJBQXFCLENBQUM7UUFDbkQsd0NBQW1DLEdBQUcsNkJBQTZCLENBQUM7UUFDcEUsd0NBQW1DLEdBQUcsOEJBQThCLENBQUM7UUFFckUsb0NBQStCLEdBQUcsMEJBQTBCLENBQUM7UUFDN0QsaUNBQTRCLEdBQUcsdUJBQXVCLENBQUM7UUFDdkQsc0NBQWlDLEdBQUcsMkJBQTJCLENBQUM7UUFDaEUsMENBQXFDLEdBQUcsZ0NBQWdDLENBQUM7UUFDekUsdUJBQWtCLEdBQUcsZ0JBQWdCLENBQUM7UUFDdEMsbUNBQThCLEdBQUcsNkJBQTZCLENBQUM7UUFDL0QsbUJBQWMsR0FBRyxZQUFZLENBQUM7UUFDOUIsK0JBQTBCLEdBQUcscUJBQXFCLENBQUM7UUFDbkQsNkJBQXdCLEdBQUcsb0JBQW9CLENBQUM7UUFFaEQsdUNBQWtDLEdBQUcsNEJBQTRCLENBQUM7UUFDbEUsa0NBQTZCLEdBQUcsMkJBQTJCLENBQUM7UUFDNUQsa0NBQTZCLEdBQUcsMkJBQTJCLENBQUM7UUFDNUQsa0NBQTZCLEdBQUcsMkJBQTJCLENBQUM7UUFFNUQsb0NBQStCLEdBQUcsZ0JBQWdCLENBQUM7UUFDbkQseUNBQW9DLEdBQUcsOEJBQThCLENBQUM7UUFFdEUsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFDOUIsbUJBQWMsR0FBRyxjQUFjLENBQUM7UUFDaEMsc0NBQWlDLEdBQUcsNkJBQTZCLENBQUM7UUFDbEUsZ0NBQTJCLEdBQUcsdUJBQXVCLENBQUM7UUFDdEQsMkJBQXNCLEdBQUcsb0JBQW9CLENBQUM7UUFDOUMsZ0NBQTJCLEdBQUcsdUJBQXVCLENBQUM7UUFFdEQsb0NBQStCLEdBQUcsdUJBQXVCLENBQUM7UUFDMUQsb0NBQStCLEdBQUcsMERBQTBELENBQUM7SUFLaEcsQ0FBQztJQUVMLGtDQUFhLEdBQWIsVUFBYyxTQUFTLEVBQUUsTUFBTSxFQUFFLFVBQVU7UUFDdkMsSUFBSSxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsS0FBSyxFQUFFLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLFdBQVcsRUFBRTtZQUM3RSxPQUFPLENBQUMsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLENBQUM7WUFDaEQsT0FBTztTQUNWO1FBRUQsSUFBSSxNQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sS0FBSyxFQUFFLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLFdBQVcsRUFBRTtZQUNwRSxPQUFPLENBQUMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLENBQUM7WUFDbEQsT0FBTztTQUNWO1FBRUQsSUFBSSxVQUFVLElBQUksSUFBSSxJQUFJLFVBQVUsS0FBSyxFQUFFLElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLFdBQVcsRUFBRTtZQUNoRixVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ25CO1FBRUQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLGtCQUFrQjtZQUNwQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNILFNBQVMsV0FBQTtnQkFDVCxNQUFNLFFBQUE7Z0JBQ04sVUFBVSxZQUFBO2FBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLElBQUk7Z0JBQ1Qsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM5QixrQkFBa0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQyxDQUFDLEVBQUUsVUFBQyxLQUFLO2dCQUNMLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQy9DLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYTtJQUNiLGdDQUFXLEdBQVgsVUFBWSxVQUFXO1FBQXZCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDcEUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLGFBQWEsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUN4RixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3BGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELHVDQUF1QztJQUN2QyxpREFBNEIsR0FBNUIsVUFBNkIsaUJBQXlCO1FBQXRELGlCQXNCQztRQXJCRyxJQUFNLGdCQUFnQixHQUFHO1lBQ3JCLGlCQUFpQixFQUFFLGlCQUFpQjtZQUNwQyxlQUFlLEVBQUUsRUFBRTtTQUN0QixDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDZCQUE2QixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN4RixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxvQ0FBb0MsRUFDL0UsZ0JBQWdCLENBQUM7cUJBQ2hCLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQ2YsSUFBTSxRQUFRLEdBQWtCLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7b0JBQ2xGLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN4RyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsRUFBRSxVQUFBLEtBQUs7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLENBQUM7YUFDVjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHVDQUF1QztJQUN2Qzs7Ozs7Ozs7Ozs7OztRQWFJO0lBRUosNkJBQTZCO0lBQzdCLHVDQUFrQixHQUFsQixVQUFtQixVQUFXO1FBQTlCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0YsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxXQUFXO29CQUN4RSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQkFDL0YsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsMEJBQUssR0FBTCxVQUFNLFFBQVE7UUFDVjsyQ0FDbUM7UUFDbkMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUN4QyxJQUFNLFVBQVUsR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQy9ELElBQU0sUUFBUSxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RSxJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7Z0JBQzFCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO2FBQ2xEO2lCQUFNO2dCQUNILElBQUksUUFBUSxFQUFFO29CQUNWLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDbEI7YUFDSjtRQUNMLENBQUMsRUFBRSxVQUFBLE9BQU87WUFDTixPQUFPLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN2RSxJQUFJLFFBQVEsRUFBRTtnQkFDVixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkI7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxtQ0FBYyxHQUFkLFVBQWUsUUFBUTtRQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRCx1Q0FBa0IsR0FBbEIsVUFBbUIsSUFBSTtRQUNuQixJQUFNLFdBQVcsR0FBVztZQUN4QixFQUFFLEVBQUUsRUFBRTtZQUNOLE1BQU0sRUFBRSxFQUFFO1lBQ1YsTUFBTSxFQUFFLEVBQUU7WUFDVixRQUFRLEVBQUUsRUFBRTtZQUNaLE1BQU0sRUFBRSxFQUFFO1NBQ2IsQ0FBQztRQUNGLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFDbEUsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7YUFDbkQ7WUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDeEUsV0FBVyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUNoRDtZQUNELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsTUFBTSxFQUFFO2dCQUM1RSxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsTUFBTSxDQUFDO2FBQ3hEO1lBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtnQkFDMUUsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDekQ7WUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUNsSSxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUNuRTtTQUNKO1FBQ0QsT0FBTyxXQUFXLENBQUM7SUFDdkIsQ0FBQztJQUVELDhDQUF5QixHQUF6QixVQUEwQixVQUFXO1FBQXJDLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FDakIsVUFBQSxRQUFRO1lBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsaUNBQWlDLEVBQUUsS0FBSSxDQUFDLGlDQUFpQyxFQUFFLFVBQVUsQ0FBQztpQkFDekcsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDWCxJQUFNLFdBQVcsR0FBVyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7R0FlRDtJQUNDLHNDQUFpQixHQUFqQixVQUFrQixVQUFXO1FBQTdCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FDakIsVUFBQSxRQUFRO1lBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsd0JBQXdCLEVBQUUsS0FBSSxDQUFDLGdDQUFnQyxFQUFFLFVBQVUsQ0FBQztpQkFDL0YsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDWCxJQUFNLFdBQVcsR0FBVyxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7U0FrQks7SUFFTDs7UUFFSTtJQUVKLDZDQUF3QixHQUF4QixVQUF5QixVQUFXO1FBQXBDLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUNsRixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbkcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLDhCQUE4QixFQUFFLEtBQUksQ0FBQyw4QkFBOEIsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUN2SCxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDbEcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQW9CLEdBQXBCLFVBQXFCLEVBQUU7UUFDbkIsSUFBTSxVQUFVLEdBQUc7WUFDZixlQUFlLEVBQUUsRUFBRTtTQUN0QixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDcEcsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7O1FBZ0JJO0lBRUoscUNBQWdCLEdBQWhCLFVBQWlCLEVBQUU7UUFDZixJQUFNLFVBQVUsR0FBRztZQUNmLFdBQVcsRUFBRSxFQUFFO1NBQ2xCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7O1FBZ0JJO0lBRUosdUNBQWtCLEdBQWxCLFVBQW1CLEVBQUU7UUFDakIsSUFBTSxVQUFVLEdBQUc7WUFDZixhQUFhLEVBQUUsRUFBRTtTQUNwQixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLHlCQUF5QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7OztTQWdCSztJQUVMLHlDQUFvQixHQUFwQixVQUFxQixFQUFFO1FBQ25CLElBQU0sVUFBVSxHQUFHO1lBQ2YsZUFBZSxFQUFFLEVBQUU7U0FDdEIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BHLENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7OztTQWdCSztJQUVMLHVDQUFrQixHQUFsQixVQUFtQixFQUFFO1FBQ2pCLElBQU0sVUFBVSxHQUFHO1lBQ2YsYUFBYSxFQUFFLEVBQUU7U0FDcEIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7UUFnQkk7SUFFSjs7UUFFSTtJQUVKLHFDQUFnQixHQUFoQixVQUFpQixVQUFXO1FBQTVCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGVBQWUsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDMUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsY0FBYyxFQUFFLEtBQUksQ0FBQywwQkFBMEIsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMzRixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQzFGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9DQUFlLEdBQWYsVUFBZ0IsVUFBVTtRQUN0QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMzRSxDQUFDO0lBRUQseUNBQW9CLEdBQXBCLFVBQXFCLFVBQVU7UUFDM0IsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsc0JBQXNCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQUVELDZDQUF3QixHQUF4QixVQUF5QixVQUFVO1FBQy9CLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxnQ0FBZ0MsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRUQ7O1FBRUk7SUFFSixnQ0FBVyxHQUFYLFVBQVksVUFBVztRQUF2QixpQkFhQztRQVpHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3BFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckYsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMzRixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3BGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtDQUFhLEdBQWI7UUFDSSxJQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxNQUFNO1NBQ3hCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxtQ0FBbUMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMvRyxDQUFDO0lBRUQsMkNBQXNCLEdBQXRCO1FBQ0ksSUFBTSxVQUFVLEdBQUc7WUFDZixhQUFhLEVBQUUsTUFBTTtTQUN4QixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxJQUFJLENBQUMsbUNBQW1DLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekgsQ0FBQztJQUVEOztTQUVLO0lBRUwsMENBQXFCLEdBQXJCLFVBQXNCLFVBQVc7UUFBakMsaUJBYUM7UUFaRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQy9FLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoRyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsdUJBQXVCLEVBQUUsS0FBSSxDQUFDLDJCQUEyQixFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzdHLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUMvRixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBWSxHQUFaLFVBQWEsVUFBVTtRQUNuQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDckYsQ0FBQztJQUVEOztZQUVRO0lBRVIsb0NBQWUsR0FBZixVQUFnQixVQUFXO1FBQTNCLGlCQWFDO1FBWkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLGNBQWMsQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDekUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsMkJBQTJCLEVBQUUsS0FBSSxDQUFDLHFCQUFxQixFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7b0JBQzNHLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDekYsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O1FBRUk7SUFFSixnQ0FBVyxHQUFYLFVBQVksVUFBVztRQUF2QixpQkFjQztRQWJHLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQSxRQUFRO1lBQzFCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ3BFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckYsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMzRixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3BGLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztRQUVJO0lBRUosa0NBQWEsR0FBYixVQUFjLFVBQVc7UUFBekIsaUJBYUM7UUFaRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN0RSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZGLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtvQkFDL0YsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN0RixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBbUIsR0FBbkIsVUFBb0IsV0FBWTtRQUFoQyxpQkFlQztRQWRHLElBQU0sVUFBVSxHQUFHO1lBQ2YsV0FBVyxFQUFFLFdBQVc7WUFDeEIsTUFBTSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDeEIsU0FBUyxFQUFFLENBQUM7Z0JBQ1osUUFBUSxFQUFFLEVBQUU7YUFDZjtTQUNKLENBQUE7UUFDRCxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxLQUFJLENBQUMseUJBQXlCLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDdEcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsb0NBQWUsR0FBZixVQUFnQixVQUFVO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRUQsd0NBQW1CLEdBQW5CLFVBQW9CLFVBQVU7UUFDMUIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFRCx3REFBbUMsR0FBbkMsVUFBb0MsRUFBRTtRQUNsQyxJQUFNLFVBQVUsR0FBRztZQUNmLGFBQWEsRUFBRSxFQUFFO1NBQ3BCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDRCQUE0QixFQUFFLElBQUksQ0FBQywyQ0FBMkMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMvSCxDQUFDO0lBRUQsaURBQTRCLEdBQTVCLFVBQTZCLEVBQUU7UUFDM0IsSUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsRUFBRTtTQUNiLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxvQ0FBb0MsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNqSCxDQUFDO0lBRUQsZ0RBQTJCLEdBQTNCO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLG1DQUFtQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCw0Q0FBdUIsR0FBdkIsVUFBd0IsS0FBYSxFQUFFLGFBQXFCLEVBQUUsT0FBZSxFQUFFLFFBQWdCLEVBQUUsWUFBb0IsRUFBRSxVQUFrQjtRQUNySSxJQUFNLFVBQVUsR0FBRztZQUNmLHVCQUF1QixFQUFFO2dCQUNyQixhQUFhLEVBQUUsS0FBSztnQkFDcEIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFlBQVksRUFBRSxZQUFZO2dCQUMxQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsa0JBQWtCLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRTt3QkFDVCxhQUFhLEVBQUUsYUFBYTt3QkFDNUIsVUFBVSxFQUFFLFVBQVU7cUJBQ3pCO2lCQUNKO2dCQUNELFVBQVUsRUFBRTtvQkFDUixJQUFJLEVBQUU7d0JBQ0YsV0FBVyxFQUFFOzRCQUNULE9BQU8sRUFBRSxPQUFPOzRCQUNoQixtQkFBbUIsRUFBRSxFQUFFO3lCQUMxQjtxQkFDSjtpQkFDSjthQUNKO1NBQ0osQ0FBQztRQUVGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNsRyxDQUFDO0lBRUQsMkNBQXNCLEdBQXRCLFVBQXVCLEtBQWEsRUFBRSxhQUFxQixFQUFFLE9BQWUsRUFBRSxRQUFnQixFQUFFLFlBQW9CLEVBQUUsVUFBa0I7UUFDcEksSUFBTSxVQUFVLEdBQUc7WUFDZixzQkFBc0IsRUFBRTtnQkFDcEIsYUFBYSxFQUFFLEtBQUs7Z0JBQ3BCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixZQUFZLEVBQUUsWUFBWTtnQkFDMUIsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLGtCQUFrQixFQUFFO29CQUNoQixXQUFXLEVBQUU7d0JBQ1QsYUFBYSxFQUFFLGFBQWE7d0JBQzVCLFVBQVUsRUFBRSxVQUFVO3FCQUN6QjtpQkFDSjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsSUFBSSxFQUFFO3dCQUNGLFdBQVcsRUFBRTs0QkFDVCxPQUFPLEVBQUUsT0FBTzs0QkFDaEIsbUJBQW1CLEVBQUUsRUFBRTt5QkFDMUI7cUJBQ0o7aUJBRUo7Z0JBQ0QsTUFBTSxFQUFFLElBQUk7Z0JBQ1osSUFBSTtnQkFDSixtRUFBbUU7Z0JBQ25FLG9CQUFvQjtnQkFDcEIscURBQXFEO2dCQUNyRCxJQUFJO2FBQ1A7U0FDSixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFJRCxtREFBOEIsR0FBOUIsVUFBK0IsS0FBYSxFQUFFLFdBQW1CLEVBQUUsT0FBZSxFQUFFLFFBQWdCLEVBQUUsWUFBb0IsRUFBRSxVQUFrQjtRQUMxSSxJQUFNLFVBQVUsR0FBRztZQUNmLHNCQUFzQixFQUFFO2dCQUNwQixhQUFhLEVBQUUsS0FBSztnQkFDcEIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFlBQVksRUFBRSxZQUFZO2dCQUMxQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsa0JBQWtCLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxXQUFXO2lCQUMzQjtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsSUFBSSxFQUFFO3dCQUNGLFdBQVcsRUFBRTs0QkFDVCxPQUFPLEVBQUUsT0FBTzs0QkFDaEIsbUJBQW1CLEVBQUUsRUFBRTt5QkFDMUI7cUJBQ0o7aUJBRUo7Z0JBQ0QsTUFBTSxFQUFFLElBQUk7Z0JBQ1osSUFBSTtnQkFDSixtRUFBbUU7Z0JBQ25FLG9CQUFvQjtnQkFDcEIscURBQXFEO2dCQUNyRCxJQUFJO2FBQ1A7U0FDSixDQUFDO1FBRUYsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCxzQ0FBaUIsR0FBakIsVUFBa0IsYUFBYSxFQUFFLFVBQVUsRUFBRSxRQUFRO1FBQ2pELElBQU0sWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN4QixhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUN6QixZQUFZLENBQUMsSUFBSSxDQUFDO2dCQUNkLGFBQWEsRUFBRSxPQUFPO2dCQUN0QixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsUUFBUSxFQUFFLFFBQVE7YUFDckIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLFlBQVksQ0FBQztJQUN4QixDQUFDO0lBRUQscUNBQWdCLEdBQWhCLFVBQWlCLEtBQWEsRUFBRSxhQUFxQyxFQUFFLE9BQWUsRUFBRSxRQUFnQixFQUFFLFlBQW9CLEVBQUUsVUFBa0IsRUFDOUksV0FBbUIsRUFBRSxjQUF1QixFQUFFLFFBQWdCLEVBQUUsbUJBQW1CLEVBQUUsTUFBTTtRQUMzRixJQUFNLFVBQVUsR0FBRztZQUNmLGlCQUFpQixFQUFFO2dCQUNmLGFBQWEsRUFBRSxLQUFLO2dCQUNwQixVQUFVLEVBQUUsVUFBVTtnQkFDdEIsWUFBWSxFQUFFLFlBQVk7Z0JBQzFCLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsUUFBUTtnQkFDbEIsa0JBQWtCLEVBQUU7b0JBQ2hCLFdBQVcsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUM7aUJBQzNFO2dCQUNELFVBQVUsRUFBRTtvQkFDUixJQUFJLEVBQUU7d0JBQ0YsV0FBVyxFQUFFOzRCQUNULE9BQU8sRUFBRSxPQUFPOzRCQUNoQixtQkFBbUIsRUFBRSxFQUFFO3lCQUMxQjtxQkFDSjtpQkFDSjtnQkFDRCxXQUFXLEVBQUUsV0FBVztnQkFDeEIsZUFBZSxFQUFFO29CQUNiLGNBQWMsRUFBRSxtQkFBbUI7aUJBQ3RDO2dCQUNELGNBQWMsRUFBRSxjQUFjO2dCQUM5QixjQUFjLEVBQUUsTUFBTTthQUN6QjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQ7Ozs7O1NBS0s7SUFFTCw2Q0FBd0IsR0FBeEI7UUFBQSxpQkFpQkM7UUFoQkcsSUFBTSxTQUFTLEdBQUc7WUFDZCxNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRTtTQUNyRCxDQUFDO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLDJCQUEyQixDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN0RixRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLEtBQUksQ0FBQyxrQ0FBa0MsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUNsSCx5R0FBeUc7b0JBQ3pHLElBQU0sc0JBQXNCLEdBQUcsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDaEcsUUFBUSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO29CQUN0QyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2FBQ047UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBb0IsR0FBcEIsVUFBcUIsVUFBVSxFQUFFLHdCQUF3QjtRQUF6RCxpQkE2QkM7UUE1QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxTQUFTLENBQUM7WUFDZCxJQUFJLHdCQUF3QixFQUFFO2dCQUMxQixTQUFTLEdBQUc7b0JBQ1Isd0JBQXdCLEVBQUU7d0JBQ3RCLEVBQUUsRUFBRSx3QkFBd0I7cUJBQy9CO29CQUNELDRCQUE0QixFQUFFLFVBQVU7aUJBQzNDLENBQUM7YUFDTDtpQkFBTTtnQkFDSCxTQUFTLEdBQUc7b0JBQ1IsNEJBQTRCLEVBQUUsVUFBVTtpQkFDM0MsQ0FBQzthQUNMO1lBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsc0JBQXNCLEVBQUUsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLDZCQUE2QixFQUFFLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7Z0JBQzdLLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDMUMsS0FBSSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDOUUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnREFBMkIsR0FBM0IsVUFBNEIsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsU0FBUztRQUFqRSxpQkF1QkM7UUF0QkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBTSxTQUFTLEdBQUc7Z0JBQ2QsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFFBQVEsRUFBRSxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFLENBQUMsRUFBRTtnQkFDbkQsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsU0FBUyxFQUFFLFNBQVM7YUFDdkIsQ0FBQTtZQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLGVBQWUsRUFBRSxLQUFJLENBQUMsaUNBQWlDLEVBQUUsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtnQkFDMUcsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsS0FBSyxLQUFLLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7b0JBQzdKLEtBQUksQ0FBQyxjQUFjLENBQUMsMkJBQTJCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDL0YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQyxFQUFFLFVBQUEsS0FBSztnQkFDSixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBb0IsR0FBcEIsVUFBcUIsZ0JBQWdCO1FBQXJDLGlCQXVCQztRQXRCRyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixJQUFNLFNBQVMsR0FBRztnQkFDZCxtQkFBbUIsRUFBRTtvQkFDakIsd0JBQXdCLEVBQUU7d0JBQ3RCLEVBQUUsRUFBRSxnQkFBZ0I7cUJBQ3ZCO2lCQUNKO2FBQ0osQ0FBQztZQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLHNCQUFzQixFQUFFLEtBQUksQ0FBQyw2QkFBNkIsRUFBRSxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUM3RyxJQUFJLFFBQVEsRUFBRTtvQkFDVixLQUFJLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQ2xFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O01BRUU7SUFDRiw2Q0FBd0IsR0FBeEIsVUFBeUIsVUFBVztRQUNoQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLCtCQUErQixFQUFFLElBQUksQ0FBQywrQkFBK0IsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM5RyxDQUFDO0lBRUQ7O01BRUU7SUFDRiwwQ0FBcUIsR0FBckIsVUFBc0IsVUFBVztRQUM3QixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDRCQUE0QixFQUFFLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN4RyxDQUFDO0lBRUQsOENBQXlCLEdBQXpCLFVBQTBCLE1BQU07UUFDNUIsSUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsTUFBTTtTQUNqQixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsRUFBRSxJQUFJLENBQUMsaUNBQWlDLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDMUgsQ0FBQztJQUVELG1EQUE4QixHQUE5QixVQUErQixNQUFNO1FBQ2pDLElBQU0sVUFBVSxHQUFHO1lBQ2YsTUFBTSxFQUFFLE1BQU07U0FDakIsQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUNBQXFDLEVBQUUsSUFBSSxDQUFDLHFDQUFxQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2xJLENBQUM7SUFFRCxtQ0FBYyxHQUFkLFVBQWUsTUFBTSxFQUFFLGFBQWEsRUFBRSxjQUFjO1FBQ2hELElBQU0sVUFBVSxHQUFHO1lBQ2YsTUFBTSxFQUFFLE1BQU07WUFDZCxhQUFhLEVBQUU7Z0JBQ1gsdUJBQXVCO2dCQUN2Qix5QkFBeUIsRUFBRSxhQUFhO2FBQzNDO1lBQ0QsY0FBYyxFQUFFO2dCQUNaLDBCQUEwQjtnQkFDMUIseUJBQXlCLEVBQUUsY0FBYzthQUM1QztTQUNKLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDekYsQ0FBQztJQUVELCtDQUEwQixHQUExQixVQUEyQixxQkFBcUI7UUFDNUMsSUFBTSxVQUFVLEdBQUc7WUFDZixvQkFBb0IsRUFBRTtnQkFDbEIseUJBQXlCLEVBQUUscUJBQXFCO2FBQ25EO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsOEJBQThCLEVBQUUsSUFBSSxDQUFDLDhCQUE4QixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BILENBQUM7SUFFRCwrQkFBVSxHQUFWLFVBQVcsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxrQkFBa0I7UUFDaEgsSUFBTSxVQUFVLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvRSxJQUFNLGFBQWEsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ25GLElBQU0sVUFBVSxHQUFHO1lBQ2YsT0FBTyxFQUFFLE9BQU87WUFDaEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsVUFBVSxFQUFFLFVBQVU7WUFDdEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsb0JBQW9CLEVBQUUsYUFBYTtZQUNuQyxTQUFTLEVBQUUsU0FBUztZQUNwQixVQUFVLEVBQUUsVUFBVTtZQUN0QixrQkFBa0IsRUFBRSxrQkFBa0I7WUFDdEMsVUFBVSxFQUFFLENBQUMsVUFBVSxJQUFJLFVBQVUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUM1RSxhQUFhLEVBQUUsQ0FBQyxhQUFhLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3hGLElBQUksRUFBRTtnQkFDRixPQUFPLEVBQUUsTUFBTTthQUNsQjtTQUNKLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFRCx3Q0FBbUIsR0FBbkIsVUFBb0IsTUFBTTtRQUN0QixJQUFNLFVBQVUsR0FBRztZQUNmLE1BQU0sRUFBRSxNQUFNO1NBQ2pCLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFLElBQUksQ0FBQywwQkFBMEIsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUM1RyxDQUFDO0lBR0QsYUFBYTtJQUNiLHlDQUFvQixHQUFwQixVQUFxQixFQUFFO1FBQ25CLElBQU0sVUFBVSxHQUFHO1lBQ2YsZUFBZSxFQUFFO2dCQUNiLG9CQUFvQixFQUFFO29CQUNsQixFQUFFLEVBQUUsRUFBRTtpQkFDVDthQUNKO1NBQ0osQ0FBQztRQUNGLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLCtCQUErQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBRXRILENBQUM7SUFFRCx5Q0FBb0IsR0FBcEIsVUFBcUIsUUFBZ0I7UUFBckMsaUJBY0M7UUFiRyxJQUFNLFNBQVMsR0FBRztZQUNkLFFBQVEsRUFBRSxRQUFRO1NBRXJCLENBQUM7UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFVBQUEsUUFBUTtZQUMxQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyx3QkFBd0IsRUFBRSxLQUFJLENBQUMsd0JBQXdCLEVBQUUsU0FBUyxDQUFDO2lCQUN0RixTQUFTLENBQUMsVUFBQSxRQUFRO2dCQUNmLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrREFBNkIsR0FBN0IsVUFBOEIsSUFBSSxFQUFFLEdBQUc7UUFDbkMsSUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEVBQUU7WUFDakQsTUFBTSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDeEIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsUUFBUSxFQUFFLEdBQUc7YUFDaEI7U0FDSixDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDdEcsQ0FBQztJQUVELHVDQUFrQixHQUFsQixVQUFtQixVQUFVO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCwwQ0FBcUIsR0FBckI7UUFBQSxpQkFpQkM7UUFoQkcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDdkUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsZUFBZSxFQUFFLEtBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO29CQUMvRixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsb0JBQW9CLEVBQUU7d0JBQzNDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUM7d0JBQzdDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7eUJBQU07d0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FCQUN2QjtnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkE5NkIwQixjQUFjOzs7SUF2R2hDLFVBQVU7UUFIdEIsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztPQUNXLFVBQVUsQ0F1aEN0QjtxQkFyaUNEO0NBcWlDQyxBQXZoQ0QsSUF1aENDO1NBdmhDWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBhY3JvbnVpIGZyb20gJy4uL2F1dGgvdXRpbGl0eSc7XHJcbmltcG9ydCAqIGFzICQgZnJvbSAnanF1ZXJ5JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBHbG9hYmFsQ29uZmlnIGFzIGNvbmZpZyB9IGZyb20gJy4uL2NvbmZpZy9jb25maWcnO1xyXG5pbXBvcnQgeyBQZXJzb24gfSBmcm9tICcuLi9vYmplY3RzL1BlcnNvbic7XHJcbmltcG9ydCB7IFNoYXJpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zaGFyaW5nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU3RvcmFnZUNvbnN0YW50cyB9IGZyb20gJy4uLy4uL3NoYXJlZC9jb25zdGFudHMvc2Vzc2lvbi1zdG9yYWdlLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gJy4uL29iamVjdHMvU3RhdHVzJztcclxuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBTZXJ2aWNlIHtcclxuXHJcbiAgICBwdWJsaWMgVVNFUl9OUyA9ICdodHRwOi8vc2NoZW1hcy5jb3JkeXMuY29tLzEuMS9sZGFwJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFNfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2FwcC9icG0vMS4wJztcclxuICAgIHB1YmxpYyBQRVJTT05fTlMgPSAnaHR0cDovL3NjaGVtYXMvTVBNQ3VzdG9taXplZEFwcGxpY2F0aW9uUGFja2FnZXMvUGVyc29uL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIE1QTV9BUFBfUk9MRVNfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTU9yZ2FuaXphdGlvbk1vZGVsL01QTV9BUFBfUm9sZXMvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgTUVESUFfTUFOQUdFUl9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX01lZGlhX01hbmFnZXJfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIFdTQVBQX0NPUkVfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICAgIHB1YmxpYyBHRVRfUEVSU09OX0JZX1VTRVJfSURfTlMgPSAnaHR0cDovL3NjaGVtYXMvTVBNQ3VzdG9taXplZEFwcGxpY2F0aW9uUGFja2FnZXMvUGVyc29uL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9QRVJTT05fQllfSURFTlRJVFlfVVNFUl9JRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9NUE1DdXN0b21pemVkQXBwbGljYXRpb25QYWNrYWdlcy9QZXJzb24vb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgVEVBTVNfQlBNX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS90ZWFtcy9icG0vMS4wJztcclxuICAgIHB1YmxpYyBSRVZJRVdfQlBNX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9yZXZpZXcvYnBtLzEuMCc7XHJcbiAgICBwdWJsaWMgQVBQX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fQXBwX0NvbmZpZy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBQUk9KRUNUX0NPTkZJR19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fUHJvamVjdF9Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgQVNTRVRfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9Bc3NldF9Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgQlJBTkRfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9CcmFuZF9Db25maWcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgQ09NTUVOVF9DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0NvbW1lbnRzX0NvbmZpZy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBCVUxLX0NSX0FQUFJPVkFMX05TID0gJ2h0dHA6Ly9zY2hlbWFzLmFjaGVyb24uY29tL21wbS9yZXZpZXcvYnBtLzEuMCc7XHJcbiAgICBwdWJsaWMgVklFV19DT05GSUdfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1ZpZXdfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIFRFQU1fUk9MRV9NQVBQSU5HX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fVGVhbV9Sb2xlX01hcHBpbmcvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9URUFNU19OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fVGVhbXMvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9QRVJTT05TX05TID0gJ2h0dHA6Ly9zY2hlbWFzL01QTUN1c3RvbWl6ZWRBcHBsaWNhdGlvblBhY2thZ2VzL1BlcnNvbi9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBERUxJVkVSQUJMRV9SRVZJRVdfTUVUSE9EX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0RlbGl2ZXJhYmxlUmV2aWV3L29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIFRBU0tfUkVWSUVXX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9EZWxpdmVyYWJsZVJldmlldy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBNUE1fRVZFTlRTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fRXZlbnRzL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIE1QTV9XT1JLRkxPV19BQ1RJT05TX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fV29ya2Zsb3dfQWN0aW9ucy9vcGVyYXRpb25zJztcclxuICAgIHB1YmxpYyBHRVRfRU1BSUxfRVZFTlRTX0ZPUl9NQU5BR0VSX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0VtYWlsX0V2ZW50X1RlbXBsYXRlL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9FTUFJTF9FVkVOVFNfRk9SX1VTRVJfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvRW1haWxfRXZlbnRfVGVtcGxhdGUvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgR0VUX01BSUxfUFJFRkVSRU5DRV9CWV9VU0VSX0lEX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9FbWFpbF9QcmVmZXJlbmNlL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIEdFVF9VU0VSX1BSRUZFUkVOQ0VfTUFJTF9URU1QTEFURVNfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL3dzYXBwL2NvcmUvMS4wJztcclxuICAgIHB1YmxpYyBNUE1fV09SS0ZMT1dfTlMgPSAnaHR0cDovL3NjaGVtYXMuYWNoZXJvbi5jb20vbXBtL2NvcmUvd29ya2Zsb3cvYnBtLzEuMCc7XHJcbiAgICBwdWJsaWMgREVMRVRFX01QTV9FTUFJTF9QUkVGRVJFTkNFX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9FbWFpbF9QcmVmZXJlbmNlL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIE1QTV9VU0VSX1BSRUZFUkVOQ0VfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX1VzZXJfUHJlZmVyZW5jZS9vcGVyYXRpb25zJztcclxuXHJcbiAgICBwdWJsaWMgQ0FNUEFJR05fREVUQUlMU19NRVRIT0RfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvQ2FtcGFpZ24vb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgU1RBVFVTX01FVEhPRF9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9NUE1fU3RhdHVzL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIHB1YmxpYyBHRVRfQUxMX1VTRVJTX05TID0gJ2h0dHA6Ly9zY2hlbWFzL09wZW5UZXh0RW50aXR5SWRlbnRpdHlDb21wb25lbnRzL1VzZXIvb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgR0VUX0VYUE9SVF9EQVRBX0JZX1VTRVJfTlMgPSAnaHR0cDovL3NjaGVtYXMvQWNoZXJvbk1QTUNvcmUvTVBNX0V4cG9ydF9EYXRhL29wZXJhdGlvbnMnO1xyXG4gICAgcHVibGljIERPV05MT0FEX0ZJTEVfQllfUEFUSF9OUyA9ICdodHRwOi8vc2NoZW1hcy5hY2hlcm9uLmNvbS9tcG0vd3NhcHAvZXhjZWwvMS4wJztcclxuXHJcbiAgICBwdWJsaWMgTVBNX05PVElGSUNBVElPTl9OUyA9ICdodHRwOi8vc2NoZW1hcy9BY2hlcm9uTVBNQ29yZS9Ob3RpZmljYXRpb24vb3BlcmF0aW9ucyc7XHJcbiAgICBwdWJsaWMgRU1BSUxfQ09ORklHX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL0dlbmVyYWxfRW1haWxfQ29uZmlnL29wZXJhdGlvbnMnO1xyXG5cclxuICAgIHB1YmxpYyBDVVJTT1JfTlMgPSAnaHR0cDovL3NjaGVtYXMub3BlbnRleHQuY29tL2Jwcy9lbnRpdHkvY29yZSc7XHJcblxyXG4gICAgcHVibGljIEdFVF9QRVJTT05fREVUQUlMU19CWV9VU0VSX0lEX1dTID0gJ0dldFBlcnNvbkJ5VXNlcklkJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFNfV1MgPSAnR2V0QXBwbGljYXRpb25Gb3JVc2VyJztcclxuICAgIHB1YmxpYyBHRVRfQUxMX1JPTEVTX1dTID0gJ0dldEFsbFJvbGVzJztcclxuICAgIHB1YmxpYyBHRVRfTUVESUFfTUFOQUdFUl9DT05GSUdfV1MgPSAnR2V0TWVkaWFNYW5hZ2VyQ29uZmlnJztcclxuICAgIHB1YmxpYyBHRVRfUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSURfV1MgPSAnR2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZCc7XHJcbiAgICBwdWJsaWMgR0VUX1RFQU1fUk9MRVNfV1MgPSAnR2V0VGVhbVJvbGVzJztcclxuICAgIHB1YmxpYyBHRVRfVVNFU19GT1JfVEVBTV9XUyA9ICdHZXRVc2Vyc0ZvclRlYW0nO1xyXG4gICAgcHVibGljIEdFVF9QT1NTSUJMRV9DUl9BQ1RJT05TX0ZPUl9VU0VSX1dTID0gJ0dldFBvc3NpYmlsZUNSQWN0aW9uc0ZvclVzZXInO1xyXG4gICAgcHVibGljIE9QRU5fRklMRV9QUk9PRklOR19TRVNTSU9OX1dTID0gJ09wZW5GaWxlUHJvb2ZTZXNzaW9uJztcclxuICAgIHB1YmxpYyBHRVRfQVBQX0NPTkZJR19CWV9JRF9XUyA9ICdHZXRBcHBDb25maWdCeUlkJztcclxuICAgIHB1YmxpYyBHRVRfUFJPSkVDVF9DT05GSUdfQllfSURfV1MgPSAnR2V0UHJvamVjdENvbmZpZ0J5SWQnO1xyXG4gICAgcHVibGljIEdFVF9BU1NFVF9DT05GSUdfQllfSURfV1MgPSAnR2V0QXNzZXRDb25maWdCeUlkJztcclxuICAgIHB1YmxpYyBHRVRfQlJBTkRfQ09ORklHX0JZX0lEX1dTID0gJ0dldEJyYW5kQ29uZmlnQnlJZCc7XHJcbiAgICBwdWJsaWMgR0VUX0NPTU1FTlRfQ09ORklHX0JZX0lEX1dTID0gJ0dldENvbW1lbnRzQ29uZmlnQnlJZCc7XHJcbiAgICBwdWJsaWMgSEFOREVMX0NSX0FQUFJPVkFMX1dTID0gJ0hhbmRsZUNSQXBwcm92YWwnO1xyXG4gICAgcHVibGljIEdFVF9BTExfTVBNX1ZJRVdfQ09ORklHX1dTID0gJ0dldEFsbE1QTVZpZXdDb25maWcnO1xyXG4gICAgcHVibGljIEdFVF9BTExfVEVBTV9ST0xFU19XUyA9ICdHZXRBbGxUZWFtUm9sZXMnO1xyXG4gICAgcHVibGljIEdFVF9GSUxFX1BST09GSU5HX1NFU1NJT05fV1MgPSAnR2V0RmlsZVByb29maW5nU2Vzc2lvbic7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9URUFNU19XUyA9ICdHZXRBbGxUZWFtcyAnO1xyXG4gICAgcHVibGljIEdFVF9BTExfUEVSU09OU19XUyA9ICdHZXRBbGxQZXJzb25zJztcclxuICAgIHB1YmxpYyBHRVRfUEVSU09OU19CWV9LRVlXT1JEX1dTID0gJ0dldFBlcnNvbnNCeUtleXdvcmQnO1xyXG4gICAgcHVibGljIEdFVF9ERUxJVkVSQUJMRV9SRVZJRVdfQllfREVMSVZFUkFCTEVfSURfV1MgPSAnR2V0RGVsaXZlcmFibGVSZXZpZXdCeURlbGl2ZXJhYmxlSUQnO1xyXG4gICAgcHVibGljIEdFVF9ERUxJVkVSQUJMRV9SRVZJRVdfQllfVEFTS19JRF9XUyA9ICdHZXREZWxpdmVyYWJsZVJldmlld0J5VGFza0lEJztcclxuICAgIHB1YmxpYyBHRVRfVVNFUlNfRk9SX1RFQU1fUk9MRV9XUyA9ICdHZXRVc2Vyc0ZvclRlYW1Sb2xlJztcclxuICAgIHB1YmxpYyBHRVRfTVBNX0VWRU5UU19CWV9DQVRFR09SWV9MRVZFTF9XUyA9ICdHZXRNUE1FdmVudHNCeUNhdGVnb3J5TGV2ZWwnO1xyXG4gICAgcHVibGljIEdFVF9XT1JLRkxPV19BQ1RJT05TX0JZX0NBVEVHT1JZX1dTID0gJ0dldFdvcmtmbG93QWN0aW9uc0J5Q2F0ZWdvcnknO1xyXG5cclxuICAgIHB1YmxpYyBHRVRfRU1BSUxfRVZFTlRTX0ZPUl9NQU5BR0VSX1dTID0gJ0dldEVtYWlsRXZlbnRzRm9yTWFuYWdlcic7XHJcbiAgICBwdWJsaWMgR0VUX0VNQUlMX0VWRU5UU19GT1JfVVNFUl9XUyA9ICdHZXRFbWFpbEV2ZW50c0ZvclVzZXInO1xyXG4gICAgcHVibGljIEdFVF9NQUlMX1BSRUZFUkVOQ0VfQllfVVNFUl9JRF9XUyA9ICdHZXRNYWlsUHJlZmVyZW5jZUJ5VXNlcklkJztcclxuICAgIHB1YmxpYyBHRVRfVVNFUl9QUkVGRVJFTkNFX01BSUxfVEVNUExBVEVTX1dTID0gJ0dldFVzZXJQcmVmZXJlbmNlTWFpbFRlbXBsYXRlcyc7XHJcbiAgICBwdWJsaWMgTUFJTF9QUkVGRVJFTkNFX1dTID0gJ01haWxQcmVmZXJlbmNlJztcclxuICAgIHB1YmxpYyBERUxFVEVfTVBNX0VNQUlMX1BSRUZFUkVOQ0VfV1MgPSAnRGVsZXRlTVBNX0VtYWlsX1ByZWZlcmVuY2UgJztcclxuICAgIHB1YmxpYyBFWFBPUlRfREFUQV9XUyA9ICdFeHBvcnREYXRhJztcclxuICAgIHB1YmxpYyBHRVRfRVhQT1JUX0RBVEFfQllfVVNFUl9XUyA9ICdHZXRFeHBvcnREYXRhQnlVc2VyJztcclxuICAgIHB1YmxpYyBET1dOTE9BRF9GSUxFX0JZX1BBVEhfV1MgPSAnRG93bmxvYWRGaWxlQnlQYXRoJztcclxuXHJcbiAgICBwdWJsaWMgR0VUX01QTV9VU0VSX1BSRUZFUkVOQ0VfQllfVVNFUl9XUyA9ICdHZXRNUE1Vc2VyUHJlZmVyZW5jZUJ5VXNlcic7XHJcbiAgICBwdWJsaWMgQ1JFQVRFX01QTV9VU0VSX1BSRUZFUkVOQ0VfV1MgPSAnQ3JlYXRlTVBNX1VzZXJfUHJlZmVyZW5jZSc7XHJcbiAgICBwdWJsaWMgVVBEQVRFX01QTV9VU0VSX1BSRUZFUkVOQ0VfV1MgPSAnVXBkYXRlTVBNX1VzZXJfUHJlZmVyZW5jZSc7XHJcbiAgICBwdWJsaWMgREVMRVRFX01QTV9VU0VSX1BSRUZFUkVOQ0VfV1MgPSAnRGVsZXRlTVBNX1VzZXJfUHJlZmVyZW5jZSc7XHJcblxyXG4gICAgcHVibGljIEdFVF9BTExfQ0FNUEFJR05fV1NfTUVUSE9EX05BTUUgPSAnR2V0QWxsQ2FtcGFpZ24nO1xyXG4gICAgcHVibGljIEdFVF9TVEFUVVNfQllfQ0FURUdPUllfTEVWRUxfTkFNRV9XUyA9ICdHZXRBbGxTdGF0dXNGb3JDYXRlZ29yeUxldmVsJztcclxuXHJcbiAgICBwdWJsaWMgR0VUX0FMTF9VU0VSUyA9ICdHZXRBbGxVc2Vycyc7XHJcbiAgICBwdWJsaWMgR0VUX0FMTF9TVEFUVVMgPSAnR2V0QWxsU3RhdHVzJztcclxuICAgIHB1YmxpYyBVUERBVEVfREVGQVVMVF9VU0VSX1BSRUZFUkVOQ0VfV1MgPSAnVXBkYXRlRGVmYXVsdFVzZXJQcmVmZXJlbmNlJztcclxuICAgIHB1YmxpYyBHRVRfTk9USUZJQ0FUSU9OX0JZX1VTRVJfV1MgPSAnR2V0Tm90aWZpY2F0aW9uQnlVc2VyJztcclxuICAgIHB1YmxpYyBVUERBVEVfTk9USUZJQ0FUSU9OX1dTID0gJ1VwZGF0ZU5vdGlmaWNhdGlvbic7XHJcbiAgICBwdWJsaWMgR0VUX0RFRkFVTFRfRU1BSUxfQ09ORklHX1dTID0gJ0dldERlZmF1bHRFbWFpbENvbmZpZyc7XHJcblxyXG4gICAgcHVibGljIERFTEVURV9NUE1fRVhQT1JUX0RBVEFfQllfSURfV1MgPSAnRGVsZXRlTVBNX0V4cG9ydF9EYXRhJztcclxuICAgIHB1YmxpYyBERUxFVEVfTVBNX0VYUE9SVF9EQVRBX0JZX0lEX05TID0gJ2h0dHA6Ly9zY2hlbWFzL0FjaGVyb25NUE1Db3JlL01QTV9FeHBvcnRfRGF0YS9vcGVyYXRpb25zJztcclxuXHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHNoYXJpbmdTZXJ2aWNlOiBTaGFyaW5nU2VydmljZSxcclxuICAgICkgeyB9XHJcblxyXG4gICAgaW52b2tlUmVxdWVzdChuYW1lc3BhY2UsIG1ldGhvZCwgcGFyYW1ldGVycyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgaWYgKG5hbWVzcGFjZSA9PSBudWxsIHx8IG5hbWVzcGFjZSA9PT0gJycgfHwgdHlwZW9mIChuYW1lc3BhY2UpID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ1BsZWFzZSBwcm92aWRlIHZhbGlkIG5hbWVzcGFjZS4nKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKG1ldGhvZCA9PSBudWxsIHx8IG1ldGhvZCA9PT0gJycgfHwgdHlwZW9mIChtZXRob2QpID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oJ1BsZWFzZSBwcm92aWRlIHZhbGlkIG1ldGhvZCBuYW1lLicpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAocGFyYW1ldGVycyA9PSBudWxsIHx8IHBhcmFtZXRlcnMgPT09ICcnIHx8IHR5cGVvZiAocGFyYW1ldGVycykgPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHBhcmFtZXRlcnMgPSAnJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShhcHBTZXJ2aWNlT2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAkLnNvYXAoe1xyXG4gICAgICAgICAgICAgICAgbmFtZXNwYWNlLFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1ldGVyc1xyXG4gICAgICAgICAgICB9KS50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBhcHBTZXJ2aWNlT2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgICAgIGFwcFNlcnZpY2VPYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9LCAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIGFwcFNlcnZpY2VPYnNlcnZlci5lcnJvcihuZXcgRXJyb3IoZXJyb3IpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gTVBNVjMtMjAwOFxyXG4gICAgZ2V0QWxsVXNlcnMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9VU0VSUykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9VU0VSUykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX1VTRVJTX05TLCB0aGlzLkdFVF9BTExfVVNFUlMsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVVNFUlMsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBhZGRlZCBmb3IgY29kZSBoYXJkZW5paW5nIC1rZWVydGhhbmFcclxuICAgIGdldFN0YXR1c0J5Q2F0ZWdvcnlMZXZlbE5hbWUoY2F0ZWdvcnlMZXZlbE5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8QXJyYXk8U3RhdHVzPj4ge1xyXG4gICAgICAgIGNvbnN0IGdldFJlcXVlc3RPYmplY3QgPSB7XHJcbiAgICAgICAgICAgIGNhdGVnb3J5TGV2ZWxOYW1lOiBjYXRlZ29yeUxldmVsTmFtZSxcclxuICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbElEOiAnJ1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUUpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5TVEFUVVNfQllfQ0FURUdPUllfTEVWRUxfTkFNRSkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5TVEFUVVNfTUVUSE9EX05TLCB0aGlzLkdFVF9TVEFUVVNfQllfQ0FURUdPUllfTEVWRUxfTkFNRV9XUyxcclxuICAgICAgICAgICAgICAgICAgICBnZXRSZXF1ZXN0T2JqZWN0KVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXNlczogQXJyYXk8U3RhdHVzPiA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICdNUE1fU3RhdHVzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuU1RBVFVTX0JZX0NBVEVHT1JZX0xFVkVMX05BTUUsIEpTT04uc3RyaW5naWZ5KHN0YXR1c2VzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoc3RhdHVzZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gYWRkZWQgZm9yIGNvZGUgaGFyZGVuaWluZyAta2VlcnRoYW5hXHJcbiAgICAvKiBnZXRBbGxDYW1wYWlnbihwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0NBTVBBSUdOKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0NBTVBBSUdOKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkNBTVBBSUdOX0RFVEFJTFNfTUVUSE9EX05TLCB0aGlzLkdFVF9BTExfQ0FNUEFJR05fV1NfTUVUSE9EX05BTUUsIG51bGwpLnN1YnNjcmliZShjYW1wYWlnbkRldGFpbHMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0NBTVBBSUdOLCBKU09OLnN0cmluZ2lmeShjYW1wYWlnbkRldGFpbHMpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGNhbXBhaWduRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgLy8gYWRkZWQgZm9yIGNvZGUgaGFyZGVuaWluZyBcclxuICAgIGdldFVzZXJEZXRhaWxzTERBUChwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuVVNFUl9ERVRBSUxTX0xEQVApICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5VU0VSX0RFVEFJTFNfTERBUCkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5VU0VSX05TLCAnR2V0VXNlckRldGFpbHMnLCAnJykuc3Vic2NyaWJlKHVzZXJEZXRhaWxzID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlVTRVJfREVUQUlMU19MREFQLCBKU09OLnN0cmluZ2lmeSh1c2VyRGV0YWlscykpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ0luKGNhbGxiYWNrKSB7XHJcbiAgICAgICAgLyogIGNvbnN0IGdldFVzZXI6IE9ic2VydmFibGU8YW55PiA9IHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlVTRVJfTlMsICdHZXRVc2VyRGV0YWlscycsICcnKTtcclxuICAgICAgICAgZ2V0VXNlci5zdWJzY3JpYmUocmVzcG9uc2UgPT4geyAqL1xyXG4gICAgICAgIHRoaXMuZ2V0VXNlckRldGFpbHNMREFQKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXNlck9iamVjdCA9IGFjcm9udWkuZmluZE9iamVjdHNCeVByb3AocmVzcG9uc2UsICd1c2VyJyk7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJOYW1lID0gdXNlck9iamVjdFswXS5hdXRodXNlcmRuLnNwbGl0KCcsJylbMF0uc3BsaXQoJz0nKVsxXTtcclxuICAgICAgICAgICAgaWYgKHVzZXJOYW1lID09PSAnYW5vbnltb3VzJykge1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBjb25maWcuY29uZmlnLmxvZ291dFVybDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgZXJyRGF0YSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIHdoaWxlIGZpcmluZyBHZXRVc2VyRGV0YWlscyA6ICcgKyBlcnJEYXRhLnN0YXR1cyk7XHJcbiAgICAgICAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tQU1Nlc3Npb24oY2FsbGJhY2spIHtcclxuICAgICAgICB0aGlzLmxvZ0luKGNhbGxiYWNrKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQZXJzb25DYXN0ZWRPYmooZGF0YSk6IFBlcnNvbiB7XHJcbiAgICAgICAgY29uc3QgdXNlckRldGFpbHM6IFBlcnNvbiA9IHtcclxuICAgICAgICAgICAgSWQ6ICcnLFxyXG4gICAgICAgICAgICBJdGVtSWQ6ICcnLFxyXG4gICAgICAgICAgICB1c2VyQ046ICcnLFxyXG4gICAgICAgICAgICBmdWxsTmFtZTogJycsXHJcbiAgICAgICAgICAgIHVzZXJJZDogJydcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLlBlcnNvbiAmJiBkYXRhLlBlcnNvbi5Vc2VyX0lEICYmIGRhdGEuUGVyc29uLlVzZXJfSUQuX190ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICB1c2VyRGV0YWlscy51c2VyQ04gPSBkYXRhLlBlcnNvbi5Vc2VyX0lELl9fdGV4dDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZGF0YS5QZXJzb24gJiYgZGF0YS5QZXJzb25bJ1BlcnNvbi1pZCddICYmIGRhdGEuUGVyc29uWydQZXJzb24taWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgdXNlckRldGFpbHMuSWQgPSBkYXRhLlBlcnNvblsnUGVyc29uLWlkJ10uSWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGRhdGEuUGVyc29uICYmIGRhdGEuUGVyc29uWydQZXJzb24taWQnXSAmJiBkYXRhLlBlcnNvblsnUGVyc29uLWlkJ10uSXRlbUlkKSB7XHJcbiAgICAgICAgICAgICAgICB1c2VyRGV0YWlscy5JdGVtSWQgPSBkYXRhLlBlcnNvblsnUGVyc29uLWlkJ10uSXRlbUlkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkYXRhLlBlcnNvbiAmJiBkYXRhLlBlcnNvbi5EaXNwbGF5TmFtZSAmJiBkYXRhLlBlcnNvbi5EaXNwbGF5TmFtZS5fX3RleHQpIHtcclxuICAgICAgICAgICAgICAgIHVzZXJEZXRhaWxzLmZ1bGxOYW1lID0gZGF0YS5QZXJzb24uRGlzcGxheU5hbWUuX190ZXh0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkYXRhLlBlcnNvbiAmJiBkYXRhLlBlcnNvbi5QZXJzb25Ub1VzZXIgJiYgZGF0YS5QZXJzb24uUGVyc29uVG9Vc2VyWydJZGVudGl0eS1pZCddICYmIGRhdGEuUGVyc29uLlBlcnNvblRvVXNlclsnSWRlbnRpdHktaWQnXS5JZCkge1xyXG4gICAgICAgICAgICAgICAgdXNlckRldGFpbHMudXNlcklkID0gZGF0YS5QZXJzb24uUGVyc29uVG9Vc2VyWydJZGVudGl0eS1pZCddLklkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB1c2VyRGV0YWlscztcclxuICAgIH1cclxuXHJcbiAgICBnZXRQZXJzb25CeUlkZW50aXR5VXNlcklkKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxQZXJzb24+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoXHJcbiAgICAgICAgICAgIG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9QRVJTT05fQllfSURFTlRJVFlfVVNFUl9JRF9OUywgdGhpcy5HRVRfUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSURfV1MsIHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckRldGFpbHM6IFBlcnNvbiA9IHRoaXMuZ2V0UGVyc29uQ2FzdGVkT2JqKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHVzZXJEZXRhaWxzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogZ2V0UGVyc29uQnlJZGVudGl0eVVzZXJJZChwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8UGVyc29uPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QRVJTT05fQllfSURFTlRJVFlfVVNFUl9JRCkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSURfTlMsIHRoaXMuR0VUX1BFUlNPTl9CWV9JREVOVElUWV9VU0VSX0lEX1dTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlckRldGFpbHM6IFBlcnNvbiA9IHRoaXMuZ2V0UGVyc29uQ2FzdGVkT2JqKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuUEVSU09OX0JZX0lERU5USVRZX1VTRVJfSUQsIEpTT04uc3RyaW5naWZ5KHVzZXJEZXRhaWxzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAqL1xyXG4gICAgZ2V0UGVyc29uQnlVc2VySWQocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPFBlcnNvbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShcclxuICAgICAgICAgICAgb2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX1BFUlNPTl9CWV9VU0VSX0lEX05TLCB0aGlzLkdFVF9QRVJTT05fREVUQUlMU19CWV9VU0VSX0lEX1dTLCBwYXJhbWV0ZXJzKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXJEZXRhaWxzOiBQZXJzb24gPSB0aGlzLmdldFBlcnNvbkNhc3RlZE9iaihkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qICBnZXRQZXJzb25CeVVzZXJJZChwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8UGVyc29uPiB7XHJcbiAgICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QRVJTT05fQllfVVNFUl9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QRVJTT05fQllfVVNFUl9JRCkpKTtcclxuICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfUEVSU09OX0JZX1VTRVJfSURfTlMsIHRoaXMuR0VUX1BFUlNPTl9ERVRBSUxTX0JZX1VTRVJfSURfV1MsIHBhcmFtZXRlcnMpXHJcbiAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1c2VyRGV0YWlsczogUGVyc29uID0gdGhpcy5nZXRQZXJzb25DYXN0ZWRPYmooZGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlBFUlNPTl9CWV9VU0VSX0lELCBKU09OLnN0cmluZ2lmeSh1c2VyRGV0YWlscykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyRGV0YWlscyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7XHJcbiAgICAgfSAqL1xyXG5cclxuICAgIC8qIGdldEFsbEFwcGxpY2F0aW9uRGV0YWlscyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfQVBQTElDQVRJT05fREVUQUlMU19OUywgdGhpcy5HRVRfQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFNfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFsbEFwcGxpY2F0aW9uRGV0YWlscyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFMpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfQVBQTElDQVRJT05fREVUQUlMUykpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFNfTlMsIHRoaXMuR0VUX0FMTF9BUFBMSUNBVElPTl9ERVRBSUxTX1dTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX0FQUExJQ0FUSU9OX0RFVEFJTFMsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJvamVjdENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIHByb2plY3RDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5QUk9KRUNUX0NPTkZJR19OUywgdGhpcy5HRVRfUFJPSkVDVF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldFByb2plY3RDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBwcm9qZWN0Q29uZmlnSWQ6IGlkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5QUk9KRUNUX0NPTkZJR19CWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlBST0pFQ1RfQ09ORklHX0JZX0lEKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlBST0pFQ1RfQ09ORklHX05TLCB0aGlzLkdFVF9QUk9KRUNUX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLlBST0pFQ1RfQ09ORklHX0JZX0lELCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFwcENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGFwcENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkFQUF9DT05GSUdfTlMsIHRoaXMuR0VUX0FQUF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEFwcENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGFwcENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQVBQX0NPTkZJR19CWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFQUF9DT05GSUdfQllfSUQpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuQVBQX0NPTkZJR19OUywgdGhpcy5HRVRfQVBQX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFQUF9DT05GSUdfQllfSUQsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9ICovXHJcblxyXG4gICAgZ2V0QXNzZXRDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBhc3NldENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkFTU0VUX0NPTkZJR19OUywgdGhpcy5HRVRfQVNTRVRfQ09ORklHX0JZX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgZ2V0QXNzZXRDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgIGFzc2V0Q29uZmlnSWQ6IGlkXHJcbiAgICAgICAgIH07XHJcbiAgICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BU1NFVF9DT05GSUdfQllfSUQpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQVNTRVRfQ09ORklHX0JZX0lEKSkpO1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkFTU0VUX0NPTkZJR19OUywgdGhpcy5HRVRfQVNTRVRfQ09ORklHX0JZX0lEX1dTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFTU0VUX0NPTkZJR19CWV9JRCwgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgIH0gKi9cclxuXHJcbiAgICBnZXRDb21tZW50Q29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgY29tbWVudENvbmZpZ0lkOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkNPTU1FTlRfQ09ORklHX05TLCB0aGlzLkdFVF9DT01NRU5UX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogIGdldENvbW1lbnRDb25maWdCeUlkKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgIGNvbW1lbnRDb25maWdJZDogaWRcclxuICAgICAgICAgfTtcclxuICAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkNPTU1FTlRfQ09ORklHX0JZX0lEKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkNPTU1FTlRfQ09ORklHX0JZX0lEKSkpO1xyXG4gICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkNPTU1FTlRfQ09ORklHX05TLCB0aGlzLkdFVF9DT01NRU5UX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DT01NRU5UX0NPTkZJR19CWV9JRCwgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH0pO1xyXG4gICAgIH0gKi9cclxuXHJcbiAgICBnZXRCcmFuZENvbmZpZ0J5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIGJyYW5kQ29uZmlnSWQ6IGlkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuQlJBTkRfQ09ORklHX05TLCB0aGlzLkdFVF9CUkFORF9DT05GSUdfQllfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEJyYW5kQ29uZmlnQnlJZChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgYnJhbmRDb25maWdJZDogaWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkJSQU5EX0NPTkZJR19CWV9JRCkgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkJSQU5EX0NPTkZJR19CWV9JRCkpKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5CUkFORF9DT05GSUdfTlMsIHRoaXMuR0VUX0JSQU5EX0NPTkZJR19CWV9JRF9XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkJSQU5EX0NPTkZJR19CWV9JRCwgSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0gKi9cclxuXHJcbiAgICAvKiBnZXRBbGxWaWV3Q29uZmlnKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVklFV19DT05GSUdfTlMsIHRoaXMuR0VUX0FMTF9NUE1fVklFV19DT05GSUdfV1MsICcnKTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxWaWV3Q29uZmlnKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVklFV19DT05GSUcpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVklFV19DT05GSUcpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVklFV19DT05GSUdfTlMsIHRoaXMuR0VUX0FMTF9NUE1fVklFV19DT05GSUdfV1MsICcnKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1ZJRVdfQ09ORklHLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJzRm9yUm9sZShwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVVNFUl9OUywgJ0dldFVzZXJzRm9yUm9sZScsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE9URFNUaWNrZXRGb3JVc2VyKHBhcmFtZXRlcnMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuV1NBUFBfQ09SRV9OUywgJ0dldE9URFNUaWNrZXRGb3JVc2VyJywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UGVyc29uRGV0YWlsc0J5VXNlckNOKHBhcmFtZXRlcnMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUEVSU09OX05TLCB0aGlzLkdFVF9QRVJTT05fREVUQUlMU19CWV9VU0VSX0lEX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiBnZXRBbGxSb2xlcyhwYXJhbWV0ZXJzPykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NUE1fQVBQX1JPTEVTX05TLCB0aGlzLkdFVF9BTExfUk9MRVNfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFsbFJvbGVzKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUk9MRVMpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfUk9MRVMpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0FQUF9ST0xFU19OUywgdGhpcy5HRVRfQUxMX1JPTEVTX1dTLCBwYXJhbWV0ZXJzKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1JPTEVTLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tFdmVudHMoKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgQ2F0ZWdvcnlMZXZlbDogJ1RBU0snXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX0VWRU5UU19NRVRIT0RfTlMsIHRoaXMuR0VUX01QTV9FVkVOVFNfQllfQ0FURUdPUllfTEVWRUxfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRhc2tXb3JrZmxvd0FjdGlvbnMoKSB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgY2F0ZWdvcnlMZXZlbDogJ1RBU0snXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1dPUktGTE9XX0FDVElPTlNfTUVUSE9EX05TLCB0aGlzLkdFVF9XT1JLRkxPV19BQ1RJT05TX0JZX0NBVEVHT1JZX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTUVESUFfTUFOQUdFUl9DT05GSUdfTlMsIHRoaXMuR0VUX01FRElBX01BTkFHRVJfQ09ORklHX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgICB9ICovXHJcblxyXG4gICAgZ2V0TWVkaWFNYW5hZ2VyQ29uZmlnKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5NRURJQV9NQU5BR0VSX0NPTkZJRykgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk1FRElBX01BTkFHRVJfQ09ORklHKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1FRElBX01BTkFHRVJfQ09ORklHX05TLCB0aGlzLkdFVF9NRURJQV9NQU5BR0VSX0NPTkZJR19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk1FRElBX01BTkFHRVJfQ09ORklHLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRlYW1Sb2xlcyhwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTVNfQlBNX05TLCB0aGlzLkdFVF9URUFNX1JPTEVTX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiAgICAgZ2V0QWxsVGVhbVJvbGVzKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlRFQU1fUk9MRV9NQVBQSU5HX01FVEhPRF9OUywgdGhpcy5HRVRfQUxMX1RFQU1fUk9MRVNfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxUZWFtUm9sZXMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9URUFNX1JPTEVTKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1RFQU1fUk9MRVMpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTV9ST0xFX01BUFBJTkdfTUVUSE9EX05TLCB0aGlzLkdFVF9BTExfVEVBTV9ST0xFU19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9URUFNX1JPTEVTLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIGdldEFsbFRlYW1zKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9URUFNU19OUywgdGhpcy5HRVRfQUxMX1RFQU1TX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH0gKi9cclxuXHJcbiAgICBnZXRBbGxUZWFtcyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1RFQU1TKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1RFQU1TKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfVEVBTVNfTlMsIHRoaXMuR0VUX0FMTF9URUFNU19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5BTExfVEVBTVMsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogZ2V0QWxsUGVyc29ucyhwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9BTExfUEVSU09OU19OUywgdGhpcy5HRVRfQUxMX1BFUlNPTlNfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfSAqL1xyXG5cclxuICAgIGdldEFsbFBlcnNvbnMocGFyYW1ldGVycz8pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZShvYnNlcnZlciA9PiB7XHJcbiAgICAgICAgICAgIGlmIChzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9QRVJTT05TKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuQUxMX1BFUlNPTlMpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9QRVJTT05TX05TLCB0aGlzLkdFVF9BTExfUEVSU09OU19XUywgcGFyYW1ldGVycykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLkFMTF9QRVJTT05TLCBKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFBlcnNvbnNCeUtleXdvcmQodXNlcktleXdvcmQ/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICB1c2VyS2V5d29yZDogdXNlcktleXdvcmQsXHJcbiAgICAgICAgICAgIEN1cnNvcjoge1xyXG4gICAgICAgICAgICAgICAgJ0B4bWxucyc6IHRoaXMuQ1VSU09SX05TLFxyXG4gICAgICAgICAgICAgICAgJ0BvZmZzZXQnOiAwLFxyXG4gICAgICAgICAgICAgICAgJ0BsaW1pdCc6IDMwXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0FMTF9QRVJTT05TX05TLCB0aGlzLkdFVF9QRVJTT05TX0JZX0tFWVdPUkRfV1MsIHBhcmFtZXRlcnMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJzRm9yVGVhbShwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuVEVBTVNfQlBNX05TLCB0aGlzLkdFVF9VU0VTX0ZPUl9URUFNX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2Vyc0ZvclRlYW1Sb2xlKHBhcmFtZXRlcnMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5URUFNU19CUE1fTlMsIHRoaXMuR0VUX1VTRVJTX0ZPUl9URUFNX1JPTEVfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlbGl2ZXJhYmxlUmV2aWV3QnlEZWxpdmVyYWJsZUlEKGlkKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBEZWxpdmVyYWJsZUlEOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkRFTElWRVJBQkxFX1JFVklFV19NRVRIT0RfTlMsIHRoaXMuR0VUX0RFTElWRVJBQkxFX1JFVklFV19CWV9ERUxJVkVSQUJMRV9JRF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGVsaXZlcmFibGVSZXZpZXdCeVRhc2tJRChpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgVGFza0lEOiBpZFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLlRBU0tfUkVWSUVXX01FVEhPRF9OUywgdGhpcy5HRVRfREVMSVZFUkFCTEVfUkVWSUVXX0JZX1RBU0tfSURfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFBvc3NpYmxlQ1JBY3Rpb25zRm9yVXNlcigpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5SRVZJRVdfQlBNX05TLCB0aGlzLkdFVF9QT1NTSUJMRV9DUl9BQ1RJT05TX0ZPUl9VU0VSX1dTLCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuRmlsZVByb29maW5nU2Vzc2lvbihhcHBJZDogc3RyaW5nLCBkZWxpdmVyYWJsZUlkOiBzdHJpbmcsIGFzc2V0SWQ6IHN0cmluZywgdGFza05hbWU6IHN0cmluZywgcmV2aWV3Um9sZUlkOiBzdHJpbmcsIHZlcnNpb25pbmc6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgb3BlbkZpbGVQcm9vZmluZ1Nlc3Npb246IHtcclxuICAgICAgICAgICAgICAgIGFwcGxpY2F0aW9uSWQ6IGFwcElkLFxyXG4gICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZyxcclxuICAgICAgICAgICAgICAgIHJldmlld1JvbGVJZDogcmV2aWV3Um9sZUlkLFxyXG4gICAgICAgICAgICAgICAgdGFza05hbWU6IHRhc2tOYW1lLFxyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGVEZXRhaWxzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVsaXZlcmFibGVJZDogZGVsaXZlcmFibGVJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmVyc2lvbmluZzogdmVyc2lvbmluZ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBjdXN0b21EYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZDogYXNzZXRJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmlld0RlbGl2ZXJhYmxlSWQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUkVWSUVXX0JQTV9OUywgdGhpcy5PUEVOX0ZJTEVfUFJPT0ZJTkdfU0VTU0lPTl9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmlsZVByb29maW5nU2Vzc2lvbihhcHBJZDogc3RyaW5nLCBkZWxpdmVyYWJsZUlkOiBzdHJpbmcsIGFzc2V0SWQ6IHN0cmluZywgdGFza05hbWU6IHN0cmluZywgcmV2aWV3Um9sZUlkOiBzdHJpbmcsIHZlcnNpb25pbmc6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgZ2V0RmlsZVByb29maW5nU2Vzc2lvbjoge1xyXG4gICAgICAgICAgICAgICAgYXBwbGljYXRpb25JZDogYXBwSWQsXHJcbiAgICAgICAgICAgICAgICB2ZXJzaW9uaW5nOiB2ZXJzaW9uaW5nLFxyXG4gICAgICAgICAgICAgICAgcmV2aWV3Um9sZUlkOiByZXZpZXdSb2xlSWQsXHJcbiAgICAgICAgICAgICAgICB0YXNrTmFtZTogdGFza05hbWUsXHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZURldGFpbHM6IHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZUlkOiBkZWxpdmVyYWJsZUlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2ZXJzaW9uaW5nOiB2ZXJzaW9uaW5nXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGN1c3RvbURhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGl2ZXJhYmxlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3NldElkOiBhc3NldElkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV2aWV3RGVsaXZlcmFibGVJZDogJydcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaXNSb290RGVsaXZlcmFibGVTZXNzaW9uOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcGVyc29uOiBudWxsXHJcbiAgICAgICAgICAgICAgICAvLyB7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgZmlyc3ROYW1lOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VyRGlzcGxheU5hbWUoKSAsXHJcbiAgICAgICAgICAgICAgICAvLyAgICAgbGFzdE5hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIGlkOiB0aGlzLnNoYXJpbmdTZXJ2aWNlLmdldEN1cnJlbnRVc2VySXRlbUlEKClcclxuICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5SRVZJRVdfQlBNX05TLCB0aGlzLkdFVF9GSUxFX1BST09GSU5HX1NFU1NJT05fV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuXHJcblxyXG4gICAgZ2V0TXVsdGlwbGVGaWxlUHJvb2ZpbmdTZXNzaW9uKGFwcElkOiBzdHJpbmcsIGRlbGl2ZXJhYmxlOiBzdHJpbmcsIGFzc2V0SWQ6IHN0cmluZywgdGFza05hbWU6IHN0cmluZywgcmV2aWV3Um9sZUlkOiBzdHJpbmcsIHZlcnNpb25pbmc6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgZ2V0RmlsZVByb29maW5nU2Vzc2lvbjoge1xyXG4gICAgICAgICAgICAgICAgYXBwbGljYXRpb25JZDogYXBwSWQsXHJcbiAgICAgICAgICAgICAgICB2ZXJzaW9uaW5nOiB2ZXJzaW9uaW5nLFxyXG4gICAgICAgICAgICAgICAgcmV2aWV3Um9sZUlkOiByZXZpZXdSb2xlSWQsXHJcbiAgICAgICAgICAgICAgICB0YXNrTmFtZTogdGFza05hbWUsXHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZURldGFpbHM6IHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZTogZGVsaXZlcmFibGVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBjdXN0b21EYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZDogYXNzZXRJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmlld0RlbGl2ZXJhYmxlSWQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlzUm9vdERlbGl2ZXJhYmxlU2Vzc2lvbjogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHBlcnNvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgLy8ge1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIGZpcnN0TmFtZTogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckRpc3BsYXlOYW1lKCkgLFxyXG4gICAgICAgICAgICAgICAgLy8gICAgIGxhc3ROYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIC8vICAgICBpZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50VXNlckl0ZW1JRCgpXHJcbiAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuUkVWSUVXX0JQTV9OUywgdGhpcy5HRVRfRklMRV9QUk9PRklOR19TRVNTSU9OX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBidWxrRGVsaXZlcmFibGVJZChkZWxpdmVyYWJsZUlkLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSkge1xyXG4gICAgICAgIGNvbnN0IGRlbGl2ZXJhYmxlcyA9IFtdO1xyXG4gICAgICAgIGRlbGl2ZXJhYmxlSWQuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgICAgICAgZGVsaXZlcmFibGVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgZGVsaXZlcmFibGVJZDogZWxlbWVudCxcclxuICAgICAgICAgICAgICAgIHZlcnNpb25pbmc6IHZlcnNpb25pbmcsXHJcbiAgICAgICAgICAgICAgICB0YXNrTmFtZTogdGFza05hbWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGRlbGl2ZXJhYmxlcztcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVDUkFwcHJvdmFsKGFwcElkOiBzdHJpbmcsIGRlbGl2ZXJhYmxlSWQ6IHN0cmluZyB8IEFycmF5PHN0cmluZz4sIGFzc2V0SWQ6IHN0cmluZywgdGFza05hbWU6IHN0cmluZywgcmV2aWV3Um9sZUlkOiBzdHJpbmcsIHZlcnNpb25pbmc6IHN0cmluZyxcclxuICAgICAgICBjb21tZW50VGV4dDogc3RyaW5nLCBpc0J1bGtBcHByb3ZhbDogYm9vbGVhbiwgc3RhdHVzSWQ6IHN0cmluZywgc2VsZWN0ZWRSZWFzb25zRGF0YSwgdXNlckNOKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBTZXRTdGF0dXNBcHByb3ZhbDoge1xyXG4gICAgICAgICAgICAgICAgYXBwbGljYXRpb25JZDogYXBwSWQsXHJcbiAgICAgICAgICAgICAgICB2ZXJzaW9uaW5nOiB2ZXJzaW9uaW5nLFxyXG4gICAgICAgICAgICAgICAgcmV2aWV3Um9sZUlkOiByZXZpZXdSb2xlSWQsXHJcbiAgICAgICAgICAgICAgICB0YXNrTmFtZTogdGFza05hbWUsXHJcbiAgICAgICAgICAgICAgICBzdGF0dXNJZDogc3RhdHVzSWQsXHJcbiAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZURldGFpbHM6IHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZTogdGhpcy5idWxrRGVsaXZlcmFibGVJZChkZWxpdmVyYWJsZUlkLCB2ZXJzaW9uaW5nLCB0YXNrTmFtZSlcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBjdXN0b21EYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWxpdmVyYWJsZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzZXRJZDogYXNzZXRJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldmlld0RlbGl2ZXJhYmxlSWQ6ICcnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgY29tbWVudFRleHQ6IGNvbW1lbnRUZXh0LFxyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRSZWFzb25zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgQXBwcm92YWxSZWFzb246IHNlbGVjdGVkUmVhc29uc0RhdGFcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBpc0J1bGtBcHByb3ZhbDogaXNCdWxrQXBwcm92YWwsXHJcbiAgICAgICAgICAgICAgICByZXZpZXdlclVzZXJDTjogdXNlckNOXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5CVUxLX0NSX0FQUFJPVkFMX05TLCB0aGlzLkhBTkRFTF9DUl9BUFBST1ZBTF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyogIGdldEN1cnJlbnRVc2VyUHJlZmVyZW5jZSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICBVc2VySWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Y3VycmVudFVzZXJJdGVtSUQoKVxyXG4gICAgICAgICB9O1xyXG4gICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1VTRVJfUFJFRkVSRU5DRV9OUywgdGhpcy5HRVRfTVBNX1VTRVJfUFJFRkVSRU5DRV9CWV9VU0VSX1dTLCBwYXJhbWV0ZXIpO1xyXG4gICAgIH0gKi9cclxuXHJcbiAgICBnZXRDdXJyZW50VXNlclByZWZlcmVuY2UoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgIFVzZXJJZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRjdXJyZW50VXNlckl0ZW1JRCgpXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UpICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UpKSk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1VTRVJfUFJFRkVSRU5DRV9OUywgdGhpcy5HRVRfTVBNX1VTRVJfUFJFRkVSRU5DRV9CWV9VU0VSX1dTLCBwYXJhbWV0ZXIpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShTZXNzaW9uU3RvcmFnZUNvbnN0YW50cy5DVVJSRU5UX01QTV9VU0VSX1BSRUZFUkVOQ0UsIEpTT04uc3RyaW5naWZ5KHJlc3BvbnNlKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlclByZWZlcmVuY2VSZXNwb25zZSA9IHJlc3BvbnNlLk1QTV9Vc2VyX1ByZWZlcmVuY2UgPyByZXNwb25zZS5NUE1fVXNlcl9QcmVmZXJlbmNlIDogW107XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh1c2VyUHJlZmVyZW5jZVJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVVc2VyUHJlZmVyZW5jZShyZXF1ZXN0T2JqLCBleGlzdGluZ1VzZXJQcmVmZXJlbmNlSWQpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICBsZXQgcGFyYW1ldGVyO1xyXG4gICAgICAgICAgICBpZiAoZXhpc3RpbmdVc2VyUHJlZmVyZW5jZUlkKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ01QTV9Vc2VyX1ByZWZlcmVuY2UtaWQnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIElkOiBleGlzdGluZ1VzZXJQcmVmZXJlbmNlSWRcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICdNUE1fVXNlcl9QcmVmZXJlbmNlLXVwZGF0ZSc6IHJlcXVlc3RPYmpcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXIgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJ01QTV9Vc2VyX1ByZWZlcmVuY2UtY3JlYXRlJzogcmVxdWVzdE9ialxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NUE1fVVNFUl9QUkVGRVJFTkNFX05TLCBleGlzdGluZ1VzZXJQcmVmZXJlbmNlSWQgPyB0aGlzLlVQREFURV9NUE1fVVNFUl9QUkVGRVJFTkNFX1dTIDogdGhpcy5DUkVBVEVfTVBNX1VTRVJfUFJFRkVSRU5DRV9XUywgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLk1QTV9Vc2VyX1ByZWZlcmVuY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNoYXJpbmdTZXJ2aWNlLnVwZGF0ZUN1cnJlbnRVc2VyUHJlZmVyZW5jZShyZXNwb25zZS5NUE1fVXNlcl9QcmVmZXJlbmNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZURlZmF1bHRVc2VyUHJlZmVyZW5jZSh2aWV3SWQsIGZpbHRlck5hbWUsIG1lbnVJZCwgZmllbGREYXRhKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgVmlld0lkOiB2aWV3SWQsXHJcbiAgICAgICAgICAgICAgICBGaWx0ZXJOYW1lOiBmaWx0ZXJOYW1lLFxyXG4gICAgICAgICAgICAgICAgUGVyc29uSWQ6IHRoaXMuc2hhcmluZ1NlcnZpY2UuZ2V0Q3VycmVudFBlcnNvbigpLklkLFxyXG4gICAgICAgICAgICAgICAgTWVudUlkOiBtZW51SWQsXHJcbiAgICAgICAgICAgICAgICBGaWVsZERhdGE6IGZpZWxkRGF0YVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9XT1JLRkxPV19OUywgdGhpcy5VUERBVEVfREVGQVVMVF9VU0VSX1BSRUZFUkVOQ0VfV1MsIHBhcmFtZXRlcikuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZSAmJiByZXNwb25zZS5BUElSZXNwb25zZS5zdGF0dXNDb2RlID09PSAnMjAwJyAmJiByZXNwb25zZS5BUElSZXNwb25zZS5kYXRhICYmIHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuTVBNX1VzZXJfUHJlZmVyZW5jZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hhcmluZ1NlcnZpY2UudXBkYXRlQ3VycmVudFVzZXJQcmVmZXJlbmNlKHJlc3BvbnNlLkFQSVJlc3BvbnNlLmRhdGEuTVBNX1VzZXJfUHJlZmVyZW5jZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVVc2VyUHJlZmVyZW5jZSh1c2VyUHJlZmVyZW5jZUlkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0ge1xyXG4gICAgICAgICAgICAgICAgTVBNX1VzZXJfUHJlZmVyZW5jZToge1xyXG4gICAgICAgICAgICAgICAgICAgICdNUE1fVXNlcl9QcmVmZXJlbmNlLWlkJzoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBJZDogdXNlclByZWZlcmVuY2VJZFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX1VTRVJfUFJFRkVSRU5DRV9OUywgdGhpcy5ERUxFVEVfTVBNX1VTRVJfUFJFRkVSRU5DRV9XUywgcGFyYW1ldGVyKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaGFyaW5nU2VydmljZS5yZW1vdmVDdXJyZW50VXNlclByZWZlcmVuY2UodXNlclByZWZlcmVuY2VJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKlxyXG4gICAgTVBNVjMtMTQ0OFxyXG4gICAgKi9cclxuICAgIEdldEVtYWlsRXZlbnRzRm9yTWFuYWdlcihwYXJhbWV0ZXJzPyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9FTUFJTF9FVkVOVFNfRk9SX01BTkFHRVJfTlMsIHRoaXMuR0VUX0VNQUlMX0VWRU5UU19GT1JfTUFOQUdFUl9XUywgJycpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qXHJcbiAgICBNUE1WMy0xNDQ4XHJcbiAgICAqL1xyXG4gICAgR2V0RW1haWxFdmVudHNGb3JVc2VyKHBhcmFtZXRlcnM/KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX0VNQUlMX0VWRU5UU19GT1JfVVNFUl9OUywgdGhpcy5HRVRfRU1BSUxfRVZFTlRTX0ZPUl9VU0VSX1dTLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgR2V0TWFpbFByZWZlcmVuY2VCeVVzZXJJZCh1c2VySWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuR0VUX01BSUxfUFJFRkVSRU5DRV9CWV9VU0VSX0lEX05TLCB0aGlzLkdFVF9NQUlMX1BSRUZFUkVOQ0VfQllfVVNFUl9JRF9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgR2V0VXNlclByZWZlcmVuY2VNYWlsVGVtcGxhdGVzKHVzZXJJZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgdXNlcklkOiB1c2VySWRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5HRVRfVVNFUl9QUkVGRVJFTkNFX01BSUxfVEVNUExBVEVTX05TLCB0aGlzLkdFVF9VU0VSX1BSRUZFUkVOQ0VfTUFJTF9URU1QTEFURVNfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIE1haWxQcmVmZXJlbmNlKHVzZXJJZCwgZW5hYmxlZEV2ZW50cywgZGlzYWJsZWRFdmVudHMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIFVzZXJJZDogdXNlcklkLFxyXG4gICAgICAgICAgICBFbmFibGVkRXZlbnRzOiB7XHJcbiAgICAgICAgICAgICAgICAvLyBlbWFpbDogZW5hYmxlZEV2ZW50c1xyXG4gICAgICAgICAgICAgICAgJ0VtYWlsX0V2ZW50X1RlbXBsYXRlLWlkJzogZW5hYmxlZEV2ZW50c1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBEaXNhYmxlZEV2ZW50czoge1xyXG4gICAgICAgICAgICAgICAgLy8gZGlzYWJsZTogZGlzYWJsZWRFdmVudHNcclxuICAgICAgICAgICAgICAgICdFbWFpbF9FdmVudF9UZW1wbGF0ZS1pZCc6IGRpc2FibGVkRXZlbnRzXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmludm9rZVJlcXVlc3QodGhpcy5NUE1fV09SS0ZMT1dfTlMsIHRoaXMuTUFJTF9QUkVGRVJFTkNFX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBEZWxldGVNUE1fRW1haWxfUHJlZmVyZW5jZSh1c2VyRW1haWxQcmVmZXJlbmNlSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIE1QTV9FbWFpbF9QcmVmZXJlbmNlOiB7XHJcbiAgICAgICAgICAgICAgICAnTVBNX0VtYWlsX1ByZWZlcmVuY2UtaWQnOiB1c2VyRW1haWxQcmVmZXJlbmNlSWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9NUE1fRU1BSUxfUFJFRkVSRU5DRV9OUywgdGhpcy5ERUxFVEVfTVBNX0VNQUlMX1BSRUZFUkVOQ0VfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4cG9ydERhdGEoY29sdW1ucywgZmlsZU5hbWUsIGV4cG9ydFR5cGUsIGl0ZW1JZCwgZmlsZVBhdGgsIHNlYXJjaFJlcXVlc3QsIHBhZ2VMaW1pdCwgdG90YWxDb3VudCwgZXhwb3J0RGF0YU1pbkNvdW50KSB7XHJcbiAgICAgICAgY29uc3Qgb3Rkc1RpY2tldCA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuT1REU19USUNLRVQpO1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRVc2VySWQgPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFNlc3Npb25TdG9yYWdlQ29uc3RhbnRzLk9URFNfVVNFUl9JRCk7XHJcbiAgICAgICAgY29uc3QgcGFyYW1ldGVycyA9IHtcclxuICAgICAgICAgICAgY29sdW1uczogY29sdW1ucyxcclxuICAgICAgICAgICAgZmlsZU5hbWU6IGZpbGVOYW1lLFxyXG4gICAgICAgICAgICBleHBvcnRUeXBlOiBleHBvcnRUeXBlLFxyXG4gICAgICAgICAgICBmaWxlUGF0aDogZmlsZVBhdGgsXHJcbiAgICAgICAgICAgIEluZGV4ZXJTZWFyY2hSZXF1ZXN0OiBzZWFyY2hSZXF1ZXN0LFxyXG4gICAgICAgICAgICBwYWdlTGltaXQ6IHBhZ2VMaW1pdCxcclxuICAgICAgICAgICAgdG90YWxDb3VudDogdG90YWxDb3VudCxcclxuICAgICAgICAgICAgZXhwb3J0RGF0YU1pbkNvdW50OiBleHBvcnREYXRhTWluQ291bnQsXHJcbiAgICAgICAgICAgIG90ZHNUaWNrZXQ6IChvdGRzVGlja2V0ICYmIG90ZHNUaWNrZXQgIT0gbnVsbCkgPyBKU09OLnBhcnNlKG90ZHNUaWNrZXQpIDogXCJcIixcclxuICAgICAgICAgICAgY3VycmVudFVzZXJJZDogKGN1cnJlbnRVc2VySWQgJiYgY3VycmVudFVzZXJJZCAhPSBudWxsKSA/IEpTT04ucGFyc2UoY3VycmVudFVzZXJJZCkgOiBcIlwiLFxyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBJVEVNX0lEOiBpdGVtSWRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLk1QTV9XT1JLRkxPV19OUywgdGhpcy5FWFBPUlRfREFUQV9XUywgcGFyYW1ldGVycyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RXhwb3J0RGF0YUJ5VXNlcih1c2VyQ04pIHtcclxuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0ge1xyXG4gICAgICAgICAgICBVc2VyQ046IHVzZXJDTlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkdFVF9FWFBPUlRfREFUQV9CWV9VU0VSX05TLCB0aGlzLkdFVF9FWFBPUlRfREFUQV9CWV9VU0VSX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLy8gTVBNVjMtMjM2OFxyXG4gICAgZGVsZXRlRXhwb3J0RGF0YUJ5SWQoaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIE1QTV9FeHBvcnRfRGF0YToge1xyXG4gICAgICAgICAgICAgICAgJ01QTV9FeHBvcnRfRGF0YS1pZCc6IHtcclxuICAgICAgICAgICAgICAgICAgICBJZDogaWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkRFTEVURV9NUE1fRVhQT1JUX0RBVEFfQllfSURfTlMsIHRoaXMuREVMRVRFX01QTV9FWFBPUlRfREFUQV9CWV9JRF9XUywgcGFyYW1ldGVycyk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldEZpbGVFbmNvZGVkU3RyaW5nKGZpbGVQYXRoOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IHtcclxuICAgICAgICAgICAgZmlsZVBhdGg6IGZpbGVQYXRoXHJcblxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuRE9XTkxPQURfRklMRV9CWV9QQVRIX05TLCB0aGlzLkRPV05MT0FEX0ZJTEVfQllfUEFUSF9XUywgcGFyYW1ldGVyKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROb3RpZmljYXRpb25Gb3JDdXJyZW50VXNlcihza2lwLCB0b3ApOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtZXRlcnMgPSB7XHJcbiAgICAgICAgICAgIFVzZXJJZDogdGhpcy5zaGFyaW5nU2VydmljZS5nZXRDdXJyZW50UGVyc29uKCkuSWQsXHJcbiAgICAgICAgICAgIEN1cnNvcjoge1xyXG4gICAgICAgICAgICAgICAgJ0B4bWxucyc6IHRoaXMuQ1VSU09SX05TLFxyXG4gICAgICAgICAgICAgICAgJ0BvZmZzZXQnOiBza2lwLFxyXG4gICAgICAgICAgICAgICAgJ0BsaW1pdCc6IHRvcFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX05PVElGSUNBVElPTl9OUywgdGhpcy5HRVRfTk9USUZJQ0FUSU9OX0JZX1VTRVJfV1MsIHBhcmFtZXRlcnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZU5vdGlmaWNhdGlvbihwYXJhbWV0ZXJzKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbnZva2VSZXF1ZXN0KHRoaXMuTVBNX05PVElGSUNBVElPTl9OUywgdGhpcy5VUERBVEVfTk9USUZJQ0FUSU9OX1dTLCBwYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZWZhdWx0RW1haWxDb25maWcoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuRU1BSUxfQ09ORklHKSAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oU2Vzc2lvblN0b3JhZ2VDb25zdGFudHMuRU1BSUxfQ09ORklHKSkpO1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW52b2tlUmVxdWVzdCh0aGlzLkVNQUlMX0NPTkZJR19OUywgdGhpcy5HRVRfREVGQVVMVF9FTUFJTF9DT05GSUdfV1MsIG51bGwpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLkdlbmVyYWxfRW1haWxfQ29uZmlnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQocmVzcG9uc2UuR2VuZXJhbF9FbWFpbF9Db25maWcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==