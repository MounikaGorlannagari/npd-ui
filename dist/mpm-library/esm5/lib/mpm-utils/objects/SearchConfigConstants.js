export var SearchConfigConstants = {
    SEARCH_NAME: {
        PROJECT: 'PM_PROJECT_VIEW',
        TASK: 'PM_TASK_VIEW',
        DELIVERABLE: 'PM_DELIVERABLE_VIEW',
        MY_TASK_GROUP_BY_PROJECT: 'MEMBER_PROJECT_VIEW',
        MY_TASK_GROUP_BY_TASK: 'MEMBER_TASK_VIEW',
        PROJECT_TEMPLATE: 'PM_TEMPLATE_VIEW',
        MY_TASK_GROUP_BY_DELIVERABLE: 'MEMBER_DELIVERABLE_VIEW',
        ASSET_DETAILS: 'ASSET_DETAILS_VIEW',
        CAMPAIGN: 'PM_CAMPAIGN_VIEW',
        MY_TASK_GROUP_BY_CAMPAIGN: 'MEMBER_CAMPAIGN_VIEW',
        RM_GROUP_BY_CAMPAIGN: 'RESOURCE_MANAGEMENT_CAMPAIGN_VIEW',
        RM_GROUP_BY_PROJECT: 'RESOURCE_MANAGEMENT_PROJECT_VIEW',
        RM_GROUP_BY_TASK: 'RESOURCE_MANAGEMENT_TASK_VIEW',
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2VhcmNoQ29uZmlnQ29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXBtLWxpYnJhcnkvIiwic291cmNlcyI6WyJsaWIvbXBtLXV0aWxzL29iamVjdHMvU2VhcmNoQ29uZmlnQ29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sQ0FBQyxJQUFNLHFCQUFxQixHQUFHO0lBQ2pDLFdBQVcsRUFBRTtRQUNULE9BQU8sRUFBRSxpQkFBaUI7UUFDMUIsSUFBSSxFQUFFLGNBQWM7UUFDcEIsV0FBVyxFQUFFLHFCQUFxQjtRQUNsQyx3QkFBd0IsRUFBRSxxQkFBcUI7UUFDL0MscUJBQXFCLEVBQUUsa0JBQWtCO1FBQ3pDLGdCQUFnQixFQUFFLGtCQUFrQjtRQUNwQyw0QkFBNEIsRUFBRSx5QkFBeUI7UUFDdkQsYUFBYSxFQUFFLG9CQUFvQjtRQUNuQyxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLHlCQUF5QixFQUFFLHNCQUFzQjtRQUNqRCxvQkFBb0IsRUFBRSxtQ0FBbUM7UUFDekQsbUJBQW1CLEVBQUUsa0NBQWtDO1FBQ3ZELGdCQUFnQixFQUFFLCtCQUErQjtLQUNwRDtDQUNKLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgU2VhcmNoQ29uZmlnQ29uc3RhbnRzID0ge1xyXG4gICAgU0VBUkNIX05BTUU6IHtcclxuICAgICAgICBQUk9KRUNUOiAnUE1fUFJPSkVDVF9WSUVXJyxcclxuICAgICAgICBUQVNLOiAnUE1fVEFTS19WSUVXJyxcclxuICAgICAgICBERUxJVkVSQUJMRTogJ1BNX0RFTElWRVJBQkxFX1ZJRVcnLFxyXG4gICAgICAgIE1ZX1RBU0tfR1JPVVBfQllfUFJPSkVDVDogJ01FTUJFUl9QUk9KRUNUX1ZJRVcnLFxyXG4gICAgICAgIE1ZX1RBU0tfR1JPVVBfQllfVEFTSzogJ01FTUJFUl9UQVNLX1ZJRVcnLFxyXG4gICAgICAgIFBST0pFQ1RfVEVNUExBVEU6ICdQTV9URU1QTEFURV9WSUVXJyxcclxuICAgICAgICBNWV9UQVNLX0dST1VQX0JZX0RFTElWRVJBQkxFOiAnTUVNQkVSX0RFTElWRVJBQkxFX1ZJRVcnLFxyXG4gICAgICAgIEFTU0VUX0RFVEFJTFM6ICdBU1NFVF9ERVRBSUxTX1ZJRVcnLFxyXG4gICAgICAgIENBTVBBSUdOOiAnUE1fQ0FNUEFJR05fVklFVycsXHJcbiAgICAgICAgTVlfVEFTS19HUk9VUF9CWV9DQU1QQUlHTjogJ01FTUJFUl9DQU1QQUlHTl9WSUVXJyxcclxuICAgICAgICBSTV9HUk9VUF9CWV9DQU1QQUlHTjogJ1JFU09VUkNFX01BTkFHRU1FTlRfQ0FNUEFJR05fVklFVycsXHJcbiAgICAgICAgUk1fR1JPVVBfQllfUFJPSkVDVDogJ1JFU09VUkNFX01BTkFHRU1FTlRfUFJPSkVDVF9WSUVXJyxcclxuICAgICAgICBSTV9HUk9VUF9CWV9UQVNLOiAnUkVTT1VSQ0VfTUFOQUdFTUVOVF9UQVNLX1ZJRVcnLFxyXG4gICAgfVxyXG59O1xyXG4iXX0=