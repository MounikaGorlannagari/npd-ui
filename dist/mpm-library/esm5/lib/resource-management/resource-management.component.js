import { __decorate } from "tslib";
import { Component } from '@angular/core';
var ResourceManagementComponent = /** @class */ (function () {
    function ResourceManagementComponent() {
    }
    ResourceManagementComponent.prototype.ngOnInit = function () {
    };
    ResourceManagementComponent = __decorate([
        Component({
            selector: 'mpm-resource-management',
            template: "<mpm-resource-management-toolbar></mpm-resource-management-toolbar>",
            styles: [""]
        })
    ], ResourceManagementComponent);
    return ResourceManagementComponent;
}());
export { ResourceManagementComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb3VyY2UtbWFuYWdlbWVudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9tcG0tbGlicmFyeS8iLCJzb3VyY2VzIjpbImxpYi9yZXNvdXJjZS1tYW5hZ2VtZW50L3Jlc291cmNlLW1hbmFnZW1lbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBT2xEO0lBRUU7SUFBZ0IsQ0FBQztJQUVqQiw4Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUxVLDJCQUEyQjtRQUx2QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLCtFQUFtRDs7U0FFcEQsQ0FBQztPQUNXLDJCQUEyQixDQU92QztJQUFELGtDQUFDO0NBQUEsQUFQRCxJQU9DO1NBUFksMkJBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnbXBtLXJlc291cmNlLW1hbmFnZW1lbnQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9yZXNvdXJjZS1tYW5hZ2VtZW50LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9yZXNvdXJjZS1tYW5hZ2VtZW50LmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUmVzb3VyY2VNYW5hZ2VtZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=